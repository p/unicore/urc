<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="de.fzj.unicore.rcp.identity" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appInfo>
         <meta.schema plugin="de.fzj.unicore.rcp.identity" id="de.fzj.unicore.rcp.identity.Credentials" name="CredentialExtension"/>
      </appInfo>
      <documentation>
         This extension point allows for introducing new types of credentials used for communication with Grid services. Each extension will lead to the creation of another column in the security profile table (in the &quot;Security Profiles&quot; view). The extension must provide a cell editor that can be used to modify the settings for the new credential type. The extending plugin can store these settings in the user data of the Profile object or to some location defined by itself. Note: the Profile object has to be (de-)serializable using Xstream which means that Objects with Java types unknown to the identity plugin must not be stored in the Profile&apos;s user data. Try to restrict yourself to native Java types. For storing complex data for a given security profile, the extending plugin should therefore have its own persistence mechanism.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appInfo>
            <meta.element />
         </appInfo>
      </annotation>
      <complexType>
         <sequence>
            <element ref="credentialType"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="credentialType">
      <complexType>
         <sequence>
            <element ref="processBefore" minOccurs="0" maxOccurs="unbounded"/>
            <element ref="processAfter" minOccurs="0" maxOccurs="unbounded"/>
         </sequence>
         <attribute name="label" type="string">
            <annotation>
               <documentation>
                  Provides a title for the header of the new column that is added to the profile list. If this attribute is not set, no additional column is created!
               </documentation>
            </annotation>
         </attribute>
         <attribute name="implementingClass" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":de.fzj.unicore.rcp.identity.extensionpoints.ICredentialTypeExtensionPoint"/>
               </appInfo>
            </annotation>
         </attribute>
         <attribute name="tooltip" type="string">
            <annotation>
               <documentation>
                  A tooltip to be displayed in the security profiles table.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="initialColumnWidth" type="string">
            <annotation>
               <documentation>
                  Initial width of the contributed column in the security profiles table.
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="processBefore">
      <annotation>
         <documentation>
            These extensions must be processed before the contributed extension is.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  The id of the other extension (= the value of the id attribute of the other extension element).
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="processAfter">
      <annotation>
         <documentation>
            These extensions must be processed after the contributed extension is.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  The id of the other extension (= the value of the id attribute of the other extension element).
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         [Enter the first release in which this extension point appears.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         [Enter extension point usage example here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         [Enter information about supplied implementation of this extension point.]
      </documentation>
   </annotation>


   <annotation>
      <appInfo>
         <meta.section type="apiInfo"/>
      </appInfo>
      <documentation>
         [Enter API information here.]
      </documentation>
   </annotation>

</schema>
