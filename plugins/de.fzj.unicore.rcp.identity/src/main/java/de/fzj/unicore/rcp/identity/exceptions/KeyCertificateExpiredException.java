package de.fzj.unicore.rcp.identity.exceptions;

import javax.security.cert.CertificateExpiredException;

public class KeyCertificateExpiredException extends CertificateExpiredException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4215655961444639668L;
	private String alias;

	public KeyCertificateExpiredException(String msg, String alias) {
		super(msg);
		this.alias = alias;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

}
