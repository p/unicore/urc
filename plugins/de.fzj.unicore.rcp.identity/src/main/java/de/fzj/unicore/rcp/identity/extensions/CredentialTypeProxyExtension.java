package de.fzj.unicore.rcp.identity.extensions;

import java.util.List;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.common.utils.ArrayUtils;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.extensionpoints.ICredentialTypeExtensionPoint;
import de.fzj.unicore.rcp.identity.profiles.Profile;
import de.fzj.unicore.rcp.identity.profiles.ProfileList;
import de.fzj.unicore.uas.security.ProxyCertOutHandler;
import eu.unicore.util.httpclient.ClientProperties;
import eu.unicore.util.httpclient.IClientConfiguration;

public class CredentialTypeProxyExtension implements
		ICredentialTypeExtensionPoint {

	public static final String CREATE_PROXY = "create Proxy Certificate";


	public void addCredentials(IClientConfiguration secProps,
			Profile profile, List<String> identifiers) {

		ClientProperties props=(ClientProperties)secProps;
		
		if (profile != null && getCreateProxy(profile)) {
			String[] outHandlers = secProps.getOutHandlerClassNames();
			String handlerName = ProxyCertOutHandler.class.getName();
			
			if(!ArrayUtils.contains(outHandlers, handlerName)){
				outHandlers=ArrayUtils.concat(outHandlers, handlerName);
			}
			props.setOutHandlerClassNames(outHandlers);
			props.getExtraSecurityTokens().put(IdentityActivator.DEFAULT_PROPERTIES,
					Boolean.FALSE.toString());
		}
	}

	public void dispose() {

	}

	public CellEditor getCellEditor(Composite parent, Profile profile) {
		return new CreateProxyCellEditor(parent);
	}

	public Image getImage(Profile profile) {
		return null;
	}

	public String getLabel(Profile profile) {
		return String.valueOf(getCreateProxy(profile));
	}

	public void initProfile(Profile newProfile) {
		setCreateProxy(newProfile, false);
	}

	public void setProfileList(ProfileList profileList) {

	}

	public static boolean getCreateProxy(Profile profile) {
		Boolean result = (Boolean) profile.getUserData(CREATE_PROXY);
		if (result == null) {
			return false;
		}
		return result.booleanValue();
	}

	public static void setCreateProxy(Profile profile, boolean b) {
		profile.setUserData(CREATE_PROXY, b);
	}

}
