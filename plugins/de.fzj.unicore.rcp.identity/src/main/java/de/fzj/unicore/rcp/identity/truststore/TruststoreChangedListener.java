package de.fzj.unicore.rcp.identity.truststore;

/**
 * A simple interface to notify a listener of a changed TrustStore.
 * 
 * @author bjoernh
 * 
 */
public interface TruststoreChangedListener {
	/**
	 * The action usually triggered by this is to refresh the truststore view.
	 */
	public void truststoreChanged();
}
