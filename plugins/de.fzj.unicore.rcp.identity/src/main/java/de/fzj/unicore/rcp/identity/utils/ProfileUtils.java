package de.fzj.unicore.rcp.identity.utils;

import java.util.List;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.extensions.CredentialTypeCertificateExtension;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.profiles.Profile;
import de.fzj.unicore.rcp.identity.profiles.ProfileList;
import de.fzj.unicore.rcp.identity.sites.Site;
import de.fzj.unicore.rcp.identity.sites.SiteList;

public class ProfileUtils {

	public static void addSecurityProfile(String profileName, String path,
			Certificate cert, boolean changeExistingProfile,
			boolean changeExistingSite) throws Exception {
		ProfileList profiles = IdentityActivator.getDefault().getProfileList();
		Profile p = profiles.getProfile(profileName);
		if (p != null) {
			// security profile with this name exists!
			if (changeExistingProfile) {
				CredentialTypeCertificateExtension.setCertificate(p, cert);
				profiles.updateProfile(p);
			}
		} else {
			p = new Profile();
			IdentityActivator.getDefault().getCredentialTypeRegistry()
					.initProfile(p);
			p.setName(profileName);
			CredentialTypeCertificateExtension.setCertificate(p, cert);
			profiles.addProfile(p);
		}

		SiteList sites = IdentityActivator.getDefault().getSiteList();
		Site s = sites.getSite(path);
		if (s != null) {
			// security profile for this site already set
			if (changeExistingSite) {
				s.setProfileName(profileName);
				sites.updateSite(s);
			}
		} else {
			s = new Site();
			s.setPattern(path);
			s.setProfileName(profileName);
			sites.addSite(s);
		}
	}

	public static String computeLongestPath(List<String> paths) {
		String path = null;
		int longest = -1;
		for (String p : paths) {
			int length = ProfileUtils.computePathLength(p);
			if (length > longest) {
				path = p;
				longest = length;
			}
		}
		return path;
	}

	public static int computePathLength(String path) {
		// matching against name path: use path length
		// and ignore the number of characters in names
		return path.split("/").length - 1;
	}

}
