/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.identity.truststore;

import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;

import de.fzj.unicore.rcp.identity.keystore.Certificate;

public class CertSorter extends ViewerSorter {
	// Criteria that the instance uses
	private int criteria;
	/**
	 * Constructor argument values that indicate to sort items
	 */
	public final static int NAME = 1;
	public final static int CERT = 2;

	/**
	 * Creates a new viewer sorter, which uses the given collator to sort
	 * strings.
	 * 
	 * @param collator
	 *            the collator to use to sort strings
	 */
	public CertSorter(Collator collator) {
		super(collator);
		this.criteria = CERT; // sort by Cert DN by default
	}

	/**
	 * Creates a new viewer sorter, which uses the default collator to sort
	 * strings.
	 */
	public CertSorter(int criteria) {
		this(Collator.getInstance());
		this.criteria = criteria;
	}

	/**
	 * Returns the category of the given element. The category is a number used
	 * to allocate elements to bins; the bins are arranged in ascending numeric
	 * order. The elements within a bin are arranged via a second level sort
	 * criterion.
	 * <p>
	 * The default implementation of this framework method returns
	 * <code>0</code>. Subclasses may reimplement this method to provide
	 * non-trivial categorization.
	 * </p>
	 * 
	 * @param element
	 *            the element
	 * @return the category
	 */
	@Override
	public int category(Object element) {
		return 0;
	}

	/*
	 * (non-Javadoc) Method declared on ViewerSorter.
	 */
	@Override
	public int compare(Viewer viewer, Object o1, Object o2) {

		Certificate c1 = (Certificate) o1;
		Certificate c2 = (Certificate) o2;

		switch (criteria) {
		case NAME:
			return compareString(c1.getName(), c2.getName());
		case CERT:
			return compareString(c1.getSubjectDN(), c2.getSubjectDN());
		default:
			return 0;
		}
	}

	private int compareString(String u1String, String u2String) {
		return collator.compare(u1String, u2String);
	}

	/**
	 * Returns the collator used to sort strings.
	 * 
	 * @return the collator used to sort strings
	 */
	@Override
	public Collator getCollator() {
		return collator;
	}

	/**
	 * Returns whether this viewer sorter would be affected by a change to the
	 * given property of the given element.
	 * 
	 * @param element
	 *            the element
	 * @param property
	 *            the property
	 * @return <code>true</code> if the sorting would be affected, and
	 *         <code>false</code> if it would be unaffected
	 */
	@Override
	public boolean isSorterProperty(Object element, String property) {
		return false;
	}

	/**
	 * Sorts the given elements in-place, modifying the given array.
	 * <p>
	 * The default implementation of this method uses the java.util.Arrays#sort
	 * algorithm on the given array, calling <code>compare</code> to compare
	 * elements.
	 * </p>
	 * 
	 * @param viewer
	 *            the viewer
	 * @param elements
	 *            the elements to sort
	 */
	@Override
	public void sort(final Viewer viewer, Object[] elements) {
		Arrays.sort(elements, new Comparator<Object>() {
			public int compare(Object a, Object b) {
				return CertSorter.this.compare(viewer, a, b);
			}
		});
	}
}
