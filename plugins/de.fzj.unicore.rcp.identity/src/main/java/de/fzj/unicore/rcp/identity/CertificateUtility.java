/*
 * Copyright (c) 2007, Forschungszentrum Juelich GmbH All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package de.fzj.unicore.rcp.identity;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.cert.CRLException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.asn1.x509.X509Name;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.x509.X509V3CertificateGenerator;

/**
 * Most of this code is from the Intel Unicore 5 client
 * 
 * @author Thomas Kenetemich (Intel GmbH)
 * @author schuller
 */

public class CertificateUtility {


	/**
	 * Constructor for the CertifcateUtility object
	 */
	private CertificateUtility() {
	}

	/**
	 * Test a X509Certifcate[] chain for Issuer/Subject order
	 * 
	 * @param chain
	 *            Certificate chain to check
	 * @return boolean true if the array contains an ordered chain of
	 *         Subject/Issuer certificates, false otherwise
	 */
	public static boolean checkOrder(X509Certificate[] chain) {

		boolean check = true;

		for (int i = 0; i < chain.length - 1; i++) {
			if (!chain[i].getIssuerDN().equals(chain[i + 1].getSubjectDN())) {
				check = false;
			}
		}

		return check;
	}

	/**
	 * Generate a MD5 fingerprint from a byte array containing a X.509
	 * certificate
	 * 
	 * @param ba
	 *            Byte array containing DER encoded X509Certificate.
	 * @return Byte array containing MD5 hash of DER encoded certificate.
	 */
	public static byte[] generateMD5Fingerprint(byte[] ba) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			return md.digest(ba);
		} catch (NoSuchAlgorithmException nsae) {
			IdentityActivator.log("MD5 algorithm not supported"
					+ nsae.getLocalizedMessage());
		}
		return null;
	}

	/**
	 * Generate new RSA keys. !! THIS METHOD RETURNS AN UNENCRYPTED PRIVATE KEY
	 * !!
	 * 
	 * @param algorithm
	 *            Key algorithm
	 * @param length
	 *            Key length
	 * @return Unencrypted private/public key pair
	 * @exception Exception
	 *                Description of the Exception
	 */
	public static KeyPair generateNewKeys(String algorithm, int length)
			throws Exception {
		KeyPairGenerator kpg = KeyPairGenerator.getInstance(algorithm);
		kpg.initialize(length);
		return kpg.generateKeyPair();
	}

	/**
	 * Generate new RSA keys. !! THIS METHOD RETURNS AN UNENCRYPTED PRIVATE KEY
	 * !!
	 * 
	 * @param algorithm
	 *            Key algorithm
	 * @param provider
	 *            Provider specification
	 * @param length
	 *            Key length
	 * @return Unencrypted private/public key pair
	 * @exception Exception
	 *                Description of the Exception
	 */
	public static KeyPair generateNewKeys(String algorithm, String provider,
			int length) throws Exception {
		KeyPairGenerator kpg = KeyPairGenerator
				.getInstance(algorithm, provider);
		kpg.initialize(length);
		return kpg.generateKeyPair();
	}

	/**
	 * Generate a SHA1 fingerprint from a byte array containing a X.509
	 * certificate
	 * 
	 * @param ba
	 *            Byte array containing DER encoded X509Certificate.
	 * @return Byte array containing SHA1 hash of DER encoded certificate.
	 */
	public static byte[] generateSHA1Fingerprint(byte[] ba) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA1");
			return md.digest(ba);
		} catch (NoSuchAlgorithmException nsae) {
			IdentityActivator.log("SHA1 algorithm not supported"
					+ nsae.getLocalizedMessage());
		}
		return null;
	}

	// genselfCert

	public static X509Certificate genSelfCert(String dn, long validity,
			PrivateKey privKey, PublicKey pubKey, boolean isCA)
			throws NoSuchAlgorithmException, SignatureException,
			InvalidKeyException, CertificateEncodingException,
			NoSuchProviderException {
		// Create self signed certificate
		String sigAlg = "SHA1WithRSA";
		Date firstDate = new Date();
		// Set back startdate ten minutes to avoid some problems with wrongly
		// set clocks.
		firstDate.setTime(firstDate.getTime() - 10 * 60 * 1000);
		Date lastDate = new Date();
		// validity in days = validity*24*60*60*1000 milliseconds
		lastDate.setTime(lastDate.getTime()
				+ (validity * (24 * 60 * 60 * 1000)));

		X509V3CertificateGenerator certgen = new X509V3CertificateGenerator();
		// Serialnumber is random bits, where random generator is initialized
		// with Date.getTime() when this
		// bean is created.
		byte[] serno = new byte[8];
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
		random.setSeed((new Date().getTime()));
		random.nextBytes(serno);
		certgen.setSerialNumber((new java.math.BigInteger(serno)).abs());
		certgen.setNotBefore(firstDate);
		certgen.setNotAfter(lastDate);
		certgen.setSignatureAlgorithm(sigAlg);
		certgen.setSubjectDN(CertificateUtility.stringToBcX509Name(dn));
		certgen.setIssuerDN(CertificateUtility.stringToBcX509Name(dn));
		certgen.setPublicKey(pubKey);
		// Basic constranits is always critical and MUST be present at-least in
		// CA-certificates.
		BasicConstraints bc = new BasicConstraints(isCA);
		certgen.addExtension(X509Extensions.BasicConstraints.getId(), true, bc);

		// Subject and Authority key identifier is always non-critical and MUST
		// be present for certificates to verify in Mozilla.
		try {
			if (isCA == true) {
				SubjectPublicKeyInfo spki = new SubjectPublicKeyInfo(
						(DERSequence) new ASN1InputStream(
								new ByteArrayInputStream(pubKey.getEncoded()))
								.readObject());
				SubjectKeyIdentifier ski = new SubjectKeyIdentifier(spki);

				SubjectPublicKeyInfo apki = new SubjectPublicKeyInfo(
						(DERSequence) new ASN1InputStream(
								new ByteArrayInputStream(pubKey.getEncoded()))
								.readObject());

				AuthorityKeyIdentifier aki = new AuthorityKeyIdentifier(apki);

				certgen.addExtension(
						X509Extensions.SubjectKeyIdentifier.getId(), false, ski);
				certgen.addExtension(
						X509Extensions.AuthorityKeyIdentifier.getId(), false,
						aki);
			}
		} catch (IOException e) {
			// do nothing
		}
		X509Certificate selfcert = certgen.generate(privKey, "BC");
		return selfcert;
	}

	// getSubjectKeyId

	/**
	 * Gets the authorityKeyId attribute of the CertificateUtility class
	 * 
	 */
	public static byte[] getAuthorityKeyId(X509Certificate cert)
			throws IOException {
		byte[] extvalue = cert.getExtensionValue("2.5.29.35");
		if (extvalue == null) {
			return null;
		}
		AuthorityKeyIdentifier keyId = new AuthorityKeyIdentifier(extvalue);
		return keyId.getKeyIdentifier();
	}

	// getCertfromPEM

	/**
	 * Generate SHA1 fingerprint in string representation.
	 */
	public static String getCertFingerprintAsString(byte[] ba) {

		try {
			X509Certificate cert = getCertfromByteArray(ba);
			byte[] res = generateSHA1Fingerprint(cert.getEncoded());
			return new String(Hex.encode(res)); // TODO hotfix ?!
		} catch (CertificateEncodingException cee) {
			IdentityActivator.log("Error encoding X509 certificate."
					+ cee.getLocalizedMessage());
		} catch (CertificateException cee) {
			IdentityActivator.log("Error decoding X509 certificate."
					+ cee.getLocalizedMessage());
		} catch (IOException ioe) {
			IdentityActivator
					.log("Error reading byte array for X509 certificate."
							+ ioe.getLocalizedMessage());
		}
		return null;
	}

	/**
	 * Creates X509Certificate from byte[].
	 * 
	 * @param cert
	 *            byte array containing certificate in DER-format
	 * @return X509Certificate
	 * @exception CertificateException
	 *                if the byte array does not contain a proper certificate.
	 * @exception IOException
	 *                if the byte array cannot be read.
	 */
	public static X509Certificate getCertfromByteArray(byte[] cert)
			throws IOException, CertificateException {
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		X509Certificate x509cert = (X509Certificate) cf
				.generateCertificate(new ByteArrayInputStream(cert));
		return x509cert;
	}

	/**
	 * Reads a certificate in PEM-format from an InputStream. The stream may
	 * contain other things, the first certificate in the stream is read.
	 * 
	 * @param certstream
	 *            Description of the Parameter
	 * @return X509Certificate
	 * @exception IOException
	 *                if the stream cannot be read.
	 * @exception CertificateException
	 *                if the stream does not contain a correct certificate.
	 */
	public static X509Certificate getCertfromPEM(InputStream certstream)
			throws IOException, CertificateException {
		String beginKey = "-----BEGIN CERTIFICATE-----";
		String endKey = "-----END CERTIFICATE-----";
		BufferedReader bufRdr = new BufferedReader(new InputStreamReader(
				certstream));
		ByteArrayOutputStream ostr = new ByteArrayOutputStream();
		PrintStream opstr = new PrintStream(ostr);
		String temp;
		while ((temp = bufRdr.readLine()) != null && !temp.equals(beginKey)) {
			continue;
		}
		if (temp == null) {
			throw new IOException("Error in " + certstream.toString()
					+ ", missing " + beginKey + " boundary");
		}
		while ((temp = bufRdr.readLine()) != null && !temp.equals(endKey)) {
			opstr.print(temp);
		}
		if (temp == null) {
			throw new IOException("Error in " + certstream.toString()
					+ ", missing " + endKey + " boundary");
		}
		opstr.close();

		byte[] certbuf = Base64.decode(ostr.toByteArray());

		// Phweeew, were done, now decode the cert from file back to
		// X509Certificate object
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		X509Certificate x509cert = (X509Certificate) cf
				.generateCertificate(new ByteArrayInputStream(certbuf));

		return x509cert;
	}

	/**
	 * Reads a certificate in PEM-format from a file. The file may contain other
	 * things, the first certificate in the file is read.
	 * 
	 * @param certFile
	 *            the file containing the certificate in PEM-format
	 * @return X509Certificate
	 * @exception IOException
	 *                if the filen cannot be read.
	 * @exception CertificateException
	 *                if the filen does not contain a correct certificate.
	 */
	public static X509Certificate getCertfromPEM(String certFile)
			throws IOException, CertificateException {
		InputStream inStrm = new FileInputStream(certFile);
		X509Certificate cert = getCertfromPEM(inStrm);
		return cert;
	}

	// getCertfromByteArray

	/**
	 * Get the common name from a X509 Certificate
	 * 
	 * @param x509
	 *            input certificate
	 * @return common name as String
	 */
	public static String getCommonName(X509Certificate x509) {
		if (x509 == null) {
			return null;
		}
		String principal = x509.getSubjectDN().toString();
		int index1 = 0;
		int index2 = 0;
		String cn = null;

		index1 = principal.indexOf("CN=");
		index2 = principal.indexOf(",", index1);

		if (index2 > 0) {
			cn = principal.substring(index1 + 3, index2);
		} else {
			cn = principal.substring(index1 + 3, principal.length() - 1);
		}
		return cn;
	}

	/**
	 * Creates X509CRL from byte[].
	 * 
	 * @param crl
	 *            byte array containing CRL in DER-format
	 * @return X509CRL
	 * @exception IOException
	 *                if the byte array can not be read.
	 * @exception CertificateException
	 *                if the byte arrayen does not contani a correct CRL.
	 * @exception CRLException
	 *                if the byte arrayen does not contani a correct CRL.
	 */
	public static X509CRL getCRLfromByteArray(byte[] crl) throws IOException,
			CertificateException, CRLException {
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		X509CRL x509crl = (X509CRL) cf
				.generateCRL(new ByteArrayInputStream(crl));
		return x509crl;
	}

	/**
	 * Generate SHA1 fingerprint of certificate in string representation.
	 * 
	 * @param cert
	 *            X509Certificate.
	 * @return String containing hex format of SHA1 fingerprint.
	 */
	public static String getFingerprintAsString(X509Certificate cert) {

		try {
			byte[] res = generateSHA1Fingerprint(cert.getEncoded());
			return new String(Hex.encode(res)); // TODO htofix ?!
		} catch (CertificateEncodingException cee) {
			IdentityActivator.log("Error encoding X509 certificate."
					+ cee.getLocalizedMessage());
		}
		return null;
	}

	/**
	 * Generate SHA1 fingerprint of CRL in string representation.
	 * 
	 * @param crl
	 *            X509CRL.
	 * @return String containing hex format of SHA1 fingerprint.
	 */
	public static String getFingerprintAsString(X509CRL crl) {

		try {
			byte[] res = generateSHA1Fingerprint(crl.getEncoded());
			return new String(Hex.encode(res)); // TODO hot fix ?
		} catch (CRLException ce) {
			IdentityActivator.log("Error encoding X509 CRL."
					+ ce.getLocalizedMessage());
		}
		return null;
	}

	// getCNFromDN

	/**
	 * Gets a specified part of a DN.
	 * 
	 * @param dn
	 *            String containing DN, The DN string has the format "C=SE,
	 *            O=xx, OU=yy, CN=zz".
	 * @param dnpart
	 *            String specifying which part of the DN to get, should be "CN"
	 *            or "OU" etc.
	 * @return String containing dnpart or null if dnpart is not present
	 */
	public static String getPartFromDN(String dn, String dnpart) {
		String trimmeddn = dn.trim();
		String part = null;
		String o = null;
		StringTokenizer st = new StringTokenizer(trimmeddn, ",=");
		while (st.hasMoreTokens()) {
			o = st.nextToken();
			if (o.trim().equalsIgnoreCase(dnpart)) {
				part = st.nextToken();
			}
		}
		return part;
	}

	// getAuthorityKeyId

	/**
	 * Gets a specified part of a DN.
	 * 
	 * @param dn
	 *            String containing DN, The DN string has the format "C=SE,
	 *            O=xx, OU=yy, CN=zz".
	 * @param dnpart
	 *            String specifying which part of the DN to get, should be "CN"
	 *            or "OU" etc.
	 * @return String containing dnpart or null if dnpart is not present
	 */
	public static List<String> getPartsFromDN(String dn, String dnpart) {
		String trimmeddn = dn.trim();
		List<String> part = new ArrayList<String>();
		String o = null;
		StringTokenizer st = new StringTokenizer(trimmeddn, ",=");
		while (st.hasMoreTokens()) {
			o = st.nextToken();
			if (o.trim().equalsIgnoreCase(dnpart)) {
				part.add(st.nextToken());
			}
		}
		return part;
	}

	/**
	 * Gets the subjectKeyId attribute of the CertificateUtility class
	 * 
	 * @param cert
	 *            Description of the Parameter
	 * @return The subjectKeyId value
	 * @exception IOException
	 *                Description of the Exception
	 */
	public static byte[] getSubjectKeyId(X509Certificate cert)
			throws IOException {
		byte[] extvalue = cert.getExtensionValue("2.5.29.14");
		if (extvalue == null) {
			return null;
		}
		SubjectKeyIdentifier keyId = new SubjectKeyIdentifier(extvalue);
		return keyId.getKeyIdentifier();
	}

	/**
	 * Imports a X509Certificate from byte array in PEM encoding; Added to
	 * handle certs coming in from other sources than a file.
	 * 
	 * @param filename
	 *            name of the PEM file
	 * @return Description of the Return Value
	 * @exception Exception
	 *                Description of the Exception
	 */
	public static Certificate importTrustedCertifcate(byte[] bytes)
			throws Exception {
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		Certificate cert = null;
		while (bais.available() > 0) {
			cert = cf.generateCertificate(bais);
		}
		return cert;
	}

	// getCRLfromByteArray

	/**
	 * Imports a X509Certificate in PEM encoding;
	 * 
	 * @param filename
	 *            name of the PEM file
	 * @return Description of the Return Value
	 * @exception Exception
	 *                Description of the Exception
	 */
	public static Certificate importTrustedCertifcate(File filename)
			throws Exception {
		DataInputStream dis = null;
		byte[] bytes = null;
		try {
			dis = new DataInputStream(new FileInputStream(filename));
			bytes = new byte[dis.available()];
			dis.readFully(bytes);
			return importTrustedCertifcate(bytes);
		} finally {
			dis.close();
		}
	}

	/**
	 * Checks if a certificate is self signed by verifying if subject and issuer
	 * are the same.
	 * 
	 * @param cert
	 *            the certificate that skall be checked.
	 * @return boolean true if the certificate has the same issuer and subject,
	 *         false otherwise.
	 */
	public static boolean isSelfSigned(X509Certificate cert) {
		boolean ret = cert.getSubjectDN().equals(cert.getIssuerDN());
		return ret;
	}

	/**
	 * Sort a X509Certifcate[] chain according to Subject and Issuer principals
	 * 
	 * @param in
	 *            Certificate chain to convert
	 * @return X509 certificate chain
	 */
	public static X509Certificate[] sort(X509Certificate in[]) {

		X509Certificate x509Cert[] = null;

		if (in == null) {
			return null;
		} else if (in.length == 0) {
			x509Cert = new X509Certificate[0];
			return x509Cert;
		}

		// Sort certificates into linked list
		LinkedList<X509Certificate> list = new LinkedList<X509Certificate>();

		// Copy certificates into set
		// from which they are sorted into the linked list
		Set<X509Certificate> inSet = new LinkedHashSet<X509Certificate>();
		inSet.addAll(Arrays.asList(in));

		// Start with first certificate
		list.addLast(in[0]);
		inSet.remove(in[0]);

		while (!inSet.isEmpty()) {
			int size = inSet.size();

			Iterator<X509Certificate> iter = inSet.iterator();
			while (iter.hasNext()) {
				X509Certificate next = iter.next();

				// Add matching certificate at the end or ...
				if ((list.getLast()).getIssuerDN().equals(next.getSubjectDN())) {
					list.addLast(next);
					inSet.remove(next);
				}
				// ... at the beginning of the linked list
				else if (next.getIssuerDN().equals(
						(list.getFirst()).getSubjectDN())) {
					list.addFirst(next);
					inSet.remove(next);
				}
			}

			// Break the while loop if no certificate could be sorted into the
			// linked list
			if (inSet.size() == size) {
				break;
			}
		}

		x509Cert = list.toArray(new X509Certificate[list.size()]);
		

		if (x509Cert.length != in.length) {
			return null;
		}

		if (!checkOrder(x509Cert)) {
			return null;
		}

		return x509Cert;
	}

	/**
	 * Every DN-string should look the same. Creates a name string ordered and
	 * looking like we want it...
	 * 
	 * @param dn
	 *            String containing DN
	 * @return String containing DN
	 */
	public static String stringToBCDNString(String dn) {
		String name = stringToBcX509Name(dn).toString();

		// Older worksround for bug in BC X509Name.java, kept for fun...
		// X509Name name = stringToBcX509Name(dn);
		// DERObject obj =name.getDERObject();
		// X509Name ret = new X509Name((DERSequence)obj);
		// return ret.toString();

		return name;
	}

	/**
	 * Creates a (Bouncycastle) X509Name object from a string with a DN.
	 * <p>
	 * 
	 * Known OID (with order) are:
	 * 
	 * <pre>
	 *  CN, SN, OU, O, L, ST, DC, C
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @param  dn  String containing DN that will be transformed into X509Name,
	 *       The DN string has the format "CN=zz,OU=yy,O=foo,C=SE". Unknown OIDs
	 *       in the string will be silently dropped.
	 * @return     X509Name
	 */
	public static X509Name stringToBcX509Name(String dn) {
		String trimmeddn = dn.trim();
		StringTokenizer st = new StringTokenizer(trimmeddn, ",=");
		Hashtable<DERObjectIdentifier,String> dntable = new Hashtable<DERObjectIdentifier, String>();
		String o = null;
		DERObjectIdentifier oid = null;
		Collection<DERObjectIdentifier> coll = new ArrayList<DERObjectIdentifier>();
		while (st.hasMoreTokens()) {
			o = st.nextToken();
			if (o.trim().equalsIgnoreCase("C")) {
				oid = X509Name.C;
				coll.add(X509Name.C);
			} else if (o.trim().equalsIgnoreCase("DC")) {
				oid = X509Name.DC;
				coll.add(X509Name.DC);
			} else if (o.trim().equalsIgnoreCase("ST")) {
				oid = X509Name.ST;
				coll.add(X509Name.ST);
			} else if (o.trim().equalsIgnoreCase("L")) {
				oid = X509Name.L;
				coll.add(X509Name.L);
			} else if (o.trim().equalsIgnoreCase("O")) {
				oid = X509Name.O;
				coll.add(X509Name.O);
			} else if (o.trim().equalsIgnoreCase("OU")) {
				oid = X509Name.OU;
				coll.add(X509Name.OU);
			} else if (o.trim().equalsIgnoreCase("SN")) {
				oid = X509Name.SN;
				coll.add(X509Name.SN);
			} else if (o.trim().equalsIgnoreCase("CN")) {
				oid = X509Name.CN;
				coll.add(X509Name.CN);
			} else if (o.trim().equalsIgnoreCase("EmailAddress")) {
				oid = X509Name.EmailAddress;
				coll.add(X509Name.EmailAddress);
			} else {
				oid = null;
			}
			// Just drop unknown entries in the DN
			if (oid != null) {
				dntable.put(oid, st.nextToken());
			}
		}
		Vector<DERObjectIdentifier> order = new Vector<DERObjectIdentifier>();
		order.add(X509Name.EmailAddress);
		order.add(X509Name.CN);
		order.add(X509Name.SN);
		order.add(X509Name.OU);
		order.add(X509Name.O);
		order.add(X509Name.L);
		order.add(X509Name.ST);
		order.add(X509Name.DC);
		order.add(X509Name.C);
		order.retainAll(coll);
		return new X509Name(order, dntable);
	}

	/**
	 * Tokenizes DN.
	 * 
	 * @param dn
	 *            String containing DN, The DN string has the format "C=SE,
	 *            O=xx, OU=yy, CN=zz".
	 * @return String containing dnpart or null if dnpart is not present
	 */
	public static Vector<String> tokenizeDN(String dn) {
		String trimmeddn = dn.trim();
		Vector<String> part = new Vector<String>();
		StringTokenizer st = new StringTokenizer(trimmeddn, ",=");
		while (st.hasMoreTokens()) {
			st.nextToken();
			part.addElement(st.nextToken());
		}
		return part;
	}

	/**
	 * Private helper to convert a Certificate[] to a X509Certifcate []
	 * 
	 * @param in
	 *            Certificate chain to convert
	 * @return X509 certificate chain
	 */
	public static X509Certificate[] toX509(Certificate in[]) {
		X509Certificate x509Cert[] = null;
		if (in != null) {
			x509Cert = new X509Certificate[in.length];
			for (int j = 0; j < in.length; j++) {
				if (in[j] instanceof X509Certificate) {
					x509Cert[j] = (X509Certificate) in[j];
				}
			}
		}
		return x509Cert;
	}

	/**
	 * Convert a collection of X509Certificates to a corresponding
	 * X509Certificate[] chain
	 * 
	 * @param coll
	 *            Collection of X509Certificates to convert
	 * @return Chain of X509 certificates
	 */
	public static X509Certificate[] toX509Chain(Collection<X509Certificate> coll) {

		X509Certificate unsortedCerts[] = coll.toArray(new X509Certificate[coll.size()]);
		X509Certificate x509Cert[] = sort(unsortedCerts);

		return x509Cert;
	}

	public static List<de.fzj.unicore.rcp.identity.keystore.Certificate> getTrustedCertificates(KeyStoreManager manager){
		List<de.fzj.unicore.rcp.identity.keystore.Certificate> certs = new ArrayList<de.fzj.unicore.rcp.identity.keystore.Certificate>();
		List<String> aliases = manager.getTrustedAliases();
		if (aliases == null) {
			return certs;
		}
		for (String alias : aliases.toArray(new String[0])) {

			try {
				X509Certificate[] X509certs = manager.getCertificateByAlias(alias);
				for (int i = 0; i < X509certs.length; i++) {
					de.fzj.unicore.rcp.identity.keystore.Certificate cert = new de.fzj.unicore.rcp.identity.keystore.Certificate(X509certs[i]);
					cert.setName(alias);
					certs.add(cert);
				}

			} catch (CertificateException e) {
				e.printStackTrace();
			} catch (KeyStoreException e) {
				e.printStackTrace();
			}
		}
		return certs;
	}
}
