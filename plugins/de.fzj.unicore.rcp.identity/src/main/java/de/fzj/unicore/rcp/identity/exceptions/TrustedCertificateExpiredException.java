package de.fzj.unicore.rcp.identity.exceptions;

import javax.security.cert.CertificateExpiredException;

public class TrustedCertificateExpiredException extends
		CertificateExpiredException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7210881495260760699L;
	private String alias;

	public TrustedCertificateExpiredException(String msg, String alias) {
		super(msg);
		this.alias = alias;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

}
