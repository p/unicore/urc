package de.fzj.unicore.rcp.identity.authn;

import java.util.Properties;

import de.fzj.unicore.rcp.identity.utils.AuthNUtils;
import eu.unicore.security.wsutil.client.authn.SAMLAuthN;

public class UnityAuthN extends SAMLAuthN {

	public UnityAuthN() {
		super();
	}

	public UnityAuthN(Properties properties) {
		super();
		this.properties = properties;
		this.truststorePasswordCallback = AuthNUtils.NOP_PASSWORD_CALLBACK;
		this.usernameCallback = AuthNUtils.NOP_USERNAME_CALLBACK;
	}

}
