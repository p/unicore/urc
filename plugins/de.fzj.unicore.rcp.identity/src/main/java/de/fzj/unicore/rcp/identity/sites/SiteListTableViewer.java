package de.fzj.unicore.rcp.identity.sites;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.common.guicomponents.FancyTableViewer;
import de.fzj.unicore.rcp.common.guicomponents.StringComboBoxCellEditor;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.profiles.IProfileListViewer;
import de.fzj.unicore.rcp.identity.profiles.Profile;
import de.fzj.unicore.rcp.identity.profiles.ProfileList;

public class SiteListTableViewer extends FancyTableViewer implements
		IProfileListViewer {

	// column headers
	public static final String SITE = "Site", PROFILE = "Profile";

	/**
	 * the order of the header names in columnProperties determines the order of
	 * columns in the table
	 */
	public List<String> columnProperties;

	private SiteList siteList;
	/**
	 * List of actions that can be executed on this viewer
	 */
	List<IAction> actions;
	private Action addSiteAction;
	private Action removeSiteAction;

	/**
	 * The constructor.
	 */
	public SiteListTableViewer(final Composite parent, SiteList siteList) {
		super(parent, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION);

		this.siteList = siteList;

		columnProperties = new ArrayList<String>();
		columnProperties.add(SITE);
		columnProperties.add(PROFILE);

		createTable();
		// initialize action list
		createActions();
		createCellEditors();
		setColumnProperties(columnProperties.toArray(new String[0]));
		setCellModifier(new SiteListCellModifier(this));

		setUseHashlookup(true);
		setContentProvider(new SiteListContentProvider());
		setLabelProvider(new SiteListLabelProvider(this));
		setInput(getSiteList());

		// create context menu for the table
		final MenuManager menuMgr = new MenuManager("Actions");
		menuMgr.setRemoveAllWhenShown(true);

		Menu menu = menuMgr.createContextMenu(getTable());
		getTable().setMenu(menu);
		IMenuListener listener = new IMenuListener() {
			public void menuAboutToShow(IMenuManager m) {
				for (Iterator<IAction> iter = actions.iterator(); iter
						.hasNext();) {
					IAction action = iter.next();
					if (action.isEnabled()) {
						menuMgr.add(action);
					}
				}
			}
		};
		menuMgr.addMenuListener(listener);
		getProfileList().addChangeListener(this);

		setResizePolicy(RESIZE_POLICY_EQUAL_WIDTH);
		packTable();

	}

	public void addProfile(Profile profile) {

	}

	private void createActions() {
		actions = new ArrayList<IAction>();
		addSiteAction = new Action() {
			@Override
			public void run() {
				String pattern = null;
				pattern = "Grid/Registry";

				Site[] sites = siteList.getSites();
				int maxIndex = 0;
				for (int i = 0; i < sites.length; i++) {
					String s = sites[i].getPattern();
					if (s.startsWith(pattern)) {
						int index = 0;
						try {
							index = 1 + Integer.parseInt(s.substring(
									pattern.length(), s.lastIndexOf("/")));

						} catch (Exception e) {
							index = 1;
						}
						if (index > maxIndex) {
							maxIndex = index;
						}
					}
				}
				if (maxIndex > 0) {
					pattern += maxIndex;
				}
				pattern += "/*";
				Site site = new Site();

				site.setPattern(pattern);

				try {
					siteList.addSite(site);
				} catch (Exception e) {
					IdentityActivator.log(IStatus.ERROR, "Unable to add site",
							e);
				}

			}
		};
		addSiteAction.setText("Add Site");
		addSiteAction
				.setToolTipText("Add a site for which specific credentials shall be used.");
		addSiteAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("add.png"));
		actions.add(addSiteAction);

		removeSiteAction = new Action() {
			@Override
			@SuppressWarnings("unchecked")
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getSelection();
				for (Iterator<Site> iter = selection.iterator(); iter.hasNext();) {
					Site site = iter.next();
					siteList.removeSite(site);
				}

			}
		};
		removeSiteAction.setText("Remove Site");
		removeSiteAction
				.setToolTipText("Remove a site that won't be used anymore.");
		removeSiteAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("delete.png"));
		actions.add(removeSiteAction);
	}

	protected void createCellEditors() {

		// Create the cell editors
		CellEditor[] cellEditors = new CellEditor[2];

		// SITE (TextField)
		TextCellEditor ce = new TextCellEditor(getTable());
		((Text) ce.getControl()).setEditable(true);
		cellEditors[0] = ce;

		setCellEditors(cellEditors);
	}

	private Table createTable() {
		Table table = getTable();

		for (int i = 0; i < columnProperties.size(); i++) {
			TableColumn col = new TableColumn(table, SWT.LEFT, i);
			String type = columnProperties.get(i);
			col.setText(type);
			if (columnProperties.get(i).equals(SITE)) {
				col.setToolTipText("Url or name path of a grid site.");
				col.setWidth(400);
			} else if (columnProperties.get(i).equals(PROFILE)) {

				col.setToolTipText("Security profile to use");
				col.setWidth(200);
			}
		}
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		return table;
	}

	public List<IAction> getActions() {
		return actions;
	}

	public Action getAddSiteAction() {
		return addSiteAction;
	}

	public int getColumnFor(String columnName) {
		for (int i = 0; i < columnProperties.size(); i++) {
			if (columnProperties.get(i).equals(columnName)) {
				return i;
			}
		}
		return -1;
	}

	protected ProfileList getProfileList() {
		return IdentityActivator.getDefault().getProfileList();
	}

	public Action getRemoveSiteAction() {
		return removeSiteAction;
	}

	public SiteList getSiteList() {
		return siteList;
	}

	@Override
	public void handleDispose(DisposeEvent event) {
		super.handleDispose(event);
	}

	public void profileNameChanged(String oldName, String newName,
			Profile profile) {
		getSiteList().removeChangeListener(
				(ISiteListViewer) getContentProvider());
		// when the name of a profile changes, the site list has to be updated!
		Site[] sites = getSiteList().getSites();
		for (Site site : sites) {
			if (oldName.equals(site.getProfileName())) {
				site.setProfileName(newName);
				getSiteList().updateSite(site);
			}
		}
		refresh();
		getSiteList().addChangeListener((ISiteListViewer) getContentProvider());
	}

	public void removeProfile(Profile profile) {

	}

	/**
	 * Dynamically update the CellEditor for selecting security profiles.
	 * Returns true iff the CellEditor is not null which means that the cell is
	 * modifiable.
	 * 
	 * @param column
	 * @param identifier
	 * @return
	 */
	public boolean updateProfileCellEditor() {

		String[] names = getProfileList().getProfileNames();
		if (names == null || names.length == 0) {
			return false;
		}
		StringComboBoxCellEditor editor = new StringComboBoxCellEditor(
				getTable(), names);
		CellEditor[] editors = getCellEditors();
		editors[getColumnFor(PROFILE)] = editor;
		setCellEditors(editors);
		return editor != null;
	}

	public void userDataChanged(Profile profile) {

	}

}