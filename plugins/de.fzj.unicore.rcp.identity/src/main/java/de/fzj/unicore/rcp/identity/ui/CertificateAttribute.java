package de.fzj.unicore.rcp.identity.ui;

import org.eclipse.swt.graphics.Image;

public class CertificateAttribute {
	private String key;
	private String value;
	private Image img;

	public CertificateAttribute(String key, String value, Image img) {
		super();
		this.key = key;
		this.value = value;
		this.img = img;
	}

	public Image getImg() {
		return img;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

}
