/**
 * (c) Copyright Mirasol Op'nWorks Inc. 2002, 2003. 
 * http://www.opnworks.com
 * Created on Apr 2, 2003 by lgauthier@opnworks.com
 */

package de.fzj.unicore.rcp.identity.truststore;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import de.fzj.unicore.rcp.identity.keystore.Certificate;

/**
 * Label provider for the TableViewerExample
 * 
 * @see org.eclipse.jface.viewers.LabelProvider
 */
public class CertLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	/**
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
	 *      int)
	 */
	public Image getColumnImage(Object element, int columnIndex) {
		return null;

	}

	/**
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
	 *      int)
	 */
	public String getColumnText(Object element, int columnIndex) {
		String result = "";
		Certificate c = (Certificate) element;
		switch (columnIndex) {
		case 0:
			result = c.getName();
			break;
		case 1:
			result = c.getSubjectCN();
			break;
		case 2:
			result = c.getValidTo();
			break;
		}
		// System.err.println("LabelProv gotValue: on col"+columnIndex+" with "+result);
		return result;
	}

}
