package de.fzj.unicore.rcp.identity;

import java.io.File;
import java.util.List;
import java.util.Properties;

import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.truststore.TruststoreChangedListener;

public interface TrustController {

	public void removeTrustedCertificate(String alias) throws Exception;
	
	public void removeTrustedCertificate(Certificate cert) throws Exception;
	
	public void addTrustedCertificate(Certificate cert) throws Exception;
	
	public void displayCertificateDetails(Certificate cert) throws Exception;
	
	public void changeAlias(Certificate cert) throws Exception;

	public boolean isExistingName(String alias);
	
	public void importKeyStore(KeyStoreManager newKm, String passwd,boolean askForUniqueTruststoreAliases) 
			throws Exception;

	public void importKeyStore(File f) throws Exception;
	
	public void reWriteKeyStore() throws Exception;
	
	public List<Certificate> getAllCertificates();

	/**
	 * create new Properties containing trust settings
	 * @return
	 */
	public Properties getTruststoreProperties();
	
	public void addTruststoreChangedListener(
			TruststoreChangedListener truststoreListener);
}
