/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.identity;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.common.utils.DelegatingExtensionClassloader;
import de.fzj.unicore.rcp.identity.authn.UnityAuthN;
import de.fzj.unicore.rcp.identity.extensionpoints.ICredentialTypeExtensionPoint;
import de.fzj.unicore.rcp.identity.keystore.CertificateList;
import de.fzj.unicore.rcp.identity.keystore.KeystoreCredentialController;
import de.fzj.unicore.rcp.identity.profiles.CredentialTypeRegistry;
import de.fzj.unicore.rcp.identity.profiles.Profile;
import de.fzj.unicore.rcp.identity.profiles.ProfileList;
import de.fzj.unicore.rcp.identity.sites.Site;
import de.fzj.unicore.rcp.identity.sites.SiteList;
import de.fzj.unicore.rcp.identity.truststore.DirectoryTrustController;
import de.fzj.unicore.rcp.identity.truststore.KeystoreTrustController;
import de.fzj.unicore.rcp.identity.utils.AuthNUtils;
import de.fzj.unicore.rcp.logmonitor.LogActivator;
import de.fzj.unicore.wsrflite.xmlbeans.WSUtilities;
import eu.unicore.security.wsutil.client.authn.AuthenticationProvider;
import eu.unicore.security.wsutil.client.authn.CachingIdentityResolver;
import eu.unicore.security.wsutil.client.authn.DelegationSpecification;
import eu.unicore.security.wsutil.client.authn.KeystoreAuthN;
import eu.unicore.util.httpclient.ClientProperties;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * The IdentityActivator offers methods to get a valid IClientConfiguration
 * depending on the credential and truststore settings
 * 
 * @author Bastian Demuth, Valentina Huber
 * @author schuller
 */
public class IdentityActivator extends AbstractUIPlugin implements IPropertyChangeListener {

	public static final String PLUGIN_ID = "de.fzj.unicore.rcp.identity";

	public final static String ICONS_PATH = "$nl$/icons/";//$NON-NLS-1$

	// name of the property representing the user certificates
	public final static String PROPERTY_KEYS = "property keys";

	/**
	 * boolean security property that specifies whether the properties returned
	 * by {@link getUASSecProps(URI uri)} are the default sec props or contain
	 * site specific credentials
	 */
	public final static String DEFAULT_PROPERTIES = "property keys";

	public final static String MSG_SELECT_KEYSTORE_FIRST = "Please go to the preferences and select a keystore first.";
	public final static String QUESTION_CREATE_KEYSTORE = "Create new Keystore at ";

	// the types of credential we support
	public static enum CredentialChoice { KEYSTORE, UNITY };

	// the types of truststore we support
	public static enum TruststoreChoice { KEYSTORE, DIRECTORY, OPENSSL};

	// The shared instance
	private static IdentityActivator plugin;

	// store service DNs mapped to URLs 
	private final CachingIdentityResolver identityResolver = new CachingIdentityResolver();

	private SiteList siteList;

	private ProfileList profileList;

	private String credentialPassword;
	private String trustPassword;

	private volatile boolean initialized = false;
	private volatile boolean initializing = false;

	private CredentialTypeRegistry credentialTypeRegistry;

	private DelegatingExtensionClassloader credentialClassLoader = new DelegatingExtensionClassloader(
			getClass().getClassLoader(), ICredentialTypeExtensionPoint.ID);

	private TrustController trust;

	private CredentialController credential;

	private CredentialChoice credentialType;

	private TruststoreChoice truststoreType;

	private URCConfigurationProvider configurationProvider;

	// guards config updates, e.g, changes of default alias 
	ReentrantReadWriteLock configLock = new ReentrantReadWriteLock(true);

	/**
	 * The constructor
	 */
	public IdentityActivator() {
		super();
		plugin = this;
	}

	public TrustController getTrust(){
		if (!this.initialized) {
			this.init();
		}
		return trust;
	}

	public TruststoreChoice getTruststoreType(){
		if (!this.initialized) {
			this.init();
		}
		return truststoreType;
	}

	public void setTruststoreChoice(TruststoreChoice truststoreType){
		this.truststoreType = truststoreType;
	}

	public CredentialController getCredential(){
		if (!this.initialized) {
			this.init();
		}
		return credential;
	}

	public CredentialChoice getCredentialType(){
		if (!this.initialized) {
			this.init();
		}
		return credentialType;
	}

	public void setCredentialChoice(CredentialChoice credentialType){
		this.credentialType = credentialType;
	}

	/**
	 * adds settings for HTTP proxy, timeout etc
	 */
	private void configure(Properties p) {
		// http connection timeout 

		IPreferencesService service = Platform.getPreferencesService();
		String prop = "client.http.connection.timeout";
		String timeout = String.valueOf(service.getInt(
				UnicoreCommonActivator.PLUGIN_ID,
				Constants.P_CONNECTION_TIMEOUT, 300, null));
		p.setProperty(prop, timeout);

		// proxy settings

		prop ="client.http.proxyHost";
		String proxyHost = service.getString(UnicoreCommonActivator.PLUGIN_ID,
				Constants.P_PROXY_HOST, null, null);
		if (proxyHost != null && proxyHost.trim().length() > 0) {
			p.setProperty(prop, proxyHost);
		}

		prop ="client.http.proxyPort";
		String proxyPort = String.valueOf(service.getInt(
				UnicoreCommonActivator.PLUGIN_ID, Constants.P_PROXY_PORT, 80,
				null));
		p.setProperty(prop, proxyPort);

		prop ="client.http.proxy.user";
		String proxyUser = service.getString(UnicoreCommonActivator.PLUGIN_ID,
				Constants.P_PROXY_USERNAME, null, null);
		if (proxyUser != null) {
			p.setProperty(prop, proxyUser);
		}

		prop ="client.http.proxy.password";
		String proxyPass = service.getString(UnicoreCommonActivator.PLUGIN_ID,
				Constants.P_PROXY_PASSWD, null, null);
		if (proxyPass != null) {
			p.setProperty(prop, proxyPass);
		}

		prop ="client.http.nonProxyHosts";
		String proxyNonHosts = service.getString(UnicoreCommonActivator.PLUGIN_ID,
				Constants.P_PROXY_NON_HOSTS, null, null);
		if (proxyNonHosts != null) {
			p.setProperty(prop, proxyNonHosts);
		}

		// TODO add preference GUI for these other settings
		p.setProperty("client.serverHostnameChecking", System.getProperty("client.serverHostnameChecking","NONE"));
		p.setProperty("client.messageLogging", System.getProperty("client.messageLogging","false"));
	}

	public CredentialTypeRegistry getCredentialTypeRegistry() {
		if (credentialTypeRegistry == null) {
			credentialTypeRegistry = new CredentialTypeRegistry();
			credentialTypeRegistry.setProfileList(getProfileList());
		}
		return credentialTypeRegistry;
	}

	public ProfileList getProfileList() {
		if (profileList == null) {
			profileList = ProfileList.reloadProfileList();
		}
		return profileList;
	}

	public SiteList getSiteList() {
		if (siteList == null) {
			siteList = SiteList.reloadSiteList();
		}
		return siteList;
	}

	/**
	 * get client config
	 * 
	 * @param targetEPR - target server EPR
	 * @param delegate - whether to delegate to the server
	 * @return
	 * @throws Exception
	 */
	public IClientConfiguration getClientConfiguration(EndpointReferenceType epr, DelegationSpecification delegate) 
			throws Exception {
		return getClientConfiguration(Collections.<String> emptyList(), epr, delegate);
	}

	public IClientConfiguration getClientConfiguration(String identifier,EndpointReferenceType epr, DelegationSpecification delegate) 
			throws Exception {
		return getClientConfiguration(Collections.singletonList(identifier), epr, delegate);
	}

	public IClientConfiguration getClientConfiguration(List<String> identifiers,EndpointReferenceType epr, DelegationSpecification delegate) 
			throws Exception {
		if(!this.initialized&&!this.initializing) {
			this.init();
		}
		configLock.readLock().lock();
		try{
			String url = epr.getAddress().getStringValue();
			String serviceIdentity = WSUtilities.extractServerIDFromEPR(epr);
			if(serviceIdentity == null && DelegationSpecification.STANDARD == delegate){
				try{
					serviceIdentity = identityResolver.resolveIdentity(url);
				}
				catch(IOException e){
					delegate = DelegationSpecification.DO_NOT;
				}
			}
			ClientProperties cp = (ClientProperties)configurationProvider.getClientConfiguration(url, serviceIdentity, delegate);
			if(identifiers !=null && !identifiers.isEmpty()){
				cp.setClassLoader(credentialClassLoader);
				Site match = getSiteList().matchSite(identifiers);
				Profile p = null;
				if (match != null) {
					p = getProfileList().getProfile(match.getProfileName());
				}
				for (ICredentialTypeExtensionPoint ext : getCredentialTypeRegistry()
						.getAllExtensions()) {
					ext.addCredentials(cp, p, identifiers);
				}
			}
			cp.getExtraSecurityTokens().put(DEFAULT_PROPERTIES, Boolean.TRUE.toString());
			return cp;
		}
		finally{
			configLock.readLock().unlock();
		}
	}

	public synchronized void init() {
		if (initialized || initializing) {
			return;
		}
		initializing = true;

		try {
			initTrustController();
		} catch (Exception ex) {
			log(IStatus.ERROR, "Could not load trust settings!", ex);
		}
		try {
			initCredentialController();
		} catch (Exception ex) {
			log(IStatus.ERROR, "Could not load credential!", ex);
		}
		try {
			reInitConfigurationProvider();
			if (credential.isReady()) {
				// do a basic check of provider as early as possible
				EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
				epr.addNewAddress().setStringValue("https://urc.unity.connectiontest.anonymous.info");
				// workaround for bug in SAMLAuthN class getKey() method
				// TODO remove when possible
				WSUtilities.addServerIdentity(epr, "CN=urc.unity.connectiontest.anonymous.info,OU=Anonymous");
				getClientConfiguration(epr, DelegationSpecification.DO_NOT);
			}
			initialized = true;
		} catch (Exception ex) {
			// throw new RuntimeException(ex);
		}

		initializing = false;
	}
	
	private void reInitConfigurationProvider() throws Exception {
		configLock.writeLock().lock();
		try{
			configurationProvider = new URCConfigurationProvider(
					createAuthProvider(),
					AuthNUtils.NOP_SESSION_PERSISTENCE,
					identityResolver,
					new HashMap<String,String[]>()
					);
		}
		finally{
			configLock.writeLock().unlock();
		}
	}
	
	private AuthenticationProvider createAuthProvider() throws Exception {
		Properties props = new Properties();
		props.putAll(credential.getCredentialProperties());
		props.putAll(trust.getTruststoreProperties());
		configure(props);
		AuthenticationProvider auth = isUsingUnity() ?
				new UnityAuthN(props) : 
				new KeystoreAuthN(props, AuthNUtils.NOP_PASSWORD_CALLBACK);
		return auth;
	}

	private void initCredentialController() throws Exception {
		switch(credentialType){
		case KEYSTORE:
			IPreferenceStore p = getPreferenceStore();
			File keystore = new File(p.getString(IdentityConstants.P_CREDENTIAL_PATH));
			if (!keystore.exists()) {
				throw new IllegalStateException("Keystore does not exist.");
			}
			KeystoreCredentialController cred = new KeystoreCredentialController(this, keystore, credentialPassword);
			String defaultAlias = p.getString(CertificateList.DEFAULT_CERT);
			cred.getKeystoreManager().setDefaultAlias(defaultAlias);
			
			boolean myProxy = Boolean.parseBoolean(p.getString(IdentityConstants.P_MYPROXY_ENABLED));
			if(myProxy){
				String host = p.getString(IdentityConstants.P_MYPROXY_HOST);
				String user = p.getString(IdentityConstants.P_MYPROXY_USER);
				String password = p.getString(IdentityConstants.P_MYPROXY_PASSWORD);
				cred.configureMyProxy(host, user, password, trust);
			}
			credential = cred;
			break;

		case UNITY:
			UnityCredentialController uCred = new UnityCredentialController(this);
			credential = uCred;
			break;

		default:
			throw new IllegalStateException();
		}
	}

	private void initTrustController() throws Exception {
		IPreferenceStore p = getPreferenceStore();
		File keystore = new File(p.getString(IdentityConstants.P_TRUSTSTORE_PATH));
		
		switch(truststoreType){
		case KEYSTORE:
			if (!keystore.exists()) {
				throw new IllegalStateException("Keystore does not exist.");
			}
			trust = new KeystoreTrustController(this, keystore, trustPassword);
			break;

		case DIRECTORY:
			trust = new DirectoryTrustController(this, keystore, false);
			break;

		case OPENSSL:
			trust = new DirectoryTrustController(this, keystore, true);
			break;

		default:
			throw new IllegalStateException();
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		if (credentialTypeRegistry != null) {
			credentialTypeRegistry.dispose();
		}
		plugin = null;
		super.stop(context);

	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static IdentityActivator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		ImageDescriptor id = imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH
				+ path);
		// inherit icons/ from common plugin
		if (id == null) {
			id = AbstractUIPlugin.imageDescriptorFromPlugin(
					UnicoreCommonActivator.PLUGIN_ID, path);
		}
		return id;
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param forceAlarmUser
	 *            forces the logging plugin to open a popup; usually, this
	 *            should NOT be set to true!
	 */
	public static void log(int status, String msg, boolean forceAlarmUser) {
		log(status, msg, null, forceAlarmUser);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 * @param forceAlarmUser
	 *            forces the logging plugin to open a popup; usually, this
	 *            should NOT be set to true!
	 */
	public static void log(int status, String msg, Exception e,
			boolean forceAlarmUser) {
		IStatus s = new Status(status, PLUGIN_ID, IStatus.OK, msg, e);
		LogActivator.log(plugin, s, forceAlarmUser);
	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 */
	public static void log(String msg) {
		log(msg, null);
	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(String msg, Exception e) {
		log(IStatus.INFO, msg, e, false);
	}

	public void setCredentialPassword(String passphrase) {
		credentialPassword = passphrase;
	}

	public String getCredentialPassphrase() {
		if (!this.initialized) {
			this.init();
		}
		return credentialPassword;
	}

	public void setTruststorePassword(String passphrase) {
		trustPassword = passphrase;
	}
	
	public boolean isUsingUnity(){
		return CredentialChoice.UNITY == credentialType;
	}

	@Override
	public void propertyChange(org.eclipse.jface.util.PropertyChangeEvent event) {
		try{
			if(!initialized)return;
			
			reInitConfigurationProvider();
		}
		catch(Exception ex){
			log(IStatus.ERROR,"Could not setup security settings!",ex);
		}
	}

	/**
	 * fired when credential/truststore settings were changed 
	 * (default alias, myproxy reload, etc)
	 */
	public static class AuthNChangeEvent extends org.eclipse.jface.util.PropertyChangeEvent{

		private static final long serialVersionUID = 1L;

		public AuthNChangeEvent(Object source, String property,
				Object oldValue, Object newValue) {
			super(source, property, oldValue, newValue);
		}
		
	}
}
