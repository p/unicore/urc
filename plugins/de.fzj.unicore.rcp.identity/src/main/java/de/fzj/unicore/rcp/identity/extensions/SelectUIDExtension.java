package de.fzj.unicore.rcp.identity.extensions;

import java.util.List;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.identity.extensionpoints.ICredentialTypeExtensionPoint;
import de.fzj.unicore.rcp.identity.profiles.Profile;
import de.fzj.unicore.rcp.identity.profiles.ProfileList;
import eu.unicore.util.httpclient.IClientConfiguration;


public class SelectUIDExtension implements
		ICredentialTypeExtensionPoint {

	public static final String SELECTED_UID = "selected User ID";


	public void addCredentials(IClientConfiguration secProps,
			Profile profile, List<String> identifiers) {

		if (profile != null) {
			String uid=getSelectedUID(profile);
			if(uid!=null && !"".equalsIgnoreCase(uid)){
				secProps.getETDSettings().getRequestedUserAttributes2()
				        .put("xlogin",new String[]{getSelectedUID(profile)});
			}
		}

	}

	public void dispose() {

	}

	public CellEditor getCellEditor(Composite parent, Profile profile) {
		return new SelectUIDCellEditor(parent);
	}

	public Image getImage(Profile profile) {
		return null;
	}

	public String getLabel(Profile profile) {
		return getSelectedUID(profile);
	}

	public void initProfile(Profile newProfile) {
		setSelectedUID(newProfile, "");
	}

	public void setProfileList(ProfileList profileList) {

	}

	public static String getSelectedUID(Profile profile) {
		return (String) profile.getUserData(SELECTED_UID);
	}

	public static void setSelectedUID(Profile profile, String uid) {
		profile.setUserData(SELECTED_UID, uid);
	}

}
