/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.identity.keystore;

import java.io.File;
import java.io.Serializable;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.eclipse.core.runtime.IStatus;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.X509Utils;

public class Certificate implements Serializable {

	private static final long serialVersionUID = 7005783621351704977L;

	/** X.509 certificate for auth the user */
	private X509Certificate cert = null;

	/** local file, stored reference to this */
	private File certFile = null;

	/** indicates if this is the default cert **/
	private boolean defaultCert = false;

	/**
	 * serial number
	 */
	private String name = "";

	/**
	 * password for the certificate
	 */
	private String password = null;

	/** creates a new Cert, not yet stored in key/trust-store */
	public Certificate() {
	}

	public Certificate(File certFile) {
		this(certFile, false);
	}

	public Certificate(File certFile, boolean defaultCert) {
		setCertFile(certFile);
		name = X509Utils.getCertificateAlias(cert);
		this.defaultCert = defaultCert;
	}

	/**
	 * creates a new Certificate
	 * 
	 * @throws CertificateException
	 *             if non valid String
	 * */
	public Certificate(String s) throws CertificateException {
		cert = X509Utils.getX509FromPEMString(s);
		name = X509Utils.getCertificateAlias(cert);
	}

	public Certificate(X509Certificate cert) throws CertificateException {
		this.cert = cert;
		name = X509Utils.getCertificateAlias(cert);
	}

	/**
	 * retrieve the current cert !! Might differ from Cert in xUUDB !!
	 * 
	 * @return X509Certificate the cert
	 */
	public X509Certificate getCert() {
		return cert;
	}

	/**
	 * retrieve the current cert as PEM String
	 * 
	 * @return String cert as PEMString
	 */
	public String getCertAsPEMString() {
		try {
			return X509Utils.getPEMStringFromX509(cert, true);
		} catch (CertificateEncodingException e) {
			IdentityActivator.log(IStatus.INFO,
					"Exception while convert cert to PEM", e, false);
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * retrieve the current cert's file loction, if locally read before !! Might
	 * differ from Cert in xUUDB !!
	 * 
	 * @return String of Filename
	 */
	public File getCertFile() {
		return certFile;
	}

	public String getIssuerCN() {
		if (cert == null) {
			return "?";
		}
		String[] dn = cert.getIssuerX500Principal().getName("CANONICAL")
				.split(",");
		for (int i = 0; i < dn.length; i++) {
			if (dn[i].startsWith("cn=")) {
				String cn = dn[i].replace("cn=", "");
				return cn;
			}
		}
		return "";
	}

	public String getIssuerDN() {
		if (cert == null) {
			return "?";
		}
		return "" + cert.getIssuerDN();
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getSubjectCN() {
		if (cert == null) {
			return "?";
		}
		String[] dn = cert.getSubjectX500Principal().getName("CANONICAL")
				.split(",");
		for (int i = 0; i < dn.length; i++) {
			if (dn[i].startsWith("cn=")) {
				String cn = dn[i].replace("cn=", "");
				return cn;
			}
		}
		return "";
	}

	/**
	 * retrieve the DN of the current Cert !! Might differ from Cert in xUUDB !!
	 * 
	 * @return
	 */
	public String getSubjectDN() {
		if (cert == null) {
			return "?";
		}
		return "" + cert.getSubjectDN();
	}

	public String getValidFrom() {
		return getValidFrom("yyyy-MM-dd hh:mm");
	}

	/**
	 * 
	 * @param format
	 *            format in which the point in time is returned.
	 * @return
	 */
	public String getValidFrom(String format) {
		if (cert == null) {
			return "?";
		}
		DateFormat df = new SimpleDateFormat(format);
		return df.format(cert.getNotBefore());
	}

	public String getValidTo() {
		return getValidTo("yyyy-MM-dd hh:mm");
	}

	/**
	 * 
	 * @param format
	 *            format in which the point in time is returned.
	 * @return
	 */
	public String getValidTo(String format) {
		if (cert == null) {
			return "?";
		}
		DateFormat df = new SimpleDateFormat(format);
		return df.format(cert.getNotAfter());
	}

	public boolean isDefaultCert() {
		return defaultCert;
	}

	/**
	 * Reads a X.509 from a File and stores it
	 * 
	 * @param certfile
	 *            the File to read.
	 */
	public void setCertFile(File certfile) {

		try {
			X509Certificate newCert = X509Utils.getX509FromFile(certfile);	
			if (cert == null
					|| !X509Utils.getPEMStringFromX509(newCert).equals(X509Utils
							.getPEMStringFromX509(cert))) {

				this.cert = newCert;
				this.certFile = certfile;
			}


		} catch (Exception e) {
			IdentityActivator.log(IStatus.WARNING, "Cant open file!");
		}
	}

	public void setDefaultCert(boolean defaultCert) {
		this.defaultCert = defaultCert;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String passwort) {
		this.password = passwort;
	}

	public void deleteUnderlyingFile() {
		if(certFile != null) {
			certFile.delete();
		}
	}
}
