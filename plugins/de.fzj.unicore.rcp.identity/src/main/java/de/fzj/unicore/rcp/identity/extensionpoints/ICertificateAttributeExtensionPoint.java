package de.fzj.unicore.rcp.identity.extensionpoints;

import java.util.List;

import org.eclipse.swt.graphics.Image;

import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.ui.CertificateAttribute;
import de.fzj.unicore.rcp.identity.ui.dialogs.CertificateDetailsDialog;

/**
 * Contributing plugins can provide implementations in order to create
 * additional attributes that should be displayed inside the
 * {@link CertificateDetailsDialog}.
 * 
 * @author bdemuth
 * 
 */
public interface ICertificateAttributeExtensionPoint {

	public static final String ID = "de.fzj.unicore.rcp.identity.extensionpoints.CertificateAttributes";
	public static final String ELEMENT_NAME_CLASS = "class";
	public static final String ATTRIBUTE_NAME_CLASSNAME = "name";

	/**
	 * Look at the certificate and find out whether you want to have additional
	 * attributes displayed in the attribute table. If so, return these
	 * attributes, otherwise, return null or an empty List.
	 * 
	 * @param cert
	 * @return
	 */
	public List<CertificateAttribute> createAttributes(Certificate cert);

	/**
	 * {@link CertificateAttribute} objects can have references to {@link Image}
	 * Objects that are supposed to be displayed in the attribute table.
	 * Remember that SWT Images open up file handles and therefore ALWAYS have
	 * to be disposed of once they are not needed anymore. This method is called
	 * when the dialog is closed and allows implementing classes to perform the
	 * disposal.
	 */
	public void disposeImages();
}
