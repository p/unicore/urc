package de.fzj.unicore.rcp.identity.problems;

import de.fzj.unicore.rcp.identity.actions.ImportKeystoreAction;
import de.fzj.unicore.rcp.identity.truststore.TruststoreView;
import de.fzj.unicore.rcp.logmonitor.problems.ShowViewSolution;
import eu.unicore.problemutil.Problem;

public class ImportPrivateKeySolution extends ShowViewSolution {

	public ImportPrivateKeySolution() {
		super("TLS_CLI_GRAPH_IMPORT", "TLS_CLI",
				"Import a keystore with a certificate that the server trusts...");

	}

	@Override
	protected String getViewId() {
		return TruststoreView.ID;
	}

	@Override
	public boolean solve(String message, Problem[] problems, String details) {
		super.solve(message, problems, details);
		ImportKeystoreAction importKeystoreAction = new ImportKeystoreAction();
		importKeystoreAction.setText("Import keystore");
		importKeystoreAction.run();
		return true;
	}

}
