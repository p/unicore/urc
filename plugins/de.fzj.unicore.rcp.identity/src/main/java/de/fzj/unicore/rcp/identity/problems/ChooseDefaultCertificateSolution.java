package de.fzj.unicore.rcp.identity.problems;

import de.fzj.unicore.rcp.identity.keystore.KeystoreView;
import de.fzj.unicore.rcp.logmonitor.problems.ShowViewSolution;

public class ChooseDefaultCertificateSolution extends ShowViewSolution {

	public ChooseDefaultCertificateSolution() {
		super("TLS_CLI_GRAPH_SET_DEFAULT", "TLS_CLI",
				"Open the Keystore view in order to choose the right default certificate");

	}

	@Override
	protected String getViewId() {
		return KeystoreView.ID;
	}

}
