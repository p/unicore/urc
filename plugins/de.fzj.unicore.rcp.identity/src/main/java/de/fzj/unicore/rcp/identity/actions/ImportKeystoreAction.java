package de.fzj.unicore.rcp.identity.actions;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.IdentityConstants;
import de.fzj.unicore.rcp.identity.keystore.KeystoreCredentialController;
import de.fzj.unicore.rcp.logmonitor.utils.FileDialogUtils;

public class ImportKeystoreAction extends Action {

	private String defaultFileType = null;

	public ImportKeystoreAction() {
		super();

		setToolTipText("Add keys and trusted CAs from another keystore");
		setImageDescriptor(IdentityActivator.getImageDescriptor("add.png"));
	}

	private String[] askImportKeystoreFile() {
		Shell shell = getShell();
		FileDialog dialog = new FileDialog(shell, SWT.OPEN | SWT.MULTI);
		dialog.setFilterExtensions(Constants.IMPORTABLE_KEYSTORE_FORMATS);
		String filterPath = FileDialogUtils.getFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS);
		dialog.setFilterPath(filterPath);
		if (defaultFileType != null) {
			List<String> types = Arrays
					.asList(Constants.IMPORTABLE_KEYSTORE_FORMATS);
			int index = types.indexOf(defaultFileType);
			if (index >= 0) {
				dialog.setFilterIndex(index);
			}
		}
		dialog.setText("Please select keystore file(s) that you want to merge with your keystore.");
		dialog.open();
		String[] filenames = dialog.getFileNames();
		if (filenames != null) {
			for (int i = 0; i < filenames.length; i++) {
				filenames[i] = dialog.getFilterPath() + File.separator
						+ filenames[i];
			}
		}
		FileDialogUtils.saveFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS,dialog.getFilterPath());
		return filenames;
	}

	public String getDefaultFileType() {
		return defaultFileType;
	}

	@Override
	public void run() {
		if(IdentityActivator.getDefault().isUsingUnity()){
			MessageBox mb = new MessageBox(getShell(),SWT.OK);
			mb.setMessage("No keystore is used!");
			mb.open();
			return;
		}
		try {
			String[] files = askImportKeystoreFile();
			if (files != null) {
				for (String s : files) {
					File f = new File(s);
					KeystoreCredentialController ks = (KeystoreCredentialController)IdentityActivator.getDefault().getCredential();
					ks.importKeysFromKeystore(f);
				}

			}
		} catch (Exception e) {
			IdentityActivator.log(IStatus.ERROR, "Could not import keystore: "
					+ e.getMessage(), e);
		}
	}

	public void setDefaultFileType(String defaultFileType) {
		this.defaultFileType = defaultFileType;
	}
	
	private Shell getShell(){
		Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();
		if (shell == null) {
			shell = new Shell();
		}
		return shell;
	}
}
