/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.identity.keystore;

import java.security.KeyStoreException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.IdentityActivator.AuthNChangeEvent;
import de.fzj.unicore.rcp.identity.KeyStoreManager;

public class CertificateList implements IPropertyChangeListener {
	public static final String DEFAULT_CERT = "Certificate List default cert:";
	public static final String PROPERTY_CERTS = "Certificate List certs";
	private List<Certificate> certs;
	private Set<ICertListViewer> changeListeners = new HashSet<ICertListViewer>();
	private String defaultCertName = null;
	private final KeyStoreManager km;
	
	public CertificateList(KeyStoreManager km) {
		this.km = km;
		certs = loadCertificates();
		fireAllCertsPropertyChange();
	}

	/**
	 * Add a new Cert to the List This will create a "empty" Cert which is NOT
	 * valid
	 */
	public void addCert(Certificate u) {
		if (certs.isEmpty()) {
			u.setDefaultCert(true);
		}
		certs.add(u);

		Iterator<ICertListViewer> iterator = changeListeners.iterator();
		while (iterator.hasNext()) {
			iterator.next().addCert(u);
		}
	}

	public void addChangeListener(ICertListViewer viewer) {
		changeListeners.add(viewer);
	}

	public void certChanged(Certificate u) {
		Iterator<ICertListViewer> iterator = changeListeners.iterator();
		while (iterator.hasNext()) {
			iterator.next().updateCert(u);
		}
	}

	private void fireAllCertsPropertyChange() {
		Iterator<ICertListViewer> iterator = changeListeners.iterator();
		while (iterator.hasNext()) {
			iterator.next().reloadAllCerts();
		}
	}

	public Certificate getCertByIndex(int index) {
		if (index < 0 || index >= getCerts().size()) {
			return null;
		}
		Certificate[] certs = getCerts().toArray(new Certificate[0]);
		return certs[index];
	}

	public Certificate getCertByName(String name) {
		return getCertByName(name, getCerts());
	}

	public Certificate getCertByName(String name, List<Certificate> certs) {
		if (name == null || certs == null) {
			return null;
		}
		for (Iterator<Certificate> iter = certs.iterator(); iter.hasNext();) {
			Certificate cert = iter.next();
			if (name.equals(cert.getName())) {
				return cert;
			}
		}
		return null;
	}

	public Certificate getCertByPublicKey(PublicKey key) {
		for (Iterator<Certificate> iterator = certs.iterator(); iterator
				.hasNext();) {
			Certificate cert = iterator.next();
			if (cert.getCert().getPublicKey().equals(key)) {
				return cert;
			}
		}
		// cert not found
		return null;
	}

	/**
	 * Return the index of the cert with the given name
	 * 
	 * @param name
	 * @return the index of the cert in the cert list, -1 if cert cannot be
	 *         found
	 */
	public int getCertIndexByName(String name) {
		if (name == null || certs == null) {
			return -1;
		}
		Certificate[] certArray = certs.toArray(new Certificate[0]);
		for (int i = 0; i < certArray.length; i++) {
			if (name.equals(certArray[i].getName())) {
				return i;
			}
		}
		// Cert not found
		return -1;
	}

	/**
	 * Return the collection of certificate names
	 */
	public String[] getCertNames() {
		List<Certificate> certs = getCerts();
		String[] names = new String[certs.size()];
		int i = 0;
		for (Iterator<Certificate> iter = certs.iterator(); iter.hasNext();) {
			names[i++] = iter.next().getName();
		}
		return names;
	}

	/**
	 * Return the collection of certificates
	 */
	public List<Certificate> getCerts() {
		return certs;
	}

	public Certificate getDefaultCert() {
		if (defaultCertName == null) {
			IPreferenceStore prefs = IdentityActivator.getDefault().getPreferenceStore();
			defaultCertName = prefs.getString(DEFAULT_CERT);
		}
		return getCertByName(defaultCertName);
	}

	

	public boolean isExistingName(String name) {
		for (int i = 0; i < certs.size(); i++) {
			if (certs.get(i).getName().equals(name)) {
				return true;
			}
		}

		return false;
	}

	private List<Certificate> loadCertificates() {
		List<Certificate> certs = new ArrayList<Certificate>();
		if (km == null) {
			return certs;
		}
		List<String> aliases = km.getIdentityAliases();
		if (aliases == null) {
			return certs;
		}
		for (String alias : aliases.toArray(new String[0])) {

			try {
				X509Certificate[] X509certs = km.getCertificateByAlias(alias);
				// use first cert in certificate chain
				Certificate cert = new Certificate(X509certs[0]);
				cert.setName(alias);
				certs.add(cert);

			} catch (CertificateException e) {
				e.printStackTrace();
			} catch (KeyStoreException e) {
				e.printStackTrace();
			}

		}

		// restore persisted name of default certificate
		IPreferenceStore prefs = IdentityActivator.getDefault()
				.getPreferenceStore();
		String certName = prefs.getString(DEFAULT_CERT);
		if (certName == null || certName.trim().length() == 0) {
			certName = km.getDefaultAlias();
		}
		if (certName == null && aliases.size() > 0) {
			certName = aliases.get(0);
		}
		Certificate defaultCert = getCertByName(certName, certs);
		if (defaultCert != null) {
			defaultCertName = certName;
			defaultCert.setDefaultCert(true);
			prefs.setValue(DEFAULT_CERT, certName);
		}
		return certs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Preferences.IPropertyChangeListener#propertyChange
	 * (org.eclipse.core.runtime.Preferences.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent event) {
		if (event instanceof AuthNChangeEvent) {
			try {
				certs = loadCertificates();
				fireAllCertsPropertyChange();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	/**
	 * Remove a Cert
	 * 
	 * @param Certificate
	 */
	public void removeCert(Certificate u) {
		certs.remove(u); // remove from local model
		if (u.isDefaultCert()) {
			setDefaultCert(null);
		}
		Iterator<ICertListViewer> iterator = changeListeners.iterator();
		while (iterator.hasNext()) {
			iterator.next().removeCert(u);
		}
	}

	public void removeChangeListener(ICertListViewer viewer) {
		changeListeners.remove(viewer);
	}

	public void setDefaultCert(Certificate cert) {
		String old=defaultCertName;
		String certName = "";
		IPreferenceStore prefs = IdentityActivator.getDefault().getPreferenceStore();

		if (cert == null) {
			prefs.setToDefault(DEFAULT_CERT);
			try {
				km.setDefaultAlias("");
			} catch (Exception e) {
				IdentityActivator.log(
						"Could not set default certificate in keystore:", e);
			}
		} else {
			certName = cert.getName();
			defaultCertName = certName;
			prefs.setValue(DEFAULT_CERT, certName);
			try {
				km.setDefaultAlias(certName);
			} catch (Exception e) {
				IdentityActivator.log(
						"Could not set default certificate in keystore:", e);
			}
		}

		for (int i = 0; i < certs.size(); i++) {
			if (certName.equals(certs.get(i).getName())) {
				certs.get(i).setDefaultCert(true);
			} else {
				certs.get(i).setDefaultCert(false);
			}
			certChanged(certs.get(i));
		}
		
		IdentityActivator.getDefault().propertyChange(
				new IdentityActivator.AuthNChangeEvent(this, "alias", old, defaultCertName));
	}
}
