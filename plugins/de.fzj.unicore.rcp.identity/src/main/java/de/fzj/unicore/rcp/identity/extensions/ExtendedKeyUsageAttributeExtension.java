package de.fzj.unicore.rcp.identity.extensions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bouncycastle.asn1.x509.KeyPurposeId;

import de.fzj.unicore.rcp.identity.extensionpoints.ICertificateAttributeExtensionPoint;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.ui.CertificateAttribute;

public class ExtendedKeyUsageAttributeExtension implements
ICertificateAttributeExtensionPoint {

	public static final String EXTENSION_ID = "2.5.29.37";

	public List<CertificateAttribute> createAttributes(Certificate cert) {
		Set<String> critical = cert.getCert().getCriticalExtensionOIDs();
		Set<String> nonCritical = cert.getCert().getNonCriticalExtensionOIDs();
		Set<String> extensions = new HashSet<String>();
		if(critical != null) extensions.addAll(critical);
		if(nonCritical != null) extensions.addAll(nonCritical);
		List<CertificateAttribute> result = new ArrayList<CertificateAttribute>();

		if(extensions != null && extensions.contains(EXTENSION_ID))
		{
			String value;
			try {
				List<String>extKeyUsage = cert.getCert().getExtendedKeyUsage();
				StringBuilder sb = new StringBuilder();
				boolean first = true;
				for(String s: extKeyUsage){
					if(!first){
						sb.append(", ");
					}
					first = false;
					sb.append(getHumanReadable(s));
				}
				value = sb.toString();
				if(value.length() > 0) result.add(new CertificateAttribute("Extended Key Usage", value, null));
			} catch (Exception e) {
				// If we can't read the extension properly, ignore it
			}

		}

		return result;
	}

	private String getHumanReadable(String sOid)
	{
		String toAdd = null;
		if(KeyPurposeId.id_kp_clientAuth.getId().equals(sOid))
		{
			toAdd = "Client Authentication";
		}
		else if(KeyPurposeId.id_kp_serverAuth.getId().equals(sOid))
		{
			toAdd = "Server Authentication";
		}
		else if(KeyPurposeId.id_kp_codeSigning.getId().equals(sOid))
		{
			toAdd = "Code Signing";
		}
		else if(KeyPurposeId.id_kp_emailProtection.getId().equals(sOid))
		{
			toAdd = "Email Protection";
		}
		else if(KeyPurposeId.id_kp_timeStamping.getId().equals(sOid))
		{
			toAdd = "Timestamping";
		}
		else if(KeyPurposeId.id_kp_OCSPSigning.getId().equals(sOid))
		{
			toAdd = "SOCSPstamping";
		}
		else toAdd = sOid;

		return toAdd;
	}

	public void disposeImages() {

	}

}
