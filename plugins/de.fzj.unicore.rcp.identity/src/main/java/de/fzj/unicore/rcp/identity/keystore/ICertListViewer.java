package de.fzj.unicore.rcp.identity.keystore;

public interface ICertListViewer {

	/**
	 * Update the view to reflect the fact that a cert was added to the task
	 * list
	 * 
	 * @param cert
	 */
	public void addCert(Certificate u);

	/**
	 * Update the view to reflect the fact that all certs might have changed
	 * 
	 * @param cert
	 */
	public void reloadAllCerts();

	/**
	 * Update the view to reflect the fact that a cert was removed from the task
	 * list
	 * 
	 * @param cert
	 */
	public void removeCert(Certificate u);

	/**
	 * Update the view to reflect the fact that one of the certs was modified
	 * 
	 * @param cert
	 */
	public void updateCert(Certificate u);
}
