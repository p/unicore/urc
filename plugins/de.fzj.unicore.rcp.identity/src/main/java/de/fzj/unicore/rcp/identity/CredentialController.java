package de.fzj.unicore.rcp.identity;

import java.util.Properties;

public interface CredentialController {
	
	/**
	 * In some cases the credential controller may not yet be ready for use
	 * (e.g. keystore is new and thus empty)
	 * 
	 * @return true if the controller is ready, false otherwise 
	 */
	public boolean isReady();
	
	public Properties getCredentialProperties();
	
}
