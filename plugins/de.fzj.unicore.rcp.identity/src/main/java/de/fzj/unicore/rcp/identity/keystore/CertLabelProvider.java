/**
 * (c) Copyright Mirasol Op'nWorks Inc. 2002, 2003. 
 * http://www.opnworks.com
 * Created on Apr 2, 2003 by lgauthier@opnworks.com
 */

package de.fzj.unicore.rcp.identity.keystore;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import de.fzj.unicore.rcp.identity.IdentityActivator;

/**
 * Label provider for the TableViewerExample
 * 
 * @see org.eclipse.jface.viewers.LabelProvider
 */
public class CertLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	private Image checkedImg = IdentityActivator.getImageDescriptor(
			"checked.png").createImage();
	private Image uncheckedImg = IdentityActivator.getImageDescriptor(
			"unchecked.png").createImage();

	@Override
	public void dispose() {
		super.dispose();
		checkedImg.dispose();
		uncheckedImg.dispose();

	}

	/**
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
	 *      int)
	 */
	public Image getColumnImage(Object element, int columnIndex) {
		if (columnIndex != KeystoreView.getColumnFor(KeystoreView.DEFAULT_CERT)) {
			return null; // ignore everything not default column
		}
		Certificate c = (Certificate) element;
		if (c.isDefaultCert()) {
			return checkedImg;
		} else {
			return uncheckedImg;
		}
	}

	/**
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
	 *      int)
	 */
	public String getColumnText(Object element, int columnIndex) {
		String result = "";
		Certificate c = (Certificate) element;

		if (columnIndex == KeystoreView.getColumnFor(KeystoreView.ALIAS)) {
			result = c.getName();
		} else if (columnIndex == KeystoreView
				.getColumnFor(KeystoreView.SUBJECT)) {
			result = c.getSubjectCN();
		} else if (columnIndex == KeystoreView
				.getColumnFor(KeystoreView.ISSUER)) {
			result = c.getIssuerCN();
		} else if (columnIndex == KeystoreView
				.getColumnFor(KeystoreView.VALID_TO)) {
			result = c.getValidTo();
		}

		return result;
	}

}
