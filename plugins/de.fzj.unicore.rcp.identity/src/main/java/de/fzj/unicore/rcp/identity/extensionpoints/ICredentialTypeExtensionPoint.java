package de.fzj.unicore.rcp.identity.extensionpoints;

import java.util.List;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.identity.extensions.CredentialTypeCertificateExtension;
import de.fzj.unicore.rcp.identity.profiles.Profile;
import de.fzj.unicore.rcp.identity.profiles.ProfileList;
import eu.unicore.util.httpclient.IClientConfiguration;

public interface ICredentialTypeExtensionPoint {

	/**
	 * Unique id for the extension point
	 */
	public static final String ID = "de.fzj.unicore.rcp.identity.extensionpoints.CredentialType";
	/**
	 * Name of the attribute that declares the header of the column defined by
	 * the extension.
	 */
	public static final String LABEL = "label";
	/**
	 * Name of the attribute that declares the tooltip of the column defined by
	 * the extension.
	 */
	public static final String TOOLTIP = "tooltip";

	/**
	 * Called when security properties are needed for accessing a Grid service.
	 * Add arbitrary security information to the properties. The matching
	 * profile and identifier String(s) for the Grid service are being passed as
	 * arguments so they can influence any decision if needed (e.g. previously
	 * stored information can be fetched via the
	 * {@link Profile#getUserData(String)} method). Note: the order in which
	 * this method is called on the different extensions is undefined so DO NOT
	 * use information from the passed security properties for your decisions!
	 * 
	 * @param secProps
	 *            security properties object to which new security information
	 *            should be added.
	 * @param profile
	 * @param identifiers
	 */
	public void addCredentials(IClientConfiguration secProps,
			Profile profile, List<String> identifiers);

	/**
	 * Hook for performing some cleanup when the extension is not needed
	 * anymore.
	 */
	public void dispose();

	/**
	 * Returns the cell editor to be used for modifying the profile in the
	 * profile list view. The cell editors are expected to take a single profile
	 * as input and return a single (modified) profile. Modifications to the
	 * profile may be done via the {@link Profile#setUserData(String, Object)}
	 * method. Note: the passed Object must be serializable through xstream!
	 * 
	 * @param parent
	 * @param profile
	 * @return the cell editor for the new column in the profile list or null if
	 *         no additional column is needed (e.g. if you just want to add a
	 *         security handler)
	 */
	public CellEditor getCellEditor(Composite parent, Profile profile);

	/**
	 * Returns the image to be displayed in the column that is defined by this
	 * extension for a given profile. Return null iff no image shall be
	 * displayed.
	 * 
	 * @param profile
	 * @return image for the new column in the profile list or null if no
	 *         additional column is needed (e.g. if you just want to add a
	 *         security handler)
	 */
	public Image getImage(Profile profile);

	/**
	 * Returns the label to be displayed in the column that is defined by this
	 * extension for a given profile.
	 * 
	 * @param profile
	 * @returnthe label for the new column in the profile list or null if no
	 *            additional column is needed (e.g. if you just want to add a
	 *            security handler)
	 */
	public String getLabel(Profile profile);

	/**
	 * Called when a new profile is being created. Initialize the profile by
	 * adding data (to your own data model or through the
	 * {@link Profile#setUserData(String, Object)} method), if needed.
	 * 
	 * @param newProfile
	 */
	public void initProfile(Profile newProfile);

	/**
	 * Extensions might need to modify profiles dynamically during runtime due
	 * to changes in their underlying data models. They can do so by storing and
	 * directly accessing the profile list. This method is called soon after the
	 * extension is instantiated. As an example of how to dynamically modify
	 * profiles, have a look at
	 * {@link CredentialTypeCertificateExtension#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)}
	 * .
	 * 
	 * @param profileList
	 */
	public void setProfileList(ProfileList profileList);

}