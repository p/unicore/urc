/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.identity.sites;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.IDialogSettings;

import com.thoughtworks.xstream.XStream;

import de.fzj.unicore.rcp.common.utils.BackwardsCompatibleURIConverter;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.utils.ProfileUtils;

/**
 * @author demuth
 * 
 */
public class SiteList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3587837120496809561L;
	private static SiteList defaultSiteList;
	List<String> siteNames = new ArrayList<String>();
	Map<String, Site> sites = new HashMap<String, Site>();

	/**
	 * views that are interested in model changes
	 */
	private transient Set<ISiteListViewer> changeListeners = new HashSet<ISiteListViewer>();

	public SiteList() {

	}

	public void addChangeListener(ISiteListViewer viewer) {
		getChangeListeners().add(viewer);
	}

	/**
	 * Add a site to the site list
	 * 
	 * @param site
	 */
	public void addSite(Site site) throws Exception {
		synchronized (sites) {
			String name = site.getPattern();
			if (sites.containsKey(name)) {
				throw new Exception("Site already exists!");
			}
			siteNames.add(name);
			sites.put(name, site);
			persistSiteList();
		}
		synchronized (getChangeListeners()) {
			Iterator<ISiteListViewer> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next().addSite(site);
			}
		}
	}

	public void changePattern(Site site, String newPattern) throws Exception {
		synchronized (sites) {
			Site old = sites.get(site.getPattern());
			if (old == null || !old.equals(site)) {
				throw new Exception(
						"Could not change the site definition, unknown site "
								+ site.getPattern());
			}
			int index = siteNames.indexOf(site.getPattern());
			siteNames.remove(index);
			sites.remove(site.getPattern());

			siteNames.add(index, newPattern);
			site.setPattern(newPattern);
			sites.put(newPattern, site);
			persistSiteList();
		}
		synchronized (getChangeListeners()) {
			Iterator<ISiteListViewer> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next().updateSite(site);
			}
		}
	}

	protected Set<ISiteListViewer> getChangeListeners() {
		if (changeListeners == null) {
			changeListeners = new HashSet<ISiteListViewer>();
		}
		return changeListeners;
	}

	public Site getSite(String siteName) {
		return sites.get(siteName);
	}

	public Site[] getSites() {
		synchronized (sites) {
			Site[] siteArr = new Site[siteNames.size()];
			int i = 0;
			Iterator<String> iter = siteNames.iterator();
			while (iter.hasNext()) {
				String name = iter.next();
				siteArr[i++] = sites.get(name);
			}
			return siteArr;
		}
	}

	/**
	 * Match the incoming identifiers against the patterns of known sites.
	 * Return the site pattern that is longest prefix of all incoming
	 * identifiers. If no site pattern is a prefix, return null;
	 * 
	 * @param identifier
	 * @return
	 */
	public Site matchSite(List<String> identifiers) {
		Site result = null;
		synchronized (sites) {
			int maxPrefixLength = 0;
			for (String identifier : identifiers) {
				for (Site site : getSites()) {
					String prefix = site.getPattern();
					int matchedLength = 0;
					boolean matchAgainstURI = false;
					try {
						matchAgainstURI = new URI(identifier).getScheme() != null;
					} catch (Exception e) {
						// do nothing
					}
					if (matchAgainstURI) {
						// matching against "real" uri: use the number of
						// characters
						// to determine longest prefix
						matchedLength = prefix.length();
					} else {
						// matching against name path: use path length
						// and ignore the number of characters in names
						matchedLength = ProfileUtils.computePathLength(prefix);
					}

					if (prefix.endsWith("*")) {
						String pattern = prefix.substring(0,
								prefix.length() - 1);

						if (identifier.startsWith(pattern)
								&& matchedLength > maxPrefixLength) {
							result = site;
							maxPrefixLength = matchedLength;
						}
					} else {
						if (identifier.equals(prefix)
								&& matchedLength > maxPrefixLength) {
							result = site;
							maxPrefixLength = matchedLength;
						}
					}
				}
			}
		}
		return result;
	}

	protected void persistSiteList() {

		IDialogSettings s = getDialogSettings();
		XStream xstream = new XStream();
		String myself = xstream.toXML(this);
		s.put(getClass().getName(), myself);

	}

	public void removeChangeListener(ISiteListViewer viewer) {

		getChangeListeners().remove(viewer);
	}

	/**
	 * Remove a site from the site list
	 * 
	 * @param site
	 */
	public void removeSite(Site site) {
		synchronized (sites) {
			String name = site.getPattern();
			siteNames.remove(name);
			sites.remove(name);
			persistSiteList();
		}
		synchronized (getChangeListeners()) {
			Iterator<ISiteListViewer> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next().removeSite(site);
			}
		}
	}

	/**
	 * Update a site
	 * 
	 * @param site
	 */
	public void updateSite(Site site) {
		synchronized (sites) {
			persistSiteList();
		}
		synchronized (getChangeListeners()) {
			Iterator<ISiteListViewer> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next().updateSite(site);
			}
		}
	}

	/**
	 * Returns the dialog settings object used to maintain state between dialogs
	 * 
	 * @return the dialog settings to be used
	 */
	private static IDialogSettings getDialogSettings() {
		String sectionName = SiteList.class.getName();
		IDialogSettings settings = IdentityActivator.getDefault()
				.getDialogSettings();
		IDialogSettings mySettings = settings.getSection(sectionName);
		if (mySettings == null) {
			mySettings = settings.addNewSection(sectionName);
		}
		return mySettings;
	}

	public static SiteList reloadSiteList() {
		try {
			IDialogSettings s = getDialogSettings();
			String list = s.get(SiteList.class.getName());
			if (list == null || list.trim().length() == 0) {
				defaultSiteList = new SiteList();
				return defaultSiteList;
			}
			XStream xstream = new XStream();
			xstream.registerConverter(new BackwardsCompatibleURIConverter());
			xstream.setClassLoader(SiteList.class.getClassLoader());
			return (SiteList) xstream.fromXML(list);

		} catch (Exception e) {
			IdentityActivator.log(IStatus.ERROR,
					"Unable to restore site specific security settings!", e);
			defaultSiteList = new SiteList();
			return defaultSiteList;
		}
	}

}
