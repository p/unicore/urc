package de.fzj.unicore.rcp.identity.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import de.fzj.unicore.rcp.common.utils.IValidityChangeListener;
import de.fzj.unicore.rcp.common.utils.ValidityChangeEvent;
import de.fzj.unicore.rcp.identity.DistinguishedName;
import de.fzj.unicore.rcp.identity.IdentityActivator;

/**
 * This dialog displays the details of certificates in a keystore.
 * 
 * @author Valentina Huber, Bastian Demuth
 */
public class CertificateRequestDialog extends Dialog implements
		IValidityChangeListener {

	CertificateRequestComposite inner;

	/** Ok button widget. */
	private Button okButton;

	private String title;
	private String message;

	private Image img = IdentityActivator.getImageDescriptor(
			"page_white_add.png").createImage();
	private DistinguishedName dn;

	/**
	 * Creates an input dialog with OK and Cancel buttons. Note that the dialog
	 * will have no visual representation (no widgets) until it is told to open.
	 * <p>
	 * Note that the <code>open</code> method blocks for input dialogs.
	 * </p>
	 * 
	 * @param parentShell
	 *            the parent shell, or <code>null</code> to create a top-level
	 *            shell
	 * @param dialogTitle
	 *            the dialog title, or <code>null</code> if none
	 */
	public CertificateRequestDialog(Shell parentShell, String dialogTitle) {
		this(
				parentShell,
				dialogTitle,
				"Please enter the personal information that should be transmitted to the certification authority:");

	}

	public CertificateRequestDialog(Shell parentShell, String dialogTitle,
			String message) {
		super(parentShell);
		setShellStyle(SWT.DIALOG_TRIM | SWT.RESIZE | SWT.MAX
				| SWT.APPLICATION_MODAL);
		title = dialogTitle;
		this.message = message;
	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			if (inner.isInputValid()) {
				dn = inner.getDN();
			}
		}
		super.buttonPressed(buttonId);
	}

	@Override
	public boolean close() {
		if (inner != null) {
			inner.removeValidityChangeListener(this);
			inner = null;
		}
		boolean b = super.close();
		img.dispose();
		img = null;
		return b;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets
	 * .Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		if (title != null) {
			shell.setText(title);
		}
		shell.setImage(img);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse
	 * .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons by default
		okButton = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		okButton.setEnabled(false);
		if (inner != null) {
			okButton.setEnabled(inner.isInputValid());
			inner.addValidityChangeListener(this);
		}
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		// create composite
		Composite composite = (Composite) super.createDialogArea(parent);

		inner = new CertificateRequestComposite(composite, message);
		applyDialogFont(inner);
		return composite;
	}

	/**
	 * Gets the distinguished name.
	 * 
	 */
	public DistinguishedName getDN() {
		return dn;
	}

	/**
	 * Returns the ok button.
	 * 
	 * @return the ok button
	 */
	protected Button getOkButton() {
		return okButton;
	}

	public void validityChanged(ValidityChangeEvent e) {
		okButton.setEnabled(e.getNewValidity());

	}
}