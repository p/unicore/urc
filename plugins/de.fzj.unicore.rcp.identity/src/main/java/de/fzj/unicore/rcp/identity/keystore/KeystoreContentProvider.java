package de.fzj.unicore.rcp.identity.keystore;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import de.fzj.unicore.rcp.identity.KeyStoreManager;

public class KeystoreContentProvider implements IStructuredContentProvider {

	private KeyStoreManager keystoreManager;

	public void dispose() {

	}

	public Object[] getElements(Object inputElement) {
		List<String> aliases = keystoreManager.getIdentityAliases();
		aliases.addAll(keystoreManager.getTrustedAliases());
		return aliases.toArray();
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		keystoreManager = (KeyStoreManager) newInput;

	}

}
