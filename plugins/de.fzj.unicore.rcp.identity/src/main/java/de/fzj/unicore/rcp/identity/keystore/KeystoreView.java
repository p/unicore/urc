/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.identity.keystore;

import java.io.File;
import java.util.Iterator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.guicomponents.ValidatingListSelectionDialog;
import de.fzj.unicore.rcp.common.utils.TableColumnSorter;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.IdentityConstants;
import de.fzj.unicore.rcp.identity.KeyStoreManager;
import de.fzj.unicore.rcp.identity.actions.ImportKeystoreAction;
import de.fzj.unicore.rcp.identity.utils.InputUtils;
import de.fzj.unicore.rcp.logmonitor.utils.FileDialogUtils;

/**
 * The keystore view to take actions on keys stored in the URC's keystore.
 * 
 * @author Bastian Demuth, Valentina Huber
 */
public class KeystoreView extends ViewPart {

	public static final String ID = "de.fzj.unicore.rcp.identity.keystore";
	private TableViewer viewer;
	private Action importKeystoreAction;
	private Action removeKeyAction;
	private Action exportPublicKeyAction;
	private Action exportKeyStoreAction;
	private Action showDetailsAction;
	private Action changeAliasAction;
	private Action changePasswordAction;
	private Action generateCSRAction;
	private Action importCAReplyAction;
	private Action doubleClickAction;

	private CheckboxCellEditor defaultCertEditor;
	// column headers
	public static final String DEFAULT_CERT = "default", ALIAS = "alias",
			SUBJECT = "issued for", ISSUER = "issued by",
			VALID_TO = "valid to";

	// the order of the header names in COLUMN_PROPERTIES determines the order
	// of columns in the table
	public static final String[] COLUMN_PROPERTIES = new String[] {
			DEFAULT_CERT, ALIAS, SUBJECT, ISSUER, VALID_TO };


	private CertificateList getCertificateList(){
		return ((KeystoreCredentialController)IdentityActivator.getDefault().getCredential())
				.getCertificateList();
	}

	private File askCAReplyFile() {
		FileDialog dialog = new FileDialog(getViewer().getControl().getShell(),
				SWT.OPEN);
		String filterPath = FileDialogUtils.getFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS);
		dialog.setFilterPath(filterPath);
		dialog.setFilterExtensions(Constants.PUBLIC_KEY_FORMATS);
		dialog.setText("Please select the reply from the certification authority.");
		String filename = dialog.open();
		FileDialogUtils.saveFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS, dialog.getFilterPath());
		return (filename != null) ? new File(filename) : null;
	}

	private File askExportKeystoreFile() {
		FileDialog dialog = new FileDialog(getViewer().getControl().getShell(),
				SWT.SAVE);
		String filterPath = FileDialogUtils.getFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS);
		dialog.setFilterPath(filterPath);
		dialog.setFilterExtensions(Constants.REUSABLE_KEYSTORE_FORMATS);
		dialog.setText("Please choose a file to which the selected keys shall be exported.");
		String filename = dialog.open();
		FileDialogUtils.saveFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS, dialog.getFilterPath());
		return (filename != null) ? new File(filename) : null;
	}

	private File askPublicKeyFile(String alias) {
		FileDialog dialog = new FileDialog(getViewer().getControl().getShell(),
				SWT.SAVE);
		String filterPath = FileDialogUtils.getFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS);
		dialog.setFilterPath(filterPath);
		dialog.setFilterExtensions(Constants.PUBLIC_KEY_FORMATS);
		dialog.setText("Please select a file you want to store " + alias
				+ " public key to.");
		String filename = dialog.open();
		FileDialogUtils.saveFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS, dialog.getFilterPath());
		File f = null;
		if (filename != null) {
			if (!filename.endsWith(Constants.PUBLIC_KEY_FORMAT)) {
				filename += Constants.PUBLIC_KEY_FORMAT;
			}
			f = new File(filename);
		}
		return f;
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		if(IdentityActivator.getDefault().getCredential() instanceof KeystoreCredentialController){
			Table table = createTable(parent);
			createTableViewer(table);
			makeActions();
			hookContextMenu();
			hookDoubleClickAction();
			contributeToActionBars();
		}
		else{
			Label label = new Label(parent, SWT.NONE);
			label.setText("No keystore is used.");
		}
	}

	private Table createTable(Composite parent) {
		int style = SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION;
		Table table = new Table(parent, style);
		for (int i = 0; i < COLUMN_PROPERTIES.length; i++) {
			TableColumn col = new TableColumn(table, SWT.LEFT, i);
			col.setText(COLUMN_PROPERTIES[i]);
			if (COLUMN_PROPERTIES[i].equals(ALIAS)) {
				col.setToolTipText("The alias used to identify the certificate in the keystore.");
				col.setWidth(150);
			} else if (COLUMN_PROPERTIES[i].equals(SUBJECT)) {
				col.setToolTipText("Common name of the certificate holder.");
				col.setWidth(150);
			} else if (COLUMN_PROPERTIES[i].equals(ISSUER)) {
				col.setToolTipText("Common name of the certificate issuer.");
				col.setWidth(180);
			} else if (COLUMN_PROPERTIES[i].equals(VALID_TO)) {
				col.setToolTipText("The date until which this certificate is usable.");
				col.setWidth(130);
			} else if (COLUMN_PROPERTIES[i].equals(DEFAULT_CERT)) {
				col.setToolTipText("Select the default certificate to be used for unknown sites.");
				col.setWidth(50);
				col.setAlignment(SWT.RIGHT);
			}
		}

		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		return table;

	}

	private TableViewer createTableViewer(Table table) {
		CertificateList certificateList = getCertificateList();

		viewer = new TableViewer(table);
		viewer.setUseHashlookup(true);

		// Create the cell editors
		CellEditor[] editors = new CellEditor[table.getColumnCount()];

		// default certificate? (Checkbox)
		defaultCertEditor = new CheckboxCellEditor(table);
		editors[getColumnFor(DEFAULT_CERT)] = defaultCertEditor;

		viewer.setCellEditors(editors);

		CertCellModifier modifier = new CertCellModifier(this);
		viewer.setCellModifier(modifier);
		viewer.setContentProvider(new CertContentProvider(viewer));
		viewer.setLabelProvider(new CertLabelProvider());
		viewer.setColumnProperties(COLUMN_PROPERTIES);
		viewer.setInput(certificateList);

		for (int i = 0; i < table.getColumnCount(); i++) {
			TableColumn col = table.getColumn(i);
			final int index = i;
			new TableColumnSorter(viewer, col) {
				@Override
				protected int doCompare(Viewer v, Object e1, Object e2) {
					ITableLabelProvider lp = ((ITableLabelProvider) viewer
							.getLabelProvider());
					String t1 = lp.getColumnText(e1, index);
					String t2 = lp.getColumnText(e2, index);
					return t1.compareTo(t2);
				}
			};
		}
		return viewer;
	}

	@Override
	public void dispose() {
		super.dispose();
		try {
			getViewer().getLabelProvider().dispose();
		} catch (Exception e) {
			// do nothing
		}
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(importKeystoreAction);
		manager.add(removeKeyAction);
		manager.add(showDetailsAction);
		manager.add(changeAliasAction);
		manager.add(exportPublicKeyAction);
		manager.add(exportKeyStoreAction);
		manager.add(changePasswordAction);
		manager.add(generateCSRAction);
		manager.add(importCAReplyAction);

		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalPullDown(IMenuManager manager) {
		// manager.add(importKeystoreAction);
		// manager.add(removeKeyAction);
		// manager.add(exportPublicKeyAction);
		// manager.add(changePasswordAction);
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(importKeystoreAction);
		manager.add(removeKeyAction);
		manager.add(showDetailsAction);
		manager.add(changeAliasAction);
		manager.add(exportPublicKeyAction);
		manager.add(exportKeyStoreAction);
		manager.add(changePasswordAction);
		manager.add(generateCSRAction);
		manager.add(importCAReplyAction);
	}

	public CheckboxCellEditor getDefaultCertEditor() {
		return defaultCertEditor;
	}

	public TableViewer getViewer() {
		return viewer;
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	private KeystoreCredentialController getKS(){
		return (KeystoreCredentialController)IdentityActivator.getDefault().getCredential();
	}
	
	private void makeActions() {
		importKeystoreAction = new ImportKeystoreAction();
		importKeystoreAction.setText("Import keystore");

		removeKeyAction = new Action() {
			@Override
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getViewer()
						.getSelection();
				for (Iterator<?> iter = selection.iterator(); iter
						.hasNext();) {
					Certificate cert = (Certificate) iter.next();
					if (MessageDialog.openConfirm(getViewer().getControl()
							.getShell(), "Confirm",
							"Do you want to remove the key '" + cert.getName()
									+ "' from your keystore?")) {
						try {
							getKS().removeKeyEntry(cert);
						} catch (Exception e) {
							IdentityActivator.log(IStatus.ERROR,
									"Could not remove key!", e);
						}
					}
				}
				;
			}
		};
		removeKeyAction.setText("Remove key");
		removeKeyAction
				.setToolTipText("Remove selected key(s) from your keystore");
		removeKeyAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("delete.png"));

		exportPublicKeyAction = new Action() {
			@Override
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getViewer()
						.getSelection();
				for (Iterator<?> iter = selection.iterator(); iter
						.hasNext();) {
					Certificate cert = (Certificate) iter.next();
					try {
						File file = askPublicKeyFile(cert.getName());
						if (file != null) {
							getKS().exportPublicKey(cert, file);
						}
					} catch (Exception e) {
						IdentityActivator.log(IStatus.ERROR,
								"Could not export public key!", e);
					}
				}
				;
			}
		};
		exportPublicKeyAction.setText("Export public key");
		exportPublicKeyAction
				.setToolTipText("Export selected public key(s) to local file(s)");
		exportPublicKeyAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("key_go.png"));

		exportKeyStoreAction = new Action() {
			@Override
			public void run() {

				try {
					Shell parentShell = PlatformUI.getWorkbench().getDisplay()
							.getActiveShell();
					KeyStoreManager manager = getKeystoreManager();
					final ValidatingListSelectionDialog dialog = new ValidatingListSelectionDialog(
							parentShell, manager,
							new KeystoreContentProvider(),
							new KeystoreLabelProvider(),
							"Select keys to be exported:");

					dialog.setBlockOnOpen(true);
					dialog.open();

					if (dialog.getReturnCode() == Window.OK
							&& dialog.getResult().length > 0) {
						File file = askExportKeystoreFile();
						if (file != null) {
							try {
								String password = new InputUtils().askPassword(file, Display.getCurrent().getActiveShell());
								if (password != null) {
									String myPassword = IdentityActivator
											.getDefault().getCredentialPassphrase();
									Object[] dialogResult = dialog.getResult();
									String[] aliases = new String[dialogResult.length];
									char[][] privateKeyPasswords = new char[aliases.length][];
									for (int i = 0; i < dialogResult.length; i++) {
										String alias = (String) dialogResult[i];
										aliases[i] = alias;
										privateKeyPasswords[i] = myPassword
												.toCharArray();
									}
									manager.exportCerts(aliases,
											privateKeyPasswords,
											file.getAbsolutePath(),
											password.toCharArray());
								}
							} catch (Exception e) {
								IdentityActivator.log(
										IStatus.ERROR,
										"Won't export keystore: "
												+ e.getMessage(), e);
							}
						}
					}

				} catch (Exception e) {
					IdentityActivator.log(IStatus.ERROR,
							"Could not export keys!", e);
				}

			}
		};
		exportKeyStoreAction.setText("Export public and private keys");
		exportKeyStoreAction
				.setToolTipText("Export public and private key(s) to a keystore file");
		exportKeyStoreAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("export_keys.png"));

		changePasswordAction = new Action() {
			@Override
			public void run() {
				try {
					getKS().changeKeystorePassword();
				} catch (Exception e) {
					IdentityActivator.log(IStatus.ERROR,
							"Could not change keystore password!", e);
				}
			}
		};
		changePasswordAction.setText("Change keystore password");
		changePasswordAction.setToolTipText("Change keystore password");
		changePasswordAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("lock_edit.png"));

		showDetailsAction = new Action() {
			@Override
			public void run() {
				showDetails();
			}
		};
		showDetailsAction.setText("Show details...");
		showDetailsAction.setToolTipText("Show certificate(s) details");
		showDetailsAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("magnifier.png"));

		changeAliasAction = new Action() {
			@Override
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getViewer()
						.getSelection();
				for (Iterator<?> iter = selection.iterator(); iter
						.hasNext();) {
					Certificate cert = (Certificate) iter.next();
					try {
						getKS().changeAlias(cert);
					} catch (Exception e) {
						IdentityActivator.log(IStatus.ERROR,
								"Could not change alias for the selected key!",
								e);
					}
				}
				;
			}
		};
		changeAliasAction.setText("Change alias...");
		changeAliasAction
				.setToolTipText("Change alias for selected key(s) in your keystore");
		changeAliasAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("alias_edit.png"));

		generateCSRAction = new Action() {
			@Override
			public void run() {
				try {
					getKS().generateCSR();
				} catch (Exception e) {
					IdentityActivator.log(IStatus.ERROR,
							"Could not generate certificate request!", e);
				}
			}
		};
		generateCSRAction.setText("Generate certificate request");
		generateCSRAction
				.setToolTipText("Generate certificate request to a file");
		generateCSRAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("page_white_add.png"));

		importCAReplyAction = new Action() {
			@Override
			public void run() {
				try {
					File f = askCAReplyFile();
					if (f == null) {
						return;
					}
					Certificate cert = new Certificate(f);
					getKS().importCAReply(cert);
				} catch (Exception e) {
					IdentityActivator.log(IStatus.ERROR,
							"Could not import reply from CA!", e);
				}
			}
		};
		importCAReplyAction.setText("Import reply from the CA");
		importCAReplyAction
				.setToolTipText("Import the reply to a certificate request");
		importCAReplyAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("key_add.png"));

		doubleClickAction = new Action() {
			@Override
			public void run() {
				showDetails();
			}
		};
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		if(viewer!=null){
			viewer.getControl().setFocus();
		}
	}

	public void setViewer(TableViewer viewer) {
		this.viewer = viewer;
	}

	private void showDetails() {
		IStructuredSelection selection = (IStructuredSelection) getViewer()
				.getSelection();
		for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
			Certificate cert = (Certificate) iter.next();
			try {
				getKS().displayCertificateDetails(cert);
			} catch (Exception e) {
				IdentityActivator.log(IStatus.ERROR,
						"Could not display certificate details!", e);
			}
		}
	}

	protected KeyStoreManager getKeystoreManager(){
		KeystoreCredentialController kcc = (KeystoreCredentialController)IdentityActivator.getDefault().getCredential();
		return kcc.getKeystoreManager();
	}
	
	public static int getColumnFor(String columnName) {
		for (int i = 0; i < COLUMN_PROPERTIES.length; i++) {
			if (COLUMN_PROPERTIES[i].equals(columnName)) {
				return i;
			}
		}
		return -1;
	}

}