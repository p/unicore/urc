package de.fzj.unicore.rcp.identity.sites;

import java.io.IOException;

import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.part.ViewPart;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.profiles.ProfileList;
import de.fzj.unicore.rcp.identity.profiles.ProfileListTableViewer;

public class SiteListView extends ViewPart {

	public static final String ID = "de.fzj.unicore.rcp.identity.sitelist";

	private SiteListTableViewer siteListTableViewer;
	private ProfileListTableViewer profileListTableViewer;

	/**
	 * The constructor.
	 * 
	 * @throws IOException
	 */
	public SiteListView() {
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		Layout layout = new FormLayout();
		composite.setLayout(layout);
		FormData data;

		int vSpace = 5;
		Label profilesLabel = new Label(composite, SWT.NONE);
		profilesLabel.setText("Available Security Profiles:");
		data = new FormData();
		data.top = new FormAttachment(0, vSpace);
		data.left = new FormAttachment(0, 20);
		profilesLabel.setLayoutData(data);

		profileListTableViewer = createProfileListTableViewer(composite);

		data = new FormData();
		data.top = new FormAttachment(profilesLabel, vSpace);
		data.bottom = new FormAttachment(40, 0);
		data.left = new FormAttachment(0, 20);
		data.right = new FormAttachment(100, -20);
		profileListTableViewer.getControl().setLayoutData(data);

		Label siteLabel = new Label(composite, SWT.NONE);
		siteLabel.setText("Map sites to security profiles:");
		data = new FormData();
		data.top = new FormAttachment(profileListTableViewer.getControl(),
				vSpace);
		data.left = new FormAttachment(0, 20);
		siteLabel.setLayoutData(data);

		siteListTableViewer = createSiteListTableViewer(composite);
		data = new FormData();
		data.top = new FormAttachment(siteLabel, vSpace);
		data.left = new FormAttachment(0, 20);
		data.right = new FormAttachment(100, -20);
		data.bottom = new FormAttachment(100, -20);
		siteListTableViewer.getControl().setLayoutData(data);

		hookContextMenu();
		contributeToActionBars();
	}

	private ProfileListTableViewer createProfileListTableViewer(Composite parent) {
		ProfileList profileList = IdentityActivator.getDefault()
				.getProfileList();
		return new ProfileListTableViewer(parent, profileList);
	}

	private SiteListTableViewer createSiteListTableViewer(Composite parent) {
		SiteList siteList = IdentityActivator.getDefault().getSiteList();
		return new SiteListTableViewer(parent, siteList);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(siteListTableViewer.getAddSiteAction());
		manager.add(siteListTableViewer.getRemoveSiteAction());

		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalPullDown(IMenuManager manager) {

	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(siteListTableViewer.getAddSiteAction());
		manager.add(siteListTableViewer.getRemoveSiteAction());
	}

	public TableViewer getSiteListTableViewer() {
		return siteListTableViewer;
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				SiteListView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(siteListTableViewer.getControl());
		siteListTableViewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, siteListTableViewer);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		siteListTableViewer.getControl().setFocus();
	}

}