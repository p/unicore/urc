/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.identity.profiles;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.IDialogSettings;

import com.thoughtworks.xstream.XStream;

import de.fzj.unicore.rcp.common.utils.DelegatingExtensionClassloader;
import de.fzj.unicore.rcp.common.utils.BackwardsCompatibleURIConverter;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.extensionpoints.ICredentialTypeExtensionPoint;

/**
 * @author demuth
 */
 public class ProfileList implements Serializable {

	private static final long serialVersionUID = -1100328049055858081L;

	private static ProfileList defaultProfileList;
	List<String> profileNames = new ArrayList<String>();
	Map<String, Profile> profiles = new HashMap<String, Profile>();

	/**
	 * views that are interested in model changes
	 */
	private transient Set<IProfileListViewer> changeListeners = new HashSet<IProfileListViewer>();

	public ProfileList() {

	}

	public void addChangeListener(IProfileListViewer viewer) {
		getChangeListeners().add(viewer);
	}

	/**
	 * Add a profile to the profile list
	 * 
	 * @param profile
	 */
	public void addProfile(Profile profile) throws Exception {
		synchronized (profiles) {
			String name = profile.getName();
			if (profiles.containsKey(name)) {
				throw new Exception("Profile already exists!");
			}
			profileNames.add(name);
			profiles.put(name, profile);
			persistProfileList();
		}
		synchronized (getChangeListeners()) {
			Iterator<IProfileListViewer> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next().addProfile(profile);
			}
		}
	}

	public void changeName(Profile profile, String newName) throws Exception {
		String oldName = profile.getName();
		synchronized (profiles) {
			Profile old = profiles.get(oldName);
			if (old == null || !old.equals(profile)) {
				throw new Exception(
						"Could not change the profile definition, unknown profile "
								+ profile.getName());
			}
			int index = profileNames.indexOf(profile.getName());
			profileNames.remove(index);
			profiles.remove(profile.getName());

			profileNames.add(index, newName);
			profile.setName(newName);
			profiles.put(newName, profile);
			persistProfileList();
		}
		synchronized (getChangeListeners()) {
			Iterator<IProfileListViewer> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next().profileNameChanged(
						oldName, newName, profile);
			}
		}
	}

	protected Set<IProfileListViewer> getChangeListeners() {
		if (changeListeners == null) {
			changeListeners = new HashSet<IProfileListViewer>();
		}
		return changeListeners;
	}

	public Profile getDefaultProfile() {
		if (profiles != null && profiles.size() > 0) {
			return profiles.get(profileNames.get(0));
		} else {
			return null;
		}
	}

	public Profile getProfile(String name) {
		return profiles.get(name);
	}

	public String[] getProfileNames() {
		Profile[] profiles = getProfiles();
		String[] result = new String[profiles.length];
		for (int i = 0; i < profiles.length; i++) {
			result[i] = profiles[i].getName();
		}
		return result;
	}

	public Profile[] getProfiles() {
		synchronized (profiles) {
			Profile[] profileArr = new Profile[profileNames.size()];
			int i = 0;
			Iterator<String> iter = profileNames.iterator();
			while (iter.hasNext()) {
				String name = iter.next();
				profileArr[i++] = profiles.get(name);
			}
			return profileArr;
		}
	}

	protected void persistProfileList() {

		IDialogSettings s = getDialogSettings();
		XStream xstream = new XStream();
		String myself = xstream.toXML(this);
		s.put(getClass().getName(), myself);

	}

	public void removeChangeListener(IProfileListViewer viewer) {
		getChangeListeners().remove(viewer);
	}

	/**
	 * Remove a profile from the profile list
	 * 
	 * @param profile
	 */
	public void removeProfile(Profile profile) {
		synchronized (profiles) {
			String name = profile.getName();
			profileNames.remove(name);
			profiles.remove(name);
			persistProfileList();
		}
		synchronized (getChangeListeners()) {
			Iterator<IProfileListViewer> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next().removeProfile(profile);
			}
		}
	}

	/**
	 * Update a profile
	 * 
	 * @param profile
	 */
	public void updateProfile(Profile profile) {
		synchronized (profiles) {
			persistProfileList();
		}
		synchronized (getChangeListeners()) {
			Iterator<IProfileListViewer> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next().userDataChanged(profile);
			}
		}
	}

	/**
	 * Returns the dialog settings object used to maintain state between dialogs
	 * 
	 * @return the dialog settings to be used
	 */
	private static IDialogSettings getDialogSettings() {
		String sectionName = ProfileList.class.getName();
		IDialogSettings settings = IdentityActivator.getDefault()
				.getDialogSettings();
		IDialogSettings mySettings = settings.getSection(sectionName);
		if (mySettings == null) {
			mySettings = settings.addNewSection(sectionName);
		}
		return mySettings;
	}

	public static ProfileList reloadProfileList() {
		try {
			IDialogSettings s = getDialogSettings();
			String list = s.get(ProfileList.class.getName());
			if (list == null || list.trim().length() == 0) {
				defaultProfileList = new ProfileList();
				return defaultProfileList;
			}
			XStream xstream = new XStream();
			xstream.registerConverter(new BackwardsCompatibleURIConverter());
			DelegatingExtensionClassloader classloader = new DelegatingExtensionClassloader(
					ProfileList.class.getClassLoader(),
					ICredentialTypeExtensionPoint.ID);
			xstream.setClassLoader(classloader);
			return (ProfileList) xstream.fromXML(list);

		} catch (Exception e) {
			IdentityActivator.log(IStatus.ERROR,
					"Unable to restore security profiles!", e);
			defaultProfileList = new ProfileList();
			return defaultProfileList;
		}
	}

}
