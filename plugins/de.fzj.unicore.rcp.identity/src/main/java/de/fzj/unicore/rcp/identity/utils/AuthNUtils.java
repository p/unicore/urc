package de.fzj.unicore.rcp.identity.utils;

import java.io.IOException;

import eu.unicore.security.canl.PasswordCallback;
import eu.unicore.security.wsutil.client.authn.SecuritySessionPersistence;
import eu.unicore.security.wsutil.client.authn.UsernameCallback;
import eu.unicore.util.httpclient.SessionIDProvider;

public class AuthNUtils {
	
	private AuthNUtils(){}
	
	public static final SecuritySessionPersistence NOP_SESSION_PERSISTENCE 
		= new SecuritySessionPersistence() {
		
		@Override
		public void storeSessionIDs(SessionIDProvider sessionProvider)
				throws IOException {
		}
		
		@Override
		public void readSessionIDs(SessionIDProvider sessionProvider)
				throws IOException {
		}
		
	};
	

	public static final PasswordCallback NOP_PASSWORD_CALLBACK = new PasswordCallback() {
		@Override
		public boolean ignoreProperties() {
			return false;
		}
		@Override
		public char[] getPassword(String arg0, String arg1) {
			throw new IllegalStateException("UNITY: password is not known!");
		}
		@Override
		public boolean askForSeparateKeyPassword() {
			return false;
		}
	};
	
	public static final UsernameCallback NOP_USERNAME_CALLBACK = new UsernameCallback() {
		@Override
		public String getUsername() {
			throw new IllegalStateException("UNITY: username is not known!");
		}
	};

}
