package de.fzj.unicore.rcp.identity.ui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.IdentityConstants;
import de.fzj.unicore.rcp.logmonitor.TextEditorDialog;

public class CertificateExpiryDialog extends TextEditorDialog {

	Button showExpiryWarnings;

	public CertificateExpiryDialog(Shell parent, String text) {
		super(parent, "Certificate expiry warning", text);
	}

	@Override
	protected void createButtons(Composite parent) {
		super.createButtons(parent);
		showExpiryWarnings = new Button(parent, SWT.CHECK);
		showExpiryWarnings.setText("Don't show expiry warnings again");
		showExpiryWarnings.setLayoutData(new GridData());
	}

	@Override
	protected boolean doExit() {
		boolean continueChecking = !showExpiryWarnings.getSelection();
		boolean ok = super.doExit();
		IdentityActivator
				.getDefault()
				.getPreferenceStore()
				.setValue(IdentityConstants.P_SHOW_EXPIRY_WARNINGS,
						continueChecking);
		return ok;
	}

	@Override
	protected Point getPreferredSize() {
		return new Point(500, 279);
	}

}
