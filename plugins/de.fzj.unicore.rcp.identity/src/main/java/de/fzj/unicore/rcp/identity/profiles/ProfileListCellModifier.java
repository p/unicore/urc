/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.identity.profiles;

import java.net.URISyntaxException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.swt.widgets.TableItem;

import de.fzj.unicore.rcp.identity.IdentityActivator;

/**
 * @author demuth
 * 
 */
public class ProfileListCellModifier implements ICellModifier {

	private ProfileListTableViewer profileListTableViewer;

	public ProfileListCellModifier(ProfileListTableViewer profileListView) {
		this.profileListTableViewer = profileListView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object,
	 * java.lang.String)
	 */
	public boolean canModify(Object element, String property) {
		if (!(element instanceof Profile)) {
			return false;
		}
		Profile s = (Profile) element;
		if (ProfileListTableViewer.PROFILE.equals(property)) {
			return true;
		}
		return profileListTableViewer.updateCellEditor(property, s);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object,
	 * java.lang.String)
	 */
	public Object getValue(Object element, String property) {

		Profile profile = (Profile) element;
		Object result = null;
		if (ProfileListTableViewer.PROFILE.equals(property)) {
			// result = profileListView.getUriEditor().getValue();
			result = profile.getName();
		} else {
			result = profile;
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object,
	 * java.lang.String, java.lang.Object)
	 */
	public void modify(Object element, String property, Object value) {
		if (element instanceof TableItem) {
			TableItem item = (TableItem) element;

			if (ProfileListTableViewer.PROFILE.equals(property)) {
				Profile profile = (Profile) item.getData();
				Profile newProfile = profile;
				String name;
				ProfileList profileList = ((ProfileList) profileListTableViewer
						.getInput());
				try {
					String s = (String) value;

					if (s != null && !"".equals(s)) {
						name = s;
					} else {
						IdentityActivator.log(IStatus.ERROR,
								"Could not change profile name: Empty input.");
						return;
					}
					if (s.equals(profile.getName())) {
						return;
					}
					if (profileList.getProfile(name) != null) {
						IdentityActivator
								.log(IStatus.ERROR,
										"Could not change profile name: A profile with that name exists already.");
						return;
					}
					profileList.changeName(newProfile, name);

				} catch (URISyntaxException e) {
					IdentityActivator.log(
							"Could not change profile name: Invalid input.", e);
				} catch (Exception e) {
					IdentityActivator.log(
							"Could not change profile URI or name.", e);
				}

			} else {
				Profile profile = (Profile) value;
				((ProfileList) profileListTableViewer.getInput())
						.updateProfile(profile);
			}
		}

	}

}
