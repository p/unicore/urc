/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.identity.truststore;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.part.ViewPart;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.TableColumnSorter;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.IdentityActivator.TruststoreChoice;
import de.fzj.unicore.rcp.identity.IdentityConstants;
import de.fzj.unicore.rcp.identity.TrustController;
import de.fzj.unicore.rcp.identity.X509Utils;
import de.fzj.unicore.rcp.identity.actions.ImportCAsFromKeystoreAction;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.logmonitor.utils.FileDialogUtils;

/**
 * The truststore view to take actions on certificate authorities certificates
 * stored in the URC's truststore.
 * 
 * @author Bastian Demuth, Valentina Huber
 */
public class TruststoreView extends ViewPart implements TruststoreChangedListener {

	class NameSorter extends ViewerSorter {

	}

	class ViewContentProvider implements IStructuredContentProvider {
		public void dispose() {
		}

		public Object[] getElements(Object parent) {
			if(TruststoreChoice.KEYSTORE ==  IdentityActivator.getDefault().getTruststoreType()){
				List<Certificate> certificateList = tm.getAllCertificates();
				if (certificateList == null) {
					return new Certificate[0];
				} else {
					return certificateList.toArray();
				}
			}
			else{
				return new Certificate[0];
			}
		}

		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
	}

	public static final String ID = "de.fzj.unicore.rcp.identity.truststore";
	private TableViewer viewer;
	private Action changeAliasAction;
	private ImportCAsFromKeystoreAction importKeystoreAction;
	private Action removeCAAction;
	private Action showDetailsAction;
	private Action importPublicKeyAction;
	private Action exportPublicKeyAction;
	private Action changePasswordAction;

	private Action doubleClickAction;

	private CheckboxCellEditor defaultCertEditor;
	// column headers
	public static final String ALIAS = "alias", CN = "CA's common name",
			VALID_TO = "valid to";

	public static final String[] COLUMN_PROPERTIES = new String[] { ALIAS, CN,
			VALID_TO };

	final TrustController tm;

	/**
	 * The constructor.
	 * 
	 * @throws IOException
	 */
	public TruststoreView() {
		this.tm = IdentityActivator.getDefault().getTrust();
		this.tm.addTruststoreChangedListener(this);
	}

	private File askExportCertFile() {
		FileDialog dialog = new FileDialog(getViewer().getControl().getShell(),
				SWT.SAVE);
		String filterPath = FileDialogUtils.getFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS);
		dialog.setFilterPath(filterPath);
		dialog.setFilterExtensions(Constants.PUBLIC_KEY_FORMATS);
		dialog.setText("Please select a file you want to store the selected public key(s) to.");
		String filename = dialog.open();
		FileDialogUtils.saveFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS, dialog.getFilterPath());
		File f = null;
		if (filename != null) {
			f = new File(filename);
		}
		if (f != null && f.exists()) {
			boolean overwrite = MessageDialog.openConfirm(getViewer()
					.getControl().getShell(), "Confirm overwriting file",
					"Selected file already exists, overwrite?");
			if (!overwrite) {
				return null;
			}
		}
		return f;
	}
	
	private File[] askImportCertFiles() {
		FileDialog dialog = new FileDialog(getViewer().getControl().getShell(), SWT.MULTI | SWT.OPEN);
		String filterPath = FileDialogUtils.getFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS);
		dialog.setFilterPath(filterPath);
		dialog.setFilterExtensions(Constants.PUBLIC_KEY_FORMATS);
		dialog.setText("Please select certificate files of authorities to trust.");
		
		dialog.open();
		FileDialogUtils.saveFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS, dialog.getFilterPath());
		String[] files = dialog.getFileNames();
		if(files == null) return new File[0];
		File parent = new File(dialog.getFilterPath());
		List<File> result = new ArrayList<File>(); 
		for (String filename : files) {
			if(filename != null) result.add(new File(parent,filename));
		}
		return result.toArray(new File[result.size()]);
	}
	
	

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		Table table = createTable(parent);
		createTableViewer(table);
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();

	}

	private Table createTable(Composite parent) {
		int style = SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION;
		Table table = new Table(parent, style);

		TableColumn col0 = new TableColumn(table, SWT.LEFT, 0);
		col0.setText(ALIAS);
		col0.setToolTipText("Alias of the certificate in the truststore.");
		col0.setWidth(220);

		TableColumn col1 = new TableColumn(table, SWT.LEFT, 1);
		col1.setText(CN);
		col1.setToolTipText("Common name of the certificate authority.");
		col1.setWidth(220);

		TableColumn col2 = new TableColumn(table, SWT.LEFT, 2);
		col2.setText(VALID_TO);
		col2.setToolTipText("The date until which this certificate authority can issue certificates.");
		col2.setWidth(130);

		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		return table;

	}

	private TableViewer createTableViewer(Table table) {
		viewer = new TableViewer(table);
		viewer.setUseHashlookup(true);
		viewer.setContentProvider(new CertContentProvider(viewer));
		viewer.setLabelProvider(new CertLabelProvider());
		viewer.setSorter(new NameSorter());
		viewer.setColumnProperties(COLUMN_PROPERTIES);
		viewer.setInput(tm);
		for (int i = 0; i < table.getColumnCount(); i++) {
			TableColumn col = table.getColumn(i);
			final int index = i;
			new TableColumnSorter(viewer, col) {
				@Override
				protected int doCompare(Viewer v, Object e1, Object e2) {
					ITableLabelProvider lp = ((ITableLabelProvider) viewer
							.getLabelProvider());
					String t1 = lp.getColumnText(e1, index);
					String t2 = lp.getColumnText(e2, index);
					return t1.compareTo(t2);
				}
			};
		}
		return viewer;
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(importPublicKeyAction);
		manager.add(importKeystoreAction);
		manager.add(removeCAAction);
		manager.add(showDetailsAction);
		manager.add(changeAliasAction);
		manager.add(exportPublicKeyAction);
		manager.add(changePasswordAction);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalPullDown(IMenuManager manager) {
		// manager.add(addCAAction);
		// manager.add(new Separator());
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(importPublicKeyAction);
		manager.add(importKeystoreAction);
		manager.add(removeCAAction);
		manager.add(showDetailsAction);
		manager.add(changeAliasAction);
		manager.add(exportPublicKeyAction);
		manager.add(changePasswordAction);
	}

	public CheckboxCellEditor getDefaultCertEditor() {
		return defaultCertEditor;
	}

	public TableViewer getViewer() {
		return viewer;
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	private void makeActions() {

		changeAliasAction = new Action() {
			@Override
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getViewer()
						.getSelection();
				for (Iterator<?> iter = selection.iterator(); iter
						.hasNext();) {
					Certificate cert = (Certificate) iter.next();
					try {
						tm.changeAlias(cert);
						viewer.refresh();
					} catch (Exception e) {
						IdentityActivator.log(IStatus.ERROR,
								"Could not change alias for the selected key!",
								e);
					}
				}
				;
			}
		};
		changeAliasAction.setText("Change alias...");
		changeAliasAction
				.setToolTipText("Change alias for selected key(s) in your trust store");
		changeAliasAction.setImageDescriptor(IdentityActivator.getImageDescriptor("alias_edit.png"));
		
		
		importPublicKeyAction = new Action() {
			public void run() {			
				try {
					File[] files = askImportCertFiles();
					for (File file : files) {
						Certificate cert = new Certificate(file);
						tm.addTrustedCertificate(cert);
					}
				} catch (Exception e) {
					IdentityActivator.log(IStatus.ERROR, "Could not add trusted CA entry!",e);
				}
				viewer.refresh();
			}
		};
		importPublicKeyAction.setText("Import trusted CAs from certificate files");
		importPublicKeyAction.setToolTipText("Import trusted CAs from certificate files");
		importPublicKeyAction.setImageDescriptor(IdentityActivator.getImageDescriptor("add_trusted.png"));

		importKeystoreAction = new ImportCAsFromKeystoreAction(tm, this);
		importKeystoreAction.setText("Import keystore");
		importKeystoreAction.setDefaultFileType("*.pem");

		removeCAAction = new Action() {
			@Override
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getViewer()
						.getSelection();
				if (MessageDialog
						.openConfirm(
								getViewer().getControl().getShell(),
								"Confirm",
								"Do you want to remove the selected trusted certificate authorities from your trust store?")) {
					for (Iterator<?> iter = selection.iterator(); iter
							.hasNext();) {
						Certificate cert = (Certificate) iter.next();
						try {
							tm.removeTrustedCertificate(cert);
							viewer.refresh();
						} catch (Exception e) {
							IdentityActivator.log(IStatus.ERROR,
									"Could not remove trusted CA entry!", e);
						}
					}
				}
			}
		};
		removeCAAction.setText("Remove CA");
		removeCAAction.setToolTipText("Permanently remove selected trusted certificate authority from  trust store");
		removeCAAction.setImageDescriptor(IdentityActivator.getImageDescriptor("delete.png"));

		exportPublicKeyAction = new Action() {
			@Override
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getViewer()
						.getSelection();
				File file = askExportCertFile();
				if (file == null) {
					return;
				}
				int i = 0;
				for (Iterator<?> iter = selection.iterator(); iter
						.hasNext();) {
					Certificate cert = (Certificate) iter.next();
					try {
						if (file != null) {
							X509Utils.exportPublicKey(cert, file, i > 0);
						}
					} catch (Exception e) {
						IdentityActivator.log(IStatus.ERROR,
								"Could not export public key!", e);
					}
					i++;
				}
				;
			}
		};
		exportPublicKeyAction.setText("Export public key");
		exportPublicKeyAction
				.setToolTipText("Export selected public key(s) to local file(s)");
		exportPublicKeyAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("key_go.png"));

		changePasswordAction = new Action() {
			@Override
			public void run() {

				try {
					((KeystoreTrustController)tm).changeKeystorePassword();
				} catch (Exception e) {
					IdentityActivator.log(IStatus.ERROR,
							"Could not change keystore password!", e);
				}
			}
		};
		changePasswordAction.setText("Change keystore password");
		changePasswordAction.setToolTipText("Change keystore password");
		changePasswordAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("lock_edit.png"));
		changePasswordAction.setEnabled(tm instanceof KeystoreTrustController);
		
		showDetailsAction = new Action() {
			@Override
			public void run() {
				showDetails();
			}
		};
		showDetailsAction.setText("Show details...");
		showDetailsAction.setToolTipText("Show certificate(s) details");
		showDetailsAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("magnifier.png"));

		doubleClickAction = new Action() {
			@Override
			public void run() {
				showDetails();
			}
		};
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	public void setViewer(TableViewer viewer) {
		this.viewer = viewer;
	}

	private void showDetails() {
		IStructuredSelection selection = (IStructuredSelection) getViewer().
				getSelection();
		for (Iterator<?> iter = selection.iterator(); iter.hasNext();) {
			Certificate cert = (Certificate) iter.next();
			try {
				tm.displayCertificateDetails(cert);
			} catch (Exception e) {
				IdentityActivator.log(IStatus.ERROR,"Could not display certificate details!", e);
			}
		}
		;
	}

	@Override
	public void truststoreChanged() {
		viewer.refresh();
	}


}