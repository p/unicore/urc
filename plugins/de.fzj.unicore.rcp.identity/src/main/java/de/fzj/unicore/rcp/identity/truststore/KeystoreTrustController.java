package de.fzj.unicore.rcp.identity.truststore;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.security.auth.x500.X500Principal;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;

import de.fzj.unicore.rcp.common.guicomponents.ChangePasswordDialog;
import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.identity.CertificateUtility;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.KeyStoreManager;
import de.fzj.unicore.rcp.identity.TrustController;
import de.fzj.unicore.rcp.identity.keystore.AliasValidator;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.ui.dialogs.CertificateDetailsDialog;
import de.fzj.unicore.rcp.identity.utils.InputUtils;
import eu.unicore.security.canl.TruststoreProperties;

/**
 * used to perform actions on a truststore of type "keystore"
 *  
 * @author schuller
 */
public class KeystoreTrustController implements TrustController {

	private final KeyStoreManager keystoreManager;

	private final String password;

	private final IdentityActivator parent;

	private Set<TruststoreChangedListener> listeners;

	public KeystoreTrustController(IdentityActivator parent, File keystore, String password) throws Exception {
		this.keystoreManager = createKeystoreManager(keystore, password);
		this.parent = parent;
		this.password = password;
		this.listeners = new HashSet<TruststoreChangedListener>();
	}

	private KeyStoreManager createKeystoreManager(File keystore, String password) throws Exception {
		KeyStoreManager keystoreManager = KeyStoreManager.getInstance(keystore, password);
		IPath trustedPath = new Path("certs/trustedcerts");
		trustedPath = PathUtils.fixInstallationRelativePath(trustedPath);
		if (trustedPath.toFile().exists()) {
			// load certs from trusted certs dir
			keystoreManager.importTrustedCerts(trustedPath.toFile());
		}
		if (!keystoreManager.checkKeyStore(password.toCharArray())) {
			throw new Exception("The keystore contains errors");
		}
		return keystoreManager;
	}

	/**
	 * Removes a trusted certificate from the list of certificates and rewrites
	 * keystore.
	 * 
	 * @param cert
	 * @throws Exception
	 */
	public void removeTrustedCertificate(Certificate cert) throws Exception {
		IdentityActivator.log("Removing trusted certificate: " + cert.getName());
		removeTrustedCertificate(cert.getName());
	}

	public void removeTrustedCertificate(String name) throws Exception {
		IdentityActivator.log("Removing trusted certificate: " + name);
		keystoreManager.removeTrustedCertificateEntry(name);
		keystoreManager.reWriteKeyStore();
		notifyListeners();
	}

	private void notifyListeners() {
		for (TruststoreChangedListener l : listeners) {
			l.truststoreChanged();
		}
	}

	/**
	 * Adds a trusted certificate to the list of certificates (if it does not already exist)
	 * and rewrites the keystore.
	 * 
	 * @param cert
	 * @throws Exception
	 */
	public void addTrustedCertificate(Certificate cert) throws Exception {
		Set<String> existingAliases = new HashSet<String>();
		existingAliases.addAll(keystoreManager.getIdentityAliases());
		existingAliases.addAll(keystoreManager.getTrustedAliases());
		String oldName = cert.getName();
		if(existingAliases.contains(oldName)){
			cert.setName(askAlias(oldName, existingAliases));
		}
		if (cert.getName() == null) {
			return;
		}
		keystoreManager.addTrustedCertificate(cert.getName(),
				cert.getCert());
		keystoreManager.reWriteKeyStore();
	}

	/**
	 * Changes the alias name of the certificate
	 */
	public void changeAlias(Certificate cert) throws Exception {
		String alias = cert.getName();
		Set<String> existingAliases = new HashSet<String>();
		existingAliases.addAll(keystoreManager.getTrustedAliases());
		IInputValidator validator = new AliasValidator(existingAliases);
		InputDialog d = new InputDialog(parent.getWorkbench()
				.getActiveWorkbenchWindow().getShell(), "Change Alias",
				"Change alias:", alias, validator);
		d.open();
		if (d.getReturnCode() == Window.OK) {
			String newAlias = d.getValue();
			keystoreManager.changeAlias(alias, newAlias);
			keystoreManager.reWriteKeyStore();
			cert.setName(newAlias);
		}

	}

	public void displayCertificateDetails(Certificate cert) throws Exception {
		String alias = keystoreManager.getAliasFromCertificate(cert.getCert());
		CertificateDetailsDialog d = new CertificateDetailsDialog(parent
				.getWorkbench().getActiveWorkbenchWindow().getShell(),
				"Certificate Details", keystoreManager, cert, alias);
		d.open();
	}
	
	/**
	 * Adds certificates from the specified keystore to the lists of the
	 * certificates and rewrites an existing keystore.
	 * 
	 * @param f
	 */
	public void importKeyStore(File f) throws Exception {
		KeyStoreManager newKm = null;
		String passwd = null;
		passwd = new InputUtils().askPassword(f);
		if (passwd == null) {
			return;
		}
		newKm = KeyStoreManager.getInstance(f, passwd);
		if (newKm == null) {
			return;
		}

		importKeyStore(newKm, passwd, true);
	}


	public void importKeyStore(KeyStoreManager newKm, String passwd,
			boolean askForUniqueTruststoreAliases) throws Exception {

		if (passwd == null) {
			return;
		}
		if (newKm == null) {
			return;
		}
		if (!newKm.checkKeyStore(passwd.toCharArray())) {
			throw new Exception("The keystore to be imported contains errors");
		}

		Set<String> existingAliases = new HashSet<String>();
		existingAliases.addAll(keystoreManager.getIdentityAliases());
		existingAliases.addAll(keystoreManager.getTrustedAliases());
		existingAliases.addAll(newKm.getIdentityAliases());
		existingAliases.addAll(newKm.getTrustedAliases());
		boolean empty = true;

		List<String>newAliases = newKm.getTrustedAliases();
		for(String alias: newAliases) {
			empty = false;
			boolean add=true;
			X509Certificate x509 = newKm.getCertificateByAlias(alias)[0];
			Certificate cert = new Certificate(x509);
			if (isExistingName(alias)) {
				String newAlias = askForUniqueTruststoreAliases ? askAlias(
						alias, existingAliases) : getUniqueAlias(alias,
								existingAliases);
						cert.setName(newAlias);
			}
			if (cert.getName() != null) {
				existingAliases.add(cert.getName()); 
				// make sure the same alias
				// is not entered twice by the user during import
				newKm.changeAlias(alias, cert.getName());
				alias=cert.getName();
			} else {
				add=false;
			}

			// check that we do not already have this trusted cert
			X500Principal newX500=cert.getCert().getSubjectX500Principal();
			for(Certificate old: getAllCertificates()){
				if(newX500.equals(old.getCert().getSubjectX500Principal())){
					add=false;
					break;
				}
			}

			if(add){
				addTrustedCertificate(cert);
			}
		}

		if (empty) {
			IdentityActivator.log(IStatus.ERROR,
					"No certificates imported, the selected file was empty!");
		}
	}

	protected String getUniqueAlias(String base, Set<String> existingAliases) {
		String result = base;
		int i = 1;
		while (existingAliases.contains(result)) {
			result = base + i;
			i++;
		}
		return result;
	}


	public String askAlias(String oldAlias, Set<String> existingAliases)
			throws Exception {
		InputDialog d = new InputDialog(parent.getWorkbench().getActiveWorkbenchWindow().getShell(),
				"Alias already exists", "A key with the alias `" + oldAlias + 
				"' is already contained in the keystore. Please enter a different " +
				"alias or press cancel to skip this key.",
				oldAlias, new AliasValidator(existingAliases));

		d.open();
		if (d.getReturnCode() == Window.OK) {
			String newAlias = d.getValue();
			return newAlias;
		}
		return null;
	}

	/**
	 * Displays the dialog to change the password of the keystore and rewrites
	 * the keystore with the new password.
	 * 
	 * @throws Exception
	 */
	public void changeKeystorePassword() throws Exception {
		String title = "Change keystore password";
		ChangePasswordDialog d = new ChangePasswordDialog(parent.getWorkbench()
				.getActiveWorkbenchWindow().getShell(), title, null, null);
		d.open();
		if (d.getReturnCode() == Window.OK) {
			String oldPasswd = d.getOldPassword();
			String newPasswd = d.getNewPassword();
			keystoreManager.changePassword(oldPasswd.toCharArray(),
					newPasswd.toCharArray());
		}
	}

	@Override
	public List<Certificate> getAllCertificates(){
		return CertificateUtility.getTrustedCertificates(keystoreManager);
	}


	@Override
	public boolean isExistingName(String alias) {
		for(Certificate c: getAllCertificates()){
			if(c.getName()!=null && c.getName().equals(alias))return true;
		}
		return false;
	}

	@Override
	public void reWriteKeyStore() throws Exception {
		keystoreManager.reWriteKeyStore();
	}

	public KeyStoreManager getKeystoreManager(){
		return keystoreManager;
	}

	public Properties getTruststoreProperties(){
		Properties p = new Properties();
		p.setProperty("truststore."+TruststoreProperties.PROP_TYPE,"keystore");
		p.setProperty("truststore."+TruststoreProperties.PROP_KS_PATH,keystoreManager.getKeyStoreName());
		p.setProperty("truststore."+TruststoreProperties.PROP_KS_PASSWORD,password);
		p.setProperty("truststore."+TruststoreProperties.PROP_KS_TYPE,"jks");
		p.setProperty("truststore."+TruststoreProperties.PROP_CRL_MODE,"IGNORE");

		return p;
	}

	@Override
	public void addTruststoreChangedListener(
			TruststoreChangedListener truststoreListener) {
		listeners.add(truststoreListener);
	}
		
}
