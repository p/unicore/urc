/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.identity.profiles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.graphics.Image;

import de.fzj.unicore.rcp.common.utils.Handler;
import de.fzj.unicore.rcp.common.utils.HandlerOrderingUtil;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.extensionpoints.ICredentialTypeExtensionPoint;

/**
 * @author demuth
 * 
 */
public class CredentialTypeRegistry {

	/**
	 * Contains labels for added credential types. Each label in this list leads
	 * to the creation of an additional column in the profile table. Extensions
	 * that don't provide a type label will not be represented by a column.
	 */
	Map<String, String> idsToTypes = new HashMap<String, String>();
	List<String> ids = new ArrayList<String>();
	Map<String, String> typesToIDs = new HashMap<String, String>();
	Map<String, String> idsToTooltips = new HashMap<String, String>();
	Map<String, ICredentialTypeExtensionPoint> idsToExtensions = new HashMap<String, ICredentialTypeExtensionPoint>();
	Map<String, Set<String>> idsToBefore = new HashMap<String, Set<String>>();
	Map<String, Set<String>> idsToAfter = new HashMap<String, Set<String>>();

	public CredentialTypeRegistry() {
		iterateOverExtensions();

	}

	public void dispose() {
		for (ICredentialTypeExtensionPoint ext : idsToExtensions.values()) {
			try {
				ext.dispose();
			} catch (Exception e) {
				IdentityActivator.log(IStatus.WARNING,
						"Could not dispose of credential type "
								+ ext.getClass().getSimpleName(), e);
			}
		}
		ids = null;
		idsToTypes = null;
		idsToExtensions = null;
		typesToIDs = null;
		idsToTooltips = null;
	}

	public List<ICredentialTypeExtensionPoint> getAllExtensions() {
		List<ICredentialTypeExtensionPoint> result = new ArrayList<ICredentialTypeExtensionPoint>();
		for (String id : ids) {
			result.add(idsToExtensions.get(id));
		}
		return result;
	}

	public List<String> getAllTypes() {
		List<String> result = new ArrayList<String>();
		for (String id : ids) {
			if (idsToTypes.get(id) != null) {
				result.add(idsToTypes.get(id));
			}
		}
		return result;
	}

	public ICredentialTypeExtensionPoint getDefiningExtension(String type) {

		String id = typesToIDs.get(type);
		ICredentialTypeExtensionPoint result = idsToExtensions.get(id);
		return result;
	}

	private String getID(String type) {
		return typesToIDs.get(type);
	}

	public Image getImage(String type, Profile profile) {
		String id = getID(type);
		if (id == null) {
			return null;
		}
		return idsToExtensions.get(id).getImage(profile);
	}

	public String getTooltip(String type) {
		return idsToTooltips.get(type);
	}

	public String getType(int index) {
		return getAllTypes().get(index);
	}

	public String getType(String id) {
		for (String s : typesToIDs.keySet()) {
			if (id.equals(typesToIDs.get(s))) {
				return s;
			}
		}
		return null;
	}

	public int indexOf(String type) {
		return getAllTypes().indexOf(type);
	}

	public void initProfile(Profile profile) {
		for (ICredentialTypeExtensionPoint ext : getAllExtensions()) {
			try {
				ext.initProfile(profile);
			} catch (Exception e) {
				IdentityActivator.log(IStatus.WARNING,
						"Could not initialize credential type "
								+ ext.getClass().getSimpleName(), e);
			}

		}

	}

	protected void iterateOverExtensions() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(ICredentialTypeExtensionPoint.ID);
		IExtension[] extensions = extensionPoint.getExtensions();
		for (IExtension ext : extensions) {
			try {
				IConfigurationElement member = ext.getConfigurationElements()[0];
				String type = member
						.getAttribute(ICredentialTypeExtensionPoint.LABEL);
				String id = ext.getUniqueIdentifier();
				String tooltip = member
						.getAttribute(ICredentialTypeExtensionPoint.TOOLTIP);

				ICredentialTypeExtensionPoint cred = (ICredentialTypeExtensionPoint) member
						.createExecutableExtension("implementingClass");
				idsToExtensions.put(id, cred);
				Set<String> before = new HashSet<String>();
				IConfigurationElement[] beforeElements = member
						.getChildren("processBefore");
				for (IConfigurationElement elt : beforeElements) {
					try {
						before.add(elt.getAttribute("id"));
					} catch (Exception e) {
						IdentityActivator.log(IStatus.WARNING,
								"Unable to determine all credential types that need to be processed before "
										+ id, e);
					}
				}
				Set<String> after = new HashSet<String>();
				IConfigurationElement[] afterElements = member
						.getChildren("processAfter");
				for (IConfigurationElement elt : afterElements) {
					try {
						after.add(elt.getAttribute("id"));
					} catch (Exception e) {
						IdentityActivator.log(IStatus.WARNING,
								"Unable to determine all credential types that need to be processed after "
										+ id, e);
					}
				}

				registerCredentialType(type, id, tooltip, cred, before, after,
						false);
			} catch (CoreException ex) {
				IdentityActivator.log(IStatus.WARNING,
						"Unable to instantiate credential type ", ex);
			}
		}
		sortExtensions();
	}

	/**
	 * 
	 * @param type
	 * @param id
	 * @param tooltip
	 * @param extension
	 * 
	 * @deprecated use
	 *             {@link #registerCredentialType(String, String, String, ICredentialTypeExtensionPoint, Set, Set, boolean)}
	 *             instead
	 */
	public void registerCredentialType(String type, String id, String tooltip,
			ICredentialTypeExtensionPoint extension) {
		registerCredentialType(type, id, tooltip, extension, null, null);
	}

	/**
	 * 
	 * @param type
	 * @param id
	 * @param tooltip
	 * @param extension
	 * @param before
	 * @param after
	 * 
	 * @deprecated use
	 *             {@link #registerCredentialType(String, String, String, ICredentialTypeExtensionPoint, Set, Set, boolean)}
	 *             instead
	 */
	public void registerCredentialType(String type, String id, String tooltip,
			ICredentialTypeExtensionPoint extension, Set<String> before,
			Set<String> after) {
		registerCredentialType(type, id, tooltip, extension, before, after,
				true);
	}

	protected void registerCredentialType(String type, String id,
			String tooltip, ICredentialTypeExtensionPoint extension,
			Set<String> before, Set<String> after, boolean sort) {
		if (id == null || ids.contains(id)) {
			return;
		}
		ids.add(id);

		idsToTypes.put(id, type);
		if (type != null) {
			typesToIDs.put(type, id);
		}
		idsToExtensions.put(id, extension);
		idsToTooltips.put(id, tooltip);
		if (before != null) {
			idsToBefore.put(id, before);
		}
		if (after != null) {
			idsToAfter.put(id, after);
		}
		if (sort) {
			sortExtensions();
		}
	}

	public void setProfileList(ProfileList profileList) {
		for (String type : getAllTypes()) {
			ICredentialTypeExtensionPoint ext = getDefiningExtension(type);
			ext.setProfileList(profileList);
		}
	}

	protected void sortExtensions() {
		HandlerOrderingUtil util = new HandlerOrderingUtil();
		for (String id : ids) {
			Handler h = new Handler(id, null);
			Set<String> before = idsToBefore.get(id);
			h.setBefore(before);
			Set<String> after = idsToAfter.get(id);
			h.setAfter(after);
			util.insertHandler(h);
		}
		ids.clear();

		for (Handler h : util.getHandlers()) {
			ids.add(h.getId());
		}
	}

	public void unregisterCredentialType(String id) {
		if (id == null || !ids.contains(id)) {
			return;
		}
		ids.remove(id);
		idsToTooltips.remove(id);
		idsToExtensions.remove(id);
		idsToBefore.remove(id);
		idsToAfter.remove(id);
		String type = getType(id);
		if (type != null) {
			typesToIDs.remove(type);
		}
		idsToTypes.remove(id);

	}
}
