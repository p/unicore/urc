package de.fzj.unicore.rcp.identity.keystore;

import java.util.Set;

import org.eclipse.jface.dialogs.IInputValidator;

public class AliasValidator implements IInputValidator {

	private Set<String> existingAliases;

	public AliasValidator(Set<String> existingAliases) {
		this.existingAliases = existingAliases;
	}

	public String isValid(String newText) {
		if (newText == null || newText.trim().length() == 0) {
			return "The new alias must not be empty";
		} else if (existingAliases.contains(newText)) {
			return "An entry with this alias already exists";
		} else if (!newText.equals(newText.toLowerCase())) {
			return "Aliases must be in lower case";
		} else {
			return null;
		}
	}
}