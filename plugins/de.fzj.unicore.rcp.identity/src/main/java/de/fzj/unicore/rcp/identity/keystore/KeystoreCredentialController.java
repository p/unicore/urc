package de.fzj.unicore.rcp.identity.keystore;

import java.io.File;
import java.io.FileOutputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.guicomponents.ChangePasswordDialog;
import de.fzj.unicore.rcp.identity.CredentialController;
import de.fzj.unicore.rcp.identity.DistinguishedName;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.KeyStoreManager;
import de.fzj.unicore.rcp.identity.TrustController;
import de.fzj.unicore.rcp.identity.ui.dialogs.CertificateDetailsDialog;
import de.fzj.unicore.rcp.identity.ui.dialogs.CertificateRequestDialog;
import de.fzj.unicore.rcp.identity.utils.AuthNUtils;
import de.fzj.unicore.rcp.identity.utils.InputUtils;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import eu.emi.security.authn.x509.X509CertChainValidator;
import eu.unicore.security.canl.CredentialProperties;
import eu.unicore.security.canl.LoggingStoreUpdateListener;
import eu.unicore.security.canl.TruststoreProperties;
import eu.unicore.util.httpclient.MyProxyLogon;

/**
 * used to perform actions on a credential of type "keystore"
 *  
 * @author schuller
 */
public class KeystoreCredentialController implements CredentialController {

	private final KeyStoreManager keystoreManager;

	private final IdentityActivator parent;

	private final CertificateList certificateList;

	private final String password;

	public Properties getCredentialProperties(){
		Properties p = new Properties();
		p.setProperty("credential."+CredentialProperties.PROP_LOCATION,keystoreManager.getKeyStoreName());
		p.setProperty("credential."+CredentialProperties.PROP_PASSWORD,password);
		p.setProperty("credential."+CredentialProperties.PROP_FORMAT,keystoreManager.getKeyStoreType());
		String alias=keystoreManager.getDefaultAlias();
		if(alias!=null && !alias.isEmpty())p.setProperty("credential."+CredentialProperties.PROP_KS_ALIAS,alias);
		return p;
	}
	
	public boolean isReady(){
		return !keystoreManager.getIdentityAliases().isEmpty();
	}


	public KeystoreCredentialController(IdentityActivator parent, File keystore, String passwd) throws Exception {
		this.keystoreManager = createKeystoreManager(keystore, passwd);
		certificateList = new CertificateList(keystoreManager);
		this.parent = parent;
		this.password = passwd;
		checkIdentityValidity();
	}

	private KeyStoreManager createKeystoreManager(File keystore, String passwd) throws Exception {
		KeyStoreManager keystoreManager = KeyStoreManager.getInstance(keystore, passwd);
		if (!keystoreManager.checkKeyStore(passwd.toCharArray())) {
			throw new Exception("The keystore contains errors");
		}
		return keystoreManager;
	}

	public CertificateList getCertificateList(){
		return certificateList;
	}

	protected void checkIdentityValidity() {
		Job j = new BackgroundJob("checking user certificates") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				PlatformUI.getWorkbench().getDisplay()
				.asyncExec(new Runnable() {

					public void run() {
						if (keystoreManager != null) {
							keystoreManager.checkIdentityValidity();
						}
					}
				});
				return Status.OK_STATUS;
			}
		};
		j.schedule(5000);
	}


	/**
	 * Changes the alias name of the trusted certificate
	 */
	public void changeAlias(Certificate cert) throws Exception {
		String alias = cert.getName();
		Set<String> existingAliases = new HashSet<String>();
		existingAliases.addAll(keystoreManager.getIdentityAliases());
		boolean isKeyEntry = existingAliases.contains(alias);
		existingAliases.addAll(keystoreManager.getTrustedAliases());
		IInputValidator validator = new AliasValidator(existingAliases);
		InputDialog d = new InputDialog(parent.getWorkbench()
				.getActiveWorkbenchWindow().getShell(), "Change Alias",
				"Change alias:", alias, validator);
		d.open();
		if (d.getReturnCode() == Window.OK) {
			String newAlias = d.getValue();
			keystoreManager.changeAlias(alias, newAlias);
			keystoreManager.reWriteKeyStore();
			cert.setName(newAlias);
			if (isKeyEntry) {
				certificateList.certChanged(cert);
				if (cert.isDefaultCert()) {
					certificateList.setDefaultCert(cert);
				}
			}
		}
	}

	/**
	 * Displays the dialog to change the password of the keystore and rewrites
	 * the keystore with the new password.
	 * 
	 * @throws Exception
	 */
	public void changeKeystorePassword() throws Exception {
		String title = "Change keystore password";
		ChangePasswordDialog d = new ChangePasswordDialog(parent.getWorkbench()
				.getActiveWorkbenchWindow().getShell(), title, null, null);
		d.open();
		if (d.getReturnCode() == Window.OK) {
			String oldPasswd = d.getOldPassword();
			String newPasswd = d.getNewPassword();
			keystoreManager.changePassword(oldPasswd.toCharArray(),
					newPasswd.toCharArray());
		}
	}

	/**
	 * Merges the CA reply with the private key and rewrites keystore.
	 * 
	 * @param cert
	 * @throws Exception
	 */
	public void importCAReply(Certificate cert) throws Exception {
		// check if we have a private key for this cert

		X509Certificate match = keystoreManager.getCertificateByKey(cert
				.getCert().getPublicKey());
		if (match != null) {
			IdentityActivator.log("Importing CA reply for : " + cert.getName());
			String alias = keystoreManager.getAliasFromCertificate(match);
			keystoreManager.replaceKeyCertificate(alias, cert.getCert());
			keystoreManager.reWriteKeyStore();
			Certificate old = certificateList.getCertByPublicKey(match
					.getPublicKey());
			if (old != null) {
				certificateList.removeCert(old);
			}
			cert.setName(alias);
			certificateList.addCert(cert);
		}

		else {
			IdentityActivator.log("No matching key entry found for: " + cert.getName());
		}
	}


	public void displayCertificateDetails(Certificate cert) throws Exception {
		String name = cert.getName();
		if (certificateList.isExistingName(name)) {
			// remove old certificates from the keystore manager
			String alias = keystoreManager.getAliasFromCertificate(cert
					.getCert());
			CertificateDetailsDialog d = new CertificateDetailsDialog(parent
					.getWorkbench().getActiveWorkbenchWindow().getShell(),
					"Certificate Details", keystoreManager, cert, alias);
			d.open();
		}
	}


	/**
	 * Generates a certificate request and writes it to a file.
	 */
	public void generateCSR() throws Exception {

		String title = "Generate Certificate Request";
		CertificateRequestDialog d = new CertificateRequestDialog(parent
				.getWorkbench().getActiveWorkbenchWindow().getShell(), title);
		d.open();
		if (d.getReturnCode() == Window.OK) {
			DistinguishedName dn = d.getDN();
			if (dn == null) {
				return;
			}
			String alias = dn.CN[0].toLowerCase();
			File file = askCSRFile(alias);
			if (file == null) {
				return;
			}

			byte[] encodedCSR = keystoreManager.generateCSR(dn.getDNString(),
					1024, alias);

			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(file);
				String certStart = "-----BEGIN CERTIFICATE REQUEST-----\n";
				String certEnd = "-----END CERTIFICATE REQUEST-----\n";
				fos.write(certStart.getBytes());
				fos.write(Base64.encodeBase64(encodedCSR, true));
				fos.write(certEnd.getBytes());

			} catch (Exception e) {
				throw new Exception(e.getMessage());
			} finally {
				if (fos != null) {
					try {
						fos.close();
					} catch (Exception e) {
					}
				}
			}
			keystoreManager.rehashKeyStoreEntries();
			keystoreManager.reWriteKeyStore();
			X509Certificate[] X509certs = keystoreManager
					.getCertificateByAlias(alias);
			Certificate cert = new Certificate(X509certs[0]);
			cert.setName(alias);
			certificateList.addCert(cert);
		}
	}

	/**
	 * Opens the dialog to select a file to save a certificate request.
	 * 
	 * @param alias
	 * @return
	 */
	private File askCSRFile(String alias) {
		FileDialog dialog = new FileDialog(parent.getWorkbench()
				.getActiveWorkbenchWindow().getShell(), SWT.SAVE);
		dialog.setFilterExtensions(Constants.CSR_FORMATS);
		dialog.setFileName("NewCSR" + Constants.CSR_FORMAT);
		dialog.setText("Please select a file you want to save " + alias
				+ " certificate request to.");
		String filename = dialog.open();
		File f = null;
		if (filename != null) {
			if (!filename.endsWith(Constants.CSR_FORMAT)) {
				filename += Constants.CSR_FORMAT;
			}
			f = new File(filename);
		}
		return f;
	}


	public void importKeysFromKeystore(File f) throws Exception {
		KeyStoreManager newKm = null;
		String passwd = null;
		passwd = new InputUtils().askPasswordSynch(f, Display.getCurrent().getActiveShell());
		if (passwd == null) {
			return;
		}
		newKm = KeyStoreManager.getInstance(f, passwd);
		if (newKm == null) {
			return;
		}
		importKeysFromKeystore(newKm, passwd, true, true);
	}


	public void importKeysFromKeystore(KeyStoreManager newKm, String passwd,
			boolean askForUniqueTruststoreAliases,
			boolean askForUniqueKeystoreAliases) throws Exception {

		if (!newKm.checkKeyStore(passwd.toCharArray())) {
			throw new Exception("The keystore to be imported contains errors");
		}
		CertificateList newCerts = new CertificateList(newKm);

		Iterator<Certificate> iter = newCerts.getCerts().iterator();
		Set<String> existingAliases = new HashSet<String>();
		existingAliases.addAll(keystoreManager.getIdentityAliases());
		existingAliases.addAll(keystoreManager.getTrustedAliases());
		existingAliases.addAll(newKm.getIdentityAliases());
		existingAliases.addAll(newKm.getTrustedAliases());
		boolean empty = true;
		while (iter.hasNext()) {
			empty = false;
			Certificate cert = iter.next();
			String alias = newKm.getAliasFromCertificate(cert.getCert());
			String newAlias = alias;
			Key key = newKm.getKeyEntry(alias, passwd.toCharArray());
			if (certificateList.isExistingName(cert.getName())) {
				newAlias = askForUniqueKeystoreAliases ? askAlias(alias,
						existingAliases) : getUniqueAlias(alias,
								existingAliases);
				cert.setName(newAlias);
			}
			if (cert.getName() != null) {
				cert.setDefaultCert(false);
				// make sure the same alias is not entered twice 
				// by the user during import
				existingAliases.add(cert.getName());
				X509Certificate[] certChain = newKm.getCertificateByAlias(alias);
				keystoreManager.addKeyEntry((PrivateKey)key, certChain, newAlias);
			} else {
				newKm.removeKeyEntry(alias);
				iter.remove();
			}
		}

		if (empty) {
			IdentityActivator.log(IStatus.ERROR,
					"No keys or certificates imported, the selected file did not contain any keys!");
		}else{
			keystoreManager.reWriteKeyStore();
			certificateList.propertyChange(new IdentityActivator.AuthNChangeEvent(this, "keys", null, null));
		}
	}

	protected String getUniqueAlias(String base, Set<String> existingAliases) {
		String result = base;
		int i = 1;
		while (existingAliases.contains(result)) {
			result = base + i;
			i++;
		}
		return result;
	}


	protected String askAlias(String oldAlias, Set<String> existingAliases)
			throws Exception {
		InputDialog d = new InputDialog(parent.getWorkbench().getActiveWorkbenchWindow().getShell(),
				"Alias already exists", "A key with the alias `" + oldAlias + 
				"' is already contained in the keystore. Please enter a different " +
				"alias or press cancel to skip this key.",
				oldAlias, new AliasValidator(existingAliases));

		d.open();
		if (d.getReturnCode() == Window.OK) {
			String newAlias = d.getValue();
			return newAlias;
		}
		return null;
	}

	public void removeKeyEntry(Certificate cert) throws Exception {
		if (certificateList.isExistingName(cert.getName())) {
			IdentityActivator.log("Removing key entry: " + cert.getName());
			certificateList.removeCert(cert);
			// remove old certificates from the keystore manager
			String alias = cert.getName();
			keystoreManager.removeKeyEntry(alias);
			keystoreManager.reWriteKeyStore();
		}
	}

	public void removeKeyEntry(String alias) throws Exception {
		Certificate cert = certificateList.getCertByName(alias);
		removeKeyEntry(cert);
	}


	public void exportPublicKey(Certificate cert, File file) throws Exception {
		exportPublicKey(cert, file, false);
	}

	/**
	 * Stores the public key of the specified certificate to the file.
	 * 
	 * @param cert
	 * @throws Exception
	 */

	public void exportPublicKey(Certificate cert, File file, boolean append)
			throws Exception {
		String filename = file.getCanonicalPath();
		IdentityActivator.log("Exporting " + cert.getName() + " public key to " + filename);
		FileOutputStream fos = null;
		boolean writePEM = filename.toLowerCase().endsWith(".pem");
		// do NOT append in DER format, doesn't make sense
		append = append && writePEM; 
		fos = new FileOutputStream(filename, append);
		try {
			if(writePEM)
			{
				// write out the certificate in PEM encoded format
				fos.write(cert.getCertAsPEMString().getBytes());
				fos.write("\n".getBytes());
			}
			else
			{
				fos.write(cert.getCert().getEncoded());
			}
		}
		finally {
			fos.close();
		}

	}

	public KeyStoreManager getKeystoreManager(){
		return keystoreManager;
	}

	public static final String MYPROXY_ALIAS = "retrieved-from-myproxy";
	MyProxyLogon myProxy = null;
	
	// - set up credential retrieval from MyProxy
	public void configureMyProxy(String url, String user, String password,
			TrustController trust) throws Exception {
		
		myProxy = new MyProxyLogon();
		String host = url;
		if(url.contains(":")){
			host = url.split(":")[0];
			myProxy.setPort(Integer.parseInt(url.split(":")[1]));
		}
		myProxy.setHost(host);
		
		myProxy.setValidator(createValidator(trust));
		char[] passphrase = password.toCharArray();
		myProxy.setPassphrase(passphrase);
		myProxy.setUsername(user);
		
		refreshMyProxy();
	}
	
	public void refreshMyProxy() throws Exception {
		if(myProxy == null || !needMyProxyRefresh())return;
		IdentityActivator.log("Retrieving cert from MyProxy");
		myProxy.connect();
		myProxy.logon();
		myProxy.getCredentials();
		myProxy.disconnect();
		PrivateKey pk=myProxy.getPrivateKey();
		X509Certificate cert=myProxy.getCertificate();
		if(cert==null || pk== null){
			throw new GeneralSecurityException("Certificate was not received.");
		}
		IdentityActivator.log("Got new certificate for "+cert.getSubjectX500Principal().getName()
				+" valid until "+cert.getNotAfter());
		keystoreManager.addKeyEntry(pk, new X509Certificate[]{cert}, MYPROXY_ALIAS);
		keystoreManager.reWriteKeyStore();
		// add to cert list so that view(s) get updated
		Certificate xCert = new Certificate(cert);
		xCert.setName(MYPROXY_ALIAS);
		certificateList.addCert(xCert);
	}
	
	protected boolean needMyProxyRefresh() throws Exception {
		Key key = keystoreManager.getKeyEntry(MYPROXY_ALIAS, password.toCharArray());
		boolean renew = true;
		if(key!=null){
			X509Certificate myProxyCert = keystoreManager.getCertificateByAlias(MYPROXY_ALIAS)[0];
			long millisValid = myProxyCert.getNotAfter().getTime()-System.currentTimeMillis();
			// less than 2 hours? renew!
			renew = millisValid < 2 * 3600 * 1000;
			IdentityActivator.log("MyProxy certificate is still valid: "+!renew);
		}
		return renew;
	}
	
	private X509CertChainValidator createValidator(TrustController trust){
		Properties properties = trust.getTruststoreProperties();
		TruststoreProperties trustProps=new TruststoreProperties(properties, 
				Collections.singleton(new LoggingStoreUpdateListener()), AuthNUtils.NOP_PASSWORD_CALLBACK, 
				TruststoreProperties.DEFAULT_PREFIX);
		return trustProps.getValidator();
	}
}
