package de.fzj.unicore.rcp.identity.ui.dialogs;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.common.utils.IValidityChangeListener;
import de.fzj.unicore.rcp.common.utils.ValidityChangeEvent;
import de.fzj.unicore.rcp.identity.DistinguishedName;
import de.fzj.unicore.rcp.identity.keystore.CountryCodes;

public class CertificateRequestComposite extends Composite {

	public static final CountryCodes COUNTRY_CODES = (CountryCodes) ResourceBundle
			.getBundle("de.fzj.unicore.rcp.identity.keystore.CountryCodes");

	private static final String validChars = "[a-zA-Z0-9():. -]";
	private static final String emailExpression = "^(\\.|[_A-Za-z0-9-])+@(\\.|[_A-Za-z0-9-])+(\\.[A-Za-z]{2,})$";

	/** Error message string. */
	private String errorMessage;

	/** Error message label widget. */
	private Text errorMessageText;

	private Text cnText, emailText, orgText, orgUnitText;
	private Text localityText, stateText;
	Combo countryCombo;
	ModifyListener modifyListener;
	SelectionListener selectionListener;
	private List<IValidityChangeListener> validityChangeListeners = new ArrayList<IValidityChangeListener>();

	private DistinguishedName dn;

	public CertificateRequestComposite(Composite parent) {
		this(parent, null);
	}

	public CertificateRequestComposite(Composite parent, String message) {
		super(parent, SWT.NONE);
		GridLayout gridLayout = new GridLayout(2, false);
		setLayout(gridLayout);
		Label msg = null;
		if (message != null) {
			int style = SWT.WRAP;
			msg = new Label(this, style);
			msg.setBackground(getBackground());
			msg.setText(message);
			GridData data = new GridData(GridData.FILL_HORIZONTAL);
			data.widthHint = 300;
			data.horizontalSpan = 2;
			msg.setLayoutData(data);
		}

		Label cnLabel = new Label(this, SWT.RIGHT);
		cnLabel.setText("Full Name: *");

		modifyListener = new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				validateInput();
			}
		};
		cnText = new Text(this, SWT.SINGLE | SWT.BORDER);
		cnText.addModifyListener(modifyListener);
		GridData textData = new GridData(GridData.FILL_HORIZONTAL);
		textData.widthHint = 300;
		cnText.setLayoutData(textData);

		Label emailLabel = new Label(this, SWT.RIGHT);
		emailLabel.setText("Email:  *");

		emailText = new Text(this, SWT.SINGLE | SWT.BORDER);
		emailText.setLayoutData(textData);
		emailText.addModifyListener(modifyListener);

		Label orgLabel = new Label(this, SWT.RIGHT);
		orgLabel.setText("Organization: *");

		orgText = new Text(this, SWT.SINGLE | SWT.BORDER);
		orgText.setLayoutData(textData);
		orgText.addModifyListener(modifyListener);

		Label orgUnitLabel = new Label(this, SWT.RIGHT);
		orgUnitLabel.setText("Organization Unit:  ");

		orgUnitText = new Text(this, SWT.SINGLE | SWT.BORDER);
		orgUnitText.setLayoutData(textData);
		orgUnitText.addModifyListener(modifyListener);

		Label localityLabel = new Label(this, SWT.RIGHT);
		localityLabel.setText("Locality:  ");

		localityText = new Text(this, SWT.SINGLE | SWT.BORDER);
		localityText.setLayoutData(textData);
		localityText.addModifyListener(modifyListener);

		Label stateLabel = new Label(this, SWT.RIGHT);
		stateLabel.setText("State:  ");

		stateText = new Text(this, SWT.SINGLE | SWT.BORDER);
		stateText.setLayoutData(textData);
		stateText.addModifyListener(modifyListener);

		Label countryLabel = new Label(this, SWT.RIGHT);
		countryLabel.setText("Country: *");

		countryCombo = new Combo(this, SWT.DROP_DOWN | SWT.READ_ONLY
				| SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		countryCombo.setItems(COUNTRY_CODES.getCountryCodes());
		countryCombo.setLayoutData(textData);
		countryCombo.setVisibleItemCount(20);
		selectionListener = new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {
				validateInput();
			}

			public void widgetSelected(SelectionEvent e) {
				validateInput();
			}
		};
		countryCombo.addSelectionListener(selectionListener);

		GridData errData = new GridData(GridData.FILL_HORIZONTAL);
		errData.horizontalSpan = 2;
		errorMessageText = new Text(this, SWT.READ_ONLY);
		errorMessageText.setBackground(errorMessageText.getDisplay()
				.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		errorMessageText.setForeground(errorMessageText.getDisplay()
				.getSystemColor(SWT.COLOR_RED));
		errorMessageText.setLayoutData(errData);

		validateInput();

	}

	public void addValidityChangeListener(IValidityChangeListener l) {
		validityChangeListeners.add(l);
	}

	protected String checkForInvalidChars(String s) {

		String result = "";
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			try {
				if (!Pattern.matches(validChars, "" + c)) {
					result += c + " ";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		if (result.length() == 0) {
			return null;
		} else {
			result = result.substring(0, result.length() - 1);
			return result;
		}
	}

	/**
	 * Gets the distinguished name.
	 * 
	 * @return The dnString value
	 */
	public DistinguishedName getDN() {
		return dn;
	}

	public boolean isInputValid() {
		return errorMessage == null;
	}

	public void removeValidityChangeListener(IValidityChangeListener l) {
		validityChangeListeners.remove(l);
	}

	/**
	 * Sets the distinguish name (DN) according the user input.
	 * 
	 */
	protected void setDN() {

		String cn = cnText.getText().trim();

		String email = emailText.getText().trim();

		String orgUnit = orgUnitText.getText().trim();

		String org = orgText.getText().trim();

		String locality = localityText.getText().trim();

		String state = stateText.getText().trim();

		String country = COUNTRY_CODES
				.getCode(countryCombo.getSelectionIndex());

		dn = new DistinguishedName(cn, email, org, orgUnit, country, state,
				locality);
	}

	/**
	 * Sets or clears the error message.
	 * 
	 * @param errorMessage
	 *            the error message, or <code>null</code> to clear
	 * @since 3.0
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
		if (errorMessageText != null && !errorMessageText.isDisposed()) {
			errorMessageText.setText(errorMessage == null ? "" : errorMessage); //$NON-NLS-1$
			errorMessageText.getParent().update();
		}
	}

	public void validateInput() {
		boolean oldValid = errorMessage == null;
		errorMessage = null;

		if (checkForInvalidChars(cnText.getText()) != null) {
			errorMessage = "The name contains invalid characters: "
					+ checkForInvalidChars(cnText.getText());
		} else if (errorMessage == null
				&& !emailText.getText().matches(emailExpression)) {
			errorMessage = "Invalid E-Mail address";
		} else if (checkForInvalidChars(orgText.getText()) != null) {
			errorMessage = "The organization contains invalid characters: "
					+ checkForInvalidChars(orgText.getText());
		} else if (checkForInvalidChars(orgUnitText.getText()) != null) {
			errorMessage = "The organization unit contains invalid characters: "
					+ checkForInvalidChars(orgUnitText.getText());
		} else if (checkForInvalidChars(localityText.getText()) != null) {
			errorMessage = "The locality contains invalid characters: "
					+ checkForInvalidChars(localityText.getText());
		} else if (checkForInvalidChars(stateText.getText()) != null) {
			errorMessage = "The state contains invalid characters: "
					+ checkForInvalidChars(stateText.getText());
		} else if (cnText.getText().trim().length() == 0
				|| orgText.getText().trim().length() == 0
				|| emailText.getText().trim().length() == 0
				|| countryCombo.getSelectionIndex() == -1) {
			errorMessage = "Fields marked with (*) are mandatory";
		}
		if (errorMessage == null) {
			setDN();
		} else {
			dn = null;
		}
		// Bug 16256: important not to treat "" (blank error) the same as null
		// (no error)
		setErrorMessage(errorMessage);
		boolean newValid = errorMessage == null;
		if (oldValid != newValid) {
			for (IValidityChangeListener l : validityChangeListeners) {
				l.validityChanged(new ValidityChangeEvent(this, newValid));
			}
		}
	}

}
