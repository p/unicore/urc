package de.fzj.unicore.rcp.identity.extensions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.fzj.unicore.rcp.identity.extensionpoints.ICertificateAttributeExtensionPoint;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.ui.CertificateAttribute;

public class BasicConstraintsAttributeExtension implements
ICertificateAttributeExtensionPoint {

	public static final String EXTENSION_ID = "2.5.29.19";

	public List<CertificateAttribute> createAttributes(Certificate cert) {
		Set<String> critical = cert.getCert().getCriticalExtensionOIDs();
		Set<String> nonCritical = cert.getCert().getNonCriticalExtensionOIDs();
		Set<String> extensions = new HashSet<String>();
		if(critical != null) extensions.addAll(critical);
		if(nonCritical != null) extensions.addAll(nonCritical);
		List<CertificateAttribute> result = new ArrayList<CertificateAttribute>();

		if(extensions != null) {
			String value = getExtensionAsString(cert.getCert().getBasicConstraints());
			result.add(new CertificateAttribute("Basic Constraints", value, null));
		}

		return result;
	}

	private String getExtensionAsString(int value) {
		StringBuilder strBuff = new StringBuilder();
		strBuff.append(value>0 ? "Subject is a CA" : "Subject is not a CA");
		if (value >0 ) {
			strBuff.append(", Certification path length constraint: ");
			if(value == Integer.MAX_VALUE){
				strBuff.append("unlimited");
			}
			else{
				strBuff.append(value);
			}
		}
		return strBuff.toString();
	}

	public void disposeImages() {

	}

}
