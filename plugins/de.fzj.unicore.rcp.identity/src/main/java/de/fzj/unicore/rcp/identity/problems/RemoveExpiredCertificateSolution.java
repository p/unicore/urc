package de.fzj.unicore.rcp.identity.problems;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.exceptions.KeyCertificateExpiredException;
import de.fzj.unicore.rcp.identity.exceptions.TrustedCertificateExpiredException;
import de.fzj.unicore.rcp.identity.keystore.KeystoreCredentialController;
import de.fzj.unicore.rcp.logmonitor.problems.GraphicalSolution;
import eu.unicore.problemutil.Problem;

public class RemoveExpiredCertificateSolution extends GraphicalSolution {

	public RemoveExpiredCertificateSolution() {
		super("CERT_EXPIRED_GRAPH_REMOVE", new String[] {
				"TRUSTSTORE_CERT_EXPIRED", "KEYSTORE_CERT_EXPIRED" },
				"Remove the expired certificate from your keystore/truststore");

	}

	@Override
	public boolean isAvailableFor(String message, Problem[] problems,
			String details) {
		if (problems.length == 0) {
			return false;
		}
		Problem p = problems[0];
		if (p.getCause() instanceof TrustedCertificateExpiredException) {
			return true;
		} else if (p.getCause() instanceof KeyCertificateExpiredException) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean solve(String message, Problem[] problems, String details)
			throws Exception {

		Problem p = problems[0];
		if (p.getCause() instanceof TrustedCertificateExpiredException) {
			TrustedCertificateExpiredException e = (TrustedCertificateExpiredException) p
					.getCause();
			String alias = e.getAlias();
			IdentityActivator.getDefault().getTrust().removeTrustedCertificate(alias);
			return false;
		} else if (p.getCause() instanceof KeyCertificateExpiredException) {
			KeyCertificateExpiredException e = (KeyCertificateExpiredException) p
					.getCause();
			String alias = e.getAlias();
			if(IdentityActivator.getDefault().getCredential() instanceof KeystoreCredentialController){
				((KeystoreCredentialController)IdentityActivator.getDefault().getCredential()).removeKeyEntry(alias);	
			}
			return false;
		} else {
			throw new Exception("Unexpected exception found: expected "
					+ TrustedCertificateExpiredException.class.getName()
					+ " or " + KeyCertificateExpiredException.class.getName()
					+ ", found " + p.getCause().getClass().getName());
		}

	}

}
