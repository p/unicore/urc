package de.fzj.unicore.rcp.identity;

import java.util.Properties;

import org.eclipse.jface.preference.IPreferenceStore;

public class UnityCredentialController implements CredentialController {

	private final IdentityActivator parent;

	public UnityCredentialController(IdentityActivator parent){
		this.parent = parent;
	}

	@Override
	public Properties getCredentialProperties() {
		Properties p = new Properties();
		IPreferenceStore prefs = parent.getPreferenceStore();
		p.put("unity.address",prefs.getString(IdentityConstants.P_UNITY_HOST));
		p.put("unity.username",prefs.getString(IdentityConstants.P_UNITY_USER));
		p.put("unity.password",parent.getCredentialPassphrase());
		return p;
	}

	public boolean isReady(){
		// TODO add checks if all our required properties are set
		return true;
	}

}
