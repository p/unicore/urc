package de.fzj.unicore.rcp.identity.profiles;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.common.guicomponents.FancyTableViewer;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.extensionpoints.ICredentialTypeExtensionPoint;

public class ProfileListTableViewer extends FancyTableViewer {

	// column headers
	public static final String PROFILE = "Profile Name";

	/**
	 * the order of the header names in columnProperties determines the order of
	 * columns in the table
	 */
	public List<String> columnProperties;

	private ProfileList profileList;
	/**
	 * List of actions that can be executed on this viewer
	 */
	List<IAction> actions;
	private Action addProfileAction;
	private Action removeProfileAction;

	/**
	 * The constructor.
	 */
	public ProfileListTableViewer(Composite parent, ProfileList _profileList) {
		super(parent, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION);

		profileList = _profileList;

		columnProperties = new ArrayList<String>(getCredentialTypeRegistry()
				.getAllTypes());
		columnProperties.add(0, PROFILE);

		createTable();
		// initialize action list
		createActions();
		createCellEditors();
		setColumnProperties(columnProperties.toArray(new String[0]));
		setCellModifier(new ProfileListCellModifier(this));

		setUseHashlookup(true);
		setContentProvider(new ProfileListContentProvider());
		setLabelProvider(new ProfileListLabelProvider(this));
		setInput(profileList);
		setResizePolicy(RESIZE_POLICY_EQUAL_WIDTH);
		packTable();
		// create context menu for the table
		final MenuManager menuMgr = new MenuManager("Actions");
		menuMgr.setRemoveAllWhenShown(true);

		Menu menu = menuMgr.createContextMenu(getTable());
		getTable().setMenu(menu);
		IMenuListener listener = new IMenuListener() {
			public void menuAboutToShow(IMenuManager m) {
				for (Iterator<IAction> iter = actions.iterator(); iter
						.hasNext();) {
					IAction action = iter.next();
					if (action.isEnabled()) {
						menuMgr.add(action);
					}
				}
			}
		};
		menuMgr.addMenuListener(listener);

	}

	private void createActions() {
		actions = new ArrayList<IAction>();
		addProfileAction = new Action() {
			@Override
			public void run() {
				String name = null;
				name = "Profile";

				Profile[] profiles = profileList.getProfiles();
				int maxIndex = 0;
				for (int i = 0; i < profiles.length; i++) {
					String s = profiles[i].getName();
					if (s.startsWith(name)) {
						try {
							int index = Integer.parseInt(s.replace(name, ""));
							if (index > maxIndex) {
								maxIndex = index;
							}
						} catch (Exception e) {
						}
					}
				}
				Profile profile = new Profile();
				try {
					name = name + (maxIndex + 1);
					profile.setName(name);
				} catch (Exception e) {

				}

				getCredentialTypeRegistry().initProfile(profile);
				try {
					profileList.addProfile(profile);
				} catch (Exception e) {
					IdentityActivator.log(IStatus.ERROR,
							"Unable to add profile", e);
				}

			}
		};
		addProfileAction.setText("Add Profile");
		addProfileAction
				.setToolTipText("Add a profile for which specific credentials shall be used.");
		addProfileAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("add.png"));
		actions.add(addProfileAction);

		removeProfileAction = new Action() {
			@Override
			@SuppressWarnings("unchecked")
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getSelection();
				for (Iterator<Profile> iter = selection.iterator(); iter
						.hasNext();) {
					Profile profile = iter.next();
					profileList.removeProfile(profile);
				}

			}
		};
		removeProfileAction.setText("Remove Profile");
		removeProfileAction
				.setToolTipText("Remove a profile that won't be used anymore.");
		removeProfileAction.setImageDescriptor(IdentityActivator
				.getImageDescriptor("delete.png"));
		actions.add(removeProfileAction);
	}

	protected void createCellEditors() {

		List<String> credentialTypes = getCredentialTypeRegistry()
				.getAllTypes();
		// Create the cell editors
		CellEditor[] cellEditors = new CellEditor[credentialTypes.size() + 1];

		// SITE (TextField)
		TextCellEditor ce = new TextCellEditor(getTable());
		((Text) ce.getControl()).setEditable(true);
		cellEditors[0] = ce;

		setCellEditors(cellEditors);
	}

	private Table createTable() {
		Table table = getTable();

		for (int i = 0; i < columnProperties.size(); i++) {
			TableColumn col = new TableColumn(table, SWT.LEFT, i);
			String type = columnProperties.get(i);
			col.setText(type);
			if (columnProperties.get(i).equals(PROFILE)) {
				col.setToolTipText("Name of the security profile.");
				col.setWidth(600);
			} else {

				col.setToolTipText(getCredentialTypeRegistry().getTooltip(type));
				col.setWidth(600);
			}
		}
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		return table;
	}

	public List<IAction> getActions() {
		return actions;
	}

	public Action getAddProfileAction() {
		return addProfileAction;
	}

	public int getColumnFor(String columnName) {
		for (int i = 0; i < columnProperties.size(); i++) {
			if (columnProperties.get(i).equals(columnName)) {
				return i;
			}
		}
		return -1;
	}

	public CredentialTypeRegistry getCredentialTypeRegistry() {
		return IdentityActivator.getDefault().getCredentialTypeRegistry();
	}

	/**
	 * Dynamically resolve the CellEditor for the given column and row. Returns
	 * true iff the CellEditor is not null which means that the cell is
	 * modifiable.
	 * 
	 * @param column
	 * @param identifier
	 * @return
	 */
	public boolean updateCellEditor(String property, Profile profile) {

		int column = getColumnFor(property);
		List<String> credentialTypes = getCredentialTypeRegistry()
				.getAllTypes();
		String type = credentialTypes.get(column - 1);

		ICredentialTypeExtensionPoint ext = getCredentialTypeRegistry()
				.getDefiningExtension(type);

		CellEditor editor = ext.getCellEditor(getTable(), profile);
		CellEditor[] editors = getCellEditors();
		editors[column] = editor;
		setCellEditors(editors);
		return editor != null;
	}

}