package de.fzj.unicore.rcp.identity.actions;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.IdentityConstants;
import de.fzj.unicore.rcp.identity.TrustController;
import de.fzj.unicore.rcp.identity.truststore.TruststoreChangedListener;
import de.fzj.unicore.rcp.logmonitor.utils.FileDialogUtils;

public class ImportCAsFromKeystoreAction extends Action {

	private String defaultFileType = null;

	private final TrustController tm;

	private TruststoreChangedListener tsListener;
	
	public ImportCAsFromKeystoreAction(TrustController tm, TruststoreChangedListener _listener) {
		super();
		setToolTipText("Add trusted CAs from a keystore");
		setImageDescriptor(IdentityActivator.getImageDescriptor("add.png"));
		this.tm = tm;
		this.tsListener = _listener;
	}

	private String[] askImportKeystoreFile() {
		Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();
		if (shell == null) {
			shell = new Shell();
		}
		FileDialog dialog = new FileDialog(shell, SWT.OPEN | SWT.MULTI);
		dialog.setFilterExtensions(Constants.IMPORTABLE_KEYSTORE_FORMATS);
		String filterPath = FileDialogUtils.getFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS);
		dialog.setFilterPath(filterPath);
		if (defaultFileType != null) {
			List<String> types = Arrays
					.asList(Constants.IMPORTABLE_KEYSTORE_FORMATS);
			int index = types.indexOf(defaultFileType);
			if (index >= 0) {
				dialog.setFilterIndex(index);
			}
		}
		dialog.setText("Please select keystore file(s) that you want to import from.");
		dialog.open();
		String[] filenames = dialog.getFileNames();
		if (filenames != null) {
			for (int i = 0; i < filenames.length; i++) {
				filenames[i] = dialog.getFilterPath() + File.separator
						+ filenames[i];
			}
		}
		FileDialogUtils.saveFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS,dialog.getFilterPath());
		return filenames;
	}

	public String getDefaultFileType() {
		return defaultFileType;
	}

	@Override
	public void run() {
		try {
			String[] files = askImportKeystoreFile();
			if (files != null) {
				for (String s : files) {
					File f = new File(s);
					tm.importKeyStore(f);
					tsListener.truststoreChanged();
				}
			}
		} catch (Exception e) {
			IdentityActivator.log(IStatus.ERROR, "Could not import keystore: "
					+ e.getMessage(), e);
		}
	}

	public void setDefaultFileType(String defaultFileType) {
		this.defaultFileType = defaultFileType;
	}
}
