package de.fzj.unicore.rcp.identity.problems;

import java.security.cert.CertPath;
import java.security.cert.CertPathValidatorException;
import java.security.cert.X509Certificate;
import java.util.Iterator;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.ui.dialogs.CertificateDetailsDialog;
import de.fzj.unicore.rcp.logmonitor.problems.GraphicalSolution;
import eu.unicore.problemutil.Problem;

public class AutomaticallyImportServerCertSolution extends GraphicalSolution {

	public AutomaticallyImportServerCertSolution() {
		super("TLS_SRV_GRAPH_AUTO", "TLS_SRV",
				"View server certificate and allow connection with this particular server...");

	}

	protected X509Certificate extractServerCert(Problem[] problems) {
		if (problems == null) {
			return null;
		}
		X509Certificate cert=null;
		for (Problem p : problems) {
			CertPathValidatorException e = findPathBuilderException(p.getCause());
			if (e != null) {
				CertPath cp=e.getCertPath();
				Iterator<?> it=cp.getCertificates().iterator();
				while(it.hasNext()) {
					cert = (X509Certificate)it.next();
				}
			}
		}
		return cert;
	}

	protected CertPathValidatorException findPathBuilderException(Throwable t) {
		if (t == null) {
			return null;
		}
		if (t instanceof CertPathValidatorException) {
			return (CertPathValidatorException) t;
		} else {
			return findPathBuilderException(t.getCause());
		}
	}

	@Override
	public boolean isAvailableFor(String message, Problem[] problems,
			String details) {
		return extractServerCert(problems) != null;
	}

	@Override
	public boolean solve(String message, Problem[] problems, String details)
			throws Exception {
		X509Certificate x509 = extractServerCert(problems);
		Certificate cert = new Certificate(x509);
		Shell shell = getParentShell();
		CertificateDetailsDialog dlg = new CertificateDetailsDialog(shell,
				"Add security exception", null, cert, null);
		dlg.setMessage("The server has presented the following certificate. You can trust the server by adding this certificate to your truststore.");
		dlg.addButton(Window.OK, "Add to truststore", false);
		dlg.addButton(Window.CANCEL, "Cancel", true);
		dlg.open();
		int returnCode = dlg.getReturnCode();
		boolean added = returnCode == Window.OK;
		if (added) {
			IdentityActivator.getDefault().getTrust().addTrustedCertificate(cert);
		}
		return !added;
	}

}
