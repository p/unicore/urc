package de.fzj.unicore.rcp.identity.ui.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.IdentityConstants;

/**
 * This class represents the UNICORE security preference page 
 * that is available in the Preferences.
 * 
 * TODO needs update to cover all security settings
 * 
 * @author Bastian Demuth, Valentina Huber
 */
public class IdentityPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public IdentityPreferencePage() {
		super(GRID);
		setPreferenceStore(IdentityActivator.getDefault().getPreferenceStore());
		setDescription("Grid security preferences.");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		addField(new BooleanFieldEditor(
				IdentityConstants.P_SHOW_EXPIRY_WARNINGS,
				"Show certificate &expiry warnings at client startup",
				getFieldEditorParent()));

	}

	public void init(IWorkbench workbench) {

	}

}