package de.fzj.unicore.rcp.identity;

import de.fzj.unicore.rcp.logmonitor.utils.FileDialogUtils;

public interface IdentityConstants {

	/**
	 * Boolean preference that states whether the user wants to store his
	 * password in the encrypted Eclipse keyring file.
	 */
	public static final String P_STORE_PASSWORD = "de.fzj.unicore.rcp.idenity.preferences.storePassword";

	/**
	 * Boolean preference that states whether the user wants to see warning
	 * dialogs when certificates will soon expire or have expired during client
	 * startup.
	 */
	public static final String P_SHOW_EXPIRY_WARNINGS = "de.fzj.unicore.rcp.idenity.preferences.showExpiryWarnings";
	
	/**
	 * Constant that can be used in combination with {@link FileDialogUtils} to
	 * remember a directory for saving and loading credentials.
	 */
	public static final String FILE_DIALOG_ID_CREDENTIALS = "de.fzj.unicore.rcp.identity.credentials";

	/**
	 * credential type
	 */
	public static final String P_CREDENTIAL_TYPE = "de.fzj.unicore.rcp.idenity.preferences.credential.type";

	/**
	 * credential keystore path
	 */
	public static final String P_CREDENTIAL_PATH = "de.fzj.unicore.rcp.idenity.preferences.credential.path";

	/**
	 * credential keystore password
	 */
	public static final String P_CREDENTIAL_PASSWORD = "de.fzj.unicore.rcp.idenity.preferences.credential.password";

	/**
	 * MyProxy enabled
	 */
	public static final String P_MYPROXY_ENABLED = "de.fzj.unicore.rcp.idenity.preferences.myproxy.enabled";

	/**
	 * MyProxy host
	 */
	public static final String P_MYPROXY_HOST = "de.fzj.unicore.rcp.idenity.preferences.myproxy.host";

	/**
	 * MyProxy username
	 */
	public static final String P_MYPROXY_USER = "de.fzj.unicore.rcp.idenity.preferences.myproxy.user";

	/**
	 * MyProxy username
	 */
	public static final String P_MYPROXY_PASSWORD = "de.fzj.unicore.rcp.idenity.preferences.myproxy.password";

	/**
	 * Unity host
	 */
	public static final String P_UNITY_HOST = "de.fzj.unicore.rcp.idenity.preferences.unity.host";

	/**
	 * Unity username
	 */
	public static final String P_UNITY_USER = "de.fzj.unicore.rcp.idenity.preferences.unity.user";

	/**
	 * Unity password
	 */
	public static final String P_UNITY_PASSWORD = "de.fzj.unicore.rcp.idenity.preferences.unity.password";

	/**
	 * Boolean preference that states whether the user wants to store his
	 * Unity password in the encrypted Eclipse keyring file.
	 */
	public static final String P_UNITY_STORE_LOGIN = "de.fzj.unicore.rcp.idenity.preferences.unity.storeLogin";
	
	/**
	 * truststore type
	 */
	public static final String P_TRUSTSTORE_TYPE = "de.fzj.unicore.rcp.idenity.preferences.truststore.type";

	/**
	 * truststore keystore/directory path
	 */
	public static final String P_TRUSTSTORE_PATH = "de.fzj.unicore.rcp.idenity.preferences.truststore.path";

	/**
	 * truststore keystore password
	 */
	public static final String P_TRUSTSTORE_PASSWORD = "de.fzj.unicore.rcp.idenity.preferences.truststore.password";

	/**
	 * if truststore is a directory, is it an OpenSSL dir?
	 */
	public static final String P_TRUSTSTORE_OPENSSL = "de.fzj.unicore.rcp.idenity.preferences.truststore.openssl";

	/**
	 * Boolean preference that states whether the user wants to store his
	 * password in the encrypted Eclipse keyring file.
	 */
	public static final String P_TRUSTSTORE_STORE_PASSWORD = "de.fzj.unicore.rcp.idenity.preferences.truststore.storePassword";

}

