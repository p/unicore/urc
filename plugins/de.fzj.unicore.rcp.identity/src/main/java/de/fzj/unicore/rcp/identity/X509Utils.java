/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.identity;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyStore;
import java.security.Security;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;

import javax.security.auth.x500.X500Principal;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.eclipse.core.runtime.IStatus;

import de.fzj.unicore.rcp.identity.keystore.Certificate;
import eu.emi.security.authn.x509.impl.X500NameUtils;

/**
 * Utilities for processing a X509 certificate Taken from xUUDB common package
 * 
 * @author (just here) hmersch
 */
@SuppressWarnings("deprecation")
public class X509Utils {

	private static String beginKey = "-----BEGIN CERTIFICATE-----";
	private static String endKey = "-----END CERTIFICATE-----";

	public static KeyStore getKeystoreInstance(File keystoreFile) {

		try {
			Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

		} catch (Exception ex) {
			IdentityActivator.log(IStatus.ERROR,
					"Could not add BC security provider, "
							+ "this may cause errors later.", ex);
		}

		KeyStore keystore = null;
		try {
			String name = keystoreFile.getName();
			String extension = name.substring(name.lastIndexOf("."));

			if (extension.equalsIgnoreCase(".p12")) {
				keystore = KeyStore.getInstance("PKCS12");
			} else if (extension.equalsIgnoreCase(".jceks")) {
				keystore = KeyStore.getInstance("JCEKS", "SunJCE");
			} else {
				keystore = KeyStore.getInstance("JKS", "SUN");
			}
		} catch (Exception e) {
			IdentityActivator.log(IStatus.ERROR,
					"Unable to get keystore instance: " + e.getMessage(), e);
		}
		return keystore;
	}

	public static String getPEMStringFromX509(X509Certificate x509)
			throws CertificateEncodingException {
		return getPEMStringFromX509(x509, false);
	}

	public static String getPEMStringFromX509(X509Certificate x509,
			boolean addLineBreaks) throws CertificateEncodingException {
		String ret;
		byte[] coded = x509.getEncoded();
		byte[] base64 = new Base64().encode(coded);
		String pem = new String(base64);
		StringBuffer crSeparated = new StringBuffer();
		if (addLineBreaks) {

			for (int i = 0; i < pem.length() - 1; i += 64) {
				int limit = Math.min(i + 64, pem.length());
				crSeparated.append(pem.substring(i, limit));
				crSeparated.append("\n");
			}
			pem = crSeparated.toString();
		} else {
			pem += "\n";
		}
		ret = beginKey + "\n" + pem + endKey;
		return ret;
	}

	public static String getStringFromPEMFile(File pemFile)
			throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(pemFile));
		try {
			while (!br.readLine().equals(beginKey)) {
				;
			}
		
		StringBuffer cert = new StringBuffer();
		cert.append(beginKey + "\n");
		String line = new String();
			while (!(line = br.readLine()).equals(endKey)) {
				cert.append(line);
			}
			cert.append("\n" + endKey);
			return cert.toString();
		} finally {
			br.close();
		}
		
	}

	public static X509Certificate getX509FromPEMString(String pemstr)
			throws CertificateException {
		String work = pemstr.replaceAll(beginKey + "\n", "").replaceAll(endKey,
				"");
		byte[] certbuf = new Base64().decode(work.getBytes());
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		X509Certificate x509cert = null;
		x509cert = (X509Certificate) cf
				.generateCertificate(new ByteArrayInputStream(certbuf));
		return x509cert;
	}

	public static X509Certificate getX509FromFile(File certfile)
			throws Exception {
		try {
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			return (X509Certificate) cf.generateCertificate(new FileInputStream(certfile));

		} catch (Exception e) {
			String newPEMCertString = X509Utils.getStringFromPEMFile(certfile);
			return getX509FromPEMString(newPEMCertString);

		}
	}

	/**
	 * For a given X.509 certificate get a representative alias for it in a
	 * keystore. For a self-signed certificate this will be the subject's common
	 * name (if any). For a non-self-signed certificate it will be the subject's
	 * common name followed by the issuer's common name in parenthesis.
	 * 
	 * @param cert
	 *            The certificate
	 * @return The alias or a blank string if none could be worked out
	 */
	public static String getCertificateAlias(X509Certificate cert) {
		X500Principal subject = cert.getSubjectX500Principal();
		X500Principal issuer = cert.getIssuerX500Principal();

		String sSubjectCN = getCommonName(subject);

		// Could not get a subject CN - return blank
		if (sSubjectCN == null) {
			return "";
		}

		String sIssuerCN = getCommonName(issuer);

		// Self-signed certificate or could not get an issuer CN
		if (subject.equals(issuer) || sIssuerCN == null) {
			// Alias is the subject CN
			return sSubjectCN;
		}
		// else non-self-signed certificate
		// Alias is the subject CN followed by the issuer CN in parenthesis
		return MessageFormat.format("{0} ({1})", sSubjectCN, sIssuerCN)
		.toLowerCase();
	}
	
	public static String getCommonName(X500Principal principal) {
		if (principal == null) {
			return null;
		}
		return X500NameUtils.getAttributeValues(principal.getName(), BCStyle.CN)[0];
	}
	
	public static void exportPublicKey(Certificate cert, File file) throws Exception {
		exportPublicKey(cert, file, false);
	}
	
	/**
	 * Stores the public key of the specified certificate to the file.
	 * 
	 * @param cert
	 * @throws Exception
	 */
	public static void exportPublicKey(Certificate cert, File file, boolean append)
	throws Exception {
		String filename = file.getCanonicalPath();
		FileOutputStream fos = null;
		boolean writePEM = filename.toLowerCase().endsWith(".pem");
		// do NOT append in DER format, doesn't make sense
		append = append && writePEM; 
		fos = new FileOutputStream(filename, append);
		try {
			if(writePEM)
			{
				// write out the certificate in PEM encoded format
				fos.write(cert.getCertAsPEMString().getBytes());
				fos.write("\n".getBytes());
			}
			else
			{
				fos.write(cert.getCert().getEncoded());
			}
		}
		finally {
			fos.close();
		}
	}

	
}