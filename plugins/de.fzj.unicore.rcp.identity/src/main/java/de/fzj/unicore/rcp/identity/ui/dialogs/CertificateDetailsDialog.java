package de.fzj.unicore.rcp.identity.ui.dialogs;

import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.bouncycastle.util.encoders.Hex;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.identity.DistinguishedName;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.KeyStoreManager;
import de.fzj.unicore.rcp.identity.extensionpoints.ICertificateAttributeExtensionPoint;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.ui.CertificateAttribute;

/**
 * This dialog displays the details of certificates in a keystore.
 * 
 * @author Valentina Huber
 */
public class CertificateDetailsDialog extends Dialog {

	class AddedButton {
		int id;
		String label;
		boolean makeDefault;

		public AddedButton(int id, String label, boolean makeDefault) {
			this.id = id;
			this.label = label;
			this.makeDefault = makeDefault;
		}
	}

	private class TableElementLabelProvider extends LabelProvider implements
			ITableLabelProvider {

		@Override
		public void dispose() {
			super.dispose();

		}

		/**
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
		 *      int)
		 */
		public Image getColumnImage(Object element, int columnIndex) {
			CertificateAttribute pair = (CertificateAttribute) element;
			if (columnIndex == 0) {
				Image result = pair.getImg();
				if (result == null) {
					return defaultAttributeImg;
				} else {
					return result;
				}
			}
			return null;

		}

		/**
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
		 *      int)
		 */
		public String getColumnText(Object element, int columnIndex) {
			CertificateAttribute pair = (CertificateAttribute) element;
			return columnIndex == 0 ? pair.getKey() : pair.getValue();
		}

	}

	private final static String FIELD = "attribute";

	private final static String VALUE = "value";
	private final static String KEY = "key.png";
	private final static String NOKEY = "nokey.png";

	private final static String NON_CRITICAL = "list_extensions.gif";

	private final static String CRITICAL = "list_errors.gif";
	private final static String VERSION = "list_links.gif";

	private String title;

	private String message = "Information about this certificate";

	private List<AddedButton> addedButtons = new ArrayList<AddedButton>();
	private Label messageLabel;

	private Certificate cert;
	private KeyStoreManager keystoreManager;
	private String alias;

	private Table attributeTable;
	private List<CertificateAttribute> attributes = new ArrayList<CertificateAttribute>();
	private List<ICertificateAttributeExtensionPoint> extensions = new ArrayList<ICertificateAttributeExtensionPoint>();

	private Image detailsImg = IdentityActivator.getImageDescriptor(
			"magnifier.png").createImage();
	private Image keyImg = IdentityActivator.getImageDescriptor(NOKEY)
			.createImage();
	private Image nokeyImg = IdentityActivator.getImageDescriptor(KEY)
			.createImage();

	private Image criticalImg = IdentityActivator.getImageDescriptor(CRITICAL)
			.createImage();

	private Image nonCriticalImg = IdentityActivator.getImageDescriptor(
			NON_CRITICAL).createImage();

	private Image defaultAttributeImg = IdentityActivator.getImageDescriptor(
			VERSION).createImage();


	/**
	 * shows detailed information about a certificate
	 * 
	 * @param parentShell
	 * @param dialogTitle
	 * @param manager - if non-null, this is used to check if a private key exists
	 * @param cert
	 * @param alias
	 */
	public CertificateDetailsDialog(Shell parentShell, String dialogTitle,
			KeyStoreManager manager, Certificate cert, String alias) {

		super(parentShell);
		setShellStyle(SWT.DIALOG_TRIM | SWT.RESIZE | SWT.MAX);
		title = dialogTitle;
		keystoreManager = manager;
		this.cert = cert;
		this.alias = alias;
	}

	private void addAttribute(String name, String value, Image img) {
		CertificateAttribute pair = new CertificateAttribute(name, value, img);
		attributes.add(pair);
	}

	public void addButton(int id, String label, boolean makeDefault) {
		Composite parent = (Composite) buttonBar;
		if (buttonBar != null) {
			super.createButton(parent, id, label, makeDefault);
		} else {
			addedButtons.add(new AddedButton(id, label, makeDefault));
		}
	}

	private void addContributedAttributes() {
		iterateOverExtensions();
		for (ICertificateAttributeExtensionPoint ext : extensions) {
			try {
				List<CertificateAttribute> toAdd = ext.createAttributes(cert);
				if (toAdd != null) {
					attributes.addAll(toAdd);
				}
			} catch (Exception e) {
				IdentityActivator.log(IStatus.WARNING,
						"Problem while reading certificate attributes.",e);
			}
		}
	}

	/**
	 * This method builds the version-1-fields for the table of the details
	 * register.
	 */
	private void addCryptoAttributes() {
		X509Certificate x509 = cert.getCert();
		addAttribute("Subject DN", x509.getSubjectDN().getName(), defaultAttributeImg);
		addAttribute("Issuer DN", x509.getIssuerDN().getName(), defaultAttributeImg);
		addAttribute("signature algorithm", x509.getSigAlgName(),
				defaultAttributeImg);
		addAttribute("public key", getPublicKeyString(), defaultAttributeImg);
	}

	@Override
	public boolean close() {
		boolean b = super.close();
		detailsImg.dispose();
		detailsImg = null;
		keyImg.dispose();
		keyImg = null;
		nokeyImg.dispose();
		nokeyImg = null;
		criticalImg.dispose();
		criticalImg = null;
		nonCriticalImg.dispose();
		nonCriticalImg = null;
		defaultAttributeImg.dispose();
		defaultAttributeImg = null;
		for (ICertificateAttributeExtensionPoint ext : extensions) {
			try {
				ext.disposeImages();
			} catch (Exception e) {
				IdentityActivator
						.log(IStatus.WARNING,
								"Problem while cleaning up certificate attribute contribution.");
			}
		}
		return b;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets
	 * .Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		if (title != null) {
			shell.setText(title);
		}
		shell.setSize(600, 800);
		shell.setImage(detailsImg);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse
	 * .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		boolean okButtonAdded = false;
		for (AddedButton b : addedButtons) {
			if (b.id == Window.OK) {
				okButtonAdded = true;
				break;
			}
		}
		if (!okButtonAdded) {
			// create OK button
			createButton(parent, IDialogConstants.OK_ID,
					IDialogConstants.OK_LABEL, true);
		}
		for (AddedButton b : addedButtons) {
			createButton(parent, b.id, b.label, b.makeDefault);
		}
	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		// create composite

		Composite composite = (Composite) super.createDialogArea(parent);
		applyDialogFont(composite);
		GridLayout gridLayout = (GridLayout) composite.getLayout();
		gridLayout.numColumns = 2;

		Font initialFont = parent.getFont();
		FontData[] fontData = initialFont.getFontData();
		for (int i = 0; i < fontData.length; i++) {
			fontData[i].setHeight(12);
			fontData[i].setStyle(SWT.BOLD);
		}
		Font titleFont = new Font(parent.getDisplay(), fontData);
		for (int i = 0; i < fontData.length; i++) {
			fontData[i].setHeight(11);
			fontData[i].setStyle(SWT.BOLD);
		}
		Font font1 = new Font(parent.getDisplay(), fontData);
		for (int i = 0; i < fontData.length; i++) {
			fontData[i].setHeight(11);
			fontData[i].setStyle(SWT.NORMAL);
		}
		Font font2 = new Font(parent.getDisplay(), fontData);
		messageLabel = new Label(composite, SWT.WRAP);
		messageLabel.setText(message);
		messageLabel.setFont(titleFont);

		GridData titleData = new GridData(GridData.FILL_HORIZONTAL);
		titleData.horizontalSpan = 2;
		messageLabel.setLayoutData(titleData);

		Label issuedForLabel = new Label(composite, SWT.RIGHT);
		issuedForLabel.setText("Issued for:"); //$NON-NLS-1$  
		issuedForLabel.setFont(font1);

		Text dNArea2 = new Text(composite, SWT.MULTI | SWT.BORDER
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION
				| SWT.READ_ONLY);
		dNArea2.setFont(font2);
		dNArea2.setText(getSubjectString());
		GridData textData = new GridData(GridData.FILL_BOTH);
		dNArea2.setLayoutData(textData);

		Label issuedByLabel = new Label(composite, SWT.RIGHT);
		issuedByLabel.setText("Issued by:"); //$NON-NLS-1$  
		issuedByLabel.setFont(font1);

		Text dNArea4 = new Text(composite, SWT.MULTI | SWT.BORDER
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION
				| SWT.READ_ONLY);
		dNArea4.setFont(font2);
		dNArea4.setText(getIssuerString());
		dNArea4.setLayoutData(textData);

		Label validFromLabel = new Label(composite, SWT.RIGHT);
		validFromLabel.setText("Valid from:"); //$NON-NLS-1$   
		validFromLabel.setFont(font1);

		Text validFromText = new Text(composite, SWT.SINGLE | SWT.BORDER
				| SWT.READ_ONLY);
		validFromText
				.setText(cert.getValidFrom("EEE MMM dd HH:mm:ss zzz yyyy"));
		validFromText.setFont(font2);
		GridData text2Data = new GridData(GridData.FILL_HORIZONTAL);
		text2Data.widthHint = 300;
		validFromText.setLayoutData(text2Data);

		Label validToLabel = new Label(composite, SWT.RIGHT);
		validToLabel.setText("Valid to:"); //$NON-NLS-1$
		validToLabel.setFont(font1);

		String validToInfo = cert.getValidTo("EEE MMM dd HH:mm:ss zzz yyyy");
		Text validToText = new Text(composite, SWT.SINGLE | SWT.BORDER
				| SWT.READ_ONLY);
		validToText.setText(validToInfo);
		validToText.setFont(font2);
		validToText.setLayoutData(text2Data);

		addCryptoAttributes();
		addContributedAttributes();

		if (attributes.size() > 0) {
			Label attributeLabel = new Label(composite, SWT.RIGHT);
			attributeLabel.setFont(font1);
			attributeLabel.setText("Additional attributes: ");
			GridData attributeLabelData = new GridData(SWT.LEFT, SWT.CENTER,
					false, false);
			attributeLabelData.horizontalSpan = 2;
			attributeLabel.setLayoutData(attributeLabelData);

			TableViewer viewer = new TableViewer(composite, SWT.MULTI
					| SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
					| SWT.FULL_SELECTION);
			attributeTable = viewer.getTable();
			attributeTable.setLinesVisible(true);
			attributeTable.setHeaderVisible(true);

			GridData attributeData = new GridData(GridData.FILL_BOTH);
			attributeData.horizontalSpan = 2;
			attributeTable.setLayoutData(attributeData);

			final String[] titles = { FIELD, VALUE };
			for (int i = 0; i < titles.length; i++) {
				TableColumn column = new TableColumn(attributeTable, SWT.NONE);
				column.setText(titles[i]);
			}

			// add cell editors in order to allow copy + paste (but no editing)
			CellEditor[] editors = new CellEditor[attributeTable
					.getColumnCount()];
			for (int i = 0; i < editors.length; i++) {
				editors[i] = new TextCellEditor(attributeTable, SWT.READ_ONLY);
			}
			viewer.setColumnProperties(titles);
			viewer.setCellEditors(editors);
			viewer.setCellModifier(new ICellModifier() {

				public boolean canModify(Object element, String property) {
					return titles[1].equals(property);
				}

				public Object getValue(Object element, String property) {
					CertificateAttribute item = (CertificateAttribute) element;
					if (titles[0].equals(property)) {
						return item.getKey();
					} else {
						return item.getValue();
					}
				}

				public void modify(Object element, String property, Object value) {
					// do nothing! => item remains unchanged
				}
			});

			viewer.setContentProvider(new IStructuredContentProvider() {

				public void dispose() {

				}

				public Object[] getElements(Object inputElement) {
					return ((List<?>) inputElement)
							.toArray();
				}

				public void inputChanged(Viewer viewer, Object oldInput,
						Object newInput) {
				}
			});
			viewer.setLabelProvider(new TableElementLabelProvider());

			viewer.setInput(attributes);
			attributeTable.setSize(200, 200);
			for (int i = 0; i < titles.length; i++) {
				attributeTable.getColumn(i).pack();
			}

		}

		if (alias != null) {
			try {
				Label privKeyImg = new Label(composite, SWT.RIGHT);
				privKeyImg.setLayoutData(new GridData(
						GridData.HORIZONTAL_ALIGN_END));
				Label privKeyLabel = new Label(composite, SWT.LEFT);
				privKeyLabel.setFont(font1);
				if (keystoreManager != null
						&& keystoreManager.isKeyEntry(alias)) {
					privKeyImg.setImage(nokeyImg);
					privKeyLabel
							.setText("You own a private key for this certificate."); //$NON-NLS-1$
				} else {
					privKeyImg.setImage(keyImg);
					privKeyLabel
							.setText("You don't have a private key for this certificate."); //$NON-NLS-1$
				}
				privKeyLabel.setLayoutData(text2Data);
			} catch (Exception e) {
				IdentityActivator.log("Open KeyStoreManager failed", e);
			}
		}
		return composite;
	}

	@Override
	public Button getButton(int id) {
		return super.getButton(id);
	}

	private String getIssuerString() {
		try {
			DistinguishedName dn = DistinguishedName.fromString(cert
					.getIssuerDN());
			return dn.getNiceString("\n");
		} catch (Exception e) {
			StringBuffer sb = new StringBuffer(cert.getIssuerCN());
			sb.append("\n");
			StringTokenizer stk = new StringTokenizer(cert.getIssuerDN(), ",");
			while (stk.hasMoreTokens()) {
				sb.append(stk.nextToken().trim());
				if (stk.hasMoreTokens()) {
					sb.append("\n");
				}
			}
			return sb.toString();
		}
	}

	private String getPublicKeyString() {
		PublicKey publicKey = cert.getCert().getPublicKey();
		StringBuffer sb = new StringBuffer(publicKey.getAlgorithm());
		sb.append(" ");
		byte[] textByte = publicKey.getEncoded();
		String zk = new String(Hex.encode(textByte));
		for (int i = 0; i < (zk.length() - 3);) {
			sb.append(zk.substring(i, i + 4)).append(" ");
			i += 4;
		}
		return sb.toString();
	}

	private String getSubjectString() {
		try {
			DistinguishedName dn = DistinguishedName.fromString(cert
					.getSubjectDN());
			return dn.getNiceString("\n");
		} catch (Exception e) {
			// fallback if DN-parsing was unsuccessful
			StringBuffer sb = new StringBuffer(cert.getSubjectCN());
			sb.append("\n");

			StringTokenizer stk = new StringTokenizer(cert.getSubjectDN(), ",");
			while (stk.hasMoreTokens()) {
				sb.append(stk.nextToken().trim());
				if (stk.hasMoreTokens()) {
					sb.append("\n");
				}
			}
			return sb.toString();
		}

	}

	private void iterateOverExtensions() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(ICertificateAttributeExtensionPoint.ID);
		IExtension[] extensions = extensionPoint.getExtensions();
		for (IExtension ext : extensions) {
			try {
				IConfigurationElement member = ext.getConfigurationElements()[0];
				ICertificateAttributeExtensionPoint certAttributeExtension = (ICertificateAttributeExtensionPoint) member
						.createExecutableExtension("name");
				this.extensions.add(certAttributeExtension);
			} catch (CoreException ex) {
				String label = ext.getLabel();
				if (label == null) {
					label = ext.getUniqueIdentifier();
				}
				IdentityActivator.log(IStatus.WARNING,
						"Unable to add certificate attributes for extension "
								+ label, ex);
			}
		}
	}

	public void setMessage(String message) {
		if (messageLabel != null) {
			messageLabel.setText(message);
		} else {
			this.message = message;
		}

	}
}