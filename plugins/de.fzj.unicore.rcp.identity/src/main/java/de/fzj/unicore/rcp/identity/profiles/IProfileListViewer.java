package de.fzj.unicore.rcp.identity.profiles;

public interface IProfileListViewer {

	/**
	 * Update the view to reflect the fact that a profile was added to the
	 * profile list
	 * 
	 * @param profile
	 */
	public void addProfile(Profile profile);

	/**
	 * Update the view to reflect the fact that one of the profile names was
	 * modified
	 * 
	 * @param oldName
	 * @param newName
	 */
	public void profileNameChanged(String oldName, String newName,
			Profile profile);

	/**
	 * Update the view to reflect the fact that a profile was removed from the
	 * profile list
	 * 
	 * @param profile
	 */
	public void removeProfile(Profile profile);

	/**
	 * Update the view to reflect the fact that one of the profiles was modified
	 * 
	 * @param profile
	 */
	public void userDataChanged(Profile profile);
}
