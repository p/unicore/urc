package de.fzj.unicore.rcp.identity.extensions;

import java.util.List;
import java.util.Properties;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.IdentityActivator.CredentialChoice;
import de.fzj.unicore.rcp.identity.extensionpoints.ICredentialTypeExtensionPoint;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.keystore.CertificateList;
import de.fzj.unicore.rcp.identity.keystore.ICertListViewer;
import de.fzj.unicore.rcp.identity.keystore.KeystoreCredentialController;
import de.fzj.unicore.rcp.identity.profiles.Profile;
import de.fzj.unicore.rcp.identity.profiles.ProfileList;
import eu.unicore.security.canl.AuthnAndTrustProperties;
import eu.unicore.security.canl.CredentialProperties;
import eu.unicore.util.httpclient.ClientProperties;
import eu.unicore.util.httpclient.IClientConfiguration;

public class CredentialTypeCertificateExtension implements
		ICredentialTypeExtensionPoint, ICertListViewer, IPropertyChangeListener {

	public static final String CERTIFICATE = "certificate";
	public static final String CERTIFICATE_INDEX = "certificate index";

	private static final Certificate _EMPTY_CERTIFICATE = new Certificate();

	private String[] certNames;
	private ProfileList profileList;

	
	private CertificateList getCertificateList(){
		return ((KeystoreCredentialController)IdentityActivator.getDefault().getCredential())
				.getCertificateList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.identity.keystore.ICertListViewer#addCert(de.fzj.unicore
	 * .rcp.identity.keystore.Certificate)
	 */
	public void addCert(Certificate u) {
		updateCertNames();

	}

	public void addCredentials(IClientConfiguration secProps,
			Profile profile, List<String> identifiers) {
		
		// currently this only works if we're using a keystore
		if(CredentialChoice.KEYSTORE != IdentityActivator.getDefault().getCredentialType()){
			return;
		}
		
		CertificateList certificateList = getCertificateList();

		Certificate cert = null;

		if (profile == null) {
			cert = certificateList.getDefaultCert();
		} else {
			String certName = getCertificate(profile).getName();
			cert = certificateList.getCertByName(certName);
			if (cert == null) {
				IdentityActivator.log(IStatus.WARNING,
						"Error configuring security: unknown certificate "
								+ certName + ". Using default certificate.");
				cert = certificateList.getDefaultCert();
			} else {
				secProps.getExtraSecurityTokens().put(IdentityActivator.DEFAULT_PROPERTIES,
						Boolean.FALSE.toString());
			}
		}

		if (cert != null) {
			// need to reload the credential! A bit ugly ... better would be to modify the properties
			// before creating the IClientConfiguration
			ClientProperties cp=(ClientProperties)secProps;
			AuthnAndTrustProperties authn=(AuthnAndTrustProperties)cp.getAuthnAndTrustConfiguration();
			CredentialProperties cred=authn.getCredentialProperties();
			Properties properties=new Properties();
			properties.setProperty("credential.path", cred.getValue(CredentialProperties.PROP_LOCATION));
			properties.setProperty("credential.password", cred.getValue(CredentialProperties.PROP_PASSWORD));
			properties.setProperty("credential.keyAlias", cert.getName());
			if(cred.getValue(CredentialProperties.PROP_FORMAT)!=null){
				properties.setProperty("credential.format", cred.getValue(CredentialProperties.PROP_FORMAT));
			}
			CredentialProperties newCred=new CredentialProperties(properties);
			cp.setCredential(newCred.getCredential());
		}
	}

	public void dispose() {
		getCertificateList().removeChangeListener(this);
	}

	public CellEditor getCellEditor(Composite parent, Profile profile) {
		if (certNames == null) {
			CertificateList certificateList = getCertificateList();
			certNames = new String[] { "" };
			if (certificateList != null) {
				certNames = certificateList.getCertNames();
				certificateList.addChangeListener(this);
			}
		}
		return new CertificateCellEditor(parent, certNames);
	}

	public Image getImage(Profile profile) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getLabel(Profile profile) {
		String label = "none";
		if (getCertificate(profile) != null) {
			label = getCertificate(profile).getName();
		}
		return label;
	}

	public void initProfile(Profile newProfile) {
		CertificateList certList = getCertificateList();
		Certificate defaultCert = certList.getDefaultCert();
		int index = defaultCert == null ? 0 : certList
				.getCertIndexByName(defaultCert.getName());
		setCertificate(newProfile, defaultCert);
		setCertificateIndex(newProfile, index);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Preferences.IPropertyChangeListener#propertyChange
	 * (org.eclipse.core.runtime.Preferences.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent event) {
		if (Constants.P_KEYSTORE_PATH.equals(event.getProperty())
				|| IdentityActivator.PROPERTY_KEYS.equals(event.getProperty())) {
			validateProfileList();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.identity.keystore.ICertListViewer#reloadAllCerts()
	 */
	public void reloadAllCerts() {
		updateCertNames();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.identity.keystore.ICertListViewer#removeCert(de.fzj
	 * .unicore.rcp.identity.keystore.Certificate)
	 */
	public void removeCert(Certificate u) {
		updateCertNames();
	}

	public void setProfileList(ProfileList profileList) {
		this.profileList = profileList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.identity.keystore.ICertListViewer#updateCert(de.fzj
	 * .unicore.rcp.identity.keystore.Certificate)
	 */
	public void updateCert(Certificate u) {
		updateCertNames();
	}

	public void updateCertNames() {
		if (certNames == null) {
			return;
		}
		CertificateList certificateList = getCertificateList();
		certNames = new String[] { "" };
		if (certificateList != null) {
			certNames = certificateList.getCertNames();
		}
		// certEditor.setValue(certificateList.getCertIndexByName(certificateList.getDefaultCert())
	}

	private void validateProfileList() {
		for (Profile profile : profileList.getProfiles()) {

			try {
				CertificateList certificateList = getCertificateList();
				Certificate old = getCertificate(profile);
				Certificate cert = certificateList.getCertByName(old.getName());
				int index = -1;
				if (cert == null) {
					cert = certificateList.getDefaultCert();

					setCertificate(profile, cert);

					if (cert != null) {
						index = certificateList.getCertIndexByName(cert.getName());
					}
					setCertificateIndex(profile, index);
					profileList.updateProfile(profile);
					if (old != _EMPTY_CERTIFICATE) {
						if (cert == null) {
							IdentityActivator.log("Certificate for profile "
									+ profile.getName()
									+ " does not exist anymore.");
						} else {
							IdentityActivator
									.log("Certificate for profile "
											+ profile.getName()
											+ " does not exist anymore. Trying to use default certificate instead.");
						}
					}
				} else {
					index = certificateList.getCertIndexByName(cert.getName());
					if (index == -1) {
						IdentityActivator.log(IStatus.WARNING,
								"Could not determine certificate for profile "
										+ profile.getName());
					}
					if (index != getCertificateIndex(profile)) {
						// make sure index is set correctly
						setCertificateIndex(profile, index);
					}
					
					/* update list in any case as the alias of
					 * the cert might have changed and we would
					 * like to reflect this
					 */
					profileList.updateProfile(profile);
				}

			} catch (Exception e) {
				IdentityActivator.log(IStatus.WARNING,
						"Could not determine certificate for profile "
								+ profile.getName(), e);
			}

		}

	}

	public static Certificate getCertificate(Profile profile) {
		Certificate c = (Certificate) profile.getUserData(CERTIFICATE);
		if (c == null) {
			return _EMPTY_CERTIFICATE;
		} else {
			return c;
		}
	}

	public static Integer getCertificateIndex(Profile profile) {
		Integer result = (Integer) profile.getUserData(CERTIFICATE_INDEX);
		if (result == null) {
			return -1;
		} else {
			return result;
		}
	}

	public static void setCertificate(Profile profile, Certificate cert) {
		profile.setUserData(CERTIFICATE, cert);
	}

	public static void setCertificateIndex(Profile profile, Integer index) {
		profile.setUserData(CERTIFICATE_INDEX, index);
	}

}
