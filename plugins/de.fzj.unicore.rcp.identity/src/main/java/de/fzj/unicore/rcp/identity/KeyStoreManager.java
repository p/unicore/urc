package de.fzj.unicore.rcp.identity;

/*
 * Copyright (c) 2005, Forschungszentrum Juelich GmbH
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.net.Socket;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.net.ssl.X509KeyManager;

import org.bouncycastle.asn1.x509.X509Name;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.eclipse.core.runtime.IStatus;

import de.fzj.unicore.rcp.identity.exceptions.KeyCertificateExpiredException;
import de.fzj.unicore.rcp.identity.exceptions.TrustedCertificateExpiredException;

/**
 * This class manages an instance of a keystore described by provider, type,
 * file name. (Most of this code is from the Intel Unicore client)<br>
 * 
 * @author Thomas Kentemich (Intel GmbH)
 * @author Bernd Schuller
 */
public class KeyStoreManager {
	
	private static Map<File, KeyStoreManager> ksManagers = new HashMap<File, KeyStoreManager>();
	
	class CustomAliasKeyManager implements X509KeyManager {
		private X509KeyManager delegate;
		private String alias;

		public CustomAliasKeyManager(X509KeyManager delegate, String alias) {
			super();
			this.delegate = delegate;
			this.alias = alias;
		}

		public String chooseClientAlias(String[] keyType, Principal[] issuers,
				Socket socket) {
			// For each keyType, call getClientAliases on the base KeyManager
			// to find valid aliases. If our requested alias is found, select it
			// for return.

			for (int i = 0; i < keyType.length; i++) {
				String[] validAliases = delegate.getClientAliases(keyType[i],
						issuers);
				if (validAliases != null) {
					for (int j = 0; j < validAliases.length; j++) {
						if (validAliases[j].equals(alias)) {
							return alias;
						}
					}
				}
			}
			return null;
		}

		public String chooseServerAlias(String arg0, Principal[] arg1,
				Socket arg2) {
			return delegate.chooseServerAlias(arg0, arg1, arg2);
		}

		public X509Certificate[] getCertificateChain(String arg0) {
			return delegate.getCertificateChain(arg0);
		}

		public String[] getClientAliases(String arg0, Principal[] arg1) {
			return delegate.getClientAliases(arg0, arg1);
		}

		public PrivateKey getPrivateKey(String arg0) {
			return delegate.getPrivateKey(arg0);
		}

		public String[] getServerAliases(String arg0, Principal[] arg1) {
			return delegate.getServerAliases(arg0, arg1);
		}
	}

	public static final char[] DUMMY_PASSWORD = "password".toCharArray();

	private List<String> cachedKeyEntries;
	private List<String> cachedTrustedCertificates;
	private String defaultAlias = "";
	private KeyStore keystore;
	private String keystoreFilename = null;
	private String keystoreType = null;
	private Cipher passwdCipher = null;
	private String provider = null;
	private byte[] rawKey;
	private byte[] rawStore;
	private List<String> rejectedEntries;

	private Key tempKey = null;

	/**
	 * Constructor for the KeyStoreManager object
	 * 
	 * @param provider
	 *            JCE provider
	 * @param type
	 *            KeyStore type
	 * @param filename
	 *            KeyStore File name
	 */
	private KeyStoreManager(String provider, String type, String filename) {

		this.keystoreFilename = filename;
		this.provider = provider;
		this.keystoreType = type;
		try {
			Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

		} catch (Exception ex) {
			IdentityActivator.log(IStatus.ERROR,
					"Could not add BC security provider, "
							+ "this may cause errors later.", ex);
		}
	}

	/**
	 * Adds a key entry to the KeyStore
	 * 
	 * @param key
	 * @param certificateChain
	 * @param alias
	 */
	public void addKeyEntry(PrivateKey key, Certificate[] certificateChain,
			String alias) throws Exception {
		alias = alias.toLowerCase();
		keystore.setKeyEntry(alias, key, unwrapKeyPassword(), certificateChain);
		rehashKeyStoreEntries();
	}

	/**
	 * Adds a trusted certificate to the keystore
	 * 
	 * @param cert
	 *            Trusted certificate entry to be added
	 */
	public void addTrustedCertificate(String alias, X509Certificate cert)
			throws Exception {
		alias = alias.toLowerCase();
		keystore.setCertificateEntry(alias, cert);
		rehashKeyStoreEntries();
	}

	/**
	 * Generate an empty keystore
	 */
	public void buildEmptyKeyStore(String name, char[] storePassword,
			char[] keyPassword) throws Exception {
		this.startPasswordEncryption(storePassword, keyPassword);
		keystore = KeyStore.getInstance(keystoreType, provider);
		this.keystoreFilename = name;
		keystore.load(null, storePassword);
		rehashKeyStoreEntries();
	}

	protected String changeAlias(KeyStore keystore,
			List<String> cachedKeyEntries, String oldAlias, String newAlias)
					throws Exception {
		if (oldAlias == null ? newAlias == null : oldAlias.equals(newAlias)) {
			return newAlias;
		}
		if (keystore.containsAlias(oldAlias)) {
			boolean isKeyEntry = isKeyEntry(oldAlias);
			if (isKeyEntry) {
				Certificate[] chain = keystore.getCertificateChain(oldAlias);
				Key key = keystore.getKey(oldAlias, unwrapKeyPassword());
				keystore.deleteEntry(oldAlias);
				if (cachedKeyEntries != null) {
					cachedKeyEntries.remove(oldAlias);
				}
				keystore.setKeyEntry(newAlias, key, unwrapKeyPassword(), chain);
			} else {
				Certificate cert = keystore.getCertificate(oldAlias);
				keystore.deleteEntry(oldAlias);
				if (cachedKeyEntries != null) {
					cachedKeyEntries.remove(oldAlias);
				}
				keystore.setCertificateEntry(newAlias, cert);
			}
			rehashKeyStoreEntries();
			return newAlias;
		}
		return oldAlias;
	}

	/**
	 * Changes the alias name of an entry
	 * 
	 * @param oldAlias
	 *            -- the original
	 * @param newAlias
	 *            -- the new name
	 * @return old alias
	 */
	public String changeAlias(String oldAlias, String newAlias)
			throws Exception {
		return changeAlias(keystore, cachedKeyEntries, oldAlias, newAlias);
	}

	/**
	 * Change the KeyStore password
	 * 
	 */
	public final void changePassword(char[] oldPassword, char[] newPassword)
			throws Exception {
		if (new String(oldPassword).equals(new String(unwrapStorePassword()))) {
			try {
				KeyGenerator generator = KeyGenerator.getInstance("DES");
				tempKey = generator.generateKey();
				passwdCipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
				passwdCipher.init(Cipher.ENCRYPT_MODE, tempKey);
				rawStore = passwdCipher.doFinal(new String(newPassword)
				.getBytes());
				rawKey = passwdCipher.doFinal(new String(newPassword)
				.getBytes());
				IvParameterSpec dps = new IvParameterSpec(passwdCipher.getIV());
				passwdCipher.init(Cipher.DECRYPT_MODE, tempKey, dps);
				tempKey = null;
			} catch (Exception e) {
				IdentityActivator.log(IStatus.ERROR,
						"Bad encryption for passwd. ", e);
			}
			/*
			 * keystorePassword = newPassword;
			 */
			// put new password into key entries
			Enumeration<String> aliases = keystore.aliases();
			while (aliases.hasMoreElements()) {
				String alias = aliases.nextElement();
				Key key = keystore.getKey(alias, oldPassword);
				if (key instanceof PrivateKey) {
					Certificate[] chain = keystore.getCertificateChain(alias);
					keystore.setKeyEntry(alias,
							keystore.getKey(alias, oldPassword), newPassword,
							chain);
					/*
					 * keyPassword = newPassword;
					 */
				}
			}
			reWriteKeyStore(new File(keystoreFilename));
		} else {
			throw new KeyStoreException("Old password was not correct.");
		}
	}

	/**
	 * check whether the user certificate will expire soon
	 */
	public void checkIdentityValidity() {
		for (String alias : getIdentityAliases()) {
			try {

				X509Certificate cert = (X509Certificate) keystore
						.getCertificate(alias);
				long start = cert.getNotBefore().getTime();
				if (start > System.currentTimeMillis()) {
					IdentityActivator.log(IStatus.ERROR,
							"WARNING: the certificate "
									+ cert.getSubjectDN().getName()
									+ " is not yet valid!");
					return;
				}
				long end = cert.getNotAfter().getTime();
				long totalValid = end - start;
				long stillValid = end - System.currentTimeMillis();

				// check whether less than 5% of validity period remain
				long threshold = (long) (.05 * totalValid);
				if (stillValid < threshold) {
					// if so, warn user
					Date date = cert.getNotAfter();
					String text = "The \""
							+ alias
							+ "\" certificate has expired or will soon expire! The validity period ends "
							+ date;
					IdentityActivator.log(IStatus.WARNING, text);
				}

			} catch (Exception e) {
				IdentityActivator.log(IStatus.ERROR,
						"Unable to check validity period of certificate "
								+ alias + ": " + e.getMessage(), e);
			}
		}
	}

	/**
	 * Check the validity of the keystore: 1.) that the keystore is not empty
	 * and contains at least one key entry and one trusted certificate 2.) that
	 * the private key can be extracted and contains a valid certificate chain
	 * 3.) that the trusted certificates contain a valid certificate chain 4.)
	 * that the certificates are not expired etc.
	 * 
	 */
	public boolean checkKeyStore(char[] keyPassword) throws Exception {
		// is the keystore empty?
		if (keystore.size() == 0) {
			return true;
		}
		// sort trusted certificates and key entries
		cachedKeyEntries = new ArrayList<String>();
		cachedTrustedCertificates = new ArrayList<String>();
		rejectedEntries = new ArrayList<String>();
		// Vector caEntries = new Vector();
		Enumeration<String> knownAliases = keystore.aliases();
		while (knownAliases.hasMoreElements()) {
			String next = knownAliases.nextElement();
			if (keystore.isKeyEntry(next)) {
				if (!cachedKeyEntries.contains(next)) {
					try {
						if (validateKeyEntry(next, keyPassword)) {
							cachedKeyEntries.add(next);
						}
					}
					// warn user about wrong certs
					catch (CertificateExpiredException e) {
						KeyCertificateExpiredException e2 = new KeyCertificateExpiredException(
								e.getMessage(), next);
						IdentityActivator.log(IStatus.WARNING,
								"Expired certificate: " + next, e2);
						rejectedEntries.add(next);
					} catch (CertificateException e) {
						if (e.getMessage() != null
								&& e.getMessage().toLowerCase()
								.contains("expired")) {
							KeyCertificateExpiredException e2 = new KeyCertificateExpiredException(
									e.getMessage(), next);
							IdentityActivator.log(IStatus.WARNING,
									"Expired certificate: " + next, e2);
							rejectedEntries.add(next);
						} else {
							IdentityActivator.log(IStatus.WARNING,
									"Broken certificate: " + next, e);
							rejectedEntries.add(next);
						}
					} catch (Exception e) {
						IdentityActivator.log(IStatus.WARNING,
								"Broken certificate: " + next, e);
						rejectedEntries.add(next);
					}
				}
			} else if (keystore.isCertificateEntry(next)) {
				Certificate caCert = keystore.getCertificate(next);
				if (caCert instanceof X509Certificate) {
					// trusted certs are put into keystore, expired or not...
					if (!cachedTrustedCertificates.contains(next)) {
						try {
							if (validateCertificateEntry(next)) {
								cachedTrustedCertificates.add(next);
							}
						}
						// warn user about wrongcerts
						catch (CertificateExpiredException e) {
							TrustedCertificateExpiredException e2 = new TrustedCertificateExpiredException(
									e.getMessage(), next);
							IdentityActivator.log(IStatus.WARNING,
									"Expired certificate: " + next, e2);
							rejectedEntries.add(next);
						} catch (Exception e) {
							IdentityActivator.log(IStatus.WARNING,
									"Broken certificate: " + next, e);
							rejectedEntries.add(next);
						}
					}
				}
			}
		}
		return true;
	}

	public void exportCerts(String[] aliases, char[][] keyPasswords,
			String filename, char[] storePassword) throws Exception {
		if(filename == null) return;
		String provider = null;
		String type = null;
		String lowerCase = filename.toLowerCase();
		if(lowerCase.endsWith(".p12") || lowerCase.endsWith(".pkcs12") || lowerCase.endsWith(".pfx")) {
			provider = BouncyCastleProvider.PROVIDER_NAME;
			type = "PKCS12";
		}
		else
		{
			provider = "SUN";
			type = "JKS";
		}
		KeyStoreManager manager = getInstance(new File(filename), new String(
				storePassword),provider,type);
		Set<String> privateKeys = new HashSet<String>(getIdentityAliases());
		for (int i = 0; i < aliases.length; i++) {
			String alias = aliases[i];
			X509Certificate[] certs = getCertificateByAlias(alias);
			if (privateKeys.contains(alias)) {
				char[] password = keyPasswords[i];
				PrivateKey key = (PrivateKey) getKeyEntry(alias, password);
				manager.addKeyEntry(key, certs, alias);
			} else {
				for (X509Certificate certificate : certs) {
					manager.addTrustedCertificate(alias, certificate);
				}
			}
		}
		manager.reWriteKeyStore();
	}

	public byte[] generateCSR(String dnString, int keyLength, String newAlias)
			throws Exception {

		X509Name subject = CertificateUtility.stringToBcX509Name(dnString);
		// generate a new RSA keypair
		KeyPair kp = CertificateUtility.generateNewKeys("RSA", keyLength);
		// generate a self signed X509 certificate valid for 3 months
		X509V3CertificateGenerator v3CertGen = new X509V3CertificateGenerator();
		X509Principal x509Principal = new X509Principal(subject);
		v3CertGen.reset();
		v3CertGen.setSerialNumber(new BigInteger(16, new Random()));
		v3CertGen.setIssuerDN(x509Principal);
		v3CertGen.setNotBefore(new Date(System.currentTimeMillis()));
		v3CertGen.setNotAfter(new Date(System.currentTimeMillis()
				+ (90 * 1000L * 60 * 60 * 24)));
		v3CertGen.setSubjectDN(x509Principal);
		v3CertGen.setPublicKey(kp.getPublic());
		v3CertGen.setSignatureAlgorithm("MD5WithRSAEncryption");
		X509Certificate[] cert = { v3CertGen.generate(kp.getPrivate(), "BC") };
		addKeyEntry(kp.getPrivate(), cert, newAlias);

		// generate a certification request to be sent to the CA
		PKCS10CertificationRequest req1 = new PKCS10CertificationRequest(
				"SHA1withRSA", subject, kp.getPublic(), null, kp.getPrivate());
		req1.verify();
		reWriteKeyStore();
		return req1.getEncoded();
	}

	/**
	 * Gets the alias for a given certificate; returns null if not found
	 * 
	 * @param cert
	 *            The certificate to test
	 * @return Alias found.
	 */
	public String getAliasFromCertificate(Certificate cert) throws Exception {
		// return keyStore.getCertificateAlias(cert);
		X509Certificate x509 = null;
		String matchedAlias = null;
		for (Enumeration<String> aliases = keystore.aliases(); aliases .hasMoreElements();) {
			String nextAlias = aliases.nextElement();
			try {
				x509 = (X509Certificate) keystore.getCertificate(nextAlias);
			} catch (Exception e) {
				continue;
			}
			if (x509.equals(cert)) {
				matchedAlias = nextAlias;
				break;
			}
		}
		return matchedAlias;
	}

	public List<String> getCachedKeyEntries() {
		return this.cachedKeyEntries;
	}

	public List<String> getCachedTrustedCertificates() {
		return this.cachedTrustedCertificates;
	}

	/**
	 * Fetches the certificate known as alias
	 * 
	 * @param alias
	 *            Alias to fetch from the keystore
	 * @return X509 certificate chain or null
	 */
	public X509Certificate[] getCertificateByAlias(String alias)
			throws KeyStoreException {
		X509Certificate[] returnedChain = CertificateUtility.toX509(keystore
				.getCertificateChain(alias.toLowerCase()));
		if (returnedChain == null) {
			// try a trusted certificate
			X509Certificate trustedCertificate = (X509Certificate) keystore
					.getCertificate(alias.toLowerCase());
			if (trustedCertificate != null) {
				returnedChain = new X509Certificate[1];
				returnedChain[0] = trustedCertificate;
			}
		}
		return returnedChain;
	}

	public X509Certificate getCertificateByKey(PublicKey key) throws Exception {
		X509Certificate x509 = null;
		X509Certificate matchedX509 = null;
		for (Enumeration<String> aliases = keystore.aliases(); aliases
				.hasMoreElements();) {
			String nextAlias = aliases.nextElement();
			try {
				x509 = (X509Certificate) keystore.getCertificate(nextAlias);
			} catch (Exception e) {
				continue;
			}
			if (key.equals(x509.getPublicKey())) {
				matchedX509 = x509;
				break;
			}
		}
		return matchedX509;
	}

	/**
	 * Fetches the certificate by principal DN
	 * 
	 * @param principal
	 *            Principal to fetch from the keystore
	 * @return X509 certificate chain or null
	 */
	public X509Certificate getCertificateByPrincipal(Principal principal)
			throws KeyStoreException {
		X509Certificate x509 = null;
		X509Certificate matchedX509 = null;
		for (Enumeration<String> aliases = keystore.aliases(); aliases
				.hasMoreElements();) {
			String nextAlias = aliases.nextElement();
			try {
				x509 = (X509Certificate) keystore.getCertificate(nextAlias);
			} catch (Exception e) {
				continue;
			}
			if (principal.equals(x509.getSubjectDN())) {
				matchedX509 = x509;
				break;
			}
		}
		return matchedX509;
	}

	/**
	 * Fetches the certificate by subject DN Does possibly not work for key
	 * entries as the Sun JCE inverts the DN string for key entries!!
	 * 
	 * @param subject
	 *            DN to fetch from the keystore
	 * @return X509 certificate chain or null
	 */
	public X509Certificate getCertificateBySubjectDN(String subject)
			throws KeyStoreException {
		X509Certificate x509 = null;
		X509Certificate matchedX509 = null;
		for (Enumeration<String> aliases = keystore.aliases(); aliases
				.hasMoreElements();) {
			String nextAlias = aliases.nextElement();
			try {
				x509 = (X509Certificate) keystore.getCertificate(nextAlias);
			} catch (Exception e) {
				continue;
			}
			if (subject.equals(x509.getSubjectDN().getName())) {
				matchedX509 = x509;
				break;
			}
		}
		return matchedX509;
	}

	public String getDefaultAlias() {
		return this.defaultAlias;
	}

	public List<String> getIdentityAliases() {
		try {
			List<String> identityAliases = new ArrayList<String>();
			Enumeration<String> all = keystore.aliases();
			while (all.hasMoreElements()) {
				String id = all.nextElement();
				if (keystore.isKeyEntry(id)) {
					identityAliases.add(id);
				}
			}
			return identityAliases;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<String>();
		}
	}

	public String getKeyAliasFromCertificate(Certificate cert) throws Exception {
		X509Certificate x509 = null;
		String matchedAlias = null;
		for (Enumeration<String> aliases = keystore.aliases(); aliases
				.hasMoreElements();) {
			String nextAlias = aliases.nextElement();
			if (!keystore.isKeyEntry(nextAlias)) {
				continue;
			}
			try {
				x509 = (X509Certificate) keystore.getCertificate(nextAlias);
			} catch (Exception e) {
				continue;
			}
			if (x509.equals(cert)) {
				matchedAlias = nextAlias;
				break;
			}
		}
		return matchedAlias;
	}

	public final Key getKeyEntry(String alias, char[] keyPassword)
			throws Exception {
		return keystore.getKey(alias, keyPassword);
	}

	public String getKeyStoreName() {
		return this.keystoreFilename;
	}

	public String getKeyStoreType() {
		return this.keystoreType;
	}

	public List<String> getRejectedEntries() {
		return this.rejectedEntries;
	}

	/**
	 * Returns a newly initialized Signature object
	 * 
	 * @return The signature
	 * @exception Exception
	 *                Exception
	 */
	public final Signature getSignature(X509Certificate cert) throws Exception {
		// log.logWrite("Exporting signature for: " + defaultAlias);
		Signature signature = Signature.getInstance("MD5withRSA");
		String alias = getKeyAliasFromCertificate(cert);
		signature.initSign((PrivateKey) (keystore.getKey(alias,
				unwrapKeyPassword())));
		return signature;
	}

	public List<String> getTrustedAliases() {
		List<String> trustedAliases = new ArrayList<String>();
		try {
			Enumeration<String> all = keystore.aliases();
			while (all.hasMoreElements()) {
				String id = all.nextElement();
				if (keystore.isCertificateEntry(id)) {
					trustedAliases.add(id);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return trustedAliases;
	}

	/**
	 * import trusted certs from the given directory
	 */
	public void importTrustedCerts(File dir) {
		if (!dir.exists() || !dir.isDirectory()) {
			IdentityActivator.log("Cannot load certificate(s) from "
					+ dir.getPath());
		}
		int c = 0;
		for (File certFile : dir.listFiles()) {
			if (certFile.isDirectory()) {
				continue;
			}
			FileInputStream fis = null;
			try {
				CertificateFactory cf = CertificateFactory.getInstance("X.509");
				fis = new FileInputStream(certFile);
				X509Certificate cert = (X509Certificate) cf
						.generateCertificate(fis);
				if (getAliasFromCertificate(cert) != null) {
					continue; // don't add the same cert twice!
				}
				// try to find a unique alias
				// first use file name
				String alias = CertificateUtility.getCommonName(cert)
						.toLowerCase();
				if (getCertificateByAlias(alias) != null) {
					// if this failed, start counting
					int i = 1;
					alias = "trusted cert ";
					while (getCertificateByAlias(alias + i) != null) {
						i++;
					}
					alias = alias + i;
				}
				addTrustedCertificate(alias, cert);
				c++;
			} catch (IOException ioe) {
				IdentityActivator.log(
						"Invalid file " + certFile.getAbsolutePath(), ioe);
			} catch (CertificateException cex) {
				IdentityActivator.log(
						"Invalid certificate " + certFile.getAbsolutePath(),
						cex);
			} catch (Exception ex) {
				IdentityActivator.log(
						"Error adding certificate "
								+ certFile.getAbsolutePath(), ex);
			} finally {
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException ex) {
					}
				}
			}
			IdentityActivator.log("Imported " + c
					+ " trusted certificates from " + dir.getAbsolutePath());
		}
	}

	/**
	 * Check if this alias is a key entry
	 */
	public boolean isKeyEntry(String alias) throws Exception {
		return keystore.isKeyEntry(alias);
	}

	public final boolean keyStoreContainsAlias(String alias) throws Exception {
		return keystore.containsAlias(alias);
	}

	/**
	 * Load a keystore or define a new keystore.
	 */
	public final String loadKeyStore(char[] storePassword, char[] keyPassword)
			throws Exception {
		startPasswordEncryption(storePassword, keyPassword);
		// try to read the keystore
		this.keystore = readKeyStore(storePassword);
		if (defaultAlias == null || !keystore.containsAlias(defaultAlias) || !keystore.isKeyEntry(defaultAlias)) {
			for (Enumeration<String> aliases = keystore.aliases(); aliases
					.hasMoreElements();) {
				String nextAlias = aliases.nextElement();
				if (keystore.isKeyEntry(nextAlias)) {
					if (defaultAlias == null
							|| defaultAlias.trim().length() == 0) {
						defaultAlias = nextAlias;
					}
					break;
				}

			}
		}

		return "OK";
	}


	/**
	 * Private helper that reads a keystore from storage.
	 */
	private KeyStore readKeyStore(char[] storePassword) throws Exception {
		if (keystoreFilename == null) {
			return null;
		}
		if (provider == null) {
			keystore = KeyStore.getInstance(keystoreType);
		} else {
			keystore = KeyStore.getInstance(keystoreType, provider);
		}
		File f = new File(keystoreFilename);
		keystore.load(new FileInputStream(f), storePassword);

		// make sure all aliases are in lower case
		Enumeration<String> aliases = keystore.aliases();
		while (aliases.hasMoreElements()) {
			String alias = aliases.nextElement();
			String lowerCase = alias.toLowerCase();
			if (!alias.equals(lowerCase)) {
				changeAlias(keystore, null, alias, lowerCase);
			}
		}
		return keystore;
	}

	/**
	 * Cache key entries and trusted certificates
	 */
	public void rehashKeyStoreEntries() throws Exception {
		cachedKeyEntries = new ArrayList<String>();
		cachedTrustedCertificates = new ArrayList<String>();
		rejectedEntries = new ArrayList<String>();
		Enumeration<String> knownAliases = keystore.aliases();
		while (knownAliases.hasMoreElements()) {
			String next = knownAliases.nextElement();
			if (keystore.isKeyEntry(next)) {
				if (!cachedKeyEntries.contains(next)) {
					try {
						if (validateKeyEntry(next, unwrapKeyPassword())) {
							cachedKeyEntries.add(next);
						}
					} catch (Exception e) {
						// warn user about wrong certs
						IdentityActivator.log(IStatus.WARNING,
								"Rejected certificate: " + next, e);
						rejectedEntries.add(next);
					}
				}
			} else if (keystore.isCertificateEntry(next)) {
				if (!cachedTrustedCertificates.contains(next)) {
					try {
						if (validateCertificateEntry(next)) {
							cachedTrustedCertificates.add(next);
						}
					} catch (Exception e) {
						// warn user about wrong certs
						rejectedEntries.add(next);
					}
				}
			}
		}
	}

	public void removeEntry(String alias) throws Exception {
		if (keystore.containsAlias(alias)) {
			if (keystore.isKeyEntry(alias)) {
				removeKeyEntry(alias);
			} else {
				removeTrustedCertificateEntry(alias);
			}
			rehashKeyStoreEntries();
		}
	}

	/**
	 * Removes a key entry from the KeyStore
	 * 
	 * @param alias
	 *            the alias of the key entry
	 * @exception KeyStoreException
	 *                Exception thrown
	 */
	public void removeKeyEntry(String alias) throws KeyStoreException {
		if (keystore.containsAlias(alias)) {
			keystore.deleteEntry(alias);
			cachedKeyEntries.remove(alias);
		}
	}

	/**
	 * Removes a trusted certificate from the keystore;
	 * 
	 * @param alias
	 *            Description of the Parameter
	 * @exception KeyStoreException
	 *                Description of the Exception
	 */
	public void removeTrustedCertificateEntry(String alias)
			throws KeyStoreException {
		if (keystore.containsAlias(alias)) {
			keystore.deleteEntry(alias);
			cachedTrustedCertificates.remove(alias);
		}
	}

	/**
	 * Replaces a certifcate associated with a key entry with a new certficate/
	 * This is done for example when importing a cert after sending a CSR
	 */
	public void replaceKeyCertificate(String alias, Certificate cert)
			throws Exception {
		if (keystore.isKeyEntry(alias)) {
			Certificate[] chain = { cert };
			addKeyEntry(
					(PrivateKey) keystore.getKey(alias, unwrapKeyPassword()),
					chain, alias);
		}
	}

	/**
	 * Writes a jks type keystore to permanent storage.
	 */
	public void reWriteKeyStore() throws Exception {
		if (keystoreFilename == null) {
			return;
		}
		File defaultFile = new File(keystoreFilename);
		reWriteKeyStore(defaultFile);
	}

	/**
	 * Writes a jks type keystore to permanent storage.
	 */
	public void reWriteKeyStore(File fileName) throws Exception {
		FileOutputStream fos = new FileOutputStream(fileName);
		try {
			keystore.store(fos, unwrapStorePassword());
		} finally {
			fos.close();
		}

	}

	/**
	 * Sets the defaultAlias attribute of the KeyStoreManager object
	 * 
	 * @param defaultAlias
	 *            The new defaultAlias value
	 */
	public void setDefaultAlias(String defaultAlias) throws Exception {
		this.defaultAlias = defaultAlias;
	}

	private void startPasswordEncryption(final char[] storePassword,
			final char[] keyPassword) {
		Thread thread = new Thread() {

			@Override
			public void run() {
				try {
					KeyGenerator generator = KeyGenerator.getInstance("DES");
					tempKey = generator.generateKey();
					passwdCipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
					passwdCipher.init(Cipher.ENCRYPT_MODE, tempKey);
					rawStore = passwdCipher.doFinal(new String(storePassword)
					.getBytes());
					rawKey = passwdCipher.doFinal(new String(keyPassword)
					.getBytes());
					IvParameterSpec dps = new IvParameterSpec(
							passwdCipher.getIV());
					passwdCipher.init(Cipher.DECRYPT_MODE, tempKey, dps);
					tempKey = null;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		thread.start();
	}

	private final char[] unwrapKeyPassword() {
		try {
			while (rawKey == null) {
				Thread.sleep(10);
			}
			return new String(passwdCipher.doFinal(rawKey)).toCharArray();
		} catch (Exception e) {
			return null;
		}
	}

	private final char[] unwrapStorePassword() {
		try {
			while (rawStore == null) {
				Thread.sleep(10);
			}
			return new String(passwdCipher.doFinal(rawStore)).toCharArray();
		} catch (Exception e) {
			return null;
		}
	}

	private boolean validateCertificateEntry(String alias) throws Exception {
		// try a trusted certificate
		X509Certificate trustedCertificate = (X509Certificate) keystore
				.getCertificate(alias.toLowerCase());
		trustedCertificate.checkValidity();
		return true;
	}

	/**
	 * Checks if the key entry specified by alias can be extrcated
	 * 
	 * @param alias
	 *            The alias to check.
	 * @param keyPassword
	 *            Description of the Parameter
	 * @return Return true if ok.
	 * @exception Exception
	 *                Various exceptions if key or password is invalid
	 */
	private boolean validateKeyEntry(String alias, char[] keyPassword)
			throws Exception {
		// can we extract the private key?
		keystore.getKey(alias, keyPassword);
		// .. and the certificate chain?
		Certificate[] chain = keystore.getCertificateChain(alias);
		for (int i = 0; i < chain.length; i++) {
			X509Certificate next = (X509Certificate) chain[i];
			Date validTo = next.getNotAfter();
			Date now = new Date(System.currentTimeMillis());
			if (now.after(validTo)) {
				throw new CertificateException("Certificate expired: "
						+ CertificateUtility.getCommonName(next));
			}
			// next.checkValidity();

		}
		return true;
	}

	/**
	 * Try to verify the user certificate (which is the first certificate in the
	 * chain) by a trusted certificate from the keystore;
	 * 
	 * @param chain
	 *            Description of the Parameter
	 */
	public boolean verify(X509Certificate[] chain) {
		// Test for orderer Subject/Issuer chain
		if (!CertificateUtility.checkOrder(chain)) {
			return false;
		}
		// Iterate over chain of certificates and try to find the issuer
		// of one of the certificates in the key store
		for (int i = 0; i < chain.length; i++) {
			// Currnet certificate
			X509Certificate cert = chain[i];
			// Look for issuer of current certificate in the key store
			Principal issuerDN = cert.getIssuerDN();
			X509Certificate issuerStored = null;
			try {
				issuerStored = getCertificateByPrincipal(issuerDN);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// If the issuer was found in the key store ...
			if (issuerStored != null) {
				X509Certificate issuer;
				// The next certificate in the chain should be identical
				// to the stored certificate
				if (i + 1 < chain.length) {
					issuer = chain[i + 1];
					if (!issuer.equals(issuerStored)) {
						return false;
					}
				}
				try {
					// Verify the current certificate by the stored certificate
					cert.verify(issuerStored.getPublicKey());
					// Now loop down the chain to verify the previous
					// certificate
					// by the current certificate, and so on
					for (int j = i - 1; j >= 0; j--) {
						issuer = cert;
						cert = chain[j];
						cert.verify(issuer.getPublicKey());
					}
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
				return true;
			}
		}
		return false;
	}

	private static KeyStore createTempStoreForPEMFile(File f,
			final String password) throws Exception {
		PEMReader reader = new PEMReader(new FileReader(f),
				new PasswordFinder() {

			public char[] getPassword() {
				if (password == null) {
					return new char[0];
				}
				return password.toCharArray();
			}
		});

		try {

			KeyStore result = KeyStore.getInstance("JKS");
			result.load(null, null);

			LinkedHashSet<KeyPair> keyPairs = new LinkedHashSet<KeyPair>();
			LinkedHashSet<Certificate> certs = new LinkedHashSet<Certificate>();

			Object obj;
			while ((obj = reader.readObject()) != null) {
				if (obj instanceof KeyPair) {
					keyPairs.add((KeyPair) obj);
				} else if (obj instanceof Certificate) {
					certs.add((Certificate) obj);
				}
			}

			// Add key pairs
			for (KeyPair keyPair : keyPairs) {
				Certificate keyPairCert = null;
				for (Iterator<Certificate> it = certs.iterator(); it.hasNext();) {
					Certificate cert = it.next();
					if (cert.getPublicKey().equals(keyPair.getPublic())) {
						keyPairCert = cert;
						it.remove();
						break;
					}
				}

				if (keyPairCert != null) {
					String alias = "keypair";
					if (keyPairCert instanceof X509Certificate) {
						alias = X509Utils.getCertificateAlias((X509Certificate) keyPairCert);
					}

					KeyStore.PrivateKeyEntry entry = new KeyStore.PrivateKeyEntry(
							keyPair.getPrivate(),
							new Certificate[] { keyPairCert });
					KeyStore.PasswordProtection prot = new KeyStore.PasswordProtection(
							password.toCharArray());

					alias = findUnusedAlias(result, alias);
					result.setEntry(alias, entry, prot);

				}
			}

			// Add remaining certificates as trusted certificate entries
			for (Certificate cert : certs) {
				String alias = "certificate";
				if (cert instanceof X509Certificate) {
					alias = X509Utils.getCertificateAlias((X509Certificate) cert);
				}

				KeyStore.TrustedCertificateEntry entry = new KeyStore.TrustedCertificateEntry(
						cert);

				result.setEntry(alias, entry, null);

			}

			return result;
		} catch (Exception e) {
			return null;
		} finally {
			reader.close();
		}

	}

	/**
	 * Find an unused alias in the keystore based on the given alias.
	 * 
	 * @param keyStore
	 *            the keystore
	 * @param alias
	 *            the alias
	 * @return alias that is not in use in the keystore
	 * @throws KeyStoreException
	 */
	public static String findUnusedAlias(KeyStore keyStore, String alias)
			throws KeyStoreException {
		if (keyStore.containsAlias(alias)) {
			int i = 1;
			while (true) {
				String nextAlias = alias + " (" + i + ")";
				if (!keyStore.containsAlias(nextAlias)) {
					alias = nextAlias;
					break;
				}
			}
		}
		return alias;
	}

	/**
	 * Gets an instance of the KeyStoreManger
	 * 
	 * @param provider
	 *            JCE provider
	 * @param type
	 *            KeyStore type
	 * @param name
	 *            keystore File name
	 * @return Instance of the keystore manager
	 */
	public static KeyStoreManager getInstance(File f, String passwd)
			throws Exception {
		return getInstance(f, passwd, "SUN", "JKS");
	}

	/**
	 * Gets an instance of the KeyStoreManger
	 * 
	 * @param provider
	 *            JCE provider
	 * @param type
	 *            KeyStore type
	 * @param name
	 *            keystore File name
	 * @return Instance of the keystore manager
	 */
	public static KeyStoreManager getInstance(File f, String passwd, String provider, String type)
			throws Exception {
		if(ksManagers.containsKey(f)) {
			return ksManagers.get(f);
		}
		
		KeyStoreManager km = null;
		char[] password = null;
		password = passwd.toCharArray();
		if (!f.exists()) {
			km = new KeyStoreManager(provider, type, f.getAbsolutePath());
			km.buildEmptyKeyStore(f.getAbsolutePath(), password, password);
			km.reWriteKeyStore();
		} else {

			try {
				km = new KeyStoreManager("SUN", "JKS", f.getAbsolutePath());
				km.loadKeyStore(password, password);
			} catch (Exception e1) {
				if (e1.getMessage().startsWith("Keystore")
						|| e1.getMessage().contains("password")) {
					// wrong password
					throw new Exception("Invalid password");
				}
				try {
					// try PKCS12 format
					km = new KeyStoreManager(null, "PKCS12",
							f.getAbsolutePath());
					km.loadKeyStore(password, password);
				} catch (Exception e2) {
					if (e2.getMessage().startsWith("Keystore")
							|| e2.getMessage().contains("failed to decrypt")) {
						// wrong password
						throw new Exception("Invalid password");
					}
					try {
						// try jceks format (VERY old)
						km = new KeyStoreManager("SunJCE", "JCEKS",
								f.getAbsolutePath());
						km.loadKeyStore(password, password);
					} catch (Exception e3) {
						if (e3.getMessage().startsWith("Keystore")) {
							//wrong password
							throw new Exception("Invalid password");
						}
						try {
							// try pem format using a temporary in-memory keystore
							// Caution! This results in a read-only keystore!

							KeyStore ks = createTempStoreForPEMFile(f, passwd);
							if (ks == null) {
								throw new Exception(
										"Unable to read keystore from file "
												+ f.getAbsolutePath());
							}
							return getInstance(ks, passwd);

						} catch (Exception e4) {

							throw new Exception(
									"Unable to read keystore from file "
											+ f.getAbsolutePath());
						}

					}
				}
			}
		}
		ksManagers.put(f, km);
		return km;
	}

	public static KeyStoreManager getInstance(KeyStore keystore, String password)
			throws Exception {
		KeyStoreManager km = new KeyStoreManager("SUN", "JKS", null);
		km.keystore = keystore;
		km.startPasswordEncryption(password.toCharArray(),
				password.toCharArray());
		km.rehashKeyStoreEntries();
		return km;
	}
}
