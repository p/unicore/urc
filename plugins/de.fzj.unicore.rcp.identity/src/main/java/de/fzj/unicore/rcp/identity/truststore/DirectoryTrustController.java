package de.fzj.unicore.rcp.identity.truststore;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;

import de.fzj.unicore.rcp.identity.CertificateUtility;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.KeyStoreManager;
import de.fzj.unicore.rcp.identity.TrustController;
import de.fzj.unicore.rcp.identity.X509Utils;
import de.fzj.unicore.rcp.identity.keystore.AliasValidator;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.ui.dialogs.CertificateDetailsDialog;
import de.fzj.unicore.rcp.identity.utils.InputUtils;
import eu.unicore.security.canl.TrustedIssuersProperties.TruststoreType;
import eu.unicore.security.canl.TruststoreProperties;

/**
 * used to perform actions on a truststore of type "directory" 
 * (which can be openssl)
 *  
 * @author schuller
 */
public class DirectoryTrustController implements TrustController {

	private final IdentityActivator parent;

	private final File directory;

	private final boolean isOpenSSL;

	public DirectoryTrustController(IdentityActivator parent, File directory, boolean isOpenSSL) throws Exception {
		this.parent = parent;
		this.directory = directory;
		this.isOpenSSL = isOpenSSL;
	}


	/**
	 * Removes a trusted certificate from the list of certificates and rewrites
	 * keystore.
	 * 
	 * @param cert
	 * @throws Exception
	 */
	public void removeTrustedCertificate(Certificate cert) throws Exception {
		cert.deleteUnderlyingFile();
	}

	public void removeTrustedCertificate(String name) throws Exception {
		for(Certificate cert: getAllCertificates()){
			if(name.equals(cert.getName())){
				cert.deleteUnderlyingFile();
				break;
			}
		}
	}

	/**
	 * Adds a trusted certificate to the list of certificates
	 * 
	 * @param cert
	 * @throws Exception
	 */
	public void addTrustedCertificate(Certificate cert) throws Exception {
		throw new Exception("This function is not supported for directory-based trust store!");
	}



	public void displayCertificateDetails(Certificate cert) throws Exception {
		String name = cert.getName();
		CertificateDetailsDialog d = new CertificateDetailsDialog(parent
				.getWorkbench().getActiveWorkbenchWindow().getShell(),
				"Certificate Details", null, cert, name);
		d.open();
	}

	/**
	 * Adds certificates from the specified keystore to the lists of the
	 * certificates and rewrites an existing keystore.
	 * 
	 * @param f
	 */
	public void importKeyStore(File f) throws Exception {
		if(isOpenSSL)throw new Exception("Not supported for OpenSSL truststore!");
		KeyStoreManager newKm = null;
		String passwd = null;
		passwd = new InputUtils().askPasswordSynch(f);
		if (passwd == null) {
			return;
		}
		newKm = KeyStoreManager.getInstance(f, passwd);
		if (newKm == null) {
			return;
		}

		importKeyStore(newKm, passwd, true);
	}


	public void importKeyStore(KeyStoreManager newKm, String passwd,
			boolean askForUniqueTruststoreAliases) throws Exception {

		if (passwd == null) {
			return;
		}
		if (newKm == null) {
			return;
		}
		if (!newKm.checkKeyStore(passwd.toCharArray())) {
			throw new Exception("The keystore to be imported contains errors");
		}

		List<Certificate>newTrustedCAs = CertificateUtility.getTrustedCertificates(newKm);

		boolean empty = true;

		Iterator<Certificate>iter = newTrustedCAs.iterator();
		while (iter.hasNext()) {
			Certificate cert = iter.next();
			// TODO check that we do not already have this trusted cert
			boolean add = true;
			File target =  new File(directory,cert.getName()+"-"+cert.getSubjectCN()+".pem");
			if(target.exists())continue;
			if(add){
				X509Utils.exportPublicKey(cert, target);
			}
		}

		if (empty) {
			IdentityActivator.log(IStatus.ERROR,
					"No certificates imported, the selected file was empty!");
		}
	}

	protected String getUniqueAlias(String base, Set<String> existingAliases) {
		String result = base;
		int i = 1;
		while (existingAliases.contains(result)) {
			result = base + i;
			i++;
		}
		return result;
	}


	public String askAlias(String oldAlias, Set<String> existingAliases)
			throws Exception {
		InputDialog d = new InputDialog(parent.getWorkbench().getActiveWorkbenchWindow().getShell(),
				"Alias already exists", "A key with the alias `" + oldAlias + 
				"' is already contained in the keystore. Please enter a different " +
				"alias or press cancel to skip this key.",
				oldAlias, new AliasValidator(existingAliases));

		d.open();
		if (d.getReturnCode() == Window.OK) {
			String newAlias = d.getValue();
			return newAlias;
		}
		return null;
	}

	@Override
	public void reWriteKeyStore() throws Exception {
	}

	@Override
	public void changeAlias(Certificate cert) throws Exception {
	}

	@Override
	public boolean isExistingName(String alias) {
		return false;
	}


	public Properties getTruststoreProperties(){
		Properties p = new Properties();
		if(isOpenSSL){
			p.setProperty("truststore."+TruststoreProperties.PROP_TYPE,TruststoreType.openssl.toString());
			p.setProperty("truststore."+TruststoreProperties.PROP_OPENSSL_DIR,
					directory.getAbsolutePath());
			//TODO
			p.setProperty("truststore."+TruststoreProperties.PROP_OPENSSL_NEW_STORE_FORMAT,"true");
			
			// there may be more options
		}
		else {
			p.setProperty("truststore."+TruststoreProperties.PROP_TYPE,TruststoreType.directory.toString());
			p.setProperty("truststore."+TruststoreProperties.PROP_DIRECTORY_LOCATIONS+"1",
					directory.getAbsolutePath()+"/*.pem");
			p.setProperty("truststore."+TruststoreProperties.PROP_DIRECTORY_LOCATIONS+"2",
					directory.getAbsolutePath()+"/*.cert");
			p.setProperty("truststore."
					+ TruststoreProperties.PROP_DIRECTORY_LOCATIONS + "3",
					"/*.crt");
			// there may be more options
		}

		p.setProperty("truststore."+TruststoreProperties.PROP_CRL_MODE,"IGNORE");

		return p;
	}


	@Override
	public List<Certificate> getAllCertificates() {
		List<Certificate> certs = new ArrayList<Certificate>();
		File[]files = directory.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				if(!isOpenSSL){
					return name.endsWith(".pem") || name.endsWith(".cert")
							|| name.endsWith(".crt");
				} else { // be strict and only accept hash.[0-9] files
					return Pattern.matches("^\\p{XDigit}{8}\\.[0-9]$", name);
				}
			}
		});
		for(File f: files){
			try{
				Certificate cert = new Certificate(f);
				cert.setName(f.getName());
				certs.add(cert);
			}catch(Exception ex){
				// probably not a certificate!
			}
		}
		return certs;
	}


	@Override
	public void addTruststoreChangedListener(
			TruststoreChangedListener truststoreListener) {
		// Nothing to do at the moment, as no changes can be triggered.

	}


}
