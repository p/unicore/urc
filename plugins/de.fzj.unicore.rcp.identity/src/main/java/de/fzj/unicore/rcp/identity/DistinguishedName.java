package de.fzj.unicore.rcp.identity;

import java.util.StringTokenizer;

import de.fzj.unicore.rcp.common.utils.ArrayUtils;

public class DistinguishedName {
	/**
	 * The common name(s)
	 */
	public String[] CN = new String[0];

	/**
	 * User's email address(es)
	 */
	public String[] EMAILADDRESS = new String[0];

	/**
	 * Organization(s) the user works for
	 */
	public String[] O = new String[0];

	/**
	 * Organization unit(s) the user works in
	 */
	public String[] OU = new String[0];

	/**
	 * Countries the user lives in
	 */
	public String[] C = new String[0];

	/**
	 * State(s) the user lives in
	 */
	public String[] ST = new String[0];

	/**
	 * Locality
	 */
	public String[] L = new String[0];

	public DistinguishedName() {

	}

	public DistinguishedName(String cN, String eMAILADDRESS, String o,
			String oU, String c, String sT, String l) {
		super();
		CN = new String[]{cN};
		EMAILADDRESS = new String[]{eMAILADDRESS};
		O = new String[]{o};
		OU = new String[]{oU};
		C = new String[]{c};
		ST = new String[]{sT};
		L = new String[]{l};
	}

	public String getDNString() {
		StringBuffer sb = new StringBuffer();

		for(String cn : CN)
		{
			if (cn != null && cn.length() != 0) {
				sb.append(",CN=").append(cn);
			}
		}

		for(String email : EMAILADDRESS)
		{
			if (email != null && email.length() != 0) {
				sb.append(",EMAILADDRESS=").append(email);
			}
		}
		for(String o : O)
		{
			if (o != null && o.length() != 0) {
				sb.append(",O=").append(o);
			}
		}
		for(String ou : OU)
		{
			if (ou != null && ou.length() != 0) {
				sb.append(",OU=").append(ou);
			}
		}

		for(String l : L)
		{
			if (l != null && l.length() != 0) {
				sb.append(",L=").append(l);
			}
		}
		for(String st : ST)
		{
			if (st != null && st.length() != 0) {
				sb.append(",ST=").append(st);
			}
		}
		for(String c : C)
		{
			if (c != null && c.length() != 0) {
				sb.append(",C=").append(c);
			}
		}
		return sb.toString();
	}

	public String getNiceString(String separator) {
		StringBuffer sb = new StringBuffer();

		if(CN.length > 0)
		{
			if(CN.length > 1) sb.append("Names");
			else sb.append("Name");
			sb.append(": ");
			for(String cn : CN)
			{
				if (cn != null && cn.length() != 0) {
					sb.append(cn).append(", ");
				}
			}
			sb.delete(sb.length()-2, sb.length());
		}

		if(EMAILADDRESS.length > 0)
		{
			sb.append(separator);
			if(EMAILADDRESS.length > 1) sb.append("Email addresses");
			else sb.append("Email address");
			sb.append(": ");
			for(String email : EMAILADDRESS)
			{
				if (email != null && email.length() != 0) {
					sb.append(email).append(", ");
				}
			}
			sb.delete(sb.length()-2, sb.length());
		}

		if(O.length > 0)
		{
			sb.append(separator);
			if(O.length > 1) sb.append("Organisations");
			else sb.append("Organisation");
			sb.append(": ");
			for(String o : O)
			{
				if (o != null && o.length() != 0) {
					sb.append(o).append(", ");
				}
			}
			sb.delete(sb.length()-2, sb.length());
		}

		if(OU.length > 0)
		{
			sb.append(separator);
			if(OU.length > 1) sb.append("Organisation Units");
			else sb.append("Organisation Unit");
			
			sb.append(": ");
			for(String ou : OU)
			{
				if (ou != null && ou.length() != 0) {
					sb.append(ou).append(", ");
				}
			}
			sb.delete(sb.length()-2, sb.length());
		}

		if(L.length > 0)
		{
			sb.append(separator);
			if(L.length > 1) sb.append("Localities");
			else sb.append("Locality");
			sb.append(": ");
			for(String l : L)
			{
				if (l != null && l.length() != 0) {
					sb.append(l).append(", ");
				}
			}
			sb.delete(sb.length()-2, sb.length());
		}

		if(ST.length > 0)
		{
			sb.append(separator);
			if(ST.length > 1) sb.append("States");
			else sb.append("State");
			sb.append(": ");
			
			for(String st : ST)
			{
				if (st != null && st.length() != 0) {
					sb.append(st).append(", ");
				}
			}
			sb.delete(sb.length()-2, sb.length());
		}

		if(C.length > 0)
		{
			sb.append(separator);
			if(C.length > 1) sb.append("Countries");
			else sb.append("Country");
			sb.append(": ");
			for(String c : C)
			{
				if (c != null && c.length() != 0) {
					sb.append(c).append(", ");
				}
			}
			sb.delete(sb.length()-2, sb.length());
		}


		return sb.toString();
	}

	public static DistinguishedName fromString(String dn) {
		if (dn == null) {
			return null;
		}
		String trimmeddn = dn.trim();
		StringTokenizer st = new StringTokenizer(trimmeddn, ",=");
		DistinguishedName result = new DistinguishedName();
		String o = null;
		while (st.hasMoreTokens()) {
			o = st.nextToken();
			if (o.trim().equalsIgnoreCase("C")) {
				result.C = ArrayUtils.add(result.C, st.nextToken());
			} else if (o.trim().equalsIgnoreCase("ST")) {
				result.ST = ArrayUtils.add(result.ST, st.nextToken());
			} else if (o.trim().equalsIgnoreCase("L")) {
				result.L = ArrayUtils.add(result.L, st.nextToken());
			} else if (o.trim().equalsIgnoreCase("O")) {
				result.O = ArrayUtils.add(result.O, st.nextToken());
			} else if (o.trim().equalsIgnoreCase("OU")) {
				result.OU = ArrayUtils.add(result.OU, st.nextToken());
			} else if (o.trim().equalsIgnoreCase("CN")) {
				result.CN = ArrayUtils.add(result.CN, st.nextToken());
			} else if (o.trim().equalsIgnoreCase("EmailAddress")) {
				result.EMAILADDRESS = ArrayUtils.add(result.EMAILADDRESS, st.nextToken());
			} else {
				st.nextToken(); // ignore unknown tokens
			}
		}
		return result;
	}

}
