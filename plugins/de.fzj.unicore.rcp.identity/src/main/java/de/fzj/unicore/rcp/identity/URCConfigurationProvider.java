package de.fzj.unicore.rcp.identity;

import java.util.Map;

import de.fzj.unicore.uas.security.WSRFClientConfigurationProviderImpl;
import eu.unicore.security.wsutil.client.authn.AuthenticationProvider;
import eu.unicore.security.wsutil.client.authn.SecuritySessionPersistence;
import eu.unicore.security.wsutil.client.authn.ServiceIdentityResolver;

public class URCConfigurationProvider extends WSRFClientConfigurationProviderImpl {

	public URCConfigurationProvider() {
		super();
	}

	public URCConfigurationProvider(AuthenticationProvider authnProvider,
			SecuritySessionPersistence sessionsPersistence,
			ServiceIdentityResolver identityResolver,
			Map<String, String[]> securityPreferences) throws Exception {
		super(authnProvider, sessionsPersistence, identityResolver, securityPreferences);
	}

}
