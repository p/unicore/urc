/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.identity.extensions;

import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.keystore.CertificateList;
import de.fzj.unicore.rcp.identity.keystore.KeystoreCredentialController;
import de.fzj.unicore.rcp.identity.profiles.Profile;

/**
 * provides a CellEditor for selecting a certificate
 * 
 * @author demuth
 * 
 */
public class CertificateCellEditor extends ComboBoxCellEditor {

	Profile profile;

	/**
	 * 
	 * @param parent
	 *            is the table on which the cell editor should work
	 * @param certNames
	 *            are the selectable strings for the comboBox
	 */
	public CertificateCellEditor(Composite parent, String[] certNames) {
		super(parent, certNames, SWT.READ_ONLY);
	}

	/**
	 * returns an object of type Profile
	 */
	@Override
	protected Object doGetValue() {
		Integer selection = (Integer) super.doGetValue();
		Certificate cert = getCertificateList()
				.getCertByIndex(selection);
		CredentialTypeCertificateExtension.setCertificate(profile, cert);
		CredentialTypeCertificateExtension.setCertificateIndex(profile,
				selection);
		return profile;
	}

	/**
	 * takes an object of type Profile
	 */
	@Override
	protected void doSetValue(Object value) {
		profile = (Profile) value;
		int index = CredentialTypeCertificateExtension
				.getCertificateIndex(profile);
		if (index >= 0) {
			super.doSetValue(index);
		}
	}

	private CertificateList getCertificateList(){
		return ((KeystoreCredentialController)IdentityActivator.getDefault().getCredential())
				.getCertificateList();
	}

}
