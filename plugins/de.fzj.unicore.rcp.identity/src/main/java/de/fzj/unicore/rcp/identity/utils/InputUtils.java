package de.fzj.unicore.rcp.identity.utils;

import java.io.File;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.common.guicomponents.HiddenInputDialog;
import de.fzj.unicore.rcp.identity.KeyStoreManager;

public class InputUtils {

	private String password;
	
	private Exception thrownException = null;

	
	private void runDialog(final File keystore, Shell shell) {
		try {
			String title = "Password";
			String question = "Please enter the password for your keystore:\n"
					+ keystore.getAbsolutePath();
			password = null;
			int i = 3;
			while (password == null && i > 0) {
				HiddenInputDialog id = new HiddenInputDialog(shell,
						title, question, "", null);
				id.open();
				if (id.getReturnCode() != Window.OK) {
					return;
				}
				password = id.getValue();
				if (!keystore.exists()) {
					id = new HiddenInputDialog(shell, title,
							"Please confirm the password:", "", null);
					id.open();
					if (id.getReturnCode() != Window.OK
							|| !password.equals(id.getValue())) {
						question = "You entered a different password, please try again.";
						password = null;
						i = 3;
					}
				} else {
					boolean correct = passwordCorrect(password, keystore);

					if (!correct) {
						question = "Wrong or empty password, please try again.";
						password = null;
						i--;
					}
				}
			}
		} catch (Exception e) {
			thrownException = e;
		}
	}
	
	/**
	 * run a password input dialog on the GUI thread
	 * 
	 * @param keystore
	 * @return
	 * @throws Exception
	 */
	public String askPasswordSynch(final File keystore) throws Exception {
		return askPasswordSynch(keystore, Display.getCurrent().getActiveShell());
	}
	
	public String askPasswordSynch(final File keystore, final Shell shell) throws Exception {
		if (passwordCorrect("", keystore)) {
			return "";
		}
		Runnable r = new Runnable(){
			public void run(){
				runDialog(keystore, shell);
			}
		};
		PlatformUI.getWorkbench().getDisplay().syncExec(r);
		if (thrownException != null) {
			Exception e = new Exception("Could not load keystore: "
					+ thrownException.getMessage(), thrownException);
			thrownException = null;
			throw e;
		} else if (password == null) {
			throw new Exception(
					"Cancel pressed or wrong password entered three times");
		} else {
			return password;
		}
	}
	
	public String askPassword(final File keystore) throws Exception {
		return askPassword(keystore, Display.getCurrent().getActiveShell());
	}
	
	/**
	 * This method tries to determine the password for the given keystore file.
	 * It returns null if the user fails to provide a valid password. It returns
	 * an empty String ("") if a password does not seem to be necessary for
	 * opening the keystore file.
	 * 
	 * @param keystore
	 * @return
	 * @throws Exception
	 */
	public String askPassword(final File keystore, final Shell shell) throws Exception {
		if (passwordCorrect("", keystore)) {
			return "";
		}
		runDialog(keystore, shell);
		if (thrownException != null) {
			Exception e = new Exception("Could not load keystore: "
					+ thrownException.getMessage(), thrownException);
			thrownException = null;
			throw e;
		} else if (password == null) {
			throw new Exception(
					"Cancel pressed or wrong password entered three times");
		} else {
			return password;
		}
	}

	/**
	 * Test if password is correct by opening keystore file
	 * 
	 * @param passwd
	 * @param keystoreFile
	 * @return
	 */
	public static boolean passwordCorrect(String passwd, File keystoreFile)
			throws Exception {
		if (!keystoreFile.exists()) {
			return false;
		}
		if (passwd == null) {
			return false;
		}
		try {
			KeyStoreManager.getInstance(keystoreFile, passwd);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
