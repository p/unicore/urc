/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.identity.truststore;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.IdentityConstants;
import de.fzj.unicore.rcp.logmonitor.utils.FileDialogUtils;

public class CertFileCellEditor extends DialogCellEditor {

	/** A CellEditor, opening a FileBrowser for selecting a pem File */
	public CertFileCellEditor(Composite parent) {
		super(parent);
	}

	protected Object openDialogBox() {
		// Create file selection dialog
		
		FileDialog dialog = new FileDialog(getControl().getShell(), SWT.OPEN);
		String filterPath = FileDialogUtils.getFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS);
		dialog.setFilterPath(filterPath);
		dialog.setFileName((String) getValue());
		dialog.setFilterExtensions(Constants.PUBLIC_KEY_FORMATS);
		dialog.setText("Please open a certificate to import...");
		String filename = dialog.open();
		FileDialogUtils.saveFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS, dialog.getFilterPath());
		// Indicate if file name is valid
		IdentityActivator.log(IStatus.INFO, "File selected was:" + filename);
		setValueValid(filename != null);
		return filename;
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		// Create file selection dialog
		FileDialog dialog = new FileDialog(cellEditorWindow.getShell(),
				SWT.OPEN);
		String filterPath = FileDialogUtils.getFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS);
		dialog.setFilterPath(filterPath);
		dialog.setFileName((String) getValue());
		dialog.setFilterExtensions(Constants.PUBLIC_KEY_FORMATS);
		dialog.setText("Please open a certificate to import...");
		String filename = dialog.open();
		FileDialogUtils.saveFilterPath(IdentityConstants.FILE_DIALOG_ID_CREDENTIALS, dialog.getFilterPath());
		// Indicate if file name is valid
		IdentityActivator.log(IStatus.INFO, "File selected was:" + filename);
		setValueValid(filename != null);
		return filename;
	}

}
