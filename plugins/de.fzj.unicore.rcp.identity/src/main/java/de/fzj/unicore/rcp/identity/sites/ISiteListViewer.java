package de.fzj.unicore.rcp.identity.sites;

public interface ISiteListViewer {

	/**
	 * Update the view to reflect the fact that a site was added to the site
	 * list
	 * 
	 * @param site
	 */
	public void addSite(Site site);

	/**
	 * Update the view to reflect the fact that a site was removed from the site
	 * list
	 * 
	 * @param site
	 */
	public void removeSite(Site site);

	/**
	 * Update the view to reflect the fact that one of the sites was modified
	 * 
	 * @param site
	 */
	public void updateSite(Site site);
}
