package de.fzj.unicore.rcp.cis;

import java.net.URI;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.swt.graphics.Image;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.cis.nodes.CISNode;
import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.Util;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.uas.util.AddressingUtil;

public class CISNodeExtensionPointImpl implements INodeExtensionPoint {

	private Image cisImage = ServiceBrowserActivator.getImageDescriptor(
			"sample.gif").createImage();
	protected String cisNodeType = CISConstants.SERVICENAME;

	public void dispose() {
		cisImage.dispose();
	}

	public Image getImage(Node node) {
		if (node instanceof CISNode) {
			return cisImage;
		} else
			return null;
	}

	public Node getNode(EndpointReferenceType epr) {
		if (epr == null)
			return null;

		QName type = Utils.InterfaceNameFromEPR(epr);
		if (type == null)
			return null;
		if (CISConstants.REGISTRY_PORT.equals(type)) {
			return new CISNode(epr);
		} else
			return null;
	}

	public Node getNode(URI uri) {
		if (uri == null)
			return null;

		String type = Util.determineTypeOfURI(uri);
		EndpointReferenceType epr = AddressingUtil.newEPR();
		epr.addNewAddress().setStringValue(uri.toString());
		if (cisNodeType.equals(type)) {
			AddressingUtil.addPortType(epr, CISConstants.REGISTRY_PORT);
		} else
			return null;
		return getNode(epr);
	}

	public String getText(Node o) {
		return null;
	}

	public void setInitializationData(IConfigurationElement arg0, String arg1,
			Object arg2) throws CoreException {

	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		dispose();
	}

}
