/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.cis.actions;

import java.net.URL;

import org.eclipse.core.runtime.Status;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.browser.WorkbenchBrowserSupport;

import de.fzj.unicore.rcp.cis.CISActivator;
import de.fzj.unicore.rcp.cis.nodes.CISNode;
import de.fzj.unicore.rcp.logmonitor.LogActivator;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import eu.unicore.unicore6.cis.model.CISPropertiesDocument;
import eu.unicore.unicore6.cis.model.CISPropertiesDocument.CISProperties;

/**
 * @author demuth
 *
 */
@SuppressWarnings("restriction")
public class OpenCISInBrowserAction extends NodeAction {

	private static final String browserId = "BROWSER";

	public OpenCISInBrowserAction(Node node)
	{
		super(node);
		setText("Show Grid");
		setToolTipText("Opens a browser and displays an overview of the Grid.");
		setImageDescriptor(CISActivator.getImageDescriptor("show_grid.png"));
	}
	@Override
	public void run()
	{

		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {

		
			public void run() {
				try {
					URL u = getCISURL();
					if(u == null) 
					{
						LogActivator.log(Status.ERROR,"Unable to open Grid overview in Web browser: could not extract correct URL");
						return;
					}

					WorkbenchBrowserSupport.getInstance().createBrowser(browserId).openURL(u);
				} catch (Exception e) {
					LogActivator.log(Status.ERROR,"Unable to open Grid overview in Web browser: "+e.getMessage(),e);
				} 
			}

		});
	}

	protected URL getCISURL()
	{
		Node node = getNode();
		if(!(node instanceof CISNode)) return null;
		CISNode n = (CISNode) node; 
		CISPropertiesDocument doc = n.getCISRPDocument();
		if(doc != null)
		{
			CISProperties props = doc.getCISProperties();
			if(props != null)
			{

				String s = props.getWebURL();
				if(s != null && s.trim().length() > 0)
				{
					try {
						return new URL(s);
					} catch (Exception e) {
						LogActivator.log(Status.ERROR,"Information service published an invalid URL: "+e.getMessage(),e);
						return null;
					}
				}
			}
		}
		return null;
	}

	public boolean isCurrentlyAvailable() {
		return getCISURL() != null;
	}


}
