package de.fzj.unicore.rcp.cis;

import javax.xml.namespace.QName;

public interface CISConstants {

	String SERVICENAME = "CISRegistry"; 
	
	public static final String REGISTRY_NS = "http://www.unicore.eu/unicore6/cis";
	public static final QName REGISTRY_PORT = new QName(REGISTRY_NS, "CISRegistryPortType");
}
