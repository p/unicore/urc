package de.fzj.unicore.rcp.cis;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;

import de.fzj.unicore.rcp.cis.actions.OpenCISInBrowserAction;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeActionExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

public class CISNodeActionExtensionPointImpl implements INodeActionExtensionPoint{

	public void addActions(Node node, IServiceViewer viewer) {
		
		if (node instanceof RegistryNode) {
			Set<String> set = new HashSet<String>();
			set.add(CISConstants.SERVICENAME);
			List<Node> lstNode = ((RegistryNode) node).getChildrenOfTypes(set);
			try {
				if (lstNode.size() > 0) {
					node.getAvailableActions().add(new OpenCISInBrowserAction(lstNode.get(0)));	
				}	
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		}
		
	}
	
	
	public void setInitializationData(IConfigurationElement arg0, String arg1,
			Object arg2) throws CoreException {
		//do nothing
	}

}
