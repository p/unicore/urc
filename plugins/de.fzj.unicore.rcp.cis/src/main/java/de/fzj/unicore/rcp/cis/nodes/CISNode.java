package de.fzj.unicore.rcp.cis.nodes;

import org.apache.xmlbeans.SchemaType;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.cis.CISConstants;
import de.fzj.unicore.rcp.cis.actions.OpenCISInBrowserAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;
import eu.unicore.unicore6.cis.model.CISPropertiesDocument;

public class CISNode extends WSRFNode {
	private static final long serialVersionUID = 1L;
	
	public CISNode(EndpointReferenceType epr) {
		super(epr);
	}

	@Override
	public void init() {
		super.init();
	}

	@Override
	public String getType() {
		return CISConstants.SERVICENAME;
	}

	@Override
	public SchemaType getSchemaType() {
		return CISPropertiesDocument.type;
	}

	@Override
	public IStatus doubleClick(IProgressMonitor monitor) {

		new OpenCISInBrowserAction(this).run();
		return super.doubleClick(monitor);
	}

	@Override
	public void retrieveName() {
		setName("InformationService");
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		availableActions.add(new OpenCISInBrowserAction(this));
	}	

	public CISPropertiesDocument getCISRPDocument() {
		return (CISPropertiesDocument)getCachedResourcePropertyDocument();
	}

}
