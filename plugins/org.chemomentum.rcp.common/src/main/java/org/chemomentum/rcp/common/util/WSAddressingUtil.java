/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.common.util;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlString;
import org.chemomentum.rcp.common.Activator;
import org.eclipse.core.runtime.IStatus;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

/**
 * @author demuth
 * 
 */
public class WSAddressingUtil {

	/**
	 * @deprecated This is only used for backwards compatibility. Don't use or
	 *             touch it!
	 * @param eprString
	 * @return
	 */
	@Deprecated
	public static String extractLogicalNameFromEprString(String eprString) {
		EndpointReferenceType epr;
		try {
			epr = EndpointReferenceType.Factory.parse(eprString);
			XmlString refParam = XmlString.Factory.parse(epr
					.getReferenceParameters().xmlText());
			return refParam.getStringValue();
		} catch (XmlException e) {
			Activator
					.log(IStatus.ERROR,
							"Could not extract logical Filename from endpoint reference",
							e);
			return null;
		}
	}

}
