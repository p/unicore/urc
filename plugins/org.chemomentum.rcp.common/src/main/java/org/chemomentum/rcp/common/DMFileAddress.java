/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.common;

import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.intel.gpe.clients.common.transfers.GridFileAddress;
import com.intel.gpe.util.collections.CollectionUtil;

/**
 * @author demuth
 * 
 */
public class DMFileAddress extends GridFileAddress {

	private String logicalName;
	private URI registryURI;

	DMFileAddress() {
		super("", "");
	}

	public DMFileAddress(String displayed) {
		super("");
		setDisplayedString(displayed);
		setProtocol(C9mRCPCommonConstants.DMFILE_PROTOCOL);
	}

	@Override
	public DMFileAddress clone() {
		DMFileAddress clone = new DMFileAddress();
		copyTo(clone);
		clone.setRegistryURI(getRegistryURI());
		clone.setLogicalName(getLogicalName());
		return clone;
	}

	@Override
	public boolean equals(Object o) {
		if (!(getClass().equals(o.getClass()))) {
			return false;
		}
		DMFileAddress other = (DMFileAddress) o;
		return super.equals(o)
				&& CollectionUtil.equalOrBothNull(getRegistryURI(),
						other.getRegistryURI())
				&& CollectionUtil.equalOrBothNull(getLogicalName(),
						other.getLogicalName());
	}

	public String getLogicalName() {
		return logicalName;
	}

	public URI getRegistryURI() {
		return registryURI;
	}

	public void setLogicalName(String logicalName) {
		this.logicalName = logicalName;
		if (logicalName != null && logicalName.trim().length() > 0) {
			Pattern p = C9mRCPCommonConstants.LOGICAL_NAME_SPLIT_PATTERN;
			Matcher m = p.matcher(logicalName);
			if (m.matches()) {
				setParentDirAddress(m.group(1));
				setRelativePath(m.group(2));
			}
		}
	}

	public void setRegistryURI(URI registryURI) {
		this.registryURI = registryURI;
	}

}
