package org.chemomentum.rcp.common;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.chemomentum.rcp.common";

	public static final String ICONS_PATH = "$NL$/icons/";

	// The shared instance
	private static Activator plugin;

	/**
	 * The constructor
	 */
	public Activator() {
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH + path);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param alarmUser
	 *            true means popup should alert user
	 */
	public static void log(int status, String msg, boolean alarmUser) {
		log(status, msg, null, alarmUser);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 * @param alarmUser
	 *            true means popup should alert user
	 */
	public static void log(int status, String msg, Exception e,
			boolean alarmUser) {
		IStatus s = new Status(status, PLUGIN_ID, IStatus.OK, msg, e);
		// log it
		plugin.getLog().log(s);
		// if user should be alarmed, quit here
		if (!alarmUser) {
			return;
		}
		// alarm the user depending on severity
		if (s.getSeverity() == IStatus.ERROR) {
			ErrorDialog.openError(plugin.getWorkbench().getDisplay()
					.getActiveShell(), "An error occured....", msg, s);
		} else if (s.getSeverity() == IStatus.WARNING) {
			MessageDialog.openInformation(plugin.getWorkbench().getDisplay()
					.getActiveShell(), "Warning", msg);
		} else if (s.getSeverity() == IStatus.INFO) {
			MessageDialog.openInformation(plugin.getWorkbench().getDisplay()
					.getActiveShell(), "Information", msg);
		} else { // unknown type
			MessageDialog.openInformation(plugin.getWorkbench().getDisplay()
					.getActiveShell(), "Message without known Severity level",
					msg);
		}

	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 */
	public static void log(String msg) {
		log(msg, null);
	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(String msg, Exception e) {
		log(IStatus.INFO, msg, e, false);
	}

}
