/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.common;

import java.util.regex.Pattern;

import javax.xml.namespace.QName;

import com.intel.gpe.clients.api.transfers.IProtocol;
import com.intel.gpe.clients.api.transfers.Protocol;

/**
 * @author demuth
 * 
 */
public class C9mRCPCommonConstants {
	public static final QName C9M_WORKFLOW_SYSTEM_TYPE = new QName(
			"org.chemomentum", "WorkflowSystem");

	public static QName C9M_DEFAULT_WF_DIALECT = new QName(
			"http://www.chemomentum.org/workflow/simple", "");

	public static final QName C9M_QNAME = new QName(
			"http://www.chemomentum.org/", "C9M");

	// constants used to identify files that are transferred within a workflow
	public static final QName WFFILE_QNAME = new QName(
			"http://chemomentum.org/", "Workflow_File");
	public static final String WFFILE_PREFIX = "c9mwf";
	public static final IProtocol WFFILE_PROTOCOL = new Protocol(WFFILE_QNAME,
			WFFILE_PREFIX);

	// constants used to identify files that are accessible via c9m data
	// management
	public static final QName DMFILE_QNAME = new QName(
			"http://www.chemomentum.org/", "Global_file");
	public static final String DMFILE_PREFIX = "c9m";
	public static final IProtocol DMFILE_PROTOCOL = new Protocol(DMFILE_QNAME,
			DMFILE_PREFIX);

	// constants used to identify Grid files that are located on some UNICORE
	// storage and registered with a location manager
	public static final QName LMFILE_QNAME = new QName(
			"http://www.unicore.eu/", "Logical_filename");
	public static final String LMFILE_PREFIX = "lm";
	public static final IProtocol LMFILE_PROTOCOL = new Protocol(LMFILE_QNAME,
			LMFILE_PREFIX);

	// constants used to identify variables that are defined read and modified
	// within a workflow
	public static final QName WFVARIABLE_QNAME = new QName(
			"http://chemomentum.org/", "Workflow_Variable");
	public static final String WFVARIABLE_PREFIX = "context";
	public static final IProtocol WFVARIABLE_PROTOCOL = new Protocol(
			WFVARIABLE_QNAME, WFVARIABLE_PREFIX);

	public static final QName DOMAIN_ALL = new QName(
			"http://www.chemomentum.org/", "AllDomains");
	public static final QName DOMAIN_QUANTUM_CHEMISTRY = new QName(
			"http://www.chemomentum.org/", "QuantumChemistry");

	private static final String SPLITTER_REGEXP = "([^*]*/)(.*)";
	/**
	 * This can be used for splitting Chemomentum logical names in two parts: A
	 * parent part that must not contain wildcards and a relative part that
	 * might contain wildcards.
	 */
	public static final Pattern LOGICAL_NAME_SPLIT_PATTERN = Pattern
			.compile(SPLITTER_REGEXP);
}
