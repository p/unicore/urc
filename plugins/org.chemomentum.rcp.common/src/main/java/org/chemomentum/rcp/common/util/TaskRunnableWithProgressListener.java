/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.common.util;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.chemomentum.common.api.IInfo;
import org.chemomentum.common.api.IResultReceiver;
import org.chemomentum.common.api.ITask;
import org.chemomentum.rcp.common.Activator;
import org.eclipse.core.runtime.IStatus;

import com.intel.gpe.clients.api.async.IProgressListener;

/**
 * @author demuth
 * 
 */
public class TaskRunnableWithProgressListener {
	ITask<Object,IInfo> task = null;

	private boolean successful = false;
	private boolean failed = false;

	

	@SuppressWarnings("unchecked")
	public TaskRunnableWithProgressListener(ITask<?,? extends IInfo> task) {
		this.task = (ITask<Object,IInfo>) task;
	}

	public boolean isFailed() {
		return failed;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public void run(final IProgressListener listener)
			throws InvocationTargetException, InterruptedException {

		int totalWork = IProgressListener.UNKNOWN;
		if (task.returnsProgress()) {
			totalWork = 100;
		}

		listener.beginTask(task.getDescription(), totalWork);
		try {
			task.addResultReceiver(new IResultReceiver<Object, IInfo>() {
				public void error(Throwable arg0, IInfo arg1) {
					Activator.log(
							IStatus.WARNING,
							"An erorr occurred while executing task \""
									+ task.getDescription()
									+ "\". Cancelling task.", new Exception(
									arg0));
					listener.setCanceled(true);
					failed = true;
				}

				public void putResult(Object arg0, IInfo arg1) {
					listener.done();
				}

				public void update(IInfo arg0) {
					listener.worked(1);
				}

				public void update(IInfo arg0, int work) {
					listener.worked(work);
				}
			});

			Collection<Runnable> threads = task.getJobs();
			for (Runnable runnable : threads) {
				runnable.run();
			}
			successful = true;
		} finally {
			listener.done();
		}

	}
}
