package de.fzj.unicore.rcp.standalone.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;

import de.fzj.unicore.rcp.identity.keystore.KeystoreView;
import de.fzj.unicore.rcp.identity.sites.SiteListView;
import de.fzj.unicore.rcp.identity.truststore.TruststoreView;
import de.fzj.unicore.rcp.standalone.Activator;
import de.fzj.unicore.rcp.standalone.ui.ICommandIds;


public class ConfigureSecurityAction extends Action {
	
	private final IWorkbenchWindow window;
	
	public ConfigureSecurityAction(IWorkbenchWindow window) {
		this.window = window;
        setText("Security Configuration");
        setToolTipText("Edit your key/truststore and manage certificates.");
        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_CONFIGURE_SECURITY);
        // Associate the action with a pre-defined command, to allow key bindings.
//		setActionDefinitionId(ICommandIds.CMD_CONFIGURE_SECURITY);
		setImageDescriptor(Activator.getImageDescriptor("/icons/sample2.gif"));
	}
	
	public void run() {
		if(window != null) {	
			try {
				window.getActivePage().showView(KeystoreView.ID);
				window.getActivePage().showView(TruststoreView.ID);
				window.getActivePage().showView(SiteListView.ID);
			} catch (PartInitException e) {
				MessageDialog.openError(window.getShell(), "Error", "Error opening view:" + e.getMessage());
			}
		}
	}
}
