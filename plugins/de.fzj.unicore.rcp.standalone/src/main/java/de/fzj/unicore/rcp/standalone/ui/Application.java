package de.fzj.unicore.rcp.standalone.ui;

import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.WorkbenchPlugin;

/**
 * This class controls all aspects of the application's execution
 */
public class Application implements IApplication{


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) throws Exception {
		Display display = PlatformUI.createDisplay();
		try {
			ApplicationWorkbenchAdvisor appAdvisor = new ApplicationWorkbenchAdvisor();
			Location instanceLoc = Platform.getInstanceLocation();
			if(!instanceLoc.isSet())
			{
				// we got no proper workspace!
				WorkbenchPlugin.unsetSplashShell(display);
				context.applicationRunning();
				return EXIT_OK;
			}
			
			IDE.registerAdapters();
			int returnCode = PlatformUI.createAndRunWorkbench(display, appAdvisor);
			if (returnCode == PlatformUI.RETURN_RESTART) {
				return EXIT_RESTART;
			}
			return EXIT_OK;
		} finally {
			display.dispose();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null)
			return;
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run() {
				if (!display.isDisposed())
					workbench.close();
			}
		});
	}

}
