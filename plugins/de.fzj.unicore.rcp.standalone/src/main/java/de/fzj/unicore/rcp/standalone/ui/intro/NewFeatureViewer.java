/*******************************************************************************
 * Copyright (c) 2006, 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.standalone.ui.intro;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.intro.config.IIntroContentProvider;
import org.eclipse.ui.intro.config.IIntroContentProviderSite;
import org.eclipse.ui.intro.config.IIntroURL;
import org.eclipse.ui.intro.config.IntroURLFactory;

import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.standalone.Activator;

public class NewFeatureViewer implements IIntroContentProvider {
	private static final IPath FILE_PATH = new Path("intro/content/newfeatures"); //$NON-NLS-1$

	private static final String MSG_LOADING = "New Features Viewer loading";

	private static final String HREF_BULLET = "bullet"; //$NON-NLS-1$

	private IIntroContentProviderSite site;

	private boolean disposed;

	private String id;

	private FormToolkit toolkit;

	private Composite parent;

	private Image bulletImage;

	private List<String> items;

	private FormText formText;

	private String changelogUrl;

	class FeatureReader implements Runnable {
		public void run() {
			// important: don't do the work if the
			// part gets disposed in the process
			if (disposed)
				return;
			readFeatures();
			if (disposed)
				return;
			PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
				public void run() {
					try {
						if (parent != null) {
							// we must recreate the content
							// for SWT because we will use
							// a gentle incremental reflow.
							// HTML reflow will simply reload the page.
							createContent(id, parent, toolkit);
							reflow(formText);
						}
						site.reflow(NewFeatureViewer.this, true);
					} catch (Exception e) {
						// do nothing
					}

				}
			});
		}
	}



	public void init(IIntroContentProviderSite site) {
		this.site = site;
		Thread newsWorker = new Thread(new FeatureReader());
		newsWorker.start();
		IPath changelogPath = new Path("changelog.txt");
		changelogPath = PathUtils.fixInstallationRelativePath(changelogPath);
		try {
			changelogUrl = "http://org.eclipse.ui.intro/openBrowser?url="+changelogPath.toFile().toURI().toURL().toString(); 
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	public void createContent(String id, PrintWriter out) {
		if (disposed)
			return;
		this.id = id;
		if (items == null) {
			out.print("<p class=\"status-text\">"); //$NON-NLS-1$
			out.print(MSG_LOADING); //$NON-NLS-1$
			out.println("</p>"); //$NON-NLS-1$
		} else {
			if (items.size() > 0) {
				out.println("<ul id=\"unicore-news\">"); //$NON-NLS-1$
				boolean inList = false;
				for (int i = 0; i < items.size(); i++) {
					String item = items.get(i);
					boolean listItem = item.trim().startsWith("-");
					boolean caption = item.startsWith(".");
					if (caption) {
						if (inList) {
							out.println("</ul>");
							inList = !inList;
						}
						out.println("<h4>");
						out.println(item.substring(1));
						out.println("</h4>");
					} else if (listItem) {
						if (!inList) {
							out.println("<ul>");
							inList = !inList;
						}
						out.println("<li>"); //$NON-NLS-1$
						out.println(item.substring(1));
						out.println("</li>"); //$NON-NLS-1$
					} else {
						if (inList) {
							out.println("</ul>");
							inList = !inList;
						}
						out.println(item);
					}
				}
				if(inList) {
					out.println("</ul>");
				}
				if(changelogUrl != null)
				{
					out.print("<p>");
					out.print("<a class=\"topicList\" href=\""+changelogUrl); //$NON-NLS-1$
					out.print("\">"); //$NON-NLS-1$
					out.print("See the changelog for additional features and bug fixes");
					out.print("</a>"); //$NON-NLS-1$
					out.print("</p>");
				}
			}
			out.println("</ul>"); //$NON-NLS-1$
		}
	}

	public void createContent(String id, Composite parent, FormToolkit toolkit) {
		if (disposed)
			return;
		if (formText == null) {
			// a one-time pass
			formText = toolkit.createFormText(parent, true);
			formText.addHyperlinkListener(new HyperlinkAdapter() {
				public void linkActivated(HyperlinkEvent e) {
					doNavigate((String) e.getHref());
				}
			});
			bulletImage  = Activator.getImageDescriptor("intro/content/images/arrow.gif").createImage(); //$NON-NLS-1$
			if (bulletImage!=null)
				formText.setImage(HREF_BULLET, bulletImage);
			this.parent = parent;
			this.toolkit = toolkit;
			this.id = id;
		}
		StringBuffer buffer = new StringBuffer();
		buffer.append("<form>"); //$NON-NLS-1$
		if (items == null) {
			buffer.append("<p>"); //$NON-NLS-1$
			buffer.append(MSG_LOADING); //$NON-NLS-1$
			buffer.append("</p>"); //$NON-NLS-1$
		} else {
			if (items.size() > 0) {
				boolean inList = false;
				for (int i = 0; i < items.size(); i++) {
					try {
						String item =  items.get(i);
						boolean listItem = item.trim().startsWith("-");
						boolean caption = item.startsWith(".");
						if (caption) {
							if (inList) {
								buffer.append("</ul>");
								inList = !inList;
							}
							buffer.append("<h4>");
							buffer.append(item.substring(1));
							buffer.append("</h4>");
						} else if (listItem) {
							if (!inList) {
								buffer.append("<ul>");
								inList = !inList;
							}
							buffer.append("<li>"); //$NON-NLS-1$
							buffer.append(item.substring(1));
							buffer.append("</li>"); //$NON-NLS-1$
						} else {
							if (inList) {
								buffer.append("</ul>");
								inList = !inList;
							}
							buffer.append(item);
						}
					} catch (Exception e) {
						// skip news item
					}

				}
				if (inList) {
					buffer.append("</ul>");
				}
				if(changelogUrl != null)
				{
					buffer.append("<p>");
					buffer.append("<a href=\"" + changelogUrl); //$NON-NLS-1$
					buffer.append("\">"); //$NON-NLS-1$
					buffer.append("See the changelog for additional features and bug fixes");
					buffer.append("</a>"); //$NON-NLS-1$
					buffer.append("</p>");
				}

			}
		}
		buffer.append("</form>"); //$NON-NLS-1$
		formText.setText(buffer.toString(), true, false);
	}






	private void readFeatures() {
		items = Collections.synchronizedList(new ArrayList<String>());
		InputStream in = null;
		try {
			in = FileLocator.openStream(Activator.getDefault().getBundle(),FILE_PATH,false);
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			while(true)
			{
				String s = reader.readLine();
				if(s == null) break;
				items.add(s);
			}
		}
		catch (Exception e) {
			// if anything goes wrong, fail silently; it will show a
			// "fetching News available" message.
		}
		finally {
			try {
				if (in != null) {
					in.close();
				}
			}
			catch (IOException e) {
				// nothing we can do here
			}
		}
	}

	/*
	 * This method is copied from Section and seems useful in general. Perhaps
	 * we should move it into content provider site, something like
	 * 'reflow(Control startingControl)'
	 */

	private void reflow(Control initiator) {
		Control c = initiator;
		while (c != null) {
			c.setRedraw(false);
			c = c.getParent();
			if (c instanceof ScrolledForm) {
				break;
			}
		}
		c = initiator;
		while (c != null) {
			if (c instanceof Composite)
				((Composite) c).layout(true);
			c = c.getParent();
			if (c instanceof ScrolledForm) {
				((ScrolledForm) c).reflow(true);
				break;
			}
		}
		c = initiator;
		while (c != null) {
			c.setRedraw(true);
			c = c.getParent();
			if (c instanceof ScrolledForm) {
				break;
			}
		}
	}

	public void dispose() {
		if (bulletImage != null) {
			bulletImage.dispose();
			bulletImage = null;
		}
		disposed = true;
	}

	private void doNavigate(final String url) {
		BusyIndicator.showWhile(PlatformUI.getWorkbench().getDisplay(),
				new Runnable() {
			public void run() {
				IIntroURL introUrl = IntroURLFactory
				.createIntroURL(url);
				if (introUrl != null) {
					// execute the action embedded in the IntroURL
					introUrl.execute();
					return;
				}
				// delegate to the browser support
				openBrowser(url);
			}
		});
	}

	private void openBrowser(String href) {
		try {
			URL url = new URL(href);
			IWorkbenchBrowserSupport support = PlatformUI.getWorkbench()
			.getBrowserSupport();
			support.getExternalBrowser().openURL(url);
		} catch (PartInitException e) {
		} catch (MalformedURLException e) {
		}
	}
}