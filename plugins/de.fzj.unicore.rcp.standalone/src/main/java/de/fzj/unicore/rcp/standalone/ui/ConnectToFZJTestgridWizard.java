package de.fzj.unicore.rcp.standalone.ui;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.PlatformUI;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.identity.DistinguishedName;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.KeyStoreManager;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.keystore.KeystoreCredentialController;
import de.fzj.unicore.rcp.identity.utils.ProfileUtils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.standalone.Activator;

public class ConnectToFZJTestgridWizard extends Wizard {


	public static final String testGridURL = "http://omiiei.fz-juelich.de/cgi-bin/unicore6/run.pl";
	public static final String testGridRegistry = "https://omiiei.fz-juelich.de:6000/Registry/services/Registry?res=default_registry";
	public static final String testGridName = "UNICORE test Grid";
	public static final String profileName = "UNICORE test Grid";

	ConnectToFZJTestgridWizardPage1 page1;
	KeyStoreManager ksm;
	Exception occuredException = null;

	public void addPages() {
		page1 = new ConnectToFZJTestgridWizardPage1();
		addPage(page1);
	}

	@Override
	public boolean performFinish() {
		try {
			PlatformUI.getWorkbench().getProgressService().run(true, false, new IRunnableWithProgress(){
				public void run(IProgressMonitor progress) {

					try {
						occuredException = null;
						progress.beginTask("Connecting to UNICORE test Grid", 100);
						DistinguishedName dn = page1.getDN();
						if(dn != null)
						{
							SubProgressMonitor sub = new SubProgressMonitor(progress, 90);
							KeyStore jks = downloadKeystore(dn,sub);
							ksm = KeyStoreManager.getInstance(jks,"unicore");

						}

					} catch (Exception e) {
						occuredException = e;
					}
					finally
					{

						progress.done();
					}

				}});
			if(occuredException != null) Activator.log(Status.ERROR,"Unable to connect to UNICORE test Grid: "+occuredException.getMessage(),occuredException);
			else if(ksm != null)
			{
				KeystoreCredentialController ks = (KeystoreCredentialController)IdentityActivator.getDefault().getCredential();
				ks.importKeysFromKeystore(ksm, "unicore",false,true);
				IdentityActivator.getDefault().getTrust().importKeyStore(ksm, "unicore", true);
				List<String> newAliases = ksm.getIdentityAliases();
				if(newAliases.size() > 0)
				{
					String pattern = addRegistryEntry();
					Certificate cert = ks.getCertificateList().getCertByName(newAliases.get(0));
					ProfileUtils.addSecurityProfile(profileName,pattern, cert, true,true);
					IdentityActivator.getDefault().propertyChange(new IdentityActivator.AuthNChangeEvent(this, "profiles", null, null));
				}
			}

		} catch (Exception e) {
			Activator.log(Status.ERROR,"Unable to connect to UNICORE test Grid: "+e.getMessage(),e);
		} 


		return true;
	}

	/**
	 * Add a new bookmark for the UNICORE test Grid and
	 * return it's namepath for setting up a security profile
	 * @return
	 * @throws Exception
	 */
	protected String addRegistryEntry() throws Exception
	{
		GridNode grid = ServiceBrowserActivator.getDefault().getGridNode();
		// check whether a node for the registry exists already
		Node[] children = grid.getChildrenArray();
		for (Node node : children) {
			if(testGridRegistry.equalsIgnoreCase(node.getEpr().getAddress().getStringValue()))
			{
				return node.getNamePathToRoot()+"/*";
			}
		}
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue(testGridRegistry);
		Node node = NodeFactory.createNode(epr);
		node.setName(testGridName);
		grid.addBookmark(node);
		grid.setExpanded(true);
		return node.getNamePathToRoot()+"/*";
	}

	

	protected KeyStore downloadKeystore(DistinguishedName dn,IProgressMonitor progress)throws IOException,KeyStoreException,CertificateException,NoSuchAlgorithmException{
		String jks=getJKSURL(dn,progress);
		//now download JKS
		URL u=new URL(jks);
		HttpURLConnection http=(HttpURLConnection)u.openConnection();
		InputStream is=http.getInputStream();
		KeyStore ks=KeyStore.getInstance("jks");
		ks.load(is, "unicore".toCharArray());
		return ks;
	}


	protected String getJKSURL(DistinguishedName dn,IProgressMonitor progress)throws IOException{
		int totalWork = 7;
		progress.beginTask("Signing up for a test Grid certificate", totalWork);
		try {
			URL u=new URL(testGridURL);
			HttpURLConnection http=(HttpURLConnection)u.openConnection();
			http.setRequestMethod("POST");
			http.setConnectTimeout(5000);

			StringBuilder request=new StringBuilder();
			request.append("KEYSTOREONLY="+URLEncoder.encode("on","UTF-8"));
			if(dn.CN.length > 0) request.append("&CN=").append(URLEncoder.encode(dn.CN[0],"UTF-8"));
			if(dn.EMAILADDRESS.length > 0) request.append("&E=").append(URLEncoder.encode(dn.EMAILADDRESS[0],"UTF-8"));
			if(dn.C.length > 0) request.append("&C=").append(URLEncoder.encode(dn.C[0],"UTF-8"));
			if(dn.O.length > 0) request.append("&O=").append(URLEncoder.encode(dn.O[0],"UTF-8"));
			if(dn.OU.length > 0) request.append("&OU=").append(URLEncoder.encode(dn.OU[0],"UTF-8"));
			if(dn.L.length > 0) request.append("&L=").append(URLEncoder.encode(dn.L[0],"UTF-8"));
			if(dn.ST.length > 0) request.append("&ST=").append(URLEncoder.encode(dn.ST[0],"UTF-8"));
			http.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(http.getOutputStream());
			wr.write(request.toString());
			wr.flush();
			ByteArrayOutputStream os=new ByteArrayOutputStream();
			int code=http.getResponseCode();
			String fullResponse = "";
			if(code==HttpURLConnection.HTTP_OK){
				InputStream response=http.getInputStream();
				byte[]data=new byte[8192];
				int len;
				int i = 0;
				String beginTag = "<div id=\"log\" />";
				

				String message = "";
				
				while(true){
					len=response.read(data);
					if(len==-1)break;
					int j = 0;
					String current = new String(data,0,len);
					fullResponse+= current;
					if(i < beginTag.length())
					{

						while(j < current.length() && i < beginTag.length())
						{
							if(current.charAt(j) == beginTag.charAt(i)){
								i++;
							}
							else {
								i = 0;
							}
							j++;
						}
					}
					if(i == beginTag.length())
					{
						while(j < current.length())
						{
							if(message != null)
							{
								if(current.charAt(j) == '.') 
								{
									if(totalWork > 0)
									{
										progress.worked(1);
										totalWork -= 1;
										progress.subTask(message);
									}
									message = null;
								}
								else {
									message+=current.charAt(j);
								}
							}
							else if(current.charAt(j) == '>')
							{
								message = "";
							}
							j++;
						}
						os.write(data, 0, len);
					}
				}

			}
			else{
				throw new IOException("Can't perform request. Error : "+http.getResponseCode()+" "+http.getResponseMessage());
			}
			String s = os.toString();
			if(s == null || s.trim().length() == 0)
			{
				String errorExpr = ".*<h1>UNICORE 6 Client Download with User Registration at TestGrid</h1><h3>([^<]*).*";
				Pattern p=Pattern.compile(errorExpr,Pattern.DOTALL);
				Matcher m=p.matcher(fullResponse);
				if(m.matches()){
					throw new IOException(m.group(1));
				}
				else throw new IOException("Unknown problem.");
				
			}
			else return extractURL(s);
		} finally {
			progress.done();
		}

	}

	protected String extractURL(String source)throws IOException{
		Pattern p=Pattern.compile(".+href=\"(.*\\.jks).*",Pattern.DOTALL);
		Matcher m=p.matcher(source);
		if(m.matches()){
			return m.group(1);
		}
		else{
			throw new IOException("Can't extract keystore URL, the server returned: "+source);
		}
	}

}
