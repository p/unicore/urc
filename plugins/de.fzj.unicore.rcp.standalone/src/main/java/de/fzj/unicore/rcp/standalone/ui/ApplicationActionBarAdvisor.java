package de.fzj.unicore.rcp.standalone.ui;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarContributionItem;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ContributionItemFactory;
import org.eclipse.ui.actions.NewWizardMenu;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.ide.IDEActionFactory;
import de.fzj.unicore.rcp.standalone.Activator;
import de.fzj.unicore.rcp.standalone.actions.DownloadClientConfigurationAction;
import de.fzj.unicore.rcp.standalone.actions.ConfigureSecurityAction;

/**
 * An action bar advisor is responsible for creating, adding, and disposing of the
 * actions added to a workbench window. Each window will be populated with
 * new actions.
 */
public class ApplicationActionBarAdvisor extends ActionBarAdvisor {

	 private IWorkbenchAction aboutAction;
	    private IWorkbenchAction showHelpAction; 
	    private IWorkbenchAction searchHelpAction;
	    private IWorkbenchAction dynamicHelpAction; 

	private IWorkbenchAction closeAllPerspsAction;

	private IWorkbenchAction closePerspAction;

	private Action configureSecurityAction;
	
	private Action importConfigurationProfileAction;
	
	private IWorkbenchAction copyAction;

	private IWorkbenchAction cutAction;

	private IWorkbenchAction editActionSetAction;

	private IWorkbenchAction exitAction;

	private IWorkbenchAction exportResourcesAction;

	private IWorkbenchAction findAction;

	private IWorkbenchAction importResourcesAction;
	
	private IWorkbenchAction introAction;

	private IWorkbenchAction openPreferencesAction;

	private IWorkbenchAction openWorkspaceAction;

	private IWorkbenchAction pasteAction;

	private IWorkbenchAction propertiesAction;

	private IWorkbenchAction redoAction;

	private IWorkbenchAction resetPerspectiveAction;

	private IWorkbenchAction saveAction;

	private IWorkbenchAction saveAllAction;

	private IWorkbenchAction saveAsAction;

	private IWorkbenchAction savePerspectiveAction;

	private IWorkbenchAction undoAction;

	private IWorkbenchWindow window;
	// Actions - important to allocate these only in makeActions, and then use them
	// in the fill methods.  This ensures that the actions aren't recreated
	// when fillActionBars is called with FILL_PROXY.
//	private OpenFileAction openFileAction;


	public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
		super(configurer);
	}

	
	
	/**
	 * Adds the perspective actions to the specified menu.
	 */
	private void addPerspectiveActions(MenuManager menu) {
		{
			String openText = "&Open Perspective";
			MenuManager changePerspMenuMgr = new MenuManager(openText,
			"openPerspective"); //$NON-NLS-1$
			IContributionItem changePerspMenuItem = ContributionItemFactory.PERSPECTIVES_SHORTLIST
			.create(getWindow());
			changePerspMenuMgr.add(changePerspMenuItem);
			menu.add(changePerspMenuMgr);
		}


		menu.add(editActionSetAction);
		menu.add(savePerspectiveAction);
		menu.add(resetPerspectiveAction);
		menu.add(closePerspAction);
		menu.add(closeAllPerspsAction);
		menu.add(new Separator());
		{
			MenuManager showViewMenuMgr = new MenuManager("Show &View", "showView"); //$NON-NLS-1$
			IContributionItem showViewMenu = ContributionItemFactory.VIEWS_SHORTLIST
			.create(getWindow());
			showViewMenuMgr.add(showViewMenu);
			menu.add(showViewMenuMgr);
		}
	}

	 /**
	 * Adds a <code>GroupMarker</code> or <code>Separator</code> to a menu.
	 * The test for whether a separator should be added is done by checking for
	 * the existence of a preference matching the string
	 * useSeparator.MENUID.GROUPID that is set to <code>true</code>.
	 * 
	 * @param menu
	 *            the menu to add to
	 * @param groupId
	 *            the group id for the added separator or group marker
	 */
	private void addSeparatorOrGroupMarker(MenuManager menu, String groupId) {
		String prefId = "useSeparator." + menu.getId() + "." + groupId; //$NON-NLS-1$ //$NON-NLS-2$
		boolean addExtraSeparators = Activator.getDefault()
				.getPreferenceStore().getBoolean(prefId);
		if (addExtraSeparators) {
			menu.add(new Separator(groupId));
		} else {
			menu.add(new GroupMarker(groupId));
		}
	}
	
	/**
	 * Creates and returns the Edit menu.
	 */
	private MenuManager createEditMenu() {
		MenuManager menu = new MenuManager("&Edit", IWorkbenchActionConstants.M_EDIT);
		menu.add(new GroupMarker(IWorkbenchActionConstants.EDIT_START));

		menu.add(undoAction);
		menu.add(redoAction);
		menu.add(new GroupMarker(IWorkbenchActionConstants.UNDO_EXT));
		menu.add(new Separator());

		menu.add(cutAction);
		menu.add(copyAction);
		menu.add(pasteAction);
		menu.add(new GroupMarker(IWorkbenchActionConstants.CUT_EXT));
		menu.add(new Separator());

//		menu.add(deleteAction);
//		menu.add(selectAllAction);
//		menu.add(new Separator());

		menu.add(findAction);
		menu.add(new GroupMarker(IWorkbenchActionConstants.FIND_EXT));
		menu.add(new Separator());


		menu.add(new GroupMarker(IWorkbenchActionConstants.ADD_EXT));

		menu.add(new GroupMarker(IWorkbenchActionConstants.EDIT_END));
		menu.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));

		return menu;
	}


	/**
	 * Creates and returns the File menu.
	 */
	private MenuManager createFileMenu() {
		MenuManager menu = new MenuManager("&File", IWorkbenchActionConstants.M_FILE);
		menu.add(new GroupMarker(IWorkbenchActionConstants.FILE_START));
		{
			// create the New submenu, using the same id for it as the New action
			String newText = "&New";
			String newId = ActionFactory.NEW.getId();
			MenuManager newMenu = new MenuManager(newText, newId);
			newMenu.add(new Separator(newId));
			NewWizardMenu newWizardMenu = new NewWizardMenu(getWindow());
			newMenu.add(newWizardMenu);
			newMenu.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
			menu.add(newMenu);
		}
		menu.add(new GroupMarker(IWorkbenchActionConstants.NEW_EXT));
		menu.add(new Separator());
		menu.add(saveAction);
		menu.add(saveAsAction);
		menu.add(saveAllAction);
		menu.add(new Separator());
		menu.add(openWorkspaceAction);
		menu.add(new GroupMarker(IWorkbenchActionConstants.OPEN_EXT));
		menu.add(new Separator());
		menu.add(importResourcesAction);
		menu.add(exportResourcesAction);
		menu.add(new GroupMarker(IWorkbenchActionConstants.IMPORT_EXT));
		menu.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		menu.add(new Separator());
		menu.add(propertiesAction);
		menu.add(new Separator());
		menu.add(exitAction);
		return menu;
	}

	private MenuManager createHelpMenu() {
		MenuManager menu = new MenuManager("&Help", IWorkbenchActionConstants.M_HELP);
		addSeparatorOrGroupMarker(menu, "group.intro"); //$NON-NLS-1$
		// See if a welcome or intro page is specified
		if (introAction != null) {
			menu.add(introAction);
		} 
		menu.add(new GroupMarker("group.intro.ext")); //$NON-NLS-1$
		menu.add(aboutAction);
		menu.add(showHelpAction);
		menu.add(searchHelpAction);
		menu.add(dynamicHelpAction);
		return menu;
	}

	/**
	 * Creates and returns the Window menu.
	 */
	private MenuManager createWindowMenu() {
		MenuManager menu = new MenuManager("&Window", IWorkbenchActionConstants.M_WINDOW);

		addPerspectiveActions(menu);
		menu.add(new Separator());
		menu.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		menu.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS + "end")); //$NON-NLS-1$
		menu.add(new Separator());
		menu.add(openPreferencesAction);
		menu.add(configureSecurityAction);
		menu.add(importConfigurationProfileAction);
		menu.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
//		menu.add(ContributionItemFactory.OPEN_WINDOWS.create(getWindow()));
		return menu;
	}

	/**
	 * Fills the coolbar with the workbench actions.
	 */
	protected void fillCoolBar(ICoolBarManager coolBar) {

		{ // Set up the context Menu
			IMenuManager popUpMenu = new MenuManager();
			popUpMenu.add(new ActionContributionItem(editActionSetAction));
			coolBar.setContextMenuManager(popUpMenu);
		}
		coolBar.add(new GroupMarker("group.file"));
		{ // File Group
			IToolBarManager fileToolBar = new ToolBarManager();
			fileToolBar.add(new Separator(IWorkbenchActionConstants.NEW_GROUP));

			fileToolBar.add(new GroupMarker(IWorkbenchActionConstants.NEW_EXT));
			fileToolBar.add(new GroupMarker(
					IWorkbenchActionConstants.SAVE_GROUP));
			fileToolBar.add(saveAction);
			fileToolBar.add(saveAllAction);
			fileToolBar
			.add(new GroupMarker(IWorkbenchActionConstants.SAVE_EXT));

			fileToolBar
			.add(new GroupMarker(IWorkbenchActionConstants.PRINT_EXT));

			fileToolBar
			.add(new Separator(IWorkbenchActionConstants.BUILD_GROUP));
			fileToolBar
			.add(new GroupMarker(IWorkbenchActionConstants.BUILD_EXT));
			fileToolBar.add(new Separator(
					IWorkbenchActionConstants.MB_ADDITIONS));

//			// Add to the cool bar manager
			coolBar.add(new ToolBarContributionItem(fileToolBar,
					IWorkbenchActionConstants.TOOLBAR_FILE));
		}

		coolBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));



		coolBar.add(new GroupMarker(IWorkbenchActionConstants.GROUP_EDITOR));


		coolBar.add(new GroupMarker(IWorkbenchActionConstants.GROUP_HELP));


	}

	protected void fillMenuBar(IMenuManager menuBar) {
		MenuManager fileMenu = createFileMenu();
		MenuManager editMenu = createEditMenu();
		MenuManager windowMenu = createWindowMenu();
		MenuManager helpMenu = createHelpMenu();

		menuBar.add(fileMenu);
		menuBar.add(editMenu);
//		Add a group marker indicating where action set menus will appear.
		menuBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		menuBar.add(windowMenu);
		menuBar.add(helpMenu);

	}

	protected Action getConfigureSecurityAction() {
		return configureSecurityAction;
	}
	
	protected Action getImportConfigurationProfileAction() {
		return importConfigurationProfileAction;
	}

	/**
	 * @return the window
	 */
	public IWorkbenchWindow getWindow() {
		return window;
	}

	protected void makeActions(final IWorkbenchWindow window) {
		// Creates the actions and registers them.
		// Registering is needed to ensure that key bindings work.
		// The corresponding commands keybindings are defined in the plugin.xml file.
		// Registering also provides automatic disposal of the actions when
		// the window is closed.
		this.window = window;
//		openFileAction = new OpenFileAction(window.getPages()[0]);
//		register(openFileAction);

		propertiesAction = ActionFactory.PROPERTIES.create(window);
		register(propertiesAction);

		findAction = ActionFactory.FIND.create(window);
		register(findAction);

		exitAction = ActionFactory.QUIT.create(window);
		register(exitAction);

		aboutAction = ActionFactory.ABOUT.create(window);
	    register(aboutAction);

	    showHelpAction = ActionFactory.HELP_CONTENTS.create(window); 
	    register(showHelpAction); 

	    searchHelpAction = ActionFactory.HELP_SEARCH.create(window); 
	    register(searchHelpAction); 

	    dynamicHelpAction = ActionFactory.DYNAMIC_HELP.create(window); 
	    register(dynamicHelpAction); 

		saveAction = ActionFactory.SAVE.create(window);
		register(saveAction);

		saveAsAction = ActionFactory.SAVE_AS.create(window);
		register(saveAsAction);

		saveAllAction = ActionFactory.SAVE_ALL.create(window);
		register(saveAllAction);

		importResourcesAction = ActionFactory.IMPORT.create(window);
		register(importResourcesAction);
		
		if (window.getWorkbench().getIntroManager().hasIntro()) {
            introAction = ActionFactory.INTRO.create(window);
            register(introAction);
        }

		exportResourcesAction = ActionFactory.EXPORT.create(window);
		register(exportResourcesAction);

		undoAction = ActionFactory.UNDO.create(window);
		register(undoAction);

		redoAction = ActionFactory.REDO.create(window);
		register(redoAction);

		cutAction = ActionFactory.CUT.create(window);
		register(cutAction);

		copyAction = ActionFactory.COPY.create(window);
		register(copyAction);

		pasteAction = ActionFactory.PASTE.create(window);
		register(pasteAction);

		editActionSetAction = ActionFactory.EDIT_ACTION_SETS.create(window);
		register(editActionSetAction);

		resetPerspectiveAction = ActionFactory.RESET_PERSPECTIVE.create(window);
		register(resetPerspectiveAction);

		openWorkspaceAction = IDEActionFactory.OPEN_WORKSPACE
		.create(window);
		register(openWorkspaceAction);

		savePerspectiveAction = ActionFactory.SAVE_PERSPECTIVE.create(window);
		register(savePerspectiveAction);

		closePerspAction = ActionFactory.CLOSE_PERSPECTIVE.create(window);
		register(closePerspAction);

		closeAllPerspsAction = ActionFactory.CLOSE_ALL_PERSPECTIVES.create(window);
		register(closeAllPerspsAction);

		openPreferencesAction = ActionFactory.PREFERENCES.create(window);
		register(openPreferencesAction);

		configureSecurityAction = new ConfigureSecurityAction(window);
		register(configureSecurityAction);
		
		importConfigurationProfileAction = new DownloadClientConfigurationAction(window);
		register(importConfigurationProfileAction);

	}

	/**
	 * @param window the window to set
	 */
	public void setWindow(IWorkbenchWindow window) {
		this.window = window;
	}
}
