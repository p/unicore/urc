package de.fzj.unicore.rcp.standalone.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IPageChangingListener;
import org.eclipse.jface.dialogs.PageChangingEvent;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.standalone.Activator;
import de.fzj.unicore.rcp.standalone.ui.ConnectToFZJTestgridWizard;
import de.fzj.unicore.rcp.standalone.ui.ConnectToFZJTestgridWizardPage1;
import de.fzj.unicore.rcp.standalone.ui.ICommandIds;


public class ConnectToFZJTestgridAction extends Action implements IPageChangingListener {
	
	

	
	public ConnectToFZJTestgridAction() {
        setText("Connect to UNICORE testgrid");

        // The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_CONNECT_TO_FZJ_TESTGRID);
		setImageDescriptor(Activator.getImageDescriptor("/icons/sample2.gif"));
	}
	
	public void run() {
		Shell s = null;
		try {
			s = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		} catch (Exception e) {

		}
		if (s == null) s = new Shell();
		
		if(isInactive()){
			showDisabledDialog(s);
			return;
		}
		
		ConnectToFZJTestgridWizard wizard = new ConnectToFZJTestgridWizard();
		

		WizardDialog dialog = new WizardDialog(s, wizard);
		dialog.addPageChangingListener(this);
		// is is safe to ignore the result of this dialog,
		// as it does the test grid connection internally
		dialog.open();
	}

	boolean isInactive(){
		return IdentityActivator.getDefault().isUsingUnity();
	}

	/**
	 * temporary workaround: if not using a keystore, just show this to the user
	 */
	void showDisabledDialog(Shell s){
		MessageBox mb = new MessageBox(s,SWT.OK);
		mb.setMessage("The test grid is not available when using Unity. Please configure a keystore instead!");
		mb.open();
	}
	
	public void handlePageChanging(PageChangingEvent event) {
		if(event.getCurrentPage() instanceof ConnectToFZJTestgridWizardPage1)
		{
			
		}
		
	}
	
	

	
}
