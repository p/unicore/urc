package de.fzj.unicore.rcp.standalone.ui;

import java.net.URL;

import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.internal.WorkbenchPlugin;
import org.eclipse.ui.internal.ide.IDEInternalWorkbenchImages;
import org.eclipse.ui.internal.ide.IDEWorkbenchPlugin;
import org.eclipse.ui.internal.registry.ActionSetRegistry;
import org.eclipse.ui.internal.registry.IActionSetDescriptor;
import org.eclipse.ui.statushandlers.AbstractStatusHandler;
import org.osgi.framework.Bundle;

import de.fzj.unicore.rcp.standalone.perspectives.UnicorePerspective;

/**
 * This workbench advisor creates the window advisor, and specifies
 * the perspective id for the initial window.
 */
public class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

	private IWorkbenchPage page;

	/**
	 * The workbench error handler.
	 */
	private AbstractStatusHandler workbenchErrorHandler;
	
	@Override
	public void initialize(IWorkbenchConfigurer configurer)
	{
		configurer.setExitOnLastWindowClose(true);
		configurer.setSaveAndRestore(true);

		// For standalone app, remove the stuff we don't use
		ActionSetRegistry reg = WorkbenchPlugin.getDefault()
		.getActionSetRegistry();

		IActionSetDescriptor[] actionSets = reg.getActionSets();
		String[] removeActionSets = new String[] {
				"org.eclipse.search.searchActionSet",
				"org.eclipse.ui.cheatsheets.actionSet",
				// "org.eclipse.ui.actionSet.keyBindings",
				"org.eclipse.ui.edit.text.actionSet.navigation",
				"org.eclipse.ui.edit.text.actionSet.annotationNavigation",
				"org.eclipse.ui.edit.text.actionSet.convertLineDelimitersTo",
				// "org.eclipse.ui.edit.text.actionSet.openExternalFile",
				"org.eclipse.rse.core.search.searchActionSet",
				"org.eclipse.ui.externaltools.ExternalToolsSet",
				"org.eclipse.jdt.debug.ui.JDTDebugActionSet",
				"org.eclipse.team.ui.actionSet",
				"org.eclipse.team.cvs.ui.CVSActionSet",
		"org.eclipse.ui.WorkingSetActionSet" };

		for (int i = 0; i < actionSets.length; i++)
		{
			boolean found = false;
			for (int j = 0; j < removeActionSets.length; j++){
				if (removeActionSets[j].equals(actionSets[i].getId()))
				{
					found = true;
					break;
				}
			}

			if (!found)
				continue;
			IExtension ext = actionSets[i].getConfigurationElement()
			.getDeclaringExtension();

			reg.removeExtension(ext, new Object[] { actionSets[i]});

		} 

		Bundle ideBundle = Platform.getBundle(IDEWorkbenchPlugin.IDE_WORKBENCH);
		final String banner = "/icons/full/wizban/saveas_wiz.png";
		URL url = ideBundle.getEntry(banner);
		ImageDescriptor desc = ImageDescriptor.createFromURL(url);
		configurer.declareImage(IDEInternalWorkbenchImages.IMG_DLGBAN_SAVEAS_DLG, desc, true);

	}

	public boolean preShutdown()
	{
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		page = window.getActivePage();
		if(page != null)
		{
			// certain views must be closed since they don't persist their state
			// TODO persist their state
			IViewReference[] allViews = page.getViewReferences();
			for (IViewReference part : allViews) {
				String id = (part != null) ? part.getId() : null;
				if(id != null && (id.startsWith("de.fzj.unicore.rcp.gpe4eclipse.views.JobOutcomeView") 
						|| id.startsWith("org.chemomentum.rcp.tracegui.TraceGraphView")))
				{
					page.hideView(part);
				}
			}
		}
		return true;
	}

	public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		return new ApplicationWorkbenchWindowAdvisor(configurer);

	}

	public String getInitialWindowPerspectiveId() {
		return UnicorePerspective.ID;
	} 
	
	@Override
	public synchronized AbstractStatusHandler getWorkbenchErrorHandler() {
		if (workbenchErrorHandler == null) {
			workbenchErrorHandler = new ApplicationWorkbenchErrorHandler();
		}
		return workbenchErrorHandler;
	}

}
