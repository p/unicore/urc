package de.fzj.unicore.rcp.standalone;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.intro.IIntroPart;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.logmonitor.LogActivator;
import de.fzj.unicore.rcp.standalone.perspectives.UnicorePerspective;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin implements
IPropertyChangeListener {

	// The plug-in ID
	public static final String PLUGIN_ID = "de.fzj.unicore.rcp.standalone";

	// The shared instance
	private static Activator plugin;

	private BundleContext context;

	// used for performance logging. Time when the constructor of
	// CustomizableIntroPart is called.
	private long uiCreationStartTime;

	// image registry that can be disposed while the
	// plug-in is still active. This is important for
	// switching themes after the plug-in has been loaded.
	private ImageRegistry volatileImageRegistry;

	/**
	 * The constructor
	 */
	public Activator() {
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	public void start(BundleContext context) throws Exception {
		UnicoreCommonActivator.getDefault().getPreferenceStore()
		.addPropertyChangeListener(this);
		super.start(context);
		this.context = context;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	public void stop(BundleContext context) throws Exception {
		resetVolatileImageRegistry();
		plugin = null;
		super.stop(context);
		UnicoreCommonActivator.getDefault().getPreferenceStore()
		.removePropertyChangeListener(this);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	public BundleContext getContext() {
		return context;
	}

	/**
	 * Returns the Intro Part.
	 */
	public static IIntroPart getIntro() {
		IIntroPart introPart = PlatformUI.getWorkbench().getIntroManager()
		.getIntro();
		return introPart;
	}

	/**
	 * Returns the Intro Part after forcing an open on it.
	 */
	public static IIntroPart showIntro(boolean standby) {
		IIntroPart introPart = PlatformUI.getWorkbench().getIntroManager()
		.showIntro(
				PlatformUI.getWorkbench().getActiveWorkbenchWindow(),
				standby);
		return introPart;
	}

	/**
	 * Returns the standby state of the Intro Part. If the intro is closed,
	 * retruns false.
	 */
	public static boolean isIntroStandby() {
		return PlatformUI.getWorkbench().getIntroManager().isIntroStandby(
				getIntro());
	}

	/**
	 * Sets the standby state of the Intro Part. If the intro is closed, retruns
	 * false.
	 */
	public static void setIntroStandby(boolean standby) {
		PlatformUI.getWorkbench().getIntroManager().setIntroStandby(getIntro(),
				standby);
	}

	/**
	 * Returns the standby state of the Intro Part. If the intro is closed,
	 * retruns false.
	 */
	public static boolean closeIntro() {
		// Relies on Workbench.
		return PlatformUI.getWorkbench().getIntroManager().closeIntro(
				getIntro());
	}

	public ImageRegistry getVolatileImageRegistry() {
		if (volatileImageRegistry == null) {
			volatileImageRegistry = createImageRegistry();
			initializeImageRegistry(volatileImageRegistry);
		}
		return volatileImageRegistry;
	}

	public void resetVolatileImageRegistry() {
		if (volatileImageRegistry != null) {
			volatileImageRegistry.dispose();
			volatileImageRegistry = null;
		}
	}
	

	/** 
	 * Log a message via Eclipse logging
	 * Defaults to INFO Level 
	 * @param msg the message to log
	 */
	public static void log(String msg) {
		log(msg, null);
	}
	/** 
	 * Log a message via Eclipse logging
	 * Defaults to INFO Level 
	 * @param msg the message to log
	 * @param e stacktrace to append
	 */
	public static void log(String msg, Exception e) {
		log(Status.INFO, msg, e, false);
	}
	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, false);
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param e stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, false);
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param forceAlarmUser forces the logging plugin to open a popup; usually, this should NOT be set to true!
	 */
	public static void log(int status, String msg, boolean forceAlarmUser) {
		log(status, msg, null, forceAlarmUser);
	}
	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param e stacktrace to append
	 * @param forceAlarmUser forces the logging plugin to open a popup; usually, this should NOT be set to true!
	 */
	public static void log(int status, String msg, Exception e, boolean forceAlarmUser) {
		IStatus s = new Status(status, PLUGIN_ID, Status.OK, msg, e);
		LogActivator.log(plugin, s,forceAlarmUser);
	}

	public long gettUICreationStartTime() {
		return uiCreationStartTime;
	}

	public void setUICreationStartTime(long uiCreationStartTime) {
		this.uiCreationStartTime = uiCreationStartTime;
	}

	public void propertyChange(PropertyChangeEvent event) {
		if (Constants.P_GRID_DETAIL_LEVEL.equals(event.getProperty())) {
			IWorkbenchPage page = PlatformUI.getWorkbench()
			.getActiveWorkbenchWindow().getActivePage();
			String title = "Reset perspective?";
			String message = "In order to apply all settings of the selected expertise level, the UNICORE perspective must be reset. Do you want to reset the perspective now?";
			if (!MessageDialog.openConfirm(getWorkbench()
					.getActiveWorkbenchWindow().getShell(), title, message))
				return;
			IPerspectiveDescriptor oldDesc = page.getPerspective();
			IPerspectiveDescriptor unicoreDesc = PlatformUI.getWorkbench()
			.getPerspectiveRegistry().findPerspectiveWithId(
					UnicorePerspective.ID);
			PlatformUI.getWorkbench().getPerspectiveRegistry()
			.revertPerspective(unicoreDesc);
			if (!unicoreDesc.equals(oldDesc))
				page.setPerspective(unicoreDesc);
			page.resetPerspective();
			if (!unicoreDesc.equals(oldDesc))
				page.setPerspective(oldDesc);

		}
	}

}
