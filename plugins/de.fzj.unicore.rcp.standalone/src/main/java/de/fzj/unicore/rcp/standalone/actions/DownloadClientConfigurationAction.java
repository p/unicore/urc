package de.fzj.unicore.rcp.standalone.actions;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.SSLHandshakeException;

import org.apache.xmlbeans.XmlException;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import xmlbeans.eu.unicore.ccm.internal.ClientProfileDocument.ClientProfile;
import xmlbeans.eu.unicore.ccm.internal.RegistryType;
import xmlbeans.eu.unicore.ccm.internal.TrustedCAType;
import xmlbeans.eu.unicore.ccm.internal.URCExtensionType;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.KeyStoreManager;
import de.fzj.unicore.rcp.identity.TrustController;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.truststore.KeystoreTrustController;
import de.fzj.unicore.rcp.identity.truststore.TruststoreView;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.standalone.Activator;
import de.fzj.unicore.rcp.standalone.ui.ICommandIds;
import de.fzj.unicore.rcp.standalone.ui.DownloadClientConfigurationDialog;
import eu.unicore.ccm.CCMManager;
import eu.unicore.ccm.ProfileHandler;

/**
 * @author Rafal
 * 
 */
public class DownloadClientConfigurationAction extends Action {

	public static String INFORMATION_DIALOG_TITLE = "Downloading client configuration";

	private final IWorkbenchWindow window;

	
	public DownloadClientConfigurationAction() {
		this(PlatformUI.getWorkbench().getActiveWorkbenchWindow());
	}
	public DownloadClientConfigurationAction(IWorkbenchWindow window) {
		this.window = window;
		setText("Download Client Configuration");
		setToolTipText("Download client configuration profile.");
		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_DOWNLOAD_CONFIGURATION_PROFILE);
		setImageDescriptor(Activator
				.getImageDescriptor("/icons/download-client-config.png"));
	}

	public void run() {
		if (window != null) {
			DownloadClientConfigurationDialog dialog = new DownloadClientConfigurationDialog(
					window.getShell());
			int result = dialog.open();

			String address = dialog.getUrlText();
			String fingerprint = dialog.getFingerprintText();
			boolean backupKeystore = dialog.getBackupOption();

			boolean apply = (Window.OK == result) && (!address.equals(""));
			if (apply) {
				ClientProfile profile = downloadClientProfile(address,
						fingerprint);
				// no need of message dialog when null, because appropriate
				// message dialogs are show in downloadClientProfile method
				// depending on the exception
				if (profile != null) {
					String profileName = profile.getName();

					RegistryType[] registries = profile.getRegistryArray();
					TrustedCAType[] trustedCAs = profile.getTrustedCAArray();
					URCExtensionType[] urcExtensions = profile
							.getUrcExtensionArray();

					int trustedErrorCount = processProfileTrustedCAs(
							trustedCAs, backupKeystore);
					int registryErrorCount = processProfileRegistries(registries);
					processProfileUrcPlugins(urcExtensions);

					String message = "Client configuration profile '"
							+ profileName + "' downloaded and processed.\n";
					if (trustedErrorCount < 0) {
						message += "Problems during downloading CA certificates!\n";
					} else {
						int errorCount = trustedErrorCount + registryErrorCount;
						if (errorCount > 0)
							message += ("Imported with " + errorCount + " error(s).\n");
						else
							message += ("Imported without any errors.\n");
					}
					MessageDialog.openInformation(window.getShell(),
							INFORMATION_DIALOG_TITLE, message);

					try {
						window.getActivePage().showView(TruststoreView.ID);
					} catch (PartInitException e) {
						MessageDialog.openError(window.getShell(), "Error",
								"Error opening view:" + e.getMessage());
					}
				}
			}
		}
	}

	/**
	 * Method processing registries array of client's profile.
	 * 
	 * @param registries
	 *            Array of registries
	 * @return number of entries with wrong address
	 */
	protected int processProfileRegistries(RegistryType[] registries) {
		GridNode gridNode = ServiceBrowserActivator.getDefault().getGridNode();
		int errorCount = 0;
		for (RegistryType registry : registries) {
			URI uri = null;
			try {
				uri = new URI(registry.getAddress());
			} catch (URISyntaxException e) {
				MessageDialog.openWarning(window.getShell(), "Warning",
						"Wrong registry address: \n" + registry.getAddress());
				++errorCount;
				continue;
			}

			Node newNode = NodeFactory.createNode(uri);
			newNode.setName(registry.getSuggestedLabel());
			newNode.setExpanded(true);
			newNode.updateChildrenFromData(Node.MAX_LEVEL);

			try {
				gridNode.addBookmark(newNode);
				// refreshing new node
				newNode.asyncRefresh(1, false);
			} catch (Exception e) {
				// GridNode.addBookmark throws Exception only when bookmark
				// already exists, so in that case just ignore it
			}
		}
		return errorCount;
	}

	/**
	 * Method processing trusted CA certificates array of client's profile.
	 * 
	 * @param trustedCAs
	 *            array of trusted certificates
	 * @param backupKeystore
	 *            option if backup keystore should be saved
	 * @return number of problematic CA certificates or -1 if there was error
	 *         enabling download of certificates
	 */
	protected int processProfileTrustedCAs(TrustedCAType[] trustedCAs,
			boolean backupKeystore) {
		if (trustedCAs == null) {
			return 0;
		}
		X509Certificate[] caCertificates;
		try {
			caCertificates = downloadAvailableTrustedCACertificates(trustedCAs);
		} catch (CertificateException e) {
			MessageDialog.openError(window.getShell(), "Error", e.getMessage());
			return -1;
		}

		TrustController tcc = IdentityActivator.getDefault().getTrust();
		if (backupKeystore && tcc instanceof KeystoreTrustController) {
			KeyStoreManager ksManager = ((KeystoreTrustController)tcc).getKeystoreManager();
			// it may overwrite previous backup if made during one minute,
			// it happens when users are playing instead of working
			String dateSuffix = new SimpleDateFormat("yyyyMMddHHmm")
					.format(new Date());
			File ksBackupFile = new File(ksManager.getKeyStoreName() + "."
					+ dateSuffix);
			try {
				ksManager.reWriteKeyStore(ksBackupFile);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			MessageDialog.openInformation(
					window.getShell(),
					"Information",
					"Keystore backup saved as a file '"
							+ ksBackupFile.getName() + "'");
		}

		int errorCount = 0;
		for (int i = 0; i < trustedCAs.length; ++i) {
			X509Certificate caCert = caCertificates[i];
			if (caCert == null) {
				// did not manage to download certificate
				++errorCount;
				continue;
			}

			try {
				String alias = trustedCAs[i].getSuggestedLabel().toLowerCase();
				Certificate cert = new Certificate(caCert);
				cert.setName(alias);
				tcc.addTrustedCertificate(cert);
			} catch (Exception e) {
				String description = trustedCAs[i].getDescription() == null ? ""
						: trustedCAs[i].getDescription();
				MessageDialog.openWarning(window.getShell(), "Warning",
						"Problems with adding certificate " + description
								+ " to keystore: \n" + e.getMessage());
			}
		}

		try {
			tcc.reWriteKeyStore();
		} catch (Exception e) {
			MessageDialog.openError(window.getShell(), "Error",
					"Error saving new keystore content:" + e.getMessage());
		}
		return errorCount;
	}

	/**
	 * Method downloading certificates from addresses in the trusted CA
	 * certificates list of client profile. Resulting array has the same length,
	 * and when there was problem with particular CA certificate, there is a
	 * null entry in this array at corresponding index.
	 * 
	 * @param trustedCAs
	 * @return X509Certificate array
	 * @throws CertificateException
	 */
	private X509Certificate[] downloadAvailableTrustedCACertificates(
			TrustedCAType[] trustedCAs) throws CertificateException {
		if (trustedCAs == null) {
			return null;
		}

		CertificateFactory factory = null;
		try {
			factory = CertificateFactory.getInstance("X.509");
		} catch (CertificateException e) {
			throw new CertificateException(
					"Error creating X509 certificate factory for CCM: "
							+ e.getMessage(), e);
		}

		X509Certificate[] caCerts = new X509Certificate[trustedCAs.length];
		for (int i = 0; i < trustedCAs.length; ++i) {
			String url = trustedCAs[i].getCertificateURL();
			String certificate;
			try {
				certificate = ProfileHandler.downloadToString(url,
						ProfileHandler.BUF_SIZE);
			} catch (IOException e) {
				caCerts[i] = null;
				continue;
			}

			String cleaned;
			try {
				cleaned = ProfileHandler.extractPEMContents(certificate);
				ByteArrayInputStream bais = new ByteArrayInputStream(
						cleaned.getBytes());
				caCerts[i] = (X509Certificate) factory
						.generateCertificate(bais);
			} catch (ParseException e) {
				caCerts[i] = null;
			}
		}
		return caCerts;
	}

	// TODO: handle URC plugin extensions
	protected void processProfileUrcPlugins(URCExtensionType[] urcExtensions) {
		if (urcExtensions == null) {
			return;
		}
	}

	/**
	 * Method downloading client's profile from address using certificate with
	 * specified fingerprint.
	 * 
	 * @param address
	 * @param fingerprint
	 * @return client's profile
	 */
	protected ClientProfile downloadClientProfile(String address,
			String fingerprint) {
		CCMManager ccmManager = new CCMManager();
		ClientProfile profile = null;
		try {
			profile = ccmManager.download(address, fingerprint);
		} catch (SSLHandshakeException e) {
			MessageDialog.openError(window.getShell(), "Error",
					"The profile can not be downloaded from the server,"
							+ " as the SSL connection can not be verified: \n"
							+ e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			MessageDialog.openError(window.getShell(), "Error",
					"There was a communication problem: \n" + e.getMessage());
			e.printStackTrace();
		} catch (XmlException e) {
			MessageDialog.openError(
					window.getShell(),
					"Error",
					"Profile was downloaded but is invalid: \n"
							+ e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			MessageDialog.openError(window.getShell(), "Error",
					"Error downloading a profile: \n" + e.getMessage());
			e.printStackTrace();
		}
		return profile;
	}

}
