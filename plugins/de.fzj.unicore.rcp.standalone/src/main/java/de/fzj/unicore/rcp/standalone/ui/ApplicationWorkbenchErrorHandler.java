package de.fzj.unicore.rcp.standalone.ui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.statushandlers.StatusAdapter;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.statushandlers.WorkbenchErrorHandler;

import de.fzj.unicore.rcp.logmonitor.LogActivator;
import de.fzj.unicore.rcp.standalone.Activator;

public class ApplicationWorkbenchErrorHandler extends WorkbenchErrorHandler {

	
	public void handle(final StatusAdapter statusAdapter, int style) {
		if (((style & StatusManager.SHOW) == StatusManager.SHOW)
				|| ((style & StatusManager.BLOCK) == StatusManager.BLOCK)) {

			final boolean block = ((style & StatusManager.BLOCK) == StatusManager.BLOCK);
			
			if (Display.getCurrent() != null) {
				showStatusAdapter(statusAdapter, block);
			} else {
				if (block) {
					Display.getDefault().syncExec(new Runnable() {
						public void run() {
							showStatusAdapter(statusAdapter, true);
						}
					});

				} else {
					Display.getDefault().asyncExec(new Runnable() {
						public void run() {
							showStatusAdapter(statusAdapter, false);
						}
					});
				}
			}

		}

		if ((style & StatusManager.LOG) == StatusManager.LOG) {
			StatusManager.getManager().addLoggedStatus(
					statusAdapter.getStatus());
			Activator.getDefault().getLog()
					.log(statusAdapter.getStatus());
		}
	}
	
	@SuppressWarnings("deprecation") 
	protected void showStatusAdapter(StatusAdapter statusAdapter, boolean block) {
		if (!PlatformUI.isWorkbenchRunning()) {
			// we are shutting down, so just log
			Activator.getDefault().getLog().log(statusAdapter.getStatus());
			return;
		}
		IStatus s = statusAdapter.getStatus();
		
		if(LogActivator.getAlarmUser(s)) 
		{
			// we don't have any more information so we gotta use the deprecated
			// version
			LogActivator.showAlarm(s);
		}

	}
	
	
	
}
