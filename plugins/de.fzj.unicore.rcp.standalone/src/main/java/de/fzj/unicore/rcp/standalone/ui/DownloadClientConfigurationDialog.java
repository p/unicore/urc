package de.fzj.unicore.rcp.standalone.ui;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.standalone.actions.DownloadClientConfigurationAction;

/**
 * @author Rafal
 * 
 */
public class DownloadClientConfigurationDialog extends Dialog {

	protected Text urlTextField;
	protected Text fingerprintTextField;
	protected Button backupOptionButton;

	protected String urlText, fingerprintText;
	protected boolean backupOption;

	public DownloadClientConfigurationDialog(Shell parentShell) {
		super(parentShell);
		this.urlText = "";
		this.fingerprintText = "";
		this.backupOption = false;
	}

	@Override
	public boolean close() {
		urlText = urlTextField.getText();
		fingerprintText = fingerprintTextField.getText();
		backupOption = backupOptionButton.getSelection();
		return super.close();
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		FillLayout fillLayout = new FillLayout(SWT.VERTICAL);
		fillLayout.marginHeight = 10;
		fillLayout.marginWidth = 10;
		fillLayout.spacing = 5;
		composite.setLayout(fillLayout);
		composite.getShell().setText(
				DownloadClientConfigurationAction.INFORMATION_DIALOG_TITLE);
		composite.getShell().setMinimumSize(400, 80);

		Label urlLabel = new Label(composite, SWT.NONE);
		urlLabel.setFont(parent.getFont());
		urlLabel.setText("Enter address of client configuration profile:");

		urlTextField = new Text(composite, SWT.SINGLE | SWT.CENTER);
		urlTextField.setFont(parent.getFont());
		urlTextField.setText("");

		Label fingerprintLabel = new Label(composite, SWT.NONE);
		fingerprintLabel.setFont(parent.getFont());
		fingerprintLabel.setText("Enter fingerprint:");

		fingerprintTextField = new Text(composite, SWT.SINGLE | SWT.CENTER);
		fingerprintTextField.setFont(parent.getFont());
		fingerprintTextField.setText("");

		Label emptyLabel = new Label(composite, SWT.NONE);
		emptyLabel.setFont(parent.getFont());
		emptyLabel.setText("");

		backupOptionButton = new Button(composite, SWT.CHECK | SWT.LEFT);
		backupOptionButton.setFont(parent.getFont());
		backupOptionButton.setText("Backup keystore");
		backupOptionButton.setSelection(this.backupOption);

		return composite;
	}

	public String getUrlText() {
		return urlText;
	}

	public String getFingerprintText() {
		return fingerprintText;
	}

	public boolean getBackupOption() {
		return backupOption;
	}
}
