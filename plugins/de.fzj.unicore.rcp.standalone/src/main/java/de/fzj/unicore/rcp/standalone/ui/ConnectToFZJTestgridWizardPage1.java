package de.fzj.unicore.rcp.standalone.ui;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.common.utils.IValidityChangeListener;
import de.fzj.unicore.rcp.common.utils.ValidityChangeEvent;
import de.fzj.unicore.rcp.identity.DistinguishedName;
import de.fzj.unicore.rcp.identity.ui.dialogs.CertificateRequestComposite;

public class ConnectToFZJTestgridWizardPage1 extends WizardPage implements IValidityChangeListener{

	CertificateRequestComposite userData;
	
	protected ConnectToFZJTestgridWizardPage1() {
		super("","Enter user data for your test account",null);
	}

	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);

		initializeDialogUnits(parent);

		composite.setLayout(new GridLayout());
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));
	
		userData = new CertificateRequestComposite(composite, null);
		GridData data = new GridData(SWT.LEFT,SWT.TOP,false,false);
		userData.setLayoutData(data);
		Dialog.applyDialogFont(composite);

		setControl(userData);
		setMessage("The entered data is not validated and will NOT be used for ANY purpose "+"\n"+"other than obtaining an overview of who is using our testgrid.");
		setPageComplete(false);
		userData.addValidityChangeListener(this);
	}
	
	public DistinguishedName getDN()
	{
		return userData == null ? null : userData.getDN();
	}

	public void validityChanged(ValidityChangeEvent e) {
		setPageComplete(e.getNewValidity());
		
	}



}
