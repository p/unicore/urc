package de.fzj.unicore.rcp.standalone.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.common.detailsView.DetailsView;
import de.fzj.unicore.rcp.gpe4eclipse.views.JobOutcomeView;
import de.fzj.unicore.rcp.identity.keystore.KeystoreView;
import de.fzj.unicore.rcp.identity.sites.SiteListView;
import de.fzj.unicore.rcp.identity.truststore.TruststoreView;
import de.fzj.unicore.rcp.logmonitor.LogView;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceView;
import de.fzj.unicore.rcp.terminal.ui.views.TerminalConfigView;
import de.fzj.unicore.rcp.terminal.ui.views.TerminalView;

public class UnicorePerspective implements IPerspectiveFactory{

	public static String ID = "de.fzj.unicore.rcp.standalone.perspectives.default";
	
	
	
	public void createInitialLayout(IPageLayout layout) {


		String editorArea = layout.getEditorArea();
		layout.addPerspectiveShortcut(ID);
		
		IFolderLayout leftTop = layout.createFolder("left_top", IPageLayout.LEFT, .5f, editorArea);
		leftTop.addView(ServiceView.ID);
		
		
		IFolderLayout leftBottom = layout.createFolder("left_bottom", IPageLayout.BOTTOM, .5f, "left_top");
		leftBottom.addView(IPageLayout.ID_RES_NAV);
		leftBottom.addView(KeystoreView.ID);
		leftBottom.addView(TruststoreView.ID);
		leftBottom.addView(LogView.ID);
		leftBottom.addPlaceholder(SiteListView.ID);
		leftBottom.addPlaceholder(DetailsView.ID);
		leftBottom.addPlaceholder("org.eclipse.ui.views.PropertySheet");
		leftBottom.addPlaceholder(JobOutcomeView.ID + ":*");
		leftBottom.addPlaceholder(TerminalView.ID);
		leftBottom.addPlaceholder(TerminalConfigView.ID);
		
		IFolderLayout right = layout.createFolder("right", IPageLayout.RIGHT, .2f, editorArea);
		right.addPlaceholder("de.fzj.unicore.rcp.servicebrowser.metadata.*");
		
		setGridDetailLevel(UnicoreCommonActivator.getDefault().getGridDetailLevel(),layout);

	}

	protected void setGridDetailLevel(int level, IPageLayout layout)
	{
		boolean layoutModifiable = level > UnicoreCommonActivator.LEVEL_BEGINNER;
		layout.getViewLayout(ServiceView.ID).setCloseable(layoutModifiable);
		layout.getViewLayout(ServiceView.ID).setMoveable(layoutModifiable);
		layout.getViewLayout(IPageLayout.ID_RES_NAV).setCloseable(layoutModifiable);
		layout.getViewLayout(IPageLayout.ID_RES_NAV).setMoveable(layoutModifiable);
		layout.getViewLayout(KeystoreView.ID).setCloseable(layoutModifiable);
		layout.getViewLayout(KeystoreView.ID).setMoveable(layoutModifiable);
		layout.getViewLayout(TruststoreView.ID).setCloseable(layoutModifiable);
		layout.getViewLayout(TruststoreView.ID).setMoveable(layoutModifiable);
		layout.setFixed(false);
		
	}
	
	
}
