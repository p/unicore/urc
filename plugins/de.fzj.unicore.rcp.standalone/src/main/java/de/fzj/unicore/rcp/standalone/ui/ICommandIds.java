package de.fzj.unicore.rcp.standalone.ui;

/**
 * Interface defining the application's command IDs.
 * Key bindings can be defined for specific commands.
 * To associate an action with a command, use IAction.setActionDefinitionId(commandId).
 *
 * @see org.eclipse.jface.action.IAction#setActionDefinitionId(String)
 */
public interface ICommandIds {

    public static final String CMD_CONFIGURE_SECURITY = "de.fzj.unicore.rcp.standalone.cmd.configureSecurity";
    public static final String CMD_CONNECT_TO_FZJ_TESTGRID = "de.fzj.unicore.rcp.standalone.cmd.connectToFZJTestgrid";

    public static final String CMD_DOWNLOAD_CONFIGURATION_PROFILE = "de.fzj.unicore.rcp.standalone.cmd.downloadConfigurationProfile";

}
