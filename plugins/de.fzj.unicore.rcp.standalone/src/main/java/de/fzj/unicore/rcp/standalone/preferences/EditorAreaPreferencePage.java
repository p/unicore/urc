package de.fzj.unicore.rcp.standalone.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.fzj.unicore.rcp.standalone.Activator;

/**
 * This class represents a UNICORE preference page that
 * is contributed to the Preferences dialog.
 * 
 * @author Bastian Demuth, Valentina Huber
 * @version $Id: EditorAreaPreferencePage.java,v 1.1 2007/10/02 09:01:10 vhuber Exp $
 */

public class EditorAreaPreferencePage
	extends FieldEditorPreferencePage
	implements IWorkbenchPreferencePage {

	public static final String P_AUTO_CLOSE_EDITOR_AREA = "close editor area automatically";
	
	
	public EditorAreaPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Preferences for the Editor Area.");
	}
	
	
	
	
	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */
	public void createFieldEditors() {
		
	
		BooleanFieldEditor closeEditorArea = new BooleanFieldEditor(P_AUTO_CLOSE_EDITOR_AREA,"Close the editor area when all editors have been closed ",getFieldEditorParent());
		addField(closeEditorArea);
		
	}

	public void init(IWorkbench workbench) {

		
	}


	
}