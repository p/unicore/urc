/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.wfeditor.utils.GridBeanUtils;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.gef.commands.Command;

import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.clients.common.transfers.AddressOrValue;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.EnvironmentVariableParameterValue;
import com.intel.gpe.gridbeans.parameters.IEnvironmentVariableParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowVariableSink;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifier;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;
import de.fzj.unicore.rcp.wfeditor.utils.TransitionUtils;
import de.fzj.unicore.uas.util.Pair;

/**
 * Instead of directly linking an input environment variable to a workflow
 * variable, it can also be linked to a {@link WorkflowVariableModifier}. Behind
 * the scenes, it is linked to the workflow variable that is modified by the
 * modifier, but it is displayed as being linked with the modifier.
 * 
 * @author demuth
 * 
 */

@XStreamAlias("VariableModifierLinker")
public class VariableModifierLinker implements PropertyChangeListener,
		IDataFlow {

	private class GridBeanUpdater extends BackgroundJob {
		Queue<Pair<VariableModifierLinker, PropertyChangeEvent>> events = new ConcurrentLinkedQueue<Pair<VariableModifierLinker, PropertyChangeEvent>>();

		public GridBeanUpdater() {
			super("updating linked environment variable");

			setSystem(true);

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.
		 * IProgressMonitor)
		 */
		@Override
		protected IStatus run(IProgressMonitor monitor) {

			while (!events.isEmpty()) {
				Pair<VariableModifierLinker, PropertyChangeEvent> p = events
						.poll();
				VariableModifierLinker linker = p.getM1();
				PropertyChangeEvent evt = p.getM2();
				linker.doHandleEvent(evt);
			}
			return Status.OK_STATUS;
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1524873417759433393L;
	private WorkflowVariableModifier linkTarget;
	private IAdaptable activity;

	private IGridBeanParameter param;
	private String id;
	private static GridBeanUpdater updater;
	private transient boolean disposed;
	private transient boolean relinkedAfterDeserialization = false;
	private transient boolean listeningToModel = true;
	private transient int sourceIndex = -1;
	private transient int sinkIndex = -1;

	private transient GPEWorkflowVariableSink sink = null;
	private transient IEnvironmentVariableParameterValue removedEnvVariable;
	private transient boolean checkedObsoleteLinks = false;
	private transient boolean invalidated = false;

	private transient Command createTransitionCommand;

	public VariableModifierLinker(WorkflowVariableModifier linkTarget,
			GPEWorkflowVariableSink sink) {
		this.sink = sink;
		this.linkTarget = linkTarget;
		this.activity = sink.getFlowElement();
		this.param = sink.getParameter();
		relinkedAfterDeserialization = true;
		id = UUID.randomUUID().toString();
	}

	public VariableModifierLinker(WorkflowVariableModifier linkTarget,
			IGridBeanParameter param, IAdaptable activity) {
		this.linkTarget = linkTarget;
		this.activity = activity;
		this.param = param;
		id = UUID.randomUUID().toString();
	}

	private boolean activityNameChanged(PropertyChangeEvent evt) {
		String s = evt.getPropertyName();
		return IFlowElement.PROP_NAME.equals(s);
	}

	protected void addToSourceAndSink(int sourceIndex, int sinkIndex) {
		if (!getDataSource().hasOutgoingFlow(this)) {
			getDataSource().addOutgoingFlow(this, sourceIndex);
		}
		if (!getDataSink().hasIncomingFlow(this)) {
			getDataSink().addIncomingFlow(this, sinkIndex);
		}
	}

	public void afterDeserialization() {
		// TODO Auto-generated method stub

	}

	public void afterSerialization() {
		// TODO Auto-generated method stub

	}

	public void beforeSerialization() {
		// TODO Auto-generated method stub

	}

	private void checkObsoleteLinks() {
		// for backwards compatibility remove ourselves as persistent listeners
		// of the link target and its parent activity
		if (linkTarget != null) {
			if (linkTarget.isAlreadyListeningPersistently(this)) {
				linkTarget.removePersistentPropertyChangeListener(this);
			}
			IFlowElement element = linkTarget.getFlowElement();
			if (element != null && element.isAlreadyListeningPersistently(this)) {
				element.removePersistentPropertyChangeListener(this);
			}
		}
		checkedObsoleteLinks = true;

	}

	public void connect() {
		reconnect();
	}

	public void disconnect() {

		IEnvironmentVariableParameterValue env = (IEnvironmentVariableParameterValue) getGridBean()
				.get(param.getName());
		removedEnvVariable = env;
		env = new EnvironmentVariableParameterValue();
		if (removedEnvVariable != null) {
			env.setMetaData(removedEnvVariable.getMetaData());
		}
		getGridBean().set(param.getName(), env);
		unlink();
		if (createTransitionCommand != null) {
			createTransitionCommand.undo();
		}
	}

	private void doHandleEvent(PropertyChangeEvent evt) {

		if (haveIBecomeValidAgain(evt)) {
			List<PropertyChangeListener> listeners = new ArrayList<PropertyChangeListener>(
					getActivity().getPersistentListeners());
			for (PropertyChangeListener l : listeners) {
				if ((l instanceof VariableModifierLinker)) {
					return; // another linker is sitting on this value => we are
							// not welcome anymore
				}
			}
			if (removedEnvVariable != null) {
				getGridBean().set(param.getName(), removedEnvVariable);
				removedEnvVariable = null;
			}
			relink();
			addToSourceAndSink(sourceIndex, sinkIndex);
			invalidated = false;
		} else if (!isConnected()) {
			return;
		}
		if (haveIBecomeInvalid(evt)) {
			sourceIndex = linkTarget.getDataSource().removeOutgoingFlow(this);
			getActivity().removePersistentPropertyChangeListener(this);
			if (sink != null) {
				sinkIndex = sink.removeIncomingFlow(this);
			}
			IEnvironmentVariableParameterValue env = (IEnvironmentVariableParameterValue) getGridBean()
					.get(param.getName());
			removedEnvVariable = env;
			env = new EnvironmentVariableParameterValue();
			if (removedEnvVariable != null) {
				env.setMetaData(removedEnvVariable.getMetaData());
			}
			getGridBean().set(param.getName(), env);
			invalidated = true;
		} else if (parentChanged(evt) || activityNameChanged(evt)
				|| workflowVarChanged(evt)) {
			IEnvironmentVariableParameterValue oldValue = getValueFromGridBean();
			if (oldValue == null) {
				return;
			}
			IEnvironmentVariableParameterValue newValue = getCurrentValue(
					oldValue, linkTarget);
			updateGridBeanModel(newValue);
		}

	}

	private IActivity getActivity() {
		try {
			IActivity act = (IActivity) activity.getAdapter(IActivity.class);
			return act;
		} catch (Exception e) {
			return null;
		}
	}

	protected String getActivityNameWithSeperator(String activityName) {
		return activityName + WFConstants.DISPLAYED_STRING_SEPERATOR;
	}

	public ICopyable getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		VariableModifierLinker copy = null;
		WorkflowVariableModifier modifier = linkTarget;
		if (modifier == null) {
			return null;
		}

		GPEWorkflowVariableSink sink = getDataSink();
		IFlowElement sourceParent = linkTarget.getFlowElement();
		IActivity sinkParent = getActivity();

		boolean isCut = (copyFlags & ICopyable.FLAG_CUT) != 0;
		boolean copySourceParent = CloneUtils.isBeingCopied(sourceParent,
				toBeCopied);
		boolean copySinkParent = CloneUtils.isBeingCopied(sinkParent,
				toBeCopied);

		if (!isCut) {
			// this is not a cut => if the target activity is not copied
			// we shouldn't be copied either!
			if (!copySinkParent) {
				return null;
			}
		}

		if (copySourceParent) {
			sourceParent = CloneUtils.getFromMapOrCopy(sourceParent,
					toBeCopied, mapOfCopies, copyFlags);
			modifier = CloneUtils.getFromMapOrCopy(modifier, toBeCopied,
					mapOfCopies, copyFlags);
		}
		if (copySinkParent) {
			sinkParent = CloneUtils.getFromMapOrCopy(sinkParent, toBeCopied,
					mapOfCopies, copyFlags);
			sink = CloneUtils.getFromMapOrCopy(sink, toBeCopied, mapOfCopies,
					copyFlags);
		}

		if (mapOfCopies.get(this) == null) {
			copy = new VariableModifierLinker(modifier, sink);

			IDataSource source = modifier.getDataSource();
			// link to object subgraph that is being copied
			// do NOT link to the diagram itself (the whole object graph)
			if (copySourceParent) {
				if (!source.hasOutgoingFlow(copy)) {
					source.addOutgoingFlow(copy);
				}
			}
			if (copySinkParent) {
				if (!sink.hasIncomingFlow(copy)) {
					sink.addIncomingFlow(copy);
				}
			}
		} else {
			return (ICopyable) mapOfCopies.get(this);
		}

		return copy;
	}

	public IEnvironmentVariableParameterValue getCurrentValue(
			IEnvironmentVariableParameterValue oldValue,
			WorkflowVariableModifier var) {

		IEnvironmentVariableParameterValue result = oldValue.clone();
		result.setVariableValue(GridBeanUtils.createAddressFromVariableName(var
				.getVariableName()));
		return result;
	}

	public GPEWorkflowVariableSink getDataSink() {
		if (sink == null) {
			restoreSink();
		}
		return sink;
	}

	public IDataSource getDataSource() {
		return linkTarget.getDataSource();
	}


	public IGridBean getGridBean() {
		try {
			IGridBean gb = (IGridBean) activity.getAdapter(IGridBean.class);
			return gb;
		} catch (Exception e) {
			return null;
		}
	}

	public String getId() {
		return id;
	}

	public IGridBeanParameter getParam() {
		return param;
	}

	public QName getType() {
		return IDataFlow.TYPE;
	}

	private IEnvironmentVariableParameterValue getValueFromGridBean() {
		IEnvironmentVariableParameterValue result = (IEnvironmentVariableParameterValue) getGridBean()
				.get(param.getName());
		return result;
	}

	protected WorkflowVariable getWorkflowVariable() {
		try {
			return linkTarget.getFlowElement().getDiagram()
					.getVariable(linkTarget.getVariableName());
		} catch (Exception e) {
			return null;
		}

	}

	private boolean haveIBecomeInvalid(PropertyChangeEvent evt) {

		if (invalidated || !isConnected()) {
			return false;
		}
		if (getGridBean() == null) {
			return false; // this may happen during copy&paste
		}
		IEnvironmentVariableParameterValue value = (IEnvironmentVariableParameterValue) getGridBean()
				.get(getParam().getName());
		Class<?> clazz = value.getTargetType();
		if (clazz == null) {
			return false;
		}
		Set<String> needed = GPEWorkflowVariableSink.classToTypeSet(clazz);
		if (needed.isEmpty()) {
			return false;
		}
		Set<String> provided = getWorkflowVariable() == null ? new HashSet<String>()
				: getWorkflowVariable().getOutgoingDataTypes();
		needed.retainAll(provided);
		return needed.isEmpty();
	}

	private boolean haveIBecomeValidAgain(PropertyChangeEvent evt) {
		if (!invalidated) {
			return false;
		}
		IEnvironmentVariableParameterValue value = (IEnvironmentVariableParameterValue) getGridBean()
				.get(getParam().getName());
		Class<?> clazz = value.getTargetType();
		Set<String> needed = GPEWorkflowVariableSink.classToTypeSet(clazz);
		if (needed.isEmpty()) {
			return true;
		}
		Set<String> provided = getWorkflowVariable() == null ? new HashSet<String>()
				: getWorkflowVariable().getOutgoingDataTypes();
		needed.retainAll(provided);
		return !needed.isEmpty();
	}

	public Set<Class<?>> insertAfter() {
		Set<Class<?>> result = new HashSet<Class<?>>();
		result.add(IActivity.class);
		result.add(Transition.class);
		return result;
	}

	public Set<Class<?>> insertBefore() {
		return new HashSet<Class<?>>(); // we don't care
	}

	public void insertCopyIntoDiagram(ICopyable c, StructuredActivity parent,
			Set<IFlowElement> toBeCopied, Map<Object, Object> mapOfCopies,
			int copyFlags) {
		// only fully connect to the diagram after pasting
		VariableModifierLinker copy = (VariableModifierLinker) c;

		copy.removedEnvVariable = copy.getCurrentValue(getValueFromGridBean(),
				copy.linkTarget);
		copy.removedEnvVariable = copy.removedEnvVariable.clone();
		copy.connect();
		if (copy.linkTarget.getFlowElement() instanceof IActivity) {
			IActivity source = (IActivity) copy.linkTarget.getFlowElement();
			copy.createTransitionCommand = TransitionUtils
					.ensureControlFlowBetween(source, copy.getActivity());
			if (copy.createTransitionCommand != null) {
				copy.createTransitionCommand.execute();
			}
		}
	}

	public boolean isConnected() {
		return sink != null && sink.hasIncomingFlow(this);

	}

	public boolean isDisposed() {
		return disposed;
	}

	private boolean isRelevant(PropertyChangeEvent evt) {
		return haveIBecomeInvalid(evt) || haveIBecomeValidAgain(evt)
				|| parentChanged(evt) || workflowVarChanged(evt)
				|| activityNameChanged(evt);
	}

	public boolean isVisible() {
		IDataSource source = getDataSource();
		IDataSink sink = getDataSink();
		if (source == null || sink == null) {
			return false;
		}
		IFlowElement sourceElt = source.getFlowElement();
		IFlowElement sinkElt = sink.getFlowElement();
		if (sourceElt == null || sinkElt == null) {
			return false;
		}

		ShowDataFlowsProperty sourceProp = (ShowDataFlowsProperty) sourceElt
				.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		ShowDataFlowsProperty sinkProp = (ShowDataFlowsProperty) sinkElt
				.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		if (sourceProp == null || sinkProp == null) {
			return false;
		}
		return source.isVisible()
				&& sink.isVisible()
				&& (sourceProp.isShowingOutgoing() || sinkProp
						.isShowingIncoming());
	}

	private boolean parentChanged(PropertyChangeEvent evt) {
		return IFlowElement.PARENT.equals(evt.getPropertyName())
				&& evt.getNewValue() != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		if (!listeningToModel) {
			return;
		}
		if (!checkedObsoleteLinks) {
			checkObsoleteLinks(); // remove old persistent links that we don't
									// want anymore
		}
		if (!relinkedAfterDeserialization) {
			restoreSink();
			if (sink != null) {
				relink();
				addToSourceAndSink(sourceIndex, sinkIndex);
				relinkedAfterDeserialization = true;
			}
		}
		if (isRelevant(evt)) {
			if (updater == null) {
				updater = new GridBeanUpdater();
			}
			updater.events
					.add(new Pair<VariableModifierLinker, PropertyChangeEvent>(
							this, evt));
			if (updater.getState() != Job.RUNNING) {
				updater.schedule(50);
			}
		}

	}

	private Object readResolve() {
		listeningToModel = true;

		return this;
	}

	public void reconnect() {
		if (linkTarget.isDisposed() || linkTarget.getFlowElement().isDisposed()
				|| sink == null || sink.isDisposed()
				|| sink.getFlowElement() == null
				|| sink.getFlowElement().isDisposed()) {
			return; // try to avoid restoring invalid links
		}
		if (removedEnvVariable != null) {
			getGridBean().set(param.getName(), removedEnvVariable);
			removedEnvVariable = null;
		}
		relink();
		addToSourceAndSink(sourceIndex, sinkIndex);
		if (createTransitionCommand != null) {
			createTransitionCommand.redo();
		}
	}

	public void relink() {

		// link to target and own activity
		// no need to worry about linking yourself twice,
		// property sources don't allow this anyway
		relinkToTarget();
		getActivity().addPersistentPropertyChangeListener(this);
	}

	private void relinkToTarget() {
		linkTarget.addPropertyChangeListener(this);
		if (linkTarget.getVariable() != null) {
			linkTarget.getVariable().addPropertyChangeListener(this);
		}
		IFlowElement element = linkTarget.getFlowElement();
		element.addPropertyChangeListener(this);
	}

	protected String removeActivityNameWithSeperator(String displayed,
			String activityName) {
		return displayed.replace(activityName
				+ WFConstants.DISPLAYED_STRING_SEPERATOR, "");
	}

	protected void removeFromSourceAndSink() {
		if (getDataSource().hasOutgoingFlow(this)) {
			sourceIndex = getDataSource().removeOutgoingFlow(this);
		}
		if (getDataSink().hasIncomingFlow(this)) {
			sinkIndex = getDataSink().removeIncomingFlow(this);
		}
	}

	protected void restoreSink() {
		for (IDataSink sink : getActivity().getDataSinkList().getDataSinks()) {
			if (param.getName().toString().equals(sink.getId())) {
				this.sink = (GPEWorkflowVariableSink) sink;
				break;
			}
		}
	}

	public void unlink() {
		unlinkFromTarget();
		getActivity().removePersistentPropertyChangeListener(this);
		if (sink != null) {
			sinkIndex = sink.removeIncomingFlow(this);
		}
	}

	private void unlinkFromTarget() {
		linkTarget.removePropertyChangeListener(this);
		if (linkTarget.getVariable() != null) {
			linkTarget.getVariable().removePropertyChangeListener(this);
		}
		sourceIndex = linkTarget.getDataSource().removeOutgoingFlow(this);
		IFlowElement element = linkTarget.getFlowElement();
		element.removePropertyChangeListener(this);
	}

	protected void updateGridBeanModel(
			IEnvironmentVariableParameterValue newValue) {
		if (newValue == null) {
			newValue = new EnvironmentVariableParameterValue();
			AddressOrValue address = new AddressOrValue("", "",
					ProtocolConstants.VALUE_PROTOCOL);
			newValue.setVariableValue(address);
		}
		getGridBean().set(param.getName(), newValue);

	}

	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		// TODO Auto-generated method stub

	}

	private boolean workflowVarChanged(PropertyChangeEvent evt) {
		String s = evt.getPropertyName();
		return linkTarget.equals(evt.getSource())
				&& WorkflowVariableModifier.PROP_VARIABLE_NAME.equals(evt
						.getPropertyName())
				|| linkTarget.getVariable() != null
				&& linkTarget.getVariable().equals(evt.getSource())
				&& (WorkflowVariable.PROP_NAME.equals(s)
						|| WorkflowVariable.PROP_FLOW_ELEMENT.equals(s)
						|| WorkflowVariable.PROP_ID.equals(s)
						|| WorkflowVariable.PROP_INITIAL_VALUE.equals(s)
						|| WorkflowVariable.PROP_PROTOCOL.equals(s) || WorkflowVariable.PROP_TYPE
						.equals(s));
	}

	private Object writeReplace() {
		if (!checkedObsoleteLinks) {
			checkObsoleteLinks();
		}
		return this;
	}
}
