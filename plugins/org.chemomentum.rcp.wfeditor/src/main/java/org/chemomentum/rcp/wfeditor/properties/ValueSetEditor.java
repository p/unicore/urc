/*********************************************************************************
 * Copyright (c) 2016 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.properties;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

/**
 * @author bjoernh
 *
 * 07.07.2016 16:41:19
 *
 */
public class ValueSetEditor extends Composite {
	public static final String PROP_VALUE_SET = "Value Set";

	private Table table;
	
	private List<ValueSetElement> valueSet;

	private final TableViewer tableViewer;
	
	private final List<PropertyChangeListener> listeners;

	private Button btnRemove;

	private Button btnAdd;
	private Button btnLoad;
	
	/**
	 * This is required to allow for editing values in the value set. We cannot
	 * use {@link String}s directly, because they are immutable.
	 * 
	 * This class is for internal use only.
	 * 
	 * @author bjoernh
	 */
	private class ValueSetElement {
		private String value;
		
		
		/**
		 * @param _value
		 */
		public ValueSetElement(String _value) {
			value = _value;
		}

		/**
		 * @param value the value to set
		 */
		public void setValue(String value) {
			this.value = value;
		}
		
		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}
		/**
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return value;
		}
	}

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public ValueSetEditor(Composite parent, int style) {
		super(parent, style);
		listeners = new ArrayList<PropertyChangeListener>();
		valueSet = new ArrayList<ValueSetElement>();
		setLayout(new GridLayout(2, false));
		
		tableViewer = new TableViewer(this, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
		table = tableViewer.getTable();
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 3));
		
		TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		tableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			public Image getImage(Object element) {
				return null;
			}
			public String getText(Object element) {
				return element == null ? "" : element.toString();
			}
		});
		
		tableViewerColumn.setEditingSupport(new EditingSupport(tableViewer) {
			CellEditor editor = new TextCellEditor(tableViewer.getTable());
			
			protected boolean canEdit(Object element) {
				return true;
			}
			protected CellEditor getCellEditor(Object element) {
				return editor;
			}
			protected Object getValue(Object element) {
				return ((ValueSetElement) element).getValue();
			}
			protected void setValue(Object element, Object value) {
				if(!((ValueSetElement) element).getValue().equals(value)) {
					((ValueSetElement) element).setValue((String) value);
					valueSetChanged();
					tableViewer.refresh();
				}
			}
		});
		
		TableColumn tblclmnValue = tableViewerColumn.getColumn();
		tblclmnValue.setWidth(100);
		tblclmnValue.setText("Value");
		
		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setInput(valueSet);
		
		btnAdd = new Button(this, SWT.NONE);
		btnAdd.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnAdd.addSelectionListener(new SelectionAdapter() {
			int counter = 0;
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				addValue();
			}
			@Override
			public void widgetSelected(SelectionEvent e) {
				addValue();
			}
			
			private void addValue() {
				valueSet.add(new ValueSetElement(Integer.toString(counter++)));
				valueSetChanged();
				tableViewer.refresh();
			}
		});
		btnAdd.setText("Add");
		
		btnRemove = new Button(this, SWT.NONE);
		btnRemove.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		btnRemove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				removeSelectedValues();
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				removeSelectedValues();
			}
			
			private void removeSelectedValues() {
				IStructuredSelection selection = (IStructuredSelection) tableViewer.getSelection();
				Iterator<ValueSetElement> it = selection.iterator();
				while (it.hasNext()) {
					ValueSetElement value = (ValueSetElement) it.next();
					valueSet.remove(value);
				}
				valueSetChanged();
				tableViewer.refresh();
			}
		});
		btnRemove.setText("Remove");

		btnLoad = new Button(this, SWT.NONE);
		btnLoad.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fd = new FileDialog(getShell());
				String path = fd.open();
				if(path != null && ! path.isEmpty()) {
					BufferedReader reader = null;
					try {
						reader = new BufferedReader(new FileReader(path));
						String line = null;
						// clear current valueSet, if everything went well so far
						valueSet.clear();
						while (	(line = reader.readLine()) != null) {
							valueSet.add(new ValueSetElement(line));
						};
					} catch (FileNotFoundException e1) {
						WFEditorActivator.log(IStatus.WARNING, "Cannot read value set from file: " + e1.getMessage(),
								e1);
					} catch (IOException e1) {
						WFEditorActivator.log(IStatus.WARNING, "Error when reading value set file: " + e1.getMessage(),
								e1);
					} finally {
						if(reader != null) {
							try {
								reader.close();
							} catch (IOException e1) {
								// ignore
							}
						}
					}
					valueSetChanged();
					tableViewer.refresh();
				}
			}
		});
		btnLoad.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		btnLoad.setText("Load...");

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	private void valueSetChanged() {
		for (PropertyChangeListener pcl : listeners) {
			pcl.propertyChange(new PropertyChangeEvent(this, PROP_VALUE_SET, null, null));
		}
		
	}

	public void setValueSet(List<String> _elements) {
		if(_elements != null) {
			valueSet = new ArrayList<ValueSetElement>();
			for (String element : _elements) {
				valueSet.add(new ValueSetElement(element));
			}
			tableViewer.setInput(valueSet);
		} else {
			tableViewer.setInput(null);
		}
	}

	/**
	 * @return
	 */
	public List<String> getValueSet() {
		List<String> values = new ArrayList<String>();
		if(valueSet != null) {
			for (ValueSetElement valueSetElement : valueSet) {
				values.add(valueSetElement.getValue());
			}
		}
		return values;
	}
	
	/**
	 * @see org.eclipse.swt.widgets.Control#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(boolean enabled) {
		table.setEnabled(enabled);
		btnAdd.setEnabled(enabled);
		btnRemove.setEnabled(enabled);
		btnLoad.setEnabled(enabled);
		super.setEnabled(enabled);
	}
	
	public void addPropertyChangeListener(PropertyChangeListener _listener) {
		listeners.add(_listener);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener _listener) {
		listeners.remove(_listener);
	}

}
