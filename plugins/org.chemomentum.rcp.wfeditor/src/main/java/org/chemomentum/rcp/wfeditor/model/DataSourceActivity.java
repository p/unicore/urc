package org.chemomentum.rcp.wfeditor.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.chemomentum.rcp.wfeditor.utils.SimpleWFUtil;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.intel.gpe.clients.all.ClientAdapter;
import com.intel.gpe.clients.all.gridbeans.GridBeanJob;
import com.intel.gpe.clients.all.gridbeans.GridBeanJobWrapper;
import com.intel.gpe.clients.all.gridbeans.InternalGridBean;
import com.intel.gpe.clients.all.gridbeans.InternalGridBeanImpl;
import com.intel.gpe.clients.all.utils.GPEFileFactoryImpl;
import com.intel.gpe.clients.api.Application;
import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.transfers.GPEFileFactory;
import com.intel.gpe.clients.common.ApplicationImpl;
import com.intel.gpe.clients.common.JAXBContextProvider;
import com.intel.gpe.gridbeans.GridBeanException;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.gridbeans.jsdl.GPEJobImpl;
import com.intel.gpe.gridbeans.parameters.GridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.plugins.swing.GridBeanPlugin;
import com.intel.gpe.util.collections.CollectionUtil;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.core.util.CompositeClassLoader;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;
import de.fzj.unicore.rcp.gpe4eclipse.utils.ParameterProcessingUtils;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.SimpleActivity;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

@XStreamAlias("DataSourceActivity")
@SuppressWarnings("rawtypes")
public class DataSourceActivity extends SimpleActivity implements
		PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -67438065303862579L;

	
	public static final QName TYPE = new QName("http://www.unicore.eu/",
			"DataSourceActivity");

	public static final String PROP_GRID_BEAN_JOB = "prop grid Bean Job";

	/**
	 * Key for Boolean property that indicates whether this data source will be
	 * used during workflow execution. If the data source is not used, it won't
	 * contribute to the resulting workflow and the controls for setting up the
	 * file set(s) are disabled.
	 */
	public static final String PROP_DATA_SOURCE_USED = "data source used";

	private transient IGridBean gridBean;

	private transient Client client;

	private WorkflowFile outputFile;

	public DataSourceActivity() {
		setPropertyValue(PROP_DATA_SOURCE_USED, true);

	}

	@Override
	public void activate() {

		super.activate();
		try {

			if (getOutputFile() == null) {
				WorkflowFile[] files = getOutputFiles();
				if (files != null && files.length > 0) {
					outputFile = files[0];
				} else {
					outputFile = createOutputFile();
				}
				if (isDataSourceUsed()) {
					addOutputFile(outputFile);
				} else {
					removeOutputFile(outputFile);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (isDataSourceUsed()) {
			if (getDataSourceList().getDataSourceById(outputFile.getId()) == null) {
				getDataSourceList().addDataSource(outputFile);
			}
		} else {
			getDataSourceList().removeDataSource(outputFile);
		}
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
		loadGridBeanFromProperty();
	}

	@Override
	public void afterSaveAs(IPath newLocation, boolean recursive,
			boolean fetchLocalInputFiles, boolean fetchRemoteInputFiles,
			IProgressMonitor monitor) throws Exception {
		super.afterSaveAs(newLocation, recursive, fetchLocalInputFiles,
				fetchRemoteInputFiles, monitor);
		setPropertyValue(PROP_GRID_BEAN_JOB, null);
	}

	@Override
	public void beforeSaveAs(IPath newLocation, boolean recursive,
			boolean fetchLocalInputFiles, boolean fetchRemoteInputFiles,
			IProgressMonitor monitor) throws Exception {
		super.beforeSaveAs(newLocation, recursive, fetchLocalInputFiles,
				fetchRemoteInputFiles, monitor);
		storeGridBeanInProperty();
	}

	@Override
	public void beforeSerialization() {
		super.beforeSerialization();
		storeGridBeanInProperty();
		if (getOutputFile() != null && getOutputFile().isDisposed()) {
			// we don't want to keep any references to the persisted inactive
			// output file
			getOutputFile().removeAllPersistentPropertyChangeListeners();
		}
	}

	/**
	 * Instead of loading a GridBean from a .jar file, use the gridbean classes
	 * in this package to create an InternalGridBean. This will mock as a
	 * "normal" GridBean.
	 * 
	 * @return
	 */
	public InternalGridBean createInternalGridBean() {
		Map<QName, String> plugins = new HashMap<QName, String>();
		Application app = ApplicationImpl.ANY_APPLICATION;
		plugins.put(InternalGridBean.SWING_PLUGIN,
				GridBeanPlugin.class.getName());
		ClassLoader classLoader = Thread.currentThread()
				.getContextClassLoader();
		return new InternalGridBeanImpl(app, "ForEachGridBean", "1.0",
				DataSourceModel.class, plugins, classLoader);
	}

	protected WorkflowFile createOutputFile() {
		WorkflowFile out = new WorkflowFile(getDiagram(), UUID.randomUUID().toString(), this);
		out.setDisplayedString("iterator");
		String path = createRelativePath();
		
		out.setRelativePath(path);
		out.setParentDirAddress("");
		out.setProtocol(C9mRCPCommonConstants.WFFILE_QNAME);
		out.activate();
		return out;
	}
	
	private String createRelativePath()
	{
		return ParameterProcessingUtils
		.escapeVariable(ParameterProcessingUtils
				.createVariableFromName(getIteratorName()));
	}

	
	@Override
	public Object getAdapter(Class adapter) {
		if (IGridBean.class.equals(adapter)) {
			return getGridBean();
		} else {
			return super.getAdapter(adapter);
		}
	}

	public Client getClient() {
		if (client == null) {
			ClientAdapter newClient = GPEActivator.getDefault().getClient()
					.clone();
			GPEFileFactory fileFactory = new GPEFileFactoryImpl();
			fileFactory.setTemporaryDirName(getTempDir());
			newClient.setFileFactory(fileFactory);
		}
		return client;
	}

	@Override
	public DataSourceActivity getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		try {
			// temporarily store the gridbean in a property in order to clone it
			storeGridBeanInProperty();
			DataSourceActivity result = (DataSourceActivity) super.getCopy(
					toBeCopied, mapOfCopies, copyFlags);
			setPropertyValue(PROP_GRID_BEAN_JOB, null);
			result.client = null;
			result.outputFile = CloneUtils.getFromMapOrCopy(outputFile,
					toBeCopied, mapOfCopies, copyFlags);
			result.loadGridBeanFromProperty();
			try {

				ClassLoader cl = JAXBContextProvider.getDefaultClassLoader();
				CompositeClassLoader union = new CompositeClassLoader();
				union.add(cl);
				union.add(getGridBean().getClass().getClassLoader());
				IGridBean clone = CloneUtils.deepClone(getGridBean(), union);
				for (QName key : clone.keySet()) {
					Object value = clone.get(key);
					Set<QName> ignored = new HashSet<QName>();
					for (IGridBeanParameter param : getGridBean()
							.getInputParameters()) {
						ignored.add(param.getName());
					}
					for (IGridBeanParameter param : getGridBean()
							.getOutputParameters()) {
						ignored.add(param.getName());
					}
					ignored.add(IGridBeanModel.JOBNAME);

					if (!ignored.contains(key)
							&& !CollectionUtil.equalOrBothNull(result
									.getGridBean().get(key), value)) {
						result.getGridBean().set(key, value);
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		} catch (Exception e) {
			WFActivator.log(IStatus.ERROR,
					"Could not create copy of the element " + getName(), e);
			return null;
		}
	}

	public IGridBean getGridBean() {
		if (gridBean == null) {
			try {
				setGridBean(createInternalGridBean().getJobInstance().getModel());
			} catch (GridBeanException e) {
				WFEditorActivator.log(Status.ERROR,"Error while creating application GUI instance: "+e.getMessage(),e);
			}
		}
		return gridBean;
	}

	

	public String getIteratorName() {
		return SimpleWFUtil.iteratorNameFromActivityName(getName());
	}

	public WorkflowFile getOutputFile() {
		return outputFile;
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_GRID_BEAN_JOB);
		propertiesToCopy.add(PROP_DATA_SOURCE_USED);
		return propertiesToCopy;
	}

	protected String getTempDir() {
		return getDiagram().getProject().getLocation().toOSString()
				+ File.separator;
	}

	@Override
	public QName getType() {
		return TYPE;
	}

	public boolean isDataSourceUsed() {
		Boolean result = (Boolean) getPropertyValue(PROP_DATA_SOURCE_USED);
		if (result == null) {
			return false;
		} else {
			return result;
		}
	}

	protected void loadGridBeanFromProperty() {
		GridBeanJobWrapper<?> wrapper;
		try {
			wrapper = (GridBeanJobWrapper<?>) getPropertyValue(PROP_GRID_BEAN_JOB);
			GridBeanJob gbJob = createInternalGridBean().getJobInstance();
			wrapper.setGridBeanJob(gbJob);
			wrapper.restoreSavedJob();
			setGridBean(gbJob.getModel());
		} catch (Exception e) {
			GPEActivator.log("Problem during serialization of "
					+ this.getClass().getName(), e);
		}

	}

	public void propertyChange(PropertyChangeEvent evt) {
		getDiagram().setDirty(true);
		firePropertyChange(evt.getPropertyName(), evt.getOldValue(),
				evt.getNewValue());
	}

	public void setDataSourceUsed(boolean active) {
		if (getPropertyValue(PROP_DATA_SOURCE_USED) != null
				&& isDataSourceUsed() == active) {
			return;
		}
		setPropertyValue(PROP_DATA_SOURCE_USED, active);
		if (active) {
			if (getOutputFile() != null && !getOutputFile().isActive()) {
				// (re-)add workflow file
				getOutputFile().activate();
				addOutputFile(getOutputFile());
				if (getDataSourceList().getDataSourceById(outputFile.getId()) == null) {
					getDataSourceList().addDataSource(outputFile);
				}
			}
		} else {
			// deactivate and remove workflow file
			if (getOutputFile() != null && !getOutputFile().isDisposed()) {
				getOutputFile().dispose();
				removeOutputFile(getOutputFile());
				getDataSourceList().removeDataSource(outputFile);

			}
		}

	}

	protected void setGridBean(IGridBean gridBean) {
		if (this.gridBean != null) {
			this.gridBean.removePropertyChangeListener(this);
		}
		this.gridBean = gridBean;

		if (gridBean.get(GPE4EclipseConstants.CONTEXT) == null) {
			gridBean.set(GPE4EclipseConstants.CONTEXT,
					GridBeanUtils.createWorkflowContext(getDiagram()));
			gridBean.getInputParameters().add(
					new GridBeanParameter(GPE4EclipseConstants.CONTEXT,
							GPE4EclipseConstants.PARAMETER_TYPE_CONTEXT));
		}
		gridBean.addPropertyChangeListener(this);
	}

	@Override
	public boolean setName(String name) {
		boolean success = super.setName(name);
		if (success && getOutputFile() != null) {
			getOutputFile().setRelativePath(createRelativePath());
		}
		return success;
	}

	protected void storeGridBeanInProperty() {
		GridBeanJobWrapper<?> wrapper = new GridBeanJobWrapper<Object>();
		try {
			GPEJobImpl job = new GPEJobImpl();
			getGridBean().setupJobDefinition(job);
			Node n = job.getDocument().newDomNode();
			Element e = (Element) n.getFirstChild();
			wrapper.setJSDLJob(e);

		} catch (Exception e) {
			GPEActivator.log("Problem during serialization of "
					+ this.getClass().getName(), e);
		} finally {
			setPropertyValue(PROP_GRID_BEAN_JOB, wrapper);
		}
	}

}
