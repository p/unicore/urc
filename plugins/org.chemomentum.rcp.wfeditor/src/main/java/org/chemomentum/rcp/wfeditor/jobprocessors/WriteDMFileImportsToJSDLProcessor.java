/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.jobprocessors;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.common.DMFileAddress;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.FileProtocolProcessor;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;

/**
 * @author demuth
 * 
 */
public class WriteDMFileImportsToJSDLProcessor extends FileProtocolProcessor {

	public WriteDMFileImportsToJSDLProcessor() {
		processingSteps.add(ProcessingConstants.WRITE_PROCESSED_PARAMS_TO_JSDL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.gridbeans.parameters.IProtocolProcessor#getSourceProtocol()
	 */
	@Override
	public QName getSourceProtocol() {
		return C9mRCPCommonConstants.DMFILE_QNAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.gridbeans.parameters.IProtocolProcessor#getTargetProtocol()
	 */
	@Override
	public QName getTargetProtocol() {
		return ProtocolConstants.JOB_WORKING_DIR_QNAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.gridbeans.parameters.IProtocolProcessor#process(com.intel
	 * .gpe.clients.api.jsdl.gpe.GPEJob, int,
	 * com.intel.gpe.gridbeans.IGridBeanParameter,
	 * com.intel.gpe.gridbeans.IGridBeanParameterValue, java.util.Map)
	 */
	@Override
	public void process(Client client, GPEJob job, int processingStep,
			IGridBeanParameter param, IGridBeanParameterValue value,
			Map<String, Object> processorParams, IProgressListener progress)
			throws Exception {
		List<IGridBeanParameterValue> result = new ArrayList<IGridBeanParameterValue>();
		result.add(value);
		if (value instanceof IFileParameterValue) {
			IFileParameterValue file = (IFileParameterValue) value;
			String logical = null;
			if (file.getProcessedSource() instanceof DMFileAddress) {
				DMFileAddress source = (DMFileAddress) file
						.getProcessedSource();
				logical = source.getLogicalName();
			}

			if (logical == null) {
				throw new Exception("Invalid input definition for parameter: "
						+ param.getDisplayedName());
			}
			String prefix = C9mRCPCommonConstants.DMFILE_PREFIX
					+ ProtocolConstants.PROTOCOL_SEPERATOR;
			if (!logical.startsWith(prefix)) {
				logical = prefix + logical;
			}
			job.addDataStagingImportElement(logical, file.getFilesystemName(), file
					.getProcessedTarget().getInternalString(),file.getFlags());

		}
	}

}
