/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.transfers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.util.URIUtil;
import org.chemomentum.common.util.C9MCommonConstants;
import org.chemomentum.common.ws.ILocationManager;
import org.chemomentum.dataManagement.locationManager.QueryLocationRequestDocument;
import org.chemomentum.dataManagement.locationManager.QueryLocationRequestDocument.QueryLocationRequest.Input;
import org.chemomentum.dataManagement.locationManager.QueryLocationResponseDocument;
import org.chemomentum.dataManagement.locationManager.QueryLocationResponseDocument.QueryLocationResponse.Result;
import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.chemomentum.rcp.wfeditor.model.LMFileAddress;
import org.eclipse.core.runtime.IStatus;
import org.unigrids.services.atomic.types.GridFileType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.GridFile;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.transfers.FileResolver;
import com.intel.gpe.clients.api.transfers.IGridFileTransfer;
import com.intel.gpe.clients.common.transfers.WildcardsInFilenamesUtil;
import com.intel.gpe.clients.impl.sms.StorageClientImpl;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.uas.client.StorageClient;
import de.fzj.unicore.wsrflite.xmlbeans.client.RegistryClient;
import eu.unicore.security.wsutil.client.UnicoreWSClientFactory;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * @author demuth
 * 
 */
public class LMFileResolver implements FileResolver {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.chemomentum.servorch.wamanage.ILocationMapper#mapToPhysicalNames(
	 * java.util.Set)
	 */
	public Map<String, List<EndpointReferenceType>> mapToPhysicalNames(
			Set<String> logicalNames, EndpointReferenceType lmEpr)
			throws Exception {
		Map<String, List<EndpointReferenceType>> map = new HashMap<String, List<EndpointReferenceType>>();
		Input[] inputs = new Input[logicalNames.size()];
		int i = 0;
		for (String name : logicalNames) {
			Input input = Input.Factory.newInstance();			
			input.setLogicalName(name);
			inputs[i++] = input;
		}
		Result[] results = queryLocationManager(inputs, lmEpr);
		if (results == null || results.length == 0) {
			return null;
		}
		for (int j = 0; j < results.length; j++) {
			List<EndpointReferenceType> list = processResult(results[j]);
			if (list != null) {
				map.put(results[j].getLogicalName(), list);
			} else {
				return null;
			}
		}
		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.fts.FileResolver#resolveFiles(com.intel.gpe
	 * .gridbeans.IGridFileTransfer,
	 * com.intel.gpe.client2.security.GPESecurityManager,
	 * com.intel.gpe.client2.providers.FileProvider,
	 * com.intel.gpe.client2.IProgressListener)
	 */
	public List<IGridFileTransfer> resolveFiles(IGridFileTransfer transfer,
			Client client, IProgressListener progressListener) throws Exception {
		List<IGridFileTransfer> result = new ArrayList<IGridFileTransfer>();
		IGridFileTransfer file = transfer.clone();
		LMFileAddress source = (LMFileAddress) file.getProcessedSource();

		String value = source.getLogicalName();
		URI uri = source.getRegistryURI();
		EndpointReferenceType epr = NodeFactory.getEPRForURI(uri);
		// TODO don't hardcode this!
		value = value.replace("**", "*");
		HashSet<String> logicalNames = new HashSet<String>();
		if (!value.startsWith(C9MCommonConstants.LOGICAL_FILENAME_PREFIX)) {
			value = C9MCommonConstants.LOGICAL_FILENAME_PREFIX + value;
		}
		
		try {
			new URI(value);
		} catch (URISyntaxException e) {
			// escape special characters in filenames!
			// do this only if necessary (they might have been escaped earlier)
			value = URIUtil.encode(value, org.apache.commons.httpclient.URI.allowed_fragment);
		}
		
		logicalNames.add(value);

		Map<String, List<EndpointReferenceType>> physicalLocations = null;

		IClientConfiguration sp = ServiceBrowserActivator.getDefault()
				.getUASSecProps(uri);
		RegistryClient regClient = new RegistryClient(uri.toString(), epr, sp);
		List<EndpointReferenceType> eprs = regClient
				.listServices(ILocationManager.PORT);
		if (eprs.size() > 0) {
			physicalLocations = mapToPhysicalNames(logicalNames, eprs.get(0));
		} else {
			throw new Exception(
					"Unable to resolve locations for logical name(s) "
							+ logicalNames.toString());
		}

		Set<String> keys = physicalLocations.keySet();
		for (String key : keys) {
			file = file.clone();
			source = (LMFileAddress) file.getProcessedSource();
			source.setLogicalName(key);
			String withoutProtocol = key
					.substring(C9MCommonConstants.LOGICAL_FILENAME_PREFIX
							.length());
			List<EndpointReferenceType> locations = physicalLocations.get(key);
			String location = locations.get(0).getAddress().getStringValue();
			int hashIndex = location.indexOf("#");
			String storageAddress = location.substring(0, hashIndex);
			if (!storageAddress.startsWith("http")) {
				storageAddress = storageAddress.substring(storageAddress
						.indexOf(":") + 1);
			}
			String relative = location.substring(hashIndex + 1);
			EndpointReferenceType storageEpr = EndpointReferenceType.Factory.newInstance();
			storageEpr.addNewAddress().setStringValue(storageAddress);
			StorageClient storage = new StorageClient(storageEpr, sp);
			GridFileType type = storage.listProperties(relative);
			GridFile gf = StorageClientImpl.getGridFileFromType(type);
			source.setResolvedGridFile(gf);

			// determine displayed string
			// TODO this is very dirty since it assumes that
			// the logical name starts with workflowid/activityid/

			String displayed = "";
			try {
				String[] tokens = withoutProtocol.split("/");
				if (tokens.length < 3) {
					displayed = withoutProtocol;
				} else {
					for (int i = 2; i < tokens.length; i++) {
						displayed += tokens[i];
						if (i < tokens.length - 1) {
							displayed += "/";
						}
					}
				}

			} catch (Exception e) {
				displayed = withoutProtocol;
			}

			source.setRelativePath(displayed);
			source.setDisplayedString(displayed);
			WildcardsInFilenamesUtil.adjustRelativePathInTarget(file, client);
			source.setUsingWildcards(false);
			result.add(file);

		}

		return result;
	}

	protected static List<EndpointReferenceType> processResult(Result result) {

		List<EndpointReferenceType> list = new ArrayList<EndpointReferenceType>();
		try {

			String[] physicalLocations = result.getPhysicalLocationArray();

			if (physicalLocations != null && physicalLocations.length > 0) {
				for (int i = 0; i < physicalLocations.length; i++) {
					if (physicalLocations[i] != null) {
						EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
						epr.addNewAddress()
								.setStringValue(physicalLocations[i]);
						list.add(epr);
					} else {
						return null;
					}
				}
				return list;
			} else if (result.getLogicalNameNotExistFault() != null) {
				WFEditorActivator.log(IStatus.WARNING,
						"Could not map logical filename to physical location: \n"
								+ result.getLogicalName()
								+ "\n"
								+ "error description: "
								+ result.getLogicalNameNotExistFault()
										.getShortDescription());

			}
		} catch (Exception e) {
			WFEditorActivator.log(IStatus.WARNING,
					"Could not parse result from LocationManager correctly: ",
					e);
		}
		return null;
	}

	/**
	 * call Location Management Service for mapping logical names to physical
	 * locations
	 */
	protected static Result[] queryLocationManager(Input[] logicalNames,
			EndpointReferenceType epr) throws Exception {

		IClientConfiguration sp = ServiceBrowserActivator.getDefault()
				.getUASSecProps(new URI(epr.getAddress().getStringValue()));
		ILocationManager lm = new UnicoreWSClientFactory(sp)
				.createPlainWSProxy(ILocationManager.class, epr.getAddress()
						.getStringValue());
		QueryLocationRequestDocument request = QueryLocationRequestDocument.Factory
				.newInstance();

		request.addNewQueryLocationRequest().setInputArray(logicalNames);
		QueryLocationResponseDocument response = lm.queryLocation(request);
		return response.getQueryLocationResponse().getResultArray();

	}

}
