/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.wfeditor.parts;

import java.net.URL;

import org.chemomentum.rcp.wfeditor.model.DataSourceActivity;
import org.eclipse.jface.resource.ImageDescriptor;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.wfeditor.figures.ActivityFigure;
import de.fzj.unicore.rcp.wfeditor.parts.SimpleActivityPart;

/**
 * @author demuth
 */
public class DataSourceActivityPart extends SimpleActivityPart {

	public DataSourceActivityPart(DataSourceActivity activity) {
		this.setModel(activity);
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		// installEditPolicy(POLICY_FETCH_OUTCOMES, null);
	}

	@Override
	public DataSourceActivity getModel() {
		return (DataSourceActivity) super.getModel();
	}

	@Override
	public ActivityFigure getFigure() {
		return super.getFigure();
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		ImageDescriptor imageDescr = null;
		if(getModel() != null && getModel().getGridBean() != null)
		{
			URL iconUrl = getModel().getGridBean().getIconURL();
			imageDescr = ImageDescriptor.createFromURL(iconUrl);
		}
		if (imageDescr == null) {
			imageDescr = GPEActivator.getImageDescriptor("empty_activity.gif");
		}

		return imageDescr;
	}

}
