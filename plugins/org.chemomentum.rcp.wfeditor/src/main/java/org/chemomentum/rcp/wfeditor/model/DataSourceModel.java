package org.chemomentum.rcp.wfeditor.model;

import java.net.URL;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;

import com.intel.gpe.gridbeans.AbstractGridBean;
import com.intel.gpe.gridbeans.parameters.FileSetParameterValue;
import com.intel.gpe.gridbeans.parameters.GridBeanParameter;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;

import de.fzj.unicore.rcp.common.utils.DelegatingExtensionClassloader;
import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;

public class DataSourceModel extends AbstractGridBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7000329846141519585L;

	public static final String NAMESPACE = "http://www.fz-juelich.de/gridbeans/foreach";

	public static final QName INPUT = new QName(NAMESPACE, "FILE_"),
			OUTPUT = new QName(NAMESPACE, "FILE ITERATOR");

	private static final ClassLoader serializationClassLoader = 
		new DelegatingExtensionClassloader(DataSourceModel.class.
				getClassLoader(),
				GPE4EclipseConstants.GB_SERIALIZATION_EXTENSION_POINTS);
	
	public DataSourceModel() {

		set(JOBNAME, "DataSource");
		getInputParameters().add(
				new GridBeanParameter(INPUT, GridBeanParameterType.FILE_SET,
						true));
		FileSetParameterValue inputFileset = new FileSetParameterValue();
		set(INPUT, inputFileset);

		getOutputParameters().clear();
		set(STDOUT, null);
		set(STDERR, null);
	}
	
	protected ClassLoader getClassLoaderForSerializiation()
	{
		return serializationClassLoader;
	}

	@Override
	public URL getIconURL() {
		return FileLocator.find(WFEditorActivator.getDefault().getBundle(),
				new Path(WFEditorActivator.ICONS_PATH + "foreach.png"), null);
	}

	public String getName() {
		return "data source";
	}

}
