/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.clientstubs;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.namespace.QName;

import org.chemomentum.common.util.C9MCommonConstants;
import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.common.DMFileAddress;
import org.chemomentum.rcp.wfeditor.model.LMFileAddress;
import org.w3.x2005.x08.addressing.EndpointReferenceType;
import org.w3c.dom.Element;

import com.intel.gpe.clients.api.FileSystem;
import com.intel.gpe.clients.api.GridFile;
import com.intel.gpe.clients.api.StorageClient;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.exceptions.GPEDirectoryNotCreatedException;
import com.intel.gpe.clients.api.exceptions.GPEDirectoryNotListedException;
import com.intel.gpe.clients.api.exceptions.GPEExportFileResourceNotCreatedException;
import com.intel.gpe.clients.api.exceptions.GPEFileAddressUnresolvableException;
import com.intel.gpe.clients.api.exceptions.GPEFileNotCopiedException;
import com.intel.gpe.clients.api.exceptions.GPEFileNotDeletedException;
import com.intel.gpe.clients.api.exceptions.GPEFileNotRenamedException;
import com.intel.gpe.clients.api.exceptions.GPEFilePropertiesNotListedException;
import com.intel.gpe.clients.api.exceptions.GPEFileTransferProtocolNotSupportedException;
import com.intel.gpe.clients.api.exceptions.GPEImportFileResourceNotCreatedException;
import com.intel.gpe.clients.api.exceptions.GPEInvalidQueryExpressionException;
import com.intel.gpe.clients.api.exceptions.GPEInvalidResourcePropertyQNameException;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareRemoteException;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareServiceException;
import com.intel.gpe.clients.api.exceptions.GPEPermissionsNotChangedException;
import com.intel.gpe.clients.api.exceptions.GPEQueryEvaluationErrorException;
import com.intel.gpe.clients.api.exceptions.GPEResourceUnknownException;
import com.intel.gpe.clients.api.exceptions.GPESecurityException;
import com.intel.gpe.clients.api.exceptions.GPEUnknownQueryExpressionDialectException;
import com.intel.gpe.clients.api.exceptions.GPEUnmarshallingException;
import com.intel.gpe.clients.api.fts.FileTransferClient;
import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.clients.api.transfers.IProtocol;

/**
 * Implementation of the GPE storage client that can be used for accessing
 * Chemomentum files, e.g. for fetching job outcomes.
 * 
 * @author demuth
 * 
 */
public class C9MStorageClient implements StorageClient {

	// id that is used as a prefix to make relative paths on this "storage"
	// absolute
	String id;
	EndpointReferenceType registryEpr;

	private boolean useDMAS = false;

	EndpointReferenceType globalStorageEpr = null;

	public C9MStorageClient(String id, EndpointReferenceType registryEpr,
			boolean useDMAS, EndpointReferenceType globalStorageEpr) {
		this.id = id;
		this.registryEpr = registryEpr;
		this.globalStorageEpr = globalStorageEpr;
		this.useDMAS = useDMAS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.StorageClient#changePermissions(java.lang.String
	 * , boolean, boolean, boolean)
	 */
	public void changePermissions(String path, boolean readable,
			boolean writable, boolean executable)
			throws GPEPermissionsNotChangedException,
			GPEResourceUnknownException, GPEMiddlewareRemoteException,
			GPESecurityException, GPEMiddlewareServiceException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.StorageClient#copyFile(java.lang.String,
	 * java.lang.String)
	 */
	public void copyFile(String destination, String source)
			throws GPESecurityException, GPEMiddlewareServiceException,
			GPEMiddlewareRemoteException, GPEFileNotCopiedException,
			GPEResourceUnknownException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.StorageClient#createDirectory(java.lang.String)
	 */
	public void createDirectory(String path)
			throws GPEResourceUnknownException,
			GPEDirectoryNotCreatedException, GPESecurityException,
			GPEMiddlewareServiceException, GPEMiddlewareRemoteException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.StorageClient#deleteFile(java.lang.String)
	 */
	public void deleteFile(String path) throws GPEResourceUnknownException,
			GPEFileNotDeletedException, GPESecurityException,
			GPEMiddlewareRemoteException, GPEMiddlewareServiceException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.StorageClient#exportFile(java.lang.String,
	 * java.lang.String, boolean)
	 */
	public <FileTransferClientType extends FileTransferClient> FileTransferClientType exportFile(
			String path, String protocol, boolean isPipe)
			throws GPEFileTransferProtocolNotSupportedException,
			GPEExportFileResourceNotCreatedException,
			GPEResourceUnknownException, GPESecurityException,
			GPEMiddlewareServiceException, GPEMiddlewareRemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.StorageClient#getAbsoluteAddress(java.lang.
	 * String, com.intel.gpe.client2.IProgressListener)
	 */
	public IGridFileAddress getAbsoluteAddress(String logicalFilename,
			IProgressListener progressListener)
			throws GPEFileAddressUnresolvableException {
		EndpointReferenceType epr = (EndpointReferenceType) registryEpr.copy();
		String prefix = C9MCommonConstants.LOGICAL_FILENAME_PREFIX + id;
		String relative = logicalFilename.replace(prefix, "");
		relative = relative.substring(relative.indexOf("/") + 1);
		if (useDMAS) {
			DMFileAddress result = new DMFileAddress(relative);
			result.setLogicalName(logicalFilename);
			try {
				result.setRegistryURI(new URI(epr.getAddress().getStringValue()));
				return result;
			} catch (URISyntaxException e) {
				throw new GPEFileAddressUnresolvableException(
						"Unable to resolve absolute address for "
								+ logicalFilename, this);
			}
		} else {
			// The registry containing the location manager is not enough.
			// We need to find out to which UNICORE storage the files should be
			// uploaded and
			// pass this information as well

			LMFileAddress result = new LMFileAddress(relative);
			result.setLogicalName(logicalFilename);
			try {
				result.setRegistryURI(new URI(epr.getAddress().getStringValue()));
				result.setStorageURI(new URI(globalStorageEpr.getAddress()
						.getStringValue()));
				return result;
			} catch (URISyntaxException e) {
				throw new GPEFileAddressUnresolvableException(
						"Unable to resolve absolute address for "
								+ logicalFilename, this);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.StorageClient#getFileSeparator()
	 */
	public String getFileSeparator() {
		return "/";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.StorageClient#getFileSystem()
	 */
	public <FileSystemType extends FileSystem> FileSystemType getFileSystem()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	public String getFullAddress() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#getMultipleResourceProperties(javax
	 * .xml.namespace.QName[])
	 */
	public Element[] getMultipleResourceProperties(QName[] resourceProperties)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.StorageClient#getRelativeAddress(java.lang.
	 * String, com.intel.gpe.client2.IProgressListener)
	 */
	public String getRelativeAddress(IGridFileAddress absoluteAddress,
			IProgressListener progressListener)
			throws GPEFileAddressUnresolvableException {

		if (absoluteAddress instanceof LMFileAddress) {
			return ((LMFileAddress) absoluteAddress).getLogicalName();
		} else if (absoluteAddress instanceof DMFileAddress) {
			return ((DMFileAddress) absoluteAddress).getLogicalName();
		}
		throw new GPEFileAddressUnresolvableException(
				"Unable to resolve relative address for "
						+ absoluteAddress.getDisplayedString(), this);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#getResourceProperty(javax.xml.namespace
	 * .QName)
	 */
	public Element getResourceProperty(QName resourceProperty)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSRPClient#getResourcePropertyDocument()
	 */
	public Element getResourcePropertyDocument() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.StorageClient#getStorageType()
	 */
	public IProtocol getStorageType() {
		return useDMAS ? C9mRCPCommonConstants.DMFILE_PROTOCOL
				: C9mRCPCommonConstants.LMFILE_PROTOCOL;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.StorageClient#getSupportedProtocols()
	 */
	public String[] getSupportedProtocols()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSRFClient#getURL()
	 */
	public String getURL() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.StorageClient#importFile(java.lang.String,
	 * java.lang.String, boolean)
	 */
	public <FileTransferClientType extends FileTransferClient> FileTransferClientType importFile(
			String file, String protocol, boolean isPipe)
			throws GPEResourceUnknownException,
			GPEImportFileResourceNotCreatedException, GPESecurityException,
			GPEFileTransferProtocolNotSupportedException,
			GPEMiddlewareServiceException, GPEMiddlewareRemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.StorageClient#listDirectory(java.lang.String)
	 */
	public <GridFileType extends GridFile> List<GridFileType> listDirectory(
			String path) throws GPESecurityException,
			GPEMiddlewareServiceException, GPEResourceUnknownException,
			GPEDirectoryNotListedException, GPEMiddlewareRemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.StorageClient#listProperties(java.lang.String)
	 */
	public <GridFileType extends GridFile> GridFileType listProperties(
			String path) throws GPESecurityException,
			GPEMiddlewareServiceException, GPEFilePropertiesNotListedException,
			GPEResourceUnknownException, GPEMiddlewareRemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#queryXPath10Properties(java.lang
	 * .String)
	 */
	public Element[] queryXPath10Properties(String expression)
			throws GPEResourceUnknownException,
			GPEInvalidResourcePropertyQNameException,
			GPEInvalidQueryExpressionException,
			GPEQueryEvaluationErrorException,
			GPEUnknownQueryExpressionDialectException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.StorageClient#renameFile(java.lang.String,
	 * java.lang.String)
	 */
	public void renameFile(String destination, String source)
			throws GPESecurityException, GPEFileNotRenamedException,
			GPEResourceUnknownException, GPEMiddlewareServiceException,
			GPEMiddlewareRemoteException {
		// TODO Auto-generated method stub

	}

}
