/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.extensions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.chemomentum.rcp.wfeditor.clientstubs.C9MActivityJobClient;
import org.chemomentum.rcp.wfeditor.model.DataSourceActivity;
import org.chemomentum.rcp.wfeditor.model.EnvVariableLinker;
import org.chemomentum.rcp.wfeditor.model.ForEachActivity;
import org.chemomentum.rcp.wfeditor.model.ForEachDataSourceActivity;
import org.chemomentum.rcp.wfeditor.model.HoldActivity;
import org.chemomentum.rcp.wfeditor.model.OverwriteWFFileLinker;
import org.chemomentum.rcp.wfeditor.model.WFFileLinker;
import org.chemomentum.rcp.wfeditor.parts.ForEachActivityPart;
import org.chemomentum.rcp.wfeditor.parts.ForEachDataSourceActivityPart;
import org.chemomentum.rcp.wfeditor.parts.HoldActivityPart;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.swt.graphics.Image;

import com.thoughtworks.xstream.converters.Converter;

import de.fzj.unicore.rcp.wfeditor.DefaultCreationTool;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.IPaletteEntryExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.PaletteCategory;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.PaletteElement;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.ActivityFactory;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;
import de.fzj.unicore.rcp.wfeditor.utils.PackageUtil;

/**
 * @author demuth
 * 
 */
public class WorkflowPaletteEntryExtension implements
		IPaletteEntryExtensionPoint {

	private static Set<Class<?>> annotatedClasses;
	private List<Image> images = new ArrayList<Image>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.extensionpoints.IPartFactoryExtensionPoint
	 * #createEditPart(javax.xml.namespace.QName, org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	public EditPart createEditPart(WFEditor editor, EditPart context,
			Object model) {
		if (!(model instanceof IFlowElement)) {
			return null;
		}
		IFlowElement element = (IFlowElement) model;
		if (ForEachDataSourceActivity.TYPE.equals(element.getType())) {
			return new ForEachDataSourceActivityPart(
					(ForEachDataSourceActivity) model);
		} else if (ForEachActivity.TYPE.equals(element.getType())) {
			return new ForEachActivityPart();
		} else if (HoldActivity.TYPE.equals(element.getType())) {
			return new HoldActivityPart();
		}
		return null;
	}

	/**
	 * Perform some cleanup! All created images should be disposed of.
	 */
	public void dispose() {
		for (Image img : images) {
			img.dispose();
		}
	}

	public Set<Converter> getAdditionalConverters() {
		return null;
	}

	/**
	 * contribute activity types to the UNICORE workflow editor
	 */
	public List<PaletteElement> getAdditionalPaletteEntries(WFEditor editor) {
		List<PaletteElement> elements = new ArrayList<PaletteElement>();

		PaletteCategory category = IPaletteEntryExtensionPoint.PALETTE_CATEGORY_WORKFLOW_STRUCTURES;

		ActivityFactory factory = new ActivityFactory(ForEachActivity.class,
				editor);
		ImageDescriptor imageDescr = WFEditorActivator
				.getImageDescriptor("foreach.png");
		Image smallImage = new Image(null, imageDescr.getImageData().scaledTo(
				DEFAULT_ICON_SIZE.width, DEFAULT_ICON_SIZE.height));
		images.add(smallImage);
		ImageDescriptor iconLarge = new DecorationOverlayIcon(smallImage,
				new ImageDescriptor[0]);
		CombinedTemplateCreationEntry combined = new CombinedTemplateCreationEntry(
				"ForEach-Loop",
				"Create a new ForEach-Loop for iterating over a set of files",
				factory, factory, iconLarge, iconLarge);
		elements.add(new PaletteElement(category, combined));
		combined.setToolClass(DefaultCreationTool.class);

		factory = new ActivityFactory(HoldActivity.class, editor);
		imageDescr = WFEditorActivator.getImageDescriptor("hold.png");
		smallImage = new Image(null, imageDescr.getImageData().scaledTo(
				DEFAULT_ICON_SIZE.width, DEFAULT_ICON_SIZE.height));
		images.add(smallImage);
		iconLarge = new DecorationOverlayIcon(smallImage,
				new ImageDescriptor[0]);
		combined = new CombinedTemplateCreationEntry("Hold",
				"Create a new Hold Activity", factory, factory, iconLarge,
				iconLarge);
		elements.add(new PaletteElement(category, combined));
		combined.setToolClass(DefaultCreationTool.class);

		return elements;
	}

	public Set<Class<?>> getAllModelClasses() {
		if (annotatedClasses == null) {
			Class<?> modelClass = DataSourceActivity.class;
			Package modelPackage = modelClass.getPackage();

			Class<?> stubsClass = C9MActivityJobClient.class;
			Package stubsPackage = stubsClass.getPackage();
			try {
				// this is expensive, so don't do it often!
				annotatedClasses = PackageUtil.getEclipseClasses(
						WFEditorActivator.getDefault(), modelPackage.getName(),
						true);
				annotatedClasses.addAll(PackageUtil.getEclipseClasses(
						WFEditorActivator.getDefault(), stubsPackage.getName(),
						true));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return annotatedClasses;
	}

	public Map<String, Class<?>> getOldTypeAliases() {
		Map<String, Class<?>> result = new HashMap<String, Class<?>>();
		result.put("org.chemomentum.rcp.wfeditor.extensions.EnvVariableLinker",
				EnvVariableLinker.class);
		result.put("org.chemomentum.rcp.wfeditor.extensions.WFFileLinker",
				WFFileLinker.class);
		result.put(
				"org.chemomentum.rcp.wfeditor.extensions.OverwriteWFFileLinker",
				OverwriteWFFileLinker.class);
		result.put(
				"org.chemomentum.rcp.wfeditor.clientstubs.C9MActivityJobClient",
				C9MActivityJobClient.class);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.IExecutableExtension#setInitializationData(org
	 * .eclipse.core.runtime.IConfigurationElement, java.lang.String,
	 * java.lang.Object)
	 */
	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
		// TODO Auto-generated method stub

	}

}
