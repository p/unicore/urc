/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.submission;

import java.net.URI;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlObject;
import org.chemomentum.common.api.workflow.WFClient;
import org.chemomentum.common.api.workflow.WorkflowWrapper;
import org.chemomentum.rcp.servicebrowser.nodes.TracerNode;
import org.chemomentum.rcp.servicebrowser.nodes.WorkflowFactoryNode;
import org.chemomentum.rcp.servicebrowser.nodes.WorkflowNode;
import org.chemomentum.rcp.wfeditor.C9MWFEditorConstants;
import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.chemomentum.rcp.wfeditor.utils.DataManagementUtils;
import org.chemomentum.workflow.xmlbeans.RulesDocument;
import org.chemomentum.workflow.xmlbeans.WorkflowDocument;
import org.chemomentum.workflow.xmlbeans.WorkflowDocument.Workflow;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.GroupNode;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.submission.IConverter;
import de.fzj.unicore.rcp.wfeditor.submission.ISubmitter;
import de.fzj.unicore.uas.client.StorageClient;

/**
 * @author demuth
 * 
 */
public class C9MSubmitter implements ISubmitter {

	private EndpointReferenceType workDirEpr;

	private void abortWorkflow(WFClient wfClient,
			final WorkflowFactoryNode workflowFactoryNode) {
		workflowFactoryNode.refresh(2, false, null);
		URI uri;
		try {
			uri = new URI(wfClient.getEPR().getAddress().getStringValue());
			NodePath p = workflowFactoryNode.getWorkflowsNode().getPathToRoot()
					.append(uri);
			final WorkflowNode wfNode = (WorkflowNode) NodeFactory
					.getNodeFor(p);
			wfNode.destroy();
			workflowFactoryNode.refresh(1, false, null);

		} catch (Exception e) {
			WFEditorActivator.log(
					IStatus.WARNING,
					"Unable to destroy workflow after failed submission: "
							+ e.getMessage(), e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.ISubmitter#canSubmit(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram)
	 */
	public boolean canDealWith(WorkflowDiagram model) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.ISubmitter#canSubmit(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram, org.eclipse.core.runtime.IAdaptable)
	 */
	public boolean canSubmit(IAdaptable target, WorkflowDiagram model) {
		return canDealWith(model) && (target instanceof WorkflowFactoryNode);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.ISubmitter#createWorkflowDesciption
	 * (org.eclipse.core.runtime.IAdaptable,
	 * de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram,
	 * org.eclipse.core.runtime.IProgressMonitor)
	 */
	public XmlObject createWorkflowDescription(IAdaptable submissionService,
			WorkflowDiagram model, boolean uploadFiles, IProgressMonitor monitor)
			throws Exception {

		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		monitor.beginTask("", 100);
		try {
			if (!(submissionService instanceof WorkflowFactoryNode)) {
				throw new Exception(
						"Internal error: Cannot create workflow description for a service of this type!");
			}
			Set<QName> dialects = WFActivator.getDefault()
					.getConverterRegistry()
					.getPossibleTargets(model.getContext().getModelType());
			WorkflowFactoryNode workflowFactoryNode = (WorkflowFactoryNode) submissionService;
			Set<QName> applicable = DialectMatcher.matchSupportedDialects(
					workflowFactoryNode, dialects);
			if (applicable.size() < 1) {
				throw new Exception(
						"Internal error: Cannot create workflow description, selected target does not support its dialect");

			}

			String wfId = C9MWFEditorConstants.WORKFLOW_ID_VARIABLE;

			String uploadFilesId = UUID.randomUUID().toString();

			IPath modelFile = new Path(model.getFilename());
			String workflowName = modelFile.removeFileExtension().lastSegment();

			Integer hoursToLive = (Integer) model
					.getPropertyValue(WorkflowDiagram.PROP_TERMINATION_TIME);
			Calendar terminationTime = null;
			if(hoursToLive != null)
			{
				 terminationTime = Calendar.getInstance();
				terminationTime.add(Calendar.HOUR, hoursToLive);
			}

			QName dialect = applicable.iterator().next();
			IConverter converter = WFActivator.getDefault()
					.getConverterRegistry()
					.getConverter(model.getContext().getModelType(), dialect);

			String engineVersion = null;
			try {
				engineVersion = workflowFactoryNode
						.getWorkflowFactoryProperties().getVersion();
			} catch (Exception e) {
				// do nothing
			}
			if (engineVersion == null) {
				engineVersion = "1.0.0";
			}

			XmlObject inner = null;
			Map<String, Object> additionalParams = new HashMap<String, Object>();
			additionalParams.put(C9MWFEditorConstants.PARAM_SUBMISSION_MODE,
					C9MWFEditorConstants.SUBMISSION_MODE_EXPORT);
			additionalParams.put(C9MWFEditorConstants.PARAM_UPLOAD_FILES,
					uploadFiles);
			additionalParams.put(C9MWFEditorConstants.PARAM_UPLOAD_FILES_ID,
					uploadFilesId);
			additionalParams.put(C9MWFEditorConstants.PARAM_WF_ENGINE_VERSION,
					engineVersion);
			if (uploadFiles) {
				determineStorage(workflowFactoryNode, workflowName, wfId,
						terminationTime, additionalParams,
						new SubProgressMonitor(monitor, 10));
			} else {
				monitor.worked(10);
			}
			try {
				inner = (XmlObject) converter.convert(submissionService, wfId,
						model, model, additionalParams, new SubProgressMonitor(
								monitor, 90));
			} catch (Exception e) {
				throw e;
			}

			return inner;
		} finally {
			monitor.done();
		}
	}

	protected void determineStorage(WorkflowFactoryNode workflowFactoryNode,
			String workflowName, String wfId, Calendar terminationTime,
			Map<String, Object> additionalParams, IProgressMonitor monitor)
			throws Exception {

		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		monitor.beginTask("Preparing storage for input files", 100);
		try {
			EndpointReferenceType registryEpr = workflowFactoryNode
					.getRegistryNode().getEpr();
			SubProgressMonitor sub = new SubProgressMonitor(monitor, 20);
			
			StorageClient workDirClient = null;
			sub = new SubProgressMonitor(monitor, 80);
			if (workDirEpr != null) {
				Node node = NodeFactory.createNode(workDirEpr);
				try {
					if (node instanceof StorageNode) {

						workDirClient = ((StorageNode) node).getStorageClient();
						sub.done(); // nothing more to do here
					} else if (node instanceof StorageFactoryNode) {

						workDirClient = DataManagementUtils
								.createWorkflowWorkingDir(
										(StorageFactoryNode) node,
										workflowName, terminationTime, sub);
						workDirEpr = workDirClient.getEPR();
					} else {
						throw new Exception(
								"Unable to submit workflow: selected service for storing files could not be recoginzed.");
					}

				} finally {
					node.dispose();
				}
			} else {
				workDirClient = DataManagementUtils
						.determineWorkflowWorkingDir(registryEpr, workflowName,
								wfId, terminationTime, sub);
				workDirEpr = workDirClient.getEPR();
			}

			additionalParams.put(
					C9MWFEditorConstants.PARAM_SELECTED_STORAGE,
					workDirClient);

		} finally {
			monitor.done();
		}
	}

	public void setWorkDirEpr(EndpointReferenceType workDirEpr) {
		this.workDirEpr = workDirEpr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.ISubmitter#submit(org.eclipse.
	 * core.runtime.IAdaptable,
	 * de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram)
	 */
	public NodePath submit(IAdaptable submissionService, WorkflowDiagram model,
			IProgressMonitor monitor) throws Exception {

		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		monitor.beginTask("", 100);
		try {
			if (!(submissionService instanceof WorkflowFactoryNode)) {
				throw new Exception(
						"Internal error: Cannot submit workflow to a service of this type!");
			}
			if (model.containsCycles()) {
				throw new Exception(
						"Unable to convert the workflow to XML: it contains a cycle! Please remove all cycles.");
			}

			Set<QName> dialects = WFActivator.getDefault()
					.getConverterRegistry()
					.getPossibleTargets(model.getContext().getModelType());
			WorkflowFactoryNode workflowFactoryNode = (WorkflowFactoryNode) submissionService;
			Set<QName> applicable = DialectMatcher.matchSupportedDialects(
					workflowFactoryNode, dialects);
			if (applicable.size() < 1) {
				throw new Exception(
						"Internal error: Cannot submit workflow, selected target does not support its dialect");
			}
			IPath parentPath = Path.fromPortableString(model.getParentFolder());
			IPath modelFile = new Path(model.getFilename());
			String workflowName = modelFile.removeFileExtension().lastSegment()
					+ " " + parentPath.lastSegment();

			// create the workflow
			Integer hoursToLive = (Integer) model
					.getPropertyValue(WorkflowDiagram.PROP_TERMINATION_TIME);
			Calendar terminationTime = null;
			if (hoursToLive != null) {
				terminationTime = Calendar.getInstance();
				terminationTime.add(Calendar.HOUR, hoursToLive);
			}
			
			WFClient wfClient = workflowFactoryNode.createWorkflow(
					workflowName, terminationTime);
			monitor.worked(5);
			String wfId = wfClient.getWorkflowID();
			QName dialect = applicable.iterator().next();
			IConverter converter = WFActivator.getDefault()
					.getConverterRegistry()
					.getConverter(model.getContext().getModelType(), dialect);

			String engineVersion = null;
			try {
				engineVersion = workflowFactoryNode
						.getWorkflowFactoryProperties().getVersion();
			} catch (Exception e) {
				// do nothing
			}
			if (engineVersion == null) {
				engineVersion = "1.0.0";
			}

			XmlObject inner = null;
			Map<String, Object> additionalParams = new HashMap<String, Object>();
			additionalParams.put(C9MWFEditorConstants.PARAM_SUBMISSION_MODE,
					C9MWFEditorConstants.SUBMISSION_MODE_WF_ENGINE);
			additionalParams.put(C9MWFEditorConstants.PARAM_UPLOAD_FILES, true);
			additionalParams.put(C9MWFEditorConstants.PARAM_WF_ENGINE_VERSION,
					engineVersion);

			SubProgressMonitor sub = new SubProgressMonitor(monitor, 10);
			determineStorage(workflowFactoryNode, workflowName, wfId,
					terminationTime, additionalParams, sub);
			try {
				sub = new SubProgressMonitor(monitor, 80);
				inner = (XmlObject) converter.convert(submissionService, wfId,
						model, model, additionalParams, sub);
			} catch (Exception e) {
				abortWorkflow(wfClient, workflowFactoryNode);
				throw e;
			}

			WorkflowDocument doc = WorkflowDocument.Factory.newInstance();

			RulesDocument rules = RulesDocument.Factory.newInstance();
			rules.addNewRules();
			Workflow workflow = doc.addNewWorkflow();

			workflow.set(inner.copy());
			workflow.setDialect(dialect.getNamespaceURI()
					+ dialect.getLocalPart());
			WorkflowWrapper workflowWrapper = new WorkflowWrapper(workflow,
					rules.getRules());
			workflowWrapper.setWorkflowName(workflowName);
			if (workDirEpr != null) {
				workflowWrapper.setStorageAddress(workDirEpr);
			}
			if (monitor.isCanceled()) {
				abortWorkflow(wfClient, workflowFactoryNode);
				return null;
			}

			try {

				wfClient.submitWorkflow(workflowWrapper);
				monitor.worked(3);
				model.getExecutionData().setSubmissionService(
						workflowFactoryNode.getPathToRoot());

				model.getExecutionData().setSubmissionID(
						wfClient.getWorkflowID());
				try {
					TracerNode tracerNode = workflowFactoryNode.getTracerNode();
					if (tracerNode != null) {
						model.getExecutionData().setTracingService(
								tracerNode.getPathToRoot());
					}
				} catch (Exception e) {
					WFEditorActivator.log(Status.WARNING, "Unable to identify tracer to use for message tracing: "+e.getMessage(),e);
				}
				
				try {
					workflowFactoryNode.setExpanded(true);
					workflowFactoryNode.refresh(2, false, null);
					monitor.worked(2);
				} catch (Exception e) {
					e.printStackTrace();
				}
				NodePath result = workflowFactoryNode.getPathToRoot();
				for (Node n : workflowFactoryNode.getChildrenArray()) {
					if (n instanceof GroupNode
							&& WorkflowNode.TYPE.equals(((GroupNode) n)
									.getChildType())) {
						result = result.append(n.getURI());
						break;
					}
				}
				URI wfURI = new URI(wfClient.getEPR().getAddress()
						.getStringValue());
				result = result.append(wfURI);
				return result;
			} catch (Exception e) {
				throw new Exception(
						"Cannot submit workflow to workflow engine!", e);
			}
		} finally {
			monitor.done();
		}

	}

}
