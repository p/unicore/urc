/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.transfers;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.chemomentum.common.util.C9MCommonConstants;
import org.chemomentum.common.ws.ILocationManager;
import org.chemomentum.dataManagement.locationManager.LogicalNameInUseFaultDocument.LogicalNameInUseFault;
import org.chemomentum.dataManagement.locationManager.StoreLocationRequestDocument;
import org.chemomentum.dataManagement.locationManager.StoreLocationRequestDocument.StoreLocationRequest;
import org.chemomentum.dataManagement.locationManager.StoreLocationRequestDocument.StoreLocationRequest.Input;
import org.chemomentum.dataManagement.locationManager.StoreLocationResponseDocument;
import org.chemomentum.dataManagement.locationManager.StoreLocationResponseDocument.StoreLocationResponse.Result;
import org.chemomentum.rcp.wfeditor.model.LMFileAddress;
import org.unigrids.services.atomic.types.ProtocolType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.transfers.FileTransmitter;
import com.intel.gpe.clients.api.transfers.IGridFileTransfer;
import com.intel.gpe.clients.impl.transfers.LocalToU6FileTransmitter;
import com.intel.gpe.util.uri.URIUtil;

import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.uas.client.StorageClient;
import de.fzj.unicore.wsrflite.xmlbeans.client.RegistryClient;
import eu.unicore.security.wsutil.client.UnicoreWSClientFactory;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * @author demuth
 * 
 */
public class LocalToLMFileTransmitter implements FileTransmitter {

	private Exception occuredException = null;

	protected Exception getOccuredException() {
		return occuredException;
	}

	protected void setOccuredException(Exception occuredException) {
		this.occuredException = occuredException;
	}

	protected void storePhysically(Client client,
			StorageClient storageClient, ProtocolType.Enum proto, List<IGridFileTransfer> files,
			IProgressListener progress) throws Exception {



		long totalSize = 0;

		for (IGridFileTransfer file : files) {
			File source = new File(file.getProcessedSource()
					.getInternalString());
			if (!source.exists()) {
				throw new Exception("File to be uploaded does not exist!");
			}
			totalSize += source.length();
		}

		try {
			progress.beginTask("", 100);
			int percent = 0;
			long worked = 0;
			for (IGridFileTransfer file : files) {
				LMFileAddress target = (LMFileAddress) file
				.getProcessedTarget();
				String logical = target.getLogicalName();
				if (logical
						.startsWith(C9MCommonConstants.LOGICAL_FILENAME_PREFIX)) {
					logical = logical
					.substring(C9MCommonConstants.LOGICAL_FILENAME_PREFIX
							.length());
				}
				File source = new File(file.getProcessedSource()
						.getInternalString());
				
				long newWorked = worked + source.length();
				int newPercent = (int) (100.0 * (newWorked / (double) totalSize));
				int diff = newPercent - percent;
				IProgressListener sub = progress.beginSubTask("", diff);
				if (source.isDirectory()) {
					LocalToU6FileTransmitter.uploadFolder(client, storageClient,
							source, logical, proto.toString(), sub);
				} else {
					LocalToU6FileTransmitter.uploadFile(client, storageClient, source,
							logical, proto.toString(), sub);
				}
				worked = newWorked;
				percent += diff;

			}
		} finally {
			progress.done();
		}

	}

	/**
	 * call Location Management Service for mapping logical names to physical
	 * locations
	 */
	protected Result[] storeToLocationManager(EndpointReferenceType lmEpr,
			EndpointReferenceType storageEpr, ProtocolType.Enum proto, List<IGridFileTransfer> files)
	throws Exception {

		String uri = lmEpr.getAddress().getStringValue();
		IClientConfiguration sp = ServiceBrowserActivator.getDefault()
		.getUASSecProps(new URI(uri));
		ILocationManager lm = new UnicoreWSClientFactory(sp)
		.createPlainWSProxy(ILocationManager.class, uri);
		StoreLocationRequestDocument requestDoc = StoreLocationRequestDocument.Factory
		.newInstance();
		StoreLocationRequest request = requestDoc.addNewStoreLocationRequest();

		String storageUri = storageEpr.getAddress().getStringValue();
		for (IGridFileTransfer file : files) {
			LMFileAddress target = (LMFileAddress) file.getProcessedTarget();
			String relative = target.getLogicalName();
			
			try {
				new URI(relative);
			} catch (URISyntaxException e) {
				relative = URIUtil.encodeAll(relative);
			}
			
			String prefix = C9MCommonConstants.LOGICAL_FILENAME_PREFIX;
			String remoteFile = relative;
			if (!relative.startsWith(prefix)) {
				remoteFile = prefix + remoteFile;
			} else {
				relative = relative.substring(prefix.length());
			}
			
			Input input = request.addNewInput();

			input.setLogicalName(remoteFile);
			String value = proto+ ":"+ storageUri + "#" + relative;
			
			input.addNewPhysicalLocation().setStringValue(value);
		}		

		StoreLocationResponseDocument response = lm.storeLocation(requestDoc);
		return response.getStoreLocationResponse().getResultArray();
	}

	public void transmitFiles(Client client, List<IGridFileTransfer> files,
			IProgressListener progress) throws Exception {
		try {
			progress.beginTask("uploading file", 100);

			setOccuredException(null);
			if (files.size() == 0) {
				return;
			}
			LMFileAddress file = (LMFileAddress) files.get(0)
			.getProcessedTarget();
			URI uri = file.getRegistryURI();
			EndpointReferenceType epr = NodeFactory.getEPRForURI(uri);
			IClientConfiguration sp = ServiceBrowserActivator.getDefault()
			.getUASSecProps(uri);
			RegistryClient regClient = new RegistryClient(uri.toString(), epr,
					sp);
			List<EndpointReferenceType> lmEprs = regClient
			.listServices(ILocationManager.PORT);
			if (lmEprs.size() > 0) {
				progress.worked(10);
				EndpointReferenceType storageEpr = NodeFactory
				.getEPRForURI(file.getStorageURI());
				IProgressListener subProgress = progress.beginSubTask(
						"transmitting", 90);
				transmitFilesViaLocationManager(client, lmEprs.get(0),
						storageEpr, files, subProgress);
				if (getOccuredException() != null) {
					throw new Exception("File transfer failed: "
							+ getOccuredException().getMessage(),
							getOccuredException());
				}
			} else {
				throw new Exception(
				"No location manager available for resolving logical file names.");
			}

		} finally {
			progress.done();
		}

	}

	public void transmitFilesViaLocationManager(Client client,
			EndpointReferenceType lmEpr, EndpointReferenceType storageEpr,
			final List<IGridFileTransfer> files, IProgressListener progress)
	throws Exception {

		try {
			progress.beginTask("", files.size() + 1);

			IClientConfiguration secProps = ServiceBrowserActivator.getDefault()
			.getUASSecProps(storageEpr);
			de.fzj.unicore.uas.client.StorageClient storageClient = new de.fzj.unicore.uas.client.StorageClient(
					storageEpr, secProps);
			ProtocolType.Enum[] protos = UnicoreStorageTools.getPreferredProtocolOrder();
			ProtocolType.Enum proto = protos.length == 1 ? protos[0] : storageClient.findSupportedProtocol(protos);

			Result[] results = storeToLocationManager(lmEpr, storageEpr, proto, files);
			progress.worked(1);
			for (int i = 0; i < results.length; i++) {
				LogicalNameInUseFault inUse = results[i]
				                                      .getLogicalNameInUseFault();
				String errorMsg = null;

				if (inUse != null) {
					errorMsg = "Could not store physical location: "
						+ inUse.getShortDescription() + "\n"
						+ inUse.getDetailedDescription();
				}

				if (errorMsg != null) {
					setOccuredException(new Exception(errorMsg));
					return;
				}
			}
			storePhysically(client, storageClient, proto, files, progress);

		} catch (Exception e) {
			setOccuredException(e);
		} finally {
			progress.done();
		}

	}

}
