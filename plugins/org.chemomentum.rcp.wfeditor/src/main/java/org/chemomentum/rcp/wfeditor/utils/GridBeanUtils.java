package org.chemomentum.rcp.wfeditor.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.jobprocessors.C9MProcessingConstants;
import org.chemomentum.rcp.wfeditor.model.OverwriteWFFileLinker;
import org.chemomentum.rcp.wfeditor.model.WFFileLinker;
import org.chemomentum.rcp.wfeditor.ui.ChooseFileDialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionType;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.clients.api.transfers.IAddressOrValue;
import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.clients.api.transfers.IGridFileSetTransfer;
import com.intel.gpe.clients.api.transfers.IGridFileTransfer;
import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.clients.common.requests.TransferGridFilesRequest;
import com.intel.gpe.clients.common.transfers.AddressOrValue;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.jsdl.GPEJobImpl;
import com.intel.gpe.gridbeans.parameters.FileParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileSetParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.parameters.InputFileParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.ParameterTypeProcessorRegistry;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;
import com.intel.gpe.util.sets.Pair;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.StringUtils;
import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSetSink;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSink;
import de.fzj.unicore.rcp.gpe4eclipse.utils.ParameterProcessingUtils;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.model.structures.RepeatUntilActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.WhileActivity;
import de.fzj.unicore.rcp.wfeditor.utils.TransitionUtils;

public class GridBeanUtils {

	private static final String MSG_USE_FILE_FROM_PREVIOUS_ITERATION = "You are trying to import a file does not exist when this activity is processed during the first loop iteration."
		+ "Would you like to import this file from the second iteration on (using the file content created in the previous iteration)?";

	private static final String TITLE_USE_FILE_FROM_PREVIOUS_ITERATION = "Use file from previous loop iteration?";

	private static String askUseFileFromPreviousIteration(
			GPEWorkflowFileSetSink sink, Shell parentShell) {

		IGridBean gb = (IGridBean) sink.getActivity().getAdapter(
				IGridBean.class);
		if (gb == null) {
			return null;
		}
		IFileSetParameterValue fileset = (IFileSetParameterValue) gb.get(sink
				.getParameter().getName());
		if (fileset == null) {
			return null;
		}
		Set<String> filesWithLinker = getFilesWithLinker(sink);

		List<IFileParameterValue> files = fileset.getFiles();
		for (Iterator<IFileParameterValue> it = files.iterator(); it.hasNext();) {
			IFileParameterValue file = it.next();
			if (!filesWithLinker.contains(file.getUniqueId())) {
				it.remove();
			}
		}

		if (parentShell == null) {
			try {
				parentShell = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getShell();
			} catch (Exception e) {
				parentShell = new Shell();
			}
		}

		String baseMsg = MSG_USE_FILE_FROM_PREVIOUS_ITERATION;
		IFileParameterValue file = null;
		if (files.size() == 1) {
			file = files.get(0);
			files = null;
		} else {
			baseMsg += "\n\n";
			baseMsg += "If so, please select the imported file you would like to replace starting with the second iteration:";
		}

		ChooseFileDialog dlg = new ChooseFileDialog(files, parentShell,
				TITLE_USE_FILE_FROM_PREVIOUS_ITERATION, baseMsg);
		int answer = dlg.open();
		if (answer == IDialogConstants.OK_ID) {
			if (file == null) {
				file = dlg.getSelectedFile();
			}
			if (file != null) {
				return file.getUniqueId();
			}
		}
		return null;
	}

	private static boolean askUseFileFromPreviousIteration(
			GPEWorkflowFileSink sink, Shell parentShell) {

		if (parentShell == null) {
			try {
				parentShell = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getShell();
			} catch (Exception e) {
				parentShell = new Shell();
			}
		}
		String baseMsg = MSG_USE_FILE_FROM_PREVIOUS_ITERATION;

		ChooseFileDialog dlg = new ChooseFileDialog(null, parentShell,
				TITLE_USE_FILE_FROM_PREVIOUS_ITERATION, baseMsg);
		int answer = dlg.open();
		return answer == IDialogConstants.OK_ID;
	}

	public static IAddressOrValue createAddressFromVariableName(String name) {
		return new AddressOrValue(name, createInternalString(name),
				C9mRCPCommonConstants.WFVARIABLE_PROTOCOL);
	}

	public static String createInternalString(String variableName) {
		return "${" + variableName + "}";
	}

	public static String createParentWFFileString() {
		return C9mRCPCommonConstants.DMFILE_PREFIX
		+ ProtocolConstants.PROTOCOL_SEPERATOR
		+ ParameterProcessingUtils
		.createVariableFromName(ProcessingConstants.WORKFLOW_ID)
		+ "/"
		+ ParameterProcessingUtils
		.createVariableFromName(GPE4EclipseConstants.ACTIVITY_ID)
		+ ParameterProcessingUtils
		.createVariableFromName(GPE4EclipseConstants.ITERATION_ID)
		+ "/";
	}

	public static String extractVariableName(String internalString) {
		if (internalString == null || internalString.trim().length() < 2) {
			return null;
		}
		return internalString.substring(2);
	}

	public static IFileParameterValue findFileInFileSet(String id,
			IFileSetParameterValue fileset) {
		if (id == null) {
			return null;
		}
		for (IFileParameterValue file : new ArrayList<IFileParameterValue>(
				fileset.getFiles())) {
			if (id.equals(file.getUniqueId())) {
				return file;
			}
		}
		return null;
	}

	public static void fixCurrentFileValue(IFileParameterValue newFile,
			WorkflowFile wfFile, Shell parentShell, IActivity sourceActivity,
			IActivity targetActivity) {
		IGridFileAddress source = newFile.getSource();
		boolean usingWildcards = source.isUsingWildcards();
		if (usingWildcards) {
			usingWildcards = handleWhileLoopCase(newFile, parentShell,
					sourceActivity, targetActivity);
		}
		if (usingWildcards) {
			newFile.getTarget().setUsingWildcards(true);
			newFile.getTarget().setDisplayedString(wfFile.getDisplayedString());
			newFile.getTarget().setRelativePath(wfFile.getRelativePath());
		} else {
			newFile.getSource().setUsingWildcards(false);
			newFile.getTarget().setUsingWildcards(false);
			String nameInWorkingDir = newFile.getTarget().getRelativePath();
			if (nameInWorkingDir == null
					|| nameInWorkingDir.trim().length() == 0) {
				newFile.getTarget().setRelativePath(
						source.getDisplayedString().replaceAll(
								WFConstants.DISPLAYED_STRING_SEPERATOR, "_"));
			}
		}

		// handle special case where we are dealing with a loop
		// that will result in MULTIPLE files in the job working dir
		// in this case we want to automatically add an asterisk to the 
		// file name, but we don't want to mark it as using wildcards, cause
		// this would disallow editing it. editing it should be allowed in this
		// case, because the workflow engine interprets the filename as a
		// pattern
		// and CAN actually adjust it instead of just using file names as they
		// are on the source storage
		String nameInWorkingDir = newFile.getTarget().getRelativePath();
		String displayedString = newFile.getTarget().getDisplayedString();
		if(wfFile.isUsingWildcards() && !newFile.getTarget().isUsingWildcards())
		{

			if(!isFilenamePattern(nameInWorkingDir))
			{
				nameInWorkingDir += "*";
				newFile.getTarget().setRelativePath(nameInWorkingDir);
			}
			if(!isFilenamePattern(displayedString))
			{
				displayedString += "*";
				newFile.getTarget().setDisplayedString(displayedString);
			}
		}
		else if(!wfFile.isUsingWildcards() && displayedString.contains("*"))
		{
			nameInWorkingDir = nameInWorkingDir.replace("*", "");
			newFile.getTarget().setRelativePath(nameInWorkingDir);
			displayedString = displayedString.replace("*", "");
			newFile.getTarget().setDisplayedString(displayedString);
		}

	}

	public static int getFileIndexInFileset(String id,
			IFileSetParameterValue fileset) {
		if (id == null) {
			return -1;
		}
		for (int i = 0; i < fileset.getFiles().size(); i++) {
			IFileParameterValue file = fileset.getFiles().get(i);
			if (id.equals(file.getUniqueId())) {
				return i;
			}
		}
		return -1;
	}

	private static Set<String> getFilesWithLinker(IDataSink sink) {
		Set<String> filesWithLinker = new HashSet<String>();
		for (IDataFlow flow : sink.getIncomingFlows()) {
			if (WFFileLinker.class.equals(flow.getClass())) {
				WFFileLinker linker = (WFFileLinker) flow;
				filesWithLinker.add(linker.getFileId());
			}
		}
		return filesWithLinker;
	}

	public static WFFileLinker getOldLinker(GPEWorkflowFileSink sink) {
		IActivity activity = sink.getActivity();
		IGridBean gb = (IGridBean) activity.getAdapter(IGridBean.class);
		IFileParameterValue oldValue = (IFileParameterValue) gb.get(sink
				.getParameter().getName());
		String id = oldValue.getUniqueId();
		for (IDataFlow flow : sink.getIncomingFlows()) {
			if ((flow instanceof WFFileLinker)) {
				WFFileLinker oldLinker = (WFFileLinker) flow;
				if (id.equals(oldLinker.getFileId())) {
					return oldLinker;
				}
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static JobDefinitionType gridBeanToJSDL(IGridBean gridBean,
			GPEJobImpl job, Client client, boolean uploadFiles,
			Map<String, Object> processorParams, IProgressListener progress)
	throws Exception {
		int scale = 1000;
		progress.beginTask("", 100 * scale);

		// process list of GridBeans parameter values.
		List<IGridBeanParameter> params = new ArrayList<IGridBeanParameter>(
				gridBean.getInputParameters());
		params.addAll(gridBean.getOutputParameters());
		List<IGridBeanParameterValue> values = new ArrayList<IGridBeanParameterValue>();
		for (IGridBeanParameter param : params) {
			IGridBeanParameterValue value = (IGridBeanParameterValue) gridBean
			.get(param.getName());
			// before processing: reset old processed values!
			value.resetProcessedValues();
			values.add(value);
		}

		progress.worked(10);

		IProgressListener subProgress = progress.beginSubTask("", 1 * scale);
		processParameters(client, job,
				C9MProcessingConstants.REPLACE_JOBDIR_WITH_C9M, params, values,
				processorParams, subProgress);

		subProgress = progress.beginSubTask("preparing uploads", 1 * scale);
		processParameters(client, job,
				ProcessingConstants.PREPARE_UPLOADS_TO_GLOBAL_STORAGES, params,
				values, processorParams, subProgress);

		if (uploadFiles) {
			subProgress = progress.beginSubTask("uploading files", 97 * scale);
			IGridFileSetTransfer<IGridFileTransfer> transfers = (IGridFileSetTransfer<IGridFileTransfer>) processorParams
			.get(ProcessingConstants.FILE_TRANSFERS);

			try {
				new TransferGridFilesRequest(client, transfers)
				.perform(subProgress);
			} catch (Throwable e) {
				if (e instanceof Exception) {
					throw (Exception) e;
				} else {
					throw new Exception(e);
				}
			}

			transfers.removeAllFiles();
		}

		subProgress = progress.beginSubTask("", 1 * scale);
		processParameters(client, job,
				ProcessingConstants.WRITE_PROCESSED_PARAMS_TO_JSDL, params,
				values, processorParams, subProgress);
		processParameters(client, job,
				C9MProcessingConstants.WRITE_WFFILE_OVERWRITES_TO_JSDL, params,
				values, processorParams, subProgress);
		if (progress.isCanceled()) {
			return null;
		}
		JobDefinitionType processed = (JobDefinitionType) job.getValue().copy();
		GPEJobImpl.packGBModel(processed);
		processParameters(client, job,
				ProcessingConstants.CLEAN_UP_AFTER_JOB_SUBMISSION, params,
				values, processorParams, subProgress);
		return processed;
	}

	/**
	 * This method is used to handle a special case where a user selected to
	 * stage in a wildcarded file from an activity within a preceeding while
	 * loop in the workflow graph. In this case, we cannot automatically decide,
	 * whether the user wanted to import only files from the last iteration or
	 * all files from all iterations. Therefore we have to ask the user.
	 * 
	 * @param newFileValue
	 * @param parentShell
	 * @param source
	 * @param target
	 * @return
	 */
	private static boolean handleWhileLoopCase(
			IFileParameterValue newFileValue, Shell parentShell,
			IActivity source, IActivity target) {
		String newParent = newFileValue.getSource().getParentDirAddress();
		int iterationIDStart = newParent
		.indexOf(GPE4EclipseConstants.ITERATION_ID) - 2;
		if (iterationIDStart > 0
				&& GridBeanUtils.isWhileLoopBetween(source, target)) {

			// we are dealing with a while loop => find out whether the user
			// wants
			// to stage in all files from that loop or just the last file
			// if(parentShell == null)
			// {
			// try {
			// parentShell =
			// PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
			// } catch (Exception e) {
			// parentShell = new Shell();
			// }
			// }
			// MessageDialog dialog = new
			// MessageDialog(parentShell,"File produced in while loop",null,
			// "You have referred to a file that may be created multiple times within a while loop. Do you mean to import all of these files "
			// +
			// " or just the last one?",MessageDialog.QUESTION,new
			// String[]{"All","Just the last one"},0);
			// int answer = dialog.open();
			// in the current model, the previous lines are actually
			// unnecessary:
			// whether a single file or multiple files should be staged in is
			// determined by the usage of fixed vs. changing filenames and
			// wildcards

			int iterationIDEnd = iterationIDStart
			+ GPE4EclipseConstants.ITERATION_ID.length() + 3;
			newParent = newParent.substring(0, iterationIDStart)
			+ newParent.substring(iterationIDEnd);
			newFileValue.getSource().setParentDirAddress(newParent);

		}
		return newFileValue.getSource().getInternalString().contains("*");
	}

	/**
	 * Returns true if both activities are children of the same while or repeat
	 * loop or one of its subgraphs.
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static boolean isInSameWhileLoop(IActivity source, IActivity target) {
		List<StructuredActivity> sourceParents = new ArrayList<StructuredActivity>();
		List<StructuredActivity> targetParents = new ArrayList<StructuredActivity>();
		StructuredActivity parent = source.getParent();
		while (parent != null) {
			sourceParents.add(parent);
			parent = parent.getParent();
		}
		parent = target.getParent();
		while (parent != null) {
			targetParents.add(parent);
			parent = parent.getParent();
		}

		for (int i = 1; i <= Math.min(sourceParents.size(),
				targetParents.size()); i++) {
			StructuredActivity sourceParent = sourceParents.get(sourceParents
					.size() - i);
			StructuredActivity targetParent = targetParents.get(targetParents
					.size() - i);
			if (sourceParent == targetParent) {
				if (sourceParent instanceof WhileActivity
						|| sourceParent instanceof RepeatUntilActivity) {
					return true;
				}
			} else {
				return false;
			}
		}
		return false;
	}

	/**
	 * Test whether a given file name represents a pattern. Patterns either
	 * contain wildcards like "*" or have workflow variables in them. 
	 * @param s
	 * @return
	 */
	private static boolean isFilenamePattern(String s)
	{
		return s != null && (s.contains("*") || s.contains("${"));
	}
	
	
	/**
	 * Returns true if the source activity is inside a while loop or the
	 * subgraph of a while loop and this while loop is not the parent of the
	 * target activity. This information is needed because an outer activity
	 * that refers to a file produced in while loops can either aggregate all of
	 * the files produced in the while loop, or just use the last file produced
	 * in the while loop. The latter does not make sense for ForEach-loops, as
	 * the files are produced in random order here. In order to find out, what
	 * the user wants, he is asked when this case occurs.
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static boolean isWhileLoopBetween(IActivity source, IActivity target) {
		String myIterationId = target.getUnresolvedIterationId();
		String fileSourceIterationId = source.getUnresolvedIterationId();
		int fileSourceIterationLength = iterationIdLength(fileSourceIterationId);

		int i = 0;
		int lastIterationLength = fileSourceIterationLength;
		IActivity parent = source.getParent();
		int commonIterationLength = iterationIdLength(StringUtils
				.longestCommonPrefix(myIterationId, fileSourceIterationId)); // number
		// of
		// loops
		// that
		// we
		// share
		while (i < fileSourceIterationLength - commonIterationLength) {
			if (parent instanceof WhileActivity
					|| parent instanceof RepeatUntilActivity) {
				return true;
			}
			int currentIterationLength = iterationIdLength(parent
					.getUnresolvedIterationId());
			if (currentIterationLength < lastIterationLength) {
				lastIterationLength = currentIterationLength;
				i++;
			}
			parent = parent.getParent();
		}
		return false;
	}

	/**
	 * Returns the number of iterationID seperators in the given iteration ID -
	 * which is equal to the number of nested loops that surrounds the activity
	 * to wich the iterationId belongs
	 * 
	 * @param iterationId
	 * @return
	 */
	private static int iterationIdLength(String iterationId) {
		return iterationId.split(Constants.ITERATION_ID_SEPERATOR).length - 1;
	}

	public static Pair<IFileParameterValue, WFFileLinker> linkToWorkflowFile(
			WorkflowFile wfFile, GPEWorkflowFileSink sink, Shell parentShell) {
		IActivity activity = sink.getActivity();
		IGridBean gb = (IGridBean) activity.getAdapter(IGridBean.class);

		WFFileLinker oldLinker = getOldLinker(sink);

		WFFileLinker linker = null;
		if (TransitionUtils.wouldCloseLoop(wfFile.getActivity(),
				sink.getActivity())) {
			if (mightUseFileFromPreviousIteration(wfFile, sink)) {
				if (askUseFileFromPreviousIteration(sink, parentShell)) {
					linker = new OverwriteWFFileLinker(wfFile, sink);
					linker.connect();
				} else {
					return new Pair<IFileParameterValue, WFFileLinker>(null,
							null);
				}
			} else {
				return new Pair<IFileParameterValue, WFFileLinker>(null, null);
			}
		}

		IFileParameterValue newValue = null;
		if (linker == null) {
			IFileParameterValue oldValue = (IFileParameterValue) gb.get(sink
					.getParameter().getName());

			newValue = new FileParameterValue();
			if (oldLinker != null) {
				oldLinker.disconnect();
			}
			linker = new WFFileLinker(wfFile, sink);
			linker.connect();
			// reset parentDir address as this might have been changed when the
			// user selected to import the
			// last file of an activity in a while-loop (instead of all files)
			oldValue = oldValue.clone();
			oldValue.getSource().setParentDirAddress(
					GridBeanUtils.createParentWFFileString());

			IGridFileAddress source = linker.getCurrentValue(oldValue
					.getSource());
			oldValue.setSource(null);
			try {
				newValue = oldValue.clone();
			} catch (Exception e) {
				newValue = oldValue;
			}
			newValue.setSource(source);
			fixCurrentFileValue(newValue, wfFile, parentShell,
					wfFile.getActivity(), activity);
		}
		return new Pair<IFileParameterValue, WFFileLinker>(newValue, linker);
	}

	public static Pair<IFileSetParameterValue, WFFileLinker> linkToWorkflowFileSet(
			WorkflowFile wfFile, GPEWorkflowFileSetSink sink, String fileId,
			Shell parentShell) {
		IActivity activity = sink.getActivity();
		IGridBeanParameter param = sink.getParameter();
		IGridBean gb = (IGridBean) activity.getAdapter(IGridBean.class);

		WFFileLinker linker = null;

		if (TransitionUtils.wouldCloseLoop(wfFile.getActivity(),
				sink.getActivity())) {
			if (mightUseFileFromPreviousIteration(wfFile, sink)) {
				if (fileId == null) {
					fileId = askUseFileFromPreviousIteration(sink, parentShell);
				}
				if (fileId == null) {
					return new Pair<IFileSetParameterValue, WFFileLinker>(null,
							null);
				} else {
					linker = new OverwriteWFFileLinker(wfFile, sink, fileId);
					linker.connect();
				}
			} else {
				return new Pair<IFileSetParameterValue, WFFileLinker>(null,
						null);
			}
		}

		IFileSetParameterValue newValue = null;
		if (linker == null) {
			IFileSetParameterValue oldValue = (IFileSetParameterValue) gb
			.get(param.getName());
			try {
				newValue = (IFileSetParameterValue) oldValue.clone();
			} catch (CloneNotSupportedException e) {
				newValue = oldValue;
			}

			int index = getFileIndexInFileset(fileId, newValue);
			IFileParameterValue oldFile = index >= 0 ? newValue.getFiles().get(
					index) : null;
					IFileParameterValue newFile = null;
					if (oldFile == null) {
						newFile = new InputFileParameterValue();
						fileId = newFile.getUniqueId();
						IStageTypeExtensionPoint ext = GPEActivator
						.getDefault()
						.getStageInTypeRegistry()
						.getDefiningExtension(
								C9mRCPCommonConstants.WFFILE_QNAME);
						newFile = ext.attachToParameterValue(activity, newFile);
						newFile.getSource().setParentDirAddress(
								GridBeanUtils.createParentWFFileString());
					} else {
						newFile = oldFile.clone();
					}

					linker = new WFFileLinker(wfFile, sink, fileId);

					linker.connect();
					IGridFileAddress source = linker.getCurrentValue(newFile
							.getSource());
					newFile.setSource(source);

					fixCurrentFileValue(newFile, wfFile, parentShell,
							wfFile.getActivity(), activity);

					if (index >= 0 && index < newValue.getFiles().size()) {
						newValue.addFile(index, newFile);
					} else {
						newValue.addFile(newFile);
					}
		}
		return new Pair<IFileSetParameterValue, WFFileLinker>(newValue, linker);
	}

	private static boolean mightUseFileFromPreviousIteration(
			WorkflowFile wfFile, GPEWorkflowFileSetSink sink) {
		IActivity sourceParent = wfFile.getActivity();
		IActivity sinkParent = sink.getActivity();
		Set<String> filesWithLinker = getFilesWithLinker(sink);
		if (filesWithLinker.size() == 0) {
			return false;
		}
		return isInSameWhileLoop(sourceParent, sinkParent);

	}

	private static boolean mightUseFileFromPreviousIteration(
			WorkflowFile wfFile, GPEWorkflowFileSink sink) {
		IActivity sourceParent = wfFile.getActivity();
		IActivity sinkParent = sink.getActivity();
		if (sink.getIncomingFlows() == null
				|| sink.getIncomingFlows().length != 1) {
			return false;
		}
		if (!(sink.getIncomingFlows()[0] instanceof WFFileLinker)) {
			return false;
		}
		return isInSameWhileLoop(sourceParent, sinkParent);
	}

	protected static void processParameters(Client client, GPEJob job,
			int processStep, List<IGridBeanParameter> inputParameters,
			List<IGridBeanParameterValue> values,
			Map<String, Object> processorParams, IProgressListener progress)
	throws Exception {
		if (progress.isCanceled()) {
			return;
		}
		ParameterTypeProcessorRegistry.getDefaultRegistry().processParameters(
				client, job, processStep, inputParameters, values,
				processorParams, progress);
	}

	public static String resolveIteratorInWFFileBeforeSubmission(
			String wfFileString, IActivity target) {
		int iterationIDStart = wfFileString
		.indexOf(GPE4EclipseConstants.ITERATION_ID) - 2;
		if (iterationIDStart > 0) {
			int activityNameStart = wfFileString.lastIndexOf("/",
					iterationIDStart) + 1;
			String fileSourceName = wfFileString.substring(activityNameStart,
					iterationIDStart);
			IActivity fileSource = target.getDiagram().getActivityByName(
					fileSourceName);
			if (fileSource != null) {
				wfFileString = resolveIteratorInWFFileBeforeSubmission(
						wfFileString, fileSource, target);
			}
		}
		return wfFileString;
	}

	/**
	 * Tries to replace the {@link GPE4EclipseConstants#ITERATION_ID} variable
	 * in a WFFileAddress with a proper iteration ID, taking into account the
	 * position of the WFFile's source and target activity in the workflow
	 * (regarding loops).
	 * 
	 * @param wfFileString
	 *            The complete wfFileString as returned by
	 *            {@link #createParentWFFileString()}, containing
	 *            {@link GPE4EclipseConstants#ITERATION_ID}
	 * @param target
	 *            The target activity of the workflow file transfer.
	 * @return the resolved WFFileAddress without
	 *         {@link GPE4EclipseConstants#ITERATION_ID}
	 */
	public static String resolveIteratorInWFFileBeforeSubmission(
			String wfFileString, IActivity source, IActivity target) {
		int iterationIDStart = wfFileString
		.indexOf(GPE4EclipseConstants.ITERATION_ID) - 2;
		if (iterationIDStart >= 0) {
			int iterationIDEnd = iterationIDStart
			+ GPE4EclipseConstants.ITERATION_ID.length() + 3;

			String myIterationId = target.getUnresolvedIterationId();

			String fileSourceIterationId = source.getUnresolvedIterationId();
			int fileSourceIterationLength = iterationIdLength(fileSourceIterationId);

			String commonIterationId = StringUtils.longestCommonPrefix(
					myIterationId, fileSourceIterationId);
			int commonIterationLength = iterationIdLength(commonIterationId); 
			// number of loops that we share
			String resultingIterationId = "";
			if (fileSourceIterationLength > commonIterationLength) {
				// we are referencing a file created inside a loop from the
				// outside
				// gather files from all iterations of the inner loop
				resultingIterationId = myIterationId
				+ Constants.ITERATION_ID_SEPERATOR + "*"; 
			} else {
				// we are referencing a file created outside from the inside of
				// a loop
				// or we are within the same loop
				resultingIterationId = fileSourceIterationId;
			}
			wfFileString = wfFileString.substring(0, iterationIDStart)
			+ resultingIterationId
			+ wfFileString.substring(iterationIDEnd);
		}
		return wfFileString;
	}
}
