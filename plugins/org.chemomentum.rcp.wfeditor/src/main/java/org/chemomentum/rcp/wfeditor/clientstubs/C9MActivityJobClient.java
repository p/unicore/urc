/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.clientstubs;

import java.util.Calendar;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlException;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionDocument;
import org.w3.x2005.x08.addressing.EndpointReferenceType;
import org.w3c.dom.Element;

import com.intel.gpe.clients.api.Job;
import com.intel.gpe.clients.api.LoggingJobClient;
import com.intel.gpe.clients.api.Status;
import com.intel.gpe.clients.api.StorageClient;
import com.intel.gpe.clients.api.TargetSystemClient;
import com.intel.gpe.clients.api.exceptions.FaultWrapper;
import com.intel.gpe.clients.api.exceptions.GPEInvalidQueryExpressionException;
import com.intel.gpe.clients.api.exceptions.GPEInvalidResourcePropertyQNameException;
import com.intel.gpe.clients.api.exceptions.GPEJobNotAbortedException;
import com.intel.gpe.clients.api.exceptions.GPEJobNotHeldException;
import com.intel.gpe.clients.api.exceptions.GPEJobNotResumedException;
import com.intel.gpe.clients.api.exceptions.GPEJobNotStartedException;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareRemoteException;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareServiceException;
import com.intel.gpe.clients.api.exceptions.GPEQueryEvaluationErrorException;
import com.intel.gpe.clients.api.exceptions.GPEResourceNotDestroyedException;
import com.intel.gpe.clients.api.exceptions.GPEResourceUnknownException;
import com.intel.gpe.clients.api.exceptions.GPESecurityException;
import com.intel.gpe.clients.api.exceptions.GPETerminationTimeChangeRejectedException;
import com.intel.gpe.clients.api.exceptions.GPEUnableToSetTerminationTimeException;
import com.intel.gpe.clients.api.exceptions.GPEUnknownQueryExpressionDialectException;
import com.intel.gpe.clients.api.exceptions.GPEUnmarshallingException;
import com.intel.gpe.clients.impl.exception.GPEFaultWrapper;
import com.intel.gpe.gridbeans.jsdl.GPEJobImpl;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.uas.StorageManagement;
import de.fzj.unicore.wsrflite.xmlbeans.client.RegistryClient;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * JobClient that represents a Chemomentum Workflow activity.
 * 
 * @author demuth
 * 
 */
@XStreamAlias("C9MActivityJobClient")
@SuppressWarnings("unchecked")
public class C9MActivityJobClient implements LoggingJobClient {

	private transient JobDefinitionDocument originalJSDL;
	private String originalJSDLString;

	private transient EndpointReferenceType locationMapperEpr;

	private String locationMapperString;

	private transient StorageClient storageClient;

	//TODO remove this
	private Boolean useDMAS = false;

	private transient EndpointReferenceType globalStorageEpr;

	private String globalStorageString;

	/**
	 * Prefix that is used for making relative paths on the associated storage
	 * client absolute.
	 */
	private String id;

	public C9MActivityJobClient(JobDefinitionDocument originalJSDL, String id,
			EndpointReferenceType locationMapperEpr, boolean useDMAS,
			EndpointReferenceType globalStorageEpr) {
		this.originalJSDL = originalJSDL;
		originalJSDLString = originalJSDL.toString();
		this.id = id;
		this.locationMapperEpr = locationMapperEpr;
		this.locationMapperString = locationMapperEpr == null ? ""
				: locationMapperEpr.toString();
		this.useDMAS = useDMAS;
		this.globalStorageEpr = globalStorageEpr;
		this.globalStorageString = globalStorageEpr == null ? ""
				: globalStorageEpr.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.JobClient#abort()
	 */
	public void abort() throws GPEMiddlewareServiceException,
	GPESecurityException, GPEResourceUnknownException,
	GPEJobNotAbortedException, GPEMiddlewareRemoteException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSLTClient#destroy()
	 */
	public void destroy() throws GPEResourceUnknownException,
	GPEResourceNotDestroyedException, GPESecurityException,
	GPEMiddlewareRemoteException, GPEMiddlewareServiceException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSLTClient#getCurrentTime()
	 */
	public Calendar getCurrentTime() throws GPEResourceUnknownException,
	GPEInvalidResourcePropertyQNameException, GPESecurityException,
	GPEUnmarshallingException, GPEMiddlewareRemoteException,
	GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.JobClient#getOriginalJSDL()
	 */
	public Job getExecutionJSDL()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		return getOriginalJSDL();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.LoggingJobClient#getExecutionLog()
	 */
	public String getExecutionLog()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	public String getFullAddress() {
		return id;
	}

	private EndpointReferenceType getGlobalStorageEpr() {
		// deserialize EPR if necessary
		if (globalStorageEpr == null && globalStorageString != null
				&& globalStorageString.trim().length() > 0) {
			try {
				globalStorageEpr = EndpointReferenceType.Factory
						.parse(globalStorageString);
			} catch (XmlException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return globalStorageEpr;
	}

	private EndpointReferenceType getLocationMapperEpr() {
		// deserialize EPR if necessary
		if (locationMapperEpr == null && locationMapperString != null
				&& locationMapperString.trim().length() > 0) {
			try {
				locationMapperEpr = EndpointReferenceType.Factory
						.parse(locationMapperString);
			} catch (XmlException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return locationMapperEpr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#getMultipleResourceProperties(javax
	 * .xml.namespace.QName[])
	 */
	public Element[] getMultipleResourceProperties(QName[] resourceProperties)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	public String getName() throws GPEInvalidResourcePropertyQNameException,
	GPEResourceUnknownException, GPEUnmarshallingException,
	GPESecurityException, GPEMiddlewareRemoteException,
	GPEMiddlewareServiceException {
		return getURL().toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.JobClient#getExecutionJSDL()
	 */
	public Job getOriginalJSDL()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (originalJSDL == null) {
			try {
				originalJSDL = JobDefinitionDocument.Factory
						.parse(originalJSDLString);
			} catch (XmlException e) {
				throw new GPEUnmarshallingException(
						"Unable to restore saved job description ", e, this);
			}
		}
		return new GPEJobImpl(originalJSDL.getJobDefinition());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#getResourceProperty(javax.xml.namespace
	 * .QName)
	 */
	public Element getResourceProperty(QName resourceProperty)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSRPClient#getResourcePropertyDocument()
	 */
	public Element getResourcePropertyDocument() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.JobClient#getStatus()
	 */
	public <StatysType extends Status> Status getStatus()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.LoggingJobClient#getStderr()
	 */
	public String getStderr() throws GPEInvalidResourcePropertyQNameException,
	GPEResourceUnknownException, GPEUnmarshallingException,
	GPESecurityException, GPEMiddlewareRemoteException,
	GPEMiddlewareServiceException {
		return "stderr";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.LoggingJobClient#getStdout()
	 */
	public String getStdout() throws GPEInvalidResourcePropertyQNameException,
	GPEResourceUnknownException, GPEUnmarshallingException,
	GPESecurityException, GPEMiddlewareRemoteException,
	GPEMiddlewareServiceException {
		return "stdout";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.JobClient#getTargetSystem()
	 */
	public <TargetSystemClientType extends TargetSystemClient> TargetSystemClientType getTargetSystem()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSLTClient#getTerminationTime()
	 */
	public Calendar getTerminationTime() throws GPEResourceUnknownException,
	GPEInvalidResourcePropertyQNameException, GPESecurityException,
	GPEUnmarshallingException, GPEMiddlewareRemoteException,
	GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSRFClient#getURL()
	 */
	public String getURL() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.JobClient#getWorkingDirectory()
	 */
	public StorageClient getWorkingDirectory()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (storageClient == null) {
			if (useDMAS == null || (!useDMAS && globalStorageString == null)) // needed
				// for
				// backwards
				// compatibility
			{
				EndpointReferenceType epr = getLocationMapperEpr();
				try {
					IClientConfiguration sp = ServiceBrowserActivator
							.getDefault().getUASSecProps(epr);
					RegistryClient regClient = new RegistryClient(epr
							.getAddress().getStringValue(), epr, sp);
					List<EndpointReferenceType> storageEprs = regClient
							.listServices(StorageManagement.SMS_PORT);
					if (storageEprs.size() > 0) {
						globalStorageEpr = storageEprs.get(0);
						globalStorageString = globalStorageEpr.toString();
					} else {
						throw new Exception(
								"Unable to find a storage for downloading files!");
					}
				} catch (Exception e) {
					FaultWrapper fw = new GPEFaultWrapper(null);
					throw new GPEResourceUnknownException(e.getMessage(),
							fw, storageClient);
				}

			}
			storageClient = new C9MStorageClient(id, getLocationMapperEpr(),
					useDMAS, getGlobalStorageEpr());
		}
		return storageClient;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.JobClient#hold()
	 */
	public void hold() throws GPEMiddlewareServiceException,
	GPESecurityException, GPEJobNotHeldException,
	GPEResourceUnknownException, GPEMiddlewareRemoteException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#queryXPath10Properties(java.lang
	 * .String)
	 */
	public Element[] queryXPath10Properties(String expression)
			throws GPEResourceUnknownException,
			GPEInvalidResourcePropertyQNameException,
			GPEInvalidQueryExpressionException,
			GPEQueryEvaluationErrorException,
			GPEUnknownQueryExpressionDialectException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.JobClient#resume()
	 */
	public void resume() throws GPEMiddlewareServiceException,
	GPESecurityException, GPEResourceUnknownException,
	GPEJobNotResumedException, GPEMiddlewareRemoteException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSLTClient#setTerminationTime(java.util.Calendar
	 * )
	 */
	public void setTerminationTime(Calendar newTT)
			throws GPEResourceUnknownException,
			GPEUnableToSetTerminationTimeException,
			GPETerminationTimeChangeRejectedException, GPESecurityException,
			GPEMiddlewareRemoteException, GPEMiddlewareServiceException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.JobClient#start()
	 */
	public void start() throws GPEJobNotStartedException,
	GPEResourceUnknownException, GPEMiddlewareRemoteException,
	GPEMiddlewareServiceException, GPESecurityException {
		// TODO Auto-generated method stub

	}

}
