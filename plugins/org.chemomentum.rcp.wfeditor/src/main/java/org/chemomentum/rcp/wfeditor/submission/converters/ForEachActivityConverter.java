/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.submission.converters;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.commons.httpclient.URI;
import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.C9MWFEditorConstants;
import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.chemomentum.rcp.wfeditor.clientstubs.C9MActivityJobClient;
import org.chemomentum.rcp.wfeditor.model.DataSourceModel;
import org.chemomentum.rcp.wfeditor.model.ForEachActivity;
import org.chemomentum.rcp.wfeditor.model.ForEachDataSourceActivity;
import org.chemomentum.rcp.wfeditor.utils.GridBeanUtils;
import org.chemomentum.simpleworkflow.api.ForEachLoopElement;
import org.chemomentum.simpleworkflow.api.IElement;
import org.chemomentum.simpleworkflow.api.SubWorkflowElement;
import org.chemomentum.simpleworkflow.xmlbeans.OptionDocument.Option;
import org.chemomentum.simpleworkflow.xmlbeans.VariableType;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.transfers.IGridFileSetTransfer;
import com.intel.gpe.clients.api.transfers.IGridFileTransfer;
import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.clients.common.transfers.GridFileSetTransfer;
import com.intel.gpe.clients.impl.transfers.U6ProtocolConstants;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.jsdl.GPEJobImpl;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileSetParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;
import com.intel.gpe.util.uri.URIUtil;

import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.SWTProgressListener;
import de.fzj.unicore.rcp.gpe4eclipse.utils.ParameterProcessingUtils;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.WebServiceNode;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableIterator;
import de.fzj.unicore.rcp.wfeditor.submission.IConverter;
import de.fzj.unicore.uas.client.StorageClient;
import eu.unicore.util.Log;

/**
 * @author demuth
 * 
 */
public class ForEachActivityConverter extends LoopActivityConverter implements IConverter {

	private static final String OPTION_MAX_CONCURRENT_ACTIVITIES = "unicore.workflow.forEach.maxConcurrentActivities";
	private static final String ITERATOR_VALUE_SUFFIX = "_VALUE";


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.IConverter#canConvert(javax.xml
	 * .namespace.QName, javax.xml.namespace.QName)
	 */
	public boolean canConvert(IAdaptable submissionService, QName modelType,
			QName wfLanguageType) {
		return getModelType().equals(modelType)
		&& C9mRCPCommonConstants.C9M_DEFAULT_WF_DIALECT
		.equals(wfLanguageType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.IConverter#convert(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram)
	 */
	public IElement convert(IAdaptable submissionService, String workflowId,
			WorkflowDiagram model, IFlowElement element,
			Map<String, Object> additionalParams, IProgressMonitor monitor)
	throws Exception {

		ForEachActivity activity = (ForEachActivity) element;

		try {
			String wfEngineVersion = (String) additionalParams
			.get(C9MWFEditorConstants.PARAM_WF_ENGINE_VERSION);
			String minimumVersion = "2.0.0";
			if (minimumVersion.compareTo(wfEngineVersion) > 0) {
				throw new Exception(
						"The selected workflow engine does not support For-Loops! Please choose a newer engine (version >= "
						+ minimumVersion + ").");
			}

			SubWorkflowElement graph = (SubWorkflowElement) additionalParams
			.get(C9MWFEditorConstants.PARAM_SUBGRAPH);

			ForEachDataSourceActivity dataSource = activity
			.getDataSourceActivity();

			ForEachLoopElement sub = new ForEachLoopElement(activity.getName(),
					graph);
			String iteratorName = dataSource.getIteratorName();
			sub.setIteratorName(iteratorName);

			String iteratorFixVersion = "6.4.1";
			String iterationMode = dataSource.getIterationMode();

			if (ForEachDataSourceActivity.ITERATION_MODE_FILES.equals(iterationMode)
					|| ForEachDataSourceActivity.ITERATION_MODE_INDIRECTED_FILES.equals(iterationMode)) {

				boolean isIndirected = ForEachDataSourceActivity.ITERATION_MODE_INDIRECTED_FILES.equals(iterationMode);
				if (iteratorFixVersion.compareTo(wfEngineVersion) <= 0) {
					// newer workflow engine versions use
					// ..ITERATOR_VALUE instead of just ..ITERATOR
					// => must fix for new versions
					fixWorkflowFile(dataSource);
				}
				processFilesets(submissionService, workflowId, activity, sub, additionalParams, monitor, isIndirected);
			} else if (ForEachDataSourceActivity.ITERATION_MODE_WORKFLOW_VAR.equals(iterationMode)) {
				processVariables(submissionService, workflowId, activity, sub, additionalParams, monitor);
			} else if (ForEachDataSourceActivity.ITERATION_MODE_VALUE_SET.equals(iterationMode)) {
				processValueSet(submissionService, workflowId, activity, sub, additionalParams, monitor);
			} else {
				WFEditorActivator.log(IStatus.WARNING, "Unkonown iteration mode in ForEach construct.");
			}

			StructuredActivity body = activity.getBody();
			if (body.getChildren().size() == 0) {
				throw new Exception(
				"Invalid workflow: Found ForEach-Loop with empty body. Cancelling.");
			}

			convertBody(sub, submissionService,
					workflowId, model, activity, element, additionalParams,
					monitor);

			List<Option> options = new ArrayList<Option>();
			Integer maxTasks = (Integer) activity.getDataSourceActivity()
			.getPropertyValue(
					ForEachDataSourceActivity.PROP_MAX_PARALLEL_TASKS);
			if (maxTasks != null) {
				Option maxConcurrentActivities = Option.Factory.newInstance();
				maxConcurrentActivities
				.setName(OPTION_MAX_CONCURRENT_ACTIVITIES);
				maxConcurrentActivities.setStringValue(maxTasks.toString());
				options.add(maxConcurrentActivities);
			}

			if (options.size() > 0) {
				sub.setOptions(options);
			}

			additionalParams.put(C9MWFEditorConstants.PARAM_SUBGRAPH, graph);

			if (monitor.isCanceled()) {
				return null;
			}
			return sub;

		} finally {
			monitor.done();
		}
	}

	/**
	 * @param submissionService
	 * @param workflowId
	 * @param activity
	 * @param sub
	 * @param additionalParams
	 * @param monitor
	 */
	private void processValueSet(IAdaptable submissionService, String workflowId, ForEachActivity activity,
			ForEachLoopElement sub, Map<String, Object> additionalParams, IProgressMonitor monitor) {
		ForEachDataSourceActivity dsAct = activity.getDataSourceActivity();
		List<String> valueSet = (List<String>) dsAct.getValueSet();
		for (String value : valueSet) {
			sub.addValue(value);	
		}
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.submission.IConverter#getModelType()
	 */
	public QName getModelType() {
		return ForEachActivity.TYPE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.submission.IConverter#getTargetType()
	 */
	public QName getTargetType() {
		return C9mRCPCommonConstants.C9M_DEFAULT_WF_DIALECT;
	}

	protected void processFilesets(IAdaptable submissionService,
			String workflowId, ForEachActivity activity,
			ForEachLoopElement sub, Map<String, Object> additionalParams,
			IProgressMonitor monitor, boolean isIndirected) throws Exception {

		// chunk size is only available for file set iterations
		Long chunkSize = (Long) activity.getDataSourceActivity()
		.getPropertyValue(ForEachDataSourceActivity.PROP_CHUNK_SIZE);
		// chunk size is only available for file set iterations
		Boolean chunkSizeIsKbytes = (Boolean) activity.getDataSourceActivity()
		.getPropertyValue(ForEachDataSourceActivity.PROP_CHUNK_SIZE_KBYTES);

		Object adapter = submissionService.getAdapter(RegistryNode.class);
		WebServiceNode service = null;
		if (adapter != null) {
			service = (RegistryNode) adapter;
		} else {
			throw new Exception(
			"Could not find valid location mapper for resolving logical names in the workflow.");
		}
		EndpointReferenceType registryEpr = service.getEpr();

		ForEachDataSourceActivity dataSource = activity.getDataSourceActivity();

		IGridBean gridBean = dataSource.getGridBean();

		// process gridbean parameters as usual
		// this will upload local files
		// afterwards, we'll have valid input file
		// addresses
		GPEJobImpl job = new GPEJobImpl();
		gridBean.setupJobDefinition(job);

		StorageClient sc = (StorageClient) additionalParams
		.get(C9MWFEditorConstants.PARAM_SELECTED_STORAGE);
		EndpointReferenceType epr = sc == null ? null : sc.getEPR();
		C9MActivityJobClient jobClient = new C9MActivityJobClient(
				job.getDocument(), "", registryEpr, sc == null, epr);
		Map<String, Object> processorParams = new HashMap<String, Object>();
		processorParams.put(ProcessingConstants.WORKFLOW_ID, workflowId);
		processorParams.put(GPE4EclipseConstants.ACTIVITY_ID,
				dataSource.getName());
		// iterationId is empty
		processorParams.put(GPE4EclipseConstants.ITERATION_ID, "");
		processorParams.put(ProcessingConstants.JOB_WORKING_DIR,
				jobClient.getWorkingDirectory());
		IGridFileSetTransfer<IGridFileTransfer> transfers = new GridFileSetTransfer<IGridFileTransfer>();
		processorParams.put(ProcessingConstants.FILE_TRANSFERS, transfers);
		IFileSetParameterValue value = (IFileSetParameterValue) gridBean
		.get(DataSourceModel.INPUT);
		value = (IFileSetParameterValue) value.clone();
		resetInputFileNames(value);
		gridBean.set(DataSourceModel.INPUT,value);
		
		boolean uploadFiles = (Boolean) additionalParams
		.get(C9MWFEditorConstants.PARAM_UPLOAD_FILES);
		if (uploadFiles) {
			processorParams.put(C9MWFEditorConstants.PARAM_UPLOAD_FILES_ID,
					additionalParams
					.get(C9MWFEditorConstants.PARAM_UPLOAD_FILES_ID));
		}
		
		
		IProgressListener progress = new SWTProgressListener(monitor);
		// now process to JSDL which will upload files if necessary
		GridBeanUtils.gridBeanToJSDL(gridBean, job,
				GPEActivator.getDefault().getClient(), uploadFiles,
				processorParams, progress);

		// now iterate over input files and add them to the subflow that
		// represents the for-loop
		value = (IFileSetParameterValue) gridBean.get(DataSourceModel.INPUT);
		List<IFileParameterValue> files = value.getFiles();
		if(files == null || files.size() == 0)
		{
			throw new Exception(activity.getName()+" set to file " +
					"iteration mode, but file list is empty!");
		}
		for (IFileParameterValue file : files) {
			String baseDir = file.getProcessedSource().getParentDirAddress();
			baseDir = ParameterProcessingUtils.replaceVariables(baseDir,
					processorParams);
			String relative = file.getProcessedSource().getRelativePath();
			String protocolPrefix = file.getProcessedSource().getProtocol()
			.getPrefix();

			// TODO this is a hack
			if (U6ProtocolConstants.UNICORE_SMS_PROTOCOL.equals(file
					.getProcessedSource().getProtocol())) {
				protocolPrefix = UnicoreCommonActivator.getDefault()
				.getDefaultFileTransferProtocol();
				protocolPrefix += ProtocolConstants.PROTOCOL_SEPERATOR;
				if (!baseDir.startsWith(protocolPrefix)) {
					baseDir = protocolPrefix + baseDir;
				}
			}
			String uri = baseDir+relative;
			try {
				new java.net.URI(uri);
			} catch (URISyntaxException e) {
				relative = org.apache.commons.httpclient.util.URIUtil.encode(relative,URI.allowed_rel_path);
			}
			sub.addFileset(baseDir, new String[] { relative }, null, false, isIndirected);
		}
		if(chunkSize > 1)
		{
			sub.setChunksize(chunkSize);
			sub.setChunksizeIsKbytes(chunkSizeIsKbytes);
		}

	}
	
	/**
	 * This method fixes the target names of input files. They are forcibly
	 * changed to match the original file names. This is necessary if the user
	 * first selects one file and then changes the file, in which case the 
	 * transfer will retain the original file name (which is invisible to the
	 * user, because that column of the stage-in view is not shown).
	 * @param value
	 */
	protected void resetInputFileNames(IFileSetParameterValue fileset)
	{
		for(IFileParameterValue file : fileset.getFiles())
		{
			file.getTarget().setRelativePath(file.getSource().getRelativePath());
		}
	}

	protected void processVariables(IAdaptable submissionService,
			String workflowId, ForEachActivity activity,
			ForEachLoopElement sub, Map<String, Object> additionalParams,
			IProgressMonitor monitor) throws Exception {
		ForEachDataSourceActivity dataSource = activity.getDataSourceActivity();
		List<WorkflowVariableIterator> iterators = dataSource
		.getVariableIteratorList().getIterators();
		if (iterators == null || iterators.size() == 0) {
			throw new Exception("No variables defined for For-Loop "
					+ activity.getName());
		}
		for (WorkflowVariableIterator iterator : iterators) {
			String variableName = iterator.getVariable().getName();
			VariableType.Enum type = VariableType.Enum.forString(iterator
					.getVariable().getType().toUpperCase());
			String initialValue = iterator.getVariable().getInitialValue();

			String expression = iterator.getModifier().getInternalExpression();
			String condition = iterator.getTerminateCondition()
			.getInternalExpression();
			sub.addVariableset(variableName, initialValue, type, expression,
					condition);
		}
	}

	private void fixWorkflowFile(ForEachDataSourceActivity dataSource)
	{
		WorkflowFile wfFile = dataSource.getOutputFile();
		if(wfFile != null)
		{
			String path = wfFile.getRelativePath(); 
			path = path.replaceFirst("\\}", ITERATOR_VALUE_SUFFIX+"}");
			wfFile.setRelativePath(path);
		}
	}



}