/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.model;

import java.beans.PropertyChangeEvent;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.wfeditor.utils.SimpleWFUtil;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.structures.ContainerActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.LoopActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifier;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

/**
 * @author demuth
 * 
 */
@XStreamAlias("ForEachActivity")
public class ForEachActivity extends LoopActivity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8131594136004796698L;

	public static final QName TYPE = new QName("http://www.unicore.eu/",
			"ForEachActivity");

	protected ForEachDataSourceActivity dataSourceActivity;
	protected StructuredActivity body;

	public ForEachActivity() {
	}


	@Override
	public void dispose() {
		super.dispose();
		getDataSourceActivity().removePersistentPropertyChangeListener(this);
	}

	public StructuredActivity getBody() {
		return body;
	}

	@Override
	public ForEachActivity getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		ForEachActivity result = (ForEachActivity) super.getCopy(toBeCopied,
				mapOfCopies, copyFlags);
		result.dataSourceActivity = CloneUtils.getFromMapOrCopy(
				dataSourceActivity, toBeCopied, mapOfCopies, copyFlags);
		result.body = CloneUtils.getFromMapOrCopy(body, toBeCopied,
				mapOfCopies, copyFlags);
		return result;
	}

	public ForEachDataSourceActivity getDataSourceActivity() {
		return dataSourceActivity;
	}


	@Override
	public WorkflowVariable getIterationCounter() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WorkflowVariableModifier getIterationIncrementer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public QName getType() {
		return TYPE;
	}

	@Override
	public void init() {
		super.init();

		StructuredActivity body = new ContainerActivity();
		setBody(body);
		addChild(body);
		body.setDeletable(false);
		body.setBoundToParent(true);
		getBody().setDiagram(getDiagram());

		dataSourceActivity = new ForEachDataSourceActivity();
		setStartActivity(dataSourceActivity);
		addChild(dataSourceActivity);
		dataSourceActivity.setDeletable(false);
		dataSourceActivity.setBoundToParent(true);
		dataSourceActivity.setDiagram(getDiagram());

		// now create transitions
		Transition transition = new Transition(dataSourceActivity, body);

		transition.setDeletable(false);
		transition.setBoundToParent(true);
		setName(getDiagram().getUniqueActivityName("ForEachActivity"));
		getBody().setName(getDiagram().getUniqueActivityName("ForEachBody"));
		getDataSourceActivity().setName(
				getDiagram().getUniqueActivityName("ForEach"));
		setPropertyValue(PROP_ITERATOR_NAME, getDataSourceActivity()
				.getIteratorName());
		getDataSourceActivity().addPersistentPropertyChangeListener(this);

		getDataSourceActivity().init();
		getBody().init();

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		super.propertyChange(evt);
		if (PROP_NAME.equals(evt.getPropertyName())
				&& getDataSourceActivity() != null
				&& getDataSourceActivity().equals(evt.getSource())) {
			// update your iterator
			setPropertyValue(PROP_ITERATOR_NAME,
					SimpleWFUtil.iteratorNameFromActivityName((String) evt
							.getNewValue()));
		}
	}

	public void setBody(StructuredActivity ifBody) {
		this.body = ifBody;
	}

}
