package org.chemomentum.rcp.wfeditor.utils;

import org.chemomentum.simpleworkflow.api.ActivityBuilder;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityDocument.Activity;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityType;
import org.chemomentum.simpleworkflow.xmlbeans.VariableType;

import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifier;

public class SimpleWFUtil {
	public static VariableType.Enum convertVariableType(String type) {
		if (WorkflowVariable.VARIABLE_TYPE_STRING.equals(type)) {
			return VariableType.STRING;
		} else if (WorkflowVariable.VARIABLE_TYPE_INTEGER.equals(type)) {
			return VariableType.INTEGER;
		} else if (WorkflowVariable.VARIABLE_TYPE_FLOAT.equals(type)) {
			return VariableType.FLOAT;
		}
		return null;
	}

	public static Activity createVariableModifierActivity(String id,
			String variableName, String expression) {
		ActivityBuilder builder = new ActivityBuilder(id);
		builder.ofType(ActivityType.MODIFY_VARIABLE);
		builder.addOption("variableName", variableName);
		builder.addOption("expression", expression);
		Activity result = builder.getActivity();
		result.setName(id);
		return result;
	}

	public static Activity createVariableModifierActivity(String id,
			WorkflowVariableModifier modifier) {
		String expression = modifier.getInternalExpression();
		if (!expression.endsWith(";")) {
			expression += ";";
		}
		return createVariableModifierActivity(id, modifier.getVariableName(),
				expression);
	}

	public static String iteratorNameFromActivityName(String activityName) {
		return activityName + "_ITERATOR";
	}
}
