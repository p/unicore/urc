/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.jobprocessors;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.wfeditor.C9MWFEditorConstants;
import org.chemomentum.rcp.wfeditor.clientstubs.C9MStorageClient;
import org.chemomentum.rcp.wfeditor.utils.GridBeanUtils;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.StorageClient;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.clients.api.transfers.IGridFileSetTransfer;
import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.FileProtocolProcessor;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;

import de.fzj.unicore.rcp.gpe4eclipse.utils.ParameterProcessingUtils;

/**
 * @author demuth
 * 
 */
public class LocalToC9MProcessor extends FileProtocolProcessor {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.gridbeans.parameters.IProtocolProcessor#getProcessingStep()
	 */
	public LocalToC9MProcessor() {
		processingSteps
				.add(ProcessingConstants.PREPARE_UPLOADS_TO_GLOBAL_STORAGES);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.gridbeans.parameters.IProtocolProcessor#getSourceProtocol()
	 */
	@Override
	public QName getSourceProtocol() {
		return ProtocolConstants.LOCAL_QNAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.gridbeans.parameters.IProtocolProcessor#getTargetProtocol()
	 */
	@Override
	public QName getTargetProtocol() {
		return ProtocolConstants.JOB_WORKING_DIR_QNAME;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void process(Client client, GPEJob job, int processingStep,
			IGridBeanParameter param, IGridBeanParameterValue value,
			Map<String, Object> processorParams, IProgressListener progress)
			throws Exception {
		StorageClient storageClient = (StorageClient) processorParams
				.get(ProcessingConstants.JOB_WORKING_DIR);
		IFileParameterValue file = (IFileParameterValue) value;
		if (processingStep == ProcessingConstants.PREPARE_UPLOADS_TO_GLOBAL_STORAGES) {
			File f = new File(file.getProcessedSource().getInternalString());
			if (!f.exists()) {
				if (!f.isAbsolute()) {
					client.getFileFactory().getTemporaryFile(
							file.getProcessedSource().getInternalString());
				} else {
					throw new FileNotFoundException("File " + f
							+ " could not be uploaded since it does not exist.");
				}
			}
			if (!(storageClient instanceof C9MStorageClient)) {
				// we're not in a workflow => return
				return;
			}
			String nameInJobDir = file.getProcessedTarget().getRelativePath();

			String logical = GridBeanUtils.createParentWFFileString();
			logical += nameInJobDir;
			logical = ParameterProcessingUtils.replaceVariables(logical,
					processorParams);
			String uploadFilesID = (String) processorParams
					.get(C9MWFEditorConstants.PARAM_UPLOAD_FILES_ID);
			if (uploadFilesID != null) {
				logical = logical.replace(
						C9MWFEditorConstants.WORKFLOW_ID_VARIABLE,
						uploadFilesID);
			}
			IGridFileAddress address = storageClient.getAbsoluteAddress(
					logical, progress);

			IFileParameterValue transfer = file.clone();
			transfer.setProcessedTarget(address);
			IGridFileSetTransfer transfers = (IGridFileSetTransfer) processorParams
					.get(ProcessingConstants.FILE_TRANSFERS);
			transfers.addFile(transfer);

			file.setProcessedSource(address);

		}
	}

}
