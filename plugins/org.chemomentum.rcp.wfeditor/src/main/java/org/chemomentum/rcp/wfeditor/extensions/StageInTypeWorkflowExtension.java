/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.extensions;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.chemomentum.rcp.wfeditor.celleditors.StageInTypeWorkflowCellEditor;
import org.chemomentum.rcp.wfeditor.utils.GridBeanUtils;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.InputFileParameterValue;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.AbstractStageInTypeExtension;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;
import de.fzj.unicore.rcp.gpe4eclipse.utils.ParameterProcessingUtils;

/**
 * @author demuth
 * 
 */
public class StageInTypeWorkflowExtension extends AbstractStageInTypeExtension {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #attachToParameterValue
	 * (de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.IAdaptable,
	 * com.intel.gpe.gridbeans.IFileParameterValue)
	 */
	public IFileParameterValue attachToParameterValue(IAdaptable activity,
			IFileParameterValue oldValue) {
		IFileParameterValue newValue;
		if (oldValue == null || oldValue.getSource() == null) {
			newValue = new InputFileParameterValue("");
		} else {
			try {
				newValue = oldValue.clone();
			} catch (Exception e) {
				WFEditorActivator
						.log(IStatus.ERROR,
								"Could not clone file address for creating workflow file.",
								e);
				newValue = oldValue;
			}
		}
		newValue.getSource().setProtocol(C9mRCPCommonConstants.WFFILE_PROTOCOL);

		return newValue;
	}

	@Override
	public boolean availableInContext(GridBeanContext context) {
		boolean insideWorkflow = context != null && context.isPartOfWorkflow();
		return super.availableInContext(context) && insideWorkflow;

	}

	@Override
	public IGridFileAddress checkAndFixAddress(IGridFileAddress address,
			String oldVersion, String currentVersion) {


		// fix workflow file addresses prior to client version 6.2.1
		if (oldVersion.compareTo("6.2.1") < 0) {
			String parent = GridBeanUtils.createParentWFFileString();
			String activityIdVar = ParameterProcessingUtils
					.createVariableFromName(GPE4EclipseConstants.ACTIVITY_ID);
			parent = parent.replace(activityIdVar,
					address.getParentDirAddress());
			address.setParentDirAddress(parent);
			if (address.getRelativePath().startsWith("/")) {
				address.setRelativePath(address.getRelativePath().substring(1));
			}
		}

		return address;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #detachFromParameterValue
	 * (de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.IAdaptable,
	 * com.intel.gpe.gridbeans.IFileParameterValue)
	 */
	@Override
	public void detachFromParameterValue(IAdaptable activity,
			IFileParameterValue value) {
		super.detachFromParameterValue(activity, value);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getCellEditor()
	 */
	public CellEditor getCellEditor(Composite parent, IStage stage,
			IAdaptable activity) {
		return new StageInTypeWorkflowCellEditor(parent, stage, activity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getPriority(de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext)
	 */
	public int getPriority(GridBeanContext context) {
		if (context != null
				&& C9mRCPCommonConstants.C9M_WORKFLOW_SYSTEM_TYPE
						.equals(context.getDefaultWorkflowSystem())) {
			return 5;
		} else {
			return 0;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getType()
	 */
	public QName getType() {
		return C9mRCPCommonConstants.WFFILE_QNAME;
	}

}
