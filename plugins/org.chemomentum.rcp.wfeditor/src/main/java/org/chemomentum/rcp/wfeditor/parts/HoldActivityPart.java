package org.chemomentum.rcp.wfeditor.parts;

import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.eclipse.gef.EditPolicy;
import org.eclipse.jface.resource.ImageDescriptor;

import de.fzj.unicore.rcp.wfeditor.parts.SimpleActivityPart;

/**
 * @author mborcz
 * 
 */

public class HoldActivityPart extends SimpleActivityPart {

	protected void createEditPolicies() {
		super.createEditPolicies();
		removeEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE);
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		return WFEditorActivator.getImageDescriptor("hold.png");
	}

}