package org.chemomentum.rcp.wfeditor.monitoring;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.chemomentum.common.impl.workflow.WorkflowManagementClient;
import org.chemomentum.rcp.servicebrowser.nodes.WorkflowNode;
import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityStatusType;
import org.chemomentum.simpleworkflow.xmlbeans.StatusDetailsDocument;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityStatusDocument.ActivityStatus;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityStatusEntryDocument.ActivityStatusEntry;
import org.chemomentum.simpleworkflow.xmlbeans.StatusDetailsDocument.StatusDetails;
import org.chemomentum.simpleworkflow.xmlbeans.SubWorkflowStatusDocument.SubWorkflowStatus;
import org.chemomentum.simpleworkflow.xmlbeans.SubWorkflowStatusEntryDocument.SubWorkflowStatusEntry;
import org.chemomentum.workflow.xmlbeans.StatusType;
import org.chemomentum.workflow.xmlbeans.DetailedStatusDocument.DetailedStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.ui.PlatformUI;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.servicebrowser.nodes.JobNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.StartActivity;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionState;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateDescriptor;

public class StatusUpdater implements Runnable {
	private Map<String, IActivity> monitoredActivities;
	private Map<String, ExecutionStateDescriptor> activityStateDescriptors = new HashMap<String, ExecutionStateDescriptor>();
	private Map<String, ExecutionStateDescriptor> subflowStateDescriptors = new HashMap<String, ExecutionStateDescriptor>();
	private NodePath monitoringService;
	private WorkflowDiagram model;
	private boolean stopped = false;
	private C9MMonitorer monitorer;

	StatusUpdater(C9MMonitorer monitorer, WorkflowDiagram model,
			Map<String, IActivity> monitoredActivities,
			NodePath monitoringService) {
		this.monitorer = monitorer;
		this.model = model;
		this.monitoredActivities = monitoredActivities;
		this.monitoringService = monitoringService;

	}

	private void processActivityStatus(ActivityStatus status) {
		IActivity activity = monitoredActivities.get(status.getId());
		// don't use the start activity's server side status;
		// instead the overall workflow state is used
		if (activity != null && !(activity instanceof StartActivity)) 
		{
			ExecutionStateDescriptor stateDescriptor = activityStateDescriptors
			.get(activity.getName());
			if (stateDescriptor == null) {
				stateDescriptor = activity.getExecutionStateDescriptor()
				.clone();
				activityStateDescriptors.put(activity.getName(),
						stateDescriptor);
			}
			ActivityStatusEntry[] entries = status
			.getActivityStatusEntryArray();

			for (int i = 0; i < entries.length; i++) {
				ActivityStatusEntry entry = entries[i];
				int cipher = 0;
				String description = "";
				if (ActivityStatusType.RUNNING.equals(entry.getStatus())) {
					cipher = ExecutionStateConstants.STATE_RUNNING;
					description = "running";
				} else if (ActivityStatusType.SUCCESSFUL.equals(entry
						.getStatus())) {
					cipher = ExecutionStateConstants.STATE_SUCCESSFUL;
					description = "successful";
				} else if (ActivityStatusType.FAILED.equals(entry
						.getStatus())) {
					if(entry.getErrorMessage() != null && entry.getErrorMessage().contains("PARENT_FAILED "))
					{
						// deal with old workflow engine that marks these
						// as failed which makes it hard to identify the
						// actual problem
						cipher = ExecutionStateConstants.STATE_ABORTED;
					}
					else cipher = ExecutionStateConstants.STATE_FAILED;
					description = entry.getErrorMessage();
				} else if (ActivityStatusType.NOT_STARTED.equals(entry
						.getStatus())) {
					cipher = ExecutionStateConstants.STATE_NOT_STARTED;
					description = "not started";
				}
				ExecutionState state = new ExecutionState(cipher);
				state.setDetailedDescription(description);
				String iteration = entry.getIteration();
				if (iteration == null) {
					iteration = ExecutionStateConstants.DEFAULT_ITERATION_ID;
				}
				state.setIterationId(iteration);
				stateDescriptor.addState(state);
			}
		}
	}

	private void processSubWorkflowStatus(SubWorkflowStatus status) {
		IActivity activity = monitoredActivities.get(status.getId());
		if (activity != null) {
			ExecutionStateDescriptor stateDescriptor = subflowStateDescriptors
			.get(activity.getName());
			if (stateDescriptor == null) {
				stateDescriptor = activity.getExecutionStateDescriptor()
				.clone();
				subflowStateDescriptors.put(activity.getName(),
						stateDescriptor);
			}
			SubWorkflowStatusEntry[] entries = status
			.getSubWorkflowStatusEntryArray();

			for (int i = 0; i < entries.length; i++) {
				SubWorkflowStatusEntry entry = entries[i];
				int cipher = 0;
				String description = "";
				if (ActivityStatusType.RUNNING.equals(entry.getStatus())) {
					cipher = ExecutionStateConstants.STATE_RUNNING;
					description = "running";
				} else if (ActivityStatusType.SUCCESSFUL.equals(entry
						.getStatus())) {
					cipher = ExecutionStateConstants.STATE_SUCCESSFUL;
					description = "successful";
				} else if (ActivityStatusType.FAILED.equals(entry
						.getStatus())) {
					if(entry.getErrorMessage() != null && entry.getErrorMessage().contains("PARENT_FAILED "))
					{
						// deal with old workflow engine that marks these
						// as failed which makes it hard to identify the
						// actual problem
						cipher = ExecutionStateConstants.STATE_ABORTED;
					}
					else cipher = ExecutionStateConstants.STATE_FAILED;
					description = entry.getErrorMessage();
				}
				ExecutionState state = new ExecutionState(cipher);
				state.setDetailedDescription(description);
				String iteration = entry.getIteration();
				if (iteration == null) {
					iteration = ExecutionStateConstants.DEFAULT_ITERATION_ID;
				}
				state.setIterationId(iteration);
				stateDescriptor.addState(state);

				// now deal with activities in this subworkflow
				ActivityStatus[] childActivities = entry
				.getActivityStatusArray();
				for (ActivityStatus activityStatus : childActivities) {
					processActivityStatus(activityStatus);
				}

				// now deal with subworklows within this subworkflow
				SubWorkflowStatus[] childWorkflows = entry
				.getSubWorkflowStatusArray();
				for (int j = 0; j < childWorkflows.length; j++) {
					processSubWorkflowStatus(childWorkflows[j]);
				}

			}
		}

	}

	private void refreshNodes(EndpointReferenceType wfClientEpr) {
		Node n = NodeFactory.createNode(wfClientEpr);
		n.refresh(1, false, null);
		for (Node child : n.getChildrenArray()) {
			if (child instanceof JobNode) {
				JobNode job = (JobNode) child;
				org.unigrids.services.atomic.types.StatusType.Enum jobStatus = job
				.getJobStatus();
				if (!org.unigrids.services.atomic.types.StatusType.SUCCESSFUL
						.equals(jobStatus)
						&& !org.unigrids.services.atomic.types.StatusType.FAILED
						.equals(jobStatus)) {
					job.refresh(0, false, null);
				}
			}
		}
		n.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		try {
			boolean workflowFailed = false;
			boolean workflowSuccessful = false;
			boolean workflowAborted = false;
			if (stopped) {
				removeMyself();
				return;
			}

			WorkflowNode node = null;
			if (monitoringService.getLength() > 1) {
				node = (WorkflowNode) NodeFactory.revealNode(
						monitoringService, null);
			} else if (monitoringService.getLength() == 1) {
				node = (WorkflowNode) NodeFactory
				.createNode(monitoringService.last()); // backwards
				// compatibility
				// with
				// versions
				// < 6.3.0
			}
			if (node == null) {
				WFEditorActivator
				.log(IStatus.ERROR,
						"Unable to retrieve status from running workflow. The server does not know this workflow anymore.");
				return;
			}
			WorkflowManagementClient wfclient = null;
			try {
				wfclient = node.createWorkflowManagementClient();
			} finally {
				if (monitoringService.getLength() == 1) {
					node.dispose();
				}
			}

			StatusType.Enum status = wfclient.getStatus();
			int modelState = ExecutionStateConstants.STATE_NOT_STARTED;
			String description = "";
			if (StatusType.UNDEFINED.equals(status)) {
				modelState = ExecutionStateConstants.STATE_FAILED;
				description = "unknown state";
				workflowFailed = true;
			} else if (StatusType.RUNNING.equals(status)) {
				modelState = ExecutionStateConstants.STATE_RUNNING;
				description = "workflow running";
			} else if (StatusType.USERINPUT_NEEDED.equals(status)) {
					modelState = ExecutionStateConstants.STATE_WAITING_FOR_USER_INPUT;
					description = "workflow held - manual continue required";
			} else if (StatusType.SUCCESSFUL.equals(status)) {
				modelState = ExecutionStateConstants.STATE_SUCCESSFUL;
				description = "workflow finished successfully";
				workflowSuccessful = true;
			} else if (StatusType.FAILED.equals(status)) {
				modelState = ExecutionStateConstants.STATE_FAILED;
				description = "workflow failed";
				// only stop monitoring when no activities are running
				// anymore
				// this is checked later
				workflowFailed = true;
			} else if (StatusType.ABORTED.equals(status)) {
				modelState = ExecutionStateConstants.STATE_ABORTED;
				description = "workflow was aborted by a user";
				// only stop monitoring when no activities are running
				// anymore
				// this is checked later
				workflowAborted = true;
			}

			StatusDetailsDocument doc = null;
			try {
				DetailedStatus ds = wfclient.getDetailedStatus();
				if (ds != null) {
					doc = StatusDetailsDocument.Factory.parse(ds
							.newInputStream());
				} else {
					stopped = true;
					removeMyself();
					WFEditorActivator
					.log(IStatus.ERROR,
							"Unable to retrieve status from running workflow.");
					return;
				}
			} catch (Exception e) {
				stopped = true;
				removeMyself();
				WFEditorActivator.log(IStatus.ERROR,
						"Unable to retrieve status from running workflow: "
						+ e.getMessage(), e);
				return;
			}
			final StatusDetails statusDetails = doc.getStatusDetails();

			// deal with activities in this workflow
			ActivityStatus[] childActivities = statusDetails
			.getActivityStatusArray();

			for (ActivityStatus activityStatus : childActivities) {
				processActivityStatus(activityStatus);
			}

			// now deal with subworkflows within this workflow
			SubWorkflowStatus[] childWorkflows = statusDetails
			.getSubWorkflowStatusArray();
			for (int j = 0; j < childWorkflows.length; j++) {
				processSubWorkflowStatus(childWorkflows[j]);
			}

			boolean hasRunningActivities = false;
			Iterator<String> it = activityStateDescriptors.keySet()
			.iterator();
			while (it.hasNext()) {
				String id = it.next();
				ExecutionStateDescriptor descriptor = activityStateDescriptors
				.get(id);
				IActivity activity = monitoredActivities.get(id);
				activity.setExecutionStateDescriptor(descriptor);
				if(!hasRunningActivities)
				{
					try {
						for(ExecutionState state : descriptor.getStates().values())
						{
							if(state.getCipher() == ExecutionStateConstants.STATE_RUNNING)
							{
								hasRunningActivities = true;
								break;
							}
						}

					} catch (Exception e) {
						// do nothing
					}
				}
			}
			refreshNodes(wfclient.getEPR());
			final int state = modelState;
			final String descr = description;
			if (!stopped) {
				PlatformUI.getWorkbench().getDisplay()
				.syncExec(new Runnable() {
					public void run() {
						updateModel(state, descr, statusDetails);

					}
				});
			}

			// only stop monitoring when no activities are running anymore
			if (model.isDisposed()
					|| ((workflowFailed || workflowSuccessful || workflowAborted) && !hasRunningActivities)) {
				stopped = true;
			}
			activityStateDescriptors.clear();
			subflowStateDescriptors.clear();
			if (stopped) {
				removeMyself();
			} else {
				reschedule();
			}
		} catch (Exception e) {
			stopped = true;
			removeMyself();
			WFEditorActivator.log(IStatus.ERROR,
					"Workflow monitoring failed: " + e.getMessage(), e);
		}

	}

	private void setActivityIterationId(ActivityStatus status,
			String parentIteration) {
		// since only loop bodies have multiple entries with different
		// iterations
		// this should be save
		String id = status.getId();
		if (status.getActivityStatusEntryArray() == null
				|| status.getActivityStatusEntryArray().length == 0) {
			return;
		}
		String iteration = status.getActivityStatusEntryArray(0)
		.getIteration();
		if (!parentIteration.equals(iteration)
				&& monitoredActivities.get(id) != null) {
			monitoredActivities.get(id).setCurrentIterationId(iteration);
		}
	}

	private void setSubWorkflowIterationIds(SubWorkflowStatus status,
			String parentIteration) {
		String id = status.getId();
		SubWorkflowStatusEntry[] entries = status
		.getSubWorkflowStatusEntryArray();
		if (entries != null && entries.length > 0) {
			// only evaluate the last entry (multiple iterations => multiple
			// entries)
			SubWorkflowStatusEntry entry = entries[entries.length - 1];
			String iteration = entry.getIteration();
			if (iteration == null) {
				iteration = parentIteration;
			}
			if (iteration.length() > parentIteration.length()) {
				if (monitoredActivities.get(id) != null) {
					IActivity act = monitoredActivities.get(id);
					act.setCurrentIterationId(iteration);
				}
			}
			if (entry.getSubWorkflowStatusArray() != null) {
				for (SubWorkflowStatus childSub : entry
						.getSubWorkflowStatusArray()) {
					setSubWorkflowIterationIds(childSub, iteration);
				}
			}
			if (entry.getActivityStatusArray() != null) {
				for (ActivityStatus child : entry.getActivityStatusArray()) {
					setActivityIterationId(child, iteration);
				}
			}
		}

	}

	private void setWorkflowIterationIds(StatusDetails status) {
		String iteration = ExecutionStateConstants.DEFAULT_ITERATION_ID;

		if (status.getSubWorkflowStatusArray() != null) {
			for (SubWorkflowStatus childSub : status
					.getSubWorkflowStatusArray()) {
				setSubWorkflowIterationIds(childSub, iteration);
			}
		}
		if (status.getActivityStatusArray() != null) {
			for (ActivityStatus child : status.getActivityStatusArray()) {
				setActivityIterationId(child, iteration);
			}
		}
	}

	public void stopMonitoring() throws Exception {
		stopped = true;
	}

	private void updateModel(int state, String description,
			StatusDetails statusDetails) {
		model.changeCurrentExecutionState(state, description);
		setWorkflowIterationIds(statusDetails);

		Iterator<String> it = subflowStateDescriptors.keySet().iterator();
		while (it.hasNext()) {
			String id = it.next();
			ExecutionStateDescriptor descriptor = subflowStateDescriptors
			.get(id);
			IActivity activity = monitoredActivities.get(id);
			activity.setExecutionStateDescriptor(descriptor);

		}

		model.saveExecutionData();
	}
	
	String getKey(NodePath address) {
		return monitorer.getKey(address);
	}
	
	void reschedule() {
		monitorer.schedule(this);
	}
	
	private void removeMyself()
	{
		monitorer.removeUpdater(getKey(monitoringService));
	}

}
