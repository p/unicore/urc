package org.chemomentum.rcp.wfeditor.jobprocessors;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.chemomentum.common.util.C9MCommonConstants;
import org.chemomentum.rcp.wfeditor.C9MWFEditorConstants;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.IStepProcessor;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;

public class AddIterationNumberToJobNameProcessor implements IStepProcessor {

	public AddIterationNumberToJobNameProcessor() {

	}

	public List<Integer> getProcessingSteps() {
		return Arrays
				.asList(ProcessingConstants.WRITE_PROCESSED_PARAMS_TO_JSDL);
	}

	public void process(Client client, GPEJob job, int processStep,
			List<IGridBeanParameter> params,
			List<IGridBeanParameterValue> values,
			Map<String, Object> processorParams, IProgressListener progress)
			throws Exception {
		Object o = processorParams
				.get(C9MWFEditorConstants.PARAM_ADD_ITERATION_NUMBER_TO_JOBNAME);
		if (o != null && (Boolean) o) {
			String newName = job.getName() + "_iteration_"
					+ C9MCommonConstants.CURRENT_TOTAL_ITERATOR;
			job.setName(newName);
		}

	}

}
