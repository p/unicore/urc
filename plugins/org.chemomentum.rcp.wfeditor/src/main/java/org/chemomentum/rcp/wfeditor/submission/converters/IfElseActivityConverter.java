/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.submission.converters;

import java.util.Collections;
import java.util.Map;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.C9MWFEditorConstants;
import org.chemomentum.simpleworkflow.api.IElement;
import org.chemomentum.simpleworkflow.api.SubWorkflowElement;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityDocument.Activity;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityType;
import org.chemomentum.simpleworkflow.xmlbeans.OptionDocument.Option;
import org.chemomentum.simpleworkflow.xmlbeans.TransitionDocument.Transition;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.ContainerActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.IfElseActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.ConditionalTransition;
import de.fzj.unicore.rcp.wfeditor.submission.IConverter;
import de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.TopDownDAGTraverser;

/**
 * @author demuth
 * 
 */
public class IfElseActivityConverter implements IConverter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.IConverter#canConvert(javax.xml
	 * .namespace.QName, javax.xml.namespace.QName)
	 */
	public boolean canConvert(IAdaptable submissionService, QName modelType,
			QName wfLanguageType) {
		return IfElseActivity.TYPE.equals(modelType)
				&& C9mRCPCommonConstants.C9M_DEFAULT_WF_DIALECT
						.equals(wfLanguageType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.IConverter#convert(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram)
	 */
	public IElement convert(IAdaptable submissionService, String workflowId,
			WorkflowDiagram model, IFlowElement element,
			Map<String, Object> additionalParams, IProgressMonitor monitor)
			throws Exception {

		IfElseActivity ifElseActivity = (IfElseActivity) element;

		int scale = 1000;
		monitor.beginTask("", 100 * scale);
		try {
			SubWorkflowElement graph = (SubWorkflowElement) additionalParams
					.get(C9MWFEditorConstants.PARAM_SUBGRAPH);

			SubWorkflowElement sub = new SubWorkflowElement(
					ifElseActivity.getName(), graph);
			IActivity splitActivity = ifElseActivity.getSplitActivity();
			Activity split = Activity.Factory.newInstance();
			split.setId(splitActivity.getName());
			split.setType(ActivityType.BRANCH);
			split.setName(ActivityType.BRANCH.toString());
			sub.addActivity(split);

			Activity join = Activity.Factory.newInstance();
			join.setType(ActivityType.MERGE);
			join.setId(ifElseActivity.getName() + "-JOIN");
			sub.addActivity(join);

			StructuredActivity ifBody = ifElseActivity.getIfBody();
			if (ifBody.getChildren().size() > 0) {
				SubWorkflowElement ifSub = convertBody(sub, ifBody,
						submissionService, workflowId, model, element,
						additionalParams, monitor);
				sub.addTransition(split.getId(), ifSub.getId());
				Transition conditional = sub.getTransition(split.getId(),
						ifSub.getId());
				ConditionalTransition conditionalTransition = (ConditionalTransition) splitActivity
						.getOutgoingTransitions().get(0);
				conditional.addNewCondition().setExpression(
						conditionalTransition.getCondition()
								.getInternalExpression());
				sub.addTransition(ifSub.getId(), join.getId());
			} else {
				throw new Exception(
						"Invalid workflow: Found If-Statement with empty if body. Cancelling.");
			}

			StructuredActivity elseBody = ifElseActivity.getElseBody();
			if (elseBody.getChildren().size() > 0) {
				SubWorkflowElement elseSub = convertBody(sub, elseBody,
						submissionService, workflowId, model, element,
						additionalParams, monitor);
				sub.addTransition(split.getId(), elseSub.getId());
				sub.addTransition(elseSub.getId(), join.getId());
			} else {
				// need this in order to keep the workflow running when
				// condition is not met
				sub.addTransition(split.getId(), join.getId());
			}
			additionalParams.put(C9MWFEditorConstants.PARAM_SUBGRAPH, graph);

			if (monitor.isCanceled()) {
				return null;
			}
			return sub;

		} finally {
			monitor.done();
		}
	}

	protected SubWorkflowElement convertBody(SubWorkflowElement parent,
			StructuredActivity body, IAdaptable submissionService,
			String workflowId, WorkflowDiagram model, IFlowElement element,
			Map<String, Object> additionalParams, IProgressMonitor monitor)
			throws Exception {
		SubWorkflowElement sub = new SubWorkflowElement(body.getName(), parent);

		// Co-brokering
		Boolean coBroBoolean = (Boolean) body
				.getPropertyValue(ContainerActivity.COBROKER);
		if (coBroBoolean != null && coBroBoolean.booleanValue()) {
			Option cobroOption = Option.Factory.newInstance();
			cobroOption.setName(ContainerActivity.COBROKER);
			cobroOption.setStringValue("true");
			sub.setOptions(Collections.singletonList(cobroOption));
		}

		additionalParams.put(C9MWFEditorConstants.PARAM_SUBGRAPH, sub);
		IGraphTraverser traverser = new TopDownDAGTraverser();
		DefaultStructuredActivityVisitor visitor = new DefaultStructuredActivityVisitor(
				submissionService, workflowId, model, monitor, getTargetType(),
				additionalParams);
		traverser.traverseGraph(body, null, visitor);

		return sub;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.submission.IConverter#getModelType()
	 */
	public QName getModelType() {
		return IfElseActivity.TYPE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.submission.IConverter#getTargetType()
	 */
	public QName getTargetType() {
		return C9mRCPCommonConstants.C9M_DEFAULT_WF_DIALECT;
	}

}
