/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.wfeditor.ui;

import org.chemomentum.rcp.wfeditor.submission.C9MSubmitter;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.common.interfaces.IFinishableWizardPage;
import de.fzj.unicore.rcp.servicebrowser.filters.ConfigurableFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.RegistryFilterCriteria;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceSelectionPanel;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.ui.WFSubmissionWizard;

/**
 * FlowWizardPage1
 * 
 * @author Daniel Lee
 */
public class StorageSelectionWizardPage extends WizardPage implements
		ISelectionChangedListener, IFinishableWizardPage {

	private static final String REGISTRY_FILTER_ID = "storage selection registry filter";
	private Node selectedNode = null;
	private WorkflowDiagram workflowDiagram;
	private boolean disposed = false;
	ServiceSelectionPanel serviceSelectionPanel;

	C9MSubmitter submitter;

	public StorageSelectionWizardPage(C9MSubmitter submitter) {
		super("");
		setTitle("Please select a storage or storage factory for input and output files.");
		this.submitter = submitter;
	}

	/**
	 * (non-Javadoc) Method declared on IDialogPage.
	 */
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);

		initializeDialogUnits(parent);

		// PlatformUI.getWorkbench().getHelpSystem().setHelp(composite,
		// IIDEHelpContextIds.NEW_PROJECT_WIZARD_PAGE);

		composite.setLayout(new GridLayout());
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));

		int style = SWT.BORDER | SWT.SINGLE;

		serviceSelectionPanel = new ServiceSelectionPanel(composite, null,
				null, this, 5, style);

		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 400;
		serviceSelectionPanel.getControl().setLayoutData(gd);

		serviceSelectionPanel.getTreeViewer().setExpandedLevels(2);

		setErrorMessage(null);

		setControl(composite);
		Dialog.applyDialogFont(composite);

	}

	@Override
	public void dispose() {
		if (disposed) {
			return; // done that before
		}
		super.dispose();
		disposed = true;
		if (serviceSelectionPanel != null) {
			serviceSelectionPanel.dispose();
		}
	}

	public boolean finish() {
		boolean nodeSelected = selectedNode != null;
		if (nodeSelected && submitter != null) {
			submitter.setWorkDirEpr(selectedNode.getEpr());
		}
		return true; // always return true, since selecting a storage is
						// optional
	}

	public Node getSelectedNode() {
		return selectedNode;
	}

	@Override
	public WFSubmissionWizard getWizard() {
		return (WFSubmissionWizard) super.getWizard();
	}

	public WorkflowDiagram getWorkflowDiagram() {
		return workflowDiagram;
	}

	public boolean isDisposed() {
		return disposed;
	}

	public void selectionChanged(SelectionChangedEvent event) {
		selectedNode = null;
		IStructuredSelection selection = (IStructuredSelection) event
				.getSelection();
		if (selection.size() == 0) {
			setErrorMessage("No suitable storage or storage factory service selected.");
		} else {
			Node selected = (Node) selection.getFirstElement();
			if (selected instanceof StorageNode
					|| selected instanceof StorageFactoryNode) {
				Node original = NodeFactory
						.getNodeFor(selected.getPathToRoot());
				selectedNode = original;
				setErrorMessage(null);
			} else {
				setErrorMessage("No suitable storage or storage factory service selected.");
			}
		}

	}

	@Override
	public void setErrorMessage(String newMessage) {
		super.setErrorMessage(newMessage);
	}

	public void setSelectedNode(Node selectedNode) {
		this.selectedNode = selectedNode;
	}

	public void setSubmitter(C9MSubmitter submitter) {
		this.submitter = submitter;
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (visible) {

			ConfigurableFilter filter = (ConfigurableFilter) serviceSelectionPanel
					.getFilterController().getFilter(REGISTRY_FILTER_ID);
			if (filter == null) {
				filter = new ConfigurableFilter(REGISTRY_FILTER_ID);
				serviceSelectionPanel.addFilter(filter);
			}
			Node n = getWizard().getSelectedNode();
			RegistryNode regNode = n.getRegistryNode();
			if (regNode != null) {
				RegistryFilterCriteria regFilter = new RegistryFilterCriteria(regNode.getName());
				regFilter.setAlwaysShowRegistries(false);
				filter.setGridFilter(regFilter);
				regNode.setExpanded(true);
			} else {
				filter.setGridFilter(null);
			}
			serviceSelectionPanel.getTreeViewer().refresh();

		}
	}

	public void setWorkflowDiagram(WorkflowDiagram workflowDiagram) {
		this.workflowDiagram = workflowDiagram;
	}

}
