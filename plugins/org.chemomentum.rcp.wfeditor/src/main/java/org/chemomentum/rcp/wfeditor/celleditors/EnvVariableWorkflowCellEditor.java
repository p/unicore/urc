/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.celleditors;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.wfeditor.model.EnvVariableLinker;
import org.chemomentum.rcp.wfeditor.utils.GridBeanUtils;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.clients.api.transfers.IAddressOrValue;
import com.intel.gpe.clients.common.transfers.Value;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

import de.fzj.unicore.rcp.common.guicomponents.StringComboBoxCellEditor;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.EnvVariable;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.conditions.CollectVariablesVisitor;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.traversal.EnclosingScopesTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser;

/**
 * CellEditor that allows selection of a single workflow variable
 * 
 * @author demuth
 * 
 */
public class EnvVariableWorkflowCellEditor extends StringComboBoxCellEditor {

	IActivity activity;
	EnvVariable variable;
	private List<WorkflowVariable> workflowVariables;
	boolean valueSet = false;

	public EnvVariableWorkflowCellEditor(Composite parent,
			Map<QName, EnvVariableLinker> linkers, IAdaptable activity) {
		super(parent);
		this.activity = (IActivity) activity.getAdapter(IActivity.class);
	}

	/**
	 * returns an object of RangeValueTypeResourceRequirementValue
	 */
	@Override
	protected EnvVariable doGetValue() {
		if (!valueSet) {
			IGridBeanParameter param = variable.getGridBeanParameter();
			// remove old (persisted) linkers for this value
			List<PropertyChangeListener> listeners = new ArrayList<PropertyChangeListener>(
					activity.getPersistentListeners());
			for (PropertyChangeListener l : listeners) {
				if ((l instanceof EnvVariableLinker)) {
					EnvVariableLinker oldLinker = (EnvVariableLinker) l;
					if (param.getName().equals(oldLinker.getParam().getName())) {
						oldLinker.unlink();
					}
				}
			}
			int selection = getComboBox().getSelectionIndex();
			IAddressOrValue newValue = new Value("none");
			if (selection >= 0 && selection < workflowVariables.size()) {
				String name = workflowVariables.get(selection).getName();
				newValue = GridBeanUtils.createAddressFromVariableName(name);
				EnvVariableLinker linker = new EnvVariableLinker(activity
						.getDiagram().getVariable(name), param, activity);
				linker.connect();

			}
			variable.getValue().setVariableValue(newValue);
			valueSet = true;
		}
		return variable;
	}

	@Override
	protected void doSetValue(Object value) {
		this.variable = (EnvVariable) value;
		initVariableSelection();
	}

	protected void initVariableSelection() {

		WorkflowDiagram diagram = activity.getDiagram();

		IGraphTraverser traverser = new EnclosingScopesTraverser();
		CollectVariablesVisitor visitor = new CollectVariablesVisitor();

		try {
			traverser.traverseGraph(diagram, activity, visitor);
		} catch (Exception e) {
			WFActivator
					.log(IStatus.ERROR,
							"Unexpected exception while searching for workflow variables",
							e);
		}
		workflowVariables = visitor.getFound();
		String[] names = new String[] { "no variables found" };
		int selection = 0;
		if (workflowVariables.size() > 0) {
			names = new String[workflowVariables.size()];
			String oldVariableName = GridBeanUtils.extractVariableName(variable
					.getValue().getVariableValue().getInternalString());
			int i = 0;
			for (WorkflowVariable variable : workflowVariables) {
				if (variable.equals(oldVariableName)) {
					selection = i;
				}
				names[i++] = variable.getName();
			}
			if (oldVariableName == null) {
				IAddressOrValue newValue = GridBeanUtils
						.createAddressFromVariableName(names[0]);
				variable.getValue().setVariableValue(newValue);
				selection = 0;
			}
		} else {
			variable.getValue().setVariableValue(null);
			selection = 0;
		}
		setItems(names);
		getComboBox().select(selection);
	}

}
