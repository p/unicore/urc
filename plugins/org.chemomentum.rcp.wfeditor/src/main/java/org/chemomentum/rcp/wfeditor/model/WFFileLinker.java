/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.chemomentum.rcp.wfeditor.utils.GridBeanUtils;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.gef.commands.Command;
import org.eclipse.swt.widgets.Display;

import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.clients.common.transfers.GridFileAddress;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileSetParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.InputFileParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;
import com.intel.gpe.util.collections.CollectionUtil;
import com.intel.gpe.util.sets.Pair;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSetSink;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSink;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageTypeRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.utils.ParameterProcessingUtils;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;
import de.fzj.unicore.rcp.wfeditor.utils.TransitionUtils;

/**
 * Used to link an input file to an output file of another activity. Whenever
 * the other activity applies changes to its output file, the linked file needs
 * to mirror these changes.
 * 
 * @author demuth
 * 
 */

@XStreamAlias("WFFileLinker")
public class WFFileLinker implements PropertyChangeListener, IDataFlow {

	private class GridBeanUpdater extends BackgroundJob {
		Queue<Pair<WFFileLinker, PropertyChangeEvent>> events = new ConcurrentLinkedQueue<Pair<WFFileLinker, PropertyChangeEvent>>();

		public GridBeanUpdater() {
			super("updating linked workflow file");

			setSystem(true);

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.
		 * IProgressMonitor)
		 */
		@Override
		protected IStatus run(IProgressMonitor monitor) {

			while (!events.isEmpty()) {
				Pair<WFFileLinker, PropertyChangeEvent> p = events.poll();
				WFFileLinker linker = p.getM1();
				PropertyChangeEvent evt = p.getM2();
				linker.doHandleEvent(evt);
			}
			return Status.OK_STATUS;
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 42685942397592552L;
	private WorkflowFile linkTarget;
	private IAdaptable activity;
	private IGridBeanParameter param;

	/**
	 * A per-job unique identifer for the GridBean input file this linker is
	 * linked to. This is important for finding "our" file when dealing with
	 * file sets.
	 */
	private String fileId;

	/**
	 * A unique identifier for ourselves
	 */
	private String id;

	private static GridBeanUpdater updater;
	private transient boolean disposed;
	private transient IFileParameterValue lastFileValue;
	private transient int lastFileValueIndex = -1;
	private transient boolean checkedObsoleteLinks = false;
	private transient boolean relinkedAfterDeserialization = false;
	private transient boolean listeningToModel = true;
	private transient int sourceIndex = -1;
	private transient int sinkIndex = -1;
	private transient Command createTransitionCommand;

	private transient IDataSink sink = null;

	public WFFileLinker(WorkflowFile linkTarget, GPEWorkflowFileSetSink sink,
			String fileId) {
		this.sink = sink;
		this.linkTarget = linkTarget;
		this.activity = sink.getFlowElement();
		this.param = sink.getParameter();
		this.fileId = fileId;
		relinkedAfterDeserialization = true;

	}

	public WFFileLinker(WorkflowFile linkTarget, GPEWorkflowFileSink sink) {
		this.sink = sink;
		this.linkTarget = linkTarget;
		this.activity = sink.getFlowElement();
		this.param = sink.getParameter();
		this.fileId = sink.getFileId();
		relinkedAfterDeserialization = true;

	}

	public WFFileLinker(WorkflowFile linkTarget, IGridBeanParameter param,
			String fileId, IAdaptable activity) {
		this.linkTarget = linkTarget;
		this.activity = sink.getFlowElement();
		this.param = param;
		this.fileId = fileId;
		relinkedAfterDeserialization = true;

	}

	private boolean activityNameChanged(PropertyChangeEvent evt) {
		String s = evt.getPropertyName();
		return IFlowElement.PROP_NAME.equals(s);
	}

	protected void addToSourceAndSink(int sourceIndex, int sinkIndex) {
		if (!getDataSource().hasOutgoingFlow(this)) {
			getDataSource().addOutgoingFlow(this, sourceIndex);
		}
		if (!getDataSink().hasIncomingFlow(this)) {
			getDataSink().addIncomingFlow(this, sinkIndex);
		}
	}

	public void afterDeserialization() {
		
	}

	public void afterSerialization() {
		
	}

	public void beforeSerialization() {
	
	}

	private void checkObsoleteLinks() {
		// for backwards compatibility remove ourselves as persistent listeners
		// of the link target and its parent activity
		if (linkTarget != null) {
			if (linkTarget.isAlreadyListeningPersistently(this)) {
				linkTarget.removePersistentPropertyChangeListener(this);
			}
			IFlowElement element = linkTarget.getFlowElement();
			if (element != null && element.isAlreadyListeningPersistently(this)) {
				element.removePersistentPropertyChangeListener(this);
			}
		}
		checkedObsoleteLinks = true;

	}

	@Override
	public WFFileLinker clone() throws CloneNotSupportedException {
		WFFileLinker result = (WFFileLinker) super.clone();

		result.lastFileValue = null;
		result.lastFileValueIndex = -1;
		result.checkedObsoleteLinks = false;
		result.listeningToModel = true;
		result.sourceIndex = -1;
		result.sinkIndex = -1;
		result.createTransitionCommand = null;
		return result;
	}

	public void connect() {
		relink();
	}

	protected IGridFileAddress createNewAddress() {
		return new C9MWFFileAddress();
	}

	public void disconnect() {
		if (sink instanceof GPEWorkflowFileSetSink) {
			// in this case, we remove the created file parameter value from the
			// file set
			// this value needs to be remembered in order to be able to restore
			// it when reconnect gets called
			IFileSetParameterValue fileSet = (IFileSetParameterValue) getGridBean()
			.get(param.getName());
			lastFileValue = GridBeanUtils.findFileInFileSet(fileId, fileSet);
			try {
				fileSet = (IFileSetParameterValue) fileSet.clone();
			} catch (CloneNotSupportedException e) {
				// do nothing
			}
			lastFileValueIndex = fileSet.getFiles().indexOf(lastFileValue);
			fileSet.removeFile(lastFileValue);
			setValueInGridBean(param.getName(), fileSet);
		} else {
			IFileParameterValue file = (IFileParameterValue) getGridBean().get(
					param.getName());
			lastFileValue = file;
			lastFileValueIndex = -1;
			if (file != null) {
				GridBeanContext context = (GridBeanContext) getGridBean().get(
						GPE4EclipseConstants.CONTEXT);
				StageTypeRegistry registry = GPEActivator.getDefault()
				.getStageInTypeRegistry();
				IStageTypeExtensionPoint myType = registry
				.getDefiningExtension(file.getSource().getProtocol()
						.getName());
				myType.detachFromParameterValue(getActivity(), file);
				IStageTypeExtensionPoint defaultType = registry
				.getDefaultStageType(context);
				file = defaultType.attachToParameterValue(getActivity(), file);
				setValueInGridBean(param.getName(), file);
			}
		}
		if (createTransitionCommand != null) {
			createTransitionCommand.undo();
		}
		unlink();
	}

	private void doHandleEvent(PropertyChangeEvent evt) {
		IFileParameterValue valueFromGridBean = getValueFromGridBean();
		IGridFileAddress oldValue = null;
		if (valueFromGridBean != null) {
			oldValue = param.isInputParameter() ? valueFromGridBean.getSource()
					: valueFromGridBean.getTarget();
		}
		if (parentChanged(evt) || activityNameChanged(evt)
				|| workflowFileChanged(evt)) {
			if (oldValue == null) {
				return;
			}
			IGridFileAddress newValue = getCurrentValue(oldValue);
			updateGridBeanModel(newValue);
		} else if (lastFileValue == null && (paramProtocolChanged(evt))) {
			unlink();
		}

	}

	private IActivity getActivity() {
		try {
			IActivity act = (IActivity) activity.getAdapter(IActivity.class);
			return act;
		} catch (Exception e) {
			return null;
		}
	}

	protected String getActivityNameWithSeperator(String activityName) {
		return activityName + WFConstants.DISPLAYED_STRING_SEPERATOR;
	}

	public ICopyable getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		try {

			WFFileLinker copy = null;
			WorkflowFile source = linkTarget;
			IFlowElement sourceParent = linkTarget.getFlowElement();
			IDataSink sink = getDataSink();
			IActivity sinkParent = getActivity();
			boolean isCut = (copyFlags & ICopyable.FLAG_CUT) != 0;
			boolean copySourceParent = CloneUtils.isBeingCopied(sourceParent,
					toBeCopied);
			boolean copySinkParent = CloneUtils.isBeingCopied(sinkParent,
					toBeCopied);
			if (!isCut) {
				// this is not a cut => if the target activity is not copied,
				// and we don't connect to a file set,
				// we shouldn't be copied either!
				if (!GridBeanParameterType.FILE_SET.equals(param.getType())
						&& !copySinkParent) {
					return null;
				}
			}

			if (copySourceParent) {
				sourceParent = CloneUtils.getFromMapOrCopy(sourceParent,
						toBeCopied, mapOfCopies, copyFlags);
				source = CloneUtils.getFromMapOrCopy(source, toBeCopied,
						mapOfCopies, copyFlags);
			}
			if (copySinkParent) {
				sinkParent = CloneUtils.getFromMapOrCopy(sinkParent,
						toBeCopied, mapOfCopies, copyFlags);
				sink = CloneUtils.getFromMapOrCopy(sink, toBeCopied,
						mapOfCopies, copyFlags);
			}

			if (mapOfCopies.get(this) == null) {
				if (sink instanceof GPEWorkflowFileSink) {
					GPEWorkflowFileSink fileSink = (GPEWorkflowFileSink) sink;
					fileSink.getFlowElement().getAdapter(IGridBean.class);
					// make sure the activity is ready and its GridBean is
					// available
					copy = clone();
					copy.sink = fileSink;
					copy.linkTarget = source;
					copy.activity = fileSink.getFlowElement();
					copy.param = fileSink.getParameter();
					copy.fileId = fileSink.getFileId();
				} else if (sink instanceof GPEWorkflowFileSetSink) {
					GPEWorkflowFileSetSink fileSetSink = (GPEWorkflowFileSetSink) sink;
					fileSetSink.getFlowElement().getAdapter(IGridBean.class);
					// make sure the activity is ready and its GridBean is
					// available
					String id = getFileId();
					if (isCut) {
						copy = clone();
						copy.sink = fileSetSink;
						copy.linkTarget = source;
						copy.activity = fileSetSink.getFlowElement();
						copy.param = fileSetSink.getParameter();
						copy.fileId = id;

					} else {
						// we have been copied => add a new file to file set
						Pair<IFileSetParameterValue, WFFileLinker> pair = GridBeanUtils
						.linkToWorkflowFileSet(source, fileSetSink,
								null, null);
						copy = pair.getM2();
						if (copy != null) {
							copy.setValueInGridBean(param.getName(),
									pair.getM1());
						}
					}
				}
				// link to object subgraph that is being copied
				// do NOT link to the diagram itself (the whole object graph)
				if (copySourceParent) {
					if (!source.hasOutgoingFlow(copy)) {
						source.addOutgoingFlow(copy);
					}
				}
				if (copySinkParent) {
					if (!sink.hasIncomingFlow(copy)) {
						sink.addIncomingFlow(copy);
					}
				}
			} else {
				return (ICopyable) mapOfCopies.get(this);
			}
			return copy;

		} catch (Exception e) {
			WFEditorActivator.log(IStatus.ERROR,
					"Unable to copy file data flow: " + e.getMessage(), e);
			return null;
		}
	}

	public IGridFileAddress getCurrentValue(IGridFileAddress oldValue) {
		IGridFileAddress newValue;
		if (oldValue != null) {
			newValue = oldValue.clone();
		} else {
			newValue = createNewAddress();
		}

		IActivity act = linkTarget.getActivity();

		if (linkTarget.getExcludes() != null) {
			newValue.setExcludes(linkTarget.getExcludes());
		}
		if (linkTarget.getMimeTypes() != null) {
			newValue.setMimeTypes(linkTarget.getMimeTypes());
		}

		String relative = linkTarget.getRelativePath();
		newValue.setRelativePath(relative);
		String parent = linkTarget.getParentDirAddress();

		// replace activity id variable with target activity name
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put(GPE4EclipseConstants.ACTIVITY_ID, act.getName());

		if (oldValue != null)
		{
			String oldParent = oldValue.getParentDirAddress();
			if(oldParent != null 
					&& oldParent.contains(ProcessingConstants.WORKFLOW_ID)
					&& !oldParent.contains(
							GPE4EclipseConstants.ITERATION_ID)) {
				// iteration ID was explicitly removed by user in order to refer
				// to last file of the loop
				variables.put(GPE4EclipseConstants.ITERATION_ID, "");
			}
		}

		parent = ParameterProcessingUtils.replaceVariables(parent, variables);

		String resolved = GridBeanUtils
		.resolveIteratorInWFFileBeforeSubmission(parent, getActivity());
		boolean usingWildcards = (resolved + relative).contains("*");
		newValue.setUsingWildcards(usingWildcards);


		String newDisplayed = linkTarget.getDisplayedString();
		newValue.setDisplayedString(getActivityNameWithSeperator(act.getName())
				+ newDisplayed);

		newValue.setParentDirAddress(parent);
		newValue.setProtocol(C9mRCPCommonConstants.WFFILE_PROTOCOL);

		return newValue;
	}

	public IDataSink getDataSink() {
		if (sink == null) {
			restoreSink();
		}
		return sink;
	}

	public IDataSource getDataSource() {
		return linkTarget;
	}



	public String getFileId() {
		return fileId;
	}

	//	private int getFilesetIndex() {
	//
	//		if (GridBeanParameterType.FILE_SET.equals(param.getType())) {
	//			IFileSetParameterValue fileset = (IFileSetParameterValue) getGridBean()
	//					.get(param.getName());
	//			return GridBeanUtils.getFileIndexInFileset(fileId, fileset);
	//		}
	//		return -1;
	//	}

	public IGridBean getGridBean() {
		try {
			IGridBean gb = (IGridBean) activity.getAdapter(IGridBean.class);
			return gb;
		} catch (Exception e) {
			return null;
		}
	}

	public String getId() {
		if (id == null) {
			id = UUID.randomUUID().toString();
		}
		return id;
	}

	public QName getType() {
		return IDataFlow.TYPE;
	}

	private IFileParameterValue getValueFromGridBean() {
		IFileParameterValue result = null;

		if (GridBeanParameterType.FILE_SET.equals(param.getType())) {
			IFileSetParameterValue fileset = (IFileSetParameterValue) getGridBean()
			.get(param.getName());
			result = GridBeanUtils.findFileInFileSet(fileId, fileset);
		} else if (GridBeanParameterType.FILE.equals(param.getType())) {
			result = (IFileParameterValue) getGridBean().get(param.getName());
		}
		return result;
	}

	public Set<Class<?>> insertAfter() {
		Set<Class<?>> result = new HashSet<Class<?>>();
		result.add(IActivity.class);
		result.add(Transition.class);
		return result;
	}

	public Set<Class<?>> insertBefore() {
		return new HashSet<Class<?>>(); // we don't care
	}

	public void insertCopyIntoDiagram(ICopyable copy,
			StructuredActivity parent, Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		// only fully connect to the diagram after pasting
		WFFileLinker copiedFlow = (WFFileLinker) copy;
		IFileParameterValue newFile = new InputFileParameterValue();
		newFile.setUniqueId(copiedFlow.fileId);
		IStageTypeExtensionPoint ext = GPEActivator.getDefault()
		.getStageInTypeRegistry()
		.getDefiningExtension(C9mRCPCommonConstants.WFFILE_QNAME);
		newFile = ext.attachToParameterValue(copiedFlow.getActivity(), newFile);

		IGridFileAddress source = copiedFlow.getCurrentValue(newFile
				.getSource());
		newFile.setSource(source);
		GridBeanUtils.fixCurrentFileValue(newFile, copiedFlow.linkTarget, null,
				copiedFlow.linkTarget.getActivity(), copiedFlow.getActivity());
		copiedFlow.lastFileValue = newFile;
		copiedFlow.reconnect();

		copiedFlow.createTransitionCommand = TransitionUtils
		.ensureControlFlowBetween(copiedFlow.linkTarget.getActivity(),
				copiedFlow.getActivity());
		if (copiedFlow.createTransitionCommand != null) {
			copiedFlow.createTransitionCommand.execute();
		}
	}

	public boolean isConnected() {
		return sink != null && sink.hasIncomingFlow(this);

	}

	public boolean isDisposed() {
		return disposed;
	}

	private boolean isRelevant(PropertyChangeEvent evt) {
		return parentChanged(evt) || workflowFileChanged(evt)
		|| activityNameChanged(evt) || paramProtocolChanged(evt)
		|| paramLinkedToDifferentWFFile(evt);
	}

	public boolean isVisible() {
		IDataSource source = getDataSource();
		IDataSink sink = getDataSink();
		if (source == null || sink == null) {
			return false;
		}
		IFlowElement sourceElt = source.getFlowElement();
		IFlowElement sinkElt = sink.getFlowElement();
		if (sourceElt == null || sinkElt == null) {
			return false;
		}

		ShowDataFlowsProperty sourceProp = (ShowDataFlowsProperty) sourceElt
		.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		ShowDataFlowsProperty sinkProp = (ShowDataFlowsProperty) sinkElt
		.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		if (sourceProp == null || sinkProp == null) {
			return false;
		}
		return source.isVisible()
		&& sink.isVisible()
		&& (sourceProp.isShowingOutgoing() || sinkProp
				.isShowingIncoming());
	}

	private boolean paramLinkedToDifferentWFFile(PropertyChangeEvent evt) {
		String s = evt.getPropertyName();
		if (evt.getSource().equals(getActivity())
				&& s.equals(param.getName().toString())) {
			IFileParameterValue valueFromGridBean = getValueFromGridBean();
			if (valueFromGridBean == null) {
				return false;
			}
			IGridFileAddress addressFromGridBean = param.isInputParameter() ? valueFromGridBean
					.getSource() : valueFromGridBean.getTarget();
					IGridFileAddress currentAddress = getCurrentValue(addressFromGridBean);
					return !(CollectionUtil.equalOrBothNull(
							currentAddress.getParentDirAddress(),
							addressFromGridBean.getParentDirAddress()) && CollectionUtil
							.equalOrBothNull(currentAddress.getRelativePath(),
									addressFromGridBean.getRelativePath()));
		}
		return false;
	}

	private boolean paramProtocolChanged(PropertyChangeEvent evt) {
		String s = evt.getPropertyName();
		if (evt.getSource().equals(getActivity())
				&& s.equals(param.getName().toString())) {
			IFileParameterValue valueFromGridBean = getValueFromGridBean();
			if (valueFromGridBean == null) {
				return true;
			}
			IGridFileAddress address = param.isInputParameter() ? valueFromGridBean
					.getSource() : valueFromGridBean.getTarget();
					return !address.getProtocol().equals(
							C9mRCPCommonConstants.WFFILE_PROTOCOL);
		}
		return false;
	}

	private boolean parentChanged(PropertyChangeEvent evt) {
		return IFlowElement.PARENT.equals(evt.getPropertyName())
		&& evt.getNewValue() != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		if (!listeningToModel) {
			return;
		}
		if (!checkedObsoleteLinks) {
			checkObsoleteLinks(); // remove old persistent links that we don't
			// want anymore
		}
		if (!relinkedAfterDeserialization) {
			restoreSink();
			if (sink != null) {
				relink();
				relinkedAfterDeserialization = true;
			}
		}
		if (isRelevant(evt)) {
			if (updater == null) {
				updater = new GridBeanUpdater();
			}
			updater.events.add(new Pair<WFFileLinker, PropertyChangeEvent>(
					this, evt));
			
			if(Display.findDisplay(Thread.currentThread()) == null)
			{
				// we are in a background thread
				// => wait for completion!
				if (updater.getState() != Job.RUNNING) {
					updater.schedule(0);
				}
				while(updater.getState() != Job.NONE)
				{
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						
					}
				}
			}
			else
			{
				if (updater.getState() != Job.RUNNING) {
					updater.schedule(50);
				}
			}
		}

	}

	// needed for XStream deserialization
	private Object readResolve() {
		listeningToModel = true;

		// make sure we add ourselves as transient listener to the link target
		// file and its parent activity
		// re-adding ourselves doesn't hurt since the property source will not
		// allow this, anyway
		relinkToTarget();

		return this;
	}

	public void reconnect() {
		if (linkTarget.isDisposed() || linkTarget.getFlowElement().isDisposed()
				|| sink == null || sink.isDisposed()
				|| getActivity().isDisposed()) {
			return; // try to avoid restoring invalid links
		}
		if (lastFileValue != null) {
			restoreLastFileValue(lastFileValue, lastFileValueIndex);
		}
		relink();

		if (createTransitionCommand != null) {
			createTransitionCommand.redo();
		}
	}

	public void relink() {
		listeningToModel = false;
		// link to target and its activity
		relinkToTarget();

		// no need to worry about linking yourself twice,
		// property sources don't allow this anyway
		getActivity().addPersistentPropertyChangeListener(this);
		addToSourceAndSink(sourceIndex, sinkIndex);
		listeningToModel = true;
	}

	private void relinkToTarget() {
		linkTarget.addPropertyChangeListener(this);
		IActivity act = linkTarget.getActivity();
		if (act != null) {
			act.addPropertyChangeListener(this);
		}
	}

	protected String removeActivityNameWithSeperator(String displayed,
			String activityName) {
		return displayed.replace(activityName
				+ WFConstants.DISPLAYED_STRING_SEPERATOR, "");
	}

	protected void removeFromSourceAndSink() {
		if (getDataSource().hasOutgoingFlow(this)) {
			sourceIndex = getDataSource().removeOutgoingFlow(this);
		}
		if (getDataSink().hasIncomingFlow(this)) {
			sinkIndex = getDataSink().removeIncomingFlow(this);
		}
	}

	protected void restoreLastFileValue(IFileParameterValue lastFileValue,
			int index) {
		if (lastFileValue != null) {
			if (GridBeanParameterType.FILE_SET.equals(param.getType())) {
				IFileSetParameterValue fileset = (IFileSetParameterValue) getGridBean()
				.get(param.getName());
				try {
					fileset = (IFileSetParameterValue) fileset.clone();
				} catch (CloneNotSupportedException e) {
					WFEditorActivator.log(IStatus.WARNING,
							"Unable to clone file set", e);
				}
				int i = GridBeanUtils.getFileIndexInFileset(fileId, fileset);
				if (i >= 0) {
					// replace other file
					index = i;
					IFileParameterValue old = fileset.getFiles().get(index);
					fileset.removeFile(old);
				}
				if (index >= 0 && index < fileset.getFiles().size()) {
					fileset.addFile(index, lastFileValue);
				} else {
					fileset.addFile(lastFileValue);
				}
				setValueInGridBean(param.getName(), fileset);
			} else {
				setValueInGridBean(param.getName(), lastFileValue);
			}
		}

		this.lastFileValue = null;
		lastFileValueIndex = -1;
	}

	protected void restoreSink() {
		for (IDataSink sink : getActivity().getDataSinkList().getDataSinks()) {
			if (param.getName().toString().equals(sink.getId())) {
				this.sink = sink;
				break;
			}
		}
	}

	protected void setValueInGridBean(QName key, Object value) {
		getGridBean().set(key, value);
	}

	public void unlink() {
		unlinkFromTarget();
		getActivity().removePersistentPropertyChangeListener(this);
		if (sink != null) {
			sinkIndex = sink.removeIncomingFlow(this);
		}
	}

	private void unlinkFromTarget() {
		linkTarget.removePropertyChangeListener(this);
		sourceIndex = linkTarget.removeOutgoingFlow(this);
		IActivity act = linkTarget.getActivity();
		act.removePropertyChangeListener(this);

	}

	protected void updateGridBeanModel(IGridFileAddress newValue) {
		if (GridBeanParameterType.FILE_SET.equals(param.getType())) {
			IFileSetParameterValue fileset = (IFileSetParameterValue) getGridBean()
			.get(param.getName());
			try {
				fileset = (IFileSetParameterValue) fileset.clone();
			} catch (CloneNotSupportedException e) {
				WFEditorActivator.log(IStatus.WARNING,
						"Unable to clone file set", e);
			}

			int i = 0;
			for (IFileParameterValue file : new ArrayList<IFileParameterValue>(
					fileset.getFiles())) {
				if (fileId.equals(file.getUniqueId())) {
					if (newValue == null) {
						fileset.removeFile(file);
						// // reset this file in the gridbean!
						// try {
						// file = (IFileParameterValue) file.clone();
						// } catch (Exception e) {
						// WFEditorActivator.log(Status.WARNING,
						// "Unable to clone file address", e);
						// }
						// if(param.isInputParameter())
						// file.setSource(createNewAddress());
						// else file.setTarget(createNewAddress());
						// fileset.addFile(i,file);
					} else {
						fileset.removeFile(file);
						try {
							file = file.clone();
						} catch (Exception e) {
							WFEditorActivator.log(IStatus.WARNING,
									"Unable to clone file address", e);
						}
						if (param.isInputParameter()) {
							file.setSource(newValue);
							if (newValue.isUsingWildcards()) {
								IActivity act = linkTarget.getActivity();
								String targetDisplayed = removeActivityNameWithSeperator(
										newValue.getDisplayedString(),
										act.getName());
								file.getTarget().setDisplayedString(
										targetDisplayed);
								file.getTarget().setRelativePath(
										newValue.getRelativePath());
							}
						} else {
							file.setTarget(newValue);
						}
						// next line is only needed for handling the special
						// case of a for loop over file chunks
						GridBeanUtils.fixCurrentFileValue(file, linkTarget, null,
								linkTarget.getActivity(), getActivity());
						fileset.addFile(i, file);
					}
					break;
				}
				i++;
			}



			setValueInGridBean(param.getName(), fileset);
		} else if (GridBeanParameterType.FILE.equals(param.getType())) {
			IFileParameterValue file = (IFileParameterValue) getGridBean().get(
					param.getName());
			try {
				file = file.clone();
			} catch (Exception e) {
				WFEditorActivator.log(IStatus.WARNING,
						"Unable to clone file address", e);
			}
			if (newValue == null) {

				// reset this file in the gridbean!
				if (param.isInputParameter()) {
					file.setSource(createNewAddress());
				} else {
					file.setTarget(new GridFileAddress());
				}
			} else {

				if (param.isInputParameter()) {
					file.setSource(newValue);
				} else {
					file.setTarget(newValue);
				}
			}
			// next line is only needed for handling the special
			// case of a for loop over file chunks
			GridBeanUtils.fixCurrentFileValue(file, linkTarget, null,
					linkTarget.getActivity(), getActivity());
			setValueInGridBean(param.getName(), file);
		}
	}

	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		// TODO Auto-generated method stub

	}

	private boolean workflowFileChanged(PropertyChangeEvent evt) {
		String s = evt.getPropertyName();
		return WorkflowFile.PROP_PARENT_DIR_ADDRESS.equals(s)
		|| WorkflowFile.PROP_RELATIVE_PATH.equals(s)
		|| WorkflowFile.PROP_DISPLAYED_STRING.equals(s)
		|| WorkflowFile.PROP_EXCLUDES.equals(s)
		|| WorkflowFile.PROP_MIME_TYPES.equals(s)
		|| WorkflowFile.PROP_USING_WILDCARDS.equals(s);
	}

	private Object writeReplace() {
		if (!checkedObsoleteLinks) {
			checkObsoleteLinks();
		}
		return this;
	}

}
