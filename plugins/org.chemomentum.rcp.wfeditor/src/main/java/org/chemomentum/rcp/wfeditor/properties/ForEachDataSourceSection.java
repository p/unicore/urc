/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.properties;

import org.chemomentum.rcp.wfeditor.model.DataSourceActivity;
import org.chemomentum.rcp.wfeditor.model.ForEachDataSourceActivity;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import de.fzj.unicore.rcp.wfeditor.properties.AbstractSection;

/**
 * @author demuth
 * 
 */
public class ForEachDataSourceSection extends AbstractSection {

	protected Composite loopSettingsParent;
	protected CCombo iterationModeCombo;
	protected CCombo chunkSizeCombo;
	protected Text maxNumTasksText;
	protected Text chunkSizeValue;
	protected Label chunkSizeLabel;
	protected Label errorLabel;
	protected SelectionListener selectionListener;
	protected SelectionListener chunkSizeSelectionListener;
	protected ModifyListener maxNumTasksModifyListener;
	protected ModifyListener chunkSizeModifyListener;

	protected void addListeners() {

		if (iterationModeCombo != null) {
			iterationModeCombo.addSelectionListener(selectionListener);
		}
		if (maxNumTasksText != null) {
			maxNumTasksText.addModifyListener(maxNumTasksModifyListener);
		}
		if (chunkSizeValue != null) {
			chunkSizeValue.addModifyListener(chunkSizeModifyListener);
		}
		if (chunkSizeCombo != null) {
			chunkSizeCombo.addSelectionListener(chunkSizeSelectionListener);
		}
	}

	public boolean checkInputValid() {
		return true;

	}

	@Override
	public void createControls(Composite parent,
			TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);

		FormData data;
		Composite flatForm = getWidgetFactory().createFlatFormComposite(parent);

		Group composite = getWidgetFactory().createGroup(flatForm,
				"For-Loop settings");
		data = new FormData();
		data.left = new FormAttachment(0, ITabbedPropertyConstants.HSPACE);
		data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(0, ITabbedPropertyConstants.VSPACE);
		data.bottom = new FormAttachment(100, -ITabbedPropertyConstants.VSPACE);
		composite.setLayoutData(data);
		composite.setLayout(new FormLayout());
		controls.add(composite);

		loopSettingsParent = getWidgetFactory().createComposite(composite);

		data = new FormData();
		data.left = new FormAttachment(0, ITabbedPropertyConstants.HSPACE);
		data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(0, ITabbedPropertyConstants.VSPACE);
		data.bottom = new FormAttachment(100, -ITabbedPropertyConstants.VSPACE);

		loopSettingsParent.setLayoutData(data);

		GridLayout layout = new GridLayout(3, false);
		loopSettingsParent.setLayout(layout);
		controls.add(loopSettingsParent);

		GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		Label label = getWidgetFactory().createLabel(loopSettingsParent,
				"Iterate over:");
		label.setLayoutData(gridData);
		controls.add(label);

		iterationModeCombo = getWidgetFactory()
				.createCCombo(loopSettingsParent);
		gridData = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		iterationModeCombo.setLayoutData(gridData);
		controls.add(iterationModeCombo);

		Label maxNumTasksLabel = getWidgetFactory().createLabel(
				loopSettingsParent, "Number of parallel tasks:");
		gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		maxNumTasksLabel.setLayoutData(gridData);
		controls.add(maxNumTasksLabel);

		getWidgetFactory().createLabel(loopSettingsParent, null);

		maxNumTasksText = getWidgetFactory().createText(loopSettingsParent, "",
				SWT.BORDER | SWT.RIGHT);
		gridData = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gridData.widthHint = 50;
		maxNumTasksText.setLayoutData(gridData);
		controls.add(maxNumTasksText);

		chunkSizeLabel = getWidgetFactory().createLabel(loopSettingsParent, "Iteration size:"); 
		gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		chunkSizeLabel.setLayoutData(gridData);
		controls.add(chunkSizeLabel);
		
		chunkSizeCombo = getWidgetFactory().createCCombo(loopSettingsParent);
		gridData = new GridData(SWT.FILL, SWT.CENTER, false, false);
		chunkSizeCombo.setLayoutData(gridData);
		controls.add(chunkSizeCombo);

		chunkSizeValue = getWidgetFactory().createText(loopSettingsParent, "",
				SWT.BORDER | SWT.RIGHT);
		gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gridData.widthHint = 50;
		chunkSizeValue.setLayoutData(gridData);
		controls.add(chunkSizeValue);

		errorLabel = getWidgetFactory().createLabel(loopSettingsParent, "");
		gridData = new GridData(SWT.LEFT, SWT.CENTER, false, true);
		errorLabel.setLayoutData(gridData);
		errorLabel.setForeground(errorLabel.getDisplay().getSystemColor(
				SWT.COLOR_RED));
		errorLabel.setVisible(false);
		createListeners();
	}

	protected void createListeners() {
		selectionListener = new SelectionListener() {
			private void updateVariableType() {
				String selected = iterationModeCombo.getItem(iterationModeCombo.getSelectionIndex());
				updateEnabled();
				getActivity().setIterationMode(selected);
				getActivity().getDiagram().setDirty(true);
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				updateVariableType();
			}

			public void widgetSelected(SelectionEvent e) {
				updateVariableType();
			}

		};

		maxNumTasksModifyListener = new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				String errorMsg = null;
				String s = maxNumTasksText.getText();

				try {
					if (s != null && s.trim().length() > 0) {
						Integer value = Integer.parseInt(s);
						if (value <= 0) {
							errorMsg = "Maximum number of parallel tasks must be positive";
						}
						Integer oldValue = (Integer) getActivity()
								.getPropertyValue(
										ForEachDataSourceActivity.PROP_MAX_PARALLEL_TASKS);
						boolean changed = oldValue == null ? value != null
								: !oldValue.equals(value);
						if (changed) {
							getActivity()
									.setPropertyValue(
											ForEachDataSourceActivity.PROP_MAX_PARALLEL_TASKS,
											value);
							getActivity().getDiagram().setDirty(true);
						}
					}
				} catch (Exception ex) {
					errorMsg = "Maximum number of parallel tasks must be an integer value";
				}
				if (errorMsg != null) {
					errorLabel.setText(errorMsg);
					errorLabel.setVisible(true);
					errorLabel.pack();
				} else {
					errorLabel.setVisible(false);
				}
			}
		};
		
		chunkSizeSelectionListener = new SelectionListener() {
			private void updateSelection() {
				int selectedIndex = chunkSizeCombo.getSelectionIndex();				
				if (selectedIndex == 0) {
					getActivity().setPropertyValue(ForEachDataSourceActivity.PROP_CHUNK_SIZE_KBYTES, Boolean.FALSE);
				} else {				
					getActivity().setPropertyValue(ForEachDataSourceActivity.PROP_CHUNK_SIZE_KBYTES, Boolean.TRUE);
					getActivity().getDiagram().setDirty(true);
				}
				getActivity().getDiagram().setDirty(true);
			}
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateSelection();
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				updateSelection();
				
			}
		};

		chunkSizeModifyListener = new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				String errorMsg = null;
				String s = chunkSizeValue.getText();

				try {
					if (s != null && s.trim().length() > 0) {
						Long value = Long.parseLong(s);
						if (value <= 0) {
							errorMsg = "Please enter a positive number";
						}
						Long oldValue = (Long) getActivity().getPropertyValue(
								ForEachDataSourceActivity.PROP_CHUNK_SIZE);
						boolean changed = oldValue == null ? value != null
								: !oldValue.equals(value);
						if (changed) {
							getActivity().setPropertyValue(
									ForEachDataSourceActivity.PROP_CHUNK_SIZE,
									value);
							getActivity().getDiagram().setDirty(true);
						}
					}
				} catch (Exception ex) {
					errorMsg = "Please enter an integer value";
				}
				if (errorMsg != null) {
					errorLabel.setText(errorMsg);
					errorLabel.setVisible(true);
					errorLabel.pack();
				} else {
					errorLabel.setVisible(false);
				}
			}
		};
		
		
	}

	@Override
	public void dispose() {
		removeListeners();
	}

	public ForEachDataSourceActivity getActivity() {
		return (ForEachDataSourceActivity) element;
	}

	protected void init() {
		if (element == null || loopSettingsParent == null) {
			return;
		}
		DataSourceActivity activity = getActivity();
		if (activity == null) {
			return;
		}
		String[] itModeStrings = getActivity().getIterationModeStrings();
		iterationModeCombo.setItems(itModeStrings);
		String iterationMode = getActivity().getIterationMode();
		int itModeIdx = 0;
		for (String string : itModeStrings) {
			if(string.equals(iterationMode)) {
				break;
			}
			itModeIdx++;
		}
		
		iterationModeCombo.select(itModeIdx);

		Integer maxNumTasks = (Integer) getActivity().getPropertyValue(
				ForEachDataSourceActivity.PROP_MAX_PARALLEL_TASKS);
		String maxNumTasksString = maxNumTasks == null ? "" : String
				.valueOf(maxNumTasks);
		maxNumTasksText.setText(maxNumTasksString);

		Long chunkSize = (Long) getActivity().getPropertyValue(
				ForEachDataSourceActivity.PROP_CHUNK_SIZE);
		String chunkSizeString = chunkSize == null ? "1" : String
				.valueOf(chunkSize);
		chunkSizeValue.setText(chunkSizeString);
		
		String [] items = {"Number of files per iteration", "Chunk size in Kbytes"};
		chunkSizeCombo.setItems(items);
				

		Boolean isChunkSizeKbytes = (Boolean) getActivity().getPropertyValue(ForEachDataSourceActivity.PROP_CHUNK_SIZE_KBYTES);
		int chunkSizeKbytesIndex = isChunkSizeKbytes == false ? 0 : 1;
		chunkSizeCombo.select(chunkSizeKbytesIndex);

	}

	protected void removeListeners() {
		if (iterationModeCombo != null && !iterationModeCombo.isDisposed()) {
			iterationModeCombo.removeSelectionListener(selectionListener);
		}
		if (maxNumTasksText != null && !maxNumTasksText.isDisposed()) {
			maxNumTasksText.removeModifyListener(maxNumTasksModifyListener);
		}
		if (chunkSizeValue != null && !chunkSizeValue.isDisposed()) {
			chunkSizeValue.removeModifyListener(chunkSizeModifyListener);
		}
		if (chunkSizeCombo != null && !chunkSizeCombo.isDisposed()) {
			chunkSizeCombo.removeSelectionListener(chunkSizeSelectionListener);			
		}
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		removeListeners();
		super.setInput(part, selection);
		addListeners();
		init();
		updateEnabled();
	}

	@Override
	protected void updateEnabled() {
		super.updateEnabled();
		boolean isFileMode = iterationModeCombo.getItem(iterationModeCombo.getSelectionIndex())
				.equals(ForEachDataSourceActivity.ITERATION_MODE_FILES);
		chunkSizeValue.setEnabled(canEditModel() && isFileMode);
		chunkSizeLabel.setEnabled(canEditModel() && isFileMode);
		chunkSizeCombo.setEnabled(canEditModel() && isFileMode);
	}
	
}
