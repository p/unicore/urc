package org.chemomentum.rcp.wfeditor.model;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;

import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSetSink;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSink;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;

@XStreamAlias("OverwriteWFFileLinker")
public class OverwriteWFFileLinker extends WFFileLinker {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7037482620554582845L;

	public OverwriteWFFileLinker(WorkflowFile linkTarget,
			GPEWorkflowFileSetSink sink, String fileId) {
		super(linkTarget, sink, fileId);
	}

	public OverwriteWFFileLinker(WorkflowFile linkTarget,
			GPEWorkflowFileSink sink) {
		super(linkTarget, sink);
	}

	public OverwriteWFFileLinker(WorkflowFile linkTarget,
			IGridBeanParameter param, String fileId, IAdaptable activity) {
		super(linkTarget, param, fileId, activity);
	}

	@Override
	protected void setValueInGridBean(QName key, Object value) {
		// don't touch the GridBean!
	}

}
