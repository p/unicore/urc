package org.chemomentum.rcp.wfeditor.submission;

import org.chemomentum.rcp.servicebrowser.nodes.WorkflowFactoryNode;
import org.chemomentum.rcp.wfeditor.ui.StorageSelectionWizardPage;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.wizard.IWizardPage;

import de.fzj.unicore.rcp.wfeditor.actions.ExportWorkflowAction;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.ISubmissionWizardPageFactory;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.submission.ISubmitter;
import de.fzj.unicore.rcp.wfeditor.ui.WFSubmissionWizard;

public class C9mSubmissionWizardPageFactory implements
		ISubmissionWizardPageFactory {

	private StorageSelectionWizardPage myPage = null;

	public IWizardPage[] createPages(WFSubmissionWizard wizard,
			WorkflowDiagram model, IAdaptable submissionService,
			ISubmitter submitter) {
		if (isUploadingInputFiles(wizard) && !isOldWorkflowEngine(wizard)) {
			return new IWizardPage[] { getMyPage((C9MSubmitter) submitter) };
		} else {
			return new IWizardPage[] {};
		}

	}

	public void dispose() {
		if (myPage != null) {
			myPage.dispose();
		}
		myPage = null;
	}

	protected StorageSelectionWizardPage getMyPage(C9MSubmitter submitter) {
		if (myPage == null) {
			myPage = new StorageSelectionWizardPage(submitter);
		}
		return myPage;
	}

	protected boolean isOldWorkflowEngine(WFSubmissionWizard wizard) {
		try {
			WorkflowFactoryNode n = (WorkflowFactoryNode) wizard
					.getSelectedNode();
			String version = n.getWorkflowFactoryProperties().getVersion();
			return version.compareTo("2.1.0") < 0;
		} catch (Exception e) {
			return true;
		}

	}

	protected boolean isUploadingInputFiles(WFSubmissionWizard wizard) {
		// check whether input files should be uploaded at all. if not return no
		// page
		IContributionItem[] items = wizard.getPage1().getLowerToolBarManager()
				.getItems();
		for (IContributionItem item : items) {
			if (ExportWorkflowAction.UPLOAD_INPUT_FILES.equals(item.getId())) {
				IAction action = ((ActionContributionItem) item).getAction();
				return action.isChecked();
			}
		}
		return true; // not exporting => always upload files
	}

	public IWizardPage[] updatePages(IWizardPage[] oldPages,
			WFSubmissionWizard wizard, WorkflowDiagram model,
			IAdaptable submissionService, ISubmitter submitter) {
		if (isUploadingInputFiles(wizard) && !isOldWorkflowEngine(wizard)) {
			if (oldPages.length > 0) {
				return oldPages;
			} else {
				return new IWizardPage[] { getMyPage((C9MSubmitter) submitter) };
			}
		} else {
			return new IWizardPage[] {};
		}

	}

}
