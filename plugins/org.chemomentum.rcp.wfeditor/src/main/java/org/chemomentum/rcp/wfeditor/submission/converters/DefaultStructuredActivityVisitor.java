/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.submission.converters;

import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.wfeditor.C9MWFEditorConstants;
import org.chemomentum.rcp.wfeditor.utils.SimpleWFUtil;
import org.chemomentum.simpleworkflow.api.ActivityElement;
import org.chemomentum.simpleworkflow.api.IElement;
import org.chemomentum.simpleworkflow.api.SubWorkflowElement;
import org.chemomentum.simpleworkflow.api.TransitionBuilder;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityDocument.Activity;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityType;
import org.chemomentum.simpleworkflow.xmlbeans.DeclareVariableDocument.DeclareVariable;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.StartActivity;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.submission.IConverter;
import de.fzj.unicore.rcp.wfeditor.traversal.IElementVisitor;

/**
 * @author demuth
 * 
 */
public class DefaultStructuredActivityVisitor implements IElementVisitor {

	private IAdaptable submissionService;
	private String workflowId;
	private WorkflowDiagram model;
	private IProgressMonitor monitor;
	private QName targetDialect;

	private static final String SPLIT_SUFFIX = "-SPLIT";

	private SubWorkflowElement graph;
	Map<String, Object> additionalParams;

	public DefaultStructuredActivityVisitor(IAdaptable submissionService,
			String workflowId, WorkflowDiagram model, IProgressMonitor monitor,
			QName targetDialect, Map<String, Object> additionalParams) {
		this.submissionService = submissionService;
		this.workflowId = workflowId;
		this.model = model;
		this.monitor = monitor;
		this.targetDialect = targetDialect;
		this.additionalParams = additionalParams;
		this.graph = (SubWorkflowElement) additionalParams
				.get(C9MWFEditorConstants.PARAM_SUBGRAPH);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.traversal.IElementVisitor#isDone()
	 */
	public boolean isDone() {
		return false;// !monitor.isCanceled();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.traversal.IElementVisitor#visit(de.fzj.unicore
	 * .rcp.wfeditor.model.IFlowElement)
	 */
	public void visit(IFlowElement element) throws Exception {
		if (!(element instanceof IActivity)) {
			return;
		}
		IActivity activity = (IActivity) element;
		String id = activity.getName();

		// declare variables
		for (WorkflowVariable var : activity.getVariableList().getVariables()) {
			DeclareVariable variable = DeclareVariable.Factory.newInstance();
			variable.setName(var.getName());
			variable.setType(SimpleWFUtil.convertVariableType(var.getType()));
			variable.setInitialValue(var.getInitialValue());
			variable.setId(var.getName());
			graph.addVariableDeclaration(variable);
		}

		IElement inner = null;

		if (activity instanceof StartActivity) {
			Activity start = Activity.Factory.newInstance();
			start.setId(id);
			start.setType(ActivityType.START);
			start.setName(ActivityType.START.toString());
			inner = new ActivityElement(start);
			graph.addElement(inner);
			monitor.worked(1);
		} else {
			boolean isSubmit = ((Integer) additionalParams
					.get(C9MWFEditorConstants.PARAM_SUBMISSION_MODE)) == C9MWFEditorConstants.SUBMISSION_MODE_WF_ENGINE;
			if (isSubmit) {
				activity.changeCurrentExecutionState(
						ExecutionStateConstants.STATE_NOT_STARTED,
						"not yet started");
			}
			SubProgressMonitor subMonitor = new SubProgressMonitor(monitor, 1,
					SubProgressMonitor.PREPEND_MAIN_LABEL_TO_SUBTASK);
			IConverter converter = WFActivator.getDefault()
					.getConverterRegistry()
					.getConverter(element.getType(), targetDialect);
			if (converter != null) {
				inner = (IElement) converter.convert(submissionService,
						workflowId, model, element, additionalParams,
						subMonitor);
			}
			if (inner == null) {
				return;
			}
		}

		if (activity.getOutgoingTransitions().size() > 1) {
			Activity split = Activity.Factory.newInstance();
			split.setId(activity.getName() + SPLIT_SUFFIX);
			split.setType(ActivityType.SPLIT);
			split.setName(ActivityType.SPLIT.toString());
			graph.addActivity(split);

			TransitionBuilder transitionBuilder = new TransitionBuilder(
					inner.getId() + "->" + split.getId());
			transitionBuilder.from(inner.getId());
			transitionBuilder.to(split.getId());
			graph.addTransition(transitionBuilder.getTransition());
		}

		List<Transition> incoming = activity.getIncomingTransitions();
		for (Transition transition : incoming) {
			String sourceId = transition.source.getName();

			IElement pred = graph.getElement(sourceId + SPLIT_SUFFIX);
			if (pred == null) {
				pred = graph.getElement(sourceId);
			}
			try {
				graph.addTransition(pred.getId(), inner.getId());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
