/*********************************************************************************
 * Copyright (c) 2016 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.properties;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import org.chemomentum.rcp.wfeditor.model.DataSourceActivity;
import org.chemomentum.rcp.wfeditor.model.ForEachDataSourceActivity;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.ISection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableList;
import de.fzj.unicore.rcp.wfeditor.properties.AbstractSection;

/**
 * An {@link ISection} to edit a value set of {@link String}s. The UI component
 * will be a {@link ValueSetEditor}.
 * 
 * @author bjoernh
 *
 * 08.07.2016 13:22:46
 *
 */
public class ValueSetSection extends AbstractSection {
	
	ValueSetEditor vse = null;
	private PropertyChangeListener dataSourceActiveListener;
	
	/**
	 * @see org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#createControls(org.eclipse.swt.widgets.Composite, org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage)
	 */
	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);
	
		Composite flatForm = getWidgetFactory().createFlatFormComposite(parent);
		Group valueSetGroup = getWidgetFactory().createGroup(flatForm, "Value Set");
		
		FormData fd = new FormData();
		fd.left = new FormAttachment(0, 5);
		fd.right = new FormAttachment(100, -5);
		fd.top = new FormAttachment(0, 5);
		fd.bottom = new FormAttachment(100, -5);
		valueSetGroup.setLayoutData(fd);
		valueSetGroup.setLayout(new FormLayout());
		
		vse = new ValueSetEditor(valueSetGroup, SWT.None);
		vse.addPropertyChangeListener(this);
		getWidgetFactory().adapt(vse);
		
		fd = new FormData();
		fd.left = new FormAttachment(0, 5);
		fd.right = new FormAttachment(100, -5);
		fd.top = new FormAttachment(0, 5);
		fd.bottom = new FormAttachment(100, -5);
		
		vse.setLayoutData(fd);

		controls.add(valueSetGroup);
		
		createListeners();
	}

	/**
	 * 
	 */
	private void createListeners() {
		dataSourceActiveListener = new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if(!ForEachDataSourceActivity.PROP_ITERATION_MODE.equals(evt.getPropertyName())) {
					return;
				}
				vse.setEnabled(ForEachDataSourceActivity.ITERATION_MODE_VALUE_SET.equals(evt.getNewValue()));
			}
		};
		
	}
	
	private ForEachDataSourceActivity getActivity() {
		return (ForEachDataSourceActivity) element;
	}
	
	
	protected void addListeners() {
		if(getActivity() != null && dataSourceActiveListener != null) {
			getActivity().addPropertyChangeListener(dataSourceActiveListener);
		}
	}
	
	protected void removeListeners() {
		if(getActivity() != null && dataSourceActiveListener != null) {
			getActivity().removePropertyChangeListener(dataSourceActiveListener);
		}
	}
	
	/**
	 * @see org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#dispose()
	 */
	@Override
	public void dispose() {
		if(vse != null) {
			vse.removePropertyChangeListener(this);
		}
		removeListeners();
	}

	/**
	 * @see de.fzj.unicore.rcp.wfeditor.properties.AbstractSection#setInput(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		removeListeners();
		ForEachDataSourceActivity old = getActivity();
		if(old != null) {
			old.setValueSet(vse.getValueSet());
		}
		super.setInput(part, selection);
		addListeners();
		if (old != null && old.equals(getActivity())) {
			return;
		}
		init();
		updateEnabled();
	}
	
	/**
	 * 
	 */
	private void init() {
		List<String> valueSet = (List<String>) getActivity().getValueSet();
		if (valueSet == null) {
			valueSet = new ArrayList<String>();
		}
		vse.setValueSet(valueSet);
	}

	@Override
	protected void updateEnabled() {
		super.updateEnabled();
		vse.setEnabled(getActivity().getPropertyValue(ForEachDataSourceActivity.PROP_ITERATION_MODE)
				.equals(ForEachDataSourceActivity.ITERATION_MODE_VALUE_SET) && canEditModel());
	}
	
	/**
	 * @see de.fzj.unicore.rcp.wfeditor.properties.AbstractSection#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if(ValueSetEditor.PROP_VALUE_SET.equals(evt.getPropertyName())) {
			getActivity().setValueSet(vse.getValueSet());
			// this should actually be done by the activity, but has been done
			// this way in other sections, too.
			getActivity().getDiagram().setDirty(true);
		}
	}

}
