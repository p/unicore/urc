package org.chemomentum.rcp.wfeditor.extensions;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.chemomentum.rcp.wfeditor.model.EnvVariableLinker;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;

import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.IEnvironmentVariableParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowVariableSink;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.IDataFlowAssembler;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.commands.DataFlowAssemblyCommand;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifier;
import de.fzj.unicore.rcp.wfeditor.traversal.IElementVisitor;
import de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.PredecessorTraverser;

public class WorkflowVariableDataFlowAssembler implements IDataFlowAssembler {

	class FindModifierVisitor implements IElementVisitor {
		boolean modifierFound = false;
		WorkflowVariable var;

		public FindModifierVisitor(WorkflowVariable var) {
			super();
			this.var = var;
		}

		public boolean isDone() {
			return modifierFound;
		}

		public void visit(IFlowElement element) throws Exception {
			List<WorkflowVariableModifier> modifiers = element
					.getVariableModifierList().getModifiers();
			for (WorkflowVariableModifier mod : modifiers) {
				if (var.equals(mod.getVariable())) {
					modifierFound = true;
				}
			}
		}

	}

	public DataFlowAssemblyCommand getAssemblyCommand(final IDataSource source,
			final IDataSink sink) throws Exception {

		if (source.getOutgoingDataTypes() != null
				&& sink.getIncomingDataTypes() != null
				&& !source.getOutgoingDataTypes().isEmpty()
				&& !sink.getIncomingDataTypes().isEmpty()) {
			Set<String> validTypes = new HashSet<String>(
					sink.getIncomingDataTypes());
			validTypes.retainAll(source.getOutgoingDataTypes());
			if (validTypes.isEmpty()) {
				return null;
			}
		}

		final WorkflowVariable variable = (WorkflowVariable) source;

		// check whether the sink's parent flow element is executed AFTER a
		// modifier that modifies the variable (the source)
		// if so, it doesn't make sense to connect to the source
		// instead, the user should connect to the modifier
		IGraphTraverser traverser = new PredecessorTraverser();
		FindModifierVisitor visitor = new FindModifierVisitor(variable);
		try {
			traverser.traverseGraph(sink.getFlowElement().getDiagram(),
					sink.getFlowElement(), visitor);
		} catch (Exception e) {
			WFActivator
					.log(IStatus.ERROR,
							"Unexpected exception while searching for existing data flows",
							e);
		}
		if (visitor.modifierFound) {
			return null;
		}
		return new DataFlowAssemblyCommand() {
			IDataFlow oldLinker;
			IEnvironmentVariableParameterValue oldValue;
			IGridBeanParameter param;
			IAdaptable activity;

			@Override
			protected IDataFlow assembleDataFlow() {

				if (sink instanceof GPEWorkflowVariableSink) {
					GPEWorkflowVariableSink variableSink = (GPEWorkflowVariableSink) sink;
					activity = variableSink.getFlowElement();
					IGridBean gb = (IGridBean) activity
							.getAdapter(IGridBean.class);
					param = variableSink.getParameter();
					oldValue = (IEnvironmentVariableParameterValue) gb
							.get(param.getName());

					oldLinker = getOldLinker(variableSink);
					if (oldLinker != null) {
						oldLinker.disconnect();
					}

					EnvVariableLinker linker = new EnvVariableLinker(variable,
							variableSink);
					linker.connect();
					gb.set(param.getName(),
							linker.getCurrentValue(oldValue, variable));
					return linker;
				}

				return null;

			}

			protected IDataFlow getOldLinker(GPEWorkflowVariableSink sink) {
				if (sink.getIncomingFlows() != null
						&& sink.getIncomingFlows().length > 0) {
					return sink.getIncomingFlows()[0];
				}
				return null;
			}

			@Override
			public void redo() {
				if (oldLinker != null) {
					oldLinker.disconnect();
				}
				super.redo();

			}

			@Override
			public void undo() {
				super.undo();
				if (oldLinker != null) {
					oldLinker.reconnect();
				}
				IGridBean gb = (IGridBean) activity.getAdapter(IGridBean.class);
				gb.set(param.getName(), oldValue);
			}

		};
	}
}
