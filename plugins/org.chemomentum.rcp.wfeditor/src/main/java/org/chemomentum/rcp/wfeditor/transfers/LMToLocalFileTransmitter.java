/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.transfers;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.chemomentum.common.util.C9MCommonConstants;
import org.chemomentum.common.ws.ILocationManager;
import org.chemomentum.dataManagement.locationManager.QueryLocationRequestDocument;
import org.chemomentum.dataManagement.locationManager.QueryLocationRequestDocument.QueryLocationRequest.Input;
import org.chemomentum.dataManagement.locationManager.QueryLocationResponseDocument;
import org.chemomentum.dataManagement.locationManager.QueryLocationResponseDocument.QueryLocationResponse.Result;
import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.chemomentum.rcp.wfeditor.model.LMFileAddress;
import org.eclipse.core.runtime.IStatus;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.StorageClient;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.transfers.FileTransmitter;
import com.intel.gpe.clients.api.transfers.IGridFileTransfer;
import com.intel.gpe.clients.impl.sms.StorageClientImpl;
import com.intel.gpe.clients.impl.transfers.U6ProtocolConstants;
import com.intel.gpe.clients.impl.transfers.U6ToLocalFileTransmitter;

import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.wsrflite.xmlbeans.client.RegistryClient;
import eu.unicore.security.wsutil.client.UnicoreWSClientFactory;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * @author demuth
 * 
 */
public class LMToLocalFileTransmitter implements FileTransmitter {

	private Exception occuredException = null;

	protected Exception getOccuredException() {
		return occuredException;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.chemomentum.servorch.wamanage.ILocationMapper#mapToPhysicalNames(
	 * java.util.Set)
	 */
	public Map<String, List<EndpointReferenceType>> mapToPhysicalNames(
			Set<String> logicalNames, EndpointReferenceType lmEpr)
			throws Exception {
		Map<String, List<EndpointReferenceType>> map = new HashMap<String, List<EndpointReferenceType>>();
		Input[] inputs = new Input[logicalNames.size()];
		int i = 0;
		for (String name : logicalNames) {
			Input input = Input.Factory.newInstance();
			input.setLogicalName(name);
			inputs[i++] = input;
		}
		Result[] results = queryLocationManager(inputs, lmEpr);
		if (results == null || results.length == 0) {
			return null;
		}
		for (int j = 0; j < results.length; j++) {
			List<EndpointReferenceType> list = processResult(results[j]);
			if (list != null) {
				map.put(results[j].getLogicalName(), list);
			} else {
				return null;
			}
		}
		return map;
	}

	protected void setOccuredException(Exception occuredException) {
		this.occuredException = occuredException;
	}

	public void transmitFiles(Client client, List<IGridFileTransfer> files,
			IProgressListener progress) throws Exception {

		try {
			progress.beginTask("downloading file", 100);

			setOccuredException(null);
			if (files.size() == 0) {
				return;
			}
			LMFileAddress file = (LMFileAddress) files.get(0)
					.getProcessedSource();
			URI uri = file.getRegistryURI();
			EndpointReferenceType epr = NodeFactory.getEPRForURI(uri);
			IClientConfiguration sp = ServiceBrowserActivator.getDefault()
					.getUASSecProps(uri);
			RegistryClient regClient = new RegistryClient(uri.toString(), epr,
					sp);
			List<EndpointReferenceType> lmEprs = regClient
					.listServices(ILocationManager.PORT);
			if (lmEprs.size() > 0) {
				progress.worked(10);
				IProgressListener subProgress = progress.beginSubTask(
						"transmitting", 90);
				transmitFilesViaLocationManager(client, lmEprs.get(0), files,
						subProgress);
				if (getOccuredException() != null) {
					throw new Exception("File transfer failed: "
							+ getOccuredException().getMessage(),
							getOccuredException());
				}
			} else {
				throw new Exception(
						"No data management access service is available.");
			}
		} finally {
			progress.done();
		}

	}

	public void transmitFilesViaLocationManager(Client client,
			final EndpointReferenceType lmEpr,
			final List<IGridFileTransfer> files, IProgressListener progress)
			throws Exception {

		try {
			progress.beginTask("downloading file", 100);

			Set<String> logicalNames = new HashSet<String>();

			for (IGridFileTransfer file : files) {
				LMFileAddress source = (LMFileAddress) file
						.getProcessedSource();
				String logical = source.getLogicalName();
				String prefix = C9MCommonConstants.LOGICAL_FILENAME_PREFIX;
				if (!logical.startsWith(prefix)) {
					logical = prefix + logical;
				}
				logicalNames.add(logical);
			}

			Map<String, List<EndpointReferenceType>> mapped;
			try {
				mapped = mapToPhysicalNames(logicalNames, lmEpr);
			} catch (Exception e2) {
				setOccuredException(new Exception(
						"Could not map logical filenames to physical locations.!",
						e2));
				return;
			}

			progress.worked(1);

			final List<IGridFileTransfer> resolvedFiles = new ArrayList<IGridFileTransfer>();
			for (IGridFileTransfer file : files) {
				LMFileAddress source = (LMFileAddress) file
						.getProcessedSource();
				String logical = source.getLogicalName();
				String prefix = C9MCommonConstants.LOGICAL_FILENAME_PREFIX;
				if (!logical.startsWith(prefix)) {
					logical = prefix + logical;
				}
				EndpointReferenceType epr;
				try {
					epr = mapped.get(logical).get(0);

				} catch (Exception e) {
					setOccuredException(new Exception(
							"No physical location found for logical filename "
									+ logical, e));
					progress.worked(1);
					continue;
				}
				try {

					String fileAddress = epr.getAddress().getStringValue();
					String storageAddress = UnicoreStorageTools
							.getStorageAddress(fileAddress);
					String relative = fileAddress.substring(fileAddress
							.indexOf("#") + 1);
					URI uri = new URI(storageAddress);
					epr = NodeFactory.getEPRForURI(uri);
					StorageClient storage = new StorageClientImpl(GPEActivator
							.getDefault().getClient().getWSRFClientFactory()
							.createWSRFClient(epr));
					IProgressListener subProgress = progress.beginSubTask("",
							IProgressListener.UNKNOWN);
					file.setProcessedSource(storage.getAbsoluteAddress(
							relative, subProgress));
					subProgress = progress.beginSubTask("",
							IProgressListener.UNKNOWN);
					resolvedFiles.addAll(client
							.getFileProvider()
							.getFileResolver(
									U6ProtocolConstants.UNICORE_SMS_QNAME)
							.resolveFiles(file, client, subProgress));

				} catch (Exception e) {
					setOccuredException(e);
				}
				progress.worked(1);
			}
			IProgressListener subProgress = progress.beginSubTask("", 98);
			try {
				new U6ToLocalFileTransmitter().transmitFiles(client,
						resolvedFiles, subProgress);
			} catch (Exception e) {
				setOccuredException(e);
			}
		} finally {
			progress.done();
		}
	}

	protected static List<EndpointReferenceType> processResult(Result result) {

		List<EndpointReferenceType> list = new ArrayList<EndpointReferenceType>();
		try {

			String[] physicalLocations = result.getPhysicalLocationArray();

			if (physicalLocations != null && physicalLocations.length > 0) {
				for (int i = 0; i < physicalLocations.length; i++) {
					if (physicalLocations[i] != null) {
						EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
						epr.addNewAddress()
								.setStringValue(physicalLocations[i]);
						list.add(epr);
					} else {
						return null;
					}
				}
				return list;
			} else if (result.getLogicalNameNotExistFault() != null) {
				WFEditorActivator.log(IStatus.ERROR,
						"Could not map logical filename to physical location: \n"
								+ "short error description: "
								+ result.getLogicalNameNotExistFault()
										.getShortDescription()
								+ "\n"
								+ "detailed error description: "
								+ result.getLogicalNameNotExistFault()
										.getDetailedDescription());
			}
		} catch (Exception e) {
			WFEditorActivator.log(IStatus.ERROR,
					"Could not parse result from LocationManager correctly: ",
					e);
		}
		return null;
	}

	/**
	 * call Location Management Service for mapping logical names to physical
	 * locations
	 */
	protected static Result[] queryLocationManager(Input[] logicalNames,
			EndpointReferenceType epr) throws Exception {

		IClientConfiguration sp = ServiceBrowserActivator.getDefault()
				.getUASSecProps(new URI(epr.getAddress().getStringValue()));
		ILocationManager lm = new UnicoreWSClientFactory(sp)
				.createPlainWSProxy(ILocationManager.class, epr.getAddress()
						.getStringValue());
		QueryLocationRequestDocument request = QueryLocationRequestDocument.Factory
				.newInstance();

		request.addNewQueryLocationRequest().setInputArray(logicalNames);
		QueryLocationResponseDocument response = lm.queryLocation(request);
		return response.getQueryLocationResponse().getResultArray();

	}

}
