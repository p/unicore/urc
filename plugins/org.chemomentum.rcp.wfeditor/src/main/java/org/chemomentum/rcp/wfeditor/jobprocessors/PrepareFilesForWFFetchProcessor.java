/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.jobprocessors;

import java.util.Map;

import org.chemomentum.rcp.common.C9mRCPCommonConstants;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.StorageClient;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.clients.api.transfers.IGridFileTransfer;
import com.intel.gpe.clients.api.transfers.IProtocol;
import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.clients.common.transfers.GridFileAddress;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.AbstractProtocolProcessor;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;

import de.fzj.unicore.rcp.gpe4eclipse.utils.ParameterProcessingUtils;

/**
 * 
 * @author demuth
 * 
 */
public class PrepareFilesForWFFetchProcessor extends AbstractProtocolProcessor {

	public PrepareFilesForWFFetchProcessor() {
		processingSteps.add(ProcessingConstants.REMOVE_UNAVAILABLE_OUTCOMES);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.gridbeans.parameters.IProtocolProcessor#matches(com.intel
	 * .gpe.gridbeans.IGridBeanParameter,
	 * com.intel.gpe.gridbeans.IGridBeanParameterValue, java.util.Map)
	 */
	public boolean matches(IGridBeanParameter param,
			IGridBeanParameterValue value, Map<String, Object> processorParams) {
		if (!(value instanceof IFileParameterValue)) {
			return false;
		}
		IFileParameterValue file = (IFileParameterValue) value;
		return ProtocolConstants.JOB_WORKING_DIR_PROTOCOL.equals(file
				.getProcessedSource().getProtocol());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.gridbeans.parameters.IProtocolProcessor#process(com.intel
	 * .gpe.clients.api.jsdl.gpe.GPEJob, int,
	 * com.intel.gpe.gridbeans.IGridBeanParameter,
	 * com.intel.gpe.gridbeans.IGridBeanParameterValue, java.util.Map)
	 */
	public void process(Client client, GPEJob job, int processingStep,
			IGridBeanParameter param, IGridBeanParameterValue value,
			Map<String, Object> processorParams, IProgressListener progress)
			throws Exception {

		if (value instanceof IGridFileTransfer) {
			IGridFileTransfer file = (IGridFileTransfer) value;
			IProtocol targetProtocol = file.getProcessedTarget().getProtocol();
			if (C9mRCPCommonConstants.WFFILE_PROTOCOL.equals(targetProtocol)) {
				StorageClient storageClient = (StorageClient) processorParams
						.get(ProcessingConstants.JOB_WORKING_DIR);
				String logical = file.getProcessedTarget().getInternalString();
				logical = ParameterProcessingUtils.replaceVariables(logical,
						processorParams);
				file.setProcessedSource(storageClient.getAbsoluteAddress(
						logical, null));
			} else if (C9mRCPCommonConstants.DMFILE_PROTOCOL
					.equals(targetProtocol)) {

				file.setProcessedSource(file.getProcessedTarget().clone());
			} else {
				// cannot fetch outcomes from additional targets
				file.setProcessedSource(new GridFileAddress("",
						ProtocolConstants.NONE_PROTOCOL));
			}

		}

	}

}
