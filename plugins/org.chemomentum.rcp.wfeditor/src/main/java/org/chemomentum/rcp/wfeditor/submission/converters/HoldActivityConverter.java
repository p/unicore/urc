package org.chemomentum.rcp.wfeditor.submission.converters;

import java.util.HashMap;
import java.util.Map;
import javax.xml.namespace.QName;
import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.C9MWFEditorConstants;
import org.chemomentum.rcp.wfeditor.model.HoldActivity;
import org.chemomentum.simpleworkflow.api.ActivityElement;
import org.chemomentum.simpleworkflow.api.IElement;
import org.chemomentum.simpleworkflow.api.SubWorkflowElement;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityDocument.Activity;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityType;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.submission.IConverter;

/**
 * @author mborcz
 * 
 */

public class HoldActivityConverter implements IConverter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.IConverter#canConvert(javax.xml
	 * .namespace.QName, javax.xml.namespace.QName)
	 */
	public boolean canConvert(IAdaptable submissionService, QName modelType,
			QName wfLanguageType) {
		return HoldActivity.TYPE.equals(modelType)
				&& C9mRCPCommonConstants.C9M_DEFAULT_WF_DIALECT
						.equals(wfLanguageType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.IConverter#convert(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram)
	 */
	public IElement convert(IAdaptable submissionService, String workflowId,
			WorkflowDiagram model, IFlowElement element,
			Map<String, Object> additionalParams, IProgressMonitor monitor)
			throws Exception {

		HoldActivity holdResumeActivity = (HoldActivity) element;

		int scale = 1000;
		monitor.beginTask("", 100 * scale);
		try {

			Activity hold = Activity.Factory.newInstance();

			hold.setId(holdResumeActivity.getName());
			hold.setType(ActivityType.HOLD);
			hold.setName(ActivityType.HOLD.toString());

			boolean addIterationNumberToJobname = false;
			StructuredActivity parent = holdResumeActivity.getParent();
			while (parent != null) {
				if (parent.isLoop()) {
					addIterationNumberToJobname = true;
					break;
				}
				parent = parent.getParent();
			}

			Map<String, Object> processorParams = new HashMap<String, Object>();
			processorParams.put(ProcessingConstants.WORKFLOW_ID, workflowId);

			processorParams.put(GPE4EclipseConstants.ITERATION_ID, "");

			processorParams.put(
					C9MWFEditorConstants.PARAM_ADD_ITERATION_NUMBER_TO_JOBNAME,
					addIterationNumberToJobname);

			SubWorkflowElement graph = (SubWorkflowElement) additionalParams
					.get(C9MWFEditorConstants.PARAM_SUBGRAPH);

			graph.addActivity(hold);

			if (monitor.isCanceled()) {
				return null;
			}

			return new ActivityElement(hold);
		} finally {
			monitor.done();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.submission.IConverter#getModelType()
	 */
	public QName getModelType() {
		return HoldActivity.TYPE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.submission.IConverter#getTargetType()
	 */
	public QName getTargetType() {
		return C9mRCPCommonConstants.C9M_DEFAULT_WF_DIALECT;
	}

}
