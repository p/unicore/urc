/*********************************************************************************
 * Copyright (c) 2014 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.submission.converters;

import java.util.Collections;
import java.util.Map;

import org.chemomentum.rcp.wfeditor.C9MWFEditorConstants;
import org.chemomentum.simpleworkflow.api.LoopBodyElement;
import org.chemomentum.simpleworkflow.api.SubWorkflowElement;
import org.chemomentum.simpleworkflow.xmlbeans.OptionDocument.Option;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.structures.ContainerActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.LoopActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.submission.IConverter;
import de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.TopDownDAGTraverser;

/**
 * @author bjoernh
 * 
 *         21.11.2014 10:08:32
 * 
 */
public abstract class LoopActivityConverter implements IConverter {

	/**
	 * 
	 */
	public LoopActivityConverter() {
		super();
	}

	protected SubWorkflowElement convertBody(SubWorkflowElement parent,
			IAdaptable submissionService, String workflowId,
			WorkflowDiagram model, LoopActivity loopActivity,
			IFlowElement element, Map<String, Object> additionalParams,
			IProgressMonitor monitor) throws Exception {
		StructuredActivity body = loopActivity.getBody();
		String iteratorName = null;
		LoopBodyElement sub = new LoopBodyElement(body.getName(), parent,
				iteratorName);

		// Co-brokering
		Boolean coBroBoolean = (Boolean) body
				.getPropertyValue(ContainerActivity.COBROKER);
		if (coBroBoolean != null && coBroBoolean.booleanValue()) {
			Option cobroOption = Option.Factory.newInstance();
			cobroOption.setName(ContainerActivity.COBROKER);
			cobroOption.setStringValue("true");
			sub.setOptions(Collections.singletonList(cobroOption));
		}

		additionalParams.put(C9MWFEditorConstants.PARAM_SUBGRAPH, sub);
		IGraphTraverser traverser = new TopDownDAGTraverser();
		DefaultStructuredActivityVisitor visitor = new DefaultStructuredActivityVisitor(
				submissionService, workflowId, model, monitor, getTargetType(),
				additionalParams);
		traverser.traverseGraph(body, null, visitor);

		return sub;
	}

}