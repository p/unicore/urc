/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor;


/**
 * @author demuth
 * 
 */
public interface C9MWFEditorConstants {

	public static final String C9M_EDITOR_ID = "de.fzj.unicore.rcp.wfeditor.extensions.editor";

	/**
	 * Key for passing a subgraph down to the subgraph converters (via the
	 * additionalParams field).
	 */
	public static final String PARAM_SUBGRAPH = "subgraph";

	/**
	 * Key for telling converters which kind of submission is being performed.
	 * Currently there are two modes: 1. real submission to a workflow engine 2.
	 * create a workflow description (used for exporting the workflow, e.g. for
	 * the UNICORE command line client)
	 */
	public static final String PARAM_SUBMISSION_MODE = "submission mode";

	public static final int SUBMISSION_MODE_WF_ENGINE = 1,
			SUBMISSION_MODE_EXPORT = 2;

	/**
	 * Key for specifying if input files shall be uploaded during export mode.
	 * The value of this parameter is of type Boolean.
	 */
	public static final String PARAM_UPLOAD_FILES = "upload files";

	/**
	 * Key for specifying the version of the workflow engine we'd like to submit
	 * to. The value is of type String.
	 */
	public static final String PARAM_WF_ENGINE_VERSION = "wf engine version";

	/**
	 * Key for passing a unique ID that acts as a workflow id replacement when
	 * uploading files during workflow export to the Activity Converters.
	 */
	public static final String PARAM_UPLOAD_FILES_ID = "upload files id";

	/**
	 * Key for specifying if the current iteration number of the job within a
	 * loop shall be appended to the job's name. The value of this parameter is
	 * of type Boolean.
	 */
	public static final String PARAM_ADD_ITERATION_NUMBER_TO_JOBNAME = "add iteration number to jobname";

	/**
	 * Key for passing the activity being processed to the processors
	 */
	public static final String PARAM_GRID_BEAN_ACTIVITY = "my gridbean activity";

	/**
	 * Key for passing a client stub for the selected storage that will hold the
	 * input files for the workflow. The value is of type StorageClient
	 */
	public static final String PARAM_SELECTED_STORAGE = "selected workflow storage";

	/**
	 * Key for collecting all job definitions that are being embedded in the
	 * workflow graph. The value is a {@link Map<String,JobDefinitionType>} that
	 * contains activity names as keys and job definitions as values.
	 */
	public static final String PARAM_CREATED_JOB_DEFINITIONS = "created job definitions";

	/**
	 * Variable String that can be used within the workflow description in order
	 * to refer to the current workflow's ID.
	 */
	public static final String WORKFLOW_ID_VARIABLE = "${WORKFLOW_ID}";

}
