package org.chemomentum.rcp.wfeditor.model;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IStatus;

import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSetSink;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.model.variables.IElementWithVariableIterators;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableIterator;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableIteratorList;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableList;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

@XStreamAlias("ForEachDataSourceActivity")
public class ForEachDataSourceActivity extends DataSourceActivity implements IElementWithVariableIterators {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3197911553738088889L;

	/**
	 * Used to obtain the actual iterator value instead of the iterator
	 * index/counter.
	 */
	public static final String ITERATOR_VALUE_SUFFIX = "_ITERATOR_VALUE";

	/**
	 * A unique id used for the one value set iterator of this action. The name
	 * will be the name of the action suffixed with
	 * {@link #ITERATOR_VALUE_SUFFIX}.
	 */
	public static final String NAMED_ITERATOR_ID = "named iterator id";

	/**
	 * A unique id used for the CURRENT_ITERTAOR_VALUE workflow variable.
	 */
	public static final String CURRENT_ITERATOR_ID = "CURRENT_ITERATOR_VALUE";

	public static final QName TYPE = new QName("http://www.unicore.eu/", "ForEachDataSourceActivity");

	/**
	 * Key for Integer property that states the iteration mode of the "For"-loop
	 * (e.g. iterate over a list of files, a list of workflow variable values
	 * ...)
	 */
	public static final String PROP_ITERATION_MODE = "iteration mode";

	/**
	 * Key for Integer property that states how many interations of the for loop
	 * should be executed simultaneously.
	 */
	public static final String PROP_MAX_PARALLEL_TASKS = "max parallel tasks";

	/**
	 * Key for Long property that states how many files should be processed in
	 * one loop iteration. Defaults to 1.
	 */
	public static final String PROP_CHUNK_SIZE = "chunk size";

	/**
	 * Key for Boolean property that states that chunk size is in kbytes
	 * Defaults to <code>false</code>.
	 */
	public static final String PROP_CHUNK_SIZE_KBYTES = "chunk size is kbytes";

	/**
	 * Key for {@link List<String>} of values.
	 */
	public static final String PROP_VALUE_SET = "value set";

	public static final String ITERATION_MODE_FILES = "Files", ITERATION_MODE_WORKFLOW_VAR = "Workflow Variable Values",
			ITERATION_MODE_INDIRECTED_FILES = "Filenames read from given files", ITERATION_MODE_VALUE_SET = "Value Set";

	protected static final String[] ITERATION_MODE_STRINGS = new String[] { ITERATION_MODE_FILES,
			ITERATION_MODE_WORKFLOW_VAR, ITERATION_MODE_INDIRECTED_FILES, ITERATION_MODE_VALUE_SET };

	private transient IDataSink fileSetSink = null;

	private transient WorkflowVariableIteratorList disposedIteratorList = null;

	private transient List<String> disposedValueSet = null;

	public ForEachDataSourceActivity() {
	}

	@Override
	public void activate() {
		super.activate();

		if (getVariableIteratorList() == null) {

			WorkflowVariableIteratorList iterators = new WorkflowVariableIteratorList();
			if (iterators.getIterators().size() == 0) {
				WorkflowVariableIterator iterator = new WorkflowVariableIterator(this);
				iterators.addIterator(iterator);
				iterator.activate();
				iterator.getModifier().setDataSourceAndSinkVisible(false);
				getVariableList().addVariable(iterator.getVariable());
				getDataSourceList().addDataSource(iterator.getVariable());
			}
			setVariableIteratorList(iterators);

		}
		if (getIterationMode() == null) {
			// by default, the for loop iterates over variables, not files
			setIterationMode(ITERATION_MODE_WORKFLOW_VAR);
		} else {
			// force update of available variables and GUI elements
			setIterationMode(getIterationMode());
		}
		if (getPropertyValue(PROP_CHUNK_SIZE) == null) {
			setPropertyValue(PROP_CHUNK_SIZE, 1l);
		}
		if (getPropertyValue(PROP_CHUNK_SIZE_KBYTES) == null) {
			setPropertyValue(PROP_CHUNK_SIZE_KBYTES, Boolean.FALSE);
		}
		updateVariables();
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
		for (IDataSink sink : getDataSinkList().getDataSinks()) {
			if (sink instanceof GPEWorkflowFileSetSink) {
				fileSetSink = sink;
			}
		}
		// update variables, otherwise they will be wrong
		updateVariables();
	}

	@Override
	public void afterSerialization() {
		super.afterSerialization();
		if (disposedIteratorList != null) {
			setVariableIteratorList(disposedIteratorList);
		}
	}

	public boolean allowsAddingIterators() {
		return true;
	}

	/**
	 * Returns true iff the for loop is set up to walk over workflow variables.
	 */
	public boolean areIteratorsUsed() {
		Boolean result = (Boolean) getPropertyValue(PROP_ITERATORS_USED);
		if (result == null) {
			return false;
		} else {
			return result;
		}
	}

	@Override
	public void beforeSerialization() {
		super.beforeSerialization();
		if (getVariableIteratorList() != null && getVariableIteratorList().isDisposed()) {
			disposedIteratorList = getVariableIteratorList();
			setVariableIteratorList(null);
		}
	}

	@Override
	public DataSourceActivity getCopy(Set<IFlowElement> toBeCopied, Map<Object, Object> mapOfCopies, int copyFlags) {
		try {
			ForEachDataSourceActivity result = (ForEachDataSourceActivity) super.getCopy(toBeCopied, mapOfCopies,
					copyFlags);
			result.fileSetSink = CloneUtils.getFromMapOrCopy(fileSetSink, toBeCopied, mapOfCopies, copyFlags);
			return result;
		} catch (Exception e) {
			WFActivator.log(IStatus.ERROR, "Could not create copy of the element " + getName(), e);
			return null;
		}
	}

	protected IGridBeanParameter getInputFilesetParameter() {
		for (IGridBeanParameter param : getGridBean().getInputParameters()) {
			if (GridBeanParameterType.FILE_SET.equals(param.getType())) {
				return param;
			}
		}
		return null; // shouldn't happen
	}

	public String[] getIterationModeStrings() {
		return ITERATION_MODE_STRINGS;
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_ITERATION_MODE);
		propertiesToCopy.add(PROP_CHUNK_SIZE);
		propertiesToCopy.add(PROP_CHUNK_SIZE_KBYTES);
		propertiesToCopy.add(PROP_MAX_PARALLEL_TASKS);
		propertiesToCopy.add(PROP_ITERATOR_LIST);
		propertiesToCopy.add(PROP_ITERATORS_USED);
		propertiesToCopy.add(PROP_VALUE_SET);
		return propertiesToCopy;
	}

	public String getIterationMode() {
		String result = (String) getPropertyValue(PROP_ITERATION_MODE);
		if (result == null) {
			return ITERATION_MODE_FILES;
		} else {
			return result;
		}
	}

	@Override
	public QName getType() {
		return TYPE;
	}

	public WorkflowVariableIteratorList getVariableIteratorList() {
		return (WorkflowVariableIteratorList) getPropertyValue(PROP_ITERATOR_LIST);
	}

	public List<String> getValueSet() {
		return (List<String>) getPropertyValue(PROP_VALUE_SET);
	}

	@Override
	public void setPropertyValue(Object key, Object value) {
		if (PROP_CHUNK_SIZE.equals(key)) {
			Long old = (Long) getPropertyValue(PROP_CHUNK_SIZE);
			if (old == null)
				old = 1l;
			Long newValue = (Long) value;
			if (newValue == null)
				newValue = 1l;
			updateOutputFileMultiplicity(old, newValue);
		}
		super.setPropertyValue(key, value);
		// need to do the following after actually setting the value
		if(PROP_CHUNK_SIZE.equals(key)) {
			updateVariables();
		}
	}

	protected void updateOutputFileMultiplicity(long oldValue, long newValue) {
		WorkflowFile wfFile = getOutputFile();

		if (wfFile != null && oldValue != newValue) {
			wfFile.setUsingWildcards(newValue > 1);
		}
	}

	public void setIterationMode(String mode) {
		setPropertyValue(PROP_ITERATION_MODE, mode);
		updateVariables();
	}

	/**
	 * @param mode
	 */
	private void updateVariables() {
		final String mode = getIterationMode();
		if (ITERATION_MODE_WORKFLOW_VAR.equals(mode)) {
			setDataSourceUsed(false);
			setUseIterators(true);
			disposeFileSetSink();
			disposeValueSet();
			setupVariableIterationVariables();
			undisposeWorkflowVariableIteratorList();
		} else if (ITERATION_MODE_VALUE_SET.equals(mode)) {
			setDataSourceUsed(false);
			setUseIterators(false);
			disposeFileSetSink();
			disposeWorkflowVariableIteratorList();
			// removeFileIterationVariables();
			undisposevalueSet();
			setupValueSetVariables();
		} else { // ITERATION_MODE(_INDIRECTED)_FILES
			setDataSourceUsed(true);
			setUseIterators(false);
			disposeWorkflowVariableIteratorList();
			// removeIteratorValueVariables();
			undisposeFileSetSink();
			setupFileIterationVariables();
		}
	}

	/**
	 * 
	 */
	private void setupVariableIterationVariables() {
		WorkflowVariableList wvl = new WorkflowVariableList();
		
		addVariable(wvl, CURRENT_ITERATOR_ID);
		
		setVariableList(wvl);
	}

	/**
	 * 
	 */
	private void setupFileIterationVariables() {
		WorkflowVariableList wvl = new WorkflowVariableList();
		long size = getChunkSize();
		if (size > 1) {
			for (int i = 1; i <= size; ++i) {
				addVariable(wvl, "ChunkedFileIterator_FILENAME_" + Integer.toString(i));
			}
			addVariable(wvl, "ChunkedFileIterator_FILENAME_FORMAT");
			addVariable(wvl, "ChunkedFileIterator_THIS_CHUNK_SIZE");
			addVariable(wvl, "ChunkedFileIterator_IS_CHUNKED");
			addVariable(wvl, "ChunkedFileIterator_ITERATOR_NAME");
		} else {
			addVariable(wvl, getName() + "_ITERATOR_FILENAME");
			addVariable(wvl, getName() + "_ITERATOR");
		}
		setPropertyValue(PROP_DECLARED_VARIABLES, wvl);
	}

	/**
	 * @return
	 */
	private long getChunkSize() {
		Object value = getPropertyValue(PROP_CHUNK_SIZE);
		if (value != null) {
			long size = ((Long) getPropertyValue(PROP_CHUNK_SIZE)).longValue();
			return size > 0 ? size : 1;
		} else {
			return 1;
		}
	}

	/**
	 * 
	 */
	private void addVariable(WorkflowVariableList _wvl, String _name) {
		if (_wvl.getVariableByName(_name) == null) {
			WorkflowVariable variable = new WorkflowVariable(_name, WorkflowVariable.VARIABLE_TYPE_STRING, "", this);
			variable.setId(_name);
			_wvl.addVariable(variable);
		}
	}

	/**
	 * Remove iterator and preserve value set (non-persistent)
	 */
	private void disposeValueSet() {
		// do not overwrite already existing disposed value set
		disposedValueSet = (disposedValueSet != null) ? disposedValueSet : getValueSet();
		setValueSet(null);
	}

	/**
	 * Add iterator and re-initialize preserved value-set
	 */
	private void undisposevalueSet() {
		if (disposedValueSet != null) {
			setValueSet(disposedValueSet);
			disposedValueSet = null;
		}
	}

	/**
	 * 
	 */
	private void setupValueSetVariables() {
		WorkflowVariableList wvl = new WorkflowVariableList();

		WorkflowVariable namedIterator = new WorkflowVariable(getName() + ITERATOR_VALUE_SUFFIX,
				WorkflowVariable.VARIABLE_TYPE_STRING, "", this);
		namedIterator.setId(NAMED_ITERATOR_ID);
		wvl.addVariable(namedIterator);

		addVariable(wvl, CURRENT_ITERATOR_ID);

		setVariableList(wvl);
	}

	/**
	 * 
	 */
	private void disposeFileSetSink() {
		if (fileSetSink != null) {
			if (!fileSetSink.isDisposed()) {
				fileSetSink.dispose();
			}
		}
	}

	/**
	 * 
	 */
	private void undisposeFileSetSink() {
		if (fileSetSink == null) {
			fileSetSink = new GPEWorkflowFileSetSink(this, getInputFilesetParameter());
			getDataSinkList().addDataSink(fileSetSink);
		} else if (fileSetSink.isDisposed()) {
			// TODO: is it ok to add a disposed sink?
			getDataSinkList().addDataSink(fileSetSink);
			// we can't undo dispose before, cause then the StageInList will
			// create a new fileset sink!!
			fileSetSink.undoDispose();
			firePropertyChange(PROP_DATA_SINK_LIST, getDataSinkList(), getDataSinkList());
		}
	}

	/**
	 * 
	 */
	private void disposeWorkflowVariableIteratorList() {
		if (getVariableIteratorList() != null && !getVariableIteratorList().isDisposed()) {
			// dispose in order to get rid of any references
			getVariableIteratorList().dispose();
			for (WorkflowVariableIterator it : getVariableIteratorList().getIterators()) {
				getVariableList().removeVariable(it.getVariable());
			}
			setPropertyValue(PROP_DECLARED_VARIABLES, getVariableList()); // inform
			// listeners
		}
	}

	/**
	 * 
	 */
	private void undisposeWorkflowVariableIteratorList() {
		if (getVariableIteratorList().isDisposed()) {
			if (getVariableIteratorList().isDisposed()) {
				getVariableIteratorList().undoDispose();
			}
			for (WorkflowVariableIterator it : getVariableIteratorList().getIterators()) {
				getVariableList().addVariable(it.getVariable());
			}
			addVariable(getVariableList(), CURRENT_ITERATOR_ID);
			setPropertyValue(PROP_DECLARED_VARIABLES, getVariableList()); // inform
			// listeners
		}
	}

	/**
	 * @see org.chemomentum.rcp.wfeditor.model.DataSourceActivity#setName(java.lang.String)
	 */
	@Override
	public boolean setName(String name) {
		String oldName = getName();
		if (super.setName(name)) {
			String oldVsIterName = oldName + ITERATOR_VALUE_SUFFIX;
			String newVsIterName = name + ITERATOR_VALUE_SUFFIX;
			WorkflowVariable genericIterator = null;
			if ((genericIterator = getVariableList().getVariableById(NAMED_ITERATOR_ID)) != null) {
				genericIterator.setName(newVsIterName);
			} else {
				genericIterator = new WorkflowVariable(newVsIterName, WorkflowVariable.VARIABLE_TYPE_STRING, "", this);
				genericIterator.setId(NAMED_ITERATOR_ID);
				getVariableList().addVariable(genericIterator);
			}
			setPropertyValue(PROP_DECLARED_VARIABLES, getVariableList());
			return true;
		}
		return false;
	}

	public void setUseIterators(boolean active) {
		setPropertyValue(PROP_ITERATORS_USED, active);
	}

	public void setVariableIteratorList(WorkflowVariableIteratorList list) {
		setPropertyValue(PROP_ITERATOR_LIST, list);
	}

	public void setValueSet(List<String> values) {
		setPropertyValue(PROP_VALUE_SET, values);
	}

	private Object writeReplace() {
		return this;
	}

}
