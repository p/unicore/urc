package org.chemomentum.rcp.wfeditor.ui;

import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.intel.gpe.gridbeans.parameters.IFileParameterValue;

public class ChooseFileDialog extends MessageDialog {

	List<IFileParameterValue> files;
	org.eclipse.swt.widgets.List fileList;
	IFileParameterValue selectedFile;

	public ChooseFileDialog(List<IFileParameterValue> files, Shell parentShell,
			String dialogTitle, String dialogMessage) {
		super(parentShell, dialogTitle, null, dialogMessage,
				MessageDialog.QUESTION_WITH_CANCEL, new String[] {
						IDialogConstants.OK_LABEL,
						IDialogConstants.CANCEL_LABEL }, 0);
		this.files = files;

	}

	@Override
	protected Control createCustomArea(Composite parent) {
		Label empty = new Label(parent.getParent(), SWT.NONE);
		empty.moveAbove(parent);

		GridData data = new GridData(GridData.FILL_BOTH);
		data.horizontalSpan = 1;
		parent.setLayoutData(data);

		if (files == null || files.size() == 0) {
			return null;
		}
		fileList = new org.eclipse.swt.widgets.List(parent, SWT.SINGLE
				| SWT.BORDER);
		fileList.setBackground(parent.getDisplay().getSystemColor(
				SWT.COLOR_WHITE));

		String[] items = new String[files.size()];
		for (int i = 0; i < items.length; i++) {
			items[i] = files.get(i).getDisplayedString();
		}
		fileList.setItems(items);
		fileList.select(0);
		selectedFile = files.get(0);
		fileList.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
				int index = fileList.getSelectionIndex();
				if (index >= 0 && index < files.size()) {
					selectedFile = files.get(index);
				} else {
					selectedFile = null;
				}
			}

			public void widgetSelected(SelectionEvent arg0) {
				int index = fileList.getSelectionIndex();
				if (index >= 0 && index < files.size()) {
					selectedFile = files.get(index);
				} else {
					selectedFile = null;
				}
			}
		});

		GridData gd = new GridData(SWT.LEFT, SWT.CENTER, true, true);
		gd.widthHint = IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH;
		fileList.setLayoutData(gd);

		return fileList;
	}

	public IFileParameterValue getSelectedFile() {
		return selectedFile;
	}

}
