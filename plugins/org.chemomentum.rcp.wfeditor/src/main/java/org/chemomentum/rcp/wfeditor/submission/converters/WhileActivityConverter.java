/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.submission.converters;

import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.C9MWFEditorConstants;
import org.chemomentum.rcp.wfeditor.utils.SimpleWFUtil;
import org.chemomentum.simpleworkflow.api.IElement;
import org.chemomentum.simpleworkflow.api.SubWorkflowElement;
import org.chemomentum.simpleworkflow.api.WhileLoopElement;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityDocument.Activity;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityType;
import org.chemomentum.simpleworkflow.xmlbeans.DeclareVariableDocument.DeclareVariable;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.WhileActivity;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.submission.IConverter;

/**
 * @author demuth
 * 
 */
public class WhileActivityConverter extends LoopActivityConverter implements
		IConverter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.IConverter#canConvert(javax.xml
	 * .namespace.QName, javax.xml.namespace.QName)
	 */
	public boolean canConvert(IAdaptable submissionService, QName modelType,
			QName wfLanguageType) {
		return getModelType().equals(modelType)
				&& C9mRCPCommonConstants.C9M_DEFAULT_WF_DIALECT
						.equals(wfLanguageType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.IConverter#convert(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram)
	 */
	public IElement convert(IAdaptable submissionService, String workflowId,
			WorkflowDiagram model, IFlowElement element,
			Map<String, Object> additionalParams, IProgressMonitor monitor)
			throws Exception {

		WhileActivity whileActivity = (WhileActivity) element;

		int scale = 1000;
		monitor.beginTask("", 100 * scale);
		try {
			SubWorkflowElement graph = (SubWorkflowElement) additionalParams
					.get(C9MWFEditorConstants.PARAM_SUBGRAPH);

			WhileLoopElement sub = new WhileLoopElement(
					whileActivity.getName(), graph);

			// declare iteration counter variable
			WorkflowVariable var = whileActivity.getIterationCounter();
			String iteratorName = var.getName();
			sub.setIteratorName(iteratorName);
			DeclareVariable variable = DeclareVariable.Factory.newInstance();
			variable.setName(var.getName());
			variable.setType(SimpleWFUtil.convertVariableType(var.getType()));
			String initial = var.getInitialValue();
			String wfEngineVersion = (String) additionalParams
					.get(C9MWFEditorConstants.PARAM_WF_ENGINE_VERSION);
			String minimumVersion = "2.1.2";
			if (minimumVersion.compareTo(wfEngineVersion) > 0) {
				if (WorkflowVariable.VARIABLE_TYPE_STRING.equals(var.getType())) {
					initial = "\"" + initial + "\"";
				}
			}
			variable.setInitialValue(initial);
			sub.addVariableDeclaration(variable);

			StructuredActivity body = whileActivity.getBody();
			if (body.getChildren().size() == 0) {
				throw new Exception(
						"Invalid workflow: Found While-Loop with empty body. Cancelling.");
			}

			SubWorkflowElement bodySub = convertBody(sub, submissionService,
					workflowId, model, whileActivity, element,
					additionalParams, monitor);

			sub.setCondition(whileActivity.getCondition()
					.getInternalExpression());

			Activity join = Activity.Factory.newInstance();
			join.setType(ActivityType.SYNCHRONIZE);
			join.setId(whileActivity.getName() + "-JOIN2");

			// create transitions from each end element of the loop body to
			// a joining activity
			List<String> endElements = bodySub.getEndElements();
			bodySub.addActivity(join);
			for (String id : endElements) {
				bodySub.addTransition(id, join.getId());
			}

			// add activity that increments the iteration counter
			Activity incrementer = SimpleWFUtil.createVariableModifierActivity(
					whileActivity.getName() + "-INCREMENT",
					whileActivity.getIterationIncrementer());
			bodySub.addActivity(incrementer);
			bodySub.addTransition(join.getId(), incrementer.getId());

			additionalParams.put(C9MWFEditorConstants.PARAM_SUBGRAPH, graph);

			if (monitor.isCanceled()) {
				return null;
			}
			return sub;

		} finally {
			monitor.done();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.submission.IConverter#getModelType()
	 */
	public QName getModelType() {
		return WhileActivity.TYPE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.submission.IConverter#getTargetType()
	 */
	public QName getTargetType() {
		return C9mRCPCommonConstants.C9M_DEFAULT_WF_DIALECT;
	}

}
