/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.properties;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.chemomentum.rcp.wfeditor.model.DataSourceActivity;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import de.fzj.unicore.rcp.common.utils.ValidityChangeEvent;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageListFactory;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in.StageInList;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in.StageInTableViewer;
import de.fzj.unicore.rcp.wfeditor.properties.AbstractSection;

/**
 * @author demuth
 * 
 */
public class DataSourceSection extends AbstractSection {

	protected Composite fileParent;
	protected StageInTableViewer stageInTableViewer;
	protected PropertyChangeListener dataSourceActiveListener;

	protected void addListeners() {

		if (getActivity() != null && dataSourceActiveListener != null) {
			getActivity().addPropertyChangeListener(dataSourceActiveListener);
		}
	}

	public boolean checkInputValid() {
		return true;

	}

	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);

		FormData data;
		Composite flatForm = getWidgetFactory().createFlatFormComposite(parent);

		Group filesGroup = getWidgetFactory().createGroup(flatForm, "Files");
		data = new FormData();
		data.left = new FormAttachment(0, ITabbedPropertyConstants.HSPACE);
		data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(0, ITabbedPropertyConstants.VSPACE);
		data.bottom = new FormAttachment(100, -ITabbedPropertyConstants.VSPACE);
		filesGroup.setLayoutData(data);
		filesGroup.setLayout(new FormLayout());
		controls.add(filesGroup);

		fileParent = getWidgetFactory().createComposite(filesGroup);

		data = new FormData();
		data.left = new FormAttachment(0, ITabbedPropertyConstants.HSPACE);
		data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(0, ITabbedPropertyConstants.VSPACE);
		data.bottom = new FormAttachment(100, -ITabbedPropertyConstants.VSPACE);

		fileParent.setLayoutData(data);
		GridLayout gl = new GridLayout(1, false);
		fileParent.setLayout(gl);
		createListeners();
	}

	protected void createListeners() {
		dataSourceActiveListener = new PropertyChangeListener() {

			public void propertyChange(PropertyChangeEvent evt) {
				if (!DataSourceActivity.PROP_DATA_SOURCE_USED.equals(evt.getPropertyName())) {
					return;
				}
				setFileGroupEnabled((Boolean) evt.getNewValue());
			}

		};

	}

	@Override
	public void dispose() {
		removeListeners();
	}

	private DataSourceActivity getActivity() {
		return (DataSourceActivity) element;
	}

	protected void init() {
		DataSourceActivity activity = getActivity();
		if (activity == null) {
			return;
		}

		StageInList stageIns;
		stageIns = StageListFactory.createStageInList(activity);

		// create the table viewer that shows the files leave out the column for
		// the target file name as the files are not staged into the working
		// directory of the activity but mapped onto a workflow file that can be
		// imported by other activities
		String[] usedColumns = new String[] { StageInTableViewer.ID,
				StageInTableViewer.SOURCE_TYPE, StageInTableViewer.SOURCE_FILE };
		if(stageInTableViewer != null)
		{
			Control c = stageInTableViewer.getControl();
			controls.remove(c);
			c.dispose();
			stageInTableViewer = null;
		}
		stageInTableViewer = new StageInTableViewer(fileParent, stageIns,
				getActivity().getDiagram().getCommandStack(), usedColumns);
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.minimumWidth = 200;
		gd.minimumHeight = 100;

		stageInTableViewer.getControl().setLayoutData(gd);

		controls.add(stageInTableViewer.getControl());


	}

	protected void removeListeners() {
		if (getActivity() != null && dataSourceActiveListener != null) {
			getActivity()
					.removePropertyChangeListener(dataSourceActiveListener);
		}
	}

	protected void setFileGroupEnabled(boolean enabled) {

		stageInTableViewer.getControl().setEnabled(enabled);
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		removeListeners();
		DataSourceActivity old = getActivity();
		super.setInput(part, selection);
		addListeners();
		if (old != null && old.equals(getActivity())) {
			return;
		}
		init();
		updateEnabled();
	}

	@Override
	protected void updateEnabled() {
		super.updateEnabled();
		setFileGroupEnabled(getActivity().isDataSourceUsed() && canEditModel());
	}

}
