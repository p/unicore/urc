/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.ui;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.common.C9mRCPCommonConstants;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.IGridBeanRegistryListener;
import de.fzj.unicore.rcp.wfeditor.model.DiagramContext;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableList;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;

/**
 * @author demuth
 * 
 */
public class C9MWFEditor extends WFEditor implements IGridBeanRegistryListener {
	
	private static final String CURRENT_TOTAL_ITERATOR = "CURRENT_TOTAL_ITERATOR";
	private static final String CURRENT_ITERATOR_INDEX = "CURRENT_ITERATOR_INDEX";

	public C9MWFEditor() {
		super();
		GPEActivator.getDefault().getGridBeanRegistry().addListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.ui.WFEditor#createNewDiagram()
	 */
	@Override
	protected WorkflowDiagram createNewDiagram() {
		WorkflowDiagram diagram = new WorkflowDiagram();
		DiagramContext context = new DiagramContext();
		context.setModelType(C9mRCPCommonConstants.C9M_QNAME);
		List<QName> appDomains = new ArrayList<QName>();
		appDomains.add(C9mRCPCommonConstants.DOMAIN_ALL);
		context.setApplicationDomains(appDomains);
		context.setDefaultWorkflowSystem(C9mRCPCommonConstants.C9M_WORKFLOW_SYSTEM_TYPE);
		diagram.setContext(context);
		addSpecialVars(diagram);
		return diagram;
	}

	protected void deserializeDiagram()
	{
		super.deserializeDiagram();
		WorkflowDiagram diagram = getDiagram();

		boolean specialVarsPresent = diagram.getVariable(CURRENT_TOTAL_ITERATOR) != null;
		
		if(!specialVarsPresent)
		{
			// add them
			addSpecialVars(diagram);
		}
	}

	/**
	 * @param diagram
	 * @param currentTotalIterator
	 */
	private void addSpecialVars(WorkflowDiagram diagram) {
		WorkflowVariableList list = diagram.getVariableList();
		WorkflowVariable curTotIt = new WorkflowVariable(CURRENT_TOTAL_ITERATOR,WorkflowVariable.VARIABLE_TYPE_STRING, "0", diagram);
		list.addVariable(curTotIt);
		curTotIt.activate();
		WorkflowVariable curIterIdx = new WorkflowVariable(CURRENT_ITERATOR_INDEX,WorkflowVariable.VARIABLE_TYPE_INTEGER, "0", diagram);
		list.addVariable(curIterIdx);
		curIterIdx.activate();
		diagram.setDirty(true);
	}

	@Override
	public void gridBeansChanged(List<GridBeanInfo> oldValue,
			List<GridBeanInfo> newValue) {
		paletteRoot = createPaletteRoot();
		getEditDomain().setPaletteRoot(paletteRoot);
	}
	
	public void dispose() {
		try {
			GPEActivator.getDefault().getGridBeanRegistry().removeListener(this);
		} catch (Exception e) {
		}
		
		super.dispose();
	}

}
