/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.monitoring;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.monitoring.IMonitorer;

/**
 * @author demuth
 * 
 */
public class C9MMonitorer implements IMonitorer {

	private 

	ScheduledExecutorService executor;

	Map<String, StatusUpdater> runningUpdaters = new HashMap<String, StatusUpdater>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.monitoring.IMonitorer#canMonitor(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram,
	 * org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public boolean canMonitor(WorkflowDiagram model, NodePath serviceAddress) {
		// QName type = Utils.InterfaceNameFromEPR(serviceEpr);
		// return WorkflowManagement.PORTTYPE.equals(type);
		return true;
	}

	String getKey(NodePath address) {
		return address.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.monitoring.IMonitorer#monitor(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram,
	 * org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public void monitor(WorkflowDiagram model, NodePath monitoringService)
	throws Exception {
		synchronized (runningUpdaters) {

			if (executor == null) {
				executor = Executors.newSingleThreadScheduledExecutor();
			}

			Map<String, IActivity> activities = model.getActivities();
			Map<String, IActivity> monitoredActivities = new HashMap<String, IActivity>();
			for (IActivity act : activities.values()) {
				monitoredActivities.put(act.getName(), act);
			}
			StatusUpdater statusUpdater = new StatusUpdater(this, model,
					monitoredActivities, monitoringService);
			schedule(statusUpdater);
			runningUpdaters.put(getKey(monitoringService), statusUpdater);
		}

	}

	StatusUpdater removeUpdater(String key) {
		synchronized (runningUpdaters) {
			StatusUpdater updater = runningUpdaters.remove(key);

			if (runningUpdaters.size() == 0) {

				try {
					if (executor != null && !executor.isShutdown()) {
						executor.shutdown();
					}
				} catch (Exception e) {

				}
				executor = null;
			}
			return updater;
		}
	}

	void schedule(StatusUpdater updater) {
		synchronized (runningUpdaters) {
			int interval = WFActivator.getDefault().getPreferenceStore()
			.getInt(WFConstants.P_WORKFLOW_POLLING_INTERVAL);
			if (executor == null) {
				executor = Executors.newSingleThreadScheduledExecutor();
			}
			executor.schedule(updater, interval, TimeUnit.SECONDS);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.monitoring.IMonitorer#stopMonitoring(de.fzj
	 * .unicore.rcp.wfeditor.model.WorkflowDiagram,
	 * org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public void stopMonitoring(WorkflowDiagram model, NodePath monitoringService)
	throws Exception {
		synchronized (runningUpdaters) {
			StatusUpdater updater = runningUpdaters
			.get(getKey(monitoringService));
			if (updater != null) {
				updater.stopMonitoring();
			}

		}
	}
}
