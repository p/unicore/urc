/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.celleditors;

import org.chemomentum.rcp.wfeditor.model.C9MWFFileAddress;
import org.chemomentum.rcp.wfeditor.utils.GridBeanUtils;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.gridbeans.parameters.IFileParameterValue;

/**
 * @author demuth
 * 
 */
public class StageOutTypeWorkflowCellEditor extends TextCellEditor {

	private ModifyListener modifyListener;
	private IFileParameterValue value;
	IFileParameterValue result;

	public StageOutTypeWorkflowCellEditor(Composite parent, IAdaptable activity) {
		super(parent);
	}

	@Override
	protected Object doGetValue() {
		if (result == null) {
			try {
				result = value.clone();
			} catch (Exception e) {
				result = value;
			}
			String displayed = text.getText();
			String parent = GridBeanUtils.createParentWFFileString();
			String relative = displayed;
			result.setTarget(new C9MWFFileAddress(displayed, parent, relative));
		}
		return result;
	}

	@Override
	protected void doSetValue(Object value) {
		Assert.isTrue(text != null && (value instanceof IFileParameterValue));
		this.value = (IFileParameterValue) value;

		text.removeModifyListener(getModifyListener());
		text.setText(this.value.getTarget().getDisplayedString());
		text.addModifyListener(getModifyListener());
	}

	/**
	 * Return the modify listener.
	 */
	protected ModifyListener getModifyListener() {
		if (modifyListener == null) {
			modifyListener = new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					editOccured(e);
				}
			};
		}
		return modifyListener;
	}
}
