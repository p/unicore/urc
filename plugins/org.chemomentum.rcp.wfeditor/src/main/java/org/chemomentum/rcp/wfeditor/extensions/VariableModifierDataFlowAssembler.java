package org.chemomentum.rcp.wfeditor.extensions;

import java.util.HashSet;
import java.util.Set;

import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.chemomentum.rcp.wfeditor.model.VariableModifierLinker;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;

import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.IEnvironmentVariableParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowVariableSink;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.IDataFlowAssembler;
import de.fzj.unicore.rcp.wfeditor.model.commands.DataFlowAssemblyCommand;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.variables.VariableModifierDataSource;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifier;

public class VariableModifierDataFlowAssembler implements IDataFlowAssembler {

	protected String flowAllowed(IDataSource source, IDataSink sink) {
		if (source.getOutgoingDataTypes() != null
				&& sink.getIncomingDataTypes() != null
				&& !source.getOutgoingDataTypes().isEmpty()
				&& !sink.getIncomingDataTypes().isEmpty()) {
			Set<String> validTypes = new HashSet<String>(
					sink.getIncomingDataTypes());
			validTypes.retainAll(source.getOutgoingDataTypes());
			if (validTypes.isEmpty()) {
				return "The data type of the selected workflow variable does not match the data type of this application parameter.";
			}
		}
		VariableModifierDataSource dataSource = (VariableModifierDataSource) source;
		// iterate over all sinks and check whether one of them has been
		// connected to the modified variable already
		// in this case, we MUST NOT create the flow: we cannot feed both the
		// unmodified and modified variable
		// value into the sink
		for (IDataSink peerSink : sink.getFlowElement().getDataSinkList()
				.getDataSinks()) {
			if (sink instanceof GPEWorkflowVariableSink) {
				for (IDataFlow flow : peerSink.getIncomingFlows()) {
					IDataSource potentialVar = flow.getDataSource();
					if (potentialVar instanceof WorkflowVariable
							&& potentialVar.equals(dataSource
									.getModifiedVariable())) {
						return "Cannot use modified and unmodified variable value as input parameters for the same job.";
					}
				}
			}
		}
		return null;
	}

	public DataFlowAssemblyCommand getAssemblyCommand(final IDataSource source,
			final IDataSink sink) throws Exception {

		return new DataFlowAssemblyCommand() {
			IDataFlow oldLinker;
			IEnvironmentVariableParameterValue oldValue;
			IGridBeanParameter param;
			IAdaptable activity;

			@Override
			protected IDataFlow assembleDataFlow() {

				String error = flowAllowed(source, sink);
				if (error != null) {
					WFEditorActivator.log(IStatus.ERROR,
							"Unable to create data flow: " + error);
				}
				VariableModifierDataSource dataSource = (VariableModifierDataSource) source;
				WorkflowVariableModifier modifier = dataSource.getModifier();

				if (sink instanceof GPEWorkflowVariableSink) {
					GPEWorkflowVariableSink variableSink = (GPEWorkflowVariableSink) sink;
					activity = variableSink.getFlowElement();
					IGridBean gb = (IGridBean) activity
							.getAdapter(IGridBean.class);
					param = variableSink.getParameter();
					oldValue = (IEnvironmentVariableParameterValue) gb
							.get(param.getName());

					oldLinker = getOldLinker(variableSink);
					if (oldLinker != null) {
						oldLinker.disconnect();
					}

					VariableModifierLinker linker = new VariableModifierLinker(
							modifier, variableSink);
					linker.connect();
					gb.set(param.getName(),
							linker.getCurrentValue(oldValue, modifier));
					return linker;
				}

				return null;

			}

			protected IDataFlow getOldLinker(GPEWorkflowVariableSink sink) {
				if (sink.getIncomingFlows() != null
						&& sink.getIncomingFlows().length > 0) {
					return sink.getIncomingFlows()[0];
				}
				return null;
			}

			@Override
			public void redo() {
				if (oldLinker != null) {
					oldLinker.disconnect();
				}
				super.redo();

			}

			@Override
			public void undo() {
				super.undo();
				if (oldLinker != null) {
					oldLinker.reconnect();
				}
				IGridBean gb = (IGridBean) activity.getAdapter(IGridBean.class);
				gb.set(param.getName(), oldValue);
			}

		};

	}
}
