package org.chemomentum.rcp.wfeditor.extensions;

import java.util.HashSet;
import java.util.Set;

import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.chemomentum.rcp.wfeditor.model.WFFileLinker;
import org.chemomentum.rcp.wfeditor.utils.GridBeanUtils;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;

import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileSetParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.util.sets.Pair;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSetSink;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSink;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.IDataFlowAssembler;
import de.fzj.unicore.rcp.wfeditor.model.commands.DataFlowAssemblyCommand;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;

public class WorkflowFileDataFlowAssembler implements IDataFlowAssembler {

	public DataFlowAssemblyCommand getAssemblyCommand(final IDataSource source,
			final IDataSink sink) throws Exception {

		WorkflowFile file = (WorkflowFile) source;
		if (sink instanceof GPEWorkflowFileSink) {
			if (file.isUsingWildcards()) {
				throw new Exception(
						"Workflow files with wildcards cannot be connected to file sinks accepting single files.");
			}
		}

		return new DataFlowAssemblyCommand() {
			WFFileLinker oldLinker;

			@Override
			protected IDataFlow assembleDataFlow() {

				if (source.getOutgoingDataTypes() != null
						&& sink.getIncomingDataTypes() != null
						&& !source.getOutgoingDataTypes().isEmpty()
						&& !sink.getIncomingDataTypes().isEmpty()) {
					Set<String> validTypes = new HashSet<String>(
							sink.getIncomingDataTypes());
					validTypes.retainAll(source.getOutgoingDataTypes());
					if (validTypes.isEmpty()) {
						WFEditorActivator
								.log(IStatus.ERROR,
										"The content type of the selected workflow file does not match the content type expected by this file sink.");
						return null;
					}
				}

				WorkflowFile file = (WorkflowFile) source;

				if (sink instanceof GPEWorkflowFileSink) {
					GPEWorkflowFileSink fileSink = (GPEWorkflowFileSink) sink;
					IAdaptable activity = fileSink.getFlowElement();
					IGridBean gb = (IGridBean) activity
							.getAdapter(IGridBean.class);
					IGridBeanParameter param = fileSink.getParameter();
					oldLinker = GridBeanUtils.getOldLinker(fileSink);
					if (oldLinker == null) {
						IFileParameterValue oldValue = (IFileParameterValue) gb
								.get(param.getName());
						IStageTypeExtensionPoint ext = GPEActivator
								.getDefault()
								.getStageInTypeRegistry()
								.getDefiningExtension(
										C9mRCPCommonConstants.WFFILE_QNAME);
						oldValue = ext.attachToParameterValue(activity,
								oldValue);
						gb.set(param.getName(), oldValue);
					}
					Pair<IFileParameterValue, WFFileLinker> pair = GridBeanUtils
							.linkToWorkflowFile(file, fileSink, null);

					IFileParameterValue newValue = pair.getM1();
					if (newValue != null) {
						gb.set(param.getName(), newValue);
					} else {
						oldLinker = null; // linker hasn't been disconnected =>
											// no need to reconnect when we are
											// being undone
					}

					return pair.getM2();
				} else if (sink instanceof GPEWorkflowFileSetSink) {
					GPEWorkflowFileSetSink fileSink = (GPEWorkflowFileSetSink) sink;
					IAdaptable activity = fileSink.getFlowElement();
					IGridBean gb = (IGridBean) activity
							.getAdapter(IGridBean.class);
					IGridBeanParameter param = fileSink.getParameter();
					Pair<IFileSetParameterValue, WFFileLinker> pair = GridBeanUtils
							.linkToWorkflowFileSet(file, fileSink, null, null);
					IFileSetParameterValue newValue = pair.getM1();
					if (newValue != null) {
						gb.set(param.getName(), newValue);
					}
					return pair.getM2();
				}
				return null;

			}

			@Override
			public void redo() {
				// if(oldLinker != null) oldLinker.disconnect();
				super.redo();

			}

			@Override
			public void undo() {
				super.undo();
				if (oldLinker != null) {
					oldLinker.reconnect();
				}
			}

		};

	}
}
