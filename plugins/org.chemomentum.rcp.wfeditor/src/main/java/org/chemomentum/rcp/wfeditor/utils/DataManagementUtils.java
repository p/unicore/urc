package org.chemomentum.rcp.wfeditor.utils;

import java.net.URI;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageFactoryNode;
import de.fzj.unicore.uas.StorageFactory;
import de.fzj.unicore.uas.StorageManagement;
import de.fzj.unicore.uas.client.StorageClient;
import de.fzj.unicore.wsrflite.xmlbeans.client.RegistryClient;
import eu.unicore.util.httpclient.IClientConfiguration;

public class DataManagementUtils {

	public static StorageClient createWorkflowWorkingDir(
			StorageFactoryNode node, String workflowName, Calendar timeToLive,
			IProgressMonitor progress) throws Exception {
		String name = "working directory of "+workflowName;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(StorageFactoryNode.PARAM_NAME, name);
		StorageClient result = node.createStorage(timeToLive, params, progress);
		Node child = NodeFactory.createNode(result.getEPR());
		try {
			child.refresh(1, false, null);
		} finally {
			child.dispose();
		}
		return result;

	}

	public static StorageClient determineWorkflowWorkingDir(
			EndpointReferenceType registryEpr, String workflowName,
			String workflowId, Calendar timeToLive, IProgressMonitor progress)
			throws Exception {
		progress = ProgressUtils.getNonNullMonitorFor(progress);
		progress.beginTask("looking for storages for workflow inputs", 2);
		try {
			String uri = registryEpr.getAddress().getStringValue();
			IClientConfiguration sp = ServiceBrowserActivator.getDefault()
					.getUASSecProps(new URI(uri)); // do not use the epr here,
													// as it might not be
													// complete
			RegistryClient regClient = new RegistryClient(uri, registryEpr, sp);
			List<EndpointReferenceType> sfEprs = regClient
					.listServices(StorageFactory.SMF_PORT);
			progress.worked(1);
			if (sfEprs.size() > 0) {
				// TODO make this more configurable

				for (int i = 0; i < sfEprs.size(); i++) {
					EndpointReferenceType sfEpr = sfEprs.get(i);
					StorageFactoryNode node = (StorageFactoryNode) NodeFactory
							.createNode(sfEpr);
					try {
						int state = node.getState();
						if (node.getData().getParents().size() == 0) {
							// this means that the registry node contains new
							// entries that we haven't seen yet
							// we must make sure that the StorageFactory node
							// can be seen in the Grid Browser
							// otherwise we couldn't use the right security
							// properties for accessing it
							// TODO use the new methods in NodeFactory 6.3.0 for
							// revealing the node ?
							RegistryNode regNode = (RegistryNode) NodeFactory
									.createNode(registryEpr);
							try {
								regNode.refresh(1, false, null);
							} finally {
								regNode.dispose();
							}
						}
						if (state == Node.STATE_NEW) {
							node.refresh(0, false, null);
						}
						if (state < Node.STATE_FAILED) {
							try {

								return createWorkflowWorkingDir(node,
										workflowName, timeToLive, null);
							} catch (Exception e) {
								if (i < sfEprs.size() - 1) {
									continue;
								} else {
									throw e;
								}
							}

						}
					} finally {
						node.dispose();
					}
				}

			}
			// when we made it here, we haven't found a valid storage factory =>
			// fallback to global storage
			List<EndpointReferenceType> storageEprs = regClient
					.listServices(StorageManagement.SMS_PORT);
			progress.worked(1);
			NodePath parentPath = ServiceBrowserActivator.getDefault()
					.getGridNode().getPathToRoot()
					.append(new URI(regClient.getUrl()));
			for (EndpointReferenceType storageEpr : storageEprs) {

				Node storage = NodeFactory.revealNode(parentPath
						.append(new URI(storageEpr.getAddress()
								.getStringValue())), progress);
				try {
					if (storage.getState() == Node.STATE_NEW) {
						storage.refresh(0, false, null);
					}
					if (storage.getState() < Node.STATE_FAILED) {
						sp = ServiceBrowserActivator.getDefault()
								.getUASSecProps(storageEpr);
						StorageClient sc = new StorageClient(storageEpr, sp);
						sc.listDirectory("/"); // test storage
						return sc;
					}
				} catch (Exception e) {
					// do nothing
				}

			}

			throw new Exception(
					"Unable to create or find a storage for uploading input files!");
		} finally {
			progress.done();
		}
	}

}
