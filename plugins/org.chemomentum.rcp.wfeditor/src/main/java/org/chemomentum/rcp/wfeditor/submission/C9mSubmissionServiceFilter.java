/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.submission;

import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.servicebrowser.nodes.WorkflowFactoryNode;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.Viewer;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.submission.ConverterRegistry;
import de.fzj.unicore.rcp.wfeditor.submission.SubmissionServiceFilter;
import de.fzj.unicore.uas.util.Pair;

/**
 * @author demuth
 * 
 */
public class C9mSubmissionServiceFilter extends SubmissionServiceFilter {

	class WorkflowFactoryNodeRefresher extends BackgroundJob {
		Queue<Pair<Viewer, Node>> nodesToRefresh = new ConcurrentLinkedQueue<Pair<Viewer, Node>>();

		public WorkflowFactoryNodeRefresher(String name) {
			super(name);

		}

		@Override
		public IStatus run(IProgressMonitor monitor) {
			monitor.beginTask("refreshing", IProgressMonitor.UNKNOWN);
			try {

				Set<Viewer> viewersToRefresh = new HashSet<Viewer>();
				while (!nodesToRefresh.isEmpty()) {
					if (monitor.isCanceled()) {
						break;
					}
					Pair<Viewer, Node> next = nodesToRefresh.poll();
					SubProgressMonitor sub = new SubProgressMonitor(monitor,
							IProgressMonitor.UNKNOWN);
					next.getM2().refresh(1, false, sub);
					viewersToRefresh.add(next.getM1());
				}
				for (Viewer viewer : viewersToRefresh) {
					viewer.refresh();
				}
			} finally {
				monitor.done();
			}
			return Status.OK_STATUS;
		}
	}

	private Set<QName> dialects;

	private WorkflowFactoryNodeRefresher updateNodesJob = new WorkflowFactoryNodeRefresher(
			"refreshing cached data about available workflow engines");

	private Set<NodePath> updatedNodes = new HashSet<NodePath>();

	/**
	 * Filter out all submission services that do not know at least one of the
	 * dialects to which the workflow can be converted.
	 */
	public C9mSubmissionServiceFilter() {
		super("submissionServiceFilter");
		setActive(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers
	 * .Viewer, java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean select(final Viewer viewer, Object parentElement,
			Object element) {
		if (element instanceof GridNode) {
			return true;
		}
		
		if (element instanceof WorkflowFactoryNode) {
			final WorkflowFactoryNode node = (WorkflowFactoryNode) element;
			if (DialectMatcher.matchSupportedDialects(node, dialects).size() > 0) {
				return true;
			} else {
				if (!node.hasCachedResourceProperties()
						&& !updatedNodes.contains(node.getPathToRoot())) {
					// refresh cached data about worklow engines in the
					// background
					updatedNodes.add(node.getPathToRoot());
					updateNodesJob.nodesToRefresh.add(new Pair<Viewer, Node>(
							viewer, node));
					if (updateNodesJob.getState() == Job.NONE) {
						updateNodesJob.schedule(1000);
					}

				}
				return false;
			}

		}
		else if (element instanceof Node) {
			Node n = (Node) element;
			Queue<Node> toCheck = new ConcurrentLinkedQueue<Node>();
			toCheck.addAll(n.getChildren());
			while(!toCheck.isEmpty())
			{
				Node next = toCheck.poll();
				if(next instanceof WorkflowFactoryNode) return true;
			}
		}
		return false;
	}

	@Override
	public void setWorkflowDiagram(WorkflowDiagram diagram) {
		super.setWorkflowDiagram(diagram);
		QName modelType = diagram.getContext().getModelType();
		ConverterRegistry converterRegistry = WFActivator.getDefault()
				.getConverterRegistry();
		this.dialects = converterRegistry.getPossibleTargets(modelType);
	}

}
