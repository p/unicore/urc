package org.chemomentum.rcp.wfeditor.extensions;

import java.util.ArrayList;
import java.util.List;

import org.chemomentum.rcp.wfeditor.submission.converters.C9MStandardWFConverter;
import org.chemomentum.rcp.wfeditor.submission.converters.ContainerActivityConverter;
import org.chemomentum.rcp.wfeditor.submission.converters.ForEachActivityConverter;
import org.chemomentum.rcp.wfeditor.submission.converters.GridBeanActivityConverter;
import org.chemomentum.rcp.wfeditor.submission.converters.HoldActivityConverter;
import org.chemomentum.rcp.wfeditor.submission.converters.IfElseActivityConverter;
import org.chemomentum.rcp.wfeditor.submission.converters.RepeatUntilActivityConverter;
import org.chemomentum.rcp.wfeditor.submission.converters.VariableDeclarationActivityConverter;
import org.chemomentum.rcp.wfeditor.submission.converters.VariableModifierActivityConverter;
import org.chemomentum.rcp.wfeditor.submission.converters.WhileActivityConverter;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;

import de.fzj.unicore.rcp.wfeditor.extensionpoints.IModelConverterExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.submission.IConverter;

public class ModelConverterExtension implements IModelConverterExtensionPoint {

	public List<IConverter> getConverters() {

		List<IConverter> result = new ArrayList<IConverter>();
		result.add(new C9MStandardWFConverter());
		result.add(new GridBeanActivityConverter());
		result.add(new IfElseActivityConverter());
		result.add(new HoldActivityConverter());
		result.add(new WhileActivityConverter());
		result.add(new RepeatUntilActivityConverter());
		result.add(new ForEachActivityConverter());
		result.add(new ContainerActivityConverter());
		result.add(new VariableDeclarationActivityConverter());
		result.add(new VariableModifierActivityConverter());
		return result;
	}

	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
		// TODO Auto-generated method stub

	}

}
