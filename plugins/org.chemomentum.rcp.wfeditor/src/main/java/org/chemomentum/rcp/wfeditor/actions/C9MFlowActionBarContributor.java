/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.wfeditor.actions;

import de.fzj.unicore.rcp.wfeditor.actions.FlowActionBarContributor;

/**
 * Contributes actions to the Editor.
 * 
 * @author Daniel Lee
 */
public class C9MFlowActionBarContributor extends FlowActionBarContributor {

}
