/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.actions;

import org.chemomentum.rcp.servicebrowser.nodes.WorkflowNode;
import org.chemomentum.rcp.wfeditor.C9MWFEditorConstants;
import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowUtils;

/**
 * @author demuth
 * 
 */
public class MonitorWFNodeAction extends NodeAction {

	IContainer targetFolder = null;

	/**
	 * @param node
	 * @param view
	 */
	public MonitorWFNodeAction(Node node) {
		super(node);
		setText("Monitor");
		setToolTipText("Resume monitoring the execution of this workflow.");
		setImageDescriptor(WFEditorActivator.getImageDescriptor("monitor.png"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.actions.ServiceBrowserAction#run()
	 */
	@Override
	public void run() {
		try {
	
			WorkflowNode wfNode = (WorkflowNode) getNode();

			final String workflowId = wfNode.getWorkflowId();

			final IFile workflowFile = WorkflowUtils.findSubmittedWorkflowFile(
					workflowId, 20000, null);
			if (workflowFile != null) {
				PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
					public void run() {
						IWorkbenchPage page = PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getActivePage();
						try {
							FileEditorInput input = new FileEditorInput(
									workflowFile);
							page.openEditor(input,
									C9MWFEditorConstants.C9M_EDITOR_ID, true);

						} catch (Exception e1) {
							WFEditorActivator.log(
									IStatus.ERROR,
									"Could not monitor workflow: "
											+ e1.getMessage(), e1);
						}
					}

				});
			} else {
				WFEditorActivator
						.log(IStatus.ERROR,
								"Could not monitor workflow: workflow file could not be found in the workspace");
			}

		} catch (Exception e) {

			WFEditorActivator.log(IStatus.ERROR, "Could not monitor workflow!",
					e);
		}

	}

}
