/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.celleditors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.WFEditorActivator;
import org.chemomentum.rcp.wfeditor.model.WFFileLinker;
import org.chemomentum.rcp.wfeditor.utils.GridBeanUtils;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileSetParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.util.sets.Pair;

import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSetSink;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSink;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.files.CollectWFFilesVisitor;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.RandomOrderActivityTraverser;
import de.fzj.unicore.rcp.wfeditor.utils.TransitionUtils;

/**
 * @author demuth
 * 
 */
public class StageInTypeWorkflowCellEditor extends ComboBoxCellEditor {

	private class WorkflowFileComparator implements Comparator<WorkflowFile> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(WorkflowFile o1, WorkflowFile o2) {
			IActivity a1 = o1.getActivity();
			IActivity a2 = o2.getActivity();
			String s1 = a1.getName() + WFConstants.DISPLAYED_STRING_SEPERATOR
					+ o1.getDisplayedString();
			String s2 = a2.getName() + WFConstants.DISPLAYED_STRING_SEPERATOR
					+ o2.getDisplayedString();
			return s1.compareTo(s2);
		}

	}

	private IFileParameterValue value;

	private IAdaptable activity;

	private IStage stage;

	private List<WorkflowFile> files = new ArrayList<WorkflowFile>();

	private IFileParameterValue result;

	public StageInTypeWorkflowCellEditor(Composite parent, IStage stage,
			IAdaptable activity) {
		super(parent, new String[] { "" });
		this.activity = activity;
		this.stage = stage;
		setItems(updateItems());
	}

	@Override
	protected Object doGetValue() {
		if (result == null) // this is necessary, since the method is called
							// twice (!?)
		{
			int index = (Integer) super.doGetValue();

			if (index == -1 || index >= files.size()) {
				return value;
			}
			WorkflowFile wfFile = files.get(index);
			IGridBeanParameter param = stage.getGridBeanParameter();
			// find the data sink that belongs to the stage for creating the
			// file link
			for (IDataSink sink : getActivity().getDataSinkList()
					.getDataSinks()) {
				if (param.getName().toString().equals(sink.getId())) {

					WFFileLinker oldFlow = null;
					if (GridBeanParameterType.FILE.equals(param.getType())) {
						GPEWorkflowFileSink gpeSink = (GPEWorkflowFileSink) sink;
						for (IDataFlow flow : sink.getIncomingFlows()) {
							if (flow instanceof WFFileLinker) {
								oldFlow = (WFFileLinker) flow;
								oldFlow.unlink();
							}
						}
						Pair<IFileParameterValue, WFFileLinker> pair = GridBeanUtils
								.linkToWorkflowFile(wfFile, gpeSink,
										getControl().getShell());
						if (pair.getM2() == null) {
							if (oldFlow != null) {
								oldFlow.relink();
							}
							WFEditorActivator
									.log(IStatus.ERROR,
											"Selected file could not be imported. Please check the control flow between this activity and the activity providing the file.");
						} else {
							result = pair.getM1();
							IFlowElement sourceElt = pair.getM2()
									.getDataSource().getFlowElement();
							if (sourceElt instanceof IActivity) {
								Command cmd = TransitionUtils
										.ensureControlFlowBetween(
												(IActivity) sourceElt,
												getActivity());
								if (cmd != null && cmd.canExecute()) {
									cmd.execute();
								}
							}
						}

					} else if (GridBeanParameterType.FILE_SET.equals(param
							.getType())) {
						GPEWorkflowFileSetSink gpeSink = (GPEWorkflowFileSetSink) sink;
						String id = value.getUniqueId();

						for (IDataFlow flow : sink.getIncomingFlows()) {
							if (flow instanceof WFFileLinker) {
								if (id.equals(((WFFileLinker) flow).getFileId())) {
									oldFlow = (WFFileLinker) flow;
									oldFlow.unlink();
								}
							}
						}
						Pair<IFileSetParameterValue, WFFileLinker> pair = GridBeanUtils
								.linkToWorkflowFileSet(wfFile, gpeSink, id,
										getControl().getShell());

						if (pair.getM2() == null) {
							if (oldFlow != null) {
								oldFlow.relink();
							}
							WFEditorActivator
									.log(IStatus.ERROR,
											"Selected file could not be imported. Please check the control flow between this activity and the activity providing the file.");
						} else {
							IFileSetParameterValue value = pair.getM1();
							result = GridBeanUtils.findFileInFileSet(id, value);
							IFlowElement sourceElt = pair.getM2()
									.getDataSource().getFlowElement();
							if (sourceElt instanceof IActivity) {
								Command cmd = TransitionUtils
										.ensureControlFlowBetween(
												(IActivity) sourceElt,
												getActivity());
								if (cmd != null && cmd.canExecute()) {
									cmd.execute();
								}
							}
						}
					}
					break;
				}
			}
		}
		return result;
	}

	@Override
	protected void doSetValue(Object value) {
		Assert.isTrue(value instanceof IFileParameterValue);
		this.value = (IFileParameterValue) value;
		// TODO don't use 0
		super.doSetValue(0);
	}

	private IActivity getActivity() {
		try {
			IActivity act = (IActivity) activity.getAdapter(IActivity.class);
			return act;
		} catch (Exception e) {
			return null;
		}
	}

	private WorkflowDiagram getDiagram() {
		try {
			IActivity act = getActivity();
			return act.getDiagram();
		} catch (Exception e) {
			return null;
		}
	}

	protected IGridBean getGridBean() {
		if (activity == null) {
			return null;
		}
		IGridBean gb = (IGridBean) activity.getAdapter(IGridBean.class);
		return gb;
	}

	protected String[] updateItems() {
		WorkflowFile[] fileArray;
		List<WorkflowFile> files = new ArrayList<WorkflowFile>();
		if (getDiagram().containsCycles()) {
			WFEditorActivator
					.log(IStatus.ERROR,
							"Cannot refresh available workflow files, the workflow contains a cycle!");
		} else {
			try {

				IGraphTraverser traverser = new RandomOrderActivityTraverser();
				CollectWFFilesVisitor visitor = new CollectWFFilesVisitor();

				traverser.traverseGraph(getDiagram(), getActivity(), visitor);
				Set<WorkflowFile> found = visitor.getFoundFiles();

				for (WorkflowFile workflowFile : found) {
					if (!C9mRCPCommonConstants.WFFILE_QNAME.equals(workflowFile
							.getProtocol())) {
						continue;
					}
					IActivity act = workflowFile.getActivity();
					// do not allow importing your own output files!
					if (act.equals(activity)) {
						continue;
					}
					files.add(workflowFile);
				}
			} catch (Exception e) {
				WFEditorActivator.log(IStatus.ERROR,
						"Cannot refresh available workflow files:", e);
			}
		}
		fileArray = files.toArray(new WorkflowFile[0]);
		Arrays.sort(fileArray, new WorkflowFileComparator());
		String[] result = new String[fileArray.length];
		for (int i = 0; i < fileArray.length; i++) {
			this.files.add(fileArray[i]);
			IActivity a1 = fileArray[i].getActivity();
			result[i] = a1.getName() + WFConstants.DISPLAYED_STRING_SEPERATOR
					+ fileArray[i].getDisplayedString();
		}
		return result;
	}
}
