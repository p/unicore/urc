/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.extensions;

import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.celleditors.EnvVariableWorkflowCellEditor;
import org.chemomentum.rcp.wfeditor.model.EnvVariableLinker;
import org.chemomentum.rcp.wfeditor.utils.GridBeanUtils;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.clients.api.transfers.IAddressOrValue;
import com.intel.gpe.clients.common.transfers.AddressOrValue;
import com.intel.gpe.gridbeans.parameters.EnvironmentVariableParameterValue;
import com.intel.gpe.gridbeans.parameters.IEnvironmentVariableParameterValue;

import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IEnvVariableSourceTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.EnvVariable;

/**
 * @author demuth
 * 
 */
public class EnvVariableSourceTypeWorkflowExtension implements
		IEnvVariableSourceTypeExtensionPoint {

	private Map<QName, EnvVariableLinker> varLinkers = new HashMap<QName, EnvVariableLinker>();

	public void attachToParameterValue(IAdaptable adaptable,
			EnvVariable oldValue) {
		IEnvironmentVariableParameterValue variable = oldValue.getValue() == null ? new EnvironmentVariableParameterValue()
				: oldValue.getValue().clone();
		AddressOrValue value = new AddressOrValue("", "",
				C9mRCPCommonConstants.WFVARIABLE_PROTOCOL);
		variable.setVariableValue(value);
		oldValue.setValue(variable);

	}

	public boolean availableInContext(GridBeanContext context) {
		return context == null || context.isPartOfWorkflow();
	}

	public IAddressOrValue checkAndFixAddress(IAddressOrValue address,
			String oldVersion, String currentVersion) {
		// fix workflow variable addresses prior to client version 6.2.1
		if (oldVersion.compareTo("6.2.1") < 0) {
			IAddressOrValue result = GridBeanUtils
					.createAddressFromVariableName(address.getInternalString());
			return result;
		}
		return address;
	}

	public void detachFromParameterValue(IAdaptable adaptable, EnvVariable value) {
		EnvVariableLinker linker = varLinkers.get(value.getGridBeanParameter()
				.getName());
		if (linker != null) {
			linker.unlink();
			varLinkers.remove(value.getGridBeanParameter().getName());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getCellEditor()
	 */
	public CellEditor getCellEditor(Composite parent, EnvVariable variable,
			IAdaptable activity) {
		return new EnvVariableWorkflowCellEditor(parent, varLinkers, activity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getType()
	 */
	public QName getType() {
		return C9mRCPCommonConstants.WFVARIABLE_QNAME;
	}

	public void updateParameterValue(IAdaptable adaptable,
			EnvVariable oldValue, EnvVariable newValue) {

	}

}
