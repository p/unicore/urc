/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.wfeditor.parts;

import org.chemomentum.rcp.wfeditor.model.ForEachDataSourceActivity;

import de.fzj.unicore.rcp.wfeditor.figures.ActivityFigure;

/**
 * @author demuth
 */
public class ForEachDataSourceActivityPart extends DataSourceActivityPart {

	public ForEachDataSourceActivityPart(ForEachDataSourceActivity activity) {
		super(activity);
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();

	}

	@Override
	public ForEachDataSourceActivity getModel() {
		return (ForEachDataSourceActivity) super.getModel();
	}

	@Override
	public ActivityFigure getFigure() {
		return super.getFigure();
	}

}
