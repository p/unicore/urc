/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.submission.converters;

import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.commons.httpclient.util.URIUtil;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.C9MWFEditorConstants;
import org.chemomentum.rcp.wfeditor.clientstubs.C9MActivityJobClient;
import org.chemomentum.rcp.wfeditor.utils.GridBeanUtils;
import org.chemomentum.simpleworkflow.api.ActivityElement;
import org.chemomentum.simpleworkflow.api.IElement;
import org.chemomentum.simpleworkflow.api.SubWorkflowElement;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityDocument.Activity;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityType;
import org.chemomentum.simpleworkflow.xmlbeans.OptionDocument.Option;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ApplicationType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.DataStagingType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDescriptionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.SourceTargetType;
import org.ggf.schemas.jsdl.x2005.x11.jsdlPosix.POSIXApplicationType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.clients.all.gridbeans.GridBeanJob;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.transfers.IGridFileSetTransfer;
import com.intel.gpe.clients.api.transfers.IGridFileTransfer;
import com.intel.gpe.clients.common.transfers.GridFileSetTransfer;
import com.intel.gpe.gridbeans.ErrorSet;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.gridbeans.jsdl.GPEJSDLConstants;
import com.intel.gpe.gridbeans.jsdl.GPEJobImpl;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.SWTProgressListener;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GridBeanActivity;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.IgnoreFailureOptions;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.SplittingOptions;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.WebServiceNode;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.submission.IConverter;
import de.fzj.unicore.uas.client.StorageClient;

/**
 * @author demuth
 * 
 */
public class GridBeanActivityConverter implements IConverter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.IConverter#canConvert(javax.xml
	 * .namespace.QName, javax.xml.namespace.QName)
	 */
	public boolean canConvert(IAdaptable submissionService, QName modelType,
			QName wfLanguageType) {
		return GridBeanActivity.TYPE.equals(modelType)
		&& C9mRCPCommonConstants.C9M_DEFAULT_WF_DIALECT
		.equals(wfLanguageType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.IConverter#convert(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram)
	 */
	@SuppressWarnings("unchecked")
	public IElement convert(IAdaptable submissionService, String workflowId,
			WorkflowDiagram model, IFlowElement element,
			Map<String, Object> additionalParams, IProgressMonitor monitor)
	throws Exception {

		IProgressListener progress = new SWTProgressListener(monitor);

		try {
			Object adapter = submissionService.getAdapter(RegistryNode.class);
			WebServiceNode service = null;
			if (adapter != null) {
				service = (RegistryNode) adapter;
			} else {
				throw new Exception(
				"Could not find valid location mapper for resolving logical names in the workflow.");
			}
			EndpointReferenceType registryEpr = service.getEpr();

			GridBeanActivity gridBeanActivity = (GridBeanActivity) element;

			Activity activity = Activity.Factory.newInstance();

			activity.setId(gridBeanActivity.getName());
			activity.setType(ActivityType.JSDL);
			activity.setName(ActivityType.JSDL.toString());

			gridBeanActivity.deserializeAndGetReady();
			GridBeanClient<?> gridBeanClient = gridBeanActivity
			.getGridBeanClient();
			GridBeanJob gbjob = gridBeanClient.getGridBeanJob();
			if(gbjob == null)
			{
				throw new Exception(
				"Could not get the job description for application "+gridBeanActivity.getName());
			}
			IGridBean gridBean = gbjob.getModel();

			ErrorSet errors;
			if (gridBeanClient.getGridBeanInputPanel() != null) {
				gridBeanClient.applyInputPanelValues();
				// application specific GUI is still open => validate user
				// inputs!
				errors = gridBeanClient.validateJob();

			} else {
				errors = (ErrorSet) gridBean.get(IGridBeanModel.ERROR_SET);
			}
			if (errors != null && errors.size() > 0) {
				String message = "Found invalid Activity: "
					+ gridBeanActivity.getName() + " \n";
				for (int i = 0; i < errors.size(); i++) {
					message += errors.getErrorAt(i).getMessage();
					if (i < errors.size() - 1) {
						message += "\n";
					}
				}
				throw new Exception(message);
			}

			boolean addIterationNumberToJobname = false;
			StructuredActivity parent = gridBeanActivity.getParent();
			while (parent != null) {
				if (parent.isLoop()) {
					addIterationNumberToJobname = true;
					break;
				}
				parent = parent.getParent();
			}

			GPEJobImpl job = new GPEJobImpl();
			gridBean.set(IGridBeanModel.NOTIFIER_BEFORE_SUBMISSION, true);
			gridBean.setupJobDefinition(job);

			StorageClient sc = (StorageClient) additionalParams
			.get(C9MWFEditorConstants.PARAM_SELECTED_STORAGE);
			EndpointReferenceType epr = sc == null ? null : sc.getEPR();
			C9MActivityJobClient jobClient = new C9MActivityJobClient(
					job.getDocument(), workflowId + "/"
					+ gridBeanActivity.getName(), registryEpr,
					sc == null, epr);
			Map<String, Object> processorParams = new HashMap<String, Object>();
			processorParams.put(ProcessingConstants.WORKFLOW_ID, workflowId);

			processorParams.put(GPE4EclipseConstants.ACTIVITY_ID,
					gridBeanActivity.getName());
			// iterationId is empty
			processorParams.put(GPE4EclipseConstants.ITERATION_ID, "");

			processorParams.put(C9MWFEditorConstants.PARAM_GRID_BEAN_ACTIVITY,
					gridBeanActivity);

			processorParams.put(ProcessingConstants.JOB_WORKING_DIR,
					jobClient.getWorkingDirectory());
			processorParams.put(
					C9MWFEditorConstants.PARAM_ADD_ITERATION_NUMBER_TO_JOBNAME,
					addIterationNumberToJobname);
			IGridFileSetTransfer<IGridFileTransfer> transfers = new GridFileSetTransfer<IGridFileTransfer>();
			processorParams.put(ProcessingConstants.FILE_TRANSFERS, transfers);
			boolean uploadFiles = (Boolean) additionalParams
			.get(C9MWFEditorConstants.PARAM_UPLOAD_FILES);
			if (uploadFiles) {
				processorParams
				.put(C9MWFEditorConstants.PARAM_UPLOAD_FILES_ID,
						additionalParams
						.get(C9MWFEditorConstants.PARAM_UPLOAD_FILES_ID));
			}
			JobDefinitionType jsdl = GridBeanUtils.gridBeanToJSDL(gridBean,
					job, gridBeanClient.getClient(), uploadFiles,
					processorParams, progress);
			if(jsdl.getJobDescription() == null)
			{
				throw new Exception("The application "+gridBeanActivity.getName()+" has not been initialized. Please double-click to open and configure it correctly.");
			}
			JobDescriptionType descr = jsdl.getJobDescription();
			if(!checkApplicationSet(descr))
			{
				throw new Exception("The application "+gridBeanActivity.getName()+" has not been initialized. Please double-click to open and configure it correctly.");
			}

			// deal with stagings: old workflow engines do not expect encoded
			// URIs inside the data staging elements
			String minimumVersion = "2.2.0";
			String wfEngineVersion = (String) additionalParams
			.get(C9MWFEditorConstants.PARAM_WF_ENGINE_VERSION);
			if (minimumVersion.compareTo(wfEngineVersion) > 0) {

				DataStagingType[] stagings = descr.getDataStagingArray();
				if(stagings != null)
				{
					for(DataStagingType staging : stagings)
					{
						String s;
						if(staging.getSource() != null && (s = staging.getSource().getURI()) != null)
						{
							staging.getSource().setURI(URIUtil.decode(s));
						}
						String t;
						if(staging.getTarget() != null && (t = staging.getTarget().getURI()) != null)
						{
							staging.getTarget().setURI(URIUtil.decode(t));
						}
					}
				}
			}
			// replace variable(s) in data staging
			DataStagingType[] stagings = descr.getDataStagingArray();
			if(stagings != null)
			{
				for(DataStagingType staging : stagings){
					if(staging.getSource()!=null){
						SourceTargetType src=staging.getSource();
						src.setURI(expandWFID(src.getURI(), workflowId));
					}
					if(staging.getTarget()!=null){
						SourceTargetType tgt=staging.getTarget();
						tgt.setURI(expandWFID(tgt.getURI(), workflowId));
					}
				}
			}
			activity.setJSDL(jsdl);
			Map<String, Activity> activities = (Map<String, Activity>) additionalParams
			.get(C9MWFEditorConstants.PARAM_CREATED_JOB_DEFINITIONS);
			if (activities == null) {
				activities = new HashMap<String, Activity>();
			}
			activities.put(activity.getId(), activity);
			additionalParams.put(
					C9MWFEditorConstants.PARAM_CREATED_JOB_DEFINITIONS,
					activities);
			gridBeanActivity.setJobClient(jobClient);

			SplittingOptions splittingOptions = gridBeanActivity
			.getSplittingOptions();

			Option o = activity.addNewOption();
			o.setName("SPLIT");
			o.setStringValue(String.valueOf(splittingOptions
					.isSplittingAllowed()));

			if (splittingOptions.isSplittingAllowed()) {
				o = activity.addNewOption();
				o.setName("MAX_SPLIT");
				o.setStringValue(String.valueOf(splittingOptions
						.maxNumSubjobs()));
			}
			
			IgnoreFailureOptions ignoreFailureOptions = gridBeanActivity.getIgnoreFailureOptions();
			o = activity.addNewOption();
			o.setName("IGNORE_FAILURE");
			o.setStringValue(String.valueOf(ignoreFailureOptions.isIgnoreFailureEnabled()));
			
			if (ignoreFailureOptions.isIgnoreFailureEnabled()) {
				o = activity.addNewOption();
				o.setName("MAX_RESUBMITS");
				o.setStringValue(String.valueOf(ignoreFailureOptions.maxNumOfResubmits()));
			}
			

			SubWorkflowElement graph = (SubWorkflowElement) additionalParams
			.get(C9MWFEditorConstants.PARAM_SUBGRAPH);
			graph.addActivity(activity);
			return new ActivityElement(activity);
		} finally {
			progress.done();
		}
	}

	private boolean checkApplicationSet(JobDescriptionType descr)
	{
		if(descr.getApplication() == null) return false;
		ApplicationType app = descr.getApplication();

		if(app.getApplicationName() != null && 
				app.getApplicationName().trim().length() > 0) 
		{
			return true;
		}

		XmlCursor acursor = app.newCursor();
		try {
			if (acursor.toFirstChild()) {
				do {
					if(acursor.getName().equals(GPEJSDLConstants.POSIX_APPLICATION)) {
						XmlObject o= acursor.getObject();

						POSIXApplicationType posix = (POSIXApplicationType) o;
						if(posix != null && posix.getExecutable() != null && 
								posix.getExecutable().getStringValue() != null &&
								posix.getExecutable().getStringValue().trim().length() > 0)
						{
							return true;
						}
						break;
					}
				} while (acursor.toNextSibling());

			}
		} finally {
			acursor.dispose();
		}

		return false;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.submission.IConverter#getModelType()
	 */
	public QName getModelType() {
		return GridBeanActivity.TYPE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.submission.IConverter#getTargetType()
	 */
	public QName getTargetType() {
		return C9mRCPCommonConstants.C9M_DEFAULT_WF_DIALECT;
	}

	private String expandWFID(String var, String workflowID){
		String[] keys= new String[]{"${WORKFLOW_ID}", "$%7BWORKFLOW_ID%7D", "$WORKFLOW_ID" };
		for(String key: keys){
			if(var.contains(key)){
				var=var.replace(key,workflowID);
			}
		}
		return var;
	}
}
