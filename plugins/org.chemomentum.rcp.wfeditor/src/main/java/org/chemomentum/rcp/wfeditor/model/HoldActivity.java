package org.chemomentum.rcp.wfeditor.model;

import javax.xml.namespace.QName;

import org.eclipse.draw2d.geometry.Dimension;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.SimpleActivity;

/**
 * @author mborcz
 */
@XStreamAlias("HoldResumeActivity")
public class HoldActivity extends SimpleActivity {

	private static final long serialVersionUID = -2725453634231164573L;

	public static QName TYPE = new QName("http://www.unicore.eu/",
			"HoldResumeActivity");

	@Override
	public QName getType() {
		return TYPE;
	}

	public void init() {
		super.init();
		WorkflowDiagram diagram = getDiagram();
		this.setDiagram(diagram);
		this.setName(getDiagram().getUniqueActivityName("Hold"));
	}

	public void dispose() {
		super.dispose();
	}

	public void undoDispose() {
		super.undoDispose();
	}

	public void setSize(Dimension size) {
		super.setSize(size);
	}

}
