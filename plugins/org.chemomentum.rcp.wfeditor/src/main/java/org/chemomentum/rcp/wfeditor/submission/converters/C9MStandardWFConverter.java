/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.wfeditor.submission.converters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlObject;
import org.chemomentum.common.util.C9MCommonConstants;
import org.chemomentum.rcp.common.C9mRCPCommonConstants;
import org.chemomentum.rcp.wfeditor.C9MWFEditorConstants;
import org.chemomentum.rcp.wfeditor.model.OverwriteWFFileLinker;
import org.chemomentum.rcp.wfeditor.model.WFFileLinker;
import org.chemomentum.rcp.wfeditor.utils.GridBeanUtils;
import org.chemomentum.simpleworkflow.api.SubWorkflowElement;
import org.chemomentum.simpleworkflow.xmlbeans.ActivityDocument.Activity;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.DataStagingType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionType;

import com.intel.gpe.clients.api.transfers.ProtocolConstants;

import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSetSink;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSink;
import de.fzj.unicore.rcp.gpe4eclipse.utils.ParameterProcessingUtils;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.submission.IConverter;
import de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.TopDownDAGTraverser;
import de.fzj.unicore.uas.util.Pair;

/**
 * @author demuth
 * 
 */
public class C9MStandardWFConverter implements IConverter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.IConverter#canConvert(javax.xml
	 * .namespace.QName, javax.xml.namespace.QName)
	 */
	public boolean canConvert(IAdaptable submissionService, QName modelType,
			QName wfLanguageType) {
		return C9mRCPCommonConstants.C9M_DEFAULT_WF_DIALECT
				.equals(wfLanguageType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.IConverter#convert(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram)
	 */
	@SuppressWarnings("unchecked")
	public XmlObject convert(IAdaptable submissionService, String workflowId,
			WorkflowDiagram model, IFlowElement element,
			Map<String, Object> additionalParams, IProgressMonitor monitor)
			throws Exception {

		try {

			SubWorkflowElement root = new SubWorkflowElement();

			additionalParams.put(C9MWFEditorConstants.PARAM_SUBGRAPH, root);

			monitor.beginTask("converting workflow to XML", model
					.getActivities().size());

			IGraphTraverser traverser = new TopDownDAGTraverser();
			DefaultStructuredActivityVisitor visitor = new DefaultStructuredActivityVisitor(
					submissionService, workflowId, model, monitor,
					getTargetType(), additionalParams);
			traverser.traverseGraph(model, model.getStartActivity(), visitor);
			if (monitor.isCanceled()) {
				return null;
			}
			Map<String, Activity> jobs = (Map<String, Activity>) additionalParams
					.get(C9MWFEditorConstants.PARAM_CREATED_JOB_DEFINITIONS);
			if (jobs != null && jobs.size() > 0) {
				fixFileDataFlowLoops(workflowId, model, jobs);
			}
			Thread thread = Thread.currentThread();
			ClassLoader loader = thread.getContextClassLoader();
			thread.setContextClassLoader(UnicoreCommonActivator.getDefault()
					.getXmlBeansClassloader());
			XmlObject result;
			try {
				result = root.getWorkflowDocument();
			} finally {
				thread.setContextClassLoader(loader);
			}

			return result;
		} finally {
			monitor.done();
		}
	}

	private void fixFileDataFlowLoops(String wfId, WorkflowDiagram diagram,
			Map<String, Activity> jobs) throws Exception {
		for (IActivity act : diagram.getActivities().values()) {
			if (jobs.get(act.getName()) != null) {
				for (IDataSink sink : act.getDataSinkList().getDataSinks()) {

					List<Pair<WorkflowFile, WorkflowFile>> toFix = new ArrayList<Pair<WorkflowFile, WorkflowFile>>();
					if (sink instanceof GPEWorkflowFileSink
							&& sink.getIncomingFlows().length > 1) {
						Exception e = new Exception(
								"Found invalid incoming data flows for activity "
										+ act.getName());

						if (sink.getIncomingFlows().length > 2) {
							throw e;
						}
						OverwriteWFFileLinker overwritingLinker = null;
						WFFileLinker initialLinker = null;

						if (sink.getIncomingFlows()[0] instanceof OverwriteWFFileLinker) {
							overwritingLinker = (OverwriteWFFileLinker) sink
									.getIncomingFlows()[0];
						} else if (sink.getIncomingFlows()[0] instanceof WFFileLinker) {
							initialLinker = (WFFileLinker) sink
									.getIncomingFlows()[0];
						}
						if (sink.getIncomingFlows()[1] instanceof OverwriteWFFileLinker) {
							overwritingLinker = (OverwriteWFFileLinker) sink
									.getIncomingFlows()[1];
						} else if (sink.getIncomingFlows()[1] instanceof WFFileLinker) {
							initialLinker = (WFFileLinker) sink
									.getIncomingFlows()[1];
						}
						if (overwritingLinker == null || initialLinker == null) {
							throw e;
						}

						WorkflowFile initialFile = (WorkflowFile) initialLinker
								.getDataSource();
						WorkflowFile overwritingFile = (WorkflowFile) overwritingLinker
								.getDataSource();
						toFix.add(new Pair<WorkflowFile, WorkflowFile>(
								initialFile, overwritingFile));

					} else if (sink instanceof GPEWorkflowFileSetSink) {
						Exception e = new Exception(
								"Found invalid incoming data flows for activity "
										+ act.getName());

						Map<String, Pair<WorkflowFile, WorkflowFile>> foundLinkers = new HashMap<String, Pair<WorkflowFile, WorkflowFile>>();

						for (IDataFlow flow : sink.getIncomingFlows()) {
							if (flow instanceof OverwriteWFFileLinker) {
								OverwriteWFFileLinker overwritingLinker = (OverwriteWFFileLinker) flow;
								Pair<WorkflowFile, WorkflowFile> pair = foundLinkers
										.get(overwritingLinker.getFileId());
								WorkflowFile overwritingFile = (WorkflowFile) overwritingLinker
										.getDataSource();
								if (pair == null) {
									pair = new Pair<WorkflowFile, WorkflowFile>(
											null, overwritingFile);
									foundLinkers
											.put(overwritingLinker.getFileId(),
													pair);
								} else {
									if (pair.getM2() != null) {
										throw e;
									}
									pair.setM2(overwritingFile);
									if (pair.getM1() != null) {
										toFix.add(pair);
									}
								}
							} else if (flow instanceof WFFileLinker) {
								WFFileLinker initialLinker = (WFFileLinker) flow;
								Pair<WorkflowFile, WorkflowFile> pair = foundLinkers
										.get(initialLinker.getFileId());
								WorkflowFile initialFile = (WorkflowFile) initialLinker
										.getDataSource();
								if (pair == null) {
									pair = new Pair<WorkflowFile, WorkflowFile>(
											initialFile, null);
									foundLinkers.put(initialLinker.getFileId(),
											pair);
								} else {
									if (pair.getM1() != null) {
										throw e;
									}
									pair.setM1(initialFile);
									if (pair.getM2() != null) {
										toFix.add(pair);
									}
								}
							}

						}
						for (Pair<WorkflowFile, WorkflowFile> pair : foundLinkers
								.values()) {
							if (pair.getM1() == null && pair.getM2() != null) {
								throw e;
							}
						}

					}
					for (Pair<WorkflowFile, WorkflowFile> pair : toFix) {
						WorkflowFile initialFile = pair.getM1();
						WorkflowFile overwritingFile = pair.getM2();
						String uuid = UUID.randomUUID().toString();
						IActivity initialActivity = initialFile.getActivity();
						// deal with iteration IDs!
						String iterationId = ParameterProcessingUtils
								.createVariableFromName(GPE4EclipseConstants.ITERATION_ID);
						iterationId = GridBeanUtils
								.resolveIteratorInWFFileBeforeSubmission(
										iterationId, initialActivity, act);
						if (iterationId.length() > 0) {
							iterationId = C9MCommonConstants.ITERATION_ID_SEPERATOR
									+ iterationId;
						}
						String targetWithoutUUID = C9mRCPCommonConstants.DMFILE_PREFIX
								+ ProtocolConstants.PROTOCOL_SEPERATOR
								+ wfId
								+ "/"
								+ initialActivity.getName()
								+ iterationId
								+ "/" + overwritingFile.getRelativePath();
						// create unique logical name for the file
						String targetWithUUID = targetWithoutUUID + "-" + uuid;

						// add stage out to activity that creates the initial
						// file content
						String activityName = initialActivity.getName();
						if (jobs.get(activityName) != null
								&& jobs.get(activityName).getJSDL() != null) {
							JobDefinitionType job = jobs.get(activityName)
									.getJSDL();
							DataStagingType stageing = job.getJobDescription()
									.addNewDataStaging();
							stageing.setFileName(initialFile.getRelativePath());
							stageing.addNewTarget().setURI(targetWithUUID);
						}

						// add stage out to activity that overwrites the initial
						// file content
						activityName = overwritingFile.getActivity().getName();
						if (jobs.get(activityName) != null
								&& jobs.get(activityName).getJSDL() != null) {
							JobDefinitionType job = jobs.get(activityName)
									.getJSDL();
							DataStagingType stageing = job.getJobDescription()
									.addNewDataStaging();
							stageing.setFileName(overwritingFile
									.getRelativePath());
							stageing.addNewTarget().setURI(targetWithUUID);
						}

						// replace stage in of the activity that uses the file
						// the reference to the initial workflow file
						// must be replaced with a reference to the newly
						// created file
						activityName = act.getName();
						if (jobs.get(activityName) != null
								&& jobs.get(activityName).getJSDL() != null) {
							JobDefinitionType job = jobs.get(activityName)
									.getJSDL();
							for (DataStagingType stageing : job
									.getJobDescription().getDataStagingArray()) {
								if (stageing.getSource() != null
										&& targetWithoutUUID.equals(stageing
												.getSource().getURI())) {
									// we've found our file!
									stageing.getSource().setURI(targetWithUUID);
									break;
								}
							}

						}
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.submission.IConverter#getModelType()
	 */
	public QName getModelType() {
		return C9mRCPCommonConstants.C9M_QNAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.submission.IConverter#getTargetType()
	 */
	public QName getTargetType() {
		return C9mRCPCommonConstants.C9M_DEFAULT_WF_DIALECT;
	}

}
