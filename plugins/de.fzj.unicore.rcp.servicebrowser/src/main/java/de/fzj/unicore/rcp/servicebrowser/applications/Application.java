package de.fzj.unicore.rcp.servicebrowser.applications;

/**
 * The abstraction of the information about the application available at the
 * target system.
 * 
 * @author Alexander Lukichev
 * @version $Id: Application.java,v 1.4 2005/10/04 11:04:16 dizhigul Exp $ This
 *          code is from the Intel Unicore client.
 * 
 */
public interface Application {

	/**
	 * The workflow application
	 */
	public static final ApplicationImpl WORKFLOW = new ApplicationImpl(
			"WORKFLOW", "1.0");

	/**
	 * @return The version of the application
	 */
	public String getApplicationVersion();

	/**
	 * @return The description of the application
	 */
	public String getDescription();

	/**
	 * @return The name of the application
	 */
	public String getName();
}
