package de.fzj.unicore.rcp.servicebrowser.dnd;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;

public class FileNodeOnClipboardTester extends PropertyTester {

	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		Clipboard clipboard = new Clipboard(PlatformUI.getWorkbench()
				.getDisplay());
		boolean supportedTransferData = false;
		for (TransferData td : clipboard.getAvailableTypes()) {
			if (NodeLocalSelectionTransfer.getTransfer().isSupportedType(td)) {
				supportedTransferData = true;
				break;
			}
		}
		if (!supportedTransferData) {
			return false;
		}
		NodeLocalSelectionContents contents = (NodeLocalSelectionContents) clipboard
				.getContents(NodeLocalSelectionTransfer.getTransfer());
		clipboard.dispose();
		if (contents == null) {
			return false;
		}
		final IStructuredSelection selection = (IStructuredSelection) contents
				.getSelection();
		if (selection == null) {
			return false;
		}
		for (Object o : selection.toArray()) {
			if (!(o instanceof AbstractFileNode)) {
				return false;
			}
		}
		return true;
	}

}
