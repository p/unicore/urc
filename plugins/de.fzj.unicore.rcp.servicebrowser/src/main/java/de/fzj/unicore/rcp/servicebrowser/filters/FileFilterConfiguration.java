/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.filters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

/**
 * builds up a GUI for configuring a file filter
 * 
 * @author Christian Hohmann
 * 
 */
public class FileFilterConfiguration {

	private Text fileTextBox;
	private String filterPattern = ".*";
	private int filterMode = FileFilterCriteria.FILES_AND_FOLDERS;
	

	String nameSpace = "de.fzj.unicore.rcp.servicebrowser.filter";
	String displayTitle = "Configure file/directory filter";
	String labelCaption = "Enter regular expression to filter files by name";
	String dirAndFilesCaption = "directories and files";
	String filesCaption = "files only";
	String dirsOnly = "directories only";
	String okButtonCaption = "OK";
	String cancelButtonCaption = "Cancel";

	private Button radFilesOnly = null;
	private Button radDirectoriesOnly = null;
	private Button radDirectoriesAndFiles = null;
	Shell platformDisplay;
	boolean isClosing;
	
	public void loadConfiguration(FileFilterCriteria filter)
	{
		filterPattern = filter.getPattern();
		filterMode = filter.getMode();
	}

	public FileFilterCriteria createFileFilter() {
		platformDisplay = openConfigurationGUI();
		platformDisplay.setActive();
		platformDisplay.pack();
		// fileTextBox.setText(fileFilter);
		platformDisplay.open();
		Display display = PlatformUI.getWorkbench().getDisplay();
		while (!platformDisplay.isDisposed()) {
			// process the next event, wait when none available
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		if ("".equals(filterPattern)) {
			return null;
		}
		return new FileFilterCriteria(filterPattern, filterMode);
	}

	private boolean doExit() {
		isClosing = true;
		platformDisplay.close();
		platformDisplay.dispose();
		return true;
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public Shell openConfigurationGUI() {
		platformDisplay = new Shell(PlatformUI.getWorkbench().getDisplay(),
				SWT.CLOSE | SWT.RESIZE);

		GridLayout platformDisplayLayout = new GridLayout();
		platformDisplay.setLayout(platformDisplayLayout);
		platformDisplayLayout.numColumns = 2;

		platformDisplay.setText(displayTitle);

		Label label = new Label(platformDisplay, SWT.CENTER);
		GridData labelGridData = new GridData(GridData.FILL_HORIZONTAL);
		labelGridData.horizontalSpan = 2;
		label.setLayoutData(labelGridData);
		label.setText(labelCaption);

		fileTextBox = new Text(platformDisplay, SWT.BORDER);
		GridData fileTextBoxgridData = new GridData(GridData.FILL_HORIZONTAL);
		fileTextBoxgridData.horizontalSpan = 3;

		fileTextBox.setLayoutData(fileTextBoxgridData);
		fileTextBox.setText(filterPattern);

		radDirectoriesAndFiles = new Button(platformDisplay, SWT.RADIO);
		radDirectoriesAndFiles.setText(dirAndFilesCaption);
		GridData directoriesAndFilesData = new GridData(
				GridData.HORIZONTAL_ALIGN_BEGINNING);
		radDirectoriesAndFiles.setLayoutData(directoriesAndFilesData);

		new Label(platformDisplay, SWT.NONE);

		radFilesOnly = new Button(platformDisplay, SWT.RADIO);
		radFilesOnly.setText(filesCaption);
		GridData filesOnlyData = new GridData(
				GridData.HORIZONTAL_ALIGN_BEGINNING);
		radDirectoriesAndFiles.setLayoutData(filesOnlyData);

		new Label(platformDisplay, SWT.NONE);

		radDirectoriesOnly = new Button(platformDisplay, SWT.RADIO);
		radDirectoriesOnly.setText(dirsOnly);
		GridData dirsOnlyData = new GridData(
				GridData.HORIZONTAL_ALIGN_BEGINNING);
		radDirectoriesAndFiles.setLayoutData(dirsOnlyData);

		new Label(platformDisplay, SWT.NONE);
		
		radDirectoriesAndFiles.setSelection(filterMode == FileFilterCriteria.FILES_AND_FOLDERS);
		radFilesOnly.setSelection(filterMode == FileFilterCriteria.FILES_ONLY);
		radDirectoriesOnly.setSelection(filterMode == FileFilterCriteria.FOLDERS_ONLY);
		

		Button buttonOK = new Button(platformDisplay, SWT.PUSH);
		buttonOK.setText(okButtonCaption);
		GridData okGridData = new GridData(GridData.FILL_HORIZONTAL);
		buttonOK.setLayoutData(okGridData);

		Button buttonCancel = new Button(platformDisplay, SWT.PUSH);
		buttonCancel.setText(cancelButtonCaption);
		GridData cancelGridData = new GridData(GridData.FILL_HORIZONTAL);
		buttonCancel.setLayoutData(cancelGridData);

		buttonOK.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				filterPattern = fileTextBox.getText();
				if (radDirectoriesOnly.getSelection()) {
					filterMode = FileFilterCriteria.FOLDERS_ONLY;
				} else {
					if (radFilesOnly.getSelection()) {
						filterMode = FileFilterCriteria.FILES_ONLY;
					} else {
						filterMode = FileFilterCriteria.FILES_AND_FOLDERS;
					}
				}
				doExit();
			}
		});

		buttonCancel
				.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
					@Override
					public void widgetSelected(
							org.eclipse.swt.events.SelectionEvent e) {
						filterPattern = "";
						doExit();
					}
				});

		platformDisplay
				.addShellListener(new org.eclipse.swt.events.ShellAdapter() {
					@Override
					public void shellClosed(org.eclipse.swt.events.ShellEvent e) {
						if (!isClosing) {
							e.doit = doExit();
						}
					}
				});
		return platformDisplay;

	}

}
