package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.jface.bindings.TriggerSequence;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.keys.IBindingService;

import de.fzj.unicore.rcp.servicebrowser.dnd.NodeLocalSelectionTransfer;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

public class CutNodesAction extends NodeAction {
	private ISelectionProvider selectionProvider;
	private Clipboard clipboard;

	public CutNodesAction(ISelectionProvider viewer, Clipboard clipboard) {
		super(null);
		this.selectionProvider = viewer;
		this.clipboard = clipboard;
		init();
	}

	public CutNodesAction(Node node) {
		super(node);
		init();
	}

	protected void init() {
		String text = "Cut";
		try {
			IBindingService bindingService = (IBindingService) PlatformUI
			.getWorkbench().getActiveWorkbenchWindow()
			.getService(IBindingService.class);
			String keyBindingText = null;

			TriggerSequence binding = bindingService
			.getBestActiveBindingFor(IWorkbenchCommandConstants.EDIT_CUT);
			if (binding != null) {
				keyBindingText = binding.format();
			}

			if (keyBindingText != null) {
				text += '\t' + keyBindingText;
			}

		} catch (Exception e) {
			// do nothing
		}

		setText(text);
		setToolTipText("Cut selected elements in order to paste them later");

		ISharedImages sharedImages = PlatformUI.getWorkbench()
		.getSharedImages();
		setImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_CUT));
		setDisabledImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_CUT_DISABLED));
	}

	@Override
	public void run() {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {

			public void run() {
				if (clipboard == null) {
					clipboard = new Clipboard(getViewer().getControl()
							.getDisplay());
				}
				IStructuredSelection selection = (IStructuredSelection) selectionProvider
				.getSelection();
				if(selection != null && selection.size() > 0)
				{
					selectionProvider.setSelection(null);
					NodeLocalSelectionTransfer transfer = NodeLocalSelectionTransfer
					.getTransfer();
					transfer.setSelection(selection);
					transfer.setOperation(DND.DROP_MOVE);
					clipboard.setContents(new Object[] { selection },
							new Transfer[] { transfer }, 1);
				}
			}
		});
	}

	@Override
	public void setViewer(IServiceViewer viewer) {
		super.setViewer(viewer);
		this.selectionProvider = viewer;
	}

}
