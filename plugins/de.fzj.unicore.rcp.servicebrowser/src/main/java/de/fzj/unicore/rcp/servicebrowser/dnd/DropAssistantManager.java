package de.fzj.unicore.rcp.servicebrowser.dnd;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.ui.internal.navigator.extensions.RegistryReader;
import org.eclipse.ui.part.PluginTransfer;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IDropAssistant;

public class DropAssistantManager extends RegistryReader {

	private static final DropAssistantManager INSTANCE = new DropAssistantManager();

	private static final IDropAssistant[] NO_ASSISTANTS = new IDropAssistant[0];

	private final List<DropAssistantDescriptor> dropAssistentDescriptors = new ArrayList<DropAssistantDescriptor>();

	private final List<IDropAssistant> dropAssistants = new ArrayList<IDropAssistant>();

	private final List<Transfer> supportedTransferTypes = new ArrayList<Transfer>();

	private DropAssistantManager() {
		super(ServiceBrowserActivator.PLUGIN_ID,
				IDropAssistant.EXTENSION_POINT_ID);
		supportedTransferTypes.add(PluginTransfer.getInstance());
		readRegistry();
	}

	/**
	 * @param aContentDescriptor
	 *            A non-null content descriptor.
	 * @param aDropDescriptor
	 *            A non-null drop descriptor.
	 */
	private void addDropAssistantDescriptor(DropAssistantDescriptor descr) {
		dropAssistentDescriptors.add(descr);
		IDropAssistant a = descr.createDropAssistant();
		dropAssistants.add(a);
		for (Transfer t : a.getSupportedTransferTypes()) {
			if (!supportedTransferTypes.contains(t)) {
				supportedTransferTypes.add(t);
			}
		}
	}

	public IDropAssistant[] findDropAssistants(TransferData[] sources,
			Object dropTarget) {

		List<IDropAssistant> found = new ArrayList<IDropAssistant>();
		DropAssistantDescriptor candidate;
		for (int i = 0; i < dropAssistentDescriptors.size(); i++) {
			candidate = dropAssistentDescriptors.get(i);
			if (candidate.isDropElementSupported(dropTarget)) {
				IDropAssistant assistant = dropAssistants.get(i);
				for (TransferData t : sources) {
					if (assistant.supportsTransferData(t)) {
						found.add(assistant);
						break;
					}
				}
			}

		}
		if (found.isEmpty()) {
			return NO_ASSISTANTS;
		}
		return found.toArray(new IDropAssistant[found.size()]);
	}

	public TransferData[] getSupportedTransferData(TransferData[] sources,
			Object dropTarget) {
		List<TransferData> found = new ArrayList<TransferData>();
		DropAssistantDescriptor candidate;
		for (TransferData t : sources) {
			for (int i = 0; i < dropAssistentDescriptors.size(); i++) {
				candidate = dropAssistentDescriptors.get(i);
				if (candidate.isDropElementSupported(dropTarget)) {
					IDropAssistant assistant = dropAssistants.get(i);
					if (assistant.supportsTransferData(t)) {
						found.add(t);
						break;
					}
				}
			}
		}
		return found.toArray(new TransferData[found.size()]);
	}

	public Transfer[] getSupportedTransferTypes() {
		return supportedTransferTypes
				.toArray(new Transfer[supportedTransferTypes.size()]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.internal.navigator.extensions.RegistryReader#readElement
	 * (org.eclipse.core.runtime.IConfigurationElement)
	 */
	@Override
	protected boolean readElement(IConfigurationElement element) {
		if (IDropAssistant.ELEMENT_DROP_ASSISTANT.equals(element.getName())) {
			addDropAssistantDescriptor(new DropAssistantDescriptor(element));
		}
		return true;
	}

	/**
	 * 
	 * @return An initialized singleton instance of the DropAssistantManager.
	 */
	public static DropAssistantManager getInstance() {
		return INSTANCE;
	}

}
