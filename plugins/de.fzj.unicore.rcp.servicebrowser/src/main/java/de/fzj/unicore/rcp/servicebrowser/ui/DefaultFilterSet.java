/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.ui;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;

import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.filters.AccessDeniedNodesViewerFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.ConfigurableFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.FailedNodesViewerFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.FileFilterCriteria;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterController;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterCriteria;
import de.fzj.unicore.rcp.servicebrowser.filters.NodeTypeViewerFilter;
import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.JobNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.GroupNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.MoreNode;

/**
 * @author demuth
 * 
 */
public class DefaultFilterSet implements IPropertyChangeListener {

	public static final String FILTER_ID_NODE_TYPES = "node types";
	public static final String FILTER_ID_FAILED_NODES = "failed nodes";
	public static final String FILTER_ID_ACCESS_DENIED_NODES = "access denied nodes";
	public static final String FILTER_ID_HIDDEN_FILES = "hidden files";

	public static final Set<String> TYPES_DEFAULT_WITHOUT_EXTENSIONS = Collections
	.unmodifiableSet(new HashSet<String>(Arrays.asList(new String[] {
			GridNode.TYPE, RegistryNode.TYPE,
			TargetSystemFactoryNode.TYPE, TargetSystemNode.TYPE,
			JobNode.TYPE, StorageFactoryNode.TYPE, StorageNode.TYPE,
			FileNode.TYPE, FolderNode.TYPE, GroupNode.TYPE,
			MoreNode.TYPE })));

	public static final Set<String> TYPES_TARGET_SYSTEM_FACTORIES = Collections
	.unmodifiableSet(new HashSet<String>(Arrays.asList(new String[] {
			GridNode.TYPE, RegistryNode.TYPE,
			TargetSystemFactoryNode.TYPE })));

	public static final Set<String> TYPES_TARGET_SYSTEMS = Collections
	.unmodifiableSet(new HashSet<String>(Arrays.asList(new String[] {
			GridNode.TYPE, RegistryNode.TYPE,
			TargetSystemFactoryNode.TYPE, TargetSystemNode.TYPE })));

	public static final Set<String> TYPES_JOBS = Collections
	.unmodifiableSet(new HashSet<String>(Arrays.asList(new String[] {
			GridNode.TYPE, RegistryNode.TYPE, JobNode.TYPE })));

	public static final Set<String> TYPES_REGISTRIES = Collections
	.unmodifiableSet(new HashSet<String>(Arrays.asList(new String[] {
			GridNode.TYPE, RegistryNode.TYPE })));

	private FilterController filterController;
	private NodeTypeViewerFilter nodeTypeViewerFilter;
	private FailedNodesViewerFilter failedNodesViewerFilter;
	private AccessDeniedNodesViewerFilter accessDeniedNodesViewerFilter;
	private FileFilterCriteria hiddenFileFilter;

	private boolean alwaysHideFailedNodes = false,
	alwaysHideUnaccessibleNodes = false;

	private String selectedGridFilterId;

	public DefaultFilterSet(FilterController filterController) {
		this.filterController = filterController;
		createDefaultFilters();
		hookToPreferenceStores();
	}
	
	protected void createDefaultFilters()
	{
		nodeTypeViewerFilter = new NodeTypeViewerFilter(FILTER_ID_NODE_TYPES,
				null);
		filterController.addFilter(nodeTypeViewerFilter);

		hiddenFileFilter = new FileFilterCriteria("[^.].*", FileFilterCriteria.FILES_AND_FOLDERS | FileFilterCriteria.SHOW_NON_FILE_NODES);
		hiddenFileFilter.setShowAllChildNodes(false);
		hiddenFileFilter.setShowDirectChildNodes(false);
		ConfigurableFilter hiddenFileConfigurableFilter = new ConfigurableFilter(FILTER_ID_HIDDEN_FILES,true,hiddenFileFilter);
		hiddenFileConfigurableFilter.setNodeSkippingEnabled(false);
		filterController.addFilter(hiddenFileConfigurableFilter);

		boolean filterFailed = ServiceBrowserActivator.getDefault()
		.getPreferenceStore()
		.getBoolean(ServiceBrowserConstants.P_HIDE_FAILED_SERIVCES);
		failedNodesViewerFilter = new FailedNodesViewerFilter(
				FILTER_ID_FAILED_NODES, filterFailed);
		filterController.addFilter(failedNodesViewerFilter);

		boolean filterDenied = ServiceBrowserActivator
		.getDefault()
		.getPreferenceStore()
		.getBoolean(
				ServiceBrowserConstants.P_HIDE_ACCESS_DENIED_SERIVCES);
		accessDeniedNodesViewerFilter = new AccessDeniedNodesViewerFilter(
				FILTER_ID_ACCESS_DENIED_NODES, filterDenied);
		filterController.addFilter(accessDeniedNodesViewerFilter);
	}

	public void hookToPreferenceStores()
	{
		UnicoreCommonActivator.getDefault().getPreferenceStore()
		.addPropertyChangeListener(this);

		ServiceBrowserActivator.getDefault().getPreferenceStore()
		.addPropertyChangeListener(this);
	}

	public void alwaysHideFailedNodes(boolean alwaysHideFailedNodes) {
		this.alwaysHideFailedNodes = alwaysHideFailedNodes;
		boolean filterFailed = alwaysHideFailedNodes
		|| ServiceBrowserActivator
		.getDefault()
		.getPreferenceStore()
		.getBoolean(
				ServiceBrowserConstants.P_HIDE_FAILED_SERIVCES);
		failedNodesViewerFilter.setActive(filterFailed);
	}

	public void alwaysHideUnaccessibleNodes(boolean alwaysHideUnaccessibleNodes) {
		this.alwaysHideUnaccessibleNodes = alwaysHideUnaccessibleNodes;
		boolean filterDenied = alwaysHideUnaccessibleNodes
		|| ServiceBrowserActivator
		.getDefault()
		.getPreferenceStore()
		.getBoolean(
				ServiceBrowserConstants.P_HIDE_ACCESS_DENIED_SERIVCES);
		accessDeniedNodesViewerFilter.setActive(filterDenied);
	}

	public void dispose() {
		UnicoreCommonActivator.getDefault().getPreferenceStore()
		.removePropertyChangeListener(this);
		ServiceBrowserActivator.getDefault().getPreferenceStore()
		.removePropertyChangeListener(this);
	}

	/**
	 * hand the filterCriteria from the ServiceView to the NodeTypeViewerFilter
	 * 
	 * @param fc
	 *            the filterCriteria
	 */
	public void filterNodesByGridFilter(FilterCriteria fc) {
		nodeTypeViewerFilter.setGridFilter(fc);
		filterController.refreshViewers();
	}

	public void filterNodesByType(Set<String> interestingTypes) {
		nodeTypeViewerFilter.setInterestingTypes(interestingTypes);
		filterController.refreshViewers();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.util.IPropertyChangeListener#propertyChange(org.eclipse
	 * .jface.util.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent event) {
		if (ServiceBrowserConstants.P_HIDE_FAILED_SERIVCES.equals(event
				.getProperty()) && !alwaysHideFailedNodes) {
			boolean filterFailed = (Boolean) event.getNewValue();
			filterController.setFilterActive(
					DefaultFilterSet.FILTER_ID_FAILED_NODES, filterFailed);
		} else if (ServiceBrowserConstants.P_HIDE_ACCESS_DENIED_SERIVCES
				.equals(event.getProperty()) && !alwaysHideUnaccessibleNodes) {
			boolean filterAccessDenied = (Boolean) event.getNewValue();
			filterController.setFilterActive(
					DefaultFilterSet.FILTER_ID_ACCESS_DENIED_NODES,
					filterAccessDenied);
		}
	}

	public FilterController getFilterController() {
		return filterController;
	}

	public String getSelectedGridFilterId() {
		return selectedGridFilterId;
	}

	public void setSelectedGridFilterId(String selectedGridFilterId) {
		this.selectedGridFilterId = selectedGridFilterId;
	}
}
