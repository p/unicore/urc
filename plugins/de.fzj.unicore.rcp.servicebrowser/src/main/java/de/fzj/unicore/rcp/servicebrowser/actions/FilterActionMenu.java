/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.util.HashMap;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;

import de.fzj.unicore.rcp.servicebrowser.filters.FilterMenuCategory;

/**
 * @author demuth
 * 
 */
public class FilterActionMenu extends Action implements IMenuCreator {
	MenuManager rootMenuManager = new MenuManager("ch");
	MenuManager fMenu;
	List<Action> filterActions;
	List<FilterMenuCategory> filterMenuCategory;
	HashMap<String,Object> menuManagersMap = new HashMap<String, Object>();

	public FilterActionMenu(List<Action> filterActions) {

		this.filterActions = filterActions;
		setText("Show");
		setToolTipText("Filter the grid for specific services or resources.");
		setMenuCreator(this);
	}

	public FilterActionMenu(List<Action> filterActions,
			List<FilterMenuCategory> filterMenuCategory) {
		this.filterActions = filterActions;
		this.filterMenuCategory = filterMenuCategory;
		setText("Show");
		setToolTipText("Filter the grid for specific services or resources.");
		setMenuCreator(this);
	}

	private void addActionToMenu(FilterMenuCategory category, Action action) {
		MenuManager subMenu = null;

		MenuManager parentMenu = fMenu;

		if (category.getDepth() == 0) {
			parentMenu.add(action);
		} else {

			for (int i = 0; i < category.getDepth(); i++) {
				if (!(menuManagersMap.containsKey(category.getElementToDepth(i)
						.getMenuPath()))) {
					subMenu = new MenuManager(category.getElement(i)
							.getLocalPart(), category.getElement(i)
							.getLocalPart());
					menuManagersMap.put(category.getElementToDepth(i)
							.getMenuPath(), subMenu);

				} else {
					subMenu = (MenuManager) menuManagersMap.get(category
							.getElementToDepth(i).getMenuPath());
				}
				parentMenu.add(subMenu);

				parentMenu = subMenu;

			}

			if (subMenu.indexOf(action.getId()) == -1) {
				subMenu.add(action);
			}
		}
	}

	private void addActionToMenu(MenuManager parent, Action action) {
		parent.add(action);
	}

	public void dispose() {
		// NOP
	}

	public Menu getMenu(Control parent) {
		if (fMenu != null) {
			fMenu.dispose();
		}

		if (this.filterMenuCategory == null) {

			fMenu = new MenuManager();

			for (Action action : filterActions) {
				addActionToMenu(fMenu, action);
			}
		} else {
			// build submenus as needed
			fMenu = new MenuManager();
			int asdf = filterActions.size();

			for (int i = 0; i < asdf; i++) {
				addActionToMenu(filterMenuCategory.get(i), filterActions.get(i));
			}
		}
		fMenu.createContextMenu(parent);
		return fMenu.getMenu();
	}

	public Menu getMenu(Menu parent) {
		return null;
	}

	@Override
	public void run() {

	}
}