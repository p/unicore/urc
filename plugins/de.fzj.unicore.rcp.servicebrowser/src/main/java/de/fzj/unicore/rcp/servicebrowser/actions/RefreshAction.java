/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.bindings.TriggerSequence;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.keys.IBindingService;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

/**
 * @author demuth
 * 
 */
public class RefreshAction extends NodeAction {

	public static final String IMAGE = "refresh.png";

	public static final String REFRESH_JOB_FAMILY = "refresh job family";

	private Node[] nodes;

	public RefreshAction(IServiceViewer serviceViewer) {
		super(null);
		setViewer(serviceViewer);
		init();
	}

	public RefreshAction(Node node) {
		super(node);
		init();
	}

	protected void init() {
		setActionDefinitionId(IWorkbenchCommandConstants.FILE_REFRESH);
		String text = "Refresh";
		try {
			IBindingService bindingService = (IBindingService) PlatformUI
					.getWorkbench().getActiveWorkbenchWindow()
					.getService(IBindingService.class);
			String keyBindingText = null;

			TriggerSequence binding = bindingService
					.getBestActiveBindingFor(IWorkbenchCommandConstants.FILE_REFRESH);
			if (binding != null) {
				keyBindingText = binding.format();
			}

			if (keyBindingText != null) {
				text += '\t' + keyBindingText;
			}

		} catch (Exception e) {
			// do nothing
		}

		setText(text);
		setToolTipText("Refresh selected Item");
		setImageDescriptor(ServiceBrowserActivator.getImageDescriptor(IMAGE));

	}

	@Override
	@SuppressWarnings("unchecked")
	public void run() {
		try {

			Display d = PlatformUI.getWorkbench().getDisplay();
			d.syncExec(new Runnable() {

				public void run() {
					setImageDescriptor(ServiceBrowserActivator
							.getImageDescriptor("hourglass.png"));
					setEnabled(false);
					IStructuredSelection selection = (IStructuredSelection) getViewer()
							.getSelection();
					nodes = (Node[]) selection.toList().toArray(
							new Node[selection.size()]);
					if (nodes == null || nodes.length == 0) {
						if (getNode() != null) {
							nodes = new Node[] { getNode() };
						} else if (getViewer() != null) {
							nodes = new Node[] { getViewer().getGridNode() };
						}
					}
					for (Node node : nodes) {
						getViewer().refresh(node, true);
					}

				}

			});

			boolean isMainThread = Thread.currentThread().equals(d.getThread());
			if (isMainThread) {
				Job job = new BackgroundJob(ServiceBrowserConstants.MSG_REFRESHING) {
					@Override
					public boolean belongsTo(Object family) {
						return super.belongsTo(family) || family.equals(REFRESH_JOB_FAMILY);
					}

					@Override
					public IStatus run(IProgressMonitor monitor) {
						if (nodes == null || nodes.length == 0) {
							return Status.CANCEL_STATUS;
						}
						monitor.beginTask("refreshing", nodes.length);
						try {
							for (Node node : nodes) {
								node.refresh(new SubProgressMonitor(monitor, 1));
							}
						} finally {
							monitor.done();
						}
						setImageDescriptor(ServiceBrowserActivator
								.getImageDescriptor(IMAGE));
						setEnabled(true);
						nodes = null;
						return Status.OK_STATUS;
					}
				};
				job.setUser(false);
				// cancel all old refresh operations
				Job.getJobManager().cancel(REFRESH_JOB_FAMILY);
				job.schedule();
			} else {
				if (nodes != null && nodes.length > 0) {
					// cancel all old refresh operations
					Job.getJobManager().cancel(REFRESH_JOB_FAMILY);
					for (Node node : nodes) {
						node.refresh(null);
					}
				}
				nodes = null;
			}

		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.ERROR, "Could not refresh", e);
			setImageDescriptor(ServiceBrowserActivator
					.getImageDescriptor(IMAGE));
			setEnabled(true);
		}

	}

}
