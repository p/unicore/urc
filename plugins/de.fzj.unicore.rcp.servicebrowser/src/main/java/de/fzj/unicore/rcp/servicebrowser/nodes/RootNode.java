/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import javax.xml.namespace.QName;

import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.common.adapters.LowLevelInfo;

/**
 * @author demuth
 * 
 */
public class RootNode extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7642435676194736552L;
	INodeData data;

	/**
	 * This will be a root node of the Tree. It is invisible but has to be
	 * provided whenever the grid shall be displayed in a viewer. It's only
	 * child should be a GridNode.
	 */
	public RootNode() {
		super();
		EndpointReferenceType epr = Utils.newEPR();
		epr.addNewAddress().setStringValue(name);
		Utils.addPortType(epr, getPortType());
		setEpr(epr);
		data = new NodeData(epr);
	}

	@Override
	public void dispose() {
		// do nothing
	}

	@Override
	public INodeData getData() {
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.Node#getDetails()
	 */
	@Override
	public DetailsAdapter getDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.Node#getLowLevelAsString()
	 */
	@Override
	public String getLowLevelAsString() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.Node#getLowLevelInfo()
	 */
	@Override
	public LowLevelInfo getLowLevelInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return getPortType().toString();
	}

	/**
	 * @return the type
	 */
	public static QName getPortType() {
		return new QName("http://www.unicore.fzj.de", "Root");
	}

}
