/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.net.URI;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.ehcache.store.disk.DiskStore;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.thoughtworks.xstream.XStream;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.XStreamConfigurator;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.logmonitor.LogActivator;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.logmonitor.utils.JavaUtilLogHandler;
import de.fzj.unicore.rcp.logmonitor.utils.Log4JEclipseLogAppender;
import de.fzj.unicore.rcp.logmonitor.utils.LogListener;
import de.fzj.unicore.rcp.servicebrowser.actions.DoubleClickAction;
import de.fzj.unicore.rcp.servicebrowser.actions.RefreshAction;
import de.fzj.unicore.rcp.servicebrowser.exceptions.GridNodeDeserializationException;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IGridBrowserFilterExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.nodes.EHPropertyCache;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeData;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeDataCache;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeDataCache;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.rcp.servicebrowser.ui.DefaultFilterSet;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;
import eu.unicore.security.wsutil.client.authn.DelegationSpecification;
import eu.unicore.util.Log;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author Bastian Demuth, Valentina Huber
 */
public class ServiceBrowserActivator extends AbstractUIPlugin implements LogListener {

	/**
	 * The plug-in ID
	 */
	public static final String PLUGIN_ID = "de.fzj.unicore.rcp.servicebrowser";
	public final static String ICONS_PATH = "$nl$/icons/";//$NON-NLS-1$


	/**
	 * The shared instance
	 */
	private static ServiceBrowserActivator plugin;

	// The root node of the Grid.
	private GridNode gridNode = null;
	
	private XStreamConfigurator xstreamConfigurator;
	/**
	 * used to store monitored entries in the DialogSettings
	 */
	public static final String MONENTRIES = ServiceBrowserConstants.DIALOG_SETTINGS_MONITORED_ENTRY_NAMES;
	public static final String MONENTRY_EPRS = ServiceBrowserConstants.DIALOG_SETTINGS_MONITORED_ENTRY_EPRS;

	/**
	 * The constructor
	 */
	public ServiceBrowserActivator() {
		plugin = this;
	}

	public Set<String> getDefaultDisplayedServiceTypes(IServiceViewer viewer) {

		Set<String> result = new HashSet<String>();
		result.addAll(DefaultFilterSet.TYPES_DEFAULT_WITHOUT_EXTENSIONS);
		/*
		 * go over all registered extensions and collect the filters in the two
		 * lists filterActions and filterMenuCategory
		 */
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
		.getExtensionPoint(ServiceBrowserConstants.GRIDBROWSER_FILTER_EXTENSION_POINT);
		IConfigurationElement[] members = extensionPoint
		.getConfigurationElements();
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			
			try {
				IGridBrowserFilterExtensionPoint ext = (IGridBrowserFilterExtensionPoint) member
				.createExecutableExtension("name");
				String[] defaults = ext.getDefaultNodeTypes(viewer);
				if (defaults != null && defaults.length > 0) {
					for (String s : defaults) {
						if (s != null) {
							result.add(s);
						}
					}
				}
			} catch (CoreException ex) {
				ServiceBrowserActivator.log(
						"Could not determine all default service types", ex);
			}
		}
		return result;
	}

	public synchronized GridNode getGridNode() {

		if (gridNode != null) {
			return gridNode;
		}
		try {
			Display d = PlatformUI.getWorkbench().getDisplay();
			if (d.isDisposed()) {
				return null;
			}
			d.syncExec(new Runnable() {
				public void run() {
					// deserialize the persisted grid
					// 1. try to load the persisted String
					try {
						gridNode = loadGrid();
						gridNode.afterDeserialization();
					} catch (FileNotFoundException e) {
						gridNode = initGridNodeAndBookmarks();
					} catch (Exception e) {
						// sth. went wrong or only monitored entries (bookmarks)
						// were persisted
						// restore grid with this information
						gridNode = initGridNodeAndBookmarks();
						log(IStatus.WARNING,
								"Unable to deserialize cached Grid service information, keeping bookmarks only.",
								new GridNodeDeserializationException(e));
					}

				}
			});
		} catch (IllegalStateException e) {
			// workbench was not ready, yet.. try again later
		}

		refreshGridAtStartup();

		return gridNode;
	}

	public IPath getPersistenceRoot() {
		return getStateLocation().append("PersistedGrid");
	}

	public IClientConfiguration getUASSecProps(EndpointReferenceType epr) {

		// load persisted grid
		getGridNode();
		IClientConfiguration result = null;
		SecuredNode n;
		try {
			n = (SecuredNode) NodeFactory.createNode(epr);
			result = n.getUASSecProps();
			n.dispose();
		} catch (Exception e) {
			if (result == null) {
				try{
					result = IdentityActivator.getDefault().getClientConfiguration(epr, DelegationSpecification.STANDARD);
				}catch(Exception ex){
					// ?? TODO
				}
			}
		}
		return result;
	}

	public IClientConfiguration getUASSecProps(URI uri) {
		IClientConfiguration result = null;
		try {
			EndpointReferenceType epr = NodeFactory.getEPRForURI(uri);
			result = getUASSecProps(epr);
		} catch (Exception e) {
			// ?? TODO
		}
		return result;
	}

	private GridNode initGridNodeAndBookmarks() {
		GridNode gridNode = NodeFactory.createNewGridNode();
		// monitored entries
		IDialogSettings s = getDialogSettings();
		String[] eprs = s.getArray(MONENTRY_EPRS);
		if (eprs != null) {
			for (int i = 0; i < eprs.length; i++) {
				try {
					String name = s.getArray(MONENTRIES)[i];
					EndpointReferenceType epr = EndpointReferenceType.Factory
					.parse(eprs[i]);
					Node node = NodeFactory.createNode(epr);
					if (name != null) {
						node.setName(name);
					}

					// add to model
					gridNode.addBookmark(node);

				} catch (Exception e) {
					log("Could not restore saved Grid root node!", e);
				}
			}
		}
		gridNode.setExpanded(true);
		return gridNode;
	}

	@Override
	protected void loadDialogSettings() {
		super.loadDialogSettings();
	}

	private GridNode loadGrid() throws Exception {
		InputStream is = null;
		try {
			// deserialize!
			File file = new File(getPersistenceRoot().append(
					ServiceBrowserConstants.FILENAME_PERSISTED_NODES)
					.toOSString());
			is = new FileInputStream(file);
			XStream xstream = getXStreamConfigurator().getXStreamForDeserialization();


			GridNode gridNode = (GridNode) xstream.fromXML(is);
			if (!Constants.CURRENT_CLIENT_VERSION.equals(gridNode.modelVersion)) {
				throw new Exception(
						"Using an old workspace with a newer client. Discarding old persisted Grid data.");
			}
			gridNode.getEpr().toString();
			return gridNode;
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (Exception e) {
				// do nothing
			}
		}
	}

	private XStreamConfigurator getXStreamConfigurator()
	{
		if(xstreamConfigurator == null)
		{
			xstreamConfigurator = new XStreamConfigurator() {

				@Override
				protected void log(int status, String msg, Exception ex) {
					ServiceBrowserActivator.log(status, msg, ex);
				}

				@Override
				protected String[] getExtensionIDs() {
					return new String[]{ServiceBrowserConstants.NODE_EXTENSION_POINT};
				}
			};
		}
		return xstreamConfigurator;
	}

	private void refreshGridAtStartup() {
		int numLevels = getPreferenceStore().getInt(
				ServiceBrowserConstants.P_NUM_LEVELS_ON_STARTUP);
		refreshGridAtStartup(numLevels);
	}

	private void refreshGridAtStartup(final int numLevels) {

		// refresh Grid!
		if (gridNode != null && numLevels > 0) {
			Job job = new BackgroundJob("refreshing Grid") {
				@Override
				public IStatus run(IProgressMonitor monitor) {
					IStatus status = getGridNode().refresh(numLevels, true,
							monitor);
					return status;
				}
			};
			job.setUser(false);
			// Doing the refresh immediately can lead to a deadlock,
			// therefore we'll only do it after 5s, which seems to work well
			job.schedule();
		}
	}

	public void saveGrid(GridNode gridNode) {
		if (gridNode == null) {
			return;
		}
		saveMonitoredEntries(gridNode);

		try {
			Job.getJobManager().cancel(RefreshAction.REFRESH_JOB_FAMILY);

			Job.getJobManager().cancel(
					DoubleClickAction.DOUBLE_CLICK_JOB_FAMILY);
			// persist the grid
			// 1. count how many node objects in the grid browser
			// share the same node data object.
			// node data objects that do not have a corresponding node
			// in the grid browser will not be persisted
			Queue<Node> nodes = new LinkedBlockingQueue<Node>();

			gridNode.beforeSerialization();
			nodes.add(gridNode);
			
			try {
				while (!nodes.isEmpty()) {
					Node n = nodes.poll();

					if (n.isPersistable()) {
						// trick: use negative numbers for counting
						// => all node data objects that have positive
						// node counts are not represented by any node in
						// the grid browser and will not be persisted
						int count = -1;
						int oldCount = n.getCache().getNodeCount(n.getEpr());
						if (oldCount < 0) {
							count = oldCount - 1;
						}
						n.getCache().setNodeCount(n.getEpr(), count);
						nodes.addAll(n.getChildren());
					} else {
						if (n.getParent() != null) {
							INodeData parentData = n.getParent().getData();
							INodeData childData = n.getData();
							if (parentData != null && childData != null) {
								parentData.removeChild(childData);
							}
							if (n.getParent() != null) {
								n.getParent().removeChild(n);
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			// 2. set node count of all node data objects to be
			// persisted to positive values; remove all unused node data objects
			INodeDataCache cache = gridNode.getCache();
			List<INodeData> data = cache.getAllNodeData();
			for (INodeData nodeData : data.toArray(new INodeData[data.size()])) {
				EndpointReferenceType epr = nodeData.getEpr();
				int count = cache.getNodeCount(epr);
				if (count < 0) {
					cache.setNodeCount(epr, -1 * count);
				} else {
					cache.removeNodeData(epr);
				}
			}
			cache.flushToDisk();
			// 3. now serialize the whole object tree to a file!
			XStream xstream = getXStreamConfigurator().getXStreamForSerialization();
			IPath path = getPersistenceRoot();
			File parent = path.toFile();
			if (!parent.exists()) {
				boolean created = parent.mkdir();
				if (!created) {
					log(IStatus.WARNING, "Could not save grid to harddisk.",
							new Exception("Unable to create directory "
									+ parent.getAbsolutePath()));
					return;
				}
			}
			File file = new File(path.append(
					ServiceBrowserConstants.FILENAME_PERSISTED_NODES)
					.toOSString());
			if (!file.exists()) {
				boolean created = file.createNewFile();
				if (!created) {
					log(IStatus.WARNING,
							"Could not save grid to harddisk.",
							new Exception("Unable to create file"
									+ file.getAbsolutePath()));
					return;
				}
			}
			OutputStream os = new FileOutputStream(file);
			try {
				try {
					xstream.toXML(gridNode, os);
				} catch (ConcurrentModificationException e) {
					e.printStackTrace();
				}

			} finally {
				os.close();
			}
			
			gridNode.afterSerialization();

		} catch (Throwable t) {
			t.printStackTrace();
			log(IStatus.WARNING, "Could not save grid to harddisk.",
					new Exception(t));
		}
	}

	public void saveMonitoredEntries(GridNode gridNode) {
		if (gridNode == null) {
			return;
		}
		IDialogSettings s = getDialogSettings();
		// save monitored entries
		Node[] entries = gridNode.getChildrenArray();// are the monitored
		// entries
		String[] names = new String[entries.length];
		String[] eprs = new String[entries.length];
		for (int i = 0; i < entries.length; i++) {
			names[i] = entries[i].getName();
			eprs[i] = entries[i].getEpr().xmlText();
		}
		s.put(ServiceBrowserActivator.MONENTRIES, names);
		s.put(ServiceBrowserActivator.MONENTRY_EPRS, eprs);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		
		// some nodes may change DURING sorting which may violate the contract
		// of the compareTo method.. this is hard to avoid, so we switch to old
		// sorting method which silently ignores this..
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");

		// make sure log4j logs to our log file instead of logging to the
		// console or some other place
		Logger l = Logger.getLogger(DiskStore.class);
		l.addAppender(new Log4JEclipseLogAppender(this));

		// same for java util based logging
		java.util.logging.Logger utilLogger = java.util.logging.Logger.getLogger("");
		utilLogger.addHandler(new JavaUtilLogHandler(this));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		saveGrid(getGridNode());
		plugin = null;
		gridNode = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static ServiceBrowserActivator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH + path);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log.
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log. Provide your own message, as the message
	 *            from the exception will be extracted automatically
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log. Provide your own message, as the message
	 *            from the exception will be extracted automatically
	 * @param e
	 *            stacktrace to append
	 * @param forceAlarmUser
	 *            forces the logging plugin to open a popup; usually, this
	 *            should NOT be set to true!
	 */
	public static void log(int status, String msg, Exception e,
			boolean forceAlarmUser) {
		final String finalMessage = (e==null) ? msg : Log.createFaultMessage(msg, e);
		final IStatus s = new Status(status, PLUGIN_ID, IStatus.OK, finalMessage, e);
		LogActivator.log(plugin, s, forceAlarmUser);
	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 */
	public static void log(String msg) {
		log(msg, null);
	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log. Provide your own message, as the message
	 *            from the exception will be extracted automatically
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(String msg, Exception e) {
		log(IStatus.INFO, msg, e, false);
	}

	public void notifyLogged(int level, String msg, Throwable t) {
		// deal with corrupt Ehcache for storing node properties
		// this is a total hack, however, we have no choice, cause DiskStore
		// eats our Exceptions
		if (t instanceof EOFException || t instanceof StreamCorruptedException) {
			Pattern p = Pattern
			.compile("(.*)Cache: Could not read disk store element for key (.*?)\\. Error was.*");
			Matcher m = p.matcher(msg);
			if (m.matches()) {
				if (getDefault().gridNode != null) {
					INodeDataCache cache = getDefault().gridNode.getCache();

					if (cache instanceof NodeDataCache) {
						NodeDataCache ndc = (NodeDataCache) cache;
						if (ndc.getPropertyCache() instanceof EHPropertyCache) {
							((EHPropertyCache) ndc.getPropertyCache())
							.clearAllProperties();
							log(IStatus.ERROR,
									"The cached data about some Grid services has become corrupted. This is probably due to an unclean shutdown. Trying to clear and refresh the client's cache.");
							int numLevels = ServiceBrowserActivator
							.getDefault()
							.getPreferenceStore()
							.getInt(ServiceBrowserConstants.P_NUM_LEVELS_ON_STARTUP);
							if (numLevels == 0) {
								numLevels = 5;
							}
							ServiceBrowserActivator.getDefault()
							.refreshGridAtStartup(numLevels);
						}
					}
				}

				return; // do NOT log these, the user can't deal with them
				// anyway
			}
		}
		else if (msg != null && msg.startsWith("Data in persisted disk stores are ignored")) {
			return;
		}
		// silently ignore the rest of this low level stuff..
		//		if(level >= Status.WARNING)
		//		{
		//			if(t instanceof Exception)
		//			{
		//				ServiceBrowserActivator.log(level,msg,(Exception) t);
		//			}
		//			else if(t != null)
		//			{
		//				ServiceBrowserActivator.log(level,msg,new Exception(t));
		//			}
		//			else 
		//			{
		//				ServiceBrowserActivator.log(level,msg,null);
		//			}
		//		}
	}

}
