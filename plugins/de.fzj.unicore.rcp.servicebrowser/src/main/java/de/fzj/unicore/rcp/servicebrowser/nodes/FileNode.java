/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.dnd.TransferData;
import org.unigrids.services.atomic.types.GridFileType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.servicebrowser.actions.DownloadFileAction;
import de.fzj.unicore.rcp.servicebrowser.metadata.FileMetadataAdapter;

/**
 * @author demuth
 * 
 */
public class FileNode extends AbstractFileNode {

	private static final long serialVersionUID = 6743855725485283166L;

	public static final String TYPE = "File";
	
	public static final QName PORTTYPE = new QName("http://www.unicore.fzj.de",	TYPE);


	public FileNode(EndpointReferenceType epr) {
		super(epr);
	}

	/**
	 * Delegates pasting to the parent folder/storage
	 */
	@Override
	public boolean canPaste(Object toPaste, TransferData data, int operation) {
		if (getParent() != null) {
			return getParent().canPaste(toPaste, data, operation);
		} else {
			return super.canPaste(toPaste, data, operation);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.Node#doubleClick()
	 */
	@Override
	public IStatus doubleClick(IProgressMonitor monitor) {
		new DownloadFileAction(this).run();
		return Status.OK_STATUS;
	}

	@Override
	public String getDoubleClickActionName() {
		return "downloading file";
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	/**
	 * Delegates pasting to the parent folder/storage
	 */
	@Override
	public void paste(Object toPaste, TransferData data, int operation) {
		if (getParent() != null) {
			getParent().paste(toPaste, data, operation);
		} else {
			super.paste(toPaste, data, operation);
		}
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		availableActions.add(new DownloadFileAction(this));
	}

	@Override
	public void retrieveName() {
		GridFileType gft = getGridFileType();
		if (gft != null) {
			String path = gft.getPath();
			setName(path.substring(path.lastIndexOf("/") + 1));
		} else {
			super.retrieveName();
		}
	}

	/**
	 * @return the type
	 */
	public static QName getPortType() {
		return PORTTYPE;
	}

	/**
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.Node#getAdapter(java.lang.Class)
	 */
	@Override
	public Object getAdapter(Class key) {
		if (key.equals(FileMetadataAdapter.class)) {
			try {
				return new FileMetadataAdapter(this);
			} catch (Exception e) {
				return null;
			}
		}
		return super.getAdapter(key);
	}
}
