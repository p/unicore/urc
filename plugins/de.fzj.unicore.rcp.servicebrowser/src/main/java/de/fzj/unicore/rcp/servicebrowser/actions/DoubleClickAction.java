/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.Job;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * @author demuth
 * 
 */
public class DoubleClickAction extends NodeAction {

	private boolean enabled;

	public static final String DOUBLE_CLICK_JOB_FAMILY = "double click job family";

	public DoubleClickAction(Node node) {
		super(node);
		setText("Refresh");
		setToolTipText("Refresh Selected Item");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("refresh.png"));
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void run() {
		final Node node = getNode();
		final String actionName = node.getDoubleClickActionName();
		Job job = new BackgroundJob(actionName) {
			@Override
			public boolean belongsTo(Object family) {
				return super.belongsTo(family) || family.equals(DOUBLE_CLICK_JOB_FAMILY);
			}

			@Override
			public IStatus run(IProgressMonitor monitor) {
				return node.doubleClick(monitor);
			}

		};
		// // cancel all old refresh operations
		// Job.getJobManager().cancel(DOUBLE_CLICK_JOB_FAMILY);
		job.setUser(false);
		job.schedule();

	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
