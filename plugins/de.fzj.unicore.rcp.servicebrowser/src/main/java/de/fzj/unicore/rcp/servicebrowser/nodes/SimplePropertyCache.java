package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SimplePropertyCache implements IPropertyCache {

	protected Map<String, Map<String, Object>> storage;

	public void clearAllProperties() {
		getStorage().clear();
	}

	public void clearPropertiesFor(INodeData data) {
		getStorage().remove(getKeyForData(data));

	}

	public void flushToDisk() throws IOException {
		// do nothing: our properties are just persisted when we get persisted
		// with XStream

	}

	private String getKeyForData(INodeData data) {
		try {
			return NodeDataCache.getKey(data.getEpr());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Object getProperty(INodeData data, String key) {
		Map<String, Object> properties = getStorage().get(getKeyForData(data));
		Object result = properties == null ? null : properties.get(key);

		return result;
	}

	protected Map<String, Map<String, Object>> getStorage() {
		if (storage == null) {
			storage = new HashMap<String, Map<String, Object>>();
		}
		return storage;
	}

	public void setProperty(INodeData data, String key, Object value) {
		Map<String, Object> properties = getStorage().get(getKeyForData(data));
		if (properties == null) {
			properties = new HashMap<String, Object>();
		}
		properties.put(key, value);
		getStorage().put(getKeyForData(data), properties);

	}
}
