package de.fzj.unicore.rcp.servicebrowser.utils;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.httpclient.util.URIUtil;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.part.PluginTransferData;
import org.unigrids.services.atomic.types.GridFileType;
import org.unigrids.services.atomic.types.ProtocolType;
import org.unigrids.x2006.x04.services.fts.SummaryType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.uas.client.StorageClient;
import de.fzj.unicore.uas.client.TransferControllerClient;

public class RemoteFileUtils {

	public static PluginTransferData fileArrayToPluginTransferData(
			Object[] sources) {
		String result = "";
		for (int i = 0; i < sources.length; i++) {
			AbstractFileNode node = (AbstractFileNode) sources[i];
			URI uri = node.getURI();
			result += i > 0 ? "," + uri.toString() : uri.toString();
		}

		try {
			PluginTransferData data = new PluginTransferData(
					"de.fzj.unicore.rcp.servicebrowser.dnd.dropFilesAction",
					result.getBytes("UTF-8"));
			return data;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}



	public static void serverToServerFileTransfer(
			AbstractFileNode[] sourceNodes, Node target,
			boolean moveIfSameStorage, IProgressMonitor monitor)
	throws Exception {
		serverToServerFileTransfer(sourceNodes, target, moveIfSameStorage,
				UnicoreStorageTools.RENAME, monitor);
	}


	public static void serverToServerFileTransfer(
			AbstractFileNode[] sourceNodes, Node target,
			boolean moveIfSameStorage, int mode, IProgressMonitor monitor)
	throws Exception {
		serverToServerFileTransfer(sourceNodes, target, moveIfSameStorage,
				mode, null, monitor);
	}

	public static void serverToServerFileTransfer(
			AbstractFileNode[] sourceNodes, Node target,
			boolean moveIfSameStorage, int mode, String protocol, IProgressMonitor monitor)
	throws Exception {
		String[] sources = new String[sourceNodes.length];
		for (int i = 0; i < sources.length; i++) {
			sources[i] = sourceNodes[i].getFileAddressWithoutProtocol()
			.toString();
		}
		serverToServerFileTransfer(sources, target, moveIfSameStorage, mode, protocol,
				monitor);
	}


	public static void serverToServerFileTransfer(String[] sources,
			Node target, boolean moveIfSameStorage, int mode, 
			IProgressMonitor monitor) throws Exception {
		serverToServerFileTransfer(sources, target, moveIfSameStorage, mode, null, monitor);
	}

	public static void serverToServerFileTransfer(String[] sources,
			Node target, boolean moveIfSameStorage, int mode, String protocol,
			IProgressMonitor monitor) throws Exception {
		serverToServerFileTransfer(sources, target, moveIfSameStorage, mode, protocol, null, monitor);
	}

	public static void serverToServerFileTransfer(String[] sources,
			Node target, boolean moveIfSameStorage, int mode, String protocol, Date startTime,
			IProgressMonitor monitor) throws Exception {
		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		StorageClient targetStorageClient = null;
		String targetParentPath = null;
		Node targetParent = target;
		if (target instanceof FolderNode) {
			FolderNode node = (FolderNode) target;
			targetStorageClient = node.getStorageClient();
			targetParentPath = node.getFileAddressWithoutProtocol().toString();
			targetParentPath = targetParentPath.substring(targetParentPath.indexOf("#") + 1);
			if (!targetParentPath.endsWith("/")) {
				targetParentPath += "/";
			}
		} else if (target instanceof StorageNode) {
			StorageNode node = (StorageNode) target;
			targetStorageClient = node.getStorageClient();
			targetParentPath = "";
		} 
		
		if (targetParentPath != null) {

			ConcurrentLinkedQueue<Job> jobs = new ConcurrentLinkedQueue<Job>();
			final ConcurrentLinkedQueue<Job> running = new ConcurrentLinkedQueue<Job>();
			long totalSize = 0;
			List<Long> sizes = new ArrayList<Long>();
			for (final String source : sources) {
				long size = obtainSize(source);
				totalSize += size;
				sizes.add(size);
			}
			int totalEffort = ProgressUtils.calculateEffort(totalSize);
			monitor.beginTask("transferring", totalEffort);
			Result r = new Result();
			try {
				for (int i = 0; i < sources.length; i++) {
					String source = sources[i];
					long size = sizes.get(i);

					Job j = createJob(source, targetStorageClient, targetParentPath,
							moveIfSameStorage, mode, protocol, startTime, monitor, totalSize, size, running,r);
					jobs.add(j);
				}
				
				while(jobs.size() > 0 && !monitor.isCanceled() && r.exceptions.isEmpty())
				{
					while(running.size() < 10 && jobs.size() > 0 && !monitor.isCanceled() && r.exceptions.isEmpty())
					{
						Job j = jobs.poll();
						j.schedule();
						running.add(j);
					}
					Thread.sleep(1000);
					
				}
				while(running.size() > 0)
				{
					if(!r.exceptions.isEmpty())
					{
						monitor.setCanceled(true);
						break;
					}
					Thread.sleep(1000);
				}
				
				targetParent.refresh(1, false, null);
				if(!r.exceptions.isEmpty())
				{
					Exception e = r.exceptions.poll();
					throw e;
				}
				
			} finally {
				monitor.done();

			}
			
		}
	}
	
	private static long obtainSize(String fileAddress) throws Exception
	{
		long result = 0;
		Node n = NodeFactory.createNode(NodeFactory.getEPRForURI(new URI(fileAddress)));
		try {
			if(n instanceof AbstractFileNode)
			{
				GridFileType gft = ((AbstractFileNode) n).getGridFileType();
				if(gft != null)	result = gft.getSize();
			}
		} finally {
			if(n!= null) n.dispose();
		}
		return Math.max(1, result);
	}
	
	private static Job createJob(final String source,
			final StorageClient targetStorageClient, 
			final String targetParentPath, 
			final boolean moveIfSameStorage, 
			final int mode, final String protocol, 
			final Date startTime, 
			final IProgressMonitor parentMonitor, 
			final long totalSize, final long mySize, 
			final Queue<Job> running,
			final Result result)
	{
		
		Job j =	new BackgroundJob("transmitting...")
		{

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				Node sourceNode = null;
				if(parentMonitor.isCanceled() || monitor.isCanceled())
				{
					return Status.CANCEL_STATUS;
				}
				try {
					int hashIndex = source.indexOf("#");
					String path = source.substring(hashIndex + 1);
					int index = path.lastIndexOf("/");
					String fileName = path.substring(index + 1);
					String sourceParentPath = index > 0 ? path.substring(0, index)
							: "";
					

					String oldParentAddress = source.substring(0, hashIndex);
					if (sourceParentPath.length() > 1) {
						oldParentAddress += "#" + sourceParentPath;
					}

					EndpointReferenceType oldParentEpr = NodeFactory.getEPRForURI(new URI(oldParentAddress));

					sourceNode = NodeFactory.createNode(oldParentEpr);

					if (!sourceParentPath.endsWith("/")) {
						sourceParentPath += "/";
					}


					StorageClient sourceStorageClient = null;
					String sourceStorageServerVersion = null;
					if (sourceNode instanceof FolderNode) {
						FolderNode node = (FolderNode) sourceNode;
						sourceStorageClient = node.getStorageClient();
						sourceStorageServerVersion = node.getServerVersion();
					} else if (sourceNode instanceof StorageNode) {
						StorageNode node = (StorageNode) sourceNode;
						sourceStorageClient = node.getStorageClient();
						sourceStorageServerVersion = node.getServerVersion();
					} else
						throw new Exception(
								"Could not create storage client for source file "
								+ source + ".");
					if(sourceStorageServerVersion == null) sourceStorageServerVersion = "1.3.0";
					boolean onSameStorage = sourceStorageClient.getUrl().equalsIgnoreCase(
							targetStorageClient.getUrl()); // are source and target
					// located on the same
					// storage?

					if (onSameStorage && moveIfSameStorage) {
						String s = sourceParentPath + fileName;
						String t = targetParentPath + fileName;
						// perform a move
						if (!s.equals(t)) {
							sourceStorageClient.rename(s, t);
						}
					} else {

						String prot = protocol;
						if(prot == null)
						{
							ProtocolType.Enum[] prots = UnicoreStorageTools.getPreferredProtocolOrder();
							prot = prots.length == 1 ? prots[0].toString() : Constants.FILE_TRANSFER_PROTOCOL_AUTO[1];
						}
						String s = sourceParentPath + fileName;
						String t = targetParentPath + fileName;
					

						if (UnicoreStorageTools.isSet(mode,
								UnicoreStorageTools.RENAME)) {
							t = UnicoreStorageTools.findUniquePath(targetStorageClient, t);
						}

						if(parentMonitor.isCanceled() || monitor.isCanceled())
						{
							return Status.CANCEL_STATUS;
						}
						TransferControllerClient tcc = null;
						Map<String,String> params = new HashMap<String, String>();
						if(startTime != null)
						{
							SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
							params.put("scheduledStartTime", format.format(startTime));
						}
						if("1.4.1".compareTo(sourceStorageServerVersion) > 0)
						{
							// deal with special characters in filenames!
							t = URIUtil.decode(t);
							tcc = targetStorageClient.fetchFile(prot + ":" + source, t, params);
						}
						else
						{
							// deal with special characters in filenames!
							s = URIUtil.decode(s);
							t = URIUtil.encode(t,org.apache.commons.httpclient.URI.allowed_fragment); 
							tcc = sourceStorageClient.sendFile(s, prot
									+ ":" + targetStorageClient.getUrl() + "#" + t,params);
						}

						if(startTime != null)
						{
							// the actual file transfer is scheduled for later
							// we're done here
							return Status.OK_STATUS;
						}
						tcc.setUpdateInterval(-1);
						long size = tcc.getSize();
						while (size < 0) {
							// we've asked too early
							size = tcc.getSize();
							if (tcc.hasFailed()) {
								break;
							}
						}

						double factor = size == 0 ? 0 : mySize/size;
						int effort = UnicoreStorageTools.calculateEffort(size);
						int totalDone = 0;
						int totalDoneForParent = 0;
						monitor.beginTask("", effort);
						long lastTransferredBytes = 0;
						long sleepInterval = (long) Math.max(1000,Math.min(5000,size/20000d));
						try {
							SummaryType.Enum status = tcc.getStatusSummary();
							while (!parentMonitor.isCanceled() && !monitor.isCanceled() && !SummaryType.DONE.equals(status)
									&& !SummaryType.FAILED.equals(status)) {
								long totalTransferredBytes = tcc
								.getTransferredBytes();
								
								int done = UnicoreStorageTools.calculateEffort(
										totalTransferredBytes, size);
								int diff = done - totalDone;
								int doneForParent = UnicoreStorageTools.calculateEffort(
										(long) (totalTransferredBytes*factor), mySize);
								int diffForParent = doneForParent-totalDoneForParent;
								double kbPerSec = (totalTransferredBytes - lastTransferredBytes) / sleepInterval;
								lastTransferredBytes = totalTransferredBytes;
								DecimalFormat df = new DecimalFormat("0");
								String msg = "Transferring file at "
									+ df.format(kbPerSec) + " kB/sec";
								monitor.setTaskName(msg);
								monitor.worked(diff);
								totalDone = done;
								
								if(mySize == totalSize)
								{
									// this is the only transfer
									parentMonitor.setTaskName(msg);
								}
								parentMonitor.worked(diffForParent);
								totalDoneForParent = doneForParent;
								
								Thread.sleep(sleepInterval);
								status = tcc.getStatusSummary();

							}

							if (SummaryType.FAILED.equals(status)) {
								ServiceBrowserActivator.log(IStatus.ERROR,
										tcc.getStatus());
								throw new Exception("File transfer has failed: "+tcc.getStatus());
							}
							
							
						} finally {
							if(tcc != null )
							{
								try {
									tcc.destroy();
								} catch (Throwable e) {	
								}
							}
							monitor.done();
							int diff = ProgressUtils.calculateEffort(mySize,totalSize) - totalDoneForParent;
							if(diff > 0) parentMonitor.worked(diff);
						}
					}
				}
				catch (Exception e) {
					result.exceptions.add(e);
					return Status.CANCEL_STATUS;

				} finally {
					if (sourceNode != null) {
						sourceNode.refresh(1, false, null);
						sourceNode.dispose();
					}
					running.remove(this);
				}
				return Status.OK_STATUS;
			}
		};
//		j.setUser(false);
		return j;
		
	}

	public static void serverToServerFileTransfer(String[] sources,
			Node target, boolean moveIfSameStorage, IProgressMonitor monitor)
	throws Exception {
		serverToServerFileTransfer(sources, target, moveIfSameStorage,
				UnicoreStorageTools.RENAME, monitor);
	}

	private static class Result
	{
		Queue<Exception> exceptions = new ConcurrentLinkedQueue<Exception>();
	}
}
