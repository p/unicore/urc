package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.ui.ChangeGroupDialog;

public class ChangeGroupAction extends NodeAction {

	protected String group;
	protected boolean recursive, apply;

	public ChangeGroupAction(AbstractFileNode node) {
		super(node);
		setText("Change Group");
		setToolTipText("Set owning group of the remote file.");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("file_permissions.png"));
	}

	@Override
	public void run() {
		final AbstractFileNode fileNode = (AbstractFileNode) getNode();
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {
				Shell s = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getShell();
				ChangeGroupDialog dialog = new ChangeGroupDialog(s, fileNode);
				int result = dialog.open();
				group = dialog.getNewGroupName();
				recursive = dialog.getRecursiveFlag();
				apply = (Window.OK == result  &&  dialog.isGroupChanged());
			}
		});
		if (apply) {
			try {
				fileNode.getStorageClient().chgrp(fileNode.getGridFileType().getPath(), group, recursive);
			} catch (Exception e) {
				ServiceBrowserActivator.log(
						IStatus.ERROR,
						"Unable to change owing group of the remote file", e);
			}
			fileNode.refresh(0, false, null);
		}
	}

}
