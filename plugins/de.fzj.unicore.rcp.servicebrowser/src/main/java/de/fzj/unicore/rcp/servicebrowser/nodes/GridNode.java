/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.adapters.LowLevelInfo;
import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.actions.RefreshAction;

/**
 * @author demuth
 * 
 */
public class GridNode extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4699261291712704338L;
	public static final QName PORTTYPE = new QName("http://www.unicore.fzj.de",
			"Grid");
	public static final String TYPE = PORTTYPE.toString();

	public static final String PROP_BOOKMARK_NAME_MAP = "bookmark name map";

	private static final String GRID_NAME = "Grid";

	public String modelVersion = Constants.CURRENT_CLIENT_VERSION;

	/**
	 * The Grid node is the top level visible node of the tree.
	 */
	public GridNode() {
		super();
		String address = "http://" + GRID_NAME;

		EndpointReferenceType epr = Utils.newEPR();
		epr.addNewAddress().setStringValue(address);
		Utils.addPortType(epr, getPortType());
		setEpr(epr);
	}

	public void addBookmark(Node child) throws Exception {
		INodeData data = getData();
		if (data == null) {
			return;
		}
		synchronized (data) {
			Map<String,String> bookMarkNameMap = new HashMap<String,String>(
					getBookMarkNameMap());
			if (bookMarkNameMap.get(child.getURI().toString()) != null) {
				throw new Exception("Bookmark for this URI does already exist!");
			}
			bookMarkNameMap.put(child.getURI().toString(), child.getName());

			data.setProperty(PROP_BOOKMARK_NAME_MAP, bookMarkNameMap, this);
			addChild(child);
			data.addChild(child.getData());
		}
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();

		INodeData data = getData();
		if (data == null) {
			return;
		}
		synchronized (data) {
			// make sure bookmark list is consistent with list of persisted data
			// children
			Map<String, String> bookMarkNameMap = new HashMap<String,String>(
					getBookMarkNameMap());
			Map<URI, INodeData> dataChildren = data.getChildrenMap();
			for (String s : bookMarkNameMap.keySet().toArray(new String[0])) {
				URI uri = null;
				try {
					uri = new URI(s);
				} catch (URISyntaxException e) {
					uri = null;
				}
				if (uri != null && !dataChildren.containsKey(uri)) {
					bookMarkNameMap.remove(s);
				}

			}
			for (URI uri : dataChildren.keySet().toArray(new URI[0])) {
				String key = uri.toString();
				if (!bookMarkNameMap.containsKey(key)) {
					try {
						String name = dataChildren.get(uri).getAllNames()
								.iterator().next();
						if (name != null) {
							bookMarkNameMap.put(key, name);
						}
					} catch (Exception e) {
						// do nothing?
					}
				}
			}
			data.setProperty(PROP_BOOKMARK_NAME_MAP, bookMarkNameMap, this);
		}
		INodeDataCache cache = getCache();
		if (cache != null) {
			cache.afterDeserialization();
		}
	}

	@Override
	public void beforeSerialization() {
		super.beforeSerialization();
		INodeDataCache cache = getCache();
		if (cache != null) {
			cache.beforeSerialization();
		}
	}
	
	
	@Override
	public void afterSerialization() {
		super.afterSerialization();
		INodeDataCache cache = getCache();
		if (cache != null) {
			cache.afterSerialization();
		}
	}

	@Override
	public IStatus doubleClick(IProgressMonitor monitor) {
		setExpanded(true);
		IStatus s = refresh(2, true, monitor);
		return s;
	}

	@Override
	public void finalize() {

	}

	@SuppressWarnings("unchecked")
	private Map<String, String> getBookMarkNameMap() {
		INodeData data = getData();
		Object o = null;
		if (data != null) {
			o = data.getProperty(PROP_BOOKMARK_NAME_MAP);
		}
		if (o == null) {
			Map<String, String> map = new HashMap<String, String>();
			data.setProperty(PROP_BOOKMARK_NAME_MAP, map, this);
			return map;
		} else {
			return (Map<String, String>) o;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.Node#getLowLevelAsString()
	 */
	@Override
	public String getLowLevelAsString() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.Node#getLowLevelInfo()
	 */
	@Override
	public LowLevelInfo getLowLevelInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void init() {
		super.init();
		setName(GRID_NAME);
	}

	@Override
	public IStatus refresh(IProgressMonitor monitor) {
		return super.refresh(5, true, monitor);
	}

	public void removeBookmark(Node child) {
		INodeData data = getData();
		if (data == null) {
			return;
		}
		synchronized (data) {
			data.removeChild(child.getData());
			Map<String,String> bookMarkNameMap = new HashMap<String,String>(getBookMarkNameMap());
			bookMarkNameMap.remove(child.getURI().toString());
			data.setProperty(PROP_BOOKMARK_NAME_MAP, bookMarkNameMap, this);
		}
	}

	@Override
	public void resetAvailableActions() {
		availableActions = new ArrayList<NodeAction>();
		availableActions.add(new RefreshAction(this));
	}

	@Override
	public void retrieveName() {
		// do nothing
	}

	@Override
	public void updateChildrenFromData(int depth) {
		super.updateChildrenFromData(depth);
		for (Node child : getChildrenArray()) {
			String name = getBookMarkNameMap().get(child.getURI().toString());
			if (name != null) {
				child.setName(name);
			}
		}
	}

	/**
	 * @return the type
	 */
	public static QName getPortType() {
		return PORTTYPE;
	}

}
