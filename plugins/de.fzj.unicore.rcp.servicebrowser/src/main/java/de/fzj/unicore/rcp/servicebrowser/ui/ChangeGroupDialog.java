package de.fzj.unicore.rcp.servicebrowser.ui;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;

public class ChangeGroupDialog extends Dialog {

	protected AbstractFileNode fileNode;
	protected Text groupTextField;
	protected Button recursiveButton;
	
	protected String currentGroup, newGroup;
	protected boolean recursive;

	public ChangeGroupDialog(Shell parentShell, AbstractFileNode fileNode) {
		super(parentShell);
		this.fileNode = fileNode;
		this.recursive = false;
		this.currentGroup = "-";
	}

	@Override
	public boolean close() {
		this.newGroup = groupTextField.getText();
		if (recursiveButton != null) {
			this.recursive = recursiveButton.getSelection();
		}
		return super.close();
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		recursive = (fileNode instanceof FolderNode);
		currentGroup = fileNode.getGridFileType().getGroup();
		
		
		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(1, true));

		GridData gd = new GridData(SWT.CENTER, SWT.CENTER, true, false);
		
		Label currentGrplabel = new Label(composite, SWT.NONE);
		currentGrplabel.setFont(parent.getFont());
		currentGrplabel.setText("Current group: " + currentGroup);

		Label newGrpLabel = new Label(composite, SWT.NONE);
		newGrpLabel.setText("New owing group:");
		newGrpLabel.setFont(parent.getFont());
		
		groupTextField = new Text(composite, SWT.SINGLE | SWT.CENTER);
		groupTextField.setFont(parent.getFont());
		groupTextField.setText("");
		groupTextField.setTextLimit(32);
		groupTextField.setLayoutData(gd);
		
		if (recursive)
		{
			recursiveButton = new Button(composite, SWT.CHECK | SWT.CENTER);
			recursiveButton.setFont(parent.getFont());
			recursiveButton.setText("Set recursively");
			recursiveButton.setSelection(false);			
			recursiveButton.setLayoutData(gd);
		}
		return composite;
	}

	/**
	 * Always use after close() operation
	 * 
	 * @return if group value needs to be updated
	 */
	public boolean isGroupChanged()
	{
		return (!(newGroup.equals(currentGroup) && !recursive)  &&  !newGroup.equals(""));
	}
	
	public String getNewGroupName() {
		return newGroup;
	}
	
	public boolean getRecursiveFlag()
	{
		return recursive;
	}
}
