/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.xmlbeans.SchemaType;
import org.eclipse.core.runtime.IStatus;
import org.oasisOpen.docs.wsrf.rp2.GetResourcePropertyDocumentResponseDocument;
import org.oasisOpen.docs.wsrf.sg2.EntryType;
import org.oasisOpen.docs.wsrf.sg2.ServiceGroupRPDocument;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.UAS;
import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.actions.EditURIAction;

/**
 * @author demuth
 * 
 */
public class RegistryNode extends WSRFNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4923163412586929371L;
	public static final String TYPE = UAS.REG;

	public RegistryNode(EndpointReferenceType epr) {
		super(epr);
	}

	public List<Node> getChildrenOfTypes(Set<String> types) {
		List<Node> result = new ArrayList<Node>();
		INodeData data = getData();
		if (data == null) {
			return result;
		}
		if (getChildrenArray().length != data.getChildrenArray().length) {
			try {
				retrieveChildren();
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.ERROR,
						"Could not update children of Registry node", e);
			}
		}

		if (types == null || types.size() == 0) {
			return result;
		}
		Node[] children = getChildrenArray();
		for (Node node : children) {
			if (types.contains(node.getType())) {
				result.add(node);
			}
		}
		return result;
	}

	/**
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode#getDetails()
	 */
	@Override
	public DetailsAdapter getDetails() {
		DetailsAdapter details = super.getDetails();
		details.getMap().put("URI (fallback)", getFallbackURI() != null ?
				getFallbackURI().toString() : "not specified");
		return details;
	}

	/**
	 * retrieve reg entries
	 * 
	 * @return
	 */
	public EntryType[] getEntries() {

		try {
			GetResourcePropertyDocumentResponseDocument resPropDoc = retrieveResourceProperties();
			ServiceGroupRPDocument regProps = ServiceGroupRPDocument.Factory
					.parse(resPropDoc.getGetResourcePropertyDocumentResponse()
							.newInputStream());
			return regProps.getServiceGroupRP().getEntryArray();
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.WARNING, "Cannot communicate with registry", e);
			return null;
		}
	}

	@Override
	public SchemaType getSchemaType() {
		return ServiceGroupRPDocument.type;
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void init() {
		super.init();
		INodeData data = getData();
		if (data == null) {
			return;
		}
		if (data.getRegistryNode() == null) {
			data.setRegistryNode(this);
		}
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		availableActions.add(new EditURIAction(this));
	}

	/**
	 * create the child nodes of this node
	 */
	@Override
	protected void retrieveChildren() throws Exception {
		List<Node> newChildren = new ArrayList<Node>();
		if (hasCachedResourceProperties()) {
			ServiceGroupRPDocument regProps = (ServiceGroupRPDocument) getCachedResourcePropertyDocument();
			EntryType[] allEntries = regProps.getServiceGroupRP()
					.getEntryArray();
			if (allEntries.length > 0) {
				
				for (EntryType et : allEntries) {
					try {
					
						Node node = NodeFactory.createNode(et
								.getMemberServiceEPR());
							if (!(node instanceof TargetSystemNode)) {
								newChildren.add(node);
							}
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		}
		updateChildren(newChildren);
	}

	@Override
	public void retrieveName() {
		// do nothing

	}

}
