/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.logmonitor.utils.FileDialogUtils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * @author demuth
 * 
 */
public class DownloadAndSaveFileAction extends AbstractNodeCommandHandler {


	protected Map<AbstractFileNode,File> filesToDownload;

	private boolean overwriteAll = false;

	private boolean skipAll = false;

	public DownloadAndSaveFileAction() {

	}

	protected boolean confirmAction(Shell shell)
	{
		filesToDownload = new HashMap<AbstractFileNode, File>();
		IStructuredSelection selection = getSelection();
		String filename = "";
		for(Object o : selection.toArray())
		{
			if(!(o instanceof AbstractFileNode)) continue;
			final AbstractFileNode node = (AbstractFileNode) o;

			FileDialog dialog = new FileDialog(shell, SWT.SAVE);
			String filterPath = FileDialogUtils.getFilterPath(Constants.FILE_DIALOG_ID_DOWNLOAD_DIR);
			dialog.setFilterPath(filterPath);
			dialog.setText("Please enter a filename");
			dialog.setFileName(node.getName());
			filename = dialog.open();
			if(filename == null) 
			{
				return false;
			}

			if (filename.equals("")) {
				continue;
			}
			FileDialogUtils.saveFilterPath(Constants.FILE_DIALOG_ID_DOWNLOAD_DIR, dialog.getFilterPath());
			File f = new File(filename);
			filename = null;
			if(f.exists())
			{
				if(!askOverwrite(shell, f))
				{
					continue;
				}
			}
			filesToDownload.put(node,f);
			
		}
		return true;
	}

	protected IStatus performAction(IProgressMonitor progress, Node n)
	{
		File f = filesToDownload.get(n);
		if(f == null) return Status.OK_STATUS;
		try {
			AbstractFileNode node = (AbstractFileNode) n;
			String path = node.getGridFileType().getPath();
			if(node instanceof FolderNode)
			{
				UnicoreStorageTools.downloadFolder(node.getStorageClient(), path, f, progress);
			}
			else if(node instanceof FileNode)
			{
				UnicoreStorageTools.downloadFile(node.getStorageClient(), path, f, progress);
			}
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.WARNING,
					"Cannot save file " + n.getPathToRoot(), e);
		} finally {
			progress.done();
			PathUtils.refreshEclipseFolder(new Path(f.getAbsolutePath()).removeLastSegments(1), 1, null);
		}
		return Status.OK_STATUS;

	}

	@Override
	protected String getJobName() {
		return "downloading file(s)";
	}

	@Override
	protected String getTaskName() {
		return "downloading...";
	}

	private boolean askOverwrite(Shell shell, final File f)
	{
		if(skipAll) return false;
		if(overwriteAll) return true;

		MessageDialog dialog = new MessageDialog(shell, "Overwrite file?", null, "A file named "+ f.getName()+" already exists in that folder. Overwrite?",
				MessageDialog.QUESTION_WITH_CANCEL, new String[] {
			"Overwrite","Overwrite all",
			"Skip" ,"Skip all"}, 3);
		int result = dialog.open();
		switch (result) {
		case 0:
			return true;
		case 1:
			overwriteAll = true;
			return true;
		case 2:
			return false;

		case 3:
			skipAll = true;
			return false;
		}

		return false;
	}

}
