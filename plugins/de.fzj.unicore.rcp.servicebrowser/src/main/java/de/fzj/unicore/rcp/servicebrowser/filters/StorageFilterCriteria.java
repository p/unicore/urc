package de.fzj.unicore.rcp.servicebrowser.filters;

import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;

public class StorageFilterCriteria extends StandardFilterCriteria{

	String storageURI;

	public StorageFilterCriteria(String storageURL) {
		this.storageURI = storageURL;
	}

	@Override
	public boolean fitCriteria(Node node) {
		// Check name against entered criteria
		if (storageURI == null || storageURI.trim().length() == 0) {
			return true;
		} else if(!StorageNode.TYPE.equals(node) && !(node instanceof StorageNode)) {
			return false;
		}

		return storageURI.equals(node.getURI().toString());
	}

}
