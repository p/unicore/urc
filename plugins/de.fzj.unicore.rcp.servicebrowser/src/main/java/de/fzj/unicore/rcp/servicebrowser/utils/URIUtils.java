package de.fzj.unicore.rcp.servicebrowser.utils;

import java.net.URI;

public class URIUtils {

	public static String extractUnicoreServiceContainerName(URI uri) {
		if (uri == null) {
			return null;
		}
		String path = uri.getPath();
		String[] fragments = path.split("/");
		if(fragments.length < 2) return null;
		String name = fragments[1];
		if ("services".equals(name)) {
			name = uri.getHost(); // if service is not behind a Gateway:
									// fallback to hostname
		}
		return name;
	}

	public static String toNormalizedString(URI uri) {
		// transform parts of the uri to lower case for additional robustness.
		// Be careful with paths and queries!!
		// e.g. translating queries to lower case leads to mixing up Grid Files
		// whose names only differs in case
		String scheme = uri.getScheme() == null ? "" : uri.getScheme()
				.toLowerCase();
		String auth = uri.getAuthority() == null ? "" : uri.getAuthority()
				.toLowerCase();
		String path = uri.getPath() == null ? "" : uri.getPath();
		String query = uri.getQuery() == null ? "" : "?" + uri.getQuery();
		String fragment = uri.getFragment() == null ? "" : "#"
				+ uri.getFragment();
		String result = scheme + "://" + auth + path + query + fragment;
		return result;
	}
}
