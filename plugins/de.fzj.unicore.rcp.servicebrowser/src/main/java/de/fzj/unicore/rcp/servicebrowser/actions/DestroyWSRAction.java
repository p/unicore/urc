/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;

/**
 * @author demuth
 * 
 */
public class DestroyWSRAction extends AbstractNodeCommandHandler {


	public DestroyWSRAction() {
	}

	@Override
	public boolean confirmAction(Shell shell) {
		return MessageDialog.openConfirm(shell, "Confirm action", "Do you really want to destroy the selected Grid resources?");
	}		

	protected boolean isEnabled(Node n)
	{
		if(!(n instanceof WSRFNode)) return false;
		WSRFNode node = (WSRFNode) n;
		return node.canBeDestroyed();
	}



	@Override
	protected IStatus performAction(IProgressMonitor monitor, 
			Node n) {
		
		monitor.beginTask("destroying", 1);
		try {
			if (n instanceof WSRFNode) {
				WSRFNode wsr = (WSRFNode) n;
				if (wsr.getState() != Node.STATE_DESTROYED) {
					String name = wsr.getNamePathToRoot();
					try {
						wsr.destroy();
					} catch (Exception e) {
						ServiceBrowserActivator.log(IStatus.WARNING,
								"Unable to destroy node " + name, e);
					}
				}
				
				return Status.OK_STATUS;
			}
			return Status.CANCEL_STATUS;
		}  finally {
			monitor.done();
		}
		
	}

	@Override
	protected void postAction(List<Node> selection,
			IProgressMonitor monitor) {
		//update parents after all children were deleted
		Set<Node>parents=new HashSet<Node>();
		for(Node n: selection){
			if (n instanceof WSRFNode) {
				WSRFNode wsr = (WSRFNode) n;
				Node parent=wsr.getParent();
				if (parent != null && !parents.contains(parent)) {
					parents.add(parent);
					parent.refresh(1, false, null);
				}
			}
		}
	}

	@Override
	protected String getJobName() {
		return "Destroying resource(s)";
	}



	@Override
	protected String getTaskName() {
		return "destroying resources...";
	}

}
