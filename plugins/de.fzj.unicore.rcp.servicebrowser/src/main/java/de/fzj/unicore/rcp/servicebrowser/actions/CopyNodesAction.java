package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.jface.bindings.TriggerSequence;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.keys.IBindingService;

import de.fzj.unicore.rcp.servicebrowser.dnd.NodeLocalSelectionTransfer;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

public class CopyNodesAction extends NodeAction {
	private ISelectionProvider selectionProvider;

	private Clipboard clipboard;

	public CopyNodesAction(ISelectionProvider selectionProvider,
			Clipboard clipboard) {
		super(null);
		this.selectionProvider = selectionProvider;
		this.clipboard = clipboard;
		init();
	}

	public CopyNodesAction(Node node) {
		super(node);
		init();
	}

	protected void init() {

		String text = "Copy";
		try {
			IBindingService bindingService = (IBindingService) PlatformUI
			.getWorkbench().getActiveWorkbenchWindow()
			.getService(IBindingService.class);
			String keyBindingText = null;

			TriggerSequence binding = bindingService
			.getBestActiveBindingFor(IWorkbenchCommandConstants.EDIT_COPY);
			if (binding != null) {
				keyBindingText = binding.format();
			}

			if (keyBindingText != null) {
				text += '\t' + keyBindingText;
			}

		} catch (Exception e) {
			// do nothing
		}

		setText(text);
		setToolTipText("Copy the selected element to the system clipboard");
		ISharedImages sharedImages = PlatformUI.getWorkbench()
		.getSharedImages();
		setImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_COPY));
		setDisabledImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_COPY_DISABLED));
	}

	@Override
	public void run() {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {

			public void run() {
				if (clipboard == null) {
					clipboard = new Clipboard(getViewer().getControl()
							.getDisplay());
				}
				IStructuredSelection selection = (IStructuredSelection) selectionProvider
				.getSelection();
				if(selection != null && selection.size() > 0)
				{
					selectionProvider.setSelection(null);
					NodeLocalSelectionTransfer selectionTransfer = NodeLocalSelectionTransfer
					.getTransfer();
					selectionTransfer.setSelection(selection);
					selectionTransfer.setOperation(DND.DROP_COPY);

					clipboard.setContents(new Object[] { selection },
							new Transfer[] { selectionTransfer });
				}

			}
		});
	}

	@Override
	public void setViewer(IServiceViewer viewer) {
		super.setViewer(viewer);
		this.selectionProvider = viewer;
	}

}
