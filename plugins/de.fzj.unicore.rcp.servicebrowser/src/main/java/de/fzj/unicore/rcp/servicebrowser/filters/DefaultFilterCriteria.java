/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.filters;

import java.util.HashSet;
import java.util.Set;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

/**
 * Implements the default filter for the GridBrowser
 * TODO: what is the default?
 * 
 * @author Christian Hohmann
 * 
 */
public class DefaultFilterCriteria extends StandardFilterCriteria {

	protected Set<String> defaultTypes;

	public DefaultFilterCriteria(IServiceViewer viewer) {
		defaultTypes = new HashSet<String>(ServiceBrowserActivator.getDefault()
				.getDefaultDisplayedServiceTypes(viewer));
		// remove registries here as we do not want to display all their content
		defaultTypes.remove(RegistryNode.TYPE);
	}

	public void addDefaultTypes(String[] types) {
		if (types == null) {
			return;
		}
		for (String string : types) {
			defaultTypes.add(string);
		}
	}

	@Override
	public boolean fitCriteria(Node node) {
		if (node == null) {
			return false;
		}
		String type = node.getType();
		boolean fits = defaultTypes.contains(type);
		return fits;

	}

	public void removeDefaultTypes(String[] types) {
		if (types == null) {
			return;
		}
		for (String string : types) {
			defaultTypes.remove(string);
		}
	}

}