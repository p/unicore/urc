/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.logmonitor.utils.FileDialogUtils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * @author demuth
 * 
 */
public class DownloadMultipleFilesAction extends AbstractNodeCommandHandler {

	protected String filename = "";
	
	private boolean overwriteAll = false;
	
	private boolean skipAll = false;
	
	private boolean overwrite = false;
	
	private Shell shell;

	public DownloadMultipleFilesAction() {

	}


	public Object doExecute(ExecutionEvent event) throws ExecutionException {
		shell = HandlerUtil.getActiveShell(event);
		
		ISelection sel = HandlerUtil.getCurrentSelection(event);
		if(!(sel instanceof IStructuredSelection)) return null;
		final IStructuredSelection selection = (IStructuredSelection) sel;
		
		if(!confirmAction(shell)) return null;
		
		final File parentDir = new File(filename);
		final IPath parentPath = new Path(filename);
		filename = null;
		
		Job j = new BackgroundJob(getJobName())
		{
			
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("", 100);
				try {
					SubProgressMonitor sub = new SubProgressMonitor(monitor,
							10);
					sub.beginTask("collecting file sizes", selection.size());
					List<AbstractFileNode> files = new ArrayList<AbstractFileNode>(
							selection.size());
					long totalFileSizes = 0;
					Map<AbstractFileNode, Long> fileSizes = new HashMap<AbstractFileNode, Long>();
					for (Object o : selection.toArray()) {
						if (o instanceof AbstractFileNode) {
							AbstractFileNode node = (AbstractFileNode) o;
							files.add(node);
							SubProgressMonitor subSub = new SubProgressMonitor(
									sub, 1);
							long fileSize = UnicoreStorageTools.diskUsage(node
									.getStorageClient(), node.getGridFileType()
									.getPath(), subSub);
							fileSizes.put(node, fileSize);
							totalFileSizes += fileSize;
						}

					}
					sub.done();
					int totalEffort = UnicoreStorageTools
					.calculateEffort(totalFileSizes);
					sub = new SubProgressMonitor(monitor, 90);
					sub.beginTask("transferring data", totalEffort);
					for (AbstractFileNode node : files) {
						int effort = UnicoreStorageTools.calculateEffort(
								fileSizes.get(node), totalFileSizes);
						SubProgressMonitor subSub = new SubProgressMonitor(sub,
								effort);
						File f = new File(parentDir, node.getName());
						if(f.exists())
						{
							if(!askOverwrite(f))
							{
								subSub.done();
								continue;
							}
						}
						if (node instanceof FileNode) {
							UnicoreStorageTools.downloadFile(node
									.getStorageClient(), node.getGridFileType()
									.getPath(), f, subSub);
						} else if (node instanceof FolderNode) {
							UnicoreStorageTools.downloadFolder(node
									.getStorageClient(), node.getGridFileType()
									.getPath(), f, subSub);
						}

					}
					sub.done();
					

				} catch (Exception e) {
					ServiceBrowserActivator.log(IStatus.ERROR, "Error while downloading files", e);
				} finally {
					if (parentPath != null) {
						PathUtils.refreshEclipseFolder(parentPath,
								IResource.DEPTH_INFINITE, null);
					}
					monitor.done();
				}

				
				return Status.OK_STATUS;
			}
		};
		j.schedule();
		return null;
	}

	private boolean askOverwrite(final File f)
	{
		if(skipAll) return false;
		if(overwriteAll) return true;
		shell.getDisplay().syncExec(new Runnable() {
			
			public void run() {
				DownloadMultipleFilesAction.this.overwrite = false;
				MessageDialog dialog = new MessageDialog(shell, "Overwrite file?", null, "A file named "+ f.getName()+" already exists in that folder. Overwrite?",
				MessageDialog.QUESTION_WITH_CANCEL, new String[] {
						"Overwrite","Overwrite all",
						"Skip" ,"Skip all"}, 3);
				int result = dialog.open();
				switch (result) {
				case 0:
					DownloadMultipleFilesAction.this.overwrite = true;
					break;
				case 1:
					DownloadMultipleFilesAction.this.overwrite = true;
					DownloadMultipleFilesAction.this.overwriteAll = true;
					break;
				case 2:
					DownloadMultipleFilesAction.this.overwrite = false;
					break;
				case 3:
					DownloadMultipleFilesAction.this.overwrite = false;
					DownloadMultipleFilesAction.this.skipAll = true;

				}
			}
		});
		return overwrite;
	}

	protected boolean confirmAction(Shell shell)
	{

		DirectoryDialog dialog = new DirectoryDialog(shell,
				SWT.SAVE);
		String filterPath = FileDialogUtils.getFilterPath(Constants.FILE_DIALOG_ID_DOWNLOAD_DIR);
		dialog.setFilterPath(filterPath);
		dialog.setText("Please choose a parent directory for files to download");
		filename = dialog.open();
		FileDialogUtils.saveFilterPath(Constants.FILE_DIALOG_ID_DOWNLOAD_DIR, dialog.getFilterPath());
		return filename != null && !filename.equals("");
		
	}

	@Override
	protected IStatus performAction(IProgressMonitor monitor, Node n) {
		// don't need this...
		return null;
	}


	@Override
	protected String getJobName() {
		return "downloading file(s)";
	}

	@Override
	protected String getTaskName() {
		return "downloading...";
	}

}
