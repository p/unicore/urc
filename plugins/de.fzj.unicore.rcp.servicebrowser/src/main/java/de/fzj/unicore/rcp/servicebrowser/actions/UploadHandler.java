package de.fzj.unicore.rcp.servicebrowser.actions;

import java.io.File;
import java.util.StringTokenizer;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.URIUtil;
import org.eclipse.core.internal.filesystem.local.LocalFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.internal.ide.dialogs.FileFolderSelectionDialog;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.logmonitor.utils.FileDialogUtils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.uas.client.StorageClient;

public class UploadHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPage wbPage = ServiceBrowserActivator.getDefault().getWorkbench().getActiveWorkbenchWindow()
				.getActivePage();
		ISelection sel = wbPage.getActivePart().getSite().getSelectionProvider().getSelection();
		StorageClient storageClientTmp = null;
		String targetPathTmp = "/";
		if (sel instanceof IStructuredSelection) {
			IStructuredSelection sSel = (IStructuredSelection) sel;
			if (sSel.getFirstElement() instanceof StorageNode) {
				try {
					storageClientTmp = ((StorageNode) sSel.getFirstElement()).getStorageClient();
				} catch (Exception e) {
					throw new ExecutionException("Cannot create storage client", e);
				}
			} else if (sSel.getFirstElement() instanceof FolderNode) {
				storageClientTmp = ((FolderNode) sSel.getFirstElement()).getStorageClient();
				targetPathTmp = ((FolderNode) sSel.getFirstElement()).getGridFileType().getPath();
			}
		}

		final StorageClient storageClient = storageClientTmp;
		final String targetBasePath = targetPathTmp;

		Shell shell = ServiceBrowserActivator.getDefault().getWorkbench().getActiveWorkbenchWindow().getShell();

		final FileFolderSelectionDialog ffs = new FileFolderSelectionDialog(shell, true,
				IResource.FILE | IResource.FOLDER);
		ffs.setTitle("Select files or folders for upload.");
		ffs.setMessage("Multiple selections are possible.");
		ffs.setDoubleClickSelects(false);
		try {
			ffs.setInput(EFS.getStore(URIUtil.toURI(Path.ROOT)));
			String sPath = FileDialogUtils.getFilterPath(Constants.FILE_DIALOG_ID_UPLOAD_DIR);
			if(sPath != null && !sPath.isEmpty()) {
				IPath path = new Path(sPath);
				ffs.setInitialSelection(EFS.getStore(URIUtil.toURI(path)));
			}
		} catch (CoreException e) {
			throw new ExecutionException("Unable to display selection dialog.", e);
		}
		ffs.setBlockOnOpen(true);
		if (ffs.open() == Window.OK) {
			File deepestCommonBase = null;
			for (Object o : ffs.getResult()) {
				try {
					File file = ((LocalFile) o).toLocalFile(EFS.NONE, null);
					deepestCommonBase = deepestCommonBase == null ? (file.isFile() ? file.getParentFile() : file)
							: commonParent(deepestCommonBase, file);

					BackgroundJob job = new BackgroundUploaderJob("Uploading " + file.getAbsolutePath(), file,
							storageClient, targetBasePath);
					job.schedule();
				} catch (CoreException e) {
					ServiceBrowserActivator.log("Unable to upload", e);
				}
			}
			FileDialogUtils.saveFilterPath(Constants.FILE_DIALOG_ID_UPLOAD_DIR, deepestCommonBase.getAbsolutePath());
		}
		return null;
	}

	/**
	 * @param deepestCommonBase
	 * @param file
	 * @return
	 */
	private File commonParent(File _file1, File _file2) {
		if(_file1.isFile()) {
			_file1 = _file1.getParentFile();
		}
		if(_file2.isFile()) {
			_file2 = _file2.getParentFile();
		}
		
		int pathElems1, pathElems2;
		do {
			pathElems1 = new StringTokenizer(_file1.getAbsolutePath(), "/").countTokens();
			pathElems2 = new StringTokenizer(_file2.getAbsolutePath(), "/").countTokens();
			
			if(pathElems1 > pathElems2) {
				_file1 = _file1.getParentFile();
			} else if(pathElems2 > pathElems1) {
				_file2 = _file2.getParentFile();
			} else {
				if(_file1.equals(_file2)) {
					return _file1;
				} else if(_file1.getParentFile() == null) {
					return _file1;
				} else if(_file2.getParentFile() == null) {
					return _file2;
				} else {
					_file1 = _file1.getParentFile();
					_file2 = _file2.getParentFile();
				}
			}
		} while(pathElems1 > 0 && pathElems2 > 0);
		return _file1;
	}

	private class BackgroundUploaderJob extends BackgroundJob {

		private final File file;
		private final StorageClient storageClient;
		private final String targetBasePath;

		public BackgroundUploaderJob(String _jobName, File _file, StorageClient _storageClient, String _targetBasePath) {
			super(_jobName);
			this.file = _file;
			this.storageClient = _storageClient;
			this.targetBasePath = _targetBasePath;
		}

		/**
		 * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.IProgressMonitor)
		 */
		@Override
		protected IStatus run(IProgressMonitor monitor) {
			try {
				final String targetPath = targetBasePath + "/" + file.getName();
				if (file.isDirectory()) {
					UnicoreStorageTools.uploadFolder(storageClient, file, targetPath, monitor);
				} else {
					monitor.beginTask("Uploading file " + file.getName(), (int) file.length());
					UnicoreStorageTools.uploadFile(storageClient, file, targetPath, monitor);
				}
				return Status.OK_STATUS;
			} catch (Exception e) {
				return Status.CANCEL_STATUS;
			}

		}

	}
}