/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.unigrids.services.atomic.types.PropertyType;
import org.unigrids.x2006.x04.services.smf.CreateSMSDocument;
import org.unigrids.x2006.x04.services.smf.CreateSMSDocument.CreateSMS;
import org.unigrids.x2006.x04.services.smf.StorageBackendParametersDocument.StorageBackendParameters;
import org.unigrids.x2006.x04.services.smf.StorageDescriptionType;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;
import de.fzj.unicore.uas.StorageFactory;
import de.fzj.unicore.uas.client.StorageFactoryClient;

/**
 * @author demuth
 * 
 */
public class CreateStorageAction extends NodeAction {

	private static final String PROP_STORAGE_NAME = "Storage name";
	private Calendar terminationTime = null;
	private Map<String, Map<String, CreateProperty>> properties = new HashMap<String, Map<String, CreateProperty>>();
	private String selectedStorageType;

	public CreateStorageAction(Node node) {
		super(node);
		setText("Create File Storage");
		setToolTipText("Create a remote file storage for holding files and directories");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("database_add.png"));
	}

	@Override
	public boolean isCurrentlyAvailable() {
		return getNode().getAdapter(StorageFactory.class) != null;
	}

	private void popupDialog() {

		Shell s = null;
		try {
			s = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		} catch (Exception e) {

		}
		if (s == null) {
			s = new Shell();
		}


		try {
			StorageDescriptionType[] sdArray = ((StorageFactoryClient) getNode().getAdapter(
					StorageFactory.class)).getBackendStorageDescription();
			for(StorageDescriptionType sd : sdArray) {
				Map<String, CreateProperty> currentProperties = new TreeMap<String, CreateProperty>();
				String currentType = sd.getStorageBackendType() != null ? sd
						.getStorageBackendType() : "DEFAULT";
				properties.put(currentType, currentProperties);
				if(sd.isSetStorageBackendParameters()) {
					// some storages, e.g. DEFAULT, do not have backend parameters
					PropertyType[] pa = sd.getStorageBackendParameters().getPropertyArray();
					for(PropertyType pt : pa) {
						currentProperties.put(pt.getName(), new CreateProperty("", pt.getValue(), false));
					}
				}
				// add storage name if not already present.
				if (!currentProperties.containsKey(PROP_STORAGE_NAME)) {
					currentProperties.put(PROP_STORAGE_NAME,
							new CreateProperty(currentType + "-storage", "Storage name", true));
				}
			}
		} catch (Exception e) {
			// TODO Assume there are no backend parameters. However, another
			// mode
			// of failure is that the Factory is currently not available,
			// therefore should ignore based on particular Exception.
			// In such cases, let it fail on calling create.
		}

		CreateStorageDialog dialog = new CreateStorageDialog(s,
				"Please set the life time of the storage to be created as well "
						+ "as any additional parameters:",
				properties);

		if (getNode() instanceof WSRFNode) {
			terminationTime = ((WSRFNode) getNode()).getTerminationTime();
		}
		if(terminationTime == null) {
			terminationTime = Calendar.getInstance();
			terminationTime.add(Calendar.MONTH, 1);
		}

		dialog.setInitialSelection(terminationTime.getTime());
		dialog.setMinTime(Calendar.getInstance().getTime());
		
		if (dialog.open() == Window.OK) {
			Date newTT = dialog.getSelectedTime();
			if (newTT != null) {
				terminationTime = Calendar.getInstance();
				terminationTime.setTime(newTT);
			}
			selectedStorageType = dialog.getCurrentStorageType();
		} else {
			terminationTime = null;
		}
	}

	@Override
	public void run() {

		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {

				popupDialog();

			}
		});
		
		// terminationTime will be null if cancel was pressed
		// no creation and no update should be needed
		if(terminationTime == null) {
			return;
		}

		StorageFactoryClient sf = (StorageFactoryClient) getNode().getAdapter(
				StorageFactory.class);
		try {
			CreateSMSDocument scd = getCreateStorageDocument();
			sf.createSMS(scd);
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.ERROR,
					"Unable to create remote storage", e);
			return;
		}

		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {
				RefreshAction a = new RefreshAction(getNode());
				a.setViewer(getViewer());
				a.run();
			}
		});

	}

	/**
	 * @return
	 */
	private CreateSMSDocument getCreateStorageDocument() {
		CreateSMSDocument cSmsDoc = CreateSMSDocument.Factory.newInstance();

		CreateSMS cSms = cSmsDoc.addNewCreateSMS();

		cSms.addNewTerminationTime().setCalendarValue(terminationTime);
		StorageDescriptionType sd = cSms.addNewStorageDescription();
		StorageBackendParameters sbp = sd.addNewStorageBackendParameters();

		// create storage of selected type
		sd.setStorageBackendType(selectedStorageType);
		
		Map<String, CreateProperty> typeProperties = properties.get(selectedStorageType);
		for (String key : typeProperties.keySet()) {
			// treat storage name separately (see below)
			if (key.equals(PROP_STORAGE_NAME)) {
				continue;
			}
			CreateProperty cp = typeProperties.get(key);
			if (cp != null && cp.isActive()) {
				PropertyType p = sbp.addNewProperty();
				p.setName(key);
				p.setValue(typeProperties.get(key).getValue());
			}
		}

		CreateProperty ncp = typeProperties.get(PROP_STORAGE_NAME);
		if(ncp != null && ncp.isActive()) {
			sd.addNewFileSystem().setName(ncp.getValue());
		}

		return cSmsDoc;
	}
}
