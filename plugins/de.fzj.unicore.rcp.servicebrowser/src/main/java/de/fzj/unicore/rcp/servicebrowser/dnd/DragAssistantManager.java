package de.fzj.unicore.rcp.servicebrowser.dnd;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.ui.internal.navigator.extensions.RegistryReader;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IDragAssistant;

public class DragAssistantManager extends RegistryReader {

	private static final DragAssistantManager INSTANCE = new DragAssistantManager();

	private static final IDragAssistant[] NO_ASSISTANTS = new IDragAssistant[0];

	private final List<DragAssistantDescriptor> dragAssistentDescriptors = new ArrayList<DragAssistantDescriptor>();

	private final List<IDragAssistant> dragAssistants = new ArrayList<IDragAssistant>();

	private final List<Transfer> supportedTransferTypes = new ArrayList<Transfer>();

	private DragAssistantManager() {
		super(ServiceBrowserActivator.PLUGIN_ID,
				IDragAssistant.EXTENSION_POINT_ID);
		readRegistry();
	}

	/**
	 * @param aContentDescriptor
	 *            A non-null content descriptor.
	 * @param aDragDescriptor
	 *            A non-null drag descriptor.
	 */
	private void addDragAssistantDescriptor(DragAssistantDescriptor descr) {
		dragAssistentDescriptors.add(descr);
		IDragAssistant a = descr.createDragAssistant();
		dragAssistants.add(a);
		for (Transfer t : a.getSupportedTransferTypes()) {
			if (!supportedTransferTypes.contains(t)) {
				supportedTransferTypes.add(t);
			}
		}
	}

	public IDragAssistant[] findDragAssistants(Object[] dragSources) {

		List<IDragAssistant> found = new ArrayList<IDragAssistant>();
		DragAssistantDescriptor candidate;
		for (int i = 0; i < dragAssistentDescriptors.size(); i++) {
			candidate = dragAssistentDescriptors.get(i);
			boolean isOk = true;
			for (Object dragSource : dragSources) {
				if (!candidate.isDragElementSupported(dragSource)) {
					isOk = false;
					break;
				}
			}
			if (isOk) {
				found.add(dragAssistants.get(i));
			}

		}
		if (found.isEmpty()) {
			return NO_ASSISTANTS;
		}
		return found.toArray(new IDragAssistant[found.size()]);
	}

	public TransferData[] getSupportedTransferData(Object dragSource) {
		List<TransferData> found = new ArrayList<TransferData>();
		DragAssistantDescriptor candidate;

		for (int i = 0; i < dragAssistentDescriptors.size(); i++) {
			candidate = dragAssistentDescriptors.get(i);
			if (candidate.isDragElementSupported(dragSource)) {
				IDragAssistant assistant = dragAssistants.get(i);
				for (TransferData td : assistant.getTransferDataFor(dragSource)) {
					found.add(td);
				}
			}
		}

		return found.toArray(new TransferData[found.size()]);
	}

	public Transfer[] getSupportedTransferTypes() {
		return supportedTransferTypes
				.toArray(new Transfer[supportedTransferTypes.size()]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.internal.navigator.extensions.RegistryReader#readElement
	 * (org.eclipse.core.runtime.IConfigurationElement)
	 */
	@Override
	protected boolean readElement(IConfigurationElement element) {
		if (IDragAssistant.ELEMENT_DRAG_ASSISTANT.equals(element.getName())) {
			addDragAssistantDescriptor(new DragAssistantDescriptor(element));
		}
		return true;
	}

	/**
	 * 
	 * @return An initialized singleton instance of the DragAssistantManager.
	 */
	public static DragAssistantManager getInstance() {
		return INSTANCE;
	}

}
