/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.filters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;

import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IGridBrowserFilterExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.ui.DefaultFilterSet;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceView;

/**
 * this class provides StandardGridFilters that give a basic possibility to
 * filter different criteria.
 * 
 * @author Christian Hohmann
 * 
 */
public class StandardGridFilterSet implements IGridBrowserFilterExtensionPoint {

	private FileFilterConfiguration ffc;
	private UserDefinedJobFilterConfiguration jfc;

	IServiceViewer viewer;
	ServiceView serviceView;
	String nameSpace = "de.fzj.unicore.rcp.servicebrowser.filter";

	private List<GridFilterAction> createFileActions() {

		List<GridFilterAction> result = new ArrayList<GridFilterAction>();
		Action userDefinedFileFilterAction = new Action("User-defined file filter",
				IAction.AS_RADIO_BUTTON) {
			@Override
			public void run() {

				if (this.isChecked()) {
					FileFilterCriteria fileFilter = ffc.createFileFilter();
					if (fileFilter != null) {
						serviceView.activateFilter(this.getId(), fileFilter);
					}

				}
			}
		};
		
		
		GridFilterAction gfa = new GridFilterAction(userDefinedFileFilterAction,
				createFileCategory(), false);
		
		ffc = new FileFilterConfiguration();
		DefaultFilterSet dfs = serviceView.getDefaultFilterSet();
		String id = dfs.getSelectedGridFilterId();
		ContentFilter cf = dfs.getFilterController().getFilter(DefaultFilterSet.FILTER_ID_NODE_TYPES);
		if(cf instanceof ConfigurableFilter)
		{
			ConfigurableFilter confFilter = (ConfigurableFilter) cf;
			FilterCriteria crit = confFilter.getGridFilter();
			if(userDefinedFileFilterAction.getId().equals(id)
					&& (crit instanceof FileFilterCriteria))
			{
				FileFilterCriteria old = (FileFilterCriteria) crit;
				ffc.loadConfiguration(old);
			}
		}
		result.add(gfa);

		Action showHiddenFilesAction = new Action("Hidden files",
				IAction.AS_CHECK_BOX) {
			@Override
			public void run() {
				ContentFilter filter = serviceView.getDefaultFilterSet().getFilterController().getFilter(DefaultFilterSet.FILTER_ID_HIDDEN_FILES);
				if(filter != null)
				{
					filter.setActive(!isChecked());
				}
			}
		};
		showHiddenFilesAction.setId(showHiddenFilesAction.getText());
		GridFilterAction gfa2 = new GridFilterAction(showHiddenFilesAction,
				createFileCategory(), false);
		result.add(gfa2);
		return result;
	}

	private FilterMenuCategory createFileCategory() {
		return new FilterMenuCategory(
				Collections.singletonList(new QName(nameSpace, "Files")));
	}

	private List<GridFilterAction> createJobActions() {
		List<GridFilterAction> result = new ArrayList<GridFilterAction>();
		FilterMenuCategory category = createJobCategory();

		Action action = new Action("User-defined Job Filter",
				IAction.AS_RADIO_BUTTON) {
			@Override
			public void run() {
				if (this.isChecked()) {
					FilterCriteria filter = jfc.createTaskFilter();
					if (filter != null) {
						serviceView.activateFilter(this.getId(), filter);
					}
				}
			}
		};
	
		GridFilterAction gfa = new GridFilterAction(action, category, false);
		
		jfc = new UserDefinedJobFilterConfiguration();
		DefaultFilterSet dfs = serviceView.getDefaultFilterSet();
		String id = dfs.getSelectedGridFilterId();
		ContentFilter cf = dfs.getFilterController().getFilter(DefaultFilterSet.FILTER_ID_NODE_TYPES);
		if(cf instanceof ConfigurableFilter)
		{
			ConfigurableFilter confFilter = (ConfigurableFilter) cf;
			FilterCriteria crit = confFilter.getGridFilter();
			if(action.getId().equals(id)
					&& (crit instanceof JobFilterCriteria))
			{
				JobFilterCriteria old = (JobFilterCriteria) crit;
				jfc.loadConfiguration(old);
			}
		}
		result.add(gfa);

		for (int i = 0; i < JobFilterCriteria.STATUS_ARRAY.length; i++) {
			final int status = JobFilterCriteria.STATUS_ARRAY[i];
			String statusName = JobFilterCriteria.STATUS_STRING_ARRAY[i];
			action = new Action(statusName, IAction.AS_RADIO_BUTTON) {
				@Override
				public void run() {
					if (this.isChecked()) {
						FilterCriteria filter = new JobFilterCriteria(status);
						if (filter != null) {
							serviceView.activateFilter(this.getId(), filter);
						}
					}
				}
			};
	
			gfa = new GridFilterAction(action, category, false);
			result.add(gfa);
		}
		return result;

	}

	/*
	 * Targetsystem filter
	 */

	/**
	 * The following part consists of one method createXXXCategory() and one
	 * method createXXXAction. the createCategory method builds up the location
	 * respectively the submenu structure where the Action created in the
	 * createAction method should be placed.
	 * 
	 */

	/*
	 * Job filter with time
	 */

	private FilterMenuCategory createJobCategory() {
		return new FilterMenuCategory(
				Collections.singletonList(new QName(nameSpace, "Jobs")));
	}

	private List<GridFilterAction> createTargetSystemActions() {

		List<GridFilterAction> result = new ArrayList<GridFilterAction>();
		Action action = new Action("All Target Systems",
				IAction.AS_RADIO_BUTTON) {
			@Override
			public void run() {
				if (this.isChecked()) {
					serviceView.activateFilter(this.getId(),
							new TargetSystemFilterCriteria());
				}
			}
		};
	
		GridFilterAction gfa = new GridFilterAction(action,
				createTargetSystemCategory(), true);
		result.add(gfa);
		return result;

	}

	/*
	 * File and folder filter
	 */

	private FilterMenuCategory createTargetSystemCategory() {
		return new FilterMenuCategory(Collections
				.singletonList(new QName(nameSpace, "Target Systems")));
	}

	public String[] getDefaultNodeTypes(IServiceViewer viewer) {
		return null;
	}

	public GridFilterAction[] getGridFilterList(IServiceViewer viewer,
			ServiceView serviceView) {
		this.viewer = viewer;
		this.serviceView = serviceView;

		List<GridFilterAction> retval = new ArrayList<GridFilterAction>();
		retval.addAll(createFileActions());
		retval.addAll(createTargetSystemActions());
		retval.addAll(createJobActions());

		return retval.toArray(new GridFilterAction[0]);
	}
}