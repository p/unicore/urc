/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.jface.action.Action;

import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

/**
 * @author demuth
 * 
 */
public abstract class ServiceBrowserAction extends Action {

	private IServiceViewer viewer;

	/**
	 * 
	 */
	public ServiceBrowserAction() {
	}

	/**
	 * @return the name
	 */
	public String getClassName() {
		return this.getClass().getName();
	}

	/**
	 * Subclasses may return a non-null value in order to pop up a message
	 * dialog and aquire confirmation by the user.
	 * 
	 * @return
	 */
	public String getConfirmationMessage() {
		return null;
	}

	/**
	 * @return the view
	 */
	public IServiceViewer getViewer() {
		return viewer;
	}

	/**
	 * Check whether the action is available at the current point in time. This
	 * can depend on different factors, e.g. the current grid detail level.
	 * 
	 * @return
	 */
	public boolean isCurrentlyAvailable() {
		return true;
	}

	@Override
	public abstract void run();

	/**
	 * The view is automatically being set when the action is executed from the
	 * context menu.
	 * 
	 * @param viewer
	 *            the view to set
	 */
	public void setViewer(IServiceViewer viewer) {
		this.viewer = viewer;
	}

	@Override
	public String getId()
	{
		return getClassName();
	}
}