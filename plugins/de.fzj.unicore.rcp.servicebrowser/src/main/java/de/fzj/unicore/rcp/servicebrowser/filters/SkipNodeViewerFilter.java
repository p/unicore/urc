/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.filters;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.jface.viewers.Viewer;

import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * @author demuth
 * 
 */
public abstract class SkipNodeViewerFilter extends ContentFilter {

	private boolean nodeSkippingEnabled = true;
	
	/**
	 * 
	 */
	public SkipNodeViewerFilter(String _id) {
		this(_id, true);
	}

	public SkipNodeViewerFilter(String _id, boolean active) {
		super(_id);
		this.active = active;
	}

	@Override
	public Object[] filter(Viewer viewer, Object parent, Object[] elements) {
		int size = elements.length;
		ArrayList<Object> out = new ArrayList<Object>(size);
		for (int i = 0; i < size; ++i) {
			Object element = elements[i];
			if (select(viewer, parent, element)) {
				out.add(element);
			} else if (nodeSkippingEnabled && (element instanceof Node)) {
				Node node = (Node) element;
				Node[] children = node.getChildrenArray();
				for (Node child : children) {
					Object[] filtered = filter(viewer, parent,
							new Object[] { child });
					for (Object object : filtered) {
						out.add(object);
					}
				}
			}
		}
		
		Object[] result = out.toArray();
		Arrays.sort(result);
		return result;
	}

	public void setNodeSkippingEnabled(boolean skip)
	{
		this.nodeSkippingEnabled = skip;
	}
	
}
