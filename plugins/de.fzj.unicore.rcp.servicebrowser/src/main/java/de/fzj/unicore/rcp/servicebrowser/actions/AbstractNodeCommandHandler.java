package de.fzj.unicore.rcp.servicebrowser.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

public abstract class AbstractNodeCommandHandler extends AbstractHandler implements Cloneable {
	
	private IStructuredSelection selection;
	
	private IServiceViewer viewer;
	
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// create a new instance in order to be able to have a different state
		// per execution
		AbstractNodeCommandHandler clone = clone();
		// since the clone will be garbage collected soon, we don't need to 
		// cleanup.
		return clone.doExecute(event);
	}

	public Object doExecute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		Shell shell = window.getShell();
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		ISelectionProvider selectionProvider = HandlerUtil.getActivePart(event).getSite().getSelectionProvider();
		if(selectionProvider instanceof IServiceViewer)
		{
			this.viewer = (IServiceViewer) selectionProvider;
		}
		if(!(selection instanceof IStructuredSelection)) return null;
		this.selection = (IStructuredSelection) selection;

		if(!confirmAction(shell)) return null;
		Job j = new BackgroundJob(getJobName())
		{
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				Object[] objects = getSelection().toArray();
				monitor.beginTask(getTaskName(), objects.length*1000);
				List<Node> nodes = getNodes(objects);
				try {
					IStatus status = performAction(nodes, monitor);
					postAction(nodes, monitor);
					return status;
				} finally {
					monitor.done();
					
				}
			}
		};
		j.schedule();
		return null;
	}

	protected List<Node> getNodes(Object[] objects){
		List<Node>res=new ArrayList<Node>();
		for(Object o: objects){
			if(o instanceof Node){
				res.add((Node)o);
			}
		}
		return res;
	}

	/**
	 * perform the action on a node
	 * @param monitor
	 * @param n
	 * @return
	 */
	protected abstract IStatus performAction(IProgressMonitor monitor, Node n);

	/**
	 * Performs the action on all selected objects. This default implementation
	 * simply invokes {@link #performAction(IProgressMonitor, Node)} for each selected
	 * object. To provide an optimized implementation, you'll need to override this method
	 */
	protected IStatus performAction(List<Node>selection, IProgressMonitor monitor) {
		for (Node n : selection) {
			if(monitor.isCanceled()) return Status.CANCEL_STATUS;
			IProgressMonitor subProgress = new SubProgressMonitor(monitor,1000);
			performAction(subProgress, n);
		}
		return Status.OK_STATUS;
	}

	/**
	 * invoked after {@link #performAction(List, IProgressMonitor)} to handle any
	 * post-processing (e.g. trigger parent node refresh etc). The default implementation does
	 * nothing
	 * @param selection
	 * @param monitor
	 */
	protected void postAction(List<Node>selection, IProgressMonitor monitor) {
		// NOP
	}
	
	/**
	 * Override to make additional checks (e.g. ask user for confirmation)
	 * @return
	 */
	protected boolean confirmAction(Shell shell)
	{
		return true;
	}

	protected abstract String getJobName();

	protected abstract String getTaskName();

	public IStructuredSelection getSelection() {
		return selection;
	}

	public IServiceViewer getViewer() {
		return viewer;
	}
	
	public AbstractNodeCommandHandler clone()
	{
		try {
			return (AbstractNodeCommandHandler) super.clone();
		} catch (CloneNotSupportedException e) {
			return null; // won't happen
		}
	}

}
