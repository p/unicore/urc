/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.TransferData;
import org.unigrids.services.atomic.types.GridFileType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.actions.CreateDirectoryAction;
import de.fzj.unicore.rcp.servicebrowser.actions.CreateFileAction;
import de.fzj.unicore.rcp.servicebrowser.actions.PasteFilesSpecialAction;
import de.fzj.unicore.rcp.servicebrowser.actions.UploadFilesAction;
import de.fzj.unicore.rcp.servicebrowser.actions.UploadFolderAction;
import de.fzj.unicore.rcp.servicebrowser.dnd.FileDropAssistant;
import de.fzj.unicore.rcp.servicebrowser.dnd.NodeLocalSelectionTransfer;

/**
 * @author demuth
 */
public class FolderNode extends AbstractFileNode {

	private static final long serialVersionUID = 7167183016583142275L;

	public static final String TYPE = "Folder";
	public static final QName PORTTYPE = new QName("http://www.unicore.fzj.de",
			TYPE);
	public static final String PROPERTY_CURRENT_OFFSET = StorageNode.PROPERTY_CURRENT_OFFSET;

	public FolderNode(EndpointReferenceType epr) {
		super(epr);
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
		setCurrentOffset(0);
	}

	@Override
	public boolean canPaste(Object toPaste, TransferData data, int operation) {
		if (toPaste == null) {
			return false;
		}
		boolean canIPaste = true;
		if (FileTransfer.getInstance().isSupportedType(data)) {
			return true;
		}
		if (NodeLocalSelectionTransfer.getTransfer().isSupportedType(data)) {
			IStructuredSelection selection = (IStructuredSelection) toPaste;
			for (Object object : selection.toArray()) {
				if (!(object instanceof AbstractFileNode) || equals(object)) // check
																				// if
																				// node
																				// represents
																				// a
																				// file
																				// and
																				// prevent
																				// pasting
																				// into
																				// yourself
				{
					canIPaste = false;
					break;
				}
			}
		} else {
			canIPaste = false;
		}
		if (!canIPaste) {
			return super.canPaste(toPaste, data, operation);
		} else {
			return true;
		}
	}

	protected MoreFilesNode createMoreFilesNode() {
		MoreFilesNode moreFilesNode;
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue(getURI().toString() + "/more");
		Utils.addPortType(epr, MoreFilesNode.PORTTYPE);
		moreFilesNode = (MoreFilesNode) NodeFactory.createNode(epr);
		moreFilesNode.setStorageEPR(getStorageEPR());
		return moreFilesNode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.Node#doubleClick()
	 */
	@Override
	public IStatus doubleClick(IProgressMonitor monitor) {
		return super.doubleClick(monitor);
	}

	public int getCurrentOffset() {
		INodeData data = getData();
		if (data == null) {
			return 0;
		}
		Integer result = (Integer) data.getProperty(PROPERTY_CURRENT_OFFSET);
		if (result != null) {
			return result;
		} else {
			return 0;
		}
	}

	protected MoreFilesNode getMoreFilesNode() {
		for (Node n : getChildrenArray()) {
			if (n instanceof MoreFilesNode) {
				return (MoreFilesNode) n;
			}
		}
		return null; // not found
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void paste(Object toPaste, TransferData data, int operation) {
		if (toPaste == null) {
			return;
		}

		FileDropAssistant assistant = new FileDropAssistant();
		assistant.performDrop(this, operation, data, toPaste);

	}

	@Override
	public IStatus refresh(IProgressMonitor monitor) {
		setExpanded(true);
		return refresh(1, true, monitor);
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		availableActions.add(new CreateDirectoryAction(this));
		availableActions.add(new CreateFileAction(this));
		availableActions.add(new PasteFilesSpecialAction(this));
		availableActions.add(new UploadFilesAction(this));
		availableActions.add(new UploadFolderAction(this));
	}

	@Override
	protected void retrieveChildren() throws Exception {
		int limit = ServiceBrowserActivator
				.getDefault()
				.getPreferenceStore()
				.getInt(ServiceBrowserConstants.P_CHUNK_SIZE_LAZY_CHILD_RETRIEVAL);
		int offset = getCurrentOffset();
		limit = Math.max(limit, offset);
		if (limit > offset) {
			setCurrentOffset(limit);
		}

		List<Node> newChildren = new ArrayList<Node>();
		GridFileType gft = getGridFileType();
		if (gft == null) {
			throw new Exception("Remote folder has been deleted.");
		}
		try {
			GridFileType[] files = null;
			StorageNode n = null;
			try {
				n = (StorageNode) NodeFactory.createNode(getStorageEPR());
				files = n.listDirectory(getStorageClient(), gft.getPath(), 0,
						limit + 1); // list one file more than necessary in
									// order to find out whether the
									// MoreFilesNode should be shown
			} catch (Exception e) {
				if (e.getCause() != null
						&& e.getCause().getMessage() != null
						&& e.getCause().getMessage().toLowerCase()
								.contains("access denied")) {
					setState(STATE_ACCESS_DENIED);
				} else {
					ServiceBrowserActivator.log(IStatus.ERROR,
							"Unable to list directory contents for " + getStorageName() + getName(), e);
					setFailReason(e.getMessage());
					setState(STATE_FAILED);
				}
			} finally {
				if (n != null) {
					n.dispose();
				}
			}
			if (files != null && files.length > 0) {
				Map<URI, Node> oldChildren = getChildrenMap();
				int max = files.length > limit ? limit : files.length;
				for (int i = 0; i < max; i++) {
					GridFileType f = files[i];
					try {
						AbstractFileNode child = StorageNode.createFileNode(
								oldChildren, getStorageEPR(), getStorageName(),
								f);
						child.setSupportedProtocols(getSupportedProtocols());
						newChildren.add(child);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (files.length > limit) {
					MoreFilesNode moreFilesNode = getMoreFilesNode();
					if (moreFilesNode == null) {
						moreFilesNode = createMoreFilesNode();
					}
					newChildren.add(moreFilesNode);
				}
			}

		} finally {
			updateChildren(newChildren);
		}
	}

	@Override
	public void retrieveName() {
		GridFileType gft = getGridFileType();
		if (gft != null) {
			String path = gft.getPath();
			setName(path.substring(path.lastIndexOf("/") + 1));
		} else {
			super.retrieveName();
		}
	}

	public void setCurrentOffset(int offset) {
		putData(PROPERTY_CURRENT_OFFSET, offset);
	}

	/**
	 * @return the type
	 */
	public static QName getPortType() {
		return PORTTYPE;
	}

}
