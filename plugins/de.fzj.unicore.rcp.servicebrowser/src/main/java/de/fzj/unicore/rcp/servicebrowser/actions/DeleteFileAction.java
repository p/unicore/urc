/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.util.URIUtil;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.unigrids.services.atomic.types.GridFileType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.uas.client.StorageClient;

/**
 * @author demuth
 * 
 */
public class DeleteFileAction extends AbstractNodeCommandHandler {

	public DeleteFileAction() {

	}


	@Override
	public boolean confirmAction(Shell shell) {
		return MessageDialog.openConfirm(shell, "Confirm action", "Do you really want to delete the selected file(s)?");
	}

	/**
	 * optimized way of deleting multiple files: group files by parent Storage,
	 * and invoke multi-delete on each Storage
	 */
	protected IStatus performAction(List<Node>selection, IProgressMonitor monitor) {
		Map<StorageClient,List<String>> orderedNodes = new HashMap<StorageClient, List<String>>();
		Set<Node>parents = new HashSet<Node>();
		
		for (Node n : selection) {
			if(monitor.isCanceled())
				return Status.CANCEL_STATUS;
			
			if(n.getParent()!=null)
				parents.add(n.getParent());
			
			AbstractFileNode node = (AbstractFileNode) n;
			EndpointReferenceType epr=node.getEpr();
			String url=epr.getAddress().getStringValue();
			
			StorageClient sms=null;
			List<String> files = null;
			
			for(Map.Entry<StorageClient,List<String>> entry: orderedNodes.entrySet()){
				StorageClient sc = entry.getKey();
				if(sc.getUrl().equals(url)){
					sms = sc;
					files = entry.getValue();
					break;
				}
			}
			if(sms==null){
				sms = node.getStorageClient();
				files = new ArrayList<String>();
				orderedNodes.put(sms,files);
			}
			
			GridFileType gft = node.getGridFileType();
			if (gft != null) {
				String path = gft.getPath();
				try{
					path = URIUtil.encode(path,
						// escape special characters in filenames!
						org.apache.commons.httpclient.URI.allowed_fragment);
					files.add(path);
				}catch(Exception use){}
			}
		}
		
		for(Map.Entry<StorageClient,List<String>> entry: orderedNodes.entrySet()){
			if(monitor.isCanceled()) return Status.CANCEL_STATUS;
			IProgressMonitor subProgress = new SubProgressMonitor(monitor,1000);
			performAction(subProgress, entry.getKey(), entry.getValue());
		}
		
		for(Node n: parents){
			try {
				n.refresh(1, false, null);
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.WARNING, "Unable to refresh file's parent", e);
			}
		}
		return Status.OK_STATUS;
	}


	/**
	 * deletes a list of files on the given storage
	 */
	protected IStatus performAction(IProgressMonitor monitor, StorageClient sms, List<String>toDelete) {
		monitor.beginTask("deleting", 1);
		try {
			sms.delete(toDelete);
		}catch(Exception ex){}
		finally {
			monitor.done();
		}
		return Status.OK_STATUS;
	}
	
	@Override
	protected IStatus performAction(IProgressMonitor monitor, Node n) {
		monitor.beginTask("deleting", 1);
		try {
			final AbstractFileNode node = (AbstractFileNode) n;
	
			StorageClient client = node.getStorageClient();
			if (client == null) {
				// this may occur when the file has been deleted on the
				// server and a refresh has revealed this to the client
				return Status.CANCEL_STATUS;
			}
			try {
				GridFileType gft = node.getGridFileType();
				if (gft == null) {
					// this too may occur when the file has been deleted on
					// the server and a refresh has revealed this to the
					// client
					return Status.CANCEL_STATUS;
				}
				String path = gft.getPath();
				path = URIUtil.encode(path,
						// escape special characters in filenames!
						org.apache.commons.httpclient.URI.allowed_fragment); 
				client.delete(path);
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.ERROR,
						"Unable to delete file " + node.getName(), e);
			}
			try {
				node.getParent().refresh(1, false, null);
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.WARNING,
						"Unable to refresh file's parent", e);
			}
		}
		finally {
			monitor.done();
		}
		return Status.OK_STATUS;
	}

	@Override
	protected String getJobName() {
		return "deleting file(s)";
	}

	@Override
	protected String getTaskName() {
		return "deleting...";
	}

}
