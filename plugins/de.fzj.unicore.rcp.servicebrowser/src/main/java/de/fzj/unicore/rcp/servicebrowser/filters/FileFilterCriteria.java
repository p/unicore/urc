/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.filters;

import java.util.regex.Pattern;

import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * implements the filter logic for files and folders based on a regular
 * expression
 * 
 * @author Christian Hohmann
 * 
 */
public class FileFilterCriteria extends StandardFilterCriteria {

	private boolean useRegexp = true;

	private boolean ignoreCase = true;
	
	private String pattern;
	private int mode = 0;
	
	public static final int FILES_ONLY = 1;
	public static final int FOLDERS_ONLY = 2;
	public static final int FILES_AND_FOLDERS = FILES_ONLY | FOLDERS_ONLY;
	public static final int SHOW_NON_FILE_NODES = 4;

	/**
	 * creates and configures the fileFilter
	 * 
	 * @param criteria
	 *            is the regular expression that is directly handed over to the
	 *            java.util.regex.Pattern.compile method to filter files and
	 *            folders
	 * @param mode
	 *            Sets the filtering mode. {@link #FILES_AND_FOLDERS} means that
	 *            both files and folders should be shown, with 
	 *            {@link FileFilterCriteria#FILES_ONLY}, only files will be shown and
	 *            with {@link #FOLDERS_ONLY}, only folders will be shown.
	 *            These 3 basic modes can be combined with 
	 *            {@link #SHOW_NON_FILE_NODES} to display all other nodes and
	 *            filtered files/folders, e.g. setting the mode to 
	 *            {@link #SHOW_NON_FILE_NODES | #FOLDERS_ONLY} will show all
	 *            other nodes and filtered folders while hiding all files. 
	 */
	public FileFilterCriteria(String criteria, int mode) {

		this.pattern = criteria;
		this.mode = mode;
		
	}

	@Override
	public boolean fitCriteria(Node node) {

		
		if (!(FileNode.TYPE.equals(node.getType()) || FolderNode.TYPE
						.equals(node.getType()))) {
			return isSet(SHOW_NON_FILE_NODES);
		}
		
		if (!isSet(FILES_ONLY) && FileNode.TYPE.equals(node.getType())) {
			return false;
		}
		if (!isSet(FOLDERS_ONLY) && FolderNode.TYPE.equals(node.getType())) {
			return false;
		}

		String myPattern = pattern;
		if(myPattern == null)
		{
			myPattern = useRegexp ? ".*" : "";
		}

		String filename = node.getName() == null ? "" : node.getName();
		if(myPattern.contains("/"))
		{
			filename = node.getNamePathToRoot();
		}
		if(ignoreCase)
		{
			filename = filename.toLowerCase();
			myPattern = myPattern.toLowerCase();
		}
		
		if(useRegexp)
		{
		
			Pattern compiledPattern = Pattern.compile(myPattern);
			boolean result = compiledPattern.matcher(filename).matches();
			return result;
		}
		else
		{
			return filename.contains(myPattern);
		}
	}


	private boolean isSet(int modeBit)
	{
		return isSet(modeBit, mode);
	}
	
	public static boolean isSet(int modeBit, int mode)
	{
		return (mode & modeBit) != 0;
	}
	
	public void setUseRegexp(boolean useRegexp) {
		this.useRegexp = useRegexp;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		
		this.pattern = pattern;
	}

	public void setIgnoreCase(boolean ignoreCase) {
		this.ignoreCase = ignoreCase;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public boolean isUseRegexp() {
		return useRegexp;
	}
}