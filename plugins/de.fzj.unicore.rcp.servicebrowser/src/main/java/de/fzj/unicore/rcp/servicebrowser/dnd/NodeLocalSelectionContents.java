package de.fzj.unicore.rcp.servicebrowser.dnd;

import org.eclipse.jface.viewers.ISelection;

public class NodeLocalSelectionContents {
	private ISelection selection;
	private int operation;

	public NodeLocalSelectionContents(ISelection selection, int operation) {
		super();
		this.selection = selection;
		this.operation = operation;
	}

	public int getOperation() {
		return operation;
	}

	public ISelection getSelection() {
		return selection;
	}

	public void setOperation(int operation) {
		this.operation = operation;
	}

	public void setSelection(ISelection selection) {
		this.selection = selection;
	}

}
