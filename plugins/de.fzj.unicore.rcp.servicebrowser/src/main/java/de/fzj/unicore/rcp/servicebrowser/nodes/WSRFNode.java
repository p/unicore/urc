/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.oasisOpen.docs.wsrf.rl2.CurrentTimeDocument;
import org.oasisOpen.docs.wsrf.rl2.TerminationTimeDocument;
import org.oasisOpen.docs.wsrf.rp2.GetResourcePropertyDocumentResponseDocument;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.logmonitor.problems.ProblemCenter;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.Util;
import de.fzj.unicore.rcp.servicebrowser.actions.SetTerminationTimeAction;
import de.fzj.unicore.rcp.servicebrowser.actions.ShowDetailsAction;
import de.fzj.unicore.uas.client.BaseUASClient;
import de.fzj.unicore.wsrflite.xmlbeans.WSUtilities;
import eu.unicore.problemutil.Problem;

/**
 * @author demuth
 */
public class WSRFNode extends WebServiceNode {

	private static final long serialVersionUID = 6010536723734300858L;

	public static final String PROPERTY_CACHED_RPS = "cached resource properties",
			PROPERTY_CURRENT_TIME = "current_time",
			PROPERTY_TERMINATION_TIME = "termination time",
			PROPERTY_CLOCK_DEVIATION_TO_CLIENT = "clock deviation to client";

	public WSRFNode(EndpointReferenceType epr) {
		super(epr);
	}

	/**
	 * 
	 */
	private void checkTimeDeviation() {
		Calendar currentServerTime = getCurrentTime();
		if (currentServerTime == null) {
			return; // cannot check it yet
		}
		Calendar currentLocalTime = Calendar.getInstance();
		long timeDiffMS = currentServerTime.getTimeInMillis()
				- currentLocalTime.getTimeInMillis();
		timeDiffMS = Math.abs(timeDiffMS);
		if (timeDiffMS > 300000) {
			ServiceBrowserActivator.log(
					IStatus.WARNING,
					"Current local time differs from current server time by more than 5 minutes.\n"
							+ "Local time: "
							+ getDefaultDateFormat().format(
									currentLocalTime.getTime())
							+ "\n"
							+ "Server time: "
							+ getDefaultDateFormat().format(
									currentServerTime.getTime()));
		}
	}

	public boolean canBeDestroyed() {
		return UnicoreCommonActivator.getDefault().getGridDetailLevel() >= UnicoreCommonActivator.LEVEL_ADVANCED;
	}

	/**
	 * Creates a UNICORE client stub for talking to the WSRF instance.
	 * 
	 * @return
	 * @throws Exception
	 */
	public BaseUASClient createUASClient() throws Exception {

		if (getURL() == null) {
			throw new Exception("Invalid URL for service "
					+ getNamePathToRoot() + ": " + getURI());
		}
		BaseUASClient client = new BaseUASClient(getURL().toString(), getEpr(),
				getUASSecProps());
		if (!client.checkConnection()) {
			if (getFallbackURL() != null) {				
				client = new BaseUASClient(getFallbackURL().toString(), getFallbackEpr(),
						getUASSecProps());		
			} 
		}
		client.setUpdateInterval(2000);
		return client;

	}

	/**
	 * Destroy the WS Resource represented by this node.
	 */
	public void destroy() {
		try {
			BaseUASClient client = new BaseUASClient(getURL().toString(), getEpr(),
					getUASSecProps());
			client.destroy();
			setState(Node.STATE_DESTROYED);
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.WARNING, "Cannot destroy node" + getNamePathToRoot(), e);
		}
	}

	public String getCachedResourceProperties() {
		INodeData data = getData();
		if (data == null) {
			return null;
		}
		Object o = data.getProperty(PROPERTY_CACHED_RPS);
		if (o != null && o instanceof String) {
			try {
				return (String) o;
			} catch (Exception e) {
			}

		}
		return null;
	}

	public XmlObject getCachedResourcePropertyDocument() {
		if (!hasCachedResourceProperties()) {
			return null;
		}

		try {
			XmlObject o = XmlObject.Factory
					.parse(getCachedResourceProperties());
			SchemaType t = getSchemaType();
			return o.changeType(t);

		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.INFO,
					"Error while parsing properties for service "
							+ getURI().toString(), e);
		}
		return null;

	}

	public Calendar getCurrentTime() {
		INodeData data = getData();
		if (data == null) {
			return null;
		}
		Object o = data.getProperty(PROPERTY_CURRENT_TIME);
		if (o != null && o instanceof Calendar) {
			return (Calendar) o;
		} else {
			return null;
		}

	}

	@Override
	public DetailsAdapter getDetails() {
		DetailsAdapter details = super.getDetails();
		Map<String, String> hm = details.getMap();
		if (null != getCurrentTime()) {
			hm.put("Current Time", getDefaultDateFormat().format(getCurrentTime().getTime()));
		} else {
			hm.put("Current Time", "unknown");
		}
		if (null != getTerminationTime()) {
			hm.put("Termination Time",
					getDefaultDateFormat().format(getTerminationTime().getTime()));
		} else {
			hm.put("Termination Time", "unknown");
		}
		return details;
	}

	@Override
	public String getLowLevelAsString() {
		if (getCachedResourceProperties() != null) {
			return getCachedResourceProperties();
		}

		return null;
	}

	/**
	 * Server times are often inaccurate. In order to interpret the server side
	 * times correctly, regard the time difference between the client clock and
	 * the the server clock.
	 * 
	 * @return the normalized server time or null if the deviation of the clocks
	 *         is unknown
	 */
	public Calendar getNormalizedTime(Calendar serverTime) {
		Long diff = getServerClockDeviation();
		if (diff == null || serverTime == null) {
			return null;
		}
		Calendar result = Calendar.getInstance();
		result.setTimeInMillis(serverTime.getTimeInMillis() - diff);
		return result;
	}

	public String getResourceId() {
		try {
			String[] parts = getURI().getQuery().split("=");
			for (int i = 0; i < parts.length - 1; i++) {
				String part = parts[i];
				if ("res".equals(part)) {
					return parts[i + 1];
				}
			}
		} catch (Exception e) {
			// do nothing
		}
		return "unknown";
	}

	public SchemaType getSchemaType() {
		return XmlObject.type;
	}

	/**
	 * The number of milliseconds of difference between the server clock and the
	 * client clock as retrieved during the latest refresh.
	 * 
	 * @return
	 */
	public Long getServerClockDeviation() {
		INodeData data = getData();
		if (data == null) {
			return null;
		}
		Object o = data.getProperty(PROPERTY_CLOCK_DEVIATION_TO_CLIENT);
		if (o != null && o instanceof Long) {
			return (Long) o;
		} else {
			return null;
		}

	}

	/**
	 * Tries to obtain a version String from a UNICORE WSRF instance. This won't
	 * work on older (<6.3) or non-UNICORE WSRF instances. In this case, null is
	 * returned.
	 * 
	 * @return
	 */
	public String getServerVersion() {
		try {
			BaseUASClient client = createUASClient();
			QName qname = new QName(Constants.UNIGRIDS_TYPES_NS, "Version");
			String versionRP = client.getResourceProperty(qname);
			String result = WSUtilities
					.extractElementTextAsString(XmlObject.Factory.parse(
							versionRP).selectChildren(qname)[0]);
			return result;
		} catch (Exception e) {
			return null;
		}
	}

	public Calendar getTerminationTime() {
		INodeData data = getData();
		if (data == null) {
			return null;
		}
		Object o = data.getProperty(PROPERTY_TERMINATION_TIME);
		if (o != null && o instanceof Calendar) {
			return (Calendar) o;
		} else {
			return null;
		}

	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return "unknown ws resource";
	}

	public boolean hasCachedResourceProperties() {
		return getCachedResourceProperties() != null;

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource() == this) {
			return; // don't react on your own changes!
		}

		if (PROPERTY_CACHED_RPS.equals(evt.getPropertyName())) {
			retrieveName();
		}
		super.propertyChange(evt);
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		availableActions.add(new SetTerminationTimeAction(this));
		availableActions.add(new ShowDetailsAction(this));
	}

	/**
	 * Retrieves the PropertyDocument and sets this, terminationTime and
	 * currentTime <br/>
	 * Used by Services as 1st step and going on with further process OR by
	 * Unknown Type.
	 */
	@Override
	public int retrieveProperties(IProgressMonitor progressMonitor) {
		try {
			INodeData data = getData();
			if (data == null) {
				return STATE_FAILED; // this shouldn't happen
			}
			synchronized (data) {
				// update name (needed for finding the right security
				// properties)
				retrieveName();
				GetResourcePropertyDocumentResponseDocument refreshedProperties = retrieveResourceProperties();
				// store properties in node
				setCachedResourceProperties(refreshedProperties
						.getGetResourcePropertyDocumentResponse().toString());
				// set term time in node
				// do not use the xmlbeans Calendar instance! it cannot be
				// deserialized by xstream.
				TerminationTimeDocument ttd = Util
						.getTerminationTimeDocument(refreshedProperties
								.getGetResourcePropertyDocumentResponse());
				if (ttd != null && !ttd.getTerminationTime().isNil()) {
					Calendar cal = Calendar.getInstance();
					cal.setTimeInMillis(ttd.getTerminationTime()
							.getCalendarValue().getTimeInMillis());
					setTerminationTime(cal);
				} else {
					setTerminationTime(null);// indicate infinity lifetime
				}

				// set currenttime in node
				// do not use the xmlbeans Calendar instance! it cannot be
				// deserialized by xstream.
				CurrentTimeDocument ctd = Util
						.getCurrentTimeDocument(refreshedProperties
								.getGetResourcePropertyDocumentResponse());
				if (ctd != null && !ctd.getCurrentTime().isNil()) {
					Calendar cal = Calendar.getInstance();
					long serverTime = ctd.getCurrentTime().getCalendarValue()
							.getTimeInMillis();
					long clientTime = cal.getTimeInMillis();
					long diff = serverTime - clientTime;
					cal.setTimeInMillis(serverTime);
					setCurrentTime(cal);
					setServerClockDeviation(diff);
					checkTimeDeviation();
				} else {
					setCurrentTime(null);// indicate "not provided"
				}

			}
		} catch (Exception e) {
			setCachedResourceProperties(null);
			updateChildren(new ArrayList<Node>());
			boolean accessDenied = false;
			Status s = new Status(IStatus.ERROR, ServiceBrowserActivator.PLUGIN_ID, "",e);
			List<Problem> problems = ProblemCenter.identifyProblems(s);
			for(Problem p : problems)
			{
				if(p.getReason() != null && "ACCESS_DENIED".equals(p.getReason().getId()))
				{
					accessDenied = true;
					break;
				}
			}
			if (accessDenied) {
				ServiceBrowserActivator.log(IStatus.INFO,
						"Could not refresh resource properties of service "
								+ getNamePathToRoot(), e);
				return STATE_ACCESS_DENIED;
			} else {

				boolean filterFailed = ServiceBrowserActivator
						.getDefault()
						.getPreferenceStore()
						.getBoolean(
								ServiceBrowserConstants.P_HIDE_FAILED_SERIVCES);
				int status = filterFailed ? IStatus.INFO : IStatus.WARNING;
				ServiceBrowserActivator.log(status,
						"Could not refresh resource properties of service " + getNamePathToRoot(), e);
				setFailReason(e.getMessage());
				return STATE_FAILED;
			}

		}
		return STATE_READY;
	}

	/**
	 * Retrieves the PropertyDocument (e.g. for detailString creation)
	 */
	protected GetResourcePropertyDocumentResponseDocument retrieveResourceProperties()
			throws Exception {
		BaseUASClient client = createUASClient();
		GetResourcePropertyDocumentResponseDocument doc = GetResourcePropertyDocumentResponseDocument.Factory
				.parse(client.getResourcePropertyDocument());
		client = null;
		return doc;	// enable gc

	}

	public void setCachedResourceProperties(String rp) {
		putData(PROPERTY_CACHED_RPS, rp);
	}

	public void setCurrentTime(Calendar currentTime) {

		putData(PROPERTY_CURRENT_TIME, currentTime);

	}

	public void setServerClockDeviation(long deviation) {
		putData(PROPERTY_CLOCK_DEVIATION_TO_CLIENT, deviation);

	}

	public void setTerminationTime(Calendar terminationTime) {
		putData(PROPERTY_TERMINATION_TIME, terminationTime);

	}

}
