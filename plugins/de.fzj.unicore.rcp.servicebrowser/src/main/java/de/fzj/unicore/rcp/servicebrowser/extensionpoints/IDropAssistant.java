package de.fzj.unicore.rcp.servicebrowser.extensionpoints;

import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;

public interface IDropAssistant {

	public static final String EXTENSION_POINT_ID = "NodeDNDExtensionPoint";
	public static final String ELEMENT_DROP_ASSISTANT = "dropAssistant";
	public static final String ELEMENT_POSSIBLE_DROP_TARGETS = "possibleDropTargets";
	public static final String ELEMENT_POSSIBLE_TRANSFER_TYPES = "possibleTransferTypes";
	public static final String ATTRIBUTE_CLASS = "class";

	/**
	 * Returns an array of supported transfers. The order of transfers is
	 * maintained during matchmaking with the source so that the first transfer
	 * in the list is preferred over the second etc.
	 * 
	 * @return
	 */
	public Transfer[] getSupportedTransferTypes();

	/**
	 * Perform the drag and return which operation was performed (see
	 * {@link DND}.
	 * 
	 * @param target
	 * @param operation
	 * @param transferType
	 * @param data
	 * @return
	 */
	public int performDrop(Object target, int operation,
			TransferData transferType, Object data);

	public boolean supportsTransferData(TransferData td);

	/**
	 * Check whether the drop can be completed. Return the operation that would
	 * be performed (see {@link DND#DROP_NONE}, {@link DND#DROP_COPY} etc.). If
	 * {@link DND#DROP_NONE} is returned, the assistant is not used for
	 * dropping.
	 * 
	 * @param target
	 * @param operation
	 * @param transferTypes
	 * @return
	 */
	public int validateDrop(Object target, int operation,
			TransferData[] transferTypes);

}
