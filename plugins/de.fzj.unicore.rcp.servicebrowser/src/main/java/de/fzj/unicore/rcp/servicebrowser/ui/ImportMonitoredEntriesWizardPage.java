package de.fzj.unicore.rcp.servicebrowser.ui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class ImportMonitoredEntriesWizardPage extends WizardPage {

	private static final String PAGE_ID = "de.fzj.unicore.rcp.servicebrowser.ui.ImportMonitoredEntriesWizardPage"; //$NON-NLS-1$

	private Button browseButton = null;

	private Text sourceText = null;

	public ImportMonitoredEntriesWizardPage() {
		super(PAGE_ID);
		setPageComplete(false);
		// setImageDescriptor(CommonImages.BANNER_EXPORT);
		setTitle("Import Grid Browser bookmarks from a file");
	}

	/** Called to indicate that a control's value has changed */
	public void controlChanged() {
		setPageComplete(validate());
	}

	/**
	 * Create the widgets on the page
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);
		createExportDirectoryControl(container);

		Dialog.applyDialogFont(container);
		setControl(container);
		setPageComplete(validate());
		sourceText.setFocus();
	}

	/**
	 * Create widgets for specifying the destination directory
	 */
	private void createExportDirectoryControl(Composite parent) {
		parent.setLayout(new GridLayout(3, false));
		parent.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(parent, SWT.NONE).setText("File containing bookmarks:");

		sourceText = new Text(parent, SWT.BORDER);
		sourceText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		sourceText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				controlChanged();
			}
		});

		browseButton = new Button(parent, SWT.PUSH);
		browseButton.setText("&Browse");
		browseButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(getShell(), SWT.OPEN);
				dialog.setText("Choose source file");
				String dir = sourceText.getText();
				dialog.setFilterPath(dir);
				dir = dialog.open();
				if (dir == null || dir.equals("")) { //$NON-NLS-1$
					return;
				}
				sourceText.setText(dir);
				controlChanged();
			}
		});
	}

	/** Returns the directory where data files are to be saved */
	public String getSource() {
		return sourceText.getText();
	}

	public void setSourceDirectory(String destinationDir) {
		sourceText.setText(destinationDir);
	}

	/** Returns true if the information entered by the user is valid */
	protected boolean validate() {
		setMessage(null);

		// Check that a destination dir has been specified
		if (sourceText.getText().equals("")) { //$NON-NLS-1$
			setMessage("Please choose a file for import", IStatus.WARNING);
			return false;
		}

		return true;
	}
}
