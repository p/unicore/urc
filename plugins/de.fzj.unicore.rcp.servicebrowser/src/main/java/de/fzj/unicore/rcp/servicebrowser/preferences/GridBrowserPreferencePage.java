package de.fzj.unicore.rcp.servicebrowser.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;

/**
 * This class represents a UNICORE Applications preference page that is
 * contributed to the UNICORE Preferences dialog.
 * 
 * @author Valentina Huber
 */
public class GridBrowserPreferencePage extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {

	public GridBrowserPreferencePage() {
		super(GRID);
		setPreferenceStore(ServiceBrowserActivator.getDefault()
				.getPreferenceStore());
		setDescription("Preferences for the Grid Browser view.");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		IntegerFieldEditor refreshLevelsOnStartup = new IntegerFieldEditor(
				ServiceBrowserConstants.P_NUM_LEVELS_ON_STARTUP,
				"Number of Grid Browser levels to refresh on startup",
				getFieldEditorParent());
		addField(refreshLevelsOnStartup);

		BooleanFieldEditor hideFailed = new BooleanFieldEditor(
				ServiceBrowserConstants.P_HIDE_FAILED_SERIVCES,
				"Hide &failed Grid services", getFieldEditorParent());
		addField(hideFailed);

		BooleanFieldEditor hideAccessDenied = new BooleanFieldEditor(
				ServiceBrowserConstants.P_HIDE_ACCESS_DENIED_SERIVCES,
				"Hide &Grid services that you don't have access to",
				getFieldEditorParent());
		addField(hideAccessDenied);

		IntegerFieldEditor filesPerLsOperation = new IntegerFieldEditor(
				ServiceBrowserConstants.P_CHUNK_SIZE_LAZY_CHILD_RETRIEVAL,
				"Chunk size when listing many services", getFieldEditorParent());
		addField(filesPerLsOperation);
		
		IntegerFieldEditor numPersistedFiles = new IntegerFieldEditor(
				ServiceBrowserConstants.P_NUM_PERSISTED_FILE_NODES,
				"Number of Grid files/folders being persisted on shutdown",
				getFieldEditorParent());
		addField(numPersistedFiles);
		
		BooleanFieldEditor showSiUnits = new BooleanFieldEditor(ServiceBrowserConstants.P_SIZE_UNITS_SI,
				"Show sizes in SI units (base 1000) rather than IEC units (base 1024)",
				getFieldEditorParent());
		addField(showSiUnits);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}

}