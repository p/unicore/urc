package de.fzj.unicore.rcp.servicebrowser.ui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class ExportMonitoredEntriesWizardPage extends WizardPage {

	private static final String PAGE_ID = "de.fzj.unicore.rcp.servicebrowser.ui.ExportMonitoredEntriesWizardPage"; //$NON-NLS-1$

	private Button browseButton = null;

	private Text destText = null;

	public ExportMonitoredEntriesWizardPage() {
		super(PAGE_ID);
		setPageComplete(false);
		// setImageDescriptor(CommonImages.BANNER_EXPORT);
		setTitle("Export Grid Browser bookmarks to a file");
	}

	/** Called to indicate that a control's value has changed */
	public void controlChanged() {
		setPageComplete(validate());
	}

	/**
	 * Create the widgets on the page
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);
		createExportDirectoryControl(container);

		Dialog.applyDialogFont(container);
		setControl(container);
		setPageComplete(validate());
	}

	/**
	 * Create widgets for specifying the destination directory
	 */
	private void createExportDirectoryControl(Composite parent) {
		parent.setLayout(new GridLayout(3, false));
		parent.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(parent, SWT.NONE).setText("Destination");

		destText = new Text(parent, SWT.BORDER);
		destText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		destText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				controlChanged();
			}
		});

		browseButton = new Button(parent, SWT.PUSH);
		browseButton.setText("&Browse");
		browseButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(getShell(), SWT.SAVE);
				dialog.setText("Choose target location");
				String dir = destText.getText();
				dialog.setFilterPath(dir);
				dir = dialog.open();
				if (dir == null || dir.equals("")) { //$NON-NLS-1$
					return;
				}
				destText.setText(dir);
				controlChanged();
			}
		});
	}

	/** Returns the directory where data files are to be saved */
	public String getDestination() {
		return destText.getText();
	}

	/** For testing only. Sets controls to the specified values */
	public void setDestinationDirectory(String destinationDir) {
		destText.setText(destinationDir);
	}

	/** Returns true if the information entered by the user is valid */
	protected boolean validate() {
		setMessage(null);

		// Check that a destination dir has been specified
		if (destText.getText().equals("")) { //$NON-NLS-1$
			setMessage("Please choose an export destination", IStatus.WARNING);
			return false;
		}

		return true;
	}
}
