/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionDelta;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IRegistryChangeEvent;
import org.eclipse.core.runtime.IRegistryChangeListener;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;

import de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.WebServiceNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.MoreNode;

/**
 * @author demuth
 * 
 */
public class ServiceLabelProvider extends LabelProvider implements
		IRegistryChangeListener, ITableColorProvider {

	private Image defaultImage = ServiceBrowserActivator.getImageDescriptor(
			"unknown.png").createImage();
	private Image wsrfImage = ServiceBrowserActivator.getImageDescriptor(
			"WSRF.png").createImage();
	private Image wsImage = ServiceBrowserActivator.getImageDescriptor(
			"service.png").createImage();
	private Image failedImage = ServiceBrowserActivator.getImageDescriptor(
			"failed.png").createImage();
	private Image accessDeniedImage = ServiceBrowserActivator
			.getImageDescriptor("access_denied.png").createImage();
	private Image refreshingImage = ServiceBrowserActivator.getImageDescriptor(
			"refresh.png").createImage();
	private Image destroyedImage = ServiceBrowserActivator.getImageDescriptor(
			"destroyed.png").createImage();

	private Map<String, INodeExtensionPoint> extensionMap = new HashMap<String, INodeExtensionPoint>();
	private List<INodeExtensionPoint> extensions = new ArrayList<INodeExtensionPoint>();

	private Color lightGray = new Color(null, 240, 240, 230);

	public ServiceLabelProvider() {

		updateExtensions();
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		registry.addRegistryChangeListener(this,
				ServiceBrowserConstants.NAMESPACE);
	}

	@Override
	public void dispose() {
		super.dispose();
		defaultImage.dispose();
		wsrfImage.dispose();
		wsImage.dispose();
		failedImage.dispose();
		accessDeniedImage.dispose();
		refreshingImage.dispose();
		destroyedImage.dispose();
		lightGray.dispose();
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		registry.removeRegistryChangeListener(this);
		if (getExtensions() != null) {
			for (INodeExtensionPoint ext : getExtensions()) {
				try {
					ext.dispose();
				} catch (Throwable t) {
					t.printStackTrace();
				}
			}
		}
		extensions = null;
		extensionMap = null;
	}

	public Color getBackground(Object element, int columnIndex) {

		if (element instanceof MoreNode) {
			return lightGray;
		}
		return null;

	}

	public List<INodeExtensionPoint> getExtensions() {
		return extensions;
	}

	public Color getForeground(Object element, int columnIndex) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ILabelProvider#getImage(java.lang.Object)
	 */
	@Override
	public Image getImage(Object element) {

		if (!(element instanceof Node)) {
			return defaultImage;
		}

		Image image = null;

		Node node = (Node) element;

		int state = node.getState();
		// check for state
		if (state == Node.STATE_FAILED) {
			return failedImage;
		} else if (state == Node.STATE_ACCESS_DENIED) {
			return accessDeniedImage;
		} else if (state == Node.STATE_REFRESH) {
			return refreshingImage;
		} else if (state == Node.STATE_DESTROYED) {
			return destroyedImage;
		}

		for (INodeExtensionPoint ext : getExtensions()) {
			image = ext.getImage(node);
			if (image != null) {
				return image;
			}
		}

		if (node instanceof WSRFNode) {
			return wsrfImage;
		} else if (node instanceof WebServiceNode) {
			return wsImage;
		} else {
			return defaultImage;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ILabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object element) {
		if (!(element instanceof Node)) {
			return "Unknown node type";
		}
		Node node = (Node) element;

		for (INodeExtensionPoint ext : getExtensions()) {
			String text = ext.getText(node);
			if (text != null) {
				return text;
			}
		}
		return node.getName();
	}

	public void registryChanged(IRegistryChangeEvent event) {
		IExtensionDelta[] extensionDeltas = event.getExtensionDeltas(
				ServiceBrowserConstants.NAMESPACE,
				ServiceBrowserConstants.NODE_EXTENSION_POINT);

		for (int i = 0; i < extensionDeltas.length; i++) {

			if (extensionDeltas[i].getKind() == IExtensionDelta.ADDED) {

				IExtension extension = extensionDeltas[i].getExtension();
				IConfigurationElement[] members = extension
						.getConfigurationElements();

				for (int m = 0; m < members.length; m++) {
					IConfigurationElement member = members[m];
					try {
						String id = member.getDeclaringExtension()
								.getUniqueIdentifier();
						if (!extensionMap.containsKey(id)) {
							INodeExtensionPoint ext = (INodeExtensionPoint) member
									.createExecutableExtension("name");
							extensionMap.put(id, ext);
							extensions.add(ext);
						}
					} catch (CoreException ex) {

					}
				}
			} else if (extensionDeltas[i].getKind() == IExtensionDelta.REMOVED) {

				IExtension extension = extensionDeltas[i].getExtension();
				IConfigurationElement[] members = extension
						.getConfigurationElements();

				for (int m = 0; m < members.length; m++) {
					IConfigurationElement member = members[m];
					String id = member.getDeclaringExtension()
							.getUniqueIdentifier();
					if (!extensionMap.containsKey(id)) {
						INodeExtensionPoint ext = extensionMap.remove(id);
						extensions.remove(ext);
					}
				}

			}
		}
	}

	public void updateExtensions() {

		// manage context menu entries added by extensions
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(ServiceBrowserConstants.NODE_EXTENSION_POINT);
		IConfigurationElement[] members = extensionPoint
				.getConfigurationElements();
		Set<String> obsoleteExtensions = new HashSet<String>(
				extensionMap.keySet());

		// For each service:
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			// IExtension extension = member.getDeclaringExtension();
			// String actionName = member.getAttribute(FUNCTION_NAME_ATTRIBUTE);
			try {
				String id = member.getDeclaringExtension()
						.getUniqueIdentifier();
				if (!extensionMap.containsKey(id)) {
					INodeExtensionPoint ext = (INodeExtensionPoint) member
							.createExecutableExtension("name");
					extensionMap.put(id, ext);
				}
				obsoleteExtensions.remove(id);
			} catch (CoreException ex) {

			}
		}

		for (String id : obsoleteExtensions) {
			extensionMap.remove(id);
		}
		extensions = new ArrayList<INodeExtensionPoint>(extensionMap.size());
		for (INodeExtensionPoint extension : extensionMap.values()) {
			extensions.add(extension);
		}

	}
}
