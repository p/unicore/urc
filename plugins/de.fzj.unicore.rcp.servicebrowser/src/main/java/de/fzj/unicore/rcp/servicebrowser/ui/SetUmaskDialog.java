package de.fzj.unicore.rcp.servicebrowser.ui;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.unigrids.x2006.x04.services.sms.StoragePropertiesDocument;

import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;

public class SetUmaskDialog extends Dialog {
	protected StorageNode storageNode;
	protected Text umaskTextField;

	protected String currentUmask, newUmask;

	
	public SetUmaskDialog(Shell parentShell, StorageNode storageNode) {
		super(parentShell);
		this.storageNode = storageNode;
	}

	@Override
	public boolean close() {
		this.newUmask = umaskTextField.getText();
		return super.close();
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		currentUmask = getStorageCurrentUmask();
				
		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(1, true));

		Label currentGrplabel = new Label(composite, SWT.NONE);
		currentGrplabel.setFont(parent.getFont());
		currentGrplabel.setText("Current umask: " + currentUmask);

		Label newGrpLabel = new Label(composite, SWT.NONE);
		newGrpLabel.setText("New storage's umask:");
		newGrpLabel.setFont(parent.getFont());
		
		umaskTextField = new Text(composite, SWT.SINGLE | SWT.CENTER);
		umaskTextField.setFont(parent.getFont());
		umaskTextField.setText(currentUmask);
		umaskTextField.setTextLimit(3);
		GridData gd = new GridData(SWT.CENTER, SWT.CENTER, true, false);
		gd.minimumWidth = 50;
		umaskTextField.setLayoutData(gd);
		
		return composite;
	}

	protected String getStorageCurrentUmask() {
		if (! storageNode.hasCachedResourceProperties()) {
			return "";
		}
		StoragePropertiesDocument doc = (StoragePropertiesDocument) storageNode.getCachedResourcePropertyDocument();
		return doc.getStorageProperties().getUmask();		
	}
	
	public boolean isUmaskChanged()	{
		return (!newUmask.equals(currentUmask)  &&  !newUmask.equals(""));
	}
	
	public String getNewUmask() {
		return newUmask;
	}	
}
