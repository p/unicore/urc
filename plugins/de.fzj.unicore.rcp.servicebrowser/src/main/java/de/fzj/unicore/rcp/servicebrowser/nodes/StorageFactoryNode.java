/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.xmlbeans.SchemaType;
import org.eclipse.core.runtime.IProgressMonitor;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.FileSystemType;
import org.unigrids.x2006.x04.services.smf.AccessibleStorageReferenceDocument;
import org.unigrids.x2006.x04.services.smf.StorageFactoryPropertiesDocument;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.UAS;
import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.common.utils.FileUtils;
import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.actions.CreateStorageAction;
import de.fzj.unicore.rcp.servicebrowser.utils.URIUtils;
import de.fzj.unicore.uas.StorageFactory;
import de.fzj.unicore.uas.client.EnumerationClient;
import de.fzj.unicore.uas.client.StorageClient;
import de.fzj.unicore.uas.client.StorageFactoryClient;


/**
 * @author demuth
 * 
 */
public class StorageFactoryNode extends WSRFNode {

	private static final long serialVersionUID = 1119845901440005784L;
	public static final String TYPE = UAS.SMF;

	public static final String PARAM_NAME = "name";

	public StorageFactoryNode(EndpointReferenceType epr) {
		super(epr);
	}

	public StorageClient createStorage(Calendar terminationTime,
			Map<String, Object> params, IProgressMonitor progress)
	throws Exception {
		progress = ProgressUtils.getNonNullMonitorFor(progress);
		StorageFactoryClient sfc = new StorageFactoryClient(getEpr()
				.getAddress().getStringValue(), getEpr(), getUASSecProps());
		String name = null;
		if(params != null)
		{
			name = (String) params.get(PARAM_NAME);
		}
		StorageClient result = sfc.createSMS(null, name, terminationTime);
		refresh(1, false, progress);
		return result;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class key) {

		if (key.getName().equals(StorageFactory.class.getName())) {
			try {
				return new StorageFactoryClient(getEpr(), getUASSecProps());
			} catch (Exception e) {
				return null;
			}

		} else {
			return super.getAdapter(key);
		}
	}

	@Override
	public StorageFactoryPropertiesDocument getCachedResourcePropertyDocument() {
		return (StorageFactoryPropertiesDocument) super
		.getCachedResourcePropertyDocument();
	}

	/**
	 * Integer value for sorting nodes in views. Nodes are usually sorted in
	 * ascending order.
	 * 
	 * @return
	 */
	@Override
	public int getCategory() {
		return 600;
	}

	@Override
	public SchemaType getSchemaType() {
		return StorageFactoryPropertiesDocument.type;
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		availableActions.add(new CreateStorageAction(this));
	}

	/**
	 * create the child nodes of this node
	 */
	@Override
	protected void retrieveChildren() throws Exception {
		List<Node> newChildren = new ArrayList<Node>();
		if (hasCachedResourceProperties()) {
			StorageFactoryPropertiesDocument props = getCachedResourcePropertyDocument();
			EndpointReferenceType[] smsEprs =null;
			EnumerationClient<AccessibleStorageReferenceDocument>enumClient=createEnumClient(props);
			if(enumClient!=null){
				long num=enumClient.getNumberOfResults();
				if(num>0) { // pre 7.x server chokes if offset = num = 0
					List<AccessibleStorageReferenceDocument>list=enumClient.getResults(0, num);
					smsEprs=new EndpointReferenceType[list.size()];
					for (int i = list.size() - 1; i >= 0; i--) {
						smsEprs[i]=list.get(i).getAccessibleStorageReference();
					}
				}
			}
			else{
				// old server
				smsEprs = props
						.getStorageFactoryProperties()
						.getAccessibleStorageReferenceArray();
			}
			
			if (smsEprs!=null && smsEprs.length > 0) {
				// add storages
				for (EndpointReferenceType epr : smsEprs) {
					try {
						Node node = NodeFactory.createNode(epr);
						newChildren.add(node);
					} catch (Exception e) {
					}
				}
			}
		}
		updateChildren(newChildren);

	}

	private EnumerationClient<AccessibleStorageReferenceDocument>createEnumClient(StorageFactoryPropertiesDocument props)
	throws Exception
	{
		EnumerationClient<AccessibleStorageReferenceDocument>client=null;
		if(props!=null && props.getStorageFactoryProperties().getAccessibleStorageEnumeration()!=null){
			EndpointReferenceType address=props.getStorageFactoryProperties().getAccessibleStorageEnumeration();
			client=new EnumerationClient<AccessibleStorageReferenceDocument>(address, getUASSecProps(), 
					AccessibleStorageReferenceDocument.type.getDocumentElementName());
		}
		return client;
	}
	
	public FileSystemType getFileSystem() {
		if (hasCachedResourceProperties()) {
			StorageFactoryPropertiesDocument storeProps = getCachedResourcePropertyDocument();
			try {
				return storeProps.getStorageFactoryProperties().getStorageDescriptionArray(0).getFileSystem();
			} catch (Exception e) {

			}
		}
		return null;
	}


	@Override
	public DetailsAdapter getDetails() {
		DetailsAdapter details = super.getDetails();
		Map<String, String> hm = details.getMap();
		FileSystemType fs = getFileSystem();
		if (fs != null)
		{
			if (fs.getFileSystemType() != null) {
				hm.put("File system type", fs.getFileSystemType().toString());
			}
			if(fs.getDescription() != null)
			{
				hm.put("Description",fs.getDescription());
			}
			if(fs.getMountPoint() != null)
			{
				hm.put("Mount point",fs.getMountPoint());
			}
			if (fs.getDiskSpace() != null) {
				try {
					long bytes = (long) Double.parseDouble(fs.getDiskSpace()
							.getExactArray(0).getStringValue());
					String space = FileUtils.humanReadableFileSize(bytes, ServiceBrowserActivator.getDefault()
							.getPreferenceStore().getBoolean(ServiceBrowserConstants.P_SIZE_UNITS_SI));
					hm.put("Disk space", space);
				} catch (Exception e) {
					// do nothing
				}
			}
		}
		return details;
	}

	@Override
	public void retrieveName() {
		URI uri = getURI();
		if (uri != null) {
			String identifier = "";
			try {
				identifier = getCachedResourcePropertyDocument().getStorageFactoryProperties().getStorageDescriptionArray(0).getStorageBackendType();
			} catch (Exception e) {
				// do nothing
			}

			setName("Storage factory "+identifier+" @"
					+ URIUtils.extractUnicoreServiceContainerName(uri));
		} else {
			super.retrieveName();
		}
	}

}
