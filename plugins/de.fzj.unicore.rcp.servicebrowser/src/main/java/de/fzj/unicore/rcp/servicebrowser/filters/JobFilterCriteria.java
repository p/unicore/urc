/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.filters;

import java.util.Calendar;

import org.unigrids.services.atomic.types.StatusType;

import de.fzj.unicore.rcp.common.UAS;
import de.fzj.unicore.rcp.servicebrowser.nodes.JobNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * implements a filter for Jobs by status and jobname as a regular expression,
 * is no more used
 * 
 * @author Christian Hohmann
 * 
 */
public class JobFilterCriteria extends TaskFilterCriteria {

	final static int int_undefined = 1;
	final static int int_ready = 2;
	final static int int_queued = 4;
	final static int int_running = 8;
	final static int int_successful = 16;
	final static int int_failed = 32;
	final static int int_stagein = 64;
	final static int int_stageout = 128;
	
	final static int int_all = int_undefined | int_ready | int_queued
			| int_running | int_successful | int_failed | int_stagein
			| int_stageout;
	final static int[] STATUS_ARRAY = new int[] { int_all, int_undefined,
			int_ready, int_queued, int_running, int_successful, int_failed,
			int_stagein, int_stageout };
	final static String[] STATUS_STRING_ARRAY = new String[] { "All Jobs",
			"Jobs with undefined status", "Ready Jobs", "Queued Jobs",
			"Running Jobs", "Successful Jobs", "Failed Jobs",
			"Jobs during stage-in", "Jobs during stage-out" };

	

	public JobFilterCriteria(int status) {
		this(status, "");
	}
	
	public JobFilterCriteria(int status, String pattern) {
		this(status, pattern, int_noTime, null, null);
	}

	public JobFilterCriteria(int status, String pattern, int timeMode, Calendar time1,
			Calendar time2) {
		super(status,pattern,timeMode,time1,time2);
	}

	public JobFilterCriteria(String pattern) {
		this(int_all, pattern);
	}

	
	

	protected boolean fitsNodeType(Node node) {
		return UAS.JMS.equals(node.getType());
	}

	
	
	protected boolean fitsTaskStatus(Node node) {
		if (!(node instanceof JobNode)) {
			return false;
		}
		// without the next line, jobs to which access was denied are not shown
		if(statusMode == int_all) return true; 	
										
		JobNode job = (JobNode) node;
		return isSet(getJobStatusCipher(job), statusMode);
	}

	

	protected int getJobStatusCipher(JobNode node) {

		StatusType.Enum status = node.getJobStatus();
		int result = 0;
		if (StatusType.QUEUED.equals(status)) {
			result = int_queued;
		} else if (StatusType.FAILED.equals(status)) {
			result = int_failed;
		} else if (StatusType.READY.equals(status)) {
			result = int_ready;
		} else if (StatusType.RUNNING.equals(status)) {
			result = int_running;
		} else if (StatusType.STAGINGIN.equals(status)) {
			result = int_stagein;
		} else if (StatusType.STAGINGOUT.equals(status)) {
			result = int_stageout;
		} else if (StatusType.SUCCESSFUL.equals(status)) {
			result = int_successful;
		} else if (StatusType.UNDEFINED.equals(status)) {
			result = int_undefined;
		}
		return result;
	}

	@Override
	protected Calendar getSubmissionTime(Node node) {
		if(!(node instanceof JobNode)) return null;
		JobNode job = (JobNode) node;
		return job.getNormalizedSubmissionTime();
	}

	@Override
	protected Calendar getTerminationTime(Node node) {
		if(!(node instanceof JobNode)) return null;
		JobNode job = (JobNode) node;
		return job.getNormalizedTime(job.getTerminationTime());
	} 

	
}