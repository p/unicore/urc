package de.fzj.unicore.rcp.servicebrowser.metadata;

import org.eclipse.core.runtime.IStatus;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;

/**
 * 
 * @author rajveer
 *
 */
public class AdvancedSearchHelper {

	public static String makeQuery(String allWords,
			String anyWords, String exactWords, String similarWords,
			String unwantedWords) {

		String advancedQuery = "";

		if(allWords != null && !allWords.equals("")) {
			advancedQuery = makePartialQuery(allWords.split(" "), " AND ");
		}

		if(anyWords != null && !anyWords.equals("")) {
			if(!advancedQuery.equals("")) {
				advancedQuery = advancedQuery + " AND " + makePartialQuery(anyWords.split(" "), " OR ");
			} else {
				advancedQuery = makePartialQuery(anyWords.split(" "), " OR ");
			}

		}

		if(exactWords != null && !exactWords.equals("")) {
			if(!advancedQuery.equals("")) {
				advancedQuery = advancedQuery + " AND " + "\"" + exactWords + "\"";
			}else {
				advancedQuery = "\"" + exactWords + "\"";
			}
		}

		if(similarWords != null && !similarWords.equals("")) {
			if(!advancedQuery.equals("")) {
				advancedQuery = advancedQuery + " "+ similarWords + "*" + " OR "+  similarWords + "~" ;
			} else {
				advancedQuery = similarWords + "*" + " OR "+  similarWords + "~" ;
			}
		}

		if(unwantedWords != null && !unwantedWords.equals("")) {
			if(!advancedQuery.equals("")) {
				advancedQuery = advancedQuery + "" + makeNagativeQuery(unwantedWords.split(" "), " AND NOT ");
			} else {
				// If only unwanted words are given but all other fields are empty, a nullpointer exception is thrown
				// to hide it, display error message
				ServiceBrowserActivator
						.log(IStatus.ERROR,
								"It is impossible to search metadata only for unwanted words.");
			}
		}
//		System.out.println("Query:"+advancedQuery);

		return advancedQuery;
	}
	private static String makeNagativeQuery(String[] words, String opr) {
		String partialQuery = "";
		for(int i=0; i<words.length; i++) {
			partialQuery = partialQuery + opr + " " +words[i] + " ";
		}
		return partialQuery;
	}

	private static String makePartialQuery (String[] words, String opr) {
		String partialQuery = "";
		for(int i=0; i<words.length; i++) {
			if(i+1 != words.length) {
				partialQuery = partialQuery + words[i] + " "+opr;
			} else {
				partialQuery = partialQuery + words[i];
			}
		}
		partialQuery = " (" + partialQuery + ") ";
//		System.out.println("partialQuery:"+partialQuery);
		return partialQuery;
	}
}
