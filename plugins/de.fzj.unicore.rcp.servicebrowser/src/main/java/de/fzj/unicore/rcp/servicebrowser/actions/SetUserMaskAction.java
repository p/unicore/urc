package de.fzj.unicore.rcp.servicebrowser.actions;

import org.apache.xmlbeans.XmlException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.oasisOpen.docs.wsrf.rp2.UpdateResourcePropertiesDocument;
import org.oasisOpen.docs.wsrf.rp2.UpdateResourcePropertiesDocument.UpdateResourceProperties;
import org.oasisOpen.docs.wsrf.rp2.UpdateType;
import org.unigrids.services.atomic.types.UmaskDocument;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.rcp.servicebrowser.ui.SetUmaskDialog;
import de.fzj.unicore.wsrflite.xmlbeans.WSUtilities;
import de.fzj.unicore.wsrflite.xmlbeans.client.BaseWSRFClient;

public class SetUserMaskAction extends NodeAction {

	protected String umaskString;
	protected boolean apply;

	public SetUserMaskAction(StorageNode node) {
		super(node);
		setText("Set Storage Umask");
		setToolTipText("Set storage user mask.");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("file_permissions.png"));
		
	}

	@Override
	public void run() {
		final StorageNode storageNode = (StorageNode) getNode();
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {
				Shell s = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getShell();
				
				SetUmaskDialog dialog = new SetUmaskDialog(s, storageNode);
				int result = dialog.open();
				apply = (Window.OK == result  &&  dialog.isUmaskChanged());
				if (apply) {
					umaskString = dialog.getNewUmask();					
				}
			}
		});
		if (apply) {
			try {
				BaseWSRFClient client = null;
				client = storageNode.createUASClient();
				
				String umaskDocS;
				UmaskDocument umaskDoc;
				umaskDocS = client.getResourceProperty(UmaskDocument.type.getDocumentElementName());
				
				if (umaskDocS == null) {
					throw new Exception("The selected service doesn't support umask setting");
				}
				try {
					umaskDoc = UmaskDocument.Factory.parse(umaskDocS);
				} catch (XmlException e) {
					throw new Exception("The received resource property has invalid format. " +
							"Perhaps the service version is incompatible with this client.", e);
				}
				
				// prepare update document
				UpdateResourcePropertiesDocument reqDoc = UpdateResourcePropertiesDocument.Factory.newInstance();
				UpdateResourceProperties req = reqDoc.addNewUpdateResourceProperties();
				UpdateType update = req.addNewUpdate();
				umaskDoc = UmaskDocument.Factory.newInstance();
				umaskDoc.setUmask( umaskString );
				WSUtilities.insertAny(umaskDoc, update);
				client.getRP().UpdateResourceProperties(reqDoc);
			
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.ERROR, "Unable to set storage umask", e);
			}			

			storageNode.refresh(0, false, null);
		}
	}

}
