/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.common.guicomponents.SelectTimeDialog;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;
import de.fzj.unicore.uas.client.BaseUASClient;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * @author demuth
 * 
 */
public class SetTerminationTimeAction extends NodeAction {

	public SetTerminationTimeAction(WSRFNode node) {
		super(node);
		setText("Set Termination Time");
		setToolTipText("Set termination time of this ws resource");
		setImageDescriptor(UnicoreCommonActivator
				.getImageDescriptor("clock_edit.png"));
	}

	@Override
	public boolean isCurrentlyAvailable() {
		if (UnicoreCommonActivator.getDefault().getGridDetailLevel() < UnicoreCommonActivator.LEVEL_ADVANCED) {
			return false;
		} else {
			return true;
		}
	}

	private void popupTermTimeDialog(final WSRFNode node) {
		Shell s = null;
		try {
			s = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		} catch (Exception e) {

		}
		if (s == null) {
			s = new Shell();
		}
		SelectTimeDialog dialog = new SelectTimeDialog(s,
				"Adjust the termination time of the selected service(s):");
		Calendar current = node.getTerminationTime();
		if(current == null)
		{
			ServiceBrowserActivator.log(IStatus.ERROR,"Error: the selected WSRF resource does not have a termination time!");
			return;
		}
		dialog.setInitialSelection(current.getTime());
		dialog.setMinTime(Calendar.getInstance().getTime());
		// only set new time if user says ok
		if(dialog.open() == MessageDialog.OK) {
			Date newTT = dialog.getSelectedTime();
			if (newTT != null) {
				Calendar c = Calendar.getInstance();
				c.setTime(newTT);
				setTermTime(c);
			}
		}
		return;
	}

	@Override
	public void run() {

		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {

				RefreshAction a = new RefreshAction(getNode());
				a.setViewer(getViewer());
				a.run();
				popupTermTimeDialog((WSRFNode) getNode());
			}
		});

	}

	private void setTermTime(Calendar newTermTime) {
		WSRFNode wsr = (WSRFNode) getNode();
		SimpleDateFormat sdf = Constants.getDefaultDateFormat();
		ServiceBrowserActivator.log("Setting termination time of "
				+ wsr.getName() + " to " + sdf.format(newTermTime.getTime()));
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue(wsr.getURL().toString());
		try {
			IClientConfiguration secProps = wsr.getUASSecProps();
			BaseUASClient client = new BaseUASClient(wsr.getURL().toString(),
					epr, secProps);
			client.setTerminationTime(newTermTime);
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.WARNING, "Cannot set new termination time", e);
		}
		wsr.refresh(0, false, null);
	}
}
