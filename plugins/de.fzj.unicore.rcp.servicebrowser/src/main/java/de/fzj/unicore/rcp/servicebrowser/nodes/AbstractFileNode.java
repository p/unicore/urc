/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.beans.PropertyChangeEvent;
import java.io.FileNotFoundException;
import java.net.URI;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.preference.IPreferenceStore;
import org.unigrids.services.atomic.types.GridFileType;
import org.unigrids.services.atomic.types.ProtocolType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.common.utils.FileUtils;
import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.actions.ChangeGroupAction;
import de.fzj.unicore.rcp.servicebrowser.actions.ChangeModeAction;
import de.fzj.unicore.rcp.servicebrowser.actions.CopyNodesAction;
import de.fzj.unicore.rcp.servicebrowser.actions.CutNodesAction;
import de.fzj.unicore.rcp.servicebrowser.actions.MonitorAction;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.actions.PasteNodesAction;
import de.fzj.unicore.rcp.servicebrowser.actions.SetFilePermissionsAction;
import de.fzj.unicore.rcp.servicebrowser.actions.ShowDetailsAction;
import de.fzj.unicore.uas.client.StorageClient;
import de.fzj.unicore.wsrflite.xmlbeans.BaseFault;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * @author demuth
 * 
 */
public abstract class AbstractFileNode extends SecuredNode {

	private static final long serialVersionUID = -1887178564535477612L;

	public static final String PROPERTY_STORAGE_EPR = "storage epr";
	public static final String PROPERTY_STORAGE_CLIENT = "storage client";
	public static final String PROPERTY_GRID_FILE_TYPE = "grid file type";
	public static final String PROPERTY_SERVER_VERSION = "server version";
	public static final String PROPERTY_SUPPORTED_PROTOCOLS = "supported protocols";


	protected String storageName;
	
	protected transient String storageEPRString;
	protected transient String gridFileTypeString;


	protected static AtomicInteger persistedFileCount = new AtomicInteger(0);


	public AbstractFileNode(EndpointReferenceType epr) {
		super(epr);

	}

	public void afterDeserialization() {
		super.afterDeserialization();

	}


	@Override
	public AbstractFileNode clone() {
		AbstractFileNode result = (AbstractFileNode) super.clone();
		result.storageName = storageName;
		return result;
	}

	@Override
	public int compareTo(Node n) {
		if (n instanceof MoreFilesNode) {
			return -1;
		} else if (n instanceof AbstractFileNode) {
			return 0; // do not sort files on the client as this looks weird
		} else {
			return super.compareTo(n);
		}
	}

	@Override
	public int getCategory() {
		return 400;
	}

	@Override
	public DetailsAdapter getDetails() {
		DetailsAdapter details = super.getDetails();
		GridFileType gft = getGridFileType();
		if (gft != null) {
			IPreferenceStore ps = ServiceBrowserActivator.getDefault().getPreferenceStore();
			long size = gft.getSize();
			details.getMap().put("File size",
					FileUtils.humanReadableFileSize(size, ps.getBoolean(ServiceBrowserConstants.P_SIZE_UNITS_SI)));

			if (gft.getLastModified() != null) {
				details.getMap().put(
						"Last modified",
						getDefaultDateFormat().format(
								gft.getLastModified().getTime()));
			}
			if (gft.getPermissions() != null) {
				boolean read = gft.getPermissions().getReadable();
				boolean write = gft.getPermissions().getWritable();
				boolean execute = gft.getPermissions().getExecutable();
				details.getMap().put(
						"File permissions",
						"read=" + read + ", write=" + write + ", execute="
						+ execute);
			}
			if(gft.getOwner()!=null){
				details.getMap().put("Owner",gft.getOwner());
			}
			if(gft.getGroup()!=null){
				details.getMap().put("Group",gft.getGroup());
			}
			if(gft.getFilePermissions()!=null){
				details.getMap().put("Unix permissions",gft.getFilePermissions());
			}
			// TODO ACL
		}
		return details;
	}

	public URI getFileAddressWithoutProtocol() {
		try {
			return new URI(getURI().toString());
		} catch (Exception e) {
			return null;
		}
	}

	public URI getFileAddressWithProtocol() {
		try {
			ProtocolType.Enum[] preferred = UnicoreStorageTools.getPreferredProtocolOrder(10*1024*1024);
			ProtocolType.Enum[] supportedProtocols = getSupportedProtocols();

			if (supportedProtocols != null) {
				for (ProtocolType.Enum p : preferred) {
					for (ProtocolType.Enum test : getSupportedProtocols()) {
						if (test.equals(p))
							return new URI(p + ":" + getURI().toString());
					}
				}
			}

			return new URI(ProtocolType.BFT + ":" + getURI().toString()); // fallback to BFT
		} catch (Exception e) {
			ServiceBrowserActivator.getDefault();
			ServiceBrowserActivator.log(IStatus.ERROR,
					"Unable to determine protocol for transferring the file "
					+ getURI(), e);
			return null;
		}
	}

	public GridFileType getGridFileType() {
		INodeData data = getData();
		
		if(gridFileTypeString == null)
		{
			if (data == null) {
				return null;
			}
			gridFileTypeString = ((String) data
					.getProperty(PROPERTY_GRID_FILE_TYPE));
		}
		try {
			return GridFileType.Factory.parse(gridFileTypeString);
		} catch (Exception e) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.Node#getLowLevelAsString()
	 */
	@Override
	public String getLowLevelAsString() {
		if (getGridFileType() != null) {
			return getGridFileType().toString();
		} else {
			return null;
		}
	}

	/**
	 * Tries to return the cached version of the server hosting this file. If
	 * the version was never requested before, this will trigger a WS call, so
	 * this operation must be considered long-running!!!
	 */
	public String getServerVersion()
	{
		String version = (String) getData().getProperty(PROPERTY_SERVER_VERSION);
		if(version != null) return version;
		StorageClient sc = getStorageClient();
		if(sc == null) return null;
		try {
			version = sc.getServerVersion();
		} catch (Exception e) {
			return null;
		}
		if(version != null)
		{
			putData(PROPERTY_SERVER_VERSION, version);
		}
		return version;
	}

	public StorageClient getStorageClient() {
		EndpointReferenceType epr = getStorageEPR();
		if (epr == null) {
			return null; // this may occur when the file has been deleted on the
			// server and a refresh has revealed this to the
			// client
		}
		StorageClient storageClient;
		try {
			storageClient = new StorageClient(epr, getUASSecProps());
			return storageClient;
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.ERROR,
					"Unable to create storage client for address " + epr);
		}
		return null;
	}

	public EndpointReferenceType getStorageEPR() {
		INodeData data = getData();
			if(storageEPRString == null)
			{
				if(data == null) return null;
				storageEPRString = ((String) data.getProperty(PROPERTY_STORAGE_EPR));
			}
			try {
				return EndpointReferenceType.Factory
				.parse(storageEPRString);
			} catch (Exception e) {
				return null;
			}
		
	}

	public String getStorageName() {
		if (storageName == null) {
			if (getParent() != null) {
				Node parent = getParent();
				if (parent instanceof FolderNode) {
					storageName = ((FolderNode) parent).getStorageName();
				} else if (parent instanceof StorageNode) {
					storageName = ((StorageNode) parent).getName();
				}
			}
		}
		return storageName;
	}

	public ProtocolType.Enum[] getSupportedProtocols() {
		try {
			return (ProtocolType.Enum[]) getData().getProperty(
					PROPERTY_SUPPORTED_PROTOCOLS);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public IClientConfiguration getUASSecProps() throws Exception {
		IClientConfiguration result = super.getUASSecProps();
		if (result != null && 
				!Boolean.parseBoolean(String.valueOf(result.getExtraSecurityTokens().get(IdentityActivator.DEFAULT_PROPERTIES)))) 
		{
			return result;
		}

		// fallback to storage's security properties
		return ServiceBrowserActivator.getDefault().getUASSecProps(new URI(getStorageEPR().getAddress().getStringValue()));
		
	}

	@Override
	public void init() {
		super.init();
		// set the state to ready as we already have retrieved all relevant
		// information
		// from the server
		// Otherwise, file nodes are not sorted properly
		setState(STATE_READY);
	}

	public boolean isPersistable()
	{
		final int max = ServiceBrowserActivator.getDefault().getPreferenceStore().
		getInt(ServiceBrowserConstants.P_NUM_PERSISTED_FILE_NODES);

		int count = persistedFileCount.incrementAndGet();
		return count <= max;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		super.propertyChange(evt);
		if (PROPERTY_GRID_FILE_TYPE.equals(evt.getPropertyName())) {
			retrieveName();
		}
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		for (Iterator<NodeAction> iterator = availableActions.iterator(); iterator
		.hasNext();) {
			Object action = iterator.next();
			if (action instanceof MonitorAction) {
				iterator.remove();
			}

		}
		GridFileType gft = getGridFileType();
		if (gft != null) {
			if (gft.getFilePermissions() != null) {
				// first, check if advanced file permissions are available
				availableActions.add(new ChangeModeAction(this));
			}			
			else if (gft.getPermissions() != null) {
				// if not, check simple permissions are available
				availableActions.add(new SetFilePermissionsAction(this));
			}

			// adding chgrp action if group information is available
			if (gft.getGroup() != null) {
				availableActions.add(new ChangeGroupAction(this));
			}
		}
		availableActions.add(new CutNodesAction(this));
		availableActions.add(new CopyNodesAction(this));
		availableActions.add(new PasteNodesAction(this));
		availableActions.add(new ShowDetailsAction(this));
	}

	@Override
	protected int retrieveProperties(IProgressMonitor progressMonitor) {
		GridFileType gft = null;
		try {
			StorageClient storageClient = getStorageClient();
			if (storageClient == null) {
				return STATE_FAILED;
			}
			gft = storageClient.listProperties(getGridFileType().getPath());
		} catch (BaseFault e) {
			ServiceBrowserActivator.log(IStatus.WARNING,
					"Unable to retrieve properties of file "
					+ getNamePathToRoot(), e);
			return STATE_FAILED;
		} catch (FileNotFoundException e) {
			getData().dispose();
		} catch (Exception e) {
			return STATE_ACCESS_DENIED;
		}

		if (gft != null) {
			setGridFileType(gft);
		}
		return super.retrieveProperties(progressMonitor);
	}

	public void setGridFileType(GridFileType gridFileType) {
		if(gridFileType != null)
		{
			gridFileTypeString = gridFileType.toString();
			putData(PROPERTY_GRID_FILE_TYPE, gridFileTypeString);
		}
		else gridFileTypeString = null;
	}

	public void setStorageEPR(EndpointReferenceType storageEPR) {
		putData(PROPERTY_STORAGE_EPR, storageEPR.toString());
	}

	public void setStorageName(String storageName) {
		this.storageName = storageName;
	}

	public void setSupportedProtocols(ProtocolType.Enum[] protocols) {
		putData(PROPERTY_SUPPORTED_PROTOCOLS, protocols);
	}
}
