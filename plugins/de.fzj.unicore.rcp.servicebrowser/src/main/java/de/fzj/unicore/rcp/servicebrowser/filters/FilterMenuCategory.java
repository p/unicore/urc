/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.filters;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

/**
 * provides services for the MenuCategory, that is used to specify the location
 * of an Action in a list of qualified names.
 * 
 * @author Christian Hohmann
 * 
 */
public class FilterMenuCategory {

	private List<QName> menuLocation;

	public FilterMenuCategory(List<QName> menuLocation) {
		if (menuLocation == null) {
			this.menuLocation = new ArrayList<QName>();
		}
		this.menuLocation = menuLocation;
	}

	/**
	 * 
	 * @return depth of MenuLocation
	 */
	public int getDepth() {
		return this.menuLocation.size();
	}

	public QName getElement(int index) {
		if (index > this.menuLocation.size() || index < 0) {
			return null;
		}
		return this.menuLocation.get(index);
	}

	/**
	 * returns a FilterMenuCategory holding the QName List with the list
	 * elements of the index lower and equals depth of this element.
	 * 
	 * @param depth
	 * @return
	 */
	public FilterMenuCategory getElementToDepth(int depth) {

		if (depth < 0 || depth > this.menuLocation.size()) {
			return null;
		}
		List<QName> retval = new ArrayList<QName>();
		for (int i = 0; i <= depth; i++) {
			retval.add(this.menuLocation.get(i));
		}
		return new FilterMenuCategory(retval);
	}

	public List<QName> getMenuLocation() {
		return menuLocation;
	}

	/**
	 * 
	 * @return a string containing the menuLocation seperated by '/'
	 */
	public String getMenuPath() {
		String retval = "";
		if (menuLocation.size() == 0) {
			retval = "/";
		} else {
			for (int i = 0; i < menuLocation.size(); i++) {
				retval = retval + this.menuLocation.get(i).getLocalPart() + "/";
			}
			retval = retval.substring(0, retval.length() - 1);
		}
		return retval;
	}

	public void setMenuLocation(List<QName> menuLocation) {
		this.menuLocation = menuLocation;
	}
	
}