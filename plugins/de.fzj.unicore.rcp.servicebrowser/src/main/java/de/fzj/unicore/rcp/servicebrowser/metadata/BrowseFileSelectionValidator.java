package de.fzj.unicore.rcp.servicebrowser.metadata;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;

public class BrowseFileSelectionValidator implements ISelectionStatusValidator {

	public BrowseFileSelectionValidator() {
	}
	
	@Override
	public IStatus validate(Object[] selection) {
		if (selection != null
				&& selection.length == 1
				&& (selection[0] instanceof FileNode)) {
				return new Status(IStatus.OK, PlatformUI.PLUGIN_ID, IStatus.OK, "",
						null);
			}
		return new Status(IStatus.ERROR, PlatformUI.PLUGIN_ID, IStatus.ERROR,
				"No suitable file selected.", null);
	}

}
