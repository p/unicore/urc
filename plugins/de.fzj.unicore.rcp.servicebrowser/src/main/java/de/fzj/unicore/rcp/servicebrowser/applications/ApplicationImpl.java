package de.fzj.unicore.rcp.servicebrowser.applications;

public class ApplicationImpl implements Application {

	/**
	 * The indication of <i>any application</i>
	 */
	public static final ApplicationImpl ANY_APPLICATION = new ApplicationImpl(
			"ANY_APPLICATION", "1.0");

	private String name;
	private String version;

	public ApplicationImpl(String name, String version) {
		super();
		this.name = name;
		this.version = version;
	}

	@Override
	public boolean equals(Object app) {
		if (app instanceof Application) {
			return ((Application) app).getName().equals(getName())
					&& ((Application) app).getApplicationVersion().equals(
							getApplicationVersion());
		}
		return false;
	}

	public String getApplicationVersion() {
		return version;
	}

	public String getDescription() {
		return "";
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public String toString() {
		return this.name + " " + this.version;
	}
}
