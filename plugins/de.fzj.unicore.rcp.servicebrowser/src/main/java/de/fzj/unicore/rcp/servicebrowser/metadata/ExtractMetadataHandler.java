/*********************************************************************************
 * Copyright (c) 2015 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.metadata;

import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.unigrids.services.atomic.types.StatusType;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.RootNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.uas.client.StorageClient;
import de.fzj.unicore.uas.client.TaskClient;

public class ExtractMetadataHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPage wbPage = ServiceBrowserActivator.getDefault()
				.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		
		ISelection sel = wbPage.getActivePart().getSite().getSelectionProvider().getSelection();
		if (sel instanceof IStructuredSelection) {
			IStructuredSelection sSel = (IStructuredSelection) sel;
			Iterator iter = sSel.iterator();
			while (iter.hasNext()) {
				Object o = iter.next();
				if (o instanceof IAdaptable) {
					// TODO reduce this code to treat storages and folders in the same way
					StorageNode sNode = (StorageNode) ((IAdaptable) o)
							.getAdapter(StorageNode.class);
					FolderNode dNode = null;
					StorageClient sClient;
					String path = null;
					try {
						if (sNode != null) {
							sClient = sNode.getStorageClient();
						} else {
							// directory
							dNode = (FolderNode) ((IAdaptable) o)
									.getAdapter(FolderNode.class);
							sClient = dNode.getStorageClient();
							// set sNode, even if only to create error message
							sNode = getStorageNode(dNode);
							path = dNode.getGridFileType().getPath();
						}
						InputDialog dialog = new InputDialog(
								PlatformUI.getWorkbench()
										.getActiveWorkbenchWindow().getShell(),
								"Depth of metadata extraction",
								"To which depth in the directory hierarchy starting at \""
										+ (path == null ? "/" : path)
										+ "\" would you like to extract the metadata? Use"
										+ " 1 to process only the current level.",
								"10", new IInputValidator() {

									@Override
									public String isValid(String newText) {
										try {
											if (Integer.parseInt(newText) < 0) {
												return "Depth must be a positive integer.";
											}
										} catch (NumberFormatException e) {
											return "Invalid integer format:"
													+ e.getMessage();
										}
										return null;
									}
								});
						if (dialog.open() == InputDialog.OK) {
							final TaskClient extraction = sClient
									.getMetadataClient()
									.startMetadataExtraction(path, Integer
											.parseInt(dialog.getValue()));
							ExtractionMonitor monitor = new ExtractionMonitor(
									"Metadata extraction on "
											+ sClient.getStorageName(),
									extraction);
							monitor.schedule();
						}
					} catch (Exception e) {
						ServiceBrowserActivator.log(Status.WARNING,
								"Unable to start metadata extraction on \""
										+ sNode.getNamePathToRoot() + "\"", e);
					}
				}
			}
		}
		return null;
	}

	/**
	 * @param dNode
	 * @return
	 */
	private StorageNode getStorageNode(FolderNode dNode) {
		Node current = dNode;
		while(!(current instanceof StorageNode)) {
			current = current.getParent();
			if(current instanceof RootNode || current == null) {
				return null;
			}
		}
		return (StorageNode) current;
	}

	private class ExtractionMonitor extends Job {

		private final TaskClient extraction;

		/**
		 * @param name
		 */
		public ExtractionMonitor(String name, TaskClient _extraction) {
			super(name);
			this.extraction = _extraction;
		}

		/**
		 * @see de.fzj.unicore.rcp.common.utils.BackgroundWorker#run(org.eclipse.core.runtime.IProgressMonitor)
		 */
		@Override
		protected IStatus run(IProgressMonitor monitor) {
			monitor.beginTask(getName(), 1000);
			boolean keepGoing = true;
			int errorCount = 0;
			boolean progressSet = false;
			final long startTime = System.currentTimeMillis();
			float lastProgress = 0;

			while(keepGoing) {
				try {
					StatusType.Enum status = extraction.getStatus();
					keepGoing = status != StatusType.SUCCESSFUL && status != StatusType.FAILED;
					String runtimeMsg = "DONE.";
					if(keepGoing) {
						runtimeMsg = " Runtime: " + (System.currentTimeMillis() - startTime) / 1000 + "s.";
					}

					Float progress = extraction.getProgress();
					if(progress != null) {
						monitor.worked((int) ((progress - lastProgress) *1000));
					} else { // no progress information available
						monitor.setTaskName(getName() + ": no progress information available." + runtimeMsg);
						if(!progressSet) { // set progress to 50% once
							monitor.worked(500);
							progressSet = true;
						}
					}
					lastProgress = progress;
				} catch (Exception e) {
					errorCount++;
					if(errorCount > 10) { // give up on too many errors
						keepGoing = false;
					}
				}
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {

				}
			}

			return Status.OK_STATUS;
		}

	}

}
