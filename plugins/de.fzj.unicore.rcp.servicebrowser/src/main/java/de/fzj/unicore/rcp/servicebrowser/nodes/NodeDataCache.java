/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.utils.BackgroundWorker;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.utils.URIUtils;

/**
 * @author demuth
 * 
 */
public class NodeDataCache implements INodeDataCache {

	private transient Lock nodeDataStorageLock = new ReentrantLock();
	private transient BackgroundWorker worker = new BackgroundWorker("");

	protected Map<String, INodeData> nodeDataStorage = new HashMap<String, INodeData>();
	protected IPropertyCache propertyCache;

	// Node listeners are kept in a set for fast evaluation of the contains()
	// method. They are also kept in an array for quickly notifying them
	// without running into ConcurrentModificationExceptions when they
	// decide to un-register during notification
	protected transient Set<INodeCacheListener> nodeListeners = new HashSet<INodeCacheListener>();
	protected transient INodeCacheListener[] nodeListenerArray = new INodeCacheListener[0];

	/**
	 * Counters for keeping track of how many nodes share this node data. If
	 * this number reaches 0, the node data should be deleted from the node data
	 * getCache().
	 */
	protected Map<String, Integer> nodeCounts = new HashMap<String, Integer>();

	public NodeDataCache() {
		super();
		loadExtensions();
	}

	public void addNodeFactoryListener(INodeCacheListener l) {
		Set<INodeCacheListener> nodeListeners = getNodeListeners();
		if (!nodeListeners.contains(l)) {
			nodeListeners.add(l);
			nodeListenerArray = nodeListeners
			.toArray(new INodeCacheListener[nodeListeners.size()]);
		}
	}

	public void afterDeserialization() {
		
		// first add all listeners
		loadExtensions();
		List<INodeData> nodeData = getAllNodeData();
		// now tell the listeners that we have lots of node data objects
		for (INodeCacheListener l : getNodeListenerArray()) {
			for (INodeData nd : nodeData) {
				l.nodeDataAdded(nd);
			}
		}
		worker = new BackgroundWorker("");
	}
	
	private Object readResolve()
	{
		nodeDataStorageLock = new ReentrantLock();
		return this;
	}
	
	public void afterSerialization() {
		nodeDataStorageLock.unlock();
	}

	public void beforeSerialization() {
		nodeDataStorageLock.lock();
	}

	public void clearAllData() {
		if (nodeDataStorage != null) {
			nodeDataStorage.clear();
		}
		getPropertyCache().clearAllProperties();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.INodeDatagetCache()#
	 * decrementNodeCount(org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public void decrementNodeCount(EndpointReferenceType epr) {
		nodeDataStorageLock.lock();
		try {
			String key = getKey(epr);
			int oldCount = getNodeCount(epr);
			oldCount--;
			if (oldCount < 0) {
				oldCount = 0;
			}
			if (oldCount == 0) {
				removeNodeData(epr);
			} else {
				nodeCounts.put(key, oldCount);
			}
		}
		finally {
			nodeDataStorageLock.unlock();
		}
	}

	public void flushToDisk() throws IOException {
		getPropertyCache().flushToDisk();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.nodes.INodeDatagetCache()#getAllNodeData
	 * ()
	 */
	public List<INodeData> getAllNodeData() {
		nodeDataStorageLock.lock();
		try {
			List<INodeData> result = new ArrayList<INodeData>(
					getNodeDataStorage().values());
			return result;
		}
		finally {
			nodeDataStorageLock.unlock();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.nodes.INodeDatagetCache()#getNodeCount
	 * (org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public Integer getNodeCount(EndpointReferenceType epr) {
		nodeDataStorageLock.lock();
		try {
			String key = getKey(epr);
			Integer count = nodeCounts.get(key);

			if (count == null) {
				return 0;
			} else {
				return count;
			}
		}
		finally {
			nodeDataStorageLock.unlock();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.nodes.INodeDatagetCache()#getNodeData
	 * (java.net.URI)
	 */
	public INodeData getNodeData(EndpointReferenceType epr) {
		nodeDataStorageLock.lock();
		try {
			String key = getKey(epr);
			INodeData data = getNodeDataStorage().get(key);
			if (data == null) {
				data = new NodeData(epr, getPropertyCache());
				putNodeData(data);
			}

			return data;
		}
		finally {
			nodeDataStorageLock.unlock();
		}
	}

	private Map<String, INodeData> getNodeDataStorage() {
		return nodeDataStorage;
	}

	private INodeCacheListener[] getNodeListenerArray() {
		if (nodeListenerArray == null) {
			nodeListenerArray = new INodeCacheListener[0];
		}
		return nodeListenerArray;
	}

	private Set<INodeCacheListener> getNodeListeners() {
		if (nodeListeners == null) {
			nodeListeners = new HashSet<INodeCacheListener>();
		}
		return nodeListeners;
	}

	public IPropertyCache getPropertyCache() {
		if (propertyCache == null) {
			propertyCache = new EHPropertyCache();
		}
		return propertyCache;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.INodeDatagetCache()#
	 * incrementNodeCount(org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public void incrementNodeCount(EndpointReferenceType epr) {
		nodeDataStorageLock.lock();
		try {
			String key = getKey(epr);
			int oldCount = getNodeCount(epr);
			nodeCounts.put(key, ++oldCount);
		}
		finally {
			nodeDataStorageLock.unlock();
		}
	}

	private void loadExtensions() {
		// iterate over extensions and find right extension to create the node
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
		.getExtensionPoint(ServiceBrowserConstants.NODE_CACHE_LISTENER_EXTENSION_POINT);
		IConfigurationElement[] members = extensionPoint
		.getConfigurationElements();
		// For each extension
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			try {
				INodeCacheListener ext = (INodeCacheListener) member
				.createExecutableExtension("class");
				getNodeListeners().add(ext);
			} catch (CoreException ex) {
				// do nothing
			}
		}
		nodeListenerArray = getNodeListeners().toArray(
				new INodeCacheListener[nodeListeners.size()]);
	}

	private void notifyNodeDataAdded(INodeData nd) {
		for (INodeCacheListener l : getNodeListenerArray()) {
			l.nodeDataAdded(nd);
		}
	}

	private void notifyNodeDataRemoved(INodeData nd) {
		for (INodeCacheListener l : getNodeListenerArray()) {
			l.nodeDataRemoved(nd);
		}
	}

	public void putNodeData(final INodeData data) {
		nodeDataStorageLock.lock();
		try {
			String key = getKey(data.getEpr());
			INodeData old = getNodeDataStorage().get(key);
			getNodeDataStorage().put(key, data);
			if (old == null) {
				Runnable r = new Runnable() {
					
					public void run() {
						notifyNodeDataAdded(data);
					}
				};
				worker.scheduleWork(r);
			}
		}
		finally {
			nodeDataStorageLock.unlock();
		}
	}

	public void removeNodeData(EndpointReferenceType epr) {
		nodeDataStorageLock.lock();
		try {
			String key = getKey(epr);
			nodeCounts.remove(key);
			final INodeData data = getNodeDataStorage().remove(key);
			if (data != null) {
				 Runnable r = new Runnable() {
					public void run() {
						notifyNodeDataRemoved(data);
						getPropertyCache().clearPropertiesFor(data);
						data.dispose();
					}
				};
				worker.scheduleWork(r);
			}
		}
		finally {
			nodeDataStorageLock.unlock();
		}
	}

	public void removeNodeFactoryListener(INodeCacheListener l) {
		getNodeListeners().remove(l);
		nodeListenerArray = nodeListeners
		.toArray(new INodeCacheListener[nodeListeners.size()]);

	}

	
	
	public void setNodeCount(EndpointReferenceType epr, int nodeCount) {
		nodeDataStorageLock.lock();
		try {
			String key = getKey(epr);
			nodeCounts.put(key, nodeCount);
		}
		finally {
			nodeDataStorageLock.unlock();
		}

	}

	static String getKey(EndpointReferenceType epr) {
		try {
			URI uri = new URI(epr.getAddress().getStringValue());
			return URIUtils.toNormalizedString(uri);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	

}
