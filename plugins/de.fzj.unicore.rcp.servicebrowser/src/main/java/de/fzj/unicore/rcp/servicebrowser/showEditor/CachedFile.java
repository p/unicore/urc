package de.fzj.unicore.rcp.servicebrowser.showEditor;

import java.io.InputStream;
import java.io.Reader;
import java.net.URI;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFileState;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IPathVariableManager;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourceAttributes;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;

/**
 * A remote file that has been cached locally. When writing to this file, the
 * changes are stored on local disk, as well as the remote location. Since we
 * cannot inherit from org.eclipse.core.internal.resources.File, we use
 * delegation here. If the remote file is kept synchronous to the local file and
 * the cached file is not needed anymore, you MUST either delete the local file
 * or call the {@link #dispose()} method.
 * 
 * @author bdemuth
 * 
 */
public class CachedFile implements IFile, IResourceChangeListener {

	protected IFile localFile;
	protected IRemoteFile remoteFile;
	protected boolean syncLocalToRemote = false, cleanUpWhenContentRetrieved;
	protected boolean cachingComplete, cachingFailed;

	public CachedFile(IFile localFile, IRemoteFile remoteFile) {
		this(localFile, remoteFile, false);

	}

	public CachedFile(IFile localFile, IRemoteFile remoteFile,
			boolean syncLocalToRemote) {
		this.localFile = localFile;
		this.remoteFile = remoteFile;
		this.syncLocalToRemote = syncLocalToRemote;
	}

	public void accept(IResourceProxyVisitor visitor, int memberFlags)
			throws CoreException {
		localFile.accept(visitor, memberFlags);
	}

	public void accept(IResourceVisitor visitor) throws CoreException {
		localFile.accept(visitor);
	}

	public void accept(IResourceVisitor visitor, int depth,
			boolean includePhantoms) throws CoreException {
		localFile.accept(visitor, depth, includePhantoms);
	}

	public void accept(IResourceVisitor visitor, int depth, int memberFlags)
			throws CoreException {
		localFile.accept(visitor, depth, memberFlags);
	}

	/**
	 * @see org.eclipse.core.resources.IResource#accept(org.eclipse.core.resources.IResourceProxyVisitor,
	 *      int, int)
	 */
	@Override
	public void accept(IResourceProxyVisitor visitor, int depth, int memberFlags)
			throws CoreException {
		localFile.accept(visitor, depth, memberFlags);

	}

	public void appendContents(InputStream source, boolean force,
			boolean keepHistory, IProgressMonitor monitor) throws CoreException {
		localFile.appendContents(source, force, keepHistory, monitor);
	}

	public void appendContents(InputStream source, int updateFlags,
			IProgressMonitor monitor) throws CoreException {
		localFile.appendContents(source, updateFlags, monitor);
	}

	public IStatus cache(IProgressMonitor progress) throws Exception {
		if (!cachingFailed && !cachingComplete) {
			try {
				if (!localFile.exists()) {
					PathUtils.createFile(localFile, null);
				}
				InputStream is = remoteFile.getContents();
				localFile.setContents(is, true, false, progress);
				cachingComplete = localFile.getLocation().toFile().length() == remoteFile
						.getSize();

				if (syncLocalToRemote) {
					ResourcesPlugin.getWorkspace().addResourceChangeListener(
							this);
				}
				if (cachingComplete) {
					return Status.OK_STATUS;
				} else {
					return Status.CANCEL_STATUS;
				}
			} finally {
				cachingFailed = !cachingComplete;
			}
		} else {
			return Status.CANCEL_STATUS;
		}
	}

	public void cacheInBackground() {
		if (!cachingFailed && !cachingComplete) {
			try {
				Job j = new BackgroundJob("caching remote file") {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						try {
							return cache(monitor);
						} catch (Exception e) {
							return Status.CANCEL_STATUS;
						}
					}
				};
				j.schedule();
				Display d = PlatformUI.getWorkbench().getDisplay();
				boolean isMainThread = Thread.currentThread().equals(
						d.getThread());
				while (!cachingFailed && !cachingComplete) {
					boolean sleep = isMainThread ? !d.readAndDispatch() : true;
					if (sleep) {
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
						}
					}
				}

			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.ERROR,
						"Unable to cache remote file", e);
			}
		}
	}

	public void clearHistory(IProgressMonitor monitor) throws CoreException {
		localFile.clearHistory(monitor);
	}

	public boolean contains(ISchedulingRule rule) {
		return localFile.contains(rule);
	}

	public void copy(IPath destination, boolean force, IProgressMonitor monitor)
			throws CoreException {
		localFile.copy(destination, force, monitor);
	}

	public void copy(IPath destination, int updateFlags,
			IProgressMonitor monitor) throws CoreException {
		localFile.copy(destination, updateFlags, monitor);
	}

	public void copy(IProjectDescription description, boolean force,
			IProgressMonitor monitor) throws CoreException {
		localFile.copy(description, force, monitor);
	}

	public void copy(IProjectDescription description, int updateFlags,
			IProgressMonitor monitor) throws CoreException {
		localFile.copy(description, updateFlags, monitor);
	}

	public void create(InputStream source, boolean force,
			IProgressMonitor monitor) throws CoreException {
		localFile.create(source, force, monitor);
	}

	public void create(InputStream source, int updateFlags,
			IProgressMonitor monitor) throws CoreException {
		localFile.create(source, updateFlags, monitor);
	}

	public void createLink(IPath localLocation, int updateFlags,
			IProgressMonitor monitor) throws CoreException {
		localFile.createLink(localLocation, updateFlags, monitor);
	}

	public void createLink(URI location, int updateFlags,
			IProgressMonitor monitor) throws CoreException {
		localFile.createLink(location, updateFlags, monitor);
	}

	public IMarker createMarker(String type) throws CoreException {
		return localFile.createMarker(type);
	}

	public IResourceProxy createProxy() {
		return localFile.createProxy();
	}

	public void delete(boolean force, boolean keepHistory,
			IProgressMonitor monitor) throws CoreException {
		localFile.delete(force, keepHistory, monitor);
	}

	public void delete(boolean force, IProgressMonitor monitor)
			throws CoreException {
		localFile.delete(force, monitor);
	}

	public void delete(int updateFlags, IProgressMonitor monitor)
			throws CoreException {
		localFile.delete(updateFlags, monitor);
	}

	public void deleteMarkers(String type, boolean includeSubtypes, int depth)
			throws CoreException {
		localFile.deleteMarkers(type, includeSubtypes, depth);
	}

	public void dispose() {
		if (syncLocalToRemote) {
			ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
		}
	}

	@Override
	public boolean equals(Object target) {
		if (this == target) {
			return true;
		}
		if (!(target instanceof IResource)) {
			return false;
		}
		IResource resource = (IResource) target;
		return getType() == resource.getType()
				&& getFullPath().equals(resource.getFullPath())
				&& getWorkspace().equals(resource.getWorkspace());
	}

	public boolean exists() {
		return true;
	}

	public IMarker findMarker(long id) throws CoreException {
		return localFile.findMarker(id);
	}

	public IMarker[] findMarkers(String type, boolean includeSubtypes, int depth)
			throws CoreException {
		return localFile.findMarkers(type, includeSubtypes, depth);
	}

	public int findMaxProblemSeverity(String type, boolean includeSubtypes,
			int depth) throws CoreException {
		return localFile.findMaxProblemSeverity(type, includeSubtypes, depth);
	}

	public Object getAdapter(Class adapter) {
		return localFile.getAdapter(adapter);
	}

	public String getCharset() throws CoreException {
		return localFile.getCharset();
	}

	public String getCharset(boolean checkImplicit) throws CoreException {
		return localFile.getCharset(checkImplicit);
	}

	public String getCharsetFor(Reader reader) throws CoreException {
		return localFile.getCharsetFor(reader);
	}

	public IContentDescription getContentDescription() throws CoreException {
		return localFile.getContentDescription();
	}

	public InputStream getContents() throws CoreException {
		return getContents(false);
	}

	public InputStream getContents(boolean force) throws CoreException {
		if (!cachingFailed && !cachingComplete) {
			cacheInBackground();
		}
		return localFile.getContents(force);
	}

	public int getEncoding() throws CoreException {
		return localFile.getEncoding();
	}

	public String getFileExtension() {
		return localFile.getFileExtension();
	}

	public IPath getFullPath() {
		return localFile.getFullPath();
	}

	public IFileState[] getHistory(IProgressMonitor monitor)
			throws CoreException {
		return localFile.getHistory(monitor);
	}

	public long getLocalTimeStamp() {
		return localFile.getLocalTimeStamp();
	}

	public IPath getLocation() {
		return localFile.getLocation();
	}

	public URI getLocationURI() {
		return localFile.getLocationURI();
	}

	public IMarker getMarker(long id) {
		return localFile.getMarker(id);
	}

	public long getModificationStamp() {
		return localFile.getModificationStamp();
	}

	public String getName() {
		return localFile.getName();
	}

	public IContainer getParent() {
		return localFile.getParent();
	}

	public IPathVariableManager getPathVariableManager() {
		return localFile.getPathVariableManager();
	}

	// new methods in Eclipse 3.5
	public Map getPersistentProperties() throws CoreException {
		return localFile.getPersistentProperties();
	}

	public String getPersistentProperty(QualifiedName key) throws CoreException {
		return localFile.getPersistentProperty(key);
	}

	public IProject getProject() {
		return localFile.getProject();
	}

	public IPath getProjectRelativePath() {
		return localFile.getProjectRelativePath();
	}

	public IPath getRawLocation() {
		return localFile.getRawLocation();
	}

	public URI getRawLocationURI() {
		return localFile.getRawLocationURI();
	}

	public ResourceAttributes getResourceAttributes() {
		return localFile.getResourceAttributes();
	}

	public Map getSessionProperties() throws CoreException {
		return localFile.getSessionProperties();
	}

	public Object getSessionProperty(QualifiedName key) throws CoreException {
		return localFile.getSessionProperty(key);
	}

	public int getType() {
		return FILE;
	}
	
	@Override
	public int hashCode() {
		int result = 42;
		
		result = 37 * result + getType();
		result = 37 * result + ((getFullPath() != null) ? getFullPath().hashCode() : 0);
		result = 37 * result + ((getWorkspace() != null) ? getWorkspace().hashCode() : 0);
		
		return result;
	}

	public IWorkspace getWorkspace() {
		return localFile.getWorkspace();
	}

	public boolean isAccessible() {
		return localFile.isAccessible();
	}

	public boolean isConflicting(ISchedulingRule rule) {
		return localFile.isConflicting(rule);
	}

	public boolean isDerived() {
		return localFile.isDerived();
	}

	public boolean isDerived(int options) {
		return localFile.isDerived(options);
	}

	public boolean isHidden() {
		return localFile.isHidden();
	}

	public boolean isHidden(int options) {
		return localFile.isHidden(options);
	}

	public boolean isLinked() {
		return localFile.isLinked();
	}

	public boolean isLinked(int options) {
		return localFile.isLinked(options);
	}

	public boolean isLocal(int depth) {
		return localFile.isLocal(depth);
	}

	public boolean isPhantom() {
		return localFile.isPhantom();
	}

	public boolean isReadOnly() {
		return localFile.isReadOnly();
	}

	public boolean isSynchronized(int depth) {
		return localFile.isSynchronized(depth);
	}

	public boolean isTeamPrivateMember() {
		return localFile.isTeamPrivateMember();
	}

	public boolean isTeamPrivateMember(int options) {
		return localFile.isTeamPrivateMember(options);
	}

	public boolean isVirtual() {
		return localFile.isVirtual();
	}

	public void move(IPath destination, boolean force, boolean keepHistory,
			IProgressMonitor monitor) throws CoreException {
		localFile.move(destination, force, keepHistory, monitor);
	}

	public void move(IPath destination, boolean force, IProgressMonitor monitor)
			throws CoreException {
		localFile.move(destination, force, monitor);
	}

	public void move(IPath destination, int updateFlags,
			IProgressMonitor monitor) throws CoreException {
		localFile.move(destination, updateFlags, monitor);
	}

	public void move(IProjectDescription description, boolean force,
			boolean keepHistory, IProgressMonitor monitor) throws CoreException {
		localFile.move(description, force, keepHistory, monitor);
	}

	public void move(IProjectDescription description, int updateFlags,
			IProgressMonitor monitor) throws CoreException {
		localFile.move(description, updateFlags, monitor);
	}

	public void refreshLocal(int depth, IProgressMonitor monitor)
			throws CoreException {
		localFile.refreshLocal(depth, monitor);
	}

	public void resourceChanged(IResourceChangeEvent event) {
		IResourceDelta delta = null;
		if (event.getDelta() != null) {
			delta = event.getDelta().findMember(localFile.getFullPath());
		}
		if (delta == null) {
			return;
		}
		if (delta.getKind() == IResourceDelta.CHANGED) {
			// the file was written to somehow
			Job remoteFileUpdater = new BackgroundJob("updating remote file") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try {
						remoteFile.setContents(localFile.getContents(),
								localFile.getLocation().toFile().length(),
								true, monitor);
					} catch (CoreException e) {
						ServiceBrowserActivator.log(IStatus.ERROR, "Unable to update remote file", e);
					}
					return Status.OK_STATUS;
				}
			};
			remoteFileUpdater.schedule();
		} else if ((delta.getKind() & IResourceDelta.REMOVED) != 0) {
			dispose();
		}
	}

	public void revertModificationStamp(long value) throws CoreException {
		localFile.revertModificationStamp(value);
	}

	public void setCharset(String newCharset) throws CoreException {
		localFile.setCharset(newCharset);
	}

	public void setCharset(String newCharset, IProgressMonitor monitor)
			throws CoreException {
		localFile.setCharset(newCharset, monitor);
	}

	public void setContents(IFileState source, boolean force,
			boolean keepHistory, IProgressMonitor monitor) throws CoreException {
		setContents(source.getContents(), IResource.FORCE, monitor);
	}

	public void setContents(IFileState source, int updateFlags,
			IProgressMonitor monitor) throws CoreException {
		setContents(source.getContents(), updateFlags, monitor);
	}

	public void setContents(InputStream source, boolean force,
			boolean keepHistory, IProgressMonitor monitor) throws CoreException {
		int updateFlags = force ? IResource.FORCE : IResource.NONE;
		updateFlags |= keepHistory ? IResource.KEEP_HISTORY : IResource.NONE;
		setContents(source, updateFlags, monitor);
	}

	public void setContents(InputStream source, int updateFlags,
			IProgressMonitor monitor) throws CoreException {
		monitor.beginTask("writing to file", 100);
		try {
			// write to local copy
			SubProgressMonitor subProgress = new SubProgressMonitor(monitor, 10);
			localFile.setContents(source, updateFlags, subProgress);
			// now write to remote location
			subProgress = new SubProgressMonitor(monitor, 90);
			boolean force = (updateFlags & IResource.FORCE) == IResource.FORCE;
			remoteFile.setContents(source, localFile.getLocation().toFile()
					.length(), force, subProgress);
		} finally {
			monitor.done();
		}
	}

	public void setDerived(boolean isDerived) throws CoreException {
		localFile.setDerived(isDerived);
	}

	public void setDerived(boolean isDerived, IProgressMonitor monitor)
			throws CoreException {
		localFile.setDerived(isDerived, monitor);

	}

	public void setHidden(boolean isHidden) throws CoreException {
		localFile.setHidden(isHidden);
	}

	public void setLocal(boolean flag, int depth, IProgressMonitor monitor)
			throws CoreException {
		localFile.setLocal(flag, depth, monitor);
	}

	public long setLocalTimeStamp(long value) throws CoreException {
		return localFile.setLocalTimeStamp(value);
	}

	public void setPersistentProperty(QualifiedName key, String value)
			throws CoreException {
		localFile.setPersistentProperty(key, value);
	}

	@Deprecated
	public void setReadOnly(boolean readOnly) {
		localFile.setReadOnly(readOnly);
	}

	public void setResourceAttributes(ResourceAttributes attributes)
			throws CoreException {
		localFile.setResourceAttributes(attributes);
	}

	public void setSessionProperty(QualifiedName key, Object value)
			throws CoreException {
		localFile.setSessionProperty(key, value);
	}

	public void setTeamPrivateMember(boolean isTeamPrivate)
			throws CoreException {
		localFile.setTeamPrivateMember(isTeamPrivate);
	}

	public void touch(IProgressMonitor monitor) throws CoreException {
		localFile.touch(monitor);
	}

}
