/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.w3.x2005.x08.addressing.EndpointReferenceType;

/**
 * @author demuth
 * 
 */
public interface INodeData extends Serializable {

	public static String PROPERTY_DATA_CHILDREN = "data children",
			PROPERTY_DATA_PARENT = "data parent";

	/**
	 * Add INodeData as child. Set this INodeData as parent of the child. Do not
	 * add child if it already exists in children list! Fire property change to
	 * inform listeners.
	 * 
	 * @param child
	 */
	public void addChild(INodeData child);

	/**
	 * Add INodeData as child. Set this INodeData as parent of the child. Do not
	 * add child if it already exists in children list! Fire property change to
	 * inform listeners.
	 * 
	 * @param child
	 */
	public void addChild(INodeData child, int index);

	public void addParent(INodeData parent);

	public void addPropertyChangeListener(PropertyChangeListener l);

	/**
	 * perform some cleanup when node data is not needed anymore
	 */
	public void dispose();

	public List<String> getAllNamePathsToRoot();

	public Set<String> getAllNames();

	public List<List<INodeData>> getAllPathsToRoot();

	public List<INodeData> getChildren();

	public INodeData[] getChildrenArray();

	public Map<URI, INodeData> getChildrenMap();

	public EndpointReferenceType getEpr();
	
	public void setEpr(EndpointReferenceType epr);

	public List<INodeData> getParents();

	public Object getProperty(String key);

	public RegistryNode getRegistryNode();

	public URI getURI();

	public boolean hasChildren();

	public boolean isKnownChild(INodeData so);

	public void removeAllChildren();

	/**
	 * Remove INodeData from list of children. Set null as parent of the child.
	 * Fire property change to inform listeners.
	 * 
	 * @param child
	 */
	public void removeChild(INodeData child);

	public void removeParent(INodeData parent);

	public void removePropertyChangeListener(PropertyChangeListener l);

	/**
	 * Deprecated
	 * 
	 * @deprecated Use {@link #setChildren(List, Object)} instead and provide
	 *             yourself as a source. This way, nodes can avoid reacting to
	 *             their own changes.
	 * @param children
	 *            the children to set
	 */
	@Deprecated
	public void setChildren(List<INodeData> children);

	public void setChildren(List<INodeData> children, Object source);

	/**
	 * Deprecated
	 * 
	 * @deprecated Please use {@link #setProperty(String, Object, Object)} and
	 *             provide yourself as source for the property change. This
	 *             allows for a safe way to ignore your own changes when
	 *             listening to the node data.
	 */
	@Deprecated
	public void setProperty(String key, Object value);

	public void setProperty(String key, Object value, Object source);

	public void setRegistryNode(RegistryNode registryNode);

}
