/*********************************************************************************
 * Copyright (c) 2015 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.awt.Dialog;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.wb.swt.SWTResourceManager;

import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;

/**
 * @author bjoernh
 *
 * 19.10.2015 12:37:46
 *
 */
public class EditRegistryUrlDialog extends MessageDialog {

	protected Object result;
	protected Shell shell;
	private Text fbUrl;
	private Text url;
	private Label lblError;
	
	private String urlString = "";
	private String fbUrlString = "";

	/**
	 * Create the dialog.
	 * @param registry 
	 * @param parent
	 * @param style
	 */
	public EditRegistryUrlDialog(Shell parent, int style) {
		super(parent, "Edit Registry URL", null, "Edit Registry URL", MessageDialog.NONE,
				new String[] { IDialogConstants.OK_LABEL, IDialogConstants.CANCEL_LABEL }, 0);
	}
	
	/**
	 * @see org.eclipse.jface.dialogs.MessageDialog#createCustomArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createCustomArea(Composite parent) {
		parent.setLayout(new GridLayout(3, false));
		
		Label lblUrl = new Label(parent, SWT.NONE);
		lblUrl.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblUrl.setText("Please enter a new address:");
		
		url = new Text(parent, SWT.BORDER);
		url.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent _e) {
				if(url.getText() == null || url.getText().trim().isEmpty()) {
					setErrorMessage("URL must not be empty.");
					return;
				}
				try {
					new URI(url.getText());
				} catch (URISyntaxException e) {
					setErrorMessage(e.getMessage());
					return;
				}
				setErrorMessage("");
			}
		});
		url.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(parent, SWT.NONE);
		
		Label lblFallback = new Label(parent, SWT.NONE);
		lblFallback.setText("Enter new fallback address:");
		
		fbUrl = new Text(parent, SWT.BORDER);
		fbUrl.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent _e) {
				try {
					new URI(fbUrl.getText());
				} catch (URISyntaxException e) {
					setErrorMessage(e.getMessage());
					return;
				}
				setErrorMessage("");
			}
		});
		fbUrl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(parent, SWT.NONE);
		
		lblError = new Label(parent, SWT.WRAP);
		lblError.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblError.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));

		
		url.setText(urlString);
		fbUrl.setText(fbUrlString);
		
		return super.createCustomArea(parent);
	}
	
	/**
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		this.urlString = url.getText();
		this.fbUrlString = fbUrl.getText();
	}
	
	protected void buttonPressed(int buttonId) {
		if (MessageDialog.OK == buttonId) {
			okPressed();
		}
		super.buttonPressed(buttonId);
	}
	
	public String getFbUrl() {
		return fbUrlString;
	}
	
	public void setFbUrl(String _fbUrl) {
		fbUrlString = _fbUrl;
	}
	
	public String getUrl() {
		return urlString;
	}
	
	public void setUrl(String _url) {
		urlString = _url;
	}
	
	private void setErrorMessage(String _msg) {
		lblError.setText(_msg);
		lblError.pack();
		if(getButton(0) != null) {
			getButton(0).setEnabled(_msg.isEmpty());
		}
	}
}
