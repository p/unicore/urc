package de.fzj.unicore.rcp.servicebrowser.nodes;

/**
 * Instances can be registered at the {@link NodeFactory}'s
 * {@link INodeDataCache} in order to be notified whenever new services are
 * found which leads to the creation of additional node data objects. Similar
 * notifications are sent whenever services/nodes are obsolete. Remember to
 * unregister yourself once you are disposed of or loose interest in these
 * events!
 * CAUTION: Implementations MUST NOT use create nodes from the node data objects
 * e.g. by calling {@link NodeFactory#createNode(INodeData)}, as this might
 * re-add a node data object to the cache that has just been removed. All
 * neccessary info must be retrieved from the node data properties..
 * 
 * @author bdemuth
 * 
 */
public interface INodeCacheListener {

	/**
	 * This gets called when a new node was found (i.e. a node is created and no
	 * node sharing the same URI existed before) leading to the creation of a
	 * new {@link INodeData} object.
	 * 
	 * @param n
	 *            The node that has just been created.
	 */
	public void nodeDataAdded(INodeData nodeData);

	/**
	 * This gets called when a node gets disposed and no other node with the
	 * same URI exists afterwards.
	 * 
	 * @param n
	 */
	public void nodeDataRemoved(INodeData nodeData);

}
