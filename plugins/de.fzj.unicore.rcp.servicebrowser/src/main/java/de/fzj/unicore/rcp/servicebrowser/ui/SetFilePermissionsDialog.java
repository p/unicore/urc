package de.fzj.unicore.rcp.servicebrowser.ui;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.unigrids.services.atomic.types.PermissionsType;

import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;

public class SetFilePermissionsDialog extends Dialog {

	protected AbstractFileNode fileNode;
	protected Button readButton, writeButton, executeButton;
	protected boolean read, write, execute;

	public SetFilePermissionsDialog(Shell parentShell, AbstractFileNode fileNode) {
		super(parentShell);
		this.fileNode = fileNode;

	}

	@Override
	public boolean close() {
		read = readButton.getSelection();
		write = writeButton.getSelection();
		execute = executeButton.getSelection();
		return super.close();
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(3, false));
		Label label = new Label(composite, SWT.NONE);
		label.setText("The remote file should be:");
		label.setFont(parent.getFont());
		GridData gd = new GridData();
		gd.horizontalSpan = 3;
		label.setLayoutData(gd);

		PermissionsType permissions = fileNode.getGridFileType()
				.getPermissions();
		readButton = new Button(composite, SWT.CHECK);
		readButton.setText("readable");
		readButton.setLayoutData(new GridData());
		readButton.setSelection(permissions.getReadable());

		writeButton = new Button(composite, SWT.CHECK);
		writeButton.setText("writable");
		writeButton.setLayoutData(new GridData());
		writeButton.setSelection(permissions.getWritable());

		executeButton = new Button(composite, SWT.CHECK);
		executeButton.setText("executable");
		executeButton.setLayoutData(new GridData());
		executeButton.setSelection(permissions.getExecutable());
		return composite;
	}

	public boolean isExecute() {
		return execute;
	}

	public boolean isRead() {
		return read;
	}

	public boolean isWrite() {
		return write;
	}
}
