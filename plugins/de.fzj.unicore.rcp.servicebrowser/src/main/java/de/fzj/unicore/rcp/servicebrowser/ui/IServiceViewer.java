package de.fzj.unicore.rcp.servicebrowser.ui;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.widgets.Control;

import de.fzj.unicore.rcp.servicebrowser.IServiceContentProvider;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

public interface IServiceViewer extends ISelectionProvider {

	
	/**
	 * Adds the given filter to this viewer, and triggers refiltering and
	 * resorting of the elements.
	 * 
	 * @param filter
	 *            a viewer filter
	 */
	public void addFilter(ViewerFilter filter);

	/**
	 * add an entry to the top entries of the view. These entries serve as
	 * starting points for the grid and are being persisted.
	 * 
	 * @param node
	 */
	public void addMonitoringEntry(Node node);

	public void dispose();

	public void expandToLevel(Object elementOrTreePath, int level);

	/**
	 * Notify listeners of selection changes.
	 * 
	 */
	public void fireSelectionChanged();

	/**
	 * Get the graphical element representing the view.
	 * 
	 * @return
	 */
	public Control getControl();

	/**
	 * Returns this viewer's filters.
	 * 
	 * @return an array of viewer filters
	 */
	public ViewerFilter[] getFilters();

	/**
	 * Return the root node of the Grid displayed by this viewer
	 * 
	 * @return
	 */
	public GridNode getGridNode();

	/**
	 * get the selected entries (nodes) within the view.
	 * 
	 * @return
	 */
	public ISelection getSelection();

	/**
	 * Retrieve the content provider that serves as a control layer between the
	 * model (the input) and the view
	 * 
	 * @param contentProvider
	 */
	public IServiceContentProvider getServiceContentProvider();

	/**
	 * load any persisted settings specific to the view.
	 * 
	 */
	public void loadSettings();

	/**
	 * Update the view to reflect changes on the model
	 * 
	 */
	public void refresh();

	public void refresh(boolean updateLabels);

	public void refresh(Object element);

	public void refresh(Object element, boolean updateLabels);

	/**
	 * Removes the given filter from this viewer, and triggers refiltering and
	 * resorting of the elements if required. Has no effect if the identical
	 * filter is not registered.
	 * 
	 * @param filter
	 *            a viewer filter
	 */
	public void removeFilter(ViewerFilter filter);

	/**
	 * remove an entry from the top entries of the view. These entries serve as
	 * starting points for the grid and are being persisted. Caution: the node
	 * will be disposed during removal, cause it's disconnected from its parent.
	 * 
	 * @param node
	 */
	public void removeMonitoringEntry(Node node);

	/**
	 * Discards this viewer's filters and triggers refiltering and resorting of
	 * the elements.
	 */
	public void resetFilters();

	public void saveGrid();

	/**
	 * Method for setting the data model of the viewer by setting the root node
	 * of the Grid.
	 * 
	 * @param gridNode
	 */
	public void setGridNode(GridNode gridNode);

	public void setSelection(ISelection selection, boolean selectionVisible);

	/**
	 * Set the content provider that serves as a control layer between the model
	 * (the input) and the view
	 * 
	 * @param contentProvider
	 */
	public void setServiceContentProvider(
			IServiceContentProvider contentProvider);

}
