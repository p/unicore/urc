package de.fzj.unicore.rcp.servicebrowser.dnd;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.util.URIUtil;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.unigrids.services.atomic.types.GridFileType;

import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IDropAssistant;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.rcp.servicebrowser.utils.RemoteFileUtils;
import de.fzj.unicore.uas.client.StorageClient;

public class FileDropAssistant implements IDropAssistant {

	private static final Transfer[] SUPPORTED_DROP_TRANSFERS = new Transfer[] {
		LocalSelectionTransfer.getTransfer(), FileTransfer.getInstance() };

	public Transfer[] getSupportedTransferTypes() {
		return SUPPORTED_DROP_TRANSFERS;
	}

	public int performDrop(final Object target, final int operation,
			final TransferData transferType, final Object data) {
		return performDrop(target,operation,transferType,data,null);
	}
	
	public int performDrop(final Object target, final int operation,
			final TransferData transferType, final Object data, final String protocol) {
		String description = "";
		if (operation == DND.DROP_MOVE) {
			description = "Moving file.";
		} else if (operation == DND.DROP_COPY) {
			description = "Copying file(s).";
		}
		Job j = new BackgroundJob(description) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					StorageClient storageClient = null;
					String parentPath = "";
					Node node = (Node) target;
					if (target instanceof StorageNode) {
						storageClient = ((StorageNode) target)
						.getStorageClient();
					} else if (target instanceof FolderNode) {
						FolderNode folderNode = (FolderNode) target;
						storageClient = folderNode.getStorageClient();
						GridFileType gft = folderNode.getGridFileType();
						if (gft == null) {
							return Status.CANCEL_STATUS;
						}
						parentPath = gft.getPath() + "/";
					}

					if (FileTransfer.getInstance()
							.isSupportedType(transferType)) {
						String[] selected = (String[]) data;
						Map<File, Long> fileSizes = new HashMap<File, Long>();
						long total = 0;
						for (String s : selected) {
							java.io.File f = new java.io.File(s);
							if (f.isDirectory()) {
								total += UnicoreStorageTools.collectFileSizes(
										f, fileSizes, true, null);
							} else {
								total += f.length();
								fileSizes.put(f, f.length());
							}
						}

						monitor.beginTask("Uploading file(s)",
								UnicoreStorageTools.calculateEffort(total));
						for (String s : selected) {
							if (monitor.isCanceled()) {
								break;
							}

							java.io.File f = new java.io.File(s);
							int work = UnicoreStorageTools.calculateEffort(
									fileSizes.get(f), total);
							SubProgressMonitor sub = new SubProgressMonitor(
									monitor, work);
							if (f.isDirectory()) {
								UnicoreStorageTools.uploadFolder(storageClient,
										f, parentPath + f.getName(),UnicoreStorageTools.RENAME,protocol, sub);
							} else {
								UnicoreStorageTools.uploadFile(storageClient,
										f, parentPath + f.getName(), UnicoreStorageTools.RENAME, protocol, sub);
							}

						}
					}

					else if (NodeLocalSelectionTransfer.getTransfer()
							.isSupportedType(transferType)) {
						IStructuredSelection selection = (IStructuredSelection) data;
						Object[] toPaste = selection.toArray();
						boolean allFiles = true;
						final AbstractFileNode[] nodes = new AbstractFileNode[toPaste.length];
						for (int i = 0; i < toPaste.length; i++) {
							Object object = toPaste[i];
							if (object instanceof AbstractFileNode) {
								nodes[i] = (AbstractFileNode) object;
							} else {
								allFiles = false;
								break;
							}
						}

						if (allFiles) {
							boolean move = operation == DND.DROP_MOVE;
							RemoteFileUtils.serverToServerFileTransfer(nodes,
									node, move, UnicoreStorageTools.RENAME, protocol, monitor);

							if (move) {
								// delete files at origin
								for (AbstractFileNode n : nodes) {
									GridFileType gft = n.getGridFileType();
									if (gft == null) {
										return Status.CANCEL_STATUS;
									}
									String path = gft.getPath();
									// are source and target located on the same
									// storage?
									boolean onMyStorage = n
									.getFileAddressWithoutProtocol()
									.toString()
									.toLowerCase()
									.startsWith(
											storageClient.getUrl()
											.toLowerCase()); 
									if (!onMyStorage) // if the file was located
										// on my storage, the
										// serverToServerFileTransfer
										// method has already
										// moved it
									{
										try {
											StorageClient client = n
											.getStorageClient();
											// escape special characters in
											// filenames!
											path = URIUtil
											.encode(path,
													org.apache.commons.httpclient.URI.allowed_fragment); 
											
											client.delete(path);
										} catch (Exception e) {
											ServiceBrowserActivator.log(IStatus.ERROR, "Unable to delete file", e);
										}
										try {
											n.getParent().refresh(1, false,
													null);
										} catch (Exception e) {
											ServiceBrowserActivator.log(IStatus.WARNING,
													"Unable to refresh file's parent", e);
										}
									}

								}
							}
						}

					} else if (LocalSelectionTransfer.getTransfer()
							.isSupportedType(transferType)) {
						IStructuredSelection selection = (IStructuredSelection) data;
						Object[] selected = selection.toArray();
						monitor.beginTask("Uploading file(s)", selected.length);
						for (Object o : selected) {
							if (monitor.isCanceled()) {
								break;
							}
							if (o instanceof IResource) {
								IResource r = (IResource) o;
								java.io.File f = new java.io.File(r
										.getLocation().toOSString());
								SubProgressMonitor sub = new SubProgressMonitor(
										monitor, 1);
								if (f.isDirectory()) {
									UnicoreStorageTools.uploadFolder(
											storageClient, f,
											parentPath + f.getName(), UnicoreStorageTools.RENAME,protocol, sub);
								} else {
									UnicoreStorageTools.uploadFile(
											storageClient, f,
											parentPath + f.getName(), UnicoreStorageTools.RENAME,protocol, sub);
								}
							}
						}
					}
					if(!monitor.isCanceled()) node.refresh(1, true, null);
				} catch (Exception e) {
					ServiceBrowserActivator.log(
							IStatus.ERROR,
							"Unable to copy/move files to UNICORE storage: "
							+ e.getMessage(), e);
				} finally {
					monitor.done();
				}
				return Status.OK_STATUS;
			}
		};
		j.setUser(true);
		j.schedule();
		return DND.DROP_COPY;
	}

	public boolean supportsTransferData(TransferData td) {
		for (Transfer t : getSupportedTransferTypes()) {
			if (t.isSupportedType(td)) {
				return true;
			}
		}
		return false;
	}

	public int validateDrop(Object target, int operation,
			TransferData[] transferTypes) {
		return DND.DROP_COPY;
	}

}
