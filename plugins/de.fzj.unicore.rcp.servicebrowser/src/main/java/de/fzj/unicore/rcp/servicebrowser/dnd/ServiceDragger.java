package de.fzj.unicore.rcp.servicebrowser.dnd;

import java.util.Iterator;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;

import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IDragAssistant;

public class ServiceDragger extends DragSourceAdapter {

	protected ISelectionProvider _selectionProvider;
	protected ISelection _selection = null; // set this on dragStart, set to
											// null on dragFinished
	protected IDragAssistant matchingAssistant = null;
	protected DragSourceEvent performedEvent = null;

	public ServiceDragger(ISelectionProvider provider) {
		_selectionProvider = provider;
	}

	@Override
	public void dragFinished(DragSourceEvent event) {

		if (matchingAssistant != null && performedEvent != null) {
			IStructuredSelection ss = (IStructuredSelection) _selection;
			Object[] sources = ss.toArray();
			try {
				matchingAssistant.dragFinished(performedEvent, sources);
			} finally {
				matchingAssistant = null;
				performedEvent = null;
			}
		}
		_selection = null; // drag has finished, forget the selection
		if (event.doit == false) {
			return;
		}
	}

	/**
	 * Method for determining the source (drag) object(s) and encoding those
	 * objects in a byte[]. We encode the profile, connection and subsystem, and
	 * then we use ISystemDragDropAdapter.getAbsoluteName() to determine the ID
	 * for the object within it's subsystem.
	 */
	@Override
	public void dragSetData(final DragSourceEvent event) {
		/*
		 * We cannot request the selection from the selection provider at this
		 * point since on some platforms (particularly Mac OS X) the selection
		 * is forgotten by the underlying OS control immediately after the drag
		 * is started. This call is invoked at the end of the drag operation but
		 * just before the corresponding drop call in the drop adapter. Thus, we
		 * must remember the selection at drag start.
		 */
		if (_selection instanceof IStructuredSelection) {
			IStructuredSelection ss = (IStructuredSelection) _selection;
			Object[] sources = ss.toArray();
			IDragAssistant[] assistants = DragAssistantManager.getInstance()
					.findDragAssistants(sources);
			if (assistants.length == 0) {
				return;
			}
			boolean dataSet = false;
			int i = 0;
			do {
				dataSet = assistants[i].dragSetData(event, sources);
				if (dataSet) {
					matchingAssistant = assistants[i];
					performedEvent = event; // remember the event that is
											// performed.. this is needed for
											// cleaning up later
				}
				i++;
			} while (!dataSet && i < assistants.length);
		}

	}

	@Override
	public void dragStart(DragSourceEvent event) {
		/*
		 * Remember the selection at drag start. This is the only point at which
		 * the selection is valid during the drag operations on all platforms.
		 */
		_selection = _selectionProvider.getSelection();
		if (_selection instanceof IStructuredSelection) {
			IStructuredSelection ss = (IStructuredSelection) _selection;
			Iterator iterator = ss.iterator();
			while (iterator.hasNext()) {
				Object dragObject = iterator.next();
				TransferData[] td = DragAssistantManager.getInstance()
						.getSupportedTransferData(dragObject);
				if (td.length == 0) {
					event.doit = false;
					event.detail = DND.DROP_NONE;
					return;
				}

			}
		}
		event.doit = true;
	}

	public Transfer[] getSupportedTransferTypes() {
		return DragAssistantManager.getInstance().getSupportedTransferTypes();
	}
}
