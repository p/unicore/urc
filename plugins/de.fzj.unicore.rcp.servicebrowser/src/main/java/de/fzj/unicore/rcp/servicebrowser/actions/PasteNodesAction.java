package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.bindings.TriggerSequence;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.keys.IBindingService;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.dnd.NodeLocalSelectionContents;
import de.fzj.unicore.rcp.servicebrowser.dnd.NodeLocalSelectionTransfer;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

public class PasteNodesAction extends NodeAction {
	private ISelectionProvider selectionProvider;
	private Clipboard clipboard;

	public PasteNodesAction(ISelectionProvider selectionProvider,
			Clipboard clipboard) {
		super(null);
		this.selectionProvider = selectionProvider;
		this.clipboard = clipboard;
		init();
	}

	public PasteNodesAction(Node node) {
		super(node);
		init();
	}

	protected void init() {
		String text = "Paste";
		try {
			IBindingService bindingService = (IBindingService) PlatformUI
					.getWorkbench().getActiveWorkbenchWindow()
					.getService(IBindingService.class);
			String keyBindingText = null;

			TriggerSequence binding = bindingService
					.getBestActiveBindingFor(IWorkbenchCommandConstants.EDIT_PASTE);
			if (binding != null) {
				keyBindingText = binding.format();
			}

			if (keyBindingText != null) {
				text += '\t' + keyBindingText;
			}

		} catch (Exception e) {
			// do nothing
		}

		setText(text);
		setToolTipText("Paste elements from system clipboard");

		ISharedImages sharedImages = PlatformUI.getWorkbench()
				.getSharedImages();
		setImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_PASTE));
		setDisabledImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_PASTE_DISABLED));
	}

	@Override
	@SuppressWarnings("unchecked")
	public void run() {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {

			public void run() {
				String ERROR_MSG = "Unable to paste clipboard content into selected node(s).";
				if (clipboard == null) {
					clipboard = new Clipboard(getViewer().getControl()
							.getDisplay());
				}
				IStructuredSelection selection = (IStructuredSelection) selectionProvider
						.getSelection();
				Node[] parents = (Node[]) selection.toList().toArray(
						new Node[selection.size()]);
				if (parents.length != 1) {
					ServiceBrowserActivator.log(IStatus.ERROR, ERROR_MSG);
					return;
				}
				Node parent = parents[0];

				int operation = DND.DROP_COPY;
				Object toPaste = null;
				Object contents = clipboard
						.getContents(NodeLocalSelectionTransfer.getTransfer());
				TransferData transferData = null;
				if (contents != null) {
					operation = ((NodeLocalSelectionContents) contents)
							.getOperation();
					toPaste = ((NodeLocalSelectionContents) contents)
							.getSelection();
					transferData = NodeLocalSelectionTransfer.getTransfer()
							.getSupportedTypes()[0];
				} else {
					contents = clipboard.getContents(FileTransfer.getInstance());
					if (contents != null) {
						toPaste = contents;
						transferData = FileTransfer.getInstance()
								.getSupportedTypes()[0];
					} else {
						boolean supportedTransferData = false;
						for (TransferData td : clipboard.getAvailableTypes()) {
							if (NodeLocalSelectionTransfer.getTransfer()
									.isSupportedType(td)) {
								supportedTransferData = true;
								break;
							}
						}
						if (!supportedTransferData) {
							ServiceBrowserActivator.log(IStatus.ERROR,
									ERROR_MSG);
							return;
						}
						contents = clipboard.getContents(LocalSelectionTransfer
								.getTransfer());
						if (contents != null) {
							toPaste = ((LocalSelectionTransfer) contents)
									.getSelection();
							transferData = LocalSelectionTransfer.getTransfer()
									.getSupportedTypes()[0];
						}
					}
				}
				if (toPaste == null) {
					ServiceBrowserActivator.log(IStatus.ERROR, ERROR_MSG);
					return;
				}
				if (parent.canPaste(toPaste, transferData, operation)) {
					parent.paste(toPaste, transferData, operation);
				}

			}
		});

	}

	@Override
	public void setViewer(IServiceViewer viewer) {
		super.setViewer(viewer);
		this.selectionProvider = viewer;
	}

}
