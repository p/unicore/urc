/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.MoreFilesNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.RootNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.MoreNode;

/**
 * @author demuth
 * 
 */
public class ServiceContentProvider implements IServiceContentProvider {

	protected TreeViewer viewer;
	protected GridNode gridNode;
	protected PropertyChangeSupport propertyChangeSupport;

	protected Object syncLock = new Object();

	protected static final Object[] EMPTY_ARRAY = new Object[0];

	public ServiceContentProvider() {
		propertyChangeSupport = new PropertyChangeSupport(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.IServiceContentProvider#
	 * addPropertyChangeListener(java.beans.PropertyChangeListener)
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);

	}

	public void dispose() {
		recursiveRemoveMyselfAsListener(getGridNode());
	}

	public Object[] getChildren(Object parent) {
		if (parent instanceof Node) {
			Node node = (Node) parent;
			return node.getChildrenArray();
		}
		return EMPTY_ARRAY;
	}

	private void getChildrenRecursive(Object parent, List<Object> childrenSoFar) {
		Object[] children = getChildren(parent);
		for (Object child : children) {
			childrenSoFar.add(child);
			getChildrenRecursive(child, childrenSoFar);
		}
	}

	public Object[] getElements(Object parent) {
		return getChildren(parent);
	}

	protected GridNode getGridNode() {
		return gridNode;
	}

	public Object getParent(Object child) {
		if (child instanceof Node) {
			Node node = (Node) child;
			return node.getParent();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.IServiceContentProvider#getAllChildren
	 * (java.lang.Object)
	 */
	public Object[] getSubtreeElements(Object parent) {
		List<Object> children = new ArrayList<Object>();
		getChildrenRecursive(parent, children);
		return children.toArray();
	}

	protected TreeViewer getViewer() {
		return viewer;
	}

	public boolean hasChildren(Object parent) {
		if (parent instanceof Node) {
			Node n = (Node) parent;
			// Target System Factories always have children: if no Target System
			// exists, one will
			// be created when expanding it
			// MoreFilesNodes are also always expandable
			if (TargetSystemFactoryNode.TYPE.equals(n.getType())
					|| MoreFilesNode.TYPE.equals(n.getType())
					|| MoreNode.TYPE.equals(n.getType())) {
				return true;
			}
		}
		return getChildren(parent).length > 0;
	}

	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		viewer = (TreeViewer) v;
		if (oldInput != null) {
			recursiveRemoveMyselfAsListener((Node) oldInput);
		}
		if (newInput != null) {
			setGridNode((GridNode) ((RootNode) newInput).getChildrenArray()[0]);
			recursiveAddMyselfAsListener(getGridNode());

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Preferences.IPropertyChangeListener#propertyChange
	 * (org.eclipse.core.runtime.Preferences.PropertyChangeEvent)
	 */
	@SuppressWarnings("unchecked")
	public void propertyChange(PropertyChangeEvent event) {
		synchronized (syncLock) {
			Node node = (Node) event.getSource();
			if (Node.PROPERTY_CHILDREN.equals(event.getPropertyName())) {

				List<Node> oldChildren = (List<Node>) event.getOldValue();
				List<Node> newChildren = (List<Node>) event.getNewValue();
				for (Iterator<Node> iter = oldChildren.iterator(); iter
						.hasNext();) {

					recursiveRemoveMyselfAsListener(iter.next());
				}
				for (Iterator<Node> iter = newChildren.iterator(); iter
						.hasNext();) {
					Node n = iter.next();
					recursiveAddMyselfAsListener(n);
				}

				// at this point, we'd like to refresh the node (which should
				// refresh the whole subtree)
				// however, if the node is currently filtered out, this won't
				// have any effect
				// therefore we find the first parent of the node that is
				// visible and refresh this node
				Node parent = node;
				while (parent.getParent() != null
						& getViewer().testFindItem(parent) == null) {
					parent = parent.getParent();

				}
				getViewer().refresh(parent, true);

			} else {
				getViewer().refresh(node);
			}
		}

		// notify listeners
		propertyChangeSupport.firePropertyChange(event);
	}

	public void recursiveAddMyselfAsListener(Node node) {
		synchronized (syncLock) {
			node.addPropertyChangeListener(this);
			Node[] children = node.getChildrenArray();
			for (Node child : children) {
				recursiveAddMyselfAsListener(child);
			}
		}
	}

	public void recursiveRemoveMyselfAsListener(Node node) {
		synchronized (syncLock) {
			if (node == null) {
				return;
			}
			node.removePropertyChangeListener(this);
			Node[] children = node.getChildrenArray();
			for (Node child : children) {
				recursiveRemoveMyselfAsListener(child);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.IServiceContentProvider#
	 * removePropertyChangeListener(java.beans.PropertyChangeListener)
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);

	}

	protected void setGridNode(GridNode gridNode) {
		this.gridNode = gridNode;
	}

	protected void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}

}
