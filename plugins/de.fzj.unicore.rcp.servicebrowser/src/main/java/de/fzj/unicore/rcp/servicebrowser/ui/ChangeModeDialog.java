package de.fzj.unicore.rcp.servicebrowser.ui;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.unigrids.services.atomic.types.GridFileType;
import org.unigrids.x2006.x04.services.sms.PermissionsChangeModeType;
import org.unigrids.x2006.x04.services.sms.PermissionsClassType;

import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;


public class ChangeModeDialog extends Dialog {

	protected AbstractFileNode fileNode;
	protected Button readUserButton, writeUserButton, executeUserButton;
	protected Button readGroupButton, writeGroupButton, executeGroupButton;
	protected Button readOthersButton, writeOthersButton, executeOthersButton;
	protected Button recursiveButton;
	protected Combo operationCombo;
	
	protected String filePermissions, rwxUserPermissions, rwxGroupPermissions, rwxOthersPermissions;
	protected PermissionsChangeModeType.Enum userOperation, groupOperation, othersOperation;
	protected boolean recursive;

	public ChangeModeDialog(Shell parentShell, AbstractFileNode fileNode) {
		super(parentShell);
		this.fileNode = fileNode;
		this.userOperation = PermissionsChangeModeType.SET;
		this.groupOperation = PermissionsChangeModeType.SET;
		this.othersOperation = PermissionsChangeModeType.SET;
	}

	@Override
	public boolean close() {
		rwxUserPermissions = (readUserButton.getSelection() ? "r" : "-") +
							(writeUserButton.getSelection() ? "w" : "-") +
							(executeUserButton.getSelection() ? "x" : "-");
		rwxGroupPermissions = (readGroupButton.getSelection() ? "r" : "-") +
							(writeGroupButton.getSelection() ? "w" : "-") +
							(executeGroupButton.getSelection() ? "x" : "-");
		rwxOthersPermissions = (readOthersButton.getSelection() ? "r" : "-") +
							(writeOthersButton.getSelection() ? "w" : "-") +
							(executeOthersButton.getSelection() ? "x" : "-");
		if (recursive)
			recursive = recursiveButton.getSelection();
		// Checking if new value is set to recursive operation
		if (recursive)
		{
			switch (operationCombo.getSelectionIndex())
			{
			case 0:
				userOperation = groupOperation = othersOperation = PermissionsChangeModeType.ADD;
				break;
			case 1:
				userOperation = groupOperation = othersOperation = PermissionsChangeModeType.SUBTRACT;
				break;
			case 2:
				userOperation = groupOperation = othersOperation = PermissionsChangeModeType.SET;
				break;
			default:
				// should never happen
				break;
			}
			/*
			userOperation = determineChangeOperation(PermissionsClassType.USER, rwxUserPermissions);
			groupOperation = determineChangeOperation(PermissionsClassType.GROUP, rwxGroupPermissions);
			othersOperation = determineChangeOperation(PermissionsClassType.OTHER, rwxOthersPermissions);
			*/
		}
		return super.close();
	}
	
	/*
	protected PermissionsChangeModeType.Enum determineChangeOperation(PermissionsClassType.Enum type, String rwxPermissions)
	{
		int index = 0;
		if (type == PermissionsClassType.GROUP) index += 3;
		else if (type == PermissionsClassType.OTHER) index += 6;	
		
		boolean addOpp = false, removeOpp = false;
		for (int i = 0; i < 3; ++i)
		{
			if (filePermissions.charAt(index+i) != '-'  &&  rwxPermissions.charAt(i) == '-')
				removeOpp = true;
			if (filePermissions.charAt(index+i) == '-'  &&  rwxPermissions.charAt(i) != '-')
				addOpp = true;
		}
		if (addOpp  &&  !removeOpp)
			return PermissionsChangeModeType.ADD;
		else if (!addOpp  &&  removeOpp)
			return PermissionsChangeModeType.SUBTRACT;
		else if (addOpp  &&  removeOpp)
			return PermissionsChangeModeType.SET;
		else
			return null;
	}
	*/
	
	@Override
	protected Control createDialogArea(Composite parent) {

		recursive = (fileNode instanceof FolderNode);
		
		GridFileType gft = fileNode.getGridFileType();
		filePermissions = gft.getFilePermissions();
		String owingUser = gft.getOwner();
		String owingGroup = gft.getGroup();
		
		//PermissionsType permissions = fileNode.getGridFileType()
		//.getPermissions();
		
		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(1, true));

		Label infoLabel = new Label(composite, SWT.NONE);
		infoLabel.setText("Owned by: " + owingUser + "  (group: " + owingGroup + ")");
		infoLabel.setFont(parent.getFont());
		infoLabel.setLayoutData(new GridData());

		if (recursive)
		{
			Group recursiveParameters = new Group(composite, SWT.SHADOW_NONE);
			recursiveParameters.setFont(parent.getFont());
			recursiveParameters.setLayout(new GridLayout(3, true));
			
			recursiveButton = new Button(recursiveParameters, SWT.CHECK | SWT.CENTER);
			recursiveButton.setFont(parent.getFont());
			recursiveButton.setText("Change recursively");
			recursiveButton.setSelection(false);
			GridData gd3 = new GridData();
			gd3.horizontalSpan = 3;
			recursiveButton.setLayoutData(gd3);			

			final Label recursiveOperationLabel = new Label(recursiveParameters, SWT.RIGHT);
			recursiveOperationLabel.setText("Permissions operation:");
			recursiveOperationLabel.setFont(parent.getFont());
			GridData gd2 = new GridData();
			gd2.horizontalSpan = 2;
			recursiveOperationLabel.setLayoutData(gd2);
			recursiveOperationLabel.setEnabled(recursiveButton.getSelection());
			
			operationCombo = new Combo (recursiveParameters, SWT.READ_ONLY);
			operationCombo.setFont(parent.getFont());
			operationCombo.setItems(new String [] { "ADD", "REMOVE", "SET" });
			operationCombo.setEnabled(recursiveButton.getSelection());
			operationCombo.select(0);
			operationCombo.addSelectionListener( new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					Combo widget = (Combo) e.widget;
					if (widget.getSelectionIndex() == 2) {
						resetsButtonsPermissions();
					}
					else {
						clearButtonsPermissions();
					}
				}
			});
				
			recursiveButton.addListener (SWT.Selection, new Listener () {
				public void handleEvent (Event event) {
					boolean enabled = ((Button) event.widget).getSelection();
					recursiveOperationLabel.setEnabled(enabled);
					operationCombo.setEnabled(enabled);
					if (enabled  &&  operationCombo.getSelectionIndex() != 2) {
						clearButtonsPermissions();
					}
					else {
						resetsButtonsPermissions();
					}
				}
			});	
		}		

		Label newPermLabel = new Label(composite, SWT.NONE);
		newPermLabel.setText("The permissions should be:");
		newPermLabel.setFont(parent.getFont());
		newPermLabel.setLayoutData(new GridData());

		Group userPerm = new Group(composite, SWT.SHADOW_NONE);
		userPerm.setFont(parent.getFont());
		userPerm.setText(" user ");		
		userPerm.setLayout(new GridLayout(3, true));
		readUserButton = createPermButton(parent, userPerm, "readable", getTypePermission(PermissionsClassType.USER, 'r'));
		writeUserButton = createPermButton(parent, userPerm, "writable", getTypePermission(PermissionsClassType.USER, 'w'));
		executeUserButton = createPermButton(parent, userPerm, "executable", getTypePermission(PermissionsClassType.USER, 'x'));
		userPerm.setLayoutData(new GridData());

		Group groupPerm = new Group(composite, SWT.SHADOW_NONE);
		groupPerm.setFont(parent.getFont());
		groupPerm.setText(" group ");		
		groupPerm.setLayout(new GridLayout(3, true));
		readGroupButton = createPermButton(parent, groupPerm, "readable", getTypePermission(PermissionsClassType.GROUP, 'r'));
		writeGroupButton = createPermButton(parent, groupPerm, "writable", getTypePermission(PermissionsClassType.GROUP, 'w'));
		executeGroupButton = createPermButton(parent, groupPerm, "executable", getTypePermission(PermissionsClassType.GROUP, 'x'));
		groupPerm.setLayoutData(new GridData());

		Group othersPerm = new Group(composite, SWT.SHADOW_NONE);
		othersPerm.setFont(parent.getFont());
		othersPerm.setText(" others ");
		othersPerm.setLayout(new GridLayout(3, true));
		readOthersButton = createPermButton(parent, othersPerm, "readable", getTypePermission(PermissionsClassType.OTHER, 'r'));
		writeOthersButton = createPermButton(parent, othersPerm, "writable", getTypePermission(PermissionsClassType.OTHER, 'w'));
		executeOthersButton = createPermButton(parent, othersPerm, "executable", getTypePermission(PermissionsClassType.OTHER, 'x'));
		othersPerm.setLayoutData(new GridData());
		
		Text note = new Text(composite, SWT.MULTI | SWT.READ_ONLY);
		note.setFont(parent.getFont());
		note.setText("NOTE: Setting group and others permissions\n" +
				"  will work only with UNICORE servers 6.4.0+\n" +
				"  and won't work with Java and Windows TSI");
		note.setLayoutData(new GridData());

		return composite;
	}
	
	protected void clearButtonsPermissions()
	{
		readUserButton.setSelection(false);
		writeUserButton.setSelection(false);
		executeUserButton.setSelection(false);

		readGroupButton.setSelection(false);
		writeGroupButton.setSelection(false);
		executeGroupButton.setSelection(false);

		readOthersButton.setSelection(false);
		writeOthersButton.setSelection(false);
		executeOthersButton.setSelection(false);		
	}
	
	protected void resetsButtonsPermissions()
	{
		readUserButton.setSelection(getTypePermission(PermissionsClassType.USER, 'r'));
		writeUserButton.setSelection(getTypePermission(PermissionsClassType.USER, 'w'));
		executeUserButton.setSelection(getTypePermission(PermissionsClassType.USER, 'x'));

		readGroupButton.setSelection(getTypePermission(PermissionsClassType.GROUP, 'r'));
		writeGroupButton.setSelection(getTypePermission(PermissionsClassType.GROUP, 'w'));
		executeGroupButton.setSelection(getTypePermission(PermissionsClassType.GROUP, 'x'));

		readOthersButton.setSelection(getTypePermission(PermissionsClassType.OTHER, 'r'));
		writeOthersButton.setSelection(getTypePermission(PermissionsClassType.OTHER, 'w'));
		executeOthersButton.setSelection(getTypePermission(PermissionsClassType.OTHER, 'x'));		
	}
	
	protected Button createPermButton(Composite parent, Composite composite, String caption, boolean selected)
	{
		Button button = new Button(composite, SWT.CHECK);
		button.setFont(parent.getFont());
		button.setText(caption);
		button.setSelection(selected);
		button.setLayoutData(new GridData());
		return button;
	}
	
	protected boolean getTypePermission(PermissionsClassType.Enum type, char mode)
	{
		int index = 0;
		if (type == PermissionsClassType.GROUP) index += 3;
		else if (type == PermissionsClassType.OTHER) index += 6;		
		switch (mode)
		{
		case 'r' : if (filePermissions.charAt(index) == 'r') return true;
					break;
		case 'w' : if (filePermissions.charAt(index+1) == 'w') return true;
					break;
		case 'x' : if (filePermissions.charAt(index+2) == 'x') return true;
					break;
		}
		return false;
	}
	
	
	// Methods to use in Action class after closing the dialog:
	
	public boolean arePermissionsChanged(PermissionsClassType.Enum permClass)
	{		
		if (recursive)
		{			
			// recursive case
			String noChangeString = "---";
			if (permClass == PermissionsClassType.USER) {
				if (userOperation != PermissionsChangeModeType.SET) {
					return !rwxUserPermissions.equals(noChangeString);
				}
			}
			else if (permClass == PermissionsClassType.GROUP) {
				if (groupOperation != PermissionsChangeModeType.SET) {
					return !rwxGroupPermissions.equals(noChangeString);
				}
			}
			else if (permClass == PermissionsClassType.OTHER) {
				if (othersOperation != PermissionsChangeModeType.SET) {
					return !rwxOthersPermissions.equals(noChangeString);
				}
			}
		}
		else {
			// simple non-recursive case 
			int index = 0;
			String rwxPermissions = rwxUserPermissions;
			if (permClass == PermissionsClassType.GROUP) {
				index = 3;
				rwxPermissions = rwxGroupPermissions;
			}
			else if (permClass == PermissionsClassType.OTHER) {
				index = 6;
				rwxPermissions = rwxOthersPermissions;
			}
			if (filePermissions.regionMatches(true, index, rwxPermissions, 0, 3))
				return false;
		}
		return true;
	}

	public PermissionsChangeModeType.Enum getChangePermissionsOperation(PermissionsClassType.Enum permClass) {
		if (permClass == PermissionsClassType.USER) {
			return userOperation;
		}
		else if (permClass == PermissionsClassType.GROUP) {
			return groupOperation;
		}
		else if (permClass == PermissionsClassType.OTHER) {
			return othersOperation;
		}
		else 
			return null; // Should never happen
	}

	public String getNewPermissionsString(PermissionsClassType.Enum permClass) {
		if (permClass == PermissionsClassType.USER) {
			return rwxUserPermissions;
		}
		else if (permClass == PermissionsClassType.GROUP) {
			return rwxGroupPermissions;
		}
		else if (permClass == PermissionsClassType.OTHER) {
			return rwxOthersPermissions;
		}
		else 
			return null; // Should never happen
	}

	public boolean getRecursiveFlag() {
		return recursive;
	}
}
