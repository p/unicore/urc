package de.fzj.unicore.rcp.servicebrowser.showEditor;

import java.io.InputStream;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

public interface IRemoteFile {

	/**
	 * Returns an open input stream on the contents of this storage. The caller
	 * is responsible for closing the stream when finished.
	 * 
	 * @return an input stream containing the contents of this storage
	 * @exception CoreException
	 *                if the contents of this storage could not be accessed. See
	 *                any refinements for more information.
	 */
	public InputStream getContents() throws CoreException;

	/**
	 * Returns the name of this storage. The name of a storage is synonymous
	 * with the last segment of its full path though if the storage does not
	 * have a path, it may still have a name.
	 * 
	 * @return the name of the data represented by this storage, or
	 *         <code>null</code> if this storage has no name
	 * @see #getFullPath()
	 */
	public String getName();

	public String getRemotePath();

	public long getSize();

	/**
	 * Returns whether this storage is read-only.
	 * 
	 * @return <code>true</code> if this storage is read-only
	 */
	public boolean isReadOnly();

	public void setContents(InputStream source, long bytes, boolean force,
			IProgressMonitor monitor) throws CoreException;

}
