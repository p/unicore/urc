/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.profiles.Profile;
import de.fzj.unicore.rcp.identity.sites.Site;
import de.fzj.unicore.rcp.identity.sites.SiteList;
import de.fzj.unicore.rcp.identity.sites.SiteListView;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.RootNode;

/**
 * @author demuth
 * 
 */
public class AddToSiteListAction extends NodeAction {

	public AddToSiteListAction(Node node) {
		super(node);
		setText("Map to Security Profile");
		setToolTipText("Add an entry to the security profiles view that maps this site to a security profile.");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("lock_open.png"));

	}

	/**
	 * Open a XmlProperties Page This shows the properties document as formated
	 * XML
	 */
	@Override
	public void run() {
		final SiteList siteList = IdentityActivator.getDefault().getSiteList();
		final Site site = new Site();
		Node parent = getNode();
		String namePath = "";
		while (parent != null && !(parent instanceof RootNode)) {
			namePath = parent.getName() + "/" + namePath;
			parent = parent.getParent();
		}

		site.setPattern(namePath + "*");
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {
				try {
					Profile p = IdentityActivator.getDefault().getProfileList()
							.getDefaultProfile();
					if (p != null) {
						site.setProfileName(p.getName());
					}
					siteList.addSite(site);
				} catch (Exception e) {
					IdentityActivator.log("Not adding site.", e);
				}
				IViewPart iview = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.findView(SiteListView.ID);
				if (null != iview) {
					PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage().bringToTop(iview);
				} else {
					try {
						iview = PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getActivePage()
								.showView(SiteListView.ID);
					} catch (PartInitException e) {
						ServiceBrowserActivator.log(IStatus.WARNING,
								"Cant open Site List view", e);
						return;
					}
				}
			}
		});

	}

}
