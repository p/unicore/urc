/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.beans.PropertyChangeEvent;
import java.math.BigInteger;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ApplicationType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDescriptionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdlPosix.EnvironmentType;
import org.ggf.schemas.jsdl.x2005.x11.jsdlPosix.POSIXApplicationDocument;
import org.unigrids.services.atomic.types.StatusType;
import org.unigrids.services.atomic.types.StorageReferenceType;
import org.unigrids.x2006.x04.services.jms.JobPropertiesDocument;
import org.unigrids.x2006.x04.services.jms.JobPropertiesDocument.JobProperties;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UAS;
import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.actions.StartJobAction;
import de.fzj.unicore.uas.StorageManagement;
import de.fzj.unicore.uas.client.JobClient;
import de.fzj.unicore.wsrflite.xmlbeans.WSUtilities;

/**
 * @author demuth
 * 
 */
public class JobNode extends WSRFNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1104576852957170030L;
	public static final String PROPERTY_SUBMISSION_TIME = "submission time";
	public static final String TYPE = UAS.JMS;
	public static final String PROPERTY_JOB_STATUS = "job status";
	public static final String PROPERTY_EXIT_CODE = "exit code";
	public static final String PROPERTY_EXECUTION_PROGRESS = "job execution progress";
	public static final String DETAIL_EXECUTION_LOG = "Execution Log";

	// cache job status in a memory-efficient way; this is crucial for 
	// determining the correct image for the job in reasonable time
	private transient int jobStatus = -1;

	// cache submission time in a memory-efficient way; this is crucial for
	// sorting jobs efficiently
	private transient long normalizedSubmissionTime = -1;

	public JobNode(EndpointReferenceType epr) {
		super(epr);
		// setPersistable(false);
	}

	public void abortJob() {
		try {
			createJobClient().abort();
		} catch (Exception e) {
			ServiceBrowserActivator
			.log(IStatus.ERROR, "Could not abort job", e);
		}
	}

	@Override
	public boolean canBeDestroyed() {
		return true;
	}

	@Override
	public int compareTo(Node n) {
		// sort jobs by submission time
		if (n instanceof JobNode) {
			JobNode other = (JobNode) n;
			Calendar c1 = other.getNormalizedSubmissionTime();
			Calendar c2 = getNormalizedSubmissionTime();
			if (c1 != null && c2 != null) {
				return c1.compareTo(c2);
			} else {
				return 0; // don't change order when submission times are not
				// known (yet)
			}

		}
		return super.compareTo(n);
	}

	private JobClient createJobClient() throws Exception {
		return new JobClient(getURL().toString(), getEpr(), getUASSecProps());
	}

	@Override
	public IStatus doubleClick(IProgressMonitor monitor) {
		setExpanded(true);
		IStatus s = refresh(2, true, monitor);

		//		for (Node n : getChildrenArray()) {
		//			n.setExpanded(true);
		//		}
		return s;
	}

	@Override
	public int getCategory() {
		return 500;
	}

	@Override
	public DetailsAdapter getDetails() {
		DetailsAdapter details = super.getDetails();
		Map<String, String> hm = details.getMap();
		StatusType.Enum jobStatus = getJobStatus();

		if (jobStatus != null) {
			hm.put("Job Status", jobStatus.toString());

		}
		JobProperties jobProps = getJobProperties();
		if (jobProps != null) {

			try {

				JobDescriptionType descr = jobProps.getOriginalJSDL()
				.getJobDescription();
				if (descr != null) {
					ApplicationType appType = descr.getApplication();
					if (appType != null) {
						String version = appType.getApplicationVersion() == null ? "not specified" : appType.getApplicationVersion();
						hm.put("Application", appType
								.getApplicationName()
								+ " (Version "
								+ version+ ")");
					}

					if (getSubmissionTime() != null) {
						hm.put("Submitted at",
								getDefaultDateFormat().format(getSubmissionTime().getTime()));
					}

					String log = jobProps.getLog();
					if (log != null) {

						String newLine = System.getProperty("line.separator");
						String s = jobProps.getLog()
						.replaceAll("\\\n", newLine);
						hm.put(DETAIL_EXECUTION_LOG, s);
					}

					if(StatusType.FAILED.equals(jobStatus) && jobProps.getStatusInfo() != null && jobProps.getStatusInfo().getDescription() != null)
					{
						hm.put("Error message", jobProps.getStatusInfo().getDescription());
					}

					BigInteger exitCode = getExitCode();
					if (exitCode != null) {
						hm.put("Exit code", exitCode.toString());
					}

					Float progress = getExecutionProgress();
					if (progress != null && progress > 0) {
						hm.put("Execution progress", progress.toString());
					}
					try {					

						XmlObject[] xml = WSUtilities.extractAnyElements(descr, POSIXApplicationDocument.type.getDocumentElementName());
						POSIXApplicationDocument posix = (POSIXApplicationDocument) xml[0];
						String vars = "";
						EnvironmentType[] envs = posix.getPOSIXApplication().getEnvironmentArray();
						for(int i = 0; i < envs.length; i++)
						{
							EnvironmentType env = envs[i];
							vars+=env.getName()+"="+env.getStringValue();
							if(i < envs.length -1) vars+=", ";
						}
						hm.put("Environment variables", vars);
					} catch (Throwable t) {
						// do nothing, this is optional
					}

				}

			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.WARNING,
						"Error while parsing job properties", e);
			}
		}
		return details;

	}

	public Float getExecutionProgress() {
		INodeData data = getData();
		if (data == null) {
			return null;
		}
		Object o = data.getProperty(PROPERTY_EXECUTION_PROGRESS);
		if (o != null && o instanceof Float) {
			return (Float) o;
		} else {
			return null;
		}
	}

	public BigInteger getExitCode() {
		INodeData data = getData();
		if (data == null) {
			return null;
		}
		Object o = data.getProperty(PROPERTY_EXIT_CODE);
		if (o != null && o instanceof BigInteger) {
			return (BigInteger) o;
		} else {
			return null;
		}
	}

	public JobProperties getJobProperties() {
		if (!hasCachedResourceProperties()) {
			return null;
		}
		JobPropertiesDocument doc = (JobPropertiesDocument) getCachedResourcePropertyDocument();
		return doc.getJobProperties();
	}

	public StatusType.Enum getJobStatus() {
		if (jobStatus > 0) {
			return jobStatusFromInt(jobStatus);
		} else {
			INodeData data = getData();
			if (data == null) {
				return null;
			}
			Object o = data.getProperty(PROPERTY_JOB_STATUS);
			if (o != null && o instanceof StatusType.Enum) {
				StatusType.Enum result = (StatusType.Enum) o;
				jobStatus = jobStatusFromEnum(result); // cache for later reuse
				return result;
			}
		}
		return null;
	}

	public Calendar getNormalizedSubmissionTime() {
		if (normalizedSubmissionTime > 0) {
			Calendar result = Calendar.getInstance();
			result.setTimeInMillis(normalizedSubmissionTime);
			return result;
		} else {
			Calendar result = getNormalizedTime(getSubmissionTime());
			if (result != null) {
				normalizedSubmissionTime = result.getTimeInMillis();
			}
			return result;
		}
	}

	@Override
	public SchemaType getSchemaType() {
		return JobPropertiesDocument.type;
	}

	/**
	 * 
	 * @return the submission time of this job
	 */
	public Calendar getSubmissionTime() {
		INodeData data = getData();
		if (data == null) {
			return null;
		}
		Object o = data.getProperty(PROPERTY_SUBMISSION_TIME);
		if (o != null && o instanceof Calendar) {
			return (Calendar) o;
		} else {
			return null;
		}
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource() == this) {
			return; // don't react on your own changes!
		}
		if (PROPERTY_JOB_STATUS.equals(evt.getPropertyName())) {
			jobStatus = jobStatusFromEnum((StatusType.Enum) evt.getNewValue()); // cache
			// for
			// later
			// reuse
		}
		super.propertyChange(evt);
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		getAvailableActions().add(new StartJobAction(this));
	}

	/**
	 * create the child nodes of this node
	 */
	@Override
	protected void retrieveChildren() throws Exception {
		List<Node> newChildren = new ArrayList<Node>();
		if (hasCachedResourceProperties()) {
			JobProperties jobProps = getJobProperties();

			// add working directory
			EndpointReferenceType epr = jobProps.getWorkingDirectoryReference();

			Node wdso = NodeFactory.createNode(epr);
			newChildren.add(wdso);

			// storage refs...
			StorageReferenceType[] allEntries = jobProps
			.getStorageReferenceArray();
			for (StorageReferenceType s : allEntries) {
				epr = Utils.newEPR();
				epr.addNewAddress().setStringValue(
						s.getStorageEndpointReference().getAddress()
						.getStringValue());
				Utils.addPortType(epr, StorageManagement.SMS_PORT);

				Node storage = NodeFactory.createNode(epr);
				newChildren.add(storage);

			}

			updateChildren(newChildren);
		}
	}

	@Override
	public void retrieveName() {
		if (hasCachedResourceProperties()) {
			JobProperties jobProps = getJobProperties();
			try {
				if (jobProps != null) {
					JobDefinitionType type = jobProps.getOriginalJSDL();
					if (type != null) {
						String jsdlName = null;
						Calendar submissionTime = null;
						try {
							jsdlName = type.getJobDescription()
							.getJobIdentification().getJobName();
							submissionTime = getNormalizedSubmissionTime();
						} catch (NullPointerException e) {
							// do nothing
						}
						if(jsdlName == null) jsdlName = type.getId();
						if(jsdlName != null)
						{
							String name = jsdlName;
							if (submissionTime != null
									&& !name.contains("submitted at")) {
								DateFormat df = Constants.getDateFormatWithoutColon();
								name += " submitted at "
									+ df.format(submissionTime.getTime());
							}
							setName(name);
							return;
						}
					}
				}
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.WARNING,
						"Error while setting name of Job correctly", e);
			}
		}
		super.retrieveName();
	}

	@Override
	public int retrieveProperties(IProgressMonitor progressMonitor) {
		int result = super.retrieveProperties(progressMonitor);
		JobProperties props = getJobProperties();
		if (props != null) {

			if (props.getStatusInfo() != null) {
				try {
					setJobStatus(props.getStatusInfo().getStatus());
					setExitCode(props.getStatusInfo().getExitCode());
					setExecutionProgress(props.getStatusInfo().getProgress());
				} catch (Exception e) {
					ServiceBrowserActivator.log(IStatus.WARNING,
							"Error while retrieving job status", e);
				}
			}

			try {
				// store the submission time
				// do not use the xmlbeans Calendar instance! it cannot be
				// deserialized by xstream.
				Calendar cal1 = Calendar.getInstance();
				long serverSubmissionTime = props.getSubmissionTime()
				.getTimeInMillis();
				cal1.setTimeInMillis(serverSubmissionTime);
				setSubmissionTime(cal1);
			} catch (Exception e) {
				// do nothing
			}
		}
		return result;
	}

	public void setExecutionProgress(Float progress) {
		putData(PROPERTY_EXECUTION_PROGRESS, progress);
	}

	public void setExitCode(BigInteger exitCode) {
		putData(PROPERTY_EXIT_CODE, exitCode);
	}

	public void setJobStatus(StatusType.Enum status) {
		jobStatus = jobStatusFromEnum(status); // cache for later reuse
		putData(PROPERTY_JOB_STATUS, status);
	}

	public void setSubmissionTime(Calendar submissionTime) {
		putData(PROPERTY_SUBMISSION_TIME, submissionTime);

	}

	public void startJob() {
		try {
			createJobClient().start();
		} catch (Exception e) {
			ServiceBrowserActivator
			.log(IStatus.ERROR, "Could not start job", e);
		}
	}
	

	public void restartJob() {
		try {
			createJobClient().restart();
		} catch (Exception e) {
			ServiceBrowserActivator
			.log(IStatus.ERROR, "Could not restart job", e);
		}
	}

	private static int jobStatusFromEnum(StatusType.Enum status) {
		if (StatusType.FAILED.equals(status)) {
			return 0;
		} else if (StatusType.QUEUED.equals(status)) {
			return 1;
		} else if (StatusType.READY.equals(status)) {
			return 2;
		} else if (StatusType.RUNNING.equals(status)) {
			return 3;
		} else if (StatusType.STAGINGIN.equals(status)) {
			return 4;
		} else if (StatusType.STAGINGOUT.equals(status)) {
			return 5;
		} else if (StatusType.SUCCESSFUL.equals(status)) {
			return 6;
		} else if (StatusType.UNDEFINED.equals(status)) {
			return 7;
		}
		return -1;
	}

	private static StatusType.Enum jobStatusFromInt(int status) {
		switch (status) {
		case 0:
			return StatusType.FAILED;
		case 1:
			return StatusType.QUEUED;
		case 2:
			return StatusType.READY;
		case 3:
			return StatusType.RUNNING;
		case 4:
			return StatusType.STAGINGIN;
		case 5:
			return StatusType.STAGINGOUT;
		case 6:
			return StatusType.SUCCESSFUL;
		case 7:
			return StatusType.UNDEFINED;
		}
		return null;
	}

}
