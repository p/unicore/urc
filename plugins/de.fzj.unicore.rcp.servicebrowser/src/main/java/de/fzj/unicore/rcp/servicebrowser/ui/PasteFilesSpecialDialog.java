/*
 * SetterminationTimeFrame.java
 *
 * Created on August 31, 2006, 4:49 PM
 */

package de.fzj.unicore.rcp.servicebrowser.ui;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;

/**
 * 
 * @author demuth
 */
public class PasteFilesSpecialDialog extends MessageDialog implements
		SelectionListener {

	DateTime dateControl, timeControl;
	Date initialSelection;
	Date minTime = Calendar.getInstance().getTime(), selectedTime = null;
	String errorMessage;
	String fileTransferProtocol;
	Combo fileTransferCombo;
	
	
	/**
	 * Error message label widget.
	 */
	private Text errorMessageText;

	public PasteFilesSpecialDialog(Shell parentShell, String message) {
		super(parentShell, "Paste file(s)", null, message,
				MessageDialog.NONE, new String[] { IDialogConstants.OK_LABEL,
						IDialogConstants.CANCEL_LABEL }, 0);
		setShellStyle(getShellStyle() | SWT.RESIZE);
	}

	@Override
	protected void cancelPressed() {
		super.cancelPressed();
		selectedTime = null;
	}

	@Override
	protected void configureShell(Shell newShell) {
		// newShell.setSize(600, 1000);
		newShell.setText("Calendar");
		super.configureShell(newShell);
	}

	@Override
	public Control createCustomArea(Composite parent) {
		Composite group = new Group(parent,SWT.BORDER);
		GridData gd = new GridData(GridData.FILL_BOTH);

		group.setLayoutData(gd);
		GridLayout layout = new GridLayout(1,false);
		layout.marginBottom = 50;

		group.setLayout(layout);
		
		Label l = new Label(group,SWT.NONE);
		l.setText("Select transfer protocol: ");
		
		fileTransferCombo = new Combo(group, SWT.READ_ONLY);
		String[] items = new String[Constants.KNOWN_TRANSFER_PROTOCOLS.length];
		String defProtocol = UnicoreCommonActivator.getDefault().getDefaultFileTransferProtocol();
		int defProtocolIndex = 0;
		for(int i = 0; i < Constants.KNOWN_TRANSFER_PROTOCOLS.length; i++)
		{
			items[i] = Constants.KNOWN_TRANSFER_PROTOCOLS[i][0];
			if(Constants.KNOWN_TRANSFER_PROTOCOLS[i][1].equals(defProtocol))
			{
				defProtocolIndex = i;
			}
		}
		fileTransferCombo.setItems(items);
		fileTransferCombo.select(defProtocolIndex);
		fileTransferCombo.addSelectionListener(new SelectionListener() {
			
			public void widgetSelected(SelectionEvent e) {
				fileTransferProtocol = Constants.KNOWN_TRANSFER_PROTOCOLS[fileTransferCombo.getSelectionIndex()][1];
			}
			
			public void widgetDefaultSelected(SelectionEvent e) {
				fileTransferProtocol = Constants.KNOWN_TRANSFER_PROTOCOLS[fileTransferCombo.getSelectionIndex()][1];	
			}
		});
		
		l = new Label(group,SWT.NONE);
		l.setText("Set start time: ");
		
		timeControl = new DateTime(group, SWT.BORDER | SWT.TIME | SWT.LONG);
		timeControl.addSelectionListener(this);
		
		
		new Label(group,SWT.NULL); // filler
		
		dateControl = new DateTime(group, SWT.BORDER | SWT.CALENDAR | SWT.LONG);
		
		dateControl.addSelectionListener(this);
		gd = new GridData(GridData.BEGINNING, SWT.CENTER, false, false);
		
		dateControl.setLayoutData(gd);
		
		gd = new GridData(GridData.BEGINNING, SWT.CENTER, false, false);
		gd.widthHint = dateControl.computeSize(-1, -1).x;
		timeControl.setLayoutData(gd);
		
		
		
		
		
		if (initialSelection != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(initialSelection);
			dateControl.setDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH),
					c.get(Calendar.DATE));
			
			timeControl.setHours(c.get(Calendar.HOUR_OF_DAY));
			
			timeControl.setMinutes(c.get(Calendar.MINUTE));
			timeControl.setSeconds(c.get(Calendar.SECOND));
		}

		errorMessageText = new Text(group, SWT.READ_ONLY | SWT.WRAP);
		errorMessageText.setText("");
		errorMessageText.setLayoutData(new GridData(GridData.BEGINNING,
				GridData.CENTER, true, true, 2, 1));
		errorMessageText.setForeground(errorMessageText.getDisplay()
				.getSystemColor(SWT.COLOR_RED));
		errorMessageText.setBackground(errorMessageText.getDisplay()
				.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));

		getShell().layout();
		validateSelection();
		return group;
	}

	/**
	 * Returns the selected time or null if the user pressed Cancel
	 * 
	 * @return
	 */
	public Date getSelectedTime() {
		return selectedTime;
	}

	public void setErrorMessage(String errorMessage) {

		this.errorMessage = errorMessage;
		if (errorMessageText != null && !errorMessageText.isDisposed()) {
			Point shellSize = getShell().getSize();
			int oldSpace = shellSize.y;
			errorMessageText
					.setText(errorMessage == null ? " \n " : errorMessage); //$NON-NLS-1$
			// Disable the error message text control if there is no error, or
			// no error text (empty or whitespace only). Hide it also to avoid
			// color change.
			// See https://bugs.eclipse.org/bugs/show_bug.cgi?id=130281
			boolean hasError = errorMessage != null
					&& (StringConverter.removeWhiteSpaces(errorMessage))
							.length() > 0;
			errorMessageText.setEnabled(hasError);
			errorMessageText.setVisible(hasError);

			errorMessageText.getParent().update();
			errorMessageText.getParent().layout();
			int newSpace = getShell().computeSize(-1, -1).y;
			if (hasError && newSpace > oldSpace) {

				getShell().setSize(shellSize.x,
						shellSize.y + newSpace - oldSpace);
			}
			// Access the ok button by id, in case clients have overridden
			// button creation.
			// See https://bugs.eclipse.org/bugs/show_bug.cgi?id=113643
			Control button = getButton(IDialogConstants.OK_ID);
			if (button != null) {
				button.setEnabled(errorMessage == null);
			}
		}
	}

	public void setInitialSelection(Date initialSelection) {
		this.initialSelection = initialSelection;
	}



	protected void validateSelection() {
		selectedTime = null;
		Calendar c = Calendar.getInstance();
		c.set(dateControl.getYear(), dateControl.getMonth(),
				dateControl.getDay(), timeControl.getHours(),
				timeControl.getMinutes(), timeControl.getSeconds());
		Date newTime = c.getTime();
		if (minTime != null && newTime.before(minTime)) {
			setErrorMessage("Invalid date. Please select a date after "
					+ Constants.getDefaultDateFormat().format(minTime) + ".");
		} else {
			setErrorMessage(null);
			selectedTime = newTime;
		}
	}

	public void widgetDefaultSelected(SelectionEvent e) {
		validateSelection();

	}

	public void widgetSelected(SelectionEvent e) {
		validateSelection();
	}

	public String getFileTransferProtocol() {
		return fileTransferProtocol;
	}

}
