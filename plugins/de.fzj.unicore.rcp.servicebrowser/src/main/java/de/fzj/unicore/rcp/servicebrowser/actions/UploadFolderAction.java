/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.unigrids.services.atomic.types.GridFileType;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.logmonitor.utils.FileDialogUtils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.uas.client.StorageClient;

/**
 * @author demuth
 * 
 */
public class UploadFolderAction extends NodeAction {

	File folder = null;

	public UploadFolderAction(Node node) {
		super(node);
		setText("Upload Folder");
		setToolTipText("Upload a folder to the remote storage or folder");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("folder_up.png"));
	}

	private String getParentPath() throws Exception {
		Node node = getNode();
		if (node instanceof StorageNode) {
			return "";
		} else if (node instanceof FolderNode) {
			FolderNode folderNode = (FolderNode) node;

			GridFileType gft = folderNode.getGridFileType();
			if (gft == null) {
				folderNode.refresh(1, false, null);
				gft = folderNode.getGridFileType();
				if (gft == null) {
					throw new Exception(
							"Remote folder cannot be resolved, cannot upload file(s) here!");
				}
			}
			return gft.getPath();
		} else {
			throw new Exception(
					"Unknown Grid service, cannot upload file(s) here!");
		}

	}

	private StorageClient getStorageClient() throws Exception {
		Node node = getNode();
		if (node instanceof StorageNode) {
			return ((StorageNode) node).getStorageClient();
		} else if (node instanceof FolderNode) {
			FolderNode folderNode = (FolderNode) node;
			return folderNode.getStorageClient();
		} else {
			throw new Exception(
					"Unknown Grid service, cannot upload file(s) here!");
		}
	}

	@Override
	public void run() {
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {
				Shell s = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getShell();
				DirectoryDialog dialog = new DirectoryDialog(s, SWT.OPEN);
				String filterPath = FileDialogUtils.getFilterPath(Constants.FILE_DIALOG_ID_UPLOAD_DIR);
				dialog.setFilterPath(filterPath);
				String filename = dialog.open();
				FileDialogUtils.saveFilterPath(Constants.FILE_DIALOG_ID_UPLOAD_DIR, dialog.getFilterPath());
				if (filename != null) {
					folder = new File(filename);
				}
			}
		});
		if (folder == null) {
			return;
		}
		Job j = new BackgroundJob("Uploading folder") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				try {
					UnicoreStorageTools.uploadFolder(getStorageClient(),
							folder, getParentPath() + "/" + folder.getName(),
							monitor);
				} catch (Exception e) {
					ServiceBrowserActivator.log(IStatus.ERROR, "Unable to upload folder", e);
				} finally {
					monitor.done();
				}
				getNode().refresh(1, true, null);
				return Status.OK_STATUS;
			}
		};
		j.schedule();

	}

}
