package de.fzj.unicore.rcp.servicebrowser.dnd;

import org.eclipse.core.expressions.EvaluationContext;
import org.eclipse.core.expressions.EvaluationResult;
import org.eclipse.core.expressions.Expression;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.dnd.Transfer;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IDragAssistant;

public class DragAssistantDescriptor {
	private final IConfigurationElement element;
	private Transfer[] supportedTransferTypes;
	private Expression dragExpr;

	DragAssistantDescriptor(IConfigurationElement aConfigElement) {
		element = aConfigElement;
		init();
	}

	public IDragAssistant createDragAssistant() {

		try {
			return (IDragAssistant) element
					.createExecutableExtension(IDragAssistant.ATTRIBUTE_CLASS);
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.ERROR, "DragAssistant could not be created", e);
			return null;
		}
	}

	public Transfer[] getSupportedTransferTypes() {
		return supportedTransferTypes;
	}

	private void init() {
		IConfigurationElement[] children = element
				.getChildren(IDragAssistant.ELEMENT_POSSIBLE_DRAG_SOURCES);
		if (children.length == 1) {
			dragExpr = new CustomAndExpression(children[0]);
		}
		IDragAssistant assistant = createDragAssistant();
		supportedTransferTypes = assistant.getSupportedTransferTypes();
	}

	/**
	 * 
	 * @param anElement
	 *            The element from the set of elements being dragged.
	 * @return True if the element matches the drag expression from the
	 *         extension.
	 */
	public boolean isDragElementSupported(Object anElement) {
		if (dragExpr != null && anElement != null) {
			try {
				EvaluationContext context = new EvaluationContext(null,
						anElement);
				context.setAllowPluginActivation(true);
				return dragExpr.evaluate(context) == EvaluationResult.TRUE;
			} catch (CoreException e) {
				ServiceBrowserActivator.log(IStatus.ERROR,
						"Unable to determine drag support for element " + anElement.toString(), e);
			}
		}
		return false;
	}

}
