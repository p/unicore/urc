/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.xmlbeans.XmlException;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.utils.BackgroundWorker;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;

/**
 * @author demuth
 * 
 */
public class NodeData implements INodeData {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4415617772646546903L;
	protected URI uri;
	protected int state;
	protected IPropertyCache propertyCache;
	
	private static final Object notifierLock = new Object();
	private static transient BackgroundWorker notifier;

	/**
	 * Also save a serialized version of the endpoint reference since xstream
	 * cannot serialize eprs.
	 */
	protected String eprString;

	private RegistryNode registryNode;

	/**
	 * The parents of this INodeData
	 */
	private List<INodeData> parents = new ArrayList<INodeData>();

	/**
	 * The children of this INodeData
	 */
	private INodeData[] children = new INodeData[0];

	/**
	 * Views and other interested parties register as a listener here.
	 */
	transient protected PropertyChangeSupport listeners = new PropertyChangeSupport(
			this);

	public NodeData(EndpointReferenceType epr) {
		this(epr, null);
	}

	public NodeData(EndpointReferenceType epr, IPropertyCache propertyCache) {
		setEpr(epr);
		setPropertyCache(propertyCache);

	}

	/**
	 * Add INodeData as child. Set this INodeData as parent of the child. Do not
	 * add child if it already exists in children list! Fire property change to
	 * inform listeners.
	 * 
	 * @param child
	 */
	public void addChild(INodeData child) {
		addChild(child, children.length);
	}

	/**
	 * Add INodeData as child. Set this INodeData as parent of the child. Do not
	 * add child if it already exists in children list! Fire property change to
	 * inform listeners.
	 * 
	 * @param child
	 */
	public void addChild(INodeData child, int index) {
		
		if (isKnownChild(child)) {
			return;
		}
		final List<INodeData> newChildren = getChildren();
		child.addParent(this);
		if(index < 0 || index > newChildren.size()) newChildren.add(child);
		else newChildren.add(index,child);
		setChildren(newChildren);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.nodes.INodeData#addParent(de.fzj.unicore
	 * .rcp.servicebrowser.nodes.INodeData)
	 */
	public void addParent(INodeData parent) {
		if (!parents.contains(parent)) {
			parents.add(parent);
		}
		if (registryNode == null && parent.getRegistryNode() != null) {
			setRegistryNode(parent.getRegistryNode());
		}
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		if (l instanceof Node) {
			Node n = (Node) l;
			if (n.isDisposed()) {
				return;
			}
		}
		// never add the same listener twice
		for (PropertyChangeListener old : getListeners()
				.getPropertyChangeListeners()) {
			if (old.equals(l)) {
				return;
			}
		}
		getListeners().addPropertyChangeListener(l);
	}

	/**
	 * perform some cleanup when node is not needed anymore
	 * 
	 */
	public void dispose() {
		for (PropertyChangeListener l : getListeners()
				.getPropertyChangeListeners()) {
			getListeners().removePropertyChangeListener(l);
		}
		for (INodeData p : parents.toArray(new INodeData[parents.size()])) {
			p.removeChild(this);
		}
		INodeData[] children = this.children;
		for (INodeData c : children) {
			c.removeParent(this);
		}
		parents.clear();
		this.children = new INodeData[0];
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof INodeData)) {
			return false;
		}
		INodeData other = (INodeData) o;
		return getURI().equals(other.getURI());
	}

	@Override
	public void finalize() throws Throwable {
		super.finalize();
	}

	public List<String> getAllNamePathsToRoot() {
		List<String> result = new ArrayList<String>();
		if (getParents().size() == 0) {
			for (String s : getAllNames()) {
				result.add(s + "/");
			}
			return result;
		} else {
			for (INodeData parent : getParents()) {
				List<String> paths = parent.getAllNamePathsToRoot();
				for (String path : paths) {
					for (String name : getAllNames()) {
						result.add(path + name + "/");
					}
				}
			}
			return result;
		}
	}

	public Set<String> getAllNames() {
		HashSet<String> names = new HashSet<String>();
		PropertyChangeListener[] listeners = getListeners()
				.getPropertyChangeListeners();
		for (PropertyChangeListener propertyChangeListener : listeners) {
			if (propertyChangeListener instanceof Node) {
				Node n = (Node) propertyChangeListener;
				if (n.getName() != null) {
					names.add(n.getName());
				}
			}
		}
		return names;
	}

	public List<List<INodeData>> getAllPathsToRoot() {
		List<List<INodeData>> result = new ArrayList<List<INodeData>>();
		if (getParents().size() == 0) {
			List<INodeData> currentPath = new ArrayList<INodeData>();
			currentPath.add(this);
			result.add(currentPath);
			return result;
		} else {
			for (INodeData parent : getParents()) {
				List<List<INodeData>> paths = parent.getAllPathsToRoot();
				for (List<INodeData> path : paths) {
					path = new ArrayList<INodeData>(path);
					path.add(this);
					result.add(path);
				}
			}
			return result;
		}
	}

	public List<INodeData> getChildren() {
		return new ArrayList<INodeData>(Arrays.asList(children));
	}

	public INodeData[] getChildrenArray() {
		return children;
	}

	public Map<URI, INodeData> getChildrenMap() {
		Map<URI, INodeData> result = new HashMap<URI, INodeData>();
		INodeData[] children = this.children;
		for (INodeData node : children) {
			result.put(node.getURI(), node);
		}
		return result;
	}

	public EndpointReferenceType getEpr() {
		if (eprString != null) {
			try {
				return EndpointReferenceType.Factory.parse(eprString);
			} catch (XmlException e) {
			}
		}
		return null;
	}

	protected PropertyChangeSupport getListeners() {
		if (listeners == null) {
			listeners = new PropertyChangeSupport(this);
		}
		return listeners;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.INodeData#getParents()
	 */
	public List<INodeData> getParents() {
		return parents;
	}

	public Object getProperty(String key) {
		IPropertyCache cache = getPropertyCache();
		if (cache == null) {
			return null;
		}
		return cache.getProperty(this, key);
	}

	public IPropertyCache getPropertyCache() {
		return propertyCache;
	}

	public RegistryNode getRegistryNode() {
		return registryNode;
	}

	public URI getURI() {
		
		if (uri == null && getEpr() != null) {
			try {
				uri = new URI(getEpr().getAddress().getStringValue());
			} catch (Exception e) {
				ServiceBrowserActivator
						.log("Unable to determine URL for node! ");
			}

		}
		return uri;
	}

	public boolean hasChildren() {
		return children.length > 0;
	}
	
	@Override
	public int hashCode() {
		return uri.hashCode();
	}

	public boolean isKnownChild(INodeData so) {
		if(so == null) return false;
		INodeData[] children = this.children;
		
		for(INodeData child : children)
		{
			if(so.equals(child)) return true;
		}
		return false;
	}

	public void removeAllChildren() {
		final List<INodeData> oldChildren = getChildren();
		final List<INodeData> newChildren = new ArrayList<INodeData>();
		children = new INodeData[0];
		getNotifier().scheduleWork(new Runnable() {
			public void run() {
				getListeners().firePropertyChange(
						new PropertyChangeEvent(this, PROPERTY_DATA_CHILDREN,
								oldChildren, newChildren));
			}
		});
		
	}

	/**
	 * Remove INodeData from list of children. Fire property change to inform
	 * listeners.
	 * 
	 * @param child
	 */
	public void removeChild(INodeData child) {
		if (!isKnownChild(child)) {
			return;
		}
		final List<INodeData> newChildren = getChildren();
		child.removeParent(this);
		newChildren.remove(child);
		setChildren(newChildren);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.nodes.INodeData#removeParent(de.fzj
	 * .unicore.rcp.servicebrowser.nodes.INodeData)
	 */
	public void removeParent(INodeData parent) {
		parents.remove(parent);

	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		if(l == null) return;
		getListeners().removePropertyChangeListener(l);
	}

	public void setChildren(List<INodeData> children) {
		setChildren(children, this);
	}

	public void setChildren(List<INodeData> children, Object source) {
		final List<INodeData> oldChildren = getChildren();
		this.children = children.toArray(new INodeData[children.size()]);
		final List<INodeData> newChildren = new ArrayList<INodeData>(children);
		getNotifier().scheduleWork(new Runnable() {
			public void run() {
				getListeners().firePropertyChange(
						new PropertyChangeEvent(this, PROPERTY_DATA_CHILDREN,
								oldChildren, newChildren));
			}
		});
	}

	public void setEpr(EndpointReferenceType epr) {
		if (epr != null) {
			this.eprString = epr.xmlText();
		}
		else eprString = null;
	}

	/**
	 * Deprecated
	 * 
	 * @deprecated Please use {@link #setProperty(String, Object, Object)} and
	 *             provide yourself as source for the property change. This
	 *             allows for a safe way to ignore your own changes when
	 *             listening to the node data.
	 */
	@Deprecated
	public void setProperty(String key, Object value) {
		setProperty(key, value, this);
	}

	public void setProperty(String key, Object value, Object source) {
		IPropertyCache cache = getPropertyCache();
		if (cache == null) {
			return;
		}
		Object oldValue = cache.getProperty(this, key);
		cache.setProperty(this, key, value);
		final PropertyChangeEvent evt = new PropertyChangeEvent(source, key,
				oldValue, value);
		getNotifier().scheduleWork(new Runnable() {
			public void run() {
				getListeners().firePropertyChange(evt);
			}
		});

	}

	public void setPropertyCache(IPropertyCache propertyCache) {
		this.propertyCache = propertyCache;

	}

	public void setRegistryNode(RegistryNode registryNode) {
		this.registryNode = registryNode;
	}

	@Override
	public String toString() {

		if (getURI() != null) {
			return "NodeData: " + getURI().toString();
		} else {
			return "NodeData";
		}
	}

	public static BackgroundWorker getNotifier() {
		synchronized (notifierLock) {
			if(notifier == null) notifier = new BackgroundWorker("");
		}
		return notifier;
	}
}
