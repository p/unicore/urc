package de.fzj.unicore.rcp.servicebrowser.ui;

import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.DialogSettings;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * Wizard for exporting a list of registry URLs and their names to an external
 * file.
 */
public class ExportMonitoredEntriesWizard extends Wizard implements
		IExportWizard {

	private ExportMonitoredEntriesWizardPage exportPage = null;

	public ExportMonitoredEntriesWizard() {
		setNeedsProgressMonitor(true);
		setWindowTitle("Title");
	}

	@Override
	public void addPages() {
		exportPage = new ExportMonitoredEntriesWizardPage();
		addPage(exportPage);
	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// no initialization needed
	}

	/**
	 * Called when the user clicks finish. Saves the task data. Waits until all
	 * overwrite decisions have been made before starting to save files. If any
	 * overwrite is canceled, no files are saved and the user must adjust the
	 * dialog.
	 */
	@Override
	public boolean performFinish() {
		final String dest = exportPage.getDestination(); // avoid invalid thread
															// access!

		Job job = new BackgroundJob("Exporting bookmarks...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				IDialogSettings toSave = new DialogSettings(null);

				Node[] entries = ServiceBrowserActivator.getDefault()
						.getGridNode().getChildrenArray();// are the monitored
															// entries
				String[] names = new String[entries.length];
				String[] eprs = new String[entries.length];
				for (int i = 0; i < entries.length; i++) {
					names[i] = entries[i].getName();
					eprs[i] = entries[i].getEpr().xmlText();
				}

				toSave.put(
						ServiceBrowserConstants.DIALOG_SETTINGS_MONITORED_ENTRY_NAMES,
						names);
				toSave.put(
						ServiceBrowserConstants.DIALOG_SETTINGS_MONITORED_ENTRY_EPRS,
						eprs);
				try {
					toSave.save(dest);
				} catch (IOException e) {
					ServiceBrowserActivator.log(IStatus.ERROR, "Bookmarks could not be saved to selected file", e);
					return Status.CANCEL_STATUS;
				}
				return Status.OK_STATUS;
			}
		};
		job.schedule();
		return true;
	}
}
