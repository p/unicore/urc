package de.fzj.unicore.rcp.servicebrowser.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.dnd.FileDropAssistant;
import de.fzj.unicore.rcp.servicebrowser.dnd.NodeLocalSelectionContents;
import de.fzj.unicore.rcp.servicebrowser.dnd.NodeLocalSelectionTransfer;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;
import de.fzj.unicore.rcp.servicebrowser.ui.PasteFilesSpecialDialog;
import de.fzj.unicore.rcp.servicebrowser.utils.RemoteFileUtils;

public class PasteFilesSpecialAction extends NodeAction {
	private ISelectionProvider selectionProvider;
	private Clipboard clipboard;

	private static final int SERVER_TO_SERVER = 1, LOCAL_TO_SERVER = 2;


	public PasteFilesSpecialAction(ISelectionProvider selectionProvider,
			Clipboard clipboard) {
		super(null);
		this.selectionProvider = selectionProvider;
		this.clipboard = clipboard;
		init();
	}

	public PasteFilesSpecialAction(Node node) {
		super(node);
		init();
	}

	protected void init() {
		String text = "Paste Special";


		setText(text);
		setToolTipText("Paste elements from system clipboard");

		ISharedImages sharedImages = PlatformUI.getWorkbench()
		.getSharedImages();
		setImageDescriptor(ServiceBrowserActivator.getImageDescriptor("paste_special.gif"));
		setDisabledImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_PASTE_DISABLED));
	}


	@Override
	@SuppressWarnings("unchecked")
	public void run() {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {

			public void run() {
				String ERROR_MSG = "Unable to paste clipboard content into selected node(s).";
				if (clipboard == null) {
					clipboard = new Clipboard(getViewer().getControl()
							.getDisplay());
				}
				IStructuredSelection selection = (IStructuredSelection) selectionProvider
				.getSelection();
				Node[] parents = (Node[]) selection.toList().toArray(
						new Node[selection.size()]);
				if (parents.length != 1) {
					ServiceBrowserActivator.log(IStatus.ERROR, ERROR_MSG);
					return;
				}
				final Node target = parents[0];

				int operation = DND.DROP_COPY;
				int mode = 0;
				Object toPaste = null;
				Object contents = clipboard
				.getContents(NodeLocalSelectionTransfer.getTransfer());
				TransferData transferData = null;
				if (contents != null) {
					mode = SERVER_TO_SERVER;
					operation = ((NodeLocalSelectionContents) contents)
					.getOperation();
					IStructuredSelection sel = (IStructuredSelection) ((NodeLocalSelectionContents) contents)
					.getSelection();
					Object[] arr = sel.toArray();
					List<String> sources = new ArrayList<String>();
					for (int i = 0; i < arr.length; i++) {
						Object object = arr[i];
						if (object instanceof AbstractFileNode) {
							sources.add(((AbstractFileNode) object).getFileAddressWithoutProtocol().toString());
						} 
					}
					transferData = NodeLocalSelectionTransfer.getTransfer()
					.getSupportedTypes()[0];
					toPaste = sources.toArray(new String[sources.size()]);
				} else {
					contents = clipboard.getContents(FileTransfer.getInstance());
					if (contents != null) {
						mode = LOCAL_TO_SERVER;
						toPaste = contents;
						transferData = FileTransfer.getInstance()
						.getSupportedTypes()[0];

					} else {
						boolean supportedTransferData = false;
						for (TransferData td : clipboard.getAvailableTypes()) {
							if (NodeLocalSelectionTransfer.getTransfer()
									.isSupportedType(td)) {
								supportedTransferData = true;
								break;
							}
						}
						if (!supportedTransferData) {
							ServiceBrowserActivator.log(IStatus.ERROR,
									ERROR_MSG);
							return;
						}
						contents = clipboard.getContents(LocalSelectionTransfer
								.getTransfer());
						if (contents != null) {
							toPaste = ((LocalSelectionTransfer) contents)
							.getSelection();
							transferData = LocalSelectionTransfer.getTransfer()
							.getSupportedTypes()[0];
						}
					}
				}
				if (contents == null) {
					ServiceBrowserActivator.log(IStatus.ERROR, ERROR_MSG);
					return;
				}


				PasteFilesSpecialDialog dialog = 
					new PasteFilesSpecialDialog(
							PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getShell(), "Please configure this special file transfer");
				int result = dialog.open();
				final Date startTime = dialog.getSelectedTime();
				final String protocol = dialog.getFileTransferProtocol(); 
				if(result != PasteFilesSpecialDialog.CANCEL && startTime != null)
				{
					doTransfer(operation, mode, transferData, toPaste, target, startTime, protocol);
				}


			}
		});
	}
	
	
	
	private void doTransfer(int operation, int mode, final TransferData transferData, Object toPaste, final Node target, final Date startTime, final String protocol)
	{
		try {

			switch (mode) {
			case SERVER_TO_SERVER:
				doRemoteTransfer(operation, mode, transferData, toPaste, target, startTime, protocol);
				break;
			case LOCAL_TO_SERVER:
				doLocalTransfer(operation, mode, transferData, toPaste, target, startTime, protocol);
				break;

			}

		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.ERROR,"Unable to perform file transfer",e);
		}
	}
	
	
	private void doLocalTransfer(final int operation, int mode, final TransferData transferData, final Object toPaste, final Node target, final Date startTime, final String protocol)
	{
		Job j = new Job("Waiting for transfer to start, do NOT close the client!")
		{
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				long start = System.currentTimeMillis();
				long timeToGo = Math.max(0,(startTime.getTime() - start));
				int effort = ProgressUtils.calculateEffort(timeToGo);
				int totalDone = 0;
				long sleep = 5000;
				
				monitor.beginTask("Scheduled for "+Constants.getDefaultDateFormat().format(startTime), effort);
				try {
					long remaining = startTime.getTime() - System.currentTimeMillis();
					while(remaining > 0)
					{
						if(monitor.isCanceled()) return Status.CANCEL_STATUS;
						try {
							
							Thread.sleep(sleep);
							int done = ProgressUtils.calculateEffort(System.currentTimeMillis()-start,timeToGo);
							int diff = done - totalDone;
							monitor.worked(diff);
							totalDone = done;
						} catch (Exception e) {
							// do nothing
						}
						remaining = startTime.getTime() - System.currentTimeMillis();
						
					}
				} finally {
					monitor.done();
				}
				FileDropAssistant assistant = new FileDropAssistant();
				assistant.performDrop(target, operation, transferData, toPaste, protocol);
				return Status.OK_STATUS;
			}

		};
		j.setPriority(Job.DECORATE);
		j.schedule();
	}
	
	private void doRemoteTransfer(final int operation, int mode, final TransferData transferData, Object toPaste, final Node target, final Date startTime, final String protocol)
	{
		final String[] sources = (String[]) toPaste;
		Job transferStarter = new BackgroundJob("starting scheduled file transfer") {
			
			@Override
			protected IStatus run(IProgressMonitor monitor)
			{
				try
				{
					RemoteFileUtils.serverToServerFileTransfer(sources, target, false, operation, protocol, startTime, monitor);
				} catch (Exception e) {
					ServiceBrowserActivator.log(IStatus.ERROR, "Unable to start scheduled file transfer: ", e);
				}	
				return Status.OK_STATUS;
			}
		};
		transferStarter.schedule(0);
		
		long delay = Math.max(5000,startTime.getTime() - System.currentTimeMillis());
		Job job = new BackgroundJob(ServiceBrowserConstants.MSG_REFRESHING) {
			@Override
			public boolean belongsTo(Object family) {
				return super.belongsTo(family) || family.equals(RefreshAction.REFRESH_JOB_FAMILY);
			}

			@Override
			public IStatus run(IProgressMonitor monitor) {
				target.refresh(1,false,monitor);

				return Status.OK_STATUS;
			}
		};
		job.setUser(false);
		// updating the grid browser at this point should
		// show at least that the target file has been
		// created.. we add 5 secs to the delay to
		// compensate out-of-sync-clocks and such
		job.schedule(delay+5000); 
	}

	@Override
	public void setViewer(IServiceViewer viewer) {
		super.setViewer(viewer);
		this.selectionProvider = viewer;
	}

}
