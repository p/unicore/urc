package de.fzj.unicore.rcp.servicebrowser.filters;

import java.util.List;

import org.eclipse.jface.viewers.Viewer;

/**
 * This filter connects other filters in a disjunctive fashion: It reveals nodes
 * that are revealed by at least one of the delegate filters
 * 
 * @author bdemuth
 * 
 */
public class DisjunctiveContentFilter extends SkipNodeViewerFilter {

	private List<ContentFilter> filters;

	public DisjunctiveContentFilter(String id, List<ContentFilter> filters) {
		super(id);
		this.filters = filters;
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		for (ContentFilter filter : filters) {
			if (filter.select(viewer, parentElement, element)) {
				return true;
			}
		}
		return false;
	}

}
