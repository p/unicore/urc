package de.fzj.unicore.rcp.servicebrowser.ui;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.IdentityActivator.CredentialChoice;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.identity.keystore.CertificateList;
import de.fzj.unicore.rcp.identity.keystore.KeystoreCredentialController;
import de.fzj.unicore.rcp.identity.utils.ProfileUtils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

public class CreateProfileDialog extends Dialog {

	private String errorMessage;
	private String[] certNames;
	private Combo certCombo = null;
	private Label labelProfileName, errorLabel;
	private Text textProfileName;
	private ServiceSelectionPanel selectionPanel;

	public CreateProfileDialog(Shell parentShell) {
		super(parentShell);
		setBlockOnOpen(true);
	}

	@Override
	public boolean close() {
		if (selectionPanel != null) {
			selectionPanel.dispose();
		}
		return super.close();
	}

	@Override
	protected Control createContents(Composite parent) {
		Control result = super.createContents(parent);
		validate();
		return result;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		// create a composite with standard margins and spacing
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(2, false);
		layout.marginHeight = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_MARGIN);
		layout.marginWidth = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_MARGIN);
		layout.verticalSpacing = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_SPACING);
		layout.horizontalSpacing = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_SPACING);
		composite.setLayout(layout);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));
		applyDialogFont(composite);

		GridData data = new GridData();
		labelProfileName = new Label(composite, SWT.NONE);
		labelProfileName.setText("Profile name: ");
		labelProfileName.setLayoutData(data);

		textProfileName = new Text(composite, SWT.BORDER);
		textProfileName.setText("Profile");
		data = new GridData(GridData.FILL_HORIZONTAL);
		textProfileName.setLayoutData(data);
		textProfileName.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				validate();
			}
		});

		Label selectionLabel = new Label(composite, SWT.NONE);
		selectionLabel.setText("Select a registry: ");
		selectionLabel.setLayoutData(new GridData());
		int style = SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL;
		ISelectionChangedListener l = new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				validate();
			}
		};
		selectionPanel = new ServiceSelectionPanel(composite, null, null, l, 1,
				style);
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 2;
		data.grabExcessVerticalSpace = true;
		data.heightHint = 200;
		selectionPanel.getControl().setLayoutData(data);
		selectionPanel.getTreeViewer().getGridNode().setExpanded(true);

		Label certLabel = new Label(composite, SWT.NONE);
		certLabel.setText("User certificate: ");
		certLabel.setLayoutData(new GridData());

		certNames = new String[] { "" };
		if(CredentialChoice.KEYSTORE == IdentityActivator.getDefault().getCredentialType()){
			CertificateList certificateList = ((KeystoreCredentialController)IdentityActivator.getDefault().getCredential())
					.getCertificateList();
			if (certificateList != null) {
				certNames = certificateList.getCertNames();
			}
		}

		certCombo = new Combo(composite, SWT.NONE);
		certCombo.setItems(certNames);
		if (certNames.length > 0) {
			certCombo.select(0);
		}
		data = new GridData(GridData.FILL_HORIZONTAL);

		certCombo.setLayoutData(data);

		certCombo
		.addSelectionListener(new org.eclipse.swt.events.SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {
				validate();
			}

			public void widgetSelected(SelectionEvent e) {
				validate();
			}
		});

		errorLabel = new Label(composite, SWT.NONE);
		GridData gridDataErrorLabel = new GridData(GridData.FILL_HORIZONTAL);
		gridDataErrorLabel.horizontalSpan = 3;
		errorLabel.setLayoutData(gridDataErrorLabel);
		Color red = PlatformUI.getWorkbench().getDisplay()
				.getSystemColor(SWT.COLOR_RED);
		errorLabel.setForeground(red);

		return composite;
	}

	@Override
	protected void okPressed() {
		String profileName = textProfileName.getText();
		String path = ((Node) selectionPanel.getSelectedNodes()[0])
				.getNamePathToRoot() + "/*";
		int index = certCombo.getSelectionIndex();
		if(index > 0){
			if(CredentialChoice.KEYSTORE == IdentityActivator.getDefault().getCredentialType()){
				CertificateList certificateList = ((KeystoreCredentialController)IdentityActivator.getDefault().getCredential())
						.getCertificateList();
				Certificate cert = certificateList.getCertByIndex(index);
				try {
					ProfileUtils
					.addSecurityProfile(profileName, path, cert, true, true);
				} catch (Exception e) {
					ServiceBrowserActivator.log(IStatus.ERROR, "Unable to create security profile", e);
				}
			}
		}
		setReturnCode(OK);
		close();
	}

	protected void validate() {
		errorMessage = null;
		if (textProfileName.getText().trim().length() == 0) {
			errorMessage = "Profile name must not be empty";
		} else {
			IAdaptable[] selected = selectionPanel.getSelectedNodes();
			if (selected.length == 0 || ((Node) selected[0]).getLevel() <= 1) {
				errorMessage = "Please select a node";
			}
		}
		if (errorMessage == null
				&& (certCombo.getSelectionIndex() == -1 || certNames[certCombo
				                                                     .getSelectionIndex()].trim().length() == 0)) {
			errorMessage = "Please select a certificate";
		}

		if (errorLabel != null) {
			if (errorMessage == null) {
				errorLabel.setText("");
			} else {
				errorLabel.setText(errorMessage);
			}
		}
		getButton(IDialogConstants.OK_ID).setEnabled(errorMessage == null);
	}

}
