package de.fzj.unicore.rcp.servicebrowser;

import org.eclipse.core.expressions.PropertyTester;

import de.fzj.unicore.rcp.common.UnicoreCommonActivator;

/**
 * Returns true if the currentn grid detail level (expert level) is greater than
 * or equal to the expected value.
 *
 * @author bjoernh
 *
 * 10.08.2015 13:47:00
 *
 */
public class GridDetailLevelPropertyTester extends PropertyTester {

	public GridDetailLevelPropertyTester() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		final int currentLevel = UnicoreCommonActivator.getDefault().getGridDetailLevel();
		if(expectedValue instanceof Integer) {
			return currentLevel >= (Integer) expectedValue;
		}

		return false;
	}

}
