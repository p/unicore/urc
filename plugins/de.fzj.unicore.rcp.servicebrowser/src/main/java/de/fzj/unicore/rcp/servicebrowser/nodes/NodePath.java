package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import de.fzj.unicore.rcp.servicebrowser.utils.URIUtils;

/**
 * Instances of this class represent unique paths to nodes in the Grid browser.
 * The uniqueness is achieved by appending a list of URIs from the top level
 * node down to a given node.
 * 
 * @author bdemuth
 * 
 */
public class NodePath {

	private List<URI> uris = new ArrayList<URI>();

	public NodePath() {

	}

	public NodePath(List<URI> uris) {
		this.uris = new ArrayList<URI>(uris);
	}

	public NodePath(URI... uris) {
		for (URI uri : uris) {
			this.uris.add(uri);
		}
	}

	public NodePath(URI uri) {
		uris.add(uri);
	}

	/**
	 * Creates a new path and appends another URI to this path. Returns the new
	 * path.
	 * 
	 * @param uri
	 * @return
	 */
	public NodePath append(URI uri) {
		NodePath result = new NodePath(uris);
		result.uris.add(uri);
		return result;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (!getClass().equals(other.getClass())) {
			return false;
		}
		return toString().equals(other.toString());
	}

	public URI first() {
		if (getLength() == 0) {
			return null;
		}
		return uris.get(0);
	}

	public URI get(int i) {
		return uris.get(i);
	}

	public int getLength() {
		return uris.size();
	}

	/**
	 * Creates a new path that represents the parent of this path and returns
	 * it.
	 * 
	 * @param uri
	 * @return
	 */
	public NodePath getParent() {
		NodePath result = new NodePath(uris);
		result.uris.remove(result.uris.size() - 1);
		return result;
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	public URI last() {
		if (getLength() == 0) {
			return null;
		}
		return uris.get(getLength() - 1);
	}

	@Override
	public String toString() {
		String result = "";
		for (int i = 0; i < uris.size(); i++) {
			URI uri = uris.get(i);
			if (i > 0) {
				result += ",";
			}
			String s = URIUtils.toNormalizedString(uri);
			result += s;
		}
		return result;
	}

	public static NodePath parseString(String in) throws URISyntaxException {
		NodePath result = new NodePath();
		String[] uriStrings = in.split(",");
		for (int i = 0; i < uriStrings.length; i++) {
			URI uri = new URI(uriStrings[i]);
			result.uris.add(uri);
		}
		return result;
	}

}
