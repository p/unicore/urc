/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes.lazy;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeData;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.rcp.servicebrowser.utils.URIUtils;

/**
 * This class implements the {@link ILazyParent} interface. Parent nodes that
 * use this node as a child node should implement the {@link ILazyRetriever}
 * interface! Note that node retrieval is considered an expensive operation and
 * MUST be called by a background thread!
 * 
 * @author demuth
 * 
 */
public class GroupNode extends SecuredNode implements ILazyParent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3006125067074314506L;
	public static final String TYPE = "Enumeration";
	public static final QName PORTTYPE = new QName("http://www.unicore.fzj.de",
			TYPE);

	public static final String PROPERTY_CURRENT_CHILD_INDEX = "current child index";

	public static final String PROPERTY_CHILD_TYPE = "child type of group";

	private String childType;

	public GroupNode(EndpointReferenceType epr) {
		super(epr);
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
		long newCurrentChildIndex = getLimit() - 1;
		if (getCurrentChildIndex() > newCurrentChildIndex
				&& getMoreNode() == null) {
			addChild(createMoreNode());
		}
		setCurrentChildIndex(newCurrentChildIndex);

	}

	@Override
	public void beforeSerialization() {
		// make sure only the first "chunk" of children gets serialized
		int i = 0;
		for (Node n : getChildrenArray()) {
			if (MoreNode.TYPE.equals(n.getType())) {
				n.setPersistable(true);
			} else {
				n.setPersistable(i < getLimit());
			}
			i++;
		}
		super.beforeSerialization();
	}

	@Override
	public int compareTo(Node n) {
		if (n instanceof GroupNode) {
			return super.compareTo(n);
		}
		// put the node to the end of each list
		return 1;
	}
	
	protected MoreNode createMoreNode() {
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue(getMoreNodeURI());
		Utils.addPortType(epr, MoreNode.PORTTYPE);
		MoreNode moreNode = (MoreNode) NodeFactory.createNode(epr);
		return moreNode;
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public int getCategory() {
		return 2000;
	}

	/**
	 * ID used to determine which image should be used to represent this node
	 * (lazy nodes usually represent collections of different resource types).
	 * 
	 * @return
	 */
	public String getChildType() {
		if (childType == null) {
			INodeData data = getData();
			if (data != null) {
				childType = (String) data.getProperty(PROPERTY_CHILD_TYPE);
			}
		}
		return childType;
	}

	public long getCurrentChildIndex() {
		INodeData data = getData();
		if (data == null) {
			return -1;
		}
		Long result = (Long) data.getProperty(PROPERTY_CURRENT_CHILD_INDEX);
		if (result == null) {
			return -1;
		} else {
			return result;
		}
	}

	public ILazyRetriever getLazyParent() {
		Node parent = getParent();
		if (!(parent instanceof ILazyRetriever)) {
			return null;
		}
		return (ILazyRetriever) parent;
	}

	protected int getLimit() {
		return ServiceBrowserActivator
		.getDefault().getPreferenceStore().getInt(ServiceBrowserConstants.P_CHUNK_SIZE_LAZY_CHILD_RETRIEVAL);
	}

	public MoreNode getMoreNode() {
		for (Node n : getChildrenArray()) {
			if (n instanceof MoreNode) {
				return (MoreNode) n;
			}
		}
		return null; // not found
	}

	protected String getMoreNodeURI() {
		return getURI().toString() + "/more";
	}

	public long getNumLazyChildren(IProgressMonitor progress) {
		ILazyRetriever r = getLazyParent();
		if (r == null) {
			return -1;
		} else {
			return r.getNumChildren(this, progress);
		}
	}

	@Override
	public NodePath getPathToRoot() {
		return super.getPathToRoot();
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void init() {
		super.init();
		INodeData data = getData();
		if (data == null) {
			return;
		}

	}

	public IStatus lazilyRetrieveAllChildren(IProgressMonitor progress) {
		progress = ProgressUtils.getNonNullMonitorFor(progress);
		progress.beginTask("", 2);
		try {
			long max = getNumLazyChildren(new SubProgressMonitor(progress, 1));
			return lazilyRetrieveChildrenUpToIndex(max - 1,
					new SubProgressMonitor(progress, 1));
		} finally {
			progress.done();
		}
	}

	public IStatus lazilyRetrieveChildrenUpTo(URI uri, IProgressMonitor progress) {
		String toFind = URIUtils.toNormalizedString(uri);
		updateChildrenFromData();

		int current;
		Node[] children = getChildrenArray();
		for (current = 0; current < children.length; current++) {
			URI childURI = children[current].getURI();
			String compare = URIUtils.toNormalizedString(childURI);
			if (toFind.equals(compare)) {
				return Status.OK_STATUS;
			}
		}
		IStatus s = lazilyRetrieveNextChildren(null);

		while (s.isOK() && getMoreNode() != null) {
			current = 0;
			children = getChildrenArray();
			while (current < children.length) {
				URI childURI = children[current].getURI();
				String compare = URIUtils.toNormalizedString(childURI);
				if (toFind.equals(compare)) {
					return Status.OK_STATUS;
				}
				current++;
			}
			s = lazilyRetrieveNextChildren(progress);
		}
		return s;
	}

	public IStatus lazilyRetrieveChildrenUpToIndex(long index,
			IProgressMonitor progress) {
		INodeData data = getData();
		if (data == null) {
			return Status.CANCEL_STATUS;
		}
		data.setProperty(PROPERTY_CURRENT_CHILD_INDEX, index, this);
		IStatus status = refresh(1, false, progress);
		if (status.isOK()) {
			List<Node> children = getChildren();
			setChildren(sortChildren(children));
			return Status.OK_STATUS;
		} else {
			return status;
		}

	}

	public IStatus lazilyRetrieveNextChildren(IProgressMonitor progress) {
		progress = ProgressUtils.getNonNullMonitorFor(progress);
		progress.beginTask("", 5);
		try {
			long offset = getCurrentChildIndex();

			long max = getNumLazyChildren(new SubProgressMonitor(progress, 1));
			if (max < 0) {
				return Status.CANCEL_STATUS;
			}
			offset += getLimit();
			if (offset > max) {
				offset = max;
			}
			return lazilyRetrieveChildrenUpToIndex(offset,
					new SubProgressMonitor(progress, 4));
		} finally {
			progress.done();
		}
	}

	@Override
	protected void retrieveChildren() {
		INodeData data = getData();
		if (data == null) {
			return;
		}
		synchronized (data) {
			long limit = getLimit();

			List<Node> newChildren = new ArrayList<Node>();

			MoreNode moreNode = getMoreNode();
			long current = getCurrentChildIndex();

			if (current < limit - 1) {
				current = limit - 1;
				putData(PROPERTY_CURRENT_CHILD_INDEX, current);
			}

			long max = getNumLazyChildren(null);
			if (max > current + 1) {
				if (moreNode == null) {
					moreNode = createMoreNode();
				}
				newChildren.add(moreNode);
			} else if (max < current) {
				current = max;
			}

			ILazyRetriever r = getLazyParent();
			if(r != null)
			{
				long numSteps = (long) Math.ceil(((double) current + 1) / limit);
				for (long i = 0; i < numSteps; i++) {
					long num = Math.min(limit, current + 1 - i * limit);

					newChildren.addAll(r.retrieveChildren(this, i * limit, num,
							null));
				}
			}
			updateChildren(newChildren);
		}

	}

	public void setChildType(String childType) {
		putData(PROPERTY_CHILD_TYPE, childType);
		this.childType = childType;
	}

	protected void setCurrentChildIndex(long index) {
		putData(PROPERTY_CURRENT_CHILD_INDEX, index);
	}

	/**
	 * @return the type
	 */
	public static QName getPortType() {
		return PORTTYPE;
	}

}
