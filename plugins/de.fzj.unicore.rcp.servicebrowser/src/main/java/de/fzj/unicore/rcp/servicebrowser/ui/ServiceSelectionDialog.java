/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.eclipse.ui.dialogs.SelectionStatusDialog;

import de.fzj.unicore.rcp.common.utils.ToolBarUtils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceContentProvider;
import de.fzj.unicore.rcp.servicebrowser.ServiceLabelProvider;
import de.fzj.unicore.rcp.servicebrowser.actions.RefreshAction;
import de.fzj.unicore.rcp.servicebrowser.filters.ContentFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterController;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.RootNode;

/**
 * A panel in which nodes of certain types can be selected. The selected nodes
 * can be retrieved with the {@link #getResult()} method. Remember to ALWAYS
 * dispose of the nodes created by this dialog by calling
 * {@link #disposeNodes()} once you are done with looking at the result nodes.
 * 
 * @author demuth
 * 
 */
public class ServiceSelectionDialog extends SelectionStatusDialog implements
		PropertyChangeListener {

	protected ServiceContentProvider contentProvider;

	protected Set<String> interestingTypes;

	protected int expandToLevel = AbstractTreeViewer.ALL_LEVELS;

	protected ServiceTreeViewer treeViewer;

	protected GridNode gridNode;

	private ILabelProvider fLabelProvider;

	private ISelectionStatusValidator fValidator = null;

	private ViewerSorter fSorter;

	private boolean fAllowMultiple = true;

	private boolean fDoubleClickSelects = true;

	private String fEmptyListMessage = "No entries available.";

	private IStatus fCurrStatus = new Status(IStatus.OK, PlatformUI.PLUGIN_ID,
			IStatus.OK, "", null); //$NON-NLS-1$

	private FilterController filterController;

	private DefaultFilterSet defaultFilterSet;

	// toolbar manager used to add additional items
	// to the toolbar above the tree viewer
	private ToolBarManager upperToolBarManager = new ToolBarManager();

	// toolbar manager used to add additional items
	// to the toolbar below the tree viewer
	private ToolBarManager lowerToolBarManager = new ToolBarManager();

	private List<IAdaptable> defaultSelectedNodes;
	
	private RefreshAction refresh;
	
	private int maxDepth;

	private int fWidth = 60;

	private int fHeight = 18;

	public ServiceSelectionDialog() {
		this(null);
	}

	/**
	 * Create a ServiceSelectionDialog.
	 * 
	 * @param interestingTypes
	 *            the types of nodes to be displayed
	 */
	public ServiceSelectionDialog(Set<String> interestingTypes) {
		this(interestingTypes, null);
	}

	/**
	 * Create a ServiceSelectionDialog.
	 * 
	 * @param interestingTypes
	 *            the types of nodes to be displayed
	 * @param selectedNodes
	 *            the default selection
	 */
	public ServiceSelectionDialog(Set<String> interestingTypes,
			List<IAdaptable> defaultSelectedNodes) {
		this(interestingTypes, defaultSelectedNodes, -1);
	}

	/**
	 * Create a ServiceSelectionDialog.
	 * 
	 * @param interestingTypes
	 *            the types of nodes to be displayed
	 * @param selectedNodes
	 *            the default selection
	 * @param maxDepth
	 *            maximal depth of services to be displayed. Used for limiting
	 *            memory and CPU usage.
	 */
	public ServiceSelectionDialog(Set<String> interestingTypes,
			List<IAdaptable> defaultSelectedNodes, int maxDepth) {
		super(ServiceBrowserActivator.getDefault().getWorkbench().getDisplay()
				.getActiveShell());
		fLabelProvider = new ServiceLabelProvider();
		contentProvider = new ServiceContentProvider();

		setResult(new ArrayList(0));
		setStatusLineAboveButtons(true);

		int shellStyle = getShellStyle();
		setShellStyle(shellStyle | SWT.MAX | SWT.RESIZE);

		this.interestingTypes = interestingTypes;
		this.defaultSelectedNodes = defaultSelectedNodes;
		this.maxDepth = maxDepth;
	}
	
	private void cloneGridNode()
	{
		if (maxDepth >= 0) {
			gridNode = (GridNode) ServiceBrowserActivator.getDefault()
					.getGridNode().clone(maxDepth);
		} else {
			gridNode = (GridNode) ServiceBrowserActivator.getDefault()
					.getGridNode().clone();
		}
	}
	
	private void initNodes()
	{
		
		RootNode rootNode = new RootNode();
		rootNode.addChild(gridNode);
	
		if (defaultSelectedNodes != null) {
			setInitialElementSelections(defaultSelectedNodes);
		}
		refresh.setNode(gridNode);
		refresh.setViewer(treeViewer);
		refresh.setEnabled(true);
		
		treeViewer.setGridNode(gridNode);
		if (filterController == null) {
			filterController = new FilterController();
		}
		filterController.addViewer(treeViewer);
		if (interestingTypes == null) {
			interestingTypes = ServiceBrowserActivator.getDefault()
					.getDefaultDisplayedServiceTypes(treeViewer);
		}
		defaultFilterSet = new DefaultFilterSet(filterController);
		defaultFilterSet.filterNodesByType(interestingTypes);

		// expand the tree
		treeViewer.setExpandedLevels(getExpandToLevel());

		Object[] filtered = treeViewer.getAllFilteredChildren(gridNode);

		if (filtered.length == 1 && getInitialElementSelections().size() == 0) {
			List<Object> initial = new ArrayList<Object>();
			initial.add(filtered[0]);
			setInitialElementSelections(initial);
		}
		treeViewer.refresh();
	}

	/**
	 * Set the result using the super class implementation of setResult.
	 * 
	 * @param result
	 * @see SelectionStatusDialog#setResult(int, Object)
	 */
	protected void access$setResult(List result) {
		super.setResult(result);
	}

	/**
	 * Set the result using the super class implementation of buttonPressed.
	 * 
	 * @param id
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	protected void access$superButtonPressed(int id) {
		super.buttonPressed(id);
	}

	private void access$superCreate() {
		super.create();
	}

	/**
	 * Adds a filter to the tree viewer.
	 * 
	 * @param filter
	 *            a filter.
	 */
	public void addFilter(ContentFilter filter) {
		if (filterController == null) {
			filterController = new FilterController();
		}

		filterController.addFilter(filter);
	}

	/**
	 * Handles cancel button pressed event.
	 */
	@Override
	protected void cancelPressed() {
		setResult(null);
		super.cancelPressed();
	}

	@Override
	public boolean close() {
		boolean success = super.close();
		if (contentProvider != null) {
			contentProvider.removePropertyChangeListener(this);
		}
		if (defaultFilterSet != null) {
			defaultFilterSet.dispose();
		}
		if (fLabelProvider != null) {
			fLabelProvider.dispose();
		}
		if (treeViewer != null) {
			treeViewer.dispose();
		}
		if(upperToolBarManager != null)
		{
			upperToolBarManager.dispose();
		}
		if(lowerToolBarManager != null)
		{
			lowerToolBarManager.dispose();
		}
		return success;
	}

	/*
	 * @see SelectionStatusDialog#computeResult()
	 */
	@Override
	protected void computeResult() {
		setResult(((IStructuredSelection) treeViewer.getSelection()).toList());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.window.Window#create()
	 */
	@Override
	public void create() {
		BusyIndicator.showWhile(null, new Runnable() {
			public void run() {
				access$superCreate();
				treeViewer.setSelection(new StructuredSelection(
						getInitialElementSelections()), true);
				updateOKStatus();
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonBar(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createButtonBar(Composite parent) {
		Font font = parent.getFont();
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.marginWidth = 0;
		layout.marginHeight = 0;
		layout.horizontalSpacing = 0;

		composite.setLayout(layout);
		composite
				.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		composite.setFont(font);

		// additionalButtonsSection = new Composite(parent, SWT.NULL);
		// layout = new GridLayout();
		// layout.marginHeight = 0;
		// layout.marginLeft =
		// convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_MARGIN);
		// layout.marginWidth = 0;
		// additionalButtonsSection.setLayout(layout);
		// additionalButtonsSection.setLayoutData(new
		// GridData(GridData.FILL_HORIZONTAL));
		// additionalButtonsSection.setFont(font);

		Control buttonSection = super.createButtonBar(composite);
		((GridData) buttonSection.getLayoutData()).grabExcessHorizontalSpace = true;

		return composite;
	}

	/*
	 * @see Dialog#createDialogArea(Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);

		Label messageLabel = createMessageArea(composite);

		// Label refreshLabel = new Label(composite,SWT.LEFT);
		// refreshLabel.setText("Click to refresh Grid:");
		refresh = new RefreshAction(treeViewer);
		refresh.setText("Refresh Grid");
		refresh.setToolTipText("Press to update the displayed list of Grid services.");
		refresh.setEnabled(false);
		
		Composite upperToolBarContainer = new Composite(composite, SWT.NONE);
		upperToolBarContainer.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER,
				true, false));

		GridLayout layout = new GridLayout(1, true);
		upperToolBarContainer.setLayout(layout);

		ActionContributionItem refreshItem = new ActionContributionItem(refresh);
		refreshItem.setMode(ActionContributionItem.MODE_FORCE_TEXT);
		upperToolBarManager.add(refreshItem);

		ToolBar upperToolBar = upperToolBarManager
				.createControl(upperToolBarContainer);
		

		GridData data = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		upperToolBar.setLayoutData(data);
		ToolBarUtils.setToolItemText(upperToolBar);
		
		
		treeViewer = createTreeViewer(composite);

		data = new GridData(GridData.FILL_BOTH);
		data.widthHint = convertWidthInCharsToPixels(fWidth);
		data.heightHint = convertHeightInCharsToPixels(fHeight);

		Tree treeWidget = treeViewer.getTree();
		treeWidget.setLayoutData(data);
		treeWidget.setFont(parent.getFont());

		if (isTreeEmpty()) {
			messageLabel.setEnabled(false);
		}

		Composite lowerToolBarContainer = new Composite(composite, SWT.NO_FOCUS);
		lowerToolBarContainer.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER,
				true, false));
		layout = new GridLayout(1, true);
		lowerToolBarContainer.setLayout(layout);

		ToolBar lowerToolBar = lowerToolBarManager
				.createControl(lowerToolBarContainer);
		lowerToolBar.setLayout(new GridLayout(lowerToolBar.getItemCount(),
				false));
		data = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		lowerToolBar.setLayoutData(data);
		
		ToolBarUtils.setToolItemText(lowerToolBar);
		
		return composite;
	}

	/**
	 * Creates the tree viewer.
	 * 
	 * @param parent
	 *            the parent composite
	 * @return the tree viewer
	 */
	protected ServiceTreeViewer createTreeViewer(Composite parent) {
		int style = SWT.BORDER | (fAllowMultiple ? SWT.MULTI : SWT.SINGLE);

		final ServiceTreeViewer treeViewer = new ServiceTreeViewer(parent,
				style);
		treeViewer.setContentProvider(contentProvider);
		contentProvider.addPropertyChangeListener(this);
		treeViewer.setLabelProvider(fLabelProvider);
		if (fDoubleClickSelects) {
			treeViewer.setDoubleClickEnabled(false);
		}
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				access$setResult(((IStructuredSelection) event.getSelection())
						.toList());
				updateOKStatus();
			}
		});
		treeViewer.setSorter(fSorter);

		if (fDoubleClickSelects) {
			Tree tree = treeViewer.getTree();
			tree.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					updateOKStatus();
					if (fCurrStatus.isOK()) {
						access$superButtonPressed(IDialogConstants.OK_ID);
					}
				}
			});
		}
		treeViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				updateOKStatus();

				// If it is not OK or if double click does not
				// select then expand
				if (!(fDoubleClickSelects && fCurrStatus.isOK())) {
					ISelection selection = event.getSelection();
					if (selection instanceof IStructuredSelection) {
						Object item = ((IStructuredSelection) selection)
								.getFirstElement();
						if (treeViewer.getExpandedState(item)) {
							treeViewer.collapseToLevel(item, 1);
						} else {
							treeViewer.expandToLevel(item, 1);
						}
					}
				}
			}
		});
		final Display d = parent.getDisplay();
		Job j = new Job("Initializing dialog") {
			
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				cloneGridNode();
				d.asyncExec(new Runnable() {
					
					public void run() {
						initNodes();
					}
				});
				return Status.OK_STATUS;
			}
		};
		j.schedule();
		return treeViewer;
	}

	public void disposeNodes() {
		if (gridNode != null) {
			gridNode.dispose();
		}
	}

	public int getExpandToLevel() {
		return expandToLevel;
	}

	public ToolBarManager getLowerToolBarManager() {
		return lowerToolBarManager;
	}

	public ServiceTreeViewer getTreeViewer() {
		return treeViewer;
	}

	/**
	 * Returns a toolbar manager that can be used to add additional items to the
	 * toolbar above the tree viewer.
	 */
	public ToolBarManager getUpperToolBarManager() {
		return upperToolBarManager;
	}

	/**
	 * @see org.eclipse.jface.window.Window#handleShellCloseEvent()
	 */
	@Override
	protected void handleShellCloseEvent() {
		super.handleShellCloseEvent();

		// Handle the closing of the shell by selecting the close icon
		if (getReturnCode() == CANCEL) {
			setResult(null);
		}
	}

	private boolean isTreeEmpty() {
		return treeViewer.getAllFilteredChildren(treeViewer.getGridNode()).length == 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.window.Window#open()
	 */
	@Override
	public int open() {

		super.open();
		return getReturnCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent arg0) {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				updateOKStatus();
			}
		});

	}

	/**
	 * Specifies if multiple selection is allowed.
	 * 
	 * @param allowMultiple
	 */
	public void setAllowMultiple(boolean allowMultiple) {
		fAllowMultiple = allowMultiple;
	}

	/**
	 * Specifies if default selected events (double click) are created.
	 * 
	 * @param doubleClickSelects
	 */
	public void setDoubleClickSelects(boolean doubleClickSelects) {
		fDoubleClickSelects = doubleClickSelects;
	}

	/**
	 * Sets the message to be displayed if the list is empty.
	 * 
	 * @param message
	 *            the message to be displayed.
	 */
	public void setEmptyListMessage(String message) {
		fEmptyListMessage = message;
	}

	public void setExpandToLevel(int expandToLevel) {
		this.expandToLevel = expandToLevel;
	}

	/**
	 * Sets the initial selection. Convenience method.
	 * 
	 * @param selection
	 *            the initial selection.
	 */
	public void setInitialSelection(Object selection) {
		setInitialSelections(new Object[] { selection });
	}


	/**
	 * Sets the size of the tree in unit of characters.
	 * 
	 * @param width
	 *            the width of the tree.
	 * @param height
	 *            the height of the tree.
	 */
	public void setSize(int width, int height) {
		fWidth = width;
		fHeight = height;
	}

	/**
	 * Sets the sorter used by the tree viewer.
	 * 
	 * @param sorter
	 */
	public void setSorter(ViewerSorter sorter) {
		fSorter = sorter;
	}

	/**
	 * Sets an optional validator to check if the selection is valid. The
	 * validator is invoked whenever the selection changes.
	 * 
	 * @param validator
	 *            the validator to validate the selection.
	 */
	public void setValidator(ISelectionStatusValidator validator) {
		fValidator = validator;
	}

	/**
	 * Validate the receiver and update the ok status.
	 * 
	 */
	public void updateOKStatus() {
		if (!isTreeEmpty()) {
			if (fValidator != null) {
				fCurrStatus = fValidator.validate(getResult());
				updateStatus(fCurrStatus);
			} else {
				fCurrStatus = new Status(IStatus.OK, PlatformUI.PLUGIN_ID,
						IStatus.OK, "", //$NON-NLS-1$
						null);
			}
		} else {
			fCurrStatus = new Status(IStatus.ERROR, PlatformUI.PLUGIN_ID,
					IStatus.ERROR, fEmptyListMessage, null);
		}
		updateStatus(fCurrStatus);
	}
}
