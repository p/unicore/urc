package de.fzj.unicore.rcp.servicebrowser.exceptions;


/**
 * This exception can be thrown when the Grid node could not be deserialized 
 * correctly, meaning cached Grid data is lost.
 * @author bdemuth
 *
 */
public class GridNodeDeserializationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1341565509493543031L;

	public GridNodeDeserializationException() {
		super();
	}

	public GridNodeDeserializationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public GridNodeDeserializationException(String arg0) {
		super(arg0);
	}

	public GridNodeDeserializationException(Throwable arg0) {
		super(arg0);
	}

}
