package de.fzj.unicore.rcp.servicebrowser.dnd;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TransferData;

public class NodeLocalSelectionTransfer extends ByteArrayTransfer {

	private static final String TYPE_NAME = "node local-selection-transfer-format" + (new Long(System.currentTimeMillis())).toString(); //$NON-NLS-1$;

	private static final int TYPEID = registerType(TYPE_NAME);

	private static final NodeLocalSelectionTransfer INSTANCE = new NodeLocalSelectionTransfer();

	private ISelection selection;

	private long selectionSetTime;

	/**
	 * The operation to be performed when the paste is completed. Can be set to
	 * any of {@link DND#DROP_COPY} or {@link DND#DROP_MOVE}.
	 */
	protected int operation = DND.DROP_COPY;

	public int getOperation() {
		return operation;
	}

	/**
	 * Returns the local transfer data.
	 * 
	 * @return the local transfer data
	 */
	public ISelection getSelection() {
		return selection;
	}

	/**
	 * Returns the time when the selection operation this transfer is associated
	 * with was started.
	 * 
	 * @return the time when the selection operation has started
	 * 
	 * @see org.eclipse.swt.events.TypedEvent#time
	 */
	public long getSelectionSetTime() {
		return selectionSetTime;
	}

	/**
	 * Returns the type id used to identify this transfer.
	 * 
	 * @return the type id used to identify this transfer.
	 */
	@Override
	protected int[] getTypeIds() {
		return new int[] { TYPEID };
	}

	/**
	 * Returns the type name used to identify this transfer.
	 * 
	 * @return the type name used to identify this transfer.
	 */
	@Override
	protected String[] getTypeNames() {
		return new String[] { TYPE_NAME };
	}

	private boolean isInvalidNativeType(Object result) {
		return !(result instanceof byte[])
				|| !TYPE_NAME.equals(new String((byte[]) result));
	}

	/**
	 * Overrides org.eclipse.swt.dnd.ByteArrayTransfer#javaToNative(Object,
	 * TransferData). Only encode the transfer type name since the selection is
	 * read and written in the same process.
	 * 
	 * @see org.eclipse.swt.dnd.ByteArrayTransfer#javaToNative(java.lang.Object,
	 *      org.eclipse.swt.dnd.TransferData)
	 */
	@Override
	public void javaToNative(Object object, TransferData transferData) {
		byte[] check = TYPE_NAME.getBytes();
		super.javaToNative(check, transferData);
	}

	/**
	 * Overrides
	 * org.eclipse.swt.dnd.ByteArrayTransfer#nativeToJava(TransferData). Test if
	 * the native drop data matches this transfer type.
	 * 
	 * @see org.eclipse.swt.dnd.ByteArrayTransfer#nativeToJava(TransferData)
	 */
	@Override
	public Object nativeToJava(TransferData transferData) {
		Object result = super.nativeToJava(transferData);
		if (isInvalidNativeType(result)) {
			return null;
		}
		NodeLocalSelectionContents contents = new NodeLocalSelectionContents(
				getSelection(), getOperation());
		return contents;
	}

	public void setOperation(int operation) {
		this.operation = operation;
	}

	/**
	 * Sets the transfer data for local use.
	 * 
	 * @param s
	 *            the transfer data
	 */
	public void setSelection(ISelection s) {
		selection = s;
	}

	/**
	 * Sets the time when the selection operation this transfer is associated
	 * with was started. If assigning this from an SWT event, be sure to use
	 * <code>setSelectionTime(event.time & 0xFFFF)</code>
	 * 
	 * @param time
	 *            the time when the selection operation was started
	 * 
	 * @see org.eclipse.swt.events.TypedEvent#time
	 */
	public void setSelectionSetTime(long time) {
		selectionSetTime = time;
	}

	public static NodeLocalSelectionTransfer getTransfer() {
		return INSTANCE;
	}
}
