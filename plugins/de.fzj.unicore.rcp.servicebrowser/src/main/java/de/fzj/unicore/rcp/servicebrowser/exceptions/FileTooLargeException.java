package de.fzj.unicore.rcp.servicebrowser.exceptions;


/**
 * This exception can be thrown when a remote site seems to be too large to
 * handle, e.g. when we would run out of memory if we were to display it in a
 * simple text editor.
 * @author bdemuth
 *
 */
public class FileTooLargeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1341565509493543031L;

	public FileTooLargeException() {
		super();
	}

	public FileTooLargeException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public FileTooLargeException(String arg0) {
		super(arg0);
	}

	public FileTooLargeException(Throwable arg0) {
		super(arg0);
	}

}
