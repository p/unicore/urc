/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.filters;

import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;

/**
 * Provides functionality for GridBrowserFilters to show also the child nodes of
 * fitting elements. It is possible to specify that only direct children of 1st
 * level of nodes that match the criteria are displayed. It is also possible to
 * specify that all children of fitting nodes are displayed.
 * 
 * @author Christian Hohmann
 * 
 */
public abstract class StandardFilterCriteria implements FilterCriteria {

	private boolean alwaysShowRegistries = true;
	boolean showDirectChildNodes = true;
	boolean showAllChildNodes = true;

	/**
	 * THIS is the method that user defined filter should overwrite! Here are the
	 * criteria specified that hold the filters functionality, must be
	 * overwritten to let the filter take effect.
	 * 
	 * @param node
	 *            is the node to be checked
	 * @return only true if the node fits all specified criteria
	 */
	public abstract boolean fitCriteria(Node node);

	/**
	 * 
	 * @return the showAllChildNodes value
	 */
	public boolean isShowAllChildNodes() {
		return showAllChildNodes;
	}

	/**
	 * 
	 * @return the showDirectChildNodes value
	 */
	public boolean isShowDirectChildNodes() {
		return showDirectChildNodes;
	}

	/**
	 * By default, registries are never filtered out so the user can still see
	 * the parent "Grid" of some service. This can be changed with this method.
	 */
	public void setAlwaysShowRegistries(boolean alwaysShowRegistries) {
		this.alwaysShowRegistries = alwaysShowRegistries;
	}

	/**
	 * If this is true, a child node is visible in the gridBrowser if any node
	 * in its upper hierarchy fitted the criteria
	 * 
	 * @param showAllChildNodes
	 */
	public void setShowAllChildNodes(boolean showAllChildNodes) {
		this.showAllChildNodes = showAllChildNodes;
	}

	/**
	 * if this is true, only direct child nodes of nodes that fit the criteria
	 * itself are displayed in the gridBrowser
	 * 
	 * @param showDirectChildNodes
	 */
	public void setShowDirectChildNodes(boolean showDirectChildNodes) {
		this.showDirectChildNodes = showDirectChildNodes;
	}

	/**
	 * return true if this element should be displayed in the GridBrowser, what
	 * includes the elements that fit the criteria itself and those elements
	 * that are children of the first group if specified
	 * 
	 * @param element
	 *            is the element to be checked
	 * @return true if the element should be displayed in the gridBrowser,
	 *         otherwise false
	 */
	public boolean showElement(Node element) {
		if(element == null) 
		{
			return false;
		}
		if (GridNode.TYPE.equals(element.getType())) {
			return true;
		}
		if (alwaysShowRegistries && RegistryNode.TYPE.equals(element.getType())) {
			return true;
		}

		if (fitCriteria(element)) {
			return true;
		} else {

			if (showAllChildNodes) {
				Node parent = element.getParent();
				while (parent != null
						&& !GridNode.TYPE.equals(parent.getType())) {
					if (fitCriteria(parent)) {
						return true;
					}
					parent = parent.getParent();
				}
			}
			if (showDirectChildNodes) {
				if (fitCriteria(element.getParent())) {
					return true;
				}
			}
		}
		return false;

	}
}
