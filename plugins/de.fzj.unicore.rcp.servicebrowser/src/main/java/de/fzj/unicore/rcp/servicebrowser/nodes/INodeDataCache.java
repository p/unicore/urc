/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.io.IOException;
import java.util.List;

import org.w3.x2005.x08.addressing.EndpointReferenceType;

/**
 * @author demuth
 * 
 */
public interface INodeDataCache {

	/**
	 * Used to add {@link INodeCacheListener} objects which are interested in
	 * the creation and disposal of node data objects. The creation of a node
	 * data object signifies that a new service/node has been added to the Grid
	 * Browser. The removal signifies that a node is obsolete. Note that these
	 * listeners are transient, i.e. they are removed when the cache gets
	 * serialized. Also note that the listener is only added if it has not been
	 * added before.
	 * 
	 * @param l
	 */
	public void addNodeFactoryListener(INodeCacheListener l);

	public void afterDeserialization();
	
	public void afterSerialization();

	public void beforeSerialization();

	public void clearAllData();

	public void decrementNodeCount(EndpointReferenceType epr);

	public void flushToDisk() throws IOException;

	public List<INodeData> getAllNodeData();

	/**
	 * Counter for keeping track of how many nodes share this node data. If this
	 * number reaches 0, the node data should be deleted from the node data
	 * cache.
	 */
	public Integer getNodeCount(EndpointReferenceType epr);

	/**
	 * This method is used for obtaining a node data object in order to read
	 * from it.
	 * 
	 * @param epr
	 * @return
	 */
	public INodeData getNodeData(EndpointReferenceType epr);

	public void incrementNodeCount(EndpointReferenceType epr);

	public void putNodeData(INodeData data);

	public void removeNodeData(EndpointReferenceType epr);

	public void removeNodeFactoryListener(INodeCacheListener l);

	public void setNodeCount(EndpointReferenceType epr, int nodeCount);

}
