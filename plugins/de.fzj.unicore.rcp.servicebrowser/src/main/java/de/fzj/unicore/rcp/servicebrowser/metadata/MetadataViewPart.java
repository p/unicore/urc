/*********************************************************************************
 * Copyright (c) 2015 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.metadata;

import java.util.Iterator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerEditor;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.INullSelectionListener;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.ResourceManager;

import de.fzj.unicore.rcp.common.detailsView.KeyValue;
import de.fzj.unicore.rcp.common.guicomponents.DoubleClickColumViewerEditorActivationStrategy;

/**
 * @author bjoernh
 *
 * 09.04.2015 09:24:01
 *
 */
public class MetadataViewPart extends ViewPart implements
 ISelectionListener,
		INullSelectionListener, ISaveablePart {

	public static final String ID = "de.fzj.unicore.rcp.servicebrowser.metadata.MetadataViewPart"; //$NON-NLS-1$
	private Table table;
	private TableViewer tableViewer;
	private Action addMdAction;
	private Action removeAction;
	private Text txtFilter;
	private boolean dirty = false;
	private Action refreshMdAction;
	private Object currentSelection = null;

	private Runnable currentUpdater = null;
	// currentUpdater may be null, so we need an extra lock object
	private Object currentUpdaterLock = new Object();

	public MetadataViewPart() {
	}

	/**
	 * Create contents of the view part.
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(new GridLayout(2, false));
		ViewerFilter mdFilter = new ViewerFilter() {

			@Override
			public boolean select(Viewer viewer, Object parentElement,
					Object element) {
				KeyValue kv = (KeyValue) element;
				if (!txtFilter.isDisposed()) {
				return txtFilter.getText().isEmpty()
						|| kv.getKey().toLowerCase()
								.contains(txtFilter.getText().toLowerCase())
						|| kv.getValue().toLowerCase()
								.contains(txtFilter.getText().toLowerCase());
				}
				return false;
			}
		};
		ViewerFilter[] viewerFilters = new ViewerFilter[] { mdFilter };
		{
			Label lblFilter = new Label(parent, SWT.NONE);
			lblFilter.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
					false, 1, 1));
			lblFilter.setText("Filter");
		}
		{
			txtFilter = new Text(parent, SWT.BORDER | SWT.H_SCROLL | SWT.SEARCH
					| SWT.CANCEL);
			txtFilter
					.setToolTipText("Case insensitive filter for keys and values containing this text.");
			txtFilter.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.keyCode == SWT.ESC) {
						txtFilter.setText("");
					}
				}
			});
			txtFilter.addModifyListener(new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					if (tableViewer != null) {
						tableViewer.refresh();
					}
				}
			});
			txtFilter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
					false, 1, 1));
		}
		tableViewer = new TableViewer(parent, SWT.BORDER | SWT.FULL_SELECTION
				| SWT.MULTI);

		table = tableViewer.getTable();
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		table.setHeaderVisible(true);
		{
			TableColumn tblclmnKey = new TableColumn(table, SWT.NONE);
			tblclmnKey.setWidth(100);
			tblclmnKey.setText("Key");
		}
		{
			TableColumn tblclmnValue = new TableColumn(table, SWT.NONE);
			tblclmnValue.setWidth(167);
			tblclmnValue.setText("Value");
		}
		tableViewer.setContentProvider(new MDContentProvider());
		tableViewer.setLabelProvider(new MDLabelProvider());
		tableViewer.setCellModifier(new MDCellModifier());
		tableViewer.setCellEditors(new CellEditor[] {
				new TextCellEditor(table), new TextCellEditor(table) });
		tableViewer.setColumnProperties(new String[] { "Key", "Value" });
		tableViewer.setFilters(viewerFilters);

		TableViewerEditor
				.create(tableViewer,
						new DoubleClickColumViewerEditorActivationStrategy(
								tableViewer),
						ColumnViewerEditor.TABBING_HORIZONTAL
								| ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR);

		createActions();
		initializeToolBar();

		tableViewer.setInput(null);

		// listen on selection changes in the service browser (?)
		getViewSite().getPage().addSelectionListener(this);
	}

	class MDContentProvider implements IStructuredContentProvider {


		private boolean disposed = false;

		/**
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		@Override
		public void dispose() {
			this.disposed = true;
		}

		/**
		 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer,
		 *      java.lang.Object, java.lang.Object)
		 */
		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			if (oldInput != null) {
				((FileMetadataAdapter) oldInput).removeChangeListener(this);
			}
			if (newInput != null) {
				((FileMetadataAdapter) newInput).addChangeListener(this);
				addMdAction.setEnabled(true);
				removeAction.setEnabled(true);
				refreshMdAction.setEnabled(true);
			} else {
				addMdAction.setEnabled(false);
				removeAction.setEnabled(false);
				refreshMdAction.setEnabled(false);
			}
			setDirty(false);
			if (!this.disposed) {
				tableViewer.refresh();
			}
		}

		/**
		 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
		 */
		@Override
		public Object[] getElements(Object inputElement) {
			if (inputElement instanceof FileMetadataAdapter) {
				FileMetadataAdapter mdAdapter = (FileMetadataAdapter) inputElement;
				return mdAdapter.getMetadata();
			}
			return null;
		}

	}

	class MDLabelProvider implements ITableLabelProvider {

		/**
		 * @see org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.jface.viewers.ILabelProviderListener)
		 */
		@Override
		public void addListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub

		}

		/**
		 * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
		 */
		@Override
		public void dispose() {
		}

		/**
		 * @see org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.Object,
		 *      java.lang.String)
		 */
		@Override
		public boolean isLabelProperty(Object element, String property) {
			return true;
		}

		/**
		 * @see org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.jface.viewers.ILabelProviderListener)
		 */
		@Override
		public void removeListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub

		}

		/**
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
		 *      int)
		 */
		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			// TODO Auto-generated method stub
			return null;
		}

		/**
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
		 *      int)
		 */
		@Override
		public String getColumnText(Object element, int columnIndex) {
			if (element instanceof KeyValue) {
				switch (columnIndex) {
				case 0:
					return ((KeyValue) element).getKey();
				case 1:
					return ((KeyValue) element).getValue();
				default:
					return null;
				}  
			}
			return null;
		}

	}

	class MDCellModifier implements ICellModifier {

		/**
		 * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object,
		 *      java.lang.String)
		 */
		@Override
		public boolean canModify(Object element, String property) {
			return true;
		}

		/**
		 * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object,
		 *      java.lang.String)
		 */
		@Override
		public Object getValue(Object element, String property) {
			if ("Key".equals(property)) {
				return ((KeyValue) element).getKey();
			} else {
				return ((KeyValue) element).getValue();
			}
		}

		/**
		 * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object,
		 *      java.lang.String, java.lang.Object)
		 */
		@Override
		public void modify(Object element, String property, Object value) {
			TableItem tableItem = (TableItem) element;
			KeyValue entry = (KeyValue) tableItem.getData();
			if("Key".equals(property)) {
				entry.setKey((String) value);
			} else {
				entry.setValue((String) value);
			}

			tableViewer.refresh(entry);
			setDirty(true);
		}

	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions

		{
			addMdAction = new Action("Add Metadata") {
				/**
				 * @see org.eclipse.jface.action.Action#run()
				 */
				@Override
				public void run() {
					super.run();
					((FileMetadataAdapter) tableViewer.getInput())
							.addKeyValue();
					setDirty(true);
					tableViewer.refresh();
					// TODO select the newly created item for editing
				}
			};
			addMdAction.setImageDescriptor(ResourceManager
					.getPluginImageDescriptor("de.fzj.unicore.rcp.identity",
							"icons/add.png"));
			addMdAction
					.setToolTipText("Add a new metadata item and according value.");
			addMdAction
.setDescription("Add new metadata");
		}
		{
			removeAction = new Action("Remove Metadata") {
				/**
				 * @see org.eclipse.jface.action.Action#run()
				 */
				@Override
				public void run() {
					super.run();
					Iterator<KeyValue> tbDel = ((IStructuredSelection) tableViewer
							.getSelection()).iterator();
					boolean setDirty = false;
					while (tbDel.hasNext()) {
						setDirty = true;
						((FileMetadataAdapter) tableViewer.getInput())
								.removeKeyValue(tbDel.next());
					}
					if (setDirty) { // only trigger setDirty once
						setDirty(true);
					}
					tableViewer.refresh();
				}
			};
			removeAction.setImageDescriptor(ResourceManager
					.getPluginImageDescriptor("de.fzj.unicore.rcp.identity",
							"icons/delete.png"));
		}
		{
			refreshMdAction = new Action("Refresh Metadata") {
				/**
				 * @see org.eclipse.jface.action.Action#run()
				 */
				@Override
				public void run() {
					super.run();
					((FileMetadataAdapter) tableViewer.getInput()).reset();
					tableViewer.refresh();
					setDirty(false);
				}
			};
			refreshMdAction.setImageDescriptor(ResourceManager
					.getPluginImageDescriptor(
							"de.fzj.unicore.rcp.servicebrowser",
							"icons/refresh.png"));
			refreshMdAction
					.setToolTipText("Refresh metadata from service and discard any local changes");
		}
	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars()
				.getToolBarManager();
		toolbarManager.add(refreshMdAction);
		toolbarManager.add(addMdAction);
		toolbarManager.add(removeAction);
	}

	@Override
	public void setFocus() {
		// Set the focus
	}

	/**
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection != null && selection instanceof IStructuredSelection) {
			IStructuredSelection strSelection = (IStructuredSelection) selection;
			if (strSelection.size() != 1) {
				currentSelection = null;
				tableViewer.setInput(null);
				return;
			}
			final Object element = strSelection.getFirstElement();
			if (element == currentSelection) {
				return;
			}
			if (isDirty()) {
				if (MessageDialog.openQuestion(this.getViewSite().getShell(),
						"Save modified metadata",
						"Would you like to save the modified metadata?")) {
					doSave(null);
				}
			}
			currentSelection = element;

			if (element instanceof IAdaptable) {
				tableViewer.setInput(null);
				synchronized (currentUpdaterLock) {
					currentUpdater = new Runnable() {
						
						@Override
						public void run() {
							synchronized (currentUpdaterLock) {
								if (this != currentUpdater) {
									// we're outdated
									return;
								}
							}
							IAdaptable adpElement = (IAdaptable) element;
							final FileMetadataAdapter mdAdapter = (FileMetadataAdapter) adpElement
									.getAdapter(FileMetadataAdapter.class);
							tableViewer.setInput(mdAdapter);
							setDirty(false);
							synchronized (currentUpdaterLock) {
								if (this == currentUpdater) {
									currentUpdater = null;
								}
							}
						}
					};
					table.getDisplay().asyncExec(currentUpdater);
				}
			}
		}
	}

	protected TableViewer getTableViewer() {
		return tableViewer;
	}

	/**
	 * @see org.eclipse.ui.part.WorkbenchPart#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();
		ResourceManager.dispose();
		getViewSite().getPage().removeSelectionListener(this);
	}

	/**
	 * @see org.eclipse.ui.ISaveablePart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		if (tableViewer != null) {
			((FileMetadataAdapter) tableViewer.getInput()).commit();
			setDirty(false);
		}
	}

	/**
	 * @see org.eclipse.ui.ISaveablePart#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
		// do nothing (not allowed anyway)
	}

	/**
	 * @see org.eclipse.ui.ISaveablePart#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return this.dirty;
	}

	/**
	 * @param dirty
	 *            the dirty to set
	 */
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
		firePropertyChange(PROP_DIRTY);
	}

	/**
	 * @see org.eclipse.ui.ISaveablePart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/**
	 * @see org.eclipse.ui.ISaveablePart#isSaveOnCloseNeeded()
	 */
	@Override
	public boolean isSaveOnCloseNeeded() {
		return true;
	}
}
