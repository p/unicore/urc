package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.io.IOException;

/**
 * A cache for storing the nodes' properties. All large pieces of data should be
 * stored there. Implementations of this must be (de-)serialziable with XStream.
 * 
 * @author bdemuth
 * 
 */
public interface IPropertyCache {

	public void clearAllProperties();

	public void clearPropertiesFor(INodeData data);

	public void flushToDisk() throws IOException;

	public Object getProperty(INodeData data, String key);

	public void setProperty(INodeData data, String key, Object value);
}
