package de.fzj.unicore.rcp.servicebrowser.problems;

import de.fzj.unicore.rcp.logmonitor.problems.ShowPreferencePageSolution;

public class ChangeFileAssociationsSolution extends ShowPreferencePageSolution {

	public ChangeFileAssociationsSolution() {
		super("CHANGE_FILE_ASSOCIATIONS_FOR_EDITING", "FILE_TOO_LARGE_FOR_EDITING",
				"Change associated editor for this content type...");
	}

	@Override
	protected String getPreferencePageId() {
		return "org.eclipse.ui.preferencePages.FileEditors";
	}

}
