/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.filters;

import org.eclipse.jface.viewers.ViewerFilter;

import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * This is the root class of any filters for {@link Node}s. It is abstract and
 * only provides the basic common functionality.
 * 
 * @author demuth
 * 
 */
public abstract class ContentFilter extends ViewerFilter {

	protected FilterController filterController;
	protected boolean active;
	
	private final String id;

	/**
	 * 
	 */
	public ContentFilter(String _id) {
		this.id = _id;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Check whether the filter is currently filtering the content.
	 * 
	 * @return
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Switch the filter on and of without changing its additional properties.
	 * 
	 * @param active
	 */
	public void setActive(boolean active) {
		if (this.active == active) {
			return;
		}
		this.active = active;
		if (filterController != null) {
			filterController.refreshViewers();
		}
	}

	public void setFilterController(FilterController filterController) {
		this.filterController = filterController;
	}
	
	public void filterChanged()
	{
		if (filterController != null) {
			filterController.refreshViewers();
		}
	}

}
