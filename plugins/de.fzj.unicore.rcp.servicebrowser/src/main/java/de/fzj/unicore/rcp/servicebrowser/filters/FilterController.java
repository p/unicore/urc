/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.filters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.jface.viewers.StructuredViewer;

/**
 * TODO: Documentation
 * 
 * @author demuth
 * 
 */
public class FilterController {

	private Map<String, ContentFilter> filters = new ConcurrentHashMap<String, ContentFilter>();
	private transient List<StructuredViewer> viewers = new ArrayList<StructuredViewer>();

	public void addFilter(ContentFilter filter) {
		synchronized (filters) {
			filters.put(filter.getId(), filter);
			filter.setFilterController(this);
			for (StructuredViewer viewer : viewers) {
				viewer.addFilter(filter);
				viewer.refresh();
			}
		}
	}

	public void addViewer(StructuredViewer viewer) {
		synchronized (filters) {
			if(viewers == null) viewers = new ArrayList<StructuredViewer>();
			boolean success = viewers.add(viewer);
			if (success) {
				for (ContentFilter filter : filters.values()) {
					viewer.addFilter(filter);
				}
			}
		}
	}

	public ContentFilter getFilter(String id) {
		return filters.get(id);
	}

	public boolean isFilterActive(String id) {
		synchronized (filters) {
			ContentFilter filter = filters.get(id);
			if (filter != null) {
				return filter.isActive();
			} else {
				return false;
			}
		}
	}

	public void refreshViewers() {
		synchronized (filters) {
			for (StructuredViewer viewer : viewers) {
				viewer.refresh();
			}
		}
	}

	public void removeFilter(ContentFilter filter) {
		synchronized (filters) {
			removeFilter(filter.getId());
		}
	}

	public void removeFilter(String ID) {
		synchronized (filters) {
			ContentFilter filter = filters.remove(ID);
			filter.setFilterController(null);
			for (StructuredViewer viewer : viewers) {
				viewer.removeFilter(filter);
				viewer.refresh();
			}
		}
	}

	public void removeViewer(StructuredViewer viewer) {
		synchronized (filters) {
			boolean success = viewers.remove(viewer);
			if (success) {
				for (ContentFilter filter : filters.values()) {
					viewer.removeFilter(filter);
				}
			}
		}
	}

	public void setFilterActive(String id, boolean active) {
		synchronized (filters) {
			ContentFilter filter = filters.get(id);
			if (filter != null) {
				filter.setActive(active);
			}
			refreshViewers();
		}
	}
}
