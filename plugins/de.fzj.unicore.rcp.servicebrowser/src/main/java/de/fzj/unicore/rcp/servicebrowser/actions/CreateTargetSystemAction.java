/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.unigrids.services.atomic.types.TextInfoType;
import org.unigrids.x2006.x04.services.tsf.CreateTSRDocument;
import org.unigrids.x2006.x04.services.tsf.TargetSystemDescriptionType;

import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.ui.CreateTSSDialog;

/**
 * @author demuth
 * 
 */
public class CreateTargetSystemAction extends NodeAction {

	public CreateTargetSystemAction(TargetSystemFactoryNode tsfNode) {
		super(tsfNode);
		setText("Create Target System...");
		setToolTipText("Create Target System for submitting jobs and accessing storages");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("tsf.png"));
	}

	private void doCreateTSS(Map<String,String>params) {
		try {
			TargetSystemFactoryNode node = (TargetSystemFactoryNode) getNode();
			CreateTSRDocument ctsr=getCreateTSRDocument(params);
			node.createTargetSystem(ctsr);
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.WARNING,
					"Could not create TSS", e);
		}
	}

	private CreateTSRDocument getCreateTSRDocument(Map<String,String>params){
		CreateTSRDocument in=CreateTSRDocument.Factory.newInstance();
		in.addNewCreateTSR();
		TargetSystemDescriptionType desc = in.getCreateTSR()
				.addNewTargetSystemDescription();
		for(Map.Entry<String, String> e: params.entrySet()){
			TextInfoType prop=desc.addNewTextInfo();
			String key = e.getKey();
			String value = e.getValue();
			if(!value.isEmpty()){
				prop.setName(key);
				prop.setValue(value);
			}
		}
		return in;
	}

	@Override
	public boolean isCurrentlyAvailable() {
		if (UnicoreCommonActivator.getDefault().getGridDetailLevel() < UnicoreCommonActivator.LEVEL_EXPERT) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Open a "Create TSS" Frame (on a TSF), gets parameters, and 
	 * creates a new TSS
	 */
	private void createTSS() {
		final Display display = PlatformUI.getWorkbench().getDisplay();
		// A list of maps would probably do, but that's too Pythonic ;)
		final List<UserInputRequest> userInputRequests = createUserInputRequests();
		if (userInputRequests.isEmpty()) {
			doCreateTSS(new HashMap<String, String>());
		} else {
			display.syncExec(new Runnable() {
				@Override
				public void run() {
					CreateTSSDialog dialog = new CreateTSSDialog(display
							.getActiveShell(), userInputRequests);
					dialog.create();
					if (dialog.open() == Window.OK) {
						Map<String, String> params = dialog.getUserInput();
						doCreateTSS(params);
					}
				}
			});
		}
	}

	/**
	 * @return
	 */
	private List<UserInputRequest> createUserInputRequests() {
		List<UserInputRequest> userInputRequests = new ArrayList<UserInputRequest>();
		if (((TargetSystemFactoryNode) getNode()).supportsVirtualization()) {
			userInputRequests.add(new UserInputRequest("imageID", "Image ID",
					""));
		}
		return userInputRequests;
	}

	@Override
	public void run() {
		
		createTSS();
		getNode().refresh(1, true, null);
	}
}
