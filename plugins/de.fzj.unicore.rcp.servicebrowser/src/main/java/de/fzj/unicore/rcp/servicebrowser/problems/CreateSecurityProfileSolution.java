package de.fzj.unicore.rcp.servicebrowser.problems;

import org.eclipse.jface.action.Action;

import de.fzj.unicore.rcp.identity.sites.SiteListView;
import de.fzj.unicore.rcp.logmonitor.problems.ShowViewSolution;
import de.fzj.unicore.rcp.servicebrowser.actions.CreateSecurityProfileAction;
import eu.unicore.problemutil.Problem;

public class CreateSecurityProfileSolution extends ShowViewSolution {

	public CreateSecurityProfileSolution() {
		super("TLS_CLI_GRAPH_PROFILE", "TLS_CLI",
				"Create a security profile for this service or one of its parents...");

	}

	@Override
	protected String getViewId() {
		return SiteListView.ID;
	}

	@Override
	public boolean solve(String message, Problem[] problems, String details) {
		super.solve(message, problems, details);
		Action action = new CreateSecurityProfileAction();
		action.run();
		return true;
	}

}
