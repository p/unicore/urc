/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes.lazy;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * This class represents a service enumeration that can be used to obtain
 * additional resources of a given type.
 * 
 * @author demuth
 * 
 */
public class MoreNode extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = 754994809516040213L;
	public static final String TYPE = "More";
	public static final QName PORTTYPE = new QName("http://www.unicore.fzj.de",
			TYPE);

	private transient boolean expanding = false;

	public MoreNode(EndpointReferenceType epr) {
		super(epr);
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.Node#doubleClick()
	 */
	@Override
	public IStatus doubleClick(IProgressMonitor monitor) {
		if (expanding) {
			return Status.CANCEL_STATUS;
		}
		expanding = true;
		try {
			return retrieveNextChildren(monitor);
		} finally {
			expanding = false;
		}

	}

	@Override
	public int getCategory() {
		return 2000;
	}

	@Override
	public String getDoubleClickActionName() {
		return "show more children";
	}

	@Override
	public String getName() {
		return super.getName();
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	protected void resetExpanded() {
		super.setExpanded(false);
	}

	@Override
	public void retrieveName() {
		setName("");
	}

	protected IStatus retrieveNextChildren(IProgressMonitor progress) {
		progress = ProgressUtils.getNonNullMonitorFor(progress);
		progress.beginTask("", 100);
		try {
			IStatus result = Status.CANCEL_STATUS;
			final Node parent = getParent();
			if (parent instanceof ILazyParent) {

				ILazyParent enu = (ILazyParent) parent;
				IProgressMonitor sub = new SubProgressMonitor(progress, 50);
				result = enu.lazilyRetrieveNextChildren(sub);

				sub = new SubProgressMonitor(progress, 50);
				Node[] peers = parent.getChildrenArray();
				if (peers.length > 0) {
					sub.beginTask("", peers.length);
					try {
						for (Node n : peers) {
							if (sub.isCanceled()) {
								return Status.CANCEL_STATUS;
							}
							if (n.getState() == STATE_NEW) {
								n.refresh(0, false, new SubProgressMonitor(sub,
										1));
							}
						}
					} finally {
						sub.done();
					}
				}
			}
			return result;
		} finally {
			progress.done();
		}
	}

	@Override
	public void setExpanded(boolean expanded) {
		// do NOT call super.setExpanded(boolean) as this will tell the viewer
		// that this node is already expanded
		// which will hide the drill down icon
		if (expanded && !expanding) {
			expanding = true;

			Job j = new BackgroundJob("retrieving children") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try {
						return retrieveNextChildren(monitor);
					} finally {
						expanding = false;

					}

				}
			};
			j.schedule();

		}

	}

	/**
	 * @return the type
	 */
	public static QName getPortType() {
		return PORTTYPE;
	}
}
