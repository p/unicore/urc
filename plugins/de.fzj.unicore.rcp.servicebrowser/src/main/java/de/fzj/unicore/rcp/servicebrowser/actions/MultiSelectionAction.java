/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

/**
 * @author demuth
 * 
 */
public class MultiSelectionAction extends Action implements Comparable<Action> {

	IServiceViewer viewer;
	NodeAction[] actions;

	/**
	 * action that processes identical actions on multiple nodes
	 * 
	 * @param viewer
	 * @param actions
	 */
	public MultiSelectionAction(IServiceViewer viewer, NodeAction[] actions) {
		this.viewer = viewer;
		this.actions = actions;
		setText(actions[0].getText());
		setToolTipText(actions[0].getToolTipText());
		setImageDescriptor(actions[0].getImageDescriptor());

	}

	public int compareTo(Action o) {
		return getText().compareToIgnoreCase(o.getText());
	}

	@Override
	public void run() {
		if (actions.length == 0) {
			return;
		}
		String confirmationMsg = actions[0].getConfirmationMessage();
		if (confirmationMsg != null) {
			String title = "Please confirm";
			if (!MessageDialog.openConfirm(viewer.getControl().getShell(),
					title, confirmationMsg)) {
				return;
			}
		}
		try {
			Job j = new BackgroundJob("executing request") {
				@Override
				public IStatus run(IProgressMonitor monitor) {
					monitor.beginTask("executing request", actions.length);
					for (int i = 0; i < actions.length; i++) {
						if (monitor.isCanceled()) {
							break;
						}
						actions[i].setViewer(viewer);
						monitor.subTask(actions[i].getText() + " "
								+ actions[i].getNode().getName());
						actions[i].run();
						viewer.refresh(actions[i].getNode());
						monitor.worked(1);
					}
					return Status.OK_STATUS;
				}
			};

			j.setPriority(Job.INTERACTIVE);
			j.schedule();
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.WARNING,
					"Unable to finish user operation", e);
		}

	}

}
