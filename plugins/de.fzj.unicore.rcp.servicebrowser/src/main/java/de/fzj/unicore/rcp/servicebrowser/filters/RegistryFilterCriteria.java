package de.fzj.unicore.rcp.servicebrowser.filters;

import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;

/**
 * 
 * @author bdemuth
 * 
 */
public class RegistryFilterCriteria extends StandardFilterCriteria {

	String registryName;

	public RegistryFilterCriteria(String registryName) {
		this.registryName = registryName;
	}

	@Override
	public boolean fitCriteria(Node node) {

		// check Name against entered criteria
		if (registryName == null || registryName.trim().length() == 0) {
			return true;
		} else if (!RegistryNode.TYPE.equals(node)
				&& !(node instanceof RegistryNode)) {
			return false;
		}
		return registryName.equals(node.getName());
	}

}
