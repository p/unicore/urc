/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.common.detailsView.DetailsView;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * @author demuth
 * 
 */
public class ShowDetailsAction extends NodeAction {

	public ShowDetailsAction(Node node) {
		super(node);
		setText("Details");
		setToolTipText("Open the details view");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("zoom.png"));

	}

	/**
	 * Open a XmlProperties Page This shows the properties document as formated
	 * XML
	 */
	@Override
	public void run() {

		if (getNode().getState() == Node.STATE_NEW) {
			RefreshAction ref = new RefreshAction(getNode());
			ref.setViewer(getViewer());
			ref.run();
		}
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {
				IViewPart iview = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.findView(DetailsView.ID);
				if (null != iview) {
					PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage().bringToTop(iview);
				} else {
					try {
						iview = PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getActivePage()
								.showView(DetailsView.ID);
					} catch (PartInitException e) {
						ServiceBrowserActivator.log(IStatus.WARNING,
								"Cant open Details view", e);
						return;
					}
				}
				DetailsView detailsView = (DetailsView) iview;
				detailsView.selectionChanged(null, getViewer().getSelection());
			}

		});

	}

}
