/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuListener2;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchActionConstants;

import de.fzj.unicore.rcp.servicebrowser.actions.MonitorRegistryAction;
import de.fzj.unicore.rcp.servicebrowser.actions.MultiSelectionAction;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeActionExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

/**
 * @author demuth
 * 
 */
public class ContextMenuListener implements IMenuListener, IMenuListener2 {

	private IServiceViewer viewer;
	private MonitorRegistryAction monitorRegistryAction;
	// NodeActions are handled differently than normal Eclipse popup menu 
	// extensions (for historical reasons..). They are created programmatically
	// and must be disposed of explicitly
	private Set<String> toCleanUp;

	public ContextMenuListener(IServiceViewer viewer) {
		this.viewer = viewer;
	}

	public void dispose() {

		monitorRegistryAction = null;
		viewer = null;
	}

	protected void getAllNodeActions(Node[] nodes) {

		for (int i = 0; i < nodes.length; i++) {
			nodes[i].resetAvailableActions();
			// iterate over all extensions and gather node actions added by them
			IExtensionRegistry registry = Platform.getExtensionRegistry();
			IExtensionPoint extensionPoint = registry
			.getExtensionPoint(ServiceBrowserConstants.NODE_ACTION_EXTENSION_POINT);
			IConfigurationElement[] members = extensionPoint
			.getConfigurationElements();
			for (int m = 0; m < members.length; m++) {
				IConfigurationElement member = members[m];
				// IExtension extension = member.getDeclaringExtension();
				// String actionName =
				// member.getAttribute(FUNCTION_NAME_ATTRIBUTE);
				try {
					INodeActionExtensionPoint ext = (INodeActionExtensionPoint) member
					.createExecutableExtension("name");
					ext.addActions(nodes[i], getViewer());
				} catch (CoreException ex) {
					ServiceBrowserActivator
					.log("Could not determine all available actions for service",
							ex);
				}
			}

		}

	}

	protected IServiceViewer getViewer() {
		return viewer;
	}

	public void menuAboutToHide(final IMenuManager manager) {
		Display d = getViewer().getControl().getShell().getDisplay();
		if (d != null && !d.isDisposed() && toCleanUp != null) {
			// clean up items a bit later.. otherwise, the items can't execute
			// the actions anymore
			d.asyncExec(new Runnable() {
				public void run() {
					// clean up old items
					for (IContributionItem item : manager.getItems()) {
						if(toCleanUp.contains(item.getId()))
						{
							item.dispose();
							manager.remove(item);
						}
					}
					toCleanUp = null;

				}
			});

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.action.IMenuListener#menuAboutToShow(org.eclipse.jface
	 * .action.IMenuManager)
	 */
	public void menuAboutToShow(IMenuManager manager) {

		if(monitorRegistryAction == null)
		{
			// add new Registry action
			monitorRegistryAction = new MonitorRegistryAction(null);
			monitorRegistryAction.setViewer(getViewer());
			manager.add(monitorRegistryAction);

			manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		}

		// iterate over all selected nodes and determine the intersection of
		// allowed actions
		IStructuredSelection selection = (IStructuredSelection) getViewer()
		.getSelection();
		List<?> nodeList = selection.toList();
		int numNodes = nodeList.size();
		Node[] nodes = new Node[numNodes];
		nodeList.toArray(nodes);
		getAllNodeActions(nodes);

		Map<String, List<NodeAction>> availableActions = new ConcurrentHashMap<String, List<NodeAction>>();
		for (int i = 0; i < nodes.length; i++) {
			NodeAction[] actions = nodes[i].getAvailableActions().toArray(
					new NodeAction[0]);
			nodes[i].getAvailableActions().clear(); // clean up available
			// actions
			for (int j = 0; j < actions.length; j++) {

				// Check whether this action is available right now.
				if (!actions[j].isCurrentlyAvailable()) {
					continue;
				}

				List<NodeAction> intersectionSoFar = availableActions
				.get(actions[j].getClassName());

				if (intersectionSoFar == null) {
					if (i == 0) {
						intersectionSoFar = new ArrayList<NodeAction>();
						intersectionSoFar.add(actions[j]);
						availableActions.put(actions[j].getClassName(),
								intersectionSoFar);
					}
				} else {

					// is the action available for all selected nodes up to now?
					if (intersectionSoFar.size() == i) {
						intersectionSoFar.add(actions[j]);
					} else {
						availableActions.remove(actions[j].getClassName());
					}
				}
			}
		}

		toCleanUp = new HashSet<String>();

		// Create one MultiSelection Action for each action in the intersection
		// and add it to context menu
		List<MultiSelectionAction> multis = new ArrayList<MultiSelectionAction>();
		for (Iterator<List<NodeAction>> iter = availableActions.values()
				.iterator(); iter.hasNext();) {
			List<NodeAction> next = iter.next();
			if (next.size() == nodes.length) {
				String id = next.get(0).getClassName();

				MultiSelectionAction multi = new MultiSelectionAction(
						getViewer(), next.toArray(new NodeAction[next.size()]));
				multis.add(multi);
				multi.setId(id);
				toCleanUp.add(id);
			}
		}

		for (MultiSelectionAction multi : multis) {
			ActionContributionItem item = new ActionContributionItem(multi);
			item.setId(multi.getId());
			manager.add(item);
		}


	}

	protected void setViewer(IServiceViewer viewer) {
		this.viewer = viewer;
	}

}
