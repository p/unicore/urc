package de.fzj.unicore.rcp.servicebrowser.filters;

import java.util.Calendar;

import de.fzj.unicore.rcp.servicebrowser.nodes.JobNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * This filter is able to filter {@link JobNode} by certain criteria, most
 * notably submissionTime and terminationTime.
 * 
 *
 */
public abstract class TaskFilterCriteria extends NameFilterCriteria {

	public final static int int_noTime = 1;
	public final static int int_terminationTime = 2;
	public final static int int_submissionTime = 4;
	public final static int int_exact = 8;
	public final static int int_before = 16;
	public final static int int_after = 32;
	public final static int int_between = 64;

	protected int statusMode;
	protected int timeMode;
	protected String pattern;
	protected Calendar time1, time2;

	public TaskFilterCriteria(int status) {
		this(status, "");
	}

	public TaskFilterCriteria(int status, String pattern) {
		this(status, pattern, int_noTime, null, null);
	}

	public TaskFilterCriteria(int status, String pattern, int timeMode, Calendar time1,
			Calendar time2) {
		super(pattern);
		this.statusMode = status;
		this.pattern = pattern;
		this.timeMode = timeMode;
		this.time1 = time1;
		this.time2 = time2;
	}

	public int getStatusMode() {
		return statusMode;
	}

	public int getTimeMode() {
		return timeMode;
	}

	public String getPattern() {
		return pattern;
	}

	public Calendar getTime1() {
		return time1;
	}

	public Calendar getTime2() {
		return time2;
	}

	@Override
	public boolean fitCriteria(Node node) {
		return super.fitCriteria(node) && fitsNodeType(node)
				&& fitsTaskStatus(node) && fitsTime(node);

	}

	protected abstract boolean fitsNodeType(Node node);

	protected abstract boolean fitsTaskStatus(Node node);

	protected abstract Calendar getSubmissionTime(Node node);

	protected abstract Calendar getTerminationTime(Node node);

	protected boolean fitsTime(Node node) {

		Calendar comparisonTime;
		if (isSet(int_noTime, timeMode)) {
			return true;
		}

		if (isSet(int_submissionTime, timeMode)) {
			comparisonTime = getSubmissionTime(node);
		} else {
			comparisonTime = getTerminationTime(node);
		}

		if (isSet(int_after, timeMode)) {
			if (comparisonTime == null || !comparisonTime.after(time1)) {
				return false;
			}
		}

		if (isSet(int_before, timeMode)) {
			if (comparisonTime == null || !comparisonTime.before(time1)) {
				return false;
			}
		}

		if (isSet(int_exact, timeMode)) {
			if (comparisonTime == null || !(comparisonTime.after(time1)
					&& comparisonTime.before(time2))) {
				return false;
			}
		}

		if (isSet(int_between, timeMode)) {
			if (comparisonTime == null || !(comparisonTime.after(time1)
					&& comparisonTime.before(time2))) {
				return false;
			}
		}
		return true;
	}

	public static boolean isSet(int bit, int mode) {
		return (bit & mode) != 0;
	}

}
