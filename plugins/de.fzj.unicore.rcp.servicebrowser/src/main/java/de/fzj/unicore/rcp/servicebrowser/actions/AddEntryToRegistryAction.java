/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.net.URI;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.wsrflite.xmlbeans.client.RegistryClient;

/**
 * @author demuth
 * 
 */
public class AddEntryToRegistryAction extends NodeAction {

	private class URIValidator implements IInputValidator {

		public String isValid(String newText) {
			if (newText == null || newText.length() == 0) {
				return "An URI must not be empty!";
			}
			try {
				new URI(newText);
				return null;
			} catch (Exception e) {
				return e.getMessage();
			}
		}

	}

	public AddEntryToRegistryAction(Node node) {
		super(node);
		setText("Add Entry");
		setToolTipText("Manually add a service address to the selected registry.");

	}

	@Override
	public void run() {

		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {
				RegistryNode node = (RegistryNode) getNode();

				InputDialog in = new InputDialog(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell(),
						"Enter service address:",
						"Please enter the service's address.", node.getURI()
								.toString(), new URIValidator());

				if(in.open() == InputDialog.CANCEL) {
					return;
				}
				String newURI = in.getValue();

				if (newURI != null) {
					Node n = null;
					try {
						URI uri = new URI(newURI);
						n = NodeFactory.createNode(uri);
						RegistryClient client = new RegistryClient(node
								.getURI().toString(), node.getEpr(), node
								.getUASSecProps());
						client.addRegistryEntry(n.getEpr());
					} catch (Exception e) {
						// TODO: handle exception
					} finally {
						if (n != null) {
							n.dispose();
						}
					}

				}
			}

		});

	}

}
