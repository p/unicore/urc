/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.filters;

import java.util.Set;

import org.eclipse.jface.viewers.Viewer;

import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.RootNode;

/**
 * Filter that filters all but the provided node types from the model. Caution!
 * This filter should be one of the first filters in the filter stack.
 * 
 * @author demuth
 * 
 */
public class NodeTypeViewerFilter extends ConfigurableFilter {

	protected Set<String> interestingTypes;

	public NodeTypeViewerFilter(String ID, Set<String> interestingTypes) {
		super(ID, true);
		this.interestingTypes = interestingTypes;
	}

	public Set<String> getInterestingTypes() {
		return interestingTypes;
	}

	@Override
	public void resetFilters() {
		this.interestingTypes = null;
		super.resetFilters();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers
	 * .Viewer, java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		FilterCriteria fc = getGridFilter();
		if (!isActive() || element instanceof RootNode
				|| element instanceof GridNode || !(element instanceof Node)
				|| !(parentElement instanceof Node)
				|| (fc == null && interestingTypes == null)) {
			return true;
		}
		Node node = (Node) element;
		if (fc == null) {
			return interestingTypes.contains(node.getType());
		} else {
			boolean show = fc.showElement(node);
			return show;
		}
	}

	public void setInterestingTypes(Set<String> interestingTypes) {
		this.resetFilters();
		this.interestingTypes = interestingTypes;
	}
}