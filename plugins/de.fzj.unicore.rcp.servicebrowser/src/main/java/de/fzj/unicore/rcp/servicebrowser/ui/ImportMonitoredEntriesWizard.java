package de.fzj.unicore.rcp.servicebrowser.ui;

import java.net.URI;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.DialogSettings;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;

/**
 * Wizard for exporting a list of registry URLs and their names to an external
 * file.
 */
public class ImportMonitoredEntriesWizard extends Wizard implements
		IImportWizard {

	private ImportMonitoredEntriesWizardPage importPage = null;

	public ImportMonitoredEntriesWizard() {
		setNeedsProgressMonitor(true);
		setWindowTitle("Title");
	}

	@Override
	public void addPages() {
		importPage = new ImportMonitoredEntriesWizardPage();
		addPage(importPage);
	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// no initialization needed
	}

	/**
	 * Called when the user clicks finish. Saves the task data. Waits until all
	 * overwrite decisions have been made before starting to save files. If any
	 * overwrite is canceled, no files are saved and the user must adjust the
	 * dialog.
	 */
	@Override
	public boolean performFinish() {

		final String source = importPage.getSource(); // avoid invalid thread
														// access!
		Job job = new BackgroundJob("Importing bookmarks...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					IDialogSettings toLoad = new DialogSettings(null);
					toLoad.load(source);
					String[] namesToImport = toLoad
							.getArray(ServiceBrowserConstants.DIALOG_SETTINGS_MONITORED_ENTRY_NAMES);
					String[] eprsToImport = toLoad
							.getArray(ServiceBrowserConstants.DIALOG_SETTINGS_MONITORED_ENTRY_EPRS);
					String skippedURIs = null;
					for (int i = 0; i < eprsToImport.length; i++) {
						String name = namesToImport[i];
						String eprString = eprsToImport[i];
						EndpointReferenceType epr = EndpointReferenceType.Factory
								.parse(eprString);
						GridNode gn = ServiceBrowserActivator.getDefault()
								.getGridNode();
						Node n = NodeFactory.getNodeFor(new NodePath(gn
								.getURI(), new URI(epr.getAddress()
								.getStringValue())));
						if (n == null) {
							n = NodeFactory.createNode(epr);
							n.setName(name);
							gn.addBookmark(n);
						} else {

							String uri = epr.getAddress().getStringValue();
							if (skippedURIs == null) {
								skippedURIs = "";
							} else {
								skippedURIs += ",\n";
							}
							skippedURIs += uri;
						}
					}

					if (skippedURIs != null) {

						ServiceBrowserActivator.log(IStatus.WARNING,
								"Some bookmarks were not imported as they seem to exist already:\n"
										+ skippedURIs);
					}
				} catch (Exception e) {
					ServiceBrowserActivator.log(IStatus.ERROR, "Problem while trying to import bookmarks", e);
				}
				return Status.OK_STATUS;
			}
		};
		job.schedule();
		return true;
	}
}
