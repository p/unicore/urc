/*********************************************************************************
 * Copyright (c) 2015 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.metadata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.fzj.unicore.rcp.common.detailsView.KeyValue;
import de.fzj.unicore.rcp.common.detailsView.KeyValueList;
import de.fzj.unicore.rcp.servicebrowser.metadata.MetadataViewPart.MDContentProvider;
import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;
import de.fzj.unicore.uas.client.MetadataClient;
import de.fzj.unicore.wsrflite.xfire.ClientException;

/**
 * @author bjoernh
 *
 *         09.04.2015 10:41:55
 *
 */
public class FileMetadataAdapter {

	private MetadataClient mdClient;
	private String path;
	private KeyValueList kv;

	private List<MDContentProvider> changeListeners = new ArrayList<MDContentProvider>();

	/**
	 * @throws Exception
	 * @throws ClientException
	 * 
	 */
	public FileMetadataAdapter(FileNode _fileNode) throws ClientException,
			Exception {
		this.mdClient = _fileNode.getStorageClient().getMetadataClient();
		this.path = _fileNode.getGridFileType().getPath();
		// get the metadata already now
		getMetadata();
	}

	public Object[] getMetadata() {
		try {
			if (kv == null) { // only update from md service the first time
								// round
				Map<String, String> md = mdClient.getMetadata(this.path);
				kv = new KeyValueList(md);
			}
			Collections.sort(kv.getKeyValues(), new Comparator<KeyValue>() {

				@Override
				public int compare(KeyValue o1, KeyValue o2) {
					return o1.getKey().toLowerCase()
							.compareTo(o2.getKey().toLowerCase());
				}

			});
			return kv.getKeyValues().toArray();
		} catch (Exception e) {
			return new Object[0];
		}
	}

	/**
	 * @param mdContentProvider
	 */
	public void removeChangeListener(MDContentProvider mdContentProvider) {
		changeListeners.remove(mdContentProvider);

	}

	/**
	 * @param mdContentProvider
	 */
	public void addChangeListener(MDContentProvider mdContentProvider) {
		changeListeners.add(mdContentProvider);

	}

	/**
	 * 
	 */
	public void commit() {
		try {
			// the server side merges, but we want to write what
			// is displayed in the GUI
			// TODO refine and rethink whether this is appropriate
			// Right now, there's no other way to really delete keys
			// once introduced
			mdClient.deleteMetadata(path);
			mdClient.updateMetadata(path, kvToMap());
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
	}

	/**
	 * @return
	 */
	private Map<String, String> kvToMap() {
		Map<String, String> map = new HashMap<String, String>();

		for (KeyValue k : kv.getKeyValues()) {
			map.put(k.getKey(), k.getValue());
		}
		return map;
	}

	/**
	 * 
	 */
	public void addKeyValue() {
		kv.getKeyValues().add(new KeyValue("New Key", "New Value"));
		for (MDContentProvider mdContentProvider : changeListeners) {
			mdContentProvider.inputChanged(null, this, this);
		}
	}

	/**
	 * @param tbDel
	 */
	public void removeKeyValue(KeyValue tbDel) {
		kv.removeKeyValue(tbDel);
		for (MDContentProvider mdContentProvider : changeListeners) {
			mdContentProvider.inputChanged(null, this, this);
		}
	}

	/**
	 * 
	 */
	public void reset() {
		kv = null;
	}

}
