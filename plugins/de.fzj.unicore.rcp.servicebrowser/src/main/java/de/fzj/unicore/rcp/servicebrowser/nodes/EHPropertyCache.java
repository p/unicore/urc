package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.ConfigurationFactory;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;

public class EHPropertyCache implements IPropertyCache {

	protected transient Cache storage;

	private static final String CACHE_NAME = EHPropertyCache.class.getName();
			
	public EHPropertyCache() {
		super();
		// try to shutdown orderly when the JVM is shut down
		System.setProperty("net.sf.ehcache.enableShutdownHook", "true"); 
	}

	public void clearAllProperties() {
		getStorage().removeAll();
	}

	public void clearPropertiesFor(INodeData data) {
		if (getStorage().get(getKeyForData(data)) != null) {
			try
			{
				getStorage().remove(getKeyForData(data));	
			} catch (Exception e)
			{
				// do nothing
			}
		}

	}

	public void clearPropertiesFor(String key) {
		getStorage().remove(key);
	}

	public void flushToDisk() throws IOException {
		if (storage != null) {
			System.out.println("Storing " + storage.calculateInMemorySize()
					/ 1000 + " Kbyte of node properties");
			storage.flush();
		}
	}

	protected String getKeyForData(INodeData data) {
		try {
			return NodeDataCache.getKey(data.getEpr());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	protected String getPersistencePath() {
		IPath path = ServiceBrowserActivator.getDefault().getPersistenceRoot();
		File parent = path.toFile();
		if (!parent.exists()) {
			boolean created = parent.mkdir();
			if (!created) {
				ServiceBrowserActivator.log(
						IStatus.ERROR,
						"Could not create Grid data cache.",
						new Exception("Unable to create directory "
								+ parent.getAbsolutePath()));
				return null;
			}
		}
		return parent.getAbsolutePath();

	}

	@SuppressWarnings("unchecked")
	public Object getProperty(INodeData data, String prop) {
		String key = getKeyForData(data);
		Element e = null;
		try {
			e = getStorage().get(key);
		} catch (Exception e1) {
			getStorage().remove(key);
		}

		if (e == null) {
			return null;
		} else if (!e.getKey().equals(key)) {
			// the db has just returned a misfitting entry
			// shouldn't happen but HAS HAPPENED
			// try to clean up this entry
			getStorage().remove(key);
			return null;
		}
		Map<String, Object> properties = (Map<String, Object>) e
				.getObjectValue();
		Object result = properties == null ? null : properties.get(prop);
		return result;
	}

	protected synchronized Cache getStorage() {
		if (storage == null) {
			URL configLocation = FileLocator.find(ServiceBrowserActivator
					.getDefault().getBundle(), new Path("ehcache.xml"), null);
			Configuration config = ConfigurationFactory
					.parseConfiguration(configLocation);
			config.getDiskStoreConfiguration().setPath(getPersistencePath());
			config.setName(CACHE_NAME);
			
			// Create a CacheManager using defaults
			CacheManager manager = CacheManager.getCacheManager(CACHE_NAME);
			if(manager == null){
				manager = new CacheManager(config);
			}
			
			storage = manager.getCache("Node properties");
			if (storage == null) {
				manager.addCache("Node properties");
				storage = manager.getCache("Node properties");
			}
			storage.flush(); // re-write index as it gets deleted by ehcache on
								// startup (if we skip this, shutting the client
								// uncleanly will cause data loss)
		}
		return storage;
	}

	@SuppressWarnings("unchecked")
	public void setProperty(INodeData data, String key, Object value) {
		Element e = getStorage().get(getKeyForData(data));
		Map<String, Object> properties = e == null ? new HashMap<String, Object>()
				: (Map<String, Object>) e.getObjectValue();
		if (properties == null) {
			properties = new HashMap<String, Object>();
		}
		properties.put(key, value);
		e = new Element(getKeyForData(data), properties);
		getStorage().put(e);

	}
}
