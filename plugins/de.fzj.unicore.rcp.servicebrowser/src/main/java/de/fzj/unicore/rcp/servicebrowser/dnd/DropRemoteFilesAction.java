package de.fzj.unicore.rcp.servicebrowser.dnd;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;

import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.part.IDropActionDelegate;
import org.unigrids.services.atomic.types.GridFileType;

import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.rcp.servicebrowser.utils.RemoteFileUtils;

public class DropRemoteFilesAction implements IDropActionDelegate {

	protected boolean dowloadToClient(Object data, Object target) {
		String localPath = null;
		IResource resource = null;

		if (target instanceof IProjectNature) {
			target = ((IProjectNature) target).getProject();
		}

		if (target instanceof IResource) {
			resource = (IResource) target;
			localPath = resource.getLocation().toOSString();
		} else if (target instanceof String) {
			localPath = (String) target;
		} else if (target instanceof IAdaptable) {
			target = ((IAdaptable) target).getAdapter(IResource.class);
			if (target != null && target instanceof IResource) {
				resource = (IResource) target;
				localPath = resource.getLocation().toOSString();
			}
		}

		if (localPath != null) {

			byte[] bytes = (byte[]) data;

			// get the sources
			final String[] sources = (new String(bytes)).split(",");
			final IResource toRefresh = resource;
			final File parent = new File(localPath);

			Job j = new BackgroundJob("Downloading file") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					for (String source : sources) {
						try {
							URI uri = new URI(source);
							AbstractFileNode node = (AbstractFileNode) NodeFactory
									.createNode(uri);
							try {
								if (node.getName() == null) {
									node.retrieveName();
								}
								File file = new File(parent, node.getName());
								if (!file.exists()) {
									final GridFileType gft = node
											.getGridFileType();
									if (gft == null) {
										return Status.CANCEL_STATUS;
									}
									if (gft.getIsDirectory()) {
										UnicoreStorageTools.downloadFolder(
												node.getStorageClient(),
												gft.getPath(), file, monitor);
									} else {
										FileOutputStream fos = new FileOutputStream(
												file);
										long size = gft.getSize();
										UnicoreStorageTools.download(
												node.getStorageClient(),
												gft.getPath(), fos, size,
												monitor);
									}
								}

							} finally {
								node.dispose();
							}
							if (toRefresh != null) {
								toRefresh.refreshLocal(
										IResource.DEPTH_INFINITE, null);
							}
						} catch (Exception e) {
							ServiceBrowserActivator.log(IStatus.ERROR,
									"Unable to create temp file for transferring dragged data", e);
						}
					}
					return Status.OK_STATUS;
				}
			};
			j.schedule();
			return true;
		}
		return false;
	}

	public boolean run(final Object data, final Object target) {
		if (target instanceof FolderNode || target instanceof StorageNode) {
			serverToServer(data, target);
			return true;
		} else {
			return dowloadToClient(data, target);
		}
	}

	protected void serverToServer(final Object data, final Object target) {

		Job j = new Job("transferring files") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {

				try {

					byte[] bytes = (byte[]) data;
					// get the sources
					String[] sources = (new String(bytes)).split(",");
					RemoteFileUtils.serverToServerFileTransfer(sources,
							(Node) target, true, monitor);
				} catch (Exception e) {
					ServiceBrowserActivator.log(IStatus.ERROR, "Unable to transfer file", e);
				}
				return Status.OK_STATUS;
			}
		};
		j.schedule();
	}
}
