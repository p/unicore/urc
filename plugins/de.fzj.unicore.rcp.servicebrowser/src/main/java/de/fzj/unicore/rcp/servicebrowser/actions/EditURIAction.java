/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.net.URI;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.ui.PlatformUI;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * @author demuth, rajveer
 * 
 */
public class EditURIAction extends NodeAction {

	private URI oldURI;
	private URI oldFallbackURI;

	public EditURIAction(Node node) {
		super(node);
		setText("Edit Address");
		setToolTipText("Edit the address of the selected Item");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("editURI.png"));		

	}

	@Override
	public void run() {

		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {			

			public void run() {
				oldURI = getNode().getURI();
				oldFallbackURI = getNode().getFallbackURI();
				EditRegistryUrlDialog editRegistryDialog = new EditRegistryUrlDialog(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), SWT.APPLICATION_MODAL);
				editRegistryDialog.setUrl(oldURI.toString());
				if(oldFallbackURI != null) {
					editRegistryDialog.setFbUrl(oldFallbackURI.toString());
				}

				int returnCode = editRegistryDialog.open();
				if (returnCode == Dialog.CANCEL) {
					return;
				}

				final EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
				epr.addNewAddress().setStringValue(editRegistryDialog.getUrl());
				getNode().setEpr(epr);

				final EndpointReferenceType fbEpr = EndpointReferenceType.Factory.newInstance();
				fbEpr.addNewAddress().setStringValue(editRegistryDialog.getFbUrl());
				getNode().setFallbackEpr(fbEpr);
			}

		});

		getViewer().refresh(getNode());
		
	}
	
}
