/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

package de.fzj.unicore.rcp.servicebrowser.filters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

/**
 * provides the configuration GUI for the JobFilter
 * 
 * @author Christian Hohmann
 * 
 */

public class UserDefinedJobFilterConfiguration extends TaskFilterConfiguration{


	private Button checkUndefinedJobs;
	private Button checkQueuedJobs;
	private Button checkStaginginJobs;
	private Button checkStagingoutJobs;
	private Button checkFailedJobs;
	private Button checkReadyJobs;
	private Button checkRunningJobs;
	private Button checkSucceededJobs;



	public UserDefinedJobFilterConfiguration() {
	}


	@Override
	protected void createStatusButtons(Composite parent) {
		checkFailedJobs = new Button(parent, SWT.CHECK);
		checkFailedJobs.setText("Failed jobs");
		checkFailedJobs.setSelection(inStatusMode(JobFilterCriteria.int_failed));
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.minimumWidth = 50;
		gd.minimumHeight = 50;
		checkFailedJobs.setLayoutData(gd);
		
		checkReadyJobs = new Button(parent, SWT.CHECK);
		checkReadyJobs.setText("Ready jobs");
		checkReadyJobs.setSelection(inStatusMode(JobFilterCriteria.int_ready));
		checkReadyJobs.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		checkRunningJobs = new Button(parent, SWT.CHECK);
		checkRunningJobs.setText("Running jobs");
		checkRunningJobs.setSelection(inStatusMode(JobFilterCriteria.int_running));
		checkRunningJobs.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		checkSucceededJobs = new Button(parent, SWT.CHECK);
		checkSucceededJobs.setText("Succeeded jobs");
		checkSucceededJobs.setSelection(inStatusMode(JobFilterCriteria.int_successful));
		checkSucceededJobs.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		checkUndefinedJobs = new Button(parent, SWT.CHECK);
		checkUndefinedJobs.setText("Undefined jobs");
		checkUndefinedJobs.setSelection(inStatusMode(JobFilterCriteria.int_undefined));
		checkUndefinedJobs.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		checkQueuedJobs = new Button(parent, SWT.CHECK);
		checkQueuedJobs.setText("Queued jobs");
		checkQueuedJobs.setSelection(inStatusMode(JobFilterCriteria.int_queued));
		checkQueuedJobs.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		checkStaginginJobs = new Button(parent, SWT.CHECK);
		checkStaginginJobs.setText("Staging in jobs");
		checkStaginginJobs.setSelection(inStatusMode(JobFilterCriteria.int_stagein));
		checkStaginginJobs.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		checkStagingoutJobs = new Button(parent, SWT.CHECK);
		checkStagingoutJobs.setText("Staging out jobs");
		checkStagingoutJobs.setSelection(inStatusMode(JobFilterCriteria.int_stageout));
		checkStagingoutJobs.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	}
		
	


	@Override
	protected void deselectAllStatusWidgets() {
		checkFailedJobs.setSelection(false);
		checkReadyJobs.setSelection(false);
		checkRunningJobs.setSelection(false);
		checkSucceededJobs.setSelection(false);
		checkUndefinedJobs.setSelection(false);
		checkQueuedJobs.setSelection(false);
		checkStaginginJobs.setSelection(false);
		checkStagingoutJobs.setSelection(false);
	}




	@Override
	protected TaskFilterCriteria doCreateTaskFilter() {
		return new JobFilterCriteria(statusMode, pattern, timeMode, time1, time2);
	}




	@Override
	protected int getDefaultStatusMode() {
		return JobFilterCriteria.int_all;
	}


	protected void okPressed()
	{
		statusMode = 0;
		statusMode |= checkFailedJobs.getSelection() ? JobFilterCriteria.int_failed : 0;
		statusMode |= checkReadyJobs.getSelection() ? JobFilterCriteria.int_ready : 0;
		statusMode |= checkRunningJobs.getSelection() ? JobFilterCriteria.int_running : 0;
		statusMode |= checkSucceededJobs.getSelection() ? JobFilterCriteria.int_successful : 0;
		statusMode |= checkUndefinedJobs.getSelection() ? JobFilterCriteria.int_undefined : 0;
		statusMode |= checkQueuedJobs.getSelection() ? JobFilterCriteria.int_queued : 0;
		statusMode |= checkStaginginJobs.getSelection() ? JobFilterCriteria.int_stagein : 0;
		statusMode |= checkStagingoutJobs.getSelection() ? JobFilterCriteria.int_stageout : 0;
		super.okPressed();

	}


	@Override
	protected void selectAllStatusWidgets() {
		checkFailedJobs.setSelection(true);
		checkReadyJobs.setSelection(true);
		checkRunningJobs.setSelection(true);
		checkSucceededJobs.setSelection(true);
		checkUndefinedJobs.setSelection(true);
		checkQueuedJobs.setSelection(true);
		checkStaginginJobs.setSelection(true);
		checkStagingoutJobs.setSelection(true);
	}
}