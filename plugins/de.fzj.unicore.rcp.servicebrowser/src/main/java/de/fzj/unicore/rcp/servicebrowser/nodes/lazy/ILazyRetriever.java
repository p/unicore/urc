package de.fzj.unicore.rcp.servicebrowser.nodes.lazy;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * This interface is used in conjunction with the {@link ILazyParent}.
 * Implementing classes allow for retrieving child nodes of the
 * {@link ILazyParent} in a windowed fashion.
 * 
 * @author bdemuth
 * 
 */
public interface ILazyRetriever {

	/**
	 * Returns the total number of children or -1 if this number is not known
	 * beforehand.
	 * 
	 * @return
	 */
	public long getNumChildren(ILazyParent n, IProgressMonitor progress);

	/**
	 * Retrieve a certain number children for the {@link ILazyParent}, starting
	 * at a given offset.
	 * 
	 * @param n
	 *            the {@link ILazyParent}
	 * @param offset
	 *            the index of the first child to be retrieved
	 * @param windowSize
	 *            the number of nodes in the result list
	 * @return
	 */
	public List<Node> retrieveChildren(ILazyParent n, long offset,
			long windowSize, IProgressMonitor progress);

}
