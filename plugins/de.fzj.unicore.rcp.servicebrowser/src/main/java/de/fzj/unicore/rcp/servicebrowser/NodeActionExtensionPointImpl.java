package de.fzj.unicore.rcp.servicebrowser;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;

import de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeActionExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

public class NodeActionExtensionPointImpl implements INodeActionExtensionPoint {

	public void addActions(Node node, IServiceViewer viewer) {

	}

	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {

	}

}
