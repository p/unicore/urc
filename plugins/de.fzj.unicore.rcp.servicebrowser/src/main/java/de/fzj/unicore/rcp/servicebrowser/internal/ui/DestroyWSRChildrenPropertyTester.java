package de.fzj.unicore.rcp.servicebrowser.internal.ui;

import java.util.List;

import org.eclipse.core.expressions.PropertyTester;

import de.fzj.unicore.rcp.servicebrowser.nodes.JobNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.GroupNode;

public class DestroyWSRChildrenPropertyTester extends PropertyTester {

	public DestroyWSRChildrenPropertyTester() {
		super();
	}

	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		if("canChildrenBeDestroyed".equalsIgnoreCase(property))
		{
			if(!(receiver instanceof Node)) return false;
			Node n = (Node) receiver;
			for (Node o : n.getChildrenArray()) {
				if (o instanceof WSRFNode) {
					WSRFNode wsr = (WSRFNode) o;
					if(!wsr.canBeDestroyed()) return false;
				}
			}
			return true;
		}
		else if("isJobGroup".equalsIgnoreCase(property))
		{
			if (receiver instanceof GroupNode) {
				GroupNode group = (GroupNode) receiver;
				List<Node> children = group.getChildren();
				if (children != null && children.size() > 0
						&& (JobNode.TYPE.equals(children.get(0).getType()))) {
					return true;
				}
			}
		}
		return false;
	}

}
