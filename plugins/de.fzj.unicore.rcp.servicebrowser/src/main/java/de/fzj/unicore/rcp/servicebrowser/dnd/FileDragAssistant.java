package de.fzj.unicore.rcp.servicebrowser.dnd;

import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.ui.part.PluginTransfer;
import org.eclipse.ui.part.PluginTransferData;
import org.eclipse.ui.part.ResourceTransfer;

import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IDragAssistant;
import de.fzj.unicore.rcp.servicebrowser.utils.RemoteFileUtils;

public class FileDragAssistant implements IDragAssistant {

	private static final Transfer[] SUPPORTED_DROP_TRANSFERS = new Transfer[] { PluginTransfer
			.getInstance() };

	public void dragFinished(DragSourceEvent event, Object[] sources) {
		// do nothing
	}

	public boolean dragSetData(final DragSourceEvent event,
			final Object[] sources) {

		if (PluginTransfer.getInstance().isSupportedType(event.dataType)) {
			PluginTransferData data = RemoteFileUtils
					.fileArrayToPluginTransferData(sources);
			if (data == null) {
				return false;
			}
			event.data = data;
			return true;
		} else {
			return false;
		}
	}

	public Transfer[] getSupportedTransferTypes() {
		return SUPPORTED_DROP_TRANSFERS;
	}

	public TransferData[] getTransferDataFor(Object source) {
		return ResourceTransfer.getInstance().getSupportedTypes();
	}

	public boolean supportsTransferData(TransferData td) {
		for (Transfer t : getSupportedTransferTypes()) {
			if (t.isSupportedType(td)) {
				return true;
			}
		}
		return false;
	}

}
