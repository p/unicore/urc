package de.fzj.unicore.rcp.servicebrowser;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import javax.swing.ImageIcon;

import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.PlatformUI;
import org.oasisOpen.docs.wsrf.rl2.CurrentTimeDocument;
import org.oasisOpen.docs.wsrf.rl2.TerminationTimeDocument;
import org.oasisOpen.docs.wsrf.rp2.GetResourcePropertyDocumentResponseDocument.GetResourcePropertyDocumentResponse;
import org.w3.x2005.x08.addressing.EndpointReferenceType;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import de.fzj.unicore.rcp.common.UAS;
import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;

/**
 * 
 * @author hmersch
 */
public class Util {

	/**
	 * Convert SWTs ImageData to SWING ImageIcon This is used for the SWING
	 * Panels for loading Icons
	 * 
	 * @param data
	 *            ImageData by SWT (like from Activtor.getImageDescriptor()
	 * 
	 *            taken from: http://www.java2s.com/Code/Java/SWT-JFace-Eclipse/
	 *            ConvertbetweenSWTImageandAWTBufferedImage.htm
	 */
	public static ImageIcon convertToSwing(ImageData data) {
		ColorModel colorModel = null;
		PaletteData palette = data.palette;
		if (palette.isDirect) {
			colorModel = new DirectColorModel(data.depth, palette.redMask,
					palette.greenMask, palette.blueMask);
			BufferedImage bufferedImage = new BufferedImage(colorModel,
					colorModel.createCompatibleWritableRaster(data.width,
							data.height), false, null);
			WritableRaster raster = bufferedImage.getRaster();
			int[] pixelArray = new int[3];
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					int pixel = data.getPixel(x, y);
					RGB rgb = palette.getRGB(pixel);
					pixelArray[0] = rgb.red;
					pixelArray[1] = rgb.green;
					pixelArray[2] = rgb.blue;
					raster.setPixels(x, y, 1, 1, pixelArray);
				}
			}
			return new ImageIcon(bufferedImage);
		} else {
			RGB[] rgbs = palette.getRGBs();
			byte[] red = new byte[rgbs.length];
			byte[] green = new byte[rgbs.length];
			byte[] blue = new byte[rgbs.length];
			for (int i = 0; i < rgbs.length; i++) {
				RGB rgb = rgbs[i];
				red[i] = (byte) rgb.red;
				green[i] = (byte) rgb.green;
				blue[i] = (byte) rgb.blue;
			}
			if (data.transparentPixel != -1) {
				colorModel = new IndexColorModel(data.depth, rgbs.length, red,
						green, blue, data.transparentPixel);
			} else {
				colorModel = new IndexColorModel(data.depth, rgbs.length, red,
						green, blue);
			}
			BufferedImage bufferedImage = new BufferedImage(colorModel,
					colorModel.createCompatibleWritableRaster(data.width,
							data.height), false, null);
			WritableRaster raster = bufferedImage.getRaster();
			int[] pixelArray = new int[1];
			for (int y = 0; y < data.height; y++) {
				for (int x = 0; x < data.width; x++) {
					int pixel = data.getPixel(x, y);
					pixelArray[0] = pixel;
					raster.setPixel(x, y, pixelArray);
				}
			}
			return new ImageIcon(bufferedImage);
		}
	}

	// static String constructDetailArea(WebServiceNode selectedItem) {
	// String details = "";
	// details = details.concat("Name: "+selectedItem.toString()+"\n");
	// if (selectedItem.getURI() != null) {details =
	// details.concat("URL: "+selectedItem.getURI().toString());}
	// if(selectedItem.getState() ==
	// de.fzj.unicore.rcp.servicebrowser.nodes.Node.STATE_FAILED) {
	// details =
	// details.concat("\nFailed Reason: "+selectedItem.getFailReason());
	// }
	// details = details.concat(selectedItem.getDetails());
	// return details;
	// }

	/**
	 * loads an image as Icon
	 */
	public static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = Util.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL);

		} else {
			return null;
		}
	}

	/**
	 * Determine the type of a URI
	 * 
	 * @param uri
	 *            to parse
	 * @return the node type or null if none could be determined
	 */
	public static String determineTypeOfURI(URI uri) {
		if (uri == null) {
			return null;
		} else if (GridNode.getPortType().toString().equals(uri.toString())) {
			return uri.toString();
		}
		// case file transfer ...we have an extra Scheme (not http/https)
		try {
			if (uri.toString().matches(".*/" + UAS.SMS + "\\?res=.*#.*")) {
				return FileNode.TYPE;
			}
			URL url = uri.toURL();
			return determineTypeOfURL(url);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Determine the type of a URL (TSS, TSK, Registry....)
	 * 
	 * @param url
	 *            to parse
	 * @return the Service type or null if none could be determined
	 */
	public static String determineTypeOfURL(URL url) {
		if (url == null) {
			return null;
		}
		return url.getPath().replaceFirst(".*/services/", "");
	}

	/**
	 * creates a XmlBeans WS-Addressing EPR Object from given URI
	 */
	public static EndpointReferenceType eprFromURI(URI uri)
			throws MalformedURLException {
		URL url = new URL(uri.toString());
		return eprFromURL(url);
	}

	/**
	 * creates a XmlBeans WS-Addressing EPR Object from given URL
	 */
	public static EndpointReferenceType eprFromURL(URL url) {
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue(url.toString());
		return epr;
	}

	public static URL getContainerFromServiceUrl(String url)
			throws MalformedURLException {
		String container = url.replaceFirst("services/.*", "services/");
		return new URL(container);
	}

	public static URL getContainerFromServiceUrl(URL url, String serviceType)
			throws MalformedURLException {
		String urlString = url.toString().replace(serviceType, "");
		return new URL(urlString);
	}

	/**
	 * retireves a 2nd level node of given type Useful for CurrentTime and
	 * Termination time
	 */
	public static Node getContentNode(String type,
			GetResourcePropertyDocumentResponse dt) {

		Node n = dt.getDomNode();
		Node c = null;

		NodeList l = n.getChildNodes();
		for (int i = 0; i < l.getLength(); i++) {
			c = l.item(i);
			NodeList l2 = c.getChildNodes();
			Node c2 = null;
			for (int j = 0; j < l2.getLength(); j++) {
				c2 = l2.item(j);
				if (c2.getNodeType() == Node.ELEMENT_NODE
						&& c2.getLocalName().equalsIgnoreCase(type)) {
					return c2;

				}
			}
			// }

		}
		return null;
	}

	/**
	 * Get the TerminationTimeDocument from a PropertyDocumentResponse
	 * 
	 * @return XmlObject
	 */
	public static CurrentTimeDocument getCurrentTimeDocument(
			GetResourcePropertyDocumentResponse dt) {
		Node ct = getContentNode("CurrentTime", dt);
		if (ct == null) {
			return null;
		}
		try {
			CurrentTimeDocument ctd = CurrentTimeDocument.Factory.parse(ct);
			return ctd;
		} catch (org.apache.xmlbeans.XmlException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * returns the pure ID (res=...) from a EPR
	 */
	public static String getIDFromEpr(
			org.w3.x2005.x08.addressing.EndpointReferenceType epr)
			throws MalformedURLException {
		URL url = new URL(epr.getAddress().getStringValue());
		return getIDFromURL(url);
	}

	/**
	 * returns the pure ID (res=...) from a URL
	 */
	public static String getIDFromURL(URL url) {
		return url.getQuery().replace("res=", "");
	}

	/**
	 * Determine name to show up for User If it has a ID, this will be used,
	 * otherwise the url itself
	 * 
	 * @param url
	 * @return
	 */
	public static String getNameFromURL(URL url) {
		return url.getHost();
	}

	public static String getServiceNameFromUri(URI uri) {
		String s = uri.toString();
		if (uri.getQuery() != null) {
			s = s.replace(uri.getQuery(), "");
		}
		String[] parts = s.split("/");
		return parts[parts.length - 1];
	}

	/**
	 * Get the TerminationTimeDocument from a PropertyDocumentResponse This
	 * avoids to contact serveru nness. often.
	 * 
	 * @return XmlObject
	 */
	public static TerminationTimeDocument getTerminationTimeDocument(
			GetResourcePropertyDocumentResponse dt) {
		Node tt = getContentNode("TerminationTime", dt);
		if (tt == null) {
			return null;
		}
		try {
			TerminationTimeDocument ttd = TerminationTimeDocument.Factory
					.parse(tt);
			return ttd;
		} catch (org.apache.xmlbeans.XmlException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static IViewPart getView(String id) {
		IViewReference viewReferences[] = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getViewReferences();
		for (int i = 0; i < viewReferences.length; i++) {
			if (id.equals(viewReferences[i].getId())) {
				return viewReferences[i].getView(false);
			}
		}
		return null;
	}

	/**
	 * Determine if a URL has NO reference, this means no query. This would
	 * mean, that a WS is NO WS-RF
	 * 
	 * @param url
	 *            the URL to check
	 * @return true if url has NO ref
	 */
	public static boolean hasNoRef(URL url) {
		if (url.getQuery() != null) {
			return false;
		}
		return true;
	}

	static URL makeTSSUrlFromServiceContainer(String containerURL, String id)
			throws MalformedURLException {
		return new URL(containerURL + UAS.TSS + "?res=" + id);
	}
}
