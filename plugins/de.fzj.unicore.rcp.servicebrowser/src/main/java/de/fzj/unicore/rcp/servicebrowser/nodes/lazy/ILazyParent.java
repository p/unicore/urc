package de.fzj.unicore.rcp.servicebrowser.nodes.lazy;

import java.net.URI;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;

/**
 * This class is used to retrieve a (possibly large) number of child nodes
 * successively. It can have a child node that is used to represent additional
 * nodes after the currently shown nodes. This child node is also used to
 * trigger retrieval of more child nodes. Note that node retrieval is considered
 * an expensive operation and MUST be called by a background thread!
 * 
 * @author demuth
 * 
 */
public interface ILazyParent {

	/**
	 * Returns the index of the last child node that is currently displayed
	 * 
	 * @return
	 */
	public long getCurrentChildIndex();

	/**
	 * Returns the number of children that can be scrolled through node or -1 if
	 * this number is not known beforehand
	 * 
	 * @return
	 */
	public long getNumLazyChildren(IProgressMonitor progress);

	/**
	 * Retrieve and show nodes until the last child node becomes visible
	 * 
	 * @param progress
	 * @return
	 */
	public IStatus lazilyRetrieveAllChildren(IProgressMonitor progress);

	/**
	 * Tell the node to show all child nodes up to the child with the given URI.
	 * Not that for performance reasons, the lazy node is actually allowed to
	 * retrieve more child nodes than necessary, but it will continue to
	 * retrieve children until it finds a child node with the given URI.
	 * 
	 * @param index
	 */
	public IStatus lazilyRetrieveChildrenUpTo(URI uri, IProgressMonitor progress);

	/**
	 * Tell the node to show all child nodes up to the given position.
	 * 
	 * @param index
	 */
	public IStatus lazilyRetrieveChildrenUpToIndex(long index,
			IProgressMonitor progress);

	public IStatus lazilyRetrieveNextChildren(IProgressMonitor progress);

}
