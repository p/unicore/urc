/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.ui;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.navigator.CommonDragAdapter;
import org.eclipse.ui.navigator.CommonDropAdapter;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.adapters.URIAdapter;
import de.fzj.unicore.rcp.common.utils.SortingMenuManager;
import de.fzj.unicore.rcp.servicebrowser.ContextMenuListener;
import de.fzj.unicore.rcp.servicebrowser.IServiceContentProvider;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceLabelProvider;
import de.fzj.unicore.rcp.servicebrowser.actions.DoubleClickAction;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.dnd.ServiceDragger;
import de.fzj.unicore.rcp.servicebrowser.dnd.ServiceDropper;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.RootNode;

/**
 * @author demuth
 * 
 */
public class ServiceTreeViewer extends TreeViewer implements IServiceViewer {

	protected int userMode;
	protected boolean showingContextMenu;
	private NodeAction doubleClickAction;
	private boolean doubleClickEnabled = true;
	private MenuManager menuManager;
	protected ContextMenuListener contextMenuListener;
	protected DragSource dragSource; 
	// use your own DragSource that is updated
	// when a different node gets selected
	protected ISelection formerSelection;

	protected AsyncQueueExecuter asyncQueueExecutor = new AsyncQueueExecuter();

	/**
	 * The root node of the grid model. Serves as entry point to all grid
	 * Services.
	 */
	GridNode gridNode;

	/**
	 * @param parent
	 */
	public ServiceTreeViewer(Composite parent) {
		super(parent);
		init();

	}

	/**
	 * @param parent
	 * @param style
	 */
	public ServiceTreeViewer(Composite parent, int style) {
		super(parent, style);
		init();
	}

	public void addMonitoringEntry(Node newNode) {
		if (newNode.isTopLevel()) {
			ServiceBrowserActivator
			.log(IStatus.ERROR,
			"Not adding bookmark: a bookmark for this URI does already exist.");
			return;
		}

		try {
			getGridNode().addBookmark(newNode);
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.ERROR, "Not adding bookmark", e);
		}

		// persistence
		saveGrid();
		if (!getGridNode().isExpanded()) {
			asyncExec(new Runnable()
			{
				public void run()
				{
					expandToLevel(getGridNode(), 0);
					getGridNode().setExpanded(true);
				}
			});
		}
	}

	@Override
	protected boolean checkBusy() {
		return isBusy(); // do not log when viewer is busy
	}

	public void dispose() {
		
	}

	public void fireSelectionChanged() {
		setSelection(getSelection());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.StructuredViewer#getFilteredChildren(java.lang
	 * .Object)
	 */
	public Object[] getAllFilteredChildren(Object parentElementOrTreePath) {
		return getFilteredChildren(parentElementOrTreePath);
	}

	/**
	 * Returns the dialog settings object used to maintain state between dialogs
	 * 
	 * @return the dialog settings to be used
	 */
	private IDialogSettings getDialogSettings() {
		IDialogSettings settings = ServiceBrowserActivator.getDefault()
		.getDialogSettings();
		IDialogSettings mySettings = settings.getSection(getClass().getName());
		if (mySettings == null) {
			mySettings = settings.addNewSection(getClass().getName());
		}
		return mySettings;
	}

	@Override
	protected boolean getExpanded(Item item) {
		if (item.getData() == null || !(item.getData() instanceof Node)) {
			return super.getExpanded(item);
		}
		return ((Node) item.getData()).isExpanded();
	}

	@Override
	protected Object[] getFilteredChildren(Object parent) {
		return super.getFilteredChildren(parent);
	}

	public GridNode getGridNode() {
		return gridNode;
	}

	public List<IAdaptable> getSelectedNodes() {
		IStructuredSelection selection = (IStructuredSelection) getSelection();
		return selection.toList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer#getServiceContentProvider
	 * ()
	 */
	public IServiceContentProvider getServiceContentProvider() {
		return (IServiceContentProvider) getContentProvider();
	}

	/**
	 * @return the userMode
	 */
	public int getUserMode() {
		return userMode;
	}

	@Override
	protected void handleSelect(SelectionEvent event) {
		super.handleSelect(event);

	}

	@Override
	protected void handleTreeCollapse(TreeEvent event) {
		if (event.item.getData() != null
				&& (event.item.getData() instanceof Node)) {
			Node n = (Node) event.item.getData();
			n.setExpanded(false);
		}
		super.handleTreeCollapse(event);
	}

	@Override
	protected void handleTreeExpand(TreeEvent event) {
		if (event.item.getData() != null
				&& (event.item.getData() instanceof Node)) {
			Node n = (Node) event.item.getData();
			n.setExpanded(true);
		}
		super.handleTreeExpand(event);
	}

	protected void hookContextMenu() {
		if (menuManager != null && contextMenuListener != null) {
			menuManager.removeMenuListener(contextMenuListener);
		}
		
		menuManager = new SortingMenuManager("Available Actions");
		contextMenuListener = new ContextMenuListener(this);
		menuManager.addMenuListener(contextMenuListener);

		Menu menu = menuManager.createContextMenu(getControl());
		
		getControl().setMenu(menu);
	}

	private void hookDoubleClickAction() {
		addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				Node node = (Node) ((IStructuredSelection) getSelection())
				.getFirstElement();
				doubleClickAction.setNode(node);
				if (isDoubleClickEnabled()) {
					doubleClickAction.run();
				}

			}
		});
	}

	protected void init() {
		setUseHashlookup(true);
		setLabelProvider(new ServiceLabelProvider());
		// setSorter(new NameSorter());
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		initDragAndDrop();
		
	}
	
	@Override
	protected void handleDispose(DisposeEvent event) {
		super.handleDispose(event);
		if (contextMenuListener != null) {
			if (menuManager != null) {
				menuManager.removeMenuListener(contextMenuListener);
			}
			contextMenuListener.dispose();
		}
		gridNode = null;
	}

	/**
	 * <p>
	 * Adds DND support to the Navigator. Uses hooks into the extensible
	 * framework for DND.
	 * </p>
	 * <p>
	 * By default, the following Transfer types are supported:
	 * <ul>
	 * <li>LocalSelectionTransfer.getInstance(),
	 * <li>PluginTransfer.getInstance()
	 * </ul>
	 * </p>
	 * 
	 * @see CommonDragAdapter
	 * @see CommonDropAdapter
	 */
	protected void initDragAndDrop() {

		/* Handle Drag and Drop */
		int operations = DND.DROP_COPY | DND.DROP_MOVE;

		ServiceDragger dragAdapter = new ServiceDragger(this);
		addDragSupport(operations, dragAdapter.getSupportedTransferTypes(),
				dragAdapter);
		ServiceDropper dropAdapter = new ServiceDropper(this);
		addDropSupport(operations, dropAdapter.getSupportedTransferTypes(),
				dropAdapter);

	}

	@Override
	protected void internalRefreshStruct(Widget widget, Object element,
			boolean updateLabels) {
		if (widget == null || element == null || getControl().isDisposed()) {
			return;
		}
		Node node = (Node) element;

		if (node.isExpanded() && node.hasChildren()
				|| node instanceof RootNode) {

			internalExpandToLevel(widget, 1);

		} else {

			internalCollapseToLevel(widget, AbstractTreeViewer.ALL_LEVELS);
		}
		
		super.internalRefreshStruct(widget, element, updateLabels);
	}

	public boolean isDoubleClickEnabled() {
		return doubleClickEnabled;
	}

	public boolean isShowingContextMenu() {
		return showingContextMenu;
	}

	public void loadSettings() {
		IDialogSettings s = getDialogSettings();

		// monitored entries
		String[] eprs = s.getArray(ServiceBrowserActivator.MONENTRY_EPRS);
		if (eprs != null) {
			for (int i = 0; i < eprs.length; i++) {
				try {
					String name = s
					.getArray(ServiceBrowserActivator.MONENTRIES)[i];
					String eprString = eprs[i];
					EndpointReferenceType epr = EndpointReferenceType.Factory
					.parse(eprString);

					Node node = NodeFactory.createNode(epr);
					if (name != null) {
						node.setName(name);
					}
					// add to model
					getGridNode().addChild(node);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	protected void makeActions() {
		// no node selected for double click yet => null value
		doubleClickAction = new DoubleClickAction(null);
		doubleClickAction.setViewer(this);
		doubleClickAction.setEnabled(isDoubleClickEnabled());
	}

	@Override
	public void refresh() {
		refresh(getRoot(), true);
	}

	@Override
	public void refresh(final Object element) {
		refresh(element, true);
	}

	@Override
	public void refresh(final Object element, final boolean updateLabels) {
		// Do NOT replace this with Display#syncExec, this can cause Deadlocks!
		asyncExec(new Runnable() {
				public void run() {
					if (!getControl().isDisposed()) {
						superRefresh(element, updateLabels);
					}
				}
			});
	}

	public void refreshLevels(Object element, int depth) {
		superRefresh(element, true);
		if (depth > 0) {
			for (Object child : getAllFilteredChildren(element)) {
				refreshLevels(child, depth - 1);
			}
		}
	}

	public void removeMonitoringEntry(Node node) {
		getGridNode().removeBookmark(node);

		// remove from persistence
		saveGrid();
		asyncExec(new Runnable() {
			public void run() {
				// inform other views (e.g. details view) by firing selection
				// changed event
				setSelection(getSelection());
			}
		});

	}

	public void saveGrid() {
		ServiceBrowserActivator.getDefault()
		.saveMonitoredEntries(getGridNode());

	}

	public void setDoubleClickEnabled(boolean doubleClickEnabled) {
		if (doubleClickAction != null) {
			doubleClickAction.setEnabled(doubleClickEnabled);
		}
		this.doubleClickEnabled = doubleClickEnabled;
	}

	@Override
	protected void setExpanded(Item item, boolean expand) {
		super.setExpanded(item, expand);
	}

	public void setExpandedLevels(int depth) {
		if (getGridNode() == null) {
			return;
		}
		if (depth > 0) {

			Queue<Node> currentLevel = new ConcurrentLinkedQueue<Node>();
			Queue<Node> nextLevel = new ConcurrentLinkedQueue<Node>();
			currentLevel.add(getGridNode());
			int currentDepth = 1;
			while (currentDepth <= depth) {
				while (!currentLevel.isEmpty()) {
					Node next = currentLevel.poll();
					next.setExpanded(true);
					nextLevel.addAll(next.getChildren());
					expandToLevel(next, 1);
				}
				currentDepth++;
				currentLevel.addAll(nextLevel);
				nextLevel.clear();
			}
			while (!currentLevel.isEmpty()) {
				Node next = currentLevel.poll();
				next.setExpanded(false);
				collapseToLevel(next, 0);
			}

		} else {
			getGridNode().setExpanded(false);
			collapseToLevel(getGridNode(), 0);
		}
	}

	/**
	 * Method for setting the data model of the viewer by setting the GridNode.
	 * Use this method instead of setInput in order to set the model! An
	 * invisible RootNode is added as a parent of the GridNode.
	 * 
	 * @param gridNode
	 */
	public void setGridNode(GridNode gridNode) {
		if(getContentProvider() == null) return;
		this.gridNode = gridNode;
		RootNode rootNode = new RootNode();
		rootNode.addChild(gridNode);
		setInput(rootNode);
		refreshLevels(gridNode, Integer.MAX_VALUE);
	}

	/**
	 * This method tries to set the current selection according to the incoming
	 * list of adaptables. To this end, it tries to find matches between the
	 * incoming list and its internal list by looking at the URIs of the
	 * incoming nodes (using an {@link URIAdapter}). The viewer is also
	 * refreshed after updating the selection.
	 * 
	 * @param selectedNodes
	 */
	public void setSelectedNodes(List<IAdaptable> selectedNodes) {
		List<IAdaptable> found = verifySelectedNodes(selectedNodes);
		setSelection(new StructuredSelection(found), true);
		refresh();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer#setContentProvider
	 * (de.fzj.unicore.rcp.servicebrowser.IServiceContentProvider)
	 */
	public void setServiceContentProvider(
			IServiceContentProvider contentProvider) {
		setContentProvider(contentProvider);

	}

	public void setShowingContextMenu(boolean showingContextMenu) {
		this.showingContextMenu = showingContextMenu;
		if (!showingContextMenu) {
			getControl().setMenu(null);
		} else {
			hookContextMenu();
		}
	}

	/**
	 * @param userMode
	 *            the userMode to set
	 */
	public void setUserMode(int userMode) {
		this.userMode = userMode;
	}

	private void superRefresh(Object element, boolean updateLabels) {
		Control c = getControl();
		if(c == null || c.isDisposed() || c.getDisplay().isDisposed()) return;
		try {
			super.refresh(element, updateLabels);	
		} catch (SWTException e) {
			// this may happen when widget gets disposed in the meantime?!
			// fail silently
		}
		

	}

	public void superUpdate(Object element, String[] properties) {
		super.update(element, properties);
	}

	@Override
	public void update(final Object element, final String[] properties) {
		// Do NOT replace this with Display#syncExec, this can cause Deadlocks!
		asyncExec(new Runnable() {
			public void run() {
				superUpdate(element, properties);
			}
		});
	}

	protected List<IAdaptable> verifySelectedNodes(
			List<IAdaptable> selectedNodes) {
		Map<URI, IAdaptable> resources = new HashMap<URI, IAdaptable>();
		for (IAdaptable adaptable : selectedNodes) {
			Object adapter = adaptable.getAdapter(URIAdapter.class);
			if (adapter != null) {
				resources.put(((URIAdapter) adapter).getURI(), null);
			}
		}
		for (Object o : getServiceContentProvider().getSubtreeElements(
				getGridNode().getParent())) {
			try {
				IAdaptable adaptable = (IAdaptable) o;
				Object adapter = adaptable.getAdapter(URIAdapter.class);
				if (adapter != null) {
					URI uri = ((URIAdapter) adapter).getURI();
					if (resources.keySet().contains(uri)) {
						IAdaptable previous = resources.get(uri);
						if (previous == null) {
							resources.put(uri, adaptable);
						} else {
							if (selectedNodes.contains(adaptable)) {
								resources.put(uri, adaptable);
								// else the found not is not better than the one
								// that we found previously
								// so don't override
							}
						}
					}
				}

			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.ERROR,
						"Could not get details about selected resource!", e);
			}
		}
		return new ArrayList<IAdaptable>(resources.values());
	}

	private void asyncExec(Runnable r)
	{
		Display d = PlatformUI.getWorkbench().getDisplay();
		asyncQueueExecutor.queue.offer(r);
		if(!asyncQueueExecutor.running)
		{
			asyncQueueExecutor.running = true;
			if(!d.isDisposed())
			{
				d.asyncExec(asyncQueueExecutor);
			}
		}
	}

	class AsyncQueueExecuter implements Runnable
	{
		boolean running = false;
		Queue<Runnable> queue = new ConcurrentLinkedQueue<Runnable>();
		public void run() {
			while(!queue.isEmpty())
			{
				Runnable r  = queue.poll();
				r.run();
				
			}
			running = false;

		}

	}

	public MenuManager getMenuManager() {
		return menuManager;
	}


}
