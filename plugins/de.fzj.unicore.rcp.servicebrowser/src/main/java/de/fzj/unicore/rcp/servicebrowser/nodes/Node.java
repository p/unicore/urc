/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.xmlbeans.XmlException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TransferData;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.common.adapters.LowLevelInfo;
import de.fzj.unicore.rcp.common.adapters.URIAdapter;
import de.fzj.unicore.rcp.common.interfaces.ISerializable;
import de.fzj.unicore.rcp.common.properties.IPropertySource;
import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.logmonitor.LogActivator;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.actions.RefreshAction;
import de.fzj.unicore.rcp.servicebrowser.actions.UnmonitorAction;
import de.fzj.unicore.rcp.servicebrowser.utils.URIUtils;

/**
 * 
 * The base class for all model objects that are used in graphical
 * representations of the Grid. Grid services usually form a hierarchical
 * structure (which can be displayed in tree viewers). Therefore each node may
 * have child nodes and a parent node. This class should heavily be subclassed
 * in order to introduce new types of services. It provides a rather lazy access
 * to the Grid services and allows for specifying customized actions that can be
 * performed on the particular Grid service type. Nodes should ALWAYS be created
 * via the {@link NodeFactory} and have to be disposed of, once you are done
 * with them (by calling the {@link #dispose()} method. The disposal is
 * necessary because nodes store their properties in a node data object that may
 * be shared by multiple node instances (each of them representing the same Grid
 * service). By refreshing one of these nodes, the node data object is updated
 * which will, in turn, update all nodes representing the service.
 * 
 * @author demuth
 * 
 */
public abstract class Node implements IAdaptable, ISerializable,
Comparable<Node>, IPropertySource, PropertyChangeListener {

	class NodeDisposer extends BackgroundJob {

		Queue<Node> toDispose = new ConcurrentLinkedQueue<Node>();

		public NodeDisposer() {
			super("disposing unused Grid Browser nodes");
			setSystem(true);
		}

		void disposeNode(Node n) {
			toDispose.add(n);
		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {
			monitor.beginTask("node disposal", IProgressMonitor.UNKNOWN);
			try {
				while (!toDispose.isEmpty() &&!monitor.isCanceled()) {
					toDispose.poll().doDispose();
				}
			} finally {
				monitor.done();
			}

			return Status.OK_STATUS;
		}

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4424375880708819403L;

	// Properties of this node

	/**
	 * Used to indicate that the node has changed and must be refreshed in
	 * potential viewers
	 */
	public static final String PROPERTY_WHOLE = "whole node";

	/**
	 * Property representing child nodes of this node
	 */
	public static final String PROPERTY_CHILDREN = "children";

	/**
	 * The name of the node.
	 */
	public static final String PROPERTY_NAME = "name";

	/**
	 * State of the node
	 */
	public static final String PROPERTY_STATE = "state";

	public static final String PROPERTY_TYPE = "node type";

	/**
	 * The last time a refresh has been called on the node data.
	 * The corresponding value is of type Long (millis since 1970).
	 */
	public static final String PROPERTY_LAST_REFRESH = "Last refresh";

	/**
	 * If the state of the node is set to failed, a reason should be provided
	 */
	public static final String PROPERTY_FAIL_REASON = "fail reason";

	/**
	 * Used to store whether the children of this node should be expanded in
	 * tree viewers or not
	 */
	public static final String PROPERTY_EXPANDED = "expandChildren";

	public static final String DOUBLE_CLICK_REFRESH = "refreshing";
	/**
	 * state of the service / node
	 */
	/** Constant for NEW state (indicates refresh on first selection */
	public static int STATE_NEW = 0;
	public static String STRING_STATE_NEW = "New";
	/** Constant for READY (indicates nothing to do) */
	public static int STATE_READY = 10;
	public static String STRING_STATE_READY = "Ready";
	/** Constant for REFRESH (indicates a currently running refresh */
	public static int STATE_REFRESH = 20;
	public static String STRING_STATE_REFRESH = "Refresh";
	/** Constant for FAILED (indicates a failure since last refresh */
	public static int STATE_FAILED = 30;
	public static String STRING_STATE_FAILED = "Failed";
	/**
	 * Constant for ACCESS_DENIED (indicates that the user has no access to the
	 * entity
	 */
	public static int STATE_ACCESS_DENIED = 35;
	public static String STRING_STATE_ACCESS_DENIED = "Access denied";
	/** Constant for DESTROYED (indicates this component was destroyed */
	public static int STATE_DESTROYED = 40;

	public static String STRING_STATE_DESTROYED = "Destroyed";

	/** Constant for details string */
	public static String NO_DETAILS = "No details.";


	/**
	 * Upper limit for the number of levels in the GridBrowser that we should
	 * respect (e.g. for avoiding infinite node loops)
	 */
	public static final int MAX_LEVEL = 1000;

	/**
	 * The human readable name of this node
	 */
	protected String name;

	protected String namePathToRoot;

	protected URI uri;

	protected URI fallbackURI;

	protected EndpointReferenceType epr;

	protected EndpointReferenceType fallbackEpr;

	/**
	 * Also save a serialized version of the endpoint reference since xstream
	 * cannot serialize eprs.
	 */
	protected String eprString;

	protected String fallbackEprString;

	protected INodeDataCache cache;

	protected Boolean expanded = false;

	protected boolean disposed = false;

	private transient boolean persistable = true;

	/**
	 * store the state directly in the node as this is a critical property for
	 * displaying the node correctly
	 */
	protected int state = STATE_NEW;

	/**
	 * Deprecated
	 * 
	 * @deprecated This was used for preventing a node from reacting to its own
	 *             changes to the node data. Now
	 *             {@link INodeData#setProperty(String, Object, Object)} is used
	 *             instead.
	 */
	@Deprecated
	protected transient boolean updatingData = false;

	/**
	 * Views and other interested parties register as a listener here.
	 */
	transient private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(
			this);

	/**
	 * The parent of this node
	 */
	private Node parent;

	/**
	 * The children of this node
	 */
	private Node[] children;

	/**
	 * List of actions that can be executed on this node
	 */
	protected transient List<NodeAction> availableActions;

	/**
	 * Reason for fail state
	 */
	protected String failReason = "";

	private transient boolean refreshCancelled = false;

	private transient boolean refreshing = false;

	private transient static NodeDisposer nodeDisposer;

	/**
	 * For creating nodes, ALWAYS use the NodeFactory!
	 */
	public Node() {
		this(null);
	}

	public Node(EndpointReferenceType epr) {
		setEpr(epr);
		children = new Node[0];
		availableActions = new ArrayList<NodeAction>();

	}

	/**
	 * Add node as child. Set this node as parent of the child. Do not add child
	 * if it already exists in children list! Fire property change to inform
	 * listeners.
	 * 
	 * @param child
	 */
	public void addChild(Node child) {
		List<Node> oldChildren = getChildren();
		INodeData data = getData();
		if (data == null) {
			return;
		}
		synchronized (data) {
			List<Node> children = getChildren();
			if (children.contains(child)) {
				return;
			}
			if (child.getName() == null) {
				child.retrieveName();
			}
			children.add(child);
			child.setParent(this);

			setChildren(sortChildren(children));

			getPropertyChangeSupport().firePropertyChange(
					new PropertyChangeEvent(this, PROPERTY_CHILDREN,
							oldChildren, children));
		}
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {

		// do NOT add Listeners twice!
		for (PropertyChangeListener propertyChangeListener : getPropertyChangeSupport().getPropertyChangeListeners()) {
			if (propertyChangeListener.equals(l)) {
				return;
			}
		}

		getPropertyChangeSupport().addPropertyChangeListener(l);
	}

	/**
	 * This is called after the service browser's object model has been
	 * deserialized from disk. It can be used to perform some re-initialization
	 * before the node is actually used.
	 */
	public void afterDeserialization() {
		if (getState() == STATE_REFRESH) {
			setState(STATE_NEW);
		}
		if (getName() == null) {
			retrieveName();
		}
		INodeData data = getData();
		if (data != null) {

			// make sure the type property is set right even if we lost node
			// properties
			if(data.getProperty(PROPERTY_TYPE) == null) {
				data.setProperty(PROPERTY_TYPE, getType(),this);
			}
			data.addPropertyChangeListener(this);
		}
		setPersistable(true); // the fact that we were persisted means that we
		// were persistable
		for (Node child : getChildrenArray()) {
			child.afterDeserialization();
		}
	}

	/**
	 * As the name suggests, this is called after serialization and can be used
	 * to undo changes that have been made in the {@link #beforeSerialization()}
	 * method.
	 */
	public void afterSerialization() {
		for (Node child : getChildrenArray()) {
			child.afterSerialization();
		}
	}

	/**
	 * Refresh the properties of the node asynchronously (inside a Job).
	 * 
	 * @param levels
	 * @param refreshAfterLastRetrieve
	 */
	public void asyncRefresh(final int levels,
			final boolean refreshAfterLastRetrieve) {
		Job j = new BackgroundJob(ServiceBrowserConstants.MSG_REFRESHING) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				return refresh(levels, refreshAfterLastRetrieve, monitor);
			}

		};
		j.schedule();
	}

	/**
	 * This is called before the service browser's object model is serialized to
	 * disk (using XStream). It should be used in order to transform any
	 * information that cannot directly be serialized into a serializable
	 * representation (or get rid of it).
	 */
	public void beforeSerialization() {
		for (Node child : getChildrenArray()) {
			child.beforeSerialization();
		}
	}

	/**
	 * Test if the given Object can be pasted into this node. Override this in
	 * subclasses in order to allow pasting. Override
	 * {@link #paste(Object, TransferData, int)} in order to implement the
	 * actual pasting behaviour
	 * 
	 * @param toPaste
	 * @param transferType
	 *            used to determine what kind of data should be pasted into this
	 *            node
	 * @param operation
	 *            the type of paste, can be {@link DND#DROP_COPY} or
	 *            {@link DND#DROP_MOVE}
	 * @return
	 */
	public boolean canPaste(Object toPaste, TransferData transferType,
			int operation) {
		return false;
	}

	/**
	 * Clone this node and all its children (and their children...)
	 */
	@Override
	public Node clone() {
		Node node = NodeFactory.createNode(getData());

		node.setName(getName());
		List<Node> clonedChildren = getChildren();
		for (int i = 0; i < clonedChildren.size(); i++) {
			Node child = clonedChildren.get(i);
			child = child.clone();
			child.setParent(node);
			clonedChildren.set(i, child);
		}
		node.setChildren(clonedChildren);
		return node;

	}

	/**
	 * Clone this node and all children up to a limited depth. Depth == 0 means
	 * "no children", Depth == 1 means "first layer of children" ...
	 */
	public Node clone(int depth) {
		Node node = NodeFactory.createNode(getData());
		node.setName(getName());
		node.expanded = expanded;
		node.persistable = persistable;
		if (depth > 0) {
			List<Node> clonedChildren = getChildren();
			for (int i = 0; i < clonedChildren.size(); i++) {
				Node child = clonedChildren.get(i);
				child = child.clone(depth-1);
				child.setParent(node);
				clonedChildren.set(i, child);
			}
			node.setChildren(clonedChildren);
		}
		return node;

	}

	/**
	 * Compare two nodes to each other returning either -1, 0, or 1 as a result.
	 * See {@link Comparable#compareTo(Object)}. This is used for bringing nodes
	 * in their natural order.
	 */
	public int compareTo(Node n) {
		int i = new Integer(getCategory()).compareTo(n.getCategory());
		if (i == 0) {
			if (getState() == STATE_READY && n.getState() != STATE_READY) {
				return -1;
			} else if (getState() != STATE_READY && n.getState() == STATE_READY) {
				return 1;
			}
		}
		if (i == 0 && getName() != null) {
			i = getName().compareToIgnoreCase(n.getName());
		}
		return i;
	}

	/**
	 * perform some cleanup when node is not needed anymore by default disposes
	 * all child nodes, too. If this is called from the main GUI thread,
	 * disposing will take place asynchronously, in a worker thread.
	 */
	public void dispose() {
		if (nodeDisposer == null) {
			nodeDisposer = new NodeDisposer();
		}
		nodeDisposer.disposeNode(this);
		if (nodeDisposer.getState() == Job.NONE) {
			nodeDisposer.schedule();
		}

	}

	protected void doDispose() {
		if (isDisposed()) {
			return; // already disposed, don't do it again!
		}
		if (getCache() != null) {
			INodeData data = getData();
			if (data != null) {
				long start = System.currentTimeMillis();
				while (refreshing) { 
					// do not dispose this node while it is being refreshed
					// otherwise we might loose the state change at the end
					// of the refresh procedure!
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
					}
					if(System.currentTimeMillis() - start  > 300000)
					{
						// we have waited for more than 5 minutes! DO SOMETHING!
						state = STATE_FAILED;
						refreshing = false;
						break;
					}
				}
				data.removePropertyChangeListener(this);

			}
			getCache().decrementNodeCount(getEpr());
		}

		if (propertyChangeSupport != null) {
			for (PropertyChangeListener l : propertyChangeSupport
					.getPropertyChangeListeners()) {
				propertyChangeSupport.removePropertyChangeListener(l);
			}
		}
		Node[] children = getChildrenArray();
		for (int i = 0; i < children.length; i++) {
			children[i].dispose();
		}
		children = null;
		// don't set the parent to null!
		// otherwise removing the node from its parent
		// during serialisation will stop working
		setDisposed(true);
	}

	/**
	 * This method gets executed when the node is double clicked. It is
	 * responsible for beginning and ending the task on the Progress Monitor.
	 */
	public IStatus doubleClick(IProgressMonitor monitor) {
		setExpanded(true);
		IStatus s = refresh(1, true, monitor);
		return s;
	}

	@Override
	public void finalize() throws Throwable {
		super.finalize();
	}

	/**
	 * used to notify any listeners of selection changes and provide them with
	 * an object they can handle
	 */
	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class key) {

		if (key.equals(URIAdapter.class)) {
			return new URIAdapter(getURI());

		} else if (key.getName().equals(DetailsAdapter.class.getName())) {
			return getDetails();

		} else if (key.getName().equals(LowLevelInfo.class.getName())) {
			// Used by the Details vie (free text area);
			if (getState() == STATE_NEW) {
				return null;
			}
			return getLowLevelInfo();
		}
		return Platform.getAdapterManager().getAdapter(this, key);
	}

	/**
	 * Returns a list of available node actions. Not intended to be overridden
	 * by subclasses. Instead, override {@link #resetAvailableActions()} and add
	 * the actions to the availableActions list there.
	 * 
	 * @return
	 */
	public List<NodeAction> getAvailableActions() {
		return availableActions;
	}

	/**
	 * The node's properties should not be stored directly in this object.
	 * Instead, a node data cache should be used that may transparently save the
	 * properties to the disk instead of keeping them in memory all the time.
	 * 
	 * @return
	 */
	public INodeDataCache getCache() {
		return cache;
	}

	/**
	 * Integer value for sorting nodes in views. Nodes are usually sorted in
	 * ascending order.
	 * 
	 * @return
	 */
	public int getCategory() {
		return 1000;
	}

	/**
	 * Returns the child nodes for this node
	 * 
	 * @return
	 */
	public List<Node> getChildren() {
		return new ArrayList<Node>(Arrays.asList(children));
	}

	public Node[] getChildrenArray() {
		return children;
	}

	public Map<URI, Node> getChildrenMap() {
		Map<URI, Node> result = new HashMap<URI, Node>();
		for (Node node : getChildren()) {
			result.put(node.getURI(), node);
		}
		return result;
	}

	/**
	 * Retrieve the node data object for storing ANY kind larger data that
	 * describes the node. Stored data should always be-serializable with
	 * xstream. Note that this returns null if the node has been disposed!!
	 * 
	 * @return
	 */
	public INodeData getData() {
		INodeDataCache cache = getCache();
		// do not try to obtain data for a disposed node as this might create a
		// new data object
		// that won't be cleaned up afterwards
		if (cache == null || isDisposed()) {
			return null;
		}
		return cache.getNodeData(getEpr());
	}

	/**
	 * Obtain String descriptions of the node's properties. This information is
	 * displayed in the details view.
	 * 
	 * @return
	 */
	public DetailsAdapter getDetails() {
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("Name", getName());
		hm.put("Type", getType());
		hm.put("URI", getURI().toString());
		hm.put("State", getStateAsString());
		if (getState() == STATE_NEW) {
			hm.put("Info", "Refresh for detailed information");
		}

		if (getState() == STATE_FAILED && getFailReason() != null) {
			hm.put("failReason", getFailReason());
		}
		return new DetailsAdapter(hm);
	}

	/**
	 * Return the String that is displayed to the user while the doubleClick
	 * method is being executed
	 * 
	 * @return
	 */
	public String getDoubleClickActionName() {
		return DOUBLE_CLICK_REFRESH;
	}

	public EndpointReferenceType getEpr() {
		if (epr == null && eprString != null) {
			try {
				epr = EndpointReferenceType.Factory.parse(eprString);
			} catch (XmlException e) {
			}
		}
		return epr;
	}
	
	public EndpointReferenceType getFallbackEpr() {
		if(fallbackEpr == null && fallbackEprString != null) {
			try {
				fallbackEpr = EndpointReferenceType.Factory.parse(fallbackEprString);
			} catch (XmlException e) {
				// should not happen
			}
		}
		return fallbackEpr;
	}

	/**
	 * Used for obtaining a reason when accessing the service associated to this
	 * node has failed.
	 * 
	 * @return
	 */
	public String getFailReason() {
		INodeData data = getData();
		if (data == null) {
			return null;
		}
		Object o = data.getProperty(PROPERTY_FAIL_REASON);
		if (o != null) {
			return (String) o;
		} else {
			return null;
		}
	}

	/**
	 * Returns the level of the node in the hierarchy of nodes. At level 0 there
	 * is usually a {@link RootNode}, level 1 is usually taken by a
	 * {@link GridNode}, followed by nodes of type {@link RegistryNode} at level
	 * 2;
	 * 
	 * @return
	 */
	public int getLevel() {
		if (getParent() == null) {
			return 0;
		} else {
			return getParent().getLevel() + 1;
		}
	}

	public String getLowLevelAsString() {
		return "";
	}

	/**
	 * Return low level information about the node that can be inspected by
	 * developers or Grid experts. The content is usually not for the faint
	 * hearted (e.g. low level XML data).
	 * 
	 * @return
	 */
	public LowLevelInfo getLowLevelInfo() {
		return new LowLevelInfo(getName(), getLowLevelAsString());
	}

	/**
	 * Returns the node's name as displayed in the Grid Browser.
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns a series of names of this node and all its parents, separated by
	 * slashes (/).
	 * 
	 * @return
	 */
	public String getNamePathToRoot() {
		if (namePathToRoot != null) {
			return namePathToRoot;
		} else if (parent == null) {
			return "";
		} else {
			String parentPath = (parent instanceof RootNode) ? "" : parent
					.getNamePathToRoot() + "/";
			return parentPath + getName();
		}
	}

	public Node getParent() {
		return parent;
	}

	// @Override
	// public boolean equals(Object o)
	// {
	// if(!(o instanceof Node)) return false;
	// Node n = (Node) o;
	// return getURI().equals(n.getURI()) && getName().equals(n.getName()) &&
	// isTopLevel() == n.isTopLevel();
	// }

	/**
	 * Returns a series of URIS of this node and all its parents
	 * 
	 * @return
	 */
	public NodePath getPathToRoot() {
		if (parent == null) {
			return new NodePath(getURI());
		} else {
			NodePath parentPath = (parent instanceof RootNode) ? new NodePath()
			: parent.getPathToRoot();
			return parentPath.append(getURI());
		}
	}

	protected PropertyChangeSupport getPropertyChangeSupport() {
		if (propertyChangeSupport == null) {
			propertyChangeSupport = new PropertyChangeSupport(this);
		}
		return propertyChangeSupport;
	}

	public RegistryNode getRegistryNode() {
		Node parent = this;
		while (parent != null && !(parent instanceof RegistryNode)) {
			parent = parent.getParent();
		}
		if (parent instanceof RegistryNode) {
			return (RegistryNode) parent;
		}
		if (getData() != null) {
			return getData().getRegistryNode();
		} else {
			return null;
		}
	}

	/**
	 * Indicates the state, one of STATE_NEW, STATE_READY, STATE_REFRESH,
	 * STATE_DESTROYED, STATE_ACCESS_DENIED, OR STATUS_FAILED
	 */
	public int getState() {
		return state;

	}

	public String getStateAsString() {
		int state = getState();
		if (getState() == STATE_NEW) {
			return STRING_STATE_NEW;
		} else if (state == STATE_READY) {
			return STRING_STATE_READY;
		} else if (state == STATE_REFRESH) {
			return STRING_STATE_REFRESH;
		} else if (state == STATE_FAILED) {
			return STRING_STATE_FAILED;
		} else if (state == STATE_ACCESS_DENIED) {
			return STRING_STATE_ACCESS_DENIED;
		} else if (state == STATE_DESTROYED) {
			return STRING_STATE_DESTROYED;
		}
		ServiceBrowserActivator.log(IStatus.ERROR,
				"Undefinied type while getStateAsString() - int-type:" + state);
		return null; // shouldnt happen
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return "unknown node type";
	}

	public URI getURI() {
		if (uri == null) {
			try {
				uri = new URI(getEpr().getAddress().getStringValue());
			} catch (Exception e) {
				if (name != null) {
					try {
						uri = new URI(name);
					} catch (URISyntaxException e2) {
						ServiceBrowserActivator
						.log("Unable to determine URL for node: "
								+ getName());
					}
				}
			}

		}
		return uri;
	}

	protected void setURI(String _uri) {
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue(_uri);
		setEpr(epr);
		uri = null;
	}

	public URI getFallbackURI() {
		return fallbackURI;
	}
	
	public boolean hasChildren() {
		return children.length > 0;
	}

	/**
	 * This method should be used for initialization instead of the constructor.
	 * It is called AFTER the node data cache has been set which is important
	 * for the node to be fully operational.
	 */
	public void init() {
		// try to extract state from data, don't use setState, as this will
		// trigger an event
		Integer state = (Integer) getData().getProperty(PROPERTY_STATE);
		if (state != null) {
			this.state = state;
		}
		String name = (String) getData().getProperty(PROPERTY_NAME);
		if (name != null) {
			this.name = name;
		}
		getData().addPropertyChangeListener(this);
	}

	public boolean isDisposed() {
		return disposed;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public boolean isKnownChild(Node so) {
		if(so == null) return false;
		for(Node n : getChildrenArray())
		{
			if(so.equals(n))
			{
				return true;
			}
		}
		return false;
	}

	public boolean isPersistable() {
		if (isTopLevel()) {
			return true; // always persist top level nodes (the ones right below
			// the Grid node)
		}

		if (isDisposed()) {
			return false;
		}
		return persistable;

	}

	public boolean isTopLevel() {
		return getParent() instanceof GridNode;
	}

	/**
	 * Override in order to allow pasting of Objects (e.g.nodes) into this node.
	 * 
	 * @param toPaste
	 * @param transferType
	 *            used to determine what kind of data should be pasted into this
	 *            node
	 * @param operation
	 *            the type of paste, can be {@link DND#DROP_COPY} or
	 *            {@link DND#DROP_MOVE}
	 * @return
	 */
	public void paste(Object toPaste, TransferData transferType, int operation) {
		// do nothing in the default implementation
		// TODO use an extension point to inject pasting behaviour
	}

	public void propertyChange(PropertyChangeEvent evt) {
		if (updatingData || evt.getSource() == this) {
			return; // don't react on your own changes!
		}

		if (INodeData.PROPERTY_DATA_CHILDREN.equals(evt.getPropertyName())) {
			updateChildrenFromData();
			return;
		} else if (PROPERTY_STATE.equals(evt.getPropertyName())) {
			state = (Integer) evt.getNewValue();
			retrieveName();
		} else if (PROPERTY_NAME.equals(evt.getPropertyName())) {
			String name = (String) evt.getNewValue();
			String oldName = getName();
			boolean equal = oldName == null ? name == null : oldName
					.equals(name);
			if (equal) {
				return;
			}
			this.name = name;
			PropertyChangeEvent e = new PropertyChangeEvent(this,
					PROPERTY_NAME, oldName, name);
			getPropertyChangeSupport().firePropertyChange(e);
		}
		if (getPropertyChangeSupport() != null) {
			getPropertyChangeSupport()
			.firePropertyChange(evt.getPropertyName(),
					evt.getOldValue(), evt.getNewValue());
		}
	}

	/**
	 * Gain write access to node data and update it. NEVER call this from the
	 * GUI thread!
	 * 
	 * @param key
	 * @param value
	 */
	protected void putData(String key, Object value) {
		INodeData data = getData();
		if (data == null) {
			return;
		}
		synchronized (data) {
			data.setProperty(key, value, this);
		}
	}

	/**
	 * Refresh this node and its children. This method must never be called from
	 * the main GUI thread, since it is long-running. From the main thread, you
	 * might call {@link Node#asyncRefresh(int, boolean)}
	 * 
	 * @param levels
	 *            the number of levels below this node to be refreshed
	 * @return return the result of the refresh, the code MUST be one of the one
	 *         of the state constants defined in this class
	 */
	public IStatus refresh(int levels, boolean refreshAfterLastRetrieve,
			IProgressMonitor progressMonitor) {
		progressMonitor = ProgressUtils.getNonNullMonitorFor(progressMonitor);
		INodeData data = getData();
		if(data == null) 
		{
			return new Status(IStatus.CANCEL,
					ServiceBrowserActivator.PLUGIN_ID, getState(), "", null);
		}
		
		int stateAfterRefresh = STATE_READY;
		
		// don't do multiple refreshs at the same time
		synchronized (data)
		{
			if(progressMonitor.isCanceled()) return new Status(IStatus.CANCEL,
					ServiceBrowserActivator.PLUGIN_ID, getState(), "", null);
			Long lastRefresh = (Long) data.getProperty(PROPERTY_LAST_REFRESH);
			if(lastRefresh != null 
					&& System.currentTimeMillis()-lastRefresh < 2000)
			{
				// last refresh has happened only 2 second ago
				return new Status(IStatus.OK,
						ServiceBrowserActivator.PLUGIN_ID, getState(),
						"ok", null);
			}
			if(levels > MAX_LEVEL) levels = MAX_LEVEL;

			


			setState(STATE_REFRESH);
			refreshing = true;
			putData(PROPERTY_LAST_REFRESH, null);
			
			try {
				progressMonitor.beginTask("refreshing... ",
						(int) Math.pow(2, levels));
				refreshCancelled = false;
				progressMonitor.subTask("retrieving node details");
				IProgressMonitor subProgress = new SubProgressMonitor(
						progressMonitor, 1,
						SubProgressMonitor.PREPEND_MAIN_LABEL_TO_SUBTASK);
				stateAfterRefresh = retrieveProperties(subProgress);
				retrieveName();
				putData(PROPERTY_LAST_REFRESH, System.currentTimeMillis());
			} finally {
				// set your state to READY before starting to retrieve and refresh
				// children
				setState(stateAfterRefresh);
				refreshing = false;
			}
		}
		try {
			if (levels > 0 && getLevel() < MAX_LEVEL) {
				retrieveChildren();
			}
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.WARNING,
					"Error while retrieving children of node" + getNamePathToRoot(), e);
		}
		progressMonitor.subTask("refreshing children");
		IProgressMonitor subProgress = new SubProgressMonitor(progressMonitor,
				(int) Math.pow(2, levels) - 1,
				SubProgressMonitor.PREPEND_MAIN_LABEL_TO_SUBTASK);
		PropertyChangeEvent e = new PropertyChangeEvent(this, PROPERTY_WHOLE,
				null, this);
		getPropertyChangeSupport().firePropertyChange(e);
		if (levels > 1 || refreshAfterLastRetrieve) {
			List<Node> parents, children = null;
			children = getChildren();
			for (int i = 0; i < levels; i++) {
				parents = new ArrayList<Node>(children);
				children = new ArrayList<Node>();
				for (Iterator<Node> iter = parents.iterator(); iter.hasNext();) {
					Node next = iter.next();
					if (i == levels - 1) {
						IProgressMonitor subProgress2 = new SubProgressMonitor(
								progressMonitor,
								0,
								SubProgressMonitor.PREPEND_MAIN_LABEL_TO_SUBTASK);
						if (refreshAfterLastRetrieve) {
							next.refresh(0, false, subProgress2);
						}
					} else {
						next.refresh(1, false, subProgress);
					}
					Node[] nextChildren = next.getChildrenArray();
					for (int j = 0; j < nextChildren.length; j++) {
						children.add(nextChildren[j]);
					}
					if (subProgress.isCanceled() || isDisposed()) {
						refreshCancelled = true;
						break;
					}
				}
				subProgress.worked((int) Math.pow(2, i));

				if (refreshCancelled) {
					break;
				}
			}
			Arrays.sort(this.children);
		}
		e = new PropertyChangeEvent(this, PROPERTY_WHOLE, null, this);
		getPropertyChangeSupport().firePropertyChange(e);
		subProgress.done();

		if (refreshCancelled) {
			return new Status(IStatus.CANCEL,
					ServiceBrowserActivator.PLUGIN_ID, stateAfterRefresh, "",
					null);
		} else {
			return new Status(IStatus.OK, ServiceBrowserActivator.PLUGIN_ID,
					stateAfterRefresh, "ok",
					null);
		}

	}

	/**
	 * Refresh the node using a default number of levels. This method must never
	 * be called from the main GUI thread, since it is long-running. From the
	 * main thread, you might call {@link Node#asyncRefresh(int, boolean)}
	 */
	public IStatus refresh(IProgressMonitor monitor) {
		int levels = 4;
		IStatus status = refresh(levels, false, monitor);
		return status;
	}

	/**
	 * refresh the children of this node
	 * 
	 * @param levels
	 *            the number of levels below this node to be refreshed
	 */
	protected void refreshChildren(int levels,
			boolean refreshAfterLastRetrieve, IProgressMonitor progressMonitor) {
		if(levels > MAX_LEVEL) levels = MAX_LEVEL;
		Node[] children = getChildrenArray();
		for (int i = 0; i < children.length; i++) {
			children[i].refresh(levels, refreshAfterLastRetrieve,
					progressMonitor);
		}

	}

	public void removeAllChildren() {
		children = new Node[0];
	}

	/**
	 * Remove node from list of children. Set null as parent of the child. Fire
	 * property change to inform listeners.
	 * 
	 * @param child
	 */
	public void removeChild(Node child) {
		List<Node> oldChildren = new ArrayList<Node>();

		synchronized (getData()) {
			List<Node> children = getChildren();
			if (!children.contains(child)) {
				return;
			}
			oldChildren.addAll(children);
			children.remove(child);
			child.setParent(null);
			setChildren(children);
			getPropertyChangeSupport().firePropertyChange(
					new PropertyChangeEvent(this, PROPERTY_CHILDREN,
							oldChildren, children));
		}

	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		getPropertyChangeSupport().removePropertyChangeListener(l);
	}

	/**
	 * @param availableActions
	 *            the availableActions to set
	 */
	public void resetAvailableActions() {
		availableActions = new ArrayList<NodeAction>();
		availableActions.add(new RefreshAction(this));
		if (isTopLevel()) {
			availableActions.add(new UnmonitorAction(this));
		}

	}

	/**
	 * Create the child nodes of this node.
	 * 
	 * @param name
	 *            
	 */
	protected void retrieveChildren() throws Exception {
	}

	/**
	 * Provide a name for this node that will be displayed to the user. Override
	 * in order to use a more reasonable value. Do nothing in this method in
	 * order to keep whatever name has been set for this node before instead of
	 * automatically overwriting it.
	 */
	public void retrieveName() {
		URI uri = getURI();
		if (uri != null) {
			String identifier = "";
			String type = getType();
			if(type.toLowerCase().contains("unknown"))
			{
				String path = uri.getPath();
				int index = path.lastIndexOf("/");
				type = path.substring(index + 1);
			}
			String location = "";
			String containerName = URIUtils.extractUnicoreServiceContainerName(uri);
			if(containerName != null) location = "@"+containerName;
			setName(type+" "+identifier+location);
		}

	}

	/**
	 * Retrieve information about yourself. Node implementations should
	 * override. Return the state of the node to be set.
	 */
	protected int retrieveProperties(IProgressMonitor progressMonitor) {
		// do nothing by default
		// override to gather information about this node
		return STATE_READY;
	}

	public void setCache(INodeDataCache cache) {
		INodeData data = getData();
		if (data != null) {
			data.removePropertyChangeListener(this);
		}
		this.cache = cache;
		data = getData();
		if (data != null && data.getProperty(PROPERTY_TYPE) == null) {
			data.setProperty(PROPERTY_TYPE, getType(),this);
		}
	}

	/**
	 * @param children
	 *            the children to set
	 */
	public void setChildren(List<Node> children) {
		this.children = children.toArray(new Node[children.size()]);
	}

	public void setDisposed(boolean disposed) {
		this.disposed = disposed;
	}

	public void setEpr(EndpointReferenceType epr) {
		if (epr != null) {
			try {
				if(!epr.getAddress().isNil()) {
					this.uri = new URI(epr.getAddress().getStringValue());
				}
				this.epr = epr;
				this.eprString = epr.xmlText();
			} catch (URISyntaxException e) {
				LogActivator.log("Cannot use this as a URI.", e);
			}
		}

	}


	public void setFallbackEpr(EndpointReferenceType epr) {
		if (epr != null) {
			try {
				this.fallbackURI = new URI(epr.getAddress().getStringValue());
				this.fallbackEpr = epr;
				this.fallbackEprString = epr.xmlText();
			} catch (URISyntaxException e) {
				LogActivator.log("Cannot use this as a URI.", e);
			}
		} else {
			this.fallbackEpr = null;
			this.fallbackEprString = null;
			this.fallbackURI = null;
		}

	}
	/**
	 * Displays or hides the children of the current node by expanding or
	 * collapsing it. Stores the expanded state persistently.
	 */
	public void setExpanded(boolean expanded) {
		boolean oldValue = this.expanded;
		if (oldValue == expanded) {
			return;
		}
		this.expanded = expanded;
		if (!expanded) {
			// if the node is collapsed, collapse all child nodes, too
			for (Node child : getChildrenArray()) {
				child.setExpanded(false);
			}
		} else if (getParent() != null && !getParent().isExpanded()) {
			getParent().setExpanded(true);
		}
		getPropertyChangeSupport().firePropertyChange(
				new PropertyChangeEvent(this, PROPERTY_EXPANDED, oldValue,
						expanded));
	}

	public void setFailReason(String failReason) {
		putData(PROPERTY_FAIL_REASON, failReason);
	}

	/**
	 * Sets a new name for the node. This will NOT influence other nodes
	 * representing the same service.
	 * 
	 */
	public void setName(String name) {
		INodeData data = getData();
		if (data != null && data.getProperty(PROPERTY_NAME) != null) {
			return; // do NOT override a name that has been set globally for
			// this node
		}
		String oldName = getName();
		boolean equal = oldName == null ? name == null : oldName.equals(name);
		if (equal) {
			return;
		}
		this.name = name;
		PropertyChangeEvent e = new PropertyChangeEvent(this, PROPERTY_NAME,
				oldName, name);
		getPropertyChangeSupport().firePropertyChange(e);
	}

	/**
	 * Sets a new name for the node. This WILL set the name for other nodes
	 * representing the same service as well.
	 * 
	 * @param name
	 */
	public void setNameForAllCopies(String name) {
		String oldName = (String) getData().getProperty(PROPERTY_NAME);
		boolean equal = oldName == null ? name == null : oldName.equals(name);

		if (equal) {
			return;
		}
		this.name = name;
		PropertyChangeEvent e = new PropertyChangeEvent(this, PROPERTY_NAME,
				oldName, name);
		getPropertyChangeSupport().firePropertyChange(e);
		putData(PROPERTY_NAME, name);

	}

	/**
	 * Set the name path to a fix value. Beware! This is generally not what you
	 * want as the name path should be determined dynamically!!!
	 * 
	 * @param namePathToRoot
	 */
	public void setNamePathToRoot(String namePathToRoot) {
		this.namePathToRoot = namePathToRoot;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public void setPersistable(boolean persistable) {
		this.persistable = persistable;
	}

	public void setState(int state) {
		int oldstate = getState();
		// store the state directly in the node as this is a critical property
		// for displaying the node correctly
		this.state = state;
		// update other nodes that share the same data
		INodeData data = getData();
		if (data != null) {
			data.setProperty(PROPERTY_STATE, state, this);
		}
		PropertyChangeEvent e = new PropertyChangeEvent(this, PROPERTY_STATE,
				oldstate, state);
		getPropertyChangeSupport().firePropertyChange(e);

	}

	public List<Node> sortChildren(List<Node> nodes) {
		List<Node> oldChildren = nodes;
		List<Node> newChildren = new ArrayList<Node>(oldChildren);
		Collections.sort(newChildren);
		return newChildren;

	}

	@Override
	public String toString() {
		return "Node: " + getName();
	}

	/**
	 * Update the List of children of this node. Do not replace children that
	 * were already there (nodes are identified via their URIs here). Caution!
	 * Do not reimplement the equals Method of Node in order to do this more
	 * elegantly! It is allowed that multiple Nodes with the same URI may occur
	 * in the Tree. They must, however, be treated as different entities, or the
	 * TreeViewer will create weird effects.
	 * 
	 * @param newChildren
	 */
	protected void updateChildren(List<Node> newChildren) {

		List<Node> oldChildren = getChildren();
		INodeData data = getData();
		if (data == null) {
			return;
		}
		synchronized (data) {
			boolean childrenChanged = false;
			for (Node node : newChildren) {

				if (node.getName() == null) {
					node.retrieveName();
				}
			}

			newChildren = sortChildren(newChildren);
			List<Node> children = new ArrayList<Node>();
			Map<URI, Node> oldChildrenMap = getChildrenMap();


			List<INodeData> dataChildren = new ArrayList<INodeData>();
			for (Node node : newChildren) {
				if (oldChildrenMap.containsKey(node.getURI())) {
					Node old = oldChildrenMap.get(node.getURI());
					if (old.getClass().equals(node.getClass())) {
						if (old != node) {
							// re-using old node, dispose of new node
							node.setParent(null);
							node.dispose();
							EndpointReferenceType epr = node.getEpr();
							node = old;
							// make sure EPR is updated
							node.setEpr(epr);
							node.getData().setEpr(epr);
						}
						oldChildrenMap.remove(node.getURI());
					}
				} else {
					childrenChanged = true;
				}
				children.add(node);
				dataChildren.add(node.getData());

				node.getData().addParent(data);
				node.setParent(this);

			}
			setChildren(children);
			data.setChildren(dataChildren, this);
			if (oldChildrenMap.size() > 0) {
				childrenChanged = true;
			}
			// dispose of old children that are not available anymore
			for (Node node : oldChildrenMap.values()) {
				INodeData nodeData = node.getData();
				if (nodeData != null) {
					nodeData.removeParent(nodeData);
					node.setParent(null);
					node.dispose();
				}
			}

			try {
				if (childrenChanged) {
					getPropertyChangeSupport().firePropertyChange(
							new PropertyChangeEvent(this, PROPERTY_CHILDREN,
									oldChildren, children));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (children.length == 0) {
			setExpanded(false);
		}
	}

	public void updateChildrenFromData() {
		updateChildrenFromData(MAX_LEVEL);
	}

	public void updateChildrenFromData(int depth) {
		List<Node> oldChildren;
		INodeData data = getData();
		if (data == null) {
			return;
		}
		synchronized (data) {
			boolean childrenChanged = false;

			List<Node> children = new ArrayList<Node>();
			oldChildren = getChildren();
			Map<URI, Node> oldChildrenMap = getChildrenMap();
			INodeData[] dataChildren = data.getChildrenArray();
			if (dataChildren != null) {
				for (INodeData childData : dataChildren) {
					Node node;
					if (oldChildrenMap.containsKey(childData.getURI())) {
						node = oldChildrenMap.remove(childData.getURI());
						EndpointReferenceType epr = childData.getEpr();
						node.setEpr(epr);
					} else {
						node = NodeFactory.createNode(childData);
						node.retrieveName();
						if(depth > 0)
						{
							// haven't seen this node before => update
							// it's children recursively
							node.updateChildrenFromData(depth-1);
						}
						childrenChanged = true;
					}
					children.add(node);
					node.setParent(this);
				}
			}
			if (oldChildrenMap.size() > 0) {
				childrenChanged = true;
			}



			// dispose old children that are not available anymore
			for (Node node : oldChildrenMap.values()) {
				node.setParent(null);
				node.dispose();
			}
			setChildren(children);
			if (childrenChanged) {
				getPropertyChangeSupport().firePropertyChange(
						new PropertyChangeEvent(this, PROPERTY_CHILDREN,
								oldChildren, children));
			}

		}
		if (children.length == 0) {
			setExpanded(false);
		}
	}

	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		// do nothing by default: right now, old nodes from previous versions
		// are
		// discarded instead of being deserialized
		// this might change in the future

	}

	protected SimpleDateFormat getDefaultDateFormat() {
		return Constants.getDefaultDateFormat();
	}

}