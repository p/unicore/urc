package de.fzj.unicore.rcp.servicebrowser.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.servicebrowser.actions.UserInputRequest;

/**
 * Dialog for setting the parameters to a CreateTSS operation
 * 
 * TODO 
 * the available keys/descriptions should be passed as 
 * parameters to this dialog 
 * 
 * @author bengi
 * @author schuller
 */
public class CreateTSSDialog extends TitleAreaDialog {

	private final Map<String,Text>fields = new HashMap<String,Text>();

	private List<String> keys = new ArrayList<String>();
	private List<String> values = new ArrayList<String>();

	private final List<UserInputRequest> userInputRequests;
	
	public CreateTSSDialog(Shell parentShell,
			List<UserInputRequest> userInputRequests) {
		super(parentShell);
		this.userInputRequests = userInputRequests;
	}

	@Override
	public void create() {
		super.create();
		setTitle("Target system provider properties");
		setMessage("Please fill the information below.",
				IMessageProvider.INFORMATION);
	}
	
	public Map<String,String>getUserInput(){
		Map<String,String>userInput = new HashMap<String, String>();
		for (int i = 0; i < keys.size(); i++) {
			userInput.put(keys.get(i), values.get(i));
		}
		return userInput;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));
		GridLayout layout = new GridLayout(2, false);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		container.setLayout(layout);
		for (UserInputRequest userInputRequest : userInputRequests) {
			createField(container, userInputRequest.getKey(),
					userInputRequest.getDescription());
		}
		return area;
	}

	private void createField(Composite container, String key, String description) {
		Label label = new Label(container, SWT.NONE);
		label.setText(description);
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		Text field = new Text(container, SWT.BORDER);
		field.setLayoutData(gridData);
		fields.put(key, field);
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	// We need to save the values of the Text fields into Strings because the UI
	// gets disposed and the Text fields are not accessible any more.
	private void saveInput() {
		for(int i=0;i<keys.size();i++){
			values.set(i, fields.get(keys.get(i)).getText());
		}
	}

	@Override
	protected void okPressed() {
		saveInput();
		super.okPressed();
	}

}