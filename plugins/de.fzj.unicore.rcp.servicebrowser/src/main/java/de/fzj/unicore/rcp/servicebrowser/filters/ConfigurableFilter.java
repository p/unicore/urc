package de.fzj.unicore.rcp.servicebrowser.filters;

import org.eclipse.jface.viewers.Viewer;

import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.RootNode;

/**
 * A {@link ContentFilter} that can be configured with {@link FilterCriteria}.
 * {@link FilterCriteria} can be set and reset during lifetime, such that
 * the concrete behavior of this filter is mutable.
 *
 */
public class ConfigurableFilter extends SkipNodeViewerFilter {

	private FilterCriteria fc = null;

	public ConfigurableFilter(String _id) {
		this(_id, true);
	}

	public ConfigurableFilter(String _id, boolean active) {
		this(_id, active,null);
	}
	
	public ConfigurableFilter(String _id, boolean active, FilterCriteria criteria) {
		super(_id, active);
		setGridFilter(criteria);
	}

	public FilterCriteria getGridFilter() {
		return fc;
	}

	public void resetFilters() {
		this.fc = null;
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if (!isActive() || element instanceof RootNode
				|| element instanceof GridNode || !(element instanceof Node)) {
			return true;
		}
		return fc == null ? true : fc.showElement((Node) element);
	}

	public void setGridFilter(FilterCriteria fc) {
		this.resetFilters();
		if (fc != null) {
			this.fc = fc;
		}
	}

}
