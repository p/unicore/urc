package de.fzj.unicore.rcp.servicebrowser.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;

/**
 * Class used to initialize default preference values.
 */
public class GridBrowserPreferenceInitializer extends
		AbstractPreferenceInitializer {

	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = ServiceBrowserActivator.getDefault()
				.getPreferenceStore();
		store.setDefault(ServiceBrowserConstants.P_NUM_LEVELS_ON_STARTUP, 3);
		store.setDefault(ServiceBrowserConstants.P_HIDE_FAILED_SERIVCES, false);
		store.setDefault(ServiceBrowserConstants.P_HIDE_ACCESS_DENIED_SERIVCES,
				true);
		store.setDefault(
				ServiceBrowserConstants.P_CHUNK_SIZE_LAZY_CHILD_RETRIEVAL, 100);
		store.setDefault(
				ServiceBrowserConstants.P_NUM_PERSISTED_FILE_NODES, 5000);
		store.setDefault(ServiceBrowserConstants.P_SIZE_UNITS_SI, false);
	}

}
