/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.servicebrowser.actions.AddToSiteListAction;
import eu.unicore.security.wsutil.client.authn.DelegationSpecification;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * @author demuth
 */
public class SecuredNode extends Node {

	private static final long serialVersionUID = -9054120235101829328L;

	public SecuredNode(EndpointReferenceType epr) {
		super(epr);
	}

	public IClientConfiguration getUASSecProps() throws Exception {
		return getUASSecProps(getURI());
	}

	public IClientConfiguration getUASSecProps(URI uri) throws Exception {

		IClientConfiguration secProps = IdentityActivator.getDefault()
		.getClientConfiguration(getEpr(), DelegationSpecification.STANDARD);

		// inherit security properties from parent in case no specific sec props
		// are set for this node
		if (Boolean.parseBoolean(String.valueOf(secProps.getExtraSecurityTokens().get(IdentityActivator.DEFAULT_PROPERTIES)))) {
			if (namePathToRoot != null) {
				// somebody has explicitly set the namePathToRoot so that we
				// don't have to search for a match
				secProps = IdentityActivator.getDefault()
						.getClientConfiguration(namePathToRoot, getEpr(), DelegationSpecification.STANDARD);
			} else {
				EndpointReferenceType epr = EndpointReferenceType.Factory
				.newInstance();
				epr.addNewAddress().setStringValue(uri.toString());
				List<String> namepaths = getCache().getNodeData(epr)
				.getAllNamePathsToRoot();
				secProps = IdentityActivator.getDefault()
						.getClientConfiguration(namepaths, getEpr(), DelegationSpecification.STANDARD);
			}

		}

		return secProps;
	}

	@Override
	public DetailsAdapter getDetails() {
		DetailsAdapter details = super.getDetails();
		Map<String, String> hm = details.getMap();
		EndpointReferenceType epr = getEpr();
		if(epr != null)
		{
			String serverId = Utils.extractServerIDFromEPR(epr);
			if (serverId != null) {
				hm.put("Server identity", serverId);
			}
		}
		return details;
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		getAvailableActions().add(new AddToSiteListAction(this));
	}

}
