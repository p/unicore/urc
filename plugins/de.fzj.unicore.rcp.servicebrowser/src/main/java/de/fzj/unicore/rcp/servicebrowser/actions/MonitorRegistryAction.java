/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;
import de.fzj.unicore.rcp.servicebrowser.ui.NewRegistryFrame;

/**
 * @author demuth
 * 
 */
public class MonitorRegistryAction extends ServiceBrowserAction {

	public MonitorRegistryAction() {
		this(null);
	}

	public MonitorRegistryAction(IServiceViewer viewer) {
		setViewer(viewer);
		setText("Add Registry");
		setToolTipText("Add a new bookmark for a registry");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("registry.png"));

	}

	/**
	 * Open a "Monitor Registry" Frame
	 */
	protected void popupMonitorRegistry() {
		setEnabled(false);
		try {

			Shell s = null;
			if (getViewer() != null) {
				s = getViewer().getControl().getShell();
			} else {
				try {
					s = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getShell();
				} catch (Exception e) {

				}
			}
			if (s == null) {
				s = new Shell();
			}
			final NewRegistryFrame r = new NewRegistryFrame(s);
			int result = r.open();

			if (Window.OK == result) {
				Job j = new BackgroundJob("adding registry") {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						final Node so = NodeFactory.createNode(r.getEpr());
						so.setName(r.getName());
						so.setFallbackEpr(r.getFallbackEpr());
						if (getViewer() == null) {
							try {
								ServiceBrowserActivator.getDefault()
										.getGridNode().addBookmark(so);
							} catch (Exception e) {
								ServiceBrowserActivator.log(IStatus.ERROR, "Unable to add registry", e);
							}
						} else {
							getViewer().addMonitoringEntry(so);
						}
						return Status.OK_STATUS;
					}

				};
				j.schedule();
			}
		} finally {
			setEnabled(true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.actions.ServiceBrowserAction#run()
	 */
	@Override
	public void run() {
		popupMonitorRegistry();
	}

}
