/*********************************************************************************
 * Copyright (c) 2015 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.metadata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.part.ViewPart;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceContentProvider;
import de.fzj.unicore.rcp.servicebrowser.filters.ConfigurableFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.ContentFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.DisjunctiveContentFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterController;
import de.fzj.unicore.rcp.servicebrowser.filters.MetadataSearchFileFilterCriteria;
import de.fzj.unicore.rcp.servicebrowser.filters.NodeTypeViewerFilter;
import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.RootNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;
import de.fzj.unicore.rcp.servicebrowser.ui.DefaultFilterSet;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceSelectionPanel;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceTreeViewer;
import de.fzj.unicore.uas.client.MetadataClient;

public class MetadataSearchViewPart extends ViewPart {
	public static final String ID = "de.fzj.unicore.rcp.servicebrowser.metadata.search";

	private Text txtAllWords;
	private Text txtAnyWord;
	private Text txtExactWords;
	private Text txtSimilar;
	private Text txtUnwanted;
	
	private ServiceSelectionPanel resultList;

	public MetadataSearchViewPart() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(new GridLayout(1, false));

		SashForm sashForm = new SashForm(parent, SWT.NONE);
		sashForm.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false,
				1, 1));

		Composite mdServices = new Composite(sashForm, SWT.NONE);
		GridLayout mdsLayout = new GridLayout();
		mdServices.setLayout(mdsLayout);
		mdServices.setLayoutData(new GridData(1, 1, true, true));

		ScrolledComposite scrolledComposite = new ScrolledComposite(mdServices, SWT.V_SCROLL | SWT.H_SCROLL);
		scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		scrolledComposite.setLayout(new FillLayout());
		scrolledComposite.setExpandVertical(false);
		scrolledComposite.setExpandHorizontal(true);

		final ServiceTreeViewer serviceTreeViewer = new ServiceTreeViewer(
				scrolledComposite, SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL);
		serviceTreeViewer.getTree().setLayoutData(
				new GridData(SWT.FILL, SWT.FILL, true, true));
		serviceTreeViewer.setContentProvider(new ServiceContentProvider());
		GridNode gridNode = (GridNode) ServiceBrowserActivator.getDefault()
				.getGridNode().clone(3);
		RootNode rootNode = new RootNode();
		rootNode.addChild(gridNode);
		serviceTreeViewer.setGridNode(gridNode);
		serviceTreeViewer.setSorter(null);
		FilterController filterController = new FilterController();
		filterController.addViewer(serviceTreeViewer);
		DefaultFilterSet filterSet = new DefaultFilterSet(filterController);
		filterSet.filterNodesByType(Collections
				.unmodifiableSet(new HashSet<String>(Arrays
						.asList(new String[] { StorageNode.TYPE }))));
		serviceTreeViewer.setExpandedLevels(4);
		serviceTreeViewer.setShowingContextMenu(false);

		scrolledComposite.setContent(serviceTreeViewer.getControl());
		Point point = serviceTreeViewer.getControl().computeSize(SWT.DEFAULT, 200);
		serviceTreeViewer.getControl().setSize(point);
		scrolledComposite.setMinSize(point);

		Composite queryComposite = new Composite(sashForm, SWT.NONE);
		queryComposite.setLayout(new GridLayout(2, false));

		Label lblAllWords = new Label(queryComposite, SWT.NONE);
		lblAllWords.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblAllWords.setText("All words");

		txtAllWords = new Text(queryComposite, SWT.BORDER);
		txtAllWords.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblAnyWord = new Label(queryComposite, SWT.NONE);
		lblAnyWord.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblAnyWord.setText("Any word");

		txtAnyWord = new Text(queryComposite, SWT.BORDER);
		txtAnyWord.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblExactWords = new Label(queryComposite, SWT.NONE);
		lblExactWords.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblExactWords.setText("Exact words");

		txtExactWords = new Text(queryComposite, SWT.BORDER);
		txtExactWords.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblSimilarLabel = new Label(queryComposite, SWT.NONE);
		lblSimilarLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER,
				false, false, 1, 1));
		lblSimilarLabel.setText("Similar words");

		txtSimilar = new Text(queryComposite, SWT.BORDER);
		txtSimilar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Label lblUnwanted = new Label(queryComposite, SWT.NONE);
		lblUnwanted.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblUnwanted.setText("Unwanted words");

		txtUnwanted = new Text(queryComposite, SWT.BORDER);
		txtUnwanted.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		Button btnSearch = new Button(queryComposite, SWT.NONE);
		btnSearch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				List<IAdaptable> mdSearchServices = serviceTreeViewer
						.getSelectedNodes();
				resultList.getTreeViewer().resetFilters();
				resultList.getTreeViewer().setFilters(
						new ViewerFilter[] { new NodeTypeViewerFilter("",
								new HashSet<String>(Arrays.asList(new String[] {
										FileNode.TYPE, FolderNode.TYPE, RegistryNode.TYPE,
										StorageNode.TYPE, TargetSystemNode.TYPE,
										TargetSystemFactoryNode.TYPE }))) });
				List<ContentFilter>	fileFilterList = new ArrayList<ContentFilter>();
				for (IAdaptable iAdaptable : mdSearchServices) {
					if(!(iAdaptable instanceof StorageNode)) {
						continue;
					}
					StorageNode storage = (StorageNode) iAdaptable
							.getAdapter(StorageNode.class);
					try {
						MetadataClient mdClient = storage.getStorageClient()
								.getMetadataClient();
						Collection<String> matches = mdClient.search(
								AdvancedSearchHelper.makeQuery(
										txtAllWords.getText(),
										txtAnyWord.getText(),
										txtExactWords.getText(),
										txtSimilar.getText(),
										txtUnwanted.getText()), true);
						fileFilterList.addAll(updateResults(matches,
								storage.getURI().toString()));
					} catch (Exception e1) {
						ServiceBrowserActivator.log(
								"Unable to create metadata service client for "
										+ storage.getURL().toString(), e1);
					}
				}
				resultList.getTreeViewer().addFilter(
						new DisjunctiveContentFilter("MD search filter",
								fileFilterList));
			}

			private List<ContentFilter> updateResults(Collection<String> matches,
					String _storageURI) {

				List<ContentFilter> contentFilterList = new ArrayList<ContentFilter>();

				if(!matches.isEmpty()) {
				
					// filters the tree and shows only search results
					for (String searchResult : matches) {
						MetadataSearchFileFilterCriteria fileFilter = new MetadataSearchFileFilterCriteria(
								_storageURI, searchResult);
						fileFilter.setAlwaysShowRegistries(true);
						fileFilter.setShowAllChildNodes(false);
						fileFilter.setShowDirectChildNodes(false);
						ConfigurableFilter configFilter = new ConfigurableFilter("FileID " + searchResult);
						configFilter.setGridFilter(fileFilter);
						contentFilterList.add(configFilter);
					}
					
				}
				return contentFilterList;
			}
		});
		btnSearch.setText("Search");

		Button btnReset = new Button(queryComposite, SWT.NONE);
		btnReset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				txtAllWords.setText("");
				txtAnyWord.setText("");
				txtExactWords.setText("");
				txtSimilar.setText("");
				txtUnwanted.setText("");
			}
		});
		btnReset.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		btnReset.setText("Reset");
		sashForm.setWeights(new int[] { 1, 1 });

		Composite resultComposite = new Composite(parent, SWT.NONE);
		resultComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1));
		resultComposite.setLayout(new GridLayout(1, false));

		resultList = new ServiceSelectionPanel(resultComposite,
				Collections.singleton(FileNode.TYPE));
		TreeViewer treeViewer = resultList.getTreeViewer();
		Tree tree = treeViewer.getTree();
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		resultComposite.pack();
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
	}
}
