package de.fzj.unicore.rcp.servicebrowser.dnd;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.EvaluationContext;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.ui.ISources;
import org.eclipse.ui.PlatformUI;
import org.unigrids.services.atomic.types.GridFileType;

import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class PasteFileNodesInContainerHandler extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public PasteFileNodesInContainerHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Object result = null;
		try {
			EvaluationContext c = (EvaluationContext) event
					.getApplicationContext();
			Object var = c.getVariable(ISources.ACTIVE_CURRENT_SELECTION_NAME);
			if (!(var instanceof IStructuredSelection)) {
				return result;
			}
			IStructuredSelection selection = (IStructuredSelection) var;
			if (selection.size() != 1) {
				return result;
			}
			Object first = selection.getFirstElement();
			if (!(first instanceof IAdaptable)) {
				return result;
			}
			final IContainer container = (IContainer) ((IAdaptable) first)
					.getAdapter(IResource.class);
			if (container == null) {
				return result;
			}
			Clipboard clipboard = new Clipboard(PlatformUI.getWorkbench()
					.getDisplay());
			NodeLocalSelectionContents contents = (NodeLocalSelectionContents) clipboard
					.getContents(NodeLocalSelectionTransfer.getTransfer());
			clipboard.dispose();
			if (contents == null) {
				return result;
			}
			final IStructuredSelection toPaste = (IStructuredSelection) contents
					.getSelection();
			if (toPaste == null) {
				return result;
			}
			for (Object o : toPaste.toArray()) {
				if (o instanceof AbstractFileNode) {
					final AbstractFileNode node = (AbstractFileNode) o;

					Job j = new BackgroundJob("") {
						@Override
						protected IStatus run(IProgressMonitor monitor) {
							monitor.beginTask(
									"Downloading remote file content", 100);
							try {
								IPath p = container.getFile(
										new Path(node.getName())).getLocation();
								GridFileType gft = node.getGridFileType();
								if (gft == null) {
									return Status.CANCEL_STATUS;
								}
								SubProgressMonitor sub = new SubProgressMonitor(
										monitor, 90);
								if (node instanceof FileNode) {
									UnicoreStorageTools.downloadFile(
											node.getStorageClient(),
											gft.getPath(), p.toFile(), sub);
								} else if (node instanceof FolderNode) {
									UnicoreStorageTools.downloadFolder(
											node.getStorageClient(),
											gft.getPath(), p.toFile(), sub);
								}
								container.refreshLocal(1,
										new SubProgressMonitor(monitor, 10));
							} catch (Exception e) {
								ServiceBrowserActivator.log(IStatus.ERROR, "Unable to download remote file content: ", e);
							} finally {
								monitor.done();
							}
							return Status.OK_STATUS;

						}
					};
					j.schedule();
				}

			}
		} catch (Exception e) {
			// TODO: handle exception

		}
		return result;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
