package de.fzj.unicore.rcp.servicebrowser.filters;

import java.util.regex.Pattern;

import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

public class NameFilterCriteria extends StandardFilterCriteria {

	private final Pattern compiledPattern;

	public NameFilterCriteria(String _pattern) {
		if(_pattern != null && !_pattern.trim().isEmpty()) {
			compiledPattern = Pattern.compile(_pattern);
		} else {
			compiledPattern = null;
		}
	}

	@Override
	public boolean fitCriteria(Node node) {

		// check Name against entered criteria
		if (compiledPattern == null) {
			return true;
		}

		return compiledPattern.matcher(node.getName()).matches();
	}

}
