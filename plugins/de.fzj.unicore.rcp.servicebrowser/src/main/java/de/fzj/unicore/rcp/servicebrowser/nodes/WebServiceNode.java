/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.runtime.IStatus;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.adapters.WebServiceAdapter;
import de.fzj.unicore.rcp.identity.IdentityActivator;

public class WebServiceNode extends SecuredNode {

	private static final long serialVersionUID = -1792635317990857467L;

	public WebServiceNode(EndpointReferenceType epr) {
		super(epr);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class key) {
		if (key.getName().equals(WebServiceAdapter.class.getName())) {
			try {
				return new WebServiceAdapter(getEpr(), getUASSecProps());
			} catch (Exception e) {
				IdentityActivator.log(IStatus.ERROR, "", e);
				return null;
			}

		} else {
			return super.getAdapter(key);
		}
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return "unknown web service";
	}

	public URL getURL() {
		try {
			return new URL(getURI().toString());
		} catch (MalformedURLException e) {
			return null;
		}
	}
	
	/**
	 * Get fallback URL if available
	 * 
	 * @return fallback URL or <code>null</code>
	 */
	public URL getFallbackURL() {
		try {
			return new URL(getFallbackURI().toString());
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
	}

}