package de.fzj.unicore.rcp.servicebrowser.filters;

import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
/**
 * 
 * @author rajveer
 *
 * filters on the basis of a given file URI and reveals the parent nodes
 */
public class MetadataSearchFileFilterCriteria extends StandardFilterCriteria {

	String fileAddress;
	String storageURI;

	public MetadataSearchFileFilterCriteria(String storageURI, String fileAddress) {
		
		this.fileAddress = fileAddress;
		this.storageURI = storageURI;

	}

	@Override
	public boolean fitCriteria(Node node) {

		// Check file address against entered criteria
		if (fileAddress == null || fileAddress.trim().length() == 0) {
			return true;
			// reveals the parent storage node	
		} else if(node instanceof StorageNode) {
			StorageNode storage = (StorageNode) node;
			return storage.getURI().toString().equals(storageURI);
		} else if(node instanceof FolderNode) {
			FolderNode folder = (FolderNode) node;
			return folder.getStorageEPR().getAddress().getStringValue().equals(storageURI) &&
					fileAddress.startsWith(folder.getGridFileType().getPath());
		} else if(node instanceof FileNode) {
			FileNode file = (FileNode) node;
			return file.getGridFileType().getPath().equals(fileAddress) &&
					file.getStorageEPR().getAddress().getStringValue().equals(storageURI);
		}
		
		return false;
	}
}


