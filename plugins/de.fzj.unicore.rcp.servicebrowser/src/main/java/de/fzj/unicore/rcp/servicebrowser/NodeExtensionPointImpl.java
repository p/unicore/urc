/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.servicebrowser;

import java.net.URI;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.swt.graphics.Image;
import org.unigrids.services.atomic.types.StatusType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.UAS;
import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.JobNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.MoreFilesNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.RootNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.GroupNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.MoreNode;
import de.fzj.unicore.uas.JobManagement;
import de.fzj.unicore.uas.StorageFactory;
import de.fzj.unicore.uas.StorageManagement;
import de.fzj.unicore.uas.TargetSystem;
import de.fzj.unicore.uas.TargetSystemFactory;
import de.fzj.unicore.wsrflite.xmlbeans.sg.Registry;

/**
 * 
 * @author hmersch, bdemuth
 * 
 */
public class NodeExtensionPointImpl implements INodeExtensionPoint {

	private Image fileImage = ServiceBrowserActivator.getImageDescriptor(
			"file.png").createImage();
	private Image folderImage = ServiceBrowserActivator.getImageDescriptor(
			"folder.png").createImage();
	private Image moreImage = ServiceBrowserActivator.getImageDescriptor(
			"more.png").createImage();
	private Image storageFactoryImage = ServiceBrowserActivator
			.getImageDescriptor("database_add.png").createImage();
	private Image storageImage = ServiceBrowserActivator.getImageDescriptor(
			"database.png").createImage();
	private Image gridImage = ServiceBrowserActivator.getImageDescriptor(
			"topList.png").createImage();
	private Image registryImage = ServiceBrowserActivator.getImageDescriptor(
			"registry.png").createImage();
	private Image tssImage = ServiceBrowserActivator.getImageDescriptor(
			"tss.png").createImage();
	private Image tsfImage = ServiceBrowserActivator.getImageDescriptor(
			"tsf.png").createImage();
	private Image jobSuccessfulImage = ServiceBrowserActivator
			.getImageDescriptor("jobSuccessful.png").createImage();
	private Image jobFailedImage = ServiceBrowserActivator.getImageDescriptor(
			"jobFailed.png").createImage();
	private Image jobQueuedImage = ServiceBrowserActivator.getImageDescriptor(
			"jobQueued.png").createImage();
	private Image jobRunningImage = ServiceBrowserActivator.getImageDescriptor(
			"jobRunning.png").createImage();
	private Image jobUnknownImage = ServiceBrowserActivator.getImageDescriptor(
			"jobUnknown.png").createImage();
	private Image jobsImage = ServiceBrowserActivator.getImageDescriptor(
			"jobs.png").createImage();

	protected String rootNodeType = RootNode.getPortType().toString(),
			gridNodeType = GridNode.getPortType().toString(),
			registryNodeType = UAS.REG, tsfNodeType = UAS.TSF,
			tssNodeType = UAS.TSS, jobNodeType = UAS.JMS,
			storageNodeType = UAS.SMS, storageFactoryNodeType = UAS.SMF,
			fileNodeType = FileNode.TYPE, folderNodeType = FolderNode.TYPE;

	public NodeExtensionPointImpl() {

	}

	public void dispose() {
		fileImage.dispose();
		folderImage.dispose();
		storageFactoryImage.dispose();
		storageImage.dispose();
		gridImage.dispose();
		registryImage.dispose();
		tssImage.dispose();
		tsfImage.dispose();
		jobsImage.dispose();
		jobSuccessfulImage.dispose();
		jobFailedImage.dispose();
		jobQueuedImage.dispose();
		jobRunningImage.dispose();
		jobUnknownImage.dispose();
	}

	@Override
	public void finalize() {
		dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.extensionpoints.IServiceExtensionPoint
	 * #getImage(de.fzj.unicore.rcp.servicebrowser.serviceNodes.ServiceObject)
	 */
	public Image getImage(Node node) {

		if (node instanceof FileNode) {
			return fileImage;
		} else if (node instanceof MoreFilesNode) {
			return moreImage;
		} else if (node instanceof FolderNode) {
			return folderImage;
		} else if (node instanceof StorageFactoryNode) {
			return storageFactoryImage;
		} else if (node instanceof StorageNode) {
			return storageImage;
		} else if (node instanceof GridNode) {
			return gridImage;
		} else if (node instanceof RegistryNode) {
			return registryImage;
		} else if (node instanceof TargetSystemNode) {
			return tssImage;
		} else if (node instanceof TargetSystemFactoryNode) {
			return tsfImage;
		} else if (node instanceof MoreNode) {
			return moreImage;
		} else if (node instanceof GroupNode) {
			GroupNode group = (GroupNode) node;
			if (JobNode.TYPE.equals(group.getChildType())) {
				return jobsImage;
			}
			return null;

		} else if (node instanceof JobNode) {
			JobNode job = (JobNode) node;
			StatusType.Enum status = job.getJobStatus();
			if (StatusType.SUCCESSFUL.equals(status)) {
				return jobSuccessfulImage;
			} else if (StatusType.FAILED.equals(status)) {
				return jobFailedImage;
			} else if (StatusType.STAGINGIN.equals(status)
					|| StatusType.READY.equals(status)
					|| StatusType.QUEUED.equals(status)) {
				return jobQueuedImage;
			} else if (StatusType.RUNNING.equals(status)
					|| StatusType.STAGINGOUT.equals(status)) {
				return jobRunningImage;
			} else {
				return jobUnknownImage;
			}
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeExtensionPoint
	 * #getNode(org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public Node getNode(EndpointReferenceType epr) {

		if (epr == null) {
			return null;
		}

		QName type = Utils.InterfaceNameFromEPR(epr);

		if (type == null) {
			return null;
		}

		if (RootNode.getPortType().equals(type)) {
			return new RootNode();
		}
		if (GridNode.getPortType().equals(type)) {
			return new GridNode();
		} else if (Registry.REGISTRY_PORT.equals(type)) {
			return new RegistryNode(epr);
		} else if (TargetSystemFactory.TSF_PORT.equals(type)) {
			return new TargetSystemFactoryNode(epr);
		} else if (TargetSystem.TSS_PORT.equals(type)) {
			return new TargetSystemNode(epr);
		} else if (JobManagement.JMS_PORT.equals(type)) {
			return new JobNode(epr);
		} else if (StorageFactory.SMF_PORT.equals(type)) {
			return new StorageFactoryNode(epr);
		} else if (StorageNode.PORTTYPE.equals(type)) {
			return new StorageNode(epr);
		} else if (FileNode.getPortType().equals(type)) {
			return new FileNode(epr);
		} else if (FolderNode.getPortType().equals(type)) {
			return new FolderNode(epr);
		} else if (MoreFilesNode.getPortType().equals(type)) {
			return new MoreFilesNode(epr);
		} else if (GroupNode.getPortType().equals(type)) {
			return new GroupNode(epr);
		} else if (MoreNode.getPortType().equals(type)) {
			return new MoreNode(epr);
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.extensionpoints.IServiceExtensionPoint
	 * #getServiceModel(org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public Node getNode(URI uri) {
		if (uri == null) {
			return null;
		}

		String type = Util.determineTypeOfURI(uri);
		EndpointReferenceType epr = Utils.newEPR();
		epr.addNewAddress().setStringValue(uri.toString());
		if (registryNodeType.equals(type)) {
			Utils.addPortType(epr, Registry.REGISTRY_PORT);
		} else if (gridNodeType.equals(type)) {
			Utils.addPortType(epr, GridNode.getPortType());
		} else if (tsfNodeType.equals(type)) {
			Utils.addPortType(epr, TargetSystemFactory.TSF_PORT);
		} else if (tssNodeType.equals(type)) {
			Utils.addPortType(epr, TargetSystem.TSS_PORT);
		} else if (jobNodeType.equals(type)) {
			Utils.addPortType(epr, JobManagement.JMS_PORT);
		} else if (storageNodeType.equals(type)) {
			Utils.addPortType(epr, StorageManagement.SMS_PORT);
		} else if (storageFactoryNodeType.equals(type)) {
			Utils.addPortType(epr, StorageFactory.SMF_PORT);
		} else if (fileNodeType.equals(type)) {
			Utils.addPortType(epr, FileNode.getPortType());
		} else if (folderNodeType.equals(type)) {
			Utils.addPortType(epr, FolderNode.getPortType());
		} else {
			return null;
		}

		return getNode(epr);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.extensionpoints.IServiceExtensionPoint
	 * #getText(de.fzj.unicore.rcp.servicebrowser.serviceNodes.ServiceObject)
	 */
	public String getText(Node node) {
		// use default text for all my nodes: getName()
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.IExecutableExtension#setInitializationData(org
	 * .eclipse.core.runtime.IConfigurationElement, java.lang.String,
	 * java.lang.Object)
	 */
	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
		// TODO Auto-generated method stub

	}

}
