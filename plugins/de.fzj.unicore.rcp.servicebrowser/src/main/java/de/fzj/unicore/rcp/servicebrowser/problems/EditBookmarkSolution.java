package de.fzj.unicore.rcp.servicebrowser.problems;

import java.net.URI;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.ui.PlatformUI;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.logmonitor.problems.GraphicalSolution;
import de.fzj.unicore.rcp.servicebrowser.actions.EditRegistryUrlDialog;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import eu.unicore.problemutil.Problem;

public class EditBookmarkSolution extends GraphicalSolution {
	
	/**
	 * 
	 */
	public EditBookmarkSolution() {
		super("EDIT_BOOKMARK_SOLUTIONS", "VSITE_NOT_REGISTERED", "Edit bookmark...");
	}

	public EditBookmarkSolution(String id, String applicability, String description) {
		super(id, applicability, description);
	}

	public EditBookmarkSolution(String id, String[] applicability, String description) {
		super(id, applicability, description);
	}

	public EditBookmarkSolution(String id, String[] applicability, String description, String[] tags) {
		super(id, applicability, description, tags);
	}

	@Override
	public boolean solve(String message, Problem[] problems, String details) throws Exception {
		Node registry = getRegistryNodeForPath(message);
		if(registry != null) {
			EditRegistryUrlDialog editReg = new EditRegistryUrlDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(), SWT.APPLICATION_MODAL);
			URI uri = registry.getURI();
			if(uri != null) {
				editReg.setUrl(uri.toString());
			}
			URI fbUrl = registry.getFallbackURI();
			if(fbUrl != null) {
				editReg.setFbUrl(fbUrl.toString());
			}
			
			if(editReg.open() == MessageDialog.OK) {
				final EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
				epr.addNewAddress().setStringValue(editReg.getUrl());
				registry.setEpr(epr);
				final EndpointReferenceType fbEpr = EndpointReferenceType.Factory.newInstance();
				fbEpr.addNewAddress().setStringValue(editReg.getFbUrl());
				registry.setFallbackEpr(fbEpr);
				
				registry.asyncRefresh(1, true);
			}
			
			return true;
		}
		return false;
	}

	/**
	 * @param details
	 * @return
	 */
	private Node getRegistryNodeForPath(String details) {
		Pattern p = Pattern.compile("Could not refresh resource properties of service ([^:]*):.*");
		Matcher m = p.matcher(details);
		if(m.matches() && m.groupCount() > 0) {
			String path = m.group(1);
			List<Node> nodes = NodeFactory.getNodesFor(path);
			if(nodes.size() == 1) {
				return nodes.get(0);
			}
		}
		
		return null;
	}

}
