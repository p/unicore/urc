package de.fzj.unicore.rcp.servicebrowser.dnd;

import org.eclipse.core.expressions.EvaluationContext;
import org.eclipse.core.expressions.EvaluationResult;
import org.eclipse.core.expressions.Expression;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.dnd.Transfer;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IDropAssistant;

public class DropAssistantDescriptor {
	private final IConfigurationElement element;
	private Transfer[] supportedTransferTypes;
	private Expression dropExpr;

	DropAssistantDescriptor(IConfigurationElement aConfigElement) {
		element = aConfigElement;
		init();
	}

	public IDropAssistant createDropAssistant() {

		try {
			return (IDropAssistant) element
					.createExecutableExtension(IDropAssistant.ATTRIBUTE_CLASS);
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.ERROR, "DropAssistant could not be created", e);
			return null;
		}
	}

	public Transfer[] getSupportedTransferTypes() {
		return supportedTransferTypes;
	}

	private void init() {
		IConfigurationElement[] children = element
				.getChildren(IDropAssistant.ELEMENT_POSSIBLE_DROP_TARGETS);
		if (children.length == 1) {
			dropExpr = new CustomAndExpression(children[0]);
		}
		IDropAssistant assistant = createDropAssistant();
		supportedTransferTypes = assistant.getSupportedTransferTypes();
	}

	/**
	 * 
	 * @param anElement
	 *            The element from the set of elements being dropped.
	 * @return True if the element matches the drop expression from the
	 *         extension.
	 */
	public boolean isDropElementSupported(Object anElement) {
		if (dropExpr != null && anElement != null) {
			try {
				EvaluationContext context = new EvaluationContext(null,
						anElement);
				context.setAllowPluginActivation(true);
				return dropExpr.evaluate(context) == EvaluationResult.TRUE;
			} catch (CoreException e) {
				ServiceBrowserActivator.log(IStatus.ERROR,
						"Unable to determin drop support for element " + anElement.toString(), e);
			}
		}
		return false;
	}

}
