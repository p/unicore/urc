package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.unigrids.x2006.x04.services.sms.PermissionsChangeModeType;
import org.unigrids.x2006.x04.services.sms.PermissionsClassType;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.ui.ChangeModeDialog;

public class ChangeModeAction extends NodeAction {

	protected PermissionsClassType.Enum permClass;
	protected String rwxUserPermissions, rwxGroupPermissions, rwxOthersPermissions;
	protected PermissionsChangeModeType.Enum userOperation, groupOperation, othersOperation;	
	protected boolean recursive, apply, rwxUserChanged, rwxGroupChanged, rwxOthersChanged;

	public ChangeModeAction(AbstractFileNode node) {
		super(node);
		setText("Change Mode");
		setToolTipText("Change permissions of the remote file.");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("file_permissions.png"));
	}

	@Override
	public void run() {
		final AbstractFileNode fileNode = (AbstractFileNode) getNode();
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {
				Shell s = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getShell();
				ChangeModeDialog dialog = new ChangeModeDialog(
						s, fileNode);
				int result = dialog.open();
				rwxUserChanged = dialog.arePermissionsChanged(PermissionsClassType.USER); 
				rwxGroupChanged = dialog.arePermissionsChanged(PermissionsClassType.GROUP); 
				rwxOthersChanged = dialog.arePermissionsChanged(PermissionsClassType.OTHER);
				apply = (Window.OK == result && (rwxUserChanged || rwxGroupChanged || rwxOthersChanged));
				if (apply)
				{
					recursive = dialog.getRecursiveFlag();			
					rwxUserPermissions = dialog.getNewPermissionsString(PermissionsClassType.USER);
					userOperation = dialog.getChangePermissionsOperation(PermissionsClassType.USER);
					rwxGroupPermissions = dialog.getNewPermissionsString(PermissionsClassType.GROUP);
					groupOperation = dialog.getChangePermissionsOperation(PermissionsClassType.GROUP);
					rwxOthersPermissions = dialog.getNewPermissionsString(PermissionsClassType.OTHER);
					othersOperation = dialog.getChangePermissionsOperation(PermissionsClassType.OTHER);
				}
			}
		});
		if (apply) {
			String message = ": ";
			try {
				message = " for user: ";
				if(rwxUserChanged) {
					
					fileNode.getStorageClient().changePermissions2(
							fileNode.getGridFileType().getPath(), 
							userOperation, PermissionsClassType.USER, rwxUserPermissions, recursive);						
				}
				message = " for group (user permissions were set): ";
				if(rwxGroupChanged) {
					
					fileNode.getStorageClient().changePermissions2(
							fileNode.getGridFileType().getPath(), 
							groupOperation, PermissionsClassType.GROUP, rwxGroupPermissions, recursive);
				}
				message = " for others (user and group permissions were set): ";
				if(rwxOthersChanged) {
					
					fileNode.getStorageClient().changePermissions2(
							fileNode.getGridFileType().getPath(), 
							othersOperation, PermissionsClassType.OTHER, rwxOthersPermissions, recursive); 
				}				
			} catch (Exception e) {
				ServiceBrowserActivator.log(
						IStatus.ERROR,
						"Unable to set remote file permissions" + message, e);
			}
			fileNode.refresh(0, false, null);
		}

	}

}
