/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser;

/**
 * @author demuth
 * 
 */
public class ServiceBrowserConstants {

	public static final String NAMESPACE = "de.fzj.unicore.rcp.servicebrowser";
	public static final String NODE_ACTION_EXTENSION_POINT = NAMESPACE
			+ ".NodeActionExtensionPoint";
	public static final String NODE_EXTENSION_POINT = NAMESPACE
			+ ".NodeExtensionPoint";
	public static final String GRIDBROWSER_FILTER_EXTENSION_POINT = NAMESPACE
			+ ".GridBrowserFilter";
	public static final String ADAPTABLE_EXTENSION_POINT = NAMESPACE
			+ ".AdaptableExtensionPoint";
	public static final String NODE_CACHE_LISTENER_EXTENSION_POINT = NAMESPACE
			+ ".NodeCacheListenerExtensionPoint";

	public static final int MODE_NORMAL_USER = 0;
	public static final int MODE_ADMIN = 1;

	public static final String P_HIDE_FAILED_SERIVCES = "hide failed services";

	public static final String P_HIDE_ACCESS_DENIED_SERIVCES = "hide access denied services";

	public static final String P_NUM_LEVELS_ON_STARTUP = "number of grid levels refreshed on startup";

	public static final String P_CHUNK_SIZE_LAZY_CHILD_RETRIEVAL = "chunk size lazy child retrieval";
	
	public static final String P_NUM_PERSISTED_FILE_NODES = "number of file/folder nodes to persist";

	public static final String FILENAME_PERSISTED_NODES = "nodes.xml";

	public static final String MSG_REFRESHING = "refreshing ";

	public static final String DIALOG_SETTINGS_MONITORED_ENTRY_NAMES = "MonitoredEntryName_";
	public static final String DIALOG_SETTINGS_MONITORED_ENTRY_EPRS = "MonitoredEntryURIs";
	public static final String P_SIZE_UNITS_SI = "sizes in SI units (base 1000)";

}
