package de.fzj.unicore.rcp.servicebrowser.filters;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

public abstract class TaskFilterConfiguration {
	
	protected static final SimpleDateFormat getDateFormat() {
		return new SimpleDateFormat("dd/MM/yyyy");
	}

	protected static final SimpleDateFormat getTimeFormat() {
		return new SimpleDateFormat("HH:mm:ss");
	}
	protected static final Calendar MIDNIGHT = Calendar.getInstance();
	protected static final Calendar BEFORE_MIDNIGHT = Calendar.getInstance();
	{

		MIDNIGHT.set(Calendar.HOUR_OF_DAY, 0);
		MIDNIGHT.set(Calendar.MINUTE, 0);
		MIDNIGHT.set(Calendar.SECOND, 0);

		BEFORE_MIDNIGHT.set(Calendar.HOUR_OF_DAY, 23);
		BEFORE_MIDNIGHT.set(Calendar.MINUTE, 59);
		BEFORE_MIDNIGHT.set(Calendar.SECOND, 59);
	}
	
	
	

	protected Shell platformDisplay;
	protected Button selectAll;
	protected Button deSelectAll;
	protected Button ok;
	protected Button cancel;
	protected boolean isClosing;
	
	
	protected String pattern = "";

	protected int statusMode;
	protected int timeMode;

	// time elements and variables
	protected Button optNoTime;
	protected Button optSubmissionTime;
	protected Button optTerminationTime;
	protected Button optBefore;
	protected Button optAfter;
	protected Button optExact;
	protected Button optBetween;
	protected Button checkTime1;
	protected Button checkTime2;
	protected Button btDate1SetToday;
	protected Button btTime1SetNow;
	protected Button btDate2SetToday;
	protected Button btTime2SetNow;

	protected Text txDate1;
	protected Text txTime1;
	
	protected Text textbox;
	
	protected Text txDate2;
	protected Text txTime2;
	
	private Label textLabel;

	private Label labDate1Desc, labTime1Desc;
	private Label labDate2Desc, labTime2Desc;
	private Label labDate1, labDate2;
	
	
	protected Calendar time1, time2;

	protected boolean tickTime1 = false;
	protected boolean tickTime2 = false;
	

	protected String strDate1 = "", strDate2 = "", strTime1 = "", strTime2 = "";

	
	public TaskFilterConfiguration() {
		statusMode = getDefaultStatusMode();
		timeMode = TaskFilterCriteria.int_noTime;
	}
	
	protected void activate() {
		if (optNoTime.getSelection()) {
			// None Time is selected -> disable all but optSubmissionTime and
			// optTerminationTime
			optAfter.setEnabled(false);
			optBefore.setEnabled(false);
			optExact.setEnabled(false);
			optBetween.setEnabled(false);

			txDate1.setEnabled(false);
			btDate1SetToday.setEnabled(false);
			checkTime1.setEnabled(false);

			txTime1.setEnabled(false);
			btTime1SetNow.setEnabled(false);

			txDate2.setEnabled(false);
			btDate2SetToday.setEnabled(false);
			checkTime2.setEnabled(false);

			txTime2.setEnabled(false);
			btTime2SetNow.setEnabled(false);
		} else {
			// Submission or TerminationTime is selected
			optAfter.setEnabled(true);
			optBefore.setEnabled(true);
			optExact.setEnabled(true);
			optBetween.setEnabled(true);

			// enalbe Date1
			txDate1.setEnabled(true);
			btDate1SetToday.setEnabled(true);
			checkTime1.setEnabled(true);

			if (checkTime1.getSelection()) {
				// checkbox time1 is selected
				txTime1.setEnabled(true);
				btTime1SetNow.setEnabled(true);
			} else {
				// checkbox time1 is not selected
				txTime1.setEnabled(false);
				btTime1SetNow.setEnabled(false);
			}
			if (!optBetween.getSelection()) {
				// after, before or exact is selected, date2 and time2 are not
				// necessary
				txDate2.setEnabled(false);
				btDate2SetToday.setEnabled(false);
				checkTime2.setEnabled(false);

				txTime2.setEnabled(false);
				btTime2SetNow.setEnabled(false);

			} else {
				// between is selected, enable date2
				txDate2.setEnabled(true);
				btDate2SetToday.setEnabled(true);
				checkTime2.setEnabled(true);
				if (checkTime2.getSelection()) {
					// time2 check is selected, activate time2
					txTime2.setEnabled(true);
					btTime2SetNow.setEnabled(true);
				} else {
					// time2 check is not selected, disable time2
					txTime2.setEnabled(false);
					btTime2SetNow.setEnabled(false);
				}
			}
		}
	}
	
	protected abstract void createStatusButtons(Composite statusPanel);
	
	public TaskFilterCriteria createTaskFilter() {
		Shell jobConfig = createUI();

		jobConfig.setActive();
		jobConfig.pack();
		jobConfig.open();
		Display display = PlatformUI.getWorkbench().getDisplay();
		while (!jobConfig.isDisposed()) {
			// process the next event, wait when none available
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

		if (time1 == null || time2 == null) {
			return null;
		}


		if (inTimeMode(JobFilterCriteria.int_after)) {
			if (!(tickTime1)) { // set 00:00:00
				time1.set(Calendar.HOUR_OF_DAY, 0);
				time1.set(Calendar.MINUTE, 0);
				time1.set(Calendar.SECOND, 0);
			}
		}

		if (inTimeMode(JobFilterCriteria.int_before)) {
			if (!(tickTime1)) { // set 00:00:00
				time1.set(Calendar.HOUR_OF_DAY, 0);
				time1.set(Calendar.MINUTE, 0);
				time1.set(Calendar.SECOND, 0);
			}
		}
		if (inTimeMode(JobFilterCriteria.int_between)) {
			if (!(tickTime1)) { // set 00:00:00
				time1.set(Calendar.HOUR_OF_DAY, 0);
				time1.set(Calendar.MINUTE, 0);
				time1.set(Calendar.SECOND, 0);
			}
			if (!(tickTime2)) { // set 00:00:00
				time2.set(Calendar.HOUR_OF_DAY, 0);
				time2.set(Calendar.MINUTE, 0);
				time2.set(Calendar.SECOND, 0);
			}

		}
		if (inTimeMode(JobFilterCriteria.int_exact)) {
			time2.setTime(time1.getTime());

			int seconds = time1.get(Calendar.SECOND);
			time1.set(Calendar.SECOND, seconds - 1);
			time2.set(Calendar.SECOND, seconds + 1);

		}
		return doCreateTaskFilter();
	}
	
	protected void createTimeArea(Composite parent)
	{
		
		GridLayout parentLayout = new GridLayout();
		parentLayout.numColumns = 1;
		parent.setLayout(parentLayout);
		
		
		Composite whichTimeGroup = new Composite(parent, SWT.NONE);
		GridLayout whichTimeGroupLayout = new GridLayout();
		whichTimeGroupLayout.numColumns = 4;
		whichTimeGroup.setLayout(whichTimeGroupLayout);

		SelectionAdapter selectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				activate();
			}
		};
		optNoTime = new Button(whichTimeGroup, SWT.RADIO);
		optNoTime.setText("No time restriction");
		optNoTime.setSelection(inTimeMode(JobFilterCriteria.int_noTime));

		optNoTime.addSelectionListener(selectionAdapter);

		optSubmissionTime = new Button(whichTimeGroup, SWT.RADIO);
		optSubmissionTime.setText("Submission Time");
		optSubmissionTime.setSelection(inTimeMode(JobFilterCriteria.int_submissionTime));
		optSubmissionTime.addSelectionListener(selectionAdapter);

		optTerminationTime = new Button(whichTimeGroup, SWT.RADIO);
		optTerminationTime.setText("Termination Time");
		optTerminationTime.setSelection(inTimeMode(JobFilterCriteria.int_terminationTime));
		optTerminationTime.addSelectionListener(selectionAdapter);

		Composite timeOperatorGroup = new Composite(parent, SWT.NONE);
		GridLayout timeOperatorGroupLayout = new GridLayout();
		timeOperatorGroupLayout.numColumns = 4;
		timeOperatorGroup.setLayout(timeOperatorGroupLayout);

		optExact = new Button(timeOperatorGroup, SWT.RADIO);
		optExact.setText("exact");
		optExact.setLocation(0, 0);
		optExact.setSelection(inTimeMode(JobFilterCriteria.int_exact));
		optExact.addSelectionListener(selectionAdapter);

		optBefore = new Button(timeOperatorGroup, SWT.RADIO);
		optBefore.setText("before");
		optBefore.setSelection(inTimeMode(JobFilterCriteria.int_before));
		optBefore.addSelectionListener(selectionAdapter);

		optAfter = new Button(timeOperatorGroup, SWT.RADIO);
		optAfter.setText("after");
		optAfter.setSelection(inTimeMode(JobFilterCriteria.int_after));
		optAfter.addSelectionListener(selectionAdapter);

		optBetween = new Button(timeOperatorGroup, SWT.RADIO);
		optBetween.setText("between Date1 and Date2");
		optBetween.setSelection(inTimeMode(JobFilterCriteria.int_between));
		optBetween.addSelectionListener(selectionAdapter);

		Composite date1Parent = new Composite(parent,SWT.NONE);
		GridLayout date1Layout = new GridLayout();
		date1Layout.numColumns = 4;
		date1Parent.setLayout(date1Layout);
		
		labDate1 = new Label(date1Parent, SWT.NONE);
		labDate1.setText("Date1:");

		labDate1Desc = new Label(date1Parent, SWT.NONE);
		labDate1Desc.setText("Enter dd/mm/yyyy");

		txDate1 = new Text(date1Parent, SWT.BORDER);
		txDate1.setText(strDate1);
		txDate1.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	
		btDate1SetToday = new Button(date1Parent, SWT.NONE);
		btDate1SetToday.setText("Set date today");
	

		btDate1SetToday
		.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Calendar c = Calendar.getInstance();
				txDate1.setText(getDateFormat().format(c.getTime()));
			}
		});

		checkTime1 = new Button(date1Parent, SWT.CHECK);
		checkTime1.setText("Time:");
		checkTime1.setSelection(tickTime1);
		checkTime1.addSelectionListener(selectionAdapter);

		labTime1Desc = new Label(date1Parent, SWT.NONE);
		labTime1Desc.setText("Enter hh:mm:ss");

		txTime1 = new Text(date1Parent, SWT.BORDER);
		txTime1.setText(strTime1);
		txTime1.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		
		btTime1SetNow = new Button(date1Parent, SWT.NONE);
		btTime1SetNow.setText("Set time now");
	
		btTime1SetNow
		.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Calendar c = Calendar.getInstance();
				txTime1.setText(getTimeFormat().format(c.getTime()));
			}
		});

		Composite date2Parent = new Composite(parent,SWT.NONE);
		GridLayout date2Layout = new GridLayout();
		date2Layout.numColumns = 4;
		date2Parent.setLayout(date2Layout);
		
		labDate2 = new Label(date2Parent, SWT.NONE);
		labDate2.setText("Date2:");

		labDate2Desc = new Label(date2Parent, SWT.NONE);
		labDate2Desc.setText(labDate1Desc.getText());

		txDate2 = new Text(date2Parent, SWT.BORDER);
		txDate2.setText(strDate2);
		txDate2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	
		btDate2SetToday = new Button(date2Parent, SWT.NONE);
		btDate2SetToday.setText("Set date today");
	

		btDate2SetToday
		.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Calendar c = Calendar.getInstance();
				txDate2.setText(getDateFormat().format(c.getTime()));
			}
		});

		checkTime2 = new Button(date2Parent, SWT.CHECK);
		checkTime2.setText(checkTime1.getText());
		checkTime2.setSelection(tickTime2);
		checkTime2.addSelectionListener(selectionAdapter);

		labTime2Desc = new Label(date2Parent, SWT.NONE);
		labTime2Desc.setText(labTime1Desc.getText());

		txTime2 = new Text(date2Parent, SWT.BORDER);
		txTime2.setText(strTime2);
		txTime2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		
		btTime2SetNow = new Button(date2Parent, SWT.NONE);
		btTime2SetNow.setText(btTime1SetNow.getText());
	
		btTime2SetNow
		.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Calendar c = Calendar.getInstance();
				txTime2.setText(getTimeFormat().format(c.getTime()));
			}
		});
	
	}
	
	
	/**
	 * creates the GUI Values of the GUI are stored to local variables for
	 * further work in the okButton selectionAction
	 * 
	 * @return
	 */
	protected Shell createUI() {
		platformDisplay = new Shell(PlatformUI.getWorkbench().getDisplay(),
				SWT.CLOSE | SWT.RESIZE | SWT.APPLICATION_MODAL);

		platformDisplay.setText("Configure Filter");

		GridLayout shellLayout = new GridLayout();

		shellLayout.makeColumnsEqualWidth = false;
		shellLayout.numColumns = 2;
		platformDisplay.setLayout(shellLayout);

		// Status buttons
		Group statusGroup = new Group(platformDisplay,SWT.BORDER);
		statusGroup.setText("Select desired status");
		GridLayout gl = new GridLayout(2,true);
		statusGroup.setLayout(gl);
		statusGroup.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		Composite statusButtonGroup = new Composite(statusGroup,SWT.NONE);
		statusButtonGroup.setLayout(new GridLayout(1,false));
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.horizontalSpan = 2;
		statusButtonGroup.setLayoutData(gd);

		createStatusButtons(statusButtonGroup);
		

		// time area
		Group timeGroup = new Group(platformDisplay,SWT.BORDER);
		timeGroup.setText("Select desired time frame");
		timeGroup.setLayoutData(new GridData(GridData.FILL_BOTH));
		createTimeArea(timeGroup);
		
		// button for selecting all status buttons
		selectAll = new Button(statusGroup, SWT.NONE);
		selectAll.setText("Select all");
		selectAll
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(
					org.eclipse.swt.events.SelectionEvent e) {
				selectAllStatusWidgets();
			}
		});
		
		// button for deselecting all status buttons
		deSelectAll = new Button(statusGroup, SWT.NONE);
		deSelectAll.setText("Deselect all");
		deSelectAll
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(
					org.eclipse.swt.events.SelectionEvent e) {
				deselectAllStatusWidgets();
			}
		});

		
		// Widgets for entering pattern
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		textLabel = new Label(platformDisplay, SWT.NONE);
		textLabel.setText("Match task name (regular expression):");
		textLabel.setLayoutData(gd);

		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		textbox = new Text(platformDisplay, SWT.BORDER);
		textbox.setText(pattern);
		textbox.setLayoutData(gd);

		
		// OK button
		ok = new Button(platformDisplay, SWT.NONE);
		ok.setText("OK");
		ok.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				okPressed();
			}
		});
		gd = new GridData();
		gd.widthHint = 150;
		ok.setLayoutData(gd);
		

		// Cancel button
		cancel = new Button(platformDisplay, SWT.NONE);
		cancel.setText("Cancel");

		cancel.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				doExit();
			}
		});
		
		gd = new GridData();
		gd.widthHint = 150;
		cancel.setLayoutData(gd);

		// react to shell closing
		platformDisplay
		.addShellListener(new org.eclipse.swt.events.ShellAdapter() {
			@Override
			public void shellClosed(org.eclipse.swt.events.ShellEvent e) {
				if (!isClosing) {
					e.doit = doExit();
				}
			}
		});
		activate();
		platformDisplay.layout();
		return platformDisplay;
	}
	
	protected abstract void deselectAllStatusWidgets();
	
	
	protected abstract TaskFilterCriteria doCreateTaskFilter();
	
	protected boolean doExit() {
		isClosing = true;
		platformDisplay.close();
		platformDisplay.dispose();
		return true;
	}
	
	protected abstract int getDefaultStatusMode();
	
	protected boolean inStatusMode(int bit)
	{
		return JobFilterCriteria.isSet(bit, statusMode);
	}
	
	protected boolean inTimeMode(int bit)
	{
		return JobFilterCriteria.isSet(bit, timeMode);
	}
	
	public void loadConfiguration(TaskFilterCriteria filter)
	{
		statusMode = filter.getStatusMode();
		timeMode = filter.getTimeMode();
		time1 = filter.getTime1();
		SimpleDateFormat df = getDateFormat();
		SimpleDateFormat tf = getTimeFormat();
		
		strDate1 = df.format(time1.getTime());
		
		String s1 = tf.format(time1.getTime());
		
		String midnight = tf.format(MIDNIGHT.getTime());
		
		String beforeMidnight = tf.format(BEFORE_MIDNIGHT.getTime());
		boolean time1Set = inTimeMode(JobFilterCriteria.int_exact) ? !beforeMidnight.equals(s1) : !midnight.equals(s1);
		if(time1Set)
		{
			strTime1 = s1;
			tickTime1 = true;
		}
		
		if(inTimeMode(JobFilterCriteria.int_between))
		{
			time2 = filter.getTime2();
			strDate2 = df.format(time2.getTime());

			String s2 = tf.format(time2.getTime());
			boolean time2Set = !midnight.equals(s2);

			if(time2Set)
			{
				strTime2 = s2;
				tickTime2 = true;
			}
		}
		pattern = filter.getPattern();
	}
	
	protected void okPressed()
	{
	


		pattern = textbox.getText();

		time1 = Calendar.getInstance();
		time2 = Calendar.getInstance();
		timeMode = 0;
		timeMode |= optSubmissionTime.getSelection() ? JobFilterCriteria.int_submissionTime : 0;
		timeMode |= optTerminationTime.getSelection() ? JobFilterCriteria.int_terminationTime : 0;
		timeMode |= optNoTime.getSelection() ? JobFilterCriteria.int_noTime : 0;
		timeMode |= optAfter.getSelection() ? JobFilterCriteria.int_after : 0;
		timeMode |= optBefore.getSelection() ? JobFilterCriteria.int_before : 0;
		timeMode |= optBetween.getSelection() ? JobFilterCriteria.int_between : 0;
		timeMode |= optExact.getSelection() ? JobFilterCriteria.int_exact : 0;

		tickTime1 = checkTime1.getSelection();
		tickTime2 = checkTime2.getSelection();

		// validation check of the entered values
		boolean invDate1 = false, invDate2 = false, invTime1 = false, invTime2 = false;

		int day1 = 0;
		int month1 = 0;
		int year1 = 0;
		int hour1 = 0;
		int minute1 = 0;
		int second1 = 0;

		int day2 = 0;
		int month2 = 0;
		int year2 = 0;
		int hour2 = 0;
		int minute2 = 0;
		int second2 = 0;

		if (txDate1.getEnabled()) {
			if (txDate1.getText() == "") {
				invDate1 = true;
			} else {
				try {
					day1 = Integer.parseInt(txDate1.getText()
							.substring(0, 2));
					month1 = Integer.parseInt(txDate1.getText()
							.substring(3, 5)) - 1;
					year1 = Integer.parseInt(txDate1.getText()
							.substring(6, 10));
				} catch (Exception e1) {
					invDate1 = true;
				}
			}
		}

		if (txTime1.getEnabled()) {
			if (txTime1.getText() == "") {
				invTime1 = true;
			} else {
				try {
					hour1 = Integer.parseInt(txTime1.getText()
							.substring(0, 2));
					minute1 = Integer.parseInt(txTime1.getText()
							.substring(3, 5));
					second1 = Integer.parseInt(txTime1.getText()
							.substring(6, 8));
				} catch (Exception e1) {
					invTime1 = true;
				}
			}
		}

		if (txDate2.getEnabled()) {
			if (txDate2.getText() == "") {
				invDate2 = true;
			} else {
				try {
					day2 = Integer.parseInt(txDate2.getText()
							.substring(0, 2));
					month2 = Integer.parseInt(txDate2.getText()
							.substring(3, 5)) - 1;
					year2 = Integer.parseInt(txDate2.getText()
							.substring(6, 10));
				} catch (Exception e1) {
					invDate2 = true;
				}
			}
		}

		if (txTime2.getEnabled()) {
			if (txTime2.getText() == "") {
				invTime2 = true;
			} else {
				try {
					hour2 = Integer.parseInt(txTime2.getText()
							.substring(0, 2));
					minute2 = Integer.parseInt(txTime2.getText()
							.substring(3, 5));
					second2 = Integer.parseInt(txTime2.getText()
							.substring(6, 8));
				} catch (Exception e1) {
					invTime2 = true;
				}
			}
		}
		boolean statusSet = statusMode != 0;

		if (!(invDate1 || invDate2 || invTime1 || invTime2 || !statusSet)) {
			// set the date/time values
			time1.set(year1, month1, day1, hour1, minute1, second1);
			time2.set(year2, month2, day2, hour2, minute2, second2);

			// check here for future dates
			// if (time1.before(now)) {
			strDate1 = txDate1.getText();
			strDate2 = txDate2.getText();
			strTime1 = txTime1.getText();
			strTime2 = txTime2.getText();

			doExit();
			// }

		} else {
			if (invDate1) {
				showWarningBox("Date 1 is not valid",
				"Please enter date in format: dd/mm/yyyy");
			}
			if (invTime1) {
				showWarningBox("Time 1 is not valid",
				"Please enter time in format: hh:mm:ss");
			}
			if (invDate2) {
				showWarningBox("Date 2 is not valid",
				"Please enter date in format: dd/mm/yyyy");
			}
			if (invTime2) {
				showWarningBox("Time 2 is not valid",
				"Please enter time in format: hh:mm:ss");
			}
			if (!statusSet) {
				showWarningBox(
						"Select a job status",
				"Please select at least one status. Otherwise the grid browser won't show any elements..");
			}
		}

	}
	
	protected abstract void selectAllStatusWidgets();

	protected void showWarningBox(String titel, String text) {
		MessageBox messageBox = new MessageBox(platformDisplay,
				SWT.ICON_WARNING | SWT.OK);
		messageBox.setMessage(text);
		messageBox.setText(titel);
		messageBox.open();
	}
}
