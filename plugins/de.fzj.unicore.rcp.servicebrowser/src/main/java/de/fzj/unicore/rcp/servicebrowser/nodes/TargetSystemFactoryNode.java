/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.xmlbeans.SchemaType;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ExactType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.OperatingSystemType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType;
import org.unigrids.services.atomic.types.SiteResourceType;
import org.unigrids.x2006.x04.services.tsf.CreateTSRDocument;
import org.unigrids.x2006.x04.services.tsf.TargetSystemFactoryPropertiesDocument;
import org.unigrids.x2006.x04.services.tsf.TargetSystemFactoryPropertiesDocument.TargetSystemFactoryProperties;
import org.unigrids.x2006.x04.services.tss.ApplicationResourceType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.UAS;
import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.common.utils.FileUtils;
import de.fzj.unicore.rcp.common.utils.RangeValueUtils;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.actions.CreateTargetSystemAction;
import de.fzj.unicore.rcp.servicebrowser.applications.Application;
import de.fzj.unicore.rcp.servicebrowser.applications.ApplicationImpl;
import de.fzj.unicore.rcp.servicebrowser.utils.URIUtils;
import de.fzj.unicore.uas.client.TSFClient;
import de.fzj.unicore.uas.client.TSSClient;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 *
 * @author demuth
 */
public class TargetSystemFactoryNode extends WSRFNode {

	private static final long serialVersionUID = -5322321414988222110L;

	public static final String TYPE = UAS.TSF;

	public TargetSystemFactoryNode(EndpointReferenceType epr) {
		super(epr);
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
	}

	/**
	 * create a TSS with default settings (i.e. no custom parameters)
	 */
	public EndpointReferenceType createTargetSystem() throws Exception {
		CreateTSRDocument request = CreateTSRDocument.Factory.newInstance();
		request.addNewCreateTSR();
		return createTargetSystem(request);
	}


	/**
	 * create a TSS
	 */
	public EndpointReferenceType createTargetSystem(CreateTSRDocument request) throws Exception {
		TSFClient tsf = createTargetSystemFactoryClient();
		TSSClient tss = null;
		try {
			tss = tsf.createTSS(request);
		} catch (Exception e) {
			if (e.getCause() != null
					&& e.getCause().getMessage() != null
					&& e.getCause().getMessage().toLowerCase()
					.contains("access denied")) {
				setState(STATE_ACCESS_DENIED);
			}
			throw e;
		}

		EndpointReferenceType tssEpr = tss.getEPR();
		ServiceBrowserActivator.log("Created TSS at "
				+ tssEpr.getAddress().getStringValue());
		// now update properties
		TargetSystemFactoryProperties props = getTargetSystemFactoryProperties();
		props.addNewAccessibleTargetSystemReference().set(tssEpr);
		TargetSystemFactoryPropertiesDocument doc = TargetSystemFactoryPropertiesDocument.Factory
				.newInstance();
		doc.setTargetSystemFactoryProperties(props);
		setCachedResourceProperties(doc.toString());
		retrieveChildren();
		// check to be on the safe side: trying to expand a node that has no 
		// children will let its drill down icon disappear
		if (hasChildren()) 
		{
			for (Node n : getChildrenArray()) {
				if (n.getURI().toString()
						.equals(tssEpr.getAddress().getStringValue())) {
					n.refresh(0, false, null);
					break;
				}
			}
			super.setExpanded(true);
		}
		return tssEpr;
	}
	
	protected TSFClient createTargetSystemFactoryClient() throws Exception {
		IClientConfiguration secProps = getUASSecProps();
		return new TSFClient(getURI().toString(), getEpr(), secProps);
	}

	/**
	 * Removes the node from the application registry.
	 */
	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public IStatus doubleClick(IProgressMonitor monitor) {
		// use super.setExpanded here in order to avoid
		// creating a target system
		super.setExpanded(true); 
		IStatus s = refresh(2, true, monitor);
		return s;
	}

	/**
	 * @return the list of available applications.
	 */
	public List<Application> getApplications() {
		List<Application> apps = new ArrayList<Application>();
		if (hasCachedResourceProperties()) {
			TargetSystemFactoryProperties props = getTargetSystemFactoryProperties();
			if (props != null) {
				try {
					for (ApplicationResourceType app : props
							.getApplicationResourceArray()) {
						apps.add(new ApplicationImpl(app.getApplicationName(),
								app.getApplicationVersion()));
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return apps;
	}

	/**
	 * Integer value for sorting nodes in views. Nodes are usually sorted in
	 * ascending order.
	 * 
	 * @return
	 */
	@Override
	public int getCategory() {
		return 500;
	}

	@Override
	public DetailsAdapter getDetails() {
		DetailsAdapter details = super.getDetails();
		Map<String, String> hm = details.getMap();
		List<Application> apps = getApplications();
		if (!apps.isEmpty()) {
			StringBuffer sb = new StringBuffer();
			for (Application app : apps) {
				sb.append(app.getName());
				sb.append(" (Version ");
				sb.append(app.getApplicationVersion()).append(")");
				sb.append(System.getProperty("line.separator"));
			}
			sb.deleteCharAt(sb.length() - 1);
			hm.put("Installed Applications", sb.toString());
		}
		if (hasCachedResourceProperties()) {
			TargetSystemFactoryProperties props = getTargetSystemFactoryProperties();
			try {
				Long value = (long) RangeValueUtils.getMaximum(props
						.getTotalResourceCount());
				hm.put("Number of Nodes", value.toString());
			} catch (Exception e) {
				// do nothing
			}
			try {
				Long value = (long) RangeValueUtils.getMaximum(props
						.getIndividualCPUCount());
				hm.put("Processors per Node", value.toString());
			} catch (Exception e) {
				// do nothing
			}
			try {
				Long value = (long) RangeValueUtils.getMaximum(props
						.getTotalCPUCount());
				hm.put("Total Number of Processors", value.toString());
			} catch (Exception e) {
				// do nothing
			}
			try {
				Long value = ((long) RangeValueUtils.getMaximum(props
						.getProcessor().getIndividualCPUSpeed()))
						/ (1024 * 1024);
				hm.put("Processor Clock", value.toString() + " Mhz");
			} catch (Exception e) {
				// do nothing
			}
			try {
				Long value = ((long) RangeValueUtils.getMaximum(props.getIndividualPhysicalMemoryArray(0)));
				hm.put("Processor Main Memory", FileUtils.humanReadableFileSize(value, ServiceBrowserActivator
						.getDefault().getPreferenceStore().getBoolean(ServiceBrowserConstants.P_SIZE_UNITS_SI)));
			} catch (Exception e) {
				// do nothing
			}
			try {
				String arch = props.getProcessor().getCPUArchitecture()
						.getCPUArchitectureName().toString();
				hm.put("Processor Architecture", arch);
			} catch (Exception e) {
				// do nothing
			}
			try {
				OperatingSystemType[] oses = props.getOperatingSystemArray();
				StringBuffer sb = new StringBuffer();
				for (OperatingSystemType os : oses) {
					sb.append(os.getOperatingSystemType()
							.getOperatingSystemName());
					sb.append(" (Version ");
					String version = os.getOperatingSystemVersion() == null ? "unknown"
							: os.getOperatingSystemVersion();
					sb.append(version).append("), ");
					sb.deleteCharAt(sb.length() - 1);
					sb.deleteCharAt(sb.length() - 1);
				}
				hm.put("Installed Operating Systems", sb.toString());
			} catch (Exception e) {
				// do nothing
			}

			List<SiteResourceType> siteResources = getSiteSpecificResources();
			if (!siteResources.isEmpty()) {
				StringBuffer sb = new StringBuffer();
				for (SiteResourceType rsc : siteResources) {
					sb.append(rsc.getName()).append(" (");
					RangeValueType value = rsc.getValue();
					if (value == null) {
						continue;
					}
					for (ExactType exact : value.getExactArray()) {
						sb.append("default="
								+ String.valueOf(exact.getDoubleValue()));
					}
					for (RangeType rt : value.getRangeArray()) {
						if (rt.getLowerBound() != null) {
							sb.append(" min=").append(
									String.valueOf(rt.getLowerBound()
											.getDoubleValue()));
						}
						if (rt.getUpperBound() != null) {
							sb.append(" max=").append(
									String.valueOf(rt.getUpperBound()
											.getDoubleValue()));
						}
					}

					sb.append(") ");
					sb.append(rsc.getDescription());
					sb.append(System.getProperty("line.separator"));
				}
				sb.deleteCharAt(sb.length() - 1);
				hm.put("Site Specific Resources", sb.toString());
			}
		}
		return details;
	}

	@Override
	public SchemaType getSchemaType() {
		return TargetSystemFactoryPropertiesDocument.type;
	}

	/**
	 * @return the list of available site specific resources.
	 */
	public List<SiteResourceType> getSiteSpecificResources() {
		List<SiteResourceType> siteResources = new ArrayList<SiteResourceType>();
		if (hasCachedResourceProperties()) {

			try {
				TargetSystemFactoryProperties props = getTargetSystemFactoryProperties();
				for (SiteResourceType resource : props.getSiteResourceArray()) {
					siteResources.add(resource);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return siteResources;

	}

	public TargetSystemFactoryProperties getTargetSystemFactoryProperties() {
		TargetSystemFactoryPropertiesDocument doc = (TargetSystemFactoryPropertiesDocument) getCachedResourcePropertyDocument();
		if (doc == null) {
			return null;
		}
		return doc.getTargetSystemFactoryProperties();
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		availableActions.add(new CreateTargetSystemAction(this));
	}

	/**
	 * create the child nodes of this node
	 */
	@Override
	protected void retrieveChildren() throws Exception {
		List<Node> newChildren = new ArrayList<Node>();
		if (hasCachedResourceProperties()) {

			TargetSystemFactoryProperties props = getTargetSystemFactoryProperties();
			EndpointReferenceType[] tssEprs = null;
			boolean is612orLater = props.getName() != null;
			if (is612orLater) {
				tssEprs = props.getAccessibleTargetSystemReferenceArray();
			} else {
				tssEprs = props.getTargetSystemReferenceArray();
			}

			if (tssEprs.length > 0) { // add target systems
				for (EndpointReferenceType epr : tssEprs) {
					try {
						Node node = NodeFactory.createNode(epr);
						newChildren.add(node);

					} catch (Exception e) {
					}
				}
			}
		}
		updateChildren(newChildren);

	}

	@Override
	public void retrieveName() {
		URI uri = getURI();
		if (uri != null) {

			setName(URIUtils.extractUnicoreServiceContainerName(uri));
		} else {
			super.retrieveName();
		}
	}

	@Override
	public void setExpanded(boolean expanded) {
		super.setExpanded(expanded);

		if (expanded) {
			if (!hasChildren()) {
				Job j = new BackgroundJob("Refreshing site") {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						INodeData data = getData();
						if (data == null) {
							return Status.CANCEL_STATUS;
						}
						synchronized (data) {
							try {
								refresh(1, true, monitor);
								if (getChildren().size() == 0) {
									createTargetSystem();
								}
							} catch (Exception e) {
								ServiceBrowserActivator
								.log(IStatus.WARNING,
										"Unable to check "
												+ TargetSystemFactoryNode.this
												.getName()
												+ " for available Target Systems: ",
												e);
							}

							return Status.OK_STATUS;
						}
					}
				};
				j.schedule();

			}
		}
	}

	/**
	 * @return
	 */
	public boolean supportsVirtualization() {
		return getTargetSystemFactoryProperties().getSupportsVirtualImages();
	}

}
