package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.ui.SetFilePermissionsDialog;

public class SetFilePermissionsAction extends NodeAction {

	protected boolean read, write, execute, apply;

	public SetFilePermissionsAction(AbstractFileNode node) {
		super(node);
		setText("Change Permissions");
		setToolTipText("Set permissions of the remote file.");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("file_permissions.png"));
	}

	@Override
	public void run() {
		final AbstractFileNode fileNode = (AbstractFileNode) getNode();
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {
				Shell s = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getShell();
				SetFilePermissionsDialog dialog = new SetFilePermissionsDialog(
						s, fileNode);
				int result = dialog.open();
				apply = Window.OK == result;
				read = dialog.isRead();
				write = dialog.isWrite();
				execute = dialog.isExecute();

			}
		});
		if (apply) {
			try {
				fileNode.getStorageClient().changePermissions(
						fileNode.getGridFileType().getPath(), read, write,
						execute);
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.ERROR, "Unable to set remote file permissions: ", e);
			}
			fileNode.refresh(0, false, null);
		}

	}

}
