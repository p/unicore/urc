package de.fzj.unicore.rcp.servicebrowser.actions;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ui.CreateProfileDialog;

public class CreateSecurityProfileAction extends Action {
	public CreateSecurityProfileAction() {
		super();
		setText("Create Security Profile");
		setToolTipText("Create a new security profile and a security mapping to a UNICORE registry");
	}

	@Override
	public void run() {
		try {
			Shell s = null;
			try {
				s = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
						.getShell();
			} catch (Exception e) {

			}
			if (s == null) {
				s = new Shell();
			}
			new CreateProfileDialog(s).open();
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.ERROR, "Could not create security profile", e);
		}
	}

}
