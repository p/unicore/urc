/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.ILazyParent;
import de.fzj.unicore.rcp.servicebrowser.utils.URIUtils;

public class NodeFactory {

	private static INodeDataCache nodeDataCache;
	private static boolean extensionsLoaded = false;
	private static Set<INodeExtensionPoint> nodeExtensions = new HashSet<INodeExtensionPoint>();

	/**
	 * Called only once to create THE main instance that represents the Grid
	 * node
	 * 
	 * @return
	 */
	public static GridNode createNewGridNode() {

		GridNode gridNode = new GridNode();
		setNodeDataCache(new NodeDataCache());
		getNodeDataCache().clearAllData(); // make sure that all persisted data
											// is removed
		gridNode.setCache(getNodeDataCache());
		getNodeDataCache().incrementNodeCount(gridNode.getEpr());
		gridNode.init();
		return gridNode;

	}

	/**
	 * creates a Node from an EPR determines type
	 * 
	 * @param epr
	 *            TODO
	 * @return created Node. If the type couldnt be determined, an UnknownNode
	 *         is created,
	 */

	public static Node createNode(EndpointReferenceType epr) {
		Node result = null;
		URI uri = null;
		try {
			uri = new URI(epr.getAddress().getStringValue());

		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.ERROR,
					"Invalid EndpointReference during Grid node creation!", e);
			return null;
		}
		// iterate over extensions and find right extension to create the node
		if (!extensionsLoaded) {
			loadExtensions();
		}
		for (INodeExtensionPoint ext : nodeExtensions) {
			try {
				result = ext.getNode(epr);
				if (result != null) {
					break;
				}
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}

		// If nothing else worked, try to create node from URI
		if (result == null) {
			result = createNode(uri, false);

			// workaround to fix bug where eprs did not contain
			// the port type
			EndpointReferenceType guessedEpr = result.getEpr();
			QName type = Utils.InterfaceNameFromEPR(epr);
			QName guessedType = Utils.InterfaceNameFromEPR(guessedEpr);
			if (type == null && guessedType != null) {
				Utils.addPortType(epr, guessedType);
			}

			// in order not to loose information that was contained in the epr
			result.setEpr(epr);
		}

		result.setCache(getNodeDataCache());
		getNodeDataCache().incrementNodeCount(epr);
		result.init();
		return result;
	}

	// create a node from data that has already been cached
	// for the node's address
	public static Node createNode(INodeData data) {
		Node node = createNode(data.getEpr());
		if (getNodeDataCache().getNodeData(data.getEpr()) == null) {
			getNodeDataCache().putNodeData(data);
		}
		return node;
	}

	public static Node createNode(URI uri) {
		return createNode(uri, true);
	}

	/**
	 * Creates a Node from an URI. Should be used if only the URI of a
	 * service/resource is known, i.e. the EPR of the service/resource was not
	 * retrieved from a Registry.
	 * 
	 * @param uri
	 * @return
	 */
	private static Node createNode(URI uri, boolean addToCache) {

		Node result = null;
		EndpointReferenceType epr = Utils.newEPR();
		epr.addNewAddress().setStringValue(uri.toString());

		// iterate over extensions and find right extension to create the node
		if (!extensionsLoaded) {
			loadExtensions();
		}
		for (INodeExtensionPoint ext : nodeExtensions) {
			try {
				Node node = ext.getNode(uri);
				if (node != null) {
					result = node;
					epr = node.getEpr();
					break;
				}
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}
		if (result == null) {

			if (uri.getQuery() != null && uri.getQuery() != null
					&& uri.getQuery().startsWith("res=")) {
				result = new WSRFNode(epr);
			} else {
				result = new UnknownNode(epr);
			}
		}
		if (addToCache) {
			result.setCache(getNodeDataCache());
			getNodeDataCache().incrementNodeCount(epr);
			result.init();
		}
		return result;
	}

	public static EndpointReferenceType getEPRForURI(URI uri) {
		EndpointReferenceType epr = Utils.newEPR();
		epr.addNewAddress().setStringValue(uri.toString());
		// Does a node with this URI exist?
		// if so, get the EPR from there
		INodeData data = getNodeDataCache().getNodeData(epr);
		if (data != null) {
			epr = data.getEpr();
		}
		return epr;
	}

	public static synchronized INodeDataCache getNodeDataCache() {
		if (nodeDataCache == null) {
			GridNode gridNode = ServiceBrowserActivator.getDefault()
					.getGridNode();
			nodeDataCache = gridNode.getCache();
			if (nodeDataCache == null) {
				nodeDataCache = new NodeDataCache();
				gridNode.setCache(nodeDataCache);
			}
		}
		return nodeDataCache;
	}

	/**
	 * Tries to find the node with the given path, returns null if no node with
	 * a matching path could be found. This method returns the ORIGINAL node
	 * from the main node tree, so take good care of it and DO NOT call dispose
	 * on it.
	 * 
	 * @param namePath
	 * @return
	 */
	public static Node getNodeFor(NodePath path) {

		if (path == null) {
			return null;
		}
		Queue<Node> next = new ConcurrentLinkedQueue<Node>();
		next.offer(ServiceBrowserActivator.getDefault().getGridNode());
		int level = 0;
		int length = path.getLength();
		while (!next.isEmpty()) {
			Node current = next.poll();
			String currentPath = URIUtils.toNormalizedString(current.getURI());
			String levelPath = URIUtils.toNormalizedString(path.get(level));
			if (currentPath.equals(levelPath)) {
				if (level + 1 == length) {
					return current;
				} else {
					level++;
					levelPath = URIUtils.toNormalizedString(path.get(level));
					for (Node child : current.getChildrenArray()) {
						String childPath = URIUtils.toNormalizedString(child
								.getURI());
						if (childPath.equals(levelPath)) {
							next.offer(child);
							break;
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * Tries to find all nodes with the given name path, (e.g.
	 * Grid/Registry/TargetSystemFactory); returns an empty list if no node with
	 * a matching name path could be found. This method returns the ORIGINAL
	 * nodes from the main node tree, so take good care of them and DO NOT call
	 * dispose on them.
	 * 
	 * @param namePath
	 * @return
	 */
	public static List<Node> getNodesFor(String namePath) {
		List<Node> result = new ArrayList<Node>();
		Queue<Node> next = new ConcurrentLinkedQueue<Node>();
		next.offer(ServiceBrowserActivator.getDefault().getGridNode());
		while (!next.isEmpty()) {
			Node current = next.poll();
			if (namePath.equals(current.getNamePathToRoot())) {
				result.add(current);
			}
			for (Node child : current.getChildrenArray()) {
				String childPath = child.getNamePathToRoot();
				if (childPath.equals(namePath)) {
					result.add(child);
				} else if (namePath.startsWith(childPath)) {
					next.offer(child);
				}
			}
		}

		return result;
	}

	private static synchronized void loadExtensions() {
		// iterate over extensions and find right extension to create the node
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(ServiceBrowserConstants.NODE_EXTENSION_POINT);
		IConfigurationElement[] members = extensionPoint
				.getConfigurationElements();
		// For each extension
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			// IExtension extension = member.getDeclaringExtension();

			// String className = member.getAttribute("name");
			try {

				INodeExtensionPoint ext = (INodeExtensionPoint) member
						.createExecutableExtension("name");
				nodeExtensions.add(ext);
			} catch (CoreException ex) {
				// do nothing
			}
		}
		extensionsLoaded = true;
	}

	/**
	 * This method tries to find the node with the given path and returns it. If
	 * the node cannot be found, the method does not give up, but tries to
	 * refresh all nodes on the path in order to find the node. Beware! This
	 * method can be VERY long-running, and should NEVER be called from the main
	 * thread.
	 * 
	 * @param path
	 * @param progress
	 * @return the node with the given path or null if the node can't be found,
	 *         even after refreshing all of its parents
	 * @throws Exception
	 */
	public static Node revealNode(NodePath path, IProgressMonitor progress) {
		if (path == null || path.getLength() == 0) {
			return null;
		}
		progress = ProgressUtils.getNonNullMonitorFor(progress);
		progress.beginTask("", path.getLength());
		try {
			Node result = getNodeFor(path);
			if (result != null) {
				return result; // the node has already been revealed, this is
								// easy
			} else if (path.getLength() == 1) {
				return null;
			}
			Node parent = revealNode(path.getParent(), new SubProgressMonitor(
					progress, path.getLength() - 1));
			if (parent == null) {
				return null;
			}

			for (Node n : parent.getChildrenArray()) {
				if (n.getPathToRoot().equals(path)) {
					return n;
				}
			}
			// if we reach this, we need to refresh the parent in order to find
			// the node
			SubProgressMonitor sub = new SubProgressMonitor(progress, 1);
			if (parent instanceof ILazyParent) {
				ILazyParent group = (ILazyParent) parent;
				group.lazilyRetrieveChildrenUpTo(
						path.get(path.getLength() - 1), progress);
			} else {
				parent.refresh(1, false, sub);
			}
			for (Node n : parent.getChildren()) {
				if (n.getPathToRoot().equals(path)) {
					return n;
				}
			}
			return null; // couldn't reveal the node, the service doesn't seem
							// to exist
		} finally {
			progress.done();
		}

	}

	private static void setNodeDataCache(INodeDataCache nodeDataCache) {
		NodeFactory.nodeDataCache = nodeDataCache;
	}

}
