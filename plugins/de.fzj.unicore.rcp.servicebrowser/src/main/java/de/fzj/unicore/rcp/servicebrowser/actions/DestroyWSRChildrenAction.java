/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import de.fzj.unicore.rcp.servicebrowser.nodes.JobNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.GroupNode;
import de.fzj.unicore.uas.client.TSSClient;

/**
 * @author demuth
 * 
 */
public class DestroyWSRChildrenAction extends AbstractNodeCommandHandler {

	static final int DELETION_CHUNK_SIZE = 10;

	public DestroyWSRChildrenAction() {

	}




	@Override
	public boolean confirmAction(Shell shell) {
		return MessageDialog.openConfirm(shell, "Confirm action", "Do you really want to destroy the selected Grid resources?");
	}		


	@Override
	protected IStatus performAction(IProgressMonitor monitor, Node n) {
		
		
		monitor.beginTask("destroying", 100);
		try {
			boolean running = true;
			while (running) {
				List<Node> children = n.getChildren();
				
				if(children.size() == 0) {
					return Status.OK_STATUS;
				}

				if (n instanceof GroupNode && n.getName().equalsIgnoreCase("jobs")) {
					GroupNode jobsNode = (GroupNode) n;
					try {
						TargetSystemNode tsn = (TargetSystemNode) jobsNode.getParent();
						TSSClient tss = new TSSClient(tsn.getURI().toString(), tsn.getEpr(), tsn.getUASSecProps());
						if(tss.checkVersion("1.7.0")) {
							tss.deleteJobs(createChildCollection(children));
							n.refresh(1, false, null);
							continue;
						}
					} catch (Exception e) {
						// default to old but slow implementation (slightly optimized)
					}
				}

				int i = 0;
				for (Iterator<Node> iter = children.iterator(); iter.hasNext() && i < DELETION_CHUNK_SIZE;) {
					Node c = iter.next();
					if (c instanceof WSRFNode) {
						WSRFNode wsrf = (WSRFNode) c;
						if (wsrf.getState() != Node.STATE_DESTROYED) {
							wsrf.destroy();
							i++;
						}
					}
					if(monitor.isCanceled()) {
						break;
					}
				}
				n.refresh(1, false, null);
				monitor.worked(DELETION_CHUNK_SIZE*100/children.size());
				running = !monitor.isCanceled();
			}
			return Status.CANCEL_STATUS;
		} finally {
			monitor.done();
		}

	}

	/**
	 * @param children
	 * @return
	 */
	private Collection<String> createChildCollection(List<Node> children) {
		Collection<String> jobIds = new ArrayList<String>();
		for (Node node : children) {
			if(node instanceof JobNode) {
				JobNode j = (JobNode) node;
				try {
					jobIds.add(j.getEpr().getAddress().getStringValue().split("res=")[1]);
				} catch (ArrayIndexOutOfBoundsException e) {
					// ignore
				}
			}
		}
		return jobIds;
	}

	@Override
	protected String getJobName() {
		return "Destroying resource(s)";
	}

	@Override
	protected String getTaskName() {
		return "destroying resources...";
	}

}
