/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.math.BigInteger;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.commons.httpclient.util.URIUtil;
import org.apache.xmlbeans.SchemaType;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.TransferData;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.FileSystemType;
import org.unigrids.services.atomic.types.GridFileType;
import org.unigrids.services.atomic.types.ProtocolType;
import org.unigrids.x2006.x04.services.sms.ListDirectoryDocument;
import org.unigrids.x2006.x04.services.sms.ListDirectoryDocument.ListDirectory;
import org.unigrids.x2006.x04.services.sms.StoragePropertiesDocument;
import org.unigrids.x2006.x04.services.sms.StoragePropertiesDocument.StorageProperties;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.UAS;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.common.utils.FileUtils;
import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.actions.CreateDirectoryAction;
import de.fzj.unicore.rcp.servicebrowser.actions.CreateFileAction;
import de.fzj.unicore.rcp.servicebrowser.actions.PasteFilesSpecialAction;
import de.fzj.unicore.rcp.servicebrowser.actions.PasteNodesAction;
import de.fzj.unicore.rcp.servicebrowser.actions.SetUserMaskAction;
import de.fzj.unicore.rcp.servicebrowser.actions.UploadFilesAction;
import de.fzj.unicore.rcp.servicebrowser.actions.UploadFolderAction;
import de.fzj.unicore.rcp.servicebrowser.dnd.FileDropAssistant;
import de.fzj.unicore.rcp.servicebrowser.dnd.NodeLocalSelectionTransfer;
import de.fzj.unicore.rcp.servicebrowser.utils.URIUtils;
import de.fzj.unicore.uas.StorageManagement;
import de.fzj.unicore.uas.client.StorageClient;

/**
 * @author demuth
 */
public class StorageNode extends WSRFNode {

	private static final long serialVersionUID = -8111353525341128207L;
	public static final String TYPE = UAS.SMS;
	public static final QName PORTTYPE = StorageManagement.SMS_PORT;
	/**
	 * Key for Boolean property that states whether this storage is capable of
	 * listing directories successively (using an offset and a limit).
	 */
	public static final String PROPERTY_SUPPORTS_SUCCESSIVE_LIST = "supports successive list";

	public static final String PROPERTY_CURRENT_OFFSET = "current offset";

	public StorageNode(EndpointReferenceType epr) {
		super(epr);
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
		setCurrentOffset(0);
	}

	@Override
	public boolean canPaste(Object toPaste, TransferData data, int operation) {
		if (toPaste == null) {
			return false;
		}
		boolean canIPaste = true;
		if (FileTransfer.getInstance().isSupportedType(data)) {
			return true;
		}
		if (NodeLocalSelectionTransfer.getTransfer().isSupportedType(data)) {
			IStructuredSelection selection = (IStructuredSelection) toPaste;
			for (Object object : selection.toArray()) {
				// check if node represents a file and prevent pasting into
				// yourself
				if (!(object instanceof AbstractFileNode) || equals(object)) 
				{
					canIPaste = false;
					break;
				}
			}
		} else {
			canIPaste = false;
		}
		if (!canIPaste) {
			return super.canPaste(toPaste, data, operation);
		} else {
			return true;
		}
	}

	protected MoreFilesNode createMoreFilesNode() {
		MoreFilesNode moreFilesNode;
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue(getURI().toString() + "/more");
		Utils.addPortType(epr, MoreFilesNode.PORTTYPE);
		moreFilesNode = (MoreFilesNode) NodeFactory.createNode(epr);
		moreFilesNode.setStorageEPR(getEpr());
		return moreFilesNode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.Node#doubleClick()
	 */
	@Override
	public IStatus doubleClick(IProgressMonitor monitor) {
		setExpanded(true);
		refresh(1, true, monitor);
		return Status.OK_STATUS;
	}

	/**
	 * Integer value for sorting nodes in views. Nodes are usually sorted in
	 * ascending order.
	 * 
	 * @return
	 */
	@Override
	public int getCategory() {
		return 600;
	}

	public int getCurrentOffset() {
		INodeData data = getData();
		if (data == null) {
			return 0;
		}
		Integer result = (Integer) data.getProperty(PROPERTY_CURRENT_OFFSET);
		if (result != null) {
			return result;
		} else {
			return 0;
		}
	}

	@Override
	public DetailsAdapter getDetails() {
		DetailsAdapter details = super.getDetails();
		Map<String, String> hm = details.getMap();
		FileSystemType fs = getFileSystem();
		if (fs != null)
		{
			if (fs.getFileSystemType() != null) {
				hm.put("File system type", fs.getFileSystemType().toString());
			}
			if(fs.getDescription() != null)
			{
				hm.put("Description",fs.getDescription());
			}
			if(fs.getMountPoint() != null)
			{
				hm.put("Mount point",fs.getMountPoint());
			}
			if (fs.getDiskSpace() != null) {
				try {
					long bytes = (long) Double.parseDouble(fs.getDiskSpace()
							.getExactArray(0).getStringValue());
					String space = FileUtils.humanReadableFileSize(bytes, ServiceBrowserActivator.getDefault()
							.getPreferenceStore().getBoolean(ServiceBrowserConstants.P_SIZE_UNITS_SI));
					hm.put("Disk space", space);
				} catch (Exception e) {
					// do nothing
				}
			}
		}
		ProtocolType.Enum[] protocols = getSupportedProtocols();
		if (protocols != null && protocols.length > 0) {
			String protos = "";
			for(int i = 0; i < protocols.length; i++)
			{
				ProtocolType.Enum p = protocols[i];
				protos += p.toString();
				if(i<protocols.length-1) protos += ", ";
			}
			hm.put("File transfer protocols", protos);
		}
		return details;
	}

	public FileSystemType getFileSystem() {
		if (hasCachedResourceProperties()) {
			StorageProperties storeProps = getStorageProperties();
			try {
				return storeProps.getFileSystem();
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.ERROR,
						"Unable to parse resouce properties for storage "
								+ getURI().toString(), e);
			}
		}
		return null;
	}

	protected MoreFilesNode getMoreFilesNode() {
		for (Node n : getChildrenArray()) {
			if (n instanceof MoreFilesNode) {
				return (MoreFilesNode) n;
			}
		}
		return null; // not found
	}

	/**
	 * Return a file address that corresponds to the root of this storage.
	 * 
	 * @return
	 */
	public URI getRootFileAddress() {
		try {
			return new URI(getURI().toString() + "#");
		} catch (Exception e) {
			ServiceBrowserActivator.getDefault();
			ServiceBrowserActivator.log(IStatus.ERROR,
					"Unable to determine address for the storage " + getURI(),
					e);
			return null;
		}
	}

	@Override
	public SchemaType getSchemaType() {
		return StoragePropertiesDocument.type;
	}

	public StorageClient getStorageClient() throws Exception {
		return new StorageClient(getURI().toString(), getEpr(), getUASSecProps());
	}

	protected StorageProperties getStorageProperties() {
		if (!hasCachedResourceProperties()) {
			return null;
		}
		StoragePropertiesDocument doc = (StoragePropertiesDocument) getCachedResourcePropertyDocument();
		return doc.getStorageProperties();
	}

	public ProtocolType.Enum[] getSupportedProtocols()
	{
		if (hasCachedResourceProperties()) {
			StorageProperties storeProps = getStorageProperties();
			try {
				return storeProps.getProtocolArray();
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.ERROR,
						"Unable to parse resouce properties for storage "
								+ getURI().toString(), e);
			}
		}
		return new ProtocolType.Enum[0];
	}
	
	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	public GridFileType[] listDirectory(StorageClient client, String path,
			int offset, int limit) throws Exception {
		int defaultChunkSize = ServiceBrowserActivator
				.getDefault()
				.getPreferenceStore()
				.getInt(ServiceBrowserConstants.P_CHUNK_SIZE_LAZY_CHILD_RETRIEVAL) + 1;
		defaultChunkSize = Math.max(101, defaultChunkSize);
		return listDirectory(client, path, offset, limit, defaultChunkSize);
	}

	public GridFileType[] listDirectory(StorageClient client, String path,
			int offset, int limit, int chunksize) throws Exception {
		StorageManagement sms = client.makeProxy(StorageManagement.class);
		ListDirectoryDocument doc = ListDirectoryDocument.Factory.newInstance();
		ListDirectory list = doc.addNewListDirectory();

		GridFileType[] all = null;
		INodeData data = getData();
		if (data == null) {
			return null;
		}
		if (data.getProperty(PROPERTY_SUPPORTS_SUCCESSIVE_LIST) == null) {
			// figure out whether the storage supports successive listing
			// correctly
			// to this end, compare the result to a normal list operation
			setSupportsSuccessiveList(getServerVersion() != null);

		}

		if (supportsSuccessiveList()) {
			chunksize = Math.min(limit, chunksize);
			int numCalls = (int) Math.ceil(((double) limit) / chunksize);
			list.setPath(path);
			list.setLimit(new BigInteger(String.valueOf(chunksize)));
			List<GridFileType> result = new ArrayList<GridFileType>();
			for (int i = 0; i < numCalls; i++) {
				list.setOffset(new BigInteger(String.valueOf(offset)));
				GridFileType[] files = sms.ListDirectory(doc)
						.getListDirectoryResponse().getGridFileArray();
				if (files == null) {
					break;
				} else {
					for (GridFileType gft : files) {
						// create a copy instead of using the original
						// otherwise we'd have references to the
						// ListDirectoryResponseDocument
						// => Memory Leak!
						GridFileType copy = GridFileType.Factory.parse(gft
								.newInputStream());
						if (result.size() <= limit) {
							result.add(copy);
						} else {
							break;
						}
					}
				}
				if (files.length < chunksize) {
					break;
				}
				offset += chunksize;
			}
			return result.toArray(new GridFileType[result.size()]);
		} else {
			// successive listing not supported or support unknown (due to a
			// small number of files on the storage)
			if (all == null) {
				all = client.listDirectory(path);
			}
			if (offset > all.length) {
				offset = all.length;
			}
			int size = Math.min(all.length - offset, limit);
			GridFileType[] result = new GridFileType[size];
			if (size > 0) {

				for (int i = 0; i < size; i++) {
					GridFileType gft = all[offset + i];
					// create a copy instead of using the original
					// otherwise we'd have references to the
					// ListDirectoryResponseDocument
					// => Memory Leak!
					GridFileType copy = GridFileType.Factory.parse(gft
							.newInputStream());
					result[i] = copy;
				}

			}
			return result;
		}

	}

	@Override
	public void paste(Object toPaste, TransferData data, int operation) {
		if (toPaste == null) {
			return;
		}

		FileDropAssistant assistant = new FileDropAssistant();
		assistant.performDrop(this, operation, data, toPaste);

	}

	@Override
	public IStatus refresh(IProgressMonitor monitor) {
		setExpanded(true);
		return refresh(1, true, monitor);
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		availableActions.add(new CreateDirectoryAction(this));
		availableActions.add(new CreateFileAction(this));
		availableActions.add(new PasteNodesAction(this));
		availableActions.add(new PasteFilesSpecialAction(this));
		availableActions.add(new UploadFilesAction(this));
		availableActions.add(new UploadFolderAction(this));
		
		if (UnicoreCommonActivator.getDefault().getGridDetailLevel() >= UnicoreCommonActivator.LEVEL_EXPERT)
		{
			availableActions.add(new SetUserMaskAction(this));
		}		
	}

	@Override
	protected void retrieveChildren() throws Exception {
		List<Node> newChildren = new ArrayList<Node>();

		try {
			if (hasCachedResourceProperties()) {
				StorageProperties storeProps = getStorageProperties();
				ProtocolType.Enum[] protocols = storeProps.getProtocolArray();
				StorageClient storageClient = getStorageClient();
				GridFileType[] files = null;
				int limit = ServiceBrowserActivator
						.getDefault()
						.getPreferenceStore()
						.getInt(ServiceBrowserConstants.P_CHUNK_SIZE_LAZY_CHILD_RETRIEVAL);
				int offset = getCurrentOffset();
				limit = Math.max(limit, offset);

				if (limit > offset) {
					setCurrentOffset(limit);
				}
				try {
					// list one file more than necessary in order to find out
					// whether the MoreFilesNode should be shown
					files = listDirectory(storageClient, "/", 0, limit + 1); 
				} catch (Exception e) {

					if (e.getCause() != null
							&& e.getCause().getMessage() != null
							&& e.getCause().getMessage().toLowerCase()
									.contains("access denied")) {
						setState(STATE_ACCESS_DENIED);
					}

					else {
						ServiceBrowserActivator.log(IStatus.ERROR,
								"Unable to list contents for " + getName(), e);
						setFailReason(e.getMessage());
						setState(STATE_FAILED);
					}
					return;
				}

				if (files != null) {
					Map<URI, Node> oldChildren = getChildrenMap();
					int max = files.length > limit ? limit : files.length;
					for (int i = 0; i < max; i++) {
						GridFileType f = files[i];
						try {
							AbstractFileNode child = createFileNode(
									oldChildren, getEpr(), getName(), f);
							child.setSupportedProtocols(protocols);
							newChildren.add(child);

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (files.length > limit) {
						MoreFilesNode moreFilesNode = getMoreFilesNode();
						if (moreFilesNode == null) {
							moreFilesNode = createMoreFilesNode();
						}
						newChildren.add(moreFilesNode);
					}
				}

			}
		} finally {
			updateChildren(newChildren);
		}
		
	}

	@Override
	public void retrieveName() {
		INodeData data = getData();
		if (data != null) {
			synchronized (data) {

				Node parent = getParent();
				String result = (String) data.getProperty(PROPERTY_NAME);
				if (result != null) {
					return;
				}
				if (parent instanceof JobNode) {
					result = "Working directory of " + parent.getName();
					setNameForAllCopies(result);
					return;
				} else  {
					URI uri = getURI();
					String siteName = URIUtils.extractUnicoreServiceContainerName(uri);
					try {
						FileSystemType fs = getFileSystem();
						if (fs != null && fs.getName()!= null) {
							String fsName = fs.getName();
							result = "Storage "
								+ fsName
								+ " @" + siteName;
						setNameForAllCopies(result);
						return;
						}
					} catch (Exception e) {
						// do nothing
					}
					String resId = getResourceId();
					result = "Storage "
						+ resId
						+ "@" + siteName;
					setName(result);
				} 

			}
		}
		super.retrieveName();
	}

	public void setCurrentOffset(int offset) {
		putData(PROPERTY_CURRENT_OFFSET, offset);
	}
	
	public void setEpr(EndpointReferenceType epr) {
		super.setEpr(epr);
	}

	public void setSupportsSuccessiveList(boolean supportsSuccessiveList) {
		putData(PROPERTY_SUPPORTS_SUCCESSIVE_LIST, supportsSuccessiveList);
	}

	public boolean supportsSuccessiveList() {
		INodeData data = getData();
		if (data == null) {
			return false;
		}
		Boolean b = (Boolean) data
				.getProperty(PROPERTY_SUPPORTS_SUCCESSIVE_LIST);
		return b == null ? false : b;
	}

	public static AbstractFileNode createFileNode(Map<URI, Node> oldChildren,
			EndpointReferenceType storageEpr, String storageName,
			GridFileType gft) throws Exception {
		String relative = gft.getPath();
		while (relative.startsWith("/")) {
			relative = relative.substring(1);
		}
		relative = URIUtil.encode(relative,
				//escape special characters in filename!
				org.apache.commons.httpclient.URI.allowed_fragment);
		String s = storageEpr.getAddress().getStringValue() + "#" + relative;
		
		EndpointReferenceType childEpr = EndpointReferenceType.Factory
				.newInstance();
		childEpr.addNewAddress().setStringValue(s);
		String dn = Utils.extractServerIDFromEPR(storageEpr);
		if(dn != null)
		{
			Utils.addServerIdentity(childEpr, dn);
		}
		
		if (gft.getIsDirectory()) {
			Utils.addPortType(childEpr, FolderNode.getPortType());
		} else {
			Utils.addPortType(childEpr, FileNode.getPortType());
		}

		AbstractFileNode child = null;
		
			Node so = NodeFactory.createNode(childEpr);

			if (gft.getIsDirectory()) {
				FolderNode node = (FolderNode) so;
				node.setStorageEPR(storageEpr);
			}
			if (so instanceof AbstractFileNode) {
				AbstractFileNode file = (AbstractFileNode) so;
				child = file;
				file.setStorageEPR(storageEpr);
				file.setStorageName(storageName);
			}

		child.setGridFileType(gft);
		//avoid immediate refresh
		child.putData(PROPERTY_LAST_REFRESH, System.currentTimeMillis());
		return child;
	}
}
