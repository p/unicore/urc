/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

package de.fzj.unicore.rcp.servicebrowser.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.part.ViewPart;

import com.thoughtworks.xstream.XStream;

import de.fzj.unicore.rcp.common.utils.XStreamConfigurator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.ServiceContentProvider;
import de.fzj.unicore.rcp.servicebrowser.actions.CopyNodesAction;
import de.fzj.unicore.rcp.servicebrowser.actions.CutNodesAction;
import de.fzj.unicore.rcp.servicebrowser.actions.FilterActionMenu;
import de.fzj.unicore.rcp.servicebrowser.actions.MonitorRegistryAction;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.actions.PasteNodesAction;
import de.fzj.unicore.rcp.servicebrowser.actions.RefreshAction;
import de.fzj.unicore.rcp.servicebrowser.actions.ServiceBrowserAction;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IGridBrowserFilterExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.filters.DefaultFilterCriteria;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterController;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterCriteria;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterMenuCategory;
import de.fzj.unicore.rcp.servicebrowser.filters.GridFilterAction;


public class ServiceView extends ViewPart {

	public static String ID = "de.fzj.unicore.rcp.servicebrowser.serviceview";

	protected IServiceViewer viewer;	

	protected DefaultFilterSet defaultFilterSet;
	protected Action adminSwitchAction;

	protected ServiceBrowserAction monitorRegistryAction;
	protected NodeAction globalRefreshAction;

	protected Action switchViewAllAction;
	protected Action switchViewDefaultAction;

	protected Action collapseAllAction;

	protected DefaultFilterCriteria defaultFilter;


	// holds the actions collected from extensions and the basic action for
	// "showAll"
	protected List<Action> filterActions = new ArrayList<Action>();
	// holds the menu locations of the collected actions and the basic action
	// for "showAll"
	protected List<FilterMenuCategory> filterMenuCategory;


	private XStreamConfigurator xstreamConfigurator;	

	/**
	 * View selection
	 */
	private static final String FILTERING_STATE = "FilteringState";


	public void activateFilter(String id, FilterCriteria criteria) {
		deselectUnused(id);
		defaultFilterSet.filterNodesByGridFilter(criteria);
		defaultFilterSet.setSelectedGridFilterId(id);
	}

	protected void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());

		Clipboard clipboard = new Clipboard(getSite().getShell().getDisplay());
		bars.setGlobalActionHandler(ActionFactory.CUT.getId(),
				new CutNodesAction(viewer, clipboard));
		bars.setGlobalActionHandler(ActionFactory.COPY.getId(),
				new CopyNodesAction(viewer, clipboard));
		bars.setGlobalActionHandler(ActionFactory.PASTE.getId(),
				new PasteNodesAction(viewer, clipboard));
		bars.setGlobalActionHandler(ActionFactory.REFRESH.getId(),
				new RefreshAction(viewer));

	}

	/**
	 * creates the basic filter that displays all nodes
	 */
	protected void createDefaultFilterAction() {
		switchViewDefaultAction = new Action("Execution Services and Storages",
				IAction.AS_RADIO_BUTTON) {
			@Override
			public void run() {
				if (this.isChecked()) {
					activateFilter(this.getId(), defaultFilter);
				}
			}
		};

		/*
		 * empty ArrayList of QName will lead to display the belonging action in
		 * the root of the filter menu
		 */
		List<QName> menuLocation = Collections.emptyList();
		filterMenuCategory.add(new FilterMenuCategory(menuLocation));
		switchViewDefaultAction.setId(switchViewDefaultAction.getText());
		filterActions.add(switchViewDefaultAction);
	}

	/**
	 * creates the basic filter that displays all nodes
	 */
	protected void createNoFilterAction() {
		switchViewAllAction = new Action("All Services",
				IAction.AS_RADIO_BUTTON) {
			@Override
			public void run() {
				if (this.isChecked()) {
					activateFilter(this.getId(), null);
				}
			}
		};

		/*
		 * empty ArrayList of QName will lead to display the belonging action in
		 * the root of the filter menu
		 */
		List<QName> menuLocation = Collections.emptyList();
		filterMenuCategory.add(new FilterMenuCategory(menuLocation));
		switchViewAllAction.setId(switchViewAllAction.getText());
		filterActions.add(switchViewAllAction);
	}

	/**
	 * Collapses the tree to the top-level node and then expand it to second level.
	 * It allows us to display registry names in the tree when collapse all button is clicked. 
	 */
	protected void createCollapseAllAction(final ServiceTreeViewer treeviewer) {

		collapseAllAction = new Action("Collapse All", IAction.AS_PUSH_BUTTON) {
			@Override
			public void run() {					
				treeviewer.collapseAll();
				treeviewer.expandToLevel(2);				
			}
		};
		collapseAllAction.setId(collapseAllAction.getText());
		collapseAllAction.setToolTipText("Collapse All");
		collapseAllAction.setImageDescriptor(ServiceBrowserActivator.getImageDescriptor("collapseall.gif"));

	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		ServiceTreeViewer treeViewer = new ServiceTreeViewer(parent, SWT.MULTI
				| SWT.H_SCROLL | SWT.V_SCROLL);

		viewer = treeViewer;
		defaultFilter = new DefaultFilterCriteria(treeViewer);

		viewer.setServiceContentProvider(new ServiceContentProvider());
		viewer.setGridNode(ServiceBrowserActivator.getDefault().getGridNode());

		loadSettings();

		getSite().setSelectionProvider(treeViewer);
		getSite().registerContextMenu(treeViewer.getMenuManager(), treeViewer);
		createCollapseAllAction(treeViewer);
		contributeToActionBars();

	}

	protected void deselectUnused(String id) {
		for (int i = 0; i < this.filterActions.size(); i++) {
			Action a = filterActions.get(i);
			if (a.getStyle() == IAction.AS_RADIO_BUTTON && !(a.getId() == id)) {
				this.filterActions.get(i).setChecked(false);
			}
		}
	}

	@Override
	public void dispose() {

		saveSettings();
	}

	protected void fillContextMenu(IMenuManager manager) {

	}

	protected void fillLocalPullDown(IMenuManager manager) {

	}

	public void fillLocalToolBar(IToolBarManager manager) {
		manager.add(collapseAllAction);
		manager.add(globalRefreshAction);
		manager.add(new Separator());
		manager.add(monitorRegistryAction);
		FilterActionMenu filterMenu = new FilterActionMenu(filterActions,
				filterMenuCategory);
		manager.add(filterMenu);
	}

	protected void getActions(FilterController fc) {
		filterMenuCategory = new ArrayList<FilterMenuCategory>();
		filterActions = new ArrayList<Action>();

		createNoFilterAction();
		createDefaultFilterAction();

		/*
		 * go over all registered extensions and collect the filters in the two
		 * lists filterActions and filterMenuCategory
		 */
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(ServiceBrowserConstants.GRIDBROWSER_FILTER_EXTENSION_POINT);
		IConfigurationElement[] members = extensionPoint
				.getConfigurationElements();
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];

			try {
				IGridBrowserFilterExtensionPoint ext = (IGridBrowserFilterExtensionPoint) member
						.createExecutableExtension("name");
				GridFilterAction[] gridFilterActions = ext.getGridFilterList(
						viewer, this);
				for (GridFilterAction gfa : gridFilterActions) {
					if (gfa != null) {
						Action currentAction = gfa.getAction();
						filterActions.add(currentAction);
						filterMenuCategory.add(gfa.getCategory());
						//						filterAsStartupFilter.add(gfa.isUseAsStartupFilter());
					}
				}

			} catch (CoreException ex) {
				ServiceBrowserActivator
				.log("Could not determine all available actions for service",
						ex);
			}
		}

		adminSwitchAction = new Action("Admin mode", IAction.AS_RADIO_BUTTON) {
			@Override
			public void run() {

			}
		};
		adminSwitchAction.setToolTipText("switch on/off admin functionality");
		adminSwitchAction.setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("admin.png"));		

		globalRefreshAction = new RefreshAction(viewer.getGridNode());
		globalRefreshAction.setToolTipText("Refresh Grid");
		globalRefreshAction.setViewer(getViewer());

		monitorRegistryAction = new MonitorRegistryAction(getViewer());

		;
	}

	public DefaultFilterSet getDefaultFilterSet() {
		return defaultFilterSet;
	}

	/**
	 * Returns the dialog settings object used to maintain state between dialogs
	 * 
	 * @return the dialog settings to be used
	 */
	protected IDialogSettings getDialogSettings() {
		IDialogSettings settings = ServiceBrowserActivator.getDefault()
				.getDialogSettings();
		IDialogSettings mySettings = settings.getSection(getClass().getName());
		if (mySettings == null) {
			mySettings = settings.addNewSection(getClass().getName());
		}
		return mySettings;
	}

	public IServiceViewer getViewer() {
		return viewer;
	}

	protected void loadSettings() {
		IDialogSettings s = getDialogSettings();
		XStream xstream = getXStreamConfigurator().getXStreamForDeserialization();
		DefaultFilterSet defaultFilterSet = null;
		try {
			defaultFilterSet = (DefaultFilterSet) xstream.fromXML(s.get(FILTERING_STATE));
			defaultFilterSet.hookToPreferenceStores();
		} catch (Exception e) {
			// do nothing
		}
		if(defaultFilterSet == null)
		{
			FilterController filterController = new FilterController();
			defaultFilterSet = new DefaultFilterSet(filterController);
			defaultFilterSet.setSelectedGridFilterId(ID);
		}
		defaultFilterSet.getFilterController().addViewer((StructuredViewer) viewer);
		this.defaultFilterSet = defaultFilterSet;
		getActions(defaultFilterSet.getFilterController());
		boolean isSet = false;
		for (int i = 0; i < filterActions.size(); i++) {
			Action filterAction = filterActions.get(i);
			if (filterAction.getId().equals(defaultFilterSet.getSelectedGridFilterId())) {

				filterAction.setChecked(true);

				isSet = true;

				break;
			}
		}
		if(!isSet)
		{
			switchViewDefaultAction.setChecked(true);
			switchViewDefaultAction.run();
		}
		getViewer().loadSettings();
	}

	public void saveSettings() {
		IDialogSettings s = getDialogSettings();
		XStream xstream = getXStreamConfigurator().getXStreamForSerialization();

		s.put(FILTERING_STATE, xstream.toXML(defaultFilterSet));
	}


	private XStreamConfigurator getXStreamConfigurator()
	{
		if(xstreamConfigurator == null)
		{
			xstreamConfigurator = new XStreamConfigurator() {

				@Override
				protected void log(int status, String msg, Exception ex) {
					ServiceBrowserActivator.log(status, msg, ex);
				}

				@Override
				protected String[] getExtensionIDs() {
					return new String[]{ServiceBrowserConstants.GRIDBROWSER_FILTER_EXTENSION_POINT};
				}
			};
		}
		return xstreamConfigurator;
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}
}