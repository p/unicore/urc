/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.servicebrowser.showEditor;

import java.net.URI;
import java.util.UUID;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IStorage;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.IPropertyListener;
import org.eclipse.ui.IStorageEditorInput;
import org.eclipse.ui.IURIEditorInput;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;

public class RemoteEditorInput extends PlatformObject implements
IPathEditorInput, IStorageEditorInput, IURIEditorInput,
IFileEditorInput, IPartListener, IPropertyListener {

	private static final String textEditorId = "org.eclipse.ui.DefaultTextEditor";

	private IRemoteFile remoteFile;
	private CachedFile cachedFile = null;
	private IFile localFile = null;
	private IEditorPart editorPart;

	public RemoteEditorInput(IRemoteFile storage) {
		this.remoteFile = storage;
	}

	public void cacheFileAndOpenEditor() {

		Job j = new BackgroundJob("Opening editor") {
			@Override
			public IStatus run(IProgressMonitor progress) {
				try {
					IPath p = new Path(UUID.randomUUID().toString() + "/"
							+ remoteFile.getName());
					localFile = PathUtils.getTempFile(p, null);

					cachedFile = new CachedFile(localFile, remoteFile, true);
					cachedFile.cache(progress);
					
					openEditor();
					return Status.OK_STATUS;

				} catch (Exception e) {
					ServiceBrowserActivator.log(IStatus.ERROR, "Can't open editor", e);
					return Status.CANCEL_STATUS;
				}
			}
		};
		j.schedule();
	}

	public void dispose() {
		if (editorPart != null) {
			editorPart.getSite().getPage().removePartListener(this);
			editorPart.removePropertyListener(this);
		}
		Job cleanup = new BackgroundJob("cleaning up temporary file(s)") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				if (localFile != null && localFile.exists()) {
					try {
						localFile.getParent().delete(true, monitor);
					} catch (CoreException e) {
						e.printStackTrace();
					}
				}
				return Status.OK_STATUS;
			}

		};
		cleanup.schedule();
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof RemoteEditorInput)) {
			return false;
		}
		RemoteEditorInput other = (RemoteEditorInput) o;
		return getRemotePath() == null ? other.getRemotePath() == null
				: getRemotePath().equals(other.getRemotePath());
	}
	
	@Override
	public int hashCode() {
		return getRemotePath().hashCode();
	}

	public boolean exists() {
		return true;
	}

	@Override
	public Object getAdapter(Class adapter) {
		Object result = null;
		// return result;
		if (adapter == IPathEditorInput.class) {
			result = this;
		}
		if (adapter.isAssignableFrom(IFile.class)) {

			result = getCachedFile();
		}
		return result == null ? super.getAdapter(adapter) : result;
	}

	protected IFile getCachedFile() {
		return cachedFile;
	}

	public IFile getFile() {
		return getCachedFile();
	}

	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	public String getName() {
		return localFile.getName();
	}

	public IPath getPath() {
		return localFile.getLocation();
	}

	public IPersistableElement getPersistable() {
		return null;
	}

	public String getRemotePath() {
		return remoteFile.getRemotePath();
	}

	public IStorage getStorage() throws CoreException {
		return localFile;
	}

	public String getToolTipText() {
		return getName();
	}

	public URI getURI() {
		return getPath().toFile().toURI();
	}

	protected void openEditor() {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				IWorkbench wb = PlatformUI.getWorkbench();
				IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
				IWorkbenchPage page = win.getActivePage();
				if (page != null) {
					IEditorDescriptor desc = PlatformUI.getWorkbench()
					.getEditorRegistry().getDefaultEditor(getName());

					String editorId = textEditorId;
					if (desc != null) {
						editorId = desc.getId();
					}
					IEditorPart editor;
					try {
						editor = page.openEditor(RemoteEditorInput.this,
								editorId,true);
						
						setEditorPart(editor);
						
					}
					
					catch(Exception e)
					{
							ServiceBrowserActivator.log(IStatus.ERROR, "Cannot open editor", e);
					}
				} else {
					ServiceBrowserActivator.log(IStatus.ERROR,
					"Can't open editor : page is null");
				}
			}
		});
	}

	public void partActivated(IWorkbenchPart part) {
	}

	public void partBroughtToTop(IWorkbenchPart part) {

	}

	public void partClosed(IWorkbenchPart part) {
		if (!part.equals(editorPart)) {
			return;
		}
		dispose();

	}

	public void partDeactivated(IWorkbenchPart part) {
	}

	public void partOpened(IWorkbenchPart part) {
	}

	public void propertyChanged(Object source, int propId) {
		if (IEditorPart.PROP_INPUT == propId) {
			IEditorInput input = editorPart.getEditorInput();
			if (this != input) {
				dispose();
			}
		}
	}

	protected void setEditorPart(IEditorPart editorPart) {
		if (this.editorPart != null) {
			this.editorPart.getSite().getPage().removePartListener(this);
			this.editorPart.removePropertyListener(this);
		}
		this.editorPart = editorPart;
		if (editorPart != null) {
			editorPart.getSite().getPage().addPartListener(this);
			editorPart.addPropertyListener(this);
		}

	}
}
