/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.actions;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;

import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.unigrids.services.atomic.types.GridFileType;

import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.uas.client.StorageClient;

/**
 * @author demuth
 */
public class CreateFileAction extends NodeAction {

	String fileName = null;

	public CreateFileAction(Node node) {
		super(node);
		setText("Create File");
		setToolTipText("Create a new file on the remote storage");
		setImageDescriptor(ServiceBrowserActivator
				.getImageDescriptor("page_white_add.png"));
	}

	@Override
	public void run() {
		final Display d = PlatformUI.getWorkbench().getDisplay();
		d.syncExec(new Runnable() {

			public void run() {
				InputDialog in = new InputDialog(d.getActiveShell(),
						"Choose a name", "Please enter a name for the "
								+ getFileType() + ":", "", null);
				if (in.open() == InputDialog.OK) {
					fileName = in.getValue();
				}
			}
		});
		if (fileName == null) {
			return;
		}
		// there is no such thing as an empty file name
		if (fileName.isEmpty()) {
			ServiceBrowserActivator.log(IStatus.WARNING,
					"An empty name does not make sense, " + getFileType()
							+ " will not be created.");
			return;
		}
		try {
			// escape special characters in filenames!
			fileName = URIUtil.encode(fileName,
					org.apache.commons.httpclient.URI.allowed_fragment);
		} catch (URIException e) {

		}
		StorageClient client = null;
		String parentPath = "";

		if (getNode() instanceof StorageNode) {
			try {
				client = ((StorageNode) getNode()).getStorageClient();
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.ERROR,
						"Unable to create " + getFileType() + " " + getNode().getNamePathToRoot(), e);
			}
		} else if (getNode() instanceof FolderNode) {
			FolderNode folder = (FolderNode) getNode();
			client = folder.getStorageClient();
			GridFileType gft = folder.getGridFileType();
			if (gft != null) {
				parentPath = gft.getPath() + "/";
				try {
					// escape special characters in filenames!
					parentPath = URIUtil.encode(parentPath,
							org.apache.commons.httpclient.URI.allowed_fragment);
				} catch (URIException e) {

				}
			}
		}

		final String path = parentPath + fileName;
		GridFileType existing = null;
		try {
			existing = client.listProperties(path);

		} catch (FileNotFoundException e) {
			// thats fine
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.ERROR, "Unable to check existence of " + path, e);
		}
		if (existing != null) {
			final boolean isDir = existing.getIsDirectory();
			d.syncExec(new Runnable() {

				public void run() {
					String existingType = isDir ? "Folder" : "File";
					MessageDialog dialog = new MessageDialog(
							d.getActiveShell(), "Overwrite?", null,
							existingType + " with name " + fileName
									+ " already exists. Overwrite?",
							MessageDialog.QUESTION, new String[] {
								IDialogConstants.NO_LABEL,
								IDialogConstants.YES_LABEL
									}, 0);
					boolean overwrite = dialog.open() == 1;
					if (!overwrite)
						fileName = null;
				}
			});
			if (fileName == null)
				return;
			if (deleteBeforeCreation(isDir)) {
				try {
					client.delete(path);
				} catch (Exception e) {
					ServiceBrowserActivator.log(IStatus.ERROR,
							"Unable to overwrite: " + e.getMessage(), e);
					return;
				}
			}
		}
		create(client, path);
		getNode().refresh(1, true, null);

	}

	protected boolean deleteBeforeCreation(boolean isDir) {
		return isDir;
	}

	protected void create(StorageClient client, String path) {
		try {
			// create file by writing an empty byte[] inside
			ByteArrayInputStream empty = new ByteArrayInputStream(new byte[0]);
			UnicoreStorageTools.upload(client, empty, 0, path, null);
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.ERROR,
					"Unable to create file", e);
		}
	}

	protected String getFileType() {
		return "file";
	}
}
