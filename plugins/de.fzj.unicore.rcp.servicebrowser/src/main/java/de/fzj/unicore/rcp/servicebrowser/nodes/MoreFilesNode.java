/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;

/**
 * @author demuth
 * 
 */
public class MoreFilesNode extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6318841610250696609L;
	public static final String TYPE = "MoreFiles";
	public static final QName PORTTYPE = new QName("http://www.unicore.fzj.de",
			TYPE);

	protected transient EndpointReferenceType storageEpr;

	public MoreFilesNode(EndpointReferenceType epr) {
		super(epr);
	}

	private void addFilesToParent() {
		BackgroundJob j = new BackgroundJob(
				"looking for additional files") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				INodeData data = getData();
				if (data == null) {
					return Status.CANCEL_STATUS;
				}
				synchronized (data) {

					int limit = ServiceBrowserActivator
							.getDefault()
							.getPreferenceStore()
							.getInt(ServiceBrowserConstants.P_CHUNK_SIZE_LAZY_CHILD_RETRIEVAL);
					int offset = getOffset();
					offset += limit;
					setOffset(offset);
					getParent().refresh(1, false, monitor);

				}
				return Status.OK_STATUS;
			}
		};
		j.schedule();
	}

	@Override
	public int compareTo(Node n) {
		// put the node to the end of each list
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.nodes.Node#doubleClick()
	 */
	@Override
	public IStatus doubleClick(IProgressMonitor monitor) {
		addFilesToParent();
		return Status.OK_STATUS;
	}

	@Override
	public void finalize() throws Throwable {
		super.finalize();
	}

	@Override
	public String getDoubleClickActionName() {
		return "checking for additional files";
	}

	@Override
	public String getName() {
		return "";
	}

	int getOffset() {
		if (getParent() != null) {
			INodeData data = getParent().getData();
			if (data != null) {
				Integer result = (Integer) data
						.getProperty(StorageNode.PROPERTY_CURRENT_OFFSET);
				if (result != null) {
					return result;
				}
			}
		}
		return 0;
	}

	public EndpointReferenceType getStorageEPR() {
		if (storageEpr == null) {
			try {
				storageEpr = EndpointReferenceType.Factory
						.parse(((String) getData().getProperty(
								AbstractFileNode.PROPERTY_STORAGE_EPR)));
			} catch (Exception e) {
			}
		}
		return storageEpr;
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public boolean isPersistable() {
		// file nodes should not be persisted when shutting down the client
		return false;
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
	}

	@Override
	public void setExpanded(boolean expanded) {
		// do NOT call super.setExpanded(boolean) as this will tell the viewer
		// that this node is already expanded
		// which will hide the drill down icon
		if (expanded) {
			addFilesToParent();
		}
	}

	public void setOffset(Integer offset) {
		if (getParent() != null) {
			getParent().putData(StorageNode.PROPERTY_CURRENT_OFFSET, offset);
		}
	}

	public void setStorageEPR(EndpointReferenceType storageEPR) {
		putData(AbstractFileNode.PROPERTY_STORAGE_EPR, storageEPR.toString());
	}

	/**
	 * @return the type
	 */
	public static QName getPortType() {
		return PORTTYPE;
	}
}
