package de.fzj.unicore.rcp.servicebrowser.dnd;

import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.ui.part.PluginDropAdapter;

import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IDropAssistant;

public class ServiceDropper extends PluginDropAdapter {

	protected IDropAssistant currentAssistant;

	public ServiceDropper(StructuredViewer viewer) {
		super(viewer);
	}

	@Override
	public void drop(DropTargetEvent e) {
		if (getCurrentAssistant() != null) {
			getCurrentAssistant().performDrop(getCurrentTarget(),
					getCurrentOperation(), e.currentDataType, e.data);
		} else {
			super.drop(e);
		}
	}

	@Override
	public void dropAccept(DropTargetEvent event) {
		setCurrentAssistant(null);
		// Make match between source's and target's supported transfers
		IDropAssistant[] assistants = DropAssistantManager.getInstance()
				.findDropAssistants(event.dataTypes, getCurrentTarget());
		if (assistants.length > 0) {
			for (IDropAssistant assistant : assistants) {
				int operations = assistant.validateDrop(getCurrentTarget(),
						event.operations, event.dataTypes);
				if (DND.DROP_NONE == operations) {
					continue;
				}
				for (Transfer t : assistant.getSupportedTransferTypes()) {
					for (TransferData td : event.dataTypes) {
						if (t.isSupportedType(td)) {
							event.currentDataType = td;
							event.detail = operations;
							setCurrentAssistant(assistant);
							return;
						}
					}
				}
			}
		}
		// if we reach this, no suitable assistant was found
		super.dropAccept(event);
	}

	protected IDropAssistant getCurrentAssistant() {
		return currentAssistant;
	}

	public Transfer[] getSupportedTransferTypes() {
		return DropAssistantManager.getInstance().getSupportedTransferTypes();
	}

	@Override
	public boolean performDrop(Object data) {
		// this should not be called at all since drop was overriden
		return false;
	}

	protected void setCurrentAssistant(IDropAssistant currentAssistant) {
		this.currentAssistant = currentAssistant;
	}

	@Override
	public boolean validateDrop(final Object target, final int operation,
			final TransferData transferType) {
		TransferData[] dataTypes = new TransferData[] { transferType };
		IDropAssistant[] assistants = DropAssistantManager.getInstance()
				.findDropAssistants(dataTypes, getCurrentTarget());
		if (assistants.length > 0) {
			for (IDropAssistant assistant : assistants) {
				int operations = assistant.validateDrop(getCurrentTarget(),
						operation, dataTypes);
				if (DND.DROP_NONE != operations) {
					return true;
				}
			}
		}
		return super.validateDrop(target, operation, transferType);

	}

}
