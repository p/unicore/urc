/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.nodes;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.xmlbeans.SchemaType;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ExactType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.OperatingSystemType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType;
import org.unigrids.services.atomic.types.SiteResourceType;
import org.unigrids.services.atomic.types.StorageReferenceType;
import org.unigrids.x2006.x04.services.tss.ApplicationResourceType;
import org.unigrids.x2006.x04.services.tss.JobReferenceDocument;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument.TargetSystemProperties;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.common.utils.FileUtils;
import de.fzj.unicore.rcp.common.utils.RangeValueUtils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.applications.Application;
import de.fzj.unicore.rcp.servicebrowser.applications.ApplicationImpl;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.GroupNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.ILazyParent;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.ILazyRetriever;
import de.fzj.unicore.rcp.servicebrowser.utils.URIUtils;
import de.fzj.unicore.rcp.common.UAS;
import de.fzj.unicore.uas.client.EnumerationClient;
import de.fzj.unicore.rcp.common.utils.Utils;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * @author demuth
 * 
 */

public class TargetSystemNode extends WSRFNode implements ILazyRetriever {

	private static final long serialVersionUID = 6919459651607721323L;

	public static final String TYPE = UAS.TSS;

	public TargetSystemNode(EndpointReferenceType epr) {
		super(epr);
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
	}

	public GroupNode createJobsNode() {
		EndpointReferenceType jobNodeEpr = EndpointReferenceType.Factory
				.newInstance();
		jobNodeEpr.addNewAddress().setStringValue(getJobsNodeURI());
		Utils.addPortType(jobNodeEpr, GroupNode.PORTTYPE);
		GroupNode jobsNode = (GroupNode) NodeFactory.createNode(jobNodeEpr);
		jobsNode.setChildType(JobNode.TYPE);
		jobsNode.setNameForAllCopies("Jobs");
		return jobsNode;
	}

	/**
	 * Removes the node from the application registry.
	 */
	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * @return the list of available applications.
	 */
	public List<Application> getApplications() {
		List<Application> apps = new ArrayList<Application>();
		if (hasCachedResourceProperties()) {
			try {
				TargetSystemProperties props = getTargetSystemProperties();
				for (ApplicationResourceType app : props
						.getApplicationResourceArray()) {
					apps.add(new ApplicationImpl(app.getApplicationName(), app
							.getApplicationVersion()));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return apps;
	}

	@Override
	public DetailsAdapter getDetails() {
		DetailsAdapter details = super.getDetails();
		Map<String, String> hm = details.getMap();
		List<Application> apps = getApplications();
		if (!apps.isEmpty()) {
			StringBuffer sb = new StringBuffer();
			for (Application app : apps) {
				sb.append(app.getName());
				sb.append(" (Version ");
				sb.append(app.getApplicationVersion()).append(")");
				sb.append(System.getProperty("line.separator"));
			}
			sb.deleteCharAt(sb.length() - 1);
			hm.put("Installed Applications", sb.toString());
		}
		if (hasCachedResourceProperties()) {
			TargetSystemProperties props = getTargetSystemProperties();
			try {
				Long value = (long) RangeValueUtils.getMaximum(props
						.getTotalResourceCount());
				hm.put("Number of Nodes", value.toString());
			} catch (Exception e) {
				// do nothing
			}
			try {
				Long value = (long) RangeValueUtils.getMaximum(props
						.getIndividualCPUCount());
				hm.put("Processors per Node", value.toString());
			} catch (Exception e) {
				// do nothing
			}
			try {
				Long value = (long) RangeValueUtils.getMaximum(props
						.getTotalCPUCount());
				hm.put("Total Number of Processors", value.toString());
			} catch (Exception e) {
				// do nothing
			}
			try {
				Long value = ((long) RangeValueUtils.getMaximum(props
						.getProcessor().getIndividualCPUSpeed()))
						/ (1024 * 1024);
				hm.put("Processor Clock", value.toString() + " Mhz");
			} catch (Exception e) {
				// do nothing
			}
			try {
				Long value = ((long) RangeValueUtils.getMaximum(props.getIndividualPhysicalMemory()));
				hm.put("Processor Main Memory", FileUtils.humanReadableFileSize(value, ServiceBrowserActivator
						.getDefault().getPreferenceStore().getBoolean(ServiceBrowserConstants.P_SIZE_UNITS_SI)));
			} catch (Exception e) {
				// do nothing
			}
			try {
				String arch = props.getProcessor().getCPUArchitecture()
						.getCPUArchitectureName().toString();
				hm.put("Processor Architecture", arch);
			} catch (Exception e) {
				// do nothing
			}
			try {
				OperatingSystemType os = props.getOperatingSystem();
				StringBuffer sb = new StringBuffer();
				sb.append(os.getOperatingSystemType().getOperatingSystemName());
				sb.append(" (Version ");
				String version = os.getOperatingSystemVersion() == null ? "unknown"
						: os.getOperatingSystemVersion();
				sb.append(version).append(")");

				hm.put("Installed Operating System", sb.toString());
			} catch (Exception e) {
				// do nothing
			}
		}
		List<SiteResourceType> siteResources = getSiteSpecificResources();
		if (!siteResources.isEmpty()) {
			StringBuffer sb = new StringBuffer();
			for (SiteResourceType rsc : siteResources) {
				sb.append(rsc.getName()).append(" (");
				RangeValueType value = rsc.getValue();
				if (value == null) {
					continue;
				}
				for (ExactType exact : value.getExactArray()) {
					sb.append("default="
							+ String.valueOf(exact.getDoubleValue()));
				}
				for (RangeType rt : value.getRangeArray()) {
					if (rt.getLowerBound() != null) {
						sb.append(" min=").append(
								String.valueOf(rt.getLowerBound()
										.getDoubleValue()));
					}
					if (rt.getUpperBound() != null) {
						sb.append(" max=").append(
								String.valueOf(rt.getUpperBound()
										.getDoubleValue()));
					}
				}

				sb.append(") ");
				sb.append(rsc.getDescription());
				sb.append(System.getProperty("line.separator"));
			}
			sb.deleteCharAt(sb.length() - 1);
			hm.put("Site Specific Resources", sb.toString());
		}
		return details;

	}


	// initialised lazily using getEnumerationClient()
	private transient EnumerationClient<JobReferenceDocument> jobRefClient = null;
	
	private synchronized EnumerationClient<JobReferenceDocument> getEnumerationClient(
			TargetSystemProperties props) throws Exception {
		if(jobRefClient==null){
			if (props != null && props.isSetJobReferenceEnumeration()
					&& props.getJobReferenceEnumeration() != null) {
				jobRefClient = new EnumerationClient<JobReferenceDocument>(
						props.getJobReferenceEnumeration(), getUASSecProps(),
						JobReferenceDocument.type.getDocumentElementName());
			}
		}
		return jobRefClient;
	}

	public GroupNode getJobsNode() {
		String uri = getJobsNodeURI();
		for (Node n : getChildrenArray()) {
			if (n instanceof GroupNode) {
				if (n.getURI().toString().equals(uri)) {
					return (GroupNode) n;
				}
			}
		}
		return null;
	}

	protected String getJobsNodeURI() {
		return getURI().toString() + "/jobs";
	}

	public long getNumChildren(ILazyParent n, IProgressMonitor progress) {
		if (!hasCachedResourceProperties()) {
			retrieveProperties(progress);
		}
		TargetSystemProperties props = getTargetSystemProperties();
		if (props == null) {
			return -1;
		}
		try {
			EnumerationClient<JobReferenceDocument>enumClient = getEnumerationClient(props);
			if (enumClient != null) {
				return enumClient.getNumberOfResults();
			} else {
				if (props.getJobReferenceArray() != null) {
					return props.getJobReferenceArray().length;
				} else {
					return -1;
				}
			}
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.WARNING,
					"Unable to retrieve number of jobs for " + getNamePathToRoot(), e);
			return -1;
		}

	}

	public GroupNode getOrCreateJobsNode() {
		GroupNode jobsNode = getJobsNode();
		if (jobsNode != null) {
			return jobsNode;
		}
		// if we reach this, we don't have a jobs node => this is wrong =>
		// create one
		jobsNode = createJobsNode();
		addChild(jobsNode);
		getData().addChild(jobsNode.getData());
		return jobsNode;
	}

	@Override
	public SchemaType getSchemaType() {
		return TargetSystemPropertiesDocument.type;
	}

	/**
	 * @return the list of available site specific resources.
	 */
	public List<SiteResourceType> getSiteSpecificResources() {
		List<SiteResourceType> siteResources = new ArrayList<SiteResourceType>();
		if (hasCachedResourceProperties()) {

			try {
				TargetSystemProperties props = getTargetSystemProperties();
				for (SiteResourceType resource : props.getSiteResourceArray()) {
					siteResources.add(resource);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return siteResources;

	}

	public TargetSystemProperties getTargetSystemProperties() {
		if (!hasCachedResourceProperties()) {
			return null;
		}
		TargetSystemPropertiesDocument doc = (TargetSystemPropertiesDocument) getCachedResourcePropertyDocument();
		return doc.getTargetSystemProperties();
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public IClientConfiguration getUASSecProps() throws Exception {
		return getUASSecProps(getURI());
	}

	/**
	 * create the child nodes of this node
	 */
	@Override
	protected void retrieveChildren() throws Exception {
		INodeData data = getData();
		if (data == null) {
			return;
		}
		synchronized (data) {

			List<Node> newChildren = new ArrayList<Node>();
			if (hasCachedResourceProperties()) {
				TargetSystemProperties props = getTargetSystemProperties();

				// storage refs...
				StorageReferenceType[] allEntries = props
						.getStorageReferenceArray();
				if (allEntries.length > 0) {
					for (StorageReferenceType s : allEntries) {
						try {
								Node storage = NodeFactory.createNode(s
										.getStorageEndpointReference());
								newChildren.add(storage);
						} catch (Exception e) {
						}
					}
				}

				// jobs
				GroupNode jobsNode = getJobsNode();
				if (jobsNode == null) {
					jobsNode = createJobsNode();
				}
				// make sure to set the name right even if the "Jobs" node
				// has lost it's properties due to cache corruption
				jobsNode.setNameForAllCopies("Jobs");
				jobsNode.setChildType(JobNode.TYPE);
				newChildren.add(jobsNode);

				updateChildren(newChildren);
			}
		}

	}

	public List<Node> retrieveChildren(ILazyParent scollableNode, long offset,
			long windowSize, IProgressMonitor progress) {
		List<Node> result = new ArrayList<Node>();
		if (windowSize == 0) {
			return result;
		}
		boolean propertiesUpdated = false;
		if (!hasCachedResourceProperties()) {
			retrieveProperties(null);
			propertiesUpdated = true;
		}
		TargetSystemProperties props = getTargetSystemProperties();
		if (props == null) {
			return result;
		}
		EnumerationClient<JobReferenceDocument>enumClient=null;
		try {
			enumClient = getEnumerationClient(props);
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.WARNING,
					"Unable to retrieve jobs for target system " + getNamePathToRoot(), e);
		}
		if (enumClient != null) {
			try {
				long total = enumClient.getNumberOfResults();
				if (total > 0) {
					long start = Math.max(0, total - offset - windowSize);
					windowSize = Math.min(total - offset, windowSize); // make
																		// sure
																		// window
																		// size
																		// is
																		// not
																		// too
																		// large
					List<JobReferenceDocument> refs = enumClient.getResults(
							start, windowSize);
					// get jobs in reverse order as they appear chronologically
					// in the enum
					for (int i = refs.size() - 1; i >= 0; i--) {
						JobReferenceDocument jobReferenceDocument = refs.get(i);
						EndpointReferenceType epr = jobReferenceDocument
								.getJobReference();
						Node n = NodeFactory.createNode(epr);
						result.add(n);
						
					}
				}

			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.WARNING,
						"Unable to retrieve jobs for target system " + getNamePathToRoot(), e);
			}

		} else {
			// old server
			if (!propertiesUpdated) {
				retrieveProperties(null);
			}

			EndpointReferenceType[] jobEprs = props.getJobReferenceArray();
			if (jobEprs.length > 0) {
				long start = jobEprs.length - 1 - offset;
				long end = Math.max(0, start - windowSize + 1);
				for (long i = start; i >= end; i--) {
					int index = (int) i;
					EndpointReferenceType epr = jobEprs[index];
					try {
						Node n = NodeFactory.createNode(epr);
						result.add(n);
					} catch (Exception e) {
					}
				}
			}
		}
		return result;
	}

	@Override
	public void retrieveName() {
		URI uri = getURI();
		if (uri != null) {
			String identifier = "";
			
			setName("Target system "+identifier+"@"
					+ URIUtils.extractUnicoreServiceContainerName(uri));
		} else {
			super.retrieveName();
		}
	}

}
