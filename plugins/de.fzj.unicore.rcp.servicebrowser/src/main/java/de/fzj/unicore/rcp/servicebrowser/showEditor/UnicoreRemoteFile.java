/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.servicebrowser.showEditor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.unigrids.services.atomic.types.GridFileType;

import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;

public class UnicoreRemoteFile implements IRemoteFile {

	protected FileNode node;
	protected Object lock = new Object();

	public UnicoreRemoteFile(FileNode node) {
		this.node = node;
	}

	public InputStream getContents() throws CoreException {
		PipedInputStream result = new PipedInputStream();
		try {
			final PipedOutputStream os = new PipedOutputStream(result);
			Job j = new BackgroundJob("Downloading remote file content") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {

					try {
						UnicoreStorageTools.download(node.getStorageClient(),
								node.getGridFileType().getPath(), os, node
										.getGridFileType().getSize(), monitor);
					} catch (Exception e) {
						ServiceBrowserActivator.log(IStatus.ERROR, "Unable to download remote file content", e);
					}
					return Status.OK_STATUS;

				}
			};
			j.setUser(true);
			j.schedule();
		} catch (Exception e) {
			throw new CoreException(new Status(IStatus.ERROR,
					ServiceBrowserActivator.PLUGIN_ID, e.getMessage(), e));
		}
		return result;

	}

	public String getName() {
		return node.getName();
	}

	public String getRemotePath() {
		return node.getFileAddressWithoutProtocol().toString();
	}

	public long getSize() {
		GridFileType gft = node.getGridFileType();
		if (gft != null) {
			return gft.getSize();
		}
		return -1;
	}

	public boolean isReadOnly() {
		return false;
	}

	public void setContents(final InputStream source, long bytes,
			boolean force, IProgressMonitor monitor) throws CoreException {
		synchronized (lock) {

			OutputStream os = null;
			try {
				UnicoreStorageTools.upload(node.getStorageClient(), source,
						bytes, node.getGridFileType().getPath(),
						UnicoreStorageTools.FORCE, monitor);
			} catch (Exception e) {
				Status s = new Status(IStatus.ERROR,
						ServiceBrowserActivator.PLUGIN_ID,
						"Unable to upload file content", e);
				throw new CoreException(s);
			} finally {
				if (os != null) {
					try {
						os.close();
					} catch (IOException e) {
					}
				}
				monitor.done();
			}
		}
	}
}
