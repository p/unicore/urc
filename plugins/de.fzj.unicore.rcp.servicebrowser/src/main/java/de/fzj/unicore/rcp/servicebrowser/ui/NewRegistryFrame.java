package de.fzj.unicore.rcp.servicebrowser.ui;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

public class NewRegistryFrame extends Window {

	protected String name;
	protected URI uri;
	protected boolean takeOver = false;

	protected GridLayout gridLayout;

	protected String errorMessageURL, errorMessageName;

	protected Label labelName, labelURL, labelFallbackURL;
	protected Label errorLabel;
	protected Text textName, textURL, textFallbackURL;
	protected Button buttonClearName, buttonClearURL,
	buttonCancel, buttonOK, buttonAddFallback, buttonClearFallbackURL;
	private URI uriFallback;
	private boolean backupURLEnabled = false;


	public NewRegistryFrame(Shell parentShell) {
		super(parentShell);
		setBlockOnOpen(true);
	}

	protected void checkOK() {
		if (getErrorMessageName() != null) {
			errorLabel.setText(getErrorMessageName());
		} else if (getErrorMessageURL() != null) {
			errorLabel.setText(getErrorMessageURL());
		} else {
			errorLabel.setText("");
		}
		buttonOK.setEnabled(getErrorMessageName() == null
				&& getErrorMessageURL() == null);
	}

	@Override
	protected Control createContents(Composite parent) {

		getShell().setText("Add a new Registry");
		getShell().setBackgroundMode(SWT.INHERIT_DEFAULT);

		Composite control = new Composite(parent, SWT.NONE);
		gridLayout = new GridLayout();
		gridLayout.makeColumnsEqualWidth = false;
		gridLayout.numColumns = 5;
		control.setLayout(gridLayout);		

		errorLabel = new Label(control, SWT.NONE);
		GridData gridDataErrorLabel = new GridData(GridData.FILL_HORIZONTAL);
		gridDataErrorLabel.horizontalSpan = 5;
		errorLabel.setLayoutData(gridDataErrorLabel);
		Color red = PlatformUI.getWorkbench().getDisplay()
				.getSystemColor(SWT.COLOR_RED);
		errorLabel.setForeground(red);		

		labelName = new Label(control, SWT.NONE);
		labelName.setText("Name:");

		textName = new Text(control, SWT.BORDER);
		textName.setText("Registry");
		GridData gridDataTxt = new GridData(GridData.FILL_HORIZONTAL);
		gridDataTxt.horizontalSpan = 3;
		textName.setLayoutData(gridDataTxt);
		textName.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				try {
					String newName = textName.getText();
					if (newName.trim().length() == 0) {
						throw new Exception("Name must not be empty");
					}
					name = newName;
					setErrorMessageName(null);
				} catch (Exception e1) {
					name = null;
					setErrorMessageName(e1.getMessage());
				}
			}
		});
		textName.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				okButtonSelected();
			}

			public void widgetSelected(SelectionEvent e) {
			}
		});

		buttonClearName = new Button(control, SWT.NONE);
		buttonClearName.setText("Clear Name");
		GridData gridDataClearName = new GridData();
		gridDataClearName.grabExcessHorizontalSpace = false;
		gridDataClearName.grabExcessVerticalSpace = false;
		gridDataClearName.horizontalAlignment = GridData.FILL;
		buttonClearName.setLayoutData(gridDataClearName);
		buttonClearName
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(
					org.eclipse.swt.events.SelectionEvent e) {
				textName.setText("");
			}
		});
		labelURL = new Label(control, SWT.NONE);
		labelURL.setText("URL:");

		textURL = new Text(control, SWT.BORDER);
		String defaultUri = "https://localhost:8080/REGISTRY/services/Registry?res=default_registry";
		textURL.setText(defaultUri);
		try {
			uri = new URI(defaultUri);
		} catch (URISyntaxException e2) {
		}
		GridData gridDataTxt1 = new GridData(GridData.FILL_HORIZONTAL);
		gridDataTxt1.horizontalSpan = 3;
		textURL.setLayoutData(gridDataTxt1);
		textURL.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				try {
					String uriText = textURL.getText();
					if (uriText.trim().length() == 0) {
						throw new Exception("URL must not be empty");
					}
					uri = new URI(uriText);
					setErrorMessageURL(null);
				} catch (Exception e1) {
					uri = null;
					setErrorMessageURL(e1.getMessage());
				}
			}
		});
		textURL.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				okButtonSelected();
			}

			public void widgetSelected(SelectionEvent e) {
			}
		});

		buttonClearURL = new Button(control, SWT.NONE);
		buttonClearURL.setText("Clear URL");
		GridData gridDataClearURL = new GridData();
		gridDataClearURL.grabExcessHorizontalSpace = false;
		gridDataClearURL.grabExcessVerticalSpace = false;
		gridDataClearURL.horizontalAlignment = GridData.FILL;
		buttonClearURL.setLayoutData(gridDataClearURL);

		buttonClearURL
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(
					org.eclipse.swt.events.SelectionEvent e) {
				textURL.setText("");
			}
		});		

		buttonAddFallback = new Button(control, SWT.CHECK);
		buttonAddFallback.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {				
				setBackupURLEnabled(buttonAddFallback.getSelection());	
				if (buttonAddFallback.getSelection() == false)
					textFallbackURL.setText("");
			}
		});

		GridData gridDataEnableBackup = new GridData();
		gridDataEnableBackup.horizontalSpan = 2;
		buttonAddFallback.setLayoutData(gridDataEnableBackup);
		buttonAddFallback.setText("Add a fallback Registry (optional)");
		new Label(control, SWT.NONE);
		new Label(control, SWT.NONE);
		new Label(control, SWT.NONE);

		labelFallbackURL = new Label(control, SWT.NONE);
		labelFallbackURL.setText("URL:");
		textFallbackURL = new Text(control, SWT.BORDER);

		buttonClearFallbackURL = new Button(control, SWT.NONE);
		buttonClearFallbackURL.setText("Clear URL");	

		boolean initiallyEnabled = backupURLEnabled ;
		buttonAddFallback.setSelection(initiallyEnabled);
		setBackupURLEnabled(initiallyEnabled);	

		textFallbackURL.setText("");
		GridData gridDataTxtBackup = new GridData(GridData.FILL_HORIZONTAL);
		gridDataTxtBackup.horizontalSpan = 3;
		textFallbackURL.setLayoutData(gridDataTxtBackup);
		textFallbackURL.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				try {
					String uriTextBackup = textFallbackURL.getText();					
					uriFallback = new URI(uriTextBackup);
					setErrorMessageURL(null);
				} catch (Exception e1) {
					uriFallback = null;
					setErrorMessageURL(e1.getMessage());
				}
			}
		});
		textFallbackURL.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				okButtonSelected();
			}

			public void widgetSelected(SelectionEvent e) {
			}
		});	

		GridData gdClearURL = new GridData();
		gdClearURL.grabExcessHorizontalSpace = false;
		gdClearURL.grabExcessVerticalSpace = false;
		gdClearURL.horizontalAlignment = GridData.FILL;
		buttonClearFallbackURL.setLayoutData(gdClearURL);

		buttonClearFallbackURL
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(
					org.eclipse.swt.events.SelectionEvent e) {
				textFallbackURL.setText("");
			}
		});	

		buttonOK = new Button(control, SWT.NONE);
		buttonOK.setText("&" + IDialogConstants.OK_LABEL);
		GridData gridDataButton2 = new GridData();
		gridDataButton2.grabExcessHorizontalSpace = false;
		gridDataButton2.grabExcessVerticalSpace = false;
		gridDataButton2.horizontalAlignment = GridData.FILL;
		buttonOK.setLayoutData(gridDataButton2);
		buttonOK.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				okButtonSelected();
			}
		});

		buttonCancel = new Button(control, SWT.NONE);
		buttonCancel.setText("&" + IDialogConstants.CANCEL_LABEL);
		GridData gridDataButton3 = new GridData();
		gridDataButton3.grabExcessHorizontalSpace = false;
		gridDataButton3.grabExcessVerticalSpace = false;
		buttonCancel.setLayoutData(gridDataButton3);
		buttonCancel
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(
					org.eclipse.swt.events.SelectionEvent e) {
				setReturnCode(CANCEL);
				close();
			}
		});

		return control;
	}

	private void setBackupURLEnabled(boolean initiallyEnabled) {

		labelFallbackURL.setEnabled(initiallyEnabled);
		textFallbackURL.setEnabled(initiallyEnabled);
		buttonClearFallbackURL.setEnabled(initiallyEnabled);

	}

	public EndpointReferenceType getEpr() {
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue(uri.toString());

		return epr;
	}

	public EndpointReferenceType getFallbackEpr() {		
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		if(uriFallback != null) {
		epr.addNewAddress().setStringValue(uriFallback.toString());
		} else {
			epr.addNewAddress().setStringValue("");
		}
		return epr;		
	}

	protected String getErrorMessageName() {
		return errorMessageName;
	}

	protected String getErrorMessageURL() {
		return errorMessageURL;
	}

	public String getName() {
		return name;
	}

	protected void okButtonSelected() {
		if (getErrorMessageName() != null || getErrorMessageURL() != null) {
			return;
		}
		name = textName.getText();
		setReturnCode(OK);
		close();

	}

	protected void setErrorMessageName(String errorMessage) {
		this.errorMessageName = errorMessage;
		checkOK();
	}

	protected void setErrorMessageURL(String errorMessage) {
		this.errorMessageURL = errorMessage;
		checkOK();
	}

}
