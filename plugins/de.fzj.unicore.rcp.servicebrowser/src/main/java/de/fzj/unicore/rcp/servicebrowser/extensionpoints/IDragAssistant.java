package de.fzj.unicore.rcp.servicebrowser.extensionpoints;

import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;

public interface IDragAssistant {

	public static final String EXTENSION_POINT_ID = "NodeDNDExtensionPoint";
	public static final String ELEMENT_DRAG_ASSISTANT = "dragAssistant";
	public static final String ELEMENT_POSSIBLE_DRAG_SOURCES = "possibleDragSources";
	public static final String ELEMENT_POSSIBLE_TRANSFER_TYPES = "possibleTransferTypes";
	public static final String ATTRIBUTE_CLASS = "class";

	/**
	 * Can be used to clean up after the drag was finished
	 * 
	 * @param e
	 * @param sources
	 */
	public void dragFinished(DragSourceEvent e, Object[] sources);

	/**
	 * This method must be used to do the actual transformation of the source
	 * objects into the requested data formats. It is called in the main GUI
	 * thread so the method is responsible for creating its own background
	 * threads if it performs any expensive operations
	 * 
	 * @param e
	 * @param sources
	 * @return
	 */
	public boolean dragSetData(DragSourceEvent e, Object[] sources);

	/**
	 * Returns an array of supported transfers. The order of transfers is
	 * maintained during matchmaking with the source so that the first transfer
	 * in the list is preferred over the second etc.
	 * 
	 * @return
	 */
	public Transfer[] getSupportedTransferTypes();

	public TransferData[] getTransferDataFor(Object source);
}
