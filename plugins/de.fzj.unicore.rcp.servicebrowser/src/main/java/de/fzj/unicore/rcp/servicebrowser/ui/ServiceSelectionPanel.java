/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.servicebrowser.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.part.DrillDownAdapter;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceContentProvider;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.actions.RefreshAction;
import de.fzj.unicore.rcp.servicebrowser.filters.ContentFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterController;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;

/**
 * A panel in which nodes of certain types can be selected. BEWARE: You MUST
 * call the {@link #dispose()} method once you are done with the service
 * selection panel! * Failing to do so will cause a memory leak!
 * 
 * @author demuth
 * 
 */
public class ServiceSelectionPanel implements ISelectionChangedListener {

	public static final int DEFAULT_VIEWER_STYLE = SWT.BORDER | SWT.MULTI
			| SWT.H_SCROLL | SWT.V_SCROLL;

	protected GridNode gridNode;
	protected ServiceTreeViewer treeViewer;
	protected List<IAdaptable> selectedNodes;
	protected NodeAction globalRefreshAction;
	protected DefaultFilterSet defaultFilterSet;
	protected FilterController filterController;
	protected int maxDepth;
	protected Set<String> interestingTypes;
	protected ISelectionChangedListener listener;

	/**
	 * Create a ServiceSelectionPanel.
	 * 
	 * @param parent
	 *            the container of the panel
	 * @param interestingTypes
	 *            the types of nodes to be displayed
	 */
	public ServiceSelectionPanel(Composite parent, Set<String> interestingTypes) {
		this(parent, interestingTypes, null);
	}

	/**
	 * Create a ServiceSelectionPanel.
	 * 
	 * @param parent
	 *            the container of the panel
	 * @param interestingTypes
	 *            the types of nodes to be displayed
	 * @param selectedNodes
	 *            the default selection
	 */
	public ServiceSelectionPanel(Composite parent,
			Set<String> interestingTypes, List<IAdaptable> defaultSelectedNodes) {
		this(parent, interestingTypes, defaultSelectedNodes, null);

	}

	/**
	 * Create a ServiceSelectionPanel.
	 * 
	 * @param parent
	 *            the container of the panel
	 * @param interestingTypes
	 *            the types of nodes to be displayed
	 * @param selectedNodes
	 *            the default selection
	 * @param listener
	 *            a selection change listener that will be notified in case of
	 *            selection changes
	 */
	public ServiceSelectionPanel(Composite parent,
			Set<String> interestingTypes,
			List<IAdaptable> defaultSelectedNodes,
			ISelectionChangedListener listener) {
		this(parent, interestingTypes, defaultSelectedNodes, listener, -1);

	}

	/**
	 * Create a ServiceSelectionPanel.
	 * 
	 * @param parent
	 *            the container of the panel
	 * @param interestingTypes
	 *            the types of nodes to be displayed
	 * @param selectedNodes
	 *            the default selection
	 * @param listener
	 *            a selection change listener that will be notified in case of
	 *            selection changes
	 * @param maxDepth
	 *            maximal depth of services to be displayed. Used for limiting
	 *            memory and CPU usage.
	 */
	public ServiceSelectionPanel(Composite parent,
			Set<String> interestingTypes,
			List<IAdaptable> defaultSelectedNodes,
			ISelectionChangedListener listener, int maxDepth) {
		this(parent, interestingTypes, defaultSelectedNodes, listener,
				maxDepth, DEFAULT_VIEWER_STYLE);
	}

	/**
	 * Create a ServiceSelectionPanel.
	 * 
	 * @param parent
	 *            the container of the panel
	 * @param interestingTypes
	 *            the types of nodes to be displayed
	 * @param selectedNodes
	 *            the default selection
	 * @param listener
	 *            a selection change listener that will be notified in case of
	 *            selection changes
	 * @param maxDepth
	 *            maximal depth of services to be displayed. Used for limiting
	 *            memory and CPU usage.
	 * @param viewerStyle
	 *            the SWT style bits used to create the Tree Viewer.
	 */
	public ServiceSelectionPanel(Composite parent,
			Set<String> interestingTypes,
			List<IAdaptable> defaultSelectedNodes,
			ISelectionChangedListener listener, int maxDepth, int viewerStyle) {
		this(parent, interestingTypes, defaultSelectedNodes, listener,
				maxDepth, viewerStyle, true);
	}

	/**
	 * Create a ServiceSelectionPanel.
	 * 
	 * @param parent
	 *            the container of the panel
	 * @param interestingTypes
	 *            the types of nodes to be displayed
	 * @param selectedNodes
	 *            the default selection
	 * @param listener
	 *            a selection change listener that will be notified in case of
	 *            selection changes
	 * @param maxDepth
	 *            maximal depth of services to be displayed. Used for limiting
	 *            memory and CPU usage.
	 * @param viewerStyle
	 *            the SWT style bits used to create the Tree Viewer.
	 */
	public ServiceSelectionPanel(Composite parent,
			Set<String> interestingTypes,
			List<IAdaptable> defaultSelectedNodes,
			ISelectionChangedListener listener, int maxDepth, int viewerStyle,
			boolean init) {
		treeViewer = new ServiceTreeViewer(parent, viewerStyle);
		treeViewer.setServiceContentProvider(new ServiceContentProvider());

		treeViewer.setShowingContextMenu(false);
		selectedNodes = defaultSelectedNodes == null ? new ArrayList<IAdaptable>()
				: defaultSelectedNodes;
		new DrillDownAdapter(treeViewer);
		this.maxDepth = maxDepth;
		this.interestingTypes = interestingTypes;
		setListener(listener);
		filterController = new FilterController();
		if (init) {
			init();
		}
	}

	/**
	 * Adds a filter to the tree viewer.
	 * 
	 * @param filter
	 *            a filter.
	 */
	public void addFilter(ContentFilter filter) {
		if (filterController == null) {
			filterController = new FilterController();
		}

		filterController.addFilter(filter);
	}

	public GridNode cloneGridNode() {
		if (maxDepth >= 0) {
			return (GridNode) ServiceBrowserActivator.getDefault()
					.getGridNode().clone(maxDepth);
		} else {
			return (GridNode) ServiceBrowserActivator.getDefault()
					.getGridNode().clone();
		}
	}

	/**
	 * You MUST call this method once you are done with the service selection
	 * panel! Failing to do so will cause a memory leak!
	 */
	public void dispose() {
		if (defaultFilterSet != null) {
			defaultFilterSet.dispose();
		}
		if (gridNode != null) {
			gridNode.dispose();
		}
		if (treeViewer != null) {
			treeViewer.dispose();
		}
		gridNode = null;
	}

	public Control getControl() {
		return treeViewer.getControl();
	}

	public FilterController getFilterController() {
		return filterController;
	}

	public NodeAction getGlobalRefreshAction() {
		return globalRefreshAction;
	}

	public ISelectionChangedListener getListener() {
		return listener;
	}

	public IAdaptable[] getSelectedNodes() {
		// do not return selectedNodes object as this might be updated too late
		// (only after selectionChanged is called)
		return treeViewer.getSelectedNodes().toArray(
				new IAdaptable[selectedNodes.size()]);
	}

	public ServiceTreeViewer getTreeViewer() {
		return treeViewer;
	}

	public void init() {
		setGridNode(cloneGridNode());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(
	 * org.eclipse.jface.viewers.SelectionChangedEvent)
	 */
	public void selectionChanged(SelectionChangedEvent event) {
		IStructuredSelection selection = (IStructuredSelection) event
				.getSelection();
		selectedNodes = selection.toList();
	}

	public void setGridNode(GridNode gridNode) {
		this.gridNode = gridNode;
		treeViewer.setGridNode(gridNode);
		filterController.addViewer(treeViewer);
		defaultFilterSet = new DefaultFilterSet(filterController);
		Set<String> shownTypes = interestingTypes;
		if (shownTypes == null) {
			shownTypes = ServiceBrowserActivator.getDefault()
					.getDefaultDisplayedServiceTypes(treeViewer);
		}
		defaultFilterSet.filterNodesByType(shownTypes);
		treeViewer.addSelectionChangedListener(this);

		globalRefreshAction = new RefreshAction(gridNode);
		globalRefreshAction.setViewer(treeViewer);

		if (selectedNodes != null && selectedNodes.size() > 0) {
			setSelectedNodes(new ArrayList<IAdaptable>(selectedNodes));
		}
	}

	public void setListener(ISelectionChangedListener listener) {
		this.listener = listener;
		if (listener != null) {
			treeViewer.addSelectionChangedListener(listener);
		}
	}

	public void setSelectedNodes(List<IAdaptable> selectedNodes) {
		this.selectedNodes = selectedNodes;
		treeViewer.removeSelectionChangedListener(this);
		treeViewer.setSelectedNodes(selectedNodes);
		treeViewer.addSelectionChangedListener(this);
	}

	public void setInterestingTypes(Set<String> interestingTypes) {
		this.interestingTypes = interestingTypes;
		defaultFilterSet.filterNodesByType(interestingTypes);
	}
}
