/*****************************************************************************
 * Copyright (c) 2007, 2008 g-Eclipse Consortium 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for the
 * g-Eclipse project founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributors:
 *    Mathias Stuempert - initial API and implementation
 *    Ariel Garcia
 *    Bastian Demuth
 *****************************************************************************/

package de.fzj.unicore.rcp.logmonitor.problems;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import eu.unicore.problemutil.Problem;

/**
 * Solution to generate a problem report, including the details of the problem
 * if any and the full stack-trace of the exception.
 */
public class ReportProblemSolution extends GraphicalSolution {

	public static final String ID = "REPORT";

	/**
	 * The constructor. It will create a full error report if the parameter is a
	 * {@link ProblemException}, a simpler one for generic problemOccurrences.
	 * 
	 * @param throwable
	 *            the exception which has to be reported.
	 */
	public ReportProblemSolution() {
		setDescription("Compose problem report...");
		setId(ID);
		setApplicableFor(new String[] { "BUG" });
	}

	private Shell getShell() {

		Shell result = null;

		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();

		if (window != null) {
			result = window.getShell();
		}

		return result;

	}

	@Override
	public boolean solve(String message, Problem[] problems, String details) {
		Problem p = problems[0];
		ProblemReportDialog dialog = new ProblemReportDialog(getShell(),
				p.getCause());
		dialog.open();
		return true;
	}

}
