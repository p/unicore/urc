/*****************************************************************************
 * Copyright (c) 2008 g-Eclipse Consortium 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for the
 * g-Eclipse project founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributors:
 *    Mathias Stuempert - initial API and implementation
 *    Ariel Garcia      - modified to work for any ProblemOccurrence
 *****************************************************************************/

package de.fzj.unicore.rcp.logmonitor.problems;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IBundleGroup;
import org.eclipse.core.runtime.IBundleGroupProvider;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

/**
 * This class creates a problem report for a given {@link ProblemOccurrence}.
 */
public class ProblemReport {

	private static final String PLUG_IN_HEADER = "Plug-In"; //$NON-NLS-1$

	private static final String DESCRIPTION_HEADER = "Description"; //$NON-NLS-1$

	private static final String STACKTRACE_HEADER = "Stacktrace"; //$NON-NLS-1$

	private static final String SEVERITY_HEADER = "Severity"; //$NON-NLS-1$
	
	private static final String ENVIRONMENT_HEADER = "JVM and OS information"; //$NON-NLS-1$
	
	private static final String URC_VERSION_HEADER = "URC Version"; //$NON-NLS-1$

	private static final String THROWABLE_DESCRIPTION_TEXT = "Caught an exception: "; //$NON-NLS-1$

	private static final String HEADER_PREFIX = ":"; //$NON-NLS-1$

	private static final String FIELD_PREFIX = "\t"; //$NON-NLS-1$
	
	private static final String NL = System.getProperty("line.separator");
	private static final String NLT = NL+"\t";
	
	private static final String NA_STRING = "N/A"; //$NON-NLS-1$

	/**
	 * The Throwable for which to create a report.
	 */
	private Throwable throwable;

	/**
	 * Create a new problem report for the given {@link ProblemOccurrence}.
	 * 
	 * @param throwable
	 *            The problemOccurrence for which to create the report.
	 */
	public ProblemReport(final Throwable throwable) {
		this.throwable = throwable;
	}

	/**
	 * Create and return the report.
	 * 
	 * @return The created report as {@link String}.
	 */
	public String createReport() {

		IStatus status = null;
		if (throwable != null) {
			if (this.throwable.getCause() instanceof CoreException) {
				status = ((CoreException) this.throwable.getCause())
						.getStatus();
			}

			StringWriter sWriter = new StringWriter();
			PrintWriter pWriter = new PrintWriter(sWriter);

			if (status != null) {
				// A CoreException
				writeField(pWriter, PLUG_IN_HEADER, status.getPlugin());
				writeField(pWriter, DESCRIPTION_HEADER, status.getMessage());
				writeField(pWriter, SEVERITY_HEADER,
						severity(status.getSeverity()));
			} else {
				// Any other Throwable
				writeField(pWriter, DESCRIPTION_HEADER,
						THROWABLE_DESCRIPTION_TEXT
								+ this.throwable.getClass().getName());
			}
			writeField(pWriter, STACKTRACE_HEADER, this.throwable);
			
			writeField(pWriter, URC_VERSION_HEADER, describeURC());
			
			writeField(pWriter, ENVIRONMENT_HEADER, describEnvironment());

			pWriter.close();

			return sWriter.toString();
		} else {
			return "";
		}
	}

	private String describeURC() {
		StringBuilder sb = new StringBuilder();
		
		Set<String> ownVendors = new HashSet<String>();
		ownVendors.add("UNICORE");
		ownVendors.add("Forschungszentrum Juelich");
		
		// TODO The following is apparently not listing all of the installed bundles
		//      having a vendor name of UNICORE or Forschungszentrum Juelich
		List<Bundle> allBundles = new ArrayList<Bundle>();
		IBundleGroupProvider[] bgps = Platform.getBundleGroupProviders();
		for (IBundleGroupProvider iBundleGroupProvider : bgps) {
			IBundleGroup[] bgs = iBundleGroupProvider.getBundleGroups();
			for (IBundleGroup iBundleGroup : bgs) {
				allBundles.addAll(Arrays.asList(iBundleGroup.getBundles()));
			}
		}
		for (Bundle bundle : allBundles) {
			String vendor = bundle.getHeaders().get("Bundle-Vendor");
			String name = bundle.getHeaders().get("Bundle-Name");
			if(vendor!=null && ownVendors.contains(vendor)) {
				sb.append("UNICORE Plugin: ");
				sb.append(name!=null?name:"unknown");
				sb.append(" ");
				sb.append(bundle.getVersion());
				sb.append(NLT);
			}
		}
		sb.append(NL);
		
		return sb.toString();
	}

	private static String describEnvironment() {
		StringBuilder sb = new StringBuilder();

		// OS and version
		sb.append("OS:\t");
		sb.append(System.getProperty("os.name"));
		sb.append(" ");
		sb.append(System.getProperty("os.version"));
		sb.append(NLT);
		
		// Java Version
		sb.append("Java:\t");
		sb.append(System.getProperty("java.vm.name"));
		sb.append(" ");
		sb.append(System.getProperty("java.version"));
		sb.append(" ");
		sb.append(System.getProperty("java.vm.version"));
		sb.append(NLT);
		sb.append("Vendor:\t");
		sb.append(System.getProperty("java.vm.vendor"));
		
		sb.append(NLT);
		sb.append(NLT);
				
		// those props already contained above or otherwise unneeded
		Set<String> specialProps = new HashSet<String>();
		specialProps.add("os.name");
		specialProps.add("os.version");
		specialProps.add("java.vm.name");
		specialProps.add("java.version");
		specialProps.add("java.vm.version");
		specialProps.add("java.vm.vendor");
		specialProps.add("line.separator");
		
		
		String[] props = System.getProperties().keySet().toArray(new String[0]);
		Arrays.sort(props);

		for (String prop : props) {
			if (!(specialProps.contains(prop)||prop.startsWith("user."))) {
				sb.append(prop);
				sb.append("\t:\t");
				sb.append(System.getProperty(prop));
				sb.append(NLT);
			}
		}
		sb.append(NL);
			
		return sb.toString();
	}

	/**
	 * Converts the integer severity value carried by a CoreException to a
	 * string.
	 * 
	 * @param severity
	 *            one of {@link IStatus.OK}, {@link IStatus.INFO},
	 *            {@link IStatus.WARNING}, {@link IStatus.ERROR},
	 *            {@link IStatus.CANCEL}.
	 * @return a string describing the severity.
	 */
	private String severity(final int severity) {
		String sSeverity = NA_STRING;

		switch (severity) {
		case IStatus.OK:
			sSeverity = "OK"; //$NON-NLS-1$
			break;
		case IStatus.INFO:
			sSeverity = "INFO"; //$NON-NLS-1$
			break;
		case IStatus.WARNING:
			sSeverity = "WARNING"; //$NON-NLS-1$
			break;
		case IStatus.ERROR:
			sSeverity = "ERROR"; //$NON-NLS-1$
			break;
		case IStatus.CANCEL:
			sSeverity = "CANCEL"; //$NON-NLS-1$
			break;
		default:
			sSeverity = NA_STRING;
		}

		return sSeverity;
	}


	/**
	 * Write the specified {@link String} to the specified {@link PrintWriter}.
	 * 
	 * @param writer
	 *            The {@link PrintWriter} where the field should be written to.
	 * @param header
	 *            The header used to specify the field.
	 * @param content
	 *            The {@link String} to be written.
	 */
	private void writeField(final PrintWriter writer, final String header,
			final String content) {
		if ((content == null) || (content.length() == 0)) {
			writeField(writer, header, NA_STRING);
		} else {
			writeField(writer, header, new String[] { content });
		}
	}

	/**
	 * Write the specified {@link String}s to the specified {@link PrintWriter}.
	 * 
	 * @param writer
	 *            The {@link PrintWriter} where the field should be written to.
	 * @param header
	 *            The header used to specify the field.
	 * @param content
	 *            The {@link String}s to be written.
	 */
	private void writeField(final PrintWriter writer, final String header,
			final String[] content) {
		if ((content == null) || (content.length == 0)) {
			writeField(writer, header, NA_STRING);
		} else {
			writer.println(header + HEADER_PREFIX);
			for (String s : content) {
				writer.println(FIELD_PREFIX + s);
			}
			writer.println();
		}
	}

	/**
	 * Write the specified {@link ProblemOccurrence} to the specified
	 * {@link PrintWriter}.
	 * 
	 * @param writer
	 *            The {@link PrintWriter} where the field should be written to.
	 * @param header
	 *            The header used to specify the field.
	 * @param t
	 *            The {@link ProblemOccurrence} to be written.
	 */
	private void writeField(final PrintWriter writer, final String header,
			final Throwable t) {
		if (t == null) {
			writeField(writer, header, NA_STRING);
		} else {
			StringWriter sWriter = new StringWriter();
			PrintWriter pWriter = new PrintWriter(sWriter);
			t.printStackTrace(pWriter);
			pWriter.close();
			writeField(writer, header, sWriter.toString());
		}
	}

}
