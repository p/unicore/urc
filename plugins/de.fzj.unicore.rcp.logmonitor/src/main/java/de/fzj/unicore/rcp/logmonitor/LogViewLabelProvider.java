package de.fzj.unicore.rcp.logmonitor;

/*******************************************************************************
 * Copyright (c) 2000, 2004 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class LogViewLabelProvider extends LabelProvider implements
		ITableLabelProvider {
	private Image infoImage;
	private Image errorImage;
	private Image warningImage;
	private Image errorWithStackImage;
	List<Object> consumers = new ArrayList<Object>();

	public LogViewLabelProvider() {
		errorImage = LogActivator.getImageDescriptor("severity_error.gif")
				.createImage();
		warningImage = LogActivator.getImageDescriptor("severity_warning.gif")
				.createImage();
		infoImage = LogActivator.getImageDescriptor("severity_info.gif")
				.createImage();
		errorWithStackImage = LogActivator.getImageDescriptor(
				"severity_error_stack.gif").createImage();
	}

	public void connect(Object consumer) {
		if (!consumers.contains(consumer)) {
			consumers.add(consumer);
		}
	}

	public void disconnect(Object consumer) {
		consumers.remove(consumer);
		if (consumers.size() == 0) {
			dispose();
		}
	}

	@Override
	public void dispose() {
		errorImage.dispose();
		infoImage.dispose();
		warningImage.dispose();
		errorWithStackImage.dispose();
		super.dispose();
	}

	public Image getColumnImage(Object element, int columnIndex) {
		LogEntry entry = (LogEntry) element;
		if (columnIndex == 0) {
			switch (entry.getSeverity()) {
			case IStatus.INFO:
				return infoImage;
			case IStatus.WARNING:
				return warningImage;
			case IStatus.ERROR:
				return (entry.getStack() == null ? errorImage
						: errorWithStackImage);
			}
		}
		return null;
	}

	public String getColumnText(Object element, int columnIndex) {
		LogEntry entry = (LogEntry) element;
		switch (columnIndex) {
		case 0:
			return entry.getMessage();
		case 1:
			return entry.getPluginId();
		case 2:
			DateFormat formatter = new SimpleDateFormat(LogEntry.F_DATE_FORMAT);
			return formatter.format(entry.getDate());
			// return entry.getDate();
		}
		return ""; //$NON-NLS-1$
	}
}
