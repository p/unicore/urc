package de.fzj.unicore.rcp.logmonitor;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.UUID;

import org.eclipse.core.runtime.IStatus;

import eu.unicore.problemutil.Problem;

public class Event {

	public static final int ASCENDING = 1, DESCENDING = -1;
	
	private String id;
	private Problem[] problems;
	private IStatus status;
	private String details;
	private Date timeStamp;

	/**
	 * @deprecated Use {@link #Event(Problem[], IStatus, String, String, Date)}
	 * @param problems
	 * @param status
	 * @param stackTrace
	 */
	
	@Deprecated
	public Event(Problem[] problems, IStatus status, String stackTrace) {
		this(null,null, problems,status,stackTrace);
	}
	
	public Event(String id,  Date timeStamp, Problem[] problems, IStatus status, String stackTrace) {
		super();
		this.problems = problems;
		this.status = status;
		this.details = stackTrace;
		if(id == null) id = UUID.randomUUID().toString();
		this.id = id;
		if(timeStamp == null) timeStamp = Calendar.getInstance().getTime();
		this.timeStamp = timeStamp;
	}

	public String getDetails() {
		return details;
	}

	public Problem[] getProblems() {
		return problems;
	}

	public IStatus getStatus() {
		return status;
	}

	public void setDetails(String stackTrace) {
		this.details = stackTrace;
	}

	public void setProblems(Problem[] problems) {
		this.problems = problems;
	}

	public void setStatus(IStatus status) {
		this.status = status;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}


	public String getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Event))
			return false;
		Event other = (Event) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	/**
	 * Creates a new Comparator for events.
	 * @param order either {@link #ASCENDING} (meaning oldest event first)
	 * or {@link #DESCENDING}
	 * @return
	 */
	public static Comparator<Event> createDateComparator(final int order)
	{
		return new Comparator<Event>() {

			public int compare(Event o1, Event o2) {
				return order * o1.getTimeStamp().compareTo(o2.getTimeStamp());
			}
		};
	}

}
