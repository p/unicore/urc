/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.logmonitor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;

import de.fzj.unicore.rcp.logmonitor.utils.ReverseFileReader;

class LogReader {
	private static final int SESSION_STATE = 10;
	public static final long MAX_FILE_LENGTH = 1024 * 1024;
	private static final int ENTRY_STATE = 20;
	private static final int SUBENTRY_STATE = 30;
	private static final int MESSAGE_STATE = 40;
	private static final int STACK_STATE = 50;
	private static final int REASON_IDS_STATE = 55;
	private static final int TEXT_STATE = 60;
	private static final int UNKNOWN_STATE = 70;

	private static String LINE_SEPARATOR;

	static {
		String s = System.getProperty("line.separator"); //$NON-NLS-1$
		LINE_SEPARATOR = s == null ? "\n" : s; //$NON-NLS-1$
	}

	private static LogSession currentSession;

	private static void addEntry(LogEntry current, List<LogEntry> entries,
			int limit, boolean useCurrentSession) {
		if (useCurrentSession) {
			current.setSession(currentSession);
		}
		entries.add(0, current);
		if (limit > 0 && entries.size() > limit) {
			entries.remove(entries.size() - 1);
		}
	}

	public static LogEntry extractLastLogEntry(File file) throws IOException {
		LogEntry[] result = extractLastLogEntries(file, 1);
		if(result.length == 0) return null;
		else return result[0];
	}



	public static LogEntry[] extractLastLogEntries(File file, int number) throws IOException {
		// read the log file backwards
		ReverseFileReader reader = new ReverseFileReader(file); 
		long pos = 0;
		try {
			while(number > 0)
			{
				String line = null;
				do {
					line = reader.readLine();
					if (line != null) {
						line = line += LINE_SEPARATOR;
						pos += line.getBytes("UTF-8").length;

						if (line.startsWith("!ENTRY")) {
							break;
						}
					}
				} while (line != null);
				number--;
			}
		} finally {
			reader.close();
		}
		if (pos > 0) {
			List<LogEntry> entries = new ArrayList<LogEntry>();
			parseLogFile(file, entries, false, number, pos + 1);
			return entries.toArray(new LogEntry[entries.size()]);
			
		}
		return new LogEntry[0];

	}

	private static void parseLogFile(File file, List<LogEntry> entries,
			boolean allSessions, int limit, long tailLength) {
		List<LogEntry> parents = new ArrayList<LogEntry>();
		LogEntry current = null;
		LogSession session = null;
		int writerState = UNKNOWN_STATE;
		StringWriter swriter = null;
		PrintWriter writer = null;
		int state = UNKNOWN_STATE;

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(
					new TailInputStream(file, tailLength), "UTF-8")); //$NON-NLS-1$
			for (;;) {
				String line = reader.readLine();
				if (line == null) {
					break;
				}
				line = line.trim();
				if (line.length() == 0) {
					continue;
				}

				if (line.startsWith("!SESSION")) { //$NON-NLS-1$
					state = SESSION_STATE;
				} else if (line.startsWith("!ENTRY")) { //$NON-NLS-1$
					state = ENTRY_STATE;
				} else if (line.startsWith("!SUBENTRY")) { //$NON-NLS-1$
					state = SUBENTRY_STATE;
				} else if (line.startsWith("!MESSAGE")) { //$NON-NLS-1$
					state = MESSAGE_STATE;
				} else if (line.startsWith("!STACK")) { //$NON-NLS-1$
					state = STACK_STATE;
				} else if (line.startsWith(LoggingConstants.REASON_ID_TAG)) {
					state = REASON_IDS_STATE;
				} else {
					state = TEXT_STATE;
				}

				if (state == TEXT_STATE) {
					if (writer != null) {
						writer.println(line);
					}
					continue;
				}

				if (writer != null) {
					if (writerState == STACK_STATE && current != null) {
						current.setStack(swriter.toString());
					} else if (writerState == REASON_IDS_STATE
							&& current != null) {
						StringBuffer sb = new StringBuffer(current
								.getReasonIDs().toString());
						sb.append(swriter.toString());
						// current.setReasonIDs(sb.toString().trim().split(","));
					} else if (writerState == SESSION_STATE && session != null) {
						session.setSessionData(swriter.toString());
					} else if (writerState == MESSAGE_STATE && current != null) {
						StringBuffer sb = new StringBuffer(current.getMessage());
						sb.append(swriter.toString());
						current.setMessage(sb.toString().trim());
					}
					writerState = UNKNOWN_STATE;
					swriter = null;
					writer.close();
					writer = null;
				}

				if (state == STACK_STATE) {
					swriter = new StringWriter();
					writer = new PrintWriter(swriter, true);
					writerState = STACK_STATE;
				} else if (state == REASON_IDS_STATE) {
					swriter = new StringWriter();
					writer = new PrintWriter(swriter, true);

					if (current != null) {
						String reasons = line;
						reasons = reasons.replace(
								LoggingConstants.REASON_ID_TAG, "").trim();
						current.setReasonIDs(reasons.split(","));
					}
					writerState = REASON_IDS_STATE;
				} else if (state == SESSION_STATE) {
					session = new LogSession();
					session.processLogLine(line);
					swriter = new StringWriter();
					writer = new PrintWriter(swriter, true);
					writerState = SESSION_STATE;
					updateCurrentSession(session);
					if (!currentSession.equals(session) && !allSessions) {
						entries.clear();
					}
				} else if (state == ENTRY_STATE) {
					LogEntry entry = new LogEntry();
					entry.setSession(session);
					entry.processEntry(line);
					// try to restore associated throwable
					Throwable t = LogActivator.getDefault().getExceptionStore()
					.getException(entry.getKey());
					entry.setCause(t);

					// add entry
					setNewParent(parents, entry, 0);
					current = entry;
					addEntry(current, entries, limit, true);
				} else if (state == SUBENTRY_STATE) {
					if (parents.size() > 0) {
						LogEntry entry = new LogEntry();
						entry.setSession(session);
						int depth = entry.processSubEntry(line);
						setNewParent(parents, entry, depth);
						current = entry;
						LogEntry parent = parents.get(depth - 1);
						// try to restore associated throwable
						Throwable t = LogActivator.getDefault()
						.getExceptionStore()
						.getException(entry.getKey());
						entry.setCause(t);

						// add entry
						parent.addChild(entry);
					}
				} else if (state == MESSAGE_STATE) {
					swriter = new StringWriter();
					writer = new PrintWriter(swriter, true);
					String message = ""; //$NON-NLS-1$
					if (line.length() > 8) {
						message = line.substring(9).trim();
					}
					message = message.trim();
					if (current != null) {
						current.setMessage(message);
					}
					writerState = MESSAGE_STATE;
				}
			}

			if (swriter != null && current != null
					&& writerState == STACK_STATE) {
				current.setStack(swriter.toString());
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e1) {
			}
			if (writer != null) {
				writer.close();
			}
		}
	}

	public static void parseLogFile(File file, List<LogEntry> entries,
			IEclipsePreferences prefs) {
		boolean allSessions = prefs.getBoolean(
				LoggingConstants.P_SHOW_ALL_SESSIONS, false);
		boolean useLimit = prefs
		.getBoolean(LoggingConstants.P_USE_LIMIT, false);
		int limit = useLimit ? prefs.getInt(LoggingConstants.P_SHOW_LOG_LIMIT,
				1000) : -1;

		parseLogFile(file, entries, allSessions, limit, MAX_FILE_LENGTH);

	}

	public static void reset() {
		currentSession = null;
	}

	private static void setNewParent(List<LogEntry> parents, LogEntry entry,
			int depth) {
		if (depth + 1 > parents.size()) {
			parents.add(entry);
		} else {
			parents.set(depth, entry);
		}
	}

	private static void updateCurrentSession(LogSession session) {
		if (currentSession == null) {
			currentSession = session;
			return;
		}
		Date currentDate = currentSession.getDate();
		Date sessionDate = session.getDate();
		if (currentDate == null && sessionDate != null) {
			currentSession = session;
		} else if (currentDate != null && sessionDate == null) {
			currentSession = session;
		} else if (currentDate != null && sessionDate != null
				&& sessionDate.after(currentDate)) {
			currentSession = session;
		}
	}
}
