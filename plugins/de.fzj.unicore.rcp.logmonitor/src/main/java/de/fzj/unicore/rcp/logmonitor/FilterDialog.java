/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.logmonitor;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IMemento;

import de.fzj.unicore.rcp.logmonitor.problems.ProblemCenter;
import eu.unicore.problemutil.Reason;

/**
 * The FilterDialog class controls the filter options for logging
 * 
 * @author Henning Mersch, Valentina Huber, Bastian Demuth
 * @version $Id: FilterDialog.java, v 1.1 2007/03/08 vhuber Exp $
 */
public class FilterDialog extends Dialog implements LoggingConstants {

	private static final IStatus INFO_STATUS = new Status(IStatus.INFO,
			LogActivator.PLUGIN_ID, "");
	private static final IStatus WARNING_STATUS = new Status(IStatus.WARNING,
			LogActivator.PLUGIN_ID, "");
	private static final IStatus ERROR_STATUS = new Status(IStatus.ERROR,
			LogActivator.PLUGIN_ID, "");
	private Button limit;
	private Text limitText;

	private Button okButton;
	private Button logErrorButton;
	private Button displayErrorButton;
	private Button popupErrorButton;
	private Button logWarningButton;
	private Button displayWarningButton;
	private Button popupWarningButton;
	private Button logInfoButton;
	private Button displayInfoButton;
	private Button popupInfoButton;
	private Button showAllButton;
	private Button showForReasonButton;
	private Button hideForReasonButton;
	private Button undecidedForReasonButton;
	private Combo fineGrainedCombo;
	private IMemento memento;

	private Reason[] reasons;
	private Boolean[] logForReasons;
	private int selectedReason = 0;

	public FilterDialog(Shell parentShell, IMemento memento) {
		super(parentShell);
		this.memento = memento;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		okButton = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		createEventTypesGroup(container);
		createSessionSection(container);
		loadSetup();
		Dialog.applyDialogFont(container);
		return container;
	}

	private void createEventTypesGroup(Composite parent) {
		Group globalGroup = new Group(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		globalGroup.setLayout(layout);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		globalGroup.setLayoutData(gd);
		globalGroup
				.setText("Global event settings depending on severity levels"); //$NON-NLS-1$

		Label logLabel = new Label(globalGroup, SWT.NONE);
		logLabel.setText("Log events:"); //$NON-NLS-1$
		gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		logLabel.setLayoutData(gd);

		Composite logGroup = new Composite(globalGroup, SWT.NONE);
		logGroup.setLayout(new GridLayout(3, false));
		gd = new GridData(GridData.FILL_HORIZONTAL);
		logGroup.setLayoutData(gd);

		logInfoButton = new Button(logGroup, SWT.CHECK);
		logInfoButton.setText("Information"); //$NON-NLS-1$
		gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		logInfoButton.setLayoutData(gd);
		logWarningButton = new Button(logGroup, SWT.CHECK);
		logWarningButton.setText("Warning"); //$NON-NLS-1$
		gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		logWarningButton.setLayoutData(gd);
		logErrorButton = new Button(logGroup, SWT.CHECK);
		logErrorButton.setText("Error"); //$NON-NLS-1$
		gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		logErrorButton.setLayoutData(gd);

		Label displayLabel = new Label(globalGroup, SWT.NONE);
		displayLabel.setText("Display events in log view:"); //$NON-NLS-1$

		Composite displayGroup = new Composite(globalGroup, SWT.NONE);
		displayGroup.setLayout(new GridLayout(3, false));
		gd = new GridData(GridData.FILL_HORIZONTAL);
		displayGroup.setLayoutData(gd);

		displayInfoButton = new Button(displayGroup, SWT.CHECK);
		displayInfoButton.setText("Information"); //$NON-NLS-1$
		gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		displayInfoButton.setLayoutData(gd);
		displayWarningButton = new Button(displayGroup, SWT.CHECK);
		displayWarningButton.setText("Warning"); //$NON-NLS-1$
		gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		displayWarningButton.setLayoutData(gd);

		displayErrorButton = new Button(displayGroup, SWT.CHECK);
		displayErrorButton.setText("Error");
		gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		displayErrorButton.setLayoutData(gd);

		Label popupLabel = new Label(globalGroup, SWT.NONE);
		popupLabel.setText("Popup dialog for events:"); //$NON-NLS-1$

		Composite popupGroup = new Composite(globalGroup, SWT.NONE);
		popupGroup.setLayout(new GridLayout(3, false));
		gd = new GridData(GridData.FILL_HORIZONTAL);
		popupGroup.setLayoutData(gd);

		popupInfoButton = new Button(popupGroup, SWT.CHECK);
		popupInfoButton.setText("Information"); //$NON-NLS-1$
		gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		popupInfoButton.setLayoutData(gd);

		popupWarningButton = new Button(popupGroup, SWT.CHECK);
		popupWarningButton.setText("Warning"); //$NON-NLS-1$
		gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		popupWarningButton.setLayoutData(gd);

		popupErrorButton = new Button(popupGroup, SWT.CHECK);
		popupErrorButton.setText("Error"); //$NON-NLS-1$
		gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		popupErrorButton.setLayoutData(gd);

		Group fineGrainedGroup = new Group(parent, SWT.NONE);
		fineGrainedGroup.setLayout(new GridLayout(2, false));
		gd = new GridData(GridData.FILL_HORIZONTAL);
		fineGrainedGroup.setLayoutData(gd);
		fineGrainedGroup
				.setText("Override global settings for events with specific reasons"); //$NON-NLS-1$

		Label fineGrainedLabel = new Label(fineGrainedGroup, SWT.NONE);
		fineGrainedLabel.setText("Popup for reason:"); //$NON-NLS-1$
		gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		fineGrainedLabel.setLayoutData(gd);

		fineGrainedCombo = new Combo(fineGrainedGroup, SWT.READ_ONLY);
		gd = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		fineGrainedCombo.setLayoutData(gd);

		reasons = ProblemCenter.getProblemConfig().getAllReasons();
		String[] items = new String[reasons.length];
		logForReasons = new Boolean[reasons.length];
		for (int i = 0; i < reasons.length; i++) {
			items[i] = reasons[i].getName();
			logForReasons[i] = LogActivator.getAlarmUser(reasons[i]);
		}
		fineGrainedCombo.setItems(items);
		fineGrainedCombo.select(selectedReason);

		Label spacer = new Label(fineGrainedGroup, SWT.NONE);
		spacer.setVisible(false);

		Composite fineGrainedRadios = new Composite(fineGrainedGroup, SWT.NONE);
		gd = new GridData(SWT.FILL, SWT.CENTER, false, false);
		gd.horizontalSpan = 1;
		fineGrainedRadios.setLayoutData(gd);
		fineGrainedRadios.setLayout(new GridLayout(3, false));
		showForReasonButton = new Button(fineGrainedRadios, SWT.RADIO);
		showForReasonButton.setText("Always");

		hideForReasonButton = new Button(fineGrainedRadios, SWT.RADIO);
		hideForReasonButton.setText("Never");
		undecidedForReasonButton = new Button(fineGrainedRadios, SWT.RADIO);
		undecidedForReasonButton.setText("Depending on event severity");

		updateFineGrainedGroup();
		fineGrainedCombo.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {

			}

			public void widgetSelected(SelectionEvent e) {

				Boolean b = undecidedForReasonButton.getSelection();
				if (b) {
					b = null;
				} else {
					b = showForReasonButton.getSelection();
				}
				logForReasons[selectedReason] = b;
				selectedReason = fineGrainedCombo.getSelectionIndex();
				updateFineGrainedGroup();
			}
		});
	}

	private void createSessionSection(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		container.setLayout(layout);
		container.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		IEclipsePreferences prefs = LogActivator.getDefault().getPreferences();
		limit = new Button(container, SWT.CHECK);
		limit.setText("Limit displayed events to"); //$NON-NLS-1$
		limit.setSelection(prefs.getBoolean(P_USE_LIMIT, true));
		limit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				limitText.setEnabled(((Button) e.getSource()).getSelection());
			}
		});

		limitText = new Text(container, SWT.BORDER);
		limitText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				try {
					if (okButton == null) {
						return;
					}
					Integer.parseInt(limitText.getText());
					okButton.setEnabled(true);
				} catch (NumberFormatException e1) {
					okButton.setEnabled(false);
				}
			}
		});
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 2;
		data.horizontalIndent = 20;

		limitText.setLayoutData(data);
		limitText.setText(prefs.get(LoggingConstants.P_SHOW_LOG_LIMIT, ""));
		limitText.setEnabled(limit.getSelection());

		Label label = new Label(container, SWT.NONE);
		label.setText("Show events from"); //$NON-NLS-1$

		GridData gd = new GridData();
		gd.horizontalIndent = 20;
		showAllButton = new Button(container, SWT.RADIO);
		showAllButton.setText("All sessions"); //$NON-NLS-1$
		showAllButton.setLayoutData(gd);

		Button button = new Button(container, SWT.RADIO);
		button.setText("Recent session"); //$NON-NLS-1$

		if (prefs.getBoolean(P_SHOW_ALL_SESSIONS, false)) {
			showAllButton.setSelection(true);
		} else {
			button.setSelection(true);
		}
	}

	private void loadSetup() {

		displayInfoButton.setSelection(!Boolean.FALSE.toString().equals(
				memento.getString(LoggingConstants.P_SHOW_INFO)));
		displayWarningButton.setSelection(!Boolean.FALSE.toString().equals(
				memento.getString(LoggingConstants.P_SHOW_WARNING)));
		displayErrorButton.setSelection(!Boolean.FALSE.toString().equals(
				memento.getString(LoggingConstants.P_SHOW_ERROR)));

		logInfoButton.setSelection(LogActivator.isloggingSeverity(
				LogActivator.PLUGIN_ID, IStatus.INFO));
		logWarningButton.setSelection(LogActivator.isloggingSeverity(
				LogActivator.PLUGIN_ID, IStatus.WARNING));
		logErrorButton.setSelection(LogActivator.isloggingSeverity(
				LogActivator.PLUGIN_ID, IStatus.ERROR));

		popupInfoButton.setSelection(LogActivator.getAlarmUser(INFO_STATUS));
		popupWarningButton.setSelection(LogActivator
				.getAlarmUser(WARNING_STATUS));
		popupErrorButton.setSelection(LogActivator.getAlarmUser(ERROR_STATUS));
	}

	@Override
	protected void okPressed() {

		saveSetup();

		super.okPressed();
	}

	private void saveSetup() {

		memento.putBoolean(LoggingConstants.P_SHOW_INFO,
				displayInfoButton.getSelection());
		memento.putBoolean(LoggingConstants.P_SHOW_WARNING,
				displayWarningButton.getSelection());
		memento.putBoolean(LoggingConstants.P_SHOW_ERROR,
				displayErrorButton.getSelection());

		IEclipsePreferences prefs = LogActivator.getDefault().getPreferences();

		prefs.put(P_SHOW_LOG_LIMIT, limitText.getText());
		prefs.putBoolean(P_USE_LIMIT, limit.getSelection());
		prefs.putBoolean(P_SHOW_ALL_SESSIONS, showAllButton.getSelection());

		LogActivator.setLoggingSeverity(IStatus.INFO,
				logInfoButton.getSelection());
		LogActivator.setLoggingSeverity(IStatus.WARNING,
				logWarningButton.getSelection());
		LogActivator.setLoggingSeverity(IStatus.ERROR,
				logErrorButton.getSelection());

		LogActivator.setAlarmUser(IStatus.INFO, popupInfoButton.getSelection());
		LogActivator.setAlarmUser(IStatus.WARNING,
				popupWarningButton.getSelection());
		LogActivator.setAlarmUser(IStatus.ERROR,
				popupErrorButton.getSelection());

		Boolean b = undecidedForReasonButton.getSelection();
		if (b) {
			b = null;
		} else {
			b = showForReasonButton.getSelection();
		}
		logForReasons[selectedReason] = b;
		for (int i = 0; i < reasons.length; i++) {
			Reason r = reasons[i];
			LogActivator.setAlarmUser(r.getId(), logForReasons[i]);
		}

	}

	private void updateFineGrainedGroup() {

		updateFineGrainedGroup(logForReasons[selectedReason]);
	}

	private void updateFineGrainedGroup(Boolean b) {
		showForReasonButton.setSelection(false);
		hideForReasonButton.setSelection(false);
		undecidedForReasonButton.setSelection(false);

		if (b == null) {
			undecidedForReasonButton.setSelection(true);
		} else if (b) {
			showForReasonButton.setSelection(true);
		} else {
			hideForReasonButton.setSelection(true);
		}
	}

}
