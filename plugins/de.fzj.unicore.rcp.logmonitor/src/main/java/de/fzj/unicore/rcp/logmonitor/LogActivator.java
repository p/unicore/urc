/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.logmonitor;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.ILogListener;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.fzj.unicore.rcp.logmonitor.problems.ProblemCenter;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import eu.unicore.problemutil.Problem;
import eu.unicore.problemutil.Reason;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author Henning Mersch, Valentina Huber
 * @version $Id: LogActivator.java, v 1.1 2007/03/08 vhuber Exp $
 */
public class LogActivator extends AbstractUIPlugin implements ILogListener,
LoggingConstants {

	// The plug-in ID
	public static final String PLUGIN_ID = "de.fzj.unicore.rcp.logmonitor";
	public static final String ICONS_PATH = "$nl$/icons/";//$NON-NLS-1$

	// The shared instance
	private static LogActivator plugin;

	private boolean fFirstEvent = true;

	private IEventQueue eventQueue = new EventQueue();
	private EventDialog eventDialog;

	private ExceptionStore exceptionStore = new ExceptionStore();
	private List<LogEntry> logEntries = new ArrayList<LogEntry>();

	private static Set<IStatus> popupForced = new HashSet<IStatus>();

	/**
	 * The constructor
	 */
	public LogActivator() {
		plugin = this;
	}

	IEventQueue getEventQueue()
	{
		return eventQueue;
	}
	void openEventDialog(Shell shell)
	{
		if (eventDialog == null) {

			EventDialog dialog = new EventDialog(shell,	eventQueue) {
				@Override
				public boolean close() {
					boolean result = super.close();
					eventDialog = null;
					eventQueue.clear();
					return result;
				}
			};
			eventDialog = dialog;
			dialog.setBlockOnOpen(false);
			dialog.open();
		}

	}
	
	public ExceptionStore getExceptionStore() {
		return exceptionStore;
	}

	public List<LogEntry> getLogEntries() {
		return logEntries;
	}

	/**
	 * Get our preferences
	 */
	public IEclipsePreferences getPreferences() {
		IEclipsePreferences root = (IEclipsePreferences) Platform
		.getPreferencesService().getRootNode()
		.node(InstanceScope.SCOPE);
		return (IEclipsePreferences) root.node(PLUGIN_ID);
	}

	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		String[][] images = { { "reason", "reason.gif" }, //$NON-NLS-1$ //$NON-NLS-2$
				{ "solution", "solution.gif" }, //$NON-NLS-1$ //$NON-NLS-2$

		};

		ImageDescriptor imgDsc = null;
		for (String[] image : images) {
			imgDsc = getImageDescriptor(image[1]);
			reg.put(image[0], imgDsc);
		}
	}

	boolean isOpeningLogViewOnEvent() {
		return getPreferences().getBoolean(P_ACTIVATE, false);
	}

	protected LogEntry loadLastLogEntry(File logFile, IStatus status) {
		try {

			// we have a problem with timing here, since additional log entries
			// might already have been written
			// => extract last 10 entries and search for the right one

			// first create an entry similar to the one we're looking for
			// we can't use this entry cause we need the correct time stamp
			// which can only be found in the log
			LogEntry lookingFor = new LogEntry(status);

			LogEntry[] entries = LogReader.extractLastLogEntries(logFile,10);
			for(LogEntry entry : entries)
			{
				if (entry != null) {
					if(entry.equals(lookingFor))
					{
						if (entry.getKey() != null) {
							storeException(entry.getKey(), status.getException());
						}
						// the following values might not have been serialized
						// properly and are therefore replaced with the ones
						// extracted from the status..
						entry.setReasonIDs(lookingFor.getReasonIDs());
						entry.setCause(status.getException());
						addLogEntry(entry);
						return entry;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Add given log entry and make sure log entries are sorted by date
	 * @param entry
	 */
	private synchronized void addLogEntry(LogEntry entry)
	{
		
		int index = Collections.binarySearch(logEntries,entry, new Comparator<LogEntry>() {
			public int compare(LogEntry o1, LogEntry o2) {
				return o1.getDate().compareTo(o2.getDate());
			}
		});
		if(index < 0)
		{
			index *= -1;
			index --;
		}
		logEntries.add(entry);
	}

	public void logging(final IStatus status, final String plugin) {
		final File logFile = Platform.getLogFileLocation().toFile();
		
		storeReasonIDs(logFile, status);
		
		final boolean forceAlarmUser = popupForced.remove(status);
		LogEntry latestEntry;
		if (fFirstEvent) {
			reloadLogFile(logFile);
			// latest log entry corresponds to this status and caused this
			// method call
			// make sure the exception from the status is saved and correctly
			// referenced by this last entry
			latestEntry = logEntries.get(0);
			exceptionStore.storeException(latestEntry.getKey(),
					status.getException());
			latestEntry.setCause(status.getException());
			fFirstEvent = false;
			refreshLogView();
			if (forceAlarmUser || getAlarmUser(status)
					&& PlatformUI.isWorkbenchRunning()) {
				Date d = latestEntry == null ? null : latestEntry.getDate();
				Event evt = createEvent(latestEntry.getKey(),d,status);
				showAlarm(evt);
			}
		} else {
			if (PlatformUI.getWorkbench().isStarting()) {
				latestEntry = loadLastLogEntry(logFile, status);
				refreshLogView();
				if (forceAlarmUser || getAlarmUser(status)) {
					Date d = latestEntry == null ? null : latestEntry.getDate();
					Event evt = createEvent(latestEntry.getKey(),d,status);
					showAlarm(evt);
				}
			} else {
				Job j = new Job("displaying log event") {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						LogEntry latestEntry = loadLastLogEntry(logFile, status);
						refreshLogView();
						if (latestEntry != null && (forceAlarmUser || getAlarmUser(status))) {
							Date d = latestEntry.getDate();
							Event evt = createEvent(latestEntry.getKey(),d,status);
							showAlarm(evt);
						}
						return Status.OK_STATUS;
					}
				};
				j.setSystem(true);
				j.schedule();
			}
		}

	}

	public List<LogEntry> readLogFile(File logFile) {
		List<LogEntry> result = new ArrayList<LogEntry>();
		logEntries.clear();
		if (!logFile.exists()) {
			return result;
		}
		LogReader.parseLogFile(logFile, result, getPreferences());
		return result;
	}

	protected void refreshLogView() {
		try {
			PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {

				public void run() {
					try {
						LogView view = (LogView) PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.findView(LogView.ID);
						view.refresh(false);
					} catch (Exception e) {
						// do nothing
					}

				}
			});
		} catch (Exception e) {
			// do nothing

		}

	}

	public void reloadLogFile(File logFile) {
		logEntries = readLogFile(logFile);
	}

	void setOpeningLogViewOnEvent(boolean open) {
		getPreferences().putBoolean(P_ACTIVATE, open);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		Platform.addLogListener(this);

		// try to avoid having log4j log to the console
		// if a plugin wants to have log4j write to the plugin log instead
		// it should add a
		// de.fzj.unicore.rcp.logmonitor.utilsLog4JEclipseLogAppender
		Logger l = Logger.getLogger(getClass());
		l.removeAllAppenders();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		
		Job.getJobManager().cancel(BackgroundJob.UNICORE_JOB_FAMILY);
		plugin = null;
		super.stop(context);
		Platform.removeLogListener(this);
	}

	private void storeException(String key, Throwable t) {
		// try to save the exception to our exception store
		if (t != null) {
			if (!exceptionStore.containsException(key)) {
				exceptionStore.storeException(key, t);
			} else {
				System.out.println("Found ambiguous key for logged exception: "
						+ key);
			}
		}
	}

	private void storeReasonIDs(File logFile, IStatus status) {
		if (logFile.canWrite() && status.getException() != null) {
			String reasonString = REASON_ID_TAG + " ";
			List<Problem> problems = ProblemCenter.identifyProblems(status);
			if (problems.size() > 0) {
				for (int i = 0; i < problems.size(); i++) {
					Problem p = problems.get(i);
					reasonString += p.getReason().getId();
					if (i < problems.size() - 1) {
						reasonString += ",";
					}
				}
				try {
					FileWriter writer = new FileWriter(logFile, true);
					writer.write(reasonString + "\n");
					writer.close();
				} catch (Exception e) {
					// do nothing, this is a best-effort service
				}
			}
		}
	}

	private static boolean getAlarmUser(int status) {
		return getAlarmUser(PLUGIN_ID, status);
	}

	/**
	 * Returns true iff a popup should appear for an error with the given
	 * status. First, the exception inside the status is evaluated. If we
	 * recognize it and have specific user preferences on how to deal with it,
	 * we'll follow these preferences. Otherwise, we'll just regard the status
	 * severity. Note, that plugin ids are ignored since client version 6.4.0
	 * cause setting up different log levels per plugin seemed to be too
	 * unintuitive for users
	 * 
	 * @since 6.4.0
	 **/
	public static boolean getAlarmUser(IStatus status) {
		LogActivator plugin = getDefault();
		if (plugin == null) {
			return false; // we have been deactivated
		}
		IEclipsePreferences p = plugin.getPreferences();

		boolean allFalse = true;
		List<Problem> problems = ProblemCenter.identifyProblems(status);
		if (problems.size() == 0) {
			Reason r = ProblemCenter.createUnknownReason(status);
			Problem problem = new Problem(r, status.getException(), null);
			problems = new ArrayList<Problem>();
			problems.add(problem);
		}

		for (Problem problem : problems) {
			String key = LoggingConstants.P_POPUP_FINE_GRAINED_PREFIX
			+ problem.getReason().getId();

			if (p.get(key, null) == null) {
				allFalse = false;
			} else {
				if (p.getBoolean(key, false)) {
					return true;
				}
			}

		}
		if (allFalse) {
			return false;
		}


		// if we made it here, no explicit rule for this status could be found
		// so we apply our general rules
		String key = LoggingConstants.P_POPUP_INFO;
		switch (status.getSeverity()) {
		case IStatus.WARNING:
			key = LoggingConstants.P_POPUP_WARNING;
			break;
		case IStatus.ERROR:
			key = LoggingConstants.P_POPUP_ERROR;
			break;
		}
		return p.getBoolean(key, false);

	}

	/**
	 * This returns true if a popup should be shown for a specific reason, false
	 * if a popup should NOT be shown and null if we're undecided, ie. showing a
	 * popup depends on event severity instead of the reason.
	 * 
	 * @param r
	 * @return
	 */
	static Boolean getAlarmUser(Reason r) {
		IEclipsePreferences p = getDefault().getPreferences();
		String key = LoggingConstants.P_POPUP_FINE_GRAINED_PREFIX + r.getId();

		if (p.get(key, null) == null) {
			return null;
		}
		return p.getBoolean(key, false);

	}

	/**
	 * Returns true iff a popup should appear for an error with the given
	 * status. Since we don't have an exception here, we only regard the
	 * severity. Plugin ids are ignored since client version 6.4.0 cause setting
	 * up different log levels per plugin seemed to be too unintuitive for users
	 * 
	 * @param pluginId
	 * @param status
	 * @return
	 */
	public static boolean getAlarmUser(String pluginId, int status) {

		IEclipsePreferences p = getDefault().getPreferences();
		String key = LoggingConstants.P_POPUP_INFO;
		switch (status) {
		case IStatus.WARNING:
			key = LoggingConstants.P_POPUP_WARNING;
			break;
		case IStatus.ERROR:
			key = LoggingConstants.P_POPUP_ERROR;
			break;
		}
		boolean popup = p.getBoolean(key, false);
		return popup;
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static LogActivator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		if (imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH + path) == null) {
			log(IStatus.WARNING, "Can't find image " + path);
		}
		return imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH + path);

	}

	/**
	 * Returns whether messages with a given severity should be logged for the
	 * plugin with the given id. At the moment, we only have global log levels
	 * and ignore the plugin ids.
	 * 
	 * @param pluginId
	 * @param severity
	 * @return
	 */
	public static boolean isloggingSeverity(String pluginId, int severity) {
		LogActivator plugin = getDefault();
		if (plugin == null) {
			return false;
		}

		String key = P_LOG_INFO;
		switch (severity) {
		case IStatus.WARNING:
			key = P_LOG_WARNING;
			break;
		case IStatus.ERROR:
			key = P_LOG_ERROR;
			break;
		}
		return Platform.getPreferencesService().getBoolean(PLUGIN_ID, key, false, null);

	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, getAlarmUser(status));
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param severity
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 * @param alarmUser
	 *            true means popup should alert user
	 */
	public static void log(int severity, String msg, Exception e,
			boolean alarmUser) {
		IStatus s = new Status(severity, PLUGIN_ID, IStatus.OK, msg, e);
		log(plugin, s, alarmUser);
	}

	public static void log(Plugin sourcePlugin, IStatus status,
			boolean forceAlarmUser) {
		String pluginId = sourcePlugin == null ? PLUGIN_ID : sourcePlugin.getBundle().getSymbolicName();
		if (forceAlarmUser) {
			popupForced.add(status);
		}
		if (isloggingSeverity(pluginId, status.getSeverity())) {
			sourcePlugin.getLog().log(status);

		}

	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 */
	public static void log(String msg) {
		log(msg, null);
	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(String msg, Exception e) {
		log(IStatus.INFO, msg, e, getAlarmUser(IStatus.INFO));
	}

	static void setAlarmUser(int severity, boolean b) {
		IEclipsePreferences p = getDefault().getPreferences();
		String key = LoggingConstants.P_POPUP_INFO;
		switch (severity) {
		case IStatus.WARNING:
			key = LoggingConstants.P_POPUP_WARNING;
			break;
		case IStatus.ERROR:
			key = LoggingConstants.P_POPUP_ERROR;
			break;
		}
		p.putBoolean(key, b);
	}

	/**
	 * Tells the logging component whether a popup should be opened when an
	 * event with the reason with the given id occurs. reason.
	 * 
	 * @param reasonId
	 * @param b
	 *            true if a popup should always be shown, false if a popup
	 *            should not be shown, null if we let the event severity decide
	 *            instead
	 */
	static void setAlarmUser(String reasonId, Boolean b) {
		IEclipsePreferences p = getDefault().getPreferences();
		String key = LoggingConstants.P_POPUP_FINE_GRAINED_PREFIX + reasonId;

		if (b == null) {
			p.remove(key);
		} else {
			p.putBoolean(key, b);
		}
	}

	static void setLoggingSeverity(int severity, boolean b) {
		IEclipsePreferences p = getDefault().getPreferences();
		String key = LoggingConstants.P_LOG_INFO;
		switch (severity) {
		case IStatus.WARNING:
			key = LoggingConstants.P_LOG_WARNING;
			break;
		case IStatus.ERROR:
			key = LoggingConstants.P_LOG_ERROR;
			break;
		}
		p.putBoolean(key, b);
	}

	/**
	 * Popup an dialog to show a message or an error to user.
	 * @deprecated Use {@link #showAlarm(IStatus, Date)} instead.
	 * @param s
	 */
	@Deprecated
	public static void showAlarm(final IStatus s) {
		if(s == null) return;
		Event e = createEvent(null,	null,s);
		showAlarm(e);
		
	}
	
	/**
	 * Popup an dialog to show a message or an error to user.
	 * 
	 * 
	 */
	public static void showAlarm(final Event record) {
		if(record == null) return;
		final IStatus s = record.getStatus();
		if(s == null) return;
		final Display display = plugin.getWorkbench().getDisplay();
		if (display.isDisposed()) {
			return;
		}
		display.asyncExec(new Runnable() {

			public void run() {

				Shell shell = display.getActiveShell();
				if (shell == null) {
					shell = new Shell(display);
				}
				IEventQueue eventQueue = plugin.getEventQueue();
				eventQueue.addEvent(record);
				eventQueue.sortEvents(Event.createDateComparator(Event.ASCENDING));
				plugin.openEventDialog(shell);
			}

		});
	}
	
	private static Event createEvent(String id, Date date, IStatus s)
	{
		Throwable t = s.getException();
		String details = null;
		List<Problem> problems = ProblemCenter.identifyProblems(s);
		if (t != null) {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(os);
			t.printStackTrace(ps);
			details = os.toString();
		}
		
		return new Event(id, date, problems.toArray(new Problem[problems
		                                                      .size()]), s, details);
	}

}
