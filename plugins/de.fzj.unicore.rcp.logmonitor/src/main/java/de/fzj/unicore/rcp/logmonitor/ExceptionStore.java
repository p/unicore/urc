package de.fzj.unicore.rcp.logmonitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class for storing exceptions that have occurred during runtime of the client.
 * This is a best-effort service and the class does not guarantee that the
 * exception will really be stored. In particular, this means that the store may
 * be full and old stored exceptions might be lost. Right now, the store only
 * keeps exceptions in memory, but this might change in the future, so
 * exceptions might be stored on disk (directly or within some kind of
 * database).
 * 
 * @author bdemuth
 * 
 */
public class ExceptionStore {

	private static final int maxSize = 1000;

	private Map<String, Throwable> map = new HashMap<String, Throwable>();
	private List<String> list = new ArrayList<String>();

	public boolean containsException(String key) {
		return map.containsKey(key);
	}

	public Throwable getException(String key) {
		return map.get(key);
	}

	public void storeException(String key, Throwable t) {
		map.put(key, t);
		list.add(key);
		if (list.size() > maxSize) {
			list.remove(0); // remove old exceptions
		}
	}

}
