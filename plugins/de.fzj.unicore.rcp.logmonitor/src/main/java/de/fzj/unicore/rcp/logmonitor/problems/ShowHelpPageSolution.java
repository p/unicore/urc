/******************************************************************************
 * Copyright (c) 2007, 2008 g-Eclipse consortium 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for
 * project g-Eclipse founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributor(s):
 *    Mathias Stuempert
 *****************************************************************************/

package de.fzj.unicore.rcp.logmonitor.problems;

import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.help.IWorkbenchHelpSystem;

import eu.unicore.problemutil.Problem;

/**
 * Base class for solutions that open the preferences window at the requested
 * properties page.
 */
public abstract class ShowHelpPageSolution extends GraphicalSolution {

	public ShowHelpPageSolution() {
		super();
	}

	public ShowHelpPageSolution(String id, String applicability,
			String description) {
		super(id, applicability, description);
	}

	public ShowHelpPageSolution(String id, String[] applicability,
			String description) {
		super(id, applicability, description);
	}

	public ShowHelpPageSolution(String id, String[] applicability,
			String description, String[] tags) {
		super(id, applicability, description, tags);
	}

	protected abstract String getHelpPageRef();

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.geclipse.core.reporting.ISolver#solve()
	 */
	@Override
	public boolean solve(String message, Problem[] problems, String details) {

		try {
			IWorkbenchHelpSystem helpSystem = PlatformUI.getWorkbench()
					.getHelpSystem();
			helpSystem.displayHelpResource(getHelpPageRef());
		} catch (Exception exc) {
			// Ignore, no workbench available or no UI thread
		}
		return false;
	}

}
