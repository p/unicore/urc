package de.fzj.unicore.rcp.logmonitor.problems;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;

import de.fzj.unicore.rcp.logmonitor.utils.HashingUtils;

import eu.unicore.problemutil.Matcher;
import eu.unicore.problemutil.Problem;
import eu.unicore.problemutil.Reason;
import eu.unicore.problemutil.Solution;
import eu.unicore.problemutil.config.ProblemConfig;
import eu.unicore.problemutil.expressions.Conjunction;
import eu.unicore.problemutil.expressions.Disjunction;
import eu.unicore.problemutil.expressions.ExceptionCondition;
import eu.unicore.problemutil.expressions.Expression;
import eu.unicore.problemutil.expressions.MessageCondition;
import eu.unicore.problemutil.expressions.Negation;
import eu.unicore.problemutil.simple.SimpleMatcher;
import eu.unicore.problemutil.simple.SimpleReason;
import eu.unicore.problemutil.simple.SimpleSolution;

public class ProblemCenter {

	public static final String EXTENSION_POINT_ID_PROBLEMS = ProblemCenter.class
	.getPackage().getName();

	private static final String NOT = "not", AND = "and", OR = "or",
	EXCEPTION_EXPRESSION = "exceptionMatches",
	MESSAGE_EXPRESSION = "messageMatches";

	private static ProblemConfig problemConfig;

	private static void addMatcher(IConfigurationElement element) {
		for (IConfigurationElement child : element.getChildren("simpleMatcher")) {
			try {
				Expression expression = extractExpression(child.getChildren()[0]);
				String reasonId = child.getAttribute("reasonId");
				SimpleMatcher matcher = new SimpleMatcher(reasonId, expression);
				problemConfig.registerMatcher(matcher);
			} catch (Exception e) {
				// do nothing and continue to next element
			}

		}
		for (IConfigurationElement child : element
				.getChildren("complexMatcher")) {
			try {
				Matcher m = (Matcher) child.createExecutableExtension("class");
				problemConfig.registerMatcher(m);
			} catch (Exception e) {
				e.printStackTrace();
				// do nothing and continue to next element
			}

		}
	}

	private static void addReason(IConfigurationElement element) {
		for (IConfigurationElement child : element.getChildren("simpleReason")) {
			String id = child.getAttribute("id");
			String name = child.getAttribute("name");
			String description = child.getAttribute("description");
			problemConfig
			.registerReason(new SimpleReason(id, name, description));
		}
		for (IConfigurationElement child : element.getChildren("complexReason")) {
			try {
				Reason r = (Reason) child.createExecutableExtension("class");
				problemConfig.registerReason(r);
			} catch (CoreException e) {
				// do nothing and continue to next element
			}

		}
	}

	private static void addSolution(IConfigurationElement element) {
		for (IConfigurationElement child : element
				.getChildren("simpleSolution")) {

			IConfigurationElement[] applicableFor = child
			.getChildren("applicableFor");
			if (applicableFor.length > 0) {
				String id = child.getAttribute("id");
				String description = child.getAttribute("description");
				String[] applicableForStrings = new String[applicableFor.length];
				for (int i = 0; i < applicableFor.length; i++) {
					String s = applicableFor[i].getValue();
					applicableForStrings[i] = s;
				}
				problemConfig.registerSolution(new SimpleSolution(id,
						applicableForStrings, description));

			} else {
				continue;
			}

		}
		for (IConfigurationElement child : element
				.getChildren("complexSolution")) {
			try {
				Solution s = (Solution) child
				.createExecutableExtension("class");
				problemConfig.registerSolution(s);
			} catch (CoreException e) {
				// do nothing and continue to next element
				e.printStackTrace();
			}

		}
	}

	private static Expression extractExpression(IConfigurationElement element) {
		if (NOT.equals(element.getName())) {
			IConfigurationElement child = element.getChildren()[0];
			return new Negation(extractExpression(child));
		} else if (AND.equals(element.getName())) {
			IConfigurationElement[] children = element.getChildren();
			Expression[] childExpressions = new Expression[children.length];
			for (int i = 0; i < childExpressions.length; i++) {
				childExpressions[i] = extractExpression(children[i]);
			}
			return new Conjunction(childExpressions);
		} else if (OR.equals(element.getName())) {
			IConfigurationElement[] children = element.getChildren();
			Expression[] childExpressions = new Expression[children.length];
			for (int i = 0; i < childExpressions.length; i++) {
				childExpressions[i] = extractExpression(children[i]);
			}
			return new Disjunction(childExpressions);
		} else if (EXCEPTION_EXPRESSION.equals(element.getName())) {
			IConfigurationElement exception = element;
			String clazz = exception.getAttribute("exceptionClass");
			String messagePattern = exception.getAttribute("messagePattern");
			String position = exception.getAttribute("position");
			ExceptionCondition condition = new ExceptionCondition(clazz);
			if (messagePattern != null) {
				condition.setMessagePattern(Pattern.compile(messagePattern));
			}
			if (position != null) {
				condition.setPositionString(position);
			}
			IConfigurationElement[] children = exception
			.getChildren(EXCEPTION_EXPRESSION);
			if (children.length == 1) {
				ExceptionCondition child = (ExceptionCondition) extractExpression(children[0]);
				condition.setCausedBy(child);
			}
			return condition;
		} else if (MESSAGE_EXPRESSION.equals(element.getName())) {
			String messagePattern = element.getAttribute("messagePattern");
			MessageCondition condition = new MessageCondition(messagePattern);
			return condition;
		}
		return null;
	}

	/**
	 * In case no "real" reason was found for a particular problem, this method
	 * can be used to create a "fake" reason which can be dealt with similar
	 * to a normal reason.
	 * @param status
	 * @return
	 */
	public static Reason createUnknownReason(IStatus status)
	{
		String s =  "";

		if(status.getException() == null) 
		{
			// we don't have an exception so our only chance is to identify this
			// problem using the message
			if(status.getMessage() != null)
			{
				s = status.getMessage();
			}
			else s = UUID.randomUUID().toString();
		}
		else
		{
			// use the exception class name and it's stack trace to identify
			// this specific problem
			// don't use the message as it can change easily
			s += status.getException().getClass().getCanonicalName();
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			PrintStream ps = new PrintStream(os);
			status.getException().printStackTrace(ps);
			s += os.toString();
			
		}
		s = HashingUtils.toMD5(s);
		return new SimpleReason(s, "Unknown");
	}

	public static ProblemConfig getProblemConfig() {
		if (problemConfig == null) {
			problemConfig = new ProblemConfig();
			iterateOverExtensions();
		}
		return problemConfig;
	}

	public static List<Problem> identifyProblems(IStatus status) {
		try
		{
			List<Problem> problems = ProblemCenter.getProblemConfig()
			.matchProblems(status.getMessage(), status.getException());
			return problems;
		} catch (Exception e)
		{
			e.printStackTrace();
			// sth. went wrong while identifying the problem..
			Problem p = new Problem(createUnknownReason(status), status.getException(), new HashMap<String, Object>());
			List<Problem> problems = new ArrayList<Problem>(1);
			problems.add(p);
			return problems;
		}
		
	}

	private static void iterateOverExtensions() {
		/*
		 * go over all registered extensions and collect problems, matchers and
		 * solutions
		 */
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
		.getExtensionPoint(EXTENSION_POINT_ID_PROBLEMS);
		IConfigurationElement[] members = extensionPoint
		.getConfigurationElements();
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			if ("reason".equals(member.getName())) {
				addReason(member);
			} else if ("matcher".equals(member.getName())) {
				addMatcher(member);
			}
		}

		// add solutions in a second loop cause this requires the associated
		// reason to be defined first
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			if ("solution".equals(member.getName())) {
				addSolution(member);
			}
		}

	}
}
