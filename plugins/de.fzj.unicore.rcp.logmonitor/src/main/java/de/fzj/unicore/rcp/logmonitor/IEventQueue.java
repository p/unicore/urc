package de.fzj.unicore.rcp.logmonitor;

import java.util.Comparator;

public interface IEventQueue {

	public void addEvent(Event record);

	public void addEventListener(IEventQueueListener l);

	public void clear();

	public Event getCurrentEvent();

	public boolean hasNextEvent();

	public boolean hasPreviousEvent();

	public Event removeCurrentEvent();

	public void removeEventListener(IEventQueueListener l);

	public void toNextEvent();

	public void toPreviousEvent();
	
	public boolean containsEvent(Event e);

	public void jumpToEvent(Event e);
	
	public void sortEvents(Comparator<Event> cmp);
}
