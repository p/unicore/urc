package de.fzj.unicore.rcp.logmonitor.problems;

import java.util.Arrays;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import eu.unicore.problemutil.Problem;
import eu.unicore.problemutil.simple.SimpleSolution;

public abstract class GraphicalSolution extends SimpleSolution {

	public GraphicalSolution() {
		super(null, new String[0], null);
	}

	public GraphicalSolution(String id, String applicability, String description) {
		super(id, applicability, description);
	}

	public GraphicalSolution(String id, String[] applicability,
			String description) {
		super(id, applicability, description);
	}

	public GraphicalSolution(String id, String[] applicability,
			String description, String[] tags) {
		super(id, applicability, description, tags);
	}

	/**
	 * This method gives the solution the possibility to look at the message,
	 * problems and details before deciding whether it is available or not.
	 * Note, that the {@link Throwable} inside the given problems might be null,
	 * if the problems are deserialized log entries! In this case, the solution
	 * might at least have a look at the message containing the error
	 * description and the details String which usually contains the
	 * stack-trace.
	 * 
	 * @param cause
	 * @return
	 */
	public boolean isAvailableFor(String message, Problem[] problems,
			String details) {
		return true;
	}

	public void setApplicableFor(String[] applicability) {
		this.applicability = Arrays.asList(applicability);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTags(String[] tags) {
		this.tags = Arrays.asList(tags);
	}

	/**
	 * Try to solve the given problem(s).
	 * 
	 * @param message
	 * @param problems
	 * @param details
	 * @return true iff the solution can be executed again.
	 * @throws Exception
	 */
	public abstract boolean solve(String message, Problem[] problems,
			String details) throws Exception;

	
	protected Shell getParentShell()
	{
		Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();
		if (shell == null) {
			shell = new Shell();
		}
		return shell;
	}
}
