/*******************************************************************************
 * Copyright (c) 2000, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.logmonitor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;

import de.fzj.unicore.rcp.logmonitor.problems.ProblemCenter;
import eu.unicore.problemutil.Problem;

public class LogEntry extends PlatformObject implements IWorkbenchAdapter {

	public static final String F_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS"; //$NON-NLS-1$

	private static final String[] EMPTY = new String[0];
	private List<LogEntry> children;
	private LogEntry parent;
	private String pluginId;
	private int severity;
	private int code;
	private Date fDate;
	private String message;
	private String stack;
	private String[] reasonIDs;
	private Throwable cause;

	private LogSession session;

	public LogEntry() {

	}

	public LogEntry(IStatus status) {
		processStatus(status);
	}

	void addChild(LogEntry child) {
		if (children == null) {
			children = new ArrayList<LogEntry>();
		}
		children.add(child);
		child.setParent(this);
	}

	public Throwable getCause() {
		return cause;
	}

	/**
	 * @see IWorkbenchAdapter#getChildren(Object)
	 */
	public Object[] getChildren(Object parent) {
		if (children == null) {
			return new Object[0];
		}
		return children.toArray();
	}

	public int getCode() {
		return code;
	}

	public Date getDate() {
		if (fDate == null) {
			fDate = new Date(0); // unknown date - return epoch
		}
		return fDate;
	}

	/**
	 * @see IWorkbenchAdapter#getImageDescriptor(Object)
	 */
	public ImageDescriptor getImageDescriptor(Object arg0) {
		return null;
	}

	/**
	 * Generate a String for identifying this log entry. The String contains the
	 * plugin id and a timestamp with milliseconds, so it should be rather
	 * unique (no guarantees, though).
	 * 
	 * @return
	 */
	public String getKey() {
		DateFormat formatter = new SimpleDateFormat(F_DATE_FORMAT);
		return getPluginId() + " " + formatter.format(getDate());
	}

	/**
	 * @see IWorkbenchAdapter#getLabel(Object)
	 */
	public String getLabel(Object obj) {
		return getSeverityText();
	}

	public String getMessage() {
		return message;
	}

	/**
	 * @see IWorkbenchAdapter#getParent(Object)
	 */
	public Object getParent(Object obj) {
		return parent;
	}

	public String getPluginId() {
		return pluginId;
	}

	public String[] getReasonIDs() {
		if (reasonIDs == null) {
			return EMPTY;
		}
		return reasonIDs;
	}

	public LogSession getSession() {
		return session;
	}

	public int getSeverity() {
		return severity;
	}

	public String getSeverityText() {
		return getSeverityText(severity);
	}

	private String getSeverityText(int severity) {
		switch (severity) {
		case IStatus.ERROR:
			return "Error";
		case IStatus.WARNING:
			return "Warning";
		case IStatus.INFO:
			return "Info";
		case IStatus.OK:
			return "Ok";
		}
		return "?"; //$NON-NLS-1$
	}

	public String getStack() {
		return stack;
	}

	public boolean hasChildren() {
		return children != null && children.size() > 0;
	}

	public boolean isOK() {
		return severity == IStatus.OK;
	}

	private int parseInteger(String token) {
		try {
			return Integer.parseInt(token);
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	void processEntry(String line) {
		// !ENTRY <pluginID> <severity> <code> <date>
		// !ENTRY <pluginID> <date> if logged by the framework!!!
		StringTokenizer stok = new StringTokenizer(line, " "); //$NON-NLS-1$
		int tokenCount = stok.countTokens();
		boolean noSeverity = stok.countTokens() < 5;

		// no severity means it should be represented as OK
		if (noSeverity) {
			severity = 0;
			code = 0;
		}
		StringBuffer dateBuffer = new StringBuffer();
		for (int i = 0; i < tokenCount; i++) {
			String token = stok.nextToken();
			switch (i) {
			case 0:
				break;
			case 1:
				pluginId = token;
				break;
			case 2:
				if (noSeverity) {
					if (dateBuffer.length() > 0) {
						dateBuffer.append(" "); //$NON-NLS-1$
					}
					dateBuffer.append(token);
				} else {
					severity = parseInteger(token);
				}
				break;
			case 3:
				if (noSeverity) {
					if (dateBuffer.length() > 0) {
						dateBuffer.append(" "); //$NON-NLS-1$
					}
					dateBuffer.append(token);
				} else {
					code = parseInteger(token);
				}
				break;
			default:
				if (dateBuffer.length() > 0) {
					dateBuffer.append(" "); //$NON-NLS-1$
				}
				dateBuffer.append(token);
			}
		}
		DateFormat formatter = new SimpleDateFormat(F_DATE_FORMAT);
		try {
			Date date = formatter.parse(dateBuffer.toString());
			if (date != null) {
				fDate = date;
			}
		} catch (ParseException e) {
		}
	}

	private void processStatus(IStatus status) {
		pluginId = status.getPlugin();
		severity = status.getSeverity();
		code = status.getCode();

		fDate = new Date();
		setMessage(status.getMessage());
		cause = status.getException();

		if (cause != null) {
			StringWriter swriter = new StringWriter();
			PrintWriter pwriter = new PrintWriter(swriter);
			cause.printStackTrace(pwriter);
			pwriter.flush();
			pwriter.close();
			stack = swriter.toString();
		}
		
		List<Problem> problems = ProblemCenter.identifyProblems(status);
		List<String> reasonIds = new ArrayList<String>(problems.size());
		for (Problem problem : problems) {
			if (problem.getReason() != null) {
				reasonIds.add(problem.getReason().getId());
			}
		}
		setReasonIDs(reasonIds.toArray(new String[reasonIds.size()]));
		
		IStatus[] schildren = status.getChildren();
		if (schildren.length > 0) {
			children = new ArrayList<LogEntry>();
			for (int i = 0; i < schildren.length; i++) {
				LogEntry child = new LogEntry(schildren[i]);
				addChild(child);
			}
		}
	}

	int processSubEntry(String line) {
		// !SUBENTRY <depth> <pluginID> <severity> <code> <date>
		// !SUBENTRY <depth> <pluginID> <date>if logged by the framework!!!
		StringTokenizer stok = new StringTokenizer(line, " "); //$NON-NLS-1$
		int tokenCount = stok.countTokens();
		boolean byFrameWork = stok.countTokens() < 5;

		StringBuffer dateBuffer = new StringBuffer();
		int depth = 0;
		for (int i = 0; i < tokenCount; i++) {
			String token = stok.nextToken();
			switch (i) {
			case 0:
				break;
			case 1:
				depth = parseInteger(token);
				break;
			case 2:
				pluginId = token;
				break;
			case 3:
				if (byFrameWork) {
					if (dateBuffer.length() > 0) {
						dateBuffer.append(" "); //$NON-NLS-1$
					}
					dateBuffer.append(token);
				} else {
					severity = parseInteger(token);
				}
				break;
			case 4:
				if (byFrameWork) {
					if (dateBuffer.length() > 0) {
						dateBuffer.append(" "); //$NON-NLS-1$
					}
					dateBuffer.append(token);
				} else {
					code = parseInteger(token);
				}
				break;
			default:
				if (dateBuffer.length() > 0) {
					dateBuffer.append(" "); //$NON-NLS-1$
				}
				dateBuffer.append(token);
			}
		}
		DateFormat formatter = new SimpleDateFormat(F_DATE_FORMAT);
		try {
			Date date = formatter.parse(dateBuffer.toString());
			if (date != null) {
				fDate = date;
			}
		} catch (ParseException e) {
		}
		return depth;
	}

	public void setCause(Throwable cause) {
		this.cause = cause;
	}

	void setMessage(String message) {
		this.message = message;
	}

	void setParent(LogEntry parent) {
		this.parent = parent;
	}

	public void setReasonIDs(String[] reasonIDs) {
		this.reasonIDs = reasonIDs;
	}

	void setSession(LogSession session) {
		this.session = session;
	}

	void setStack(String stack) {
		this.stack = stack;
	}

	@Override
	public String toString() {
		return getSeverityText();
	}

	public void write(PrintWriter writer) {
		if (session != null) {
			writer.println(session.getSessionData());
		}
		writer.println(getSeverityText());
		if (fDate != null) {
			writer.println(getDate());
		}

		if (message != null) {
			writer.println(getMessage());
		}

		if (stack != null) {
			writer.println();
			writer.println(stack);
		}
		if (reasonIDs != null && reasonIDs.length > 0) {
			writer.println();
			writer.println(reasonIDs.toString());
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + code;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result
				+ ((pluginId == null) ? 0 : pluginId.hashCode());
		result = prime * result + severity;
		result = prime * result + ((stack == null) ? 0 : stack.replaceAll("\\s", "").hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof LogEntry))
			return false;
		LogEntry other = (LogEntry) obj;
		if (code != other.code)
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!removeSpaces(message).equals(removeSpaces(other.message)))
			return false;
		if (pluginId == null) {
			if (other.pluginId != null)
				return false;
		} else if (!pluginId.equals(other.pluginId))
			return false;
		if (severity != other.severity)
			return false;
		if (stack == null) {
			if (other.stack != null)
				return false;
		} else 
		{	
			if(other.stack == null) return false;
			if (!removeSpaces(stack).equals(removeSpaces(other.stack)))
			return false;
		}
		return true;
	}

	
	/**
	 * When comparing log entry messages, we remove all spaces, cause these can
	 * be altered during the serialization process.
	 * @param s
	 * @return
	 */
    private static String removeSpaces(String s)
    {
    	return s == null ? "" : s.replaceAll("\\s", "");
    }
	
}
