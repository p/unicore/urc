package de.fzj.unicore.rcp.logmonitor.utils;

public interface LogListener {

	
	public void notifyLogged(int level, String message, Throwable t);
}
