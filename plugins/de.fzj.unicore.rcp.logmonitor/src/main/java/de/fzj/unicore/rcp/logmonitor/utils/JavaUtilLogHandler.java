package de.fzj.unicore.rcp.logmonitor.utils;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

import org.eclipse.core.runtime.IStatus;


/**
 * This class can be used to be notified of java util logging events
 * provided by this plugin.
 * @author bdemuth
 *
 */
public class JavaUtilLogHandler extends Handler {
	
	LogListener listener;

	public JavaUtilLogHandler(LogListener listener) {
		super();
		this.listener = listener;
	}



	@Override
	public void publish(LogRecord event) {
		String text = event.getMessage();
		Throwable thrown = null;

		thrown = event.getThrown();


		java.util.logging.Level level = event.getLevel();
		int severity = IStatus.OK;

		if (level.intValue() >= java.util.logging.Level.SEVERE.intValue()) {
			severity = IStatus.ERROR;
		} else if (level.intValue() >= java.util.logging.Level.WARNING.intValue()) {
			severity = IStatus.WARNING;
		} else if (level.intValue() >= java.util.logging.Level.INFO.intValue()) {
			severity = IStatus.INFO;
		}
		listener.notifyLogged(severity, text, thrown);
		
	}

	@Override
	public void flush() {
		
	}

	@Override
	public void close() throws SecurityException {
		
	}
}
