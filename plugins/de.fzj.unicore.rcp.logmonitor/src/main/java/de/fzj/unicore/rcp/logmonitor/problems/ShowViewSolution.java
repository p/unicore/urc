/******************************************************************************
 * Copyright (c) 2007, 2008 g-Eclipse consortium 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for
 * project g-Eclipse founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributor(s):
 *    Mathias Stuempert
 *****************************************************************************/

package de.fzj.unicore.rcp.logmonitor.problems;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.logmonitor.LogActivator;
import eu.unicore.problemutil.Problem;

/**
 * Resolve a problem by instructing the user and opening the view with the
 * requested viewID.
 */
public abstract class ShowViewSolution extends GraphicalSolution {

	private IViewPart view;

	public ShowViewSolution() {
		super();
	}

	public ShowViewSolution(String id, String applicability, String description) {
		super(id, applicability, description);

	}

	public ShowViewSolution(String id, String[] applicability,
			String description) {
		super(id, applicability, description);
	}

	public ShowViewSolution(String id, String[] applicability,
			String description, String[] tags) {
		super(id, applicability, description, tags);
	}

	public IViewPart getView() {
		return view;
	}

	protected abstract String getViewId();

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.geclipse.core.reporting.ISolver#solve()
	 */
	@Override
	public boolean solve(String message, Problem[] problems, String details) {
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		IWorkbenchPage page = window.getActivePage();
		try {
			view = page.showView(getViewId());
		} catch (PartInitException piExc) {
			LogActivator.log(IStatus.ERROR, "Unable to open requested view",
					piExc);
		}
		return true;
	}

}
