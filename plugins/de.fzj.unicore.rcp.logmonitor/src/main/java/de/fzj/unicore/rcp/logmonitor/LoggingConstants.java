package de.fzj.unicore.rcp.logmonitor;

public interface LoggingConstants {

	public static final String P_LOG_WARNING = "warning"; //$NON-NLS-1$
	public static final String P_LOG_ERROR = "error"; //$NON-NLS-1$
	public static final String P_LOG_INFO = "info"; //$NON-NLS-1$
	public static final String P_POPUP_WARNING = "popupWarning"; //$NON-NLS-1$
	public static final String P_POPUP_ERROR = "popupError"; //$NON-NLS-1$
	public static final String P_POPUP_INFO = "popupInfo"; //$NON-NLS-1$
	public static final String P_POPUP_FINE_GRAINED_PREFIX = "popupPrefix "; //$NON-NLS-1$
	public static final String P_SHOW_WARNING = "showWarning"; //$NON-NLS-1$
	public static final String P_SHOW_ERROR = "showError"; //$NON-NLS-1$
	public static final String P_SHOW_INFO = "showInfo"; //$NON-NLS-1$
	public static final String P_SHOW_LOG_LIMIT = "limit"; //$NON-NLS-1$
	public static final String P_USE_LIMIT = "useLimit"; //$NON-NLS-1$
	public static final String P_SHOW_ALL_SESSIONS = "allSessions"; //$NON-NLS-1$
	public static final String P_ACTIVATE = "activate"; //$NON-NLS-1$
	public static final String P_ORDER_TYPE = "orderType"; //$NON-NLS-1$
	public static final String P_ORDER_VALUE = "orderValue"; //$NON-NLS-1$

	/**
	 * Tag for writing problem reason ids to the log file
	 */
	public static final String REASON_ID_TAG = "!REASONS";//$NON-NLS-1$

	/**
	 * Reason identifier for problems that were probably caused by a bug.
	 */
	public static final String REASON_ID_BUG = "BUG";//$NON-NLS-1$

}
