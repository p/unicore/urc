package de.fzj.unicore.rcp.logmonitor.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashingUtils {
	
	public static String toMD5(String s)
	{
		MessageDigest md5;
		try {
			md5 = MessageDigest.getInstance("MD5");
			 md5.reset();
		        md5.update(s.getBytes());
		        byte[] result = md5.digest();

		        /* Ausgabe */
		        StringBuffer hexString = new StringBuffer();
		        for (int i=0; i<result.length; i++) {
		            hexString.append(Integer.toHexString(0xFF & result[i]));
		        }
		        return hexString.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
       
	}

}
