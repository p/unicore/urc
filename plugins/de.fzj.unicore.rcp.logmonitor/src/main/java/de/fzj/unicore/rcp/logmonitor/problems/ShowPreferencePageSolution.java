/******************************************************************************
 * Copyright (c) 2007, 2008 g-Eclipse consortium 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for
 * project g-Eclipse founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributor(s):
 *    Mathias Stuempert
 *****************************************************************************/

package de.fzj.unicore.rcp.logmonitor.problems;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.PreferencesUtil;

import eu.unicore.problemutil.Problem;

/**
 * Base class for solutions that open the preferences window at the requested
 * properties page.
 */
public abstract class ShowPreferencePageSolution extends GraphicalSolution {

	public ShowPreferencePageSolution() {
		super();
	}

	public ShowPreferencePageSolution(String id, String applicability,
			String description) {
		super(id, applicability, description);
	}

	public ShowPreferencePageSolution(String id, String[] applicability,
			String description) {
		super(id, applicability, description);
	}

	public ShowPreferencePageSolution(String id, String[] applicability,
			String description, String[] tags) {
		super(id, applicability, description, tags);
	}

	protected abstract String getPreferencePageId();

	private Shell getShell() {

		Shell result = null;

		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();

		if (window != null) {
			result = window.getShell();
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.geclipse.core.reporting.ISolver#solve()
	 */
	@Override
	public boolean solve(String message, Problem[] problems, String details) {

		PreferenceDialog dialog = PreferencesUtil.createPreferenceDialogOn(
				getShell(), getPreferencePageId(), null, null);

		Shell dialogShell = dialog.getShell();
		Shell currentShell = dialogShell.getDisplay().getActiveShell();
		Shell parentShell = currentShell;

		// Create the chain of shells to be closed for making the preference
		// dialog the top level component
		List<Shell> toClose = new ArrayList<Shell>();
		while ((parentShell != null) && (parentShell != dialogShell)) {
			toClose.add(parentShell);
			Composite parent = parentShell.getParent();
			if (parent instanceof Shell) {
				parentShell = (Shell) parent;
			} else {
				parentShell = null;
			}
		}

		// If the preference dialog is part of the chain close
		// all shells "above" the dialog
		if (parentShell == dialogShell) {
			for (Shell shell : toClose) {
				shell.close();
			}
		}

		dialogShell.forceActive();
		dialog.open();
		return true;
	}

}
