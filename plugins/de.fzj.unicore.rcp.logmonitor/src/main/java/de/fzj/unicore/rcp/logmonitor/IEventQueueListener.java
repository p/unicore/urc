package de.fzj.unicore.rcp.logmonitor;

public interface IEventQueueListener {

	public void eventAdded(Event record);

	public void eventRemoved(Event record);
	
	public void currentEventChanged(Event selected);
}
