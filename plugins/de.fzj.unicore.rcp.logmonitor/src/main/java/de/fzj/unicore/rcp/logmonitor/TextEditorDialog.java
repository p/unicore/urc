package de.fzj.unicore.rcp.logmonitor;

/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class TextEditorDialog extends Dialog {

	private Text textArea = null;

	protected Button okButton = null;

	private boolean hasChanged = false;

	private boolean isClosing = false;

	private String title = "";
	private String text = "";
	private boolean editable = false;

	private Shell parent;
	private Shell sShell;

	public TextEditorDialog(Shell parent, String title, String text) {
		this(parent, title, text, false);
	}

	public TextEditorDialog(Shell parent, String title, String text,
			boolean editable) {
		super(parent);
		this.parent = parent;

		this.title = title;
		this.text = text;
		this.editable = editable;

	}

	protected void createButtons(Composite parent) {
		GridLayout layout = new GridLayout();
		layout.numColumns = 4;
		parent.setLayout(layout);
		GridData okButtonLayoutData = new GridData();
		okButton = new Button(parent, SWT.NONE);
		okButton.setText("     OK     ");
		okButton.setLayoutData(okButtonLayoutData);
		okButtonLayoutData.horizontalAlignment = GridData.CENTER;
		okButtonLayoutData.verticalAlignment = GridData.CENTER;
		okButton.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				doExit();
			}
		});
	}

	protected void createControls(final Shell shell) {
		GridLayout shellLayout = new GridLayout();
		GridData textLayoutData = new GridData();
		textArea = new Text(shell, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL
				| SWT.BORDER);
		textArea.setText(text);
		textArea.setEditable(editable);
		shell.setText(title);
		shellLayout.numColumns = 1;
		shellLayout.makeColumnsEqualWidth = false;
		shell.setLayout(shellLayout);
		textLayoutData.horizontalAlignment = GridData.FILL;
		textLayoutData.verticalAlignment = GridData.FILL;
		textLayoutData.grabExcessHorizontalSpace = true;
		textLayoutData.grabExcessVerticalSpace = true;
		textArea.setLayoutData(textLayoutData);
		shell.setSize(getPreferredSize());
		textArea.addModifyListener(new org.eclipse.swt.events.ModifyListener() {
			public void modifyText(org.eclipse.swt.events.ModifyEvent e) {
				if (!hasChanged) {
					shell.setText(title + " *");
					hasChanged = true;
				}
			}
		});
		shell.addShellListener(new org.eclipse.swt.events.ShellAdapter() {
			@Override
			public void shellClosed(org.eclipse.swt.events.ShellEvent e) {
				if (!isClosing) {
					e.doit = doExit();
				}
			}
		});
		Composite buttonGroup = new Composite(shell, SWT.NONE);
		GridData buttonGroupData = new GridData();
		buttonGroup.setLayoutData(buttonGroupData);
		createButtons(buttonGroup);
		shell.open();
		if (okButton != null) {
			okButton.setFocus();
		}
		Display display = shell.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	protected boolean doExit() {
		isClosing = true;
		text = textArea.getText();
		sShell.close();
		sShell.dispose();
		return true;
	}

	protected Point getPreferredSize() {
		return new Point(393, 279);
	}

	@Override
	public String getText() {
		return text;
	}

	public String open() {
		sShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.RESIZE | SWT.MAX
				| SWT.APPLICATION_MODAL);
		createControls(sShell);
		return getText();
	}

	@Override
	public void setText(String string) {
		text = string;
		if (textArea != null) {
			textArea.setText(string);
		}
	}
}
