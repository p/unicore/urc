/*****************************************************************************
 * Copyright (c) 2007-2008 g-Eclipse Consortium
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for the
 * g-Eclipse project founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributors:
 *    Mathias Stuempert - initial API and implementation
 *    Bastian Demuth - integration into the UNICORE Rich Client
 *****************************************************************************/

package de.fzj.unicore.rcp.logmonitor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.ErrorSupportProvider;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IconAndMessageDialog;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.Policy;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.logmonitor.problems.GraphicalSolution;
import de.fzj.unicore.rcp.logmonitor.problems.ProblemCenter;
import eu.unicore.problemutil.Problem;
import eu.unicore.problemutil.Reason;
import eu.unicore.problemutil.Solution;

/**
 * The EventDialog is a user-friendly way of reporting events and problems to
 * the user. It displays a description of the event as well as registered
 * reasons and solutions in case of problem events. The active solutions are
 * clickable and perform some action, like generating a problem report.
 */
public class EventDialog extends IconAndMessageDialog implements IEventQueueListener {

	/**
	 * Static to prevent opening of error dialogs for automated testing.
	 */
	public static boolean AUTOMATED_MODE = false;

	/**
	 * Return code determining that a solution was chosen.
	 */
	public static final int SOLVE = 2;

	/**
	 * The current clipboard. To be disposed when closing the dialog.
	 */
	private Clipboard clipboard;

	private IEventQueue eventQueue;

	private Button previousButton, nextButton;

	/**
	 * Filter mask for determining which status items to display.
	 */
	private int displayMask = 0xFFFF;

	/**
	 * The SWT control that displays the error details.
	 */
	private Text detailsArea;

	/**
	 * Indicates whether the error details viewer is currently created.
	 */
	private boolean detailsAreaCreated = false;

	public EventDialog(final Shell parentShell, IEventQueue eventQueue) {
		super(parentShell);
		this.displayMask = IStatus.OK | IStatus.INFO | IStatus.WARNING
		| IStatus.ERROR;
		this.eventQueue = eventQueue;
		eventQueue.addEventListener(this);
		setShellStyle(SWT.SHELL_TRIM | SWT.MODELESS);
	}

	/*
	 * (non-Javadoc) Method declared on Dialog. Handles the pressing of the Ok
	 * or Details button in this dialog. If the Ok button was pressed then close
	 * this dialog. If the Details button was pressed then toggle the displaying
	 * of the error details area. Note that the Details button will only be
	 * visible if the error being displayed specifies child details.
	 */
	@Override
	protected void buttonPressed(int id) {
		if (id == IDialogConstants.DETAILS_ID) {
			// was the details button pressed?
			toggleDetailsArea();
		} else if (id == IDialogConstants.BACK_ID) {
			eventQueue.toPreviousEvent();
		} else if (id == IDialogConstants.NEXT_ID) {
			eventQueue.toNextEvent();
		} else {
			super.buttonPressed(id);
		}
	}

	protected void checkButtonsEnabled() {
		if (previousButton != null && !previousButton.isDisposed()) {
			previousButton.setEnabled(eventQueue.hasPreviousEvent());
		}
		if (nextButton != null && !nextButton.isDisposed()) {
			nextButton.setEnabled(eventQueue.hasNextEvent());
		}
	}


	/*
	 * (non-Javadoc) Method declared in Window.
	 */
	@Override
	protected void configureShell(final Shell shell) {
		super.configureShell(shell);
		shell.setText(getTitle());
		shell.addDisposeListener(new DisposeListener() {
			
			public void widgetDisposed(DisposeEvent e) {
				// this was done in close before, but
				// didn't seem to work sometimes
				if(eventQueue != null) eventQueue.removeEventListener(EventDialog.this);
				if (clipboard != null) {
					clipboard.dispose();
				}
				shell.removeDisposeListener(this);
			}
		});
	}

	private void copyToClipboard() {
		if (clipboard != null) {
			clipboard.dispose();
		}
		String details = detailsArea.getSelectionText();
		if (details == null || details.trim().length() == 0) {
			return;
		}
		StringBuffer statusBuffer = new StringBuffer();
		statusBuffer.append(details);
		clipboard = new Clipboard(detailsArea.getDisplay());
		clipboard.setContents(new Object[] { statusBuffer.toString() },
				new Transfer[] { TextTransfer.getInstance() });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse
	 * .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		previousButton = createButton(parent, IDialogConstants.BACK_ID,
				IDialogConstants.BACK_LABEL, false);
		// create OK and Details buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				false);
		createDetailsButton(parent);
		nextButton = createButton(parent, IDialogConstants.NEXT_ID,
				IDialogConstants.NEXT_LABEL, false);
		checkButtonsEnabled();
	}

	protected Text createDetailsArea(Composite parent) {
		// create the list
		detailsArea = new Text(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.MULTI | SWT.READ_ONLY);
		String details = getDetails();
		if(details != null) detailsArea.setText(details);
		else
		{
			System.out.println("Event dialog encountered a problem: details null");
		}
		GridData data = new GridData(GridData.HORIZONTAL_ALIGN_FILL
				| GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL
				| GridData.GRAB_VERTICAL);
		// data.heightHint = detailsArea.get;
		data.horizontalSpan = 2;
		data.heightHint = parent.getShell().getSize().y/2;
		data.minimumHeight = 300;
		detailsArea.setLayoutData(data);
		detailsArea.setFont(parent.getFont());
		Menu copyMenu = new Menu(detailsArea);
		MenuItem copyItem = new MenuItem(copyMenu, SWT.NONE);
		copyItem.addSelectionListener(new SelectionListener() {
			/*
			 * @see SelectionListener.widgetDefaultSelected(SelectionEvent)
			 */
			public void widgetDefaultSelected(SelectionEvent e) {
				copyToClipboard();
			}

			/*
			 * @see SelectionListener.widgetSelected (SelectionEvent)
			 */
			public void widgetSelected(SelectionEvent e) {
				copyToClipboard();
			}
		});
		copyItem.setText(JFaceResources.getString("copy")); //$NON-NLS-1$
		detailsArea.setMenu(copyMenu);
		detailsAreaCreated = true;
		return detailsArea;
	}

	/**
	 * Create the details button if it should be included.
	 * 
	 * @param parent
	 *            the parent composite
	 * @since 3.2
	 */
	protected void createDetailsButton(Composite parent) {
		if (shouldShowDetailsButton()) {
			createButton(parent,
					IDialogConstants.DETAILS_ID,
					IDialogConstants.SHOW_DETAILS_LABEL, false);
		}
	}

	/*
	 * @see IconAndMessageDialog#createDialogAndButtonArea(Composite)
	 */
	@Override
	protected void createDialogAndButtonArea(Composite parent) {
		super.createDialogAndButtonArea(parent);
		if (this.dialogArea instanceof Composite) {
			// Create a label if there are no children to force a smaller layout
			Composite dialogComposite = (Composite) dialogArea;
			if (dialogComposite.getChildren().length == 0) {
				new Label(dialogComposite, SWT.NULL);
			}
		}
	}
	
	protected void initializeBounds()
	{
		super.initializeBounds();
		currentRecordChanged();
	}

	/**
	 * This implementation of the <code>Dialog</code> framework method creates
	 * and lays out a composite. Subclasses that require a different dialog area
	 * may either override this method, or call the <code>super</code>
	 * implementation and add controls to the created composite.
	 * 
	 * Note: Since 3.4, the created composite no longer grabs excess vertical
	 * space. See https://bugs.eclipse.org/bugs/show_bug.cgi?id=72489. If the
	 * old behavior is desired by subclasses, get the returned composite's
	 * layout data and set grabExcessVerticalSpace to true.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		// Create a composite with standard margins and spacing
		// Add the messageArea to this composite so that as subclasses add
		// widgets to the messageArea
		// and dialogArea, the number of children of parent remains fixed and
		// with consistent layout.
		// Fixes bug #240135
		Composite composite = new Composite(parent, SWT.NONE);
		createMessageArea(composite);
		createSupportArea(parent);
		GridLayout layout = new GridLayout();
		layout.marginHeight = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_MARGIN);
		layout.marginWidth = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_MARGIN);
		layout.verticalSpacing = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_SPACING);
		layout.horizontalSpacing = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_SPACING);
		layout.numColumns = 2;
		composite.setLayout(layout);
		GridData childData = new GridData(GridData.FILL_BOTH);
		childData.horizontalSpan = 2;
		composite.setLayoutData(childData);
		composite.setFont(parent.getFont());

		return composite;
	}

	protected void createLabelArea(final Composite parent) {

		String message = getMessage();
		if (message != null) {

			int wHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
			IStatus s = getStatus();

			if (s == null) {
				return;
			}

			final ScrolledComposite sComp = new ScrolledComposite(parent,
					SWT.V_SCROLL);
			sComp.setLayoutData(new GridData(GridData.FILL_BOTH));
			GridData gd = new GridData(GridData.FILL_BOTH);

			sComp.setLayoutData(gd);

			final Composite comp = new Composite(sComp, SWT.NONE);
			sComp.setContent(comp);
			comp.setLayout(new GridLayout(1, false));

			Text messageText = new Text(comp, SWT.WRAP | SWT.READ_ONLY);
		
			messageText.setText(message);
			GridData gData = new GridData(GridData.FILL_HORIZONTAL);

			gData.grabExcessHorizontalSpace = true;
			gData.widthHint = wHint;
			messageText.setLayoutData(gData);
			messageText.setBackground(comp.getBackground());
			comp.layout();
			Point size = comp.computeSize(wHint, SWT.DEFAULT);
			comp.setSize(size);

			gd.minimumHeight = Math.min(100,size.y+20);

			sComp.addControlListener(new ControlAdapter() {
				@Override
				public void controlResized(final ControlEvent e) {
					Rectangle r = sComp.getClientArea();
					Point cSize = comp.computeSize(r.width, SWT.DEFAULT);
					comp.setSize(cSize);
				}
			});

		}

	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IconAndMessageDialog#createMessageArea(org.
	 * eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createMessageArea(final Composite parent) {

		Image image = getImage();
		if (image != null) {
			this.imageLabel = new Label(parent, SWT.NULL);
			image.setBackground(this.imageLabel.getBackground());
			this.imageLabel.setImage(image);

			this.imageLabel.setLayoutData(new GridData(
					GridData.HORIZONTAL_ALIGN_CENTER
					| GridData.VERTICAL_ALIGN_BEGINNING));
		}

		Composite composite = new Composite(parent, SWT.NONE);
		GridData gd = new GridData(GridData.FILL_BOTH);

		composite.setLayoutData(gd);
		composite.setLayout(new GridLayout(1, true));

		createLabelArea(composite);
		IStatus status = getStatus();
		if(status.getSeverity() > IStatus.INFO || status.getException() != null)
		{
			createReasonAndSolutionArea(composite);
		}


		return composite;

	}

	protected void createReasonAndSolutionArea(final Composite parent) {

		Reason[] reasons = getReasons();
		IStatus status = getStatus();
		Reason bug = null;
		if ((reasons == null) || (reasons.length == 0)) {
			Reason r = ProblemCenter.createUnknownReason(status);
			reasons = new Reason[]{r};
		}
		else if(reasons.length > 1)
		{
			// the "BUG" reason should not be displayed if other known reasons exist..
			// TODO this is a bit hackish - other way to do this?
			List<Reason> okReasons = new ArrayList<Reason>();

			for(Reason r : reasons)
			{
				if(r != null && "BUG".equals(r.getId()))
				{
					bug = r;
				}
				else okReasons.add(r);
			}
			if(bug != null)
			{
				reasons = okReasons.toArray(new Reason[okReasons.size()]);
			}
		}

		ImageRegistry imgReg = LogActivator.getDefault().getImageRegistry();
		Image reasonImage = imgReg.get("reason"); //$NON-NLS-1$


		Composite comp = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		comp.setLayout(layout);
		GridData gd = new GridData(GridData.FILL_BOTH);
		comp.setLayoutData(gd);
		Label reasonLabel = new Label(comp, SWT.NONE);
		reasonLabel.setText("Possible reasons:");

		final ScrolledComposite sComp = new ScrolledComposite(comp,
				SWT.V_SCROLL);
		sComp.setLayoutData(new GridData(GridData.FILL_BOTH));


		final Composite reasonList = new Composite(sComp, SWT.NONE);
		sComp.setContent(reasonList);
		reasonList.setLayout(new GridLayout(3, false));

		for (final Reason reason : reasons) {

			Label imgLabel = new Label(reasonList, SWT.NONE);
			imgLabel.setImage(reasonImage);
			GridData gData = new GridData();
			gData.horizontalAlignment = GridData.CENTER;
			gData.verticalAlignment = GridData.BEGINNING;
			imgLabel.setLayoutData(gData);

			Label label = new Label(reasonList, SWT.WRAP);
			label.setText(reason.getName());
			label.setToolTipText(reason.getDescription());
			gData = new GridData(GridData.FILL_HORIZONTAL);
			gData.grabExcessHorizontalSpace = true;
			gData.horizontalAlignment = GridData.BEGINNING;
			gData.horizontalSpan = 2;
			label.setLayoutData(gData);

			Solution[] solutions = getSolutions(reason);
			if(bug != null)
			{
				// even though we don't display this as bug with unknown reason,
				// we still want to report it, cause it occurred in an
				// unexpected place
				Solution[] newSolutions;
				Solution[] dealWithBug = getSolutions(bug);
				if(solutions != null)
				{
					newSolutions = new Solution[solutions.length+dealWithBug.length];
					System.arraycopy(solutions, 0, newSolutions, 0, solutions.length);
					System.arraycopy(dealWithBug, 0, newSolutions, solutions.length, dealWithBug.length);
				}
				else newSolutions = dealWithBug;
				solutions = newSolutions;
			}
			if ((solutions != null) && (solutions.length > 0)) {
				Label spacer = new Label(reasonList, SWT.NONE);
				spacer.setVisible(false);
				gData = new GridData();
				gData.horizontalAlignment = GridData.CENTER;
				gData.verticalAlignment = GridData.BEGINNING;
				spacer.setLayoutData(gData);
				Label solutionLabel = new Label(reasonList, SWT.NONE);
				solutionLabel.setText("Available solutions:");
				gData = new GridData();
				gData.horizontalAlignment = GridData.BEGINNING;
				gData.verticalAlignment = GridData.BEGINNING;
				gData.horizontalSpan = 2;
				solutionLabel.setLayoutData(gData);
				Image solutionImage = imgReg.get("solution"); //$NON-NLS-1$

				for (final Solution solution : solutions) {

					if (solution instanceof GraphicalSolution) {
						final GraphicalSolution graphicalSolution = (GraphicalSolution) solution;
						if (!graphicalSolution.isAvailableFor(getMessage(),
								getProblems(), getDetails())) {
							continue;
						}
						spacer = new Label(reasonList, SWT.NONE);
						spacer.setVisible(false);
						gData = new GridData();
						gData.horizontalAlignment = GridData.CENTER;
						gData.verticalAlignment = GridData.BEGINNING;
						spacer.setLayoutData(gData);
						final Label solImgLabel = new Label(reasonList,
								SWT.NONE);
						solImgLabel.setImage(solutionImage);
						gData = new GridData();
						gData.horizontalAlignment = GridData.CENTER;
						gData.verticalAlignment = GridData.BEGINNING;
						solImgLabel.setLayoutData(gData);
						final Link link = new Link(reasonList, SWT.WRAP);
						String text = graphicalSolution.getDescription();
						link.setText("<a>" + text + "</a>"); //$NON-NLS-1$ //$NON-NLS-2$

						gData = new GridData(GridData.FILL_HORIZONTAL);
						gData.grabExcessHorizontalSpace = true;
						gData.horizontalAlignment = GridData.BEGINNING;
						link.setLayoutData(gData);
						link.addSelectionListener(new SelectionAdapter() {
							@SuppressWarnings("synthetic-access")
							@Override
							public void widgetSelected(
									final SelectionEvent e) {

								try {
									boolean enabled = graphicalSolution
									.solve(getMessage(),
											getProblems(),
											getDetails());
									solImgLabel.setEnabled(enabled);
									link.setEnabled(enabled);

								} catch (Exception itExc) {

									LogActivator
									.log(IStatus.ERROR,
											"Error while trying to resolve problem",
											itExc);
								}
							}
						});
					} else {
						spacer = new Label(reasonList, SWT.NONE);
						spacer.setVisible(false);
						gData = new GridData();
						gData.horizontalAlignment = GridData.CENTER;
						gData.verticalAlignment = GridData.BEGINNING;
						spacer.setLayoutData(gData);
						Label solImgLabel = new Label(reasonList, SWT.NONE);
						solImgLabel.setImage(solutionImage);
						gData = new GridData();
						gData.horizontalAlignment = GridData.CENTER;
						gData.verticalAlignment = GridData.BEGINNING;
						solImgLabel.setLayoutData(gData);
						Label solutionDescription = new Label(reasonList,
								SWT.WRAP);
						solutionDescription.setText(solution
								.getDescription());

						gData = new GridData(GridData.FILL_HORIZONTAL);
						gData.grabExcessHorizontalSpace = true;
						gData.horizontalAlignment = GridData.BEGINNING;
						solutionDescription.setLayoutData(gData);
					}
				}
			}
			Label spacer = new Label(reasonList, SWT.NONE);
			spacer.setVisible(false);
			gData = new GridData();
			gData.horizontalAlignment = GridData.CENTER;
			gData.verticalAlignment = GridData.BEGINNING;
			spacer.setLayoutData(gData);

			Composite fineGrainedRadios = new Composite(reasonList,
					SWT.NONE);
			Label fineGrainedLabel = new Label(fineGrainedRadios, SWT.NONE);
			gData = new GridData(SWT.FILL, SWT.CENTER, false, false);
			gData.horizontalSpan = 3;
			fineGrainedLabel.setLayoutData(gData);
			fineGrainedLabel
			.setText("Show this dialog when an event with this reason occcurs:");
			gData = new GridData();
			gData.horizontalAlignment = GridData.BEGINNING;
			gData.verticalAlignment = GridData.BEGINNING;
			gData.horizontalSpan = 2;
			fineGrainedRadios.setLayoutData(gData);
			fineGrainedRadios.setLayout(new GridLayout(3, false));
			Button showForReasonButton = new Button(fineGrainedRadios,
					SWT.RADIO);
			showForReasonButton.setText("Always");
			showForReasonButton
			.addSelectionListener(new SelectionListener() {

				public void widgetDefaultSelected(SelectionEvent e) {

				}

				public void widgetSelected(SelectionEvent e) {
					LogActivator.setAlarmUser(reason.getId(), true);
				}
			});

			Button hideForReasonButton = new Button(fineGrainedRadios,
					SWT.RADIO);
			hideForReasonButton.setText("Never");
			hideForReasonButton
			.addSelectionListener(new SelectionListener() {

				public void widgetDefaultSelected(SelectionEvent e) {

				}

				public void widgetSelected(SelectionEvent e) {
					LogActivator.setAlarmUser(reason.getId(), false);
				}
			});

			Button undecidedForReasonButton = new Button(fineGrainedRadios,
					SWT.RADIO);
			undecidedForReasonButton
			.addSelectionListener(new SelectionListener() {

				public void widgetDefaultSelected(SelectionEvent e) {

				}

				public void widgetSelected(SelectionEvent e) {
					LogActivator.setAlarmUser(reason.getId(), null);
				}
			});
			undecidedForReasonButton.setText("Depending on event severity");
			Boolean b = LogActivator.getAlarmUser(reason);
			if (b == null) {
				undecidedForReasonButton.setSelection(true);
			} else if (b) {
				showForReasonButton.setSelection(true);
			} else {
				hideForReasonButton.setSelection(true);
			}



			int wHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
			reasonList.setSize(reasonList.computeSize(wHint, SWT.DEFAULT));

			sComp.addControlListener(new ControlAdapter() {
				@Override
				public void controlResized(final ControlEvent e) {
					Rectangle r = sComp.getClientArea();
					Point cSize = reasonList.computeSize(r.width, SWT.DEFAULT);
					reasonList.setSize(cSize);
				}
			});

		}

	}

	/**
	 * Create the area for extra error support information.
	 * 
	 * @param parent
	 */
	private void createSupportArea(Composite parent) {

		ErrorSupportProvider provider = Policy.getErrorSupportProvider();
		IStatus s = getStatus();
		if (provider == null || s == null) {
			return;
		}

		Composite supportArea = new Composite(parent, SWT.NONE);
		provider.createSupportArea(supportArea, s);

		GridData supportData = new GridData(SWT.FILL, SWT.FILL, true, true);
		supportData.verticalSpan = 3;
		supportArea.setLayoutData(supportData);
		if (supportArea.getLayout() == null) {
			GridLayout layout = new GridLayout();
			layout.marginWidth = 0;
			layout.marginHeight = 0;
			supportArea.setLayout(layout); // Give it a default layout if one
			// isn't set
		}

	}

	public void currentRecordChanged() {
		Event record = getCurrentEvent();
		if (record == null) {
			close(); // no problems left => no need to stay open
		} else {
			if (detailsAreaCreated) {
				toggleDetailsArea();
			}
			if (dialogArea != null) {
				Composite parent = dialogArea.getParent();
				dialogArea.dispose();
				buttonBar.dispose();
				createDialogAndButtonArea(parent);
				
				getShell().layout();
			
				
			}
		}
		Point windowSize = getShell().getSize();
		Point newSize = getShell().computeSize(SWT.DEFAULT, SWT.DEFAULT);
		getShell().setSize(windowSize.x, newSize.y + 20);
		getShell().setText(getTitle());
		
		// the next lines seem braindead, but without them, the lower window
		// decoration of the shell is messed up under gtk..
//		getShell().setVisible(false);
//		getShell().setVisible(true);
//		getShell().setFocus();

	}

	public Event getCurrentEvent() {
		return eventQueue.getCurrentEvent();
	}

	public String getDetails() {
		return getCurrentEvent().getDetails();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.IconAndMessageDialog#getImage()
	 */
	@Override
	protected Image getImage() {
		IStatus status = getStatus();
		if (status != null) {
			if (status.getSeverity() == IStatus.WARNING) {
				return getWarningImage();
			}
			if (status.getSeverity() == IStatus.INFO) {
				return getInfoImage();
			}
		}
		// If it was not a warning or an error then return the error image
		return getErrorImage();
	}

	public String getMessage() {
		return getCurrentEvent().getStatus().getMessage();
	}

	public Problem[] getProblems() {
		return getCurrentEvent().getProblems();
	}

	protected Reason[] getReasons() {
		Problem[] problems = getProblems();
		Reason[] result = new Reason[problems.length];

		for (int i = 0; i < problems.length; i++) {
			result[i] = problems[i].getReason();
		}
		return result;
	}

	protected Solution[] getSolutions(Reason reason) {

		java.util.List<Solution> resultList = new ArrayList<Solution>();
		resultList
		.addAll(ProblemCenter.getProblemConfig().getSolutions(reason));
		// resultList.add( new ReportProblemSolution(t) );
		return resultList.toArray(new Solution[resultList.size()]);

	}

	public IStatus getStatus() {
		Event e = getCurrentEvent();
		if (e != null) {
			return e.getStatus();
		} else {
			return null;
		}
	}

	protected String getTitle() {
		String title = null;
		IStatus s = getStatus();
		if (s == null) {
			title = "";
		} else if (s.getSeverity() == IStatus.ERROR) {
			title = "An error occured....";
		} else if (s.getSeverity() == IStatus.WARNING) {
			title = "Warning";
		} else if (s.getSeverity() == IStatus.INFO) {
			title = "Info";
		}
		return title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#isResizable()
	 */
	@Override
	protected boolean isResizable() {
		return true;
	}

	/*
	 * (non-Javadoc) Method declared on Window.
	 */
	/**
	 * Extends <code>Window.open()</code>. Opens an error dialog to display the
	 * error. If you specified a mask to filter the displaying of these
	 * children, the error dialog will only be displayed if there is at least
	 * one child status matching the mask.
	 */
	@Override
	public int open() {
		if (!AUTOMATED_MODE && shouldDisplay(getStatus(), displayMask)) {
			return super.open();
		}
		setReturnCode(OK);
		return OK;
	}


	public void eventAdded(Event record) {
		checkButtonsEnabled();

	}

	public void eventRemoved(Event record) {
		checkButtonsEnabled();
	}

	/**
	 * Set the status displayed by this error dialog to the given status. This
	 * only affects the status displayed by the Details list. The message, image
	 * and title should be updated by the subclass, if desired.
	 * 
	 * @param status
	 *            the status to be displayed in the details list
	 * @since 3.1
	 */
	protected final void setStatus(IStatus status) {
		if (detailsAreaCreated) {
			detailsArea.setText(getDetails());
		}
	}

	protected boolean shouldShowDetailsButton() {
		return getDetails() != null;
	}

	protected final void showDetailsArea() {
		if (!detailsAreaCreated) {
			Control control = getContents();
			if (control != null && !control.isDisposed()) {
				toggleDetailsArea();
			}
		}
	}

	protected void toggleDetailsArea() {
		Point windowSize = getShell().getSize();

		if (detailsAreaCreated) {
			detailsArea.dispose();
			detailsAreaCreated = false;
			getButton(IDialogConstants.DETAILS_ID).setText(
					IDialogConstants.SHOW_DETAILS_LABEL);
		} else {
			detailsArea = createDetailsArea((Composite) getContents());
			getButton(IDialogConstants.DETAILS_ID).setText(
					IDialogConstants.HIDE_DETAILS_LABEL);

		}
		Point newSize = getShell().computeSize(SWT.DEFAULT, SWT.DEFAULT);
		getShell().setSize(windowSize.x, newSize.y);
		// the next lines seem braindead, but without them, the lower window
		// decoration of the shell is messed up under gtk..
		getShell().setVisible(false);
		getShell().setVisible(true);
		getShell().setFocus();
	}

	/**
	 * Returns whether the given status object should be displayed.
	 * 
	 * @param status
	 *            a status object
	 * @param mask
	 *            a mask as per <code>IStatus.matches</code>
	 * @return <code>true</code> if the given status should be displayed, and
	 *         <code>false</code> otherwise
	 * @see org.eclipse.core.runtime.IStatus#matches(int)
	 */
	protected static boolean shouldDisplay(IStatus status, int mask) {
		if (status == null) {
			return false;
		}
		IStatus[] children = status.getChildren();
		if (children == null || children.length == 0) {
			return status.matches(mask);
		}
		for (int i = 0; i < children.length; i++) {
			if (children[i].matches(mask)) {
				return true;
			}
		}
		return false;
	}

	public void currentEventChanged(Event selected) {
		currentRecordChanged();
		checkButtonsEnabled();
	}

}
