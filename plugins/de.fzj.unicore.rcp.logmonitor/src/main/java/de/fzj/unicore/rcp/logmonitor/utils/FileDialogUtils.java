package de.fzj.unicore.rcp.logmonitor.utils;

import org.eclipse.jface.dialogs.IDialogSettings;

import de.fzj.unicore.rcp.logmonitor.LogActivator;

public class FileDialogUtils {
	
	private static final String SECTION_FILTER_PATHS = "DialogFilterPaths";
	private static String mostRecentlySavedPath = System.getProperty("user.home");
	
	public static String getFilterPath(String dialogId)
	{
		
		LogActivator activator = LogActivator.getDefault();
		IDialogSettings settings = activator.getDialogSettings();
		IDialogSettings s = settings.getSection(SECTION_FILTER_PATHS);
		if(s == null) s = settings.addNewSection(SECTION_FILTER_PATHS);
		String result = dialogId == null ? null : s.get(dialogId);
		if(result == null) result = mostRecentlySavedPath;
		return result;
	}
	
	public static void saveFilterPath(String dialogId, String filterPath)
	{
		if(dialogId == null || filterPath == null || filterPath.trim().length() == 0) return;
		LogActivator activator = LogActivator.getDefault();
		IDialogSettings settings = activator.getDialogSettings();
		IDialogSettings s = settings.getSection(SECTION_FILTER_PATHS);
		if(s == null) s = settings.addNewSection(SECTION_FILTER_PATHS);
		s.put(dialogId, filterPath);
	}
	
	
	

}
