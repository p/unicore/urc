package de.fzj.unicore.rcp.logmonitor.utils;

import org.eclipse.core.runtime.jobs.Job;

public abstract class BackgroundJob extends Job {
	
	/**
	 * Extend this class or use this job family for all your background jobs to
	 * make sure that they get cancelled on shutdown.
	 */
	public static final Object UNICORE_JOB_FAMILY = new Object();

	public BackgroundJob(String name) {
		super(name);
	}

	public boolean belongsTo(Object family) {
		return UNICORE_JOB_FAMILY.equals(family);
	}
	
	

}
