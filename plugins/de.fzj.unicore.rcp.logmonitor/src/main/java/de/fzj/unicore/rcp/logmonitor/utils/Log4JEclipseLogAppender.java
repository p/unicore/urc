package de.fzj.unicore.rcp.logmonitor.utils;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.Priority;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;
import org.eclipse.core.runtime.IStatus;

/**
 * This class is a custom Log4J appender that sends Log4J events to the Eclipse
 * log
 */
public class Log4JEclipseLogAppender extends AppenderSkeleton {

	private LogListener listener;


	public Log4JEclipseLogAppender(LogListener listener) {
		super();
		this.listener = listener;
	}



	/**
	 * Log event happened. Translates level to status instance codes: level >
	 * Level.ERROR - Status.ERROR level > Level.WARN - Status.WARNING level >
	 * Level.DEBUG - Status.INFO default - Status.OK
	 * 
	 * @param event
	 *            LoggingEvent instance
	 */
	@Override
	public void append(LoggingEvent event) {

		String text = event.getRenderedMessage();
		Throwable thrown = null;

		ThrowableInformation info = event.getThrowableInformation();
		if (info != null) {
			thrown = info.getThrowable();
		}

		Level level = event.getLevel();
		int severity = IStatus.OK;

		if (level.toInt() >= Priority.ERROR_INT) {
			severity = IStatus.ERROR;
		} else if (level.toInt() >= Priority.WARN_INT) {
			severity = IStatus.WARNING;
		} else if (level.toInt() >= Priority.DEBUG_INT) {
			severity = IStatus.INFO;
		}
		
		listener.notifyLogged(severity, text, thrown);
		
	}

	/**
	 * Closes this appender
	 */
	@Override
	public void close() {
		this.closed = true;
	}

	/**
	 * Checks if this appender requires layout
	 * 
	 * @return true if layout is required.
	 */
	@Override
	public boolean requiresLayout() {
		return true;
	}

}
