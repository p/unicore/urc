package de.fzj.unicore.rcp.logmonitor;

/*******************************************************************************
 * Copyright (c) 2000, 2004 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.SelectionProviderAction;

import de.fzj.unicore.rcp.logmonitor.problems.ProblemCenter;
import eu.unicore.problemutil.Problem;
import eu.unicore.problemutil.Reason;

public class EventDetailsDialogAction extends SelectionProviderAction {

	/**
	 * The shell in which to open the property dialog
	 */
	private Shell shell;

	/**
	 * Creates a new action for opening a property dialog on the elements from
	 * the given selection provider
	 * 
	 * @param shell
	 *            - the shell in which the dialog will open
	 * @param provider
	 *            - the selection provider whose elements the property dialog
	 *            will describe
	 */
	public EventDetailsDialogAction(Shell shell, ISelectionProvider provider) {
		super(provider, "Event Details"); //$NON-NLS-1$
		this.shell = shell;
		// setToolTipText
		// WorkbenchHelp.setHelp
	}

	@Override
	public void run() {

		// get initial selection
		Object element = getStructuredSelection().getFirstElement();
		if (element == null || !(element instanceof LogEntry)) {
			return;
		}

		Display d = PlatformUI.getWorkbench().getDisplay();
		if(shell == null)
		{
			shell = d.getActiveShell();
		}

		if (shell == null) {
			shell = new Shell(d);
		}

		LogViewContentProvider contentProvider = (LogViewContentProvider) ((TreeViewer) getSelectionProvider())
		.getContentProvider();
		Object[] entries = contentProvider.getElements(null);
		Event selected = null;
		LogEntry selectedLogEntry = (LogEntry) element;
		IEventQueue eventQueue = LogActivator.getDefault().getEventQueue();
		for (int i = 0; i < entries.length; i++) {
			LogEntry current = (LogEntry) entries[i];
			int severity = current.getSeverity();
			Throwable cause = current.getCause();
			IStatus status = new Status(severity, current.getPluginId(),
					current.getMessage(), cause);

			List<Problem> problems = new ArrayList<Problem>(
					current.getReasonIDs().length);
			for (String r : current.getReasonIDs()) {
				Reason reason = ProblemCenter.getProblemConfig().getReason(r);
				if (reason != null) {
					Problem p = new Problem(reason, cause,
							new HashMap<String, Object>());
					problems.add(p);
				}
			}
			Event record = new Event(current.getKey(),current.getDate(),problems.toArray(new Problem[problems
			                                                      .size()]), status, current.getStack());
			if(!eventQueue.containsEvent(record))
			{
				eventQueue.addEvent(record);
			}
			if(current == selectedLogEntry)
			{
				selected = record;
			}
		}

		if(selected != null)
		{
			eventQueue.jumpToEvent(selected);
		}
		eventQueue.sortEvents(Event.createDateComparator(Event.ASCENDING));
		LogActivator.getDefault().openEventDialog(shell);

	}

}
