/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

package de.fzj.unicore.rcp.logmonitor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.util.Policy;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.XMLMemento;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.part.ViewPart;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

/**
 * The LogView represents the GUI for log monitor.
 * 
 * @author Henning Mersch, Valentina Huber
 */
public class LogView extends ViewPart implements LoggingConstants {

	public static final String ID = "de.fzj.unicore.rcp.logmonitor.view";

	private static final String P_COLUMN_1 = "column2"; //$NON-NLS-1$
	private static final String P_COLUMN_2 = "column3"; //$NON-NLS-1$
	private static final String P_COLUMN_3 = "column4"; //$NON-NLS-1$
	private int MESSAGE_ORDER;
	private int PLUGIN_ORDER;
	private int DATE_ORDER;

	public final static byte MESSAGE = 0x1;
	public final static byte PLUGIN = 0x2;
	public final static byte DATE = 0x0;
	public static int ASCENDING = 1;
	public static int DESCENDING = -1;

	private Clipboard fClipboard;

	private IMemento fMemento;
	private File fInputFile;
	private String fDirectory;
	private List<LogEntry> logEntries;
	private LogEntry[] logEntryArray;

	private Comparator<LogEntry> fComparator;

	public static final String P_LOG_IGNOREECLIPSE = "showAllPlugins";
	public static final String LOG_PLUGINPREFIX = "PluginLogConfig_";

	// hover text

	private Text fTextLabel;
	private Shell fTextShell;

	private TreeColumn fColumn1;
	private TreeColumn fColumn2;
	private TreeColumn fColumn3;

	private Tree fTree;
	private TreeViewer fTreeViewer;
	private LogViewLabelProvider fLabelProvider;

	private Action fPropertiesAction;
	private Action fClearLogAction;
	private Action fReloadLogAction;
	private Action fCopyAction;
	private Action fActivateViewAction;
	private Action fOpenLogAction;
	private Action fExportAction;

	public LogView() {
		fInputFile = Platform.getLogFileLocation().toFile();
	}

	private void addMouseListeners() {
		Listener tableListener = new Listener() {
			public void handleEvent(Event e) {
				switch (e.type) {
				case SWT.MouseMove:
					onMouseMove(e);
					break;
				case SWT.MouseHover:
					onMouseHover(e);
					break;
				case SWT.MouseDown:
					onMouseDown(e);
					break;
				}
			}
		};
		int[] tableEvents = new int[] { SWT.MouseDown, SWT.MouseMove,
				SWT.MouseHover };
		for (int i = 0; i < tableEvents.length; i++) {
			fTree.addListener(tableEvents[i], tableListener);
		}
	}

	public void asyncRefresh(final boolean activate) {
		Display display = fTree.getDisplay();
		// final ViewPart view = this;
		if (display != null) {
			display.asyncExec(new Runnable() {
				public void run() {
					refresh(activate);
				}
			});
		}
	}

	private void copy(File inputFile, File outputFile) {
		BufferedReader reader = null;
		BufferedWriter writer = null;
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(inputFile), "UTF-8")); //$NON-NLS-1$
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(outputFile), "UTF-8")); //$NON-NLS-1$
			while (reader.ready()) {
				writer.write(reader.readLine());
				writer.write(System.getProperty("line.separator")); //$NON-NLS-1$
			}
		} catch (IOException e) {
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e1) {
			}
		}
	}

	private void copyToClipboard(ISelection selection) {

		if (selection.isEmpty()) {
			return;
		}
		LogEntry entry = (LogEntry) ((IStructuredSelection) selection)
				.getFirstElement();

		String textVersion = entry.getMessage();

		if (textVersion.trim().length() > 0) {
			// set the clipboard contents
			fClipboard.setContents(new Object[] { textVersion },
					new Transfer[] { TextTransfer.getInstance() });
		}
	}

	private void createActions() {
		IActionBars bars = getViewSite().getActionBars();

		fCopyAction = createCopyAction();
		bars.setGlobalActionHandler(ActionFactory.COPY.getId(), fCopyAction);

		IToolBarManager toolBarManager = bars.getToolBarManager();

		fExportAction = createExportAction();
		toolBarManager.add(fExportAction);

		toolBarManager.add(new Separator());

		fClearLogAction = createClearAction();
		toolBarManager.add(fClearLogAction);

		fOpenLogAction = createOpenLogAction();
		toolBarManager.add(fOpenLogAction);

		fReloadLogAction = createReloadLogAction();
		toolBarManager.add(fReloadLogAction);

		toolBarManager.add(new Separator());

		IMenuManager mgr = bars.getMenuManager();
		mgr.add(createFilterAction());
		mgr.add(new Separator());

		fActivateViewAction = createActivateViewAction();
		mgr.add(fActivateViewAction);

		createPropertiesAction();

		MenuManager popupMenuManager = new MenuManager("#PopupMenu"); //$NON-NLS-1$
		IMenuListener listener = new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				manager.add(fCopyAction);
				manager.add(new Separator());
				manager.add(fClearLogAction);
				manager.add(fOpenLogAction);
				manager.add(fReloadLogAction);
				manager.add(new Separator());
				manager.add(fExportAction);
				manager.add(new Separator());
				manager.add(fPropertiesAction);
				manager.add(new Separator(
						IWorkbenchActionConstants.MB_ADDITIONS));
			}
		};
		popupMenuManager.addMenuListener(listener);
		popupMenuManager.setRemoveAllWhenShown(true);
		getSite().registerContextMenu(popupMenuManager,
				getSite().getSelectionProvider());
		Menu menu = popupMenuManager.createContextMenu(fTree);
		fTree.setMenu(menu);
	}

	private Action createActivateViewAction() {
		Action action = new Action("Activate on new Events") { //
			@Override
			public void run() {
				LogActivator.getDefault().setOpeningLogViewOnEvent(isChecked());
			}
		};
		action.setChecked(LogActivator.getDefault().isOpeningLogViewOnEvent());
		return action;
	}

	private Action createClearAction() {
		Action action = new Action("Clear") {
			@Override
			public void run() {
				handleClear();
			}
		};
		action.setImageDescriptor(LogActivator.getImageDescriptor("clear.gif"));
		action.setDisabledImageDescriptor(LogActivator
				.getImageDescriptor("clear.gif"));
		action.setToolTipText("Clear log");
		action.setText("Clear Log");
		return action;
	}

	private void createColumns(Tree tree) {
		fColumn1 = new TreeColumn(tree, SWT.LEFT);
		fColumn1.setText("Severity / Message");
		fColumn1.setWidth(fMemento.getInteger(P_COLUMN_1).intValue());
		fColumn1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				MESSAGE_ORDER *= -1;
				ViewerComparator comparator = getViewerComparator(MESSAGE);
				fTreeViewer.setComparator(comparator);
				setComparator(MESSAGE);
				fMemento.putInteger(P_ORDER_VALUE, MESSAGE_ORDER);
				fMemento.putInteger(P_ORDER_TYPE, MESSAGE);
				setColumnSorting(fColumn1, MESSAGE_ORDER);
			}
		});

		fColumn2 = new TreeColumn(tree, SWT.LEFT);
		fColumn2.setText("Plugin");
		fColumn2.setWidth(fMemento.getInteger(P_COLUMN_2).intValue());
		fColumn2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				PLUGIN_ORDER *= -1;
				ViewerComparator comparator = getViewerComparator(PLUGIN);
				fTreeViewer.setComparator(comparator);
				setComparator(PLUGIN);
				fMemento.putInteger(P_ORDER_VALUE, PLUGIN_ORDER);
				fMemento.putInteger(P_ORDER_TYPE, PLUGIN);
				setColumnSorting(fColumn2, PLUGIN_ORDER);
			}
		});

		fColumn3 = new TreeColumn(tree, SWT.LEFT);
		fColumn3.setText("Date");
		fColumn3.setWidth(fMemento.getInteger(P_COLUMN_3).intValue());
		fColumn3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DATE_ORDER *= -1;
				ViewerComparator comparator = getViewerComparator(DATE);
				fTreeViewer.setComparator(comparator);
				setComparator(DATE);
				fMemento.putInteger(P_ORDER_VALUE, DATE_ORDER);
				fMemento.putInteger(P_ORDER_TYPE, DATE);
				setColumnSorting(fColumn3, DATE_ORDER);
			}
		});

		tree.setHeaderVisible(true);
	}

	private Action createCopyAction() {
		Action action = new Action("Copy") {
			@Override
			public void run() {
				copyToClipboard(fTreeViewer.getSelection());
			}
		};
		action.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_TOOL_COPY));
		return action;
	}

	private Action createExportAction() {
		Action action = new Action("Export Log...") {
			@Override
			public void run() {
				handleExport();
			}
		};
		action.setToolTipText("Export log to a file");
		action.setImageDescriptor(LogActivator
				.getImageDescriptor("export_log.gif"));
		action.setDisabledImageDescriptor(LogActivator
				.getImageDescriptor("export_log.gif"));
		action.setEnabled(fInputFile.exists());
		return action;
	}

	private Action createFilterAction() {
		Action action = new Action("Filter") {
			@Override
			public void run() {
				handleFilter();
			}
		};
		action.setToolTipText("Configure filter for logs");
		action.setImageDescriptor(LogActivator
				.getImageDescriptor("filter_ps.gif"));
		action.setDisabledImageDescriptor(LogActivator
				.getImageDescriptor("filter_ps.gif"));
		return action;
	}

	private Action createOpenLogAction() {

		Action action = null;
		try {
			// check to see if org.eclipse.ui.ide is available
			Class.forName("org.eclipse.ui.ide.IDE"); //$NON-NLS-1$
			// check to see if org.eclipse.core.filesystem is available
			Class.forName("org.eclipse.core.filesystem.IFileStore"); //$NON-NLS-1$
			action = new OpenIDELogFileAction();
		} catch (ClassNotFoundException e) {
			action = new Action() {
				@Override
				public void run() {
					if (fInputFile.exists()) {
						Job job = getOpenLogFileJob();
						job.setUser(false);
						job.setPriority(Job.SHORT);
						job.schedule();
					}
				}
			};
		}
		action.setText("Open log");
		action.setImageDescriptor(LogActivator
				.getImageDescriptor("open_log.gif"));
		action.setDisabledImageDescriptor(LogActivator
				.getImageDescriptor("open_log.gif"));
		action.setEnabled(fInputFile.exists());
		action.setToolTipText("Open log in text editor or external viewer");
		return action;

	}

	@Override
	public void createPartControl(Composite parent) {
		createViewer(parent);
		createActions();
		fClipboard = new Clipboard(fTree.getDisplay());
		fTree.setToolTipText(""); //$NON-NLS-1$
		getSite().setSelectionProvider(fTreeViewer);
		initializeViewerSorter();

		makeHoverShell();

		// PlatformUI.getWorkbench().getHelpSystem().setHelp(fTree,
		// IHelpContextIds.LOG_VIEW);
	}

	private void createPropertiesAction() {
		fPropertiesAction = new EventDetailsDialogAction(fTree.getShell(),
				fTreeViewer);
		fPropertiesAction.setImageDescriptor(LogActivator
				.getImageDescriptor("properties.gif"));
		fPropertiesAction.setDisabledImageDescriptor(LogActivator
				.getImageDescriptor("properties.gif"));
		fPropertiesAction.setToolTipText("Show Properties");
		fPropertiesAction.setEnabled(false);
	}

	private Action createReloadLogAction() {
		Action action = new Action("Reload log") {
			@Override
			public void run() {
				fInputFile = Platform.getLogFileLocation().toFile();
				reloadLog();
			}
		};
		action.setToolTipText("Reload a log from disc");
		action.setImageDescriptor(LogActivator
				.getImageDescriptor("restore_log.gif"));
		action.setDisabledImageDescriptor(LogActivator
				.getImageDescriptor("restore_log.gif"));
		return action;
	}

	private void createViewer(Composite parent) {
		fTreeViewer = new TreeViewer(parent, SWT.FULL_SELECTION);
		fTree = fTreeViewer.getTree();
		fTree.setLinesVisible(true);
		createColumns(fTree);
		fTreeViewer.setContentProvider(new LogViewContentProvider(this));
		fTreeViewer
				.setLabelProvider(fLabelProvider = new LogViewLabelProvider());
		fLabelProvider.connect(this);
		fTreeViewer
				.addSelectionChangedListener(new ISelectionChangedListener() {
					public void selectionChanged(SelectionChangedEvent e) {
						handleSelectionChanged(e.getSelection());
					}
				});
		fTreeViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				fPropertiesAction.run();
			}
		});
		fTreeViewer.setInput(this);
		addMouseListeners();
	}

	@Override
	public void dispose() {
		writeSettings();
		fClipboard.dispose();
		if (fTextShell != null) {
			fTextShell.dispose();
		}
		LogReader.reset();
		fLabelProvider.disconnect(this);
		super.dispose();
	}

	public void fillContextMenu(IMenuManager manager) {
	}

	public Comparator<LogEntry> getComparator() {
		return fComparator;
	}

	private Comparator<Object> getDefaultComparator() {
		return Policy.getComparator();
	}

	private List<LogEntry> getLogList() {
		if (logEntries == null) {
			return LogActivator.getDefault().getLogEntries(); // either use the
																// globally
																// shared log
																// entry list
		} else {
			return logEntries; // or use your own list (only after loading
								// entries from some other log file)
		}
	}

	/**
	 * Returns the plugin preferences used to maintain state of log view
	 * 
	 * @return the plugin preferences
	 */
	private IEclipsePreferences getLogPreferences() {
		return LogActivator.getDefault().getPreferences();
	}

	public LogEntry[] getLogs() {
		if (logEntryArray == null) {
			List<LogEntry> list = getLogList();
			List<LogEntry> result = new ArrayList<LogEntry>(list.size());
			for (LogEntry current : list) {
				if (showLogEntry(current)) {
					result.add(current);
				}
			}
			logEntryArray = result.toArray(new LogEntry[result.size()]);
		}

		return logEntryArray;
	}


	protected Job getOpenLogFileJob() {
		final Shell shell = getViewSite().getShell();
		return new BackgroundJob("Open...") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				boolean failed = false;
				if (fInputFile.length() <= LogReader.MAX_FILE_LENGTH) {
					failed = !Program.launch(fInputFile.getAbsolutePath());
					if (failed) {
						Program p = Program.findProgram(".txt"); //$NON-NLS-1$
						if (p != null) {
							p.execute(fInputFile.getAbsolutePath());
							return Status.OK_STATUS;
						}
					}
				}
				if (failed) {
					final OpenLogDialog openDialog = new OpenLogDialog(shell,
							fInputFile);
					Display.getDefault().asyncExec(new Runnable() {
						public void run() {
							openDialog.create();
							openDialog.open();
						}
					});
				}
				return Status.OK_STATUS;
			}
		};
	}
	
	

	private ViewerComparator getViewerComparator(byte sortType) {
		if (sortType == PLUGIN) {
			return new ViewerComparator() {
				@SuppressWarnings("unchecked")
				@Override
				public int compare(Viewer viewer, Object e1, Object e2) {
					LogEntry entry1 = (LogEntry) e1;
					LogEntry entry2 = (LogEntry) e2;
					Comparator<String> c = getComparator();
					return c.compare(entry1.getPluginId(),
							entry2.getPluginId())
							* PLUGIN_ORDER;
				}
			};
		} else if (sortType == MESSAGE) {
			return new ViewerComparator() {
				@SuppressWarnings("unchecked")
				@Override
				public int compare(Viewer viewer, Object e1, Object e2) {
					LogEntry entry1 = (LogEntry) e1;
					LogEntry entry2 = (LogEntry) e2;
					Comparator<String> c = getComparator();
					return c.compare(entry1.getMessage(),
							entry2.getMessage())
							* MESSAGE_ORDER;
				}
			};
		} else {
			return new ViewerComparator() {
				@Override
				public int compare(Viewer viewer, Object e1, Object e2) {
					long date1 = ((LogEntry) e1).getDate().getTime();
					long date2 = ((LogEntry) e2).getDate().getTime();
					if (date1 == date2) {
						int result = getLogList().indexOf(e2)
								- getLogList().indexOf(e1);
						if (DATE_ORDER == DESCENDING) {
							result *= DESCENDING;
						}
						return result;
					}
					if (DATE_ORDER == DESCENDING) {
						return date1 > date2 ? DESCENDING : ASCENDING;
					}
					return date1 < date2 ? DESCENDING : ASCENDING;
				}
			};
		}
	}

	protected void handleClear() {
		BusyIndicator.showWhile(fTree.getDisplay(), new Runnable() {
			public void run() {
				getLogList().clear();
				asyncRefresh(false);
				FileOutputStream os;
				try {
					// clear log file by creating a new output stream and
					// closing it without writing
					os = new FileOutputStream(fInputFile, false);
					os.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	private void handleExport() {
		FileDialog dialog = new FileDialog(getViewSite().getShell(), SWT.SAVE);
		dialog.setFilterExtensions(new String[] { "*.log" }); //$NON-NLS-1$
		if (fDirectory != null) {
			dialog.setFilterPath(fDirectory);
		}
		String path = dialog.open();
		if (path != null) {
			if (path.indexOf('.') == -1 && !path.endsWith(".log")) {
				path += ".log"; //$NON-NLS-1$
			}
			File outputFile = new Path(path).toFile();
			fDirectory = outputFile.getParent();
			if (outputFile.exists()) {
				String message = NLS.bind("Really overwrite?",
						outputFile.toString());
				if (!MessageDialog.openQuestion(getViewSite().getShell(),
						"Export log", message)) {
					return;
				}
			}
			copy(fInputFile, outputFile);
		}
	}

	private void handleFilter() {
		FilterDialog dialog = new FilterDialog(this.getSite().getShell(),
				fMemento);
		dialog.create();
		dialog.getShell().setText("Filter log messages");
		if (dialog.open() == Window.OK) {
			reloadLog();
		}
	}

	private void handleSelectionChanged(ISelection selection) {
		updateStatus(selection);
		fCopyAction.setEnabled(!selection.isEmpty());
		fPropertiesAction.setEnabled(!selection.isEmpty());
	}

	@Override
	public void init(IViewSite site, IMemento memento) throws PartInitException {
		super.init(site, memento);
		if (memento == null) {
			this.fMemento = XMLMemento.createWriteRoot("LOGVIEW"); //$NON-NLS-1$
		} else {
			this.fMemento = memento;
		}
		readSettings();

		// initialize column ordering
		final byte type = this.fMemento.getInteger(P_ORDER_TYPE).byteValue();
		switch (type) {
		case DATE:
			DATE_ORDER = this.fMemento.getInteger(P_ORDER_VALUE).intValue();
			MESSAGE_ORDER = DESCENDING;
			PLUGIN_ORDER = DESCENDING;
			break;
		case MESSAGE:
			MESSAGE_ORDER = this.fMemento.getInteger(P_ORDER_VALUE).intValue();
			DATE_ORDER = DESCENDING;
			PLUGIN_ORDER = DESCENDING;
			break;
		case PLUGIN:
			PLUGIN_ORDER = this.fMemento.getInteger(P_ORDER_VALUE).intValue();
			MESSAGE_ORDER = DESCENDING;
			DATE_ORDER = DESCENDING;
			break;
		default:
			DATE_ORDER = DESCENDING;
			MESSAGE_ORDER = DESCENDING;
			PLUGIN_ORDER = DESCENDING;
		}
		setComparator(fMemento.getInteger(P_ORDER_TYPE).byteValue());
	}

	private void initializeMemento() {

		Integer width = fMemento.getInteger(P_COLUMN_1);
		if (width == null || width.intValue() == 0) {
			fMemento.putInteger(P_COLUMN_1, 300);
		}
		width = fMemento.getInteger(P_COLUMN_2);
		if (width == null || width.intValue() == 0) {
			fMemento.putInteger(P_COLUMN_2, 150);
		}
		width = fMemento.getInteger(P_COLUMN_3);
		if (width == null || width.intValue() == 0) {
			fMemento.putInteger(P_COLUMN_3, 150);
		}

		if (fMemento.getInteger(P_ORDER_VALUE) == null) {
			fMemento.putInteger(P_ORDER_VALUE, DESCENDING);
		}
		if (fMemento.getInteger(P_ORDER_TYPE) == null) {
			fMemento.putInteger(P_ORDER_TYPE, DATE);
		}
	}

	private void initializeViewerSorter() {
		byte orderType = fMemento.getInteger(P_ORDER_TYPE).byteValue();
		ViewerComparator comparator = getViewerComparator(orderType);
		fTreeViewer.setComparator(comparator);
		if (orderType == DATE) {
			setColumnSorting(fColumn3, DATE_ORDER);
		} else if (orderType == MESSAGE) {
			setColumnSorting(fColumn1, MESSAGE_ORDER);
		} else if (orderType == PLUGIN) {
			setColumnSorting(fColumn2, PLUGIN_ORDER);
		}
	}

	private void makeHoverShell() {
		fTextShell = new Shell(fTree.getShell(), SWT.NO_FOCUS | SWT.ON_TOP
				| SWT.TOOL);
		Display display = fTextShell.getDisplay();
		fTextShell.setBackground(display
				.getSystemColor(SWT.COLOR_INFO_BACKGROUND));
		GridLayout layout = new GridLayout(1, false);
		int border = ((fTree.getShell().getStyle() & SWT.NO_TRIM) == 0) ? 0 : 1;
		layout.marginHeight = border;
		layout.marginWidth = border;
		fTextShell.setLayout(layout);
		fTextShell.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		Composite shellComposite = new Composite(fTextShell, SWT.NONE);
		layout = new GridLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		shellComposite.setLayout(layout);
		shellComposite.setLayoutData(new GridData(GridData.FILL_BOTH
				| GridData.VERTICAL_ALIGN_BEGINNING));
		fTextLabel = new Text(shellComposite, SWT.WRAP | SWT.MULTI
				| SWT.READ_ONLY);
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.widthHint = 100;
		gd.grabExcessHorizontalSpace = true;
		fTextLabel.setLayoutData(gd);
		Color c = fTree.getDisplay().getSystemColor(SWT.COLOR_INFO_BACKGROUND);
		fTextLabel.setBackground(c);
		c = fTree.getDisplay().getSystemColor(SWT.COLOR_INFO_FOREGROUND);
		fTextLabel.setForeground(c);
		fTextLabel.setEditable(false);
		fTextShell.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				onTextShellDispose(e);
			}
		});
	}

	void onMouseDown(Event e) {
		if (fTextShell != null && !fTextShell.isDisposed()
				&& !fTextShell.isFocusControl()) {
			fTextShell.setVisible(false);
		}
	}

	void onMouseHover(Event e) {
		
	}

	void onMouseMove(Event e) {
		if (fTextShell != null && !fTextShell.isDisposed()
				&& fTextShell.isVisible()) {
			fTextShell.setVisible(false);
		}

	}

	void onTextShellDispose(DisposeEvent e) {

		setFocus();
	}

	private void readSettings() {
		IEclipsePreferences prefs = getLogPreferences();
		initializeMemento();
		if (prefs == null) {
			return;
		}
		fMemento.putInteger(P_COLUMN_1, prefs.getInt(P_COLUMN_1, 300));
		fMemento.putInteger(P_COLUMN_2, prefs.getInt(P_COLUMN_2, 150));
		fMemento.putInteger(P_COLUMN_3, prefs.getInt(P_COLUMN_3, 150));
		int order = prefs.getInt(P_ORDER_VALUE, DESCENDING);
		fMemento.putInteger(P_ORDER_VALUE, order == 0 ? DESCENDING : order);
		fMemento.putInteger(P_ORDER_TYPE, prefs.getInt(P_ORDER_TYPE, DATE));

		fMemento.putBoolean(P_SHOW_INFO, prefs.getBoolean(P_SHOW_INFO, true));
		fMemento.putBoolean(P_SHOW_WARNING,
				prefs.getBoolean(P_SHOW_WARNING, true));
		fMemento.putBoolean(P_SHOW_ERROR, prefs.getBoolean(P_SHOW_ERROR, true));

	}

	public void refresh(final boolean activate) {
		if (fTree.isDisposed()) {
			return;
		}
		logEntryArray = null;

		if (!fTree.isDisposed()) {
			fTreeViewer.refresh();
			fClearLogAction.setEnabled(fInputFile.exists()
					&& fInputFile.canWrite());
			fOpenLogAction.setEnabled(fInputFile.exists());
			fExportAction.setEnabled(fInputFile.exists());
			// if (activate && fActivateViewAction.isChecked()) {
			// IWorkbenchPage page = UnicoreLogActivator.getActivePage();
			// if (page != null)
			// page.bringToTop(view);
			// }
		}
	}

	protected void reloadLog() {
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				monitor.beginTask("Reload log", IProgressMonitor.UNKNOWN);
				LogActivator.getDefault().reloadLogFile(fInputFile);
			}
		};
		ProgressMonitorDialog pmd = new ProgressMonitorDialog(getViewSite()
				.getShell());
		try {
			pmd.run(true, true, op);
		} catch (InvocationTargetException e) {
		} catch (InterruptedException e) {
		} finally {
			asyncRefresh(false);
		}
	}

	@Override
	public void saveState(IMemento memento) {
		if (this.fMemento == null || memento == null) {
			return;
		}
		this.fMemento.putInteger(P_COLUMN_1, fColumn1.getWidth());
		this.fMemento.putInteger(P_COLUMN_2, fColumn2.getWidth());
		this.fMemento.putInteger(P_COLUMN_3, fColumn3.getWidth());
		memento.putMemento(this.fMemento);
		writeSettings();
	}

	private void setColumnSorting(TreeColumn column, int order) {
		fTree.setSortColumn(column);
		fTree.setSortDirection(order == ASCENDING ? SWT.UP : SWT.DOWN);
	}

	private void setComparator(byte sortType) {
		if (sortType == DATE) {
			fComparator = new Comparator<LogEntry>() {
				public int compare(LogEntry e1, LogEntry e2) {
					long date1 = e1.getDate().getTime();
					long date2 = e2.getDate().getTime();
					if (date1 == date2) {
						int result = getLogList().indexOf(e2)
								- getLogList().indexOf(e1);
						if (DATE_ORDER == DESCENDING) {
							result *= DESCENDING;
						}
						return result;
					}
					if (DATE_ORDER == DESCENDING) {
						return date1 > date2 ? DESCENDING : ASCENDING;
					}
					return date1 < date2 ? DESCENDING : ASCENDING;
				}
			};
		} else if (sortType == PLUGIN) {
			fComparator = new Comparator<LogEntry>() {
				public int compare(LogEntry e1, LogEntry e2) {
					LogEntry entry1 =  e1;
					LogEntry entry2 = e2;
					return getDefaultComparator().compare(entry1.getPluginId(),
							entry2.getPluginId())
							* PLUGIN_ORDER;
				}
			};
		} else {
			fComparator = new Comparator<LogEntry>() {
				public int compare(LogEntry e1, LogEntry e2) {
					LogEntry entry1 = e1;
					LogEntry entry2 = e2;
					return getDefaultComparator().compare(entry1.getMessage(),
							entry2.getMessage())
							* MESSAGE_ORDER;
				}
			};
		}
	}

	
	@Override
	public void setFocus() {
		if (fTree != null && !fTree.isDisposed()) {
			fTree.setFocus();
		}
	}

	private boolean showLogEntry(LogEntry logEntry) {

		int severity = logEntry.getSeverity();
		switch (severity) {
		case IStatus.INFO:
			return !Boolean.FALSE.equals(fMemento
					.getBoolean(LoggingConstants.P_SHOW_INFO));

		case IStatus.WARNING:
			return !Boolean.FALSE.equals(fMemento
					.getBoolean(LoggingConstants.P_SHOW_WARNING));

		case IStatus.ERROR:
			return !Boolean.FALSE.equals(fMemento
					.getBoolean(LoggingConstants.P_SHOW_ERROR));

		default:
			return true;

		}
	}

	public void sortByDateDescending() {
		setColumnSorting(fColumn3, DESCENDING);
	}

	private void updateStatus(ISelection selection) {
		IStatusLineManager status = getViewSite().getActionBars()
				.getStatusLineManager();
		if (selection.isEmpty()) {
			status.setMessage(null);
		} else {
			LogEntry entry = (LogEntry) ((IStructuredSelection) selection)
					.getFirstElement();
			status.setMessage(((LogViewLabelProvider) fTreeViewer
					.getLabelProvider()).getColumnText(entry, 0));
		}
	}

	private void writeFilterSettings() {
		IEclipsePreferences prefs = getLogPreferences();

		prefs.put(P_SHOW_INFO, fMemento.getString(P_SHOW_INFO));
		prefs.put(P_SHOW_WARNING, fMemento.getString(P_SHOW_WARNING));
		prefs.put(P_SHOW_ERROR, fMemento.getString(P_SHOW_ERROR));

	}

	private void writeSettings() {
		writeViewSettings();
		writeFilterSettings();
	}

	private void writeViewSettings() {
		IEclipsePreferences prefs = getLogPreferences();
		prefs.put(P_COLUMN_1, fMemento.getString(P_COLUMN_1));
		prefs.put(P_COLUMN_2, fMemento.getString(P_COLUMN_2));
		prefs.put(P_COLUMN_3, fMemento.getString(P_COLUMN_3));
		int order = fMemento.getInteger(P_ORDER_VALUE).intValue();
		prefs.putInt(P_ORDER_VALUE, order);
		prefs.putInt(P_ORDER_TYPE, fMemento.getInteger(P_ORDER_TYPE).intValue());
	}
}
