package de.fzj.unicore.rcp.logmonitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EventQueue implements IEventQueue {

	private List<Event> list = new ArrayList<Event>();
	private Set<String> keys = new HashSet<String>();
	private int index = 0;

	private List<IEventQueueListener> listeners = new ArrayList<IEventQueueListener>();

	public void addEvent(Event record) {
		if(record == null) return;
		list.add(record);
		keys.add(record.getId());
		for (IEventQueueListener l : listeners.toArray(new IEventQueueListener[listeners
				.size()])) {
			l.eventAdded(record);
		}
	}

	public void addEventListener(IEventQueueListener l) {
		if (!listeners.contains(l)) {
			listeners.add(l);
		}
	}

	public void clear() {
		list.clear();
		keys.clear();
		index = 0;
		notifyCurrentEventChanged();
	}

	public Event getCurrentEvent() {
		if (index >= 0 && index < list.size()) {
			return list.get(index);
		} else {
			return null;
		}
	}

	public int getCurrentIndex() {
		return index;
	}

	public boolean hasNextEvent() {

		return index < list.size() - 1;
	}

	public boolean hasPreviousEvent() {
		return index > 0;
	}

	public Event removeCurrentEvent() {
		Event result = list.remove(index);
		keys.remove(result.getId());
		int oldIndex = index;
		index = Math.max(0, index - 1);
		for (IEventQueueListener l : listeners.toArray(new IEventQueueListener[listeners
				.size()])) {
			l.eventRemoved(result);
		}
		if(oldIndex != index) notifyCurrentEventChanged();
		return result;
	}

	public void removeEventListener(IEventQueueListener l) {
		listeners.remove(l);

	}

	public void toNextEvent() {
		if (hasNextEvent()) {
			index++;
			notifyCurrentEventChanged();
		}
	}

	public void toPreviousEvent() {
		if (hasPreviousEvent()) {
			index--;
			notifyCurrentEventChanged();
		}
	}

	public boolean containsEvent(Event e) {
		if(e == null) return false;
		return keys.contains(e.getId());
	}

	public void jumpToEvent(Event e) {
		int oldIndex = index;
		if(e == null) return;
		int index = list.indexOf(e);
		if(index >= 0) 
		{
			
			this.index = index;
			if(index != oldIndex)
			{
				notifyCurrentEventChanged();
			}
		}
	}
	
	private void notifyCurrentEventChanged()
	{
		Event evt = getCurrentEvent();
		for (IEventQueueListener l : listeners.toArray(new IEventQueueListener[listeners
		                                                       				.size()])) {
		                                                       			l.currentEventChanged(evt);
		                                                       		}
	}

	public void sortEvents(Comparator<Event> cmp) {
		Event oldSelected = getCurrentEvent();
		Collections.sort(list, cmp);
		jumpToEvent(oldSelected);
	}

}
