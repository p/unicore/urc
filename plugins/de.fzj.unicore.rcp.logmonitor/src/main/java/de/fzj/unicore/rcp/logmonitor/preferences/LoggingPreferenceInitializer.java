package de.fzj.unicore.rcp.logmonitor.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;

import de.fzj.unicore.rcp.logmonitor.LogActivator;
import de.fzj.unicore.rcp.logmonitor.LoggingConstants;

/**
 * Class used to initialize default preference values.
 */
public class LoggingPreferenceInitializer extends AbstractPreferenceInitializer
		implements LoggingConstants {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IEclipsePreferences prefs = LogActivator.getDefault().getPreferences();
		prefs.putBoolean(P_LOG_ERROR, true);
		prefs.putBoolean(P_LOG_WARNING, true);
		prefs.putBoolean(P_LOG_INFO, true);

		prefs.putBoolean(P_POPUP_ERROR, true);
		prefs.putBoolean(P_POPUP_WARNING, true);
		prefs.putBoolean(P_POPUP_INFO, false);

		prefs.putBoolean(P_SHOW_WARNING, true);
		prefs.putBoolean(P_SHOW_ERROR, true);
		prefs.putBoolean(P_SHOW_ALL_SESSIONS, false);

		prefs.putBoolean(P_USE_LIMIT, true);

		prefs.putInt(P_SHOW_LOG_LIMIT, 50);
		prefs.putBoolean(P_SHOW_INFO, true);

	}

}
