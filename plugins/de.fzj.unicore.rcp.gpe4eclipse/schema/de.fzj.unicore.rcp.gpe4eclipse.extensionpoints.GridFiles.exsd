<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="de.fzj.unicore.rcp.gpe4eclipse" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appInfo>
         <meta.schema plugin="de.fzj.unicore.rcp.gpe4eclipse" id="de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.GridFiles" name="GridFiles"/>
      </appInfo>
      <documentation>
         Through this extension point, new file transmitters and file resolvers can be injected into GPE for supporting additional source and target types for file transfers.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appInfo>
            <meta.element />
         </appInfo>
      </annotation>
      <complexType>
         <sequence>
            <element ref="transmitter" minOccurs="0" maxOccurs="unbounded"/>
            <element ref="resolver" minOccurs="0" maxOccurs="unbounded"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="transmitter">
      <annotation>
         <documentation>
            A transmitter implements the actual logic needed for files to be transferred to/from the Grid.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":com.intel.gpe.clients.api.transfers.FileTransmitter"/>
               </appInfo>
            </annotation>
         </attribute>
         <attribute name="sourceNamespace" type="string" use="required">
            <annotation>
               <documentation>
                  The namespace of the protocol from which files can be transferred.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="sourceLocalPart" type="string" use="required">
            <annotation>
               <documentation>
                  The localpart of the protocol from which files can be transferred.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="targetNamespace" type="string" use="required">
            <annotation>
               <documentation>
                  The namespace of the protocol to which files can be transferred.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="targetLocalPart" type="string" use="required">
            <annotation>
               <documentation>
                  The localpart of the protocol to which files can be transferred.
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="resolver">
      <annotation>
         <documentation>
            A file resolver is used to resolve wildcards in Grid file paths right before file transfers are started.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":com.intel.gpe.clients.api.transfers.FileResolver"/>
               </appInfo>
            </annotation>
         </attribute>
         <attribute name="namespace" type="string" use="required">
            <annotation>
               <documentation>
                  The namespace of the protocol for which files can be resolved.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="localPart" type="string">
            <annotation>
               <documentation>
                  The localpart of the protocol for which files can be resolved.
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="apiInfo"/>
      </appInfo>
      <documentation>
         [Enter API information here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         [Enter the first release in which this extension point appears.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         [Enter extension point usage example here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         [Enter information about supplied implementation of this extension point.]
      </documentation>
   </annotation>


</schema>
