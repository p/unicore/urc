/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.gridbeans.IGridBean;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IEnvVariableSourceTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.QNameComboBoxCellEditor;

/**
 * @author demuth
 * 
 */
public class VariableSourceTypeSelectionCellEditor extends
		QNameComboBoxCellEditor {
	protected IGridBean gridBean;
	protected IAdaptable adaptable;
	protected EnvVariable variable;

	public VariableSourceTypeSelectionCellEditor(Composite parent,
			IGridBean gridBean, IAdaptable adaptable) {
		super(parent);
		this.gridBean = gridBean;
		this.adaptable = adaptable;

	}

	@Override
	protected Object doGetValue() {
		QName oldType = null;
		try {
			oldType = variable.getValue().getVariableValue().getProtocol()
					.getName();
			IEnvVariableSourceTypeExtensionPoint oldExtension = getRegistry()
					.getDefiningExtension(oldType);
			oldExtension.detachFromParameterValue(adaptable, variable);
		} catch (Exception e) {

		}

		QName type = (QName) super.doGetValue();
		IEnvVariableSourceTypeExtensionPoint extension = getRegistry()
				.getDefiningExtension(type);
		extension.attachToParameterValue(adaptable, variable);
		return variable;

	}

	@Override
	protected void doSetValue(Object value) {
		Assert.isTrue(value instanceof EnvVariable);
		variable = (EnvVariable) value;
		List<QName> types = getRegistry().getAllTypes();
		List<QName> availableTypes = new ArrayList<QName>();
		for (QName type : types) {
			IEnvVariableSourceTypeExtensionPoint extension = getRegistry()
					.getDefiningExtension(type);
			GridBeanContext context = (GridBeanContext) gridBean
					.get(GPE4EclipseConstants.CONTEXT);
			if (extension != null && extension.availableInContext(context)) {
				availableTypes.add(type);
			}
		}
		setQNames(availableTypes.toArray(new QName[0]));
		QName type = null;
		try {

			type = variable.getValue().getVariableValue().getProtocol()
					.getName();
		} catch (Exception e) {
			if (availableTypes.size() > 0) {
				type = availableTypes.get(0);
			}
		}
		if (type != null) {
			super.doSetValue(type);
		}
	}

	protected VariableSourceTypeRegistry getRegistry() {
		return GPEActivator.getDefault().getVariableSourceTypeRegistry();
	}

}
