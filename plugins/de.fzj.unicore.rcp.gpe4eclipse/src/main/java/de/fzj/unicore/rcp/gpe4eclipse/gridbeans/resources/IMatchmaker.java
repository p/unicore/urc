package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import com.intel.gpe.clients.api.Job;

public interface IMatchmaker {

	/**
	 * Test whether the represented resource can deal with the given job. Return
	 * a String that describes the shortcomings of the resource in case the
	 * resource does not meet all of the job's requirements.
	 * 
	 * @param j
	 * @return
	 */
	public String canHandleJob(Job j);

}
