/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.clientstubs;

import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.client.impl.security.SecurityProviderImpl;
import com.intel.gpe.clients.api.WSLTClient;
import com.intel.gpe.clients.api.cache.GPEClientProperties;
import com.intel.gpe.clients.impl.WSLTClientImpl;
import com.intel.gpe.clients.impl.WSRFClientImpl;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.SWTSecurityProvider;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * @author demuth
 * 
 */
public class WSRFClientFactory implements IWSRFClientFactory {

	SWTSecurityProvider secProvider = new SWTSecurityProvider();

	public WSRFClientFactory() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.utils.IWSRFClientFactory#createWSLTClient
	 * (org.w3.x2005.x08.addressing.EndpointReferenceType,
	 * de.fzj.unicore.uas.security.IUASSecurityProperties)
	 */
	public WSLTClient createWSLTClient(EndpointReferenceType epr,
			IClientConfiguration secProps) {
		return new WSLTClientImpl(createWSRFClient(epr, secProps));
	}

	public WSRFClientImpl createWSRFClient(EndpointReferenceType epr) {
		GPEClientProperties props = GPEActivator.getDefault().getClient()
				.getGPEClientProperties();
		return new WSRFClientImpl(epr, secProvider, props);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.utils.IWSRFClientFactory#createWSRFClient
	 * (org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public WSRFClientImpl createWSRFClient(EndpointReferenceType epr,
			IClientConfiguration secProps) {

		SecurityProviderImpl securityProvider = new SecurityProviderImpl();
		securityProvider.init(secProps);
		GPEClientProperties props = GPEActivator.getDefault().getClient()
				.getGPEClientProperties();
		return new WSRFClientImpl(epr, securityProvider, props);
	}

	public WSRFClientImpl createWSRFClient(NodePath path) {
		SecurityProviderImpl securityProvider = new SecurityProviderImpl();
		Node n = null;
		// there are situations when NodeFactory.revealNode returns null, e.g.
		// when submitting jobs in quick succession. Slightly delayed, repeated
		// attempts can mitigate the problem
		// see bug #11 - https://sourceforge.net/p/unicore/urc-issues/11/
		int attempt = 0;
		do {
			n = NodeFactory.revealNode(path, null);
			if(n == null) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
		} while (attempt++ < 3 && n == null);
		if (n != null && n instanceof SecuredNode) {
			try{
			IClientConfiguration secProps = ((SecuredNode) n)
					.getUASSecProps();
			securityProvider.init(secProps);
			GPEClientProperties props = GPEActivator.getDefault().getClient()
					.getGPEClientProperties();
			return new WSRFClientImpl(n.getEpr(), securityProvider, props);
			}catch(Exception ex){
				throw new RuntimeException("Unable to create client stub for service "
						+ path.last(),ex);
			}
		} else {
			throw new RuntimeException(
					"Unable to create client stub for service " + path.last()
							+ ": service could not be found on the Grid");
		}
	}

}
