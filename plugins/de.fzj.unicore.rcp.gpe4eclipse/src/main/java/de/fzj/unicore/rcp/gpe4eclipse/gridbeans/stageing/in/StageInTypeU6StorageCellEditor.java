/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.util.URIUtil;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.ControlContribution;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.unigrids.services.atomic.types.GridFileType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.clients.impl.transfers.U6ProtocolConstants;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;

import de.fzj.unicore.rcp.common.utils.StringUtils;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.servicebrowser.filters.ContentFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.FileFilterCriteria;
import de.fzj.unicore.rcp.servicebrowser.filters.SkipNodeViewerFilter;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceSelectionDialog;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceTreeViewer;

/**
 * @author demuth
 * 
 */
public class StageInTypeU6StorageCellEditor extends DialogCellEditor implements ISelectionStatusValidator {

	protected static final int MODE_NORMAL = 0, MODE_SELECT_ALL_IN_DIR = 1, MODE_SELECT_FILTERED = 2, MODE_FILTERING = 4, MODE_SELECTED_DIR = 8;

	protected IFileParameterValue file;
	protected boolean multiFileSelector = false;
	protected FileFilterCriteria filter;
	protected ContentFilter contentFilter;
	protected Button checkBoxUseRegexp, checkBoxAllInDir, checkBoxSelectFiltered;
	protected int mode;
	protected ServiceSelectionDialog serviceSelectionDialog;

	protected Object[] dirChildren;
	protected Object[] filtered;


	/**
	 * The label that gets reused by <code>updateLabel</code>.
	 */
	private Label defaultLabel;

	/** A CellEditor, opening a FileBrowser for selecting a File */
	public StageInTypeU6StorageCellEditor(Composite parent) {
		this(parent, false);
	}

	public StageInTypeU6StorageCellEditor(Composite parent,
			boolean multiFileSelector) {
		super(parent);
		this.multiFileSelector = multiFileSelector;
	}

	@Override
	protected Control createContents(Composite cell) {
		defaultLabel = new Label(cell, SWT.LEFT);
		defaultLabel.setFont(cell.getFont());
		defaultLabel.setBackground(cell.getBackground());
		return defaultLabel;
	}

	@Override
	protected void doSetValue(Object value) {

		if (!(value instanceof IFileParameterValue[])) {
			IFileParameterValue file = (IFileParameterValue) value;
			value = new IFileParameterValue[] { file };
			super.doSetValue(value);
			updateContents(file.getSource().getInternalString());
		} else {
			super.doSetValue(value);
		}
	}

	public boolean isMultiFileSelector() {
		return multiFileSelector;
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		AbstractFileNode fileNode = null;
		serviceSelectionDialog = new ServiceSelectionDialog();
		try {
			file = ((IFileParameterValue[]) this.getValue())[0];

			serviceSelectionDialog.setTitle("Please select a file.");
			serviceSelectionDialog.setEmptyListMessage("No file selected.");
			serviceSelectionDialog.setDoubleClickSelects(false);
			serviceSelectionDialog.setAllowMultiple(multiFileSelector);
			serviceSelectionDialog.setValidator(this);
			serviceSelectionDialog.setExpandToLevel(2);

			contributeToUpperToolbar();
			contributeToLowerToolbar();

			serviceSelectionDialog.create();
			serviceSelectionDialog.getTreeViewer().addSelectionChangedListener(new ISelectionChangedListener() {

				@Override
				public void selectionChanged(SelectionChangedEvent event) {
					ServiceTreeViewer treeViewer = serviceSelectionDialog.getTreeViewer();
					IStructuredSelection selection = (IStructuredSelection) event.getSelection();
					Node firstSelected = (Node) selection.getFirstElement();
					boolean selectedDir = selection.size() == 1 && (FolderNode.TYPE
							.equals(firstSelected.getType()) ||StorageNode.TYPE
							.equals(firstSelected.getType()));
					if(selectedDir != inMode(MODE_SELECTED_DIR))
					{
						toggleMode(MODE_SELECTED_DIR);
					}

					if(selectedDir) 
					{
						dirChildren = ((IStructuredContentProvider) treeViewer.getContentProvider()).getElements(firstSelected);
					}
					else
					{
						dirChildren = null;
					}

					// make sure we check the controls (toggling normal doesn't
					// have any other effects)
					toggleMode(MODE_NORMAL);

				}
			});

			// make sure we check the controls (toggling normal doesn't
			// have any other effects)
			toggleMode(MODE_NORMAL);
			serviceSelectionDialog.open();

			Object[] selected = null; 
			if(inMode(MODE_SELECT_FILTERED))
			{
				selected = filtered;
			}
			else 
			{
				selected = serviceSelectionDialog.getResult();
			}

			if (selected == null || selected.length == 0) {
				return null;
			}

			List<IFileParameterValue> result = new ArrayList<IFileParameterValue>(
					selected.length);
			for (int i = 0; i < selected.length; i++) {
				if(!(selected[i] instanceof AbstractFileNode)) continue;
				fileNode = (AbstractFileNode) selected[i];
				IFileParameterValue current = file.clone();
				EndpointReferenceType epr = fileNode.getStorageEPR();
				if(epr == null) continue;
				String parentDir = epr.getAddress()
				.getStringValue()
				+ "#";
				current.getSource().setParentDirAddress(parentDir);
				GridFileType gft = fileNode.getGridFileType();
				if (gft == null) {
					continue;
				}
				String relative = gft.getPath();
				// escape special characters in filenames!
				relative = URIUtil.encode(relative,
						org.apache.commons.httpclient.URI.allowed_fragment); 
				if(inMode(MODE_SELECT_ALL_IN_DIR))
				{
					List<String> childNames = new ArrayList<String>();
					for(Object o : dirChildren)
					{
						if(!(o instanceof Node)) continue;
						childNames.add(((Node) o).getName());
					}
					String[] names = childNames.toArray(new String[childNames.size()]);

					if(names.length <= 1)
					{
						relative += "/*";
					}
					else
					{
						String longestPrefix = StringUtils.longestCommonPrefix(names);
						if(longestPrefix.length()>0)
						{
							relative += "/"+longestPrefix+"*";

						}
						else
						{
							String longestSuffix = StringUtils.longestCommonSuffix(names);
							if(longestSuffix.length()>0)
							{
								relative += "/*"+longestSuffix;
							}
							else
							{
								relative += "/*";
							}
						}
					}
				}
				current.getSource().setRelativePath(relative);
				String displayed = fileNode.getStorageName() + relative;
				current.getSource().setDisplayedString(displayed);
				boolean usingWildcards = current.getSource().getRelativePath()
				.contains("*");
				current.getSource().setUsingWildcards(usingWildcards);
				current.getSource().setProtocol(
						U6ProtocolConstants.UNICORE_SMS_PROTOCOL);
				String nameInWorkingDir = current.getTarget().getRelativePath();
				if (selected.length > 1 || nameInWorkingDir == null
						|| nameInWorkingDir.trim().length() == 0) {
					int cut = relative.lastIndexOf("/");
					String fileName = relative.substring(cut + 1);
					current.getTarget().setRelativePath(fileName);
					current.getTarget().setDisplayedString(fileName);
				}

				if (selected.length > 0) {
					// use a different UUID for the newly created file transfer
					String uuid = UUID.randomUUID().toString();
					current.setUniqueId(uuid);
				}
				result.add(current);
			}
			return result.toArray(new IFileParameterValue[result.size()]);
		} catch (Exception e) {
			GPEActivator.log(IStatus.WARNING,
					"Could not apply value to table entry", e);
			return file;
		} finally {
			serviceSelectionDialog.disposeNodes();
		}
	}

	protected void contributeToUpperToolbar()
	{
		ToolBarManager upperToolbarManager = serviceSelectionDialog.getUpperToolBarManager();
		filter = new FileFilterCriteria("",FileFilterCriteria.FILES_AND_FOLDERS);
		filter.setUseRegexp(false);
		contentFilter = new SkipNodeViewerFilter("file filter") {

			@Override
			public boolean select(Viewer viewer, Object parentElement, Object element) {
				if(!(element instanceof Node)) return false;
				Node n = (Node) element;

				if(!(n instanceof AbstractFileNode))
				{
					return (n instanceof GridNode) || "".equals(filter.getPattern());
				}
				return filter.fitCriteria(n);
			}

		};

		serviceSelectionDialog.addFilter(contentFilter);

		upperToolbarManager.add(new ControlContribution("filterText") {

			@Override
			protected Control createControl(Composite parent) {
				Composite c = new Composite(parent, SWT.NONE);

				GridLayout layout = new GridLayout(3,false);

				c.setLayout(layout);
				Label label = new Label(c, SWT.NONE);
				label.setText("File name filter:" );
				label.setLayoutData(new GridData(GridData.BEGINNING,GridData.CENTER,false,false));
				final Text filterText = new Text(c, SWT.BORDER);
				GridData textData = new GridData(GridData.BEGINNING,GridData.CENTER,true,false);
				textData.minimumWidth = 250;
				filterText.setLayoutData(textData);
				filterText.addModifyListener(new ModifyListener() {

					public void modifyText(ModifyEvent e) {
						String pattern = filterText.getText();
						if(filter.isUseRegexp())
						{
							// do some validation
							try {
								Pattern.compile(pattern);
								filter.setPattern(pattern);
								contentFilter.filterChanged();
							} catch (Exception ex) {
								// match nothing
								filter.setPattern(UUID.randomUUID().toString());
								contentFilter.filterChanged();
							}
						}
						else
						{
							filter.setPattern(pattern);
							contentFilter.filterChanged();
						}
						if((pattern.trim().length() != 0) != inMode(MODE_FILTERING))
						{
							toggleMode(MODE_FILTERING);
						}

					}
				});
				checkBoxUseRegexp = new Button(c,SWT.CHECK);
				checkBoxUseRegexp.setText("Regular expression");
				checkBoxUseRegexp.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						boolean selected = checkBoxUseRegexp.getSelection();
						filter.setUseRegexp(selected);
						contentFilter.filterChanged();
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
						widgetSelected(e);
					}
				});

				ToolItem toolItem = new ToolItem((ToolBar)parent, SWT.NONE);
				toolItem.setControl(c);
				return c;
			}
		});
	}


	protected void toggleMode(int mode)
	{


		this.mode ^= mode;

		filtered = null;
		checkBoxSelectFiltered.setEnabled(true);
		checkBoxAllInDir.setEnabled(true);

		if(!inMode(MODE_FILTERING))
		{
			this.mode &= ~MODE_SELECT_FILTERED;
			checkBoxSelectFiltered.setEnabled(false);
		}

		if(!inMode(MODE_SELECTED_DIR) || dirChildren == null )
		{
			this.mode &= ~MODE_SELECT_ALL_IN_DIR;
			checkBoxAllInDir.setEnabled(false);
		}

		if(mode == MODE_SELECT_FILTERED && inMode(MODE_SELECT_FILTERED))
		{
			// just switched to this mode
			this.mode &= ~MODE_SELECT_ALL_IN_DIR;
			ServiceTreeViewer treeViewer = serviceSelectionDialog.getTreeViewer(); 
			filtered = treeViewer.getAllFilteredChildren(treeViewer.getGridNode());
		}
		if(mode == MODE_SELECT_ALL_IN_DIR && inMode(MODE_SELECT_ALL_IN_DIR))
		{
			// just switched to this mode
			this.mode &= ~MODE_SELECT_FILTERED;
		}


		checkBoxAllInDir.setSelection(inMode(MODE_SELECT_ALL_IN_DIR));
		checkBoxSelectFiltered.setSelection(inMode(MODE_SELECT_FILTERED));

	}

	protected boolean inMode(int mode)
	{
		if(mode == 0) return this.mode == 0;
		return (this.mode & mode) == mode;
	}

	protected void contributeToLowerToolbar()
	{

		ToolBarManager lowerToolbarManager = serviceSelectionDialog.getLowerToolBarManager();
		lowerToolbarManager.add(new ControlContribution("filesInDir") {

			@Override
			protected Control createControl(Composite parent) {
				checkBoxAllInDir = new Button(parent, SWT.CHECK);
				checkBoxAllInDir.setText("All files in selected folder");
				checkBoxAllInDir.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						toggleMode(MODE_SELECT_ALL_IN_DIR);
						serviceSelectionDialog.updateOKStatus();
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
						widgetSelected(e);

					}
				});
				ToolItem toolItem = new ToolItem((ToolBar)parent, SWT.NONE);
				toolItem.setControl(checkBoxAllInDir);
				return checkBoxAllInDir;
			}
		});

		lowerToolbarManager.add(new ControlContribution("selectFiltered") {

			@Override
			protected Control createControl(Composite parent) {
				checkBoxSelectFiltered = new Button(parent, SWT.CHECK);
				checkBoxSelectFiltered.setText("All files that match filter");
				checkBoxSelectFiltered.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						toggleMode(MODE_SELECT_FILTERED);
						serviceSelectionDialog.updateOKStatus();
					}

					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
						widgetSelected(e);

					}
				});
				ToolItem toolItem = new ToolItem((ToolBar)parent, SWT.NONE);
				toolItem.setControl(checkBoxSelectFiltered);
				return checkBoxSelectFiltered;
			}
		});
	}

	public void setMultiFileSelector(boolean filesetsAllowed) {
		this.multiFileSelector = filesetsAllowed;
	}

	@Override
	protected void updateContents(Object value) {
		if (defaultLabel == null) {
			return;
		}

		String text = "";//$NON-NLS-1$
		if (value != null) {
			text = ((IFileParameterValue[]) getValue())[0].getSource()
			.getDisplayedString();
		}
		defaultLabel.setText(text);
	}

	@Override
	public IStatus validate(Object[] selection) {
		if(inMode(MODE_SELECT_FILTERED) && filtered != null && filtered.length > 0)
		{
			return new Status(IStatus.OK, PlatformUI.PLUGIN_ID,IStatus.OK, "", null);
		}

		if (selection != null) {
			if (selection.length == 1
					&& (selection[0] instanceof AbstractFileNode)) {
				if (selection[0] instanceof FileNode || isMultiFileSelector()) {
					return new Status(IStatus.OK, PlatformUI.PLUGIN_ID,
							IStatus.OK, "", null);
				}
			} else if (selection.length > 1) {
				for (int i = 0; i < selection.length; i++) {
					if (!(selection[i] instanceof FileNode)
							&& !(selection[i] instanceof FolderNode)) {
						new Status(
								IStatus.ERROR,
								PlatformUI.PLUGIN_ID,
								IStatus.ERROR,
								"No suitable set of files and folders selected.",
								null);
					}
				}
				return new Status(IStatus.OK, PlatformUI.PLUGIN_ID, IStatus.OK,
						"", null);
			}
		}

		return new Status(IStatus.ERROR, PlatformUI.PLUGIN_ID, IStatus.ERROR,
				"No suitable file selected.", null);
	}

}
