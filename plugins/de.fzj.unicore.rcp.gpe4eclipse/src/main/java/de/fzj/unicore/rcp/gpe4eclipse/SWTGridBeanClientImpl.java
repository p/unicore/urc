/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.ui.IWorkbenchPart;

import com.intel.gpe.clients.all.ClientAdapter;
import com.intel.gpe.clients.all.exceptions.GPEGridBeanPluginUnavailableException;
import com.intel.gpe.clients.all.gridbeans.BaseGridBeanClient;
import com.intel.gpe.clients.all.gridbeans.InternalGridBean;
import com.intel.gpe.clients.all.providers.OutcomeProvider;
import com.intel.gpe.clients.all.utils.GPEFileFactoryImpl;
import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.security.GPESecurityManager;
import com.intel.gpe.clients.api.transfers.FileProvider;
import com.intel.gpe.clients.api.transfers.GPEFileFactory;
import com.intel.gpe.gridbeans.ErrorSet;
import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.gridbeans.JobError;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileSetParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.plugins.IGridBeanPlugin;
import com.intel.gpe.util.defaults.preferences.IPreferences;
import com.intel.gpe.util.swing.controls.configurable.IConfigurable;

import de.fzj.unicore.rcp.common.utils.swing.SafeSwingUtilities;
import de.fzj.unicore.rcp.gpe4eclipse.clientstubs.ModelBasedSelectionClient;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in.StageInTypeRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out.StageOutTypeRegistry;

/**
 * @author demuth
 * 
 */
public class SWTGridBeanClientImpl extends BaseGridBeanClient<IWorkbenchPart> {

	private boolean wrappingSwingComponents = false;

	private IGridBeanPlugin<IWorkbenchPart> gridBeanPlugin;

	private Exception occurredException;

	public SWTGridBeanClientImpl(GPESecurityManager securityManager,
			ClassLoader classLoader, OutcomeProvider outcomeProvider,
			ClientAdapter client, FileProvider fileProvider,
			IPreferences userDefaults, IConfigurable parent) throws Exception {
		super(securityManager, classLoader, outcomeProvider, null,
				fileProvider, userDefaults, parent);

		ModelBasedSelectionClient selectionClient = new ModelBasedSelectionClient(
				this);

		// each Grid Bean is presented a slightly different Client object, e.g.
		// each of them has
		// its own selection client for determining which target resource has
		// been selected.
		ClientAdapter newClient = client.clone();
		newClient.setSelectionClient(selectionClient);
		GPEFileFactory fileFactory = new GPEFileFactoryImpl();
		fileFactory.setTemporaryDirName(client.getFileFactory()
				.getTemporaryDirName());
		newClient.setFileFactory(fileFactory);

		this.setClient(newClient);
	}

	private void checkFileValue(IGridBeanParameter param,
			IFileParameterValue value, ErrorSet errors) {
		IGridBeanModel model = getGridBeanJob().getModel();
		GridBeanContext context = (GridBeanContext) model
				.get(GPE4EclipseConstants.CONTEXT);
		if (context == null) {
			return;
		}
		if (param.isInputParameter()) {
			GPEActivator activator = GPEActivator.getDefault();
			if (activator == null) {
				return;
			}
			StageInTypeRegistry stageInRegistry = activator
					.getStageInTypeRegistry();
			if (value != null && value.getSource() != null
					&& value.getSource().getProtocol() != null) {
				QName protocol = value.getSource().getProtocol().getName();
				IStageTypeExtensionPoint ext = stageInRegistry
						.getDefiningExtension(protocol);
				if (ext != null && !ext.availableInContext(context)) {
					errors.addError(new JobError("The file source type "
							+ protocol.getLocalPart()
							+ " is not available for this job. "
							+ "Please revise the value of parameter "
							+ param.getDisplayedName() + "\n"));
				}
			} else {
				errors.addError(new JobError("Invalid file in parameter "
						+ param.getDisplayedName() + "\n"));
			}
		} else {
			if (value != null && value.getTarget() != null
					&& value.getTarget().getProtocol() != null) {
				QName protocol = value.getTarget().getProtocol().getName();
				GPEActivator activator = GPEActivator.getDefault();
				if (activator != null) {
					StageOutTypeRegistry stageOutRegistry = activator
							.getStageOutTypeRegistry();
					IStageTypeExtensionPoint ext = stageOutRegistry
							.getDefiningExtension(protocol);
					if (ext != null && !ext.availableInContext(context)) {
						errors.addError(new JobError("The file target type "
								+ protocol.getLocalPart()
								+ " is not available for this job. "
								+ "Please revise the value of parameter "
								+ param.getDisplayedName() + "\n"));
					}
				}
				// skip if the bundle has already shut down in order to avoid
				// NPE
			} else {
				errors.addError(new JobError("Invalid file in parameter "
						+ param.getDisplayedName() + "\n"));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.common.utils.BaseGridBeanClient#
	 * createGridBeanInputPanel()
	 */
	@Override
	protected void createGridBeanInputPanel() throws Exception {
		inputPanel = new SWTGridBeanInputPanel(this,
				isWrappingSwingComponents());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.common.utils.BaseGridBeanClient#
	 * createGridBeanOutputPanel()
	 */
	@Override
	protected void createGridBeanOutputPanel() throws Exception {

		outputPanel = new SWTGridBeanOutputPanel(this, true,
				isWrappingSwingComponents());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.common.utils.BaseGridBeanClient#createGridBeanPlugin
	 * (com.intel.gpe.client2.gridbeans.GridBean)
	 */
	@Override
	protected IGridBeanPlugin<IWorkbenchPart> createGridBeanPlugin(InternalGridBean gridBean)
			throws Exception {
		try {
			return getInternalGridBean()
					.getPluginInstance(
							GPE4EclipseConstants.SWT_PLUGIN,
							getGridBeanJobWrapper().getGridBeanJob().getModel(),
							client);
		} catch (GPEGridBeanPluginUnavailableException e) {
			setWrappingSwingComponents(true);
			occurredException = null;
			gridBeanPlugin = null;
			// do this in the Swing thread in order to avoid deadlocks,
			// as Swing has the habit of locking on components
			SafeSwingUtilities.invokeAndWait(new Runnable() {
				public void run() {
					try {
						gridBeanPlugin = getInternalGridBean()
								.getPluginInstance(
										InternalGridBean.SWING_PLUGIN,
										getGridBeanJobWrapper()
												.getGridBeanJob().getModel(),
										client);
					} catch (Exception e) {
						occurredException = e;
					}
				}
			});
			if (occurredException != null) {
				throw occurredException;
			}
			return gridBeanPlugin;
		}

	}

	@Override
	public void finalize() throws Throwable {
		super.finalize();
	}

	@Override
	public SWTGridBeanInputPanel getGridBeanInputPanel() {
		return (SWTGridBeanInputPanel) inputPanel;
	}

	public SWTGridBeanOutputPanel getGridBeanOutputPanel() {
		return (SWTGridBeanOutputPanel) inputPanel;
	}

	public boolean isWrappingSwingComponents() {
		return wrappingSwingComponents;
	}

	public void setWrappingSwingComponents(boolean wrappingSwingComponents) {
		this.wrappingSwingComponents = wrappingSwingComponents;
	}
	
	@Override
	public void setClient(Client client) {
		super.setClient(client);
		gridBeanProvider = new SWTGridBeanProvider(userDefaults, securityManager,
		client, classLoader, client);
	}

	@Override
	public ErrorSet validateJob() {
		ErrorSet errors = super.validateJob();

		// in addition to normal GPE validation, also validate the availability
		// of source and target types of files
		// in this context (e.g. workflow files are not valid in single job
		// mode)
		// this is necessary due to the "restore job" function that allows to
		// restore a workflow job as a single job
		IGridBeanModel model = getGridBeanJob().getModel();

		List<IGridBeanParameter> inputParams = new ArrayList<IGridBeanParameter>(
				model.getInputParameters());
		inputParams.addAll(model.getOutputParameters());
		for (IGridBeanParameter param : inputParams) {
			if (GridBeanParameterType.FILE.equals(param.getType())) {
				IFileParameterValue value = (IFileParameterValue) model
						.get(param.getName());
				checkFileValue(param, value, errors);
			} else if (GridBeanParameterType.FILE_SET.equals(param.getType())) {
				IFileSetParameterValue value = (IFileSetParameterValue) model
						.get(param.getName());
				for (IFileParameterValue file : value.getFiles()) {
					checkFileValue(param, file, errors);
				}
			}
		}
		return errors;
	}

}
