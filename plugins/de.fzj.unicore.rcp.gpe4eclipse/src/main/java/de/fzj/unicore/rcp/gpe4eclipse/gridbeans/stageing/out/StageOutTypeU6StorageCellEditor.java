/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out;

import org.apache.commons.httpclient.util.URIUtil;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.unigrids.services.atomic.types.GridFileType;

import com.intel.gpe.clients.impl.transfers.U6ProtocolConstants;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceSelectionDialog;

/**
 * @author demuth
 * 
 */
public class StageOutTypeU6StorageCellEditor extends DialogCellEditor {

	protected IFileParameterValue file;

	/**
	 * The label that gets reused by <code>updateLabel</code>.
	 */
	private Label defaultLabel;

	/** A CellEditor, opening a FileBrowser for selecting a File */
	public StageOutTypeU6StorageCellEditor(Composite parent) {
		super(parent);
	}

	@Override
	protected Control createContents(Composite cell) {
		defaultLabel = new Label(cell, SWT.LEFT);
		defaultLabel.setFont(cell.getFont());
		defaultLabel.setBackground(cell.getBackground());
		return defaultLabel;
	}

	@Override
	protected void doSetValue(Object value) {
		IFileParameterValue file = (IFileParameterValue) value;
		super.doSetValue(value);
		updateContents(file.getSource().getInternalString());
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		AbstractFileNode fileNode = null;

		ServiceSelectionDialog serviceSelectionDialog = new ServiceSelectionDialog();
		try {
			file = (IFileParameterValue) this.getValue();
			IFileParameterValue result;
			try {
				result = file.clone();
			} catch (Exception e) {
				result = file;
			}
			serviceSelectionDialog.setTitle("Please select a file.");
			serviceSelectionDialog
					.setEmptyListMessage("No storage or directory selected.");
			serviceSelectionDialog.setDoubleClickSelects(false);
			serviceSelectionDialog.setAllowMultiple(false);
			serviceSelectionDialog
					.setValidator(new U6OutputFileSelectionValidator());
			serviceSelectionDialog.setExpandToLevel(2);
			serviceSelectionDialog.open();

			Object o = serviceSelectionDialog.getFirstResult();
			String relative = result.getSource().getRelativePath();
			relative = URIUtil.encode(relative,
					org.apache.commons.httpclient.URI.allowed_fragment); // escape
																			// special
																			// characters
																			// in
																			// filenames!
			String parentDir = null;
			String displayed = null;
			if (o instanceof StorageNode) {
				StorageNode storageNode = (StorageNode) o;
				parentDir = storageNode.getRootFileAddress().toString();
				displayed = storageNode.getName() + "/" + relative;
			} else {
				fileNode = (AbstractFileNode) o;
				if (fileNode == null) {
					return null;
				}
				parentDir = fileNode.getFileAddressWithoutProtocol().toString();
				if (!parentDir.endsWith("/")) {
					;
				}
				parentDir += "/";
				GridFileType gft = fileNode.getGridFileType();
				if (gft != null) {
					String fileNodePath = URIUtil.encode(gft.getPath(),
							org.apache.commons.httpclient.URI.allowed_fragment); // escape
																					// special
																					// characters
																					// in
																					// filenames!
					displayed = fileNode.getStorageName() + "/" + fileNodePath
							+ "/" + relative;
				}
			}

			result.getTarget().setParentDirAddress(parentDir);
			result.getTarget().setRelativePath(relative);
			result.getTarget().setDisplayedString(displayed);
			result.getTarget().setUsingWildcards(false);
			result.getTarget().setProtocol(
					U6ProtocolConstants.UNICORE_SMS_PROTOCOL);
			return result;
		} catch (Exception e) {
			GPEActivator.log(IStatus.WARNING,
					"Could not apply value to table entry", e);
			return file;
		} finally {
			serviceSelectionDialog.disposeNodes();
		}
	}

	@Override
	protected void updateContents(Object value) {
		if (defaultLabel == null) {
			return;
		}

		String text = "";//$NON-NLS-1$
		if (value != null) {
			text = ((IFileParameterValue) getValue()).getSource()
					.getDisplayedString();
		}
		defaultLabel.setText(text);
	}

}
