/*
 * Copyright (c) 2000-2005, Intel Corporation All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Intel Corporation nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.fzj.unicore.rcp.gpe4eclipse.security;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import org.eclipse.core.runtime.IStatus;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.ClientFactory;
import com.intel.gpe.clients.api.DownloadGridBeanClient;
import com.intel.gpe.clients.api.RegistryClient;
import com.intel.gpe.clients.api.StandaloneClient;
import com.intel.gpe.clients.api.TargetSystemClient;
import com.intel.gpe.clients.api.TargetSystemFactoryClient;
import com.intel.gpe.clients.api.gpe.GPETargetSystemFactoryClient;
import com.intel.gpe.clients.api.security.GPESecurityManager;
import com.intel.gpe.clients.api.virtualworkspace.GPEVirtualTargetSystemFactoryClient;
import com.intel.gpe.clients.gpe4gtk.rms.ResourceManagementServiceClient;
import com.intel.gpe.clients.impl.WSRFClientImpl;
import com.intel.gpe.clients.impl.gbs.DownloadGridBeanClientImpl;
import com.intel.gpe.clients.impl.registry.RegistryClientImpl;
import com.intel.gpe.util.defaults.preferences.IPreferences;
import com.intel.gpe.util.swing.controls.configurable.IConfigurable;
import com.intel.gpe.util.swing.controls.configurable.MessageProvider;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.clientstubs.IWSRFClientFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;

/**
 * This class is pretty much obsolete in the SWT client, yet it exists in order
 * to stay compatible.
 * 
 * @author demuth
 * @version $Id: GPESecurityManagerImpl.java,v 1.2 2006/12/18 07:43:36 lukichev
 *          Exp $
 */
public class SWTGPESecurityManagerImpl implements GPESecurityManager {

	//GPESecurityManagerImpl original = new GPESecurityManagerImpl();

	public SWTGPESecurityManagerImpl() {

	}

	// These methods aren't used in the SWT client
	public void applyPreferences(IPreferences prefs) throws Exception {
		// original.applyPreferences(prefs);
	}

	public void configure(MessageProvider messageProvider, IPreferences prefs)
			throws Exception {
		// original.configure(messageAdapter, prefs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.security.GPESecurityManager#createTargetSystemClient
	 * (com.intel.gpe.clients.api.TargetSystemFactoryClient,
	 * com.intel.gpe.client2.Client)
	 */
	public TargetSystemClient createTargetSystemClient(
			TargetSystemFactoryClient factory, Client client) throws Exception {
		throw new Exception("Not implemented");
	}

	public DownloadGridBeanClient getGBSClient(URL url) {

		return new DownloadGridBeanClientImpl(getWSRFClient(url));
	}

	public String getIdentity() {
		return "";
	}

	public RegistryClient getRegistryClient(String url) throws Exception {

		return new RegistryClientImpl(getWSRFClient(url));
	}

	public ResourceManagementServiceClient getRMSClient(String rmsName,
			String servicesURL) {
		return null;
	}

	public ClientFactory<TargetSystemFactoryClient> getTargetSystemFactoryBuilder() {
		throw new RuntimeException("Not implemented");
	}

	public GPETargetSystemFactoryClient getTargetSystemFactoryClient(String arg0)
			throws Exception {
		throw new Exception("Not implemented");
	}

	public GPEVirtualTargetSystemFactoryClient getVirtualTargetSystemFactoryClient(
			String arg0) throws Exception {
		throw new Exception("Not implemented");
	}

	public WSRFClientImpl getWSRFClient(String url)
			throws MalformedURLException {
		return getWSRFClient(new URL(url));
	}

	private WSRFClientImpl getWSRFClient(URL url) {
		IWSRFClientFactory factory = GPEActivator.getDefault().getClient()
				.getWSRFClientFactory();
		EndpointReferenceType epr;
		try {
			epr = NodeFactory.getEPRForURI(url.toURI());
			return factory.createWSRFClient(epr);
		} catch (URISyntaxException e) {
			GPEActivator.log(
					IStatus.ERROR,
					"Unable to create client stub for service "
							+ url.toString(), e);
			return null;
		}
	}

	public void init(MessageProvider messageAdapter, IPreferences prefs,
			StandaloneClient standaloneClient, IConfigurable parent)
			throws Exception {
		// original.init(messageAdapter, prefs, standaloneClient, parent);
	}

}