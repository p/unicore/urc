/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.Status;

import com.intel.gpe.clients.api.apps.ArgumentMetaData;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;

/**
 * @author sholl
 * 
 *         Represents an execution environment argument. An argument can have
 *         meta data, a value and a name
 * 
 */
@SuppressWarnings("unchecked")
public class ExecEnvArgument<T> implements Cloneable {

	protected QName name;
	private Class<T> tClass = null;
	protected ArgumentMetaData<T> metaData;
	private Boolean enabled = true;
	private ArgumentType type;
	private T value;

	public static final String qNameId = "de.fzj.unicore.rcp.gpe4eclipse.ResourceParameter";

	public ExecEnvArgument() {

	}
	
	public ExecEnvArgument(QName name, ArgumentType type, ArgumentMetaData<T> metaData) {
		this(name, metaData.getTargetType(),metaData,false,type,metaData.getDefaultValue());
	}

	public ExecEnvArgument(QName name, Class<T> tClass,
			ArgumentMetaData<T> metaData, boolean enabled, ArgumentType type,
			T value) {
		Assert.isNotNull(metaData);
//		Assert.isNotNull(value);
		this.value = value;
		this.name = name;
		this.metaData = metaData;
		this.tClass = tClass;
		this.metaData = metaData;
		metaData.setName(name);
		this.enabled = enabled;
		this.type = type;
	}

	
	@Override
	public ExecEnvArgument<T> clone() {
		try {
			ExecEnvArgument<T> clonedContainer = (ExecEnvArgument<T>) super
					.clone();
			clonedContainer.enabled = new Boolean(this.isEnabled());

			if (this.tClass != null) {
				clonedContainer.tClass = tClass;
			}
			if (this.name != null) {
				clonedContainer.name = new QName(this.getName()
						.getNamespaceURI(), this.getName().getLocalPart(), this
						.getName().getPrefix());
			}
			if (this.type != null) {
				clonedContainer.type = ArgumentType.create(type);

			}

			if (this.metaData != null) {
				clonedContainer.metaData = this.metaData.clone();
			}

			return clonedContainer;

		} catch (CloneNotSupportedException e) {
			GPEActivator.log(Status.ERROR, "Error while cloning execution environment settings: "+e.getMessage(), e);

			return null;
		}
	}

	

	public T getArgumentValue() {
		return value;
	}

	public String getHumanReadableArgumentValue() {
		if (value == null) {
			return "";
		}

		return value.toString();
	}

	public ArgumentMetaData<?> getMetaData() {
		return metaData;
	}

	public QName getName() {
		return name;
	}

	public Class<T> getTClass() {
		return tClass;
	}

	public ArgumentType getType() {
		return type;
	}

	public String getUnit() {
		return metaData.getUnit();
	}

	public T getValue() {
		return value;
	}

	public Boolean isEnabled() {
		return enabled;
	}

	public void setArgumentValue(Object translatedValue) {

		this.value = ((T) translatedValue);
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
		this.metaData.setEnabled(enabled);
	}

	public void setMetaData(ArgumentMetaData<T> metaData) {
		this.metaData = metaData;
	}

	public void setName(QName name) {
		this.name = name;
	}

	public void setTClass(Class<T> class1) {
		tClass = class1;
	}

	public void setType(ArgumentType type) {
		this.type = type;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public static String getQNameId() {
		return qNameId;
	}
	
	public String toString()
	{
		return getName().getLocalPart()+getHumanReadableArgumentValue();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((enabled == null) ? 0 : enabled.hashCode());
		result = prime * result
				+ ((metaData == null) ? 0 : metaData.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ExecEnvArgument)) {
			return false;
		}
		ExecEnvArgument<?> other = (ExecEnvArgument<?>) o;
		return (CollectionUtil.equalOrBothNull(getName(), other.getName())
				&& CollectionUtil.equalOrBothNull(getValue(), other.getValue())
				&& CollectionUtil.equalOrBothNull(getMetaData(),
						other.getMetaData()) && CollectionUtil.equalOrBothNull(
				isEnabled(), other.isEnabled()));
	}
}
