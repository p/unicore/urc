/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import com.intel.gpe.clients.api.apps.ArgumentMetaData;
import com.intel.gpe.clients.impl.tss.ApplicationUtils;
import com.intel.gpe.gridbeans.apps.ArgumentMetaDataImpl;
import com.intel.gpe.gridbeans.apps.BooleanValidArgumentRange;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.resources.ExecEnvValue;
import eu.unicore.jsdl.extensions.AllowedType;
import eu.unicore.jsdl.extensions.AllowedType.Enum;
import eu.unicore.jsdl.extensions.ArgumentDocument.Argument;
import eu.unicore.jsdl.extensions.ArgumentMetadataDocument.ArgumentMetadata;
import eu.unicore.jsdl.extensions.CmdType;
import eu.unicore.jsdl.extensions.ExecutionEnvironmentDescriptionDocument.ExecutionEnvironmentDescription;
import eu.unicore.jsdl.extensions.OptionMetadataDocument.OptionMetadata;
import eu.unicore.jsdl.extensions.OptionType;

/**
 * @author sholl
 * 
 *         This class provides some static methods to handle execution
 *         environments
 * 
 */

public class ExecEnvUtils {

	private ExecEnvUtils() {
		throw new AssertionError();
	}

	public static QName[] convertToName(String[] s) {

		QName[] q = new QName[s.length];
		int i = 0;
		for (String string : s) {
			q[i] = new QName(string);
			i++;
		}
		return q;
	}
	
	public static ExecEnvRequirementSettings createResourceRequirement(List<ExecEnv> execEnvs)
	{
		if(execEnvs == null || execEnvs.size() == 0) return null;
		ExecEnvRequirementSettings settings = new ExecEnvRequirementSettings();
		settings.setRequirementName(new QName("Execution_Environments"));
		settings.setDescription("The execution environments for the selected site");
		ExecEnvRequirementValue value = new ExecEnvRequirementValue();
		settings.setValue(value);
		value.setExecEnvs(execEnvs);
		value.setName(new QName("Execution_Evironment"));
		value.setSelected(0);
		return settings;
	}

	public static ExecEnv createExecEnv(ExecutionEnvironmentDescription execEnv)
	{
		Argument[] arguments = execEnv.getArgumentArray();
		List<ExecEnvArgument<?>> list = new ArrayList<ExecEnvArgument<?>>();
		OptionType[] option = execEnv.getOptionArray();
		for (Argument argument : arguments) {
			ArgumentMetadata argumentMetadata = argument
			.getArgumentMetadata();
			AllowedType.Enum type = (argumentMetadata != null) ? argumentMetadata
					.getType() : AllowedType.STRING;
					ExecEnvArgument<?> arg = ExecEnvUtils
					.createExecEnvArgument(
							type,
							argument.getName(),
							argument,
							de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ArgumentType.ARGUMENT);
					arg.setEnabled(arg.getMetaData().getEnabled());
					list.add(arg);
		}
		for (OptionType optionType : option) {// as boolean
			ExecEnvArgument<?> arg = ExecEnvUtils
			.createExecEnvOption(
					optionType,
					de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ArgumentType.OPTION);
			arg.setEnabled(arg.getMetaData().getEnabled());
			list.add(arg);
		}
		CmdType[] precommand = execEnv.getPreCommandArray();
		for (CmdType optionType : precommand) {
			ExecEnvArgument<?> arg = ExecEnvUtils
			.createExecEnvOption(
					optionType,
					de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ArgumentType.PRECOMMAND);
			arg.setEnabled(arg.getMetaData().getEnabled());
			list.add(arg);
		}
		CmdType[] postcommand = execEnv.getPostCommandArray();
		for (CmdType optionType : postcommand) {
			ExecEnvArgument<?> arg = ExecEnvUtils
			.createExecEnvOption(
					optionType,
					de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ArgumentType.POSTCOMMAND);
			arg.setEnabled(arg.getMetaData().getEnabled());
			list.add(arg);
		}

		ExecEnv ex = new ExecEnv((new QName((execEnv.getName()))), list);
		ex.addUserPreAndPostcommand();

		ex.setDescription(execEnv.getDescription());
		ex.setVersion(execEnv.getVersion());
		return ex;
	}
	
	/**
	 * This method creates a execution environment argument for the specific
	 * type
	 * 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static ExecEnvArgument<?> createExecEnvArgument(
			Enum type2,
			String name,
			Argument argument,
			de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ArgumentType type) {

		
		ExecEnvArgument<?> execEnvArgument = null;

		ArgumentMetaData<?> metaData = ApplicationUtils
				.extractArgumentMetaData(argument);

		execEnvArgument = new ExecEnvArgument(new QName(name),
				metaData.getTargetType(), metaData, true, type,
				metaData.getDefaultValue());

		return execEnvArgument;

	}

	/**
	 * This method creates a execution environment argument for the specific
	 * type boolean to manage the options in the Simple IDB
	 */
	public static ExecEnvArgument<?> createExecEnvOption(
			OptionType optionType,
			de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ArgumentType type) {

		OptionMetadata optionMetadata = optionType.getOptionMetadata();

		Boolean enabled = true;
		String description = "";
		QName[] dep = {};
		QName[] ex = {};
		if (optionMetadata != null) {
			enabled = optionMetadata.getEnabledByDefault();
			description = optionMetadata.getDescription();
			dep = convertToName(optionMetadata.getDependsOnArray());
			ex = convertToName(optionMetadata.getExcludesArray());
		}

		BooleanValidArgumentRange[] validValues = new BooleanValidArgumentRange[1];
		validValues[0] = new BooleanValidArgumentRange(true);

		ArgumentMetaData<Boolean> metaData2 = new ArgumentMetaDataImpl<Boolean>(
				Boolean.class, description, validValues);

		metaData2.setDependencies(dep);
		metaData2.setExclusions(ex);
		metaData2.setEnabled(enabled);
		boolean enabledByDefault = optionMetadata == null ? true
				: optionMetadata.getEnabledByDefault();
		
		ExecEnvArgument<Boolean> execEnvArgument = new ExecEnvArgument<Boolean>(
				new QName(optionType.getName()), Boolean.class, metaData2,
				enabledByDefault, type, true);

		return execEnvArgument;

	}
	
	/**
	 * This method creates a execution environment argument for the specific
	 * type boolean to manage the options in the Simple IDB
	 */
	public static ExecEnvArgument<?> createExecEnvOption(
			CmdType optionType,
			de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ArgumentType type) {

		OptionMetadata optionMetadata = optionType.getOptionMetadata();

		Boolean enabled = true;
		String description = "";
		QName[] dep = {};
		QName[] ex = {};
		if (optionMetadata != null) {
			enabled = optionMetadata.getEnabledByDefault();
			description = optionMetadata.getDescription();
			dep = convertToName(optionMetadata.getDependsOnArray());
			ex = convertToName(optionMetadata.getExcludesArray());
		}

		BooleanValidArgumentRange[] validValues = new BooleanValidArgumentRange[1];
		validValues[0] = new BooleanValidArgumentRange(true);

		ArgumentMetaData<Boolean> metaData2 = new ArgumentMetaDataImpl<Boolean>(
				Boolean.class, description, validValues);

		metaData2.setDependencies(dep);
		metaData2.setExclusions(ex);
		metaData2.setEnabled(enabled);
		boolean enabledByDefault = optionMetadata == null ? true
				: optionMetadata.getEnabledByDefault();
		
		ExecEnvArgument<Boolean> execEnvArgument = new ExecEnvArgument<Boolean>(
				new QName(optionType.getName()), Boolean.class, metaData2,
				enabledByDefault, type, true);

		return execEnvArgument;

	}

	/**
	 * This method generates the specific value for an execution environment
	 * argument
	 */
	public static ExecEnvValue<?> getValueforArgument(
			ExecEnvArgument<?> execEnvArgument) {
		return new ExecEnvValue<Object>(execEnvArgument.getArgumentValue());
	}

}
