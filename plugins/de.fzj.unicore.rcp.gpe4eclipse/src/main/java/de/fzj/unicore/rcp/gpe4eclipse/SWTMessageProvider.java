/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.util.defaults.preferences.INode;
import com.intel.gpe.util.defaults.preferences.IPreferences;
import com.intel.gpe.util.swing.controls.CloseListener;
import com.intel.gpe.util.swing.controls.configurable.ConfigurableDialog;
import com.intel.gpe.util.swing.controls.configurable.GPEPanel;
import com.intel.gpe.util.swing.controls.configurable.GPEPanelResult;
import com.intel.gpe.util.swing.controls.configurable.GUIKeys;
import com.intel.gpe.util.swing.controls.configurable.MessageProvider;

import de.fzj.unicore.rcp.common.utils.swing.AwtEnvironment;

/**
 * @author demuth
 * 
 */
public class SWTMessageProvider implements MessageProvider {

	private static class MyCloseListener implements CloseListener {

		private ConfigurableDialog dialog;
		private GPEPanel panel;
		private IPreferences preferences;

		public MyCloseListener(ConfigurableDialog dialog, GPEPanel panel,
				IPreferences preferences) {
			this.dialog = dialog;
			this.panel = panel;
			this.preferences = preferences;
		}

		public void closed() {
			dialog.store(preferences);
			if (GPEPanelResult.isPositive(panel.getResult())) {
				panel.store(preferences);
			}
			dialog.setVisible(false);
		}

	}

	private static class MyWindowAdapter extends WindowAdapter {

		private GPEPanel panel;

		public MyWindowAdapter(GPEPanel panel) {
			this.panel = panel;
		}

		@Override
		public void windowClosed(WindowEvent e) {
			panel.closePanel();
		}

		// public void windowClosing(WindowEvent e) {
		// panel.closePanel();
		// }

	}

	private IPreferences userPreferences;
	private Frame frame;
	private Rectangle parentBounds;

	private Object lock = new Object();

	private int answer;

	public SWTMessageProvider(IPreferences userPrefs) {
		this.userPreferences = userPrefs;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.providers.MessageProvider#show(com.intel.gpe.client2
	 * .panels.GPEPanel)
	 */
	public synchronized void show(GPEPanel panel) {
		final Display display = PlatformUI.getWorkbench().getDisplay();
		final INode node = panel.getNode();
		ConfigurableDialog dialog = new ConfigurableDialog(frame, panel,
				panel.getTitle(), node, true);
		display.syncExec(new Runnable() {
			public void run() {
				AwtEnvironment env = AwtEnvironment.getInstance(display);
				frame = env.createDialogParentFrame();

				parentBounds = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell().getBounds();
			}
		});

		// if the panel hasn't been opened before: place it in the middle of the
		// active window
		String xValue = userPreferences.get(node.getKey(GUIKeys.x.getKey()));
		String yValue = userPreferences.get(node.getKey(GUIKeys.y.getKey()));
		if (xValue == null || yValue == null) {
			int newXValue = parentBounds.x + parentBounds.width / 2 - 200;
			int newYValue = parentBounds.y + parentBounds.height / 2 - 150;
			userPreferences.set(node.getKey(GUIKeys.x.getKey()),
					String.valueOf(newXValue));
			userPreferences.set(node.getKey(GUIKeys.y.getKey()),
					String.valueOf(newYValue));
			userPreferences.set(node.getKey(GUIKeys.width.getKey()), "400");
			userPreferences.set(node.getKey(GUIKeys.height.getKey()), "300");
		}

		dialog.getContentPane().setLayout(new BorderLayout());
		dialog.getContentPane().add((Component) panel, BorderLayout.CENTER);
		dialog.addWindowListener(new MyWindowAdapter(panel));

		panel.addCloseListener(new MyCloseListener(dialog, panel,
				userPreferences));

		panel.load(userPreferences);
		dialog.load(userPreferences);
		// dialog.setLocationRelativeTo(frame);
		try {
			dialog.setVisible(true);
		} catch (Throwable e) {
			// I have no clue why we try this twice, but there must be a reason
			dialog.setVisible(true);
		}

		// IWorkbenchWindow window =
		// PlatformUI.getWorkbench().getActiveWorkbenchWindow();

		// Composite awtContainer = new
		// Composite(window.getShell(),SWT.EMBEDDED);
		// Frame frame = SWT_AWT.new_Frame(awtContainer);
		// frame.add(panel.getComponent());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.providers.SimpleMessageProvider#show(com.intel.
	 * gpe.client2.panels.GPEPanel,
	 * com.intel.gpe.client2.defaults.preferences.INode)
	 */
	public void show(GPEPanel panel, INode node) {
		show(panel);

	}

	public void showException(String message, Throwable t) {
		Exception e = null;
		if (t instanceof Exception) {
			e = (Exception) t;
		} else {
			e = new Exception(t);
		}
		GPEActivator.log(IStatus.ERROR, message, e);
	}

	public void showMessage(String message) {
		GPEActivator.log(IStatus.INFO, message, true);
	}

	public int showQuestion(final String question,
			final String[] possibleAnswers, final int defaultAnswer) {
		synchronized (lock) {
			answer = -1;
			final Display display = PlatformUI.getWorkbench().getDisplay();
			display.syncExec(new Runnable() {
				public void run() {
					MessageDialog d = new MessageDialog(display
							.getActiveShell(), "Question", null, question,
							MessageDialog.QUESTION, possibleAnswers,
							defaultAnswer) {
						@Override
						protected void initializeBounds() {
							super.initializeBounds();
							// fix the order of buttons... by default, the
							// MessageDialog moves the default button to the
							// right on GTK which is undesirable in this case
							Button defaultButton = getShell()
									.getDefaultButton();
							if (defaultButton != null
									&& isContained(buttonBar, defaultButton)) {
								if (defaultAnswer > 0) {
									defaultButton
											.moveBelow(getButton(defaultAnswer - 1));
								} else {
									defaultButton.moveAbove(null);
								}
								((Composite) buttonBar).layout();
							}

						}

						private boolean isContained(Control container,
								Control control) {
							Composite parent;
							while ((parent = control.getParent()) != null) {
								if (parent == container) {
									return true;
								}
								control = parent;
							}
							return false;
						}
					};

					answer = d.open();
					if (answer == SWT.DEFAULT) {
						answer = defaultAnswer;
					}
				}
			});
			if (answer == -1) {
				return defaultAnswer;
			} else {
				return answer;
			}
		}
	}

}
