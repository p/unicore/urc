package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing;

public interface IStageListObserver {

	/**
	 * Update the view to reflect the fact that an entry was added
	 * 
	 * @param out
	 */
	public void addStage(IStage stage);

	/**
	 * Update the view to reflect the fact that an entry was removed
	 * 
	 * @param out
	 */
	public void removeStage(IStage stage);

	/**
	 * Update the view to reflect the fact that one of the entries was modified
	 * 
	 * @param out
	 */
	public void updateStage(IStage stage);
}
