/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.intel.gpe.clients.api.jsdl.JSDLJob;
import com.intel.gpe.gridbeans.parameters.IFileSetParameterValue;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageList;

class StageOutLabelProvider extends LabelProvider implements
ITableLabelProvider {

	StageOutTableViewer viewer;

	private Image checkedImg = GPEActivator.getImageDescriptor("checked.png")
	.createImage();
	private Image uncheckedImg = GPEActivator.getImageDescriptor(
	"unchecked.png").createImage();

	public StageOutLabelProvider(StageOutTableViewer viewer) {
		this.viewer = viewer;
	}

	@Override
	public void dispose() {
		super.dispose();
		checkedImg.dispose();
		uncheckedImg.dispose();

	}

	/**
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
	 *      int)
	 */
	public Image getColumnImage(Object element, int columnIndex) {
		//		 IStageOut stage = (IStageOut) element;
		//
		//		if(columnIndex ==
		//			StageOutTableViewer.getColumnFor(StageOutTableViewer.FORCE))
		//		{
		//			return (stage.getValue().getFlags() & JSDLJob.FLAG_OVERWRITE) != 0 ? checkedImg :
		//				uncheckedImg;
		//		}
		// else if(columnIndex ==
		// StageOutTableViewer.getColumnFor(StageOutTableViewer.BINARY))
		// {
		// return stage.isBinaryContent() ? enabledCheckboxImg :
		// disabledCheckboxImg;
		// }
		return null;

	}

	/**
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
	 *      int)
	 */
	public String getColumnText(Object element, int columnIndex) {
		String result = "";
		IStage stage = (IStage) element;

		if (columnIndex == StageOutTableViewer
				.getColumnFor(StageOutTableViewer.ID)) {
			try {
				result = stage.getGridBeanParameter().getName().getLocalPart();
				StageList list = viewer.getStageOutList();
				IFileSetParameterValue parent = list.getFileSets().get(
						stage.getGridBeanParameter());
				if (parent != null) {
					result += "_"
						+ (1 + parent.getFiles().indexOf(stage.getValue()));
				}
			} catch (Exception e) {
				result = "";
			}

		} else if (columnIndex == StageOutTableViewer
				.getColumnFor(StageOutTableViewer.SOURCE_FILE)) {
			result = stage.getNameInWorkingDir();
		} else if (columnIndex == StageOutTableViewer
				.getColumnFor(StageOutTableViewer.TARGET_TYPE)) {
			try {
				result = stage.getType().getLocalPart();
			} catch (Exception e) {
				result = "unknown type";
			}
		} else if (columnIndex == StageOutTableViewer
				.getColumnFor(StageOutTableViewer.TARGET_FILE)) {
			try {
				result = stage.getValue().getTarget().getDisplayedString();
			} catch (Exception e) {
				result = "";
			}
		}
		else if(columnIndex ==
			StageOutTableViewer.getColumnFor(StageOutTableViewer.FORCE))
		{
			Boolean b = (stage.getValue().getFlags() & JSDLJob.FLAG_OVERWRITE) != 0;
			return b.toString();
		}
		else if (columnIndex == StageOutTableViewer.getColumnFor(StageOutTableViewer.DELETE_OUTPUT_FILE)) {
			
			Boolean b = (stage.getValue().getFlags() & JSDLJob.FLAG_DELETE_ON_TERMINATE) != 0;
			return b.toString();
		}


		return result;
	}
}