/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse;

import org.eclipse.ui.IWorkbenchPart;

import com.intel.gpe.clients.all.ClientAdapter;
import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.clients.all.providers.OutcomeProvider;
import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.StandaloneClient;
import com.intel.gpe.clients.api.cache.GPEClientProperties;
import com.intel.gpe.clients.api.security.GPESecurityManager;
import com.intel.gpe.clients.api.transfers.FileProvider;
import com.intel.gpe.util.defaults.preferences.IPreferences;

import de.fzj.unicore.rcp.gpe4eclipse.clientstubs.IWSRFClientFactory;

/**
 * @author demuth
 * 
 */
public interface ISWTClient extends Client, StandaloneClient {

	public ClientAdapter clone();

	/**
	 * Create a GridBeanClient instance for managing a grid bean
	 */
	public GridBeanClient<IWorkbenchPart> createGridBeanClient();

	/**
	 * @return shared FileProvider instance for handling file transfers
	 */
	public FileProvider getFileProvider();

	/**
	 * 
	 * @return
	 */
	public GPEClientProperties getGPEClientProperties();

	/**
	 * 
	 * @return shared outcome provider instance for fetching job outcomes and
	 *         caching them for later reuse
	 */
	public OutcomeProvider getOutcomeProvider();

	/**
	 * 
	 * @return shared GPESecurityManager instance for creating secure web
	 *         service clients
	 */
	public GPESecurityManager getSecurityManager();

	/**
	 * 
	 * @return shared IPreferences instance for accessing user perferences
	 */
	public IPreferences getUserPreferences();

	public IWSRFClientFactory getWSRFClientFactory();
}
