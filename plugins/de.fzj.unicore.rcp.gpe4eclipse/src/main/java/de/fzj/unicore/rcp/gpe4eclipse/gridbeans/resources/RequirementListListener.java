package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

/**
 * Base class that does nothing by default.. Can be subclassed to react on
 * certain types of events.
 * @author bdemuth
 *
 */
public class RequirementListListener implements IRequirementListViewer {

	@Override
	public void notifyListCleared() {
	
	}

	@Override
	public void notifyRequirementAdded(ResourceRequirement resourceRequirement) {
	
	}

	@Override
	public void notifyRequirementRemoved(ResourceRequirement resourceRequirement) {
	
	}

	@Override
	public void notifyRequirementUpdated(ResourceRequirement resourceRequirement) {
		
	}

}
