/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.common.utils.ArrayUtils;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnv;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnvDataControler;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnvRequirementValue;

/**
 * @author sholl
 * 
 *         Provides the dialog for the user to select an execution environment
 *         and specify the parameter. It manages the creation of the selection
 *         area for the specific execution environment. In detail, the creation
 *         of the list of arguments and the creation of the composite that
 *         contains several GridRows, that represent the arguments.
 * 
 * 
 */
public class ExecEnvViewer extends Dialog {

	private ExecEnvRequirementValue requirement;
	private ExecEnvRequirementValue backup;
	private Combo comboName, comboVersion;
	private Composite parent; 
	Label description;
	private ExecEnvDataControler controler;
	private ExecutionEnvironmentListener currentListener;
	
	private int selectedName = 0;
	private String selectedVersion = null; 
	
	private static final String VERSION_NONE = "unspecified";

	private ExecEnvDetailsViewer execEnvpanel;
	
	// mapping from name+version to ExecEnv
	private Map<String, ExecEnv[]> envs = new TreeMap<String, ExecEnv[]>(new Comparator<String>(){
		@Override
		public int compare(String o1, String o2) {
			return o1.compareTo(o2);
		}});

	public ExecEnvViewer(Shell parent, ExecEnvRequirementValue workingObject) {
		super(parent);

		setShellStyle(SWT.APPLICATION_MODAL | SWT.MIN | SWT.MAX | SWT.RESIZE | SWT.CLOSE
				| SWT.BORDER | SWT.TITLE);
		this.requirement = workingObject;
		// store a backup, otherwise cancel will not work!!!
		// because everything is actually
		// directly stored in the model.
		this.backup = workingObject.copy();
		controler = new ExecEnvDataControler(requirement.getSelectedExecEnv());
		for(ExecEnv env : requirement.getExecEnvs())
		{
			ExecEnv[] envArr = envs.get(env.getReadableName());
			if(envArr == null) envArr = new ExecEnv[0];
			envArr = ArrayUtils.concat(envArr, env);
			
			envs.put(env.getReadableName(), envArr);
		}
		
		for(ExecEnv[] env : envs.values())
		{
			Arrays.sort(env,new Comparator<ExecEnv>() {

				@Override
				public int compare(ExecEnv o1, ExecEnv o2) {
					if(o1.getVersion() == null)
					{
						if(o2.getVersion() == null) return 0;
						else return -1;
					}
					if(o2.getVersion() == null)
					{
						if(o1.getVersion() == null) return 0;
						else return 1;
					}
					// sort in reverse order: biggest version first!
					return o2.getVersion().compareTo(o1.getVersion());
				}
			});
		}

	}
	
	
	
	
	@Override
	protected void cancelPressed() {

		requirement = backup;
		super.cancelPressed();
	}

	@Override
	public boolean close() {

		if (currentListener != null) {
			requirement.getSelectedExecEnv().removePropertyChangeListener(
					currentListener); // make sure we stop listening
		}
		return super.close();
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);

		shell.setText("Execution Environment Settings");

	}

	@Override
	protected Control createButtonBar(Composite parent) {

		Control createButtonBar = super.createButtonBar(parent);
		getShell().layout();
		getShell().pack();
		return createButtonBar;
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		Composite clientArea = new Composite(parent, SWT.FILL | SWT.RESIZE);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		clientArea.setLayout(gridLayout);

		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.horizontalIndent = 1;
		gridData.horizontalSpan = 1;
		clientArea.setLayoutData(gridData);

		createSelectionArea(clientArea);

		refresh(clientArea);
		currentListener = new ExecutionEnvironmentListener(
				requirement.getSelectedExecEnv(), controler, execEnvpanel);
		requirement.getSelectedExecEnv().addPropertyChangeListener(
				currentListener);
		parent.layout();
		parent.pack();

		return clientArea;
	}

	private Composite createSelectionArea(final Composite parent) {
		this.parent = parent;
		final Composite panel = new Composite(parent, SWT.NONE);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		panel.setLayout(gridLayout);

		GridData gridData = new GridData(SWT.FILL, SWT.TOP, true, true);
		panel.setLayoutData(gridData);

		

		
		createNameCombo(panel);
		createVersionCombo(panel);
		
		
		description = new Label(panel, SWT.NONE);
		description.setText(requirement.getSelectedExecEnv().getDescription());


		setItemsAndSelections();
		return panel;

	}
	
	protected void createNameCombo(Composite panel)
	{
		Label l = new Label(panel, SWT.NONE);
		l.setText("Execution Environment");
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = false;
		gridData.horizontalSpan = 1;
		l.setLayoutData(gridData);
		comboName = new Combo(panel, SWT.NONE);
		gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalSpan = 1;
		comboName.setLayoutData(gridData);
		comboName.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				if (comboName.getSelectionIndex() != selectedName) {
					selectedName = comboName.getSelectionIndex();
					selectionChanged();
					setVersionItemsAndSelection();
				}
			}
			public void widgetSelected(SelectionEvent e) {
				if (comboName.getSelectionIndex() != selectedName) {
					selectedName = comboName.getSelectionIndex();
					selectionChanged();
					setVersionItemsAndSelection();
				}
			}

		});
	}
	
	private String getSelectedVersion(int index)
	{
		String[] versions = comboVersion.getItems();
		if(index <= 0 || index >= versions.length) return null;
		return versions[index];
	}
	
	protected void createVersionCombo(Composite panel)
	{
		Label l = new Label(panel, SWT.NONE);
		l.setText("Version");
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = false;
		gridData.horizontalSpan = 1;
		l.setLayoutData(gridData);
		comboVersion = new Combo(panel, SWT.NONE);
		gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalSpan = 1;
		comboVersion.setLayoutData(gridData);
		comboVersion.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				int index = comboVersion.getSelectionIndex();
				String newSelected = getSelectedVersion(index);
				if (!CollectionUtil.equalOrBothNull(newSelected,selectedVersion)) {
					selectedVersion = newSelected;
					selectionChanged();
				}
			}
			public void widgetSelected(SelectionEvent e) {
				int index = comboVersion.getSelectionIndex();
				String newSelected = getSelectedVersion(index);
				if (!CollectionUtil.equalOrBothNull(newSelected,selectedVersion)) {
					selectedVersion = newSelected;
					selectionChanged();
				}
			}

		});
	}
	
	protected ExecEnv getSelectedExecEnv()
	{
		String name = comboName.getItem(selectedName);
		ExecEnv[] envArr = envs.get(name);
		for(ExecEnv env : envArr)
		{
			if(CollectionUtil.equalOrBothNull(env.getVersion(), selectedVersion))
			{
				return env;
			}
		}
		return envArr[0];
	}
	
	
	
	protected void selectionChanged()
	{
		
		if (currentListener != null) {
			// make sure we stop listening
			requirement.getSelectedExecEnv()
					.removePropertyChangeListener(currentListener); 
		}
		int i = 0;
		ExecEnv selected = getSelectedExecEnv();
		for(ExecEnv env : requirement.getExecEnvs())
		{
			if(selected.equals(env))
			{
				requirement.setSelected(i);
				requirement.setSelectedVersion(selectedVersion);
				break;
			}
			i++;
		}
		description.setText(selected.getDescription());
		controler = new ExecEnvDataControler(selected);
		execEnvpanel = refresh(parent);
		currentListener = new ExecutionEnvironmentListener(selected, controler, execEnvpanel);
		selected.addPropertyChangeListener(
				currentListener);
		parent.layout(true);
		parent.getShell().pack();

	}
	
	protected void setItemsAndSelections()
	{
		
		selectedVersion = requirement.getSelectedVersion();
		
		List<String> execEnvNames = new ArrayList<String>();
		
		int i = 0;
		for(String name : envs.keySet())
		{
			ExecEnv[] envArr = envs.get(name);
			execEnvNames.add(envArr[0].getReadableName());
			
			for(int j = 0; j < envArr.length; j++)
			{
				ExecEnv env = envArr[j];
				if(env.equals(requirement.getSelectedExecEnv()))
				{
					selectedName = i;
				}
				
			}
			i++;
		}
		comboName.setItems(execEnvNames.toArray(new String[execEnvNames.size()]));
		comboName.select(selectedName);
		
		setVersionItemsAndSelection();
	}
	
	protected void setVersionItemsAndSelection()
	{
		ArrayList<String> execEnvVersions = new ArrayList<String>();
		execEnvVersions.add(VERSION_NONE);
		int i = 0;
	
		int selectedVersionIndex = 0;
		for(String name : envs.keySet())
		{
			if(i == selectedName)
			{
				ExecEnv[] selectedArr = envs.get(name);
				int j = 0;
				for(ExecEnv env : selectedArr)
				{
					if(env.getVersion() != null)
					{
						execEnvVersions.add(env.getVersion());
						j++;
						if(env.getVersion().equals(selectedVersion))
						{
							selectedVersionIndex = j;
						}
						
					}
					
					
				}
			}
			i++;
		}
		
		comboVersion.setItems(execEnvVersions.toArray(new String[execEnvVersions.size()]));
		comboVersion.select(selectedVersionIndex);
	}

	public ExecEnvRequirementValue getBckup() {

		return backup;
	}

	public ExecEnvDataControler getControler() {
		return controler;
	}

	public ExecEnvRequirementValue getExecEnvRequirement() {

		return requirement;
	}

	@Override
	public void okPressed() {

		close();
	}

	private ExecEnvDetailsViewer refresh(Composite parent) {

		if (execEnvpanel != null) {
			execEnvpanel.dispose();
		}
		execEnvpanel = new ExecEnvDetailsViewer(parent,
				requirement.getSelectedExecEnv(), this);
		execEnvpanel
				.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		execEnvpanel.createControls();
		execEnvpanel.validate();
		return execEnvpanel;

	}

	public void setControler(ExecEnvDataControler controler) {
		this.controler = controler;
	}

}
