package de.fzj.unicore.rcp.gpe4eclipse.problems;

import java.io.File;

import de.fzj.unicore.rcp.gpe4eclipse.exceptions.InvalidGridBeanException;
import de.fzj.unicore.rcp.logmonitor.problems.GraphicalSolution;
import eu.unicore.problemutil.Problem;

public class DeleteInvalidGridBeanSolution extends GraphicalSolution {

	public DeleteInvalidGridBeanSolution() {
		super("INVALID_GRIDBEAN_DELETE", "INVALID_GRIDBEAN",
				"Delete the broken application GUI from your client installation (Caution!)");

	}

	@Override
	public boolean isAvailableFor(String message, Problem[] problems,
			String details) {
		for (Problem p : problems) {
			if (p.getCause() instanceof InvalidGridBeanException) {
				return true;
			}

		}
		return false;
	}

	@Override
	public boolean solve(String message, Problem[] problems, String details)
			throws Exception {
		for (Problem p : problems) {
			if (p.getCause() instanceof InvalidGridBeanException) {
				InvalidGridBeanException e = (InvalidGridBeanException) p
						.getCause();
				File f = e.getGridBeanFile();
				f.delete();
				return false;
			}
		}

		return true;
	}

}
