/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.commands;

import java.util.List;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out.StageOutList;

/**
 * @author demuth
 */
public class RemoveStageOutFromModelCommand extends Command {

	private StageOutList model;
	private List<IStage> stages;

	public RemoveStageOutFromModelCommand(StageOutList model,
			List<IStage> stages) {
		this.model = model;
		this.stages = stages;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {

		redo();
	}

	@Override
	public void redo() {
		for (IStage stage : stages) {
			model.removeStage(stage);
		}
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		for (IStage stage : stages) {
			model.addStage(stage);
		}
	}
}
