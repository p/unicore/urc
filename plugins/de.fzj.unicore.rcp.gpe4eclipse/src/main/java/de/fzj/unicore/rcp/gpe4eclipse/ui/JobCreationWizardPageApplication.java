/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.FilteredList;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.intel.gpe.clients.api.Application;
import com.intel.gpe.clients.api.TargetSystemClient;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.common.guicomponents.EnhancedWizardDialog;
import de.fzj.unicore.rcp.common.utils.ToolBarUtils;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.actions.DownloadGridBeansAction;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.GridBeanRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.IGridBeanRegistryListener;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

/**
 * Job creation wizard page for selecting the application (= GridBean) to use
 * 
 * @author demuth
 */
public class JobCreationWizardPageApplication extends WizardPage implements
		IGridBeanRegistryListener, PropertyChangeListener {

	public static final String NAME = "applicationPage";

	// toolbar manager used to add actions
	private ToolBarManager toolBarManager = new ToolBarManager(SWT.RIGHT);
	private GridBeanInfo[] gridbeans;
	private FilteredList gridBeanList;

	protected boolean listeningToCreationInfo = true;
	private JobCreationInfo jobCreationInfo;
	
	private DownloadGridBeansAction downloadGridBeansAction;

	public JobCreationWizardPageApplication(JobCreationInfo jobCreationInfo) {
		super(NAME);
		this.setTitle("Select application");
		this.setDescription("Choose an application to be run on the target system when the job is executed.");
		// this.setImageDescriptor(ImageDescriptor.createFromFile(WFActivator.class,"images/flowbanner.gif"));
		this.jobCreationInfo = jobCreationInfo;
		jobCreationInfo.addPropertyChangeListener(this);
	}

	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(1, true));
		gridBeanList = createGridBeanList(composite);
		GridBeanInfo[] gbs = gridbeans;
		if (gbs.length > 0) {
			setSelectedGridBean(gbs[0]);
		}
		Composite toolBarContainer = new Composite(composite, SWT.NO_FOCUS);
		toolBarContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				false));
		toolBarContainer.setLayout(new GridLayout(1, true));
		downloadGridBeansAction = new DownloadGridBeansAction();
		toolBarManager.add(downloadGridBeansAction);
		ToolBar toolBar = toolBarManager.createControl(toolBarContainer);
		GridData data = new GridData(SWT.RIGHT, SWT.CENTER, true, false);
		toolBar.setLayoutData(data);
		ToolBarUtils.setToolItemText(toolBar);
		// gridBeanList.pack();
		Display d = parent.getDisplay();
		gridBeanList.setBackground(new Color(d, 1, 0, 0));
		setControl(composite);
	}

	/**
	 * Creates a filtered list.
	 * 
	 * @param parent
	 *            the parent composite.
	 * @return returns the filtered list widget.
	 */
	protected FilteredList createGridBeanList(Composite parent) {
		int flags = SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.SINGLE;

		ILabelProvider labelProvider = new LabelProvider() {
			@Override
			public String getText(Object element) {
				return element.toString();
			}
		};

		FilteredList list = new FilteredList(parent, flags, labelProvider,
				true, false, false);
		determineAvailableGridBeans();
		GridBeanRegistry registry = GPEActivator.getDefault()
				.getGridBeanRegistry();
		registry.addListener(this);
		list.setElements(gridbeans);
		GridData data = new GridData();
		data.widthHint = convertWidthInCharsToPixels(60);
		data.heightHint = convertHeightInCharsToPixels(18);
		data.grabExcessVerticalSpace = true;
		data.grabExcessHorizontalSpace = true;
		data.horizontalAlignment = GridData.FILL;
		data.verticalAlignment = GridData.FILL;
		list.setLayoutData(data);
		list.setFont(parent.getFont());
		list.setFilter("*");

		list.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {
				handleWidgetSelected();
				if (getWizard().canFinish()
						&& (getContainer() instanceof EnhancedWizardDialog)) {
					EnhancedWizardDialog dialog = (EnhancedWizardDialog) getContainer();
					dialog.finishPressed();
				}
			}

			public void widgetSelected(SelectionEvent e) {
				handleWidgetSelected();
			}
		});

		return list;
	}

	protected void determineAvailableGridBeans() {
		GridBeanRegistry registry = GPEActivator.getDefault()
				.getGridBeanRegistry();
		List<GridBeanInfo> gbList = registry.getGridBeans();
		gridbeans = gbList.toArray(new GridBeanInfo[gbList.size()]);
		filterGridBeans();

	}

	protected void filterGridBeans() {
		if (jobCreationInfo.getResources() != null
				&& jobCreationInfo.getResources().size() > 0) {
			GridBeanRegistry registry = GPEActivator.getDefault()
					.getGridBeanRegistry();
			List<TargetSystemClient> clients = new ArrayList<TargetSystemClient>();
			for (IAdaptable a : jobCreationInfo.getResources()) {
				TargetSystemClient client = (TargetSystemClient) a
						.getAdapter(TargetSystemClient.class);
				if (client != null) {
					clients.add(client);
				} else {
					return; // as soon as we find any non-target system, we
							// don't have a valid selection and assume that all
							// gridbeans can be used
				}
			}
			GridBeanInfo[] gbs = gridbeans;
			List<GridBeanInfo> supported = new ArrayList<GridBeanInfo>();
			for (int i = 0; i < gbs.length; i ++) {
				GridBeanInfo info = gbs[i];
				Application app = registry.getGridBean(info)
						.getSupportedApplication();
			
				for (TargetSystemClient client : clients) {
					try {
						if (client.supportsApplication(app.getName(),
								app.getApplicationVersion())) {
							supported.add(info);
							break;
						}
					} catch (Exception e1) {
						GPEActivator.log(
								IStatus.WARNING,
								"Unable to test whether target system supports application "
										+ app.getName() + ": "
										+ e1.getMessage(), e1);
					}
				}
				
			}
			gridbeans = supported.toArray(new GridBeanInfo[supported.size()]);
		}
	}

	public boolean finish() {

		GridBeanRegistry registry = GPEActivator.getDefault()
				.getGridBeanRegistry();
		registry.removeListener(this);
		if(downloadGridBeansAction != null)
		{
			downloadGridBeansAction.dispose();
		}
		jobCreationInfo.removePropertyChangeListener(this);
		return true;
	}

	public GridBeanInfo getSelectedGridBean() {
		return jobCreationInfo.getGridBeanInfo();
	}

	public void gridBeansChanged(List<GridBeanInfo> oldValue,
			List<GridBeanInfo> newValue) {
		gridbeans = newValue.toArray(new GridBeanInfo[newValue.size()]);
		filterGridBeans();
		gridBeanList.setElements(gridbeans);
	}

	private void handleWidgetSelected() {
		Object[] newSelection = gridBeanList.getSelection();
		GridBeanInfo selectedGridBean = null;
		if (newSelection.length > 0) {
			selectedGridBean = (GridBeanInfo) newSelection[0];
		}
		setSelectedGridBean(selectedGridBean);
		setPageComplete(selectedGridBean != null);
	}

	public void propertyChange(PropertyChangeEvent evt) {
		if (JobCreationInfo.PROP_RESOURCES.equals(evt.getPropertyName())) {
			final GridBeanInfo oldSelected = getSelectedGridBean();
			if (gridBeanList != null) {
				Job j = new BackgroundJob("Updating list of available applications") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						determineAvailableGridBeans();
						PlatformUI.getWorkbench().getDisplay()
								.asyncExec(new Runnable() {

									public void run() {
										
										GridBeanInfo[] gbs = gridbeans;
										gridBeanList.setElements(gbs);
										for(int i = 0; i < gbs.length; i++)
										{
											if (CollectionUtil.equalOrBothNull(oldSelected, gbs[i])) {
												gridBeanList
														.setSelection(new int[] {i});
											} else if(i == gbs.length-1){
												gridBeanList
												.setSelection(new int[] { -1 });
											}
										}
										
									}
								});
						return Status.OK_STATUS;
					}

				};
				j.schedule();

			}

		}

	}

	public void setSelectedGridBean(GridBeanInfo selectedGridBean) {
		jobCreationInfo.setGridBeanInfo(selectedGridBean);
	}

}
