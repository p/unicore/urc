/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.eclipse.swt.widgets.TableItem;

import com.intel.gpe.util.collections.CollectionUtil;

/**
 * provides a list of ResourceProperties
 * 
 * @author Christian Hohmann
 * 
 */
public class RequirementList implements Serializable {
	private static final long serialVersionUID = 7020083904105191292L;
	private Set<IRequirementListViewer> changeListeners = new HashSet<IRequirementListViewer>();
	private List<ResourceRequirement> resourceRequirements = new ArrayList<ResourceRequirement>();

	private Map<QName, Set<QName>> requirementClashes = new HashMap<QName, Set<QName>>();

	private Map<QName, String> selectedUnits = new HashMap<QName, String>();

	public RequirementList() {
		super();
	}

	public void addChangeListener(IRequirementListViewer viewer) {
		changeListeners.add(viewer);
	}

	public void addResourceRequirement(ResourceRequirement resourceRequirement) {
		addResourceRequirement(resourceRequirement, -1);
	}

	public void addResourceRequirement(ResourceRequirement resourceRequirement, int index) {

		// make sure that we keep the units even when refilling the requirement
		// list!
		String selectedUnit = selectedUnits.get(resourceRequirement
				.getRequirementName());
		if (selectedUnit != null && resourceRequirement.getValue() != null) {
			resourceRequirement.getValue().setSelectedUnit(selectedUnit);
		}
		if(index < 0 || index > resourceRequirements.size())
		{
			index = resourceRequirements.size();
		}
		resourceRequirements.add(index,resourceRequirement);

		addClashes(resourceRequirement);

		Iterator<IRequirementListViewer> iterator = changeListeners.iterator();
		while (iterator.hasNext()) {
			iterator.next()
			.notifyRequirementAdded(resourceRequirement);
		}

		disableClashingRequirements(resourceRequirement);

	}

	/**
	 * Removes all requirements and clashes from the list.
	 */
	public void clear() {
		resourceRequirements.clear();
		requirementClashes.clear();
		Iterator<IRequirementListViewer> iterator = changeListeners.iterator();
		while (iterator.hasNext()) {
			iterator.next().notifyListCleared();
		}
	}

	public void disableClashingRequirements(
			ResourceRequirement resourceRequirement) {
		if (resourceRequirement.isEnabled()) {
			Set<QName> currentClashes = getClashForRequirement(resourceRequirement
					.getRequirementName());
			Iterator<QName> clashesIterator = currentClashes.iterator();
			while (clashesIterator.hasNext()) {
				ResourceRequirement currentRequirement = getResourceRequirement(clashesIterator
						.next());
				if (resourceRequirements.contains(currentRequirement)) {
					if (currentRequirement.isEnabled()) {
						currentRequirement.setEnabled(Boolean.FALSE);
						Iterator<IRequirementListViewer> listeners = changeListeners
						.iterator();
						while (listeners.hasNext()) {
							IRequirementListViewer listener = listeners.next();
							listener.notifyRequirementUpdated(currentRequirement);
						}
					}
				}
			}
		}
	}

	private Set<QName> getClashForRequirement(QName qname) {
		if (requirementClashes.containsKey(qname)) {
			return requirementClashes.get(qname);
		} else {
			return new HashSet<QName>();
		}
	}

	public ResourceRequirement getResourceRequirement(QName qname) {

		for (int i = 0; i < resourceRequirements.size(); i++) {
			if (resourceRequirements.get(i).getRequirementName().equals(qname)) {
				return resourceRequirements.get(i);
			}
		}
		return null;
	}

	/**
	 * changes a value
	 * 
	 * @param element
	 *            is the TableItem that holds the ResourceRequirement that has
	 *            changed
	 * @param property
	 *            identifies the property that changed
	 * @param value
	 *            is the new Value (type depends on the property)
	 */
	public void modifyValue(Object element, String property, Object value) {
		ResourceRequirement requirement = (ResourceRequirement) ((TableItem) element)
		.getData();
		if (ResourceRequirementsViewer.ENABLED.equals(property)) {
			boolean newValue = (Boolean) value;
			requirement.setEnabled(newValue);
		}

		else if (ResourceRequirementsViewer.UNITS.equals(property)) {
			selectedUnits.put(requirement.getRequirementName(), requirement
					.getValue().getSelectedUnit());
		} else if (ResourceRequirementsViewer.NAME.equals(property)) {

			QName newValue = new QName(ResourceRequirement.qNameId,
					(String) value);
			requirement.setRequirementName(newValue);
		} else if (ResourceRequirementsViewer.VALUE.equals(property)) {

			IRequirementValue newValue = (IRequirementValue) value;
			Object oldValue = requirement.getValue();
			requirement.setValue(newValue);
			if(!CollectionUtil.equalOrBothNull(oldValue, newValue))
			{
				requirement.setDirty(true);
				// when the value has changed, the
				// user probably wants to use this
				// requirement
				requirement.setEnabled(true); 
			}
			

		}
		this.resourceRequirementChanged(requirement);

	}

	public void removeChangeListener(IRequirementListViewer viewer) {
		changeListeners.remove(viewer);
	}

	private void addClashes(ResourceRequirement resourceRequirement)
	{
		if (requirementClashes.get(resourceRequirement.getRequirementName()) != null) {
			Iterator<QName> resourceRequirementClashes = resourceRequirement
			.getClashes().iterator();
			while (resourceRequirementClashes.hasNext()) {
				requirementClashes
				.get(resourceRequirement.getRequirementName()).add(
						resourceRequirementClashes.next());
			}
		} else {
			requirementClashes.put(resourceRequirement.getRequirementName(),
					resourceRequirement.getClashes());
		}

		Iterator<?> clashesIterator = resourceRequirement.getClashes().iterator();
		while (clashesIterator.hasNext()) {
			QName currentClash = (QName) clashesIterator.next();
			if (requirementClashes.containsKey(currentClash)) {
				requirementClashes.get(currentClash).add(
						resourceRequirement.getRequirementName());
			} else {
				HashSet<QName> newSet = new HashSet<QName>();
				newSet.add(resourceRequirement.getRequirementName());
				requirementClashes.put(currentClash, newSet);
			}
		}
	}

	private void removeClash(QName toRemove) {
		if (requirementClashes.containsKey(toRemove)) {
			requirementClashes.remove(toRemove);
		}
		Iterator<QName> clashesIterator = requirementClashes.keySet().iterator();
		while (clashesIterator.hasNext()) {
			QName currentKey = clashesIterator.next();
			if (requirementClashes.get(currentKey).contains(toRemove)) {
				requirementClashes.get(currentKey).remove(toRemove);
			}

		}
	}

	/**
	 * uses removeResourceRequirment(ResourceRequirment) method!
	 * 
	 * @param qname
	 */
	public int removeResourceRequirement(QName qname) {
		if (qname != null) {
			ResourceRequirement resourceRequirement = getResourceRequirement(qname);
			if (resourceRequirement != null) {
				resourceRequirement.setEnabled(false);
				return removeResourceRequirement(resourceRequirement);
			}
		}
		return -1;
	}

	public int removeResourceRequirement(
			ResourceRequirement resourceRequirement) {
		int result = resourceRequirements.indexOf(resourceRequirement);
		resourceRequirements.remove(resourceRequirement);
		removeClash(resourceRequirement.getRequirementName());
		Iterator<IRequirementListViewer> iterator = changeListeners.iterator();
		while (iterator.hasNext()) {
			iterator.next()
			.notifyRequirementRemoved(resourceRequirement);
		}
		return result;
	}

	public void replaceResourceRequirement(ResourceRequirement oldValue,
			ResourceRequirement resourceRequirement) {

		int index = resourceRequirements.indexOf(oldValue);
		if(oldValue == null || index == -1)
		{
			if(resourceRequirement != null) addResourceRequirement(resourceRequirement);
			return;
		}

		resourceRequirements.remove(oldValue);
		removeClash(oldValue.getRequirementName());
		resourceRequirements.add(index,resourceRequirement);
		if(resourceRequirement != null)
		{
			addClashes(resourceRequirement);
			resourceRequirementChanged(resourceRequirement);
		}
	}

	/**
	 * uses resourceRequirementChanged(ResourceRequirement resourceRequirement)
	 * 
	 * @param qname
	 */
	public void resourceRequirementChanged(QName qname) {
		if (qname != null) {
			ResourceRequirement resourceRequirement = getResourceRequirement(qname);
			if (resourceRequirement != null) {
				resourceRequirementChanged(resourceRequirement);
			}
		}
	}

	public void resourceRequirementChanged(
			ResourceRequirement resourceRequirement) {
		Iterator<IRequirementListViewer> iterator = changeListeners.iterator();
		while (iterator.hasNext()) {
			IRequirementListViewer currentListViewer = iterator.next();
			currentListViewer.notifyRequirementUpdated(resourceRequirement);
		}
		// requirement may have become enabled -> disable clashes
		disableClashingRequirements(resourceRequirement);
	}

	public ResourceRequirement[] toArray() {
		return resourceRequirements.toArray(new ResourceRequirement[0]);
	}

}