/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.clients.common.transfers.GridFileAddress;
import com.intel.gpe.clients.impl.transfers.U6ProtocolConstants;
import com.intel.gpe.gridbeans.parameters.FileParameterValue;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in.StageInTypeU6StorageCellEditor;

/**
 * @author demuth
 * 
 */
public class StageInTypeU6StorageExtension extends AbstractStageInTypeExtension {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #attachToParameterValue
	 * (de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.IAdaptable,
	 * com.intel.gpe.gridbeans.IFileParameterValue)
	 */
	public IFileParameterValue attachToParameterValue(IAdaptable adaptable,
			IFileParameterValue oldValue) {
		IFileParameterValue input;
		GridFileAddress source = new GridFileAddress(null, "", "",
				U6ProtocolConstants.UNICORE_SMS_PROTOCOL, false);
		if (oldValue == null) {
			GridFileAddress target = new GridFileAddress(null, "", "",
					ProtocolConstants.JOB_WORKING_DIR_PROTOCOL, false);
			input = new FileParameterValue(source, target, true);
		} else {
			try {
				input = oldValue.clone();
			} catch (Exception e) {
				GPEActivator
						.log(IStatus.ERROR,
								"Could not clone file address for creating UNICORE input file.",
								e);
				input = oldValue;
			}
		}

		input.setSource(source);
		return input;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #detachFromParameterValue
	 * (de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.IAdaptable,
	 * com.intel.gpe.gridbeans.IFileParameterValue)
	 */
	@Override
	public void detachFromParameterValue(IAdaptable adaptable,
			IFileParameterValue value) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getCellEditor()
	 */
	public CellEditor getCellEditor(Composite parent, IStage stage,
			IAdaptable adaptable) {
		boolean multiFileSelector = GridBeanParameterType.FILE_SET.equals(stage
				.getGridBeanParameter().getType());
		return new StageInTypeU6StorageCellEditor(parent, multiFileSelector);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getPriority(de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext)
	 */
	public int getPriority(GridBeanContext context) {
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getType()
	 */
	public QName getType() {
		return U6ProtocolConstants.UNICORE_SMS_QNAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #isWorkflowFileType()
	 */
	@Override
	public boolean isWorkflowFileType() {
		return false;
	}

}
