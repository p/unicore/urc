package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values;

import javax.xml.namespace.QName;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.RequirementConstants;

/**
 * provides a string value for DateTimeRequirementCellEditor
 * @author rajveer
 * 
 */
public class DateTimeRequirementValue extends RequirementValue implements Cloneable{
	
	private String value;
	public static final QName TYPE = new QName(
			RequirementConstants.REQUIREMENT_TYPE_NAMESPACE,
			RequirementConstants.REQUIREMENT_TYPE_DATE);

	public DateTimeRequirementValue(String value) {		
	
		this.value = value;
	}

	@Override
	public Object clone() {
		return super.clone();
	}

	public String getDisplayedString() {
		return toString();

	}

	public QName getType() {
		return TYPE;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

}