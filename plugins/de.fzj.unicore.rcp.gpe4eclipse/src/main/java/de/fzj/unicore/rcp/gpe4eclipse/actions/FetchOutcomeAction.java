package de.fzj.unicore.rcp.gpe4eclipse.actions;

import org.eclipse.core.runtime.IStatus;
import org.unigrids.services.atomic.types.StatusType;

import com.intel.gpe.clients.api.JobClient;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.commands.FetchOutcomeCommand;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.JobNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

public class FetchOutcomeAction extends NodeAction {

	public FetchOutcomeAction(Node node) {
		super(node);
		setText("Fetch Output Files");
		setToolTipText("Download the job outcomes and visualize them with an application specific user interface.");
		setImageDescriptor(GPEActivator.getImageDescriptor("export_wiz.gif"));
	}

	@Override
	public boolean isCurrentlyAvailable() {

		if (getNode() instanceof JobNode
				&& StatusType.SUCCESSFUL.equals(((JobNode) getNode())
						.getJobStatus())) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.actions.ServiceBrowserAction#run()
	 */
	@Override
	public void run() {

		JobClient jobClient = (JobClient) getNode().getAdapter(JobClient.class);
		try {
			jobClient.getStatus();
		} catch (Exception e) {
			GPEActivator
					.log(IStatus.ERROR,
							"Unable to fetch output files. The job management service does not respond.",
							e);
		}
		FetchOutcomeCommand cmd = new FetchOutcomeCommand(jobClient);
		cmd.execute();

	}

}