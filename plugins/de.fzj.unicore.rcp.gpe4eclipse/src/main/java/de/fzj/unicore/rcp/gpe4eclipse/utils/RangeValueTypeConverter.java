package de.fzj.unicore.rcp.gpe4eclipse.utils;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlCursor;

import com.intel.gpe.clients.api.jsdl.RangeValueType;

public abstract class RangeValueTypeConverter {

	public static QName RANGE_VALUE_EXPRESSION_QNAME = new QName(
			"http://www.unicore.eu/unicore/jsdl-extensions", "expression",
			"jsdl-unicore");

	public static RangeValueType convertToJava(
			org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType xmlRange) {

		if (xmlRange == null) {
			return null;
		}
		RangeValueType gpeRange = new RangeValueType();
		
		if (xmlRange.getExactArray() != null
				&& xmlRange.getExactArray().length > 0) {
			if (xmlRange.getExactArray()[0] != null) {
				gpeRange.setExact(xmlRange.getExactArray()[0].getDoubleValue());
				gpeRange.setEpsilon(xmlRange.getExactArray()[0].getEpsilon());
			}
		}

		if (xmlRange.getRangeArray() != null
				&& xmlRange.getRangeArray().length > 0) {
			if (xmlRange.getRangeArray()[0] != null) {
				if (xmlRange.getRangeArray()[0].getLowerBound() != null) {
					gpeRange.setLowerBound(xmlRange.getRangeArray()[0]
							.getLowerBound().getDoubleValue());
					// value of schemasRange needs to be negated to fit the
					// local client schema
					gpeRange.setIncludeLowerBound(!(xmlRange.getRangeArray()[0]
							.getLowerBound().getExclusiveBound()));
				}
				if (xmlRange.getRangeArray()[0].getUpperBound() != null) {
					gpeRange.setUpperBound(xmlRange.getRangeArray()[0]
							.getUpperBound().getDoubleValue());
					gpeRange.setIncludeUpperBound(!(xmlRange.getRangeArray()[0]
							.getUpperBound().getExclusiveBound()));
				}

			}
		}

		final String expression = xmlRange.newCursor().getAttributeText(
				RANGE_VALUE_EXPRESSION_QNAME);
		if (expression != null) {
			gpeRange.setExpression(expression);
		}

		return gpeRange;
	}

	public static org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType convertToXBean(
			RangeValueType gpeRange) {

		if (gpeRange == null) {
			return null;
		}
		org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType xmlRange = org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType.Factory
				.newInstance();
		if (!Double.isNaN(gpeRange.getExact())) {
			xmlRange.addNewExact().setDoubleValue(gpeRange.getExact());
			if (!Double.isNaN(gpeRange.getEpsilon())) {
				xmlRange.getExactArray(0).setEpsilon(gpeRange.getEpsilon());
			}
		}
		boolean lowerBound = !Double.isNaN(gpeRange.getLowerBound())
				&& !Double.isInfinite(gpeRange.getLowerBound());
		boolean upperBound = !Double.isNaN(gpeRange.getUpperBound())
				&& !Double.isInfinite(gpeRange.getUpperBound());
		if (lowerBound || upperBound) {
			xmlRange.addNewRange();
			if (lowerBound) {
				xmlRange.getRangeArray(0).addNewLowerBound()
						.setDoubleValue(gpeRange.getLowerBound());
				xmlRange.getRangeArray(0).getLowerBound()
						.setExclusiveBound(!gpeRange.isIncludeLowerBound());
			}
			if (upperBound) {
				xmlRange.getRangeArray(0).addNewUpperBound()
						.setDoubleValue(gpeRange.getUpperBound());
				xmlRange.getRangeArray(0).getUpperBound()
						.setExclusiveBound(!gpeRange.isIncludeUpperBound());
			}
		}
		if (gpeRange.isExpression()) {
			XmlCursor cursor = xmlRange.newCursor();
			cursor.toNextToken();
			cursor.insertAttributeWithValue(RANGE_VALUE_EXPRESSION_QNAME,
					gpeRange.getExpression());
		}

		return xmlRange;
	}

}
