/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing;

import java.util.List;

import org.eclipse.core.runtime.IAdaptable;

import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in.StageInList;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out.StageOutList;

/**
 * @author demuth
 * 
 */
public class StageListFactory {

	public static StageInList createStageInList(IAdaptable adaptable) {
		return createStageInList(getGridBean(adaptable), adaptable);
	}

	public static StageInList createStageInList(IGridBean gridBean,
			IAdaptable adaptable) {
		StageInList stageInList = new StageInList(gridBean, adaptable);
		List<IGridBeanParameter> params = gridBean.getInputParameters();
		for (int i = 0; i < params.size(); i++) {
			IGridBeanParameter param = params.get(i);
			IGridBeanParameterValue value = (IGridBeanParameterValue) gridBean
					.get(param.getName());

			if (GridBeanParameterType.FILE.equals(param.getType())
					|| GridBeanParameterType.FILE_SET.equals(param.getType())) {
				stageInList.addParameter(param, value);
			}
		}
		return stageInList;
	}

	public static StageOutList createStageOutList(IAdaptable adaptable) {
		return createStageOutList(getGridBean(adaptable), adaptable);
	}

	public static StageOutList createStageOutList(IGridBean gridBean) {
		return createStageOutList(gridBean, null);
	}

	protected static StageOutList createStageOutList(IGridBean gridBean,
			IAdaptable adaptable) {
		StageOutList stageOutList = new StageOutList(gridBean, adaptable);
		List<IGridBeanParameter> params = gridBean.getOutputParameters();
		for (int i = 0; i < params.size(); i++) {
			IGridBeanParameter param = params.get(i);
			IGridBeanParameterValue value = (IGridBeanParameterValue) gridBean
					.get(param.getName());
			if (GridBeanParameterType.FILE.equals(param.getType())
					|| GridBeanParameterType.FILE_SET.equals(param.getType())) {
				stageOutList.addParameter(param, value);
			}
		}
		return stageOutList;
	}

	protected static IGridBean getGridBean(IAdaptable adaptable) {
		if (adaptable != null) {
			Object adapter = adaptable.getAdapter(IGridBean.class);
			if (adapter != null) {
				return (IGridBean) adapter;
			}
		}
		return null;
	}
}
