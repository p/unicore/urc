/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.Status;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.RequirementConstants;

/**
 * Holds a List of enum values that can be selected, e.g. through a combo box.
 * Allows for displaying just a subset of enum values by providing an Integer[]
 * with indices of enum values to display. In this case, all other enum values
 * are still valid but should not be suggested to the user. The user may freely
 * enter a String instead of an integer to specify which enum value he would
 * like to set. Note that this not only requires transforming enum values to
 * Strings, but also Strings to enum values. For this to work, we apply one of
 * two heuristics, depending on whether the enum implements a static method that 
 * converts from String to enum values (let's assume it is called "fromString"):
 * 1. Use {@link Object#toString()} in combination with fromString if possible
 * 2. Use {@link Enum#name()} in combination with
 *  {@link Enum#valueOf(Class, String)} if there is no such method.
 *  
 *  Note also, that in the second case, the names of enum values as seen by
 *  programmers are exposed to the user, so better implement toString and 
 *  fromString properly! 
 *  If none of the two heuristics is acceptable for you, you may subclass and
 *  override {@link #toString()} and {@link #fromString(String)}.
 * 
 * 
 * @param <E>
 */

public class EnumRequirementValue<E> extends RequirementValue implements
Cloneable {
	public static final QName TYPE = new QName(
			RequirementConstants.REQUIREMENT_TYPE_NAMESPACE,
			RequirementConstants.REQUIREMENT_TYPE_ENUM);
	private int selection = 0;

	private E selected;

	private List<E> validValues = new ArrayList<E>();

	private List<E> notDisplayed;

	private Class<E> itemType = null;

	private String toStringMethodName = null;
	private String fromStringMethodName = null;

	@SuppressWarnings({"rawtypes" })
	EnumRequirementValue(Class enumType) {
		this(enumType,0);
	}

	/**
	 * 
	 * @param enumType
	 *            is a class of the type of enum
	 * @param selection
	 *            is the initial selection
	 */
	public EnumRequirementValue(Class<?> enumType, int selection) {
		this(enumType, null, null, null, null);
		setSelection(selection);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public EnumRequirementValue(Class enumType, E selected, Integer[] displayOnly, String toStringMethodName, String fromStringMethodName) {
		
		List<E> validValues = new ArrayList<E>();
		EnumSet elements = EnumSet.allOf(enumType);
		Iterator iterator = elements.iterator();
		while (iterator.hasNext()) {	
			E next = (E) iterator.next();
			validValues.add(next);
		}
		init(validValues,enumType,selected,displayOnly,fromStringMethodName);
		setToStringMethodName(enumType, toStringMethodName);
		
	}



	/**
	 * Constructor with an explicit list of valid values.
	 * @param validValuesList
	 */
	public EnumRequirementValue(List<E> validValuesList) {
		this(validValuesList, null, null, null, null);

	}

	public EnumRequirementValue(List<E> validValuesList, Class<E> itemType, E selected, Integer[] displayOnly, String fromStringMethodName) {
		init(validValuesList, itemType, selected, displayOnly, fromStringMethodName);

	}

	public String isValid() {
		if(validator != null) return super.isValid();
		if(getElements() == null) return null;
		Object selected = getSelectedElement();
		if(getElements().contains(selected)) return null;
		if(getNotDisplayed() != null && getNotDisplayed().contains(selected)) return null;
		return "Invalid value selected.";
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void init(List<E> validValuesList, Class<E> itemType, E selected, Integer[] displayOnly, String fromStringMethodName) {
		this.itemType = itemType;

		List<Integer> displayOnlyList = displayOnly == null ? null : Arrays.asList(displayOnly);
		Iterator iterator = validValuesList.iterator();
		int i = 0;
		while (iterator.hasNext()) {	
			E next = (E) iterator.next();
			this.validValues.add(next);
			if(next.equals(selected))
			{
				if(displayOnlyList == null || displayOnlyList.contains(i))
				{
					selection = i;
				}
				else selection = -1;
			}
			i++;
		}

		if(displayOnly != null)
		{
			List<E> cleanValidValues = new ArrayList<E>();
			for(int j : displayOnly)
			{
				cleanValidValues.add(validValues.get(j));
			}

			validValues.removeAll(cleanValidValues);
			notDisplayed = validValues;
			validValues = cleanValidValues;

			setFromStringMethodName(fromStringMethodName);
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public EnumRequirementValue clone() {
		return (EnumRequirementValue) super.clone();
	}

	public String getDisplayedString() {
		return toString();

	}

	public List<E> getElements() {
		return this.validValues;
	}

	/**
	 * 
	 * @return the enum that is selected or null if none
	 */
	public E getSelectedElement() {
		return selected;
	}

	public int getSelection() {
		return selection;
	}

	public QName getType() {
		return TYPE;
	}

	/**
	 * set the Integer selection to the value that corresponds to the given enum
	 * 
	 * @param element
	 *            is the enum that should set selected
	 */
	public void setSelectedElement(E element) {
		selected = element;
		if(element != null)
		{
			int i = 0;
			for (Iterator<E> iterator = validValues.iterator(); iterator
			.hasNext();) {
				E next = iterator.next();

				// elements is generated by xml, enum.equals is final and compares
				// with '==' so next.equals(element) is false, but the results of
				// .toString() are equal

				if (next.equals(element)
						|| toString(next).equals(toString(element))) {
					selection = i;
					return;
				}
				i++;
			}
		}
		selection = -1;
	}

	public void setSelection(int selection) {
		this.selection = selection;
		if(selection >= 0 && selection < validValues.size())
		{
			selected = validValues.get(selection);
		}
		else selected = null;
	}

	/**
	 * replaces the current list with the validValues, if selection is no more
	 * valid, first element of valid values is set as selection
	 * 
	 * @param validValues
	 */
	public void setValidElements(List<E> validValuesList) {
		if (validValuesList != null && !(validValuesList.isEmpty())) {
			E previousSelected = this.getSelectedElement();
			this.validValues = validValuesList;
			if (validValues.contains(previousSelected)) {
				setSelectedElement(previousSelected);
			} else {
				setSelectedElement(validValues.iterator().next());
			}
		}
	}

	@Override
	public String toString() {
		return toString(getSelectedElement());
	}

	/**
	 * 
	 * @return an Array of the EnumNames
	 */
	public String[] toStringArray() {
		ArrayList<String> strings = new ArrayList<String>();
		for (Iterator<E> iterator = getElements().iterator(); iterator
		.hasNext();) {
			strings.add(toString(iterator.next()));
		}
		return strings.toArray(new String[strings.size()]);
	}

	public List<E> getNotDisplayed() {
		return notDisplayed;
	}

	public void setNotDisplayed(List<E> notDisplayed) {
		this.notDisplayed = notDisplayed;
	}


	@SuppressWarnings("rawtypes")
	public String toString(E element)
	{

		Method toMethod = null;
		if(toStringMethodName != null)
		{
			try {
				toMethod = element.getClass().getMethod(toStringMethodName);
			} catch (Exception e) {
				// should not happen - tested before
			}
		}
		if(toMethod != null)
		{
			try {
				return (String) toMethod.invoke(element);
			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, "Unable to convert enumeration value to String: "+e.getMessage(),e);
				return element.toString();
			}
		}
		else if((element instanceof Enum) && getNotDisplayed() != null && getNotDisplayed().size() > 0)
		{
			// fall back to heuristic 2
			return ((Enum) element).name();
		}

		return element.toString();

	}

	@SuppressWarnings({ "unchecked", "rawtypes"})
	public E fromString(String s) throws IllegalArgumentException
	{
		E result = null;
		if(itemType != null)
		{
			Method fromMethod = null;
			if(fromStringMethodName != null)
			{
				try {
					fromMethod = itemType.getMethod(fromStringMethodName, String.class);
				} catch (Exception e) {
					// should not happen - tested before
				}
			}

			if(fromMethod != null)
			{
				try {
					result = (E) fromMethod.invoke(null, s);
				} catch (Exception e) {
					if(e instanceof IllegalArgumentException)
					{
						throw (IllegalArgumentException) e;
					}
					else throw new IllegalArgumentException(e);
				}
			}
			else
			{
				Class type = itemType;
				if (type.getSuperclass() != Enum.class) {
					type = type.getSuperclass(); // polymorphic enums
				}
				result = (E) Enum.valueOf(type, s);
			}
		}
		if(result == null) throw new IllegalArgumentException(s);
		else return result;
	}

	public void setToStringMethodName(Class<E> enumType, String toStringMethodName) {
		if(toStringMethodName == null)
		{
			this.toStringMethodName = null;
		}
		else
		{
			// test whether the class has a toString method (which may be named
			// differently)..
			Method m = null;
			try {
				m = enumType.getMethod(toStringMethodName);
			} catch (Exception e) {
				toStringMethodName = null;
				return;
			}
			int modifiers = m.getModifiers();
			if(Modifier.isPublic(modifiers) && String.class.equals(m.getReturnType()))
			{
				this.toStringMethodName = toStringMethodName;
				this.itemType = enumType;
			}

		}
	}

	public void setFromStringMethodName(String fromStringMethodName) {
		if(fromStringMethodName == null)
		{
			this.fromStringMethodName = null;
		}
		else
		{
			// test whether the enum has a fromString method..
			try {
				Method m = itemType.getMethod(fromStringMethodName, String.class);
				int modifiers = m.getModifiers();
				if(Modifier.isPublic(modifiers) && Modifier.isStatic(modifiers) && itemType.equals(m.getReturnType()))
				{
					this.fromStringMethodName = fromStringMethodName;
				}
			} catch (Exception e) {
				//do nothing
			}
		}
	}
}