/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.clients.api.GridBean;

import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeData;
import de.fzj.unicore.rcp.servicebrowser.nodes.WebServiceNode;

/**
 * @author demuth
 * 
 */
public class GridBeanServiceNode extends WebServiceNode {

	private static final long serialVersionUID = 3558005399929318960L;

        private static final String NS="http://gpe.intel.com/services/gbs";
        public static final String TYPE = "GridBeanService";
        public static final QName PORT = new QName(NS,TYPE);

	private boolean registered = false;

	public static String PROPERTY_GRIDBEANS = "gridbeans";

	public GridBeanServiceNode(EndpointReferenceType epr) {
		super(epr);
	}


	/**
	 * Removes the node from the application registry.
	 */
	@Override
	public void dispose() {
		super.dispose();
		if (registered) {
			GPEActivator.getDefault().getGridBeanServiceRegistry()
					.unregisterService(getEpr());
		}

	}

	@Override
	public DetailsAdapter getDetails() {
		DetailsAdapter details = super.getDetails();
		Map<String, String> hm = details.getMap();
		List<GridBean> gbs = getGridBeans();
		if (gbs != null && !gbs.isEmpty()) {
			StringBuffer sb = new StringBuffer();
			for (GridBean gb : gbs) {
				sb.append(gb.getName());
				sb.append(" (Version ");
				sb.append(gb.getVersion()).append(")");
				sb.append(System.getProperty("line.separator"));
			}
			sb.deleteCharAt(sb.length() - 1);
			hm.put("Available application GUIs", sb.toString());
		}

		return details;

	}

	/**
	 * @return the list of available applications.
	 */
	@SuppressWarnings("unchecked")
	public List<GridBean> getGridBeans() {
		INodeData data = getData();
		if (data == null) {
			return null;
		}
		Object o = data.getProperty(PROPERTY_GRIDBEANS);
		if (o == null) {
			return null;
		}
		return (List<GridBean>) o;

	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void retrieveName() {
		super.retrieveName();
	}


}
