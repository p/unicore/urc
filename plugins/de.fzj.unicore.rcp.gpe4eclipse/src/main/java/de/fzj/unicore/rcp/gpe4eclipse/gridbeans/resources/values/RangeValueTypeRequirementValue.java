/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values;

import javax.xml.namespace.QName;

import com.intel.gpe.clients.api.jsdl.RangeValueType;
import com.intel.gpe.gridbeans.jsdl.GPEJSDLUtil;

import de.fzj.unicore.rcp.common.utils.RangeValueUtils;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.RequirementConstants;
import de.fzj.unicore.rcp.gpe4eclipse.utils.RangeValueFormattingUtils;

/**
 * Provides the value for the RangeValueRequirementCellEditor.
 * 
 * @author Christian Hohmann
 */
public class RangeValueTypeRequirementValue extends RequirementValue implements
Cloneable {

	public static final QName TYPE = new QName(
			RequirementConstants.REQUIREMENT_TYPE_NAMESPACE,
			RequirementConstants.REQUIREMENT_TYPE_RANGEVALUE);

	// the specified valid bounds for the Range, if an exactValue is specified,
	// this is the default Value for exact of the chosen Value
	RangeValueType validValue;
	// the user chosen bounds that should be transmitted
	RangeValueType chosenValue;

	private boolean wholeNumbersOnly = true;

	public RangeValueTypeRequirementValue(RangeValueType validValue) {
		this(validValue, null);
	}

	public RangeValueTypeRequirementValue(RangeValueType validValue,
			RangeValueType chosenValue) {
		this(validValue, chosenValue, RequirementConstants.NO_UNIT,
				RequirementConstants.NO_UNIT, null);
	}

	public RangeValueTypeRequirementValue(RangeValueType validValue,
			RangeValueType chosenValue, String internalUnit, String chosenUnit,
			String[] selectableUnits) {
		this(validValue, chosenValue, internalUnit, chosenUnit,
				selectableUnits, true);
	}

	public RangeValueTypeRequirementValue(RangeValueType validValue,
			RangeValueType chosenValue, String internalUnit, String chosenUnit,
			String[] selectableUnits, boolean wholeNumberOnly) {
		if (validValue == null) {
			this.validValue = new RangeValueType(0.0);
		} else {
			this.validValue = validValue;
		}
		if (chosenValue == null) {
			this.chosenValue = new RangeValueType(getDefaultValue(validValue,
					wholeNumbersOnly));
		} else {
			this.chosenValue = chosenValue;
		}
		this.wholeNumbersOnly = wholeNumberOnly;
		setInternalUnit(internalUnit);
		setSelectedUnit(chosenUnit);
		setSelectableUnits(selectableUnits);

	}

	@Override
	public RangeValueTypeRequirementValue clone() {
		RangeValueTypeRequirementValue result = (RangeValueTypeRequirementValue) super
		.clone();
		result.setValidValue(validValue.clone());
		result.setChosenValue(chosenValue.clone());
		return result;
	}

	/**
	 * Returns the value that was chosen by the user in the INTERNAL unit.
	 * 
	 * @return
	 */
	public RangeValueType getChosenValue() {
		return chosenValue;
	}

	public String getDisplayedString() {
		return toString();

	}

	public QName getType() {
		return TYPE;
	}

	
	public String isValid() {
		if(validator != null) return super.isValid();
		RangeValueType chosen = getChosenValue();
		if(chosen == null) return "No value selected.";
		RangeValueType valid = getValidValue();
		if(valid == null) return null;
		org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType jsdlValid =
		org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType.Factory.newInstance();
		GPEJSDLUtil.setRangeValue(valid,jsdlValid);
		org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType jsdlChosen =
		org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType.Factory.newInstance();
		GPEJSDLUtil.setRangeValue(chosen,jsdlChosen);
		if(RangeValueUtils.fitsInto(jsdlChosen, jsdlValid)) return null;
		return "Selected value is out of range";
	}
	
	/**
	 * Returns the set of valid values for this resource requirement on the
	 * selected target system in the INTERNAL unit.
	 * 
	 * @return
	 */
	public RangeValueType getValidValue() {
		return validValue;
	}

	public boolean isWholeNumbersOnly() {
		return wholeNumbersOnly;
	}

	public void setChosenValue(RangeValueType chosenValue) {
		this.chosenValue = chosenValue;
	}

	public void setValidValue(RangeValueType validValue) {
		this.validValue = validValue;
	
	}
	
	/**
	 * Can be used to set the chosen value to a default given by the valid
	 * value (uses either its exact value or a value in the middle of a range).
	 * CAUTION: this must not be called before a valid value has been set!
	 * 
	 * TODO if no resource was selected the chosen value should be NaN, not = 1
	 */
	public void setChosenValueToDefault()
	{
		double current = getChosenValue().getExact();
		double newDefault = getDefaultValue(validValue, this.wholeNumbersOnly);
		if(Double.isNaN(current) || (current==1 && current<=newDefault)){
			getChosenValue().setExact(newDefault);
		}
	}

	@Override
	public String toString() {
		return RangeValueFormattingUtils.formatRangeValue(chosenValue,
				getInternalUnit(), getSelectedUnit());
	}

	public static double getDefaultValue(RangeValueType validValues,
			boolean wholeNumbersOnly) {
		Double def;
		def = validValues.getExact();
		if (def == null || def.isNaN()) {
			if (validValues.isIncludeLowerBound()) {
				def = validValues.getLowerBound();
			} else {
				def = (validValues.getUpperBound() - validValues
						.getLowerBound()) / 2;
				if (wholeNumbersOnly) {
					def = Math.ceil(def);
				}
			}
		}
		return def;
	}

}