package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;

import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.IFileSetParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;

@XStreamAlias("GPEWorkflowFileSetSink")
@SuppressWarnings("unchecked")
public class GPEWorkflowFileSetSink extends DataSink {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8745832214802561131L;

	public static final QName TYPE = new QName("http://www.unicore.eu/",
			"GPEFileSetSink");

	private IGridBeanParameter parameter;

	public GPEWorkflowFileSetSink(IActivity activity,
			IGridBeanParameter parameter) {
		super(activity);
		this.parameter = parameter;
		setId(parameter.getName().toString());
		setName(parameter.getDisplayedName());
		IGridBean gb = (IGridBean) activity.getAdapter(IGridBean.class);
		IFileSetParameterValue value = (IFileSetParameterValue) gb
				.get(parameter.getName());
		setPropertyValue(PROP_ACCEPTED_DATA_TYPES,
				new HashSet<String>(value.getMimeTypes()));
	}

	public IActivity getActivity() {
		return (IActivity) super.getFlowElement();
	}

	@Override
	public Set<String> getIncomingDataTypes() {
		return (Set<String>) getPropertyValue(PROP_ACCEPTED_DATA_TYPES);
	}

	public IGridBeanParameter getParameter() {
		return parameter;
	}

	@Override
	public QName getSinkType() {
		return TYPE;
	}

	public boolean isVisible() {
		IFlowElement element = getFlowElement();
		if (element == null) {
			return false;
		}
		ShowDataFlowsProperty prop = (ShowDataFlowsProperty) element
				.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		if (prop == null || !prop.isShowingSinks()) {
			return false;
		}
		if (!prop.isShowingSinks()) {
			return false;
		}
		if (getIncomingDataTypes().size() == 0) {
			return prop.isShowingFlowsWithDataType("text/any");
		} else {
			return prop.isShowingFlowsWithDataTypes(getIncomingDataTypes());
		}
	}

	public void setParameter(IGridBeanParameter parameter) {
		this.parameter = parameter;
	}

}
