package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import com.intel.gpe.clients.api.Job;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.ResourceProviderAdapter;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;

public class TSFMatchmaker extends TSMatchmaker {

	public TSFMatchmaker(NodePath nodePath) {
		super(nodePath);
	}

	public String canHandleJob(Job j) {
		TargetSystemFactoryNode n = (TargetSystemFactoryNode) NodeFactory
				.createNode(getEPR());
		ResourceProviderAdapter props = new ResourceProviderAdapter(
				n.getTargetSystemFactoryProperties());
		try {
			return canHandleJob(j, props, n.getURI());
		} finally {
			n.dispose();
		}
	}
}
