package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types;

import com.intel.gpe.clients.api.jsdl.RangeValueType;


public class SiteSpecificRequirementSettingType extends AbstractSSRSettingType {
	protected RangeValueType range;

	public SiteSpecificRequirementSettingType() {
	}

	public SiteSpecificRequirementSettingType(String name, RangeValueType range) {
		super(name);
		this.range = range;
	}

	public RangeValueType getRange() {
		return range;
	}

	public void setRange(RangeValueType range) {
		this.range = range;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((range == null) ? 0 : range.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SiteSpecificRequirementSettingType other = (SiteSpecificRequirementSettingType) obj;
		if (range == null) {
			if (other.range != null)
				return false;
		} else if (!range.equals(other.range))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return enabled ? "SiteSpecificRequirementSettingType [range=" + range + "]" : "disabled";
	}
}
