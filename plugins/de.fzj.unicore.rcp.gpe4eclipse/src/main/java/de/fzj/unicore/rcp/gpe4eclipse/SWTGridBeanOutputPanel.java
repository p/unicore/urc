/*
 * Copyright (c) 2000-2005, Intel Corporation All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Intel Corporation nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.fzj.unicore.rcp.gpe4eclipse;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.clients.all.gridbeans.GridBeanJob;
import com.intel.gpe.clients.all.gridbeans.panels.BaseGridBeanOutputPanel;
import com.intel.gpe.gridbeans.plugins.DataSetException;
import com.intel.gpe.gridbeans.plugins.IGridBeanPlugin;
import com.intel.gpe.gridbeans.plugins.IPanel;
import com.intel.gpe.gridbeans.plugins.TranslationException;

import de.fzj.unicore.rcp.common.utils.swing.SafeSwingUtilities;
import de.fzj.unicore.rcp.gpe4eclipse.views.JobOutcomeView;

/**
 * 
 * @author Alexander Lukichev, demuth
 * @version $Id: SwingGridBeanProvider.java,v 1.5 2006/12/26 08:29:54 dizhigul
 *          Exp $
 * 
 *          This class creates an SWT view from a GridBean. It serves as a
 *          bridge between a GridBeanClient which uses Swing panels and the SWT
 *          view.
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class SWTGridBeanOutputPanel extends
		BaseGridBeanOutputPanel<IWorkbenchPart> {

	protected JobOutcomeView view;
	protected GridBeanClient<IWorkbenchPart> gbClient;
	private TextPanel stdoutArea, stderrArea, logArea;
	protected boolean wrappingSwingComponents;

	protected static Logger logger = Logger
			.getLogger(SWTGridBeanOutputPanel.class.getName());

	public SWTGridBeanOutputPanel(GridBeanClient<IWorkbenchPart> gbClient,
			boolean hasStdFiles, boolean wrappingSwingComponents) {
		super(gbClient.getGridBeanPlugin(), gbClient.getClient(), hasStdFiles);
		this.gbClient = gbClient;
		this.wrappingSwingComponents = wrappingSwingComponents;
		buildComponents(hasStdFiles);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.panels.BaseGridBeanOutputPanel#buildComponents(
	 * boolean)
	 */
	protected void buildComponents(boolean hasStdFiles) {
		// Show Output panels in a tab folder within a View
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		try {
			view = (JobOutcomeView) page.showView(
					"de.fzj.unicore.rcp.gpe4eclipse.views.JobOutcomeView", ""
							+ UUID.randomUUID(), IWorkbenchPage.VIEW_ACTIVATE);
		} catch (PartInitException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		view.setGridBeanClient(gbClient);

		List<IPanel> panelsToAdd = new ArrayList<IPanel>();
		// show the output panels defined by the grid bean plugin
		IPanel[] outputPanels = plugin.getOutputPanels();
		if (outputPanels != null) {
			for (IPanel panel : outputPanels) {
				panelsToAdd.add(panel);
			}
		}

		// show standard out and standard error of the application
		if (hasStdFiles) {

			stdoutArea = new TextPanel(SWT.BORDER | SWT.V_SCROLL
					| SWT.READ_ONLY);
			stdoutArea.setName("Stdout");
//			Display display = GPEActivator.getDefault().getWorkbench()
//					.getDisplay();
			// stdoutArea.getComponent().setFont(new Font(display, "Courier",
			// 11, SWT.NORMAL));
			// stdoutArea.getComponent().setBounds(10, 10, 100, 100);
			panelsToAdd.add(stdoutArea);

			stderrArea = new TextPanel(SWT.BORDER | SWT.V_SCROLL
					| SWT.READ_ONLY);
			stderrArea.setName("Stderr");
			// stderrArea.getComponent().setFont(new Font(display, "Courier",
			// 11, SWT.NORMAL));
			// stderrArea.getComponent().setBounds(10,90,200,100);
			// stderrArea.getComponent().setBackground(new
			// Color(display,0,1,0));
			panelsToAdd.add(stderrArea);

			logArea = new TextPanel(SWT.BORDER | SWT.V_SCROLL | SWT.READ_ONLY);
			logArea.setName("Log");
			// logArea.getComponent().setFont(new Font(display, "Courier", 11,
			// SWT.NORMAL));
			// logArea.getComponent().setBounds(10, 10, 100, 100);
			panelsToAdd.add(logArea);

		}
		view.setPanels(panelsToAdd.toArray(new IPanel[panelsToAdd.size()]));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.panels.IGridBeanOutputPanel#getComponent()
	 */
	@Override
	public IViewPart getComponent() {
		return view;
	}

	public boolean isWrappingSwingComponents() {
		return wrappingSwingComponents;
	}

	@Override
	public void load(IGridBeanPlugin<IWorkbenchPart> plugin, GridBeanJob job)
			throws DataSetException, TranslationException,
			FileNotFoundException {
		if (isWrappingSwingComponents()) {
			SwingRunnable r = null;
			try {
				Method m = SWTGridBeanOutputPanel.class.getDeclaredMethod(
						"superLoad", IGridBeanPlugin.class, GridBeanJob.class);
				r = new SwingRunnable(this, m, plugin, job);
				SafeSwingUtilities.invokeAndWait(r);
			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, "Error while reading values from the application GUI's internal model into the GUI: "+e.getMessage(), e);
				return;
			}
			Object o = r.getResult();
			if (o instanceof InvocationTargetException) {
				super.load(plugin, job);
			} else if (o instanceof DataSetException) {
				throw (DataSetException) o;
			} else if (o instanceof TranslationException) {
				throw (TranslationException) o;
			} else if (o instanceof FileNotFoundException) {
				throw (FileNotFoundException) o;
			}
		} else {
			super.load(plugin, job);
		}
	}

	public void setWrappingSwingComponents(boolean wrappingSwingComponents) {
		this.wrappingSwingComponents = wrappingSwingComponents;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.panels.BaseGridBeanOutputPanel#showExecutionLog
	 * (java.lang.String)
	 */
	@Override
	protected void showExecutionLog(final String log) {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				try {
					logArea.setText(log);
				} catch (Exception e) {
					GPEActivator.log("Could not obtain execution log of job.",
							e);
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.panels.BaseGridBeanOutputPanel#showStderr(java.
	 * io.InputStream)
	 */
	@Override
	protected void showStderr(final InputStream stderr) {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				try {
					stderrArea.setText(stream2String(stderr));
				} catch (IOException e) {
					GPEActivator.log("Could not obtain stderr of job.", e);
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.panels.BaseGridBeanOutputPanel#showStdout(java.
	 * io.InputStream)
	 */
	@Override
	protected void showStdout(final InputStream stdout) {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				try {
					stdoutArea.setText(stream2String(stdout));
				} catch (IOException e) {
					GPEActivator.log("Could not obtain stdout of job.", e);
				}
			}
		});
	}

	@Override
	public void store(final IGridBeanPlugin plugin, final GridBeanJob job)
			throws DataSetException, TranslationException {

		if (isWrappingSwingComponents()) {
			SwingRunnable r = null;
			try {
				Method m = SWTGridBeanInputPanel.class.getDeclaredMethod(
						"superStore", IGridBeanPlugin.class, GridBeanJob.class);
				r = new SwingRunnable(this, m, plugin, job);
				SafeSwingUtilities.invokeAndWait(r);
			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, "Error while carrying values from the application GUI over to its internal model: "+e.getMessage(), e);
				return;
			}
			Object o = r.getResult();
			if (o instanceof InvocationTargetException) {

				super.store(plugin, job);
			} else if (o instanceof DataSetException) {
				throw (DataSetException) o;
			} else if (o instanceof TranslationException) {
				throw (TranslationException) o;
			}
		} else {
			super.store(plugin, job);
		}
	}

	private String stream2String(InputStream in) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String result = "";
		String next;
		next = br.readLine();

		while (next != null) {
			result = result + "\n" + next;
			next = br.readLine();
		}
		return result;
	}

	public void superLoad(IGridBeanPlugin<IWorkbenchPart> plugin,
			GridBeanJob job) throws DataSetException, TranslationException,
			FileNotFoundException {
		super.load(plugin, job);
	}

	public void superStore(final IGridBeanPlugin plugin, final GridBeanJob job)
			throws DataSetException, TranslationException {
		super.store(plugin, job);
	}

}
