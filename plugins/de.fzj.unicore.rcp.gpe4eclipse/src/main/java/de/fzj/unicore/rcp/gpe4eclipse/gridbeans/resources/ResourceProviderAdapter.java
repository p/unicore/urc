package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import org.ggf.schemas.jsdl.x2005.x11.jsdl.OperatingSystemType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType;
import org.unigrids.services.atomic.types.AvailableResourceType;
import org.unigrids.services.atomic.types.ProcessorType;
import org.unigrids.services.atomic.types.SiteResourceType;
import org.unigrids.x2006.x04.services.tsf.TargetSystemFactoryPropertiesDocument.TargetSystemFactoryProperties;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument.TargetSystemProperties;

import eu.unicore.jsdl.extensions.ExecutionEnvironmentDescriptionDocument.ExecutionEnvironmentDescription;

/**
 * Wraps {@link TargetSystemProperties} or {@link TargetSystemFactoryProperties}
 * and provides a common interface, useful for getting resources offered by a site.
 * The implementation is trivial, needed only as there is no inheritance in the XSD 
 * (and thus XmlBeans) types.
 * 
 * @author K. Benedyczak
 *
 */
public class ResourceProviderAdapter {
	private TargetSystemFactoryProperties tsfProperties;
	private TargetSystemProperties tssProperties;
	
	public ResourceProviderAdapter(TargetSystemFactoryProperties tsfProperties) {
		this.tsfProperties = tsfProperties;
		this.tssProperties = null;
	}

	public ResourceProviderAdapter(TargetSystemProperties tssProperties) {
		this.tsfProperties = null;
		this.tssProperties = tssProperties;
	}
	
	public ProcessorType getProcessor()	{
		if(tsfProperties != null) return tsfProperties.getProcessor();
		else if(tssProperties != null) return tssProperties.getProcessor();
		else return null;
	}

	
	public RangeValueType getIndividualCPUCount() {
		if(tsfProperties != null) return tsfProperties.getIndividualCPUCount();
		else if(tssProperties != null) return tssProperties.getIndividualCPUCount();
		else return null;
			
	}
	
	public ExecutionEnvironmentDescription[] getExecutionEnvironmentDescriptionArray() {
		if(tsfProperties != null) return tsfProperties.getExecutionEnvironmentDescriptionArray();
		else if(tssProperties != null) return tssProperties.getExecutionEnvironmentDescriptionArray();
		else return new ExecutionEnvironmentDescription[0];
	}
	
	public RangeValueType getTotalResourceCount() {
		if(tsfProperties != null) return tsfProperties.getTotalResourceCount();
		else if(tssProperties != null) return tssProperties.getTotalResourceCount();
		else return null;
	}
	
	public OperatingSystemType[] getOperatingSystemArray() {
		if(tsfProperties != null) return tsfProperties.getOperatingSystemArray();
		else if(tssProperties != null) return new OperatingSystemType[]{tssProperties.getOperatingSystem()};
		else return new OperatingSystemType[0];
	}
	
	public RangeValueType[] getIndividualPhysicalMemoryArray() {
		if(tsfProperties != null) return tsfProperties.getIndividualPhysicalMemoryArray();
		else if(tssProperties != null) return new RangeValueType[]{tssProperties.getIndividualPhysicalMemory()};
		else return new RangeValueType[0];
	}
	
	public SiteResourceType[] getSiteResourceArray() {
		if(tsfProperties != null) return tsfProperties.getSiteResourceArray();
		else if(tssProperties != null) return tssProperties.getSiteResourceArray();
		else return new SiteResourceType[0];
	}
	
	public AvailableResourceType[] getAvailableResourceArray() {
		if(tsfProperties != null) return tsfProperties.getAvailableResourceArray();
		else if(tssProperties != null) return tssProperties.getAvailableResourceArray();
		else return new AvailableResourceType[0];
	}
	
	public RangeValueType getTotalCPUCount() {
		if(tsfProperties != null) return tsfProperties.getTotalCPUCount();
		else if(tssProperties != null) return tssProperties.getTotalCPUCount();
		else return null;
	}
	
	public RangeValueType getIndividualCPUTime() {
		if(tsfProperties != null) return tsfProperties.getIndividualCPUTime();
		else if(tssProperties != null) return tssProperties.getIndividualCPUTime();
		else return null;
	}
}
