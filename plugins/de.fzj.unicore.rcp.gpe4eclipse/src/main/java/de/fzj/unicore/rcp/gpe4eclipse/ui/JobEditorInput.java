package de.fzj.unicore.rcp.gpe4eclipse.ui;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.IURIEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GridBeanActivity;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GPEPathUtils;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;

public class JobEditorInput implements IURIEditorInput, IPathEditorInput,
IPersistableElement {

	private GridBeanActivity activity;
	private String activityName;
	private GridBeanClient<IWorkbenchPart> gridBeanClient;
	private WFEditor wfEditor = null;
	private boolean invalidWhilePersisting = false;
	private long checksum;
	private IWorkbenchPage workbenchPage;

	/**
	 * Absolute (!) path pointing to the job file
	 */
	private IPath path;

	/**
	 * This constructor should only be used when persisting the JobEditorInput
	 * was unsuccessful.
	 */
	JobEditorInput() {

	}

	/**
	 * Creates a new JobEditorInput for an existing {@link GridBeanActivity). The 
	 * GridBeanClient and an absolute file path are extracted from the activity.
	 * @param activity
	 */
	public JobEditorInput(GridBeanActivity activity) {
		this.activity = activity;
		this.gridBeanClient = activity.getGridBeanClient();
		IProject p = activity.getDiagram().getProject();

		this.path = p.getLocation().append(activity.getJobFile());
		checksum = computeChecksum(this.path);
	}

	/**
	 * Creates a new JobEditorInput linked to an existing {@link GridBeanClient}
	 * and a file that is referenced by an absolute (!) path.
	 * 
	 * @param gridBeanClient
	 * @param path
	 */
	public JobEditorInput(GridBeanClient<IWorkbenchPart> gridBeanClient,
			IPath path) {
		super();
		this.gridBeanClient = gridBeanClient;
		this.path = path;
		checksum = computeChecksum(this.path);
	}

	/**
	 * Creates a new JobEditorInput linked to an existing {@link GridBeanActivity)
	 * that is identified by its name. The job file must be given by an absolute (!) path. 
	 * @param path
	 * @param activityName
	 */
	public JobEditorInput(IPath path, String activityName) {
		this.path = path;
		this.activityName = activityName;
		checksum = computeChecksum(this.path);
	}

	protected long computeChecksum(IPath path) {
		File f = getFile();
		if (f == null) {
			return -1;
		}
		
		BufferedInputStream in = null;
		try {
			// check whether file has changed
			FileInputStream file = new FileInputStream(f);
			CheckedInputStream check = new CheckedInputStream(file, new CRC32());
			in = new BufferedInputStream(check);
			while (in.read() != -1) {
				// Read file in completely
			}
			return check.getChecksum().getValue();
		} catch (Exception e) {
			return -1;
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// cannot do anything else
				}
			}
		}
	}

	public void dispose()
	{
		this.activity = null;
		this.activityName = null;
		this.gridBeanClient = null;
		this.wfEditor = null;
		this.workbenchPage = null;
		// do NOT set the path to null! Otherwise closing a job editor during
		// submission will cause problems because the job file won't be found
		// anymore!
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (o instanceof JobEditorInput) {
			JobEditorInput other = (JobEditorInput) o;
			if (workbenchPage == null) {
				init(null);
			}
			if (other.workbenchPage == null) {
				other.init(null);
			}
			String myActivityName = getActivity() == null ? null
					: getActivity().getName();
			String otherActivityName = other.getActivity() == null ? null
					: other.getActivity().getName();
			return CollectionUtil.equalOrBothNull(myActivityName,
					otherActivityName)
					&& CollectionUtil.equalOrBothNull(getURI(), other.getURI());
		}
		if (o instanceof IURIEditorInput) {
			IURIEditorInput uriEditorInput = (IURIEditorInput) o;
			return CollectionUtil.equalOrBothNull(uriEditorInput.getURI(),
					getURI());
		}
		return false;
	}

	public boolean exists() {
		return getFile().exists();
	}

	public GridBeanActivity getActivity() {
		return activity;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object getAdapter(Class adapter) {
		if (adapter == GridBeanClient.class) {
			return gridBeanClient;
		} else if (adapter == IPathEditorInput.class) {
			return this;
		} else if (adapter == GridBeanActivity.class) {
			return activity;
		} else if (adapter == IFile.class) {
			return PathUtils.absolutePathToEclipseFile(getPath());
		}
		return null;
	}

	public long getChecksum() {
		return checksum;
	}

	public String getFactoryId() {
		return JobEditorInputFactory.ID_FACTORY;
	}

	public File getFile() {
		if (wasInvalidWhilePersisting() || path == null) {
			return null;
		}
		return path.toFile();
	}

	public ImageDescriptor getImageDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getName() {
		if (getFile() == null) {
			return "Invalid input";
		}
		return getFile().getName();
	}

	public IPath getPath() {
		return path;
	}

	public IPersistableElement getPersistable() {
		return this;
	}

	public String getToolTipText() {
		if (getFile() == null) {
			return "";
		}
		return getFile().toString();
	}

	public URI getURI() {
		if (getFile() == null) {
			return null;
		}
		return getFile().toURI();
	}

	public WFEditor getWfEditor() {
		return wfEditor;
	}

	public void init(IWorkbenchPage page) {
		if (activity != null || activity == null && activityName != null) {

			IPath workflowPath = null;
			try {
				workflowPath = GPEPathUtils
				.extractWorkflowPathFromJobPath(path);
				if (workflowPath != null) {
					FileEditorInput wfInput = new FileEditorInput(
							PathUtils.absolutePathToEclipseFile(workflowPath));
					for (IWorkbenchPage currentPage : PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getPages()) {
						if (page == null) {
							page = currentPage;
						}
						wfEditor = (WFEditor) page.findEditor(wfInput);
						if (wfEditor != null) {
							break;
						}
					}
					if (wfEditor == null && page != null) {
						wfEditor = (WFEditor) PathUtils.openEditor(page,
								workflowPath);
					}
					try {
						if(wfEditor != null)
						{
							if (activity == null) {
								activity = (GridBeanActivity) wfEditor.getDiagram()
								.getActivityByName(activityName);
							}
							if (activity == null) {
								GPEActivator
								.log(IStatus.ERROR,
								"Unable to find the job in its parent workflow. \nNot opening job editor. ");
							} else {
								gridBeanClient = activity.getGridBeanClient();
							}
						}
					} catch (Exception e) {
						GPEActivator
						.log(IStatus.ERROR,
								"Unable to find the job in its parent workflow. \nNot opening job editor. ",
								e);
					}
				}

			} catch (PartInitException e) {
				GPEActivator
				.log(IStatus.ERROR,
						"Unable to open the workflow editor for this job's parent workflow:\n"
						+ e.getMessage()
						+ "\nNot opening job editor. ", e);
			} catch (FileNotFoundException e) {
				GPEActivator
				.log(IStatus.ERROR,
						"Unable to open the workflow editor for this job's parent workflow:\n The file "
						+ workflowPath.toString()
						+ " which is supposed to hold the workflow does not exist. \nNot opening job editor. ",
						e);
			} catch (ClassCastException e) {
				GPEActivator
				.log(IStatus.ERROR,
						"Unable to open the workflow editor for this job's parent workflow:\n A wrong editor type has been chosen for opening the workflow. \nNot opening job editor. ",
						e);
			}
		}
		this.workbenchPage = page;
	}

	public void saveState(IMemento memento) {
		JobEditorInputFactory.saveState(memento, this);

	}

	public void setActivity(GridBeanActivity activity) {
		this.activity = activity;
	}

	public void setInvalidWhilePersisting(boolean invalidWhilePersisting) {
		this.invalidWhilePersisting = invalidWhilePersisting;
	}

	public void setPath(IPath path) {
		this.path = path;
	}

	public void setWfEditor(WFEditor wfEditor) {
		this.wfEditor = wfEditor;
	}

	public boolean wasInvalidWhilePersisting() {
		return invalidWhilePersisting;
	}

}
