package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.common.guicomponents.FancyTableViewer;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.commands.AddVariableToModelCommand;
import de.fzj.unicore.rcp.gpe4eclipse.commands.MoveVariableDownCommand;
import de.fzj.unicore.rcp.gpe4eclipse.commands.MoveVariableUpCommand;
import de.fzj.unicore.rcp.gpe4eclipse.commands.RemoveVariableFromModelCommand;

@SuppressWarnings("unchecked")
public class EnvVariableTableViewer extends FancyTableViewer {

	// column headers
	public static final String ID = "Name", SOURCE_TYPE = "Source Type",
			VALUE = "Value/Reference";

	/**
	 * the order of the header names in COLUMN_PROPERTIES determines the order
	 * of columns in the table
	 */
	public static final String[] COLUMN_PROPERTIES = new String[] { ID,
			SOURCE_TYPE, VALUE };

	private EnvVariableList envVariableList;

	/**
	 * The cell editors, one for each column of the table. Note that these are
	 * changed dynamically by the CellModifier being used!
	 */
	private CellEditor[] cellEditors;

	/**
	 * List of actions that can be executed on this viewer
	 */
	List<IAction> actions;
	private Action addVariableAction;
	private Action removeVariableAction;
	private Action moveVariableUpAction;
	private Action moveVariableDownAction;
	private CommandStack commandStack;

	/**
	 * The constructor.
	 */
	public EnvVariableTableViewer(Composite parent,
			EnvVariableList envVariableList, CommandStack commandStack) {
		super(parent, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION);

		this.envVariableList = envVariableList;
		this.commandStack = commandStack;
		Table table = createTable();
		// initialize action list
		createActions();

		setUseHashlookup(true);

		// Create the cell editors
		cellEditors = new CellEditor[table.getColumnCount()];

		// ID (TextCellEditor)
		cellEditors[getColumnFor(ID)] = new TextCellEditor(table);

		// Source Type (Combo Box)
		cellEditors[getColumnFor(SOURCE_TYPE)] = new VariableSourceTypeSelectionCellEditor(
				table, envVariableList.getGridBean(),
				envVariableList.getAdaptable());

		// cell editor for the source file
		// this is just a dummy editor, the real editor to be used
		// may be injected by extensions based on the SOURCE_TYPE
		cellEditors[getColumnFor(VALUE)] = new TextCellEditor(table);

		setCellEditors(cellEditors);

		EnvVariableTableCellModifier modifier = new EnvVariableTableCellModifier(
				this);
		setCellModifier(modifier);
		setContentProvider(new EnvVariableContentProvider());
		setLabelProvider(new EnvVariableLabelProvider());
		setColumnProperties(COLUMN_PROPERTIES);

		setInput(getEnvVariableList());
		packTable();
		// create context menu for the table
		final MenuManager menuMgr = new MenuManager("Actions");
		menuMgr.setRemoveAllWhenShown(true);

		Menu menu = menuMgr.createContextMenu(getTable());
		getTable().setMenu(menu);
		IMenuListener listener = new IMenuListener() {
			public void menuAboutToShow(IMenuManager m) {

				for (Iterator<IAction> iter = actions.iterator(); iter
						.hasNext();) {
					IAction action = iter.next();
					if (action.isEnabled()) {
						menuMgr.add(action);
					}
				}
			}
		};
		menuMgr.addMenuListener(listener);

	}

	private void createActions() {
		actions = new ArrayList<IAction>();
		addVariableAction = new Action() {
			@Override
			public void run() {
				Command cmd = new AddVariableToModelCommand(
						getEnvVariableList());
				if (commandStack != null) {
					commandStack.execute(cmd);
				} else {
					cmd.execute();
				}
			}
		};
		addVariableAction.setText("add Environment Variable");
		addVariableAction
				.setToolTipText("Set the value of a variable which is available in the job environment during execution.");
		addVariableAction.setImageDescriptor(GPEActivator
				.getImageDescriptor("add.png"));
		actions.add(addVariableAction);

		removeVariableAction = new Action() {
			@Override
			public boolean isEnabled() {
				// We cannot remove variables that are defined by the GridBean!
				IStructuredSelection selection = (IStructuredSelection) getSelection();
				List<EnvVariable> variables = selection.toList();
				for (EnvVariable v : variables) {
					if (v.isDefinedInGridBean()) {
						return false;
					}
				}
				return variables.size() > 0;
			}

			
			@Override
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getSelection();
				Command cmd = new RemoveVariableFromModelCommand(
						getEnvVariableList(), selection.toList());
				if (commandStack != null) {
					commandStack.execute(cmd);
				} else {
					cmd.execute();
				}

			}
		};
		removeVariableAction.setText("remove Environment Variable");
		removeVariableAction
				.setToolTipText("Remove a variable that won't be used anymore.");
		removeVariableAction.setImageDescriptor(GPEActivator
				.getImageDescriptor("delete.png"));
		actions.add(removeVariableAction);

		moveVariableUpAction = new Action() {
			@Override
			public boolean isEnabled() {
				IStructuredSelection selection = (IStructuredSelection) getSelection();
				EnvVariable var = (EnvVariable) selection.getFirstElement();
				return getEnvVariableList().getPosition(var) > 0;
			}

			@Override
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getSelection();
				EnvVariable var = (EnvVariable) selection.getFirstElement();
				Command cmd = new MoveVariableUpCommand(getEnvVariableList(),
						var);
				if (commandStack != null) {
					commandStack.execute(cmd);
				} else {
					cmd.execute();
				}

			}
		};
		moveVariableUpAction.setText("up");
		moveVariableUpAction
				.setToolTipText("Change the order in which variables are evaluated during job execution.");
		moveVariableUpAction.setImageDescriptor(UnicoreCommonActivator
				.getImageDescriptor("arrow_up.png"));
		actions.add(moveVariableUpAction);

		moveVariableDownAction = new Action() {
			@Override
			public boolean isEnabled() {
				IStructuredSelection selection = (IStructuredSelection) getSelection();
				EnvVariable var = (EnvVariable) selection.getFirstElement();
				int pos = getEnvVariableList().getPosition(var);
				return -1 < pos && pos < getEnvVariableList().size() - 1;
			}

			@Override
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getSelection();
				EnvVariable var = (EnvVariable) selection.getFirstElement();
				Command cmd = new MoveVariableDownCommand(getEnvVariableList(),
						var);
				if (commandStack != null) {
					commandStack.execute(cmd);
				} else {
					cmd.execute();
				}

			}
		};
		moveVariableDownAction.setText("down");
		moveVariableDownAction
				.setToolTipText("Change the order in which variables are evaluated during job execution.");
		moveVariableDownAction.setImageDescriptor(UnicoreCommonActivator
				.getImageDescriptor("arrow_down.png"));
		actions.add(moveVariableDownAction);
	}

	private Table createTable() {
		Table table = getTable();
		for (int i = 0; i < COLUMN_PROPERTIES.length; i++) {
			TableColumn col = new TableColumn(table, SWT.LEFT, i);
			col.setText(COLUMN_PROPERTIES[i]);
			if (COLUMN_PROPERTIES[i].equals(ID)) {
				col.setToolTipText("The name of the environment variable.");
			} else if (COLUMN_PROPERTIES[i].equals(VALUE)) {
				col.setToolTipText("Set a concrete value or obtain the value by referencing an external source (e.g. another variable).");
			} else if (COLUMN_PROPERTIES[i].equals(SOURCE_TYPE)) {
				col.setToolTipText("Type of the source that provides the variable's value.");
			}

		}

		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		return table;
	}

	public List<IAction> getActions() {
		return actions;
	}

	public EnvVariableList getEnvVariableList() {
		return envVariableList;
	}

	public void setCellEditor(int column, CellEditor editor) {
		cellEditors[column] = editor;
	}

	public static int getColumnFor(String columnName) {
		for (int i = 0; i < COLUMN_PROPERTIES.length; i++) {
			if (COLUMN_PROPERTIES[i].equals(columnName)) {
				return i;
			}
		}
		return -1;
	}

}