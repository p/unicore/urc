/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import de.fzj.unicore.rcp.identity.IdentityActivator;

/**
 * is the labelProvider for the ResourcePropertiesViewer, the Column 'Enabled'
 * is labeled by two images (ticked and unticked box)
 * 
 * @author Christian Hohmann
 * 
 */
class RequirementLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	ResourceRequirementsViewer viewer;
	private Image checkedImg = IdentityActivator.getImageDescriptor(
			"checked.png").createImage();
	private Image uncheckedImg = IdentityActivator.getImageDescriptor(
			"unchecked.png").createImage();

	public RequirementLabelProvider(ResourceRequirementsViewer viewer) {
		this.viewer = viewer;
	}

	@Override
	public void dispose() {
		super.dispose();
		checkedImg.dispose();
		checkedImg = null;
		uncheckedImg.dispose();
		uncheckedImg = null;
	}

	/**
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
	 *      int)
	 */
	public Image getColumnImage(Object element, int columnIndex) {
		if (columnIndex != ResourceRequirementsViewer
				.getColumnFor(ResourceRequirementsViewer.ENABLED)) {
			return null; // ignore everything not default column
		}
		ResourceRequirement resourceRequirement = (ResourceRequirement) element;
		if (resourceRequirement.isEnabled()) {
			return checkedImg;
		} else {
			return uncheckedImg;
		}
	}

	/**
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
	 *      int)
	 */
	public String getColumnText(Object element, int columnIndex) {
		String result = "";
		ResourceRequirement resourceRequirement = (ResourceRequirement) element;

		if (columnIndex == ResourceRequirementsViewer
				.getColumnFor(ResourceRequirementsViewer.ENABLED)) {
			result = "";

		} else if (columnIndex == ResourceRequirementsViewer
				.getColumnFor(ResourceRequirementsViewer.NAME)) {
			try {
				result = resourceRequirement.getRequirementName()
						.getLocalPart();
			} catch (Exception e) {
				result = "";
			}

		} else if (columnIndex == ResourceRequirementsViewer
				.getColumnFor(ResourceRequirementsViewer.VALUE)) {
			try {
				result = resourceRequirement.getValue().getDisplayedString();
			} catch (Exception e) {
				result = "";
			}

		} else if (columnIndex == ResourceRequirementsViewer
				.getColumnFor(ResourceRequirementsViewer.UNITS)) {
			try {
				result = resourceRequirement.getValue().getSelectedUnit();
			} catch (Exception e) {
				result = "";
			}
		}

		else if (columnIndex == ResourceRequirementsViewer
				.getColumnFor(ResourceRequirementsViewer.DESCRIPTION)) {
			try {
				result = resourceRequirement.getDescription();
			} catch (Exception e) {
				result = "";
			}
		}

		return result;
	}
}