package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import java.util.Arrays;

import org.unigrids.services.atomic.types.AvailableResourceTypeType;

public class AvailableResource implements Cloneable {

	private String[] allowedValues;
	
	private String description;
	
	private String min, max, defaultValue;
	
	private String name;
	
	private AvailableResourceTypeType.Enum type;
	

	public AvailableResource(String name, AvailableResourceTypeType.Enum type, String[] allowedValues, String description,
			String min, String max, String defaultValue) {
		super();
		this.allowedValues = allowedValues;
		this.description = description;
		this.min = min;
		this.max = max;
		this.defaultValue = defaultValue;
		this.name = name;
		this.type = type;
	}

	public String[] getAllowedValueArray() {
		return allowedValues;
	}

	public void setAllowedValueArray(String[] allowedValues) {
		this.allowedValues = allowedValues;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	public String getDefault() {
		return defaultValue;
	}

	public void setDefault(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AvailableResourceTypeType.Enum getType() {
		return type;
	}

	public void setType(AvailableResourceTypeType.Enum type) {
		this.type = type;
	}

	public String toString()
	{
		return name + " ("+type+")"; 
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(allowedValues);
		result = prime * result
				+ ((defaultValue == null) ? 0 : defaultValue.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((max == null) ? 0 : max.hashCode());
		result = prime * result + ((min == null) ? 0 : min.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AvailableResource))
			return false;
		AvailableResource other = (AvailableResource) obj;
		if (!Arrays.equals(allowedValues, other.allowedValues))
			return false;
		if (defaultValue == null) {
			if (other.defaultValue != null)
				return false;
		} else if (!defaultValue.equals(other.defaultValue))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (max == null) {
			if (other.max != null)
				return false;
		} else if (!max.equals(other.max))
			return false;
		if (min == null) {
			if (other.min != null)
				return false;
		} else if (!min.equals(other.min))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
	public AvailableResource clone()
	{
		AvailableResource result = null;
		try {
			result = (AvailableResource) super.clone();
		} catch (CloneNotSupportedException e) {
			// won't happen
		}
		if(this.allowedValues != null)
		{
			String[] allowedValues = new String[this.allowedValues.length];
			System.arraycopy(this.allowedValues, 0, allowedValues, 0, allowedValues.length);
			result.allowedValues = allowedValues;
		}
		return result;
	}
	
}

