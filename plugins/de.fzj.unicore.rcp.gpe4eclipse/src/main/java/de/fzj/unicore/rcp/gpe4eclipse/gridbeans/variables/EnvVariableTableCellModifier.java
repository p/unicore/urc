/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.swt.widgets.TableItem;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;

/**
 * @author demuth
 * 
 */
public class EnvVariableTableCellModifier implements ICellModifier {

	private EnvVariableTableViewer tableViewer;

	public EnvVariableTableCellModifier(EnvVariableTableViewer tableViewer) {
		this.tableViewer = tableViewer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object,
	 * java.lang.String)
	 */
	public boolean canModify(Object element, String property) {
		if (!(element instanceof EnvVariable)) {
			return false;
		}
		EnvVariable variable = (EnvVariable) element;
		if (EnvVariableTableViewer.ID.equals(property)) {
			return !variable.isDefinedInGridBean();
		} else if (EnvVariableTableViewer.VALUE.equals(property)) {
			int column = EnvVariableTableViewer.getColumnFor(property);
			CellEditor editor = resolveCellEditor(variable);
			if (editor == null) {
				return false;
			}
			tableViewer.setCellEditor(column, editor);
			return true;
		}

		return true;
	}

	protected VariableSourceTypeRegistry getRegistry() {
		return GPEActivator.getDefault().getVariableSourceTypeRegistry();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object,
	 * java.lang.String)
	 */
	public Object getValue(Object element, String property) {
		EnvVariable v = ((EnvVariable) element);
		if (EnvVariableTableViewer.ID.equals(property)) {
			return v.getGridBeanParameter().getName().getLocalPart();
		} else {
			return v.clone();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object,
	 * java.lang.String, java.lang.Object)
	 */
	public void modify(Object element, String property, Object value) {
		if (value == null) {
			return;
		}
		if (element instanceof TableItem) {
			if (EnvVariableTableViewer.ID.equals(property)) {
				EnvVariable variable = (EnvVariable) ((TableItem) element)
						.getData();
				tableViewer.getEnvVariableList().reportVariableNameChange(
						variable, (String) value);
			} else {
				try {
					EnvVariable variable = (EnvVariable) value;
					EnvVariableList envVariableList = tableViewer
							.getEnvVariableList();
					envVariableList.reportVariableValueChange(variable);

				} catch (Exception e) {
					GPEActivator.log("Could not change " + property
							+ ": Invalid input.", e);
				}
			}
			tableViewer.packTable();
		}

	}

	protected CellEditor resolveCellEditor(EnvVariable variable) {
		EnvVariableList stageList = tableViewer.getEnvVariableList();
		return getRegistry().getDefiningExtension(variable.getType())
				.getCellEditor(tableViewer.getTable(), variable,
						stageList.getAdaptable());
	}

}
