/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse;

import javax.xml.namespace.QName;

import com.intel.gpe.clients.api.transfers.IProtocol;
import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;

/**
 * In this class, common keys for storing information in the GridBean model are
 * defined.
 * 
 * @author demuth
 * 
 */
public interface GPE4EclipseConstants {

	/**
	 * @deprecated use server default instead of always setting a client default
	 */
	@Deprecated
	public static final String P_JOB_TERMINATION_TIME = "job_termination_time";

	/** Used to store the path of the Grid Beans (String value) */
	public static String P_GRIDBEAN_PATH = "greadbeans_path";

	/**
	 * Key to store the interval in which submitted jobs are polled while
	 * monitoring. (In seconds, Integer value).
	 */
	public static String P_JOB_POLLING_INTERVAL = "job_polling_interval";

	public static final String P_PERSISTED_APPLICATION_REGISTRY = "persisted application registry";

	public static final String P_PERSISTED_RESOURCES_REGISTRY = "persisted resources registry";
	
	public static final String P_PERSISTED_GB_EXTENSIONS = "persisted gridbean extensions";
	
	public static final String PREFERENCE_PAGE_ID_JOBS = "de.fzj.unicore.rcp.gpe4eclipse.preferences.JobPreferencePage";

	public static final String FILE_EXTENSION_JOBS = "job";

	
	/**
	 * The key for storing a {@link GridBeanContext} object in the Grid Bean
	 * model;
	 */
	public static final QName CONTEXT = new QName(
			"http://gpe.intel.com/gridbeans", "Context", "gb");

	/**
	 * The context is handled as a new InputParameter with a new type. This
	 * allows to save the context in the JSDL.
	 */
	public static final GridBeanParameterType PARAMETER_TYPE_CONTEXT = new GridBeanParameterType(
			CONTEXT.getLocalPart());

	/**
	 * The key for telling listeners on a Grid Bean model that the model has
	 * been disposed of and the Grid Bean client is now managing a different
	 * model (e.g. after reloading a saved job). Should be used with a Boolean
	 * value.
	 */
	public static final QName MODEL_DISPOSED = new QName(
			"http://gpe.intel.com/gridbeans", "model disposed", "gb");

	public static final String RESOURCEREQUIREMENT_CELLEDITOR_EXTENSION_POINT = "de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.ResourceRequirementCellEditor";
	public static final String RESOURCEREQUIREMENT_EXTENSION_POINT = "de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.ResourceRequirement";

	public static final String SITE_SPECIFIC_RESOURCE = "de.fzj.unicore.rcp.gpe4eclipse.siteSpecificResources";

	public static final String SINGLE_JOBS_PROJECT_NAME = "SingleJobs";

	/**
	 * Default name for jobs when Grid Bean does not set an initial job name.
	 */
	public static final String DEFAULT_JOB_NAME = "Job";

	public final static String IMAGE_FILE_SET_SINK = "file_set_sink.png";

	public final static String IMAGE_FILE_SINK = "file_sink.png";

	/**
	 * Key for storing the currently selected iteration id which is used for
	 * fetching the outcomes of the selected iteration within a loop in a
	 * workflow.
	 */
	public final static String ITERATION_ID = "com.intel.gpe.IterationId";

	/**
	 * Key for storing the current activity id in the processor params that are
	 * passed to the processors of input and output parameters during JSDL
	 * creation
	 */
	public final static String ACTIVITY_ID = "com.intel.gpe.ActivityId";

	public final static QName SWT_PLUGIN = new QName(
			"http://gpe.intel.com/gridbeans", "SWT");

	public static final String EXTENSION_POINT_STAGE_OUT_TYPE = "de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.StageOutType";

	public static final String EXTENSION_POINT_STAGE_IN_TYPE = "de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.StageInType";

	public static final String EXTENSION_POINT_ENV_VARIABLE_SOURCE_TYPE = "de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.EnvVariableSourceType";

	public static final String EXTENSION_POINT_GRID_BEANS = "de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.GridBeanExtensions";

	public static final String EXTENSION_POINT_GRID_FILES = "de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.GridFiles";

	public static final String EXTENSION_POINT_JOB_PROCESSORS = "de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.JobProcessors";

	public static final String EXTENSION_POINT_JAXB = "de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.jaxb";

	public static final String[] GB_SERIALIZATION_EXTENSION_POINTS = 
	new String[]{EXTENSION_POINT_GRID_BEANS,					
	EXTENSION_POINT_ENV_VARIABLE_SOURCE_TYPE,
	EXTENSION_POINT_STAGE_IN_TYPE,
	EXTENSION_POINT_STAGE_OUT_TYPE,
	EXTENSION_POINT_JAXB};
	
	public static final String GB_EXTENSION_DATA_FILE = "ApplicationGUIContributions.xml";
	
	public static final String DIRECTORY_OUTCOMES = "output files";

	public static final String DIRECTORY_INPUTS = "input files";

	// Constants used to identify file addresses that are written to the JSDL by
	// simply writing their internal String.
	// One use case are file addresses that are directly entered by the user as
	// Strings in a TextCellEditor
	public static final QName OTHER_QNAME = ProtocolConstants.OTHER_QNAME;
	public static final String OTHER_PREFIX = ProtocolConstants.OTHER_PREFIX;
	public static final IProtocol OTHER_PROTOCOL = ProtocolConstants.OTHER_PROTOCOL;

}
