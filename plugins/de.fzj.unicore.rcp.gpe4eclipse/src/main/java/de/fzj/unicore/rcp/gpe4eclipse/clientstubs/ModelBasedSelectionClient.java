/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.clientstubs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbenchPart;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.clients.api.RegistryClient;
import com.intel.gpe.clients.api.SelectionClient;
import com.intel.gpe.clients.api.TargetSystemClient;
import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;

/**
 * This class implements a SelectionClient that actually monitors the GridBean
 * model. Whenever the Property {@link IGridBeanModel.RESOURCES}
 * is changed, it will reflect the selection. This mechanism is a bad hack, but
 * it seems reasonable because the selected target resources should be persisted
 * in the GridBeanModel anyway. This class is not responsible for setting the
 * RESOURCES GridBean parameter but only for informing the GridBean.
 * 
 * @author demuth
 * 
 */
public class ModelBasedSelectionClient implements SelectionClient {

	protected TargetSystemClient targetSystem;
	protected RegistryClient registry;

	protected TargetSystemClient gridTargetSystemClient;

	private GridBeanClient<IWorkbenchPart> gbClient;

	public ModelBasedSelectionClient(GridBeanClient<IWorkbenchPart> gbClient) {
		this.gbClient = gbClient;
		TargetSystemClient tsc = new GridTargetSystemClient();
		gridTargetSystemClient = tsc;
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.SelectionClient#getSelectedRegistryWrapper()
	 */
	public RegistryClient getSelectedRegistry() {
		return registry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.SelectionClient#getSelectedTargetSystemWrapper()
	 */
	public TargetSystemClient getSelectedTargetSystem() {
		try {
			targetSystem = null;
			List<String> hosts = new ArrayList<String>();
			ResourcesParameterValue value = (ResourcesParameterValue) gbClient
					.getGridBeanJob().getModel().get(IGridBeanModel.RESOURCES);

			if (value != null) {
				hosts = value.getCandidateHosts();
			}

			if (hosts != null && hosts.size() == 1) {
				Node node = NodeFactory.getNodeFor(NodePath.parseString(hosts
						.get(0)));
				if (node != null) {
					targetSystem = TargetSystemClientFactory
							.createTargetResourceClient(node);
				}
			}

			if (targetSystem == null) {
				targetSystem = gridTargetSystemClient;
			}

			return targetSystem;

		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while obtaining selected target system from the application GUI model: "+e.getMessage(), e);
		}
		return null;
	}

}
