/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.unigrids.services.atomic.types.AvailableResourceType;

import com.intel.gpe.clients.all.gridbeans.GridBeanJobWrapper;
import com.intel.gpe.clients.api.async.RequestExecutionService;
import com.intel.gpe.clients.api.configurators.UserDefaultsConfigurator;
import com.intel.gpe.clients.api.transfers.Protocol;
import com.intel.gpe.clients.common.ApplicationImpl;
import com.intel.gpe.clients.common.JAXBContextProvider;
import com.intel.gpe.gridbeans.JobError;
import com.intel.gpe.gridbeans.apps.ArgumentMetaDataImpl;
import com.intel.gpe.gridbeans.jsdl.JavaToXBeanConverterRegistry;
import com.intel.gpe.gridbeans.parameters.FileParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.IParameterTypeProcessor;
import com.intel.gpe.gridbeans.parameters.processing.IProtocolProcessor;
import com.intel.gpe.gridbeans.parameters.processing.IStepProcessor;
import com.intel.gpe.gridbeans.parameters.processing.ParameterTypeProcessorRegistry;
import com.intel.gpe.gridbeans.parameters.processing.ProtocolProcessorRegistry;
import com.intel.gpe.gridbeans.parameters.processing.StepProcessorRegistry;
import com.intel.gpe.util.defaults.preferences.CommonKeys;
import com.intel.gpe.util.defaults.preferences.IPreferences;
import com.intel.gpe.util.preferences.PropertiesPreferencesFactory;
import com.thoughtworks.xstream.converters.SingleValueConverter;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.DelegatingExtensionClassloader;
import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.GridBeanRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.GridBeanServiceRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.RequirementFactoryRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.resources.ExecEnvToXBeanConverter;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.ReservationIdToXbeanConverter;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.ScheduleTypeToXBeanConverter;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.SiteSpecificRequirementSettingType;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.SiteSpecificSettingToXBeanConverter;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.StringSSRSettingToXBeanConverter;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.StringSSRSettingType;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.UserEmailToXBeanConverter;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.XLoginToXBeanConverter;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in.StageInTypeRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out.StageOutTypeRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.VariableSourceTypeRegistry;
import de.fzj.unicore.rcp.logmonitor.LogActivator;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author Bastian Demuth, Valentina Huber
 */
public class GPEActivator extends AbstractUIPlugin implements
IPropertyChangeListener {

	// The plug-in ID
	public static final String PLUGIN_ID = "de.fzj.unicore.rcp.gpe4eclipse";

	// path to icons folder
	public static final String ICONS_PATH = "$NL$/icons/"; //$NON-NLS-1$

	public final static String LOCAL_DIR = ".gpe4unicore";

	public final static String USER_DEFAULTS = "userPreferences.properties";

	protected ISWTClient client;

	// The shared instance
	private static GPEActivator plugin;

	// The main directory holding gridbeans; new gridbeans are downloaded here
	private File gridBeanDir;

	// Additional gridbeans might be loaded from a bunch of directories 
	private File[] alternativeGridBeanDirs;

	// the persisted preferences used for GPE
	private IPreferences gpePreferences;

	// The gridbeans registry
	private GridBeanRegistry gridBeanRegistry;

	// The registry for services from which GridBeans can be downloaded
	private GridBeanServiceRegistry gridBeanServiceRegistry;

	// The registry for registering new types of stage ins / stage outs
	// e.g. local files, remote files, workflow files ...
	private StageInTypeRegistry stageInTypeRegistry;

	private StageOutTypeRegistry stageOutTypeRegistry;

	private VariableSourceTypeRegistry variableSourceTypeRegistry;

	private RequirementFactoryRegistry requirementFactoryRegistry;

	private boolean listeningToPreferences = true;

	/**
	 * The constructor
	 */
	public GPEActivator() {
		plugin = this;

	}

	private void addGridFileExtensions() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
		.getExtensionPoint(GPE4EclipseConstants.EXTENSION_POINT_GRID_FILES);
		IConfigurationElement[] members = extensionPoint
		.getConfigurationElements();
		for (IConfigurationElement member : members) {
			Bundle b = null;
			String id = member.getDeclaringExtension().getContributor()
			.getName();
			if (id != null) {
				b = Platform.getBundle(id);
			}
			if (b != null) {
				try {
					if ("transmitter".equals(member.getName())) {
						QName source = new QName(
								member.getAttribute("sourceNamespace"),
								member.getAttribute("sourceLocalPart"));
						QName target = new QName(
								member.getAttribute("targetNamespace"),
								member.getAttribute("targetLocalPart"));
						Class<?> clazz = b.loadClass(member.getAttribute("class"));
						client.getFileProvider().addFileTransmitter(source,
								target, clazz);
					} else if ("resolver".equals(member.getName())) {
						QName source = new QName(
								member.getAttribute("namespace"),
								member.getAttribute("localPart"));
						Class<?> clazz = b.loadClass(member.getAttribute("class"));
						client.getFileProvider().addFileResolver(source, clazz);
					}

				} catch (Exception e) {
					log(IStatus.ERROR,
							"Unable to add file transmitter or file resolver for "
							+ id, e);
				}
			}
		}
	}

	private void addJobProcessorExtensions() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
		.getExtensionPoint(GPE4EclipseConstants.EXTENSION_POINT_JOB_PROCESSORS);
		IConfigurationElement[] members = extensionPoint
		.getConfigurationElements();
		for (IConfigurationElement member : members) {
			try {
				if ("protocolProcessor".equals(member.getName())) {
					IProtocolProcessor proc = (IProtocolProcessor) member
					.createExecutableExtension("class");
					ProtocolProcessorRegistry.getDefaultRegistry()
					.registerProcessor(proc);
				} else if ("parameterTypeProcessor".equals(member.getName())) {
					IParameterTypeProcessor proc = (IParameterTypeProcessor) member
					.createExecutableExtension("class");
					ParameterTypeProcessorRegistry.getDefaultRegistry()
					.registerProcessor(proc);
				} else if ("stepProcessor".equals(member.getName())) {
					IStepProcessor proc = (IStepProcessor) member
					.createExecutableExtension("class");
					StepProcessorRegistry.getDefaultRegistry()
					.registerProcessor(proc);
				}
			} catch (Exception e) {
				log(IStatus.ERROR, "Unable to add job processor", e);
			}
		}
	}

	private void askGridBeanDir() {
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {
				while (gridBeanDir == null || !gridBeanDir.exists()) {
					DirectoryDialog dialog = new DirectoryDialog(plugin
							.getWorkbench().getActiveWorkbenchWindow()
							.getShell(), SWT.SAVE);

					dialog.setText("Please select the applications directory path."); //$NON-NLS-1$
					String selected = dialog.open();
					if(selected == null) gridBeanDir = null;
					else gridBeanDir = new File(selected);
				}
			}

		});
	}

	private void createGPEPreferences() {
		
		try {
			Properties clientProperties = new Properties();
			IPath pluginMetaDir = GPEActivator.getDefault().getStateLocation();
			pluginMetaDir = pluginMetaDir.append(File.separator + LOCAL_DIR);

			File prefsDir = pluginMetaDir.toFile();
			if (!prefsDir.exists()) {
				if (!prefsDir.mkdir()) {
					throw new Exception(
							"Unable to create directory for storing GPE user preferences: "
							+ prefsDir.getAbsolutePath());
				}

			}
			File prefsFile = new File(prefsDir, USER_DEFAULTS);
			Map<String, String> defaults = new HashMap<String, String>();
			if (prefsFile.exists()) {
				clientProperties.load(new FileInputStream(prefsFile));
				// clientProperties.load(ApplicationClient.class.getResourceAsStream("client.properties"));
				// clientProperties.load(ApplicationClient.class.getResourceAsStream("clientgui.properties"));
				// clientProperties.load(ApplicationClient.class.getClassLoader().getResourceAsStream(
				// "com/intel/gpe/client2/defaults/common.properties"));
				// clientProperties.load(ApplicationClient.class.getClassLoader().getResourceAsStream(
				// "com/intel/gpe/client2/defaults/commongui.properties"));

				for (Map.Entry<Object, Object> entry : clientProperties
						.entrySet()) {
					defaults.put((String) entry.getKey(),
							(String) entry.getValue());
				}

			} else {
				prefsFile.createNewFile();
			}

			System.setProperty("java.util.prefs.PreferencesFactory",
					PropertiesPreferencesFactory.class.getCanonicalName());
			System.setProperty("java.util.prefs.userRoot",
					prefsFile.getAbsolutePath());
			gpePreferences = UserDefaultsConfigurator.getConfigurator()
			.getUserDefaults(defaults);

			// TODO listen to changes on preferences in order to have an
			// up-to-date value here
			gpePreferences.set(CommonKeys.TERMINATION_TIME,
					String.valueOf(getTerminationTime()));

			setPluginDirForGPE();

		} catch (Exception e) {
			log(IStatus.ERROR,
					"Could not create user preferences for application GUIs!",
					e);
			System.exit(1);
		}
	}
	
	private void setPluginDirForGPE()
	{
		String path = gridBeanDir == null ? "" : gridBeanDir.getAbsolutePath();
		if(alternativeGridBeanDirs != null)
		{
			for(File f : alternativeGridBeanDirs)
			{
				if(f != null && f.exists())
				{
					path +="," +f.getAbsolutePath();
				}
			}
		}
		getGPEPreferences().set(CommonKeys.PLUGINDIR, path);
	}

	/**
	 * Creates the registry for local gridbeans.
	 * 
	 */
	private void createGridBeanRegistry() {
		try {
			gridBeanRegistry = new GridBeanRegistry(getGridBeanDir(),alternativeGridBeanDirs);
		} catch (Exception e) {
			log(IStatus.ERROR, "Application registry could not be initialized",
					e);
		}
	}

	public ISWTClient getClient() {
		return client;
	}

	public IPreferences getGPEPreferences() {
		if (gpePreferences == null) {
			createGPEPreferences();
		}
		return gpePreferences;
	}

	private File getGridBeanDir() {
		if (gridBeanDir != null && gridBeanDir.exists()) {
			return gridBeanDir;
		}

		// ask for gridbean directory to use
		askGridBeanDir();
		saveGridBeanDir(gridBeanDir);
		return gridBeanDir;
	}


	private File getInstallationRelative(String s)
	{
		IPath gridBeanPath = new Path(s);
		if(gridBeanPath.toOSString().length() > 0)
		{
			gridBeanPath = PathUtils.fixInstallationRelativePath(gridBeanPath);
			return gridBeanPath.toFile();
		}
		else return null;
	}

	public GridBeanRegistry getGridBeanRegistry() {
		if (gridBeanRegistry == null) {
			createGridBeanRegistry();
		}
		if(gridBeanRegistry.getGridBeanDir() == null)
		{
			gridBeanRegistry.setGridBeanDir(getGridBeanDir());
		}
		return gridBeanRegistry;
	}

	public GridBeanServiceRegistry getGridBeanServiceRegistry() {
		if (gridBeanServiceRegistry == null) {
			gridBeanServiceRegistry = new GridBeanServiceRegistry();
		}
		return gridBeanServiceRegistry;
	}

	public String[] getJaxbContextAdditions() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
		.getExtensionPoint(GPE4EclipseConstants.EXTENSION_POINT_JAXB);
		IConfigurationElement[] members = extensionPoint
		.getConfigurationElements();
		String[] result = new String[members.length];
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			result[m] = member.getAttribute("class");

		}
		return result;
	}

	public RequirementFactoryRegistry getResourceRequirementRegistry() {
		if (requirementFactoryRegistry == null) {
			requirementFactoryRegistry = new RequirementFactoryRegistry();
		}
		return requirementFactoryRegistry;
	}

	public StageInTypeRegistry getStageInTypeRegistry() {
		if (stageInTypeRegistry == null) {
			stageInTypeRegistry = new StageInTypeRegistry();
		}
		return stageInTypeRegistry;
	}

	public StageOutTypeRegistry getStageOutTypeRegistry() {
		if (stageOutTypeRegistry == null) {
			stageOutTypeRegistry = new StageOutTypeRegistry();
		}
		return stageOutTypeRegistry;
	}

	/**
	 * Returns the default termination time for jobs (in hours)
	 * 
	 * @return the terminationTime
	 */
	
	public int getTerminationTime() {
		
		return Constants.DEFAULT_TERMINATION_TIME;
	}

	public VariableSourceTypeRegistry getVariableSourceTypeRegistry() {
		if (variableSourceTypeRegistry == null) {
			variableSourceTypeRegistry = new VariableSourceTypeRegistry();
		}
		return variableSourceTypeRegistry;
	}

	public void propertyChange(PropertyChangeEvent event) {
		if (!listeningToPreferences) {
			return;
		}
		if (GPE4EclipseConstants.P_GRIDBEAN_PATH.equals(event.getProperty())) {
			String dirs = (String) event.getNewValue();
			gridBeanDirsChanged(dirs);
		}

	}
	
	
	
	private void initBeanDirs(String dirString)
	{
		gridBeanDirsChanged(dirString, false);
	}

	private void gridBeanDirsChanged(String dirString)
	{
		gridBeanDirsChanged(dirString, true);
	}

	private void gridBeanDirsChanged(String dirString, boolean createReg)
	{
		String[] dirs = PathUtils.parseMultiplePathString(dirString);
		List<File> alts = new ArrayList<File>();
		if(dirs.length == 0)
		{
			gridBeanDir = null;
			alternativeGridBeanDirs = null;
			if (gridBeanRegistry != null) {
				gridBeanRegistry.setGridBeanDir(gridBeanDir);
				gridBeanRegistry.setAlternativeGridBeanDirs(alternativeGridBeanDirs);
				gridBeanRegistry.reloadGridBeans();
			}
			saveGridBeanDir(gridBeanDir);
		}
		else
		{
			gridBeanDir = new File(dirs[0]);
			if(!gridBeanDir.exists()) gridBeanDir = getInstallationRelative(dirs[0]);

			if (gridBeanRegistry != null) {
				gridBeanRegistry.setGridBeanDir(gridBeanDir);
			} else if(createReg){
				try {
					gridBeanRegistry = new GridBeanRegistry(gridBeanDir);
				} catch (Exception e) {
					log(IStatus.ERROR,
							"Application registry could not be initialized", e);
				}
			}

			for(int i = 1; i < dirs.length; i ++)
			{
				File alt  = new File(dirs[i]);
				if(!alt.exists()) 
				{
					alt = getInstallationRelative(dirs[i]);
				}
				if(alt != null && alt.exists()) alts.add(alt);
			}
			alternativeGridBeanDirs = alts.toArray(new File[alts.size()]);
			if(gridBeanRegistry != null)
			{
				gridBeanRegistry.setAlternativeGridBeanDirs(alternativeGridBeanDirs);
				gridBeanRegistry.reloadGridBeans();
			}
			setPluginDirForGPE();
		}
	}

	private void saveGridBeanDir(File gridBeanDir) {
		getPreferenceStore().removePropertyChangeListener(this);
		String path = gridBeanDir == null ? "" : gridBeanDir.getAbsolutePath();
		getPreferenceStore().setValue(GPE4EclipseConstants.P_GRIDBEAN_PATH,path);
		getPreferenceStore().addPropertyChangeListener(this);
		setPluginDirForGPE();
	}

	public void setGridBeanRegistry(GridBeanRegistry gridBeanRegistry) {
		this.gridBeanRegistry = gridBeanRegistry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);

		JavaToXBeanConverterRegistry.getDefault().register(
				new SiteSpecificSettingToXBeanConverter());
		JavaToXBeanConverterRegistry.getDefault().register(
				new XLoginToXBeanConverter());
		JavaToXBeanConverterRegistry.getDefault().register(
				new UserEmailToXBeanConverter());
		JavaToXBeanConverterRegistry.getDefault().register(
				new ReservationIdToXbeanConverter());
		JavaToXBeanConverterRegistry.getDefault().register(
				new ExecEnvToXBeanConverter());
		JavaToXBeanConverterRegistry.getDefault().register(
				new StringSSRSettingToXBeanConverter());
		JavaToXBeanConverterRegistry.getDefault().register(
				new ScheduleTypeToXBeanConverter());
		JAXBContextProvider.addPackage(GridBeanContext.class.getPackage());
		JAXBContextProvider.addPackage(GridBeanJobWrapper.class.getPackage());
		JAXBContextProvider.addPackage(FileParameterValue.class.getPackage());
		JAXBContextProvider.addPackage(JobError.class.getPackage());
		JAXBContextProvider.addPackage(Protocol.class.getPackage());
		JAXBContextProvider.addPackage(SiteSpecificRequirementSettingType.class
				.getPackage());
		JAXBContextProvider.addPackage(StringSSRSettingType.class
				.getPackage());
		JAXBContextProvider.addPackage(ApplicationImpl.class.getPackage());
		JAXBContextProvider.addPackage(ArgumentMetaDataImpl.class.getPackage());
		// GridRowRegistry.getDefault().register(Integer.class, new
		// IntegerGridRow());

		ClassLoader cll = new DelegatingExtensionClassloader(
				GPEActivator.class.getClassLoader(),
				GPE4EclipseConstants.EXTENSION_POINT_JAXB);

		for (String className : getJaxbContextAdditions()) {
			Package pkg = cll.loadClass(className).getPackage();
			JAXBContextProvider.addPackage(pkg);
		}

		// in case of newly introduced gridbean classes for
		// stage types or environment variable types
		// use this special class loader for deserialization

		ClassLoader cl = new DelegatingExtensionClassloader(
				GPEActivator.class.getClassLoader(),
				GPE4EclipseConstants.GB_SERIALIZATION_EXTENSION_POINTS);
		JAXBContextProvider.setDefaultClassLoader(cl);

		getPreferenceStore().addPropertyChangeListener(this);

		RequestExecutionService.setDefaultSyncClient(new SWTSyncClient());
		RequestExecutionService.setDefaultAsyncClient(new SWTAsyncClient());



		client = new SWTClient();
		client.startup();
		addGridFileExtensions();
		addJobProcessorExtensions();

		IPreferenceStore p = getPreferenceStore();
		String dirString = p.getString(GPE4EclipseConstants.P_GRIDBEAN_PATH);
		initBeanDirs(dirString);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		JAXBContextProvider
		.removePackage(GridBeanJobWrapper.class.getPackage());
		JAXBContextProvider
		.removePackage(FileParameterValue.class.getPackage());
		JAXBContextProvider.removePackage(JobError.class.getPackage());
		JAXBContextProvider.removePackage(Protocol.class.getPackage());

		plugin = null;
		client.shutdown();
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static GPEActivator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH + path);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param forceAlarmUser
	 *            forces the logging plugin to open a popup; usually, this
	 *            should NOT be set to true!
	 */
	public static void log(int status, String msg, boolean forceAlarmUser) {
		log(status, msg, null, forceAlarmUser);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 * @param forceAlarmUser
	 *            forces the logging plugin to open a popup; usually, this
	 *            should NOT be set to true!
	 */
	public static void log(int status, String msg, Exception e,
			boolean forceAlarmUser) {
		IStatus s = new Status(status, PLUGIN_ID, IStatus.OK, msg, e);
		LogActivator.log(plugin, s, forceAlarmUser);
	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 */
	public static void log(String msg) {
		log(msg, null);
	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(String msg, Exception e) {
		log(IStatus.INFO, msg, e, false);
	}

	public static class AvailableResourceConverter implements SingleValueConverter {

		@SuppressWarnings({ "rawtypes" })
		@Override
		public boolean canConvert(Class arg0) {
			return AvailableResourceType.class.isAssignableFrom(arg0);
		}

		@Override
		public String toString(Object arg0) {
			AvailableResourceType input = (AvailableResourceType) arg0;
			return input.xmlText();
		}

		@Override
		public Object fromString(String arg0) {
			try {
				return AvailableResourceType.Factory.parse(arg0);
			} catch (XmlException e) {
				throw new IllegalArgumentException("Can't deserialize XStream object", e);
			}
		}
	}
}
