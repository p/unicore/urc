package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import com.intel.gpe.clients.api.jsdl.RangeValueType;
import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;
import com.intel.gpe.gridbeans.parameters.SiteSpecificResourceRequirement;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.SiteSpecificRequirementSettingType;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.RangeValueTypeRequirementValue;

public class SiteSpecificRequirementSetting extends ResourceRequirement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7937869869558965962L;

	@Override
	public void loadFrom(ResourcesParameterValue value) {
		if (value.getSiteSpecificRequirements() == null) {
			return;
		}
		for (SiteSpecificResourceRequirement req : value
				.getSiteSpecificRequirements()) {
			if (req.getName().equals(getRequirementName())) {
				SiteSpecificRequirementSettingType setting = (SiteSpecificRequirementSettingType) req.getRequirement();
				RangeValueTypeRequirementValue range = (RangeValueTypeRequirementValue) getValue();
				setEnabled(setting.isEnabled());
				setDirty(isEnabled());
				range.setChosenValue(setting.getRange());
				break;
			}
		}

	}

	@Override
	public void writeTo(ResourcesParameterValue value) {
		RangeValueType range = ((RangeValueTypeRequirementValue) getValue())
				.getChosenValue();
		SiteSpecificRequirementSettingType type = new SiteSpecificRequirementSettingType(
				getRequirementName().getLocalPart(), range);
		type.setEnabled(isEnabled());
		SiteSpecificResourceRequirement req = new SiteSpecificResourceRequirement(
				getRequirementName(), type);
		
		value.removeSiteSpecificRequirement(req);
		
		if(isEnabled() || isDirty())
		{
			value.addSiteSpecificRequirement(req);
		}
	}

}
