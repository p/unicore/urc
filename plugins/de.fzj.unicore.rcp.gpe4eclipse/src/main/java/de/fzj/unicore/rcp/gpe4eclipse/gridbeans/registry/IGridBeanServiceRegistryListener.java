package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry;

import java.util.List;

import com.intel.gpe.clients.api.DownloadGridBeanClient;

public interface IGridBeanServiceRegistryListener {

	public void gridBeanServicesChanged(List<DownloadGridBeanClient> newValue);
}
