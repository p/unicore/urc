/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.Tree;

import com.intel.gpe.clients.api.Application;

import de.fzj.unicore.rcp.common.utils.ToolBarUtils;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser.ApplicationResourceFilter;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceContentProvider;
import de.fzj.unicore.rcp.servicebrowser.ServiceLabelProvider;
import de.fzj.unicore.rcp.servicebrowser.actions.RefreshAction;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterController;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.RootNode;
import de.fzj.unicore.rcp.servicebrowser.ui.DefaultFilterSet;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceTreeViewer;

/**
 * FlowWizardPage1
 * 
 * @author Daniel Lee
 */
@SuppressWarnings("unchecked")
public class JobCreationWizardPageResource extends WizardPage implements
		PropertyChangeListener {

	public static final String NAME = "resourcePage";

	protected ServiceContentProvider contentProvider;

	protected ServiceTreeViewer treeViewer;

	protected GridNode gridNode;

	// a collection of the initially-selected elements
	private List<IAdaptable> initialResourceSelection = new ArrayList<IAdaptable>();

	private JobCreationInfo jobCreationInfo;

	private ILabelProvider fLabelProvider;

	private ViewerSorter fSorter;
	

	private FilterController filterController;
	private ApplicationResourceFilter applicationFilter;

	private DefaultFilterSet defaultFilterSet;

	// toolbar manager used to add additional items
	// to the toolbar above the tree viewer
	private ToolBarManager upperToolBarManager = new ToolBarManager(SWT.RIGHT);

	// toolbar manager used to add additional items
	// to the toolbar below the tree viewer
	private ToolBarManager lowerToolBarManager = new ToolBarManager(SWT.RIGHT);

	private int fWidth = 60;

	private int fHeight = 18;

	protected boolean listeningToTreeViewer = true;

	public JobCreationWizardPageResource(JobCreationInfo jobCreationInfo) {
		super(NAME);
		setTitle("Select target resource");
		setDescription("Choose a computational resource to which the job shall be submitted.");
		// setImageDescriptor(ImageDescriptor.createFromFile(WFActivator.class,"images/flowbanner.gif"));
		this.jobCreationInfo = jobCreationInfo;
		jobCreationInfo.addPropertyChangeListener(this);
	}

	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(1, true));
		RefreshAction refresh = new RefreshAction(treeViewer);
		refresh.setText("Refresh Grid");
		refresh.setToolTipText("Press to update the displayed list of Grid services.");

		int numColumns = 1;
		GridLayout layout = new GridLayout(numColumns, true);

		Composite upperToolBarContainer = new Composite(composite, SWT.NO_FOCUS);
		upperToolBarContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
				true, false));
		upperToolBarContainer.setLayout(layout);

		upperToolBarManager.add(refresh);

		ToolBar upperToolBar = upperToolBarManager
				.createControl(upperToolBarContainer);
		upperToolBar.setLayout(new GridLayout(upperToolBar.getItemCount(),
				false));
		GridData data = new GridData(SWT.RIGHT, SWT.CENTER, true, false);
		upperToolBar.setLayoutData(data);
		ToolBarUtils.setToolItemText(upperToolBar);

		treeViewer = createTreeViewer(composite);
		
		setInitialResourceSelection(jobCreationInfo.getResources());

		refresh.setNode(gridNode);
		refresh.setViewer(treeViewer);

		data = new GridData(GridData.FILL_BOTH);
		data.widthHint = convertWidthInCharsToPixels(fWidth);
		data.heightHint = convertHeightInCharsToPixels(fHeight);

		Tree treeWidget = treeViewer.getTree();
		treeWidget.setLayoutData(data);
		treeWidget.setFont(parent.getFont());

		Composite lowerToolBarContainer = new Composite(composite, SWT.NO_FOCUS);
		lowerToolBarContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
				true, false));
		lowerToolBarContainer.setLayout(layout);

		ToolBar lowerToolBar = lowerToolBarManager
				.createControl(lowerToolBarContainer);
		lowerToolBar.setLayout(new GridLayout(lowerToolBar.getItemCount(),
				false));
		data = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		lowerToolBar.setLayoutData(data);
		ToolBarUtils.setToolItemText(lowerToolBar);
		setControl(composite);

	}

	/**
	 * Creates the tree viewer.
	 * 
	 * @param parent
	 *            the parent composite
	 * @return the tree viewer
	 */
	protected ServiceTreeViewer createTreeViewer(Composite parent) {
		int style = SWT.BORDER | SWT.SINGLE;

		final ServiceTreeViewer treeViewer = new ServiceTreeViewer(parent,
				style);
		fLabelProvider = new ServiceLabelProvider();
		contentProvider = new ServiceContentProvider();

		gridNode = (GridNode) ServiceBrowserActivator.getDefault()
				.getGridNode().clone(3);
		RootNode rootNode = new RootNode();
		rootNode.addChild(gridNode);
		treeViewer.setContentProvider(contentProvider);
		treeViewer.setLabelProvider(fLabelProvider);
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			public void selectionChanged(SelectionChangedEvent event) {
				if (!listeningToTreeViewer) {
					return;
				}
				jobCreationInfo.setResources(((IStructuredSelection) event
						.getSelection()).toList());
			}
		});
		treeViewer.setSorter(fSorter);

		treeViewer.setGridNode(gridNode);
		if (filterController == null) {
			filterController = new FilterController();
		}
		filterController.addViewer(treeViewer);
		defaultFilterSet = new DefaultFilterSet(filterController);
		defaultFilterSet.alwaysHideFailedNodes(true);
		defaultFilterSet.alwaysHideUnaccessibleNodes(true);
		defaultFilterSet
				.filterNodesByType(DefaultFilterSet.TYPES_TARGET_SYSTEMS);
		initApplicationFilter(jobCreationInfo.getApplication());
		// expand the tree by default
		treeViewer.setExpandedLevels(2);
		return treeViewer;
	}

	public boolean finish() {
		if (defaultFilterSet != null) {
			defaultFilterSet.dispose();
		}
		if (fLabelProvider != null) {
			fLabelProvider.dispose();
		}
		if (treeViewer != null) {
			treeViewer.dispose();
		}
		jobCreationInfo.removePropertyChangeListener(this);
		jobCreationInfo.setResources(treeViewer.getSelectedNodes());
		return true;
	}

	protected List<IAdaptable> getInitialResourceSelection() {
		return initialResourceSelection;
	}

	protected void initApplicationFilter(Application app) {
		if (treeViewer == null) {
			return;
		}
		if (applicationFilter == null) {
			applicationFilter = new ApplicationResourceFilter(app, treeViewer);
			filterController.addFilter(applicationFilter);
		} else {
			applicationFilter.setApplication(app);
		}
		applicationFilter.scheduleUpdate();

	}

	
	public void propertyChange(PropertyChangeEvent evt) {
		if (JobCreationInfo.PROP_RESOURCES.equals(evt.getPropertyName())) {
			setInitialResourceSelection(jobCreationInfo.getResources());
		} else if (JobCreationInfo.PROP_APPLICATION.equals(evt
				.getPropertyName())) {
			initApplicationFilter(jobCreationInfo.getApplication());
		}
	}

	public void setInitialResourceSelection(List<IAdaptable> selectedElements) {
		initialResourceSelection = selectedElements;
		if (treeViewer != null) {
			listeningToTreeViewer = false;
			try {
				treeViewer.expandAll();
				treeViewer.setSelectedNodes(selectedElements);
			} finally {
				listeningToTreeViewer = true;
			}
		}
	}

}
