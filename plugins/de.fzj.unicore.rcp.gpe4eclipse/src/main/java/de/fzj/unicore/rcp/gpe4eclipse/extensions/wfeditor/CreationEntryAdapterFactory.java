package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IAdapterFactory;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;

@SuppressWarnings({"unchecked","rawtypes"})
public class CreationEntryAdapterFactory implements IAdapterFactory{

	
	private static final Class<GridBeanInfo>[] adapterList = new Class[]{GridBeanInfo.class};
	
	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		// delegate to adaptable
		// this factory is only needed due to Eclipse bug 201743
		if(adaptableObject instanceof IAdaptable)
			return ((IAdaptable) adaptableObject).getAdapter(adapterType);
		return null;
	}

	@Override
	public Class[] getAdapterList() {
		return adapterList;
	}

}
