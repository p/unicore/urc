package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import java.util.Arrays;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IStatus;
import org.unigrids.services.atomic.types.AvailableResourceTypeType;

import com.intel.gpe.clients.api.jsdl.RangeValueType;
import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;
import com.intel.gpe.gridbeans.parameters.SiteSpecificResourceRequirement;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.StringSSRSettingType;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.BooleanRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.EnumRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.RangeValueTypeRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.StringRequirementValue;
import de.fzj.unicore.rcp.logmonitor.LogActivator;

/**
 * Represents a content of one line in the resources editor in case of site
 * specific resources, new style (6.4+).
 * 
 * @author K. Benedyczak
 */
public class SSRSetting extends ResourceRequirement {
	private static final long serialVersionUID = 7937869869558965962L;

	public static SSRSetting fromSiteProperties(AvailableResource siteResource) {
		AvailableResourceTypeType.Enum type = siteResource.getType();
		IRequirementValue value;

		try {
			if (type == AvailableResourceTypeType.BOOLEAN) {
				boolean def = siteResource.getDefault() != null ? Boolean.parseBoolean(siteResource.getDefault())
						: true;
				value = new BooleanRequirementValue(def);
			} else if (type == AvailableResourceTypeType.CHOICE) {
				String[] allowed = siteResource.getAllowedValueArray();
				if (allowed == null)
					return null;
				EnumRequirementValue<String> toBeSet = new EnumRequirementValue<String>(Arrays.asList(allowed));
				if (siteResource.getDefault() != null)
					toBeSet.setSelectedElement(siteResource.getDefault());
				value = toBeSet;
			} else if (type == AvailableResourceTypeType.DOUBLE) {
				double min = siteResource.getMin() != null ? Double.parseDouble(siteResource.getMin())
						: Integer.MIN_VALUE;
				double max = siteResource.getMax() != null ? Double.parseDouble(siteResource.getMax())
						: Integer.MAX_VALUE;
				double def = siteResource.getDefault() != null ? Double.parseDouble(siteResource.getDefault()) : min;
				value = new RangeValueTypeRequirementValue(new RangeValueType(def, min, max), null,
						RequirementConstants.NO_UNIT, RequirementConstants.NO_UNIT, null, false);
			} else if (type == AvailableResourceTypeType.INT) {
				int min = siteResource.getMin() != null ? Integer.parseInt(siteResource.getMin()) : Integer.MIN_VALUE;
				int max = siteResource.getMax() != null ? Integer.parseInt(siteResource.getMax()) : Integer.MAX_VALUE;
				int def = siteResource.getDefault() != null ? Integer.parseInt(siteResource.getDefault()) : min;
				value = new RangeValueTypeRequirementValue(new RangeValueType(def, min, max), null,
						RequirementConstants.NO_UNIT, RequirementConstants.NO_UNIT, null, true);
			} else if (type == AvailableResourceTypeType.STRING) {
				String def = siteResource.getDefault() == null ? "" : siteResource.getDefault();
				value = new StringRequirementValue(def);
			} else {
				LogActivator.log(IStatus.WARNING, "Unsupported type of site-specific resource requirement: " + type);
				return null;
			}
		} catch (NumberFormatException e) {
			LogActivator.log(IStatus.WARNING, "Malformed site-specific resource requirement: " + e.getMessage(), e);
			return null;
		}
		SSRSetting setting = new SSRSetting();
		setting.setDescription(siteResource.getDescription());
		setting.setRequirementName(new QName(type.toString(), siteResource.getName()));
		setting.setValue(value);
		return setting;
	}

	@Override
	public void loadFrom(ResourcesParameterValue value) {
		if (value.getSiteSpecificRequirements() == null)
			return;

		for (SiteSpecificResourceRequirement req : value.getSiteSpecificRequirements()) {
			if (!req.getName().equals(getRequirementName()))
				continue;
			StringSSRSettingType setting = (StringSSRSettingType) req.getRequirement();
			boolean enabled = setting.isEnabled();
			IRequirementValue toUpdate = getValue();

			if (toUpdate instanceof BooleanRequirementValue) {
				Boolean v = Boolean.parseBoolean(setting.getValue());
				((BooleanRequirementValue) toUpdate).setValue(v);
			} else if (toUpdate instanceof RangeValueTypeRequirementValue) {
				Double d = Double.parseDouble(setting.getValue());
				RangeValueTypeRequirementValue range = (RangeValueTypeRequirementValue) toUpdate;
				RangeValueTypeRequirementValue clone = range.clone();
				clone.setChosenValue(new RangeValueType(d));
				if (clone.isValid() == null) {
					// value is valid
					range.setChosenValue(new RangeValueType(d));
				} else {
					enabled = false;
				}
			} else if (toUpdate instanceof EnumRequirementValue<?>) {
				@SuppressWarnings("unchecked")
				EnumRequirementValue<String> enu = (EnumRequirementValue<String>) toUpdate;
				String oldValue = setting.getValue();
				if (enu.getElements().contains(oldValue)) {
					enu.setSelectedElement(oldValue);
				} else {
					enabled = false;
				}
			} else if (toUpdate instanceof StringRequirementValue) {
				((StringRequirementValue) toUpdate).setValue(setting.getValue());
			} else {
				LogActivator.log(IStatus.ERROR,
						"Type of the site specific resource is unknown (loading): " + toUpdate.getClass().getName());
				return;
			}
			setDirty(enabled);
			setEnabled(enabled);

			break;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public void writeTo(ResourcesParameterValue value) {

		IRequirementValue toSave = getValue();

		Object v;
		if (toSave instanceof BooleanRequirementValue) {
			v = ((BooleanRequirementValue) toSave).getValue();
		} else if (toSave instanceof RangeValueTypeRequirementValue) {
			RangeValueTypeRequirementValue rangeValue = (RangeValueTypeRequirementValue) toSave;
			if (rangeValue.isWholeNumbersOnly())
				v = Math.round(rangeValue.getChosenValue().getExact()) + "";
			else
				v = rangeValue.getChosenValue().getExact() + "";
		} else if (toSave instanceof EnumRequirementValue<?>) {
			v = ((EnumRequirementValue<String>) toSave).getSelectedElement();
		} else if (toSave instanceof StringRequirementValue) {
			v = ((StringRequirementValue) toSave).getValue();
		} else {
			LogActivator.log(IStatus.ERROR,
					"Type of the site specific resource is unknown (storing): " + toSave.getClass().getName());
			return;
		}
		StringSSRSettingType type = new StringSSRSettingType(getRequirementName().getLocalPart(), v.toString());
		type.setEnabled(isEnabled());

		SiteSpecificResourceRequirement req = new SiteSpecificResourceRequirement(getRequirementName(), type);

		value.removeSiteSpecificRequirement(req);
		if (isEnabled() || isDirty()) {
			value.addSiteSpecificRequirement(req);
		}

	}

}
