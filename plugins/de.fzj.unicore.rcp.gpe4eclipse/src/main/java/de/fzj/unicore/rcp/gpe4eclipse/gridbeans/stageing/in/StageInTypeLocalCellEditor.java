/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in;

import java.io.File;
import java.util.UUID;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;

import de.fzj.unicore.rcp.common.guicomponents.OpenFileOrDirectoryCellEditor;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

/**
 * @author demuth
 * 
 */
public class StageInTypeLocalCellEditor extends OpenFileOrDirectoryCellEditor {

	IFileParameterValue file;
	String filterPath;

	/** A CellEditor, opening a FileBrowser for selecting a local file */
	public StageInTypeLocalCellEditor(Composite parent, String filterPath,
			boolean allowDirectories, boolean allowMultiple) {
		super(parent);
		this.filterPath = filterPath;
		if (filterPath != null) {
			setFilterPath(filterPath);
		}
		setAllowDirectories(allowDirectories);
		setAllowMultiple(allowMultiple);
	}

	@Override
	protected void doSetValue(Object value) {
		if (!(value instanceof IFileParameterValue[])) {
			IFileParameterValue file = (IFileParameterValue) value;
			value = new IFileParameterValue[] { file };
			super.doSetValue(value);
			updateContents(file.getSource().getInternalString());
		} else {
			super.doSetValue(value);
		}
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {

		try {
			// workaround since filterpath is not working correctly when no
			// filename is set:
			// the dialog always opens the filterpath's parent directory!
			file = ((IFileParameterValue[]) this.getValue())[0];
			try {
				File f = new File(getFilterPath(), file.getSource()
						.getRelativePath());
				if (f.exists()) {
					setFileName(f.getName());
				} else {
					String[] files = f.getParentFile().list();
					setFileName(files[0]);
				}
			} catch (Exception e) {
				// do nothing
			}

			String[] results = (String[]) super.openDialogBox(cellEditorWindow);
			if (results == null || results.length == 0) {
				return null;
			}
			IFileParameterValue[] result = new IFileParameterValue[results.length];
			for (int i = 0; i < results.length; i++) {
				String fileName = results[i];
				File f = new File(fileName);
				if(!f.exists())
				{
					if(isReturningDirectories())
					{
						try {
							// do not ask user whether to create dir:
							// the gtk dialog automatically creates it so for OS
							// independence, we should always create it
							if(!f.mkdirs())
							{
								GPEActivator.log(Status.ERROR, "Unable to create directory "+f.getAbsolutePath()+".");
							}
						} catch (Exception e) {
							GPEActivator.log(Status.ERROR, "Unable to create directory "+f.getAbsolutePath()+": "+e.getMessage(),e);
						}						
					}
					else
					{
						try {
							if(!f.createNewFile())
							{
								GPEActivator.log(Status.ERROR, "Unable to create file "+f.getAbsolutePath()+".");
							}
						} catch (Exception e) {
							GPEActivator.log(Status.ERROR, "Unable to create file "+f.getAbsolutePath()+": "+e.getMessage(),e);
						}

					}
					
				}
				
				if (filterPath != null && fileName.startsWith(filterPath)) {
					fileName = fileName.substring(filterPath.length());
				}

				IFileParameterValue current = file.clone();

				int cut = fileName.lastIndexOf(File.separator);
				String parentDirAddress = fileName.substring(0, cut + 1);
				String relativePath = fileName.substring(cut + 1);
				current.getSource().setParentDirAddress(parentDirAddress);
				current.getSource().setRelativePath(relativePath);
				current.getSource().setDisplayedString(null);
				boolean usingWildcards = current.getSource().getRelativePath()
				.contains("*");
				current.getSource().setUsingWildcards(usingWildcards);
				current.getSource().setProtocol(
						ProtocolConstants.LOCAL_PROTOCOL);

				String nameInWorkingDir = current.getTarget().getRelativePath();
				if (results.length > 1 || nameInWorkingDir == null
						|| nameInWorkingDir.trim().length() == 0) {
					current.getTarget().setRelativePath(relativePath);
					current.getTarget().setDisplayedString(relativePath);
				}
				if (results.length > 0) {
					// use a different UUID for the newly created file transfer
					String uuid = UUID.randomUUID().toString();
					current.setUniqueId(uuid);
				}
				result[i] = current;

			}
			Job j = new BackgroundJob("refreshing workspace.")
			{

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try {
						ResourcesPlugin.getWorkspace().getRoot()
								.refreshLocal(IResource.DEPTH_INFINITE, monitor);
					} catch (Exception e) {
						GPEActivator.log(Status.WARNING, "Unable to refresh workspace: "+e.getMessage(),e);
					}
					return Status.OK_STATUS;
				}
				
			};
			j.schedule();
			return result;
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Could not apply value to table entry", e);
			return file;
		}
	}

}
