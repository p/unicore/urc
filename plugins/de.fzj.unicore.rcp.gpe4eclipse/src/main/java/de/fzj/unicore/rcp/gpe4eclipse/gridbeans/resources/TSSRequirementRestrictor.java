package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument.TargetSystemProperties;


public class TSSRequirementRestrictor extends TSRequirementRestrictor {
	public TSSRequirementRestrictor(TargetSystemProperties props) {
		super(new ResourceProviderAdapter(props));
	}
}
