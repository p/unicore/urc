package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types;


public class StringSSRSettingType extends AbstractSSRSettingType {
	protected String value;

	public StringSSRSettingType() {
	}
	
	public StringSSRSettingType(String name, String value) {
		super(name);
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		StringSSRSettingType other = (StringSSRSettingType) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return enabled ? "StringSSRSettingType [value=" + value + "]" : "disabled";
	}
}
