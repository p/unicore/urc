package de.fzj.unicore.rcp.gpe4eclipse.clientstubs;

public interface IClientChangeListener {

	public void clientChanged(IMutableClient client);
}
