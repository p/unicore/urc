package de.fzj.unicore.rcp.gpe4eclipse.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;

import com.intel.gpe.clients.api.jsdl.RangeValueType;

import de.fzj.unicore.rcp.common.utils.RangeValueUtils;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.UnitConverter;

public class RangeValueFormattingUtils {

	public static String formatDouble(double d) {
		return formatDouble(d, null);
	}

	public static String formatDouble(double d, RoundingMode rm) {
		if (rm == null) {
			rm = RoundingMode.HALF_EVEN;
		}
		BigDecimal bd = new BigDecimal(d);
		bd = bd.setScale(3, rm);
		d = bd.doubleValue();
		NumberFormat nf = NumberFormat.getInstance();
		return nf.format(d);
	}

	public static String formatRangeValue(RangeValueType chosen,
			RangeValueType valid, String internalUnit, String selectedUnit) {
		Double exactValue = chosen.getExact();

		if (chosen.isExpression()) {
			return chosen.getExpression();
		}

		RangeValueType displayed = null;
		try {
			displayed = UnitConverter.getInstance().convertValue(internalUnit,
					selectedUnit, RangeValueType.class, chosen);
		} catch (Exception e) {
			return "Error while converting value to selected unit";
		}
		if (displayed == null) {
			return "";
		}

		if (exactValue != null && !exactValue.isNaN()) {

			String exact = null;
			if (valid != null) {
				org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType bounds = RangeValueTypeConverter
						.convertToXBean(valid);
				boolean isMinValue = Math.abs(exactValue
						- RangeValueUtils.getMinimum(bounds)) < RangeValueUtils.EPSILON;
				boolean isMaxValue = Math.abs(exactValue
						- RangeValueUtils.getMaximum(bounds)) < RangeValueUtils.EPSILON;
				if (isMinValue) {
					exact = formatDouble(displayed.getExact(),
							RoundingMode.CEILING);
				} else if (isMaxValue) {
					exact = formatDouble(displayed.getExact(),
							RoundingMode.FLOOR);
				} else {
					exact = formatDouble(displayed.getExact());
				}
			} else {
				exact = formatDouble(displayed.getExact());
			}
			if (new Double(displayed.getEpsilon()).isNaN()
					|| displayed.getEpsilon() == 0.0) {
				return exact;
			} else {
				return exact + " eps: " + formatDouble(displayed.getEpsilon());
			}
		} else {
			String retval = formatDouble(displayed.getLowerBound()) + " <";
			if (displayed.isIncludeLowerBound()) {
				retval = retval + "=";
			}
			retval = retval + " VALUE <";

			if (displayed.isIncludeUpperBound()) {
				retval = retval + "=";
			}
			retval = retval + " " + formatDouble(displayed.getUpperBound());

			return retval;
		}
	}

	public static String formatRangeValue(RangeValueType chosen,
			String internalUnit, String selectedUnit) {
		Double exactValue = chosen.getExact();

		RangeValueType displayed = null;

		if (chosen.isExpression()) {
			return chosen.getExpression();
		}

		try {
			displayed = UnitConverter.getInstance().convertValue(internalUnit,
					selectedUnit, RangeValueType.class, chosen);
		} catch (Exception e) {
			return "Error while converting value to selected unit";
		}
		if (displayed == null) {
			return "";
		}

		if (exactValue != null && !exactValue.isNaN()) {

			String exact = null;
			exact = formatDouble(displayed.getExact());
			if (new Double(displayed.getEpsilon()).isNaN()
					|| displayed.getEpsilon() == 0.0) {
				return exact;
			} else {
				return exact + " eps: " + formatDouble(displayed.getEpsilon());
			}
		} else {
			String retval = formatDouble(displayed.getLowerBound()) + " <";
			if (displayed.isIncludeLowerBound()) {
				retval = retval + "=";
			}
			retval = retval + " VALUE <";

			if (displayed.isIncludeUpperBound()) {
				retval = retval + "=";
			}
			retval = retval + " " + formatDouble(displayed.getUpperBound());

			return retval;
		}
	}
}
