/*
 * Copyright (c) 2000-2005, Intel Corporation All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Intel Corporation nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.fzj.unicore.rcp.gpe4eclipse;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.clients.all.gridbeans.GridBeanJob;
import com.intel.gpe.clients.all.gridbeans.panels.BaseGridBeanInputPanel;
import com.intel.gpe.gridbeans.ErrorSet;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.gridbeans.plugins.DataSetException;
import com.intel.gpe.gridbeans.plugins.IGridBeanPlugin;
import com.intel.gpe.gridbeans.plugins.TranslationException;

import de.fzj.unicore.rcp.common.utils.swing.SafeSwingUtilities;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditor;

/**
 * 
 * This class tries to take care of the threading that must be used in order to
 * access GridBean panels. Embedded SWT panels must be accessed via the SWT main
 * thread whereas Swing panels should be accessed via the Swing thread.
 * 
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class SWTGridBeanInputPanel extends
		BaseGridBeanInputPanel<IWorkbenchPart> {

	protected GridBeanClient<IWorkbenchPart> gbClient;
	protected JobEditor component;

	protected boolean wrappingSwingComponents;

	public SWTGridBeanInputPanel(GridBeanClient<IWorkbenchPart> gbClient,
			boolean wrappingSwingComponents) throws DataSetException,
			TranslationException {
		super(gbClient.getGridBeanPlugin(), gbClient.getGridBeanJob(), gbClient
				.getClient());
		this.gbClient = gbClient;
		this.wrappingSwingComponents = wrappingSwingComponents;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.panels.IGridBeanInputPanel#getComponent()
	 */
	public JobEditor getComponent() {
		return component;
	}

	@Override
	public GridBeanJob getJob() {
		try {
			ReflectionRunnable r = null;

			Method m = SWTGridBeanInputPanel.class
					.getDeclaredMethod("superGetJob");
			r = new ReflectionRunnable(this, m);

			if (isWrappingSwingComponents()) {
				SafeSwingUtilities.invokeAndWait(r);

			} else {
				PlatformUI.getWorkbench().getDisplay().syncExec(r);
			}

			Object o = r.getResult();
			if (o instanceof Exception) {
				throw (Exception) o;
			} else {
				return (GridBeanJob) o;
			}
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while initializing the application GUI: "+e.getMessage(), e);
			return null;
		}
	}

	public boolean isWrappingSwingComponents() {
		return wrappingSwingComponents;
	}

	
	@Override
	public void load(final IGridBeanPlugin plugin, final GridBeanJob job)
			throws DataSetException, TranslationException {

		try {
			ReflectionRunnable r = null;

			Method m = SWTGridBeanInputPanel.class.getDeclaredMethod(
					"superLoad", IGridBeanPlugin.class, GridBeanJob.class);
			r = new ReflectionRunnable(this, m, plugin, job);

			if (isWrappingSwingComponents()) {
				SafeSwingUtilities.invokeAndWait(r);

			} else {
				PlatformUI.getWorkbench().getDisplay().syncExec(r);
			}

			Object o = r.getResult();
			if (o instanceof Exception) {
				throw (Exception) o;
			}
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while reading values from the application GUI's internal model into the GUI: "+e.getMessage(), e);
			return;
		}
	}

	public void setComponent(JobEditor component) {
		this.component = component;
	}

	@Override
	public void setEnabled(QName key, boolean enabled) throws DataSetException {
		try {
			ReflectionRunnable r = null;

			Method m = SWTGridBeanInputPanel.class.getDeclaredMethod(
					"superSetEnabled", QName.class, Boolean.class);
			r = new ReflectionRunnable(this, m, key, enabled);

			if (isWrappingSwingComponents()) {
				SafeSwingUtilities.invokeAndWait(r);

			} else {
				PlatformUI.getWorkbench().getDisplay().syncExec(r);
			}

			Object o = r.getResult();
			if (o instanceof InvocationTargetException) {
				super.setEnabled(key, enabled);
			} else if (o instanceof DataSetException) {
				throw (DataSetException) o;
			}
		} catch (Exception e) {
			String enabling = enabled ? "enabling":"disabling";
			GPEActivator.log(Status.ERROR, "Error while " +enabling+" the application GUI : "+e.getMessage(), e);;
			return;
		}
	}

	public void setWrappingSwingComponents(boolean wrappingSwingComponents) {
		this.wrappingSwingComponents = wrappingSwingComponents;
	}

	@Override
	public void store(final IGridBeanPlugin plugin, final GridBeanJob job)
			throws DataSetException, TranslationException {

		try {
			ReflectionRunnable r = null;

			Method m = SWTGridBeanInputPanel.class.getDeclaredMethod(
					"superStore", IGridBeanPlugin.class, GridBeanJob.class);
			r = new ReflectionRunnable(this, m, plugin, job);

			if (isWrappingSwingComponents()) {
				SafeSwingUtilities.invokeAndWait(r);

			} else {
				PlatformUI.getWorkbench().getDisplay().syncExec(r);
			}

			Object o = r.getResult();
			if (o instanceof Exception) {
				throw (Exception) o;
			}

		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while carrying values from the application GUI over to its internal model: "+e.getMessage(), e);
			return;
		}
	}

	public GridBeanJob superGetJob() {
		return super.getJob();
	}

	public void superLoad(final IGridBeanPlugin plugin, final GridBeanJob job)
			throws DataSetException, TranslationException {
		super.load(plugin, job);

		IGridBean gridBean = job.getModel();
		if (gridBean == null) {
			return;
		}
		GridBeanContext context = (GridBeanContext) gridBean
				.get(GPE4EclipseConstants.CONTEXT);
		if (context != null && !context.isEditable()) {
			
			gridBean.set(IGridBeanModel.ALLOW_INPUT_PANEL_EDITING, false);
		}
	}
	
	public void dispose()
	{
		GridBeanJob job = getJob();
		if(job != null)
		{
			
			IGridBean gridBean = job.getModel();
			if(gridBean != null)
			{
				gridBean.set(IGridBeanModel.ALLOW_INPUT_PANEL_EDITING, true);
			}
		}
		
		super.dispose();
	}

	public void superSetEnabled(QName key, boolean enabled)
			throws DataSetException {
		super.setEnabled(key, enabled);
	}

	public void superStore(final IGridBeanPlugin plugin, final GridBeanJob job)
			throws DataSetException, TranslationException {
		super.store(plugin, job);
	}

	public void superValidate(ErrorSet errors) {
		super.validate(errors);
	}

	@Override
	public void validate(ErrorSet errors) {
		try {
			ReflectionRunnable r = null;

			Method m = SWTGridBeanInputPanel.class.getDeclaredMethod(
					"superValidate", ErrorSet.class);
			r = new ReflectionRunnable(this, m, errors);

			if (isWrappingSwingComponents()) {
				SafeSwingUtilities.invokeAndWait(r);

			} else {
				PlatformUI.getWorkbench().getDisplay().syncExec(r);
			}

			Object o = r.getResult();
			if (o instanceof Exception) {
				throw (Exception) o;
			}
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while validating application GUI parameters: "+e.getMessage(), e);
			return;
		}
	}

}
