/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.parameters;

import java.io.File;
import java.util.Map;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.clients.api.transfers.IGridFileSetTransfer;
import com.intel.gpe.clients.api.transfers.IGridFileTransfer;
import com.intel.gpe.clients.common.transfers.GridFileSetTransfer;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.AbstractProtocolProcessor;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;

/**
 * 
 * This processor is responsible for the
 * {@link ProcessingConstants#LET_USER_SELECT_DOWLOAD_DIR} step. In the SWT
 * surrounding, the download dir should usually have been selected by the
 * {@link LetUserSelectOutcomesProcessor} which is responsible for the
 * {@link ProcessingConstants#LET_USER_SELECT_OUTCOMES} step. In case the user
 * provided an invalid directory, or sth. else goes wrong, this processor will
 * pop up another dialog for selecting a directory. It will also iterate over
 * all local targets for grid files to be downloaded and make their download
 * paths absolute (using the selected parent directory for the download).
 * 
 * @author demuth
 * 
 */
public class LetUserSelectDownloadDirProcessor extends
		AbstractProtocolProcessor {

	public LetUserSelectDownloadDirProcessor() {
		processingSteps.add(ProcessingConstants.LET_USER_SELECT_DOWLOAD_DIR);
	}

	public boolean matches(IGridBeanParameter param,
			IGridBeanParameterValue value, Map<String, Object> processorParams) {
		return (value instanceof ResourcesParameterValue);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.gridbeans.parameters.IProtocolProcessor#process(com.intel
	 * .gpe.clients.api.jsdl.gpe.GPEJob, int,
	 * com.intel.gpe.gridbeans.IGridBeanParameter,
	 * com.intel.gpe.gridbeans.IGridBeanParameterValue, java.util.Map)
	 */
	public void process(final Client client, GPEJob job, int processingStep,
			IGridBeanParameter param, IGridBeanParameterValue value,
			final Map<String, Object> processorParams,
			IProgressListener progress) throws Exception {
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			@SuppressWarnings("unchecked")
			public void run() {
				IGridFileSetTransfer<IGridFileTransfer> transfers = (IGridFileSetTransfer<IGridFileTransfer>) processorParams
						.get(ProcessingConstants.FILE_TRANSFERS);
				if (transfers.getFiles().size() > 0) {
					String downloadDir = (String) processorParams
							.get(ProcessingConstants.DOWNLOAD_DIR);
					boolean downloadDirSelected = downloadDir != null
							&& downloadDir.trim().length() > 0;

					if (!downloadDirSelected) {
						downloadDir = letUserSelectDownloadDir(client);
						downloadDirSelected = downloadDir != null
								&& downloadDir.trim().length() > 0;
					}
					if (downloadDirSelected) {
						for (IGridFileTransfer file : transfers.getFiles()) {
							file.getProcessedTarget().setParentDirAddress(
									downloadDir);
						}
					} else {
						transfers = new GridFileSetTransfer<IGridFileTransfer>();
					}

				}
				processorParams.put(ProcessingConstants.FILE_TRANSFERS,
						transfers);
			}

		});

	}

	public static String letUserSelectDownloadDir(Client client) {
		Shell parent = PlatformUI.getWorkbench().getDisplay().getActiveShell();
		if (parent == null) {
			parent = new Shell();
		}
		DirectoryDialog dialog = new DirectoryDialog(parent);
		dialog.setFilterPath(ResourcesPlugin.getWorkspace().getRoot()
				.getLocation().toOSString());
		dialog.setMessage("Select a target folder for the job's output files");
		String result = dialog.open();
		if (result != null && result.trim().length() > 0
				&& !result.endsWith(File.separator)) {
			result += File.separator;
		}
		return result;
	}

}
