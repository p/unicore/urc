/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.clientstubs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IStatus;
import org.w3c.dom.Element;

import com.intel.gpe.clients.api.Application;
import com.intel.gpe.clients.api.Job;
import com.intel.gpe.clients.api.JobClient;
import com.intel.gpe.clients.api.JobType;
import com.intel.gpe.clients.api.StorageClient;
import com.intel.gpe.clients.api.TargetSystemClient;
import com.intel.gpe.clients.api.exceptions.FaultWrapper;
import com.intel.gpe.clients.api.exceptions.GPEInvalidQueryExpressionException;
import com.intel.gpe.clients.api.exceptions.GPEInvalidResourcePropertyQNameException;
import com.intel.gpe.clients.api.exceptions.GPEJobNotSubmittedException;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareRemoteException;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareServiceException;
import com.intel.gpe.clients.api.exceptions.GPEQueryEvaluationErrorException;
import com.intel.gpe.clients.api.exceptions.GPEResourceNotDestroyedException;
import com.intel.gpe.clients.api.exceptions.GPEResourceUnknownException;
import com.intel.gpe.clients.api.exceptions.GPESecurityException;
import com.intel.gpe.clients.api.exceptions.GPEUnknownQueryExpressionDialectException;
import com.intel.gpe.clients.api.exceptions.GPEUnmarshallingException;
import com.intel.gpe.clients.api.exceptions.GPEWrongJobTypeException;
import com.intel.gpe.gridbeans.jsdl.GPEJobImpl;
import com.intel.gpe.util.sets.Pair;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.applications.ApplicationRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.IMatchmaker;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;

/**
 * @author demuth
 * 
 */
public class RegistryTargetSystemClient implements TargetSystemClient {

	protected NodePath regPath;

	public RegistryTargetSystemClient(NodePath regPath) {
		super();
		this.regPath = regPath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSLTClient#destroy()
	 */
	public void destroy() throws GPEResourceUnknownException,
			GPEResourceNotDestroyedException, GPESecurityException,
			GPEMiddlewareRemoteException, GPEMiddlewareServiceException {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.TargetSystemClient#getApplications()
	 */
	public List<Application> getApplications()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO make this more intelligent
		return ApplicationRegistry.getDefaultInstance()
				.getEntryList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#getApplications(java.lang
	 * .String)
	 */
	public List<Application> getApplications(String applicationName)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO make this more intelligent
		List<Application> apps = getApplications();
		List<Application> result = new ArrayList<Application>();
		for (Application app : apps) {
			if (app.getName().equals(applicationName)) {
				result.add(app);
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSLTClient#getCurrentTime()
	 */
	public Calendar getCurrentTime() {
		return Calendar.getInstance();
	}

	public String getFullAddress() {
		return regPath.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.TargetSystemClient#getJobs()
	 */
	public <JobClientType extends JobClient> List<JobClientType> getJobs()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		return new ArrayList<JobClientType>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#getJobType(java.lang.String)
	 */
	public JobType getJobType(String jobDefinition) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#getMultipleResourceProperties(javax
	 * .xml.namespace.QName[])
	 */
	public Element[] getMultipleResourceProperties(QName[] resourceProperties) {
		return new Element[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.TargetSystemClient#getName()
	 */
	public String getName() {
		Node n = getRegistryNode();
		if (n == null) {
			return getURL();
		}
		return n.getName();
	}

	protected Node getRegistryNode() {
		return NodeFactory.getNodeFor(regPath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#getResourceProperty(javax.xml.namespace
	 * .QName)
	 */
	public Element getResourceProperty(QName resourceProperty) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSRPClient#getResourcePropertyDocument()
	 */
	public Element getResourcePropertyDocument() throws Exception {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#getStorage(java.lang.String)
	 */
	public <StorageClientType extends StorageClient> StorageClientType getStorage(
			String type) throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.TargetSystemClient#getStorages()
	 */
	public <StorageClientType extends StorageClient> List<StorageClientType> getStorages()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		return new ArrayList<StorageClientType>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSLTClient#getTerminationTime()
	 */
	public Calendar getTerminationTime() {
		Calendar result = Calendar.getInstance();
		result.add(Calendar.YEAR, 20);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#getTextInfo(java.lang.String
	 * )
	 */
	public List<String> getTextInfo(String name)
			throws GPEInvalidQueryExpressionException,
			GPEQueryEvaluationErrorException,
			GPEUnknownQueryExpressionDialectException,
			GPEResourceUnknownException,
			GPEInvalidResourcePropertyQNameException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareServiceException,
			GPEMiddlewareRemoteException {
		return new ArrayList<String>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSRFClient#getURL()
	 */
	public String getURL() {
		return regPath.last().toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#newJob(com.intel.gpe.clients
	 * .api.JobType)
	 */
	@SuppressWarnings("unchecked")
	public Job newJob(JobType type) throws GPEWrongJobTypeException {
		return new GPEJobImpl();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#queryXPath10Properties(java.lang
	 * .String)
	 */
	public Element[] queryXPath10Properties(String expression) {
		return new Element[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSLTClient#setTerminationTime(java.util.Calendar
	 * )
	 */
	public void setTerminationTime(Calendar newTT) {
		// do nothing

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#submit(com.intel.gpe.clients
	 * .api.Job, java.util.Calendar)
	 */
	public <JobClientType extends JobClient> JobClientType submit(Job job,
			Calendar initialTerminationTime) throws GPEWrongJobTypeException,
			GPEJobNotSubmittedException, GPEResourceUnknownException,
			GPEMiddlewareRemoteException, GPEMiddlewareServiceException,
			GPESecurityException {
		List<Pair<String, TargetSystemClient>> suitableTargetSystems = new ArrayList<Pair<String, TargetSystemClient>>();// find
																															// a
																															// matching
																															// target
																															// system
																															// for
																															// the
																															// job

		Queue<Node> q = new ConcurrentLinkedQueue<Node>();
		q.add(getRegistryNode());

		while (!q.isEmpty()) {
			Node next = q.poll();
			if (next instanceof TargetSystemNode) {
				continue; // rather submit to factories instead of target
							// systems
			}
			TargetSystemClient targetSystem = (TargetSystemClient) next
					.getAdapter(TargetSystemClient.class);
			if (targetSystem != null
					&& !(targetSystem instanceof RegistryTargetSystemClient)) // ignore
																				// RegistryTargetSystemClient
																				// as
																				// this
																				// would
																				// imply
																				// brokering
																				// twice
			{
				IMatchmaker mm = (IMatchmaker) next
						.getAdapter(IMatchmaker.class);
				if (mm != null) {
					if (mm.canHandleJob(job) == null) {
						suitableTargetSystems
								.add(new Pair<String, TargetSystemClient>(next
										.getNamePathToRoot(), targetSystem));
					}
				}
			}
			for (Node child : next.getChildrenArray()) {
				q.add(child);
			}
		}
		if (suitableTargetSystems.size() == 0) {
			throw new GPEJobNotSubmittedException(
					"No suitable target system found.", new FaultWrapper() {
						public String[] getDescriptionAsStrings() {
							return new String[] { "" };
						}

						public Throwable getFault() {
							return new Exception();
						}
					}, this);
		}

		int rand = new Random().nextInt(suitableTargetSystems.size());
		Pair<String, TargetSystemClient> pair = suitableTargetSystems.get(rand);
		TargetSystemClient target = pair.getM2();

		JobClientType result = target.submit(job, initialTerminationTime);
		try {
			GPEActivator.log(IStatus.INFO, "Submitted job to " + pair.getM1(),
					true);
		} catch (Exception e) {
			// do nothing
		}
		return result;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#supportsApplication(java
	 * .lang.String, java.lang.String)
	 */
	public boolean supportsApplication(String name, String version)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (Application.ANY_APPLICATION_NAME.equals(name)) {
			return true;
		}
		List<Application> apps = getApplications(name);
		if ((version == null || version
				.equals(Application.ANY_APPLICATION_VERSION))
				&& apps.size() > 0) {
			return true;
		}
		for (Application app : apps) {
			if (app.getApplicationVersion() == null) {
				continue;
			}
			if (version.equalsIgnoreCase(app.getApplicationVersion())) {
				return true;
			}
		}
		return false;
	}
}
