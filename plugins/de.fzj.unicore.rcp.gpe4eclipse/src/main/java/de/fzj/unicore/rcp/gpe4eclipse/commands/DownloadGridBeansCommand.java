/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.commands;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.gef.commands.Command;

import com.intel.gpe.clients.all.utils.FileTools;
import com.intel.gpe.clients.api.DownloadGridBeanClient;
import com.intel.gpe.clients.api.GridBean;
import com.intel.gpe.gridbeans.GridBeanConstants;
import com.intel.gpe.util.sets.Pair;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;

public class DownloadGridBeansCommand extends Command {

	private List<Pair<GridBean, DownloadGridBeanClient>> gridBeansAndSources;

	public DownloadGridBeansCommand(
			List<Pair<GridBean, DownloadGridBeanClient>> gridBeansAndSources) {
		this.gridBeansAndSources = gridBeansAndSources;
	}

	@Override
	public boolean canUndo() {
		return false;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		redo();
	}

	@Override
	public void redo() {
		for (Pair<GridBean, DownloadGridBeanClient> pair : gridBeansAndSources) {
			GridBean gb = pair.getM1();
			DownloadGridBeanClient downloadGridBeanClient = pair.getM2();
			byte[] data = downloadGridBeanClient.getGridBean(gb.getName());

			if (data == null) {
				GPEActivator.log(IStatus.ERROR,
						"Unable to download application " + gb.getName());
				continue;
			}

			File gridBeanDir = GPEActivator.getDefault().getGridBeanRegistry()
					.getGridBeanDir();
			if (!gridBeanDir.exists()) {
				gridBeanDir.mkdirs();
			}
			File gridBeanFile = new File(gridBeanDir, gb.getName()
					+ GridBeanConstants.GB_SUFFIX + "-v" + gb.getVersion()
					+ GridBeanConstants.GB_EXTENSION);
			try {
				FileTools.writeBytesToFile(data, gridBeanFile);
			} catch (IOException e) {
				GPEActivator.log(IStatus.ERROR, "Unable to store application "
						+ gb.getName() + " locally.", e);
			}
		}
		GPEActivator.getDefault().getGridBeanRegistry().reloadGridBeans();
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {

	}

}
