/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import java.net.URI;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.swt.graphics.Image;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.servicebrowser.Util;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * 
 * @author bdemuth
 * 
 */
public class NodeExtensionPointImpl implements INodeExtensionPoint {


	private Image gbServiceImage = GPEActivator.getImageDescriptor(
			"gridbean.gif").createImage();

	public NodeExtensionPointImpl() {

	}

	public void dispose() {
		gbServiceImage.dispose();

	}

	@Override
	public void finalize() {
		dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.extensionpoints.IServiceExtensionPoint
	 * #getImage(de.fzj.unicore.rcp.servicebrowser.serviceNodes.ServiceObject)
	 */
	public Image getImage(Node node) {

		if (node instanceof GridBeanServiceNode) {
			return gbServiceImage;
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeExtensionPoint
	 * #getNode(org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public Node getNode(EndpointReferenceType epr) {

		if (epr == null) {
			return null;
		}

		QName type = Utils.InterfaceNameFromEPR(epr);

		if (type == null) {
			return null;
		}

		if (GridBeanServiceNode.PORT.equals(type)) {
			return new GridBeanServiceNode(epr);
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.extensionpoints.IServiceExtensionPoint
	 * #getServiceModel(org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public Node getNode(URI uri) {
		if (uri == null) {
			return null;
		}

		String type = Util.determineTypeOfURI(uri);
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue(uri.toString());
		if (GridBeanServiceNode.TYPE.equals(type)) {
			Utils.addPortType(epr, GridBeanServiceNode.PORT);
		}
		return getNode(epr);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.servicebrowser.extensionpoints.IServiceExtensionPoint
	 * #getText(de.fzj.unicore.rcp.servicebrowser.serviceNodes.ServiceObject)
	 */
	public String getText(Node node) {
		// use default text for all my nodes: getName()
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.IExecutableExtension#setInitializationData(org
	 * .eclipse.core.runtime.IConfigurationElement, java.lang.String,
	 * java.lang.Object)
	 */
	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
		// TODO Auto-generated method stub

	}

}
