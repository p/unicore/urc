package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types;

import com.intel.gpe.clients.api.jsdl.ResourceRequirementType;

public class UserEmailType implements ResourceRequirementType {

	protected boolean enabled = false;
	protected String emailAddress;

	public UserEmailType() {

	}

	public UserEmailType(String login) {
		this.emailAddress = login;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof UserEmailType)) {
			return false;
		}
		UserEmailType other = (UserEmailType) obj;
		if (emailAddress == null) {
			if (other.emailAddress != null) {
				return false;
			}
		} else if (!emailAddress.equals(other.emailAddress)) {
			return false;
		}
		if (enabled != other.enabled) {
			return false;
		}
		return true;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result + (enabled ? 1231 : 1237);
		return result;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEmailAddress(String name) {
		this.emailAddress = name;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return enabled ? "UserEmailType [emailAddress=" + emailAddress + "]" : "disabled";
	}

}
