/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.commands;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.ui.IWorkbenchPart;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.util.observer.IObserver;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GridBeanActivity;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditorInput;

/**
 * @author demuth
 */
public class OpenJobEditorForActivityCommand extends OpenJobEditorCommand {

	private GridBeanActivity gridBeanActivity;

	public OpenJobEditorForActivityCommand(GridBeanActivity gridBeanActivity) {
		super(null);
		this.gridBeanActivity = gridBeanActivity;
	}

	@Override
	public GridBeanClient<IWorkbenchPart> getGridBeanClient() {
		return gridBeanActivity.getGridBeanClient();
	}

	@Override
	protected JobEditorInput getJobEditorInput() {
		return new JobEditorInput(gridBeanActivity);
	}

	@Override
	protected IPath getJobPath() {
		return gridBeanActivity.getJobFile();
	}

	@Override
	public void redo() {

		try {

			long start = System.currentTimeMillis();
			while (!gridBeanActivity.isGridBeanSelected()
					&& System.currentTimeMillis() - start < 10000) {
				Thread.sleep(50);
			}

			if (gridBeanActivity.jobLoaded()) {
				// GridBean already loaded => just reopen editor or pass focus
				// to it
				openEditor();
			} else {
				// GridBean isn't loaded but was loaded before
				// => restore persisted data
				gridBeanActivity.loadJob(new IObserver() {
					public void observableUpdate(Object arg0, Object arg1) {
						openEditor();
					}
				});
			}
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR, "Could not open job editor!", e);
		}
	}

}