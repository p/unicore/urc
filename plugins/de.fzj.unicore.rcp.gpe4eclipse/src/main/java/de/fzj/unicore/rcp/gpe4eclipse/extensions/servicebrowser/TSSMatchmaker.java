package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import com.intel.gpe.clients.api.Job;
import com.intel.gpe.gridbeans.jsdl.JobWithParameters;
import com.intel.gpe.gridbeans.parameters.GridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.ResourceProviderAdapter;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;

public class TSSMatchmaker extends TSMatchmaker {

	private static final IGridBeanParameter CONTEXT_PARAM = new GridBeanParameter(GPE4EclipseConstants.CONTEXT,GPE4EclipseConstants.PARAMETER_TYPE_CONTEXT);
	
	public TSSMatchmaker(NodePath nodePath) {
		super(nodePath);
	}

	public String canHandleJob(Job j) {
		if(j instanceof JobWithParameters)
		{
			// return false when the job is part of a workflow
			// (as only TSFs maybe selected there)
			// TODO this is a little hackish
			JobWithParameters jobWithParams = (JobWithParameters) j;
			GridBeanContext context = (GridBeanContext) jobWithParams.getParameterValue(CONTEXT_PARAM);
			if(context != null && context.isPartOfWorkflow())
			{
				return "Target Systems cannot directly be selected for workflow jobs.";
			}
		}
		TargetSystemNode n = (TargetSystemNode) NodeFactory.createNode(getEPR());
		ResourceProviderAdapter props = new ResourceProviderAdapter(
				n.getTargetSystemProperties());
		try {
			return canHandleJob(j, props, n.getURI());
		} finally {
			n.dispose();
		}
	}
}
