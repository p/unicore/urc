/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse;

import java.util.List;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.IWorkbenchPart;
import org.unigrids.services.atomic.types.ProtocolType;

import com.intel.gpe.client.impl.configurators.UnicoreCommonKeys;
import com.intel.gpe.clients.all.ClientAdapter;
import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.clients.all.providers.OutcomeProvider;
import com.intel.gpe.clients.all.utils.GPEFileFactoryImpl;
import com.intel.gpe.clients.api.RegistryClient;
import com.intel.gpe.clients.api.async.RequestExecutionService;
import com.intel.gpe.clients.api.cache.DefaultProperties;
import com.intel.gpe.clients.api.cache.GPEClientProperties;
import com.intel.gpe.clients.api.configurators.FileProviderConfigurator;
import com.intel.gpe.clients.api.configurators.NetworkConfigurator;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.common.utils.DelegatingExtensionClassloader;
import de.fzj.unicore.rcp.common.utils.UnicoreStorageTools;
import de.fzj.unicore.rcp.gpe4eclipse.clientstubs.IWSRFClientFactory;
import de.fzj.unicore.rcp.gpe4eclipse.clientstubs.WSRFClientFactory;
import de.fzj.unicore.rcp.gpe4eclipse.security.SWTGPESecurityManagerImpl;

/**
 * @author demuth
 * 
 */
public class SWTClient extends ClientAdapter implements ISWTClient,
		IPropertyChangeListener {

	private OutcomeProvider outcomeProvider;
	private GPEClientProperties gPEClientProperties;
	private IWSRFClientFactory wSRFCLientFactory;

	public SWTClient() {
		try {

			setUserPreferences(GPEActivator.getDefault().getGPEPreferences());

			updatePreferredUnicoreFileTransferProtocols();

			NetworkConfigurator.getConfigurator().configure();

			asyncClient = new SWTAsyncClient();
			messageProvider = new SWTMessageProvider(getUserPreferences());
			GPEFileFactoryImpl fileFactory = new GPEFileFactoryImpl();

			this.fileFactory = fileFactory;
			fileProvider = FileProviderConfigurator.getConfigurator()
					.getFileProvider();
			securityManager = new SWTGPESecurityManagerImpl();

			outcomeProvider = new OutcomeProvider();

			securityManager.init(messageProvider, getUserPreferences(), this,
					null);

			// wsdlProvider = new WSDLProviderImpl();

			DefaultProperties props = new DefaultProperties();
			gPEClientProperties = props;

			syncClient = RequestExecutionService.getDefaultSyncClient();

		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while initializing application GUI: "+e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.gpe4eclipse.ISWTClient#createGridBeanClient()
	 */
	public GridBeanClient<IWorkbenchPart> createGridBeanClient() {

		ClassLoader classLoader = new DelegatingExtensionClassloader(
				SWTClient.class.getClassLoader(),
				GPE4EclipseConstants.GB_SERIALIZATION_EXTENSION_POINTS);
		try {
			return new SWTGridBeanClientImpl(securityManager, classLoader,
					outcomeProvider, this, fileProvider, getUserPreferences(),
					null);
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while initializing application GUI: "+e.getMessage(), e);
			return null;
		}
	}

	public GPEClientProperties getGPEClientProperties() {
		return gPEClientProperties;
	}

	public OutcomeProvider getOutcomeProvider() {
		return outcomeProvider;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.Client#getRegistries()
	 */
	public List<RegistryClient> getRegistries() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.gpe4eclipse.ISWTClient#getWSRFClientFactory()
	 */
	public IWSRFClientFactory getWSRFClientFactory() {
		if (wSRFCLientFactory == null) {
			wSRFCLientFactory = new WSRFClientFactory();
		}
		return wSRFCLientFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.all.ClientAdapter#internalClone()
	 */
	@Override
	public ClientAdapter internalClone() {
		SWTClient clone = new SWTClient();
		clone.gPEClientProperties = getGPEClientProperties();
		clone.wSRFCLientFactory = getWSRFClientFactory();
		clone.outcomeProvider = getOutcomeProvider();
		return clone;
	}

	public void propertyChange(PropertyChangeEvent event) {
		if (Constants.P_DEFAULT_FILE_TRANSFER_PROTOCOL.equals(event
				.getProperty())) {
			updatePreferredUnicoreFileTransferProtocols();
		}

	}

	public int showQuestion(String question, String[] possibleAnswers,
			int defaultAnswer) {
		return messageProvider.showQuestion(question, possibleAnswers,
				defaultAnswer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.StandaloneClient#shutdown()
	 */
	public void shutdown() {
		UnicoreCommonActivator.getDefault().getPreferenceStore()
				.removePropertyChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.StandaloneClient#startup()
	 */
	public void startup() {
		UnicoreCommonActivator.getDefault().getPreferenceStore()
				.addPropertyChangeListener(this);
	}

	private void updatePreferredUnicoreFileTransferProtocols() {
		ProtocolType.Enum[] protos = UnicoreStorageTools.getPreferredProtocolOrder();
		String s = "";
		for(int i = 0; i<protos.length; i++)
		{
			s+=protos[i].toString();
			if(i < protos.length-1) s += ",";
		}
		getUserPreferences().set(
				UnicoreCommonKeys.PREFERRED_FILE_TRANSFER_PROTOCOLS, s);
	}

}
