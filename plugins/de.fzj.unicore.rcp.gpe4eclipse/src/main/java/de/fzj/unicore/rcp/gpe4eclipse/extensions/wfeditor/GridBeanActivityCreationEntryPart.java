package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import org.eclipse.gef.internal.ui.palette.editparts.ToolEntryEditPart;
import org.eclipse.gef.palette.PaletteEntry;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;

public class GridBeanActivityCreationEntryPart extends ToolEntryEditPart {

	public GridBeanActivityCreationEntryPart(PaletteEntry paletteEntry) {
		super(paletteEntry);
	}
	
	public Object getAdapter(Class key) {
		if (key == GridBeanInfo.class) {
			GridBeanActivityCreationEntry entry = (GridBeanActivityCreationEntry) getModel();
			return entry.getFactory().getGridBeanInfo();
		}
		return super.getAdapter(key);
	}
	

}
