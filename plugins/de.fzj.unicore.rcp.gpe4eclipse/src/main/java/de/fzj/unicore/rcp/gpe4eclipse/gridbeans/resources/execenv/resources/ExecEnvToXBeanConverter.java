/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.resources;

import java.util.Map;
import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.eclipse.core.runtime.IStatus;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ResourcesType;

import com.intel.gpe.clients.api.jsdl.JSDLElement;
import com.intel.gpe.gridbeans.jsdl.JavaToXBeanElementConverter;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ArgumentType;
import eu.unicore.jsdl.extensions.ArgumentDocument.Argument;
import eu.unicore.jsdl.extensions.CmdType;
import eu.unicore.jsdl.extensions.ExecutionEnvironmentDocument;
import eu.unicore.jsdl.extensions.ExecutionEnvironmentDocument.ExecutionEnvironment;
import eu.unicore.jsdl.extensions.OptionType;
import eu.unicore.jsdl.extensions.UserCmdType;

/**
 * @author sholl
 */
public class ExecEnvToXBeanConverter extends CommonXBeanConverter implements JavaToXBeanElementConverter {

	public void convertToXBean(JobDefinitionType jobDef, JSDLElement element) {
		ExecEnvValueType val = (ExecEnvValueType) element;

		ResourcesType id = getOrCreateResources(jobDef);
		ExecutionEnvironmentDocument posix = null;
		UserCmdType userPreCommand = null;
		UserCmdType userPostCommand = null;
		XmlCursor cursor = id.newCursor();

		try {
			boolean posixExists = cursor
					.toChild(eu.unicore.jsdl.extensions.ExecutionEnvironmentDocument.type
							.getDocumentElementName());
			if (posixExists) {
				XmlObject o = cursor.getObject();
				posix = ExecutionEnvironmentDocument.Factory.newInstance();
				posix.setExecutionEnvironment((ExecutionEnvironment) ExecutionEnvironmentDocument.Factory
						.parse(o.newReader()));
				cursor.removeXml();

			} else {
				posix = ExecutionEnvironmentDocument.Factory.newInstance();
				posix.addNewExecutionEnvironment();

			}
			cursor.dispose();
			cursor = id.newCursor();
			cursor.toEndToken();

			ExecutionEnvironment executionEnvironment = posix
					.getExecutionEnvironment();
			int selected = val.getSelected();
			if (selected < 0 || val.getExecEnv() == null
					|| selected >= val.getExecEnv().size())
				return;
			ExecEnvType execEnvType = val.getExecEnv().get(selected);
			executionEnvironment.setName(execEnvType.getName().getLocalPart());
			String version = execEnvType.getVersion();
			if (version != null) {
				executionEnvironment.setVersion(version);
			}

			Map<QName, ExecEnvValue<?>> valuereferences = execEnvType
					.getValuereferences();
			for (QName name : valuereferences.keySet()) {
				ExecEnvValue<?> execEnvValue = valuereferences.get(name);
				if (execEnvValue.isEnabled()) {
					switch (execEnvValue.getType()) {
					case ARGUMENT: {
						Argument addNewArgument = executionEnvironment
								.addNewArgument();
						addNewArgument.setName(name.getLocalPart());
						if (execEnvValue.getValue() != null) {
							addNewArgument.setValue(execEnvValue.getValue()
									.toString());
						} else {
							addNewArgument.setNil();
						}
						break;
					}
					case OPTION: {
						Object value = execEnvValue.getValue();
						if ((value instanceof Boolean) && ((Boolean) value)) {
							OptionType op = executionEnvironment.addNewOption();
							op.setName(name.getLocalPart());
						}
						break;
					}
					case PRECOMMAND: {
						Object value = execEnvValue.getValue();
						if ((value instanceof Boolean) && ((Boolean) value)) {
							CmdType addNewPreCommand = executionEnvironment
									.addNewPreCommand();
							addNewPreCommand.setName(name.getLocalPart());
						}
						break;
					}
					case POSTCOMMAND: {
						Object value = execEnvValue.getValue();
						if ((value instanceof Boolean) && ((Boolean) value)) {
							CmdType addNewPostCommand = executionEnvironment
									.addNewPostCommand();
							addNewPostCommand.setName(name.getLocalPart());
						}
						break;
					}
					case USERPRECOMMAND: {
						Object value = execEnvValue.getValue();
						if (value != null) {
							userPreCommand = executionEnvironment
									.addNewUserPreCommand();
							userPreCommand.setStringValue(value.toString());

						}
						break;
					}
					case USERPOSTCOMMAND: {
						Object value = execEnvValue.getValue();
						if (value != null) {
							userPostCommand = executionEnvironment
									.addNewUserPostCommand();
							userPostCommand.setStringValue(value.toString());
						}
						break;
					}

					default:
						break;
					}

				}

			}
			// The execution environment arguments are stored in a map without
			// any ordering. We have to be sure that first userPreCommand and
			// userPostCommand are set and then their child arguments.
			for (QName name : valuereferences.keySet()) {

				ExecEnvValue<?> execEnvValue = valuereferences.get(name);
				if (execEnvValue.isEnabled()) {
					if (execEnvValue.getType().equals(
							ArgumentType.USERPRECOMMAND_RUNONLOGINNODE)) {
						if (userPreCommand != null) {
							Object value = execEnvValue.getValue();
							userPreCommand.setRunOnLoginNode((Boolean) value);
						}
					} else if (execEnvValue.getType().equals(
							ArgumentType.USERPOSTCOMMAND_RUNONLOGINNODE)) {
						if (userPostCommand != null) {
							Object value = execEnvValue.getValue();
							userPostCommand.setRunOnLoginNode((Boolean) value);
						}

					}

				}
			}

			XmlCursor from = posix.newCursor();
			try {
				from.toFirstChild();
				from.copyXml(cursor);
			} finally {

				from.dispose();
			}

		} catch (Exception e) {

			GPEActivator.log(IStatus.ERROR,
					"Unable to add Execution Environment to job.", e);
		} finally {
			cursor.dispose();
		}

	}

	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.gridbeans.jsdl.JavaToXBeanElementConverter#getSourceType()
	 */

	public Class<ExecEnvValueType> getSourceType() {

		return ExecEnvValueType.class;
	}

}
