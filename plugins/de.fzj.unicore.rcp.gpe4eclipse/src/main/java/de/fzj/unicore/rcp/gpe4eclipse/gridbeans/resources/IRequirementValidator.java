package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

public interface IRequirementValidator {

	/**
	 * Returns null if the given value is valid and an error message if the
	 * value is invalid.
	 * 
	 * @param value
	 * @return
	 */
	public String isValid(IRequirementValue value);
}
