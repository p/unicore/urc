package de.fzj.unicore.rcp.gpe4eclipse.ui;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IElementFactory;
import org.eclipse.ui.IMemento;

import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GPEPathUtils;

public class JobEditorInputFactory implements IElementFactory {

	/**
	 * Factory id. The plug-in registers a factory by this name with the
	 * "org.eclipse.ui.elementFactories" extension point.
	 */
	public static final String ID_FACTORY = "de.fzj.unicore.rcp.gpe4eclipse.extensions.JobEditorInputFactory";

	/**
	 * Tag for the URI of the job resource.
	 */
	private static final String TAG_URI = "jobEditorUri";

	private static final String INVALID_URI = "invalidURI";

	/*
	 * (non-Javadoc) Method declared on IElementFactory.
	 */
	public IAdaptable createElement(IMemento memento) {
		// Get the input location
		String s = memento.getString(TAG_URI);
		if (s == null) {
			return null;
		}
		JobEditorInput result = null;
		if (s.equals(INVALID_URI)) {
			result = new JobEditorInput();
			result.setInvalidWhilePersisting(true);
		} else {
			try {
				IPath p = new Path(s);

				// path is supposed to be workspace-relative
				if (p.isAbsolute()) {
					// deal with old paths
					p = PathUtils.extractWorkspaceRelativePath(p.toFile()
							.toURI());
					if (p == null) {
						throw new Exception(
								"File "
										+ s
										+ " does not belong to a project in the workspace. Please re-open the job from the workspace.");
					}
				}
				IPath resolved = ResourcesPlugin.getWorkspace().getRoot().getFile(p)
						.getLocation();
				if (resolved == null) {
					GPEActivator
							.log(IStatus.ERROR,
									"Unable to restore the job file's address: the file "+p.toOSString()+" could not be found inside the workspace.");
					result = new JobEditorInput();
					result.setInvalidWhilePersisting(true);
				} else {
					String activityName = GPEPathUtils
							.extractJobNameFromJobPath(resolved);
					result = new JobEditorInput(resolved, activityName);
				}

			} catch (Exception e) {
				GPEActivator
						.log(IStatus.ERROR,
								"Unable to restore the job file's address:\n"
										+ e.getMessage()
										+ "\nNot opening job editor. ", e);
				result = new JobEditorInput();
				result.setInvalidWhilePersisting(true);
			}
		}
		return result;
	}

	/**
	 * Saves the state of the given job editor input into the given memento.
	 * 
	 * @param memento
	 *            the storage area for element state
	 * @param input
	 *            the file editor input
	 */
	public static void saveState(IMemento memento, JobEditorInput input) {
		try {
			if (input.getFile() == null || !input.getFile().exists()) {
				memento.putString(TAG_URI, INVALID_URI);
			} else {
				IPath p = input.getPath();
				p = PathUtils.extractWorkspaceRelativePath(p.toFile().toURI());
				memento.putString(TAG_URI, p.toOSString());
			}

		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while saving job editor state: "+e.getMessage(), e);
		}

	}

}
