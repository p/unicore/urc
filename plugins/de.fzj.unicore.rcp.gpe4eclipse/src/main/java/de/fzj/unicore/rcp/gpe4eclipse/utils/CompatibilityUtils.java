package de.fzj.unicore.rcp.gpe4eclipse.utils;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;

import com.intel.gpe.clients.api.transfers.IAddressOrValue;
import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IEnvironmentVariableParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileSetParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IEnvVariableSourceTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageTypeRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.VariableSourceTypeRegistry;

public class CompatibilityUtils {

	/**
	 * This method is needed for backwards compatibility with versions < 6.2.1.
	 * In these versions, workflow variables were referenced differently. The
	 * method lets the associated extension check for any obsolete file
	 * addresses and fix them.
	 */
	public static void fixEnvironmentParameterValues(IGridBean gridBean,
			String oldVersion, String currentVersion) {
		List<IGridBeanParameter> params = new ArrayList<IGridBeanParameter>(
				gridBean.getInputParameters());

		for (IGridBeanParameter param : params) {
			if (GridBeanParameterType.ENVIRONMENT_VARIABLE.equals(param
					.getType())) {
				IEnvironmentVariableParameterValue value = (IEnvironmentVariableParameterValue) gridBean
						.get(param.getName());
				if (value != null) {
					IAddressOrValue fixed = value.getVariableValue().clone();
					VariableSourceTypeRegistry registry = GPEActivator
							.getDefault().getVariableSourceTypeRegistry();
					IEnvVariableSourceTypeExtensionPoint ext = registry
							.getDefiningExtension(value.getVariableValue()
									.getProtocol().getName());
					if (ext != null) {
						try {
							fixed = ext.checkAndFixAddress(fixed, oldVersion,
									currentVersion);
							if (value.getVariableValue().equals(fixed)) {
								IEnvironmentVariableParameterValue clone = value
										.clone();
								clone.setVariableValue(fixed);
								gridBean.set(param.getName(), clone);
							}
						} catch (AbstractMethodError e) {
							// this method has been introduced later
							// nevermind if this is not implemented
						}
					}

				}
			}
		}

	}

	/**
	 * This method is needed for backwards compatibility with versions < 6.2.1.
	 * In these versions, workflow files did not contain any variables that were
	 * later replaced during workflow submission. The method lets the associated
	 * extension check for any obsolete file addresses and fix them.
	 */
	public static void fixFileParameterValue(IFileParameterValue file,
			String oldVersion, String currentVersion) {

		IGridFileAddress address = file.isInputParameter() ? file.getSource()
				: file.getTarget();
		IGridFileAddress fixed = address.clone();
		QName protocol = address.getProtocol().getName();
		StageTypeRegistry registry = file.isInputParameter() ? GPEActivator
				.getDefault().getStageInTypeRegistry() : GPEActivator
				.getDefault().getStageOutTypeRegistry();
		IStageTypeExtensionPoint ext = registry.getDefiningExtension(protocol);
		if (ext != null) {

			try {
				fixed = ext.checkAndFixAddress(fixed, oldVersion,
						currentVersion);
			} catch (AbstractMethodError e) {
				// this method has been introduced later
				// nevermind if this is not implemented
			}

		}
		if (!address.equals(fixed)) {
			if (file.isInputParameter()) {
				file.setSource(fixed);
			} else {
				file.setTarget(fixed);
			}
		}
	}

	public static void fixFileParameterValues(IProject project,
			IGridBean gridBean, String oldVersion, String currentVersion)
			throws Exception {
		IFolder newParent = PathUtils.absolutePathToEclipseFolder(GPEPathUtils
				.inputFileDirForProject(project));

		if (!newParent.exists()) {
			newParent.create(true, true, null);
		}
		List<IGridBeanParameter> params = new ArrayList<IGridBeanParameter>(
				gridBean.getInputParameters());
		params.addAll(gridBean.getOutputParameters());

		for (IGridBeanParameter param : params) {
			if (GridBeanParameterType.FILE.equals(param.getType())) {
				IFileParameterValue value = (IFileParameterValue) gridBean
						.get(param.getName());
				if (value != null) {
					IFileParameterValue fixed = value.clone();
					fixRelativeInputFile(project, newParent, fixed, oldVersion,
							currentVersion);
					fixFileParameterValue(fixed, oldVersion, currentVersion);
					if (!value.equals(fixed)) {
						gridBean.set(param.getName(), fixed);
					}
				}

			} else if (GridBeanParameterType.FILE_SET.equals(param.getType())) {
				IFileSetParameterValue value = (IFileSetParameterValue) gridBean
						.get(param.getName());
				if (value != null) {
					List<IFileParameterValue> newFiles = new ArrayList<IFileParameterValue>();
					boolean changeOccured = false;
					for (IFileParameterValue file : value.getFiles()) {
						IFileParameterValue fixed = file.clone();

						fixRelativeInputFile(project, newParent, fixed,
								oldVersion, currentVersion);
						fixFileParameterValue(fixed, oldVersion, currentVersion);
						if (file.equals(fixed)) {
							newFiles.add(file);
						} else {
							changeOccured = true;
							newFiles.add(fixed);
						}
					}
					if (changeOccured) {
						value.setFiles(newFiles);
						gridBean.set(param.getName(), value);
					}
				}
			}
		}
	}

	private static void fixRelativeInputFile(IProject project,
			IFolder newParent, IFileParameterValue file, String oldVersion,
			String currentVersion) throws Exception {

		if (oldVersion.compareTo("6.2.1") >= 0) {
			return;
		}
		if (!file.isInputParameter()
				|| !ProtocolConstants.LOCAL_PROTOCOL.equals(file.getSource()
						.getProtocol())) {
			return;
		}
		if (file.getSource().getParentDirAddress() == null
				|| file.getSource().getParentDirAddress().trim().length() == 0) {
			IFile oldFile = project.getFile(file.getSource().getRelativePath());
			// move the file to the target dir
			IFile newFile = newParent.getFile(file.getSource()
					.getRelativePath());
			oldFile.move(newFile.getProjectRelativePath(), false, null);
		}
	}
}
