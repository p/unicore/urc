package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.jface.resource.ImageDescriptor;

public class GridBeanActivityCreationEntry extends CombinedTemplateCreationEntry {

	private GridBeanActivityFactory factory;
	


	public GridBeanActivityCreationEntry(String appName,
			GridBeanActivityFactory factory, ImageDescriptor iconSmall,
			ImageDescriptor iconLarge) {
		super(appName, 
				"Create a new application activity for the " + appName
				+ " application", factory, factory, iconSmall, iconLarge);
		this.factory = factory;
	}



	public GridBeanActivityFactory getFactory() {
		return factory;
	}

}
