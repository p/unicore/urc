package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlCursor;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDescriptionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ResourcesType;

import com.intel.gpe.clients.api.jsdl.JSDLElement;
import com.intel.gpe.gridbeans.jsdl.JavaToXBeanElementConverter;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.resources.CommonXBeanConverter;
import de.fzj.unicore.rcp.gpe4eclipse.utils.RangeValueTypeConverter;

/**
 * Maps {@link SiteSpecificRequirementSettingType} to the old-style (pre 6.4) resource requirement in XNJS namespace. 
 *
 */
public class SiteSpecificSettingToXBeanConverter extends CommonXBeanConverter implements
		JavaToXBeanElementConverter {

	public void convertToXBean(JobDefinitionType jobDef, JSDLElement element) {
		SiteSpecificRequirementSettingType child = (SiteSpecificRequirementSettingType) element;

		XmlCursor parentCursor = getOrCreateResources(jobDef).newCursor();
		parentCursor.toEndToken();
		QName RESOURCE_SETTING_QNAME = new QName(
				"http://www.fz-juelich.de/unicore/xnjs/idb", "ResourceSetting",
				"xnjs");
		QName NAME_QNAME = new QName(
				"http://www.fz-juelich.de/unicore/xnjs/idb", "Name", "xnjs");
		QName VALUE_QNAME = new QName(
				"http://www.fz-juelich.de/unicore/xnjs/idb", "Value", "xnjs");
		parentCursor.beginElement(RESOURCE_SETTING_QNAME);
		parentCursor.beginElement(NAME_QNAME);
		parentCursor.insertChars(child.getName());
		parentCursor.toNextToken();
		parentCursor.beginElement(VALUE_QNAME);

		XmlCursor childCursor = RangeValueTypeConverter.convertToXBean(
				child.getRange()).newCursor();
		childCursor.toChild(0);
		childCursor.copyXml(parentCursor);
	}

	public Class<SiteSpecificRequirementSettingType> getSourceType() {
		return SiteSpecificRequirementSettingType.class;
	}

}
