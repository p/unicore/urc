/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;

/**
 * is the contentProvider for the ResourceRequirementsViewer
 * 
 * @author Christian Hohmann
 * 
 */
public class RequirementContentProvider implements IStructuredContentProvider,
		IRequirementListViewer, PropertyChangeListener {

	private class Refresher implements Runnable {

		public void run() {

			try {
				if (requirementList != null && lastModified != null) {
					requirementList.disableClashingRequirements(lastModified);
				}
			} finally {
				refreshing = false; // call this BEFORE actually refreshing the
									// viewer, otherwise you might miss changes
									// during refresh
			}

			if (viewer != null) {
				viewer.refresh();
			}
		}

	}

	private RequirementList requirementList;

	private Viewer viewer;
	private IGridBeanModel gridBeanModel;
	private boolean listeningToGridBeanModel = false;
	private final Refresher refresher = new Refresher();
	private boolean refreshing = false;

	private ResourceRequirement lastModified = null;

	public RequirementContentProvider(IGridBeanModel gridBeanModel) {
		setGridBeanModel(gridBeanModel);

	}

	public void dispose() {
		if (requirementList != null) {
			requirementList.removeChangeListener(this);
		}
		if (gridBeanModel != null) {
			gridBeanModel.removePropertyChangeListener(this);
		}
	}

	public Object[] getElements(Object inputElement) {
		return requirementList.toArray();
	}

	public IGridBeanModel getGridBeanModel() {
		return gridBeanModel;
	}

	public RequirementList getResourceRequirementList() {
		return this.requirementList;
	}

	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		lastModified = null;
		viewer = v;

		if (newInput != null) {
			requirementList = (RequirementList) newInput;
		} else {
			requirementList = null;
		}
		if (oldInput != null) {
			((RequirementList) oldInput).removeChangeListener(this);
		}

		if (requirementList != null) {

			requirementList.addChangeListener(this);
		}

	}

	public void notifyListCleared() {
		if(!listeningToGridBeanModel) return;
		lastModified = null;
		refreshViewer();
	}

	public void notifyRequirementAdded(ResourceRequirement resourceRequirement) {
		if(!listeningToGridBeanModel) return;
		lastModified = resourceRequirement;
		refreshViewer();
	}

	public void notifyRequirementRemoved(ResourceRequirement resourceRequirement) {
		if(!listeningToGridBeanModel) return;
		writeRequirementToModel(resourceRequirement);
		lastModified = resourceRequirement;
		refreshViewer();
	}

	public void notifyRequirementUpdated(ResourceRequirement resourceRequirement) {
		if(!listeningToGridBeanModel) return;
		writeRequirementToModel(resourceRequirement);
		lastModified = resourceRequirement;
		refreshViewer();
	}

	public void propertyChange(PropertyChangeEvent arg0) {
		if (!listeningToGridBeanModel) {
			return;
		}
		if (IGridBeanModel.RESOURCES.toString().equals(arg0.getPropertyName())) {
			ResourcesParameterValue value = (ResourcesParameterValue) arg0
					.getNewValue();
			readRequirementsFromValue(value);
		}

	}

	public void readRequirementsFromValue(ResourcesParameterValue value) {
		if (getResourceRequirementList() == null) {
			return;
		}
		ResourceRequirement[] reqs = getResourceRequirementList().toArray();
		for (ResourceRequirement resourceRequirement : reqs) {
			resourceRequirement.loadFrom(value);
		}
		refreshViewer();
	}

	private void refreshViewer() {
		if (refreshing) {
			return;
		}
		refreshing = true;
		Display d = PlatformUI.getWorkbench().getDisplay();
		if (d.isDisposed()) {
			return;
		}
		boolean isMainThread = Thread.currentThread().equals(d.getThread());
		if (isMainThread) {
			refresher.run();
		} else {
			d.asyncExec(refresher);
		}
	}

	public void setGridBeanModel(IGridBeanModel gridBeanModel) {
		if (this.gridBeanModel != null) {
			this.gridBeanModel.removePropertyChangeListener(this);
		}
		if (gridBeanModel != null) {
			gridBeanModel.addPropertyChangeListener(this);
		}
		this.gridBeanModel = gridBeanModel;
	}

	public void writeRequirementsToValue(ResourcesParameterValue value) {
		if (getResourceRequirementList() == null) {
			return;
		}
		ResourceRequirement[] reqs = getResourceRequirementList().toArray();
		for (ResourceRequirement resourceRequirement : reqs) {
			resourceRequirement.writeTo(value);
		}
	}

	private void writeRequirementToModel(ResourceRequirement resourceRequirement) {
		ResourcesParameterValue value = (ResourcesParameterValue) getGridBeanModel()
				.get(IGridBeanModel.RESOURCES);
		value = value.clone();
		resourceRequirement.writeTo(value);
		if (getGridBeanModel() != null) {
			listeningToGridBeanModel = false;
			getGridBeanModel().set(IGridBeanModel.RESOURCES, value);
			listeningToGridBeanModel = true;
		}
	}

	public void setListeningToGridBeanModel(boolean listeningToGridBeanModel)
	{
		this.listeningToGridBeanModel = listeningToGridBeanModel;
	}

}
