package de.fzj.unicore.rcp.gpe4eclipse.problems;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.logmonitor.problems.ShowPreferencePageSolution;

public class CheckGridBeanDirSolution extends ShowPreferencePageSolution {

	public CheckGridBeanDirSolution() {
		super("CHECK_GRIDBEAN_DIR", "GRIDBEAN_NOT_FOUND",
				"Check the list of application GUI plugin directories...");
	}

	@Override
	protected String getPreferencePageId() {
		return GPE4EclipseConstants.PREFERENCE_PAGE_ID_JOBS;
	}

}
