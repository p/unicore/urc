package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeCacheListener;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeData;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

public class GridBeanServiceNodeCacheListener implements INodeCacheListener {


	public void nodeDataRemoved(INodeData nodeData) {
		if(isOfInterest(nodeData))
		{
			GPEActivator.getDefault().getGridBeanServiceRegistry().unregisterService(nodeData.getEpr());
		}
	}

	@Override
	public void nodeDataAdded(INodeData nodeData) {

		if (isOfInterest(nodeData)) {
			GPEActivator.getDefault().getGridBeanServiceRegistry().registerService(nodeData.getEpr());
		}

	}

	private boolean isOfInterest(INodeData data)
	{
		if(data == null) return false;
		Object type = data.getProperty(Node.PROPERTY_TYPE);
		return GridBeanServiceNode.TYPE.equals(type);
	}



}
