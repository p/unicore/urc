package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import java.util.ArrayList;
import java.util.List;

import org.apache.xmlbeans.XmlObject;
import org.unigrids.x2006.x04.services.tsf.TargetSystemFactoryPropertiesDocument;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument;

import de.fzj.unicore.rcp.servicebrowser.nodes.INodeData;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;

public abstract class TargetSystemNodeCacheListener<T> extends AbstractNodeCacheListener {

	protected abstract void updateEntries(List<T> entries, INodeData nodeData);
	
	protected abstract List<T> getEntries(XmlObject resourcePropertyDocument); 


	public void nodeDataAdded(INodeData nodeData) {
		super.nodeDataAdded(nodeData);
	}
	
	@Override
	protected void update(INodeData nodeData) {
		
		List<T> list = new ArrayList<T>();
		try {
			int state = (Integer) nodeData.getProperty(Node.PROPERTY_STATE);
			if(state < Node.STATE_FAILED)
			{
				String s = (String) nodeData.getProperty(WSRFNode.PROPERTY_CACHED_RPS);
				XmlObject o = XmlObject.Factory.parse(s);
				if(isTSS(nodeData))
				{
					o = o.changeType(TargetSystemPropertiesDocument.type);
				}
				else
				{
					o = o.changeType(TargetSystemFactoryPropertiesDocument.type);
				}
				list = getEntries(o);
			}
		} catch (Exception e) {
			// do nothing
		}
		updateEntries(list, nodeData);
	}
	
	
	
	protected boolean isOfInterest(INodeData nodeData) {
		return isTSF(nodeData) || isTSS(nodeData);
	}

	protected boolean isTSF(INodeData nodeData)
	{
		Object type = nodeData.getProperty(Node.PROPERTY_TYPE);
		return TargetSystemFactoryNode.TYPE.equals(type);
	}
	
	protected boolean isTSS(INodeData nodeData)
	{
		Object type = nodeData.getProperty(Node.PROPERTY_TYPE);
		return TargetSystemNode.TYPE.equals(type);
	}
}
