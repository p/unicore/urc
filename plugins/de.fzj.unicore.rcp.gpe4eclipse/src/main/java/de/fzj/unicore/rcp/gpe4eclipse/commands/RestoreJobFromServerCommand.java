/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.commands;

import java.io.File;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.Job;
import com.intel.gpe.clients.api.JobClient;
import com.intel.gpe.clients.api.StorageClient;
import com.intel.gpe.clients.api.exceptions.GPEFileAddressUnresolvableException;
import com.intel.gpe.clients.api.transfers.IAddressOrValue;
import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.clients.api.transfers.IGridFileSetTransfer;
import com.intel.gpe.clients.api.transfers.IGridFileTransfer;
import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.clients.common.requests.TransferGridFilesRequest;
import com.intel.gpe.clients.common.transfers.GridFileSetTransfer;
import com.intel.gpe.clients.common.transfers.GridFileTransfer;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileSetParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.util.observer.IObserver;

import de.fzj.unicore.rcp.common.guicomponents.EnhancedWizardDialog;
import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobCreationWizard;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GPEPathUtils;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

/**
 * @author demuth
 */
public class RestoreJobFromServerCommand extends OpenJobEditorCommand {

	JobClient jobClient;

	public RestoreJobFromServerCommand(IAdaptable adaptable, JobClient jobClient) {
		super(adaptable);
		this.jobClient = jobClient;
	}

	/**
	 * This method checks whether an input file was uploaded from the local disk
	 * and adds it to the list of files to download. Local files with absolute
	 * paths are ignored.
	 */
	protected void analyzeAndAdd(IFileParameterValue value,
			IGridFileSetTransfer<IGridFileTransfer> transfers, StorageClient workingDir) {
		if (ProtocolConstants.LOCAL_PROTOCOL.equals(value.getSource()
				.getProtocol())) {
			if (!Path.fromOSString(value.getSource().getInternalString())
					.isAbsolute()) {
				IAddressOrValue target = value.getTarget();
				IGridFileAddress absoluteTarget = null;
				try {
					absoluteTarget = workingDir.getAbsoluteAddress(
							target.getInternalString(), null);
				} catch (GPEFileAddressUnresolvableException e) {
					GPEActivator.log(
							IStatus.ERROR,
							"Unable to resolve input file "
									+ target.getDisplayedString()
									+ " for download.", e);
					return;
				}

				IGridFileAddress absoluteSource = value.getSource().clone();
				absoluteSource.setParentDirAddress(getGridBeanClient()
						.getClient().getFileFactory().getTemporaryDirName());
				IGridFileTransfer transfer = new GridFileTransfer(
						absoluteTarget, absoluteSource);
				transfers.addFile(transfer);
			}
		}
	}

	/**
	 * This method tries to identify input files that were uploaded from the
	 * local disk and download them back to where they came from. Local files
	 * with absolute paths are ignored.
	 */
	protected void downloadInputFiles() {
		try {
			IGridBean model = getGridBeanClient().getGridBeanJob().getModel();
			Client client = getGridBeanClient().getClient();
			StorageClient workingDir = jobClient.getWorkingDirectory();
			IGridFileSetTransfer<IGridFileTransfer> transfers = new GridFileSetTransfer<IGridFileTransfer>();
			for (IGridBeanParameter param : model.getInputParameters()) {
				if (GridBeanParameterType.FILE.equals(param.getType())) {
					IFileParameterValue value = (IFileParameterValue) model
							.get(param.getName());
					analyzeAndAdd(value, transfers, workingDir);
				} else if (GridBeanParameterType.FILE_SET.equals(param
						.getType())) {
					IFileSetParameterValue value = (IFileSetParameterValue) model
							.get(param.getName());
					for (IFileParameterValue file : value.getFiles()) {
						analyzeAndAdd(file, transfers, workingDir);
					}
				}
			}

			TransferGridFilesRequest req = new TransferGridFilesRequest(client,
					transfers);
			client.executeRequest(req, new IObserver() {
				public void observableUpdate(Object arg0, Object arg1) {
					getGridBeanClient().reloadInputPanel(null);

					BackgroundJob j = new BackgroundJob (
							"refreshing project") {

						@Override
						protected IStatus run(IProgressMonitor monitor) {
							try {
								wizard.getProject().refreshLocal(
										IResource.DEPTH_INFINITE, monitor);
							} catch (CoreException e) {
								// this is a best effort service
							}
							return Status.OK_STATUS;
						}

					};
					j.schedule();

				}

			});
		} catch (Exception e) {
			GPEActivator
					.log(IStatus.ERROR,
							"Unable to download job input files from the job's working directory.",
							e);
		}
	}

	@Override
	public void redo() {
		super.redo();
		if (wizard.isJobProjectCreated()) {
			downloadInputFiles();
		}
	}

	@Override
	protected JobCreationWizard runWizard() {

		GridBeanInfo gbInfo = null;
		Job job = null;
		try {
			job = jobClient.getOriginalJSDL();
		} catch (Exception e) {
			GPEActivator
					.log(IStatus.ERROR,
							"Could not extract the job description from the submitted job.",
							e);
			return null;
		}
		try {
			gbInfo = GridBeanUtils.reconstructGridBeanInfo(job);
		} catch (Exception e) {
		}
		if (gbInfo == null) {
			GPEActivator
					.log(IStatus.ERROR,
							"Could not find application used for job submission on your system.");
			return null;
		}

		Shell shell = viewer == null ? new Shell(SWT.RESIZE) : viewer
				.getControl().getShell();
		GridBeanUtils.loadGridBeanAndWait(getGridBeanClient(), gbInfo,
				shell.getDisplay());
		GridBeanUtils.reconstructInputAndWait(getGridBeanClient(), jobClient,
				shell.getDisplay());
		try {
			GridBeanContext oldContext = (GridBeanContext) getGridBeanClient()
					.getGridBeanJob().getModel()
					.get(GPE4EclipseConstants.CONTEXT);
			GridBeanContext newContext = GridBeanUtils.createSolitaryContext();
			if (oldContext != null) {
				newContext.setTerminationTime(oldContext.getTerminationTime());
				
			}
			newContext.setNewlyCreated(false);
			getGridBeanClient().getGridBeanJob().getModel()
					.set(GPE4EclipseConstants.CONTEXT, newContext);

		} catch (Exception e) {
			GPEActivator
					.log(IStatus.ERROR,
							"Could not adjust context information for restored job.",
							e);
			return null;
		}

		JobCreationWizard wizard = new JobCreationWizard(false, true, false);
		StructuredSelection selection = new StructuredSelection();
		wizard.init(PlatformUI.getWorkbench(), selection);
		wizard.setApplication(gbInfo);
		wizard.setGridBeanClient(getGridBeanClient());

		EnhancedWizardDialog dialog = new EnhancedWizardDialog(shell, wizard);
		int result = dialog.open();
		if (result == IStatus.OK) {
			IProject project = wizard.getProject();
			String tempDir = GPEPathUtils.inputFileDirForProject(project)
					.toOSString() + File.separator;
			getGridBeanClient().getClient().getFileFactory()
					.setTemporaryDirName(tempDir);
			downloadInputFiles();

		}
		return wizard;
	}

}
