/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.utils;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.all.exceptions.GPEJobFailedException;
import com.intel.gpe.clients.api.JobClient;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditor;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.uas.JobManagement;

/**
 * This service keeps track of monitored job editors and updates job status in a
 * sequential manner until the job reaches a final state.
 * 
 * TODO: consider factoring the monitoring to do it concurrently and
 * independently. Right now, a pending update on one monitor will keep all
 * others from being updated in a timely fashion.
 * 
 * @author demuth, bjoernh
 * 
 */
public class JobMonitoringService implements Runnable {

	static Map<String, JobMonitorer> activeMonitorers = new HashMap<String, JobMonitorer>();
	static ScheduledExecutorService executor;
	
	/**
	 * Whether we are listening to changes of preferences. This is used to
	 * initially register a {@link PropertyChangeListener} that will change the
	 * scheduling interval if needed.
	 */
	static boolean listening = false;

	public void run() {
		if (activeMonitorers.size() > 0) {
			List<JobMonitorer> monitorers;
			synchronized (activeMonitorers) {
				monitorers = new ArrayList<JobMonitorer>(
						activeMonitorers.values());
			}
			for (JobMonitorer monitorer : monitorers) {
				final JobEditor jobEditor = monitorer.getJobEditor();
				if (jobEditor.isDisposed()) {
					stopMonitoring(jobEditor);
					continue;
				}

				try {
					final com.intel.gpe.clients.api.Status status = monitorer
					.pollStatus();
					JobClient client = jobEditor.getJobClient().getClient();
					if (status.isSuccessful()) {
						PlatformUI.getWorkbench().getDisplay()
						.asyncExec(new Runnable() {
							public void run() {
								jobEditor
								.setExecutionState(ExecutionStateConstants.STATE_SUCCESSFUL);
							}
						});
						stopMonitoring(jobEditor);
					} else if (status.isFailed()) {

						org.w3c.dom.Element log = client
						.getResourceProperty(JobManagement.RPLog);
						Exception e = new GPEJobFailedException(
								"Target site failed the job during execution.",
								log.getFirstChild().getNodeValue(), client);
						String msg = "The job " + client.getName()
						+ " has failed";
						GPEActivator.log(IStatus.ERROR, msg, e);
						jobEditor
						.setExecutionState(ExecutionStateConstants.STATE_FAILED);
						stopMonitoring(jobEditor);
					}
					
				} catch (Exception e) {
					GPEActivator
					.log(IStatus.ERROR,
							"Error while polling job status: "
							+ e.getMessage(), e);
					stopMonitoring(jobEditor);
					jobEditor
					.setExecutionState(ExecutionStateConstants.STATE_UNKNOWN);
				}

			}
		}

	}

	private static String getKey(JobEditor editor) {
		return editor.getJobClient().getClient().getURL();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.monitoring.IMonitorer#monitor(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram,
	 * org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public static void startMonitoring(JobEditor editor) {
		synchronized (activeMonitorers) {
			String key = getKey(editor);
			if (activeMonitorers.get(key) != null) {
				return;
			}
			JobMonitorer m = new JobMonitorer(editor);
			activeMonitorers.put(key, m);
			if (executor == null) {
				executor = Executors.newSingleThreadScheduledExecutor();
				int interval = GPEActivator.getDefault().getPreferenceStore()
				.getInt(GPE4EclipseConstants.P_JOB_POLLING_INTERVAL);
				if (!listening) {
					GPEActivator
					.getDefault()
					.getPreferenceStore()
					.addPropertyChangeListener(
							new IPropertyChangeListener() {
								public void propertyChange(
										PropertyChangeEvent event) {
									if (GPE4EclipseConstants.P_JOB_POLLING_INTERVAL
											.equals(event.getProperty())
											&& executor != null) {
										executor.shutdown();
										executor = Executors
										.newSingleThreadScheduledExecutor();
										int interval = (Integer) event
										.getNewValue();
										executor.scheduleWithFixedDelay(
												new JobMonitoringService(),
												interval, interval,
												TimeUnit.SECONDS);
									}
								}
							});
					listening = true;
				}
				executor.scheduleWithFixedDelay(new JobMonitoringService(),
						interval, interval, TimeUnit.SECONDS);
			}

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.monitoring.IMonitorer#stopMonitoring(de.fzj
	 * .unicore.rcp.wfeditor.model.WorkflowDiagram,
	 * org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	protected static void stopMonitoring(JobEditor editor) {

		synchronized (activeMonitorers) {
			String key = getKey(editor);
			activeMonitorers.remove(key);
			if (activeMonitorers.size() == 0) {
				executor.shutdown();
				executor = null;
			}
		}
	}
}
