/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.guicomponents.BooleanPropertyDescriptor;
import de.fzj.unicore.rcp.common.guicomponents.IntegerPropertyDescriptor;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.utils.PropertyMapComparator;

/**
 * @author demuth
 * 
 */
@XStreamAlias("SplittingOptions")
public class SplittingOptions extends PropertySource {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3427577865577301985L;
	// keys defining property names
	public static String PROP_ALLOW_SPLITTING = "allow splitting"; // is server
																	// side
																	// splitting
																	// enabled
																	// at all?
	public static String PROP_MAX_NUM_SUBJOBS = "maximum number of subjobs"; // how
																				// many
																				// subjobs
																				// may
																				// be
																				// created?

	public SplittingOptions() {
		setPropertyValue(PROP_ALLOW_SPLITTING, false);
		setPropertyValue(PROP_MAX_NUM_SUBJOBS, 10);
	}

	@Override
	protected void addPropertyDescriptors() {
		addPropertyDescriptor(new BooleanPropertyDescriptor(
				PROP_ALLOW_SPLITTING, PROP_ALLOW_SPLITTING));
		addPropertyDescriptor(new IntegerPropertyDescriptor(
				PROP_MAX_NUM_SUBJOBS, PROP_MAX_NUM_SUBJOBS));
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof SplittingOptions)) {
			return false;
		}
		SplittingOptions other = (SplittingOptions) o;
		if (this == other) {
			return true;
		}
		return PropertyMapComparator.equal(getProperties(),
				other.getProperties());

	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_ALLOW_SPLITTING);
		propertiesToCopy.add(PROP_MAX_NUM_SUBJOBS);
		return propertiesToCopy;
	}

	@Override
	public int hashCode() {
		return PropertyMapComparator.hashCode(getProperties());
	}

	public boolean isSplittingAllowed() {
		return (Boolean) getPropertyValue(PROP_ALLOW_SPLITTING);
	}

	public int maxNumSubjobs() {
		return (Integer) getPropertyValue(PROP_MAX_NUM_SUBJOBS);
	}

	@Override
	public String toString() {
		return "";
	}

}
