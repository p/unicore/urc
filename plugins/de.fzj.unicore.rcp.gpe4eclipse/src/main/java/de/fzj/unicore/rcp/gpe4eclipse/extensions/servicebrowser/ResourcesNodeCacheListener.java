package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.xmlbeans.XmlObject;
import org.unigrids.services.atomic.types.AvailableResourceType;
import org.unigrids.x2006.x04.services.tsf.TargetSystemFactoryPropertiesDocument;
import org.unigrids.x2006.x04.services.tsf.TargetSystemFactoryPropertiesDocument.TargetSystemFactoryProperties;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument.TargetSystemProperties;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.AvailableResource;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.ResourcesRegistry;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeData;

public class ResourcesNodeCacheListener extends TargetSystemNodeCacheListener<AvailableResource> {
	
	

	protected List<AvailableResource> getEntries(XmlObject rpDoc) {
		AvailableResourceType [] resources = null;
		if ((rpDoc instanceof TargetSystemFactoryPropertiesDocument)) {
			TargetSystemFactoryPropertiesDocument doc = (TargetSystemFactoryPropertiesDocument) rpDoc;
			TargetSystemFactoryProperties props = doc.getTargetSystemFactoryProperties();
			if (props == null)
				return Collections.emptyList();
			resources = props.getAvailableResourceArray();
		} else if (rpDoc instanceof TargetSystemPropertiesDocument) {
			TargetSystemPropertiesDocument doc = (TargetSystemPropertiesDocument) rpDoc;
			TargetSystemProperties props = doc.getTargetSystemProperties();
			if (props == null)
				return Collections.emptyList();
			resources = props.getAvailableResourceArray();
		}
		if (resources == null)
			return Collections.emptyList();
		return ResourcesRegistry.toPOJO(Arrays.asList(resources));
	}

	public void nodeDataRemoved(INodeData nodeData) {
		ResourcesRegistry.getDefaultInstance().remove(nodeData.getURI());
		super.nodeDataRemoved(nodeData);
	}


	@Override
	protected void updateEntries(List<AvailableResource> entries, INodeData nodeData) {
		ResourcesRegistry.getDefaultInstance().updateEntries(entries, nodeData.getURI());
	}


	

}
