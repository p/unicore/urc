package de.fzj.unicore.rcp.gpe4eclipse.views;

import java.awt.EventQueue;

import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.gridbeans.plugins.IPanel;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;

/**
 * This sample class demonstrates how to plug-in a new workbench view. The view
 * shows data obtained from the gridbean. The sample creates a dummy gridbean on
 * the fly, but a real implementation would connect to the gridbean available
 * either in this or another plug-in (e.g. the workspace). The view is connected
 * to the gridbean using a content provider.
 * <p>
 * The view uses a label provider to define how gridbean objects should be
 * presented in the view. Each view can present the same gridbean objects using
 * different labels and icons, if needed. Alternatively, a single label provider
 * can be shared between views in order to ensure that objects of the same type
 * are presented in the same way everywhere.
 * <p>
 */

public class JobOutcomeView extends ViewPart {

	public static String ID = "de.fzj.unicore.rcp.gpe4eclipse.views.JobOutcomeView";

	protected static final String PART_NAME_PREFIX = "Output for Job ";

	protected TabFolder tabFolder;
	protected GridBeanClient<IWorkbenchPart> gbClient;

	protected Control[] gridBeanPanels;

	/**
	 * The constructor.
	 */
	public JobOutcomeView() {
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {

		tabFolder = new TabFolder(parent, SWT.BOTTOM);
		createToolbar();
	}

	private void createToolbar() {

	}

	@Override
	public void dispose() {
		super.dispose();
		if (gridBeanPanels != null) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					for (Control frame : gridBeanPanels) {
						if (frame != null) {
							frame.dispose();
						}
					}
				}
			});
		}
		gbClient.setGridBeanOutputPanel(null);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
	}

	public void setGridBeanClient(GridBeanClient<IWorkbenchPart> gbClient) {

		try {
			setPartName(PART_NAME_PREFIX + gbClient.getGridBeanJob().getName());
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while setting view name: "+e.getMessage(), e);
		}
		this.gbClient = gbClient;
	}

	public void setJobName(String partName) {
		super.setPartName(PART_NAME_PREFIX + partName);
	}

	public void setPanels(final IPanel<?>[] panels) {

		if (gridBeanPanels != null) {
			// remove old panels first
			for (int i = 0; i < gridBeanPanels.length; i++) {
				tabFolder.getItem(0).dispose();
			}
		}
		gridBeanPanels = new Composite[panels.length];
		int i = 0;
		for (final IPanel<?> panel : panels) {
			TabItem tabItem = new TabItem(tabFolder, SWT.FILL, i);
			Control control = GridBeanUtils.addGridBeanPanel(tabFolder, panel);
			if (control != null) {
				gridBeanPanels[i++] = control;
				// control.setBackground(new
				// org.eclipse.swt.graphics.Color(PlatformUI.getWorkbench().getDisplay(),0,255,0));
				tabItem.setText(panel.getName());
				tabItem.setControl(control);
				// if(panel.getComponent() instanceof Control)
				// {
				// ((Control)panel.getComponent()).setBounds(control.getBounds());
				// }
			} else {
				tabItem.dispose();
			}
		}

	}

}