package de.fzj.unicore.rcp.gpe4eclipse.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.intel.gpe.clients.api.Application;

import de.fzj.unicore.rcp.common.utils.ToolBarUtils;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.actions.DownloadGridBeansAction;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.GridBeanRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.IGridBeanRegistryListener;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;

/**
 * Dialog for letting the user choose a Gridbean.
 * 
 * @author bdemuth
 * 
 */
public class SelectGridBeanDialog extends ElementListSelectionDialog implements
		IGridBeanRegistryListener {

	// toolbar manager used to add actions
	private ToolBarManager toolBarManager = new ToolBarManager(SWT.RIGHT);
	List<GridBeanInfo> gridbeans;
	private Application app = null;
	
	private DownloadGridBeansAction downloadGridBeansAction;

	/**
	 * Dialog for letting the user choose from all available Gridbeans.
	 * 
	 * @param parent
	 */
	public SelectGridBeanDialog(Shell parent) {
		super(parent, new LabelProvider() {

			@Override
			public String getText(Object element) {
				return element.toString();
			}
		});
		GridBeanRegistry registry = GPEActivator.getDefault()
				.getGridBeanRegistry();
		gridbeans = registry.getGridBeans();
		registry.addListener(this);
		setTitle("Select an application");
		setElements(gridbeans.toArray());
	}

	/**
	 * Dialog for letting the user choose a Gridbean that matches the given
	 * application.
	 * 
	 * @param parent
	 */
	public SelectGridBeanDialog(Shell parent, Application app) {
		super(parent, new LabelProvider() {

			@Override
			public String getText(Object element) {
				return element.toString();
			}
		});
		this.app = app;
		GridBeanRegistry registry = GPEActivator.getDefault()
				.getGridBeanRegistry();
		gridbeans = GridBeanUtils.getMatchingGridBeans(registry.getGridBeans(),
				app);
		setTitle("Select an application");
		setElements(gridbeans.toArray());

		registry.addListener(this);
	}

	@Override
	public boolean close() {
		GridBeanRegistry registry = GPEActivator.getDefault()
				.getGridBeanRegistry();
		registry.removeListener(this);
		if(downloadGridBeansAction != null)
		{
			downloadGridBeansAction.dispose();
		}
		return super.close();
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		int numColumns = 1;
		GridLayout layout = new GridLayout(numColumns, true);

		Composite toolBarContainer = new Composite(composite, SWT.NO_FOCUS);
		toolBarContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				false));
		toolBarContainer.setLayout(layout);
		downloadGridBeansAction = new DownloadGridBeansAction();
		toolBarManager.add(downloadGridBeansAction);
		ToolBar toolBar = toolBarManager.createControl(toolBarContainer);
		GridData data = new GridData(SWT.RIGHT, SWT.CENTER, true, false);
		toolBar.setLayoutData(data);
		ToolBarUtils.setToolItemText(toolBar);

		return composite;
	}

	public void gridBeansChanged(List<GridBeanInfo> oldValue,
			List<GridBeanInfo> newValue) {
		if (app == null) {
			gridbeans = new ArrayList<GridBeanInfo>(newValue);
		} else {
			gridbeans = GridBeanUtils.getMatchingGridBeans(newValue, app);
		}
		setElements(gridbeans.toArray());
		setListElements(gridbeans.toArray());
	}

}
