/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import javax.xml.namespace.QName;

/**
 * holds constats for this package
 * 
 * @author Christian Hohmann
 * 
 */
public interface RequirementConstants {

	public static String nameSpace = ResourceRequirement.qNameId;

	public static final String OS = "OS";
	public static final String CPU_ARCHITECTURE = "CPU Architecture";
	public static final String TOTAL_CPU = "Total number of CPUs";
	public static final String TOTAL_RESOURCES = "Number of nodes";
	public static final String CPU_PER_NODE = "CPUs per node";
	public static final String CPU_SPEED = "CPU speed";
	public static final String RAM_PER_NODE = "RAM per node";
	public static final String TIME_PER_CPU = "Wall time";
	public static final String DISK_SPACE_PER_NODE = "Disk space per node";
	public static final String EXCLUSIVE_EXECUTION = "Exclusive execution";
	public static final String XLOGIN = "Remote login";
	public static final String USER_EMAIL = "Notification email";
	public static final String EXECENV = "ExecEnv";
	public static final String JOB_SCHED = "Not before";
	public static final String RESERVATION_ID = "Reservation ID";

	public static final QName osRequirement = new QName(nameSpace, OS);
	public static final QName cpuArchitectureRequirement = new QName(nameSpace,
			CPU_ARCHITECTURE);
	public static final QName totalCpuRequirement = new QName(nameSpace,
			TOTAL_CPU);
	public static final QName totalResourcesRequirement = new QName(nameSpace,
			TOTAL_RESOURCES);
	public static final QName cpuPerNodeRequirement = new QName(nameSpace,
			CPU_PER_NODE);
	public static final QName cpuSpeedPerNodeRequirement = new QName(nameSpace,
			CPU_SPEED);
	public static final QName ramPerNodeRequirement = new QName(nameSpace,
			RAM_PER_NODE);
	public static final QName timePerCPURequirement = new QName(nameSpace,
			TIME_PER_CPU);
	public static final QName diskSpacePerNodeRequirement = new QName(
			nameSpace, DISK_SPACE_PER_NODE);
	public static final QName exclusiveExecutionRequirement = new QName(
			nameSpace, EXCLUSIVE_EXECUTION);
	public static final QName XLOGIN_REQUIREMENT = new QName(nameSpace, XLOGIN);
	public static final QName EMAIL_REQUIREMENT = new QName(nameSpace,
			USER_EMAIL);
	public static final QName executionEnvironmentRequirement = new QName(
			nameSpace, EXECENV);
	public static final QName jobSchedulingRequirement = new QName(nameSpace, JOB_SCHED);
	public static final QName RESERVATION_REQUIREMENT = new QName(nameSpace,
			RESERVATION_ID);

	public static final Double KILO = 1024d;
	public static final Double MEGA = KILO * 1024;
	public static final Double GIGA = MEGA * 1024;
	public static final Double TERA = GIGA * 1024;
	public static final Double HUNDRED_THOUSAND = 100000.0;
	public static final Double MILLION = 1000000d;
	public static final Double BILLION = 1000000000d;

	public static final String NO_UNIT = "";

	public static final String UNIT_HZ = "Hz";
	public static final String UNIT_KILO_HZ = "KHz";
	public static final String UNIT_MEGA_HZ = "MHz";
	public static final String UNIT_GIGA_HZ = "GHz";
	public static final String[] UNITS_FREQUENCY = new String[] { UNIT_HZ,
			UNIT_KILO_HZ, UNIT_MEGA_HZ, UNIT_GIGA_HZ, NO_UNIT };

	public static final String UNIT_BYTE = "Bytes";
	public static final String UNIT_KILO_BYTE = "KBytes";
	public static final String UNIT_MEGA_BYTE = "MBytes";
	public static final String UNIT_GIGA_BYTE = "GBytes";
	public static final String UNIT_TERA_BYTE = "TBytes";
	public static final String[] UNITS_DATA = new String[] { UNIT_BYTE,
			UNIT_KILO_BYTE, UNIT_MEGA_BYTE, UNIT_GIGA_BYTE, UNIT_TERA_BYTE,
			NO_UNIT };

	public static final String UNIT_MILLI_SECOND = "milliseconds";
	public static final String UNIT_SECOND = "seconds";
	public static final String UNIT_MINUTE = "minutes";
	public static final String UNIT_HOUR = "hours";
	public static final String UNIT_DAY = "days";
	public static final String[] UNITS_TIME = new String[] { UNIT_MILLI_SECOND,
			UNIT_SECOND, UNIT_MINUTE, UNIT_HOUR, UNIT_DAY, NO_UNIT };
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
	public static final String HUMAN_READABLE_DATE_FORMAT = "yyyy-MM-dd HH:mm";
	public static final String NO_ADD_INFORMATION = "none";

	/**
	 * Types of Requirement
	 */
	public static final String REQUIREMENT_TYPE_NAMESPACE = "de.fzj.unicore.rcp.gpe4eclipse.ns";
	public static final String REQUIREMENT_TYPE_BOOLEAN = "Boolean";
	public static final String REQUIREMENT_TYPE_ENUM = "Enum";
	public static final String REQUIREMENT_TYPE_RANGEVALUE = "RangeValue";
	public static final String REQUIREMENT_TYPE_STRING = "String";
	public static final String REQUIREMENT_TYPE_DOUBLE = "Double";
	public static final String REQUIREMENT_TYPE_INT = "Int";
	public static final String REQUIREMENT_TYPE_DATE = "Date";

}
