package de.fzj.unicore.rcp.gpe4eclipse.exceptions;

import java.io.File;

public class InvalidGridBeanException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8639285433474270778L;
	private File gridBeanFile;

	public InvalidGridBeanException() {
		super();
	}

	public InvalidGridBeanException(String message, File gridBeanFile) {
		super(message);
		this.gridBeanFile = gridBeanFile;
	}

	public InvalidGridBeanException(String message, Throwable cause,
			File gridBeanFile) {
		super(message, cause);
		this.gridBeanFile = gridBeanFile;
	}

	public InvalidGridBeanException(Throwable cause, File gridBeanFile) {
		super(cause);
		this.gridBeanFile = gridBeanFile;
	}

	public File getGridBeanFile() {
		return gridBeanFile;
	}

	public void setGridBeanFile(File gridBeanFile) {
		this.gridBeanFile = gridBeanFile;
	}

}
