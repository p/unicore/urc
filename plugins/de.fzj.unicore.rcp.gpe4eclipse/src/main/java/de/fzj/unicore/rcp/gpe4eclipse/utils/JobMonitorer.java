package de.fzj.unicore.rcp.gpe4eclipse.utils;

import com.intel.gpe.clients.api.JobClient;
import com.intel.gpe.clients.common.clientwrapper.ClientWrapper;

import de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditor;

/**
 * Class for watching a job's status in a background Thread. Updates an open
 * JobEditor and enables its fetch outcome action when job is finished.
 */
class JobMonitorer {

	ClientWrapper<JobClient, String> job;
	JobEditor jobEditor;

	JobMonitorer(JobEditor jobEditor) {
		this.job = jobEditor.getJobClient();
		this.jobEditor = jobEditor;
	}

	public JobEditor getJobEditor() {
		return jobEditor;
	}

	public com.intel.gpe.clients.api.Status pollStatus() throws Exception {
		return job.getClient().getStatus();
	}

}
