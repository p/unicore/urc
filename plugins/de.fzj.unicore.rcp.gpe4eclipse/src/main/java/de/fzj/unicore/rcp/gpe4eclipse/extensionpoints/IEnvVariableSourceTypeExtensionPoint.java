/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensionpoints;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.clients.api.transfers.IAddressOrValue;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.EnvVariable;

/**
 * Through this extension point new types of data stages (stage ins or stage
 * outs) can be added by eclipse plugins. Examples of stage types are stage ins
 * from local files, files on remote UNICORE storages, files that are defined
 * within a workflow etc.
 * 
 * @author demuth
 * 
 */
public interface IEnvVariableSourceTypeExtensionPoint {

	public static final String ID = GPE4EclipseConstants.EXTENSION_POINT_ENV_VARIABLE_SOURCE_TYPE;

	/**
	 * Sets the GridBean parameter to an initial value. This method will be
	 * called when the user selects the extension for setting a variable value.
	 * 
	 * @param adaptable
	 *            if the GridBean is part of a workflow, this parameter must be
	 *            set, otherwise it might be null.
	 * @param oldValue
	 */
	public void attachToParameterValue(IAdaptable adaptable,
			EnvVariable oldValue);

	/**
	 * Check whether variable sources of this type should be available for the
	 * given GridBean by looking at its context. E.g. stage ins from the
	 * Chemomentum data management services should not be available for a
	 * GridBean job that will be directly submitted to a target system in a
	 * non-Chemomentum Grid.
	 * 
	 * @param context
	 * @return
	 */
	public boolean availableInContext(GridBeanContext context);

	/**
	 * This method is used to provide a way to fix old address formats that have
	 * changed over time. The extension might have a look at the given address
	 * and modify it if it doesn't conform to the current address format (trying
	 * not to loose any information in so doing). Then the modified address must
	 * be returned.
	 * 
	 * @param adaptable
	 * @param value
	 */
	public IAddressOrValue checkAndFixAddress(IAddressOrValue address,
			String oldVersion, String currentVersion);

	/**
	 * Gets called when a different extension is selected for a parameter value.
	 * May be used to perform some cleanup.
	 * 
	 * @param adaptable
	 * @param value
	 */
	public void detachFromParameterValue(IAdaptable adaptable, EnvVariable value);

	/**
	 * Get the cell editor that is used for changing the value.
	 * 
	 * @param parent
	 * @return
	 */
	public CellEditor getCellEditor(Composite parent, EnvVariable oldValue,
			IAdaptable adaptable);

	/**
	 * The qualified name identifying the type of variable source. The local
	 * part of this will be displayed to the user and should be human readable
	 * (e.g. "Local file")
	 * 
	 * @return
	 */
	public QName getType();

	/**
	 * Whenever the parameter value is changed this gets called in order to
	 * allow the extension to keep track of changes.
	 * 
	 * @param adaptable
	 * @param newValue
	 * @return
	 */
	public void updateParameterValue(IAdaptable adaptable,
			EnvVariable oldValue, EnvVariable newValue);

}
