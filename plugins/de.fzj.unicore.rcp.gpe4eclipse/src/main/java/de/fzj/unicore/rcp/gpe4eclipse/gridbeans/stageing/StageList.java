/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileSetParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;

/**
 * @author demuth
 * 
 */
public abstract class StageList implements Serializable, PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3400988809206158405L;

	/**
	 * views that are interested in gridbean changes
	 */
	private transient Set<IStageListObserver> changeListeners;

	private Map<IGridBeanParameter, IFileSetParameterValue> fileSets = new HashMap<IGridBeanParameter, IFileSetParameterValue>();
	private IGridBeanParameter defaultFileset = null;

	protected Map<String, IGridBeanParameter> params = new HashMap<String, IGridBeanParameter>();

	// used as the underlying model with which this model object
	// communicates
	protected IGridBean gridBean;

	protected IAdaptable adaptable;

	private Map<String, List<IStage>> stages = new ConcurrentHashMap<String, List<IStage>>();
	private List<IStage> stageList = createEmptyStageList();

	public StageList(IGridBean gridBean, IAdaptable adaptable) {
		this.adaptable = adaptable;
		this.gridBean = gridBean;
		getGridBean().addPropertyChangeListener(this);
	}

	public void addChangeListener(IStageListObserver viewer) {
		getChangeListeners().add(viewer);
	}

	public void addParameter(IGridBeanParameter param,
			IGridBeanParameterValue value) {
		if (value == null) {
			return; // do NOT add parameters with null values!
		}
		synchronized (stages) {

			// store parameter for being able to access it in case of a property
			// change event
			params.put(param.getName().toString(), param);

			if (GridBeanParameterType.FILE_SET.equals(param.getType())) {
				IFileSetParameterValue fileset = (IFileSetParameterValue) value;
				
				if (defaultFileset == null) {
					defaultFileset = param;
				}
				getFileSets().put(param, fileset);
			}
			updateStagesForParameter(param, value);

		}
	}

	/**
	 * Add an empty stageing element to the list
	 * 
	 * @param stage
	 */
	public IStage addStage() {
		IStage stage = createStage();
		stage.setDefinedInGridBean(false);
		return addStage(stage);

	}

	/**
	 * Add a stageing element to the list
	 * 
	 * @param stage
	 */
	public IStage addStage(IStage stage) {
		synchronized (stages) {

			IGridBeanParameter param = stage.getGridBeanParameter();
			IFileSetParameterValue fsValue;
			if (param == null) {
				// add the stage to the first one of the file sets
				// defined by the GridBean
				param = defaultFileset;
				fsValue = getFileSets().get(param);

				try {
					fsValue = (IFileSetParameterValue) fsValue.clone();
					getFileSets().put(param, fsValue);
				} catch (Exception e) {
					GPEActivator.log(Status.ERROR, "Error while cloning file set: "+e.getMessage(), e);
				}

				fsValue.addFile(stage.getValue());
				stage.setGridBeanParameter(param);

			} else {
				fsValue = getFileSets().get(stage.getGridBeanParameter());
				IStageTypeExtensionPoint ext = null;
				if (param.isInputParameter()) {
					ext = GPEActivator.getDefault().getStageInTypeRegistry()
							.getDefiningExtension(stage.getType());
				} else {
					ext = GPEActivator.getDefault().getStageOutTypeRegistry()
							.getDefiningExtension(stage.getType());
				}
				if (ext != null) {
					ext.attachToParameterValue(getAdaptable(), stage.getValue());
				}

				if (!fsValue.getFiles().contains(stage.getValue())) {
					fsValue.addFile(stage.getValue());
				}
			}
			IFileParameterValue value = stage.getValue();

			updateActivity(stage, value);

			// add stage to GridBeanModel
			getGridBean().removePropertyChangeListener(this);
			getGridBean().set(param.getName(), fsValue);
			getGridBean().addPropertyChangeListener(this);

			List<IStage> l = stages.get(param.getName().toString());
			if (l == null) {
				l = createEmptyStageList();
			}
			l.add(stage);
			stages.put(param.getName().toString(), l);
			updateStageList();
		}
		informObserversOfAdd(stage);
		return stage;
	}

	private List<IStage> createEmptyStageList() {
		List<IStage> arr = new ArrayList<IStage>();
		return Collections.synchronizedList(arr);
	}

	public abstract IStage createStage();

	public abstract IStage createStage(IGridBeanParameter param,
			IFileParameterValue file);

	/**
	 * perform some cleanup
	 */
	public void dispose() {
		getGridBean().removePropertyChangeListener(this);
	}

	protected IActivity getActivity() {
		if (getAdaptable() == null) {
			return null;
		}
		Object adapter = getAdaptable().getAdapter(IActivity.class);
		return (IActivity) adapter;
	}

	public IAdaptable getAdaptable() {
		return adaptable;
	}

	protected Set<IStageListObserver> getChangeListeners() {
		if (changeListeners == null) {
			changeListeners = new HashSet<IStageListObserver>();
		}
		return changeListeners;
	}

	public Map<IGridBeanParameter, IFileSetParameterValue> getFileSets() {
		return fileSets;
	}

	public IGridBean getGridBean() {
		return gridBean;
	}

	public GridBeanContext getGridBeanContext() {
		IGridBean gridBean = getGridBean();
		if (gridBean == null) {
			return null;
		}
		return (GridBeanContext) gridBean.get(GPE4EclipseConstants.CONTEXT);
	}

	public IStage[] getStages() {
		synchronized (stages) {
			IStage[] siteArr = stageList.toArray(new IStage[0]);
			return siteArr;
		}
	}

	public List<IStage> getStages(String id) {
		synchronized (stages) {
			List<IStage> l = stages.get(id);
			List<IStage> result = createEmptyStageList();
			if (l != null) {
				result.addAll(l);
			}
			return result;
		}
	}

	protected void informObserversOfAdd(IStage stage) {
		synchronized (stages) {
			Iterator<IStageListObserver> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next().addStage(stage);
			}
		}
	}

	protected void informObserversOfRemove(IStage stage) {
		synchronized (stages) {
			Iterator<IStageListObserver> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next().removeStage(stage);
			}
		}
	}

	public void informObserversOfUpdate(IStage stage) {
		synchronized (stages) {
			Iterator<IStageListObserver> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next().updateStage(stage);
			}
		}
	}

	public abstract boolean isInputStageList();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	public void propertyChange(final PropertyChangeEvent evt) {

		QName name = QName.valueOf(evt.getPropertyName());
		final IGridBeanParameter oldParam = params.get(evt.getPropertyName());
		final IGridBeanParameter newParam = isInputStageList() ? getGridBean()
				.findInputParameter(name) : getGridBean().findOutputParameter(
				name);

		if (oldParam == null) {
			if (newParam == null) {
				// ignore
			} else {

				if (isInputStageList() == newParam.isInputParameter()
						&& (GridBeanParameterType.FILE.equals(newParam
								.getType()) || GridBeanParameterType.FILE_SET
								.equals(newParam.getType()))) {
					// found a new file(set) input parameter that has not been
					// there before!
					addParameter(newParam,
							(IGridBeanParameterValue) evt.getNewValue());
				}
			}
		} else {
			// we had this parameter
			if (newParam == null) {
				// parameter has been removed
				removeParameter(oldParam);

			} else {
				if (oldParam.equals(newParam)) {
					// parameter type is the same as before, "only" need to
					// update parameter
					updateStagesForParameter(oldParam,
							(IGridBeanParameterValue) evt.getNewValue());
				} else {
					// parameter type has changed => remove and re-add
					removeParameter(oldParam);
					if (isInputStageList() == newParam.isInputParameter()
							&& (GridBeanParameterType.FILE.equals(newParam
									.getType()) || GridBeanParameterType.FILE_SET
									.equals(newParam.getType()))) {
						addParameter(newParam,
								(IGridBeanParameterValue) evt.getNewValue());
					}
				}
			}
		}

	}

	public void removeChangeListener(IStageListObserver viewer) {
		getChangeListeners().remove(viewer);
	}

	public void removeParameter(IGridBeanParameter param) {
		synchronized (stages) {

			Set<String> removed = new HashSet<String>();
			// remove parameter from map
			params.remove(param.getName().toString());
			if (param.isInputParameter()) {
				removed.add(param.getName().toString());
			}
			// remove it from fileSets
			if (GridBeanParameterType.FILE_SET.equals(param.getType())) {
				fileSets.remove(param);
				if (defaultFileset == param && fileSets.size() > 0) {
					defaultFileset = fileSets.keySet().iterator().next();
				}
			}

			// remove all associated stages

			List<IStage> toBeRemoved = getStages(param.getName().toString());
			for (IStage rem : toBeRemoved) {
				removeStageFromMyData(rem);
				if (!param.isInputParameter()) {
					removed.add(rem.getId());
				}
			}

			// now update the activity
			removeStagesFromActivity(removed);
		}
	}

	/**
	 * Remove a stage from the stage list
	 * 
	 * @param stage
	 */
	public void removeStage(IStage stage) {
		synchronized (stages) {
			IGridBeanParameter param = stage.getGridBeanParameter();
			IStageTypeExtensionPoint ext = null;
			if (stage.getGridBeanParameter().isInputParameter()) {
				ext = GPEActivator.getDefault().getStageInTypeRegistry()
						.getDefiningExtension(stage.getType());
			} else {
				ext = GPEActivator.getDefault().getStageOutTypeRegistry()
						.getDefiningExtension(stage.getType());
			}
			if (ext != null) {
				ext.detachFromParameterValue(getAdaptable(), stage.getValue());
			}

			updateActivity(stage, null);

			// first: remove Stage from GridBean
			IFileSetParameterValue fileset = getFileSets().get(param);
			try {
				fileset = (IFileSetParameterValue) fileset.clone();
				getFileSets().put(param, fileset);
			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, "Error while cloning file set: "+e.getMessage(), e);
			}

			fileset.removeFile(stage.getValue());
			getGridBean().removePropertyChangeListener(this);
			getGridBean().set(param.getName(), fileset);
			getGridBean().addPropertyChangeListener(this);
		}

		removeStageFromMyData(stage);
	}

	private void removeStageFromMyData(IStage stage) {
		synchronized (stages) {
			IGridBeanParameter param = stage.getGridBeanParameter();
			List<IStage> l = stages.get(param.getName().toString());
			l.remove(stage);
			if (l.size() == 0) {
				stages.remove(param.getName().toString());
			} else {
				stages.put(param.getName().toString(), l);
			}
			updateStageList();
			informObserversOfRemove(stage);
		}
	}

	protected abstract void removeStagesFromActivity(Set<String> stageIDs);

	/**
	 * Used to store the associated GridBeanParameter value in the GridBeanModel
	 * after the Stage has been altered by the user
	 * 
	 * @param stage
	 * @param newValue
	 */
	public void reportStageValueChange(IStage stage,
			IGridBeanParameter newParam, IFileParameterValue newValue) {
		if (CollectionUtil.equalOrBothNull(stage.getGridBeanParameter(),
				newParam)
				&& CollectionUtil.equalOrBothNull(stage.getValue(), newValue)) {
			return;
		}
		getGridBean().removePropertyChangeListener(this);

		if (!newValue.getSource().equals(stage.getValue().getSource())) {
			newValue = updateTarget(stage.getValue(), newValue);
		}
		updateActivity(stage, newValue);

		if (GridBeanParameterType.FILE.equals(stage.getGridBeanParameter()
				.getType())) {
			stage.setValue(newValue);
			getGridBean().set(stage.getGridBeanParameter().getName(), newValue);
		} else if (GridBeanParameterType.FILE_SET.equals(stage
				.getGridBeanParameter().getType())) {

			IFileSetParameterValue newParent = getFileSets().get(newParam);
			try {
				newParent = (IFileSetParameterValue) newParent.clone();
				getFileSets().put(newParam, newParent);
			} catch (Exception e1) {
				e1.printStackTrace();
			}

			boolean parentChanged = !newParam.equals(stage
					.getGridBeanParameter());
			if (parentChanged) {
				IFileSetParameterValue oldParent = getFileSets().get(
						stage.getGridBeanParameter());
				try {
					oldParent = (IFileSetParameterValue) oldParent.clone();
					getFileSets().put(stage.getGridBeanParameter(), newParent);
				} catch (Exception e) {
					GPEActivator.log(Status.ERROR, "Error while cloning file set: "+e.getMessage(), e);
				}

				List<IStage> l = stages.get(stage.getGridBeanParameter()
						.getName().toString());
				l.remove(stage);
				if (l.size() == 0) {
					stages.remove(stage.getGridBeanParameter().getName()
							.toString());
				} else {
					stages.put(stage.getGridBeanParameter().getName()
							.toString(), l);
				}

				oldParent.removeFile(stage.getValue());

				l = stages.get(newParam.getName().toString());
				if (l == null) {
					l = createEmptyStageList();
				}
				l.add(stage);
				stages.put(newParam.getName().toString(), l);

				stage.setValue(newValue);
				newParent.addFile(newValue);

				getGridBean().set(stage.getGridBeanParameter().getName(),
						oldParent);

				stage.setGridBeanParameter(newParam);
				getGridBean().set(newParam.getName(), newParent);
				updateStageList();

			} else if (!stage.getValue().equals(newValue)) {

				List<IFileParameterValue> files = newParent.getFiles();
				int i = files.indexOf(stage.getValue());
				newParent.removeFile(stage.getValue());
				newParent.addFile(i, newValue);
				stage.setValue(newValue);
				getGridBean().set(newParam.getName(), newParent);
				updateStageList();
			}

		}

		getGridBean().addPropertyChangeListener(this);
		informObserversOfUpdate(stage);
	}

	public void setFileSets(
			Map<IGridBeanParameter, IFileSetParameterValue> fileSets) {
		this.fileSets = fileSets;
	}

	/**
	 * update the activity's list of data sinks or data sources
	 * 
	 * @param stage
	 *            the stage which has changed - still with its old value!
	 * @param newValue
	 *            the new value for the stage, null if the stage has been
	 *            deleted
	 */
	protected abstract void updateActivity(IStage stage,
			IFileParameterValue newValue);

	protected void updateStageForFile(IGridBeanParameter param,
			IFileParameterValue newValue) {

		synchronized (stages) {
			List<IStage> changedStages = getStages(param.getName().toString());
			int size = changedStages.size();
			if (size == 0) {
				List<IStage> result = createEmptyStageList();
				IStage stage = createStage(param, newValue);
				result.add(stage);
				informObserversOfAdd(stage);
				stages.put(param.getName().toString(), result);
				updateActivity(stage, newValue);
				updateStageList();
				informObserversOfAdd(stage);
			} else if (size == 1) {
				IStage stage = changedStages.get(0);
				updateActivity(stage, newValue);
				stage.setValue(newValue);
				informObserversOfUpdate(stage);
			} else {
				GPEActivator
						.log(IStatus.ERROR,
								"Stored multiple stages for a single file parameter value!");
			}
		}
	}

	private void updateStageList() {
		synchronized (stages) {

			stageList.clear();
			Collection<List<IStage>> allStages = stages.values();
			for (List<IStage> list : allStages) {
				stageList.addAll(list);
			}
		}
	}

	protected void updateStagesForFileSet(IGridBeanParameter param,
			IFileSetParameterValue fileset) {
		synchronized (stages) {
			// file set might have changed, so
			// update list of filesets!
			getFileSets().put(param, fileset);

			List<IStage> result = createEmptyStageList();
			List<IStage> removed = createEmptyStageList();
			List<IStage> added = createEmptyStageList();
			List<IStage> updated = createEmptyStageList();

			Map<String, IFileParameterValue> map = new HashMap<String, IFileParameterValue>();
			List<IFileParameterValue> files = fileset.getFiles();
			for (IFileParameterValue file : files) {
				map.put(file.getUniqueId(), file);
			}

			List<IStage> oldStages = getStages(param.getName().toString());
			for (IStage stage : oldStages) {
				IFileParameterValue file = map.remove(stage.getId());
				if (file == null) {
					// file has been removed from fileset
					updateActivity(stage, null);
					removed.add(stage);
				} else {
					// file might have been changed
					updateActivity(stage, file);
					stage.setValue(file);
					updated.add(stage);
					result.add(stage);
				}
			}

			// the remaining files are new and have to be added
			for (IFileParameterValue file : map.values()) {
				IStage stage = createStage(param, file);
				stage.setDefinedInGridBean(false);
				updateActivity(stage, file);
				added.add(stage);
				result.add(stage);
			}

			stages.put(param.getName().toString(), result);
			updateStageList();

			// inform observers
			for (IStage stage : removed) {
				informObserversOfRemove(stage);
			}
			for (IStage stage : updated) {
				informObserversOfUpdate(stage);
			}
			for (IStage stage : added) {
				informObserversOfAdd(stage);
			}
		}
	}

	protected void updateStagesForParameter(IGridBeanParameter oldParam,
			IGridBeanParameterValue value) {

		IGridBeanParameter currentParam = oldParam.isInputParameter() ? getGridBean()
				.findInputParameter(oldParam.getName()) : getGridBean()
				.findOutputParameter(oldParam.getName());
		if (currentParam == null) {
			removeParameter(oldParam);
		} else {
			if (!currentParam.equals(oldParam)) {
				removeParameter(oldParam);
				addParameter(currentParam, value);
			} else {
				if (value == null) {
					removeParameter(oldParam); // when someone sets the value to
												// null, we don't like the
												// parameter anymore
				} else if (currentParam != null
						&& GridBeanParameterType.FILE.equals(currentParam
								.getType())) {
					IFileParameterValue file = (IFileParameterValue) value;
					updateStageForFile(oldParam, file);
				} else if (currentParam != null
						&& GridBeanParameterType.FILE_SET.equals(currentParam
								.getType())) {
					IFileSetParameterValue fileset = (IFileSetParameterValue) value;
					updateStagesForFileSet(currentParam, fileset);
				}
			}
		}
	}

	/**
	 * When the source of a stage changes, this might influence the target as
	 * well, e.g. when the new source uses wildcards, the target must use
	 * wildcards as well
	 * 
	 * @param newValue
	 */
	private IFileParameterValue updateTarget(IFileParameterValue oldValue,
			IFileParameterValue newValue) {
		IStageTypeExtensionPoint ext = null;
		if (newValue != null) {
			ext = GPEActivator
					.getDefault()
					.getStageOutTypeRegistry()
					.getDefiningExtension(
							newValue.getTarget().getProtocol().getName());
		}
		if (ext != null) {
			ext.updateParameterValue(getAdaptable(), oldValue, newValue);
		}
		return newValue;
	}

}
