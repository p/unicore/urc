package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.unigrids.services.atomic.types.AvailableResourceType;
import org.unigrids.services.atomic.types.AvailableResourceTypeType;
import org.unigrids.services.atomic.types.AvailableResourceTypeType.Enum;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GenericRegistry;


/**
 * Resources registry caches info about site specific resources available for
 * all sites available in the grid.
 * @author K. Benedyczak
 */
public class ResourcesRegistry extends GenericRegistry<AvailableResource> {
	
	private static ResourcesRegistry instance = new ResourcesRegistry();


	/**
	 * @return the ResourcesRegistry instance
	 */
	public static ResourcesRegistry getDefaultInstance() {
		return instance;
	}

	public static void setDefaultInstance(ResourcesRegistry reg) {
		instance = reg;
	}

	@Override
	protected boolean isValid(AvailableResource resource) {
		return resource.getName() != null && !resource.getName().trim().equals("");
	}
	
	/**
	 * 
	 * @return a union of SSRs of all sites. Resources with the same name and type are
	 * merged into one entry. The permitted values are merged to cover all possible choices.
	 * The default value and description is set from the random of the available resources.
	 */
	public synchronized Collection<AvailableResource> getUnifiedResources() {
		return unifyResources(getEntryList());
	}

	public static Collection<AvailableResource> unifyResources(List<AvailableResource> all) {
		Map<String, AvailableResource> union = new TreeMap<String, AvailableResource>(
				new Comparator<String>() {
					@Override
					public int compare(String o1, String o2) {
						String o1Bis = o1.split("::")[1];
						String o2Bis = o2.split("::")[1];
						return o1Bis.compareTo(o2Bis);
					}
		});
		for (AvailableResource r: all) {
			String key = r.getType() + "::" + r.getName();
			AvailableResource existing = union.get(key);
			if (existing == null)
				union.put(key, (AvailableResource) r.clone());
			else {
				try {
					combineValues(existing, r);
				} catch (NumberFormatException nfe) {
					GPEActivator.log("Min/max number set for the site-specific resource is invalid", nfe);
				}
			}
		}
		return union.values();
	}
	
	private static void combineValues(AvailableResource existing, AvailableResource added) {
		if (added.getDescription() != null && (existing.getDescription() == null || 
				existing.getDescription().trim().equals("")))
			existing.setDescription(added.getDescription());
		if (added.getDefault() != null && existing.getDefault() == null)
			existing.setDefault(added.getDefault());

		Enum type = existing.getType();
		if (type == AvailableResourceTypeType.CHOICE) {
			String[] addedV = added.getAllowedValueArray();
			if (addedV == null)
				return;

			String[] existingV = existing.getAllowedValueArray();
			if (existingV == null) {
				existing.setAllowedValueArray(addedV);
				return;
			}
			Set<String> set = new HashSet<String>();
			Collections.addAll(set, existingV);
			for (String a: addedV)
				set.add(a);
			existing.setAllowedValueArray(set.toArray(new String[set.size()]));
		} else if (type == AvailableResourceTypeType.DOUBLE) {
			double newMin = Double.parseDouble(added.getMin());
			double existingMin = Double.parseDouble(existing.getMin());
			if (newMin < existingMin)
				existing.setMin(added.getMin());
			double newMax = Double.parseDouble(added.getMax());
			double existingMax = Double.parseDouble(existing.getMax());
			if (newMax > existingMax)
				existing.setMax(added.getMax());
			
		} else if (type == AvailableResourceTypeType.INT) {
			int newMin = Integer.parseInt(added.getMin());
			int existingMin = Integer.parseInt(existing.getMin());
			if (newMin < existingMin)
				existing.setMin(added.getMin());
			int newMax = Integer.parseInt(added.getMax());
			int existingMax = Integer.parseInt(existing.getMax());
			if (newMax > existingMax)
				existing.setMax(added.getMax());
		}
	}
	
	public static List<AvailableResource> toPOJO(List<AvailableResourceType> resources)
	{
		if(resources == null) return null;
		List<AvailableResource> result = new ArrayList<AvailableResource>();
		for(AvailableResourceType res : resources)
		{
			result.add(toPOJO(res));
		}
		return result;
	}
	
	public static AvailableResource[] toPOJO(AvailableResourceType[] resources)
	{
		if(resources == null) return null;
		AvailableResource[] result = new AvailableResource[resources.length];
		int i = 0;
		for(AvailableResourceType res : resources)
		{
			result[i++] = toPOJO(res);
		}
		return result;
	}
	
	public static AvailableResource toPOJO(AvailableResourceType resource)
	{
		if(resource == null) return null;
		return new AvailableResource(resource.getName(), resource.getType(), 
				resource.getAllowedValueArray(), resource.getDescription(), 
				resource.getMin(), resource.getMax(), resource.getDefault());
	}
}
