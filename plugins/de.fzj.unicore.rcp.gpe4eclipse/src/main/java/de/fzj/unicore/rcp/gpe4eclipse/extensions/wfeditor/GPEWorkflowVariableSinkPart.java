package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import java.util.Iterator;

import org.eclipse.draw2d.geometry.Dimension;

import de.fzj.unicore.rcp.wfeditor.figures.DataSinkFigure;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.parts.DataSinkPart;

public class GPEWorkflowVariableSinkPart extends DataSinkPart {

	@Override
	public GPEWorkflowVariableSink getModel() {
		return (GPEWorkflowVariableSink) super.getModel();
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		if (!(getFigure() instanceof DataSinkFigure)) {
			return;
		}
		DataSinkFigure figure = (DataSinkFigure) getFigure();

		Iterator<String> it = getModel().getIncomingDataTypes().iterator();
		String dataType = it.hasNext() ? it.next()
				: WorkflowVariable.VARIABLE_TYPE_STRING;

		String symbol = WorkflowVariable.getSymbolFromTypeName(dataType);
		figure.setDataTypeSymbol(symbol);
		figure.setToolTipText(new String[] {
				"Variable: " + getModel().getName(), "Type: " + dataType });
		Dimension d = figure.calculatePreferredSize();
		figure.setPreferredSize(d.width, d.height);

	}

}
