package de.fzj.unicore.rcp.gpe4eclipse.clientstubs;

import com.intel.gpe.clients.api.TargetSystemClient;


/**
 * Interface for identifying client stubs representing remote resources that
 * might change. Listeners can be added in order to be notified of state or
 * property changes. {@link TargetSystemClient} implementations should implement
 * this as well, because their properties (list of applications, hardware
 * resources, etc.) might change.
 * Implementations MUST use weak references to refer to the listener so 
 * {@link #removeChangeListener(IClientChangeListener)} does not have to be
 * called.
 * @author bdemuth
 *
 */
public interface IMutableClient {

	public void addChangeListener(IClientChangeListener changeListener);
	
	public void removeChangeListener(IClientChangeListener changeListener);
}
