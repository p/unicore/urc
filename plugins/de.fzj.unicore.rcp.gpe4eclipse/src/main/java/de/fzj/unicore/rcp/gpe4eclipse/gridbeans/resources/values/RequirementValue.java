/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values;

import org.eclipse.core.runtime.Status;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.IRequirementValidator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.IRequirementValue;

/**
 * @author Christian Hohmann
 * 
 */
public abstract class RequirementValue implements IRequirementValue {

	protected IRequirementValidator validator;
	private String internalUnit = "";
	private String selectedUnit = "";
	private String[] selectableUnits;

	@Override
	public Object clone() {
		try {
			RequirementValue result = (RequirementValue) super.clone();
			return result;
		} catch (CloneNotSupportedException e) {
			GPEActivator.log(Status.ERROR, "Error while cloning job requirement: "+e.getMessage(), e);
			return null;
		}

	}

	public String getInternalUnit() {
		return internalUnit;
	}

	public String[] getSelectableUnits() {
		return selectableUnits;
	}

	public String getSelectedUnit() {
		return selectedUnit;
	}

	public IRequirementValidator getValidator() {
		return validator;
	}

	public String isValid() {
		return validator == null ? null : validator.isValid(this);
	}

	public void setInternalUnit(String internalUnit) {
		this.internalUnit = internalUnit;
	}

	public void setSelectableUnits(String[] selectableUnits) {
		this.selectableUnits = selectableUnits;
	}

	public void setSelectedUnit(String selectedUnit) {
		this.selectedUnit = selectedUnit;
	}

	public void setValidator(IRequirementValidator validator) {
		this.validator = validator;
	}

}