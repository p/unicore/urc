package de.fzj.unicore.rcp.gpe4eclipse.utils;

import java.io.File;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import com.intel.gpe.clients.api.JobClient;
import com.intel.gpe.clients.common.clientwrapper.ClientWrapper;

import de.fzj.unicore.rcp.gpe4eclipse.commands.FetchOutcomeCommand;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditor;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

public class JobEditorOutcomeFetcher {

	public static void fetchOutcome(final JobEditor jobEditor,
			final ClientWrapper<JobClient, String> job) {
		Job j = new BackgroundJob("") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				FetchOutcomeCommand cmd = new FetchOutcomeCommand(
						job.getClient());
				File f = jobEditor.getInputFile();
				if(f == null) return Status.CANCEL_STATUS;
				String workingDirectoryName = f.getParent();
				IPath path = Path.fromOSString(workingDirectoryName);
				String downloadDir = GPEPathUtils.outputFileDirForPath(path)
						.toOSString() + File.separator;
				cmd.setDownloadDir(downloadDir);
				cmd.execute();
				return Status.OK_STATUS;
			}

		};
		j.schedule();
	}
}
