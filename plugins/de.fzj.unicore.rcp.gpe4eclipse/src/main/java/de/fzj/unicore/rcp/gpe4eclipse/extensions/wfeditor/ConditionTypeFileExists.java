/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.IPropertySourceWithListeners;
import de.fzj.unicore.rcp.wfeditor.model.StateConstants;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

/**
 * @author demuth
 * 
 */
@XStreamAlias("ConditionTypeFileExists")
public class ConditionTypeFileExists implements IConditionType,
		PropertyChangeListener {

	public static final String TYPE_NAME = "Output file";

	private IActivity activity;
	private transient IActivity disposedActivity;
	private String filePath = "";
	private Condition condition;

	private boolean disposed = false;

	public ConditionTypeFileExists() {

	}

	@Override
	public ConditionTypeFileExists clone() throws CloneNotSupportedException {
		ConditionTypeFileExists clone = (ConditionTypeFileExists) super.clone();

		return clone;
	}

	public void dispose() {
		disposed = true;
		// perform some cleanup
		// must be called when this is not needed anymore!
		if (activity != null) {
			activity.removePersistentPropertyChangeListener(this);
		}
		if (disposedActivity != null) {
			disposedActivity.removePersistentPropertyChangeListener(this);
		}

	}

	public void finalDispose(IProgressMonitor progress) {
		// do nothing
	}

	public IActivity getActivity() {
		return activity;
	}

	

	public Condition getCondition() {
		return condition;
	}

	public IConditionType getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		ConditionTypeFileExists result;
		try {
			result = (ConditionTypeFileExists) super.clone();
		} catch (CloneNotSupportedException e) {
			WFActivator
					.log(IStatus.ERROR,
							"Unable to copy condition of type 'exit status comparison'",
							e);
			return null;
		}
		Condition newCondition = CloneUtils.getFromMapOrCopy(condition,
				toBeCopied, mapOfCopies, copyFlags);
		result.setCondition(newCondition);
		result.activity = CloneUtils.getFromMapOrCopy(activity, toBeCopied,
				mapOfCopies, copyFlags);
		result.disposedActivity = null;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType#
	 * getDisplayedExpression()
	 */
	public String getDisplayedExpression() {
		
		String activityName = "?";
		if (getActivity() != null) {
			activityName = getActivity().getName();
		}
		return "File "+filePath +"\nwas created by "+activityName;
	}

	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType#getExtensionId
	 * ()
	 */
	public String getExtensionId() {
		return ConditionTypeFileExistsExtension.ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType#
	 * getInternalExpression()
	 */
	public String getInternalExpression() throws Exception {
		if (getActivity() == null) {
			throw new Exception("Invalid condition " + getDisplayedExpression()
					+ ". Please set an activity for output file check.");
		}
		
		return " fileExists("+ "\"" + getActivity().getName() + "\",\"" + filePath + "\")";
	}

	public String getTypeName() {
		return TYPE_NAME;
	}

	public Set<Class<?>> insertAfter() {
		return new HashSet<Class<?>>(); // we don't care
	}

	public Set<Class<?>> insertBefore() {
		return new HashSet<Class<?>>(); // we don't care
	}

	public void insertCopyIntoDiagram(ICopyable copy,
			StructuredActivity parent, Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		ConditionTypeFileExists result = (ConditionTypeFileExists) copy;
		if (result.activity != null) {
			result.activity.addPersistentPropertyChangeListener(this);
		}

	}

	public boolean isDisposable() {
		return true;
	}

	public boolean isDisposed() {
		return disposed;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) {

		if (IFlowElement.PROP_NAME.equals(evt.getPropertyName())) {
			updateCondition();
		} else if (IPropertySourceWithListeners.PROP_STATE.equals(evt
				.getPropertyName())) {
			if (activity == null && disposedActivity == null) {
				return;
			}
			int state = activity != null ? activity.getState()
					: disposedActivity.getState();
			Integer oldState = (Integer) evt.getOldValue();
			if (oldState == null) {
				oldState = StateConstants.STATE_CREATED;
			}
			if (StateConstants.STATE_DISPOSED == state) {
				// target activity has been deleted
				disposedActivity = activity;
				activity = null;
				updateCondition();
			} else if (StateConstants.STATE_ACTIVE == state
					&& oldState.intValue() == StateConstants.STATE_DISPOSED
					&& disposedActivity != null) {
				// deletion has been undone
				activity = disposedActivity;
				disposedActivity = null;
				updateCondition();
			}

		}

	}

	private Object readResolve() {
		return this;
	}

	public void setActivity(IActivity activity) {
		if (this.activity != null) {
			this.activity.removePersistentPropertyChangeListener(this);
		}
		this.activity = activity;
		if (disposedActivity != null) {
			disposedActivity.removePersistentPropertyChangeListener(this);
		}
		disposedActivity = null;
		if (this.activity != null) {
			this.activity.addPersistentPropertyChangeListener(this);
		}
	}

	
	public void setCondition(Condition condition) {
		this.condition = condition;
	}



	@Override
	public String toString() {
		return getDisplayedExpression();
	}

	public void undoDispose() {
		disposed = false;
		if (activity != null) {
			activity.addPersistentPropertyChangeListener(this);
		}
		if (disposedActivity != null) {
			disposedActivity.addPersistentPropertyChangeListener(this);
		}

	}

	private void updateCondition() {
		Condition condition = getCondition();
		IPropertySourceWithListeners el = condition.getConditionalElement();
		el.setPropertyValue(Condition.PROP_CONDITION, condition);
	}

	private Object writeReplace() {
		return this;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}
