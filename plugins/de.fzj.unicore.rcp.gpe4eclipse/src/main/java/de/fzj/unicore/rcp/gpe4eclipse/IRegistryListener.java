package de.fzj.unicore.rcp.gpe4eclipse;

import java.net.URI;
import java.util.List;

public interface IRegistryListener<T> {
	
	public void entryListChanged(List<T> newEntries);
	
	public void entriesChangedForURI(URI uri, List<T> newEntries);

}
