/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.properties.PropertyDescriptor;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.intel.gpe.clients.all.gridbeans.GridBeanJob;
import com.intel.gpe.clients.all.gridbeans.GridBeanJobWrapper;
import com.intel.gpe.clients.all.gridbeans.InternalGridBean;
import com.intel.gpe.clients.all.gridbeans.panels.IGridBeanInputPanel;
import com.intel.gpe.clients.all.i18n.Messages;
import com.intel.gpe.clients.all.i18n.MessagesKeys;
import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.JobClient;
import com.intel.gpe.clients.api.transfers.GPEFileFactory;
import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.clients.common.JAXBContextProvider;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.gridbeans.jsdl.GPEJobImpl;
import com.intel.gpe.gridbeans.parameters.GridBeanParameter;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IEnvironmentVariableParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileSetParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.plugins.DataSetException;
import com.intel.gpe.util.collections.CollectionUtil;
import com.intel.gpe.util.observer.IObserver;
import com.intel.gpe.util.sets.Pair;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.core.util.CompositeClassLoader;

import de.fzj.unicore.rcp.common.adapters.InputFileContainerAdapter;
import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.common.utils.StringUtils;
import de.fzj.unicore.rcp.gpe4eclipse.FakeProgressListener;
import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.ISWTClient;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.StageOutTypeNoneExtension;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.GridBeanRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditor;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditorInput;
import de.fzj.unicore.rcp.gpe4eclipse.utils.CompatibilityUtils;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GPEPathUtils;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.SimpleActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateDescriptor;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowUtils;

/**
 * Model object storing the current state (parameters) of a GridBean. The
 * GridBean is steered via a GridBeanClient instance. When an instance of this
 * class gets serialized, it stores the Grid Bean Job representing the current
 * state as a String (XML). Changes in the properties of the underlying
 * GridBeanModel are propagated to PropertyChangeListeners that have been
 * registered to this. Files that need to be staged in or staged out are stored,
 * too.
 * 
 * @author demuth
 * 
 */
@XStreamAlias("GridBeanActivity")
@SuppressWarnings({"unchecked","rawtypes"})
public class GridBeanActivity extends SimpleActivity implements
PropertyChangeListener {

	class FileInitializer implements Runnable {
		Queue<Pair<IGridBeanParameter, String>> singleFiles = new ConcurrentLinkedQueue<Pair<IGridBeanParameter, String>>();

		Queue<Pair<IGridBeanParameter, Set<String>>> filesets = new ConcurrentLinkedQueue<Pair<IGridBeanParameter, Set<String>>>();
		boolean running = false;
		public FileInitializer() {

		}

		public void run() {
			running = true;
			try {


				while (!singleFiles.isEmpty() || !filesets.isEmpty()) {
					IGridBean gb = getGridBean();
					if (gb == null) {
						break;
					}
					if (!singleFiles.isEmpty()) {
						Pair<IGridBeanParameter, String> pair = singleFiles.poll();
						IGridBeanParameter param = pair.getM1();
						IGridBeanParameter currentParam = param.isInputParameter() ? gb
								.findInputParameter(param.getName()) : gb
								.findOutputParameter(param.getName());
								if (!param.equals(currentParam)) {
									continue; // the parameter has changed!
								}
								String id = pair.getM2();
								Object currentValue = gb.get(param.getName());
								if (currentValue instanceof IFileParameterValue) {
									IFileParameterValue file = (IFileParameterValue) currentValue;
									if (id.equals(file.getUniqueId())) {
										file = file.clone();
										file = doInitNewFile(file);
										gb.set(param.getName(), file);
									}

								}
					}
					if (!filesets.isEmpty()) {
						Pair<IGridBeanParameter, Set<String>> pair = filesets
						.poll();
						IGridBeanParameter param = pair.getM1();
						IGridBeanParameter currentParam = param.isInputParameter() ? gb
								.findInputParameter(param.getName()) : gb
								.findOutputParameter(param.getName());
								if (!param.equals(currentParam)) {
									continue; // the parameter has changed!
								}
								Set<String> ids = pair.getM2();
								Object currentValue = gb.get(param.getName());
								if (currentValue instanceof IFileSetParameterValue) {
									IFileSetParameterValue fileset = (IFileSetParameterValue) currentValue;

									if (fileset != null) {
										List<IFileParameterValue> files = fileset
										.getFiles();
										IFileParameterValue toRemove = null;
										IFileParameterValue replacement = null;
										int i = 0;
										for (; i < files.size(); i++) {
											if (ids.contains(files.get(i).getUniqueId())) {
												toRemove = files.get(i);
												replacement = toRemove.clone();
												replacement = doInitNewFile(replacement);
												break;
											}
										}
										if (toRemove != null) {
											try {
												fileset = (IFileSetParameterValue) fileset
												.clone();
											} catch (CloneNotSupportedException e) {
												GPEActivator.log(Status.ERROR, "Error while cloning file set: "+e.getMessage(), e);
											}
											fileset.removeFile(toRemove);
											fileset.addFile(i, replacement);
											gb.set(param.getName(), fileset);
										}
									}
								}
					}
				}
			} finally {
				running = false;
			}
		}
	}


	private static final long serialVersionUID = -7290266075748561507L;

	/**
	 * Keys for storing information in this activity's properties
	 */
	public static final String JOB = "gridBeanJob";

	public static final String GRID_BEAN_INFO = "gridBeanInfo";

	/**
	 * The key for storing options for steering the server side splitting
	 * mechanism in a property. Note that server side splitting may not be
	 * supported for all applications and by all workflow engines.
	 */
	public static final String PROP_SPLITTING_OPTIONS = "Splitting Options";
	
	public static final String PROP_IGNORE_FAILURE_OPTIONS = "Ignore Failure Options";

	public static QName TYPE = new QName("http://www.unicore.eu/",
	"GridBeanActivity");

	/**
	 * Client stub for managing the job that runs remotely after submission must
	 * be set by the Submitter of the workflow (@see
	 * de.fzj.unicore.rcp.wfeditor.submission.ISubmitter)
	 */
	private JobClient jobClient;
	private transient GridBeanClient<IWorkbenchPart> gridBeanClient;

	private transient IGridBean gridBean;

	protected boolean gridBeanSelected = false;

	/**
	 * IPath at which the job file is stored
	 */
	protected IPath jobFile = null;

	protected transient boolean jobLoaded = false;

	protected transient boolean listeningToGridBean = false;

	private transient boolean loadingJob = false;

	private transient boolean dirty = false; // true iff the GridBean model has
	// been changed by someone in
	// the diagram

	private transient FileInitializer fileInitializer = new FileInitializer();

	public GridBeanActivity() {
		setPropertyValue(PROP_SPLITTING_OPTIONS, new SplittingOptions());
		setPropertyValue(PROP_IGNORE_FAILURE_OPTIONS, new IgnoreFailureOptions());
	}

	@Override
	public void activate() {

		if (getDiagram().getActivityByName(getName()) != this
				&& !getDiagram().isUniqueActivityName(getName())) {
			// fix the name if it is used by some other activity.. this can
			// happen when pasting jobs quickly
			String name = StringUtils.cutOffDigits(getName());
			if (WorkflowUtils.isAllowedAsActivityName(name) == null) {
				name = getDiagram().getUniqueActivityName(name);
			} else {
				name = getDiagram().getUniqueActivityName(
						GPE4EclipseConstants.DEFAULT_JOB_NAME);
			}
			setName(name);
		}

		// called after being added to the diagram
		super.activate();

		long start = System.currentTimeMillis();
		// wait until the grid bean has been loaded but give up after 30 seconds
		while (!isGridBeanSelected()
				&& System.currentTimeMillis() - start < 30000) {
			boolean sleep = !PlatformUI.getWorkbench().getDisplay()
			.readAndDispatch();
			if (sleep) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// do nothing
				}
			}
		}
		try {
			if (getJobFile() == null) {
				deserializeAndGetReady();
				setJobFile(createPathForJobFile());
			}
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to create job file for job activity " + getName(),
					e);
		}

		IPath path = GPEPathUtils.inputFileDirForProject(getDiagram()
				.getProject());
		IFolder f = PathUtils.absolutePathToEclipseFolder(path);
		if (!f.exists()) {
			try {
				f.create(true, true, null);
			} catch (CoreException e) {
				GPEActivator.log(Status.ERROR, "Error while creating input directory for job: "+e.getMessage(), e);
			}
		}
		setTempDir();

	}

	protected void addActivityNameToFilename(IFileParameterValue file) {
		String sourceFileName = file.getSource().getRelativePath();

		if (ProtocolConstants.LOCAL_PROTOCOL.equals(file.getSource()
				.getProtocol())
				&& sourceFileName != null
				&& !sourceFileName.startsWith(getName())) {
			File f = new File(sourceFileName);
			if (!f.exists()) {
				file.getSource().setRelativePath(
						getName() + "_" + sourceFileName);
				file.getSource().setDisplayedString(null);
			}
		}
	}

	protected void addOutputWorkflowFile(IFileParameterValue value) {
		IGridFileAddress address = null;
		if (value.isInputParameter()) {
			return;
		} else {
			address = value.getTarget();
		}

		IStageTypeExtensionPoint ext = GPEActivator.getDefault()
		.getStageOutTypeRegistry()
		.getDefiningExtension(address.getProtocol().getName());
		if (ext == null || !ext.isWorkflowFileType()) {
			return;
		}

		WorkflowFile wfFile = GridBeanUtils.createWFFileFrom(this,
				value.getUniqueId(), address);
		wfFile.getMimeTypes().addAll(value.getMimeTypes());
		addOutputFile(wfFile);
		getDataSourceList().addDataSource(wfFile);
	}

	@Override
	public void addPersistentPropertyChangeListener(PropertyChangeListener l) {
		super.addPersistentPropertyChangeListener(l);
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener l) {
		super.addPropertyChangeListener(l);
	}

	@Override
	protected void addPropertyDescriptors() {
		super.addPropertyDescriptors();
		addPropertyDescriptor(new PropertyDescriptor(PROP_SPLITTING_OPTIONS,
				PROP_SPLITTING_OPTIONS));
		addPropertyDescriptor(new PropertyDescriptor(PROP_IGNORE_FAILURE_OPTIONS, PROP_IGNORE_FAILURE_OPTIONS));
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
		{
			GridBeanInfo gridbeanInfo = (GridBeanInfo) getPropertyValue(GRID_BEAN_INFO);
			GridBeanRegistry reg = GPEActivator.getDefault()
			.getGridBeanRegistry();
			if (gridbeanInfo != null && reg.getGridBean(gridbeanInfo) == null) {
				gridbeanInfo = reg.getNewerGridBean(gridbeanInfo);
				if (gridbeanInfo != null) {
					setPropertyValue(GRID_BEAN_INFO, gridbeanInfo);
				}
			}
		}
		if (getSplittingOptions() != null) {
			getSplittingOptions().afterDeserialization();
		}
		if (getIgnoreFailureOptions() != null) {
			getIgnoreFailureOptions().afterDeserialization();
		}
		else{
			//ignore failure is null, could happen after loading old workflow
			setPropertyValue(PROP_IGNORE_FAILURE_OPTIONS, new IgnoreFailureOptions());
		}
	}

	@Override
	public void afterSaveAs(IPath oldLocation, boolean recursive,
			boolean fetchLocalInputFiles, boolean fetchRemoteInputFiles,
			IProgressMonitor monitor) throws Exception {

		setJobFile(createPathForJobFile());

	}

	@Override
	public void afterSerialization() {
		super.afterSerialization();
	}

	@Override
	public void beforeSaveAs(IPath newLocation, boolean recursive,
			boolean fetchLocalInputFiles, boolean fetchRemoteInputFiles,
			IProgressMonitor monitor) throws Exception {
		deserializeAndGetReady();

		if (getGridBeanClient().getGridBeanInputPanel() != null) {
			try {
				getGridBeanClient().getGridBeanPlugin()
				.saveDataToExternalSource(new FakeProgressListener());
			} catch (DataSetException e) {
				GPEActivator.log(IStatus.ERROR,
						"Could not save Job's data to external files", e);
			}
			getGridBeanClient().applyInputPanelValues();
			getGridBeanClient().validateJob();

		}


		IPath fullPath = newLocation.append(WFConstants.WORKFLOW_JOB_DIR)
		.append(getName() + "."
				+ GPE4EclipseConstants.FILE_EXTENSION_JOBS);
		IPath projectPath = getDiagram().getProject().getLocation();
		IPath jobFile = PathUtils.makeRelativeTo(projectPath, fullPath);

		File targetDir = fullPath.toFile().getParentFile();
		if (!targetDir.exists()) {
			targetDir.mkdirs();
		}
		if (targetDir.canWrite()) {
			File target = fullPath.toFile();
			GridBeanUtils.saveJobAndWait(getGridBeanClient(), target,
					PlatformUI.getWorkbench().getDisplay());
			setJobFile(jobFile);
		}

		// TODO fetch and copy additional local and remote files

	}

	/**
	 * This is called right before serializing the activity Here we use it for
	 * serializing the GridBeanJob into a File
	 * 
	 */
	@Override
	public void beforeSerialization() {

		try {
			getGridBeanClient().applyInputPanelValues();
		} catch (Exception e) {
			GPEActivator.log(IStatus.WARNING,
					"Problem while applying job input values", e);
		}
		if (getGridBean() != null) {
			try {
				String name = (String) getGridBean()
				.get(IGridBeanModel.JOBNAME);
				if (!getName().equals(name)) {
					setName(name);
				}
			} catch (Exception e) {

			}
		}

		// check whether the old job file should be renamed
		// only do this when saving the diagram as doing this
		// immediately will lead to a broken workflow when the
		// workflow file is not saved!
		IPath oldPath = getJobFile();
		if (oldPath != null) {
			IPath newPath = createPathForJobFile();
			if (!oldPath.equals(newPath)) {
				File source = PathUtils.pathToFile(oldPath, getDiagram()
						.getProject());
				setJobFile(newPath);
				String oldActivityName = oldPath.removeFileExtension()
				.lastSegment();
				if (getDiagram().isUniqueActivityName(oldActivityName)) {
					// only delete old job files that are not needed by other
					// activities
					source.delete();
				}
				IGridBeanInputPanel<IWorkbenchPart> panel = getGridBeanClient()
				.getGridBeanInputPanel();
				if (panel != null) {
					JobEditor editor = (JobEditor) panel.getComponent();
					IPath absolute = getDiagram().getProject().getFile(newPath)
					.getLocation();
					JobEditorInput input = editor.getEditorInput();
					input.setPath(absolute);
				}
			}
		}

		if (getGridBeanClient().getInternalGridBean() != null) {
			if (getGridBeanClient().getGridBeanInputPanel() != null) {
				JobEditor jobEditor = ((JobEditor) getGridBeanClient()
						.getGridBeanInputPanel().getComponent());
				if (jobEditor.isDirty() || dirty) 
				{
					// don't rely on the job editor's dirty flag: 
					// it is reset when the editor is opened so any model
					// changes prior to opening would be ignored
					jobEditor.doActualSave(jobEditor.getInputFile(), null);
					jobEditor.setDirty(false);
				} else if (getJobFile() != null) {
					File f = PathUtils.pathToFile(getJobFile(), getDiagram()
							.getProject());
					if (!f.exists()) {
						storeJobInFile(null);
					}
				}
			} else if (getJobFile() != null
					&& (dirty || !getJobFile().toFile().exists())) {
				storeJobInFile(null);
			}
		}
		setPropertyValue(JOB, null);
		super.beforeSerialization();
		dirty = false;
	}

	@Override
	public void beforeSubmission() {
		super.beforeSubmission();
		IGridBean gridBean = getGridBean();
		if (gridBean == null) {
			return;
		}
		GridBeanContext context = (GridBeanContext) gridBean
		.get(GPE4EclipseConstants.CONTEXT);
		context = context.clone();

		context.setEditable(false);
		gridBean.set(GPE4EclipseConstants.CONTEXT, context);
	}

	@Override
	public GridBeanActivity clone() throws CloneNotSupportedException {
		GridBeanActivity result = (GridBeanActivity) super.clone();
		// these should NOT be cloned:
		result.gridBean = null;
		result.gridBeanClient = null;
		result.jobClient = null;
		result.jobFile = null;
		result.jobLoaded = false;
		result.listeningToGridBean = false;
		result.loadingJob = false;
		result.dirty = false;
		return result;
	}

	/**
	 * set local reference to the GridBean's gridbean
	 */
	protected void connectToGridBeanModel() {

		try {
			GridBeanJob gridBeanJob = getGridBeanClient().getGridBeanJob();
			if (gridBeanJob == null) {
				Exception e = new NullPointerException();
				GPEActivator
				.log(IStatus.ERROR,
						"Error while connecting to application plugin: Model unavailable",e );
				return;
			}
			gridBean = gridBeanJob.getModel();
			String oldName = (String) gridBean.get(IGridBeanModel.JOBNAME);
			if (oldName == null) {
				oldName = GPE4EclipseConstants.DEFAULT_JOB_NAME;
			}
			if (getName().trim().length() == 0) {
				oldName = StringUtils.cutOffDigits(oldName);

				// when the activity has just been created try to find a name
				// that is close to the name defined in the GridBean
				String name = getDiagram().getUniqueActivityName(oldName);
				if (WorkflowUtils.isAllowedAsActivityName(name) != null) {
					name = getDiagram().getUniqueActivityName(
							GPE4EclipseConstants.DEFAULT_JOB_NAME);
				}
				setName(name);

			} else if (!oldName.equals(getName())) {
				// make sure the names are equal in order to avoid any
				// inconsistencies
				// TODO does the following do any harm? the "advantage" would be
				// that the job name is reverted if the job editor is closed
				// without saving.. do we want this?
//				if(canSetNameTo(oldName) == null)
//				{
//					setName(oldName);
//				}
//				else
//				{
					gridBean.set(IGridBeanModel.JOBNAME, getName());
//				}
			}
			if (gridBean.get(GPE4EclipseConstants.CONTEXT) == null) {
				gridBean.set(GPE4EclipseConstants.CONTEXT,
						GridBeanUtils.createWorkflowContext(getDiagram()));
				gridBean.getInputParameters().add(
						new GridBeanParameter(GPE4EclipseConstants.CONTEXT,
								GPE4EclipseConstants.PARAMETER_TYPE_CONTEXT));
			}

		} catch (Exception e) {
			GPEActivator
			.log(IStatus.ERROR,
					"Error while connecting to application plugin gridbean.",
					e);
		}
	}

	protected void createJobFile(GridBeanInfo gbInfo) throws Exception {
		if (gbInfo == null) {
			return;
		}
		GridBeanJobWrapper<IWorkbenchPart> wrapper = new GridBeanJobWrapper<IWorkbenchPart>();
		wrapper.setGridBeanInfo(gbInfo);
		InternalGridBean internal = GPEActivator.getDefault()
		.getGridBeanRegistry().getGridBean(gbInfo);
		GridBeanJob job = internal.getJobInstance();
		IGridBean gb = job.getModel();
		gb.set(IGridBeanModel.JOBNAME, getName());
		wrapper.setGridBeanJob(job);
		ClassLoader cl = gb.getClass().getClassLoader();
		Marshaller m = JAXBContextProvider.getContext(cl)
		.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		File outputFile = PathUtils.pathToFile(getJobFile(), getDiagram()
				.getProject());
		if (!outputFile.getParentFile().exists()) {
			outputFile.getParentFile().mkdirs();
		}
		FileOutputStream os = new FileOutputStream(outputFile);
		try {
			m.marshal(wrapper, os);
		} finally {
			os.close();
		}
		refreshWorkspace(null);
	}

	protected IPath createPathForJobFile() {
		String fileName = getName() + "."
		+ GPE4EclipseConstants.FILE_EXTENSION_JOBS;
		IPath diagramParent = null;
		if (diagram.getParentFolder() != null
				&& diagram.getParentFolder().trim().length() > 0) {
			diagramParent = Path.fromPortableString(diagram.getParentFolder());
		}
		String parentDir = WFConstants.WORKFLOW_JOB_DIR;
		IFolder parent = diagramParent == null ? diagram.getProject()
				.getFolder(parentDir) : diagram.getProject()
				.getFolder(diagramParent).getFolder(parentDir);
				return parent.getFile(fileName).getProjectRelativePath();
	}

	/**
	 * load saved job from the property or job file and and initialize the grid
	 * bean correctly CAUTION! This may take a while!
	 */
	public void deserializeAndGetReady() {

		try {
			if (!isGridBeanSelected()) {
				return;
			}
			if (getGridBean() == null) {
				if (getGridBeanClient().getGridBeanJob() != null
						&& getGridBeanClient().getGridBeanJob().getModel() != null) {
					// this might happen when the gridbean is first loaded by a
					// JobEditor
					initAfterLoadingGridBean(getGridBeanClient()
							.getGridBeanJobWrapper().getGridBeanInfo());
				}
			}
			if (getGridBean() != null) {
				return; // make sure to never load the gridbean twice!
			}

			boolean loadJob = !jobLoaded()
			&& (hasJobInProperty() || jobFile != null);
			if (!jobLoaded() && !loadJob) {
				GPEActivator.log(IStatus.ERROR, "Broken activity in workflow: "
						+ getName());
				return;
			}
			loadJob = loadJob && !loadingJob; // just wait when job is already
			// loaded by another thread
			if (loadJob) {
				loadingJob = true;
				loadJob(null);
			}
			long start = System.currentTimeMillis();
			// wait until the job is loaded but give up after 60 seconds
			while (!jobLoaded() && System.currentTimeMillis() - start < 60000) {
				Display d = PlatformUI.getWorkbench().getDisplay();
				boolean isMainThread = Thread.currentThread().equals(
						d.getThread());
				boolean sleep = true;
				if (isMainThread) {
					sleep = !d.readAndDispatch();
				}
				if (sleep) {
					Thread.sleep(50);
				}
			}

			if (getGridBean() == null) {
				connectToGridBeanModel();
				startListeningToGridBeanModel();
			}
			dirty = false;

		} catch (IOException e) {
			GPEActivator.log(IStatus.ERROR, e.getMessage(), e);
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Could not deserialize saved data for application "
					+ getName(), e);
		}

	}

	@Override
	public void dispose() {
		super.dispose();

		// this is needed in the following scenario:
		// workflow gets deserialized, this hasn't been fully deserialized yet
		// this gets deleted, workflow is saved, deletion is undone, workflow is
		// saved
		// without the following line, the job file is not restored when undoing
		// the deletion!
		deserializeAndGetReady();

	}

	private IFileParameterValue doInitNewFile(IFileParameterValue file) {
		// set default target protocol if the protocol has not been set
		if (file.isInputParameter()) {
			// addActivityNameToFilename(file); TODO should we do something like
			// this or not?
			return file;
		} else {
			IStageTypeExtensionPoint ext = GPEActivator
			.getDefault()
			.getStageOutTypeRegistry()
			.getDefiningExtension(
					file.getTarget().getProtocol().getName());
			if (ext == null || (ext instanceof StageOutTypeNoneExtension)) {
				IStageTypeExtensionPoint defaultType = GPEActivator
				.getDefault().getStageOutTypeRegistry()
				.getDefaultStageType(getGridBeanContext());
				if (defaultType != null) {
					return defaultType.attachToParameterValue(this, file);
				}
			}
		}
		return file;
	}

	@Override
	public void finalDispose(IProgressMonitor progress) {
		if (progress != null) {
			progress.beginTask("", 2);
		}
		try {
			SubProgressMonitor sub = null;
			if (progress != null) {
				new SubProgressMonitor(progress, 1);
			}
			super.finalDispose(sub);
			if (progress != null) {
				sub = new SubProgressMonitor(progress, 1);
			}
			if (getJobFile() != null) {
				File file = PathUtils.pathToFile(getJobFile(), getDiagram()
						.getProject());
				if (file.exists()) {
					if (file.delete()) {
						refreshWorkspace(sub);
					}
				}
			}

		} finally {
			if (progress != null) {
				progress.done();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@Override
	public Object getAdapter(Class adapter) {
		if (IGridBean.class.equals(adapter)) {
			if (getGridBean() == null) {
				deserializeAndGetReady();
			}
			return getGridBean();
		} else if (InputFileContainerAdapter.class.equals(adapter)) {
			return new InputFileContainerAdapter(
					GPEPathUtils.inputFileDirForProject(getDiagram()
							.getProject()));
		} else if (Client.class.equals(adapter)) {
			if (getGridBean() == null) {
				deserializeAndGetReady();
			}
			return getGridBeanClient().getClient();
		} else {
			return super.getAdapter(adapter);
		}
	}

	/**
	 * Create a copy of this activity. This may take a while and while its done,
	 * no other GUI events are processed.
	 */
	@Override
	public GridBeanActivity getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		try {
			// temporarily store the job in a property in order to clone it
			storeJobInProperty();
			final GridBeanActivity result = (GridBeanActivity) super.getCopy(
					toBeCopied, mapOfCopies, copyFlags);
			setPropertyValue(JOB, null);

			// avoid re-initializing the gridbean's parameters!
			result.gridBeanSelected = true;
			result.deserializeAndGetReady();

			Set<Object> delayedProps = new HashSet<Object>();
			delayedProps.add(PROP_DATA_SINK_LIST);
			delayedProps.add(PROP_DATA_SOURCE_LIST);
			result.copyPropertyValuesFrom(this, delayedProps, toBeCopied,
					mapOfCopies, copyFlags);

			// don't set the result's JOB property back to null!
			// the property is needed to undo changes when the JobEditor
			// is closed without saving!

			// copy values in GridBean gridbean that are neither input nor
			// output params
			try {
				ClassLoader cl = JAXBContextProvider.getDefaultClassLoader();
				CompositeClassLoader union = new CompositeClassLoader();
				union.add(cl);
				union.add(getGridBean().getClass().getClassLoader());
				IGridBean clone = getGridBean();

				Set<QName> ignored = new HashSet<QName>();
				for (IGridBeanParameter param : getGridBean()
						.getInputParameters()) {
					ignored.add(param.getName());
				}
				for (IGridBeanParameter param : getGridBean()
						.getOutputParameters()) {
					ignored.add(param.getName());
				}
				ignored.add(IGridBeanModel.JOBNAME);
				for (QName key : clone.keySet()) {
					Object value = clone.get(key);
					if (!ignored.contains(key)
							&& !CollectionUtil.equalOrBothNull(result
									.getGridBean().get(key), value)) {
						result.getGridBean().set(key,
								CloneUtils.deepClone(value));
					}
				}

			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, "Error while copying job: "+e.getMessage(), e);
			}

			result.getGridBean().afterCopy();

			return result;
		} catch (Exception e) {
			WFActivator.log(IStatus.ERROR,
					"Could not create copy of the element " + getName(), e);
			return null;
		}
	}

	private FileInitializer getFileInitializer() {
		if (fileInitializer == null) {
			fileInitializer = new FileInitializer();
		}
		return fileInitializer;
	}

	public IGridBean getGridBean() {
		return gridBean;
	}

	/**
	 * Retrieve the GridBeanClient for handling the GridBean. If none exists,
	 * create one.
	 * 
	 * @return
	 */
	public GridBeanClient<IWorkbenchPart> getGridBeanClient() {
		if (gridBeanClient == null) {
			ISWTClient client = GPEActivator.getDefault().getClient();
			gridBeanClient = client.createGridBeanClient();
		}
		return gridBeanClient;
	}

	public GridBeanContext getGridBeanContext() {
		try {
			return (GridBeanContext) getGridBean().get(
					GPE4EclipseConstants.CONTEXT);
		} catch (Exception e) {
			return null;
		}
	}



	public JobClient getJobClient() {
		return jobClient;
	}

	public IPath getJobFile() {
		return jobFile;
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.remove(PROP_DATA_SINK_LIST); // copy this later
		propertiesToCopy.remove(PROP_DATA_SOURCE_LIST); // copy this later
		propertiesToCopy.add(GRID_BEAN_INFO);
		propertiesToCopy.add(JOB);
		propertiesToCopy.add(PROP_SPLITTING_OPTIONS);
		propertiesToCopy.add(PROP_IGNORE_FAILURE_OPTIONS);
		return propertiesToCopy;
	}

	public SplittingOptions getSplittingOptions() {
		return (SplittingOptions) getPropertyValue(PROP_SPLITTING_OPTIONS);
	}
	
	public IgnoreFailureOptions getIgnoreFailureOptions() {
		return (IgnoreFailureOptions) getPropertyValue(PROP_IGNORE_FAILURE_OPTIONS);
	}

	@Override
	public QName getType() {
		return TYPE;
	}

	/**
	 * Handles the situation when the underlying gridBean model of the gridbean
	 * client was changed. This happens, for instance, when a {@link JobEditor}
	 * is closed without saving.
	 */
	protected void gridBeanModelChanged() {
		if (gridBean != null) {
			gridBean.removePropertyChangeListener(this);
		}
		GridBeanInfo gridbeanInfo = (GridBeanInfo) getPropertyValue(GridBeanActivity.GRID_BEAN_INFO);
		initAfterLoadingGridBean(gridbeanInfo);
	}

	public boolean hasJobInProperty() {
		return (getPropertyValue(JOB) != null);
	}

	protected void initAfterLoadingGridBean(GridBeanInfo gbInfo) {
		connectToGridBeanModel();

		// if gridbean is first loaded:
		// set its parameter values to defaults
		if (!gridBeanSelected) {
			GridBeanContext gridBeanContext = (GridBeanContext) getGridBean()
			.get(GPE4EclipseConstants.CONTEXT);
			setDefaultParameterValues(getGridBean(), gridBeanContext);
		}
		gridBeanSelected = true;
		setPropertyValue(GRID_BEAN_INFO, gbInfo);
		startListeningToGridBeanModel();
		jobLoaded = true;
	}

	protected void initNewFiles(PropertyChangeEvent evt) {
		QName name = QName.valueOf(evt.getPropertyName());
		IGridBeanParameter param = getGridBean().findInputParameter(name);
		if (param == null) {
			param = getGridBean().findOutputParameter(name);
		}
		if (param == null) {
			return;
		}
		boolean workAdded = false;
		if (evt.getNewValue() instanceof IFileParameterValue
				&& !(evt.getOldValue() instanceof IFileParameterValue)) {
			IFileParameterValue file = (IFileParameterValue) evt.getNewValue();
			getFileInitializer().singleFiles
			.offer(new Pair<IGridBeanParameter, String>(param, file
					.getUniqueId()));
			workAdded = true;
		} else if (evt.getNewValue() instanceof IFileSetParameterValue) {
			Set<String> oldIds = new HashSet<String>();
			if (evt.getOldValue() instanceof IFileSetParameterValue) {
				IFileSetParameterValue oldFileset = (IFileSetParameterValue) evt
				.getOldValue();
				for (IFileParameterValue file : oldFileset.getFiles()) {
					oldIds.add(file.getUniqueId());
				}
			}

			IFileSetParameterValue newFileset = (IFileSetParameterValue) evt
			.getNewValue();
			Set<String> idsToInit = new HashSet<String>();
			for (IFileParameterValue file : newFileset.getFiles()) {
				String id = file.getUniqueId();
				if (!oldIds.contains(id)) {
					idsToInit.add(id);
				}
			}
			if (idsToInit.size() > 0) {
				getFileInitializer().filesets
				.offer(new Pair<IGridBeanParameter, Set<String>>(param,
						idsToInit));
				workAdded = true;
			}
		}
		if (workAdded && !getFileInitializer().running) {
			// need to restart
			Display d = PlatformUI.getWorkbench().getDisplay();
			if(!d.isDisposed())
			{
				// Not doing this in the UI thread in the past lead to a
				// classic race condition. This was visible when selecting
				// multiple local files simultaneously as input for a workflow
				// job.
				d.asyncExec(fileInitializer);
			}
		}
	}

	public boolean isGridBeanSelected() {
		return gridBeanSelected;
	}

	public boolean jobLoaded() {
		return jobLoaded;
	}

	public void loadJob(IObserver obs) throws IOException {
		if (hasJobInProperty()) {
			loadJobFromProperty(obs);
		} else {
			loadJobFromFile(obs);
		}
	}

	/**
	 * Deserialize the job description from a file back to a GridBeanJobWrapper
	 * object and load this into the GridBeanClient.
	 */
	protected void loadJobFromFile(final IObserver obs) throws IOException {
		File file = PathUtils.pathToFile(getJobFile(), getDiagram()
				.getProject());
		if (!file.exists()) {
			throw new FileNotFoundException("Cannot load job from file: File "
					+ file + " does not exist.");
		} else if (!file.canRead()) {
			throw new IOException("Cannot load job from file: File " + file
					+ " cannot be read.");
		} else {
			try {

				getGridBeanClient().loadJob(file.getAbsolutePath(),
						new IObserver() {
					public void observableUpdate(Object arg0,
							Object arg1) {
						connectToGridBeanModel();
						startListeningToGridBeanModel();
						jobLoaded = true;
						if (obs != null) {
							obs.observableUpdate(arg0, arg1);
						}

					}
				});

			} catch (Exception e) {
				GPEActivator.log("Error reloading saved job description", e);
			}
		}
	}

	/**
	 * Deserialize the job description (which is a string) back to a
	 * GridBeanJobWrapper object and load this into the GridBeanClient.
	 */
	protected void loadJobFromProperty(final IObserver obs) {

		try {
			GridBeanJobWrapper<IWorkbenchPart> wrapper = (GridBeanJobWrapper<IWorkbenchPart>) getPropertyValue(JOB);
			getGridBeanClient().loadJob(wrapper, new IObserver() {
				public void observableUpdate(final Object arg0,
						final Object arg1) {
					connectToGridBeanModel();
					startListeningToGridBeanModel();

					// make sure the name is ok
					// this is needed when using getCopy() in order to implement
					// pasting
					// GridBeanActivities
					listeningToGridBean = false;
					getGridBean().set(IGridBeanModel.JOBNAME, getName());
					listeningToGridBean = true;
					jobLoaded = true;
					if (obs != null) {
						obs.observableUpdate(arg0, arg1);
					}
				}
			});
		} catch (Exception e) {
			GPEActivator.log("Error reloading saved job description", e);
		}
	}

	@Override
	public void move(int dx, int dy) {
		super.move(dx, dy);
	}

	/**
	 * forward property change from gridBean to registered listeners
	 */
	public void propertyChange(final PropertyChangeEvent evt) {
		if (GPE4EclipseConstants.MODEL_DISPOSED.toString().equals(
				evt.getPropertyName())) {
			gridBeanModelChanged();
			return;
		}
		if (!listeningToGridBean) {
			return;
		}
		if (getDiagram().isEditable()) {
			dirty = true;
			// final GridBeanModelChangeCommand cmd = new
			// GridBeanModelChangeCommand(this, gridBean, evt);

			// PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable()
			// {
			// public void run() {
			// getCommandStack().execute(cmd);

			// }}
			// );

		} else {
			return;
		}

		if (IGridBeanModel.JOBNAME.toString().equals(evt.getPropertyName())) {
			boolean success = this.setName((String) evt.getNewValue());
			if (success) {
				getDiagram().setDirty(true);
			} else {
				// revert name to old value
				// do this asynchronously, cause otherwise, the
				// GenericGridBeanPanel
				// would not realize the property change (since it has fired the
				// event we have
				// just received))
				Job job = new BackgroundJob("") {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						listeningToGridBean = false;
						getGridBean().set(IGridBeanModel.JOBNAME,
								evt.getOldValue());
						listeningToGridBean = true;
						return Status.OK_STATUS;
					}
				};
				job.setSystem(true);
				job.schedule(300);
			}
		}

		initNewFiles(evt);

		// note: the list of file and fileset data sources is updated by the
		// stage list
		// cause it is opened anyway and contains all the necessary infos to
		// deal
		// with changes appropriately (especially a list of old stageouts that
		// are not valid anymore
		// when a fileset gets emptied

		firePropertyChange(evt.getPropertyName(), evt.getOldValue(),
				evt.getNewValue());

	}

	private Object readResolve() {
		loadingJob = false;
		jobLoaded = false;
		return this;
	}

	@Override
	public boolean readyForSubmission() {
		return isGridBeanSelected();
	}

	protected void refreshWorkspace(IProgressMonitor monitor) {
		try {
			getDiagram().getProject().refreshLocal(2, monitor);
		} catch (CoreException e) {
			// this is a best effort service
		}
	}

	@Override
	public void removePersistentPropertyChangeListener(PropertyChangeListener l) {
		super.removePersistentPropertyChangeListener(l);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener l) {
		super.removePropertyChangeListener(l);
	}

	protected void setDefaultParameterValues(IGridBean gridBean,
			GridBeanContext context) {

		List<IGridBeanParameter> params = gridBean.getInputParameters();
		for (int i = 0; i < params.size(); i++) {
			try {
				IGridBeanParameter param = params.get(i);
				IGridBeanParameterValue value = (IGridBeanParameterValue) gridBean
				.get(param.getName());
				if (GridBeanParameterType.FILE.equals(param.getType())) {
					IFileParameterValue file = (IFileParameterValue) value;
					addActivityNameToFilename(file);
					IDataSink sink = new GPEWorkflowFileSink(this, param,
							file.getUniqueId());
					sink.setPropertyValue(IDataSink.PROP_MUST_BE_FILLED, true);
					getDataSinkList().addDataSink(sink);
				} else if (GridBeanParameterType.FILE_SET.equals(param
						.getType())) {
					IFileSetParameterValue fileSet = (IFileSetParameterValue) value;
					for (IFileParameterValue file : fileSet.getFiles()) {
						addActivityNameToFilename(file);
					}
					getDataSinkList().addDataSink(
							new GPEWorkflowFileSetSink(this, param));
				} else if (GridBeanParameterType.ENVIRONMENT_VARIABLE
						.equals(param.getType())) {
					IEnvironmentVariableParameterValue env = (IEnvironmentVariableParameterValue) value;
					if (!Boolean.class.equals(env.getTargetType())) {
						getDataSinkList().addDataSink(
								new GPEWorkflowVariableSink(this, param));
					}
				}
			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, "Error while initializing parameters: "+e.getMessage(), e);
			}
		}

		IStageTypeExtensionPoint defaultType = GPEActivator.getDefault()
		.getStageOutTypeRegistry().getDefaultStageType(context);

		if (defaultType != null) {
			params = gridBean.getOutputParameters();
			for (int i = 0; i < params.size(); i++) {
				try {
					IGridBeanParameter param = params.get(i);
					IGridBeanParameterValue value = (IGridBeanParameterValue) gridBean
					.get(param.getName());
					if (GridBeanParameterType.FILE.equals(param.getType())) {
						IFileParameterValue file = (IFileParameterValue) value;
						if (ProtocolConstants.NONE_PROTOCOL.equals(file
								.getTarget().getProtocol())
								&& file.isTargetModifiable()
								&& !defaultType.equals(file.getTarget()
										.getProtocol())) {
							IFileParameterValue inited = defaultType
							.attachToParameterValue(this, file);

							gridBean.set(param.getName(), inited);
							addOutputWorkflowFile(inited);
						}

					}

					else if (GridBeanParameterType.FILE_SET.equals(param
							.getType())) {
						IFileSetParameterValue fileSet = (IFileSetParameterValue) value;
						List<IFileParameterValue> defaultFiles = new ArrayList<IFileParameterValue>();
						for (IFileParameterValue file : fileSet.getFiles()) {
							if (ProtocolConstants.NONE_PROTOCOL.equals(file
									.getTarget().getProtocol())
									&& file.isTargetModifiable()
									&& !defaultType.equals(file.getTarget()
											.getProtocol())) {
								IFileParameterValue inited = defaultType
								.attachToParameterValue(this, file);
								// String old =
								// inited.getTarget().getUserDefinedString();
								// inited.getTarget().setUserDefinedString(getID()+"/"+old);
								// inited.getTarget().setDisplayedString(old);

								defaultFiles.add(inited);
								addOutputWorkflowFile(inited);

							} else {
								defaultFiles.add(file);
							}
						}
						fileSet.setFiles(defaultFiles);
						gridBean.set(param.getName(), fileSet);
					}
				} catch (Exception e) {
					GPEActivator.log(Status.ERROR, "Error while initializing parameters: "+e.getMessage(), e);
				}

			}
		}
	}

	@Override
	public void setExecutionStateDescriptor(final ExecutionStateDescriptor state) {
		int oldState = getCurrentExecutionStateCipher();
		super.setExecutionStateDescriptor(state);
		int newState = getCurrentExecutionStateCipher();
		if (getGridBean() != null) {
			GridBeanContext context;
			context = getGridBeanContext().clone();
			listeningToGridBean = false;
			context.setJobExecutionState(newState);
			boolean editable = newState == ExecutionStateConstants.STATE_UNSUBMITTED;
			context.setEditable(editable);
			getGridBean().set(GPE4EclipseConstants.CONTEXT, context);
			listeningToGridBean = true;

		}
		if (oldState != ExecutionStateConstants.STATE_FAILED
				&& newState == ExecutionStateConstants.STATE_FAILED) {
			GPEActivator.log(IStatus.INFO, "Failed workflow job " + getName()
					+ ": " + getCurrentExecutionStateDescription());
		}
	}

	public void setGridBeanClient(GridBeanClient<IWorkbenchPart> gridBeanClient) {
		this.gridBeanClient = gridBeanClient;
	}

	public void setJobClient(JobClient jobClient) {
		this.jobClient = jobClient;
	}

	public void setJobFile(IPath jobFile) {
		this.jobFile = jobFile;
	}

	@Override
	public boolean setName(final String name) {
		boolean success = super.setName(name);
		if (!success) {
			return false;
		}
		// update the GridBeanModel
		if (getGridBean() != null) {
			listeningToGridBean = false;
			getGridBean().set(IGridBeanModel.JOBNAME, name);
			listeningToGridBean = true;
		}
		return true;
	}

	public void setSelectedGridBean(final GridBeanInfo gbInfo,
			final IObserver observer) {

		String gbPath = GPEActivator.getDefault().getGridBeanRegistry()
		.getGridBeanPath(gbInfo);
		getGridBeanClient().loadGridBean(gbPath, new IObserver() {
			public void observableUpdate(Object arg0, Object arg1) {
				try {
					initAfterLoadingGridBean(gbInfo);
					if (observer != null) {
						observer.observableUpdate(arg0, arg1);
					}
				} catch (Exception e) {
					GPEActivator.log(Status.ERROR, "Error while initializing workflow job: "+e.getMessage(), e);
				}
			}
		});
	}

	public void setSelectedGridBean(InternalGridBean internalGridBean) {
		try {
			getGridBeanClient().setInternalGridBeanAndCreateJob(
					internalGridBean);
			GridBeanInfo gbInfo = new GridBeanInfo(internalGridBean.getName(),
					internalGridBean.getVersion());
			initAfterLoadingGridBean(gbInfo);
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while setting selected application GUI: "+e.getMessage(), e);
		}
	}

	@Override
	public void setState(int state) {
		super.setState(state);
	}

	protected void setTempDir() {
		GPEFileFactory fileFactory = getGridBeanClient().getClient()
		.getFileFactory();
		String tempDir = GPEPathUtils.inputFileDirForProject(
				getDiagram().getProject()).toOSString()
				+ File.separator;
		fileFactory.setTemporaryDirName(tempDir);
	}

	/**
	 * start listening on property changes
	 */
	protected void startListeningToGridBeanModel() {
		if(gridBean == null) return;
		try {
			gridBean.addPropertyChangeListener(this);
			listeningToGridBean = true;
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Problem starting to listen to application GUI model.",e);
		}
	}

	protected void storeJobInFile(final IObserver obs) {
		IPath fullPath = getDiagram().getProject().getLocation()
		.append(getJobFile());
		final String saveTo = fullPath.toOSString();
		// save the job to an external file instead of a property
		getGridBeanClient().saveJob(saveTo, new IObserver() {
			public void observableUpdate(Object theObserved, Object changeCode) {
				if (changeCode instanceof Exception) {
					GPEActivator.log(
							IStatus.ERROR,
							Messages.getString(MessagesKeys.common_actions_SaveGridBeanJobAction_Cannot_save_job_in),
							(Exception) changeCode);
				} else {
					Job j = new BackgroundJob("refreshing workspace") {
						
						@Override
						protected IStatus run(IProgressMonitor monitor) {
							refreshWorkspace(monitor);
							return Status.OK_STATUS;
						}
					};
					j.schedule();
				}
				if (obs != null) {
					obs.observableUpdate(theObserved, changeCode);
				}
			}
		});
	}

	/**
	 * Serialize the job and store it in a property. This is used for keeping
	 * the job in memory when this activity gets deleted in order to be able to
	 * undo deletion.
	 * 
	 */
	protected void storeJobInProperty() {
		GridBeanJobWrapper<?> gridBeanJob = null;
		boolean wasListening = listeningToGridBean;
		try {
			listeningToGridBean = false;
			getGridBeanClient().applyInputPanelValues();
			if (getGridBeanClient().getGridBeanInputPanel() != null) {
				// application specific GUI is still open => validate user
				// inputs!
				getGridBeanClient().validateJob();

			}

			GridBeanJobWrapper<?> wrapper = getGridBeanClient()
			.getGridBeanJobWrapper();
			gridBeanJob = new GridBeanJobWrapper<Object>();
			if (wrapper.getJSDLJob() == null) {
				deserializeAndGetReady();
				wrapper = getGridBeanClient().getGridBeanJobWrapper();
			}
			GPEJobImpl job = new GPEJobImpl(wrapper.getJSDLJob());
			// GPEJobImpl.packGBModel(job.getValue());
			Node n = job.getDocument().newDomNode();
			Element e = (Element) n.getFirstChild();
			gridBeanJob.setJSDLJob(e);
			gridBeanJob.setErrors(wrapper.getErrors());
			gridBeanJob.setGridBeanInfo(wrapper.getGridBeanInfo());

		} catch (Exception e) {
			GPEActivator.log("Problem during serialization of "
					+ this.getClass().getName(), e);
		} finally {
			listeningToGridBean = wasListening;
			setPropertyValue(JOB, gridBeanJob);
		}
	}

	@Override
	public String toString() {
		return getClass().getName() + " " + getName();
	}

	@Override
	public void undoDispose() {
		super.undoDispose();
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		if (updatedToCurrentModelVersion) {
			return; // don't do this twice!
		}
		super.updateToCurrentModelVersion(oldVersion, currentVersion);
		GridBeanInfo gridbeanInfo = (GridBeanInfo) getPropertyValue(GRID_BEAN_INFO);
		GridBeanRegistry reg = GPEActivator.getDefault().getGridBeanRegistry();
		if (gridbeanInfo != null && reg.getGridBean(gridbeanInfo) == null) {
			gridbeanInfo = reg.getNewerGridBean(gridbeanInfo);
			if (gridbeanInfo != null) {
				setPropertyValue(GRID_BEAN_INFO, gridbeanInfo);
			}
		}
		deserializeAndGetReady();
		IGridBean gridBean = getGridBean();
		listeningToGridBean = false;
		// fix input files
		try {
			CompatibilityUtils.fixFileParameterValues(
					getDiagram().getProject(), getGridBean(), oldVersion,
					currentVersion);
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to move input files of activity " + getName()
					+ " to new folder "
					+ GPE4EclipseConstants.DIRECTORY_INPUTS);
		}

		if (oldVersion.compareTo("6.4.0") < 0) {
			// prior to 6.4.0 data sinks and sources didn't exist
			// make sure files are turned into data sinks and sources
			List<IGridBeanParameter> params = gridBean.getInputParameters();
			for (int i = 0; i < params.size(); i++) {
				try {
					IGridBeanParameter param = params.get(i);
					IGridBeanParameterValue value = (IGridBeanParameterValue) gridBean
					.get(param.getName());
					if (GridBeanParameterType.FILE.equals(param.getType())) {
						IFileParameterValue file = (IFileParameterValue) value;
						IDataSink sink = new GPEWorkflowFileSink(this, param,
								file.getUniqueId());
						sink.setPropertyValue(IDataSink.PROP_MUST_BE_FILLED,
								true);
						getDataSinkList().addDataSink(sink);
					} else if (GridBeanParameterType.FILE_SET.equals(param
							.getType())) {
						getDataSinkList().addDataSink(
								new GPEWorkflowFileSetSink(this, param));
					} else if (GridBeanParameterType.ENVIRONMENT_VARIABLE
							.equals(param.getType())) {
						IEnvironmentVariableParameterValue env = (IEnvironmentVariableParameterValue) value;
						if (!Boolean.class.equals(env.getTargetType())) {
							getDataSinkList().addDataSink(
									new GPEWorkflowVariableSink(this, param));
						}
					}
				} catch (Exception e) {
					GPEActivator.log(Status.ERROR, "Error while updating job to latest client version: "+e.getMessage(), e);
				}
			}

			for (WorkflowFile wfFile : getOutputFiles()) {
				getDataSourceList().addDataSource(wfFile);
			}

			setSize(WFConstants.DEFAULT_SIMPLE_ACTIVITY_SIZE);
		}

		// fix environment variables
		CompatibilityUtils.fixEnvironmentParameterValues(getGridBean(),
				oldVersion, currentVersion);

		listeningToGridBean = true;

		// fix stuff when dealing with version prior to client version 6.2.1
		if (oldVersion.compareTo("6.2.1") < 0) {
			setBoundToParent(false);

		}
	}

}
