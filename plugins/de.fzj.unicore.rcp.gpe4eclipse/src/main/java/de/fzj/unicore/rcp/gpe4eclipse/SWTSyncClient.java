/*
 * Copyright (c) 2000-2005, Intel Corporation All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Intel Corporation nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.fzj.unicore.rcp.gpe4eclipse;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.api.async.Request;
import com.intel.gpe.clients.api.async.SyncClient;

/**
 * Implementation of the SyncClient interface for Eclipse.
 * 
 * @author Bastian Demuth
 * 
 */
public class SWTSyncClient implements SyncClient {

	private Queue<Request> requests = new ConcurrentLinkedQueue<Request>(); 

	public SWTSyncClient() {

	}

	public void processOutstandingRequests()
	{
		Request request = requests.poll();
		while(request != null)
		{
			try {
				request.perform(new FakeProgressListener());
				request = requests.poll();
			} catch (Throwable e) {
				GPEActivator.log(
						IStatus.WARNING,
						"Problem while executing request "
						+ request.getName(), new Exception(e));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.AsyncClient#executeRequest(com.intel.gpe.client2
	 * .requests.Request, com.intel.gpe.util.observer.IObserver)
	 */
	public void executeRequest(Request request) {
		requests.add(request);
		Display d = PlatformUI.getWorkbench().getDisplay();
		if (d.isDisposed()) {
			return;
		}
		boolean isSWTThread = Thread.currentThread().equals(d.getThread());
		if (isSWTThread) {
			processOutstandingRequests();
		} else {
			d.syncExec(new Runnable() {

				public void run() {
					processOutstandingRequests();
				}
			});
		}
	}

}
