package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.intel.gpe.clients.api.Job;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.IMatchmaker;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;

public class RegistryMatchmaker implements IMatchmaker {
	private NodePath regPath;
	
	public RegistryMatchmaker(NodePath nodePath) {
		regPath = nodePath;
	}

	public String canHandleJob(Job job) {
		Queue<Node> q = new ConcurrentLinkedQueue<Node>();
		Node registryNode = NodeFactory.getNodeFor(regPath);
		if(registryNode == null) return "Registry does not exist anymore";
		q.addAll(registryNode.getChildren());
		while (!q.isEmpty()) {
			Node next = q.poll();
			for (Node child : next.getChildrenArray()) {
				if(child.getLevel() < 5) 
				{
					q.add(child);
				}
			}
			if (next.getState() == Node.STATE_NEW)
			{
				next.refresh(0,false,null);
			}
				
			IMatchmaker mm = (IMatchmaker) next.getAdapter(IMatchmaker.class);
			if (mm != null) {
				if (mm.canHandleJob(job) == null) {
					return null;
				}
			}
		}
		return "No TSS under this registry can handle this job";
	}
}
