package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types;

import com.intel.gpe.clients.api.jsdl.ResourceRequirementType;

public abstract class AbstractSSRSettingType implements ResourceRequirementType {
	protected boolean enabled = false;
	protected String name;

	public AbstractSSRSettingType() {
	}

	public AbstractSSRSettingType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (enabled ? 1231 : 1237);
		result = prime * result
				+ ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractSSRSettingType other = (AbstractSSRSettingType) obj;
		if (enabled != other.enabled)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
