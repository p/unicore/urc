/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out;

import java.io.Serializable;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IStatus;

import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.Stage;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;

/**
 * @author demuth
 * 
 */
public class StageOut extends Stage implements Serializable, IStageOut {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7996164608014419183L;

	/**
	 * overwrite files at destination?
	 */
	private boolean overwriteFiles;

	/**
	 * binary content?
	 */
	private boolean binaryContent;

	public StageOut(IGridBeanParameter gridBeanParameter,
			IFileParameterValue value) {
		setGridBeanParameter(gridBeanParameter);
		QName type = GridBeanUtils.getType(value);
		if (value != null && type != null) {
			setValue(value);
		}
	}

	public void dispose() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out.IStageOut#
	 * getSourceDisplayName()
	 */
	public String getNameInWorkingDir() {
		try {
			return getValue().getSource().getDisplayedString();
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to display the source of this stage out.", e);
			return "";
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageOut#isBinaryContent
	 * ()
	 */
	public boolean isBinaryContent() {
		return binaryContent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageOut#isOverwriteFiles
	 * ()
	 */
	public boolean isOverwriteFiles() {
		return overwriteFiles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageOut#setBinaryContent
	 * (boolean)
	 */
	public void setBinaryContent(boolean binaryContent) {
		this.binaryContent = binaryContent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageOut#setOverwriteFiles
	 * (boolean)
	 */
	public void setOverwriteFiles(boolean overwriteFiles) {
		this.overwriteFiles = overwriteFiles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageIn#setTargetValue
	 * (com.intel.gpe.gridbeans.IFileParameterValue)
	 */
	@Override
	public void setValue(IFileParameterValue value) {
		super.setValue(value);
		if (value != null) {
			setId(value.getUniqueId());
			if (GridBeanUtils.getType(value) != null) {
				setType(GridBeanUtils.getType(value));
			}
		}

	}

}
