package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import de.fzj.unicore.rcp.servicebrowser.nodes.INodeCacheListener;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeData;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;

public abstract class AbstractNodeCacheListener implements INodeCacheListener,
		PropertyChangeListener {

	protected Set<URI> monitoredNodes = new HashSet<URI>();

	public void nodeDataAdded(INodeData nodeData) {
	
			if (!monitoredNodes.contains(nodeData.getURI())
					&& isOfInterest(nodeData)) {
				nodeData.addPropertyChangeListener(this);
				monitoredNodes.add(nodeData.getURI());
				update(nodeData);
			}
	}
	
	protected abstract boolean isOfInterest(INodeData n);

	public void nodeDataRemoved(INodeData nodeData) {
		nodeData.removePropertyChangeListener(this);
		monitoredNodes.remove(nodeData.getURI());
	}

	public void propertyChange(PropertyChangeEvent evt) {
		INodeData data = null;
		if (evt.getSource() instanceof Node) {
			data = ((Node) evt.getSource()).getData();
		} else if (evt.getSource() instanceof INodeData) {
			data = (INodeData) evt.getSource();
		}
		if (data == null || !monitoredNodes.contains(data.getURI())) {
			return;
		}
		if (!WSRFNode.PROPERTY_CACHED_RPS.equals(evt.getPropertyName())) {
			return;
		}
		update(data);
	}

	protected abstract void update(INodeData node); 
	
}
