package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types;

import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDescriptionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobIdentificationType;


import com.intel.gpe.clients.api.jsdl.JSDLElement;
import com.intel.gpe.gridbeans.jsdl.JavaToXBeanElementConverter;

public class ScheduleTypeToXBeanConverter implements JavaToXBeanElementConverter{

	public void convertToXBean(JobDefinitionType jobDef, JSDLElement element) {
		ScheduleType scheduledTime = (ScheduleType) element;
		JobIdentificationType id = getOrCreateApplication(jobDef);
		
		id.addJobAnnotation("notBefore: " + scheduledTime.getScheduledTime());

	}

	protected JobIdentificationType getOrCreateApplication(JobDefinitionType jobDef) {
		JobDescriptionType jobDescr = jobDef.getJobDescription();
		if (jobDescr == null) {
			jobDescr = jobDef.addNewJobDescription();
		}
		JobIdentificationType id = jobDescr.getJobIdentification();
		if (id == null) {
			id = jobDescr.addNewJobIdentification();
		}
		return id;
	}

	public Class<ScheduleType> getSourceType() {
		return ScheduleType.class;
	}

}
