package de.fzj.unicore.rcp.gpe4eclipse.extensions;

import org.eclipse.core.runtime.IAdaptable;

import com.intel.gpe.gridbeans.parameters.IFileParameterValue;

public abstract class AbstractStageInTypeExtension extends
		AbstractStageTypeExtension {
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #updateParameterValue(org.eclipse.core.runtime.IAdaptable,
	 * com.intel.gpe.gridbeans.parameters.IFileParameterValue)
	 */
	public void updateParameterValue(IAdaptable adaptable,
			IFileParameterValue oldValue, IFileParameterValue newValue) {

	}
}
