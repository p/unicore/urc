/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions;

import java.io.File;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.intel.gpe.clients.all.gridbeans.InternalGridBean;
import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.clients.common.transfers.GridFileAddress;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.gridbeans.parameters.FileParameterValue;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

import de.fzj.unicore.rcp.common.adapters.InputFileContainerAdapter;
import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in.StageInTypeLocalCellEditor;

/**
 * @author demuth
 * 
 */
public class StageInTypeLocalExtension extends AbstractStageInTypeExtension {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #attachToParameterValue
	 * (de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GridBeanActivity,
	 * com.intel.gpe.gridbeans.IFileParameterValue)
	 */
	public IFileParameterValue attachToParameterValue(IAdaptable adaptable,
			IFileParameterValue oldValue) {

		IFileParameterValue input;

		IGridFileAddress source = null;

		if (oldValue == null) {
			source = new GridFileAddress(null, "", "",
					ProtocolConstants.LOCAL_PROTOCOL, false);
			GridFileAddress target = new GridFileAddress(null, "", "",
					ProtocolConstants.JOB_WORKING_DIR_PROTOCOL, false);
			input = new FileParameterValue(source, target, true);
		} else {
			if (oldValue.getSource() == null
					|| !ProtocolConstants.LOCAL_PROTOCOL.equals(oldValue
							.getSource().getProtocol())) {
				// try to restore the original filename/location as set by a
				// "fresh" GridBean
				// doing this, we face the following problems:
				// 1. We don't know the GridBeanParameter, so we're having a
				// hard time finding the value in the "fresh" GridBean
				// 2. We don't know the version of the GridBean, only its name
				// 3. When dealing with a workflow, we should prepend the job
				// name to the fresh file name

				// the following code is very hackish and can be easily broken,
				// but at the moment of writing, it works :)
				// anyway, its a best-effort service
				if (adaptable != null) {
					try {
						IGridBean gb = (IGridBean) adaptable
								.getAdapter(IGridBean.class);
						if (gb != null) {
							for (IGridBeanParameter param : gb
									.getInputParameters()) {

								// deal with 1. problem
								// try to find the parameter that corresponds to
								// our file, this might fail in which case we're
								// out of luck
								if (gb.get(param.getName()) == oldValue) {
									// deal with 2nd Problem: try to find the
									// latest version of the given GridBean
									GridBeanInfo gbInfo = new GridBeanInfo(
											gb.getName(), null);
									GridBeanInfo completeWithVersion = GPEActivator
											.getDefault().getGridBeanRegistry()
											.getNewerGridBean(gbInfo);
									InternalGridBean internal = GPEActivator
											.getDefault().getGridBeanRegistry()
											.getGridBean(completeWithVersion);
									IGridBean fresh = internal.getJobInstance()
											.getModel();
									IFileParameterValue value = (IFileParameterValue) fresh
											.get(param.getName());
									if (value != null) {
										source = value.getSource();
										// deal with 3rd problem
										GridBeanContext context = (GridBeanContext) gb
												.get(GPE4EclipseConstants.CONTEXT);
										if (context != null
												&& context.isPartOfWorkflow()) {
											String jobName = (String) gb
													.get(IGridBeanModel.JOBNAME);
											if (jobName != null) {
												String newRelative = jobName
														+ "_"
														+ source.getRelativePath();
												source.setRelativePath(newRelative);
											}
										}
									}
									break;
								}
							}

						}
					} catch (Exception e) {
						GPEActivator.log(Status.ERROR, "Error while initializing file transfer from localhost: "+e.getMessage(), e);
						// do nothing
					}
				}
			}
			if (source == null) {
				source = new GridFileAddress(null, "", "",
						ProtocolConstants.LOCAL_PROTOCOL, false);
			}

			try {
				input = oldValue.clone();
				input.setSource(source);

			} catch (Exception e) {
				GPEActivator
						.log(IStatus.ERROR,
								"Could not clone file address for creating local input file.",
								e);
				input = oldValue;
			}
		}

		return input;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getCellEditor()
	 */
	public CellEditor getCellEditor(Composite parent, IStage stage,
			IAdaptable adaptable) {
		String parentDirPath = null;
		try {
			InputFileContainerAdapter act = (InputFileContainerAdapter) adaptable
					.getAdapter(InputFileContainerAdapter.class);
			parentDirPath = act.getPath().toOSString();
			if (!parentDirPath.endsWith(File.separator)) {
				parentDirPath += File.separator;
			}

		} catch (Exception e) {
			// ignore
		}
		boolean isFileSet = GridBeanParameterType.FILE_SET.equals(stage
				.getGridBeanParameter().getType());
		return new StageInTypeLocalCellEditor(parent, parentDirPath, isFileSet,
				isFileSet);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getPriority(de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext)
	 */
	public int getPriority(GridBeanContext context) {
		return 10;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getType()
	 */
	public QName getType() {
		return ProtocolConstants.LOCAL_QNAME;
	}

}
