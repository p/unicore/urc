package de.fzj.unicore.rcp.gpe4eclipse.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;

public class DeleteGridBeanHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		List<GridBeanInfo> toDelete = new ArrayList<GridBeanInfo>();
		if(selection instanceof StructuredSelection)
		{
			StructuredSelection struct = (StructuredSelection) selection;
			for(Object o : struct.toArray())
			{
				if(o instanceof IAdaptable)
				{
					IAdaptable adaptable = (IAdaptable) o;
					GridBeanInfo adapter = (GridBeanInfo) adaptable.getAdapter(GridBeanInfo.class);
					if(adapter != null)
					{
						toDelete.add(adapter);
					}
				}
			}
		}
		if(toDelete.size() > 0)
		{
			String msg = "Are you sure you would like to delete the selected " +
					"application GUIs?\n This cannot be undone!";
			boolean confirmed = MessageDialog.openConfirm(HandlerUtil.getActiveShell(event), "Delete selected application GUIs?", msg);
			if(confirmed) GPEActivator.getDefault().getGridBeanRegistry().
			deleteGridBeans(toDelete.toArray(new GridBeanInfo[toDelete.size()]));
		}
		return null;
	}

}
