/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnv;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnvArgument;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnvDataControler;

/**
 * @author sholl
 * 
 */
public class ExecutionEnvironmentListener implements PropertyChangeListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @seejava.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	public ExecutionEnvironmentListener(ExecEnv selectedExecEnv,
			ExecEnvDataControler controler, ExecEnvDetailsViewer execEnvpanel) {
	}

	public void propertyChange(PropertyChangeEvent arg0) {
		if (arg0.getNewValue() instanceof ExecEnvArgument<?>) {
			validateValues((ExecEnvArgument<?>) arg0.getNewValue(),
					arg0.getPropertyName());
		}

	}

	private void validateValues(ExecEnvArgument<?> execEnvArgument,
			String propertyName) {
		// if (listenToModel) {
		// listenToModel = false;
		// try {
		//
		// List<ExecEnvArgument<?>> arguments = selectedExecEnv
		// .getArguments();
		// for (ExecEnvArgument<?> arg : arguments) {
		//
		// if (arg.isEnabled()) {
		//
		// QName key = arg.getName();
		//
		// SWTDataControl control = null;
		// try {
		// control = (SWTDataControl) controler
		// .getControl(key);
		// } catch (DataSetException e) {
		// 
		
		// }
		// if (control != null) {
		// StringBuffer buffer = new StringBuffer("");
		// control.getValueValidator().isValid(
		// arg.getValue(), buffer);
		// if ((buffer.length() > 0) ) {
		// ((ExecEnvDetailsViewer) execEnvpanel)
		// .setProblemMessage(buffer.substring(
		// 0, buffer.length()));
		// break;
		// } else {
		// ((ExecEnvDetailsViewer) execEnvpanel)
		// .setProblemMessage(null);
		// }
		// }
		// }
		// }
		// } finally {
		// listenToModel = true;
		// }
		// }
	}
}
