/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.namespace.QName;

import com.intel.gpe.clients.api.jsdl.OSType;
import com.intel.gpe.clients.api.jsdl.OperatingSystemRequirementsType;
import com.intel.gpe.clients.api.jsdl.ProcessorType;
import com.intel.gpe.clients.api.jsdl.RangeValueType;
import com.intel.gpe.gridbeans.parameters.CPUArchitecture;
import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;
import com.intel.gpe.gridbeans.parameters.SiteSpecificResourceRequirement;

import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IResourceRequirementExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnv;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnvUtils;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecutionEnvironmentRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.ReservationIdType;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.ScheduleType;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.StringSSRSettingType;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.UserEmailType;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types.XLoginType;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.DateTimeRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.EnumRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.RangeValueTypeRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.StringRequirementValue;

/**
 * Provides a factory for the standard JSDL resource requirements.
 * Crucial bits and pieces for each requirement are 
 * 1. Name
 * 2. Description
 * 3. The {@link ResourceRequirement#loadFrom(ResourcesParameterValue) method 
 *    which is used to obtain the current requirement value from the
 *    {@link ResourcesParameterValue} which is stored in the Grid Bean model
 * 4. The {@link ResourceRequirement#writeTo(ResourcesParameterValue) method 
 *    which is used to write the current requirement value back to the
 *    {@link ResourcesParameterValue} which is stored in the Grid Bean model  
 * 
 * @author Christian Hohmann
 * @author bdemuth
 * 
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class StandardRequirements implements
IResourceRequirementExtensionPoint, RequirementConstants {

	private ResourceRequirement createCPUArchitecture() {
		
		EnumRequirementValue<?> value1 = new EnumRequirementValue<Object>(
				ProcessorType.class, 0);
		ResourceRequirement archReq = new ResourceRequirement(false,
				cpuArchitectureRequirement, value1, NO_ADD_INFORMATION) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -140946455297665848L;

			@Override
			public void loadFrom(ResourcesParameterValue value) {
				CPUArchitecture element = value.getCpuArchitecture();
				if (element == null) {
					return;
				}
				setEnabled(element.isEnabled());
				((EnumRequirementValue<ProcessorType>) getValue()).setSelectedElement(element
						.getProcessor());
			}

			@Override
			public void writeTo(ResourcesParameterValue value) {
				ProcessorType proc = (ProcessorType) ((EnumRequirementValue) getValue())
						.getSelectedElement();
				CPUArchitecture arch = new CPUArchitecture();
				arch.setProcessor(proc);
				arch.setEnabled(isEnabled());
				value.setCpuArchitecture(arch);
			}
		};
		archReq.setDescription("Required processor architecture");
		return archReq;
	}

	private ResourceRequirement createCPUPerNode() {
		RangeValueType cPUPerNodeValidValues = new RangeValueType(1, 1,
				BILLION);

		RangeValueTypeRequirementValue value4 = new RangeValueTypeRequirementValue(
				cPUPerNodeValidValues);
		ResourceRequirement cpuPerNodeReq = new ResourceRequirement(false,
				cpuPerNodeRequirement, value4, NO_ADD_INFORMATION) {

			private static final long serialVersionUID = -1836643757668294978L;

			@Override
			public void loadFrom(ResourcesParameterValue value) {
				RangeValueType element = value.getCPUPerNode();
				if (element == null) {
					return;
				}
				element = element.clone();
				setEnabled(element.isEnabled());
				((RangeValueTypeRequirementValue) getValue())
				.setChosenValue(element);
			}

			@Override
			public void writeTo(ResourcesParameterValue value) {
				RangeValueType element = ((RangeValueTypeRequirementValue) getValue())
						.getChosenValue();
				element.setEnabled(isEnabled());
				value.setCPUPerNode(element.clone());
			}
		};
		cpuPerNodeReq.getClashes().add(totalCpuRequirement);
		cpuPerNodeReq.setDescription("Number of CPUs per node");
		return cpuPerNodeReq;
	}

	private ResourceRequirement createCPUSpeed() {
		
		RangeValueType cPUSpeedPerNodeValidValues = new RangeValueType(GIGA, 1,
				TERA);

		RangeValueTypeRequirementValue value5 = new RangeValueTypeRequirementValue(
				cPUSpeedPerNodeValidValues, null, UNIT_HZ, UNIT_MEGA_HZ,
				UNITS_FREQUENCY);
		ResourceRequirement cpuSpeedReq = new ResourceRequirement(false,
				cpuSpeedPerNodeRequirement, value5, NO_ADD_INFORMATION) {

			private static final long serialVersionUID = -5151335044410497790L;

			@Override
			public void loadFrom(ResourcesParameterValue value) {
				RangeValueType element = value.getCPUSpeedPerNode();
				if (element == null) {
					return;
				}
				element = element.clone();
				setEnabled(element.isEnabled());
				((RangeValueTypeRequirementValue) getValue())
				.setChosenValue(element);
			}

			@Override
			public void writeTo(ResourcesParameterValue value) {
				RangeValueType element = ((RangeValueTypeRequirementValue) getValue())
						.getChosenValue();
				element.setEnabled(isEnabled());
				value.setCPUSpeedPerNode(element.clone());
			}
		};
		cpuSpeedReq.setDescription("Minimal clock rate of the processors");
		return cpuSpeedReq;
	}

	private ResourceRequirement createOSType() {
		
		Integer[] selectable = new Integer[24];
		for(int i = 0; i < selectable.length; i++)
		{
			selectable[i] = i;
		}
		EnumRequirementValue<?> value0 = new EnumRequirementValue<Object>(OSType.class, null, selectable, "toString", "fromString");
		ResourceRequirement osReq = new ResourceRequirement(false,
				osRequirement, value0, NO_ADD_INFORMATION) {
			
			private static final long serialVersionUID = -8302282111123727224L;

			@Override
			public void loadFrom(ResourcesParameterValue value) {
				OperatingSystemRequirementsType element = value.getOsType();
				if (element == null) {
					return;
				}
				setEnabled(element.isEnabled());
				OSType os = element.getOSType();
				((EnumRequirementValue<OSType>) getValue()).setSelectedElement(os);
			}


			@Override
			public void writeTo(ResourcesParameterValue value) {
				OSType selected = (OSType) ((EnumRequirementValue) getValue())
						.getSelectedElement();
				OperatingSystemRequirementsType osType = new OperatingSystemRequirementsType(
						selected, "");
				osType.setEnabled(isEnabled());
				value.setOsType(osType);
			}
		};
		osReq.setDescription("Installed operating system");
		return osReq;
	}

	private ResourceRequirement createRAMPerNode() {

		RangeValueType rAMPerNodeValidValues = new RangeValueType(GIGA, 1,
				TERA);

		RangeValueTypeRequirementValue value6 = new RangeValueTypeRequirementValue(
				rAMPerNodeValidValues, null, UNIT_BYTE, UNIT_MEGA_BYTE,
				UNITS_DATA);
		ResourceRequirement ramPerNodeReq = new ResourceRequirement(false,
				ramPerNodeRequirement, value6, NO_ADD_INFORMATION) {

			private static final long serialVersionUID = -3353791987689954929L;

			@Override
			public void loadFrom(ResourcesParameterValue value) {
				RangeValueType element = value.getRAMPerNode();
				if (element == null) {
					return;
				}
				element = element.clone();
				setEnabled(element.isEnabled());
				((RangeValueTypeRequirementValue) getValue())
				.setChosenValue(element);
			}

			@Override
			public void writeTo(ResourcesParameterValue value) {
				RangeValueType element = ((RangeValueTypeRequirementValue) getValue())
						.getChosenValue();
				element.setEnabled(isEnabled());
				value.setRAMPerNode(element.clone());
			}
		};
		ramPerNodeReq
		.setDescription("Minimal amount of physical memory on each computing node");
		return ramPerNodeReq;
	}

	private ResourceRequirement createTimePerNode() {
		
		RangeValueType validValues = new RangeValueType(60 * 60.0, 1.0,
				365 * 24 * 60 * 60.0);

		RangeValueTypeRequirementValue value = new RangeValueTypeRequirementValue(
				validValues, null, UNIT_SECOND, UNIT_MINUTE, UNITS_TIME);
		ResourceRequirement req = new ResourceRequirement(false,
				timePerCPURequirement, value, NO_ADD_INFORMATION) {
		
			private static final long serialVersionUID = -7364650920572991493L;

			@Override
			public void loadFrom(ResourcesParameterValue value) {
				RangeValueType element = value.getCPUTimePerNode();
				if (element == null) {
					return;
				}
				element = element.clone();
				setEnabled(element.isEnabled());
				((RangeValueTypeRequirementValue) getValue())
				.setChosenValue(element);
			}

			@Override
			public void writeTo(ResourcesParameterValue value) {
				RangeValueType element = ((RangeValueTypeRequirementValue) getValue())
						.getChosenValue();
				element.setEnabled(isEnabled());
				value.setCPUTimePerNode(element.clone());
			}
		};
		req.setDescription("Required time on each computing node");
		return req;
	}


	private ResourceRequirement startJobNotBefore() {
		
		DateTimeRequirementValue value = new DateTimeRequirementValue("");		
		ResourceRequirement resReq = new ResourceRequirement(false, jobSchedulingRequirement, value, NO_ADD_INFORMATION) {			

			
			private static final long serialVersionUID = -3353791987689954929L;

			@Override
			public void loadFrom(ResourcesParameterValue value) {

				if (value.getSiteSpecificRequirements() == null) {
					return;
				}
				for (SiteSpecificResourceRequirement req : value
						.getSiteSpecificRequirements()) {
					if (req.getName().equals(getRequirementName())) {
						//TODO: create Date/Schedule type
						ScheduleType type = (ScheduleType) req.getRequirement();
						DateTimeRequirementValue startingTime = (DateTimeRequirementValue) getValue();
						setEnabled(type.isEnabled());
						startingTime.setValue(type.getScheduledTime());
						break;

					}
				}
			}

			@Override
			public void writeTo(ResourcesParameterValue value) {
				DateTimeRequirementValue startingTime = (DateTimeRequirementValue) getValue();

				ScheduleType type = new ScheduleType(startingTime.getValue());
				type.setEnabled(isEnabled());
				SiteSpecificResourceRequirement req = new SiteSpecificResourceRequirement(
						getRequirementName(), type);

				value.removeSiteSpecificRequirement(req);
				value.addSiteSpecificRequirement(req);
			}


		};
		resReq.setDescription("Scheduled start time");
		return resReq;
	}		


	private ResourceRequirement createTotalCPUs() {

		RangeValueType totalCPUValidValues = new RangeValueType(1, 1,
				BILLION);

		RangeValueTypeRequirementValue value2 = new RangeValueTypeRequirementValue(
				totalCPUValidValues);
		ResourceRequirement totalCPUReq = new ResourceRequirement(false,
				totalCpuRequirement, value2, NO_ADD_INFORMATION) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 5889168275256469367L;

			@Override
			public void loadFrom(ResourcesParameterValue value) {
				RangeValueType element = value.getTotalCPUs();
				if (element == null) {
					return;
				}
				element = element.clone();
				setEnabled(element.isEnabled());
				((RangeValueTypeRequirementValue) getValue())
				.setChosenValue(element);
			}

			@Override
			public void writeTo(ResourcesParameterValue value) {
				RangeValueType element = ((RangeValueTypeRequirementValue) getValue())
						.getChosenValue();
				element.setEnabled(isEnabled());
				value.setTotalCPUs(element.clone());
			}
		};
		totalCPUReq.getClashes().add(cpuPerNodeRequirement);
		totalCPUReq.getClashes().add(totalResourcesRequirement);
		totalCPUReq
		.setDescription("Total number of CPUs, distributed over all nodes");
		return totalCPUReq;
	}

	private ResourceRequirement createTotalNodes() {

		RangeValueType totalNodesValidValues = new RangeValueType(1, 1,
				BILLION);

		RangeValueTypeRequirementValue value3 = new RangeValueTypeRequirementValue(
				totalNodesValidValues);
		ResourceRequirement totalNodesReq = new ResourceRequirement(false,
				totalResourcesRequirement, value3, NO_ADD_INFORMATION) {

			private static final long serialVersionUID = 2212916559000953743L;

			@Override
			public void loadFrom(ResourcesParameterValue value) {
				RangeValueType element = value.getTotalResources();
				if (element == null) {
					return;
				}
				element = element.clone();
				setEnabled(element.isEnabled());
				((RangeValueTypeRequirementValue) getValue())
				.setChosenValue(element);
			}

			@Override
			public void writeTo(ResourcesParameterValue value) {
				RangeValueType element = ((RangeValueTypeRequirementValue) getValue())
						.getChosenValue();
				element.setEnabled(isEnabled());
				value.setTotalResources(element.clone());
			}
		};
		totalNodesReq.getClashes().add(totalCpuRequirement);
		totalNodesReq.setDescription("Total number of computing nodes");
		return totalNodesReq;
	}

	private ResourceRequirement createUserEmail() {
		StringRequirementValue value1 = new StringRequirementValue("");
		ResourceRequirement archReq = new ResourceRequirement(false,
				EMAIL_REQUIREMENT, value1, NO_ADD_INFORMATION) {

			private static final long serialVersionUID = 4738483542599412135L;

			@Override
			public void loadFrom(ResourcesParameterValue value) {
				if (value.getSiteSpecificRequirements() == null) {
					return;
				}
				for (SiteSpecificResourceRequirement req : value
						.getSiteSpecificRequirements()) {
					if (req.getName().equals(getRequirementName())) {
						UserEmailType type = (UserEmailType) req
								.getRequirement();
						StringRequirementValue reqValue = (StringRequirementValue) getValue();
						setEnabled(type.isEnabled());
						reqValue.setValue(type.getEmailAddress());
						break;
					}
				}
			}

			@Override
			public void writeTo(ResourcesParameterValue value) {
				StringRequirementValue reqValue = (StringRequirementValue) getValue();

				UserEmailType type = new UserEmailType(reqValue.getValue());
				type.setEnabled(isEnabled());
				SiteSpecificResourceRequirement req = new SiteSpecificResourceRequirement(
						getRequirementName(), type);

				value.removeSiteSpecificRequirement(req);
				value.addSiteSpecificRequirement(req);
			}
		};
		archReq.setDescription("Email address for notification of sucessful/failed jobs");
		return archReq;
	}

	private ResourceRequirement createXLogin() {

		StringRequirementValue value1 = new StringRequirementValue("");
		ResourceRequirement archReq = new ResourceRequirement(false,
				XLOGIN_REQUIREMENT, value1, NO_ADD_INFORMATION) {


			private static final long serialVersionUID = -4138440633062388614L;

			@Override
			public void loadFrom(ResourcesParameterValue value) {
				if (value.getSiteSpecificRequirements() == null) {
					return;
				}
				for (SiteSpecificResourceRequirement req : value
						.getSiteSpecificRequirements()) {
					if (req.getName().equals(getRequirementName())) {
						XLoginType type = (XLoginType) req.getRequirement();
						StringRequirementValue login = (StringRequirementValue) getValue();
						setEnabled(type.isEnabled());
						login.setValue(type.getLogin());
						break;
					}
				}
			}

			@Override
			public void writeTo(ResourcesParameterValue value) {
				StringRequirementValue login = (StringRequirementValue) getValue();

				XLoginType type = new XLoginType(login.getValue());
				type.setEnabled(isEnabled());
				SiteSpecificResourceRequirement req = new SiteSpecificResourceRequirement(
						getRequirementName(), type);

				value.removeSiteSpecificRequirement(req);
				value.addSiteSpecificRequirement(req);
			}
		};
		archReq.setDescription("Login name for the selected resource");
		return archReq;
	}

	private ResourceRequirement createReservationId() {
		StringRequirementValue strReq = new StringRequirementValue("");
		ResourceRequirement resReq = new ResourceRequirement(false,
				RESERVATION_REQUIREMENT, strReq,
				"Reservation ID, must be available at the target site") {


			/**
					 * 
					 */
			private static final long serialVersionUID = 798676912376877L;

			@Override
			public void writeTo(ResourcesParameterValue value) {
				StringRequirementValue reservation = (StringRequirementValue) getValue();
				ReservationIdType type = new ReservationIdType(
						reservation.getValue());
				type.setEnabled(isEnabled());
				SiteSpecificResourceRequirement req = new SiteSpecificResourceRequirement(
						getRequirementName(), type);

				value.removeSiteSpecificRequirement(req);
				value.addSiteSpecificRequirement(req);
			}

			@Override
			public void loadFrom(ResourcesParameterValue value) {
				if (value.getSiteSpecificRequirements() == null) {
					return;
				}
				for (SiteSpecificResourceRequirement req : value
						.getSiteSpecificRequirements()) {
					if (req.getName().equals(getRequirementName())) {
						ReservationIdType type = (ReservationIdType) req
								.getRequirement();
						StringRequirementValue reservation = (StringRequirementValue) getValue();
						reservation.setValue(type.getReservationId());
						setEnabled(type.isEnabled());
						break;
					}
				}
			}
		};

		return resReq;
	}

	private List<ResourceRequirement> getExecEnvRequirement() {
		List<ResourceRequirement> requirementList = new ArrayList<ResourceRequirement>();
		ExecutionEnvironmentRegistry registry = ExecutionEnvironmentRegistry.getDefaultInstance();
		List<ExecEnv> available = registry.getUnifiedExecutionEnvironments();
		ResourceRequirement req = ExecEnvUtils.createResourceRequirement(available);
		if(req != null)
		{
			requirementList.add(req);
		}
		return requirementList;
	}

	private List<ResourceRequirement> getAllSSRRequirements(ResourcesParameterValue selectedResources) {
		ResourcesRegistry registry = ResourcesRegistry.getDefaultInstance();
		Collection<AvailableResource> available = registry.getUnifiedResources();
		List<ResourceRequirement> requirementList = new ArrayList<ResourceRequirement>();
		Set<QName> found = new HashSet<QName>();
		if (available != null)
		{
			for (AvailableResource siteResource: available) {
				if (siteResource == null)
					continue;
				SSRSetting setting = SSRSetting.fromSiteProperties(siteResource);
				if (setting == null)
					continue;
				requirementList.add(setting);
				found.add(setting.getRequirementName());
			}
		}

		// now deal with the problem of old persisted requirements that are
		// enabled but NOT AVAILABLE ANYMORE
		if(selectedResources.getSiteSpecificRequirements() != null)
		{
			for(SiteSpecificResourceRequirement res : selectedResources.getSiteSpecificRequirements())
			{
				if(res.getRequirement() instanceof StringSSRSettingType 
						&& !found.contains(res.getName()))
				{
					StringSSRSettingType type = (StringSSRSettingType) res.getRequirement();
					if(type.isEnabled())
					{
						SSRSetting setting = new SSRSetting();
						setting.setRequirementName(res.getName());
						setting.setValue(new StringRequirementValue(type.getValue()));
						setting.setDescription("WARNING: Resource that does not seem to be available anymore!");
						setting.setEnabled(true);
						requirementList.add(setting);
					}
				}
			}
		}

		return requirementList;
	}	

	public IRequirementFactory getResourceRequirementFactory() {
		return new IRequirementFactory() {

			public ResourceRequirement[] createResourceRequirements(ResourcesParameterValue selectedResources) {
				List<ResourceRequirement> resourceRequirements = new ArrayList<ResourceRequirement>();
				resourceRequirements.add(createTotalCPUs());
				resourceRequirements.add(createTotalNodes());
				resourceRequirements.add(createCPUPerNode());
				resourceRequirements.add(createCPUSpeed());
				resourceRequirements.add(createRAMPerNode());
				resourceRequirements.add(createTimePerNode());
				resourceRequirements.add(createOSType());
				resourceRequirements.add(createCPUArchitecture());
				resourceRequirements.add(createXLogin());
				resourceRequirements.add(createUserEmail());
				resourceRequirements.add(startJobNotBefore());
				resourceRequirements.add(createReservationId());
				resourceRequirements.addAll(getExecEnvRequirement());
				resourceRequirements.addAll(getAllSSRRequirements(selectedResources));
				for(ResourceRequirement r : resourceRequirements)
				{
					r.loadFrom(selectedResources);
				}
				return resourceRequirements.toArray(new ResourceRequirement[0]);
			}

		};
	}

}