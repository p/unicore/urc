/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.security.DigestInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarFile;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IRegistryEventListener;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.Bundle;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.intel.gpe.clients.all.gridbeans.InternalGridBean;
import com.intel.gpe.clients.all.gridbeans.InternalGridBeanImpl;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.util.collections.CollectionUtil;
import com.intel.gpe.util.defaults.preferences.CommonKeys;
import com.intel.gpe.util.defaults.preferences.IPreferences;
import com.thoughtworks.xstream.XStream;

import de.fzj.unicore.rcp.common.utils.ArrayUtils;
import de.fzj.unicore.rcp.common.utils.DelegatingExtensionURLClassloader;
import de.fzj.unicore.rcp.common.utils.DirectoryListener;
import de.fzj.unicore.rcp.common.utils.DirectoryWatcher;
import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.exceptions.InvalidGridBeanException;

/**
 * @author Valentina Huber
 */
public class GridBeanRegistry implements DirectoryListener {

	final static long updateInterval = 20000;

	protected Map<URL, DelegatingExtensionURLClassloader> existingClassloaders = new ConcurrentHashMap<URL, DelegatingExtensionURLClassloader>();
	protected Map<URL, Long> existingChecksums = new ConcurrentHashMap<URL, Long>();
	List<GridBeanInfo> gridBeanInfos;
	Set<GridBeanInfo> oldGridBeanInfos;
	Map<GridBeanInfo, GridBeanEntry> gridBeanEntries;
	private Object classLoaderLock = new Object(); 
	private Object gridBeanDataLock = new Object(); 
	private DirectoryWatcher directoryWatcher;
	private Map<String,List<String>> extensionInfo;

	// The main directory holding gridbeans; new gridbeans are downloaded here
	File gridBeanDir;

	// Additional gridbeans might be loaded from a bunch of directories 
	File[] alternativeGridBeanDirs;

	List<IGridBeanRegistryListener> listeners = new ArrayList<IGridBeanRegistryListener>();

	public GridBeanRegistry(File gridBeanDir) throws Exception {
		this(gridBeanDir,(File[])null);
	}
	/**
	 * Creates a registry for gridbeans.
	 * 
	 */
	public GridBeanRegistry(File gridBeanDir, File... alternativeGridBeanDirs) throws Exception {

		if (gridBeanDir == null || !gridBeanDir.exists()) {
			throw new Exception("Directory " + gridBeanDir + "does not exist.");
		}
		this.gridBeanDir = gridBeanDir;
		this.alternativeGridBeanDirs = alternativeGridBeanDirs;
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		loadExtensionInfo();
		reloadExtensions();
		File[] dirs = ArrayUtils.concat(gridBeanDir,alternativeGridBeanDirs);

		registry.addListener(new IRegistryEventListener() {

			@Override
			public void removed(IExtensionPoint[] extensionPoints)
			{
				reloadExtensionsAsync();
			}

			@Override
			public void removed(IExtension[] extensions)
			{
				reloadExtensionsAsync();
			}

			@Override
			public void added(IExtensionPoint[] extensionPoints)
			{
				reloadExtensionsAsync();
			}

			@Override
			public void added(IExtension[] extensions)
			{
				reloadExtensionsAsync();
			}
		}, GPE4EclipseConstants.EXTENSION_POINT_GRID_BEANS);

		directoryWatcher = new DirectoryWatcher("Checking application directories", updateInterval, dirs);
		directoryWatcher.addDirectoryListener(this);
		directoryWatcher.schedule(updateInterval);

	}


	private void reloadExtensionsAsync()
	{
		Display d = PlatformUI.getWorkbench().getDisplay();
		d.asyncExec(new Runnable() {

			@Override
			public void run()
			{
				reloadExtensions();
			}
		});
	}

	private void reloadExtensions()
	{
		loadExtensions();
		saveExtensionInfo();
		reloadGridBeans();
	}



	/**
	 * Adds the specified InternalGridBean to the list of gridbeans.
	 * 
	 * @param gridbean
	 *            InternalGridBean
	 * @param path
	 *            path to the gridbean
	 */
	private void addGridBean(InternalGridBean gridbean, String path, 
			List<GridBeanInfo> gridBeanInfos, 
			Set<GridBeanInfo> oldGridBeanInfos, 
			Map<GridBeanInfo,GridBeanEntry> gridBeanEntries) {
		GridBeanInfo info = new GridBeanInfo(gridbean.getName(),
				gridbean.getVersion());
		if (!gridBeanEntries.containsKey(info)) {
			URL iconUrl = null;
			IGridBean gb = null;
			try {
				gb = gridbean.getJobInstance().getModel();
			} catch (Throwable t) {
				GPEActivator.log(IStatus.WARNING,
						"Found invalid application GUI " + info.toString()
						+ ".", new InvalidGridBeanException(t,
								new File(path)));
				return;
			}

			int index = 0;
			;
			for (GridBeanInfo existing : gridBeanInfos) {
				if (existing.getGridBeanName().compareToIgnoreCase(
						info.getGridBeanName()) < 0) {
					index++;
				} else {
					break;
				}
			}
			if (gridBeanInfos.size() > index) {
				GridBeanInfo prev = gridBeanInfos.get(index);
				if (prev.getGridBeanName().equalsIgnoreCase(
						info.getGridBeanName())) {
					// try to find out which one is newer
					int sign = prev.getGridBeanVersion().compareTo(
							info.getGridBeanVersion());
					if (sign == 0) {
						// same version => don't add
						return;
					} else if (sign > 0) {
						// we've found an old gridbean version
						oldGridBeanInfos.add(info);
					} else {
						// we need to replace the previous gridbean with the
						// new one that we've just found
						gridBeanInfos.remove(index);
						oldGridBeanInfos.add(prev);
						gridBeanInfos.add(index, info);
					}

				} else {
					gridBeanInfos.add(index, info);
				}
			} else {
				gridBeanInfos.add(index, info);
			}

			iconUrl = gb.getIconURL();

			if (iconUrl == null) {
				GPEActivator.log(
						IStatus.WARNING,
						"Could not load icon for application "
						+ info.getGridBeanName());
			}

			gridBeanEntries.put(info,
					new GridBeanEntry(info, path, iconUrl));

			// set path to GridBean correctly so that the GridBeanProvider
			// can find the gridbean
			// TODO maybe the GridBeanProvider should be pluggable into the
			// GridBeanClient, so we can use our own implementation?
			GPEActivator
			.getDefault()
			.getGPEPreferences()
			.set(CommonKeys.COMMON_GRIDBEANS
					.getKey(info.toString()),
					path);
		}

	}

	public void addListener(IGridBeanRegistryListener l) {
		listeners.add(l);
	}

	/**
	 * TODO: This looks like abusing an internal checksum API for zip files.
	 * Maybe {@link DigestInputStream} would be a better solution?
	 * 
	 * @param pathToJar
	 * @return
	 * @throws IOException
	 */
	protected long computeChecksum(String pathToJar) throws IOException {
		// check whether file has changed
		FileInputStream is = new FileInputStream(pathToJar);
		CheckedInputStream check;
		BufferedInputStream in = null;
		try {
			check = new CheckedInputStream(is, new CRC32());
			in = new BufferedInputStream(check);
			while (in.read() != -1) {
				// Read file in completely
			}
		}
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				GPEActivator.log(Status.ERROR, "Error while closing file: "+e.getMessage(), e);
			}
		}
		return check.getChecksum().getValue();
	}

	/**
	 * Returns true if this list contains the GridBean with the specified
	 * GridBeanInfo.
	 * 
	 * @param info
	 * @return true if the specified gridbean is present; false otherwise.
	 */
	public boolean containsGridBean(GridBeanInfo info) {
		return gridBeanEntries.get(info) != null;
	}

	/**
	 * Returns true if this list contains the the specified InternalGridBean.
	 * 
	 * @param info
	 * @return true if the specified gridbean is present; false otherwise.
	 */
	public boolean containsGridBean(InternalGridBean gridbean) {
		GridBeanInfo info = new GridBeanInfo(gridbean.getName(),
				gridbean.getVersion());
		return containsGridBean(info);
	}

	/**
	 * Returns true iff this list contains a GridBean with a version as recent
	 * as the version in the input parameter.
	 * 
	 * @param info
	 * @return true if the specified gridbean is present; false otherwise.
	 */
	public boolean containsNewerGridBean(GridBeanInfo info) {
		return getNewerGridBean(info) != null;
	}



	@Override
	public void contentChanged(File[] added, File[] deleted, File[] modified) {
		boolean gbModifiedOrDeleted = false;
		for(File f : modified)
		{
			try {
				if(existingChecksums.containsKey(f.toURI().toURL()))
				{
					gbModifiedOrDeleted = true;
					break;
				}
			} catch (Exception e) {

			}

		}
		if(!gbModifiedOrDeleted)
		{
			for(File f : deleted)
			{
				try {
					if(existingChecksums.containsKey(f.toURI().toURL()))
					{
						gbModifiedOrDeleted = true;
						break;
					}
				} catch (Exception e) {

				}

			}
		}
		if(added.length > 0 || gbModifiedOrDeleted)
		{
			reloadGridBeans();
		}
	}

	private void copyFoundGridBean(String contributorId, String pathToJar, IProgressMonitor subProgress)
	{
		URL source = getSourceURL(contributorId, pathToJar);
		File target = getTargetFile(contributorId, pathToJar);
		InputStream is = null;
		OutputStream os = null;
		try {

			if (!target.exists()) {
				if (!target.createNewFile()) {
					throw new Exception(
							"Unable to create file "
							+ target);
				}
			}
			// if we arrive here, we need to
			// copy the file
			is = source.openStream();
			os = new FileOutputStream(target,
					false);
			byte[] buf = new byte[4096];
			int n = 0;
			while ((n = is.read(buf)) > -1) {
				os.write(buf, 0, n);
			}
		} catch (Exception ex) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to contribute Application GUI "
					+ source, ex);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				GPEActivator.log(Status.ERROR, "Error while closing file: "+e.getMessage(), e);
			}
			try {
				if (os != null) {
					os.close();
				}
			} catch (IOException e) {
				GPEActivator.log(Status.ERROR, "Error while closing file: "+e.getMessage(), e);
			}
			subProgress.done();
		}
	}

	private void copyFoundGridBeans(final Map<String,List<String>> gbsToCopy, final int totalEffort) throws Exception
	{

		PlatformUI.getWorkbench().getProgressService()
		.run(false, false, new IRunnableWithProgress() {
			public void run(IProgressMonitor progress)
			throws InvocationTargetException,
			InterruptedException {
				try {
					progress.beginTask(
							"Updating application GUIs ",
							totalEffort);
					for (String contributorId : gbsToCopy.keySet()) {
						for(String pathToJar:gbsToCopy.get(contributorId))
						{
							IProgressMonitor subProgress = new SubProgressMonitor(
									progress,
									1,
									SubProgressMonitor.PREPEND_MAIN_LABEL_TO_SUBTASK);
							copyFoundGridBean(contributorId, pathToJar, subProgress);
						}
					}
				} finally {
					progress.done();
				}
			}
		});

	}

	public void deleteGridBeans(GridBeanInfo... gbInfos)
	{
		boolean deletedOne = false;
		for(GridBeanInfo gbInfo : gbInfos)
		{
			String path = getGridBeanPath(gbInfo);
			
			File f = new File(path);
			String errorMsg = "Unable to delete application GUI file "+path;
			try {
				DelegatingExtensionURLClassloader loader = existingClassloaders.get(f.toURI().toURL());
				if(loader != null)
				{
					loader.close();
				}
				boolean deleted = f.delete();
				if(deleted)
				{
					deletedOne = true;

				}
				else GPEActivator.log(Status.ERROR, errorMsg);
			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, errorMsg,e);
			}
		}
		if(deletedOne) reloadGridBeans();
	}

	private void fireGridBeansChanged(final List<GridBeanInfo> oldValue,
			final List<GridBeanInfo> newValue) {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {

			@Override
			public void run() {
				for (IGridBeanRegistryListener l : listeners) {
					l.gridBeansChanged(oldValue, newValue);
				}
			}
		});

	}

	public File[] getAlternativeGridBeanDirs() {
		return alternativeGridBeanDirs;
	}

	private File getExtensionInfoFile() throws IOException
	{
		IPath path = GPEActivator.getDefault().getStateLocation();
		File parent = path.toFile();

		if (!parent.exists()) {
			boolean created = parent.mkdir();
			if (!created) {
				throw new IOException("Unable to create directory "
						+ parent.getAbsolutePath());
			}
		}
		File file = new File(path.append(
				GPE4EclipseConstants.GB_EXTENSION_DATA_FILE)
				.toOSString());
		if (!file.exists()) {
			boolean created = file.createNewFile();
			if(!created) throw new IOException("Unable to create file "
					+ file.getAbsolutePath());

		}
		return file;
	}

	/**
	 * Returns the GridBean for the specified GridBeanInfo.
	 * 
	 * @param info
	 * @return InternalGridBean for the specified GridBeanInfo or null if the
	 *         list does not contain the specified GridBean.
	 */
	public InternalGridBean getGridBean(GridBeanInfo info) {

		GridBeanEntry entry = gridBeanEntries.get(info);
		if (entry != null) {
			try {
				return getGridBean(entry.getPath());
			} catch (Exception e) {
				GPEActivator.log(
						IStatus.ERROR,
						"Unable to instantiate GUI for application "
						+ info.getGridBeanName(), e);
			}
		}
		return null;
	}

	/**
	 * Loads the gridbean from the specified jar file.
	 * 
	 * @param pathToJar
	 * @return
	 * @throws Exception
	 */
	public InternalGridBean getGridBean(String pathToJar) throws Exception {

		File f = new File(pathToJar);
		JarFile pluginJar = new JarFile(pathToJar);
		URL url = f.toURI().toURL();
		URL[] urls = new URL[] { url };

		DelegatingExtensionURLClassloader loader = existingClassloaders.get(url);

		long checksum = 0;
		if (loader != null) {
			// check whether file has changed
			checksum = computeChecksum(pathToJar);
			if (checksum != existingChecksums.get(url)) {
				loader = null;
			}
		}
		if (loader == null) {

			loader = new DelegatingExtensionURLClassloader(urls,
					GridBeanRegistry.class.getClassLoader(),
					GPE4EclipseConstants.EXTENSION_POINT_GRID_BEANS,					
					GPE4EclipseConstants.EXTENSION_POINT_ENV_VARIABLE_SOURCE_TYPE,
					GPE4EclipseConstants.EXTENSION_POINT_STAGE_IN_TYPE,
					GPE4EclipseConstants.EXTENSION_POINT_STAGE_OUT_TYPE,
					GPE4EclipseConstants.EXTENSION_POINT_JAXB);
			if (checksum == 0) {
				computeChecksum(pathToJar);
			}
			synchronized (classLoaderLock) {
				existingChecksums.put(url, checksum);
				existingClassloaders.put(url, loader);
			}
		}

		return new InternalGridBeanImpl(pluginJar, loader,
				CommonKeys.GRIDBEANS);

	}

	public File getGridBeanDir() {
		return gridBeanDir;
	}

	/**
	 * Returns the gridbean's path for the specified GridBeanInfo.
	 * 
	 * @param info
	 * @return InternalGridBean for the specified GridBeanInfo or null if the
	 *         list does not contain the specified GridBean.
	 */
	public String getGridBeanPath(GridBeanInfo info) {
		GridBeanEntry entry = gridBeanEntries.get(info);
		if (entry != null) {
			return entry.getPath();
		}
		return null;
	}

	/**
	 * Returns the list of gridbeans (only returns the latest versions).
	 * 
	 * @return the list of the gridbeans.
	 */
	public List<GridBeanInfo> getGridBeans() {

		return new ArrayList<GridBeanInfo>(gridBeanInfos);
	}

	public URL getIconURLFor(GridBeanInfo info) {

		GridBeanEntry entry = gridBeanEntries.get(info);
		if (entry == null) {
			info = getNewerGridBean(info);
		}
		if(info != null) entry = gridBeanEntries.get(info);
		if (entry != null) {
			return entry.getIconURL();
		}
		return null;

	}

	/**
	 * Returns info about an equally new or newer version of the input gridbean
	 * if it exists or null if no newer version exists
	 * 
	 * @param info
	 * @return true if the specified gridbean is present; false otherwise.
	 */
	public GridBeanInfo getNewerGridBean(GridBeanInfo info) {
		if (info.getGridBeanVersion() == null) {
			// don't seem to care about version => return newest GridBean
			GridBeanInfo newest = null;
			for (GridBeanInfo i : gridBeanInfos) {
				if (i.getGridBeanName().equals(info.getGridBeanName())
						&& (newest == null || i.getGridBeanVersion()
								.compareTo(newest.getGridBeanVersion()) >= 0)) {
					newest = i;
				}
			}
			return newest;
		} else {
			for (GridBeanInfo i : gridBeanInfos) {
				if (i.getGridBeanName().equals(info.getGridBeanName())
						&& i.getGridBeanVersion().compareTo(
								info.getGridBeanVersion()) >= 0) {
					return i;
				}
			}
		}
		return null;
	}

	private URL getSourceURL(String contributorId, String pathToJar)
	{
		Bundle b = Platform.getBundle(contributorId);
		return b.getEntry(pathToJar);

	}

	private File getTargetFile(String contributorId, String pathToJar)
	{
		File f = new File(pathToJar);

		return new File(gridBeanDir, f.getName());
	}


	@SuppressWarnings("unchecked")
	private void loadExtensionInfo()
	{
		InputStream is = null;
		try {
			// deserialize!
			File file = getExtensionInfoFile();
			if(file.length() == 0) 
			{
				extensionInfo = new HashMap<String, List<String>>();
				return;
			}
			is = new FileInputStream(file);
			XStream xstream = new XStream();

			extensionInfo = (Map<String,List<String>>) xstream.fromXML(is);
		}
		catch(Exception e)
		{
			GPEActivator.log(IStatus.WARNING,"Could not load application " +
					"GUI extension data.",e);
			extensionInfo = new HashMap<String, List<String>>();
		}
		finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (Exception e) {
				// do nothing
			}
		}
	}

	private void loadExtensions() {
		try {
			final Map<String,List<String>> allGbExtensions = new HashMap<String,List<String>>();
			final Map<String,List<String>> gbsToCopy = new HashMap<String,List<String>>();

			IExtensionRegistry registry = Platform.getExtensionRegistry();

			IExtensionPoint extensionPoint = registry
			.getExtensionPoint(GPE4EclipseConstants.EXTENSION_POINT_GRID_BEANS);
			IConfigurationElement[] members = extensionPoint
			.getConfigurationElements();

			int numToCopy = 0;
			// For each extension
			for (int m = 0; m < members.length; m++) {
				IConfigurationElement member = members[m];
				String contributorID = member.getDeclaringExtension().getContributor()
				.getName();
				try {

					if (contributorID == null) continue;
					Bundle b = Platform.getBundle(contributorID);
					List<String> pathsToJars = extensionInfo.get(contributorID);

					List<String> pathList = allGbExtensions.get(contributorID);
					if(pathList ==null)
					{

						pathList = new ArrayList<String>();
						allGbExtensions.put(contributorID,pathList);
					}
					if (b != null) {
						IConfigurationElement[] gridBeans = member
						.getChildren("gridBeanJar");

						for (IConfigurationElement gridBean : gridBeans) {

							String pathToJar = gridBean
							.getAttribute("pathToJar");
							pathList.add(pathToJar);
							File target = getTargetFile(contributorID, pathToJar);
							if (target.exists()) {
								InternalGridBean gb = getGridBean(target
										.getAbsolutePath());
								String version1 = gb.getVersion();
								String version2 = gridBean
								.getAttribute("gridBeanVersion");
								if (version1.compareTo(version2) >= 0) {
									continue;
								}
							}
							if(pathsToJars != null && pathsToJars.contains(pathToJar))
							{
								// already installed this Grid Bean previously
								// the user might have removed it manually, so
								// lets not install it again
								continue;
							}

							List<String> toCopy = gbsToCopy.get(contributorID);
							if(toCopy ==null)
							{

								toCopy = new ArrayList<String>();
								gbsToCopy.put(contributorID,toCopy);
							}
							toCopy.add(pathToJar);
							numToCopy++;
						}
					}
				} catch (NullPointerException ex) {
					GPEActivator
					.log(IStatus.WARNING,
							"Unable to contribute Application GUI from plugin: the provided file path is invalid"
							+ contributorID, ex);
				} catch (Throwable t) {
					Exception e = (t instanceof Exception) ? (Exception) t
							: new Exception(t);
					GPEActivator.log(IStatus.WARNING,
							"Unable to contribute Application GUI from plugin "
							+ contributorID, e);
				}
			}

			cleanupOldExtensions(allGbExtensions);
			if(numToCopy > 0) copyFoundGridBeans(gbsToCopy, numToCopy);
			extensionInfo = allGbExtensions;

		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to contribute Application GUIs.", e);
		}

	}

	private void cleanupOldExtensions(Map<String,List<String>> currentExtensions){
		for (Iterator<Entry<String, List<String>>> iterator = 
			extensionInfo.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, List<String>> next = iterator.next();
			String contributorId = next.getKey();
			List<String> pathsToJar = currentExtensions.get(contributorId);
			boolean stillThere = pathsToJar != null && pathsToJar.size() > 0;
			if(!stillThere)
			{
				// this extension has disappeared!
				for(String pathToJar : next.getValue())
				{
					File target = getTargetFile(contributorId, pathToJar);
					boolean deleted = false;
					try {
						deleted = target.delete();
					} catch (Exception e) {
						deleted = false;
					}
					if(!deleted)
					{
						GPEActivator.log("Unable to delete application GUI file "
								+target.getAbsolutePath()+" that "
								+"was contributed by a plugin which is no longer" +
						"installed.");
					}
				}
			}
		}
	}


	/**
	 * Loads plugins from the specified directory.
	 * 
	 */
	public void loadGridBeans(File... gridBeanDirs) {
		if(gridBeanDirs == null) gridBeanDirs = new File[0];
		Map<GridBeanInfo, GridBeanEntry> gridBeanEntries = new ConcurrentHashMap<GridBeanInfo, GridBeanEntry>();
		List<GridBeanInfo> gridBeanInfos = new ArrayList<GridBeanInfo>();
		Set<GridBeanInfo> oldGridBeanInfos = new HashSet<GridBeanInfo>();

		for(File gridBeanDir : gridBeanDirs)
		{
			if(gridBeanDir == null) continue;
			File[] files = gridBeanDir.listFiles();
			if(files == null) continue;
			for (int i = 0; i < files.length; i++) {
				if (files[i].getName().endsWith(".jar")) {
					try {
						InternalGridBean gb = getGridBean(files[i].getPath());
						addGridBean(gb, files[i].getCanonicalPath(),gridBeanInfos,oldGridBeanInfos,gridBeanEntries);
					} catch (Throwable t) {
						Exception e = new InvalidGridBeanException(t, files[i]);
						GPEActivator.log(IStatus.WARNING,
								"Unable to load Application GUI from  "
								+ files[i], e);
					}
				}
			}
		}
		List<GridBeanInfo> oldValue = this.gridBeanInfos;
		synchronized (gridBeanDataLock) {
			this.gridBeanInfos = gridBeanInfos;
			this.oldGridBeanInfos = oldGridBeanInfos;
			this.gridBeanEntries = gridBeanEntries;
		}
		List<GridBeanInfo> newValue = new ArrayList<GridBeanInfo>(gridBeanInfos);
		if(!CollectionUtil.containSameElements(oldValue, newValue))
		{
			fireGridBeansChanged(oldValue, newValue);
		}

	}

	/**
	 * Refresh list of available GridBeans in the GridBean directory
	 */
	public void reloadGridBeans() {
		if(gridBeanInfos != null && gridBeanInfos.size() > 0)
		{
			IPreferences prefs = GPEActivator.getDefault().getGPEPreferences();
			for(GridBeanInfo info : gridBeanInfos)
			{

				prefs.set(CommonKeys.COMMON_GRIDBEANS.getKey(info.toString()),"");
			}
		}

		File[] dirs = ArrayUtils.concat(gridBeanDir, alternativeGridBeanDirs);
		loadGridBeans(dirs);
	}

	public void removeListener(IGridBeanRegistryListener l) {
		listeners.remove(l);
	}

	private void saveExtensionInfo()
	{
		XStream xstream = new XStream();

		String errorMsg = "Could not save application " +
		"GUI extension data.";
		try {
			File file = getExtensionInfoFile();
			OutputStream os = new FileOutputStream(file);
			try {
				xstream.toXML(extensionInfo, os);	
			} finally {
				os.close();
			}
		} catch (Exception e) {
			GPEActivator.log(IStatus.WARNING,
					errorMsg,e);
		}
	}
	public void setAlternativeGridBeanDirs(File[] alternativeGridBeanDirs) {
		this.alternativeGridBeanDirs = alternativeGridBeanDirs;
		directoryWatcher.cancel();
		if(gridBeanDir != null || alternativeGridBeanDirs != null)
		{
			directoryWatcher.setWatched(ArrayUtils.concat(gridBeanDir, alternativeGridBeanDirs));
			directoryWatcher.schedule(updateInterval);
		}

	}

	public void setGridBeanDir(File gridBeanDir) {
		this.gridBeanDir = gridBeanDir;
		directoryWatcher.cancel();
		if(gridBeanDir != null || alternativeGridBeanDirs != null)
		{
			directoryWatcher.setWatched(ArrayUtils.concat(gridBeanDir, alternativeGridBeanDirs));
			directoryWatcher.schedule(updateInterval);

		}

	}


}
