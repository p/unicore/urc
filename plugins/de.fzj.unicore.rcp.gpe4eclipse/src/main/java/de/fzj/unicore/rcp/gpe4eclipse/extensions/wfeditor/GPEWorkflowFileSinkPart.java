package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.figures.DataSinkFigure;
import de.fzj.unicore.rcp.wfeditor.parts.DataSinkPart;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowFileUtils;

/**
 * Objects of this class can be used for either {@link GPEWorkflowFileSink}s or
 * {@link GPEWorkflowFileSetSink}s. The only discriminator is the type, which is
 * passed in as a string.
 * 
 * @author bjoernh
 *
 *         04.11.2015 09:05:10
 *
 */
public class GPEWorkflowFileSinkPart extends DataSinkPart implements
		IPropertyChangeListener {

	private Color mimeTypeColor = null;
	private String mimeType = null;
	private final String type;
	
	/**
	 * Should be one of "File" or "Fileset"
	 */
	public GPEWorkflowFileSinkPart(String _type) {
		this.type = _type;
	}

	public void propertyChange(PropertyChangeEvent event) {
		if (event.getProperty().equals(
				WFConstants.P_MIME_COLOR_PREFIX + mimeType)) {
			RGB rgb = (RGB) event.getNewValue();
			if (rgb.equals(mimeTypeColor.getRGB())) {
				return;
			}
			Color temp = mimeTypeColor;
			mimeTypeColor = new Color(temp.getDevice(), rgb);
			((DataSinkFigure) getFigure()).setDataTypeColor(mimeTypeColor);
			temp.dispose();
			getFigure().repaint();
		}
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		if (!(getFigure() instanceof DataSinkFigure)) {
			return;
		}
		DataSinkFigure figure = (DataSinkFigure) getFigure();
		String mimeTypes = "any";
		int i = 0;
		if (getModel().getIncomingDataTypes() != null
				&& getModel().getIncomingDataTypes().size() > 0) {

			for (String mime : getModel().getIncomingDataTypes()) {

				if (i == 0) {
					mimeTypes = mime;
					String subtype = mime.split("/")[1];

					if (subtype.startsWith("x-")) {
						subtype = subtype.substring(2);
					}
					figure.setDataTypeSymbol("." + subtype);
				} else {
					mimeTypes += ", " + mime;
				}
				RGB rgb = WorkflowFileUtils.getColorForMimeType(mime);
				if (rgb == null) {
					rgb = WorkflowFileUtils.getMimeColorGenerator().nextColor(
							mime);
					WorkflowFileUtils.registerColorForMimeType(mime, rgb);
				}
				if (mimeTypeColor == null) {
					mimeTypeColor = new Color(getViewer().getControl()
							.getDisplay(), rgb);
					mimeType = mime;
					figure.setDataTypeColor(mimeTypeColor);
					WFActivator.getDefault().getPreferenceStore()
							.addPropertyChangeListener(this);
				}
				i++;
			}

		} else {
			figure.setDataTypeSymbol(".*");
		}
		String typeString = "Type";
		if (i > 1) {
			typeString += "s";
		}
		figure.setToolTipText(new String[] { this.type + ": " + getModel().getName(),
				typeString + ": " + mimeTypes });
		Dimension d = figure.calculatePreferredSize();
		figure.setPreferredSize(d.width, d.height);
	}

}
