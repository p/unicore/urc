/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.api.jsdl.RangeValueType;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.RangeValueTypeRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.utils.RangeValueFormattingUtils;

/**
 * provides a GUI to specify an RangeValueTypeResourceRequirementValue
 * 
 * @author Christian Hohmann
 * 
 */
public class SimpleRangeValueTypeDialog implements IInputValidator {

	private static final double EPSILON = 1e-15; // constant used to go close to
													// excluded interval ends
													// without hitting them

	public static String LESS_THAN = "<"; // @jve:decl-index=0:
	public static String LESS_THAN_OR_EQUAL = "<=";

	private IUnitConverter unitConverter = UnitConverter.getInstance();
	private String internalUnit;
	private String selectedUnit;

	/**
	 * the valid Bounds specified to the GUI
	 */
	double validUpper = 0.0;
	double validLower = 0.0;
	double validExact;
	double validEpsilon;
	boolean validUpperEquals = true; // true means '<=' false means '<'
	boolean validLowerEquals = true; // true means '<=' false means '<'

	private RangeValueTypeRequirementValue requirement;
	private RangeValueType chosenValue;
	private RangeValueType validValue;
	boolean onlyWholeNumberValues;

	private String exactValueString;

	/**
	 * 
	 * @param requirement
	 *            specifies the valid bounds and takes the choosenvalues
	 * @param onlyIntegerValues
	 *            if true, only wholeNumbers are accepted as input i.e. Number
	 *            of CPUs
	 */
	public SimpleRangeValueTypeDialog(RangeValueTypeRequirementValue requirement) {
		this.onlyWholeNumberValues = requirement.isWholeNumbersOnly();
		this.requirement = requirement;
		internalUnit = requirement.getInternalUnit();
		selectedUnit = requirement.getSelectedUnit();
		try {

			chosenValue = unitConverter.convertValue(internalUnit,
					selectedUnit, RangeValueType.class,
					requirement.getChosenValue());
			validValue = unitConverter.convertValue(internalUnit, selectedUnit,
					RangeValueType.class, requirement.getValidValue());

			validLowerEquals = validValue.isIncludeLowerBound();
			validUpperEquals = validValue.isIncludeUpperBound();
			validLower = validValue.getLowerBound();
			validUpper = validValue.getUpperBound();

			validExact = chosenValue.getExact();
			if (Double.isNaN(validExact)) {
				RangeValueTypeRequirementValue.getDefaultValue(validValue,
						onlyWholeNumberValues);
			}

			validEpsilon = validValue.getEpsilon();
			if (Double.isNaN(validExact)) {
				validEpsilon = 0.0;
			}

			createSShell();
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Problem while creating dialog: "
					+ e.getMessage(), e);
		}
	}

	/**
	 * This method initializes sShell
	 */
	private void createSShell() {

		String dialogTitle = onlyWholeNumberValues ? "Specify Range Value Parameter - only whole numbers are allowed"
				: "Specify Range Value Parameter";
		String validBounds = formatDouble(validLower, RoundingMode.CEILING)
				+ " ";
		if (validLowerEquals) {
			validBounds += LESS_THAN_OR_EQUAL;
		} else {
			validBounds += LESS_THAN;
		}
		validBounds += " VALUE ";
		if (validUpperEquals) {
			validBounds += LESS_THAN_OR_EQUAL;
		} else {
			validBounds += LESS_THAN;
		}
		validBounds += " " + formatDouble(validUpper, RoundingMode.FLOOR);

		double exact = validExact;
		chosenValue.setExact(exact);
		// determine String to be displayed, note that both chosenValue and
		// validValue are in selectedUnit already, so we don't need to convert
		// units
		exactValueString = RangeValueFormattingUtils.formatRangeValue(
				chosenValue, validValue, selectedUnit, selectedUnit);

		Display display = PlatformUI.getWorkbench().getDisplay();
		Shell parent = display.getActiveShell();
		InputDialog dialog = new InputDialog(parent, dialogTitle, validBounds,
				exactValueString, this);
		if (dialog.open() == InputDialog.OK) {
			exactValueString = dialog.getValue();
			doExit();
		}

	}

	/**
	 * Must only be called after successful validation. This method stores the
	 * values in the RangeValueTypeResourceRequirementValue and disposes the
	 * RangeValueTypeDialogCellEditor
	 * 
	 * @return true
	 */
	private boolean doExit() {
		try {
			// convert back to internal unit
			chosenValue = unitConverter.convertValue(selectedUnit,
					internalUnit, RangeValueType.class, chosenValue);

			if (exactValueString.startsWith("${")) {
				chosenValue.setExpression(exactValueString);
				requirement.setSelectedUnit(RequirementConstants.NO_UNIT);
				requirement.setChosenValue(chosenValue);
				return true;
			}

			chosenValue.setExpression(null);

			double exact = parseString(exactValueString);
			String low = formatDouble(validLower, RoundingMode.CEILING);
			String high = formatDouble(validUpper, RoundingMode.FLOOR);
			if (high.equals(exactValueString)) {
				// user probably wants to get as close to the upper bound as
				// possible
				if (validUpperEquals)
					exact = validUpper;
				else
					exact = validUpper - EPSILON;
			} else if (low.equals(exactValueString)) {
				// user probably wants to get as close to the lower bound as
				// possible
				if (validLowerEquals)
					exact = validLower;
				else
					exact = validLower + EPSILON;
			}
			double internalExact = UnitConverter.getInstance().convertValue(
					selectedUnit, internalUnit, Double.class, exact);
			if (onlyWholeNumberValues) {
				internalExact = Math.round(internalExact);
			}
			chosenValue.setExact(internalExact);
			requirement.setChosenValue(chosenValue);
			return true;
		} catch (Exception e1) {
			return false;
		}
	}

	private String formatDouble(double d, RoundingMode rm) {
		return RangeValueFormattingUtils.formatDouble(d, rm);
	}

	public double parseString(String s) throws ParseException {
		return NumberFormat.getInstance().parse(s).doubleValue();
	}

	public String isValid(String newText) {
		
		if(newText.startsWith("${")) {
			if(!newText.endsWith("}")) {
				return "Expression must end with \"}\".";
			}
			return null;
		}

		Double exact;
		try {
			exact = parseString(newText);
		} catch (Exception e1) {
			return "Unable to parse value: " + e1.getMessage();
		}
		String low = formatDouble(validLower, RoundingMode.CEILING);
		String high = formatDouble(validUpper, RoundingMode.FLOOR);
		if (high.equals(newText)) {
			// user probably wants to get as close to the upper bound as
			// possible
			if (validUpperEquals) {
				exact = validUpper;
			} else {
				exact = validUpper - EPSILON;
			}
		} else if (low.equals(newText)) {
			// user probably wants to get as close to the lower bound as
			// possible
			if (validLowerEquals) {
				exact = validLower;
			} else {
				exact = validLower + EPSILON;
			}
		}

		if (validLowerEquals) {
			if ((exact) < validLower) {
				return "Value must be greater than or equal "
						+ Double.toString(validLower);
			}
		} else {
			if ((exact) <= validLower) {
				return "Value must be greater than "
						+ Double.toHexString(validLower);
			}
		}
		if (validUpperEquals) {
			if (!((exact) <= validUpper)) {
				return "Value must be less than or equal "
						+ Double.toString(validUpper);
			}

		} else {
			if (!((exact) < validUpper)) {
				return "Value must be less than " + Double.toString(validUpper);
			}
		}

		return null;
	}

}