package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import de.fzj.unicore.rcp.common.guicomponents.FancyTableViewer;
import de.fzj.unicore.rcp.common.utils.TableColumnSorter;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.commands.AddStageInToModelCommand;
import de.fzj.unicore.rcp.gpe4eclipse.commands.RemoveStageInFromModelCommand;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.FilenameInJobDirCellEditor;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IDComboBoxCellEditor;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;


@SuppressWarnings("unchecked")
public class StageInTableViewer extends FancyTableViewer {

	// column headers
	public static final String ID = "Name", SOURCE_TYPE = "Source Type",
			SOURCE_FILE = "Source File(s)",
			TARGET_FILE = "File(s) in Job Directory",
			DELETE_INPUT_FILE = "Delete on Termination"; 
			
	// FORCE = "Overwrite", BINARY = "Binary";

	/**
	 * the order of the header names in COLUMN_PROPERTIES determines the order
	 * of columns in the table
	 */
	public static final String[] COLUMN_PROPERTIES = new String[] { ID,
			SOURCE_TYPE, SOURCE_FILE, TARGET_FILE, DELETE_INPUT_FILE };

	private StageInList stageInList;

	/**
	 * The cell editors, one for each column of the table. Note that these are
	 * changed dynamically by the CellModifier being used!
	 */
	private CellEditor[] cellEditors;

	/**
	 * List of actions that can be executed on this viewer
	 */
	List<IAction> actions;
	private Action addStageAction;
	private Action removeStageAction;
	private CommandStack commandStack;

	private String[] usedColumns;

	/**
	 * The constructor.
	 */
	public StageInTableViewer(Composite parent, StageInList stageInList,
			CommandStack commandStack) {
		this(parent, stageInList, commandStack, COLUMN_PROPERTIES);
	}

	/**
	 * The constructor.
	 */
	public StageInTableViewer(Composite parent, StageInList stageInList,
			CommandStack commandStack, String[] usedColumns) {
		super(parent, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION);
		this.usedColumns = usedColumns;
		this.stageInList = stageInList;
		this.commandStack = commandStack;
		Table table = createTable();
		// initialize action list
		createActions();

		setUseHashlookup(true);

		// Create the cell editors
		cellEditors = new CellEditor[table.getColumnCount()];

	
		CellEditor editor = null;
		for (String column : usedColumns) {
			if (ID.equals(column)) {
				// Combo Box
				editor = new IDComboBoxCellEditor(table, stageInList);
			} else if (SOURCE_TYPE.equals(column)) {
				editor = new StageInTypeSelectionCellEditor(table,
						stageInList.getGridBean(), stageInList.getAdaptable());
			} else if (SOURCE_FILE.equals(column)) {
				// cell editor for the source file
				// this is just a dummy editor, the real editor to be used
				// may be injected by extensions based on the SOURCE_TYPE
				editor = new TextCellEditor(table);
			} else if (TARGET_FILE.equals(column)) {
				editor = new FilenameInJobDirCellEditor(table, true);
			} else if (DELETE_INPUT_FILE.equals(column)) {
				editor = new CheckboxCellEditor(table);
			}
			if (editor != null) {
				cellEditors[getColumnFor(column)] = editor;
				// cellEditors[getColumnFor(FORCE)] = new
				// CheckboxCellEditor(table);
				// cellEditors[getColumnFor(BINARY)] = new
				// CheckboxCellEditor(table);
			}
		}

		setCellEditors(cellEditors);

		StageInTableCellModifier modifier = new StageInTableCellModifier(this);
		setCellModifier(modifier);
		setContentProvider(new StageInContentProvider());
		setLabelProvider(new StageInLabelProvider(this));
		setColumnProperties(COLUMN_PROPERTIES);

		setInput(getStageInList());
		packTable();
		// create context menu for the table
		final MenuManager menuMgr = new MenuManager("Actions");
		menuMgr.setRemoveAllWhenShown(true);

		Menu menu = menuMgr.createContextMenu(getTable());
		getTable().setMenu(menu);
		IMenuListener listener = new IMenuListener() {
			public void menuAboutToShow(IMenuManager m) {
				for (Iterator<IAction> iter = actions.iterator(); iter
						.hasNext();) {
					IAction action = iter.next();
					if (action.isEnabled()) {
						menuMgr.add(action);
					}
				}
			}
		};
		menuMgr.addMenuListener(listener);

	}

	private void createActions() {
		actions = new ArrayList<IAction>();
		addStageAction = new Action() {
			@Override
			public boolean isEnabled() {
				return getStageInList().getFileSets().size() > 0;
			}

			@Override
			public void run() {
				Command cmd = new AddStageInToModelCommand(getStageInList());
				if (commandStack != null) {
					commandStack.execute(cmd);
				} else {
					cmd.execute();
				}
			}
		};
		addStageAction.setText("Add File");
		addStageAction
				.setToolTipText("Add a file to be staged in before job execution.");
		addStageAction.setImageDescriptor(GPEActivator
				.getImageDescriptor("add.png"));
		actions.add(addStageAction);

		removeStageAction = new Action() {
			
			@Override
			public boolean isEnabled() {
				// We cannot remove stage ins that are define by the GridBean!
				IStructuredSelection selection = (IStructuredSelection) getSelection();
				List<IStage> stages = selection.toList();
				for (IStage in : stages) {
					if (in.isDefinedInGridBean()) {
						return false;
					}
				}
				return !getSelection().isEmpty();
			}

			@Override
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getSelection();
				Command cmd = new RemoveStageInFromModelCommand(
						getStageInList(), selection.toList());
				if (commandStack != null) {
					commandStack.execute(cmd);
				} else {
					cmd.execute();
				}

			}
		};
		removeStageAction.setText("Remove File");
		removeStageAction
				.setToolTipText("Remove a file stage in that won't be used anymore.");
		removeStageAction.setImageDescriptor(GPEActivator
				.getImageDescriptor("delete.png"));
		actions.add(removeStageAction);
	}

	private Table createTable() {
		Table table = getTable();
		for (int i = 0; i < usedColumns.length; i++) {
			TableColumn col = new TableColumn(table, SWT.LEFT, i);
			col.setText(usedColumns[i]);
			if (usedColumns[i].equals(ID)) {
				col.setToolTipText("An identifier for the file import.");
			} else if (usedColumns[i].equals(SOURCE_FILE)) {
				col.setToolTipText("The name or ID of the file to be imported.");
			} else if (usedColumns[i].equals(SOURCE_TYPE)) {
				col.setToolTipText("Type of the source from which the file(s) shall be imported into the job's working directory.");
			} else if (usedColumns[i].equals(TARGET_FILE)) {
				col.setToolTipText("Name of the file in the working directory of the job.");
			} else if (usedColumns[i].equals(DELETE_INPUT_FILE)) {
				col.setToolTipText("Specify whether input file should be deleted from storage.");
			}
			
			// else if(usedColumns[i].equals(FORCE))
			// {
			// col.setToolTipText("If this is set, file(s) at the destination will be overwritten by this file export.");
			// col.setWidth(70);
			// }

			// else if(usedColumns[i].equals(BINARY))
			// {
			// col.setToolTipText("Specify whether the exported file(s) have binary content.");
			// col.setWidth(70);

			// }

			final int index = i;
			new TableColumnSorter(this, col) {
				@Override
				protected int doCompare(Viewer v, Object e1, Object e2) {
					ITableLabelProvider lp = ((ITableLabelProvider) getLabelProvider());
					String t1 = lp.getColumnText(e1, index);
					String t2 = lp.getColumnText(e2, index);
					return t1.compareTo(t2);
				}
			};

		}
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		return table;
	}

	public List<IAction> getActions() {
		return actions;
	}

	public StageInList getStageInList() {
		return stageInList;
	}

	public void setCellEditor(int column, CellEditor editor) {
		cellEditors[column] = editor;
	}
	
	

	public static int getColumnFor(String columnName) {
		for (int i = 0; i < COLUMN_PROPERTIES.length; i++) {
			if (COLUMN_PROPERTIES[i].equals(columnName)) {
				return i;
			}
		}
		return -1;
	}

}