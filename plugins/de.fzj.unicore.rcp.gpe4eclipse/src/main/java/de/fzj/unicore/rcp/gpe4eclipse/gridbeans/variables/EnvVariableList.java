/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;

import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.EnvironmentVariableParameterValue;
import com.intel.gpe.gridbeans.parameters.GridBeanParameter;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IEnvironmentVariableParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IEnvVariableSourceTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowVariableSink;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;

/**
 * @author demuth
 * 
 */
public class EnvVariableList implements Serializable, PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8739654720836534895L;

	/**
	 * views that are interested in gridbean changes
	 */
	private transient Set<IEnvVariableListObserver> changeListeners;

	// used as the underlying gridBean gridbean with which this gridbean
	// communicates
	protected IGridBean gridBean;

	protected IAdaptable adaptable;

	protected boolean listeningToModel = true;

	private List<EnvVariable> list = createEmptyVariableList();
	private Map<String, EnvVariable> map = new HashMap<String, EnvVariable>();

	public EnvVariableList(IGridBean gridBean, IAdaptable adaptable) {
		this.adaptable = adaptable;
		this.gridBean = gridBean;
		getGridBean().addPropertyChangeListener(this);
	}

	public void addChangeListener(IEnvVariableListObserver viewer) {
		getChangeListeners().add(viewer);
	}

	public void addNewVariable(EnvVariable variable) {
		synchronized (list) {
			// update GridBeanModel
			listeningToModel = false;
			getGridBean().set(variable.getGridBeanParameter().getName(),
					variable.getValue());
			getGridBean().getInputParameters().add(
					variable.getGridBeanParameter());
			listeningToModel = true;
		}
		addVariableToList(variable);

	}

	public EnvVariable addNewVariable(String name) {
		QName unique = new QName(EnvVariable.USER_DEFINED_VARIABLE_PREFIX
				+ UUID.randomUUID().toString(), name);
		IGridBeanParameter param = new GridBeanParameter(unique,
				GridBeanParameterType.ENVIRONMENT_VARIABLE);
		EnvironmentVariableParameterValue value = new EnvironmentVariableParameterValue();
		EnvVariable variable = new EnvVariable(param, value);
		addNewVariable(variable);
		return variable;
	}

	public void addParameter(IGridBeanParameter param,
			IGridBeanParameterValue value) {
		synchronized (list) {
			if (GridBeanParameterType.ENVIRONMENT_VARIABLE.equals(param
					.getType())) {
				EnvVariable variable = new EnvVariable(param,
						(IEnvironmentVariableParameterValue) value);
				addVariableToList(variable);
			}
		}
	}

	protected void addSinkToActivity(EnvVariable variable) {
		IActivity activity = getActivity();
		if (activity == null) {
			return;
		}
		IGridBeanParameter param = variable.getGridBeanParameter();
		IDataSource source = activity.getDataSourceList().getDataSourceById(
				param.getName().toString());
		if (source == null) {
			GPEWorkflowVariableSink sink = new GPEWorkflowVariableSink(
					activity, param);
			activity.getDataSinkList().addDataSink(sink);
			activity.setDataSinks(activity.getDataSinkList());
		}

	}

	/**
	 * Add an element to the list
	 * 
	 * @param variable
	 */
	public void addVariableToList(EnvVariable variable) {
		synchronized (list) {
			list.add(variable);
			map.put(keyFor(variable.getGridBeanParameter().getName()), variable);
		}
		informObserversOfAdd(variable);
		addSinkToActivity(variable);
	}

	public void changeVariablePosition(EnvVariable variable, int newPosition) {
		synchronized (list) {
			IGridBeanParameter param = null;
			for (Iterator<IGridBeanParameter> iter = getGridBean()
					.getInputParameters().iterator(); iter.hasNext();) {
				IGridBeanParameter p = iter.next();
				if (variable.getGridBeanParameter().equals(p)) {
					param = p;
					iter.remove();
					break;
				}
			}
			list.remove(variable);
			list.add(newPosition, variable);
			if (newPosition == 0) {
				getGridBean().getInputParameters().add(0, param);
			} else {
				EnvVariable previous = list.get(newPosition - 1);
				int index = 0;
				for (IGridBeanParameter p : getGridBean().getInputParameters()) {
					if (p.equals(previous.getGridBeanParameter())) {
						break;
					}
					index++;
				}
				getGridBean().getInputParameters().add(index + 1, param);
			}
		}
		// make the model dirty
		getGridBean().set(variable.getGridBeanParameter().getName(),
				variable.getValue());
		informObserversOfPositionChange(variable, newPosition);
	}

	private List<EnvVariable> createEmptyVariableList() {
		List<EnvVariable> arr = new ArrayList<EnvVariable>();
		return Collections.synchronizedList(arr);
	}

	/**
	 * perform some cleanup
	 */
	public void dispose() {
		getGridBean().removePropertyChangeListener(this);
	}

	private IActivity getActivity() {
		if (getAdaptable() == null) {
			return null;
		}
		Object adapter = getAdaptable().getAdapter(IActivity.class);
		return (IActivity) adapter;
	}

	public IAdaptable getAdaptable() {
		return adaptable;
	}

	protected Set<IEnvVariableListObserver> getChangeListeners() {
		if (changeListeners == null) {
			changeListeners = new HashSet<IEnvVariableListObserver>();
		}
		return changeListeners;
	}

	public IGridBean getGridBean() {
		return gridBean;
	}

	public int getPosition(EnvVariable var) {
		synchronized (list) {
			return list.indexOf(var);
		}
	}

	public EnvVariable[] getVariables() {
		synchronized (list) {
			EnvVariable[] siteArr = list.toArray(new EnvVariable[0]);
			return siteArr;
		}
	}

	protected void informObserversOfAdd(EnvVariable variable) {
		synchronized (list) {
			Iterator<IEnvVariableListObserver> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next()
						.addVariable(variable);
			}
		}
	}

	protected void informObserversOfPositionChange(EnvVariable variable,
			int newPosition) {
		synchronized (list) {
			Iterator<IEnvVariableListObserver> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next()
						.changeVariablePosition(variable, newPosition);
			}
		}
	}

	protected void informObserversOfRemove(EnvVariable variable) {
		synchronized (list) {
			Iterator<IEnvVariableListObserver> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next()
						.removeVariable(variable);
			}
		}
	}

	public void informObserversOfUpdate(EnvVariable variable) {
		synchronized (list) {
			Iterator<IEnvVariableListObserver> iterator = getChangeListeners().iterator();
			while (iterator.hasNext()) {
				iterator.next()
						.updateVariable(variable);
			}
		}
	}

	private String keyFor(QName qname) {
		return qname.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	public void propertyChange(final PropertyChangeEvent evt) {
		if (!listeningToModel) {
			return;
		}
		EnvVariable variable = map.get(evt.getPropertyName());
		if (evt.getNewValue() == null) {
			if (variable != null) {
				// variable has been set to null => check whether it still
				// exists as input parameter

				IGridBeanParameter param = getGridBean().findInputParameter(
						variable.getGridBeanParameter().getName());
				if (param == null) {
					// this variable is obsolete and should be removed
					removeVariableInternally(variable);
				} else if (!GridBeanParameterType.ENVIRONMENT_VARIABLE
						.equals(param.getType())) {
					removeVariableInternally(variable);
				}

			}
		} else if (evt.getNewValue() instanceof IEnvironmentVariableParameterValue) {
			IEnvironmentVariableParameterValue newValue = (IEnvironmentVariableParameterValue) evt
					.getNewValue();
			synchronized (list) {
				if (variable == null) {
					IGridBeanParameter param = getGridBean()
							.findInputParameter(
									QName.valueOf(evt.getPropertyName()));
					if (param != null) {
						addParameter(param, newValue);
						variable = map.get(evt.getPropertyName());
					}
					if (variable == null) {
						// this seems to be an undeclared env variable... ignore
						// it
						return;
					}
				}
				variable.setValue(newValue);
				informObserversOfUpdate(variable);
			}
		} else {
			if (variable != null) {
				removeVariableInternally(variable);
			}
		}

	}

	public void removeChangeListener(IEnvVariableListObserver viewer) {
		getChangeListeners().remove(viewer);
	}

	protected void removeSinkFromActivity(EnvVariable variable) {
		IActivity activity = getActivity();
		if (activity == null) {
			return;
		}
		IGridBeanParameter param = variable.getGridBeanParameter();
		IDataSink sink = activity.getDataSinkList().getDataSinkById(
				param.getName().toString());
		if (sink != null) {
			sink.dispose();
			activity.setDataSinks(activity.getDataSinkList());
		}

	}

	/**
	 * Remove an environment variable both from this list and from the GridBean
	 * model.
	 * 
	 * @param variable
	 */
	public void removeVariable(EnvVariable variable) {

		synchronized (list) {
			IGridBeanParameter param = variable.getGridBeanParameter();
			// remove Variable from GridBean gridbean
			listeningToModel = false;
			getGridBean().set(param.getName(), null);
			getGridBean().getInputParameters().remove(param);
			listeningToModel = true;
			removeVariableInternally(variable);

		}

	}

	/**
	 * Remove the environment variable from the list but do not touch the
	 * GridBean model
	 * 
	 * @param variable
	 */
	protected void removeVariableInternally(EnvVariable variable) {
		synchronized (list) {
			IEnvVariableSourceTypeExtensionPoint ext = null;
			if (variable.getGridBeanParameter().isInputParameter()) {
				ext = GPEActivator.getDefault().getVariableSourceTypeRegistry()
						.getDefiningExtension(variable.getType());
			}

			if (ext != null) {
				ext.detachFromParameterValue(getAdaptable(), variable);
			}
			list.remove(variable);
			map.remove(keyFor(variable.getGridBeanParameter().getName()));
		}
		informObserversOfRemove(variable);
		removeSinkFromActivity(variable);
	}

	/**
	 * Used to store the associated GridBeanParameter value in the GridBeanModel
	 * after the Variable has been altered by the user
	 * 
	 * @param modifier
	 * @param variable
	 */
	public void reportVariableNameChange(EnvVariable variable, String newName) {
		synchronized (list) {
			String key = keyFor(variable.getGridBeanParameter().getName());
			EnvVariable changed = map.remove(key);
			IGridBeanParameter oldParam = changed.getGridBeanParameter();
			GridBeanParameter newParam = new GridBeanParameter();
			newParam.setType(oldParam.getType());
			QName newQName = new QName(oldParam.getName().getNamespaceURI(),
					newName);
			newParam.setName(newQName);

			variable.setGridBeanParameter(newParam);
			map.put(keyFor(newQName), variable);

			listeningToModel = false;
			getGridBean().getInputParameters().remove(oldParam);
			getGridBean().getInputParameters().add(newParam);
			getGridBean().set(oldParam.getName(), null);
			getGridBean().set(newQName, variable.getValue());
			listeningToModel = true;
			informObserversOfUpdate(variable);
		}
	}

	/**
	 * Used to store the associated GridBeanParameter value in the GridBeanModel
	 * after the Variable has been altered by the user
	 * 
	 * @param modifier
	 * @param newValue
	 */
	public void reportVariableValueChange(EnvVariable newValue) {
		synchronized (list) {
			String key = keyFor(newValue.getGridBeanParameter().getName());
			EnvVariable changed = map.get(key);
			changed.setValue(newValue.getValue());
			listeningToModel = false;
			getGridBean().set(newValue.getGridBeanParameter().getName(),
					newValue.getValue());
			listeningToModel = true;
			informObserversOfUpdate(newValue);
		}
	}

	public int size() {
		return list.size();
	}

}
