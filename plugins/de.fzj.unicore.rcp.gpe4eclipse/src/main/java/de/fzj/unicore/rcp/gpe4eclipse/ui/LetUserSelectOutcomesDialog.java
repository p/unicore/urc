package de.fzj.unicore.rcp.gpe4eclipse.ui;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.api.transfers.IGridFileSetTransfer;

import de.fzj.unicore.rcp.common.guicomponents.ValidatingListSelectionDialog;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.parameters.FileTransferContentProvider;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.parameters.FileTransferLabelProvider;

public class LetUserSelectOutcomesDialog extends ValidatingListSelectionDialog {

	private String downloadDir;
	private Text downloadDirText;

	public LetUserSelectOutcomesDialog(Shell parentShell,
			IGridFileSetTransfer<?> input, String downloadDir) {
		super(parentShell, input, new FileTransferContentProvider(),
				new FileTransferLabelProvider(), "Select files for download");
		this.downloadDir = downloadDir;
	}

	@Override
	protected Composite createDialogArea(Composite parent) {
		Composite result = super.createDialogArea(parent);

		Composite downloadDirGroup = new Composite(result, SWT.NONE);
		downloadDirGroup.setLayout(new GridLayout(2, false));
		Label downloadDirLabel = new Label(downloadDirGroup, SWT.NONE);
		downloadDirLabel.setText("Download files to: ");
		GridData data = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		data.horizontalSpan = 2;
		downloadDirLabel.setLayoutData(data);
		downloadDirText = new Text(downloadDirGroup, SWT.BORDER);
		downloadDirText.setText(downloadDir);
		data = new GridData(SWT.CENTER, SWT.CENTER, false, true);
		data.widthHint = 500;
		downloadDirText.setLayoutData(data);
		downloadDirText.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				downloadDir = downloadDirText.getText();

			}
		});

		Button downloadDirButton = new Button(downloadDirGroup, SWT.PUSH);
		downloadDirButton.setText("&Browse");
		downloadDirButton.setLayoutData(new GridData());
		downloadDirButton.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				String dir = letUserSelectDownloadDir();
				if (dir != null) {
					setDownloadDir(dir);
				}

			}

			public void widgetSelected(SelectionEvent e) {
				String dir = letUserSelectDownloadDir();
				if (dir != null) {
					setDownloadDir(dir);
				}

			}
		});
		return result;
	}

	public void setDownloadDir(String _downloadDir) {
		this.downloadDir = _downloadDir;
		this.downloadDirText.setText(this.downloadDir);
	}

	public String getDownloadDir() {
		return downloadDir;
	}

	private String letUserSelectDownloadDir() {
		Shell parent = PlatformUI.getWorkbench().getDisplay().getActiveShell();
		if (parent == null) {
			parent = new Shell();
		}
		DirectoryDialog dialog = new DirectoryDialog(parent);
		String filterPath = downloadDir;
		dialog.setFilterPath(filterPath);
		dialog.setMessage("Select a target folder for the job's output files");
		String result = dialog.open();
		if (result != null && result.trim().length() > 0
				&& !result.endsWith(File.separator)) {
			result += File.separator;
		}
		return result;
	}

}
