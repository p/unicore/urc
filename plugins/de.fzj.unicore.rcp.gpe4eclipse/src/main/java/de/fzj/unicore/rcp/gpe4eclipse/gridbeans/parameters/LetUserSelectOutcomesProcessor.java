/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.parameters;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ISelectionValidator;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.clients.api.transfers.IGridFileSetTransfer;
import com.intel.gpe.clients.api.transfers.IGridFileTransfer;
import com.intel.gpe.clients.common.transfers.GridFileSetTransfer;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.AbstractProtocolProcessor;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;

import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.ui.LetUserSelectOutcomesDialog;

/**
 * This processor will open a dialog and ask the user which files he'd like to
 * download. In order not to bother the user with another dialog, the same
 * dialog can be used for setting the directory to which remote files should be
 * downloaded (this is normally done in the next step called
 * {@link ProcessingConstants#LET_USER_SELECT_DOWLOAD_DIR}).
 * 
 * @author demuth
 * 
 */
public class LetUserSelectOutcomesProcessor extends AbstractProtocolProcessor {

	public LetUserSelectOutcomesProcessor() {
		processingSteps.add(ProcessingConstants.LET_USER_SELECT_OUTCOMES);
	}

	public boolean matches(IGridBeanParameter param,
			IGridBeanParameterValue value, Map<String, Object> processorParams) {
		return (value instanceof ResourcesParameterValue);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.gridbeans.parameters.IProtocolProcessor#process(com.intel
	 * .gpe.clients.api.jsdl.gpe.GPEJob, int,
	 * com.intel.gpe.gridbeans.IGridBeanParameter,
	 * com.intel.gpe.gridbeans.IGridBeanParameterValue, java.util.Map)
	 */
	public void process(final Client client, GPEJob job, int processingStep,
			IGridBeanParameter param, IGridBeanParameterValue value,
			final Map<String, Object> processorParams,
			IProgressListener progress) throws Exception {
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			@SuppressWarnings("unchecked")
			public void run() {
				IGridFileSetTransfer<IGridFileTransfer> transfers = (IGridFileSetTransfer<IGridFileTransfer>) processorParams
						.get(ProcessingConstants.FILE_TRANSFERS);
				transfers = letUserSelect(client, transfers, processorParams);
				processorParams.put(ProcessingConstants.FILE_TRANSFERS,
						transfers);
			}

		});

	}

	public static IGridFileSetTransfer<IGridFileTransfer> letUserSelect(Client client,
			IGridFileSetTransfer<IGridFileTransfer> transfers, Map<String, Object> processorParams) {
		GridFileSetTransfer<IGridFileTransfer> result = new GridFileSetTransfer<IGridFileTransfer>();

		Shell parent = PlatformUI.getWorkbench().getDisplay().getActiveShell();
		if (parent == null) {
			parent = new Shell();
		}
		String downloadDir = (String) processorParams
				.get(ProcessingConstants.DOWNLOAD_DIR);
		if (downloadDir == null) {
			downloadDir = client.getFileFactory().getTemporaryDirName();
		}
		if (downloadDir != null) {
			File f = new File(downloadDir);
			if (!f.exists()) {
				f.mkdirs();
				IPath p = new Path(downloadDir);
				PathUtils.refreshEclipseFolder(p, 1, null);
			}
		}
		final LetUserSelectOutcomesDialog dialog = new LetUserSelectOutcomesDialog(
				parent, transfers, downloadDir);

		List<IGridFileTransfer> files = transfers.getResolvedFiles();
		if (files == null) {
			files = transfers.getFiles();
		}
		final List<IGridFileTransfer> preSelected = new ArrayList<IGridFileTransfer>();
		for (Object object : files) {
			IGridFileTransfer file = (IGridFileTransfer) object;
			if (file.getProcessedSource().getResolvedGridFile() != null) {
				preSelected.add(file);
			}
			String relative = file.getProcessedSource().getRelativePath();
			if (".".equals(relative) || "/".equals(relative)
					|| "\\".equals(relative)) {
				// user seemed to have exported the whole working dir
				// TODO make this check more robust
				file.getProcessedTarget().setRelativePath("working dir");
				file.getProcessedSource().setDisplayedString("working dir");
			}
		}

		if (preSelected.size() == 0) {
			GPEActivator.log(IStatus.ERROR, "No output files available.");
			return result; // no files available for download!
		}

		dialog.setValidator(new ISelectionValidator() {
			public String isValid(Object selection) {
				IGridFileTransfer transfer = (IGridFileTransfer) selection;
				if (!preSelected.contains(transfer)) {
					return "Invalid file in selection: "
							+ dialog.getLabelProvider().getText(transfer);
				}
				return null;
			}
		});

		dialog.setInitialElementSelections(preSelected);

		dialog.setBlockOnOpen(true);
		dialog.open();

		if (dialog.getReturnCode() == Window.OK) {
			downloadDir = dialog.getDownloadDir();
			if (downloadDir.length() > 0
					&& !downloadDir.endsWith(File.separator)) {
				downloadDir += File.separator;
			}
			processorParams.put(ProcessingConstants.DOWNLOAD_DIR, downloadDir);
			Object[] dialogResult = dialog.getResult();
			for (Object o : dialogResult) {
				IGridFileTransfer gridFileTransfer = (IGridFileTransfer) o;
				if (gridFileTransfer.getProcessedSource().getResolvedGridFile() != null) {
					result.addFile(gridFileTransfer);
				}
			}
		}
		return result;
	}

}
