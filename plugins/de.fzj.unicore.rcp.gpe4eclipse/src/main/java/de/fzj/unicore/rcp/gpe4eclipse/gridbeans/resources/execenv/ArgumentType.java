/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv;

/**
 * @author sholl
 * 
 */
public enum ArgumentType {

	ARGUMENT, OPTION, PRECOMMAND, POSTCOMMAND, USERPRECOMMAND, USERPOSTCOMMAND, USERPRECOMMAND_RUNONLOGINNODE, USERPOSTCOMMAND_RUNONLOGINNODE;

	public static ArgumentType create(ArgumentType type) {

		switch (type) {
		case ARGUMENT:
			return ArgumentType.ARGUMENT;
		case OPTION:
			return ArgumentType.OPTION;
		case PRECOMMAND:
			return ArgumentType.PRECOMMAND;
		case POSTCOMMAND:
			return ArgumentType.POSTCOMMAND;
		case USERPRECOMMAND:
			return ArgumentType.USERPRECOMMAND;
		case USERPOSTCOMMAND:
			return ArgumentType.USERPOSTCOMMAND;
		case USERPRECOMMAND_RUNONLOGINNODE:
			return ArgumentType.USERPRECOMMAND_RUNONLOGINNODE;
		case USERPOSTCOMMAND_RUNONLOGINNODE:
			return ArgumentType.USERPOSTCOMMAND_RUNONLOGINNODE;

		default:
			return null;

		}

	}

}
