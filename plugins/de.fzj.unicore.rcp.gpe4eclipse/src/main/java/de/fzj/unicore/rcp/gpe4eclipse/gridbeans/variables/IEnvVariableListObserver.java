package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables;

public interface IEnvVariableListObserver {

	/**
	 * Update the view to reflect the fact that an entry was added
	 * 
	 * @param out
	 */
	public void addVariable(EnvVariable variable);

	/**
	 * Update the view to reflect the fact that one of the entries has switched
	 * its position
	 * 
	 * @param out
	 */
	public void changeVariablePosition(EnvVariable variable, int newPosition);

	/**
	 * Update the view to reflect the fact that an entry was removed
	 * 
	 * @param out
	 */
	public void removeVariable(EnvVariable variable);

	/**
	 * Update the view to reflect the fact that one of the entries was modified
	 * 
	 * @param out
	 */
	public void updateVariable(EnvVariable variable);
}
