/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.commands;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.EnvVariable;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.EnvVariableList;

/**
 * @author demuth
 */
public class MoveVariableUpCommand extends Command {

	private EnvVariableList model;
	private EnvVariable variable;
	private int oldPosition;
	private int newPosition;

	public MoveVariableUpCommand(EnvVariableList model, EnvVariable variable) {
		this.model = model;
		this.variable = variable;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldPosition = model.getPosition(variable);
		newPosition = oldPosition - 1;
		redo();
	}

	@Override
	public void redo() {
		model.changeVariablePosition(variable, newPosition);

	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		model.changeVariablePosition(variable, oldPosition);
	}
}
