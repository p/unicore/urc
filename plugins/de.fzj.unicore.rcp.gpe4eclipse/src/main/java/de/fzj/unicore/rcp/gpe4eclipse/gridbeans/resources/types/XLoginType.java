package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types;

import com.intel.gpe.clients.api.jsdl.ResourceRequirementType;

public class XLoginType implements ResourceRequirementType {

	protected boolean enabled = false;
	protected String login;

	public XLoginType() {

	}

	public XLoginType(String login) {
		this.login = login;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof XLoginType)) {
			return false;
		}
		XLoginType other = (XLoginType) obj;
		if (enabled != other.enabled) {
			return false;
		}
		if (login == null) {
			if (other.login != null) {
				return false;
			}
		} else if (!login.equals(other.login)) {
			return false;
		}
		return true;
	}

	public String getLogin() {
		return login;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (enabled ? 1231 : 1237);
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setLogin(String name) {
		this.login = name;
	}

	@Override
	public String toString() {
		return enabled ? "XLoginType [login=" + login + "]" : "disabled";
	}

}
