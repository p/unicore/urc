/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.editors;

import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnvRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ui.ExecEnvViewer;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;

/**
 * @author sholl
 * 
 *         provides a Cell editor for execution environments and opens a new
 *         dialog for the detailed configuration of the execution environment.
 */
public class ExecEnvCellEditor extends DialogCellEditor {
	ExecEnvRequirementValue requirement;

	public ExecEnvCellEditor(Composite parent) {
		super(parent);
	}

	/**
	 * returns an object of ExecutionEnvironemtnRequirement
	 */
	@Override
	protected Object doGetValue() {

		return requirement;
	}

	@Override
	protected void doSetValue(Object value) {

		requirement = (ExecEnvRequirementValue) value;
		requirement = requirement.clone();
		super.doSetValue(requirement.getName().getLocalPart());
		
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {

		ExecEnvRequirementValue workingObject = requirement;
		ExecEnvViewer dialog = new ExecEnvViewer(ServiceBrowserActivator
				.getDefault().getWorkbench().getDisplay().getActiveShell(),
				workingObject);

		if (dialog.open() == Window.OK) {
			workingObject = dialog.getExecEnvRequirement();
		} else {
			workingObject = dialog.getBckup();
		}
		return workingObject;
	}

}
