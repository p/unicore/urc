/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.xml.namespace.QName;

import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

/**
 * @author demuth
 * 
 */
public class IDComboBoxCellEditor extends QNameComboBoxCellEditor {

	/**
	 * The available filesets to which a user defined file import/export may be
	 * added.
	 */
	List<IGridBeanParameter> filesets;
	StageList stageList;

	/**
	 * @param parent
	 */
	public IDComboBoxCellEditor(Composite parent, StageList stageList) {
		super(parent);

		this.stageList = stageList;

	}

	@Override
	protected Object doGetValue() {
		int i = comboBox.getSelectionIndex();
		if (i != -1) {
			return filesets.get(i);
		} else {
			return null;
		}
	}

	@Override
	protected void doSetValue(Object value) {
		Set<IGridBeanParameter> params = stageList.getFileSets().keySet();
		filesets = Arrays.asList(params.toArray(new IGridBeanParameter[params
				.size()]));
		Collections.sort(filesets, new Comparator<IGridBeanParameter>() {

			public int compare(IGridBeanParameter o1, IGridBeanParameter o2) {
				return o1.getDisplayedName().compareTo(o2.getDisplayedName());
			}
		});
		QName[] qnames = new QName[filesets.size()];
		int i = 0;

		for (IGridBeanParameter param : filesets) {
			qnames[i++] = param.getName();
		}
		setQNames(qnames);

		IGridBeanParameter fileset = (IGridBeanParameter) value;
		i = 0;
		for (IGridBeanParameter candidate : filesets) {
			if (fileset.equals(candidate)) {
				selection = i;
				if (comboBox != null) {
					comboBox.select(selection);
				}
				return;
			}
			i++;
		}
	}

}
