package de.fzj.unicore.rcp.gpe4eclipse;

import java.net.URI;
import java.net.URISyntaxException;

import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.clients.impl.SecurityProvider;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import eu.unicore.util.httpclient.IClientConfiguration;

public class SWTSecurityProvider implements SecurityProvider {

	public IClientConfiguration getDefaultSecurityProperties() {
		try{
			return ServiceBrowserActivator.getDefault().getUASSecProps(new URI("http://anonymous"));
		}catch(URISyntaxException u){
			// can't happen
			return null;
		}
	}

	public IClientConfiguration getSecurityProperties(
			EndpointReferenceType epr) {
		return ServiceBrowserActivator.getDefault().getUASSecProps(epr);
	}

}
