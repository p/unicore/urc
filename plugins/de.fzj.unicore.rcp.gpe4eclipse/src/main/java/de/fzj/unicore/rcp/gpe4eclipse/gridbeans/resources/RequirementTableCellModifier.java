/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.swt.widgets.TableItem;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IResourceRequirementCellEditorExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.editors.RequirementCellEditor;

/**
 * is the cellModifier for the ResourceRequirements
 * 
 * @author Christian Hohmann
 * 
 */
public class RequirementTableCellModifier implements ICellModifier {

	private ResourceRequirementsViewer resourceRequirementsViewer;

	public RequirementTableCellModifier(
			ResourceRequirementsViewer resourceRequirementsViewer) {
		this.resourceRequirementsViewer = resourceRequirementsViewer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object,
	 * java.lang.String)
	 */

	/**
	 * links the correct cellEditor to the value that should be modified
	 */
	public boolean canModify(Object element, String property) {
		if (!(element instanceof ResourceRequirement)) {
			return false;
		}
		ResourceRequirement requirement = (ResourceRequirement) element;

		if (ResourceRequirementsViewer.ENABLED.equals(property)) {
			return true;
		} else if (ResourceRequirementsViewer.VALUE.equals(property)) {

			int column = ResourceRequirementsViewer.getColumnFor(property);
			CellEditor editor = resolveCellEditor(requirement.getValue()
					.getType());
			if (editor == null) {
				return false;
			}
			resourceRequirementsViewer.setCellEditor(column, editor);
			return true;
		} else if (ResourceRequirementsViewer.UNITS.equals(property)) {
			int column = ResourceRequirementsViewer.getColumnFor(property);
			resourceRequirementsViewer.setCellEditor(column,
					new UnitCellEditor(resourceRequirementsViewer.getTable()));
			return requirement.getValue().getSelectableUnits() != null
					&& requirement.getValue().getSelectableUnits().length > 1;
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object,
	 * java.lang.String)
	 */
	public Object getValue(Object element, String property) {

		ResourceRequirement resourceRequirement = (ResourceRequirement) element;
		Object result = null;

		if (property.equals(ResourceRequirementsViewer.ENABLED)) {
			result = resourceRequirement.isEnabled();
		}
		if (property.equals(ResourceRequirementsViewer.NAME)) {
			result = resourceRequirement.getRequirementName();
		}
		if (property.equals(ResourceRequirementsViewer.VALUE)) {
			result = resourceRequirement.getValue();
		}
		if (property.equals(ResourceRequirementsViewer.UNITS)) {
			result = resourceRequirement;
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object,
	 * java.lang.String, java.lang.Object)
	 */
	public void modify(Object element, String property, Object value) {

		if (value == null) {
			return;
		}
		if (element instanceof TableItem) {
			resourceRequirementsViewer.getResourceRequirementList()
					.modifyValue(element, property, value);
		}
	}

	/**
	 * 
	 * @param requirementType
	 *            identifies the requirement that should be changed
	 * @return the fitting CellEditor
	 */
	private CellEditor resolveCellEditor(QName requirementType) {

		/*
		 * go over all registered extensions and collect the
		 * resourceRequirementCellEditor links in the list
		 */

		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(GPE4EclipseConstants.RESOURCEREQUIREMENT_CELLEDITOR_EXTENSION_POINT);
		IConfigurationElement[] members = extensionPoint
				.getConfigurationElements();
		RequirementCellEditor rpce = null;
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			try {
				IResourceRequirementCellEditorExtensionPoint ext = (IResourceRequirementCellEditorExtensionPoint) member
						.createExecutableExtension("name");
				rpce = ext.getResourceRequirementCellEditor(
						resourceRequirementsViewer.getTable(), requirementType);
				if (rpce != null) {
					break;
				}
			} catch (CoreException ex) {
				GPEActivator
						.log("Could not determine all available CellEditors for resource requirements",
								ex);
			}
		}
		return rpce.getCellEditor();
	}
}