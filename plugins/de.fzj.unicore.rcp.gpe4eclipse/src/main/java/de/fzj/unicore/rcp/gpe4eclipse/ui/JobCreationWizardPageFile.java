/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;

/**
 * Wizard page for selecting the job file within the workspace
 * 
 * @author demuth
 */
public class JobCreationWizardPageFile extends WizardPage implements Listener,
		PropertyChangeListener {

	public static final String NAME = "projectPage";
	protected JobCreationInfo jobCreationInfo;
	protected boolean listeningToCreationInfo = true;
	protected boolean listeningToResourceGroup = true;
	protected boolean userChangedFileName = false;
	protected boolean userChangedProjectName = false;

	protected SelectFileAndProjectGroup resourceGroup;

	public JobCreationWizardPageFile(JobCreationInfo jobCreationInfo) {
		super(NAME);
		setTitle("Choose a location for the job file");
		// this.setImageDescriptor(ImageDescriptor.createFromFile(WFActivator.class,"images/flowbanner.gif"));
		this.jobCreationInfo = jobCreationInfo;
		jobCreationInfo.addPropertyChangeListener(this);
	}

	public void createControl(Composite parent) {
		initializeDialogUnits(parent);
		// top level group
		Composite topLevel = new Composite(parent, SWT.NONE);
		topLevel.setLayout(new GridLayout());
		topLevel.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_FILL
				| GridData.HORIZONTAL_ALIGN_FILL));
		topLevel.setFont(parent.getFont());

		writeFileNameToGroup();
		writeProjectNameToGroup();

		// resource and container group
		resourceGroup = new SelectFileAndProjectGroup(topLevel, this,
				"File name:", "project", false, 250);
		resourceGroup.setAllowExistingResources(false);

		// initialPopulateContainerNameField();
		// createAdvancedControls(topLevel);
		// if (initialFileName != null) {
		// resourceGroup.setResource(initialFileName);
		// }
		// if (initialFileExtension != null) {
		// resourceGroup.setResourceExtension(initialFileExtension);
		// }
		validatePage();
		// Show description on opening
		setErrorMessage(null);
		setMessage(null);
		setControl(topLevel);
	}

	protected void enableFolderButton(boolean enable) {

	}

	public boolean finish() {
		jobCreationInfo.removePropertyChangeListener(this);
		return true;
	}

	

	public void handleEvent(Event event) {
		if (!listeningToResourceGroup) {
			return;
		}
		if (event.widget == resourceGroup.getResourceNameField()) {
			userChangedFileName = true;
		} else if (event.widget == resourceGroup.getContainerNameField()) {
			userChangedProjectName = true;
		}
		if (getControl() != null) {
			setPageComplete(validatePage());
		}
	}

	public void propertyChange(PropertyChangeEvent evt) {
		if (!listeningToCreationInfo) {
			return;
		}
		if ((JobCreationInfo.PROP_APPLICATION.equals(evt.getPropertyName()) || JobCreationInfo.PROP_RESOURCES
				.equals(evt.getPropertyName()))) {
			updateFile();
		} else if (JobCreationInfo.PROP_FILE.equals(evt.getPropertyName())) {
			writeFileNameToGroup();
			writeProjectNameToGroup();
		}

	}

	protected void updateFile() {
		if (userChangedFileName && userChangedProjectName) {
			return;
		}
		String appName = jobCreationInfo.getGridBeanInfo() == null ? "unknown"
				: jobCreationInfo.getGridBeanInfo().getGridBeanName();
		boolean singleResource = jobCreationInfo.getResources() != null
				&& jobCreationInfo.getResources().size() == 1;
		String resourceName = null;
		if (singleResource) {
			DetailsAdapter details = (DetailsAdapter) jobCreationInfo
					.getResources().get(0).getAdapter(DetailsAdapter.class);
			if (details != null) {
				resourceName = details.getName();
			}
		}
		String timeStamp = new SimpleDateFormat(
				Constants.DATE_FORMAT_WITHOUT_COLON_PATTERN).format(Calendar
				.getInstance().getTime());

		String name = appName;
		if (resourceName != null) {
			name += "@" + resourceName;
		}
		name += "_created_at_" + timeStamp;
		IPath project = jobCreationInfo.getFile().uptoSegment(1);
		if (!userChangedProjectName) {
			project = ResourcesPlugin.getWorkspace().getRoot().getFullPath()
					.append(name);
		}
		IPath relative = jobCreationInfo.getFile().removeFirstSegments(1);
		if (!userChangedFileName) {
			relative = new Path(appName);
		}
		IPath file = project.append(relative);
		listeningToCreationInfo = false;
		try {
			jobCreationInfo.setFile(file);
			if (!userChangedFileName) {
				writeFileNameToGroup();
			}
			if (!userChangedProjectName) {
				writeProjectNameToGroup();
			}
		} finally {
			listeningToCreationInfo = true;
		}
	}

	protected boolean validatePage() {
		setMessage(null);
		boolean valid = true;
		if (!resourceGroup.areAllValuesValid()) {
			int problemType = resourceGroup.getProblemType();
			if (problemType == SelectFileAndProjectGroup.PROBLEM_PROJECT_DOES_NOT_EXIST) {
				setMessage("The project "
						+ resourceGroup.getContainerFullPath().segment(0)
						+ " will be created.");
			} else if (!(problemType == SelectFileAndProjectGroup.PROBLEM_RESOURCE_EMPTY || problemType == SelectFileAndProjectGroup.PROBLEM_CONTAINER_EMPTY)) {
				setErrorMessage(resourceGroup.getProblemMessage());
				valid = false;
			}

		}
		String resourceName = resourceGroup.getResource();
		if (valid) {
		
			if (resourceName == null || resourceName.trim().length() == 0) {
				setErrorMessage("Please provide a name for the job file.");
				valid = false;
			}
		}
		if (valid) {
			setErrorMessage(null);
			if (resourceGroup.getContainerFullPath() != null) {
				jobCreationInfo.setFile(resourceGroup.getContainerFullPath()
						.append(resourceName));
			}
		}
		return valid;
	}

	protected void writeFileNameToGroup() {
		IPath file = jobCreationInfo.getFile();
		if (file != null && resourceGroup != null) {
			listeningToResourceGroup = false;
			try {
				if (!CollectionUtil.equalOrBothNull(
						resourceGroup.getResource(), file.lastSegment())) {
					resourceGroup.setResource(file.lastSegment());
				}
			} finally {
				listeningToResourceGroup = true;
			}

		}
	}

	protected void writeProjectNameToGroup() {
		IPath file = jobCreationInfo.getFile();
		if (file != null && resourceGroup != null) {
			listeningToResourceGroup = false;
			try {
				IPath path = file.uptoSegment(file.segmentCount() - 1);
				if (!CollectionUtil.equalOrBothNull(
						resourceGroup.getContainerFullPath(), path)) {
					resourceGroup.setContainerFullPath(path);
				}

			} finally {
				listeningToResourceGroup = true;
			}
		}
	}

}
