package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.api.Application;
import com.intel.gpe.clients.api.TargetSystemClient;
import com.intel.gpe.clients.api.async.IProgressListener;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.SWTProgressListener;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.IServiceContentProvider;
import de.fzj.unicore.rcp.servicebrowser.filters.SkipNodeViewerFilter;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceTreeViewer;

public class ApplicationResourceFilter extends SkipNodeViewerFilter {

	class Updater extends BackgroundJob {

		public Updater() {
			super("updating matching target systems");

		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {

			IProgressListener progress = new SWTProgressListener(monitor);
			progress.beginTask("matchmaking", 3);
			try {
				runAgain = false;
				updateScheduled = false;

				checkApplicationSupported();
				if (filterController != null) {
					PlatformUI.getWorkbench().getDisplay()
							.asyncExec(new Runnable() {
								public void run() {
									filterController.refreshViewers();
									serviceTreeViewer.expandAll();
								}
							});
				}

			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, "Error while filtering suitable target systems by selected application: "+e.getMessage(), e);
			} finally {
				progress.done();
				if (runAgain) {
					schedule();
				}
			}

			return Status.OK_STATUS;
		}
	}

	ServiceTreeViewer serviceTreeViewer;
	Application application;
	Updater updater = new Updater();

	boolean runAgain = false;
	boolean updateScheduled = false;
	Set<Node> matchingObjects = null;
	Set<Node> refusedObjects = null;

	boolean firstRunComplete = false;

	public ApplicationResourceFilter(Application app,
			ServiceTreeViewer serviceTreeViewer) {
		super("application resource filter", true);
		this.application = app;
		this.serviceTreeViewer = serviceTreeViewer;

	}

	protected void checkApplicationSupported() {

		try {
			active = false;
			matchingObjects = new HashSet<Node>();
			refusedObjects = new HashSet<Node>();
			if (application != null) {
				IServiceContentProvider cp = serviceTreeViewer
						.getServiceContentProvider();
				if (cp == null) {
					return;
				}
				GridNode node = serviceTreeViewer.getGridNode();
				if (node == null) {
					return;
				}
				Node root = node.getParent();
				if (root == null) {
					return;
				}
				Object[] all = cp.getSubtreeElements(root);
				for (Object element : all) {

					if (!(element instanceof Node)) {
						continue;
					}
					Node n = (Node) element;
					if (nodeMatches(n)) {
						matchingObjects.add(n);
					} else {
						refusedObjects.add(n);
					}
				}
			}
			active = true;
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while filtering suitable target systems by selected application: "+e.getMessage(), e);
		}
		firstRunComplete = true;
	}

	public void dispose() {
		serviceTreeViewer = null;
		matchingObjects = null;
		refusedObjects = null;
	}

	protected boolean nodeMatches(Node node) throws Exception {
		boolean ok = true;
		if (node.getParent() != null
				&& refusedObjects.contains(node.getParent())) {
			return false;
		}
		TargetSystemClient client = (TargetSystemClient) node
				.getAdapter(TargetSystemClient.class);
		if (client == null || application == null) {
			ok = true;
		} else {
			ok = client.supportsApplication(application.getName(),
					application.getApplicationVersion());
		}
		return ok;
	}

	public void scheduleUpdate() {
		// only run ONE instance of the update job at the same time!
		synchronized (updater) {
			if (updater.getState() == org.eclipse.core.runtime.jobs.Job.NONE) {
				updater.schedule(100);
			} else {
				runAgain = true;
			}
		}
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if (!(element instanceof Node)) {
			return false;
		}
		if (!isActive() || matchingObjects == null) {
			return true;
		}
		if (matchingObjects.contains(element)) {
			return true;
		} else if (refusedObjects.contains(element)) {
			return false;
		} else {
			// haven't seen this node before.. take a closer look
			Node n = (Node) element;
			boolean matches = false;
			try {
				matches = nodeMatches(n);
			} catch (Exception e) {
				// do nothing
			}
			if (matches) {
				matchingObjects.add(n);
				return true;
			} else {
				refusedObjects.add(n);
				return false;
			}
		}

	}

	public void setApplication(Application application) {
		this.application = application;
	}

}
