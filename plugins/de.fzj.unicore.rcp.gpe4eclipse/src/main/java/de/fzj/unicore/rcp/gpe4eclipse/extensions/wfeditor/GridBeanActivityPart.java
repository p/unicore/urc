/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import java.beans.PropertyChangeEvent;
import java.net.URL;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditor;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditorInput;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.parts.SimpleActivityPart;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;
import de.fzj.unicore.rcp.wfeditor.ui.WFGraphicalViewer;

/**
 * @author demuth
 */
public class GridBeanActivityPart extends SimpleActivityPart {

	public static final String REQ_SHOW_GRID_BEAN_VIEW = "show GridBean request";

	public static final String CMD_SHOW_GRID_BEAN_VIEW = "show GridBean";

	public static final String REQ_FETCH_OUTCOMES = "fetch job outcomes request";

	public static final String CMD_FETCH_OUTCOMES = "fetch job outcomes";

	public static final String POLICY_FETCH_OUTCOMES = "fetchOutcome";

	private boolean removed = false;

	public GridBeanActivityPart(GridBeanActivity activity) {
		this.setModel(activity);
	}

	@Override
	public void activate() {
		super.activate();
		final JobEditorInput input = new JobEditorInput(getGridBeanActivity());
		final WFGraphicalViewer viewer = getViewer();
		if (viewer == null) {
			return;
		}
		Control c = viewer.getControl();
		if (c == null) {
			return;
		}
		Display d = c.getDisplay();
		if (d == null) {
			return;
		}

		d.asyncExec(new Runnable() {

			public void run() {
				// force initialization on open job editors that have not been
				// initialized yet
				// this is needed in order to associate the wfeditor with
				// persisted job editors
				// otherwise the old job editor is not closed when deleting the
				// corresponding
				// GridBeanActivity
				viewer.getEditor().getEditorSite().getPage().findEditor(input);
			}
		});

	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		installEditPolicy(WFConstants.POLICY_OPEN, new ShowGridBeanViewPolicy());
		installEditPolicy(POLICY_FETCH_OUTCOMES, new FetchOutcomePolicy());
	}

	@Override
	public void deactivate() {
		final WFEditor wfeditor = getViewer().getEditor();

		super.deactivate();
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {

			public void run() {

				if (!wfeditor.isDisposed()
						&& wfeditor.getDiagram().getActivityByName(
								getModel().getName()) != null) {
					return; // part has been re-added!
				}
				IWorkbenchPage page = null;
				try {
					page = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage();
				} catch (Exception e) {
					return;
				}
				try {
					if (page == null) {
						return;
					}
					JobEditor editor = getJobEditor();
					if (editor != null) {
						boolean askForSave = !(removed || wfeditor.isDisposed()
								&& wfeditor.isDirty()); // do not ask for saving
														// if the parent diagram
														// was closed without
														// saving
						page.closeEditor(editor, askForSave);
					}
				} catch (Exception e) {
					GPEActivator.log(IStatus.WARNING,
							"Could not close job editor!", e);
				}

			}
		});

	}

	protected GridBeanActivity getGridBeanActivity() {
		return (GridBeanActivity) getModel();

	}

	protected JobEditor getJobEditor() {
		try {
			return (JobEditor) getGridBeanActivity().getGridBeanClient()
					.getGridBeanInputPanel().getComponent();
		} catch (NullPointerException e) {
			return null;
		}

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (GridBeanActivity.GRID_BEAN_INFO.equals(evt.getPropertyName())) {
			PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
				public void run() {
					activityFigure.setImage(getImageDescriptor());
				}
			});

		} else if (IFlowElement.PROP_NAME.equals(evt.getPropertyName())) {
			PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
				public void run() {
					JobEditor editor = getJobEditor();
					if (editor != null) {
						editor.updatePartName();
					}
				}
			});
		}
		super.propertyChange(evt);
	}

	@Override
	public void removeNotify() {
		super.removeNotify();
		removed = true;

	}
	

	@Override
	public ImageDescriptor getImageDescriptor() {
		ImageDescriptor imageDescr = null;
		try {
			GridBeanActivity act = getGridBeanActivity();
			if (act.isGridBeanSelected()) {
				GridBeanInfo gridbeanInfo = (GridBeanInfo) act.getPropertyValue(GridBeanActivity.GRID_BEAN_INFO);
				URL iconUrl = GPEActivator.getDefault().getGridBeanRegistry()
				.getIconURLFor(gridbeanInfo);
				if (iconUrl != null) {
					imageDescr = ImageDescriptor.createFromURL(iconUrl);
				}
			}
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Unable to load application GUI icon: "+e.getMessage(),e);
		}
		if (imageDescr == null) {
			imageDescr = GPEActivator
			.getImageDescriptor("unknown_gridbean.gif");
		}
		return imageDescr;
	}

}
