/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.actions;

import java.util.List;

import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IWorkbenchPart;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GridBeanActivityPart;
import de.fzj.unicore.rcp.wfeditor.actions.WFEditorAction;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.requests.WFEditorRequest;

/**
 */
public class ShowGridBeanViewAction extends WFEditorAction {
	/**
	 * Constructs a <code>ShowGridBeanViewAction</code> using the specified
	 * part.
	 * 
	 * @param part
	 *            The part for this action
	 */
	public ShowGridBeanViewAction(IWorkbenchPart part) {
		super(part);
		setLazyEnablementCalculation(true);
	}

	/**
	 * Returns <code>true</code> if the selected object is of type
	 * GridBeanActivityPart.
	 * 
	 * @return <code>true</code> if the command should be enabled
	 */
	@Override
	protected boolean calculateEnabled() {
		Command cmd = createCommand(getSelectedObjects());
		if (cmd == null) {
			return false;
		}
		return cmd.canExecute();
	}

	public Command createCommand(List<?> objects) {
		if (objects.isEmpty() || objects.size() > 1) {
			return null;
		}
		if (!(objects.get(0) instanceof GridBeanActivityPart)) {
			return null;
		}
		GridBeanActivityPart part = (GridBeanActivityPart) objects.get(0);
		Request req = new WFEditorRequest(
				GridBeanActivityPart.REQ_SHOW_GRID_BEAN_VIEW) {
			@Override
			public boolean isExecutable(WorkflowDiagram diagram) {
				return true;
			}
		};
		return part.getCommand(req);

	}

	/**
	 * Initializes this action's text and images.
	 */
	@Override
	protected void init() {
		super.init();
		setText("Open Application");
		setToolTipText("Opens a new view for displaying and editing application parameters.");
		setId(getClass().getName());
		setImageDescriptor(GPEActivator.getImageDescriptor("script_edit.png"));
		setDisabledImageDescriptor(GPEActivator
				.getImageDescriptor("script_edit.png"));
		setEnabled(false);
	}

	@Override
	public void run() {
		Command cmd = createCommand(getSelectedObjects());
		execute(cmd);
	}

}
