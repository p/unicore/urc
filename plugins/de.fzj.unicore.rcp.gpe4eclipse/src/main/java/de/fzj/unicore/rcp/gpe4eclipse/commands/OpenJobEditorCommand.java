/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;

import de.fzj.unicore.rcp.common.guicomponents.EnhancedWizardDialog;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.ISWTClient;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobCreationWizard;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditor;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditorInput;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

/**
 * @author demuth
 */
public class OpenJobEditorCommand extends Command {

	private GridBeanClient<IWorkbenchPart> gridBeanClient;
	protected IAdaptable adaptable;
	protected IServiceViewer viewer;
	protected JobCreationWizard wizard;

	public OpenJobEditorCommand(IAdaptable adaptable) {
		this.adaptable = adaptable;
	}

	public OpenJobEditorCommand(IAdaptable adaptable, IServiceViewer viewer) {
		this.adaptable = adaptable;
		this.viewer = viewer;
	}

	@Override
	public boolean canUndo() {
		return false;
	}

	protected GridBeanContext createGridBeanContext() {
		return GridBeanUtils.createSolitaryContext();
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		redo();
	}

	public IAdaptable getAdaptable() {
		return adaptable;
	}

	public GridBeanClient<IWorkbenchPart> getGridBeanClient() {
		if (gridBeanClient == null) {
			ISWTClient client = GPEActivator.getDefault().getClient();
			gridBeanClient = client.createGridBeanClient();
		}
		return gridBeanClient;
	}

	protected JobEditorInput getJobEditorInput() {
		return new JobEditorInput(getGridBeanClient(), getJobPath());
	}

	protected IPath getJobPath() {
		return wizard.getFile();
	}

	protected void openEditor() {
		try {
			IWorkbenchPage workbenchPage = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage();
			workbenchPage.openEditor(getJobEditorInput(), JobEditor.ID);
		} catch (PartInitException e) {
			GPEActivator.log(IStatus.ERROR, "Unable to open job editor", e);
		}
	}

	@Override
	public void redo() {
		wizard = runWizard();
	}

	/**
	 * Runs the {@link JobCreationWizard} and returns it for results
	 */
	protected JobCreationWizard runWizard() {
		Shell shell = viewer == null ? new Shell(SWT.RESIZE) : viewer
				.getControl().getShell();
		JobCreationWizard wizard = new JobCreationWizard();
		StructuredSelection selection = new StructuredSelection();
		wizard.init(PlatformUI.getWorkbench(), selection);
		List<IAdaptable> resources = new ArrayList<IAdaptable>();
		resources.add(adaptable);
		wizard.setResources(resources);
		EnhancedWizardDialog dialog = new EnhancedWizardDialog(shell, wizard);
		int result = dialog.open();
		if (result == IStatus.OK) {
			return wizard;

		} else {
			return null;
		}
	}

	public void setAdaptable(IAdaptable adaptable) {
		this.adaptable = adaptable;
	}

	public void setGridBeanClient(GridBeanClient<IWorkbenchPart> gbClient) {
		this.gridBeanClient = gbClient;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {

	}

}
