/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import com.intel.gpe.gridbeans.plugins.ValueChangeEvent;
import com.intel.gpe.gridbeans.plugins.swt.controls.SWTDataControler;
import com.intel.gpe.util.collections.CollectionUtil;

/**
 * @author sholl
 * 
 *         Stores the links of datacontrols and widgets and manages if the value
 *         of a widget is modified.
 * 
 */

public class ExecEnvDataControler extends SWTDataControler {

	ExecEnv execEnv;
	private List<ExecEnvArgument<?>> arguments = new ArrayList<ExecEnvArgument<?>>();

	public ExecEnvDataControler() {

	}

	public ExecEnvDataControler(ExecEnv execEnv) {
		this.arguments = execEnv.getArguments();
		this.execEnv = execEnv;
	}

	@Override
	public boolean equals(Object o) {

		if (!(o instanceof ExecEnvDataControler)) {
			return false;
		}
		ExecEnvDataControler other = (ExecEnvDataControler) o;

		return CollectionUtil.equalOrBothNull(execEnv, other.execEnv)
				&& CollectionUtil.containSameElements(arguments,
						other.arguments)
				&& CollectionUtil.containSameElements(controlMap.values(),
						other.controlMap.values())
				&& CollectionUtil.containSameElements(controlMap.keySet(),
						other.controlMap.keySet());
	}

	public List<ExecEnvArgument<?>> getArguments() {
		return arguments;
	}

	public ExecEnv getExecEnv() {
		return execEnv;
	}

	@Override
	public void set(QName key, Object value) {

		ExecEnvArgument<?> elem = execEnv.getArgument(key);

		Object translatedValue = "";
		try {
			translatedValue = getControl(key).getTranslatedValue(value);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ExecEnvArgument<? extends Object> e = elem.clone();
		e.setArgumentValue(translatedValue);
		execEnv.setArgument(key, e);

	}

	public void setArguments(List<ExecEnvArgument<?>> arguments) {
		this.arguments = arguments;
	}

	public void setExecEnv(ExecEnv execEnv) {
		this.execEnv = execEnv;
	}

	@Override
	public void valueChanged(ValueChangeEvent evt) {
		super.valueChanged(evt);

		execEnv.setDirty(true);

	}

}
