package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;
import com.intel.gpe.gridbeans.parameters.SiteSpecificResourceRequirement;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.ResourceRequirement;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.resources.ExecEnvType;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.resources.ExecEnvValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.resources.ExecEnvValueType;

/**
 * @author sholl
 * 
 *         Writes the abstract execution environment to the gridbean execution
 *         value, that is serialized. Additionally, it restores the serialized
 *         values from the gridbean execution value to the abstract execution
 *         environment that is used for user modifications.
 * 
 */

public class ExecEnvRequirementSettings extends ResourceRequirement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3674673236104534825L;

	private ExecEnvArgument<?> findArgument(List<ExecEnvArgument<?>> arguments,
			QName name) {

		for (ExecEnvArgument<?> execEnvArgument : arguments) {
			if (execEnvArgument.getName().equals(name)) {
				return execEnvArgument;
			}
		}

		return null;
	}

	private ExecEnv findExecEnv(List<ExecEnv> execEnvs, QName name, String version) {

		for (ExecEnv execEnv : execEnvs) {
			
			if (CollectionUtil.equalOrBothNull(execEnv.getName(),name)
				&& (version == null || version.equals(execEnv.getVersion()))) {
				return execEnv;
			}
		}

		return null;
	}

	private int findExEnvPosition(ExecEnvType execEnvType,
			List<ExecEnv> execEnvs) {
		if(execEnvType == null || execEnvType.getName() == null) return 0;
		for (int i = 0; i < execEnvs.size(); i++) {
			String version = execEnvType.getVersion();
			if (CollectionUtil.equalOrBothNull(execEnvType.getName(),execEnvs.get(i).getName())
				&& (version == null || version.equals(execEnvs.get(i).getVersion()))) {
				return i;
			}

		}
		return -1;
	}

	@Override
	public void loadFrom(ResourcesParameterValue value) {
		if (value.getSiteSpecificRequirements() == null) {
			return;
		}

		List<SiteSpecificResourceRequirement> siteSpecificRequirements = value
				.getSiteSpecificRequirements();

		for (SiteSpecificResourceRequirement req : siteSpecificRequirements) {

			if (req.getName().equals(getRequirementName())) {
				
				ExecEnvRequirementValue val = (ExecEnvRequirementValue) getValue();
				List<ExecEnv> fromGUI = val.getExecEnvs();

				ExecEnvValueType model = (ExecEnvValueType) req.getRequirement();
				boolean enabled = model.isEnabled();
				List<ExecEnvType> fromModel = model.getExecEnv();
				int selected = model.getSelected();
				selected = model.getSelected();
				if(selected >= 0 && selected < fromModel.size())
				{
					ExecEnvType oldSelectedEnv = fromModel.get(selected);
					selected = findExEnvPosition(oldSelectedEnv, fromGUI);
					if(selected < 0) 
					{
						enabled = false;
						selected = 0;
					}
					val.setSelected(selected);
					val.setSelectedVersion(oldSelectedEnv.getVersion());
				}
				else
				{
					enabled = false;
				}

				for (Iterator<ExecEnvType> it = fromModel.iterator(); it
						.hasNext();) {
					ExecEnvType execEnvType = it.next();
					ExecEnv found = findExecEnv(fromGUI, execEnvType.getName(), execEnvType.getVersion());
					if (found == null) {
						// this persisted Execution Environment is not available
						// on currently selected system
						continue;
					}

					Map<QName, ExecEnvValue<?>> valuereferences = execEnvType
							.getValuereferences();
					List<ExecEnvArgument<?>> newArguments = found.getArguments();
					found.setDirty(true);

					for (Iterator<QName> iterator = valuereferences.keySet()
							.iterator(); iterator.hasNext();) {
						QName qName = iterator.next();
						ExecEnvArgument<?> foundArgument = findArgument(newArguments, qName);
						if (foundArgument == null) {
							// this persisted argument is not present in the
							// description of the execution environment on the
							// selected system
							iterator.remove();
							continue;
						}
						ExecEnvValue<?> oldArgument = valuereferences
								.get(qName);
						foundArgument.setArgumentValue(oldArgument.getValue());
						foundArgument.setType(oldArgument.getType());
						foundArgument.setEnabled(oldArgument.isEnabled());
					}
				}
				
				setEnabled(enabled);
				setDirty(enabled);
				break;
			}

		}

	}


	@Override
	public void writeTo(ResourcesParameterValue value) {

		// we used to store all dirty execution environments, but this is 
		// difficult and not really needed
		// => just store the selected one
		ExecEnvRequirementValue val = (ExecEnvRequirementValue) getValue();
		List<ExecEnvType> list = new ArrayList<ExecEnvType>();
		ExecEnv execEnv = val.getSelectedExecEnv();
		if(execEnv != null)
		{
			ExecEnvType selectedEnv = createExecEnvType(execEnv,val.getSelectedVersion());
			list.add(selectedEnv);
		}
		

		ExecEnvValueType type = new ExecEnvValueType(list, val.getName()
				.getLocalPart(), 0);
		type.setEnabled(isEnabled());
		SiteSpecificResourceRequirement req = new SiteSpecificResourceRequirement(
				getRequirementName(), type);
		
		value.removeSiteSpecificRequirement(req);
		if (list.size() > 0 && (isEnabled() || isDirty())) {
			value.addSiteSpecificRequirement(req);
		}
	}
	
	private ExecEnvType createExecEnvType(ExecEnv execEnv, String version)
	{
		ExecEnvType type = new ExecEnvType(execEnv.getName(),version);
		List<ExecEnvArgument<?>> arguments = execEnv.getArguments();
		Map<QName, ExecEnvValue<?>> args = new HashMap<QName, ExecEnvValue<?>>();
		for (ExecEnvArgument<?> execEnvArgument : arguments) {

			ExecEnvValue<?> valu = ExecEnvUtils
					.getValueforArgument(execEnvArgument);
			boolean enabled = execEnvArgument.isEnabled();
			valu.setEnabled(enabled);
			valu.setType(execEnvArgument.getType());
			args.put(execEnvArgument.getName(), valu);
		}
		type.setValuereferences(args);
		return type;
	}

}