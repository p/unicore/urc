package de.fzj.unicore.rcp.gpe4eclipse.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.intel.gpe.clients.all.gridbeans.InternalGridBean;
import com.intel.gpe.clients.api.Application;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.GridBeanRegistry;

public class JobCreationInfo {

	public static final String PROP_APPLICATION = "Job Creation Info Application",
			PROP_FILE = "Job Creation Info File",
			PROP_RESOURCES = "Job Creation Info Resources";

	private IPath file;

	private GridBeanInfo gridBeanInfo;

	private List<IAdaptable> resources = new ArrayList<IAdaptable>();

	private List<PropertyChangeListener> listeners = new ArrayList<PropertyChangeListener>();

	public JobCreationInfo() {
		file = new Path("");
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		listeners.add(l);
	}
	
	/** 
	 * This method notifies the listeners of wizard pages when some properties are changed.
	 * It is important to know that from which page the event is being fired. In the previous version, just all listeners
	 * were notified on a property change which leads to Bug #665. When a target resource was selected, it changed the
	 * GridBeanInfo value on first page (applicationPage) to null and caused a NPE 
	 * @param evt
	 */
	protected void fireEvent(PropertyChangeEvent evt) { 
		
		// If an event is fired from first or second page, notifies all listeners of property change
		if ((evt.getPropertyName().equals(PROP_APPLICATION)) || evt.getPropertyName().equals(PROP_FILE)) {	
			for (PropertyChangeListener l : listeners) {
				l.propertyChange(evt);
			}
			
			// Event is triggered from third page(resourcePage), do not notify the listeners from applicationPage
			// Otherwise it sets the value of GridBeanInfo to null (Bug #665). Only change the properties of second and third page.
		} else {
			for (PropertyChangeListener l : listeners) {
				if (l.toString().equals(JobCreationWizardPageFile.NAME)) 
					l.propertyChange(evt);
				else if (l.toString().equals(JobCreationWizardPageResource.NAME))
					l.propertyChange(evt);
			}
		}
	}

	public Application getApplication() {
		if (getGridBeanInfo() == null) {
			return null;
		}
		GridBeanRegistry registry = GPEActivator.getDefault()
				.getGridBeanRegistry();
		InternalGridBean gb = registry.getGridBean(getGridBeanInfo());
		return gb.getSupportedApplication();
	}

	public IPath getFile() {
		return file;
	}

	public GridBeanInfo getGridBeanInfo() {
		return gridBeanInfo;
	}

	public List<IAdaptable> getResources() {
		return resources;
	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		listeners.remove(l);
	}

	public void setFile(IPath file) {
		Object old = this.file;
		this.file = file;
		fireEvent(new PropertyChangeEvent(this, PROP_FILE, old, file));
	}

	public void setGridBeanInfo(GridBeanInfo gridBeanInfo) {
		Object old = this.gridBeanInfo;
		this.gridBeanInfo = gridBeanInfo;
		fireEvent(new PropertyChangeEvent(this, PROP_APPLICATION, old,
				gridBeanInfo));
	}

	public void setResources(List<IAdaptable> resources) {
		Object old = this.resources;
		this.resources = resources;
		fireEvent(new PropertyChangeEvent(this, PROP_RESOURCES, old, resources));
	}

}
