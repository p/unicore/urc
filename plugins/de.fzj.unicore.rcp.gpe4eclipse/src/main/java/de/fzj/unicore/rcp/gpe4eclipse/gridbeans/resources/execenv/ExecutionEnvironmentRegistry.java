package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import javax.xml.namespace.QName;

import com.intel.gpe.clients.api.apps.ArgumentMetaData;
import com.intel.gpe.clients.api.apps.ParameterMetaData;
import com.intel.gpe.gridbeans.apps.ArgumentMetaDataImpl;
import com.intel.gpe.util.collections.CollectionUtil;
import com.intel.gpe.util.sets.Pair;

import de.fzj.unicore.rcp.gpe4eclipse.GenericRegistry;


/**
 * Resources registry caches info about site specific resources available for
 * all sites available in the grid.
 * @author K. Benedyczak
 */
public class ExecutionEnvironmentRegistry extends GenericRegistry<ExecEnv> {

	private static ExecutionEnvironmentRegistry instance = new ExecutionEnvironmentRegistry();


	/**
	 * @return the ResourcesRegistry instance
	 */
	public static ExecutionEnvironmentRegistry getDefaultInstance() {
		return instance;
	}

	public static void setDefaultInstance(ExecutionEnvironmentRegistry reg) {
		instance = reg;
	}

	@Override
	protected boolean isValid(ExecEnv resource) {
		return resource.getName() != null;
	}

	/**
	 * 
	 * @return a union of Execution Environments of all sites. Execution Environments with the same name are
	 * merged into one entry. 
	 */
	public synchronized List<ExecEnv> getUnifiedExecutionEnvironments() {
		return unifyExecutionEnvironments(getEntryList());
	}

	public static List<ExecEnv> unifyExecutionEnvironments(List<ExecEnv> all) {
		Map<String, ExecEnv> union = new HashMap<String, ExecEnv>();
		for (ExecEnv r: all) {
			String key = getKey(r);
			ExecEnv existing = union.get(key);
			if (existing == null)
				union.put(key, (ExecEnv) r.clone());
			else {
				ExecEnv combined = combineValues(existing, r);
				union.put(key, combined);
			}
		}
		return new ArrayList<ExecEnv>(union.values());
	}

	public static String getKey(ExecEnv env)
	{
		String name = env.getName().getLocalPart();
		String version = env.getVersion() == null ? "": env.getVersion();
		return name+" v"+version;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static ExecEnv combineValues(ExecEnv... envs) {

		ExecEnv first = envs[0];

		Map<QName,ParameterMetaData<?>> allParams = new HashMap<QName, ParameterMetaData<?>>();
		Map<QName,ArgumentType> types = new HashMap<QName, ArgumentType>();

		// sorting the parameters is a bit tricky here
		// we use a cumulative moving average in order to merge the
		// original orderings of parameters
		PriorityQueue<Pair<QName,Double>> sorted;

		Map<QName,Double> paramWeights = new HashMap<QName,Double>();
		Map<QName,Integer> paramCounts = new HashMap<QName,Integer>();

		int order = 0;

		for(ExecEnv added : envs)
		{
			if(added.getArguments() != null)
			{
				for(ExecEnvArgument<?> a : added.getArguments())
				{

					ArgumentType type = types.get(a.getName());
					if(type == null){
						type = a.getType();
					}
					else if(!type.equals(a.getType()))
					{
						if(getPrio(type) < getPrio(a.getType()))
						{
							type = a.getType();
						}
					}
					types.put(a.getName(), type);

					int currentOrder = getPrio(type);

					currentOrder += order;

					ArgumentMetaData<?> arg = a.getMetaData();
					ParameterMetaData<?> meta = allParams.get(arg.getName());
					if(meta == null)
					{
						allParams.put(arg.getName(), arg);
						paramWeights.put(arg.getName(), (double)currentOrder);
						paramCounts.put(arg.getName(), 1);
					}
					else {
						allParams.put(arg.getName(), mergeParams(arg, meta));
						int count = paramCounts.get(arg.getName());
						double movingAvg = paramWeights.get(arg.getName());
						movingAvg += (currentOrder-movingAvg)/++count;
						paramCounts.put(arg.getName(), count);
					}
					order ++;
				}
			}
		}

		sorted = new PriorityQueue<Pair<QName,Double>>(Math.max(10,allParams.size()),new Comparator<Pair<QName,Double>>() {
			public int compare(Pair<QName, Double> o1, Pair<QName, Double> o2) {
				return o1.getM2().compareTo(o2.getM2());
			}
		});
		for(QName name : allParams.keySet())
		{
			sorted.add(new Pair<QName,Double>(name,paramWeights.get(name)));
		}

		ExecEnv result = first.clone();
		result.getArguments().clear();

		Pair<QName,Double> current = null;
		while(!sorted.isEmpty())
		{
			current = sorted.poll(); 
			QName name = current.getM1();
			ParameterMetaData<?> meta = allParams.get(name);
			if(meta instanceof ArgumentMetaData<?>)
			{
				ArgumentMetaData<?> metaData = (ArgumentMetaData<?>) meta;
				result.addArgument(new ExecEnvArgument(name,types.get(name),metaData));
			}

		}
		return result;

	}


	private static ParameterMetaData<?> mergeParams(ParameterMetaData<?> arg1, ParameterMetaData<?> arg2)
	{
		ParameterMetaData<?> result = null;
		if(arg1.equals(arg2)) return arg1;
		if(!CollectionUtil.equalOrBothNull(arg1.getType(), arg2.getType()))
		{
			// ambiguous parameter type description
			ArgumentMetaData<String> newArg = new ArgumentMetaDataImpl<String>();
			newArg.setName(arg1.getName());
			newArg.setTargetType(String.class);
			result = newArg;
		}

		else {
			result = mergeArguments((ArgumentMetaData<?>) arg1, (ArgumentMetaData<?>) arg2);
			Set<QName> exclusions1 = new HashSet<QName>(Arrays.asList(arg2.getExclusions()));
			Set<QName> exclusions2 = new HashSet<QName>(Arrays.asList(arg1.getExclusions()));
			exclusions1.retainAll(exclusions2);
			result.setExclusions(exclusions1.toArray(new QName[exclusions1.size()]));
			Set<QName> dependencies1 = new HashSet<QName>(Arrays.asList(arg2.getDependencies()));
			Set<QName> dependencies2 = new HashSet<QName>(Arrays.asList(arg1.getDependencies()));
			dependencies1.retainAll(dependencies2);
			result.setDependencies(dependencies1.toArray(new QName[dependencies1.size()]));
		}
		//		result.setDescription(AMBIGUOUS_PARAMETER_MARKER); // mark param as ambiguous... bit of a hack
		return result;
	}

	private static ArgumentMetaData<?> mergeArguments(ArgumentMetaData<?> arg1, ArgumentMetaData<?> arg2)
	{
		
		if(!CollectionUtil.equalOrBothNull(arg1.getTargetType(),arg2.getTargetType()))
		{
			// ambiguous parameter description
			ArgumentMetaDataImpl<String> result = new ArgumentMetaDataImpl<String>();
			result.setName(arg1.getName());
			result.setTargetType(String.class);
			return result;
		}
		else
		{
			ArgumentMetaData<?> result = arg1.clone();
			if(!CollectionUtil.equalOrBothNull(arg1.getDefaultValue(),arg2.getDefaultValue()))
			{
				result.setDefaultValue(null);

			}
			if(!CollectionUtil.equalOrBothNull(arg1.getUnit(),arg2.getUnit()))
			{
				result.setUnit(null);
			}
			if(!CollectionUtil.equalOrBothNull(arg1.getValidValues(),arg2.getValidValues()))
			{
				result.setValidValues(null);
			}
			
			return result;
		}
	}


	private static int getPrio(ArgumentType type)
	{
		switch (type) {
		case ARGUMENT:
			return 0;
		case OPTION:
			return 1000;
		case PRECOMMAND:
			return 2000;
		case POSTCOMMAND:
			return 3000;
		case USERPRECOMMAND:
			return 4000;
		case USERPOSTCOMMAND:
			return 5000;
		case USERPRECOMMAND_RUNONLOGINNODE:
			return 6000;
		case USERPOSTCOMMAND_RUNONLOGINNODE:
			return 7000;
		}
		return 0;
	}

}
