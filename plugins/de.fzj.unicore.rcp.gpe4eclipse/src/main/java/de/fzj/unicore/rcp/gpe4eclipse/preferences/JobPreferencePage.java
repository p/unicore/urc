package de.fzj.unicore.rcp.gpe4eclipse.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.PathEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;

/**
 * This class represents a UNICORE Applications preference page that is
 * contributed to the UNICORE Preferences dialog.
 * 
 * @author Valentina Huber
 */
public class JobPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public JobPreferencePage() {
		super(GRID);
		setPreferenceStore(GPEActivator.getDefault().getPreferenceStore());
		setDescription("Preferences for UNICORE jobs.");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		PathEditor gridBeanDirs = new PathEditor(
				GPE4EclipseConstants.P_GRIDBEAN_PATH,
				"&Application directories:", "add Directory",
				getFieldEditorParent());
		// DirectoryFieldEditor gridBeanDir = new DirectoryFieldEditor(
		// GPE4EclipseConstants.P_GRIDBEAN_PATH,
		// "&Application directory:", getFieldEditorParent());
		addField(gridBeanDirs);

		IntegerFieldEditor pollingTime = new IntegerFieldEditor(
				GPE4EclipseConstants.P_JOB_POLLING_INTERVAL,
				"Time between polling job status (seconds):",
				getFieldEditorParent());
		pollingTime.setValidRange(1, Integer.MAX_VALUE);
		addField(pollingTime);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}

}