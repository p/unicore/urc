/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.gridbeans.parameters.IFileParameterValue;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;

/**
 * @author demuth
 * 
 */
public class FilenameInJobDirCellEditor extends TextCellEditor {

	private ModifyListener modifyListener;
	private IFileParameterValue value;
	private boolean stageingIn;

	public FilenameInJobDirCellEditor(Composite parent, boolean stageingIn) {
		super(parent);
		this.stageingIn = stageingIn;
	}

	@Override
	protected Object doGetValue() {
		String displayed = text.getText();
		String relative = displayed;
		IFileParameterValue result;
		try {
			result = value.clone();
		} catch (Exception e) {
			GPEActivator
					.log(IStatus.ERROR,
							"Could not clone address for setting jobname in working dir",
							e);
			result = value;
		}
		if (stageingIn) {

			result.getTarget().setRelativePath(relative);
			result.getTarget().setDisplayedString(displayed);

		} else {
			result.getSource().setRelativePath(relative);
			result.getSource().setDisplayedString(displayed);
			boolean usingWildcards = result.getSource().getRelativePath()
					.contains("*");
			result.getSource().setUsingWildcards(usingWildcards);
		}
		return result;
	}

	@Override
	protected void doSetValue(Object value) {
		Assert.isTrue(text != null && (value instanceof IFileParameterValue));
		this.value = (IFileParameterValue) value;

		text.removeModifyListener(getModifyListener());
		String oldValue = "";
		if (stageingIn) {
			oldValue = this.value.getTarget().getDisplayedString();
		} else {
			oldValue = this.value.getSource().getDisplayedString();
		}
		text.setText(oldValue);
		text.addModifyListener(getModifyListener());
	}

	/**
	 * Return the modify listener.
	 */
	protected ModifyListener getModifyListener() {
		if (modifyListener == null) {
			modifyListener = new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					editOccured(e);
				}
			};
		}
		return modifyListener;
	}

}
