/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.Status;

import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.IRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.RequirementConstants;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.RequirementValue;

/**
 * @author sholl
 * 
 *         Represents the overall Requirement value for execution environments.
 *         It stores a list with all available execution environments and the
 *         selected execution environment.
 * 
 */

public class ExecEnvRequirementValue extends RequirementValue implements
		Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<ExecEnv> execEnvs = new ArrayList<ExecEnv>();
	public static final QName TYPE = RequirementConstants.executionEnvironmentRequirement;
	private int selected = 0;
	private String selectedVersion = null;
	public QName name;

	public ExecEnvRequirementValue() {

	}

	public void addExecEnv(ExecEnv execEnv) {
		this.execEnvs.add(execEnv);
	}

	@Override
	public ExecEnvRequirementValue clone() {
		try {
			ExecEnvRequirementValue clone = (ExecEnvRequirementValue) super
					.clone();

			if (this.name != null) {
				clone.name = new QName(this.getName()
						.getNamespaceURI(), this.getName().getLocalPart(), this
						.getName().getPrefix());
			}

			clone.setSelected(getSelected());
			clone.setSelectedVersion(getSelectedVersion());

			clone.execEnvs = new java.util.ArrayList<ExecEnv>();

			for (ExecEnv flat : execEnvs) {
				clone.execEnvs.add(flat.clone());
			}
			return clone;

		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while cloning execution environment settings: "+e.getMessage(), e);

			return null;
		}

	}

	public ExecEnvRequirementValue copy() {

		return this.clone();
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ExecEnvRequirementValue)) {
			return false;
		}
		ExecEnvRequirementValue other = (ExecEnvRequirementValue) o;
		return CollectionUtil.equalOrBothNull(getName(), other.getName())
				&& CollectionUtil.equalOrBothNull(getSelected(),
						other.getSelected())
				&& CollectionUtil.equalOrBothNull(getSelectedVersion(),
						other.getSelectedVersion())		
				&& CollectionUtil.equalOrBothNull(getExecEnvs(),
						other.getExecEnvs());

	}

	public String getDisplayedString() {
		return toString();
	}

	public List<ExecEnv> getExecEnvs() {
		return execEnvs;
	}

	public QName getName() {
		return name;
	}

	public String[] getReadableList() {
		String[] list = new String[execEnvs.size()];
		for (int i = 0; i < list.length; i++) {
			ExecEnv env = execEnvs.get(i);
			list[i] = env.getReadableName();
		}
		return list;
	}

	public int getSelected() {
		return selected;
	}

	public ExecEnv getSelectedExecEnv() {
		if(execEnvs == null || 
				selected < 0 || selected >= execEnvs.size()) return null;
		return execEnvs.get(selected);

	}

	public QName getType() {

		return TYPE;
	}

	public IRequirementValue getValue() {

		return this;
	}

	@Override
	public String isValid() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setExecEnvs(List<ExecEnv> execEnvs) {
		this.execEnvs = execEnvs;
	}

	public void setName(QName name) {
		this.name = name;
	}

	public void setPersistableExecEnvs(List<ExecEnv> execEnvs) {
		this.execEnvs = execEnvs;
	}

	public void setSelected(int selected) {
		this.selected = selected;
	}


	@Override
	public String toString() {

		return execEnvs.get(selected).getName().getLocalPart();
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public static QName getTYPE() {
		return TYPE;
	}

	public String getSelectedVersion() {
		return selectedVersion;
	}

	public void setSelectedVersion(String selectedVersion) {
		this.selectedVersion = selectedVersion;
	}

}
