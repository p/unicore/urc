package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

public interface IUnitConverter {

	public <T> T convertValue(String sourceUnit, String targetUnit,
			Class<T> valueType, T value) throws Exception;

}
