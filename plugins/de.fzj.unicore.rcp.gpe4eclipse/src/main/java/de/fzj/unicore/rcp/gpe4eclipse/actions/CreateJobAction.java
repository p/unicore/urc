package de.fzj.unicore.rcp.gpe4eclipse.actions;

import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.commands.OpenJobEditorCommand;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

public class CreateJobAction extends NodeAction {

	public CreateJobAction(Node node) {
		super(node);
		setText("Create Job");
		setToolTipText("Load an application user interface in order to set up a job.");
		setImageDescriptor(GPEActivator.getImageDescriptor("job.png"));
	}

	/**
		 */
	@Override
	public boolean isCurrentlyAvailable() {
		return getNode() != null && getNode().getState() == Node.STATE_READY;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.actions.ServiceBrowserAction#run()
	 */
	@Override
	public void run() {
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
			public void run() {
				OpenJobEditorCommand cmd = new OpenJobEditorCommand(getNode(),
						getViewer());
				cmd.execute();
			}

		});
	}

}