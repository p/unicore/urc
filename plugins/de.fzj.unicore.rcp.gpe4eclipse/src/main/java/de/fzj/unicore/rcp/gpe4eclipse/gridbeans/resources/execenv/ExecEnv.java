/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.Status;

import com.intel.gpe.clients.api.apps.ArgumentMetaData;
import com.intel.gpe.gridbeans.apps.ArgumentMetaDataImpl;
import com.intel.gpe.gridbeans.apps.BooleanValidArgumentRange;
import com.intel.gpe.gridbeans.apps.StringValidArgumentRange;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;

/**
 * @author sholl
 * 
 *         Represents one execution environment with arguments (parameter), a
 *         name and a controler, that manages the data control between the
 *         arguments of the execution environment and the controls
 * 
 */

public class ExecEnv implements Cloneable {

	private QName name = new QName("");

	private boolean dirty;

	private List<ExecEnvArgument<?>> arguments = new ArrayList<ExecEnvArgument<?>>();

	private String description;

	private String version;

	transient protected PropertyChangeSupport listeners = new PropertyChangeSupport(
			this);

	public ExecEnv() {

	}

	public ExecEnv(QName name, List<ExecEnvArgument<?>> arguments) {
		this.name = name;
		this.arguments = arguments;

	}

	public void addPropertyChangeListener(PropertyChangeListener l) {

		listeners.addPropertyChangeListener(l);
	}

	
	public void addPropertyChangeListener(QName propertyName,
			PropertyChangeListener listener) {
		listeners.addPropertyChangeListener(propertyName.toString(), listener);
	}

	public void addUserPreAndPostcommand() {
		StringValidArgumentRange[] validValues = new StringValidArgumentRange[1];
		StringValidArgumentRange val = new StringValidArgumentRange(".*");
		BooleanValidArgumentRange[] validBooleanValue = new BooleanValidArgumentRange[1];
		validBooleanValue[0] = new BooleanValidArgumentRange();
		val.setRegexp(true);
		validValues[0] = val;
		ArgumentMetaData<String> argumentMetadata = new ArgumentMetaDataImpl<String>(
				String.class, "User Pre", validValues);
		ArgumentMetaData<String> argumentMetadata2 = argumentMetadata;
		ExecEnvArgument<String> arge = new ExecEnvArgument<String>(new QName(
				"User Precommand"), String.class, argumentMetadata2, true,
				ArgumentType.USERPRECOMMAND, "");		
		arguments.add(arge);		
		
		ArgumentMetaData<Boolean> preRunOnLoginNodeMetadata = new ArgumentMetaDataImpl<Boolean>(
				Boolean.class, "User PreCommand run on login node", validBooleanValue);
		
		ExecEnvArgument<Boolean> preRunOnLoginNodeArgument = new ExecEnvArgument<Boolean>(new QName(
				"User Precommand run on Login Node"), Boolean.class, preRunOnLoginNodeMetadata, true,
				ArgumentType.USERPRECOMMAND_RUNONLOGINNODE, false);		
		arguments.add(preRunOnLoginNodeArgument);		
		
		argumentMetadata = new ArgumentMetaDataImpl<String>(String.class,
				"User Postcommand", validValues);
		argumentMetadata2 = argumentMetadata;
		arge = new ExecEnvArgument<String>(new QName("User Postcommand"),
				String.class, argumentMetadata2, true,
				ArgumentType.USERPOSTCOMMAND, "");
		arguments.add(arge);
		
		ArgumentMetaData<Boolean> postRunOnLoginNodeMetadata = new ArgumentMetaDataImpl<Boolean>(
				Boolean.class, "User PostCommand run on login node", validBooleanValue);
		
		ExecEnvArgument<Boolean> postRunOnLoginNodeArgument = new ExecEnvArgument<Boolean>(new QName(
				"User Postcommand run on Login Node"), Boolean.class, postRunOnLoginNodeMetadata, true,
				ArgumentType.USERPOSTCOMMAND_RUNONLOGINNODE, false);		
		arguments.add(postRunOnLoginNodeArgument);		
	}

	@Override
	public ExecEnv clone() {
		try {
			ExecEnv clonedContainer = (ExecEnv) super.clone();
			clonedContainer.arguments = new java.util.ArrayList<ExecEnvArgument<?>>();
			for (ExecEnvArgument<?> flat : arguments) {
				clonedContainer.arguments.add(flat.clone());
			}

			if (this.name != null) {
				clonedContainer.name = new QName(this.getName()
						.getNamespaceURI(), this.getName().getLocalPart(), this
						.getName().getPrefix());
			}

			clonedContainer.dirty = this.dirty;
			if (this.description != null) {
				clonedContainer.description = new String(this.getDescription());
			}
			if (this.version != null) {
				clonedContainer.version = new String(this.getVersion());
			}

			return clonedContainer;

		} catch (CloneNotSupportedException e) {
			GPEActivator.log(Status.ERROR, "Error while cloning execution environment settings: "+e.getMessage(), e);

			return null;
		}
	}

	

	protected void firePropertyChange(String prop, Object old, Object newValue) {
		if (old != newValue || (old == null && newValue == null)) {
			listeners.firePropertyChange(prop, old, newValue);
			// force firing the event!
		} else {
			PropertyChangeListener[] l = listeners
					.getPropertyChangeListeners(prop);
			PropertyChangeEvent evt = new PropertyChangeEvent(this, prop, old,
					newValue);
			for (PropertyChangeListener propertyChangeListener : l) {
				propertyChangeListener.propertyChange(evt);
			}
		}
	}

	public ExecEnvArgument<?> getArgument(QName name) {
		for (ExecEnvArgument<?> arg : getArguments()) {
			if (arg.getName().equals(name)) {
				return arg;
			}
		}
		return null;
	}
	
	
	public void addArgument(ExecEnvArgument<?> arg) {
		arguments.add(arg);
	}

	public List<ExecEnvArgument<?>> getArguments() {
		return arguments;
	}

	public String getDescription() {
		return description;
	}

	public QName getName() {
		return name;
	}

	public String getReadableName() {
		return name.getLocalPart();
	}

	public String getVersion() {
		return version;
	}

	public boolean isDirty() {

		return dirty;
	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		listeners.removePropertyChangeListener(l);
	}

	public void removePropertyChangeListener(QName propertyName,
			PropertyChangeListener listener) {
		listeners.addPropertyChangeListener(propertyName.toString(), listener);
	}

	

	public void setArgument(QName key, ExecEnvArgument<? extends Object> e) {

		ExecEnvArgument<?> oldArg = null;
		int index = 0;
		for (ExecEnvArgument<?> argument : arguments) {
			if (argument.getName().equals(key)) {
				oldArg = argument;
				break;
			} else {
				index++;

			}
		}

		arguments.set(index, e);

		firePropertyChange(key.toString(), oldArg, e);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDirty(boolean isDirty) {
		this.dirty = isDirty;
	}

	public void setName(QName name) {
		this.name = name;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		if(arguments != null)
		{
			for(ExecEnvArgument<?> arg :arguments)
			{
				result = prime * result
				+ ((arg == null) ? 0 : arg.hashCode());
			}
		}
		
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + (dirty ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object o) {

		if (!(o instanceof ExecEnv)) {
			return false;
		}
		ExecEnv other = (ExecEnv) o;
		return CollectionUtil.equalOrBothNull(getName(), other.getName())

				&& CollectionUtil.containSameElements(getArguments(),
						other.getArguments())
				&& CollectionUtil.equalOrBothNull(getVersion(), getVersion())
				&& CollectionUtil.equalOrBothNull(getDescription(),
						other.getDescription())
				&& (isDirty() == other.isDirty());

	}

}
