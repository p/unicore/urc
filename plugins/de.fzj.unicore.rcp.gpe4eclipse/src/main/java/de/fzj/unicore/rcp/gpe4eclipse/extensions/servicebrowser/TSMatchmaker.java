package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionDocument;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDescriptionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.OperatingSystemType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ProcessorArchitectureEnumeration;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ResourcesType;
import org.unigrids.services.atomic.types.AvailableResourceType;
import org.unigrids.services.atomic.types.AvailableResourceTypeType;
import org.unigrids.services.atomic.types.AvailableResourceTypeType.Enum;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.clients.api.Application;
import com.intel.gpe.clients.api.Job;
import com.intel.gpe.clients.api.jsdl.OSType;
import com.intel.gpe.clients.api.jsdl.OperatingSystemRequirementsType;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.clients.common.ApplicationImpl;
import com.intel.gpe.gridbeans.jsdl.JSDLJobImpl;

import de.fzj.unicore.rcp.common.utils.RangeValueUtils;
import de.fzj.unicore.rcp.gpe4eclipse.applications.ApplicationRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.IMatchmaker;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.ResourceProviderAdapter;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnv;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecutionEnvironmentRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;
import de.fzj.unicore.rcp.gpe4eclipse.utils.RangeValueTypeConverter;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import eu.unicore.jsdl.extensions.ExecutionEnvironmentDocument;
import eu.unicore.jsdl.extensions.ExecutionEnvironmentDocument.ExecutionEnvironment;
import eu.unicore.jsdl.extensions.ResourceRequestType;

/**
 * Generic matchmaker for TSS and TSF nodes. 
 * @author K. Benedyczak
 */
public abstract class TSMatchmaker implements IMatchmaker {
	private NodePath nodePath;

	public TSMatchmaker(NodePath nodePath) {
		this.nodePath = nodePath;
	}

	public String canHandleJob(Job j, ResourceProviderAdapter props, URI uri) {
		if (!(j instanceof GPEJob)) {
			return "Invalid job type.";
		}
		GPEJob job = (GPEJob) j;
		String s = matchApplication(job, uri);
		if (s != null) {
			return s;
		}
		s = matchExecEnvs(job, uri);
		if (s != null) {
			return s;
		}
		s = matchArchitecture(job, props);
		if (s != null) {
			return s;
		}
		s = matchCPUsPerNode(job, props);
		if (s != null) {
			return s;
		}
		s = matchNumberOfNodes(job, props);
		if (s != null) {
			return s;
		}
		s = matchOS(job, props);
		if (s != null) {
			return s;
		}
		s = matchRAMPerNode(job, props);
		if (s != null) {
			return s;
		}
		s = matchSpeedPerNode(job, props);
		if (s != null) {
			return s;
		}
		s = matchTime(job, props);
		if (s != null) {
			return s;
		}
		s = matchTotalNumberOfCPUs(job, props);
		if (s != null) {
			return s;
		}
		s = matchSSRs(job, props);
		if (s != null) {
			return s;
		}

		return null;
	}

	public EndpointReferenceType getEPR() {
		return NodeFactory.getEPRForURI(nodePath.last());
	} 

	protected String matchApplication(GPEJob job, URI u) {
		if (job.getApplicationName() == null 
				|| job.getApplicationName().trim().length() == 0) {
			return null;
		}
		try {
			List<Application> apps = ApplicationRegistry.getDefaultInstance()
			.getEntries(u);
			boolean supportsApp = GridBeanUtils.matchApplication(
					new ApplicationImpl(job.getApplicationName(), job
							.getApplicationVersion()), apps);
			return supportsApp ? null : "Application "
				+ job.getApplicationName() + " (v "
				+ job.getApplicationVersion() + ") not supported.";
		} catch (Exception e) {
			return "Unable to retrieve information about installed applications.";

		}
	}

	protected String matchArchitecture(GPEJob job, ResourceProviderAdapter props) {
		try {

			com.intel.gpe.clients.api.jsdl.ProcessorType cpu = job
			.getCPUArchitectureRequirements();
			if (cpu == null) {
				return null;
			}
			String requested = cpu.getValue();
			if (requested == null) {
				return null;
			}

			ProcessorArchitectureEnumeration.Enum name = props.getProcessor()
			.getCPUArchitecture().getCPUArchitectureName();
			boolean ok = name.toString().equalsIgnoreCase(requested);
			return ok ? null
					: "Target systen does not support CPU architecture "
						+ requested;
		} catch (Exception e) {
			return "Unable to retrieve information about the CPU architecture.";

		}

	}

	protected String matchCPUsPerNode(GPEJob job, ResourceProviderAdapter props) {
		try {
			RangeValueType requested = RangeValueTypeConverter
			.convertToXBean(job.getIndividualCPUCountRequirements());
			if (requested != null) {
				RangeValueType available = props.getIndividualCPUCount();
				boolean ok = RangeValueUtils.fitsInto(requested, available);
				if (!ok) {
					return "Insufficient number of CPUs per node";
				}
			}
		} catch (Exception e) {
			return "Unable to determine number of CPUs per node";
		}
		return null;
	}

	protected String matchNumberOfNodes(GPEJob job, ResourceProviderAdapter props) {
		try {
			RangeValueType requested = RangeValueTypeConverter
			.convertToXBean(job.getTotalResourceCountRequirements());
			if (requested != null) {
				RangeValueType available = props.getTotalResourceCount();
				boolean ok = RangeValueUtils.fitsInto(requested, available);
				if (!ok) {
					return "Insufficient number of nodes";
				}
			}
		} catch (Exception e) {
			return "Unable to determine number of nodes";
		}
		return null;
	}

	protected String matchOS(GPEJob job, ResourceProviderAdapter props) {
		try {

			OperatingSystemRequirementsType os = job
			.getOperatingSystemRequirements();
			if (os == null) {
				return null;
			}
			OSType type = os.getOSType();
			String version = os.getOSVersion();
			if (type == null || "unknown".equalsIgnoreCase(type.getValue())) {
				return null;
			}

			OperatingSystemType[] systems = props.getOperatingSystemArray();
			if (systems == null || systems.length == 0) {
				return null; // TODO most sites do not set this properly, but
				// it REALLY SHOULD BE SET
			}
			for (OperatingSystemType available : systems) {
				boolean ok = type
				.getValue()
				.toString()
				.equalsIgnoreCase(
						available.getOperatingSystemType()
						.getOperatingSystemName().toString())
						&& (version == null || version.trim().length() == 0 || version
								.equalsIgnoreCase(available
										.getOperatingSystemVersion()));
				if (ok) {
					return null;
				}
			}
			return "Target system does not support the Operating System "
			+ type.toString() + " " + version;
		} catch (Exception e) {
			return "Unable to retrieve information about installed Operating System.";

		}
	}

	protected String matchRAMPerNode(GPEJob job, ResourceProviderAdapter props) {
		try {
			RangeValueType requested = RangeValueTypeConverter
			.convertToXBean(job
					.getIndividualPhysicalMemoryRequirements());
			if (requested != null) {
				double maximum = Double.MIN_VALUE;
				for (RangeValueType mem : props.getIndividualPhysicalMemoryArray()) {
					maximum = Math.max(maximum, RangeValueUtils.getMaximum(mem));
				}
				RangeValueType available = RangeValueType.Factory.newInstance();
				RangeType rt = available.addNewRange();
				rt.addNewLowerBound().setStringValue("0");
				rt.addNewUpperBound().setStringValue("" + maximum);
				boolean ok = RangeValueUtils.fitsInto(requested, available);
				if (!ok) {
					return "Insufficient amount of memory per node";
				}
			}
		} catch (Exception e) {
			return "Unable to determine the amount of memory per node";
		}
		return null;
	}

	protected String matchSpeedPerNode(GPEJob job, ResourceProviderAdapter props) {
		try {
			RangeValueType requested = RangeValueTypeConverter
			.convertToXBean(job.getIndividualCPUSpeedRequirements());
			if (requested != null) {
				RangeValueType available = props.getProcessor().getIndividualCPUSpeed();
				boolean ok = RangeValueUtils.fitsInto(requested, available);
				if (!ok) {
					return "Insufficient CPU speed per node";
				}
			}
		} catch (Exception e) {
			return "Unable to determine CPU speed per node";
		}
		return null;
	}

	protected String matchTime(GPEJob job, ResourceProviderAdapter props) {
		try {
			RangeValueType requested = RangeValueTypeConverter
			.convertToXBean(job.getIndividualCPUTimeRequirements());
			if (requested != null) {
				RangeValueType available = props.getIndividualCPUTime();
				boolean ok = RangeValueUtils.fitsInto(requested, available);
				if (!ok) {
					return "Insufficient computation time available.";
				}
			}
		} catch (Exception e) {
			return "Unable to determine the maximum computation time";
		}
		return null;
	}

	protected String matchTotalNumberOfCPUs(GPEJob job,
			ResourceProviderAdapter props) {
		try {
			RangeValueType requested = RangeValueTypeConverter
			.convertToXBean(job.getTotalCPUCountRequirements());
			if (requested != null) {

				RangeValueType available = props.getTotalCPUCount();
				if (available == null) {
					return "Unable to determine total number of CPUs";
				}
				boolean ok = RangeValueUtils.fitsInto(requested, available);
				// double numNodes =
				// RangeValueUtils.getMaximum(n.getTargetSystemFactoryProperties().getTotalResourceCount());
				// double CPUsPerNode =
				// RangeValueUtils.getMaximum(n.getTargetSystemFactoryProperties().getIndividualCPUCount());
				// available = RangeValueType.Factory.newInstance();
				// available.addNewExact().setDoubleValue(numNodes*CPUsPerNode);

				if (!ok) {
					return "Insufficient total number of CPUs";
				}
			}
		} catch (Exception e) {
			return "Unable to determine total number of CPUs";
		}
		return null;
	}


	private List<ResourceRequestType> getJobSSRRequests(GPEJob job) {
		//FIXME - any better way?
		if (!(job instanceof JSDLJobImpl))
			return Collections.emptyList();
		JSDLJobImpl impl = (JSDLJobImpl) job;

		JobDefinitionDocument jobDefinition = impl.getDocument();
		if (jobDefinition == null || jobDefinition.getJobDefinition() == null)
			return Collections.emptyList();
		JobDescriptionType jobDescription = jobDefinition.getJobDefinition().getJobDescription();
		if (jobDescription == null || jobDescription.getResources() == null)
			return Collections.emptyList();
		ResourcesType resources = jobDescription.getResources();

		List<ResourceRequestType> ssr = new ArrayList<ResourceRequestType>();
		XmlCursor c = resources.newCursor();
		try {
			QName SSR_QNAME = new QName("http://www.unicore.eu/unicore/jsdl-extensions", 
			"ResourceRequest");
			boolean found = c.toChild(SSR_QNAME);
			while (found) {
				XmlObject o = c.getObject();
				if (o instanceof ResourceRequestType)
					ssr.add((ResourceRequestType) o);
				found = c.toNextSibling(SSR_QNAME);
			}
		} finally {
			c.dispose();
		}

		return ssr;
	}

	private List<ExecutionEnvironment> getExecEnvRequests(GPEJob job) {
		//FIXME - any better way?
		if (!(job instanceof JSDLJobImpl))
			return Collections.emptyList();
		JSDLJobImpl impl = (JSDLJobImpl) job;

		JobDefinitionDocument jobDefinition = impl.getDocument();
		if (jobDefinition == null || jobDefinition.getJobDefinition() == null)
			return Collections.emptyList();
		JobDescriptionType jobDescription = jobDefinition.getJobDefinition().getJobDescription();
		if (jobDescription == null || jobDescription.getResources() == null)
			return Collections.emptyList();
		ResourcesType resources = jobDescription.getResources();
		List<ExecutionEnvironment> execEnvs = new ArrayList<ExecutionEnvironment>();
		XmlCursor c = resources.newCursor();
		try {
			QName execEnvName = 
			ExecutionEnvironmentDocument.type.getDocumentElementName();
			boolean found = c.toChild(execEnvName);
			while (found) {
				XmlObject o = c.getObject();
				if (o instanceof ExecutionEnvironment)
				{
					ExecutionEnvironment ee =(ExecutionEnvironment) o;
					execEnvs.add(ee);
				}
				found = c.toNextSibling(execEnvName);
			}
		} finally {
			c.dispose();
		}
		return execEnvs;
	}

	protected String matchExecEnvs(GPEJob job, URI uri) {
		
		ExecutionEnvironmentRegistry registry = ExecutionEnvironmentRegistry.getDefaultInstance();
		List<ExecEnv> available = registry.getEntries(uri);
		Map<QName,Set<String>> keys = new HashMap<QName, Set<String>>();
		for(ExecEnv e : available)
		{
			if(e.getName() == null) continue;
			Set<String> versions = keys.get(e.getName());
			if(versions == null) 
			{
				versions = new HashSet<String>();
				keys.put(e.getName(), versions);
			}
			if(e.getVersion() != null) versions.add(e.getVersion());
		}
		List<ExecutionEnvironment> ees = getExecEnvRequests(job);
		for(ExecutionEnvironment e : ees)
		{
			if(e.getName() == null) continue;
			Set<String> versions = keys.get(new QName(e.getName()));
			if(versions == null) return "Execution environment "+e.getName()+" not supported by target system.";
			if(e.getVersion() == null)
			{
				return null;
			}
			else if (!versions.contains(e.getVersion()))
			{
				return "Version "+e.getVersion() +" of Execution environment "+e.getName()+"not supported by target system.";
			}
		}
		return null;
	}

	protected String matchSSRs(GPEJob job, ResourceProviderAdapter props) {

		List<ResourceRequestType> ssr = getJobSSRRequests(job);

		AvailableResourceType[] available = props.getAvailableResourceArray();
		try {

			Map<String, AvailableResourceType> availableMap = new HashMap<String, AvailableResourceType>();
			for (AvailableResourceType a: available)
				availableMap.put(a.getName(), a);

			for (ResourceRequestType rr: ssr) {
				AvailableResourceType aval = availableMap.get(rr.getName());
				String ret = matchSSR(rr, aval);
				if (ret != null)
					return ret;
			}
			return null;
		} catch (Exception e) {
			if(ssr != null && ssr.size() > 0)
			{
				return "Unable to determine site specific resources.";
			}
			else return null;
		}
	}


	private String matchSSR(ResourceRequestType rr, AvailableResourceType aval) {
		String value = rr.getValue();
		if (aval == null)
			return "Resource requirement " + rr.getName() + " is not supported";
		Enum type = aval.getType();
		if (type == AvailableResourceTypeType.BOOLEAN) {
			if (!value.equalsIgnoreCase("true") && 
					!value.equalsIgnoreCase("false"))
				return "Invalid value of the requirement " + rr.getName();
		} else if (type == AvailableResourceTypeType.CHOICE) {
			String[] valid = aval.getAllowedValueArray();
			for (String v: valid)
				if (v.equals(value))
					return null;
			return "Resource requirement " + aval.getName() + " is invalid";
		} else if (type == AvailableResourceTypeType.DOUBLE) {
			return checkDouble(aval, value);
		} else if (type == AvailableResourceTypeType.INT) {
			return checkInt(aval, value);
		} else if (type == AvailableResourceTypeType.STRING) {
			return null;
		}
		return null;
	}

	private String checkDouble(AvailableResourceType aval, String value) {
		try { 
			double min = aval.getMin() != null ? Double.parseDouble(aval.getMin()) : 
				Integer.MIN_VALUE;
			double max = aval.getMax() != null ? Double.parseDouble(aval.getMax()) : 
				Integer.MAX_VALUE;
			double pValue = Double.parseDouble(value);

			if (pValue <= max && pValue >= min)
				return null;
		} catch (NumberFormatException e) {
			return "Resource requirement " + aval.getName() + " is invalid";
		}
		return "Resource requirement " + aval.getName() + " is out of range";
	}

	private String checkInt(AvailableResourceType aval, String value) {
		try { 
			int min = aval.getMin() != null ? Integer.parseInt(aval.getMin()) : 
				Integer.MIN_VALUE;
			int max = aval.getMax() != null ? Integer.parseInt(aval.getMax()) : 
				Integer.MAX_VALUE;
			int pValue = Integer.parseInt(value);

			if (pValue <= max && pValue >= min)
				return null;
		} catch (NumberFormatException e) {
			return "Resource requirement " + aval.getName() + " is invalid";
		}
		return "Resource requirement " + aval.getName() + " is out of range";
	}
}









