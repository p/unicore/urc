/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse;

import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.Status;

import com.intel.gpe.clients.api.apps.ParameterMetaData;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;

/**
 * Instances of this class describe the context in which the GridBean job will
 * be run. E.g. it can be used for checking whether the GridBean defines a part
 * of a workflow and if so, which workflow processing system is used.
 * 
 * @author demuth
 * 
 */
public class GridBeanContext implements IGridBeanParameterValue, Cloneable {

	private boolean partOfWorkflow = false;
	private QName defaultWorkflowSystem;
	private List<QName> applicationDomains;
	private boolean isEditable = true;
	private int jobExecutionState;
	private String jobClientAddress;
	private Integer terminationTime;
	private boolean newlyCreated = false;

	public GridBeanContext() {
		super();

	}

	@Override
	public GridBeanContext clone() {
		try {
			return (GridBeanContext) super.clone();
		} catch (CloneNotSupportedException e) {
			GPEActivator.log(Status.ERROR, "Error while cloning application GUI context model: "+e.getMessage(), e);
			return null;
		}
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof GridBeanContext)) {
			return false;
		}
		GridBeanContext other = (GridBeanContext) o;
		String myJobClientUrl = getJobClientAddress() == null ? null
				: getJobClientAddress().toString();
		String otherJobClientUrl = other.getJobClientAddress() == null ? null
				: other.getJobClientAddress().toString();
		return other.isEditable() == isEditable
				&& other.isPartOfWorkflow() == isPartOfWorkflow()
				&& CollectionUtil.containSameElements(
						other.getApplicationDomains(), getApplicationDomains())
				&& CollectionUtil.equalOrBothNull(
						other.getDefaultWorkflowSystem(),
						getDefaultWorkflowSystem())
				&& CollectionUtil.equalOrBothNull(otherJobClientUrl,
						myJobClientUrl)
				&& other.getJobExecutionState() == getJobExecutionState()
				&& CollectionUtil.equalOrBothNull(other.getTerminationTime(),
						getTerminationTime());
	}

	/**
	 * The scientific field(s) in wich the diagram is used, e.g. "Chemistry",
	 * "Physics", "Supply Chain Management" etc.
	 * 
	 * @return
	 */
	public List<QName> getApplicationDomains() {
		return applicationDomains;
	}

	public QName getDefaultWorkflowSystem() {
		return defaultWorkflowSystem;
	}

	public String getDisplayedString() {
		return "Context";
	}

	/**
	 * Returns the serialized NodePath of a job client for monitoring the job
	 * once it has been submitted
	 * 
	 * @return
	 */
	public String getJobClientAddress() {
		return jobClientAddress;
	}

	/**
	 * Returns the execution state of the job. See
	 * {@link ExecutionStateConstants} for possible values.
	 * 
	 * @return
	 */
	public int getJobExecutionState() {
		return jobExecutionState;
	}

	public ParameterMetaData<?> getMetaData() {
		// no metadata necessary
		return null;
	}

	public Integer getTerminationTime() {
		return terminationTime;
	}

	public boolean isEditable() {
		return isEditable;
	}

	public boolean isInputParameter() {
		return true;
	}

	public boolean isPartOfWorkflow() {
		return partOfWorkflow;
	}

	public void resetProcessedValues() {
		// do nothing

	}

	public void setApplicationDomains(List<QName> applicationDomains) {
		this.applicationDomains = applicationDomains;
	}

	public void setDefaultWorkflowSystem(QName workflowSystemType) {
		this.defaultWorkflowSystem = workflowSystemType;
	}

	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	public void setJobClientAddress(String jobClientAddress) {
		this.jobClientAddress = jobClientAddress;
	}

	public void setJobExecutionState(int jobExecutionState) {
		this.jobExecutionState = jobExecutionState;
	}

	public void setPartOfWorkflow(boolean partOfWorkflow) {
		this.partOfWorkflow = partOfWorkflow;
	}

	public void setTerminationTime(Integer terminationTime) {
		this.terminationTime = terminationTime;
	}

	/**
	 * Returns whether the GridBean has been newly created. This is set to
	 * false after the job has been properly persisted for the first time.
	 * @return
	 */
	public boolean isNewlyCreated() {
		return newlyCreated;
	}

	public void setNewlyCreated(boolean newlyCreated) {
		this.newlyCreated = newlyCreated;
	}

	@Override
	public String toString() {
		return "GridBeanContext [partOfWorkflow=" + partOfWorkflow
				+ ", defaultWorkflowSystem=" + defaultWorkflowSystem
				+ ", applicationDomains=" + applicationDomains
				+ ", isEditable=" + isEditable + ", jobExecutionState="
				+ jobExecutionState + ", jobClientAddress=" + jobClientAddress
				+ ", terminationTime=" + terminationTime + ", newlyCreated="
				+ newlyCreated + "]";
	}
}
