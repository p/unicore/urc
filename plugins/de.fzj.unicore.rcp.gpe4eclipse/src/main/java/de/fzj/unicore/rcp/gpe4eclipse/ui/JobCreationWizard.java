/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.ui;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResourceStatus;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.ide.undo.CreateFileOperation;
import org.eclipse.ui.ide.undo.CreateProjectOperation;
import org.eclipse.ui.ide.undo.WorkspaceUndoUtil;
import org.eclipse.ui.statushandlers.IStatusAdapterConstants;
import org.eclipse.ui.statushandlers.StatusAdapter;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.wizards.newresource.BasicNewResourceWizard;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.util.observer.IObserver;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GPEPathUtils;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

/**
 * Wizard for creating a new workflow project
 */
public class JobCreationWizard extends BasicNewResourceWizard implements
		IExecutableExtension {

	protected JobCreationWizardPageFile pageFile;
	protected JobCreationWizardPageApplication pageApplication;
	protected JobCreationWizardPageResource pageResources;
	protected IProject project;

	private JobCreationInfo jobCreationInfo = new JobCreationInfo();
	private GridBeanClient<IWorkbenchPart> gridBeanClient;

	private boolean jobProjectCreated = false;

	/**
	 * Creates a wizard for creating a new job project in the workspace.
	 */

	public JobCreationWizard() {
		this(true, true, true);
	}

	/**
	 * Creates a wizard for creating a new job project in the workspace.
	 */

	public JobCreationWizard(boolean showApplicationPage, boolean showFilePage,
			boolean showResourcePage) {
		IDialogSettings workbenchSettings = GPEActivator.getDefault()
				.getDialogSettings();
		IDialogSettings section = workbenchSettings
				.getSection("BasicNewProjectResourceWizard");//$NON-NLS-1$
		if (section == null) {
			section = workbenchSettings
					.addNewSection("BasicNewProjectResourceWizard");//$NON-NLS-1$
		}
		setDialogSettings(section);

		if (showApplicationPage) {
			pageApplication = new JobCreationWizardPageApplication(
					jobCreationInfo);
		}
		if (showFilePage) {
			pageFile = new JobCreationWizardPageFile(jobCreationInfo);
		}
		if (showResourcePage) {
			pageResources = new JobCreationWizardPageResource(jobCreationInfo);
		}
	}

	/*
	 * (non-Javadoc) Method declared on IWizard.
	 */
	@Override
	public void addPages() {
		super.addPages();
		if (pageApplication != null) {
			addPage(pageApplication);
		}
		if (pageFile != null) {
			addPage(pageFile);
		}
		if (pageResources != null) {
			addPage(pageResources);
		}
	}

	protected void createJobFileAndOpenEditor(final IFile newFile) {
		Job j = new BackgroundJob("Creating job") {
			@Override
			public IStatus run(IProgressMonitor monitor) {
				CreateFileOperation cfo = new CreateFileOperation(newFile,
						null, null, "Creating job");
				try {
					PlatformUI
							.getWorkbench()
							.getOperationSupport()
							.getOperationHistory()
							.execute(
									cfo,
									monitor,
									WorkspaceUndoUtil
											.getUIInfoAdapter(getShell()));
					final GridBeanClient<IWorkbenchPart> gbClient = getGridBeanClient();
					String jobName = newFile.getName();
					String ext = newFile.getFileExtension();
					jobName = jobName.substring(0,
							jobName.length() - ext.length() - 1);
					gbClient.getGridBeanJob().getModel()
							.set(IGridBeanModel.JOBNAME, jobName);
					List<IAdaptable> resources = getResources();
					if (resources != null && resources.size() > 0) {
						GridBeanUtils.insertSelectedTargetHosts(gbClient
								.getGridBeanJob().getModel(), resources);
					}

					// save the job.. afterwards, open the editor
					gbClient.saveJob(newFile.getLocation().toOSString(),
							new IObserver() {
								public void observableUpdate(
										Object theObserved, Object changeCode) {
									if (changeCode instanceof Exception) {
										GPEActivator.log(IStatus.ERROR,
												"Unable to create job in file "
														+ newFile.getLocation()
																.toOSString(),
												(Exception) changeCode);
									} else {
										// Since the file resource was created
										// fine, open it for editing
										openEditor(newFile);
									}
								}
							});
				} catch (Exception e) {
					GPEActivator.log(Status.ERROR,
							"Error while creating job file or opening editor: "
									+ e.getMessage(), e);
				}

				return Status.OK_STATUS;
			}
		};
		j.schedule();
	}

	/**
	 * Creates a new project resource with the selected name.
	 * <p>
	 * In normal usage, this method is invoked after the user has pressed Finish
	 * on the wizard; the enablement of the Finish button implies that all
	 * controls on the pages currently contain valid values.
	 * </p>
	 * <p>
	 * Note that this wizard caches the new project once it has been
	 * successfully created; subsequent invocations of this method will answer
	 * the same project resource without attempting to create it again.
	 * </p>
	 * 
	 * @return the created project resource, or <code>null</code> if the project
	 *         was not created
	 */
	private IProject createNewProject() {
		// get a project descriptor
		IPath projectPath = jobCreationInfo.getFile().uptoSegment(1);
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IProject project = workspace.getRoot().getProject(
				projectPath.segment(0));
		if (!project.exists()) {
			final IProjectDescription description = workspace
					.newProjectDescription(project.getName());
			// description.setLocation(workspace.getRoot().getLocation().append(projectPath));
			description.setNatureIds(new String[] { "de.fzj.unicore.rcp.gpe4eclipse.JobNature" });

			// create the new project operation
			IRunnableWithProgress op = new IRunnableWithProgress() {
				public void run(IProgressMonitor monitor)
						throws InvocationTargetException {
					CreateProjectOperation op = new CreateProjectOperation(
							description, "Creating project");
					try {
						PlatformUI
								.getWorkbench()
								.getOperationSupport()
								.getOperationHistory()
								.execute(
										op,
										monitor,
										WorkspaceUndoUtil
												.getUIInfoAdapter(getShell()));
					} catch (ExecutionException e) {
						throw new InvocationTargetException(e);
					}
				}
			};

			// run the new project creation operation
			try {
				getContainer().run(true, true, op);
			} catch (InterruptedException e) {
				return null;
			} catch (InvocationTargetException e) {
				Throwable t = e.getTargetException();
				if (t instanceof ExecutionException
						&& t.getCause() instanceof CoreException) {
					CoreException cause = (CoreException) t.getCause();
					StatusAdapter status;
					if (cause.getStatus().getCode() == IResourceStatus.CASE_VARIANT_EXISTS) {
						status = new StatusAdapter(
								new Status(
										IStatus.ERROR,
										GPEActivator.PLUGIN_ID,
										" underlying file system is case insensitive."
												+ "There is an existing project or directory"
												+ " that conflicts with ''{0}''.",
										cause));
					} else {
						status = new StatusAdapter(new Status(IStatus.ERROR,
								GPEActivator.PLUGIN_ID, "Error"));
					}
					status.setProperty(IStatusAdapterConstants.TITLE_PROPERTY,
							"Error");
					StatusManager.getManager().handle(status,
							StatusManager.BLOCK);
				} else {
					StatusAdapter status = new StatusAdapter(new Status(
							IStatus.WARNING, GPEActivator.PLUGIN_ID, 0,
							NLS.bind("Internal error: {0}", t.getMessage()), t));
					status.setProperty(IStatusAdapterConstants.TITLE_PROPERTY,
							"Creation Problems");
					StatusManager.getManager().handle(status,
							StatusManager.LOG | StatusManager.BLOCK);
				}
				return null;
			}
		}
		IPath path = GPEPathUtils.inputFileDirForProject(project);
		IFolder f = PathUtils.absolutePathToEclipseFolder(path);
		if (!f.exists()) {
			try {
				f.create(true, true, null);
			} catch (CoreException e) {
				GPEActivator.log(
						Status.ERROR,
						"Error while creating input directory for job: "
								+ e.getMessage(), e);
			}
		}
		jobProjectCreated = true;
		return project;
	}

	public IFile getAbsoluteFile() {
		return project.getFile(jobCreationInfo.getFile().removeFirstSegments(1)
				.addFileExtension(Constants.JOB_FORMAT));
	}

	public GridBeanInfo getApplication() {
		return jobCreationInfo.getGridBeanInfo();
	}

	/**
	 * Returns the newly created project.
	 * 
	 * @return the created project, or <code>null</code> if project not created
	 */
	public IPath getFile() {
		return jobCreationInfo.getFile();
	}

	public GridBeanClient<IWorkbenchPart> getGridBeanClient() {
		if (gridBeanClient == null) {
			gridBeanClient = GPEActivator.getDefault().getClient()
					.createGridBeanClient();
		}
		try {
			if (gridBeanClient.getGridBeanJob().getModel() == null) {
				loadGridBean();
			}
		} catch (Exception e) {
			loadGridBean();
		}

		return gridBeanClient;
	}

	public IProject getProject() {
		return project;
	}

	public List<IAdaptable> getResources() {
		return jobCreationInfo.getResources();
	}

	/*
	 * (non-Javadoc) Method declared on IWorkbenchWizard.
	 */
	@Override
	public void init(IWorkbench workbench, IStructuredSelection currentSelection) {
		super.init(workbench, currentSelection);
		setNeedsProgressMonitor(true);
		setWindowTitle("Job Creation");
	}

	/*
	 * (non-Javadoc) Method declared on BasicNewResourceWizard.
	 */
	@Override
	protected void initializeDefaultPageImageDescriptor() {
		// TODO the image does not exist
		ImageDescriptor desc = GPEActivator.imageDescriptorFromPlugin(
				GPEActivator.PLUGIN_ID, "wizban/newprj_wiz.png");
		setDefaultPageImageDescriptor(desc);
	}

	public boolean isJobProjectCreated() {
		return jobProjectCreated;
	}

	protected void loadGridBean() {
		GridBeanUtils.loadGridBeanAndWait(gridBeanClient, getApplication(),
				getWorkbench().getDisplay());
	}

	protected JobEditor openEditor(final IFile newFile) {
		try {
			IWorkbenchWindow dwindow = getWorkbench()
					.getActiveWorkbenchWindow();
			IWorkbenchPage page = dwindow.getActivePage();
			if (page != null) {
				JobEditorInput input = new JobEditorInput(getGridBeanClient(),
						newFile.getLocation());
				return (JobEditor) IDE.openEditor(page, input, JobEditor.ID,
						true);
			}
		} catch (org.eclipse.ui.PartInitException e) {
			GPEActivator.log(Status.ERROR,
					"Error while opening editor: " + e.getMessage(), e);

		}
		return null;
	}

	/*
	 * (non-Javadoc) Method declared on IWizard.
	 */
	@Override
	public boolean performFinish() {

		project = createNewProject();

		if (jobCreationInfo.getFile() == null) {
			return false;
		}

		// updatePerspective();
		selectAndReveal(project);
		// check for success
		// make sure that each "finish" method is called in order to clean up!!

		boolean projectOk = pageFile == null || pageFile.finish();
		boolean appOk = pageApplication == null || pageApplication.finish();
		boolean resourcesOk = pageResources == null || pageResources.finish();
		boolean success = projectOk && appOk && resourcesOk;
		if (!success) {
			return false;
		}
		IFile jobFile = getAbsoluteFile();
		createJobFileAndOpenEditor(jobFile);
		return true;
	}

	public void setApplication(GridBeanInfo application) {
		jobCreationInfo.setGridBeanInfo(application);
	}

	public void setFile(IPath path) {
		jobCreationInfo.setFile(path);

	}

	public void setGridBeanClient(GridBeanClient<IWorkbenchPart> gridBeanClient) {
		this.gridBeanClient = gridBeanClient;
	}

	/**
	 * Stores the configuration element for the wizard. The config element will
	 * be used in <code>performFinish</code> to set the result perspective.
	 */
	public void setInitializationData(IConfigurationElement cfig,
			String propertyName, Object data) {
	}

	public void setResources(List<IAdaptable> resources) {
		jobCreationInfo.setResources(resources);
	}


}
