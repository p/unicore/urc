/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.actions;

import java.util.List;

import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IWorkbenchPart;

import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.common.actions.UnicoreCommonActionConstants;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.FetchActivityOutcomeRequest;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GridBeanActivityPart;
import de.fzj.unicore.rcp.wfeditor.actions.WFEditorAction;

/**
 * An action to delete selected objects.
 */
public class FetchActivityOutcomeAction extends WFEditorAction {
	/**
	 * Constructs a <code>ShowGridBeanViewAction</code> using the specified
	 * part.
	 * 
	 * @param part
	 *            The part for this action
	 */
	public FetchActivityOutcomeAction(IWorkbenchPart part) {
		super(part);
		setLazyEnablementCalculation(true);
	}

	/**
	 * Returns <code>true</code> if the selected object is of type
	 * GridBeanActivityPart.
	 * 
	 * @return <code>true</code> if the command should be enabled
	 */
	@Override
	protected boolean calculateEnabled() {
		Command cmd = createCommand(getSelectedObjects());
		if (cmd == null) {
			return false;
		}
		return cmd.canExecute();
	}

	public Command createCommand(List<?> objects) {
		if (objects.isEmpty() || objects.size() > 1) {
			return null;
		}
		if (!(objects.get(0) instanceof GridBeanActivityPart)) {
			return null;
		}
		GridBeanActivityPart part = (GridBeanActivityPart) objects.get(0);
		Request req = new FetchActivityOutcomeRequest();
		return part.getCommand(req);

	}

	/**
	 * Initializes this action's text and images.
	 */
	@Override
	protected void init() {
		super.init();
		setText("Fetch Output Files");
		setToolTipText("Fetch and display output files for this activity.");
		setId(UnicoreCommonActionConstants.ACTION_FETCH_OUTCOMES);
		setImageDescriptor(UnicoreCommonActivator
				.getImageDescriptor("fetchOutput.gif"));
		setDisabledImageDescriptor(UnicoreCommonActivator
				.getImageDescriptor("fetchOutput.gif"));
		setEnabled(false);
	}

	@Override
	public void run() {
		Command cmd = createCommand(getSelectedObjects());
		execute(cmd);
	}

}
