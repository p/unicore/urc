package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types;

import com.intel.gpe.clients.api.jsdl.ResourceRequirementType;

public class ScheduleType implements ResourceRequirementType{

	protected boolean enabled = false;
	protected String scheduledTime;

	public ScheduleType() {

	}

	public ScheduleType(String scheduledTime) {
		this.scheduledTime = scheduledTime;
	}


	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ScheduleType)) {
			return false;
		}
		ScheduleType other = (ScheduleType) obj;
		if (enabled != other.enabled) {
			return false;
		}
		if (scheduledTime == null) {
			if (other.scheduledTime != null) {
				return false;
			}
		} else if (!scheduledTime.equals(other.scheduledTime)) {
			return false;
		}
		return true;
	}

	public String getScheduledTime() {
		return scheduledTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (enabled ? 1231 : 1237);
		result = prime * result + ((scheduledTime == null) ? 0 : scheduledTime.hashCode());
		return result;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setScheduledTime(String scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	@Override
	public String toString() {
		return enabled ? "ScheduledType [Scheduled time=" + scheduledTime + "]" : "disabled";
	}

}
