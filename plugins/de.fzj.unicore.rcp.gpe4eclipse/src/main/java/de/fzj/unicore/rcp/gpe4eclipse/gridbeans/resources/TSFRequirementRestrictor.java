package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import org.unigrids.x2006.x04.services.tsf.TargetSystemFactoryPropertiesDocument.TargetSystemFactoryProperties;


public class TSFRequirementRestrictor extends TSRequirementRestrictor {
	public TSFRequirementRestrictor(TargetSystemFactoryProperties props) {
		super(new ResourceProviderAdapter(props));
	}
}
