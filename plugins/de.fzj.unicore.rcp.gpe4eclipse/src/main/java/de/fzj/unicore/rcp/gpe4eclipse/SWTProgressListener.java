/*
 * Copyright (c) 2000-2005, Intel Corporation All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Intel Corporation nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.fzj.unicore.rcp.gpe4eclipse;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;

import com.intel.gpe.clients.api.async.IProgressListener;

/**
 * This class serves as a bridge between the GPE IProgessListener and the
 * Eclipse IProgressMonitor
 * 
 * @author Bastian Demuth
 * @version $Id: AsyncProvider.java,v 1.3 2006/09/28 12:00:24 vashorin Exp $
 * 
 */
public class SWTProgressListener implements IProgressListener {

	IProgressMonitor monitor;

	public SWTProgressListener(IProgressMonitor monitor) {
		this.monitor = monitor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.IProgressListener#beginSubTask(java.lang.String,
	 * int)
	 */
	public IProgressListener beginSubTask(String name, int work) {
		monitor.subTask(name);
		return new SWTProgressListener(new SubProgressMonitor(monitor, work,
				SubProgressMonitor.PREPEND_MAIN_LABEL_TO_SUBTASK));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.IProgressListener#beginTask(java.lang.String,
	 * int)
	 */
	public void beginTask(String name, int totalWork) {
		monitor.beginTask(name, totalWork);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.IProgressListener#done()
	 */
	public void done() {
		monitor.done();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.IProgressListener#internalWorked(double)
	 */
	public void internalWorked(double work) {
		monitor.internalWorked(work);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.IProgressListener#isCanceled()
	 */
	public boolean isCanceled() {
		return monitor.isCanceled();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.IProgressListener#setCanceled(boolean)
	 */
	public void setCanceled(boolean value) {
		monitor.setCanceled(value);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.IProgressListener#setTaskName(java.lang.String)
	 */
	public void setTaskName(String name) {
		monitor.setTaskName(name);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.client2.IProgressListener#worked(int)
	 */
	public void worked(int work) {
		monitor.worked(work);
	}

}
