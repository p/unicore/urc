package de.fzj.unicore.rcp.gpe4eclipse;

import org.eclipse.core.runtime.Status;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.intel.gpe.clients.all.gridbeans.InternalGridBean;
import com.intel.gpe.clients.all.providers.GridBeanProvider;
import com.intel.gpe.clients.api.async.AsyncClient;
import com.intel.gpe.clients.api.security.GPESecurityManager;
import com.intel.gpe.util.defaults.preferences.IPreferences;
import com.intel.gpe.util.swing.controls.configurable.MessageProvider;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.GridBeanRegistry;

public class SWTGridBeanProvider extends GridBeanProvider {

	public SWTGridBeanProvider(IPreferences userDefaults,
			GPESecurityManager securityManager, AsyncClient asyncClient,
			ClassLoader classLoader, MessageProvider messageProvider) {
		super(userDefaults, securityManager, asyncClient, classLoader, messageProvider);
	}

	
	protected InternalGridBean resolve(GridBeanInfo info) throws Exception {
		GridBeanRegistry registry = GPEActivator.getDefault().getGridBeanRegistry();
		
		InternalGridBean result = registry.getGridBean(info);
		if(result == null) info = registry.getNewerGridBean(info);
		if(info != null) return registry.getGridBean(info);
		return null;
	}
	
	protected InternalGridBean getGridBean(String pathToJar) {
		GridBeanRegistry registry = GPEActivator.getDefault().getGridBeanRegistry();
		try {
			return registry.getGridBean(pathToJar);
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Unable to load application GUI " +
					"plugin with path "+pathToJar+": "+e.getMessage(),e);
			return null;
		}
	}
}
