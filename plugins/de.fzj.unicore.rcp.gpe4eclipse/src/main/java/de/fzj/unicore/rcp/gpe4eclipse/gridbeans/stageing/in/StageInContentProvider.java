/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Display;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageListObserver;

/**
 * The content provider class is responsible for providing objects to the view.
 * It can wrap existing objects in adapters or simply return objects as-is.
 * These objects may be sensitive to the current input of the view, or ignore it
 * and always show the same content (like Task List, for example).
 */
class StageInContentProvider implements IStructuredContentProvider,
		IStageListObserver {

	Viewer viewer;
	StageInList stageList;
	boolean refreshing = false;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.identity.sitelist.ISiteListViewer#addSite(de.fzj.unicore
	 * .rcp.identity.sitelist.Site)
	 */
	public void addStage(IStage stage) {
		refreshViewer();	}

	public void dispose() {
		stageList.removeChangeListener(this);
	}

	public Object[] getElements(Object parent) {
		if (parent instanceof StageInList) {
			StageInList siteList = (StageInList) parent;
			return siteList.getStages();
		}
		return null;
	}

	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		viewer = v;
		if (newInput != null) {
			stageList = (StageInList) newInput;
			stageList.addChangeListener(this);
		}
		if (oldInput != null) {
			((StageInList) oldInput).removeChangeListener(this);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.identity.sitelist.ISiteListViewer#removeSite(de.fzj
	 * .unicore.rcp.identity.sitelist.Site)
	 */
	public void removeStage(IStage stage) {
		refreshViewer();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.identity.sitelist.ISiteListViewer#updateSite(de.fzj
	 * .unicore.rcp.identity.sitelist.Site)
	 */
	public void updateStage(IStage stage) {
		refreshViewer();

	}
	
	private void refreshViewer()
	{
		if(refreshing) return;
		refreshing = true;
		Display.getCurrent().asyncExec(new Runnable() {
			
			@Override
			public void run() {
				refreshing = false;
				if(!viewer.getControl().isDisposed())
				{
					viewer.refresh();
				}
				
			}
		});

	}
}