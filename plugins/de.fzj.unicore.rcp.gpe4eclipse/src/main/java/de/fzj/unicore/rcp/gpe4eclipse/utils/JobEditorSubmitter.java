package de.fzj.unicore.rcp.gpe4eclipse.utils;

import java.io.File;
import java.util.Calendar;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Display;

import com.intel.gpe.clients.all.exceptions.GPEJobFailedException;
import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.clients.all.gridbeans.GridBeanJob;
import com.intel.gpe.clients.all.i18n.Messages;
import com.intel.gpe.clients.all.i18n.MessagesKeys;
import com.intel.gpe.clients.all.requests.SubmitRequest;
import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.JobClient;
import com.intel.gpe.clients.api.TargetSystemClient;
import com.intel.gpe.clients.api.async.AsyncClient;
import com.intel.gpe.clients.common.clientwrapper.ClientWrapper;
import com.intel.gpe.gridbeans.ErrorSet;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.util.observer.IObserver;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.FileUtils;
import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditor;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowUtils;

public class JobEditorSubmitter {

	class SubmitObserver implements IObserver {
		JobEditor jobEditor;
		String oldName;

		public SubmitObserver(JobEditor jobEditor, String oldName) {
			super();
			this.jobEditor = jobEditor;
			this.oldName = oldName;
		}

		public void observableUpdate(Object theObserved, Object changeCode) {
			try {
				
				if (changeCode instanceof JobClient) {
					
					JobClient jobClient = (JobClient) changeCode;
					final ClientWrapper<JobClient, String> job = new ClientWrapper<JobClient, String>(
							jobClient, jobClient.getName());
					if (jobEditor.isDisposed()) {
						deleteFailedJobCopy(jobEditor);
						return;
					}
					jobEditor.setJobClient(job);
					jobEditor
							.setExecutionState(ExecutionStateConstants.STATE_RUNNING);

				} else if (changeCode instanceof Throwable) {
					if (changeCode instanceof GPEJobFailedException) {
						GPEJobFailedException e = (GPEJobFailedException) changeCode;
						String msg = "The job " + e.getJobClient().getName()
								+ " has failed";
						GPEActivator.log(IStatus.ERROR, msg, e);
					} else {
						Throwable t = (Throwable) changeCode;

						GPEActivator.log(IStatus.ERROR,
								"Could not submit job: " + t.getMessage(),
								new Exception(t));
					}
					jobEditor
							.setExecutionState(ExecutionStateConstants.STATE_FAILED);
				}
			} catch (Exception e) {
				GPEActivator.log(IStatus.ERROR,
						"Unable to start monitoring submitted job", e);
			} finally {
				refreshWorkspaceAsync();
			}

		}
	}

	protected boolean copyComplete;

	protected JobEditor copyAndPrepare(final JobEditor jobEditor) {

		copyComplete = false;
		File newFile;
		try {
			IProject project = jobEditor.getInputFileProject();
			if (project == null) {
				GPEActivator
						.log(IStatus.ERROR,
								"Unable to create copy of job for submission. The job project could not be determined.");
				return null;
			}
			IPath submittedPath = WorkflowUtils.getSubmittedPath();
			try {
				jobEditor.getGridBeanClient().applyInputPanelValues();
			} catch (Exception e) {
				GPEActivator.log(IStatus.ERROR,
						"Could not apply pending Job input values", e);
			}
			String jobName = (String) jobEditor.getGridBeanModel().get(
					IGridBeanModel.JOBNAME);
			File f = jobEditor.getInputFile();
			if(f == null) return null;
			String fileName = jobName == null ? f.getName() : new Path(jobName).addFileExtension(
					Constants.JOB_FORMAT).toOSString();
			File newDir = PathUtils.pathToFile(submittedPath, project);
			newDir.mkdirs();
			newFile = new File(newDir, fileName);

		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to create copy of job for submission", e);
			return null;
		}
		final File finalNewFile = newFile;
		Job j = new BackgroundJob("") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					jobEditor.doActualSave(finalNewFile, monitor);
				} finally {
					copyComplete = true;

				}
				return Status.OK_STATUS;
			}
		};
		j.schedule();

		Display d = jobEditor.getSite().getShell().getDisplay();
		boolean isMainThread = Thread.currentThread().equals(d.getThread());

		long start = System.currentTimeMillis();
		// wait until the job is loaded but give up after 30 seconds
		while (!copyComplete && System.currentTimeMillis() - start < 30000) {
			boolean sleep = isMainThread ? !d.readAndDispatch() : true;
			if (sleep) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
				}
			}
		}

		JobEditor submittedEditor = null;
		try {
			IPath path = new Path(newFile.getAbsolutePath());
			submittedEditor = (JobEditor) PathUtils.openEditor(jobEditor
					.getEditorSite().getPage(), path);

		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to open Job Editor in monitoring mode", e);
		}

		// wait until the editor is opened but give up after 30 seconds
		start = System.currentTimeMillis();
		while (submittedEditor.isLoading()
				&& System.currentTimeMillis() - start < 30000) {
			boolean sleep = isMainThread ? !d.readAndDispatch() : true;
			if (sleep) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
				}
			}
		}

		return submittedEditor;

	}

	protected void deleteFailedJobCopy(JobEditor jobEditor) {
		try {
			if (!jobEditor.isDisposed()) {
				jobEditor.getSite().getPage().closeEditor(jobEditor, false);
			}
			File f = jobEditor.getInputFile();
			if(f == null) return;
			File parent = f.getParentFile();
			FileUtils.rm(parent);
			refreshWorkspaceAsync();

		} catch (Exception e) {
			GPEActivator.log(IStatus.WARNING,
					"Unable to clean up after failed job submission", e);
		}
	}

	protected boolean jobValid(GridBeanClient<?> gridBeanClient,
			GridBeanJob gridBeanJob) {

		ErrorSet errors = gridBeanClient.validateJob();
		if (errors.size() > 0) {
			String message = Messages
					.getString(MessagesKeys.common_actions_SubmitAction_Job_is_not_valid__nReason__n);
			for (int i = 0; i < errors.size(); i++) {
				message += " "+errors.getErrorAt(i).getMessage();
			}

			GPEActivator.log(IStatus.ERROR, message);
			return false;
		}
		return true;
	}

	protected IStatus refreshWorkspace(IProgressMonitor progress) {

		try {
			ResourcesPlugin.getWorkspace().getRoot()
					.refreshLocal(IResource.DEPTH_INFINITE, progress);
		} catch (Exception e) {
			// this is a best effort service
		}
		return Status.OK_STATUS;
	}

	protected void refreshWorkspaceAsync() {
		BackgroundJob todo = new BackgroundJob(
				"Refreshing workspace...") {
			@Override
			public IStatus run(final IProgressMonitor progress) {
				return refreshWorkspace(progress);
			}
		};
		todo.schedule();
	}

	public void submit(final JobEditor jobEditor) {
		submit(jobEditor, true);
	}

	public void submit(final JobEditor jobEditor, boolean makeCopy) {
		TargetSystemClient targetSystem = jobEditor.getTargetSystem();
		if (targetSystem == null) {
			GPEActivator
					.log(IStatus.ERROR,
							"No valid target system selected for job. Please use the \"Resources\" tab for choosing one.");
			return;
		}
		jobEditor.setSubmittingJob(true);
		jobEditor.checkSubmitActionEnabled();

		try {
			if (makeCopy) {
				JobEditor submittedEditor = copyAndPrepare(jobEditor);
				if (submittedEditor != null) {
					Calendar terminationTimeCalendar = null;
					if (jobEditor.getTerminationTime() != null) {
						terminationTimeCalendar = Calendar.getInstance();
						terminationTimeCalendar.add(Calendar.HOUR,
								jobEditor.getTerminationTime());
					}

					boolean success = submitCopy(submittedEditor,
							terminationTimeCalendar, targetSystem);
					if (!success) {
						deleteFailedJobCopy(submittedEditor);
					}
				}
			} else {
					Calendar terminationTimeCalendar = null;
					if(jobEditor.getTerminationTime() != null)
					{
						terminationTimeCalendar = Calendar.getInstance();
						terminationTimeCalendar
							.add(Calendar.HOUR,			
											jobEditor.getTerminationTime());
					}
					submitCopy(jobEditor,
							terminationTimeCalendar, targetSystem);
			}
		} finally {
			jobEditor.setSubmittingJob(false);
			jobEditor.checkSubmitActionEnabled();
		}
	}

	protected boolean submitCopy(JobEditor copy, Calendar terminationTime,
			TargetSystemClient targetSystem) {
		GridBeanClient<?> gridBeanClient = copy.getGridBeanClient();
		Client client = gridBeanClient.getClient();
		AsyncClient asyncClient = client;

		GridBeanJob gridBeanJob;
		try {
			gridBeanJob = gridBeanClient.getGridBeanJob();
			if (!jobValid(gridBeanClient, gridBeanJob)) {
				return false;
			}
		} catch (Exception ex) {
			GPEActivator.log(IStatus.ERROR, "Cannot get job instance", ex);
			return false;
		}

		IGridBean model = gridBeanJob.getModel();

		// append time stamp to the original job name
		String oldName = (String) model.get(IGridBeanModel.JOBNAME);
		String name = oldName == null ? "Job" : oldName;
		File f = copy.getInputFile();
		if(f == null) 
		{
			GPEActivator.log(IStatus.ERROR, "Cannot find job input file.");
			return false;
		}
		name += " " + f.getParentFile().getName();

		// the SubmitObserver will trigger monitoring the job
		// and reset the job name to the original value
		IObserver submitObserver = new SubmitObserver(copy, oldName);

		try {
			copy.setExecutionState(ExecutionStateConstants.STATE_SUBMITTING);
			copy.setListeningToGridBeanModel(false);
			model.set(IGridBeanModel.JOBNAME, name);
			copy.setListeningToGridBeanModel(true);
			SubmitRequest request = new SubmitRequest(targetSystem, model,
					terminationTime, client);
			int interval = 1000 * GPEActivator.getDefault()
					.getPreferenceStore()
					.getInt(GPE4EclipseConstants.P_JOB_POLLING_INTERVAL);
			request.setRefreshInterval(interval);
			asyncClient.executeRequest(request, submitObserver);

		} catch (Exception e1) {
			GPEActivator
					.log(IStatus.ERROR,
							Messages.getString(MessagesKeys.common_actions_SubmitAction_Cannot_get_list_of_remote_files),
							e1);
			return false;
		}

		return true;
	}

}
