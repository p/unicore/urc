package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.Job;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.gridbeans.jsdl.GPEJobImpl;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.ParameterTypeProcessorRegistry;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.SWTProgressListener;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.IMatchmaker;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.IServiceContentProvider;
import de.fzj.unicore.rcp.servicebrowser.ServiceContentProvider;
import de.fzj.unicore.rcp.servicebrowser.filters.SkipNodeViewerFilter;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceSelectionPanel;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceTreeViewer;

public class MatchmakingResourceFilter extends SkipNodeViewerFilter implements
		PropertyChangeListener {

	class Updater extends BackgroundJob {

		public Updater() {
			super("updating matching target systems");

		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {

			IProgressListener progress = new SWTProgressListener(monitor);
			progress.beginTask("matchmaking", 3);
			try {
				runAgain = false;
				String oldAppName = currentJob == null ? null : currentJob
						.getApplicationName();
				String oldAppVersion = currentJob == null ? null : currentJob
						.getApplicationVersion();
				currentJob = new GPEJobImpl();
				if (gridbean == null) {
					return Status.CANCEL_STATUS; // can happen when the editor
													// gets closed before we are
													// executed
				}
				gridbean.setupJobDefinition(currentJob);

				progress.worked(2);
				// process list of GridBeans input parameter values. Processors
				// MUST NOT override the user
				// settings for parameters but store processed values in the
				// appropriate additional fields.
				List<IGridBeanParameter> params = new ArrayList<IGridBeanParameter>(
						gridbean.getInputParameters());
				params.addAll(gridbean.getOutputParameters());
				List<IGridBeanParameterValue> values = new ArrayList<IGridBeanParameterValue>();
				for (IGridBeanParameter param : params) {
					IGridBeanParameterValue value = (IGridBeanParameterValue) gridbean
							.get(param.getName());
					if (value == null) {
						continue;
					}
					// before processing: reset old processed values!
					value.resetProcessedValues();
					values.add(value);
				}

				Map<String, Object> processorParams = new HashMap<String, Object>();
				IProgressListener subProgress = progress.beginSubTask(
						"writing job description", 1);
				processParameters(currentJob,
						ProcessingConstants.WRITE_PROCESSED_PARAMS_TO_JSDL,
						params, values, processorParams, subProgress);

				if (resourcesChanged
						|| !CollectionUtil.equalOrBothNull(
								currentJob.getApplicationName(), oldAppName)
						|| !CollectionUtil.equalOrBothNull(
								currentJob.getApplicationVersion(),
								oldAppVersion)) {
					resourcesChanged = false;
					runMatchMakers();
					if (filterController != null) {
						PlatformUI.getWorkbench().getDisplay()
								.asyncExec(new Runnable() {
									public void run() {
										filterController.refreshViewers();
									}
								});
					}
				}

			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, "Error while filtering suitable target systems: "+e.getMessage(), e);
			} finally {
				progress.done();
				if (runAgain) {
					schedule();
				}
			}

			return Status.OK_STATUS;
		}
	}

	ServiceSelectionPanel panel;
	// store a reference to the content provider as he can notify us of node
	// changes
	ServiceContentProvider contentProvider;
	Client client;
	IGridBean gridbean;
	GPEJob currentJob;
	Updater updater = new Updater();
	BackgroundJob backgroundMatcher = null;
	Queue<Node> nodesToMatch = new ConcurrentLinkedQueue<Node>();
	boolean runAgain = false;
	boolean resourcesChanged = false;

	boolean autoSchedule = true;

	/**
	 * Cache nodes that can process the job in here. We use a concurrent map
	 * here for thread safety.
	 */
	Map<Node, Integer> matchingObjects;

	/**
	 * Cache nodes that can NOT process the job in here. We use a concurrent map
	 * here for thread safety.
	 */
	Map<Node, Integer> refusedObjects;

	public MatchmakingResourceFilter(Client client, IGridBean model,
			ServiceSelectionPanel panel) {
		super("matchmaking filter", true);
		this.client = client;
		this.gridbean = model;
		this.panel = panel;
		IContentProvider cp = panel.getTreeViewer().getContentProvider();
		if (cp instanceof ServiceContentProvider) {
			contentProvider = (ServiceContentProvider) cp;
			contentProvider.addPropertyChangeListener(this);
		}

	}

	public void dispose() {
		if (gridbean != null) {
			gridbean.removePropertyChangeListener(this);
			gridbean = null;
		}
		if (contentProvider != null) {
			contentProvider.removePropertyChangeListener(this);
		}
		panel = null;
	}

	public void init() {
		this.gridbean.addPropertyChangeListener(this);
	}

	public boolean isAutoSchedule() {
		return autoSchedule;
	}

	protected boolean nodeMatches(Node node) {
		
		
		IMatchmaker mm = (IMatchmaker) node.getAdapter(IMatchmaker.class);
		if (mm == null) {
			return false;
		}
		else
		{
			if(node.getState() >= Node.STATE_FAILED) return false;	
		}
		return mm.canHandleJob(currentJob) == null;
	}

	protected void processParameters(Job job, int processStep,
			List<IGridBeanParameter> inputParameters,
			List<IGridBeanParameterValue> values,
			Map<String, Object> processorParams, IProgressListener progress)
			throws Exception {
		if (job instanceof GPEJob) {
			GPEJob gpeJob = (GPEJob) job;
			ParameterTypeProcessorRegistry.getDefaultRegistry()
					.processParameters(client, gpeJob, processStep,
							inputParameters, values, processorParams, progress);
		}

	}

	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource() instanceof Node) {
			// a node has changed
			Node n = (Node) evt.getSource();
			if (!nodesToMatch.contains(n)) {
				nodesToMatch.add(n);
			}
			Node parent = n.getParent();
			if (parent != null) {
				panel.getTreeViewer().refresh(parent);
			}
			
		} else {
			// the gridbean has changed, so the job might have changed
			if (isAutoSchedule()) {
				scheduleUpdate(evt);
			}
		}
	}

	protected void runMatchMakers() {

		try {
			active = false;
			if (matchingObjects == null) {
				matchingObjects = new ConcurrentHashMap<Node, Integer>();
				refusedObjects = new ConcurrentHashMap<Node, Integer>();
			} else {
				matchingObjects.clear();
				refusedObjects.clear();
			}
			IServiceContentProvider cp = panel.getTreeViewer()
					.getServiceContentProvider();
			if (cp == null) {
				return;
			}
			ServiceTreeViewer viewer = panel.getTreeViewer();
			if (viewer == null) {
				return;
			}
			GridNode gridNode = viewer.getGridNode();
			if (gridNode == null) {
				return;
			}
			Node root = gridNode.getParent();
			if (root == null) {
				return;
			}
			Object[] all = cp.getSubtreeElements(root);

			for (Object element : all) {
				boolean ok = true;
				Node node = null;
				if (!(element instanceof Node)) {
					ok = false;
				} else {

					node = (Node) element;
					ok = nodeMatches(node);
				}
				if (ok) {
					matchingObjects.put(node, 1);
				} else {
					refusedObjects.put(node, 1);
				}
			}
			active = true;
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while filtering suitable target systems: "+e.getMessage(), e);
		}
	}

	public void scheduleUpdate(final PropertyChangeEvent evt) {
		// only run ONE instance of the update job at the same time!
		synchronized (updater) {

			if (evt == null
					|| IGridBeanModel.RESOURCES.toString().equals(
							evt.getPropertyName())) {
				resourcesChanged = true;
			}
			if (resourcesChanged
					|| IGridBeanModel.APPLICATION_NAME.toString().equals(
							evt.getPropertyName())
					|| IGridBeanModel.APPLICATION_VERSION.toString().equals(
							evt.getPropertyName())) {
				// update is necessary
				if (updater.getState() == org.eclipse.core.runtime.jobs.Job.NONE) {
					// updater is not running => schedule it
					updater.schedule(100);
				} else {
					runAgain = true; // updater is already running => schedule
										// later
				}
			}

		}

	}

	@Override
	public boolean select(Viewer viewer, Object parentElement,
			final Object element) {
		if (!isActive() || matchingObjects == null) {
			return true;
		}
		if (matchingObjects.containsKey(element)
				&& !nodesToMatch.contains(element)) {
			return true;
		} else if (refusedObjects.containsKey(element)
				&& !nodesToMatch.contains(element)) {
			return false;
		} else {
			// haven't seen this node before.. take a closer look
			// this may take long, so lets do it in a background thread
			Node n = (Node) element;
			if (!nodesToMatch.contains(n)) {
				nodesToMatch.add(n);
			}
			if (backgroundMatcher == null) {
				backgroundMatcher = new BackgroundJob (
						"matching job against target system") {

					@Override
					protected IStatus run(IProgressMonitor monitor) {
						while (!nodesToMatch.isEmpty()) {
							Node n = nodesToMatch.poll();
							if (nodeMatches(n)) {
								matchingObjects.put(n, 1);
								refusedObjects.remove(n);
							} else {
								refusedObjects.put(n, 1);
								matchingObjects.remove(n);
							}
						}
						return Status.OK_STATUS;
					}
				};
			}
			if (!nodesToMatch.isEmpty()
					&& backgroundMatcher.getState() == org.eclipse.core.runtime.jobs.Job.NONE) {
				backgroundMatcher.schedule();
			}

			long start = System.currentTimeMillis();
			long timeout = UnicoreCommonActivator.getDefault()
					.getPreferenceStore()
					.getLong(Constants.P_CONNECTION_TIMEOUT) + 500;
			Display d = PlatformUI.getWorkbench().getDisplay();
			// wait until background thread has finished, but process events and
			// set a timeout
			while (!d.isDisposed()
					&& backgroundMatcher.getState() != org.eclipse.core.runtime.jobs.Job.NONE
					&& System.currentTimeMillis() - start < timeout) {
				boolean sleep = !d.readAndDispatch();
				if (sleep) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
					}
				}
			}
			return matchingObjects.containsKey(element);

		}
	}

	public void setAutoSchedule(boolean autoSchedule) {
		this.autoSchedule = autoSchedule;
	}

}
