/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import com.intel.gpe.gridbeans.IGridBeanModel;

/**
 * provides the table for the ResourceRequirements
 * 
 * @author Christian Hohmann
 * 
 */
public class ResourceRequirementsViewer extends TableViewer {

	public static final String ENABLED = "Use", NAME = "Property",
			VALUE = "Value", UNITS = "Unit", DESCRIPTION = "Description";
	public static final String[] COLUMN_PROPERTIES = new String[] { ENABLED,
			NAME, VALUE, UNITS, DESCRIPTION };
	private RequirementList requirementList;

	private CellEditor[] cellEditors;
	private IGridBeanModel gridBeanModel;

	public ResourceRequirementsViewer(Composite parent,
			IGridBeanModel gridBeanModel, RequirementList requirementList) {
		super(parent, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION);
		this.gridBeanModel = gridBeanModel;
		this.requirementList = requirementList;
		Table table = createTable();

		// initialize action list
		setUseHashlookup(true);

		// Create the cell editors
		cellEditors = new CellEditor[COLUMN_PROPERTIES.length];

		cellEditors[getColumnFor(ENABLED)] = new CheckboxCellEditor(table);

		TextCellEditor nameValueEditor = new TextCellEditor(table);
		((Text) nameValueEditor.getControl()).setEditable(false);
		cellEditors[getColumnFor(NAME)] = nameValueEditor;

		cellEditors[getColumnFor(VALUE)] = new TextCellEditor(table);

		TextCellEditor unitsValueEditor = new TextCellEditor(table);
		((Text) unitsValueEditor.getControl()).setEditable(false);
		cellEditors[getColumnFor(UNITS)] = unitsValueEditor;

		setCellEditors(cellEditors);

		RequirementTableCellModifier modifier = new RequirementTableCellModifier(
				this);
		setCellModifier(modifier);
		RequirementContentProvider requirementContentProvider = new RequirementContentProvider(
				getGridBeanModel());
		setContentProvider(requirementContentProvider);

		setLabelProvider(new RequirementLabelProvider(this));
		// setSorter(new NameSorter());
		setColumnProperties(COLUMN_PROPERTIES);

		setInput(getResourceRequirementList());

		// create context menu for the table
		final MenuManager menuMgr = new MenuManager("Actions");
		menuMgr.setRemoveAllWhenShown(true);

		// Menu menu = menuMgr.createContextMenu(getTable());
		// getTable().setMenu(menu);
		// IMenuListener listener = new IMenuListener() {
		// public void menuAboutToShow(IMenuManager m) {
		// IStructuredSelection selection = (IStructuredSelection)
		// getSelection();
		// for (Iterator<IAction> iter = actions.iterator(); iter.hasNext();) {
		// IAction action = iter.next();
		// if(action.isEnabled()) menuMgr.add(action);
		// }
		// }
		// };
		// menuMgr.addMenuListener(listener);
		packTable();
	}

	private Table createTable() {
		Table table = getTable();
		CellEditor[] editors = new CellEditor[COLUMN_PROPERTIES.length];

		for (int i = 0; i < COLUMN_PROPERTIES.length; i++) {
			TableColumn col = new TableColumn(table, SWT.LEFT, i);
			col.setText(COLUMN_PROPERTIES[i]);
			if (COLUMN_PROPERTIES[i].equals(ENABLED)) {
				col.setToolTipText("Decide if this requirement should be regarded.");
			} else if (COLUMN_PROPERTIES[i].equals(NAME)) {
				col.setToolTipText("Identity of this resource requirement");
				// Column 2 : Description (Free text)
				TextCellEditor textEditor = new TextCellEditor(table);
				editors[i] = textEditor;
			} else if (COLUMN_PROPERTIES[i].equals(VALUE)) {
				col.setToolTipText("Enter here the value you want to set.");
			} else if (COLUMN_PROPERTIES[i].equals(UNITS)) {
				col.setToolTipText("The unit in which the resource requirement is measured.");
			}

			// final int index = i;
			// TableColumnSorter cSorter = new TableColumnSorter(this, col) {
			// protected int doCompare(Viewer v, Object e1, Object e2) {
			// ITableLabelProvider lp = ((ITableLabelProvider)
			// getLabelProvider());
			// String t1 = lp.getColumnText(e1, index);
			// String t2 = lp.getColumnText(e2, index);
			// return t1.compareTo(t2);
			// }
			// };
			// cSorter.setSorter(cSorter, TableColumnSorter.ASC);
		}
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		return table;
	}

	public IGridBeanModel getGridBeanModel() {
		return gridBeanModel;
	}

	public RequirementList getResourceRequirementList() {
		return requirementList;
	}

	private void packTable() {
		for (TableColumn col : getTable().getColumns()) {
			col.pack();
		}
	}

	public void setCellEditor(int column, CellEditor editor) {
		cellEditors[column] = editor;
	}

	public void setGridBeanModel(IGridBeanModel gridBeanModel) {
		this.gridBeanModel = gridBeanModel;
		if (getContentProvider() == null) {
			return;
		}
		((RequirementContentProvider) getContentProvider())
				.setGridBeanModel(gridBeanModel);
	}

	public void setResourceRequirementList(final RequirementList requirementList) {
		if (requirementList == null) {
			return;
		}

		// this.getContentProvider().inputChanged(this, this.requirementList,
		// requirementList);
		this.requirementList = requirementList;
		// check if content provider is ok (its not when the job editor is
		// closed before this gets executed)
		if (getContentProvider() != null) {
			setInput(requirementList);
			packTable();
		}

	}

	public static int getColumnFor(String columnName) {
		for (int i = 0; i < COLUMN_PROPERTIES.length; i++) {
			if (COLUMN_PROPERTIES[i].equals(columnName)) {
				return i;
			}
		}
		return -1;
	}
}