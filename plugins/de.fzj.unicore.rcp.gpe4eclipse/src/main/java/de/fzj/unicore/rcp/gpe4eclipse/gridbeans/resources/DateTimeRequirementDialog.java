package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.DateTimeRequirementValue;

/**
 * provides a dialog to select the time for scheduled job submission
 * 
 * @author rajveer
 */
public class DateTimeRequirementDialog extends MessageDialog implements
		SelectionListener {

	DateTime dateControl, timeControl;
	Date initialSelection;
	Date minTime = Calendar.getInstance().getTime(), selectedTime = null;
	String errorMessage;

	/**
	 * Error message label widget.
	 */
	private Text errorMessageText;
	private Button btnNow;
	private GridData gd_1;

	public DateTimeRequirementDialog(Shell parentShell, String message) {
		super(parentShell, "Please specify the scheduled start time", null,
				message, MessageDialog.NONE, new String[] {
						IDialogConstants.OK_LABEL,
						IDialogConstants.CANCEL_LABEL }, 0);
		setShellStyle(SWT.DIALOG_TRIM | SWT.RESIZE | SWT.APPLICATION_MODAL);
	}

	@Override
	protected void cancelPressed() {
		selectedTime = null;
		super.cancelPressed();
	}

	@Override
	protected void configureShell(Shell newShell) {
		newShell.setMinimumSize(new Point(250, 200));
		newShell.setText("Calendar");
		super.configureShell(newShell);
	}

	@Override
	public Control createCustomArea(Composite parent) {
		Composite group = new Group(parent, SWT.BORDER);

		group.setLayoutData(new GridData(GridData.FILL_BOTH));
		GridLayout layout = new GridLayout(2, false);

		group.setLayout(layout);
		
				Label l = new Label(group, SWT.NONE);
				l.setText("Set start time: ");
		new Label(group, SWT.NONE);

		timeControl = new DateTime(group, SWT.BORDER | SWT.TIME | SWT.LONG);
		timeControl.addSelectionListener(this);

		gd_1 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_1.minimumHeight = -1;
		gd_1.minimumWidth = -1;
		timeControl.setLayoutData(gd_1);

		btnNow = new Button(group, SWT.NONE);
		btnNow.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Calendar now = Calendar.getInstance();
				timeControl.setTime(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE),
						now.get(Calendar.SECOND));
				dateControl.setDate(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
				validateSelection();
			}
		});
		btnNow.setText("Now");

		dateControl = new DateTime(group, SWT.BORDER | SWT.CALENDAR | SWT.LONG);
		GridData gd_dateControl = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_dateControl.minimumHeight = -1;
		gd_dateControl.minimumWidth = -1;
		dateControl.setLayoutData(gd_dateControl);

		dateControl.addSelectionListener(this);
		new Label(group, SWT.NONE);

		if (initialSelection != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(initialSelection);
			dateControl.setDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH),		c.get(Calendar.DATE));

			timeControl.setHours(c.get(Calendar.HOUR_OF_DAY));
			timeControl.setMinutes(c.get(Calendar.MINUTE));
			timeControl.setSeconds(c.get(Calendar.SECOND));
		}

		errorMessageText = new Text(group, SWT.READ_ONLY | SWT.WRAP);
		errorMessageText.setText("");
		GridData gd_errorMessageText = new GridData(GridData.BEGINNING,
				GridData.CENTER, true, false, 2, 1);
		gd_errorMessageText.minimumHeight = -1;
		gd_errorMessageText.minimumWidth = -1;
		errorMessageText.setLayoutData(gd_errorMessageText);
		errorMessageText.setForeground(errorMessageText.getDisplay()
				.getSystemColor(SWT.COLOR_RED));
		errorMessageText.setBackground(errorMessageText.getDisplay()
				.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));

		getShell().layout();
		validateSelection();
		return group;
	}

	// handle shell close events explicitly
	protected void handleShellCloseEvent() {		
		setReturnCode(CANCEL);
		super.handleShellCloseEvent();
	}

	/**
	 * Returns the selected time or null if the user pressed Cancel
	 * 
	 * @return
	 */
	public DateTimeRequirementValue getSelectedTime() {
		String selectedTimeString;
		if (selectedTime != null) {
			selectedTimeString = new SimpleDateFormat(RequirementConstants.DEFAULT_DATE_FORMAT)
					.format(selectedTime).toString();
		} else {
			selectedTimeString = "";
		}
		DateTimeRequirementValue selectedValue = new DateTimeRequirementValue(
				selectedTimeString);
		return selectedValue;
	}

	public void setErrorMessage(String errorMessage) {

		this.errorMessage = errorMessage;
		if (errorMessageText != null && !errorMessageText.isDisposed()) {
			Point shellSize = getShell().getSize();
			int oldSpace = shellSize.y;
			errorMessageText
					.setText(errorMessage == null ? " \n " : errorMessage); //$NON-NLS-1$
			// Disable the error message text control if there is no error, or
			// no error text (empty or whitespace only). Hide it also to avoid
			// color change.
			// See https://bugs.eclipse.org/bugs/show_bug.cgi?id=130281
			boolean hasError = errorMessage != null
					&& (StringConverter.removeWhiteSpaces(errorMessage))
							.length() > 0;
			errorMessageText.setEnabled(hasError);
			errorMessageText.setVisible(hasError);

			errorMessageText.getParent().update();
			errorMessageText.getParent().layout();
			int newSpace = getShell().computeSize(-1, -1).y;
			if (hasError && newSpace > oldSpace) {

				getShell().setSize(shellSize.x,
						shellSize.y + newSpace - oldSpace);
			}
			// Access the ok button by id, in case clients have overridden
			// button creation.
			// See https://bugs.eclipse.org/bugs/show_bug.cgi?id=113643
			Control button = getButton(IDialogConstants.OK_ID);
			if (button != null) {
				button.setEnabled(errorMessage == null);
			}
		}
	}

	public void setInitialSelection(Date initialSelection) {
		this.initialSelection = initialSelection;
	}

	protected void validateSelection() {
		selectedTime = null;
		Calendar c = Calendar.getInstance();
		c.set(dateControl.getYear(), dateControl.getMonth(),
				dateControl.getDay(), timeControl.getHours(),
				timeControl.getMinutes(), timeControl.getSeconds());
		Date newTime = c.getTime();
		if (minTime != null && newTime.before(minTime)) {
			setErrorMessage("Invalid time. Please select time after "
					+ new SimpleDateFormat(RequirementConstants.HUMAN_READABLE_DATE_FORMAT)
							.format(minTime) + ".");
		} else {
			setErrorMessage(null);
			selectedTime = newTime;
		}
	}

	public void widgetSelected(SelectionEvent e) {
		validateSelection();

	}

	public void widgetDefaultSelected(SelectionEvent e) {
		validateSelection();
	}

}