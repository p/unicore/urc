package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;

public interface IRequirementRestrictor {

	public void restrictRequirementValues(RequirementList list, ResourcesParameterValue param);
}
