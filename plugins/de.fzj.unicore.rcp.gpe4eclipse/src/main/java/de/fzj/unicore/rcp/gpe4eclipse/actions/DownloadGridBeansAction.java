package de.fzj.unicore.rcp.gpe4eclipse.actions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.intel.gpe.clients.api.DownloadGridBeanClient;
import com.intel.gpe.clients.api.GridBean;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareRemoteException;
import com.intel.gpe.util.sets.Pair;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.commands.DownloadGridBeansCommand;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.GridBeanRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.GridBeanServiceRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.IGridBeanServiceRegistryListener;
import de.fzj.unicore.rcp.gpe4eclipse.ui.SelectGridBeansFromServerDialog;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

public class DownloadGridBeansAction extends Action implements
IWorkbenchWindowActionDelegate, IGridBeanServiceRegistryListener {

	IAction wrapper;

	private List<DownloadGridBeanClient> clients;

	public DownloadGridBeansAction() {
		setText("Download Applications");
		setToolTipText("Download applications from an application server.");
		setImageDescriptor(GPEActivator.getImageDescriptor("script_add.png"));
		GridBeanServiceRegistry registry = GPEActivator.getDefault()
		.getGridBeanServiceRegistry();
		clients = registry.getAllDownloadGridBeanClients();
		checkEnabled();
		registry.addListener(this);
	}

	private void checkEnabled() {
		boolean enabled = clients != null && clients.size() > 0;
		setEnabled(enabled);
		if (wrapper != null) {
			wrapper.setEnabled(enabled);
		}
	}

	public void dispose() {
		GridBeanServiceRegistry registry = GPEActivator.getDefault()
		.getGridBeanServiceRegistry();
		registry.removeListener(this);
	}

	public void gridBeanServicesChanged(List<DownloadGridBeanClient> newValue) {
		clients = newValue;
		checkEnabled();

	}

	public void init(IWorkbenchWindow window) {
		checkEnabled();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.actions.ServiceBrowserAction#run()
	 */
	@Override
	public void run() {

		try {
			Job j = new BackgroundJob("retrieving available Applications") {
				
				
				@Override
				protected IStatus run(IProgressMonitor monitor) {

					List<Pair<GridBean, DownloadGridBeanClient>> availableGridBeans = new ArrayList<Pair<GridBean, DownloadGridBeanClient>>();
					Set<GridBeanInfo> added = new HashSet<GridBeanInfo>();
					GridBeanRegistry registry = GPEActivator.getDefault()
					.getGridBeanRegistry();
					registry.reloadGridBeans();
					for (DownloadGridBeanClient client : clients) {
						try {
							GridBean[] gridBeans = client.listGridBeans();
							for (GridBean gridBean : gridBeans) {
								GridBeanInfo info = new GridBeanInfo(
										gridBean.getName(),
										String.valueOf(gridBean.getVersion()));
								if (!added.contains(info)
										&& !registry
										.containsNewerGridBean(info)) {
									availableGridBeans
									.add(new Pair<GridBean, DownloadGridBeanClient>(
											gridBean, client));
									// don't add the same GridBean twice!
									added.add(info);
								}
							}
						} catch (GPEMiddlewareRemoteException e) {
							GPEActivator
							.log(IStatus.ERROR,
									"Unable to get full list of available application GUIs from the Grid, one of the servers threw a fault: "+e.getMessage(),
									e);
						}



					}
					selectGridBeansAndStartDownload(availableGridBeans);

					return Status.OK_STATUS;
				}
			};
			j.schedule();

		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to retrieve list of available applications", e);
			return;
		}

	}

	public void run(IAction action) {
		run();
	}

	private void selectGridBeansAndStartDownload(
			final List<Pair<GridBean, DownloadGridBeanClient>> availableGridBeans) {
		final Display d = PlatformUI.getWorkbench().getDisplay();
		d.asyncExec(new Runnable() {
			public void run() {
				Shell shell = d.getActiveShell();
				SelectGridBeansFromServerDialog dialog = new SelectGridBeansFromServerDialog(
						shell, availableGridBeans);
				dialog.open();
				List<Pair<GridBean, DownloadGridBeanClient>> selectedGridBeans = dialog
				.getSelectedGridBeans();
				if (selectedGridBeans == null) {
					return;
				}
				startDownload(selectedGridBeans);
			}

		});
	}
	
	
	public void selectionChanged(IAction action, ISelection selection) {
		action.setEnabled(isEnabled());
		wrapper = action;
	}

	private void startDownload(
			final List<Pair<GridBean, DownloadGridBeanClient>> selectedGridBeans) {
		Job j = new BackgroundJob("retrieving available Application GUIs") {
			
			
			
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				if (selectedGridBeans != null) {
					DownloadGridBeansCommand cmd = new DownloadGridBeansCommand(
							selectedGridBeans);
					if (cmd.canExecute()) {
						cmd.execute();
					}
				}

				return Status.OK_STATUS;
			}
		};
		j.schedule();
	}

}