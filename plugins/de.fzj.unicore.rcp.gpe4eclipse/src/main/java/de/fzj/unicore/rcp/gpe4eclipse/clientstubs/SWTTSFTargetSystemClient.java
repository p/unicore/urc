/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.clientstubs;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.unigrids.x2006.x04.services.tsf.TargetSystemFactoryPropertiesDocument.TargetSystemFactoryProperties;
import org.unigrids.x2006.x04.services.tss.ApplicationResourceType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;
import org.w3c.dom.Element;

import com.intel.gpe.clients.api.Application;
import com.intel.gpe.clients.api.Job;
import com.intel.gpe.clients.api.JobClient;
import com.intel.gpe.clients.api.JobType;
import com.intel.gpe.clients.api.StorageClient;
import com.intel.gpe.clients.api.TargetSystemClient;
import com.intel.gpe.clients.api.exceptions.GPEInvalidQueryExpressionException;
import com.intel.gpe.clients.api.exceptions.GPEInvalidResourcePropertyQNameException;
import com.intel.gpe.clients.api.exceptions.GPEJobNotSubmittedException;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareRemoteException;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareServiceException;
import com.intel.gpe.clients.api.exceptions.GPEQueryEvaluationErrorException;
import com.intel.gpe.clients.api.exceptions.GPEResourceNotDestroyedException;
import com.intel.gpe.clients.api.exceptions.GPEResourceUnknownException;
import com.intel.gpe.clients.api.exceptions.GPESecurityException;
import com.intel.gpe.clients.api.exceptions.GPETerminationTimeChangeRejectedException;
import com.intel.gpe.clients.api.exceptions.GPEUnableToSetTerminationTimeException;
import com.intel.gpe.clients.api.exceptions.GPEUnknownQueryExpressionDialectException;
import com.intel.gpe.clients.api.exceptions.GPEUnmarshallingException;
import com.intel.gpe.clients.api.exceptions.GPEWrongJobTypeException;
import com.intel.gpe.clients.common.ApplicationImpl;
import com.intel.gpe.clients.impl.tss.ApplicationUtils;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;

/**
 * @author demuth
 * 
 */
public class SWTTSFTargetSystemClient implements TargetSystemClient, IAdaptable {

	NodePath nodePath;
	Integer lock = 1;
	TargetSystemClient wrapped;

	/**
	 * @param client
	 */
	public SWTTSFTargetSystemClient(NodePath nodePath) {
		this.nodePath = nodePath;
	}

	private void connect() throws GPEMiddlewareRemoteException {
		synchronized (lock) {
			TargetSystemFactoryNode node = (TargetSystemFactoryNode) NodeFactory
					.getNodeFor(nodePath);

			if (node.getTargetSystemFactoryProperties() == null) {
				node.refresh(0, false, null);
			}

			wrapped = searchForUsableTargetSystem(node);
			if (wrapped == null) {
				node.refresh(1, true, null);
				wrapped = searchForUsableTargetSystem(node);
				if (wrapped == null) {
					EndpointReferenceType tssEpr;
					try {
						tssEpr = node.createTargetSystem();
						URI uri = new URI(tssEpr.getAddress().getStringValue());
						NodePath path = node.getPathToRoot().append(uri);
						Node tssNode = NodeFactory.getNodeFor(path);
						wrapped = retrieveClientFrom(tssNode);

					} catch (Exception e) {
						throw new GPEMiddlewareRemoteException(
								"Unable to connect to site: could not create a target system",
								e, this);
					}
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSLTClient#destroy()
	 */
	public void destroy() throws GPEResourceUnknownException,
			GPEResourceNotDestroyedException, GPESecurityException,
			GPEMiddlewareRemoteException, GPEMiddlewareServiceException {
		if (wrapped != null) {
			wrapped.destroy();
		}

	}

	@Override
	public boolean equals(Object o) {
		if (o == null || !getClass().equals(o.getClass())) {
			return false;
		}
		SWTTSFTargetSystemClient other = (SWTTSFTargetSystemClient) o;
		return CollectionUtil.equalOrBothNull(nodePath, other.nodePath);
	}

	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class key) {
		if (key.equals(TargetSystemClient.class)) {
			return this;
		} else {
			Node node = NodeFactory.getNodeFor(nodePath);
			if (node == null) {
				return null;
			}
			Object adapter = node.getAdapter(key);
			return adapter;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.TargetSystemClient#getApplications()
	 */
	public List<Application> getApplications()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		List<Application> loadApplications = new ArrayList<Application>();
		TargetSystemFactoryNode node = (TargetSystemFactoryNode) NodeFactory
				.createNode(getEPR());
		try {
			if (node.getTargetSystemFactoryProperties() == null) {
				node.refresh(0, false, null);
			}
			TargetSystemFactoryProperties targetSystemFactoryProperties = node
					.getTargetSystemFactoryProperties();
			if (targetSystemFactoryProperties != null) {
				ApplicationResourceType[] applicationResourceArray = targetSystemFactoryProperties
						.getApplicationResourceArray();
				loadApplications = ApplicationUtils
						.loadApplications(applicationResourceArray);
			}

		} catch (Exception e) {
			GPEActivator
					.log(IStatus.WARNING,
							"Problem while querying target system factory for applications",
							e);
		} finally {
			node.dispose();
		}

		return loadApplications;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#getApplications(java.lang
	 * .String)
	 */
	public List<Application> getApplications(String applicationName)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {

		List<Application> loadApplications = new ArrayList<Application>();
		TargetSystemFactoryNode node = (TargetSystemFactoryNode) NodeFactory
				.createNode(getEPR());
		try {
			if (node.getTargetSystemFactoryProperties() == null) {
				node.refresh(0, false, null);
			}
			TargetSystemFactoryProperties targetSystemFactoryProperties = node
					.getTargetSystemFactoryProperties();
			if (targetSystemFactoryProperties != null) {
				ApplicationResourceType[] applicationResourceArray = targetSystemFactoryProperties
						.getApplicationResourceArray();
				loadApplications = ApplicationUtils.loadApplication(
						applicationResourceArray, applicationName);
			}

		} catch (Exception e) {
			GPEActivator
					.log(IStatus.WARNING,
							"Problem while querying target system factory for an application",
							e);
		} finally {
			node.dispose();
		}

		return loadApplications;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSLTClient#getCurrentTime()
	 */
	public Calendar getCurrentTime() throws GPEResourceUnknownException,
			GPEInvalidResourcePropertyQNameException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (wrapped == null) {
			connect();
		}
		return wrapped.getCurrentTime();
	}

	public EndpointReferenceType getEPR() {
		return NodeFactory.getEPRForURI(nodePath.last());
	}

	public String getFullAddress() {
		return nodePath.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.TargetSystemClient#getJobs()
	 */
	public <JobClientType extends JobClient> List<JobClientType> getJobs()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (wrapped == null) {
			connect();
		}
		return wrapped.getJobs();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#getJobType(java.lang.String)
	 */
	public JobType getJobType(String jobDefinition) {
		if (wrapped == null) {
			try {
				connect();
			} catch (GPEMiddlewareRemoteException e) {
				throw new RuntimeException("Unable to connect to target system", e);
			}
		}

		return wrapped.getJobType(jobDefinition);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#getMultipleResourceProperties(javax
	 * .xml.namespace.QName[])
	 */
	public Element[] getMultipleResourceProperties(QName[] resourceProperties)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (wrapped == null) {
			connect();
		}
		return wrapped.getMultipleResourceProperties(resourceProperties);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.TargetSystemClient#getName()
	 */
	public String getName() throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		TargetSystemFactoryNode node = (TargetSystemFactoryNode) NodeFactory
				.createNode(getEPR());
		if (node.getTargetSystemFactoryProperties() == null) {
			node.refresh(0, false, null);
		}
		String name = node.getName();
		if (name == null) {
			node.retrieveName();
			name = node.getName();
		}
		node.dispose();
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#getResourceProperty(javax.xml.namespace
	 * .QName)
	 */
	public Element getResourceProperty(QName resourceProperty)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (wrapped == null) {
			connect();
		}
		return wrapped.getResourceProperty(resourceProperty);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSRPClient#getResourcePropertyDocument()
	 */
	public Element getResourcePropertyDocument() throws Exception {
		if (wrapped == null) {
			connect();
		}
		return wrapped.getResourcePropertyDocument();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#getStorage(java.lang.String)
	 */
	public <StorageClientType extends StorageClient> StorageClientType getStorage(
			String type) throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (wrapped == null) {
			connect();
		}
		return wrapped.getStorage(type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.TargetSystemClient#getStorages()
	 */
	public <StorageClientType extends StorageClient> List<StorageClientType> getStorages()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (wrapped == null) {
			connect();
		}
		return wrapped.getStorages();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSLTClient#getTerminationTime()
	 */
	public Calendar getTerminationTime() throws GPEResourceUnknownException,
			GPEInvalidResourcePropertyQNameException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (wrapped == null) {
			connect();
		}
		return wrapped.getTerminationTime();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#getTextInfo(java.lang.String
	 * )
	 */
	public List<String> getTextInfo(String name)
			throws GPEInvalidQueryExpressionException,
			GPEQueryEvaluationErrorException,
			GPEUnknownQueryExpressionDialectException,
			GPEResourceUnknownException,
			GPEInvalidResourcePropertyQNameException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareServiceException,
			GPEMiddlewareRemoteException {
		if (wrapped == null) {
			connect();
		}
		return wrapped.getTextInfo(name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSRFClient#getURL()
	 */
	public String getURL() {
		return nodePath.last().toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#newJob(com.intel.gpe.clients
	 * .api.JobType)
	 */
	public <JobDefinitionType extends Job> JobDefinitionType newJob(JobType type)
			throws GPEWrongJobTypeException {
		if (wrapped == null) {
			try {
				connect();
			} catch (GPEMiddlewareRemoteException e) {
				throw new GPEWrongJobTypeException(
						"Unable to create job description: " + e.getMessage(),
						this);
			}
		}
		return wrapped.newJob(type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#queryXPath10Properties(java.lang
	 * .String)
	 */
	public Element[] queryXPath10Properties(String expression)
			throws GPEResourceUnknownException,
			GPEInvalidResourcePropertyQNameException,
			GPEInvalidQueryExpressionException,
			GPEQueryEvaluationErrorException,
			GPEUnknownQueryExpressionDialectException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (wrapped == null) {
			connect();
		}
		return wrapped.queryXPath10Properties(expression);
	}

	private TargetSystemClient retrieveClientFrom(Node node) {
		Object adapter = node.getAdapter(TargetSystemClient.class);
		if (adapter != null) {
			return (TargetSystemClient) adapter;
		} else {
			return null;
		}
	}

	private TargetSystemClient searchForUsableTargetSystem(
			TargetSystemFactoryNode node) {
		Node[] children = node.getChildrenArray();
		for (Node child : children) {
			TargetSystemClient tss = retrieveClientFrom(child);
			if (tss != null) {
				// test whether target system is really responding
				try {
					IStatus status = child.refresh(1, false, null);
					if (status.getCode() == Node.STATE_READY) {
						return tss;
					}
				} catch (Exception e) {
					continue;
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSLTClient#setTerminationTime(java.util.Calendar
	 * )
	 */
	public void setTerminationTime(Calendar newTT)
			throws GPEResourceUnknownException,
			GPEUnableToSetTerminationTimeException,
			GPETerminationTimeChangeRejectedException, GPESecurityException,
			GPEMiddlewareRemoteException, GPEMiddlewareServiceException {
		if (wrapped == null) {
			connect();
		}
		wrapped.setTerminationTime(newTT);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#submit(com.intel.gpe.clients
	 * .api.Job, java.util.Calendar)
	 */
	public <JobClientType extends JobClient> JobClientType submit(Job job,
			Calendar initialTerminationTime) throws GPEWrongJobTypeException,
			GPEJobNotSubmittedException, GPEResourceUnknownException,
			GPEMiddlewareRemoteException, GPEMiddlewareServiceException,
			GPESecurityException {
		if (wrapped == null) {
			try {
				connect();
			} catch (GPEMiddlewareRemoteException e) {
				throw new GPEWrongJobTypeException(
						"Unable to create target system: " + e.getMessage(),
						this);
			}
		}
		try {
			return wrapped.submit(job, initialTerminationTime);
		} catch (GPEWrongJobTypeException e) {
			wrapped = null;
			throw e;
		} catch (GPEJobNotSubmittedException e) {
			wrapped = null;
			throw e;
		} catch (GPEResourceUnknownException e) {
			// target system has been destroyed in the meantime.. try to connect
			// once more
			try {
				connect();
				return wrapped.submit(job, initialTerminationTime);
			} catch (GPEMiddlewareRemoteException e2) {
				wrapped = null;
				throw e2;
			} catch (GPEMiddlewareServiceException e2) {
				wrapped = null;
				throw e2;
			} catch (GPESecurityException e2) {
				wrapped = null;
				throw e2;
			} catch (GPEWrongJobTypeException e2) {
				wrapped = null;
				throw e2;
			} catch (GPEJobNotSubmittedException e2) {
				wrapped = null;
				throw e2;
			} catch (GPEResourceUnknownException e2) {
				wrapped = null;
				throw e2;
			}

		} catch (GPEMiddlewareRemoteException e) {
			wrapped = null;
			throw e;
		} catch (GPEMiddlewareServiceException e) {
			wrapped = null;
			throw e;
		} catch (GPESecurityException e) {
			wrapped = null;
			throw e;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#supportsApplication(java
	 * .lang.String, java.lang.String)
	 */
	public boolean supportsApplication(String applicationName,
			String applicationVersion)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (Application.ANY_APPLICATION_NAME.equals(applicationName)) {
			return true;
		}
		List<Application> apps = getApplications(applicationName);
		return GridBeanUtils.matchApplication(new ApplicationImpl(
				applicationName, applicationVersion), apps);
	}

	@Override
	public String toString() {
		try {
			return getName();
		} catch (Exception e) {
			return getURL();
		}
	}

}
