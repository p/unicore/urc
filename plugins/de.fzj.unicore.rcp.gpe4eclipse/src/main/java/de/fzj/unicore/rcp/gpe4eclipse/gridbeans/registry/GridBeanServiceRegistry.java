/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.xmlbeans.XmlException;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.clients.api.DownloadGridBeanClient;
import com.intel.gpe.clients.api.security.GPESecurityManager;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;

public class GridBeanServiceRegistry {


	Map<URL, String> map = new ConcurrentHashMap<URL, String>();
	List<IGridBeanServiceRegistryListener> listeners = new ArrayList<IGridBeanServiceRegistryListener>();
	
	
	/** and fixed sourceforge bug #3568390
	 * Creates a registry for gridbeans.
	 * 
	 * @param userDefaults
	 */
	public GridBeanServiceRegistry() {

	}

	public void addListener(IGridBeanServiceRegistryListener l) {
		listeners.add(l);
	}

	private void fireClientsChanged(List<DownloadGridBeanClient> newValue) {
		for (IGridBeanServiceRegistryListener l : listeners) {
			l.gridBeanServicesChanged(newValue);
		}
	}

	/**
	 * Returns a list of clients for GridBeanDownload services for READ-ONLY
	 * access. Modification of the list won't change the registry.
	 * 
	 * @return
	 */
	private List<DownloadGridBeanClient> createClients() {
		List<DownloadGridBeanClient> result = new ArrayList<DownloadGridBeanClient>();
		for(URL key : map.keySet())
		{
			String address = map.get(key);
			if(address == null) continue;
			GPESecurityManager secManager = GPEActivator.getDefault().getClient()
			.getSecurityManager();
	
			DownloadGridBeanClient client = secManager.getGBSClient(key);
			if(client != null)
			{
				result.add(client);
			}
		}
		return result;
	}
	
	public List<DownloadGridBeanClient> getAllDownloadGridBeanClients() {
		return createClients();
	}

	/**
	 * Returns a list of addresses of GridBeanDownload services for READ-ONLY
	 * access. Modification of the list won't change the registry.
	 * 
	 * @return
	 */
	public List<EndpointReferenceType> getServiceAddresses() {
		
		List<EndpointReferenceType> result = new ArrayList<EndpointReferenceType>();
		for(String s : map.values())
		{
			EndpointReferenceType epr;
			try {
				epr = EndpointReferenceType.Factory.parse(s);
				result.add(epr);
			} catch (XmlException e) {
			
			}
			
		}
		return result;
	}

	public void registerService(EndpointReferenceType epr) {
		
			try {
				
				URL url = new URL(epr.getAddress().getStringValue());
				String s = epr.toString();
				String previous = map.put(url, s);
				
				if (!CollectionUtil.equalOrBothNull(s, previous)) {
					List<DownloadGridBeanClient> newClients = createClients();
					fireClientsChanged(newClients);
				}
			} catch (Exception e) {
				// do nothing
			}
			
		
	}

	public void removeListener(IGridBeanServiceRegistryListener l) {
		listeners.remove(l);
	}

	public void unregisterService(EndpointReferenceType epr) {
		try {
			URL url = new URL(epr.getAddress().getStringValue());
			if(!map.containsKey(url))
			{
				return;
			}
			String previous = map.remove(url);
			if (previous != null) {
				List<DownloadGridBeanClient> newClients = createClients();
				fireClientsChanged(newClients);
			}
		} catch (Exception e) {
			// do nothing
		}
	}

	

}
