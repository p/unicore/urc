package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.eclipse.core.runtime.IStatus;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ApplicationType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDescriptionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdlPosix.POSIXApplicationDocument;
import org.ggf.schemas.jsdl.x2005.x11.jsdlPosix.POSIXApplicationType;

import com.intel.gpe.clients.api.jsdl.JSDLElement;
import com.intel.gpe.gridbeans.jsdl.JavaToXBeanElementConverter;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;

public class XLoginToXBeanConverter implements JavaToXBeanElementConverter {

	public void convertToXBean(JobDefinitionType jobDef, JSDLElement element) {
		XLoginType login = (XLoginType) element;
		ApplicationType app = getOrCreateApplication(jobDef);
		POSIXApplicationDocument posix = null;
		XmlCursor cursor = app.newCursor();
		try {
			boolean posixExists = cursor.toChild(POSIXApplicationDocument.type
					.getDocumentElementName());
			if (posixExists) {
				XmlObject o = cursor.getObject();
				posix = POSIXApplicationDocument.Factory.newInstance();
				posix.setPOSIXApplication(POSIXApplicationType.Factory.parse(o
						.newReader()));
				cursor.removeXml();

			} else {
				posix = POSIXApplicationDocument.Factory.newInstance();
				posix.addNewPOSIXApplication();
			}
			cursor.dispose();
			cursor = app.newCursor();
			cursor.toEndToken();
			posix.getPOSIXApplication().addNewUserName()
					.setStringValue(login.getLogin());

			XmlCursor from = posix.newCursor();
			from.toFirstChild();
			from.copyXml(cursor);
			from.dispose();

		} catch (Exception e) {

			GPEActivator.log(IStatus.ERROR, "Unable to add XLogin to job.", e);
		} finally {
			cursor.dispose();
		}

	}

	protected ApplicationType getOrCreateApplication(JobDefinitionType jobDef) {
		JobDescriptionType jobDescr = jobDef.getJobDescription();
		if (jobDescr == null) {
			jobDescr = jobDef.addNewJobDescription();
		}
		ApplicationType app = jobDescr.getApplication();
		if (app == null) {
			app = jobDescr.addNewApplication();
		}
		return app;
	}

	public Class<XLoginType> getSourceType() {
		return XLoginType.class;
	}

}
