/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.utils;

import java.awt.BorderLayout;
import java.awt.Component;
import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.intel.gpe.clients.all.gridbeans.InternalGridBean;
import com.intel.gpe.clients.all.i18n.Messages;
import com.intel.gpe.clients.all.i18n.MessagesKeys;
import com.intel.gpe.clients.api.Application;
import com.intel.gpe.clients.api.Job;
import com.intel.gpe.clients.api.JobClient;
import com.intel.gpe.clients.api.TargetSystemClient;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.clients.common.clientwrapper.ClientWrapper;
import com.intel.gpe.gridbeans.ApplicationMatcher;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.gridbeans.jsdl.GPEJobImpl;
import com.intel.gpe.gridbeans.jsdl.JobWithParameters;
import com.intel.gpe.gridbeans.parameters.GridBeanParameter;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;
import com.intel.gpe.gridbeans.plugins.IPanel;
import com.intel.gpe.gridbeans.plugins.swt.panels.ISWTGridBeanPanel;
import com.intel.gpe.util.collections.CollectionUtil;
import com.intel.gpe.util.observer.IObserver;

import de.fzj.unicore.rcp.common.utils.swing.AwtEnvironment;
import de.fzj.unicore.rcp.common.utils.swing.EmbeddedSwingComposite;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.GridBeanRegistry;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeData;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;

/**
 * @author demuth
 * 
 */
public class GridBeanUtils {

	private static Boolean loadingGridBean = false;
	private static final Integer loadingGridBeanLock = 1;
	private static Boolean reconstructingInput = false;
	private static final Integer reconstructingInputLock = 1;
	private static Boolean saving = false;
	private static final Integer savingLock = 1;

	public static Control addGridBeanPanel(final Composite parent,
			final IPanel<?> panel) {

		AwtEnvironment.getInstance(PlatformUI.getWorkbench().getDisplay());
		Composite control = null;
		if (panel.getComponent() instanceof Component) {
			final EmbeddedSwingComposite embeddedComposite = new EmbeddedSwingComposite(
					parent, SWT.NONE) {
				@Override
				protected JComponent createSwingComponent() {
					JPanel p = new JPanel(true);
					p.setLayout(new BorderLayout());
					Component c = (Component) panel.getComponent();
					p.add(c, BorderLayout.CENTER);
					return p;
				}
			};

			embeddedComposite.populate();
			control = embeddedComposite;
		} else if (panel instanceof ISWTGridBeanPanel) {
			final ISWTGridBeanPanel swtPanel = (ISWTGridBeanPanel) panel;
			control = new Composite(parent, SWT.NONE);
			// control.setLayoutData(new GridData(SWT.LEFT,SWT.TOP,true,true));
			swtPanel.createControl(control);
			control.setLayout(new FillLayout(SWT.HORIZONTAL | SWT.VERTICAL));
			control.layout();
			parent.layout();

		} else {
			control = null;
		}
		return control;
	}

	/**
	 * Creates a {@link GridBeanContext} object for a standalone job that can be
	 * edited and submitted by itself.
	 * 
	 * @param diagram
	 * @return
	 */
	public static GridBeanContext createSolitaryContext() {
		GridBeanContext gridBeanContext = new GridBeanContext();
		gridBeanContext.setNewlyCreated(true);
		return gridBeanContext;
	}

	public static WorkflowFile createWFFileFrom(IActivity activity,
			String fileId, IGridFileAddress address) {
		WorkflowFile wfFile = new WorkflowFile(activity.getDiagram(), fileId, activity);
		syncWFFileWithAddress(wfFile, address);
		return wfFile;
	}

	/**
	 * Creates a {@link GridBeanContext} object reflecting some properties of
	 * the job's surrounding workflow diagram.
	 * 
	 * @param diagram
	 * @return
	 */
	public static GridBeanContext createWorkflowContext(WorkflowDiagram diagram) {
		GridBeanContext gridBeanContext = new GridBeanContext();
		gridBeanContext.setNewlyCreated(true);
		gridBeanContext.setPartOfWorkflow(true);
		gridBeanContext.setDefaultWorkflowSystem(diagram.getContext()
				.getDefaultWorkflowSystem());
		gridBeanContext.setApplicationDomains(diagram.getContext()
				.getApplicationDomains());
		return gridBeanContext;
	}

	public static ResourcesParameterValue extractResourcesParameterValue(
			IGridBean gridBean) {
		return (ResourcesParameterValue) gridBean.get(IGridBeanModel.RESOURCES);
	}

	public static List<IAdaptable> extractSelectedTargetHosts(IGridBean gridBean) {
		ResourcesParameterValue resources = extractResourcesParameterValue(gridBean);
		return extractSelectedTargetHosts(resources);
	}

	public static List<IAdaptable> extractSelectedTargetHosts(
			JobWithParameters job) {
		ResourcesParameterValue resources = (ResourcesParameterValue) job
				.getParameterValue(new GridBeanParameter(
						IGridBeanModel.RESOURCES,
						GridBeanParameterType.RESOURCES));
		return extractSelectedTargetHosts(resources);
	}

	public static List<IAdaptable> extractSelectedTargetHosts(
			ResourcesParameterValue resources) {
		List<String> hosts = resources.getCandidateHosts();
		List<IAdaptable> targetHosts = new ArrayList<IAdaptable>();
		if (hosts != null) {
			for (String host : hosts) {
				Node n;
				try {
					NodePath path = NodePath.parseString(host);
					n = NodeFactory.getNodeFor(path);
					if (n == null) {
						if (path.getLength() == 1) {
							// required for backwards compatibility with
							// versions < 6.3.0
							// we don't know which node was meant, try to find
							// one valid
							// path to root and take this one
							Node temp = NodeFactory.createNode(path.last());
							path = new NodePath();
							try {
								INodeData data = temp.getData();
								List<List<INodeData>> allPaths = data
										.getAllPathsToRoot();
								if (allPaths.size() > 0) {
									List<INodeData> onePath = allPaths.get(0);
									for (INodeData curr : onePath) {
										path = path.append(curr.getURI());
									}
									n = NodeFactory.getNodeFor(path);
									if (n == null) {
										continue;
									}
								} else {
									continue;
								}
							} finally {
								temp.dispose();
							}
						} else {
							continue;
						}
					}
					Object adapter = n.getAdapter(TargetSystemClient.class);

					if (adapter != null) {
						targetHosts.add(n);
					} else {
						GPEActivator.log(IStatus.WARNING, "Target system "
								+ host + " from saved job is not available.");
					}

				} catch (URISyntaxException e) {
					GPEActivator.log(IStatus.WARNING, "Target system " + host
							+ " from saved job is not available.", e);
				}
			}
		}
		return targetHosts;
	}

	public static List<GridBeanInfo> getMatchingGridBeans(
			List<GridBeanInfo> gbInfos, Application app) {
		List<GridBeanInfo> result = new ArrayList<GridBeanInfo>();
		GridBeanRegistry registry = GPEActivator.getDefault()
				.getGridBeanRegistry();
		for (GridBeanInfo gridBeanInfo : gbInfos) {
			InternalGridBean gb = registry.getGridBean(gridBeanInfo);
			if (ApplicationMatcher.conformApplication(app,
					gb.getSupportedApplication())) {
				result.add(gridBeanInfo);
			}
		}
		return result;
	}

	public static QName getType(IFileParameterValue value) {
		try {
			if (value == null) {
				return null;
			}
			if (value.isInputParameter()) {
				return value.getSource().getProtocol().getName();
			} else {
				return value.getTarget().getProtocol().getName();
			}

		} catch (Exception e) {
			GPEActivator.log(IStatus.WARNING, "Could not get type", e);
		}
		return new QName("UNKNOWN");
	}

	public static void insertSelectedTargetHosts(IGridBean gridBean,
			List<IAdaptable> adaptables) {
		ResourcesParameterValue resources = (ResourcesParameterValue) gridBean
				.get(IGridBeanModel.RESOURCES);
		if (resources == null) {
			resources = new ResourcesParameterValue();
		} else {
			resources = resources.clone();
		}
		List<String> addresses = new ArrayList<String>();
		if (adaptables != null) {
			for (IAdaptable adaptable : adaptables) {
				if (adaptable instanceof TargetSystemClient) {
					addresses.add(((TargetSystemClient) adaptable)
							.getFullAddress());
				} else {
					try {

						TargetSystemClient adapter = (TargetSystemClient) adaptable
								.getAdapter(TargetSystemClient.class);
						if (adapter != null) {
							addresses.add(adapter.getFullAddress());
						}

					} catch (Exception e) {
						GPEActivator.log(IStatus.WARNING,
								"Could not save target resource " + adaptable
										+ " in job ", e);
					}
				}
			}
		}
		resources.setCandidateHosts(addresses);
		gridBean.set(IGridBeanModel.RESOURCES, resources);
	}

	public static void loadGridBeanAndWait(GridBeanClient<?> gridBeanClient,
			GridBeanInfo info, Display d) {
		synchronized (loadingGridBeanLock) {

			loadingGridBean = true;
			gridBeanClient.loadGridBean(info, new IObserver() {
				public void observableUpdate(Object theObserved,
						Object changeCode) {
					loadingGridBean = false;
				}
			});

			boolean isMainThread = Thread.currentThread().equals(d.getThread());
			long start = System.currentTimeMillis();
			// wait until the Grid Bean is loaded but give up after 30 seconds
			while (loadingGridBean
					&& System.currentTimeMillis() - start < 30000) {
				boolean sleep = isMainThread ? !d.readAndDispatch() : true;
				if (sleep) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
					}
				}
			}
		}
	}

	public static boolean matchApplication(Application toMatch,
			List<Application> available) {
		String applicationName = toMatch.getName();
		if ((applicationName == null || applicationName
				.equals(Application.ANY_APPLICATION_NAME))
				&& available.size() > 0) {
			return true;
		}
		String applicationVersion = toMatch.getApplicationVersion();
		boolean anyVersion = applicationVersion == null
				|| applicationVersion
						.equals(Application.ANY_APPLICATION_VERSION);
		for (Application app : available) {
			if (app.getName() != null
					&& app.getName().equalsIgnoreCase(applicationName)) {
				if (anyVersion) {
					return true;
				}
				if (app.getApplicationVersion() == null) {
					continue;
				}
				if (applicationVersion.equalsIgnoreCase(app
						.getApplicationVersion())) {
					return true;
				}
			}
		}
		return false;
	}

	public static GridBeanInfo reconstructGridBeanInfo(Job j) throws Exception {
		if (!(j instanceof GPEJob)) {
			return null;
		}
		GPEJob gpeJob = (GPEJob) j;
		String gbName = null;
		try {
			gbName = gpeJob.getGridBeanName();
		} catch (Exception e) {
			return null;
		}
		if (gbName == null) {
			return null;
		}
		GridBeanInfo gbInfo = null;
		GridBeanRegistry registry = GPEActivator.getDefault()
				.getGridBeanRegistry();
		List<GridBeanInfo> gbInfos = registry.getGridBeans();
		for (GridBeanInfo gridBeanInfo : gbInfos) {
			if (gridBeanInfo.getGridBeanName().equals(gbName)) {
				gbInfo = gridBeanInfo;
				break;
			}
		}
		return gbInfo;
	}

	public static void reconstructInputAndWait(GridBeanClient<?> gridBeanClient,
			JobClient jobClient, Display d) {
		synchronized (reconstructingInputLock) {

			reconstructingInput = true;
			gridBeanClient.reconstructInput(
					new ClientWrapper<JobClient, String>(jobClient, ""),
					new IObserver() {
						public void observableUpdate(Object theObserved,
								Object changeCode) {
							reconstructingInput = false;
						}
					});

			boolean isMainThread = Thread.currentThread().equals(d.getThread());
			long start = System.currentTimeMillis();
			// wait until the Grid Bean is loaded but give up after 30 seconds
			while (reconstructingInput
					&& System.currentTimeMillis() - start < 30000) {
				boolean sleep = isMainThread ? !d.readAndDispatch() : true;
				if (sleep) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
					}
				}
			}
		}
	}

	public static boolean sameJob(GPEJobImpl oldJob, GPEJobImpl currentJob) {
		if (!CollectionUtil.equalOrBothNull(currentJob.getApplicationName(),
				oldJob.getApplicationName())) {
			return false;
		}
		if (!CollectionUtil.equalOrBothNull(currentJob.getApplicationVersion(),
				oldJob.getApplicationVersion())) {
			return false;
		}
		if (!CollectionUtil.equalOrBothNull(currentJob.getName(),
				oldJob.getName())) {
			return false;
		}
		if (!CollectionUtil.equalOrBothNull(currentJob.getId(), oldJob.getId())) {
			return false;
		}
		List<IGridBeanParameter> currentParams = currentJob.getParameters();
		List<IGridBeanParameter> oldParams = oldJob.getParameters();
		if (!CollectionUtil.containSameElements(currentParams, oldParams)) {
			return false;
		}
		for (IGridBeanParameter param : currentParams) {
			IGridBeanParameterValue v1 = currentJob.getParameterValue(param);
			IGridBeanParameterValue v2 = oldJob.getParameterValue(param);
			if (!CollectionUtil.equalOrBothNull(v1, v2)) {
				CollectionUtil.equalOrBothNull(v1, v2);
				return false;
			}
		}

		return true;
	}

	public static void saveJobAndWait(GridBeanClient<?> gridBeanClient, File file,
			Display d) {
		synchronized (savingLock) {

			saving = true;
			gridBeanClient.saveJob(file.getAbsolutePath(), new IObserver() {
				public void observableUpdate(Object theObserved,
						Object changeCode) {
					if (changeCode instanceof Exception) {
						GPEActivator.log(
								IStatus.ERROR,
								Messages.getString(MessagesKeys.common_actions_SaveGridBeanJobAction_Cannot_save_job_in),
								(Exception) changeCode);
					}
					saving = false;
				}
			});

			// wait until the editor is saved but give up after 30 seconds
			boolean isMainThread = Thread.currentThread().equals(d.getThread());

			long start = System.currentTimeMillis();
			while (saving && System.currentTimeMillis() - start < 30000) {
				boolean sleep = isMainThread ? !d.readAndDispatch() : true;
				if (sleep) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
					}
				}
			}
		}
	}

	public static void syncWFFileWithAddress(WorkflowFile wfFile,
			IGridFileAddress address) {
		wfFile.setParentDirAddress(address.getParentDirAddress());
		wfFile.setRelativePath(address.getRelativePath());
		wfFile.setMimeTypes(address.getMimeTypes());
		wfFile.setDisplayedString(address.getDisplayedString());
		wfFile.setProtocol(address.getProtocol().getName());
		wfFile.setExcludes(address.getExcludes());
		wfFile.setUsingWildcards(address.isUsingWildcards());
	}

}
