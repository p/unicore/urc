/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.commands;

import java.util.List;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.EnvVariable;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.EnvVariableList;

/**
 * @author demuth
 */
public class RemoveVariableFromModelCommand extends Command {

	private EnvVariableList model;
	private List<EnvVariable> variables;

	public RemoveVariableFromModelCommand(EnvVariableList model,
			List<EnvVariable> variables) {
		this.model = model;
		this.variables = variables;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {

		redo();
	}

	@Override
	public void redo() {
		for (EnvVariable variable : variables) {
			model.removeVariable(variable);
		}
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		for (EnvVariable variable : variables) {
			model.addNewVariable(variable);
		}
	}
}
