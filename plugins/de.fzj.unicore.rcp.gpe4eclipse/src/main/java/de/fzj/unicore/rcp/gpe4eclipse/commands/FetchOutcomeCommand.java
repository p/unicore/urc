/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.commands;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.intel.gpe.clients.api.Application;
import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.GridFile;
import com.intel.gpe.clients.api.JobClient;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.exceptions.GPEException;
import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.clients.api.transfers.IGridFileSetTransfer;
import com.intel.gpe.clients.api.transfers.IGridFileTransfer;
import com.intel.gpe.clients.common.clientwrapper.ClientWrapper;
import com.intel.gpe.clients.common.transfers.GridFileAddress;
import com.intel.gpe.clients.common.transfers.GridFileSetTransfer;
import com.intel.gpe.clients.common.transfers.GridFileTransfer;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;
import com.intel.gpe.util.observer.IObserver;

import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.gpe4eclipse.FakeProgressListener;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.ISWTClient;
import de.fzj.unicore.rcp.gpe4eclipse.SWTProgressListener;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.parameters.LetUserSelectOutcomesProcessor;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.GridBeanRegistry;
import de.fzj.unicore.rcp.gpe4eclipse.ui.SelectGridBeanDialog;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;
import de.fzj.unicore.rcp.logmonitor.TextEditorDialog;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

/**
 * @author demuth
 */
public class FetchOutcomeCommand extends Command {

	private JobClient jobClient;
	private GridBeanClient<IWorkbenchPart> gridBeanClient;
	private GridBeanInfo selectedGBInfo;
	private IGridFileSetTransfer<IGridFileTransfer> transfers;
	private String downloadDir;

	public FetchOutcomeCommand(JobClient jobClient) {
		this.jobClient = jobClient;
	}

	/**
	 * Displays the dialog to select the gridbean.
	 * 
	 * @return the path to the selected gridbean.
	 */
	protected void askGridBeanInfo(final Application app) {
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {
				SelectGridBeanDialog dlg = new SelectGridBeanDialog(PlatformUI
						.getWorkbench().getDisplay().getActiveShell(), app);
				if (dlg.open() == IDialogConstants.OK_ID) {
					selectedGBInfo = (GridBeanInfo) dlg.getFirstResult();
				} else {
					selectedGBInfo = null;
				}
			}

		});

	}

	@Override
	public boolean canUndo() {
		return false;
	}

	@Override
	public void execute() {
		redo();
	}

	public String getDownloadDir() {
		return downloadDir;
	}

	public GridBeanClient<IWorkbenchPart> getGridBeanClient() {
		if (gridBeanClient == null) {
			ISWTClient client = GPEActivator.getDefault().getClient();
			setGridBeanClient(client.createGridBeanClient());
		}
		return gridBeanClient;
	}

	protected IGridFileSetTransfer<IGridFileTransfer> letUserSelectOutcomes(Client client,
			IGridFileSetTransfer<IGridFileTransfer> transfers, Map<String, Object> processorParams) {
		return LetUserSelectOutcomesProcessor.letUserSelect(client, transfers,
				processorParams);
	}

	@Override
	public void redo() {
		try {

			try {
				selectedGBInfo = GridBeanUtils
						.reconstructGridBeanInfo(jobClient.getOriginalJSDL());
			} catch (GPEException e) {
				GPEActivator.log(IStatus.ERROR,
						"Unable to access the job for fetching outcomes.", e);
				return;
			}
			final Map<String, Object> processorParams = new HashMap<String, Object>();
			String downloadDir = getDownloadDir();
			if (downloadDir == null) {
				downloadDir = PathUtils.getTempProject().getLocation()
						.toOSString();
			}
			processorParams.put(ProcessingConstants.DOWNLOAD_DIR, downloadDir);

			if (selectedGBInfo == null) {

				// job has not been submitted via the Rich Client and doesn't
				// have
				// a GridBeanModel element in its JSDL!
				// since the mapping between output files and file parameters to
				// be visualized by the GridBean cannot be reproduced, files
				// are only downloaded and not visualized with a GridBean
				final Client client = GPEActivator.getDefault().getClient();

				PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
					public void run() {
						Display display = PlatformUI.getWorkbench()
								.getDisplay();
						Shell shell = display.getActiveShell();
						if (shell == null) {
							shell = new Shell(display);
						}
						TextEditorDialog dialog = new TextEditorDialog(
								shell,
								"Info",
								"Job outcomes cannot be visualized with an application GUI. Selected files will only be downloaded.");
						dialog.open();
					}
				});

				transfers = new GridFileSetTransfer<IGridFileTransfer>();

				List<?> files = jobClient.getWorkingDirectory().listDirectory("");
				for (Object object : files) {
					GridFile file = (GridFile) object;

					IGridFileAddress source = jobClient.getWorkingDirectory()
							.getAbsoluteAddress(file.getPath(), null);

					GridFileAddress target = new GridFileAddress("",
							file.getPath());
					IGridFileTransfer transfer = new GridFileTransfer(source,
							target);
					transfers.addFile(transfer);
				}
				transfers.resolveFiles(client, new FakeProgressListener());
				PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
					public void run() {

						transfers = letUserSelectOutcomes(client, transfers,
								processorParams);
					}
				});

				BackgroundJob todo = new BackgroundJob(
						"") {
					
					
					@Override
					public IStatus run(final IProgressMonitor progress) {
						progress.beginTask("Downloading output files... ", 100);
						try {
							IProgressMonitor sub;
							if (transfers.getFiles().size() > 0) {
								sub = new SubProgressMonitor(progress, 90);
								sub.setTaskName("transferring...");
								IProgressListener listener = new SWTProgressListener(
										sub);
								client.getFileFactory().cacheGridFiles(
										transfers, client, listener);
							} else {
								progress.worked(90);
							}
							sub = new SubProgressMonitor(
									progress,
									10,
									SubProgressMonitor.PREPEND_MAIN_LABEL_TO_SUBTASK);
							sub.setTaskName("refreshing workspace...");
							String downloadDir = (String) processorParams
									.get(ProcessingConstants.DOWNLOAD_DIR);
							if (downloadDir != null) {
								Path p = new Path(downloadDir);
								PathUtils.refreshEclipseFolder(p,
										IResource.DEPTH_INFINITE, sub);
							}
							return Status.OK_STATUS;
						} catch (Exception e) {
							GPEActivator.log(IStatus.ERROR,
									"Unable to download files", e);
							return Status.CANCEL_STATUS;
						} finally {
							progress.done();
						}
					}
				};

				todo.schedule();

			} else {

				GridBeanRegistry registry = GPEActivator.getDefault()
						.getGridBeanRegistry();
				String gbPath = registry.getGridBeanPath(selectedGBInfo);
				getGridBeanClient().loadGridBean(gbPath, new IObserver() {
					public void observableUpdate(Object arg0, Object arg1) {

						if (jobClient != null) {

							ClientWrapper<JobClient, String> jobWrapper = new ClientWrapper<JobClient, String>(
									jobClient, "");
							getGridBeanClient().fetchOutcome(jobWrapper,
									new IObserver() {
										@SuppressWarnings("unchecked")
										public void observableUpdate(
												Object arg0, Object arg1) {

											if (arg1 instanceof Throwable) {
												GPEActivator
														.log(IStatus.WARNING,"Unable to fetch and show job outcomes",
																new Exception(
																		(Throwable) arg1));
												return;
											}

											transfers = (IGridFileSetTransfer<IGridFileTransfer>) processorParams
													.get(ProcessingConstants.FILE_TRANSFERS);

											if (transfers.getFiles().size() > 0) {

												getGridBeanClient()
														.openGridBeanOutputPanel();
											}

											String downloadDir = (String) processorParams
													.get(ProcessingConstants.DOWNLOAD_DIR);

											if (downloadDir != null) {
												Path p = new Path(downloadDir);
												PathUtils
														.refreshEclipseFolder(
																p,
																IResource.DEPTH_INFINITE,
																null);
											}

										}
									}, processorParams, null);
						}
					}
				});
			}
		} catch (Throwable t) {
			GPEActivator.log(IStatus.ERROR, "Unable to open Output view",
					new Exception(t));
		}
	}

	protected IStatus refreshWorkspace(IProgressMonitor progress) {

		try {
			ResourcesPlugin.getWorkspace().getRoot()
					.refreshLocal(IResource.DEPTH_INFINITE, progress);
		} catch (Exception e) {
			// this is a best effort service
		}
		return Status.OK_STATUS;
	}

	protected void refreshWorkspaceAsync() {
		BackgroundJob todo = new BackgroundJob(
				"Refreshing workspace...") {
			
			
			
			@Override
			public IStatus run(final IProgressMonitor progress) {
				return refreshWorkspace(progress);
			}
		};
		todo.schedule();
	}

	public void setDownloadDir(String downloadDir) {
		this.downloadDir = downloadDir;
	}

	public void setGridBeanClient(GridBeanClient<IWorkbenchPart> gbClient) {
		this.gridBeanClient = gbClient;
	}

}