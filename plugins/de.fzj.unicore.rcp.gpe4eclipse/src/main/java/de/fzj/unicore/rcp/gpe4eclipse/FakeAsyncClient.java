/*
 * Copyright (c) 2000-2005, Intel Corporation All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Intel Corporation nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.fzj.unicore.rcp.gpe4eclipse;

import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.api.async.AsyncClient;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.async.Request;
import com.intel.gpe.util.observer.IObserver;

/**
 * This implementation of AsyncClient actually executes requests syncronously.
 * This is helpful in order not to nest requests and observers too much.
 * 
 * @author demuth
 * @version $Id: AsyncProvider.java,v 1.3 2006/09/28 12:00:24 vashorin Exp $
 * 
 */
public class FakeAsyncClient implements AsyncClient {

	private class RequestRunnable implements Runnable {

		private Request request;
		private IObserver observer;

		public RequestRunnable(Request request, IObserver observer) {
			this.request = request;
			this.observer = observer;
		}

		public void run() {

			try {
				final Object result = request.perform(progress);
				if (observer != null) {
					PlatformUI.getWorkbench().getDisplay()
							.asyncExec(new Runnable() {
								public void run() {
									observer.observableUpdate(request, result);
								}

							});
				}
			} catch (final Throwable e) {
				// GPEActivator.log(Status.WARNING,
				// "Task \""+request.getName()+"\" could not be finished successfully.",
				// new Exception(e));
				if (observer != null) {
					PlatformUI.getWorkbench().getDisplay()
							.syncExec(new Runnable() {
								public void run() {
									observer.observableUpdate(request, e);
								}

							});
				}
			}

		}

	}

	private final IProgressListener progress = new FakeProgressListener();

	public FakeAsyncClient() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.AsyncClient#executeRequest(com.intel.gpe.client2
	 * .requests.Request, com.intel.gpe.util.observer.IObserver)
	 */
	public void executeRequest(Request request,
			com.intel.gpe.util.observer.IObserver observer) {
		try {
			new RequestRunnable(request, observer).run();
		} catch (Throwable t) {
			GPEActivator.log("Unable to execute request: ", new Exception(t));
		}

	}

}
