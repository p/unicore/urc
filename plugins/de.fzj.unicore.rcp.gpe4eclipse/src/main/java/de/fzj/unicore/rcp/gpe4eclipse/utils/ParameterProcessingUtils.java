package de.fzj.unicore.rcp.gpe4eclipse.utils;

import java.util.Map;

public class ParameterProcessingUtils {

	/**
	 * Surrounds the input String with String tokens that mark it as a variable
	 * value that needs to be replaced during GridBean input/output parameter
	 * processing. Used to add variables to file addresses.
	 * 
	 * @param variableName
	 * @return
	 */
	public static String createVariableFromName(String variableName) {
		return "${" + variableName + "}";
	}

	/**
	 * Adds String tokens to the input variable String that escape it so that it
	 * does NOT get replaced during GridBean input/output parameter processing.
	 * Used to refer to variables that are resolved on the server side. Example:
	 * variable = ${VARNAME} => result = \${VARNAME}
	 * 
	 * @param variableName
	 * @return
	 */
	public static String escapeVariable(String variable) {
		if (!variable.startsWith("${") || !variable.endsWith("}")) {
			return variable;
		}
		return '\\' + variable;
	}

	/**
	 * Used to replace all occurrences of variables in the given address with
	 * variable values from the processorParams.
	 * 
	 * @param address
	 * @param processorParams
	 * @return
	 */
	public static String replaceVariables(String address,
			Map<String, Object> processorParams) {
		if (address == null) {
			return null;
		}
		String result = "";
		int i = 0;
		while (i < address.length()) {
			if (address.charAt(i) == '$' && i < address.length() - 1
					&& address.charAt(i + 1) == '{') {
				if (i == 0 || address.charAt(i - 1) != '\\') // test for escaped
																// variables!
				{
					// found variable! try to replace it
					int j = i + 2;
					while (j < address.length() && address.charAt(j) != '}') {
						j++;
					}
					String varName = address.substring(i + 2, j);
					Object o = processorParams.get(varName);
					if (o != null && o instanceof String) {
						String resolved = (String) o;
						result += resolved;
					} else {
						result += address.substring(i, j + 1);
					}
					i = j;
				} else {
					// remove escape character
					result = result.substring(0, i - 1);
					result += address.charAt(i);
				}
			} else {
				result += address.charAt(i);
			}
			i++;
		}
		return result;
	}

}
