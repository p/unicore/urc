/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import org.eclipse.core.runtime.IAdapterFactory;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument.TargetSystemProperties;

import com.intel.gpe.clients.api.TargetSystemClient;

import de.fzj.unicore.rcp.gpe4eclipse.clientstubs.SWTTargetSystemClient;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.IMatchmaker;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.IRequirementRestrictor;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.TSSRequirementRestrictor;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;

/**
 * @author demuth
 * 
 */
@SuppressWarnings("rawtypes")
public class TSS2GPEAdapterFactory implements IAdapterFactory {

	public Object getAdapter(Object adaptableObject, Class adapterType) {
		WSRFNode node = (WSRFNode) adaptableObject;
		int state = node.getState();
		if (TargetSystemClient.class.equals(adapterType)
				&& state > Node.STATE_NEW && state < Node.STATE_FAILED) {
			if (!node.hasCachedResourceProperties()) {
				node.refresh(1, false, null);
				if (!node.hasCachedResourceProperties()) {
					return null;
				}
			}
			return new SWTTargetSystemClient(node.getPathToRoot());
		} else if (IRequirementRestrictor.class.equals(adapterType)) {
			TargetSystemProperties props = ((TargetSystemNode) node)
					.getTargetSystemProperties();
			return new TSSRequirementRestrictor(props);
		} else if (IMatchmaker.class.equals(adapterType)) {
			TSSMatchmaker result = new TSSMatchmaker(node.getPathToRoot());
			return result;
		} else {
			return null;
		}
	}

	public Class[] getAdapterList() {
		return new Class[] { TargetSystemClient.class,
				IRequirementRestrictor.class, IMatchmaker.class };
	}
}