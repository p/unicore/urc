package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.editors;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.DateTimeRequirementDialog;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.RequirementConstants;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.DateTimeRequirementValue;

/**
 * Provides a dialog cell editor for DateTimeRequirementValue
 * 
 * @author rajveer
 * 
 */
public class DateTimeRequirementCellEditor extends DialogCellEditor {

	DateTimeRequirementValue requirement;

	/**
	 * @param parent
	 *            is the table for that the CellEditor is provided
	 */
	public DateTimeRequirementCellEditor(Composite parent) {
		super(parent);
	}

	/**
	 * returns an object of DateTimeRequirementValue
	 */
	@Override
	protected Object doGetValue() {
		return requirement;
	}

	/**
	 * estimates an object of DateTimeRequirementValue
	 */
	@Override
	protected void doSetValue(Object value) {
		requirement = (DateTimeRequirementValue) value;
		super.doSetValue(requirement);
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {

		DateTimeRequirementValue workObject = requirement;
		DateTimeRequirementDialog dialog = new DateTimeRequirementDialog(
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				"");
		try {
			dialog.setInitialSelection(
					new SimpleDateFormat(RequirementConstants.DEFAULT_DATE_FORMAT).parse(workObject.getValue()));
		} catch (ParseException e) {
			GPEActivator.log("Unable to parse currently set value: ", e);
		}
		dialog.open();
		// set the new time only if user presses OK. Keep the old
		// settings otherwise.
		if (dialog.getReturnCode() == Dialog.OK) {
			workObject = dialog.getSelectedTime();
		}
		return workObject;
	}

}
