/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.resources;

import java.util.Map;

import javax.xml.namespace.QName;

import com.intel.gpe.util.collections.CollectionUtil;

/**
 * @author sholl
 * 
 *         Represents an execution environment. Stores all Arguments =
 *         ExecEnvValue and the name. Does not store any metadata or such,
 *         only the arguments.
 * 
 */
public class ExecEnvType {

	private Map<QName, ExecEnvValue<?>> valuereferences;

	private QName name;
	
	private String version;

	public ExecEnvType()
	{
		this(null,null);
	}
			
	public ExecEnvType(QName name, String version) {
		this.name = name;
		this.version = version;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ExecEnvType)) {
			return false;
		}
		ExecEnvType other = (ExecEnvType) o;
		return CollectionUtil.equalOrBothNull(getName(), other.getName())
		        && CollectionUtil.equalOrBothNull(getVersion(), other.getVersion())
				&& CollectionUtil.containSameElements(getValuereferences().entrySet(),
						other.getValuereferences().entrySet())
						;

	}

	public QName getName() {
		return name;
	}

	public Map<QName, ExecEnvValue<?>> getValuereferences() {
		return valuereferences;
	}

	public void setName(QName name) {
		this.name = name;
	}

	public void setValuereferences(Map<QName, ExecEnvValue<?>> valuereferences) {
		this.valuereferences = valuereferences;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}