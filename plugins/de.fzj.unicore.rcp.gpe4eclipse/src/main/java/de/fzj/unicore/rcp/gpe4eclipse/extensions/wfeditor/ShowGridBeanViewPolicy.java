/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;

import de.fzj.unicore.rcp.gpe4eclipse.commands.OpenJobEditorForActivityCommand;

public class ShowGridBeanViewPolicy extends AbstractEditPolicy {

	@Override
	public Command getCommand(Request request) {
		if (GridBeanActivityPart.REQ_SHOW_GRID_BEAN_VIEW.equals(request
				.getType()) || REQ_OPEN.equals(request.getType())) {
			EditPart host = getHost();
			if (host instanceof GridBeanActivityPart) {
				return new OpenJobEditorForActivityCommand(
						(GridBeanActivity) host.getModel());
			}
		}
		return null;
	}

}
