/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;

import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;

/**
 * represents a whole data-set (line) in the resourceRequirments table
 * 
 * @author Christian Hohmann
 * 
 */
public abstract class ResourceRequirement implements Serializable {

	public static final String qNameId = "de.fzj.unicore.rcp.gpe4eclipse.ResourceParameter";
	private static final long serialVersionUID = -480879980590772088L;

	private boolean enabled = false;
	private boolean dirty = false;
	private QName requirementName;
	private IRequirementValue value;
	private String description = "";

	private Set<QName> clashes = new HashSet<QName>();

	public ResourceRequirement() {
	}

	public ResourceRequirement(Boolean enabled, QName requirementName,
			IRequirementValue value, String addInformation) {
		this.enabled = enabled;
		this.requirementName = requirementName;
		this.value = value;
		this.description = addInformation;
	}

	public Set<QName> getClashes() {
		return clashes;
	}

	public String getDescription() {
		return description;
	}

	public QName getRequirementName() {
		return requirementName;
	}

	public IRequirementValue getValue() {
		return value;
	}

	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * reads the value of the current Requirement from the correct position out
	 * of the ResourceParameterValue
	 * 
	 * @param value
	 *            is the value from where the current Requirement should be read
	 *            in
	 */
	public abstract void loadFrom(ResourcesParameterValue value);

	public void setClashes(HashSet<QName> clashes) {
		this.clashes = clashes;
	}

	public void setDescription(String addInformation) {
		this.description = addInformation;
	}

	public void setEnabled(boolean enabled) {
		if(this.enabled == enabled) return;
		this.enabled = new Boolean(enabled);
	}

	public void setRequirementName(QName requirementName) {
		this.requirementName = requirementName;
	}

	public void setValue(IRequirementValue value) {
		this.value = value;
	}

	/**
	 * writes the value of the current Requirement on the correct position in
	 * the ResourceParameterValue
	 * 
	 * @param value
	 *            is the value where the current Requirement should be written
	 *            in
	 */
	public abstract void writeTo(ResourcesParameterValue value);

	public boolean isDirty() {
		return dirty;
	}

	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	@Override
	public String toString() {
		return "ResourceRequirement [enabled=" + enabled + ", requirementName="
				+ requirementName + ", value=" + value + "]";
	}
}