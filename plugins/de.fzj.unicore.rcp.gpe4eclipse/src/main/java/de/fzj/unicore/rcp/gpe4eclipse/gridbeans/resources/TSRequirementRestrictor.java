package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IStatus;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.OperatingSystemType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.OperatingSystemTypeEnumeration;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ProcessorArchitectureEnumeration;
import org.unigrids.services.atomic.types.SiteResourceType;

import com.intel.gpe.clients.api.jsdl.OSType;
import com.intel.gpe.clients.api.jsdl.ProcessorType;
import com.intel.gpe.clients.api.jsdl.RangeValueType;
import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnv;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnvRequirementSettings;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnvUtils;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.EnumRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.RangeValueTypeRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.utils.RangeValueTypeConverter;
import eu.unicore.jsdl.extensions.ExecutionEnvironmentDescriptionDocument.ExecutionEnvironmentDescription;
import eu.unicore.jsdl.extensions.ExecutionEnvironmentDocument.ExecutionEnvironment;

public abstract class TSRequirementRestrictor implements IRequirementRestrictor {

	private ResourceProviderAdapter props;

	protected TSRequirementRestrictor(ResourceProviderAdapter props) {
		this.props = props;
	}

	protected TSRequirementRestrictor() {
	}

	protected void init(ResourceProviderAdapter props) {
		this.props = props;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void restrictCPUArch(ResourceProviderAdapter props,
			RequirementList requirementList) {
		// RequirementConstants.cpuArchitectureRequirement
		ResourceRequirement req = requirementList
		.getResourceRequirement(RequirementConstants.cpuArchitectureRequirement);
		ProcessorArchitectureEnumeration.Enum cpuArchitectureValue = null;

		try {
			if (props.getProcessor() != null) {
				if (props.getProcessor().getCPUArchitecture() != null) {
					cpuArchitectureValue = props.getProcessor()
					.getCPUArchitecture().getCPUArchitectureName();
				}
				if (cpuArchitectureValue != null) {
					List<ProcessorType> validValues = new ArrayList<ProcessorType>();
					validValues.add(ProcessorType
							.fromString(cpuArchitectureValue.toString()));
					EnumRequirementValue val = (EnumRequirementValue) req.getValue();
					Object oldVal = val.getSelectedElement();
					val.setValidElements(validValues);
					if(!CollectionUtil.equalOrBothNull(val.getSelectedElement(), oldVal))
					{
						req.setEnabled(false);
					}
					requirementList.resourceRequirementChanged(RequirementConstants.cpuArchitectureRequirement);

				} else {
					removeRequirement(requirementList,req);
				}

				// RequirementConstants.cpuSpeedPerNodeRequirement
				ResourceRequirement cpuSpeedPerNode = requirementList
				.getResourceRequirement(RequirementConstants.cpuSpeedPerNodeRequirement);
				RangeValueType cpuSpeedPerNodeRequirementValue = RangeValueTypeConverter
				.convertToJava(props.getProcessor()
						.getIndividualCPUSpeed());
				if (cpuSpeedPerNodeRequirementValue != null) {
					RangeValueTypeRequirementValue range = 
						(RangeValueTypeRequirementValue) cpuSpeedPerNode
						.getValue();
					range.setValidValue(cpuSpeedPerNodeRequirementValue);
				} else {
					requirementList.removeResourceRequirement(RequirementConstants.cpuSpeedPerNodeRequirement);
				}
			} else {
				requirementList.removeResourceRequirement(RequirementConstants.cpuArchitectureRequirement);
				requirementList.removeResourceRequirement(RequirementConstants.cpuSpeedPerNodeRequirement);

			}
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR, "Unable to retrieve information about the server's CPU(s). This is probably a server side configuration issue.",
					e);
			requirementList.removeResourceRequirement(RequirementConstants.cpuArchitectureRequirement);
			requirementList.removeResourceRequirement(RequirementConstants.cpuSpeedPerNodeRequirement);
		}
	}

	protected void restrictCPUsPerNode(ResourceProviderAdapter props,
			RequirementList requirementList) {
		ResourceRequirement req = requirementList
		.getResourceRequirement(RequirementConstants.cpuPerNodeRequirement);
		try {
			
			if (props.getIndividualCPUCount() != null) {
				RangeValueType range = RangeValueTypeConverter
				.convertToJava(props.getIndividualCPUCount());
				restrictRangeValueRequirement(requirementList, req, range);
			} else {
				removeRequirement(requirementList,req);
			}
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to retrieve information about the server's number of CPUs per node. This is probably a server side configuration issue.",
					e);
			removeRequirement(requirementList,req);
		}
	}

	protected void restrictExecutionEnvironments(ResourceProviderAdapter props,
			RequirementList requirementList, ResourcesParameterValue param) {


		ExecutionEnvironment.Factory.newInstance();

		ExecutionEnvironmentDescription[] execEnvs = null;

		try {
			execEnvs = props.getExecutionEnvironmentDescriptionArray();

		} catch (Exception e) {

		}
		Set<QName> added = new HashSet<QName>();
		if (execEnvs != null && execEnvs.length != 0) {
			List<ExecEnv> envs = new ArrayList<ExecEnv>();
			for (ExecutionEnvironmentDescription execEnv : execEnvs) {
				try {
					ExecEnv ex = ExecEnvUtils.createExecEnv(execEnv);
					envs.add(ex);
				} catch (Exception e) {
					GPEActivator.log(IStatus.WARNING, "Problem while parsing execution environment defined by the site: "+e.getMessage()+". This hints to a misconfigured server, please contact the site administrator.",e);
				}

			}
			ExecEnvRequirementSettings settings = ExecEnvUtils.createResourceRequirement(envs);	
			if(settings != null)
			{
				// value.getSelectedExecEnv().setDirty();
				QName name = settings.getRequirementName();
				added.add(name);
				ExecEnvRequirementSettings oldSettings = (ExecEnvRequirementSettings) requirementList.getResourceRequirement(name);
				if(oldSettings == null)
				{
					requirementList.addResourceRequirement(settings);
				}
				else
				{
					settings.loadFrom(param);
					requirementList.replaceResourceRequirement(oldSettings,settings);	
				}
			}
		}
		ResourceRequirement[] allReqs = requirementList.toArray();
		for (ResourceRequirement req: allReqs) {
			if (!(req instanceof ExecEnvRequirementSettings) || added.contains(req.getRequirementName()))
				continue;
			removeRequirement(requirementList,req);
		}
	}

	protected void restrictNumNodes(ResourceProviderAdapter props,
			RequirementList requirementList) {
		ResourceRequirement req = requirementList
		.getResourceRequirement(RequirementConstants.totalResourcesRequirement);

		try {
			if(props.getTotalResourceCount() != null)
			{
				RangeValueType range = RangeValueTypeConverter
				.convertToJava(props.getTotalResourceCount());
				restrictRangeValueRequirement(requirementList, req, range);
			}
			else  removeRequirement(requirementList,req);
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to retrieve information about the server's total number of nodes. This is probably a server side configuration issue.",
					e);
			removeRequirement(requirementList,req);
		}

	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void restrictOS(ResourceProviderAdapter props,
			RequirementList requirementList) {

		ResourceRequirement req = requirementList
		.getResourceRequirement(RequirementConstants.osRequirement);
		try{
			List<OperatingSystemTypeEnumeration.Enum> osValue = null;
			if (props.getOperatingSystemArray() != null
					&& props.getOperatingSystemArray().length > 0) {
				osValue = new ArrayList<OperatingSystemTypeEnumeration.Enum>(
						props.getOperatingSystemArray().length);
				for (OperatingSystemType current : props
						.getOperatingSystemArray()) {
					if (current != null && current.getOperatingSystemType() != null
							&& current.getOperatingSystemType()
							.getOperatingSystemName() != null) {
						osValue.add(current.getOperatingSystemType()
								.getOperatingSystemName());
					}
				}
			}
			if (osValue != null && osValue.size() > 0) {
				List<OSType> validValues = new ArrayList<OSType>();
				for (OperatingSystemTypeEnumeration.Enum e : osValue) {
					validValues.add(OSType.fromString(e.toString()));
				}
				EnumRequirementValue val = (EnumRequirementValue) req.getValue();
				Object oldVal = val.getSelectedElement();
				val.setValidElements(validValues);
				if(!CollectionUtil.equalOrBothNull(val.getSelectedElement(), oldVal))
				{
					req.setEnabled(false);
				}

				requirementList.resourceRequirementChanged(RequirementConstants.osRequirement);
			} else {
				removeRequirement(requirementList,req);
			}
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,	"Unable to retrieve information about the server's operating system. This is probably a server side configuration issue.", e);
			removeRequirement(requirementList,req);
		}
	}

	protected void restrictRAM(ResourceProviderAdapter props,
			RequirementList requirementList) {

		ResourceRequirement req = requirementList
		.getResourceRequirement(RequirementConstants.ramPerNodeRequirement);
		try {
			if (props.getIndividualPhysicalMemoryArray() != null
					&& props.getIndividualPhysicalMemoryArray().length > 0) {
				RangeValueType range = RangeValueTypeConverter
				.convertToJava(props.getIndividualPhysicalMemoryArray()[0]);
				restrictRangeValueRequirement(requirementList, req, range);
			} else {
				removeRequirement(requirementList,req);
			}
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to retrieve information about the server's amount of RAM per node. This is probably a server side configuration issue.",
					e);
			removeRequirement(requirementList,req);
		}
	}


	public void restrictRequirementValues(RequirementList requirementList, ResourcesParameterValue param) {
		if (props == null) {
			return;
		}

		restrictCPUArch(props, requirementList);
		restrictCPUsPerNode(props, requirementList);
		restrictNumNodes(props, requirementList);
		restrictOS(props, requirementList);
		restrictRAM(props, requirementList);
		restrictSiteSpecificOldStyle(props, requirementList);
		restrictTotalCPUs(props, requirementList);
		restrictWallTime(props, requirementList);
		restrictExecutionEnvironments(props, requirementList, param);

		restrictSiteSpecific(ResourcesRegistry.toPOJO(props.getAvailableResourceArray()), requirementList,param);

	}

	protected void restrictSiteSpecificOldStyle(ResourceProviderAdapter props,
			RequirementList requirementList) {
		SiteResourceType[] siteResources = props.getSiteResourceArray();
		for (SiteResourceType siteResource : siteResources) {
			if (siteResource.getValue() == null) {
				continue;
			}
			try {
				SiteSpecificRequirementSetting setting = new SiteSpecificRequirementSetting();
				setting.setRequirementName(new QName(siteResource.getName()));
				setting.setDescription(siteResource.getDescription());
				RangeValueType value = RangeValueTypeConverter
				.convertToJava(siteResource.getValue());
				setting.setValue(new RangeValueTypeRequirementValue(value));
				requirementList.addResourceRequirement(setting);
			} catch (Exception e) {
				GPEActivator.log(IStatus.ERROR, "Unable to retrieve information about a site specific resource. This is probably a server side configuration issue.", e);
			}
		}
	}

	protected void restrictSiteSpecific(AvailableResource[] siteResources,
			RequirementList requirementList, ResourcesParameterValue param) {
		Arrays.sort(siteResources, new Comparator<AvailableResource>() {
			public int compare(AvailableResource o1,
					AvailableResource o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		Set<QName> added = new HashSet<QName>();
		if (siteResources != null) {
			for (AvailableResource available: siteResources) {
				if (available == null)
					continue;
				SSRSetting settings = SSRSetting.fromSiteProperties(available);
				if (settings == null)
					continue;
				QName name = settings.getRequirementName();
				added.add(name);
				SSRSetting oldSettings = (SSRSetting) requirementList.getResourceRequirement(name);
				if(oldSettings == null)
				{
					requirementList.addResourceRequirement(settings);
				}
				else
				{
					settings.loadFrom(param);
					requirementList.replaceResourceRequirement(oldSettings,settings);	
				}
			}
		}

		ResourceRequirement[] allReqs = requirementList.toArray();
		for (ResourceRequirement req: allReqs) {
			if (!(req instanceof SSRSetting) || added.contains(req.getRequirementName()))
				continue;

			removeRequirement(requirementList,req);
		}
	}


	protected void restrictTotalCPUs(ResourceProviderAdapter props,
			RequirementList requirementList) {

		// RequirementConstants.cpuPerNodeRequirement
		ResourceRequirement req = requirementList
		.getResourceRequirement(RequirementConstants.totalCpuRequirement);
		try {
			if (props.getTotalCPUCount() != null) {
				RangeValueType range = RangeValueTypeConverter
				.convertToJava(props.getTotalCPUCount());
				restrictRangeValueRequirement(requirementList, req, range);
			} else {
				removeRequirement(requirementList,req);
			}

		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,	"Unable to retrieve information about the server's total number of CPUs. This is probably a server side configuration issue.",
					e);
			removeRequirement(requirementList,req);
		}
	}

	protected void restrictWallTime(ResourceProviderAdapter props,
			RequirementList requirementList) {

		// RequirementConstants.timePerNodeRequirement
		ResourceRequirement req = requirementList
		.getResourceRequirement(RequirementConstants.timePerCPURequirement);
		try {
			if (props.getIndividualCPUTime() != null) {
				RangeValueType range = RangeValueTypeConverter
				.convertToJava(props.getIndividualCPUTime());
				restrictRangeValueRequirement(requirementList, req, range);
			} else {
				removeRequirement(requirementList,req);
			}
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to retrieve information about possible values for reserving CPU time. This is probably a server side configuration issue.",
					e);
			removeRequirement(requirementList,req);
		}
	}

	protected void restrictRangeValueRequirement(RequirementList requirementList, ResourceRequirement req, RangeValueType range)
	{
		if (range == null) {
			removeRequirement(requirementList,req);
		}
		else{
			RangeValueTypeRequirementValue oldRange = 
				(RangeValueTypeRequirementValue) req
				.getValue();
			oldRange.setValidValue(range);
			if(oldRange.isValid() != null)
			{
				req.setEnabled(false);
			}
			requirementList.resourceRequirementChanged(RequirementConstants.totalCpuRequirement);
		}
	}

	private void removeRequirement(RequirementList list, ResourceRequirement req)
	{
		list.removeResourceRequirement(req.getRequirementName());
	}
}
