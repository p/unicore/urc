package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry;

import java.net.URL;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;

public class GridBeanEntry {
	private GridBeanInfo info;
	private String path;
	private URL iconURL;

	public GridBeanEntry(GridBeanInfo info, String path, URL iconURL) {
		this.info = info;

		this.path = path;
		this.iconURL = iconURL;
	}

	public URL getIconURL() {
		return iconURL;
	}

	public GridBeanInfo getInfo() {
		return info;
	}

	public String getPath() {
		return path;
	}

	public void setIconURL(URL iconURL) {
		this.iconURL = iconURL;
	}

	public void setInfo(GridBeanInfo info) {
		this.info = info;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
