package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry;

import java.util.List;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;

public interface IGridBeanRegistryListener {

	public void gridBeansChanged(List<GridBeanInfo> oldValue,
			List<GridBeanInfo> newValue);
}
