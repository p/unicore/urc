package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables;

import javax.xml.namespace.QName;

import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.gridbeans.parameters.IEnvironmentVariableParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

public class EnvVariable {

	public static final String USER_DEFINED_VARIABLE_PREFIX = "USER_DEFINED";

	protected IGridBeanParameter gridBeanParameter;
	protected IEnvironmentVariableParameterValue value;
	protected boolean definedInGridBean = false;

	public EnvVariable(IGridBeanParameter gridBeanParameter,
			IEnvironmentVariableParameterValue value) {
		super();
		this.gridBeanParameter = gridBeanParameter;
		this.value = value;
	}

	@Override
	public EnvVariable clone() {
		EnvVariable clone = new EnvVariable(getGridBeanParameter(), getValue()
				.clone());
		return clone;
	}

	public IGridBeanParameter getGridBeanParameter() {
		return gridBeanParameter;
	}

	public QName getType() {
		try {
			return value.getVariableValue().getProtocol().getName();
		} catch (Exception e) {
			return ProtocolConstants.VALUE_QNAME;
		}
	}

	public IEnvironmentVariableParameterValue getValue() {
		return value;
	}

	public boolean isDefinedInGridBean() {
		return !getGridBeanParameter().getName().getNamespaceURI()
				.startsWith(USER_DEFINED_VARIABLE_PREFIX);
	}

	public void setGridBeanParameter(IGridBeanParameter parameter) {
		this.gridBeanParameter = parameter;
	}

	public void setValue(IEnvironmentVariableParameterValue value) {
		this.value = value;
	}
}
