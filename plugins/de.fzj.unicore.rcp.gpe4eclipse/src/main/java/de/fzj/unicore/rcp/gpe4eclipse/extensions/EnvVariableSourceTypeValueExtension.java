/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.clients.api.transfers.IAddressOrValue;
import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.clients.common.transfers.Value;
import com.intel.gpe.gridbeans.parameters.EnvironmentVariableParameterValue;
import com.intel.gpe.gridbeans.parameters.IEnvironmentVariableParameterValue;

import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IEnvVariableSourceTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.BooleanVariableSourceTypeValueCellEditor;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.EnvVariable;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.VariableSourceTypeValueCellEditor;

/**
 * @author demuth
 * 
 */
public class EnvVariableSourceTypeValueExtension implements
		IEnvVariableSourceTypeExtensionPoint {

	public void attachToParameterValue(IAdaptable adaptable,
			EnvVariable oldValue) {
		IEnvironmentVariableParameterValue variable = oldValue.getValue() == null ? new EnvironmentVariableParameterValue()
				: oldValue.getValue().clone();
		variable.setVariableValue(new Value(""));
		oldValue.setValue(variable);
	}

	public boolean availableInContext(GridBeanContext context) {
		return true;
	}

	public IAddressOrValue checkAndFixAddress(IAddressOrValue address,
			String oldVersion, String currentVersion) {
		// do nothing
		return address;

	}

	public void detachFromParameterValue(IAdaptable adaptable, EnvVariable value) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getCellEditor()
	 */
	public CellEditor getCellEditor(Composite parent, EnvVariable variable,
			IAdaptable activity) {
		if (ProtocolConstants.VALUE_PROTOCOL.equals(variable.getValue()
				.getVariableValue().getProtocol())
				&& Boolean.class.equals(variable.getValue().getTargetType())) {
			return new BooleanVariableSourceTypeValueCellEditor(parent);
		} else {
			return new VariableSourceTypeValueCellEditor(parent);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getType()
	 */
	public QName getType() {
		return ProtocolConstants.VALUE_QNAME;
	}

	public void updateParameterValue(IAdaptable adaptable,
			EnvVariable oldValue, EnvVariable newValue) {
		// TODO Auto-generated method stub

	}

}
