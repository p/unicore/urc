/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.ISaveablesLifecycleListener;
import org.eclipse.ui.ISaveablesSource;
import org.eclipse.ui.IURIEditorInput;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.Saveable;
import org.eclipse.ui.internal.DefaultSaveable;
import org.eclipse.ui.internal.SaveablesList;

import com.intel.gpe.clients.all.gridbeans.GridBeanClient;
import com.intel.gpe.clients.all.gridbeans.GridBeanJobWrapper;
import com.intel.gpe.clients.api.JobClient;
import com.intel.gpe.clients.api.TargetSystemClient;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.async.RequestExecutionService;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.clients.api.transfers.GPEFileFactory;
import com.intel.gpe.clients.common.JAXBContextProvider;
import com.intel.gpe.clients.common.clientwrapper.ClientWrapper;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.IGridBeanModel;
import com.intel.gpe.gridbeans.jsdl.GPEJobImpl;
import com.intel.gpe.gridbeans.parameters.GridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.ParameterTypeProcessorRegistry;
import com.intel.gpe.gridbeans.plugins.DataSetException;
import com.intel.gpe.gridbeans.plugins.IGenericGridBeanPanel;
import com.intel.gpe.gridbeans.plugins.IGridBeanPlugin;
import com.intel.gpe.gridbeans.plugins.IPanel;
import com.intel.gpe.util.observer.IObserver;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.common.actions.UnicoreCommonActionConstants;
import de.fzj.unicore.rcp.common.adapters.InputFileContainerAdapter;
import de.fzj.unicore.rcp.common.guicomponents.EditorInputChangedListener;
import de.fzj.unicore.rcp.common.guicomponents.EditorPartWithActions;
import de.fzj.unicore.rcp.common.guicomponents.IEditorPartWithSettableInput;
import de.fzj.unicore.rcp.common.guicomponents.SetRelativeTerminationTimeDialog;
import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.common.utils.swing.SafeSwingUtilities;
import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.ISWTClient;
import de.fzj.unicore.rcp.gpe4eclipse.SWTGridBeanInputPanel;
import de.fzj.unicore.rcp.gpe4eclipse.SWTProgressListener;
import de.fzj.unicore.rcp.gpe4eclipse.SWTSyncClient;
import de.fzj.unicore.rcp.gpe4eclipse.clientstubs.IClientChangeListener;
import de.fzj.unicore.rcp.gpe4eclipse.clientstubs.IMutableClient;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser.MatchmakingResourceFilter;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GridBeanActivity;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.IRequirementRestrictor;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.RequirementContentProvider;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.RequirementList;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.RequirementListFiller;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.ResourceRequirement;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.ResourceRequirementsViewer;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageListFactory;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in.StageInList;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in.StageInTableViewer;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out.StageOutList;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out.StageOutTableViewer;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.EnvVariableList;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.EnvVariableListFactory;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.variables.EnvVariableTableViewer;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GPEPathUtils;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;
import de.fzj.unicore.rcp.gpe4eclipse.utils.JobEditorOutcomeFetcher;
import de.fzj.unicore.rcp.gpe4eclipse.utils.JobEditorSubmitter;
import de.fzj.unicore.rcp.gpe4eclipse.utils.JobMonitoringService;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceSelectionPanel;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;

/**
 * The editor that opens jobs by opening gridbeans.
 * 
 * @author Bastian Demuth, Valentina Huber
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class JobEditor extends EditorPartWithActions implements
ISelectionChangedListener, PropertyChangeListener,
IEditorPartWithSettableInput,
IClientChangeListener,
ISaveablesSource {

	public static String ID = "de.fzj.unicore.rcp.gpe4eclipse.ui.JobEditor";

	protected GPEJobImpl currentJob;
	protected Job jobUpdater;
	protected boolean dirty = false;
	protected boolean disposed = false;
	protected EnvVariableList envVariableList;
	protected boolean envVariablePanelAdded = false;
	protected EnvVariableTableViewer envVariableTableViewer;
	protected int executionState = ExecutionStateConstants.STATE_UNSUBMITTED;

	protected Action fetchOutcomeAction;

	protected Button filterAutomaticallyButton, filterNowButton;

	protected GridBeanClient<IWorkbenchPart> gridBeanClient;

	protected List<IPanel> gridBeanPanels;
	protected Map<IPanel, Widget> gridBeanPanelControls = new HashMap<IPanel, Widget>();

	/**
	 * This variable states whether the editor has been placed in the
	 * EditorStack that holds all job editors or not. This is needed in order
	 * not to put the editor there twice.
	 */
	protected boolean hasBeenPositioned = false;

	/**
	 * GPE JobClient that can be used for monitoring a running job and fetching
	 * outcomes.
	 */
	protected ClientWrapper<JobClient, String> jobClient;

	protected boolean listeningToGridBeanModel = false;

	protected boolean loading = false;

	private RequirementList requirementList = null;

	protected Composite resourceComposite, stagingComposite;
	protected boolean resourcePropertiesPanelAdded = false;

	private ResourceRequirementsViewer resourceRequirementsViewer;
	protected ServiceSelectionPanel resourceSelectionPanel;

	protected TabItem resourceSelectionTabItem;
	protected Set<String> selectableResourceTypes = null;
	protected MatchmakingResourceFilter matchmakingResourceFilter;
	protected Action setTerminationTimeAction;

	protected boolean showingDataStaging = true;

	protected boolean showingResourceParameterPanel = true;
	protected Label stageInLabel, stageOutLabel;
	protected StageInList stageInList;

	protected StageInTableViewer stageInTableViewer;

	protected StageOutList stageOutList;

	protected StageOutTableViewer stageOutTableViewer;

	protected boolean stagingPanelsAdded = false;

	protected IAction submitAction;
	protected boolean submittingJob = false;

	protected TabFolder tabFolder;

	protected List<IAdaptable> targetResources;

	protected Saveable[] saveables = null;

	TargetSystemClient targetSystem = null;

	public void addDataStageingPanels(StageInList stageInList,
			StageOutList stageOutList, CommandStack commandStack) {
		if (isShowingDataStaging()) {
			// panels already created? if so, return
			if (stagingPanelsAdded) {
				return;
			}

			this.stageInList = stageInList;
			this.stageOutList = stageOutList;

			TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
			tabItem.setText("Files");

			stagingComposite = new Composite(tabFolder, SWT.NONE);
			tabItem.setControl(stagingComposite);
			Layout layout = new FormLayout();
			FormData data;
			stagingComposite.setLayout(layout);

			stagingPanelsAdded = true;
			if (stageInList != null) {
				stageInLabel = new Label(stagingComposite, SWT.NONE);
				stageInLabel.setText("Imports to job directory:");
				data = new FormData();
				data.top = new FormAttachment(0, 25);
				data.left = new FormAttachment(0, 20);
				stageInLabel.setLayoutData(data);

				stageInTableViewer = new StageInTableViewer(stagingComposite,
						stageInList, commandStack);
				data = new FormData();
				data.top = new FormAttachment(stageInLabel, 10);
				if (stageOutList != null) {
					data.bottom = new FormAttachment(50, 0);
				} else {
					data.bottom = new FormAttachment(100, -25);
				}
				data.left = new FormAttachment(0, 20);
				data.right = new FormAttachment(100, -20);
				stageInTableViewer.getControl().setLayoutData(data);
			}
			if (stageOutList != null) {
				stageOutLabel = new Label(stagingComposite, SWT.NONE);
				stageOutLabel.setText("Exports from job directory:");
				data = new FormData();
				if (stageInTableViewer != null) {
					data.top = new FormAttachment(
							stageInTableViewer.getControl(), 20);
				} else {
					data.top = new FormAttachment(0, 25);
				}
				data.left = new FormAttachment(0, 20);
				stageOutLabel.setLayoutData(data);

				stageOutTableViewer = new StageOutTableViewer(stagingComposite,
						stageOutList, commandStack);
				data = new FormData();
				data.top = new FormAttachment(stageOutLabel, 10);
				data.left = new FormAttachment(0, 20);
				data.right = new FormAttachment(100, -20);
				data.bottom = new FormAttachment(100, -20);
				stageOutTableViewer.getControl().setLayoutData(data);
			}
			stagingComposite.layout();
		}
	}

	public void addEnvVariablePanel(EnvVariableList envVariableList,
			CommandStack commandStack) {
		// panel already created? if so, return
		if (envVariablePanelAdded) {
			return;
		}
		TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
		tabItem.setText("Variables");
		envVariableTableViewer = new EnvVariableTableViewer(tabFolder,
				envVariableList, commandStack);
		tabItem.setControl(envVariableTableViewer.getControl());
		this.envVariableList = envVariableList;
		envVariablePanelAdded = true;

	}

	public void addResourceSelectionPanel() {
		// don't add the panel twice!
		// it is needed to create new tab for target system selection
		if (resourceSelectionPanel != null) {
			resourceSelectionPanel.dispose();
			resourceSelectionPanel = null;
			resourceSelectionTabItem.dispose();

			resourceSelectionTabItem = null;
		}

		resourceComposite = new Composite(tabFolder, SWT.NONE);
		Layout layout = new FormLayout();
		FormData data;
		resourceComposite.setLayout(layout);

		resourceSelectionTabItem = new TabItem(tabFolder, SWT.NONE);
		resourceSelectionTabItem.setText("Resources");

		resourceSelectionTabItem.setControl(resourceComposite);

		Label requirementLabel = new Label(resourceComposite, SWT.NONE);
		requirementLabel.setText("Job Properties:");
		data = new FormData();
		data.top = new FormAttachment(0, 25);
		data.left = new FormAttachment(0, 20);
		requirementLabel.setLayoutData(data);

		IGridBean model;
		try {
			model = getGridBeanClient().getGridBeanJob().getModel();
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR, "Unable to create resource panel: "
					+ e.getMessage(), e);
			return;
		}
		resourceRequirementsViewer = new ResourceRequirementsViewer(
				resourceComposite, model, requirementList);
		data = new FormData();
		data.top = new FormAttachment(requirementLabel, 20);
		data.bottom = new FormAttachment(100, -20);
		data.left = new FormAttachment(0, 20);
		data.right = new FormAttachment(60, 0);
		resourceRequirementsViewer.getControl().setLayoutData(data);

		Label resourceSelectionLabel = new Label(resourceComposite, SWT.NONE);
		resourceSelectionLabel.setText("Selected Target System:");
		data = new FormData();
		data.top = new FormAttachment(0, 25);
		data.right = new FormAttachment(100, -20);
		data.left = new FormAttachment(resourceRequirementsViewer.getControl(),
				20);
		resourceSelectionLabel.setLayoutData(data);

		resourceSelectionPanel = new ServiceSelectionPanel(resourceComposite,
				getSelectableResourceTypes(), targetResources, null, 3,
				ServiceSelectionPanel.DEFAULT_VIEWER_STYLE, false);
		matchmakingResourceFilter = new MatchmakingResourceFilter(
				getGridBeanClient().getClient(), model, resourceSelectionPanel);

		GC gc = new GC(resourceSelectionLabel);
		int buttonHeight = 2 * gc.getFontMetrics().getHeight() + 20;
		gc.dispose();
		data = new FormData();
		data.top = new FormAttachment(resourceSelectionLabel, 20);
		data.left = new FormAttachment(resourceRequirementsViewer.getControl(),
				20);
		data.right = new FormAttachment(100, -20);
		data.bottom = new FormAttachment(100, -40 - buttonHeight);
		resourceSelectionPanel.getControl().setLayoutData(data);

		filterAutomaticallyButton = new Button(resourceComposite, SWT.CHECK);
		filterAutomaticallyButton.setText("Filter automatically");
		filterAutomaticallyButton
		.setToolTipText("Automatically hide target systems that do not match the selected application or the resource requirements.");

		data = new FormData();
		data.top = new FormAttachment(resourceSelectionPanel.getControl(), 10);
		data.left = new FormAttachment(resourceRequirementsViewer.getControl(),
				20);
		data.right = new FormAttachment(100, -20);
		filterAutomaticallyButton.setLayoutData(data);
		filterAutomaticallyButton.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {
				matchmakingResourceFilter.scheduleUpdate(null);
				matchmakingResourceFilter.setActive(!matchmakingResourceFilter
						.isActive());
			}

			public void widgetSelected(SelectionEvent e) {
				matchmakingResourceFilter.scheduleUpdate(null);
				matchmakingResourceFilter
				.setAutoSchedule(!matchmakingResourceFilter
						.isAutoSchedule());
			}
		});
		filterAutomaticallyButton.setSelection(true);

		filterNowButton = new Button(resourceComposite, SWT.PUSH);
		data = new FormData();
		data.top = new FormAttachment(filterAutomaticallyButton, 10);
		data.left = new FormAttachment(resourceRequirementsViewer.getControl(),
				20);
		data.right = new FormAttachment(100, -20);
		filterNowButton.setLayoutData(data);
		filterNowButton.setText("Filter now");
		filterNowButton
		.setToolTipText("Run filter that hides target systems that do not match the selected application or the resource requirements.");

		filterNowButton.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {
				matchmakingResourceFilter.scheduleUpdate(null);
			}

			public void widgetSelected(SelectionEvent e) {
				matchmakingResourceFilter.scheduleUpdate(null);
			}
		});

		// make refresh action available only if this panel is selected
		// tabFolder.addSelectionListener(resourceSelectionPanelListener);

		// refreshResourceSelectionPanelAction.setNode(resourceSelectionPanel.getGlobalRefreshAction().getNode());
		// refreshResourceSelectionPanelAction.setViewer(resourceSelectionPanel.getGlobalRefreshAction().getViewer());
	}

	public void checkActionsEnabled() {
		checkSubmitActionEnabled();
		checkFetchOutcomeActionEnabled();
		checkSetTerminationTimeActionEnabled();

	}

	public void checkFetchOutcomeActionEnabled() {
		fetchOutcomeAction
		.setEnabled(jobClient != null
				&& getExecutionState() == ExecutionStateConstants.STATE_SUCCESSFUL);

	}

	public void checkGenericTabsEnabled() {
		if (isDisposed()) {
			return;
		}
		boolean enabled = getExecutionState() == ExecutionStateConstants.STATE_UNSUBMITTED;
		if (stageInTableViewer != null
				&& stageInTableViewer.getControl() != null) {
			stageInTableViewer.setEnabled(enabled);
		}
		if (stageOutTableViewer != null
				&& stageOutTableViewer.getControl() != null) {
			stageOutTableViewer.setEnabled(enabled);
		}
		if (envVariableTableViewer != null
				&& envVariableTableViewer.getControl() != null) {
			envVariableTableViewer.setEnabled(enabled);
		}
		if (resourceRequirementsViewer != null
				&& resourceRequirementsViewer.getControl() != null) {
			resourceRequirementsViewer.getControl().setEnabled(enabled);
		}
		if (resourceSelectionPanel != null
				&& resourceSelectionPanel.getControl() != null) {
			resourceSelectionPanel.getControl().setEnabled(enabled);
		}
		if (filterNowButton != null) {
			filterNowButton.setEnabled(enabled);
		}
		if (filterAutomaticallyButton != null) {
			filterAutomaticallyButton.setEnabled(enabled);
		}
	}

	public void checkSetTerminationTimeActionEnabled() {
		setTerminationTimeAction
		.setEnabled(!isSubmittingJob()
				&& getExecutionState() == ExecutionStateConstants.STATE_UNSUBMITTED);

	}

	public void checkSubmitActionEnabled() {
		// can we submit?
		boolean canSubmit = !isSubmittingJob()
		&& getExecutionState() == ExecutionStateConstants.STATE_UNSUBMITTED;
		if (canSubmit) {
			// disallow submission if gridbean is part of a workflow
			canSubmit = canSubmit && !isPartOfWorkflow();
		}
		submitAction.setEnabled(canSubmit);

	}

	@Override
	public void clientChanged(IMutableClient client) {
		if(client == targetSystem)
		{
			GridBeanClient gbClient = getGridBeanClient();
			if(gbClient == null) return;
			GridBeanJobWrapper jobWrapper = gbClient.getGridBeanJobWrapper();
			if(jobWrapper == null) return;
			IGridBeanPlugin plugin = jobWrapper.getGridBeanPlugin();
			if(plugin == null) return;

			try {
				gbClient.applyInputPanelValues();
				gbClient.reloadInputPanel(null);
			} catch (Exception e) {
				GPEActivator.log("Unable to reload application UI: "+e.getMessage(),e);
			}

		}
	}

	public File computeInputFileForJobName() {
		File old = getInputFile();
		if(old == null) return null;
		File parent = old.getParentFile();
		String jobName = (String) getGridBeanModel()
		.get(IGridBeanModel.JOBNAME);
		if (jobName == null || jobName.trim().length() == 0) {
			return old;
		} else {
			IPath p = Path.fromOSString(parent.getAbsolutePath());
			p = p.append(jobName).addFileExtension(Constants.JOB_FORMAT);
			return p.toFile();
		}
	}

	public void continueMonitoring(final String jobClientAddress) {
		Job j = new BackgroundJob("continuing to monitor job execution") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				NodePath path = null;
				try {
					path = NodePath.parseString(jobClientAddress);
				} catch (URISyntaxException e1) {
					GPEActivator.log(IStatus.ERROR,
							"Found invalid serialized job address: "
							+ jobClientAddress);
					return Status.CANCEL_STATUS;
				}
				Node n = NodeFactory.revealNode(path, monitor);
				if (n != null) {
					JobClient jobClient = (JobClient) n
					.getAdapter(JobClient.class);
					if (jobClient != null) {
						try {
							final ClientWrapper<JobClient, String> wrapper = new ClientWrapper<JobClient, String>(
									jobClient, jobClient.getName());
							PlatformUI.getWorkbench().getDisplay()
							.asyncExec(new Runnable() {

								public void run() {
									setJobClient(wrapper);
								}
							});
						} catch (Exception e) {
							GPEActivator.log(
									IStatus.ERROR,
									"Unable to continue job monitoring for job "
									+ jobClientAddress + ": "
									+ e.getMessage(), e);
						}
					}
				}
				return Status.OK_STATUS;
			}
		};
		j.schedule();
	}

	private void createActions() {

		submitAction = new Action("Submit",
				UnicoreCommonActivator.getImageDescriptor("run.gif")) {
			@Override
			public void run() {
				new JobEditorSubmitter().submit(JobEditor.this);
			}
		};
		submitAction.setId(UnicoreCommonActionConstants.ACTION_SUBMIT);
		checkSubmitActionEnabled();
		setAction(UnicoreCommonActionConstants.ACTION_SUBMIT, submitAction);

		setTerminationTimeAction = new Action("Set termination time...",
				UnicoreCommonActivator.getImageDescriptor("clock_edit.png")) {
			@Override
			public void run() {
				setTerminationTime();
			}
		};
		setAction(UnicoreCommonActionConstants.ACTION_SET_TERMINATION_TIME,
				setTerminationTimeAction);

		fetchOutcomeAction = new Action("fetch output files of...",
				GPEActivator.getImageDescriptor("export_wiz.gif")) {
			@Override
			public void run() {
				JobEditorOutcomeFetcher.fetchOutcome(JobEditor.this,
						getJobClient());
			}
		};
		fetchOutcomeAction.setEnabled(false);
		setAction(UnicoreCommonActionConstants.ACTION_FETCH_OUTCOMES,
				fetchOutcomeAction);

		checkActionsEnabled();
	}

	protected ResourcesParameterValue createAndInitRequirementList() {
		ResourcesParameterValue value = (ResourcesParameterValue) getGridBeanModel().get(IGridBean.RESOURCES);
		if (value == null) {
			value = new ResourcesParameterValue();
		} else {
			value = value.clone();
		}
		if (resourceRequirementsViewer != null) {

			if (requirementList == null) {
				requirementList = new RequirementList();

			}
			ResourceRequirement[] oldReqs = requirementList.toArray();
			RequirementListFiller.initAvailableResourceList(requirementList, value);
			if (targetResources != null && targetResources.size() == 1) {
				// if an adapter is available, update requirement list according
				// to selected target resource otherwise use standard list
				Object adapter = targetResources.get(0).getAdapter(
						IRequirementRestrictor.class);

				if (adapter != null) {


					// now restrict the requirement list
					((IRequirementRestrictor) adapter)
					.restrictRequirementValues(requirementList,value);

					for(ResourceRequirement req : oldReqs)
					{
						if(requirementList.getResourceRequirement(req.getRequirementName()) == null)
						{
							// has been removed
							req.setEnabled(false);
							req.writeTo(value);
						}
					}
					for(ResourceRequirement req : requirementList.toArray())
					{

						// store what is left of the old settings
						req.writeTo(value);
					}
				}
			}

		}
		// this MUST be done whether the requirement list is reused or not!
		// otherwise, the requirement values are not restored from the saved job
		resourceRequirementsViewer.setResourceRequirementList(requirementList);
		return value;
	}

	private GPEJobImpl createCurrentJob(IProgressMonitor progress)
	throws Exception {
		GPEJobImpl currentJob = new GPEJobImpl();
		IGridBean gridbean = getGridBeanModel();
		if (gridbean == null) {
			return null; // can happen when the editor gets closed before we are
			// executed
		}
		gridbean.setupJobDefinition(currentJob);
		return currentJob;

	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		JobEditorInput input = getEditorInput();
		if (input == null || input.wasInvalidWhilePersisting()) {
			return;
		}

		tabFolder = new TabFolder(parent, SWT.BOTTOM);
		tabFolder.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, true));

		createActions();
		hookContextMenu();

		if (PlatformUI.getWorkbench().isStarting()) {
			// wait with showing the editor's content which might contain Swing
			// components until the workbench has started up successfully
			// if we don't do this, we can run into severe threading problems
			// since background jobs and asyncExecs are delayed until the
			// workbench
			// has started
			parent.getDisplay().asyncExec(new Runnable() {
				public void run() {
					initModelAndPanels();
				}
			});

		} else {
			initModelAndPanels();
		}

		// used for closing the editor if the underlying resource is closed or
		// deleted
		// don't do this if we are part of a workflow as files of old activities
		// will be
		// deleted when saving the diagram!
		if (!isPartOfWorkflow()) {
			new EditorInputChangedListener(this);
		}

	}

	@Override
	public void dispose() {
		// set disposed to true early, even if the actual
		// disposal takes some time... this will signal
		// others that they should abandon this JobEditor
		disposed = true; 

		if (getEditorInput().wasInvalidWhilePersisting()) {
			super.dispose();
			return;
		}

		// When closing the editor immediately after modifying the value in a
		// Swing gridbean's text field, there might be pending changes to the
		// gridbean model. These should be processed NOW, otherwise we won't 
		// notice that we should be dirty
		SWTSyncClient syncClient = (SWTSyncClient) RequestExecutionService.getDefaultSyncClient();
		syncClient.processOutstandingRequests();

		if (matchmakingResourceFilter != null) {
			matchmakingResourceFilter.dispose();
		}
		try {
			GridBeanClient<?> gridBeanClient = getGridBeanClient();
			if (gridBeanClient != null
					&& gridBeanClient.getGridBeanJob() != null) {
				getGridBeanModel()
				.removePropertyChangeListener(this);
			}
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while disposing job editor: "+e.getMessage(), e);
		}

		if(gridBeanClient != null)
		{
			try {
				File file = getInputFile();

				if (dirty && file != null && file.exists()) // don't use getDirty() here!!
				{

					if (getExecutionState() >= ExecutionStateConstants.STATE_SUBMITTING) {
						// job has been submitted => always save its current state
						doSaveGridBean(file);
						// set input panel to null.. this will also dispose the
						// gridBeanPanels
						gridBeanClient.setGridBeanInputPanel(null);
					} else {
						// job has not been submitted and not been saved => restore
						// last saved state
						final IGridBean oldGridBean = getGridBeanModel();
						// set input panel to null.. this will also dispose the
						// gridBeanPanels
						gridBeanClient.setGridBeanInputPanel(null);
						gridBeanClient.loadJob(file.getAbsolutePath(),
								new IObserver() {
							public void observableUpdate(
									Object theObserved, Object changeCode) {
								oldGridBean
								.set(GPE4EclipseConstants.MODEL_DISPOSED,
										true);
							}
						});

					}
				} else {
					// set input panel to null.. this will also dispose the
					// gridBeanPanels
					gridBeanClient.setGridBeanInputPanel(null);
				}


				// the next line is necessary to remove the plugin as a
				// PropertyChangeListener from the GridBean..
				gridBeanClient.getGridBeanPlugin().setGridBeanModel(null);
				gridBeanClient.getGridBeanJobWrapper().setGridBeanPlugin(null);
			} catch (Exception e) {
				GPEActivator.log("An error occurred while disposing the job editor: "+e.getMessage(), e);
			}
			gridBeanClient = null;
		}

		if (resourceSelectionPanel != null) {
			resourceSelectionPanel.dispose();
			resourceSelectionPanel = null;
		}
		if (resourceSelectionTabItem != null) {
			resourceSelectionTabItem.dispose();
			resourceSelectionTabItem = null;
		}

		targetResources = null;

		if (stageInList != null) {
			stageInList.dispose();
		}
		if (stageOutList != null) {
			stageOutList.dispose();
		}
		if (envVariableList != null) {
			envVariableList.dispose();
		}

		super.dispose();
		getEditorInput().dispose();
	}

	public void doActualSave(File file, IProgressMonitor monitor) {
		IProgressListener progress = null;
		monitor = ProgressUtils.getNonNullMonitorFor(monitor);

		progress = new SWTProgressListener(monitor);
		progress.beginTask("Saving job", 3);

		try {

			if (file != null) {
				final IProgressListener saveExternal = progress.beginSubTask(
						"Saving external files", 1);
				SafeSwingUtilities.invokeAndWait(new Runnable() {
					public void run() {
						try {
							getGridBeanClient().getGridBeanPlugin()
							.saveDataToExternalSource(saveExternal);
						} catch (DataSetException e) {
							GPEActivator
							.log(IStatus.ERROR,
									"Could not save Job's data to external files",
									e);
						}
					}
				});
				getGridBeanClient().validateJob();
				doSaveGridBean(file);
				SubProgressMonitor sub = new SubProgressMonitor(monitor, 1);
				IProject p = getInputFileProject();
				if (p != null) {
					p.refreshLocal(IResource.DEPTH_INFINITE, sub);
				}
			}
			currentJob = createCurrentJob(null);
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR, "Error while saving job.", e);
		} finally {
			progress.done();
		}
		updatePartName();
	}

	public void doLoad() {
		File f = getInputFile();
		if(f == null) return;
		getGridBeanClient().loadJob(f.getAbsolutePath(),
				new IObserver() {
			public void observableUpdate(Object theObserved,
					Object changeCode) {
				if (isDisposed()) {
					return; // the editor may have been disposed in the
					// meantime as this is called asynchronously
				}
				if (changeCode instanceof Exception) {
					loading = false;
					// don't log exception here, it has already been logged by
					// the GridBeanClient
				} else {
					initGridBeanModel();
					initPanels();
				}

			}
		});

	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		if (isPartOfWorkflow()) {
			// do NOT save here, but save by saving the entire parent workflow
			// this is done in order to always stay consistent with the
			// surrounding workflow
			// it will trigger doActualSave and thus save myself, too
			getEditorInput().getWfEditor().doSave(monitor);
		} else {
			File oldFile = getInputFile();
			File newFile = computeInputFileForJobName();
			if(newFile == null) return;
			if (!newFile.equals(oldFile)) {
				setInput(new JobEditorInput(getGridBeanClient(),
						Path.fromOSString(newFile.getAbsolutePath())));
				if (!oldFile.delete()) {
					GPEActivator.log(
							IStatus.ERROR,
							"Unable to delete old job file "
							+ oldFile.getAbsolutePath()
							+ " after renaming the job ");
				}
			}
			monitor.beginTask("Saving job", 5);
			try {
				SubProgressMonitor sub = new SubProgressMonitor(monitor, 1);
				IProject p = getInputFileProject();
				if (p != null) {
					p.refreshLocal(IResource.DEPTH_INFINITE, sub);
				}
				sub = new SubProgressMonitor(monitor, 4);
				try {
					getGridBeanClient().applyInputPanelValues();
				} catch (Exception e) {
					GPEActivator.log(IStatus.ERROR,
							"Could not apply pending Job input values", e);
				}
				doActualSave(getInputFile(), sub);
			} catch (CoreException e) {
				GPEActivator.log(IStatus.ERROR,
						"Error while refreshing project "
						+ getInputFileProject().getName(), e);
			} finally {
				monitor.done();
			}
		}
		setDirty(false);
	}

	/**
	 * Intentionally left empty
	 * @see #isSaveAsAllowed()
	 * @see org.eclipse.ui.part.EditorPart#doSaveAs()
	 */
	@Override
	public void doSaveAs() {

	}

	public void doSaveGridBean(File file) {
		try {

			Display d = getSite().getShell().getDisplay();
			GridBeanUtils.saveJobAndWait(getGridBeanClient(), file, d);
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while serializing job to file: "+e.getMessage(), e);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof JobEditor)) {
			return false;
		}
		JobEditor other = (JobEditor) o;
		return other.getEditorInput() == null ? getEditorInput() == null
				: other.getEditorInput().equals(getEditorInput());
	}

	@Override
	public JobEditorInput getEditorInput() {
		return (JobEditorInput) super.getEditorInput();
	}

	public int getExecutionState() {
		return executionState;
	}

	public GridBeanClient<IWorkbenchPart> getGridBeanClient() {
		if (gridBeanClient == null) {
			gridBeanClient = (GridBeanClient<IWorkbenchPart>) getEditorInput()
			.getAdapter(GridBeanClient.class);
			// if this didn't work fall back to creating a new GridBeanClient
			if (gridBeanClient == null) {
				ISWTClient client = GPEActivator.getDefault().getClient();
				gridBeanClient = client.createGridBeanClient();
			}
			setTempDir();
		}
		return gridBeanClient;
	}

	public GridBeanContext getGridBeanContext() {
		try {
			return (GridBeanContext) getGridBeanModel().get(
					GPE4EclipseConstants.CONTEXT);
		} catch (Exception e) {
			return null;
		}
	}

	public IGridBean getGridBeanModel() {
		try {
			return getGridBeanClient().getGridBeanJob().getModel();
		} catch (Exception e) {
			return null;
		}
	}



	/**
	 * Returns the editor's input file or null if the input file is somehow 
	 * invalid.
	 * @return
	 */
	public File getInputFile() {
		IPathEditorInput file = null;
		if (getEditorInput() instanceof IPathEditorInput) {
			file = getEditorInput();
		} else {
			file = (IPathEditorInput) getEditorInput().getAdapter(
					IPathEditorInput.class);
		}
		if(file == null)
		{
			return null;
		}
		IPath path = file.getPath();
		if(path == null)
		{
			return null;
		}
		return path.toFile();
	}

	public IProject getInputFileProject() {
		IPathEditorInput file = null;
		if (getEditorInput() instanceof IPathEditorInput) {
			file = getEditorInput();
		} else {
			file = (IPathEditorInput) getEditorInput().getAdapter(
					IPathEditorInput.class);
		}
		IProject result = null;
		try {
			result = ResourcesPlugin.getWorkspace().getRoot()
			.getContainerForLocation(file.getPath()).getProject();
		} catch (Exception e) {
			GPEActivator.log(IStatus.WARNING,
					"Unable to determine project for job file "
					+ file.getPath().toOSString(), e);
		}
		return result;

	}

	public ClientWrapper<JobClient, String> getJobClient() {
		return jobClient;
	}

	public Set<String> getSelectableResourceTypes() {
		return selectableResourceTypes;
	}

	public Label getStageInLabel() {
		return stageInLabel;
	}

	public StageInTableViewer getStageInTableViewer() {
		return stageInTableViewer;
	}

	public Label getStageOutLabel() {
		return stageOutLabel;
	}

	public StageOutTableViewer getStageOutTableViewer() {
		return stageOutTableViewer;
	}

	public List<IAdaptable> getTargetResources() {
		return targetResources;
	}

	public TargetSystemClient getTargetSystem() {
		return targetSystem;
	}

	/**
	 * Return number of hours until the job should terminate
	 * 
	 * @return
	 */
	public Integer getTerminationTime() {
		Integer result = getGridBeanContext().getTerminationTime();
		return result;
	}

	@Override
	public String getTitleToolTip() {
		String toolTip = super.getTitleToolTip();

		int executionState = getExecutionState();
		if (executionState != ExecutionStateConstants.STATE_UNSUBMITTED) {
			toolTip += " ("
				+ ExecutionStateConstants.getStringForState(executionState)
				+ ")";
		}

		return toolTip;
	}



	public boolean hasBeenPositioned() {
		return hasBeenPositioned;
	}

	private void hookContextMenu() {

	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
	throws PartInitException {
		setSite(site);
		setInput(input);

	}

	protected void initGridBeanModel() {
		listeningToGridBeanModel = false;
		IGridBean gridBean = null;
		try {

			gridBean = getGridBeanModel();
			if(gridBean == null)
			{
				GPEActivator.log(IStatus.ERROR,
				"Unable to load internal application GUI model.");
				return;
			}
			gridBean.removePropertyChangeListener(this);
			if (!isPartOfWorkflow()) {
				IPath withoutExtension = getEditorInput().getPath()
				.removeFileExtension();
				gridBean.set(IGridBeanModel.JOBNAME,
						withoutExtension.lastSegment());
			}
			GridBeanContext context = (GridBeanContext) gridBean
			.get(GPE4EclipseConstants.CONTEXT);
			if (context == null) {

				if (getEditorInput().getActivity() != null) {
					context = GridBeanUtils
					.createWorkflowContext(getEditorInput()
							.getActivity().getDiagram());
				} else {
					context = GridBeanUtils.createSolitaryContext();
				}
				gridBean.set(GPE4EclipseConstants.CONTEXT, context);
				gridBean.getInputParameters().add(
						new GridBeanParameter(GPE4EclipseConstants.CONTEXT,
								GPE4EclipseConstants.PARAMETER_TYPE_CONTEXT));
			} else {
				int executionState = ExecutionStateConstants.STATE_UNSUBMITTED;
				if (getEditorInput().getActivity() != null) {
					executionState = getEditorInput().getActivity()
					.getCurrentExecutionStateCipher();
				} else if (context.getJobExecutionState() > ExecutionStateConstants.STATE_UNSUBMITTED) {
					executionState = context.getJobExecutionState();
				}

				setExecutionState(executionState);

				if (context.getJobClientAddress() != null) {
					continueMonitoring(context.getJobClientAddress());
				}
				if (context.isPartOfWorkflow()
						&& getEditorInput().getActivity() == null) {
					// This job was part of a workflow but the workflow cannot
					// be found anymore!
					context = GridBeanUtils.createSolitaryContext();
					gridBean.set(GPE4EclipseConstants.CONTEXT, context);
				}

			}
			gridBean.addPropertyChangeListener(this);
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Unable to initialize job context properly.", e);
		}

		try {
			ClassLoader cl = gridBean.getClass().getClassLoader();

			JAXBContext jaxbContext = JAXBContextProvider.getContext(cl);
			Unmarshaller m = jaxbContext.createUnmarshaller();
			File inputFile = getInputFile();
			if(inputFile == null) return;
			GridBeanJobWrapper<?> wrapper = (GridBeanJobWrapper<?>) m
			.unmarshal(inputFile);
			wrapper.setGridBeanJob(getGridBeanClient().getGridBeanJob());

			currentJob = new GPEJobImpl(wrapper.getJSDLJob());
			// the job name might have changed compared to the saved job
			// e.g. due to a rename in the workflow editor
			String jobName = (String) getGridBeanModel().get(
					IGridBeanModel.JOBNAME);
			if (jobName != null) {
				currentJob.setName(jobName);
			}
			gridBean.parseJobDefinition(currentJob);
		} catch (Exception e) {
			// do nothing
		}

		listeningToGridBeanModel = true;
	}

	protected void initModelAndPanels() {
		loading = true;
		File file = getInputFile();
		try {
			if (file != null && getGridBeanClient().getGridBeanJob() == null) {
				if (!file.exists()) {
					GPEActivator.log(IStatus.ERROR,
							"Cannot load job from file: File " + file
							+ " does not exist.");
					loading = false;
				} else if (!file.canRead()) {
					GPEActivator.log(IStatus.ERROR,
							"Cannot load job from file: File " + file
							+ " cannot be read.");
					loading = false;
				} else {
					doLoad();
				}
			} else {
				initGridBeanModel();
				initPanels();
			}

		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR, "Unable to open job editor", e);
			loading = false;
		}
	}

	protected void initPanels() {

		IGridBeanPlugin<?> plugin = getGridBeanClient().getGridBeanPlugin();
		if (plugin == null) {
			return;
		}
		setPanels(plugin.getInputPanels());

		IGridBean model = null;
		try {
			model = getGridBeanClient().getGridBeanJob().getModel();
		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR,
					"Could not retrieve application GUI gridbean.", e);
			loading = false;
			return;
		}
		final IGridBean gridBean = model;

		// do some stuff that depends on whether the job is part of a workflow
		GridBeanContext context = (GridBeanContext) gridBean
		.get(GPE4EclipseConstants.CONTEXT);
		boolean partOfWorkflow = context != null && context.isPartOfWorkflow();

		StageInList stageIns;
		StageOutList stageOuts;

		if (partOfWorkflow) {
			//			setSelectableResourceTypes(DefaultFilterSet.TYPES_TARGET_SYSTEM_FACTORIES);
			GridBeanActivity activity = (GridBeanActivity) getEditorInput()
			.getAdapter(GridBeanActivity.class);
			stageIns = StageListFactory.createStageInList(activity);
			stageOuts = StageListFactory.createStageOutList(activity);
			envVariableList = EnvVariableListFactory
			.createEnvVariableList(activity);
		} else {
			//			setSelectableResourceTypes(DefaultFilterSet.TYPES_TARGET_SYSTEMS);
			IProject p = getInputFileProject();
			IPath inputFilePath = null;
			if (p != null) {
				inputFilePath = GPEPathUtils.inputFileDirForProject(p);
			} else {
				try {
				inputFilePath = new Path(Files.createTempDirectory("urc-input-files-").toString());
				GPEActivator.log(
						IStatus.WARNING,
						"Unable to determine project for job file "
						+ getEditorInput().getPath().toOSString()
						+ ". Using directory "
						+ inputFilePath.toOSString()
						+ " for job input files.");
				} catch (IOException e) {
					GPEActivator
					.log(IStatus.WARNING,
							"Unable to determine project for job file "
							+ getEditorInput().getPath()
							.toOSString()
							+ ". No directory available for job input files.");
				}
			}

			/**
			 * needed to refer to it in getAdapter() below
			 */
			final IPath finalInputFilePath = inputFilePath;
			stageIns = StageListFactory.createStageInList(gridBean,
					new IAdaptable() {

				public Object getAdapter(Class adapter) {
					if (InputFileContainerAdapter.class.equals(adapter)) {
						// make sure that the local input file cell
						// editor
						// knows were input files for the job are found
						// by default
						return new InputFileContainerAdapter(
								finalInputFilePath);
					} else {
						return null;
					}
				}
			});
			stageOuts = StageListFactory.createStageOutList(gridBean);
			envVariableList = EnvVariableListFactory
			.createEnvVariableList(gridBean);
		}

		addDataStageingPanels(stageIns, stageOuts, null);

		addEnvVariablePanel(envVariableList, null);

		// this must be done BEFORE the resource selection panel is added, cause
		// the resource selection panel
		// might change the RESOURCES parameter value which in turn triggers a
		// store on the input panels
		// which in turn writes bad (empty) values to the GridBean model
		// => fill the controls with reasonable values here
		getGridBeanClient().reloadInputPanel(null);

		addResourceSelectionPanel();

		updatePartName();
		checkActionsEnabled();

		tabFolder.setSelection(0);

		try {
			getGridBeanClient().openGridBeanInputPanel(new IObserver() {

				public void observableUpdate(Object theObserved,
						Object changeCode) {

					try {
						if(changeCode instanceof Exception)
						{
							throw (Exception) changeCode; 
						}
						((SWTGridBeanInputPanel) getGridBeanClient()
								.getGridBeanInputPanel())
								.setComponent(JobEditor.this);
						initResourcePanel(gridBean);
						checkGenericTabsEnabled();
						listeningToGridBeanModel = true;
					} catch (Exception e) {
						GPEActivator.log(Status.ERROR, "Error while opening application GUI: "+e.getMessage(), e);
					} finally {
						loading = false;
					}
				}
			});

		} catch (Exception e) {
			GPEActivator.log(IStatus.ERROR, "Unable to open job input panels",
					e);
			loading = false;
		}

	}

	protected void initResourcePanel(final IGridBean gridBean) {

		Job j = new BackgroundJob("initializing resource panel") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {

					listeningToGridBeanModel = false;

					ResourcesParameterValue resources = GridBeanUtils
					.extractResourcesParameterValue(gridBean);
					List<String> persistedCandidates = resources.getCandidateHosts();
					final int numPersistedTargetSystems = persistedCandidates == null ? 0
							: persistedCandidates.size();
					final List<IAdaptable> targetResources = GridBeanUtils
					.extractSelectedTargetHosts(gridBean);
					final int numFoundTargetSystems = targetResources.size();

					setTargetResources(targetResources);

					if (resourceSelectionPanel == null) {
						return Status.CANCEL_STATUS; // happens when editor is
						// closed before this
						// gets executed
					}

					final GridNode gridNode = resourceSelectionPanel
					.cloneGridNode();
					// gridNode.setName("Unspecified");
					listeningToGridBeanModel = true;

					PlatformUI.getWorkbench().getDisplay()
					.asyncExec(new Runnable() {
						public void run() {
							listeningToGridBeanModel = false;
							if (resourceSelectionPanel != null) {
								resourceSelectionPanel
								.setGridNode(gridNode);
								resourceSelectionPanel.
								setInterestingTypes(
										getSelectableResourceTypes());
								resourceSelectionPanel
								.setSelectedNodes(targetResources);
								resourceSelectionPanel.getTreeViewer()
								.setExpandedLevels(2);
								resourceSelectionPanel
								.setListener(JobEditor.this);

								if (numPersistedTargetSystems > numFoundTargetSystems) {
									int missing = numPersistedTargetSystems
									- numFoundTargetSystems;
									String plural = missing > 1 ? "s"
											: "";
									GPEActivator
									.log(IStatus.ERROR,
											"Found "
											+ missing
											+ " unavailable selected target system"
											+ plural
											+ " for saved job "
											+ getPartName());
								}
								matchmakingResourceFilter.init();
								resourceSelectionPanel
								.getFilterController()
								.addFilter(
										matchmakingResourceFilter);
								matchmakingResourceFilter
								.scheduleUpdate(null);

							}
							if (resourceRequirementsViewer != null) {
								resourceRequirementsViewer
								.setGridBeanModel(gridBean);
							}
							checkGenericTabsEnabled();
							listeningToGridBeanModel = true;



							boolean foundInvalidTargetSystems = numPersistedTargetSystems > numFoundTargetSystems;
							if(foundInvalidTargetSystems)
							{
								setDirty(true);
							}
							else
							{
								GridBeanContext context = (GridBeanContext) gridBean
								.get(GPE4EclipseConstants.CONTEXT);
								if(context != null && context.isNewlyCreated())
								{
									saveAfterFirstLoad();
								}
							}
						}
					});

				} catch (Exception e) {
					GPEActivator.log(IStatus.ERROR,
							"Unable to init the resource panel", e);
				}
				return Status.OK_STATUS;
			}
		};
		j.schedule();
	}

	@Override
	public boolean isDirty() {
		return getExecutionState() <= ExecutionStateConstants.STATE_UNSUBMITTED
		&& dirty;
	}

	public boolean isDisposed() {
		return disposed;
	}

	public boolean isLoading() {
		return loading;
	}

	private boolean isPartOfWorkflow() {
		try {
			GridBeanContext context = (GridBeanContext) getGridBeanClient()
			.getGridBeanJob().getModel()
			.get(GPE4EclipseConstants.CONTEXT);
			return context != null && context.isPartOfWorkflow();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * SaveAs is not allowed for {@link JobEditor}s
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	public boolean isShowingDataStaging() {
		return showingDataStaging;
	}

	public boolean isShowingResourceParameterPanel() {
		return showingResourceParameterPanel;
	}

	public boolean isSubmittingJob() {
		return submittingJob;
	}

	protected void processParameters(com.intel.gpe.clients.api.Job job,
			int processStep, List<IGridBeanParameter> inputParameters,
			List<IGridBeanParameterValue> values,
			Map<String, Object> processorParams, IProgressListener progress)
	throws Exception {
		if (job instanceof GPEJob) {
			GPEJob gpeJob = (GPEJob) job;
			ParameterTypeProcessorRegistry.getDefaultRegistry()
			.processParameters(getGridBeanClient().getClient(), gpeJob,
					processStep, inputParameters, values,
					processorParams, progress);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		IGridBean gridBean = getGridBeanModel();
		if (!listeningToGridBeanModel || gridBean == null) {
			return;
		}
		if (GPE4EclipseConstants.CONTEXT.toString().equals(
				evt.getPropertyName())) {
			GridBeanContext context = (GridBeanContext) evt.getNewValue();
			int newState = context.getJobExecutionState();
			int oldState = getExecutionState();
			boolean editable = newState == ExecutionStateConstants.STATE_UNSUBMITTED;
			boolean oldEditable = oldState == ExecutionStateConstants.STATE_UNSUBMITTED;
			if (editable != oldEditable) {
				getGridBeanClient().reloadInputPanel(null);
			}
			// don't use setter as this would trigger
			// another modification of the GridBean
			executionState = newState; 
			updatePartName();
			checkActionsEnabled();
		}
		if(disposed) 
		{
			// must react immediately
			updateCurrentJob(null);
		}
		else scheduleCurrentJobUpdate();

	}

	protected void reloadPlugin() {
		try {
			IGridBeanPlugin<?> plugin = getGridBeanClient().getGridBeanPlugin();
			IGenericGridBeanPanel[] components = plugin.getInputPanels();
			if (components != null) {
				setPanels(components);
			}
		} catch (Exception e1) {
			GPEActivator.log("Application plugin could not be retrieved.", e1);
		}

		updatePartName();
		checkActionsEnabled();
	}

	private void saveAfterFirstLoad()
	{
		Job j = new BackgroundJob("saving job")
		{

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				IGridBean gridBean = getGridBeanModel();
				if(gridBean != null)
				{
					GridBeanContext context = (GridBeanContext) gridBean
					.get(GPE4EclipseConstants.CONTEXT);
					if (context != null && context.isNewlyCreated()) {
						context.setNewlyCreated(false);
						doActualSave(getInputFile(), monitor);
					}
				}
				return Status.OK_STATUS; 
			}

		};
		j.schedule();
	}

	public void scheduleCurrentJobUpdate() {
		// try to find out whether we should mark the editor dirty
		if (jobUpdater == null) {
			jobUpdater = new BackgroundJob("Updating resulting job description") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					updateCurrentJob(monitor);
					return Status.OK_STATUS;
				}
			};
			jobUpdater.setUser(false);
		}
		if (jobUpdater.getState() == Job.NONE) {
			jobUpdater.schedule(300);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(
	 * org.eclipse.jface.viewers.SelectionChangedEvent)
	 */
	public void selectionChanged(SelectionChangedEvent event) {
		final IStructuredSelection selection = (IStructuredSelection) event
		.getSelection();
		Job j = new BackgroundJob("updating selected resources") {
			public IStatus run(IProgressMonitor monitor) {
				setTargetResources((List<IAdaptable>) selection.toList());
				return Status.OK_STATUS;

			}
		};
		j.schedule();
	}

	public void setDirty(final boolean dirty) {
		if (dirty == this.dirty || loading) {
			return;
		}
		IGridBean gridBean = getGridBeanModel();
		if(gridBean != null)
		{
			GridBeanContext context = (GridBeanContext) gridBean
			.get(GPE4EclipseConstants.CONTEXT);
			if (context != null && context.isNewlyCreated()) {
				return;
			}
		}

		this.dirty = dirty;

		Display display = PlatformUI.getWorkbench().getDisplay();
		if(disposed && dirty && Thread.currentThread().equals(display.getThread()))
		{

			// this is happening WHILE we're being disposed
			// => without this, user wouldn't be asked to save
			IWorkbenchWindow window = getSite().getWorkbenchWindow();

			if(window != null && getExecutionState() < ExecutionStateConstants.STATE_SUBMITTING)
			{
				SaveablesList saveableList = (SaveablesList) window.getService(ISaveablesLifecycleListener.class);
				List<Saveable> saveables = new ArrayList<Saveable>(Arrays.asList(getSaveables()));
				saveableList.promptForSaving(saveables, window, window, true, false);
			}
		}
		else
		{
			display.asyncExec(new Runnable() {
				public void run() {
					if (getExecutionState() <= ExecutionStateConstants.STATE_UNSUBMITTED) {
						firePropertyChange(IEditorPart.PROP_DIRTY);
					}
				}
			});
		}
	}

	public void setExecutionState(int executionState) {
		int oldExecutionState = this.executionState;
		IGridBean gb = getGridBeanModel();
		if (oldExecutionState == executionState) {
			return;
		}
		this.executionState = executionState;

		if (gb != null) {
			GridBeanContext context;
			context = getGridBeanContext().clone();
			listeningToGridBeanModel = false;
			context.setJobExecutionState(executionState);
			boolean editable = executionState == ExecutionStateConstants.STATE_UNSUBMITTED;
			context.setEditable(editable);
			gb.set(GPE4EclipseConstants.CONTEXT, context);
			boolean oldEditable = oldExecutionState == ExecutionStateConstants.STATE_UNSUBMITTED;
			if (editable != oldEditable) {
				getGridBeanClient().reloadInputPanel(null);
			}
			listeningToGridBeanModel = true;
			scheduleCurrentJobUpdate();

		}
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {

			public void run() {
				updatePartName();
				checkActionsEnabled();
				checkGenericTabsEnabled();
			}
		});

	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		tabFolder.setFocus();
	}

	public void setGridBeanClient(GridBeanClient<IWorkbenchPart> gridBeanClient) {

		if (this.gridBeanClient != null) {
			try {
				this.gridBeanClient.getGridBeanJob().getModel()
				.removePropertyChangeListener(this);
			} catch (Exception e) {
			}
		}
		this.gridBeanClient = gridBeanClient;
		setTempDir();
		reloadPlugin();
		try {
			getGridBeanClient().getGridBeanJob().getModel()
			.addPropertyChangeListener(this);
		} catch (Exception e) {
			GPEActivator.log(Status.ERROR, "Error while subscribing to application GUI model changes: "+e.getMessage(), e);
		}
	}

	public void setHasBeenPositioned(boolean hasBeenPositioned) {
		this.hasBeenPositioned = hasBeenPositioned;
	}

	public void setInput(IEditorInput input) {
		if (!(input instanceof JobEditorInput)) {
			// replace the input with a JobEditorInput in order to use the
			// correct
			// equals implementation
			IPath jobPath = null;
			IFile f = (IFile) input.getAdapter(IFile.class);
			if(f == null && input instanceof IURIEditorInput)
			{
				IURIEditorInput uriInput = (IURIEditorInput) input;
				URI uri = uriInput.getURI();
				IFile[] files = ResourcesPlugin.getWorkspace().getRoot().findFilesForLocationURI(uri);
				if(files != null && files.length == 1) f = files[0];
			}
			if(f != null)
			{
				jobPath = f.getLocation();
			}


			if(jobPath != null) {
				String activityName = null;
				IPath workflowPath = GPEPathUtils
				.extractWorkflowPathFromJobPath(jobPath);
				if (workflowPath != null) {
					// has parent workflow => set activityName correctly
					activityName = GPEPathUtils.extractJobNameFromJobPath(jobPath);
				}
				JobEditorInput newInput = new JobEditorInput(jobPath, activityName);

				input = newInput;
			}
			else 
			{
				return;
			}
		}
		super.setInput(input);
		if (getEditorInput().wasInvalidWhilePersisting()) {
			getSite().getPage().closeEditor(this, false);
		} else {
			getEditorInput().init(getSite().getPage());
		}

		if (tabFolder != null) {
			initModelAndPanels();
		}
	}

	public void setJobClient(ClientWrapper<JobClient, String> jobClient) {
		if (this.jobClient == null
				&& jobClient != null
				&& (getExecutionState() <= ExecutionStateConstants.STATE_RUNNING || getExecutionState() == ExecutionStateConstants.STATE_UNKNOWN)) {
			this.jobClient = jobClient;
			JobMonitoringService.startMonitoring(this);
		} else {
			this.jobClient = jobClient;
		}
		if (getGridBeanModel() != null) {
			GridBeanContext context;

			context = getGridBeanContext().clone();
			listeningToGridBeanModel = false;
			context.setJobClientAddress(jobClient.getClient().getFullAddress());
			getGridBeanModel().set(GPE4EclipseConstants.CONTEXT, context);
			listeningToGridBeanModel = true;
			scheduleCurrentJobUpdate();

		}
		checkFetchOutcomeActionEnabled();
	}

	public void setListeningToGridBeanModel(boolean listeningToGridBeanModel) {
		this.listeningToGridBeanModel = listeningToGridBeanModel;
	}

	public void setPanels(IPanel[] panels) {
		if (panels == null) {
			panels = new IPanel[0];
		}
		List<IPanel> newPanels = Arrays.asList(panels);
		if (gridBeanPanels != null) {
			// remove old panels first
			for (IPanel oldPanel : gridBeanPanels) {
				if (!newPanels.contains(oldPanel)) {
					oldPanel.dispose();
					gridBeanPanelControls.remove(oldPanel).dispose();
				}
			}
		}

		gridBeanPanels = newPanels;
		int i = 0;
		for (Iterator<IPanel> iterator = newPanels.iterator(); iterator
		.hasNext();) {
			IPanel<?> panel = iterator.next();
			if (!gridBeanPanelControls.containsKey(panel)) {
				Control control = GridBeanUtils.addGridBeanPanel(tabFolder,
						panel);
				if (control != null) {
					TabItem tabItem = new TabItem(tabFolder, SWT.NONE, i);
					tabItem.setText(panel.getName());
					tabItem.setControl(control);
					gridBeanPanelControls.put(panel, tabItem);
					i++;

				} else {
					iterator.remove();
					gridBeanPanelControls.remove(panel);
				}
			} else {
				i++;
			}
		}

	}

	public void setSelectableResourceTypes(Set<String> selectableResourceTypes) {
		this.selectableResourceTypes = selectableResourceTypes;
	}

	public void setShowingDataStaging(boolean showingDataStageing) {
		this.showingDataStaging = showingDataStageing;
	}

	public void setSubmittingJob(boolean runningJob) {
		this.submittingJob = runningJob;
	}

	public void setTargetResources(List<IAdaptable> targetRes) {
		targetSystem = null;
		if(targetRes == null || targetRes.size() == 0) {
			targetRes = new ArrayList<IAdaptable>();
			targetRes.add(ServiceBrowserActivator
					.getDefault().getGridNode());
		}
		this.targetResources = targetRes;
		final List<String> hosts = new ArrayList<String>();
		for (IAdaptable adaptable : targetResources) {
			try {
				Object adapter = adaptable
				.getAdapter(TargetSystemClient.class);
				if (adapter != null) {
					TargetSystemClient client = (TargetSystemClient) adapter;
					if (targetResources.size() == 1) {
						if(targetSystem instanceof IMutableClient)
						{
							((IMutableClient) targetSystem).removeChangeListener(this);
						}
						targetSystem = client;
						if(targetSystem instanceof IMutableClient)
						{
							((IMutableClient) targetSystem).addChangeListener(this);
						}
					}
					hosts.add(client.getFullAddress());
				} else {
					// if a single selected resource is not a target system,
					// assume an invalid selection and set an empty
					// candidate host list
					hosts.clear();

					break;
				}

			} catch (Exception e) {
				GPEActivator.log(Status.ERROR,
						"Cannot store selected target system.", e);
			}

			if (resourceRequirementsViewer != null) {
				PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
					public void run() {
						listeningToGridBeanModel = false;
						RequirementContentProvider prov = null;
						try
						{
							prov = (RequirementContentProvider) resourceRequirementsViewer
							.getContentProvider();
							prov.setListeningToGridBeanModel(false);
							ResourcesParameterValue resources = createAndInitRequirementList();

							resources.setCandidateHosts(hosts);
							getGridBeanClient().getGridBeanJob().getModel()
							.set(IGridBean.RESOURCES, resources);	

							scheduleCurrentJobUpdate();

						} catch (Exception e) {
							GPEActivator.log(Status.ERROR,
									"Cannot store selected target systems.", e);
						} finally
						{
							listeningToGridBeanModel = true;
							if(prov != null) prov.setListeningToGridBeanModel(true);
						}

						checkSubmitActionEnabled();
					}
				});
			}
		}

	}

	protected void setTempDir() {
		GPEFileFactory fileFactory = gridBeanClient.getClient()
		.getFileFactory();
		IProject p = getInputFileProject();
		if (p != null) {
			String tempDir = GPEPathUtils.inputFileDirForProject(p)
			.toOSString() + File.separator;
			fileFactory.setTemporaryDirName(tempDir);
		}
	}

	protected void setTerminationTime() {
		Integer terminationTime = getTerminationTime();
		if (terminationTime == null) {
			terminationTime = GPEActivator.getDefault().getTerminationTime();
		}
		Shell s = null;
		try {
			s = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		} catch (Exception e) {

		}
		if (s == null) {
			s = new Shell();
		}
		SetRelativeTerminationTimeDialog d = new SetRelativeTerminationTimeDialog(
				s, "Set Termination Time",
				"Enter the lifetime interval for your job:", terminationTime);
		d.open();
		if (d.getReturnCode() == InputDialog.OK) {
			terminationTime = new Integer(d.getTerminationTime());
			GridBeanContext newContext = getGridBeanContext().clone();
			newContext.setTerminationTime(terminationTime);
			listeningToGridBeanModel = false;
			getGridBeanModel().set(GPE4EclipseConstants.CONTEXT, newContext);
			listeningToGridBeanModel = true;
			scheduleCurrentJobUpdate();
		}
	}

	private void updateCurrentJob(IProgressMonitor monitor)
	{
		GPEJobImpl oldJob = currentJob;
		try {

			currentJob = createCurrentJob(monitor);

		} catch (Exception e) {
			currentJob = null;
		}
		if (oldJob == null || currentJob == null) {
			setDirty(true);
		} else {
			if (!GridBeanUtils.sameJob(oldJob, currentJob)) {
				setDirty(true);
			}
		}
	}

	public void updatePartName() {
		String partName = "";
		try {
			partName += (String) getGridBeanModel().get(IGridBean.JOBNAME);
		} catch (Exception e1) {
			partName += getGridBeanClient().getInternalGridBean().getName();
		}
		int executionState = getExecutionState();
		if (executionState != ExecutionStateConstants.STATE_UNSUBMITTED) {
			partName += " ("
				+ ExecutionStateConstants.getStringForState(executionState)
				+ ")";
		}

		final String name = partName;
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				setPartName(name);
			}
		});
	}

	@Override
	public Saveable[] getSaveables() {
		if(saveables == null)
		{
			saveables = new Saveable[] { new DefaultSaveable(this) };
		}
		return saveables;
	}

	@Override
	public Saveable[] getActiveSaveables() {
		if(saveables == null)
		{
			saveables = new Saveable[] { new DefaultSaveable(this) };
		}
		return saveables;
	}

}