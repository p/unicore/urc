/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.resources;

import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ArgumentType;

/**
 * @author sholl
 * 
 *         Represents a slim execution environment argument. It only stores the
 *         value of this argument, if it was modified by the user. Otherwise it
 *         can be recreated from the simpleIDB
 * 
 */
public class ExecEnvValue<T> {

	private T value;
	private boolean enabled = false;
	private ArgumentType type;

	public ExecEnvValue() {

	}

	public ExecEnvValue(T argumentValue) {
		setValue(argumentValue);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ExecEnvValue)) {
			return false;
		}
		ExecEnvValue<?> other = (ExecEnvValue<?>) o;
		return CollectionUtil.equalOrBothNull(getValue(), other.getValue());

	}

	public ArgumentType getType() {
		return type;
	}

	public T getValue() {
		return value;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setType(ArgumentType type) {
		this.type = type;
	}

	public void setValue(T value) {
		this.value = value;
	}

}
