/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.swt.widgets.TableItem;

import com.intel.gpe.clients.api.jsdl.JSDLJob;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageTypeRegistry;

/**
 * @author demuth
 * 
 */
public class StageInTableCellModifier implements ICellModifier {

	private StageInTableViewer stageInTable;

	public StageInTableCellModifier(StageInTableViewer stageInTable) {
		this.stageInTable = stageInTable;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object,
	 * java.lang.String)
	 */
	public boolean canModify(Object element, String property) {
		if (!(element instanceof IStageIn)) {
			return false;
		}
		IStageIn stage = (IStageIn) element;
		if (StageInTableViewer.ID.equals(property)) {
			return !stage.isDefinedInGridBean()
					&& stageInTable.getStageInList().getFileSets().size() > 1;
		} else if (StageInTableViewer.SOURCE_FILE.equals(property)) {

			int column = StageInTableViewer.getColumnFor(property);
			CellEditor editor = resolveCellEditor(stage);
			if (editor == null) {
				return false;
			}
			stageInTable.setCellEditor(column, editor);
			return stage.getValue().isSourceModifiable();
		} else if (StageInTableViewer.TARGET_FILE.equals(property)) {
			return stage.getValue().isTargetModifiable();
		}
		return true;
	}

	protected StageTypeRegistry getStageTypeRegistry() {
		return GPEActivator.getDefault().getStageInTypeRegistry();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object,
	 * java.lang.String)
	 */
	public Object getValue(Object element, String property) {

		IStageIn stage = (IStageIn) element;
		Object result = null;

		if (StageInTableViewer.ID.equals(property)) {
			IGridBeanParameter param = stage.getGridBeanParameter();
			result = param;
		} else if (StageInTableViewer.SOURCE_TYPE.equals(property) || StageInTableViewer.SOURCE_FILE.equals(property) || StageInTableViewer.TARGET_FILE.equals(property)){
			try {
				result = stage.getValue();
			} catch (Exception e) {
				GPEActivator.log(IStatus.WARNING,
						"Could not determine type of stage in!", e);
				return 0;
			}
		} else if (StageInTableViewer.DELETE_INPUT_FILE.equals(property)) {

			result = (stage.getValue().getFlags() & JSDLJob.FLAG_DELETE_ON_TERMINATE) != 0;
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object,
	 * java.lang.String, java.lang.Object)
	 */
	public void modify(Object element, String property, Object value) {
		if (value == null) {
			return;
		}
		if (element instanceof TableItem) {
			TableItem item = (TableItem) element;
			IStageIn stage = (IStageIn) item.getData();
			StageInList stageList = stageInTable.getStageInList();
			try {
				if (StageInTableViewer.ID.equals(property)) {
					IGridBeanParameter val = (IGridBeanParameter) value;
					stageList.reportStageValueChange(stage, val,
							stage.getValue());
				} else if (StageInTableViewer.SOURCE_TYPE.equals(property)) {

					IFileParameterValue val = (IFileParameterValue) value;
					stageList.reportStageValueChange(stage,
							stage.getGridBeanParameter(), val);

				} else if (StageInTableViewer.SOURCE_FILE.equals(property)) {

					IFileParameterValue[] values;
					if (value instanceof IFileParameterValue[]) {
						values = (IFileParameterValue[]) value;
					} else {
						values = new IFileParameterValue[] { (IFileParameterValue) value };
					}
					stageList.reportStageValueChange(stage,
							stage.getGridBeanParameter(), values[0]);
					if (values.length > 1) {
						// cell editor returned multiple values.. add them
						for (int i = 1; i < values.length; i++) {
							IStageIn current = new StageIn(
									stage.getGridBeanParameter(), values[i]);
							current.setDefinedInGridBean(stage
									.isDefinedInGridBean());
							current.setType(stage.getType());
							current.setId(values[i].getUniqueId());
							stageList.addStage(current);
						}
					}
				} else if (StageInTableViewer.TARGET_FILE.equals(property)) {
					IFileParameterValue val = (IFileParameterValue) value;
					stageList.reportStageValueChange(stage,
							stage.getGridBeanParameter(), val);
				} else if (StageInTableViewer.DELETE_INPUT_FILE.equals(property)) {
					boolean deleteOnTermination = (Boolean) value;
					IFileParameterValue val = stage.getValue().clone();
					int flags = val.getFlags();
					flags |= JSDLJob.FLAG_DELETE_ON_TERMINATE;
					if (!deleteOnTermination) flags ^= JSDLJob.FLAG_DELETE_ON_TERMINATE;
					val.setFlags(flags);
					stageList.reportStageValueChange(stage, stage.getGridBeanParameter(), val);	

				}

				// else if(StageInTableViewer.OVERWRITE.equals(property)){
				// Boolean b = (Boolean) value;
				// stage.setOverwriteFiles(b);
				// stageList.updateStageIn(stage);
				// }
				// else if(StageInTableViewer.BINARY.equals(property)){
				// Boolean b = (Boolean) value;
				// stage.setBinaryContent(b);
				// stageList.updateStageIn(stage);
				// }

			} catch (Exception e) {
				GPEActivator.log(IStatus.WARNING,"Could not change " + property
						+ ": Invalid input.", e);
			}
			stageInTable.packTable();
		}

	}

	protected CellEditor resolveCellEditor(IStage stage) {
		StageInList stageList = stageInTable.getStageInList();
		return getStageTypeRegistry().getDefiningExtension(stage.getType())
				.getCellEditor(stageInTable.getTable(), stage,
						stageList.getAdaptable());
	}

}
