/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing;

import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;

/**
 * @author demuth
 * 
 */
public class StageTypeOtherCellEditor extends TextCellEditor {

	IFileParameterValue file = null;

	/** A CellEditor, opening a text field for entering arbitrary file addresses */
	public StageTypeOtherCellEditor(Composite parent) {
		super(parent);

	}

	@Override
	protected Object doGetValue() {
		String s = (String) super.doGetValue();
		IGridFileAddress address = file.isInputParameter() ? file.getSource()
				: file.getTarget();
		int index = -1;
		int firstWildcard = s.indexOf("*");
		if (firstWildcard > 0) {
			index = s.lastIndexOf("/", firstWildcard - 1);
			if (index < 0) {
				index = 0;
			}
		} else {
			index = s.lastIndexOf("/");
		}
		if (index >= 0) {
			address.setParentDirAddress(s.substring(0, index));
			address.setRelativePath(s.substring(index));
		} else {
			address.setRelativePath(s);
		}
		address.setDisplayedString(s);
		return file;
	}

	@Override
	protected void doSetValue(Object value) {
		file = ((IFileParameterValue) value).clone();
		IGridFileAddress address = file.isInputParameter() ? file.getSource()
				: file.getTarget();
		super.doSetValue(address.getInternalString());

	}

}
