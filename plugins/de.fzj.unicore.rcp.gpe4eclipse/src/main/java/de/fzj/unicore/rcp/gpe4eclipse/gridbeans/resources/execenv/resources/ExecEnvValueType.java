/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.resources;

import java.util.List;

import com.intel.gpe.clients.api.jsdl.ResourceRequirementType;
import com.intel.gpe.util.collections.CollectionUtil;

/**
 * @author sholl
 * 
 *         Stores the Execution environment types in the gridbean value.
 * 
 */

public class ExecEnvValueType implements ResourceRequirementType {

	protected boolean enabled = true;
	protected List<ExecEnvType> execEnv;
	protected int selected;
	protected String name;

	public ExecEnvValueType() {
	}

	public ExecEnvValueType(List<ExecEnvType> execEnv, String string,
			int selected) {
		super();
		this.execEnv = execEnv;
		this.name = string;
		this.selected = selected;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ExecEnvValueType)) {
			return false;
		}
		ExecEnvValueType other = (ExecEnvValueType) o;
		return isEnabled() == other.isEnabled()
				&& CollectionUtil.equalOrBothNull(getName(), other.getName())
				&& CollectionUtil.equalOrBothNull(getSelected(),
						other.getSelected())
				&& CollectionUtil.equalOrBothNull(getExecEnv(),
						other.getExecEnv());

	}

	public List<ExecEnvType> getExecEnv() {
		return execEnv;
	}

	public String getName() {
		return name;
	}

	public int getSelected() {
		return selected;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.jsdl.ResourceRequirementType#isEnabled()
	 */
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setExecEnv(List<ExecEnvType> execEnv) {
		this.execEnv = execEnv;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSelected(int selected) {
		this.selected = selected;
	}

}
