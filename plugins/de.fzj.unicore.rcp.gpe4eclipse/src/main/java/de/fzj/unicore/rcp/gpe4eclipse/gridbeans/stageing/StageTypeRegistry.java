/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.namespace.QName;

import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint;

/**
 * @author demuth
 * 
 */
public class StageTypeRegistry {

	Map<QName, IStageTypeExtensionPoint> extensions = new ConcurrentHashMap<QName, IStageTypeExtensionPoint>();
	List<QName> types = new ArrayList<QName>();

	public List<QName> getAllTypes() {
		return types;
	}

	public IStageTypeExtensionPoint getDefaultStageType(GridBeanContext context) {
		if (context == null) {
			return getDefiningExtension(getType(0));
		}
		List<QName> types = getAllTypes();
		int maxSoFar = 0;
		IStageTypeExtensionPoint defaultType = null;
		for (QName type : types) {
			IStageTypeExtensionPoint current = getDefiningExtension(type);
			int prio = current.getPriority(context);
			if (prio > maxSoFar && current.availableInContext(context)) {
				maxSoFar = prio;
				defaultType = current;
			}
		}
		return defaultType;
	}

	public IStageTypeExtensionPoint getDefiningExtension(QName type) {
		return extensions.get(type);
	}

	public QName getType(int index) {
		return types.get(index);
	}

	public int indexOf(QName type) {
		return types.indexOf(type);
	}

	public void registerStageType(QName type, IStageTypeExtensionPoint extension) {
		if (!types.contains(type)) {
			types.add(type);
			extensions.put(type, extension);
		}
	}

	public void unregisterStageType(QName type) {
		if (types.contains(type)) {
			types.remove(type);
			extensions.remove(type);
		}
	}
}
