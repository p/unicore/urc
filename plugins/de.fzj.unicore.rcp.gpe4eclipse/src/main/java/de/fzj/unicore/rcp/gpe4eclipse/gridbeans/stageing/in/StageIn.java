/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in;

import java.io.Serializable;

import javax.xml.namespace.QName;

import com.intel.gpe.clients.api.transfers.IAddressOrValue;
import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.Stage;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;

/**
 * @author demuth
 * 
 */
public class StageIn extends Stage implements Serializable, IStageIn {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5372128485742413939L;

	public StageIn(IGridBeanParameter gridBeanParameter,
			IFileParameterValue value) {
		setId(value.getUniqueId());
		setGridBeanParameter(gridBeanParameter);
		QName type = GridBeanUtils.getType(value);
		if (type != null) {
			setValue(value);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in.IStageIn#
	 * getTargetDisplayName()
	 */
	public String getNameInWorkingDir() {
		// if wildcards are being used, the name of the file in the Uspace will
		// equal the name
		// of the source file
		return getValue().getTarget().getDisplayedString();
	}

	protected boolean isUsingWildcards() {
		IAddressOrValue source = getValue().getSource();
		if (source instanceof IGridFileAddress) {
			IGridFileAddress address = (IGridFileAddress) source;
			return address.isUsingWildcards();
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageIn#setTargetValue
	 * (com.intel.gpe.gridbeans.IFileParameterValue)
	 */
	@Override
	public void setValue(IFileParameterValue value) {
		super.setValue(value);
		if (value != null && GridBeanUtils.getType(value) != null) {
			setType(GridBeanUtils.getType(value));
		}
	}

}
