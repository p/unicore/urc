package de.fzj.unicore.rcp.gpe4eclipse.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.wfeditor.WFActivator;

public class GPEPathUtils {

	public static String extractJobNameFromJobPath(IPath jobPath) {
		String fileName = jobPath.lastSegment();
		return fileName.substring(0, fileName.length()
				- jobPath.getFileExtension().length() - 1);
	}

	/**
	 * Tries to extract an absolute workflow path from an absolute job path
	 * 
	 * @param jobPath
	 * @return
	 */
	public static IPath extractWorkflowPathFromJobPath(IPath jobPath) {
		if (jobPath == null) {
			return null;
		}
		IProject[] projects = ResourcesPlugin.getWorkspace().getRoot()
				.getProjects();
		final Set<String> workflowExtensions = WFActivator.getDefault()
				.getDiagramTypeRegistry().getFileNameExtensions();
		for (IProject project : projects) {
			IPath p = project.getLocation();
			if (p.isPrefixOf(jobPath)) {
				IPath workflowParent = jobPath.removeLastSegments(2);
				File workflowParentFile = workflowParent.toFile();
				if (workflowParentFile.isDirectory()) {
					File[] workflows = workflowParentFile
							.listFiles(new FilenameFilter() {
								public boolean accept(File dir, String name) {
									int index = name.lastIndexOf(".");
									if (index < 1) {
										return false;
									}
									String extension = name
											.substring(index + 1);
									return workflowExtensions
											.contains(extension);
								}
							});
					if (workflows.length > 1) {
						GPEActivator
								.log(IStatus.ERROR,
										"Parent workflow for job "
												+ jobPath.toOSString()
												+ " could not be determined: The parent folder contains multiple workflows!");
					} else if (workflows.length == 1) {
						File workflow = workflows[0];
						String workflowFilename = workflow.getName();
						IPath result = workflowParent.append(workflowFilename);
						if (workflow.exists() && workflow.canRead()) {
							return result;
						} else {
							return null;
						}
					}
				}

			}
		}
		return null;
	}

	public static IPath inputFileDirForProject(IProject project) {
		return project.getLocation().append(
				GPE4EclipseConstants.DIRECTORY_INPUTS);
	}

	public static IPath outputFileDirForPath(IPath path) {
		return path.append(GPE4EclipseConstants.DIRECTORY_OUTCOMES);
	}

}
