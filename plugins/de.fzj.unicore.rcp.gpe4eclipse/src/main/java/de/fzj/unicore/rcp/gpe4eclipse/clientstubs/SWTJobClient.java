/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.clientstubs;

import java.net.URI;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.oasisOpen.docs.wsrf.bf2.BaseFaultType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.intel.gpe.clients.api.Status;
import com.intel.gpe.clients.api.StorageClient;
import com.intel.gpe.clients.api.exceptions.GPEInvalidResourcePropertyQNameException;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareRemoteException;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareServiceException;
import com.intel.gpe.clients.api.exceptions.GPEResourceUnknownException;
import com.intel.gpe.clients.api.exceptions.GPESecurityException;
import com.intel.gpe.clients.api.exceptions.GPEUnmarshallingException;
import com.intel.gpe.clients.impl.WSRFClientImpl;
import com.intel.gpe.clients.impl.exception.GPEFaultWrapper;
import com.intel.gpe.clients.impl.jms.JobClientImpl;
import com.intel.gpe.clients.impl.jms.StatusImpl;
import com.intel.gpe.clients.impl.sms.StorageClientImpl;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.JobNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;

/**
 * @author demuth
 * 
 */
public class SWTJobClient extends JobClientImpl {

	protected NodePath nodePath;

	/**
	 * @param client
	 */
	public SWTJobClient(NodePath nodePath) {
		super(nodePath == null ? null : GPEActivator.getDefault().getClient().getWSRFClientFactory()
				.createWSRFClient(nodePath));
		this.nodePath = nodePath;
	}

	@Override
	public String getFullAddress() {
		return nodePath.toString();
	}

	@Override
	public String getName() {
		Node n = NodeFactory.getNodeFor(nodePath);
		if (n != null) {
			return n.getNamePathToRoot();
		} else {
			return getURL();
		}

	}

	protected Node getNodeReady(NodePath path, IProgressMonitor progress) {
		return NodeFactory.revealNode(path, progress);
	}

	@Override
	public Status getStatus() throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {

		JobNode jobNode = (JobNode) getNodeReady(nodePath, null);
		if (jobNode == null) {
			throw new GPEMiddlewareServiceException(
					"Could not find job with address "
							+ nodePath.last().toString(), new Exception(), this);
		}

		Status status = null;
		try {
			status = new StatusImpl(jobNode.getJobStatus());
		} catch (Exception e) {
			// do nothing
		}
		if (status == null || !(status.isFailed() || status.isSuccessful())) {
			IStatus refreshResult = jobNode.refresh(1, false, null);
			if (refreshResult.getCode() == Node.STATE_FAILED) {
				if (jobNode.getFailReason() != null
						&& jobNode.getFailReason().contains("not found")) {
					BaseFaultType fault = BaseFaultType.Factory.newInstance();
					fault.addNewDescription().setStringValue(
							jobNode.getFailReason());
					throw new GPEResourceUnknownException(
							jobNode.getFailReason(),
							new GPEFaultWrapper(fault), this);
				} else {
					throw new GPEMiddlewareRemoteException(
							jobNode.getFailReason(), null, this);
				}
			}
			status = new StatusImpl(jobNode.getJobStatus());
		}
		return status;
	}

	@Override
	public StorageClient getWorkingDirectory()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {

		try {
			JobNode jobNode = (JobNode) getNodeReady(nodePath, null);
			if (jobNode == null) {
				throw new GPEMiddlewareServiceException(
						"Could not find job with address "
								+ nodePath.last().toString(), new Exception(),
						this);
			}

			if (!jobNode.hasChildren()) {
				jobNode.refresh(1, true, null);
			}

			EndpointReferenceType storageEpr = getUasClient().getUspaceClient()
					.getEPR();
			URI storageURI = new URI(storageEpr.getAddress().getStringValue());
			WSRFClientImpl inner = GPEActivator.getDefault().getClient()
					.getWSRFClientFactory()
					.createWSRFClient(nodePath.append(storageURI));
			return new StorageClientImpl(inner);

		} catch (Exception e) {
			throw new GPEMiddlewareRemoteException(
					"Unable to get working directory of job " + getName(), e,
					this);
		}

	}

	@Override
	public String toString() {
		try {
			return getName();
		} catch (Exception e) {
			return getURL();
		}
	}
}
