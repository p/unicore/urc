/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ui;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.clients.api.apps.ArgumentMetaData;
import com.intel.gpe.clients.api.apps.ParameterMetaData;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnv;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnvArgument;

/**
 * @author sholl
 * 
 */
public class ExecEnvDetailsViewer extends AbstractExecEnvSWTGridContentPage {

	public ExecEnvDetailsViewer(Composite parent, ExecEnv selectedExecEnv,
			ExecEnvViewer panel) {

		super(parent, selectedExecEnv,
				selectedExecEnv.getName().getLocalPart(), panel);

	}

	@Override
	public void createControls() {

		super.createControls();

		ArrayList<ParameterMetaData<?>> list = new ArrayList<ParameterMetaData<?>>();
		List<ExecEnvArgument<?>> arguments = getModel().getArguments();
		List<Boolean> checkboxes = new ArrayList<Boolean>(arguments.size());
		List<String> values = new ArrayList<String>(arguments.size());
		for (ExecEnvArgument<?> execEnvArgument : arguments) {
			ArgumentMetaData<?> metaData = execEnvArgument.getMetaData();

			checkboxes.add(true);
			values.add(execEnvArgument.getHumanReadableArgumentValue());
			list.add(metaData);
		}
		createRowsViaMetaData(list, values, checkboxes);

		refreshGridLayout();
	}

	private void updateValue(QName qName, Boolean arg) {

		List<ExecEnvArgument<?>> arguments = getModel().getArguments();

		for (ExecEnvArgument<?> execEnvArgument : arguments) {
			if (execEnvArgument.getName().equals(qName)) {
				ExecEnvArgument<? extends Object> e = execEnvArgument.clone();
				e.setEnabled(arg);
				getModel().setArgument(qName, e);
			}
		}

	}

	@Override
	public void updateValues(List<QName> enableOn, List<QName> enableOff) {
		for (QName qName : enableOn) {
			updateValue(qName, true);
		}
		for (QName qName : enableOff) {
			updateValue(qName, false);
		}

		getModel().setDirty(true);

	}

}
