package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.parameters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.TargetSystemClient;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.IParameterTypeProcessor;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;

import de.fzj.unicore.rcp.gpe4eclipse.clientstubs.GridTargetSystemClient;
import de.fzj.unicore.rcp.gpe4eclipse.clientstubs.RegistryTargetSystemClient;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;

/**
 * In GPE4Eclipse, full URL paths from the Grid node down to the nodes
 * representing the target systems are used as candidate host strings inside the
 * {@link ResourcesParameterValue}. This processor is used to replace the URL
 * paths from the candidate hosts with the URLs of the actual target systems
 * (the last URL in the node path).
 * 
 * @author bdemuth
 * 
 */
public class CorrectCandidateHostsProcessor implements IParameterTypeProcessor {

	public GridBeanParameterType getType() {
		return GridBeanParameterType.RESOURCES;
	}

	public void process(Client client, GPEJob job, int processingStep,
			IGridBeanParameter param, IGridBeanParameterValue value,
			Map<String, Object> processorParams, IProgressListener progress)
			throws Exception {
		if (!GridBeanParameterType.RESOURCES.equals(param.getType())) {
			return;
		}

		if (processingStep == ProcessingConstants.PREPARE_UPLOADS_TO_GLOBAL_STORAGES) {
			ResourcesParameterValue resources = (ResourcesParameterValue) value;
			List<String> hosts = resources.getProcessedCandidateHosts();
			if (hosts != null && hosts.size() > 0) {
				List<String> newHosts = new ArrayList<String>(hosts.size());
				if (hosts != null) {
					for (String host : hosts) {
						NodePath path = NodePath.parseString(host);
						Node n = NodeFactory.getNodeFor(path);
						if (n != null) {
							Object o = n.getAdapter(TargetSystemClient.class);
							if (o instanceof TargetSystemClient
									&& !(o instanceof GridTargetSystemClient)
									&& !(o instanceof RegistryTargetSystemClient)) {
								newHosts.add(path.last().toString());
							}

						}
					}
				}
				resources.setProcessedCandidateHosts(newHosts);
			}
		}

	}

}
