package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;

import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.IEnvironmentVariableParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;

@XStreamAlias("GPEWorkflowVariableSink")
public class GPEWorkflowVariableSink extends DataSink {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5615138242345926582L;

	public static final QName TYPE = new QName("http://www.unicore.eu/",
	"GPEWorkflowVariableSink");

	private IGridBeanParameter parameter;

	public GPEWorkflowVariableSink(IActivity activity,
			IGridBeanParameter parameter) {
		super(activity);
		this.parameter = parameter;
		setId(parameter.getName().toString());
		setName(parameter.getDisplayedName());
		IGridBean gb = (IGridBean) activity.getAdapter(IGridBean.class);
		IEnvironmentVariableParameterValue value = (IEnvironmentVariableParameterValue) gb
		.get(parameter.getName());
		if(value != null)
		{
			Class<?> targetType = value.getTargetType();
			Set<String> types = classToTypeSet(targetType);
			setPropertyValue(PROP_ACCEPTED_DATA_TYPES, types);
		}
	}

	@Override
	public int compareTo(IDataSink o) {
		int result = getSinkType().getLocalPart().compareTo(
				o.getSinkType().getLocalPart());
		if (result != 0) {
			return result;
		}
		Set<String> dataTypes = getIncomingDataTypes();
		Set<String> otherDataTypes = o.getIncomingDataTypes();
		if (dataTypes != null && dataTypes.size() == 1
				&& otherDataTypes != null && otherDataTypes.size() == 1) {
			result = dataTypes.iterator().next()
			.compareTo(otherDataTypes.iterator().next());
			if (result != 0) {
				return result;
			}
		}
		return getName().compareTo(o.getName());
	}

	public IActivity getActivity() {
		return (IActivity) super.getFlowElement();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<String> getIncomingDataTypes() {
		return (Set<String>) getPropertyValue(PROP_ACCEPTED_DATA_TYPES);
	}

	public IGridBeanParameter getParameter() {
		return parameter;
	}

	@Override
	public QName getSinkType() {
		return TYPE;
	}

	public boolean isVisible() {
		IFlowElement element = getFlowElement();
		if (element == null) {
			return false;
		}
		ShowDataFlowsProperty prop = (ShowDataFlowsProperty) element
		.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		if (prop == null || !prop.isShowingSinks()) {
			return false;
		}
		return getIncomingDataTypes().size() == 0
		|| prop.isShowingFlowsWithDataTypes(getIncomingDataTypes());
	}

	public void setParameter(IGridBeanParameter parameter) {
		this.parameter = parameter;
	}

	public static Set<String> classToTypeSet(Class<?> clazz) {
		Set<String> types = new HashSet<String>();
		if (clazz != null) {
			if (Integer.class.equals(clazz)) {
				types.add(WorkflowVariable.VARIABLE_TYPE_INTEGER);
			} else if (Double.class.equals(clazz)) {
				types.add(WorkflowVariable.VARIABLE_TYPE_FLOAT);
			} else if (Float.class.equals(clazz)) {
				types.add(WorkflowVariable.VARIABLE_TYPE_FLOAT);
			}

		}
		if (types.size() == 0) {
			types.add(WorkflowVariable.VARIABLE_TYPE_STRING);
		}
		return types;
	}

	public static Class<?> typeSetToClass(Set<String> types) {
		if (types.isEmpty()) {
			return null;
		}
		String type = types.iterator().next();
		if (type != null) {
			if (WorkflowVariable.VARIABLE_TYPE_INTEGER.equals(type)) {
				return Integer.class;
			} else if (WorkflowVariable.VARIABLE_TYPE_FLOAT.equals(type)) {
				return Double.class;
			} else if (WorkflowVariable.VARIABLE_TYPE_STRING.equals(type)) {
				return String.class;
			}
		}
		return null;
	}

}
