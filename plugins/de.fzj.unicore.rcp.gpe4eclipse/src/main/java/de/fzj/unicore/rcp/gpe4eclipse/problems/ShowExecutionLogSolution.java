package de.fzj.unicore.rcp.gpe4eclipse.problems;

import java.util.List;

import org.eclipse.swt.widgets.Shell;

import com.intel.gpe.clients.all.exceptions.GPEJobFailedException;

import de.fzj.unicore.rcp.logmonitor.TextEditorDialog;
import de.fzj.unicore.rcp.logmonitor.problems.GraphicalSolution;
import de.fzj.unicore.rcp.servicebrowser.nodes.JobNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import eu.unicore.problemutil.Problem;

public class ShowExecutionLogSolution extends GraphicalSolution {

	public ShowExecutionLogSolution() {
		super("JOB_FAILED_SHOW_LOG", "JOB_FAILED",
				"Look for errors inside the job execution log...");

	}

	private String getExecutionLog(String message) {

		String nodeNamePath = message.substring(8, message.length() - 11);
		List<Node> jobNodes = NodeFactory.getNodesFor(nodeNamePath);
		if (jobNodes != null && jobNodes.size() == 1) {
			Node n = jobNodes.get(0);
			return n.getDetails().getMap().get(JobNode.DETAIL_EXECUTION_LOG);
		}
		return null;
	}

	@Override
	public boolean isAvailableFor(String message, Problem[] problems,
			String details) {
		if (message == null) {
			return false;
		}
		for (Problem p : problems) {
			if (p.getCause() instanceof GPEJobFailedException) {
				return true;
			}

		}
		String log = getExecutionLog(message);
		return log != null;
	}

	@Override
	public boolean solve(String message, Problem[] problems, String details)
			throws Exception {
		for (Problem p : problems) {
			if (p.getCause() instanceof GPEJobFailedException) {
				GPEJobFailedException e = (GPEJobFailedException) p.getCause();
				String log = e.getExecutionLog();
				Shell s = getParentShell();
				TextEditorDialog dialog = new TextEditorDialog(s,
						"Job exceution log", log);
				dialog.open();
			}
		}

		return true;
	}

}
