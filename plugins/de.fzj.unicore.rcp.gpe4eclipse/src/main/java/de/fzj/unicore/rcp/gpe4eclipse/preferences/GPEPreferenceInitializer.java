package de.fzj.unicore.rcp.gpe4eclipse.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;

/**
 * Class used to initialize default preference values.
 */
public class GPEPreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = GPEActivator.getDefault().getPreferenceStore();
		store.setDefault(GPE4EclipseConstants.P_GRIDBEAN_PATH, "applications");
		store.setDefault(GPE4EclipseConstants.P_JOB_POLLING_INTERVAL, 5);
	}

}
