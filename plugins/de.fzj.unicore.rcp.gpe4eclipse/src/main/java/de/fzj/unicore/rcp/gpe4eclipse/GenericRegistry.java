package de.fzj.unicore.rcp.gpe4eclipse;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.core.runtime.Status;

import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.common.utils.HashableWeakReference;



/**
 * Generic registry storing mapping of configurable type to a set of URIs of supporting sites.
 * This class is fully thread safe.
 * 
 * @author Valentina Huber, Bastian Demuth, K. Benedyczak
 */
public abstract class GenericRegistry<T> {

	private Map<T, Set<URI>> registry;

	Map<HashableWeakReference<IRegistryListener<T>>,Object> listeners = new ConcurrentHashMap<HashableWeakReference<IRegistryListener<T>>, Object>();

	/**
	 * Creates the GenericRegistry instance
	 * 
	 */
	protected GenericRegistry() {
		registry = new HashMap<T, Set<URI>>();
	}



	/**
	 * Returns true if the registry contains a specified entry.
	 * 
	 * @param appl
	 * @return
	 */
	public boolean containsEntry(T appl) {
		synchronized(registry)
		{
			return registry.containsKey(appl);
		}
	}

	/**
	 * Returns the list of entries in the registry. The list is copied,
	 * so its modifications do not take effect on the registry contents.
	 * 
	 * @return
	 */
	public List<T> getEntryList() {
		synchronized(registry)
		{
			List<T> entries = new ArrayList<T>();
			entries.addAll(registry.keySet());
			return entries;
		}
	}


	/**
	 * Returns the list of available entries for the specified URI.
	 * The list is copied,
	 * so its modifications do not take effect on the registry contents.
	 * @param uri
	 * @return
	 */
	public List<T> getEntries(URI uri) {
		synchronized(registry)
		{
			List<T> entries = new ArrayList<T>();
			for (T entry : registry.keySet()) {
				Set<URI> uris = getURIs(entry);
				if (uris.contains(uri)) {
					entries.add(entry);
				}
			}
			return entries;
		}
	}

	/**
	 * Returns the set of URIs for all nodes supporting the specified
	 * entry. The set is a copy of the used value.
	 * 
	 * @param entry entry
	 * @return the set of the URIs supporting the specified entry or empty set if 
	 *         no such entry exists.
	 */
	public Set<URI> getURIs(T entry) {
		synchronized(registry)
		{
			Set<URI> uris = new HashSet<URI>();
			if (registry.get(entry) != null) {
				uris.addAll(registry.get(entry));
			}
			return uris;
		}
	}

	/**
	 * Adds a uri which supports the specified entry to the registry.
	 * 
	 * @param entries
	 * @param uri
	 */
	public void add(List<T> entries, URI uri) {
		Set<T> oldEntries = null;
		List<T> oldURIEntries = null;
		synchronized(registry)
		{
			oldEntries = new HashSet<T>(
					registry.keySet());
			oldURIEntries = getEntries(uri);
			for (T entry : entries) {
				if (entry == null)
					throw new IllegalArgumentException("Element added to the registry must not be null");
				Set<URI> uris = getURIs(entry);
				uris.add(uri);
				registry.put(entry, uris);
			}
		}
		if(!CollectionUtil.containSameElements(oldEntries, getEntryList()))
		{
			fireEntryListChanged();
		}
		List<T> newURIEntries = getEntries(uri);
		if(!CollectionUtil.containSameElements(oldURIEntries, newURIEntries))
		{
			fireEntryListChanged(uri,newURIEntries);
		}
	}

	public void remove(URI uri) {
		Set<T> oldEntries = null;
		List<T> oldURIEntries = null;
		synchronized(registry)
		{
			oldEntries = new HashSet<T>(
					registry.keySet());
			oldURIEntries = getEntries(uri);
			for (T entry : oldEntries) {
				Set<URI> uris = getURIs(entry);
				uris.remove(uri);
				if (uris.size() == 0)
					registry.remove(entry);
				else
				{
					registry.put(entry, uris);
				}
			}
		}
		if(!CollectionUtil.containSameElements(oldEntries, getEntryList()))
		{
			fireEntryListChanged();
		}
		List<T> newURIEntries = getEntries(uri);
		if(!CollectionUtil.containSameElements(oldURIEntries, newURIEntries))
		{
			fireEntryListChanged(uri,newURIEntries);
		}
	}

	/**
	 * Removes a specified entry from the registry and all associated
	 * URIs.
	 * 
	 * @param entry entry
	 */
	public void removeEntry(T entry) {
		Set<T> oldEntries = null;
		Map<URI,List<T>> oldURIEntries = null;
		Set<URI> uris = null;
		synchronized(registry)
		{
			oldEntries = new HashSet<T>(
					registry.keySet());
			oldURIEntries = new HashMap<URI, List<T>>();
			uris = registry.get(entry);
			for(URI uri : uris)
			{
				oldURIEntries.put(uri,getEntries(uri));
			}
			registry.remove(entry);
		}
		if(!CollectionUtil.containSameElements(oldEntries, getEntryList()))
		{
			fireEntryListChanged();
		}
		for(URI uri : uris)
		{
			List<T> newURIEntries = getEntries(uri);
			if(!CollectionUtil.containSameElements(oldURIEntries.get(uri), newURIEntries))
			{
				fireEntryListChanged(uri,newURIEntries);
			}
		}
	}

	/**
	 * Remove the specified entries from the given URI, stating that the
	 * service identified by this URI does not support these entries anymore.
	 * 
	 * @param entries
	 * @param uri
	 */
	public void removeEntries(List<T> entries, URI uri) {
		Set<T> oldEntries = null;
		List<T> oldURIEntries = null;
		synchronized(registry)
		{
			oldEntries = new HashSet<T>(
					registry.keySet());
			oldURIEntries = getEntries(uri);
			for (T entry : entries) {
				Set<URI> uris = getURIs(entry);
				uris.remove(uri);
				if (uris.size() == 0)
					registry.remove(entry);
			}
		}
		if(!CollectionUtil.containSameElements(oldEntries, getEntryList()))
		{
			fireEntryListChanged();
		}
		List<T> newURIEntries = getEntries(uri);
		if(!CollectionUtil.containSameElements(oldURIEntries, newURIEntries))
		{
			fireEntryListChanged(uri,newURIEntries);
		}
	}

	/**
	 * 
	 * @param arg
	 * @return true if argument is a valid element to be registered
	 */
	protected abstract boolean isValid(T arg);

	/**
	 * Updates the list of entries in the registry supported by a specified uri.
	 * 
	 * @param entries
	 * @param uri
	 */
	public void updateEntries(List<T> entries, URI uri) {
		Set<T> oldEntries = null;
		List<T> oldURIEntries = null;
		synchronized(registry)
		{
			oldEntries = new HashSet<T>(
					registry.keySet());
			oldURIEntries = getEntries(uri);

			Set<T> allEntries = new HashSet<T>(oldEntries);
			for (T entry : entries) {
				if (isValid(entry)) {
					allEntries.add(entry); // only add valid entries
				}
			}
			for (T entry : allEntries) {
				Set<URI> uris = getURIs(entry);
				if (entries.contains(entry)) {
					uris.add(uri);
				} else {
					uris.remove(uri);
				}

				if (uris.size() == 0) {
					registry.remove(entry);
				} else {
					if (entry == null)
						throw new IllegalArgumentException("Element added to the registry must not be null");
					registry.put(entry, uris);
				}
			}
		}
		if(!CollectionUtil.containSameElements(oldEntries, getEntryList()))
		{
			fireEntryListChanged();
		}
		List<T> newURIEntries = getEntries(uri);
		if(!CollectionUtil.containSameElements(oldURIEntries, newURIEntries))
		{
			fireEntryListChanged(uri,newURIEntries);
		}
	}

	protected void fireEntryListChanged()
	{
		List<T> entries = getEntryList();
		for (Iterator<HashableWeakReference<IRegistryListener<T>>> iterator = listeners.keySet().iterator(); iterator.hasNext();) {
			HashableWeakReference<IRegistryListener<T>> listener = iterator.next();
			try {
				IRegistryListener<T> l = listener.get();
				if(l == null) iterator.remove();
				else l.entryListChanged(entries);
			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, "Error while notifying registry listener: "+e.getMessage(),e);
			}
		}
	}

	protected void fireEntryListChanged(URI uri,List<T> entries)
	{
		for (Iterator<HashableWeakReference<IRegistryListener<T>>> iterator = listeners.keySet().iterator(); iterator.hasNext();) {
			HashableWeakReference<IRegistryListener<T>> listener = iterator.next();
			try {
				IRegistryListener<T> l = listener.get();
				if(l == null) iterator.remove();
				else l.entriesChangedForURI(uri, entries);
			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, "Error while notifying registry listener: "+e.getMessage(),e);
			}
		}
	}


	public void addRegistryListener(IRegistryListener<T> listener)
	{
		listeners.put(new HashableWeakReference<IRegistryListener<T>>(listener),"");
	}

	public void removeRegistryListener(IRegistryListener<T> listener)
	{
		listeners.remove(listener);
	}
}
