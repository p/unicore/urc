package de.fzj.unicore.rcp.gpe4eclipse.extensions;

import org.eclipse.core.runtime.IAdaptable;

import com.intel.gpe.gridbeans.parameters.IFileParameterValue;

public abstract class AbstractStageOutTypeExtension extends
		AbstractStageTypeExtension {

	protected void checkSourceChanges(IFileParameterValue oldValue,
			IFileParameterValue newValue) {
		boolean containsWildcards = newValue.getSource().isUsingWildcards();
		newValue.getTarget().setUsingWildcards(containsWildcards);
		if (containsWildcards) {
			newValue.getTarget().setRelativePath(
					newValue.getSource().getRelativePath());
			String displayed = oldValue.getTarget().getDisplayedString();
			if (displayed != null) {
				String oldRelative = oldValue.getTarget().getRelativePath();
				String newRelative = newValue.getTarget().getRelativePath();
				if (oldRelative == null) {
					oldRelative = "";
				}
				if (newRelative == null) {
					newRelative = "";
				}
				if (oldRelative.trim().length() > 0
						&& newRelative.trim().length() > 0) {
					displayed = displayed.replace(oldRelative, newRelative);
				} else {
					displayed = newRelative;
				}
			}
			newValue.getTarget().setDisplayedString(displayed);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #updateParameterValue(org.eclipse.core.runtime.IAdaptable,
	 * com.intel.gpe.gridbeans.parameters.IFileParameterValue)
	 */
	public void updateParameterValue(IAdaptable adaptable,
			IFileParameterValue oldValue, IFileParameterValue newValue) {
		checkSourceChanges(oldValue, newValue);

	}

}
