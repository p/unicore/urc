/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.commands;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.xml.namespace.QName;

import org.eclipse.gef.commands.Command;

import com.intel.gpe.gridbeans.IGridBeanModel;

/**
 * @author demuth
 */
public class GridBeanModelChangeCommand extends Command {

	public static final String PROPAGATION_ID = GridBeanModelChangeCommand.class
			.getName();

	// instance that created this command in order to avoid an infinite loop,
	// the actor is notified of a property change seperately
	private PropertyChangeListener actor;
	private IGridBeanModel gridBeanModel;
	private PropertyChangeEvent evt;

	public GridBeanModelChangeCommand(PropertyChangeListener actor,
			IGridBeanModel gridBeanModel, PropertyChangeEvent evt) {
		this.actor = actor;
		this.gridBeanModel = gridBeanModel;
		this.evt = evt;
		evt.setPropagationId(PROPAGATION_ID);
	}

	@Override
	public boolean canUndo() {
		return true;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {

	}

	@Override
	public void redo() {
		gridBeanModel.removePropertyChangeListener(actor);
		QName qname = String2QName(evt.getPropertyName());
		gridBeanModel.set(qname, evt.getNewValue());
		actor.propertyChange(evt);
		gridBeanModel.addPropertyChangeListener(actor);
	}

	private QName String2QName(String s) {
		String[] tokens = s.split("}");
		String ns = tokens[0].substring(1);
		String localPart = tokens[1];
		return new QName(ns, localPart);
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		gridBeanModel.removePropertyChangeListener(actor);
		QName qname = String2QName(evt.getPropertyName());
		gridBeanModel.set(qname, evt.getOldValue());
		actor.propertyChange(evt);
		gridBeanModel.addPropertyChangeListener(actor);
	}

}