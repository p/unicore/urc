package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.guicomponents.BooleanPropertyDescriptor;
import de.fzj.unicore.rcp.common.guicomponents.IntegerPropertyDescriptor;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.utils.PropertyMapComparator;

@XStreamAlias("IgnoreFailureOptions")
public class IgnoreFailureOptions extends PropertySource{
	
	
	private static final long serialVersionUID = -1934944540960006774L;
	public static String PROP_IGNORE_FAILURE = "Ignore Failure";
	public static String PROP_MAX_RESUBMITS = "Maximum Number of Resubmits";
	
	public IgnoreFailureOptions() {
		setPropertyValue(PROP_IGNORE_FAILURE, false);
		setPropertyValue(PROP_MAX_RESUBMITS, 3);
	}

	@Override
	protected void addPropertyDescriptors() {
		addPropertyDescriptor(new BooleanPropertyDescriptor(
				PROP_IGNORE_FAILURE, PROP_IGNORE_FAILURE));
		addPropertyDescriptor(new IntegerPropertyDescriptor(
				PROP_MAX_RESUBMITS, PROP_MAX_RESUBMITS));
		
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof IgnoreFailureOptions)) {
			return false;
		}
		IgnoreFailureOptions other = (IgnoreFailureOptions) o;
		if (this == other) {
			return true;
		}
		return PropertyMapComparator.equal(getProperties(),
				other.getProperties());

	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_IGNORE_FAILURE);
		propertiesToCopy.add(PROP_MAX_RESUBMITS);
		return propertiesToCopy;
	}

	@Override
	public int hashCode() {
		return PropertyMapComparator.hashCode(getProperties());
	}

	public boolean isIgnoreFailureEnabled() {
		return (Boolean) getPropertyValue(PROP_IGNORE_FAILURE);
	}

	public int maxNumOfResubmits() {
		return (Integer) getPropertyValue(PROP_MAX_RESUBMITS);
	}

	@Override
	public String toString() {
		return "";
	}

}
