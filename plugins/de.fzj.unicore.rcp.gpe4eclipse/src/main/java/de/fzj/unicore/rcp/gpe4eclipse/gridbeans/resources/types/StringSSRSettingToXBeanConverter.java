package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.types;

import org.apache.xmlbeans.XmlCursor;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionType;

import com.intel.gpe.clients.api.jsdl.JSDLElement;
import com.intel.gpe.gridbeans.jsdl.JavaToXBeanElementConverter;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.resources.CommonXBeanConverter;
import eu.unicore.jsdl.extensions.ResourceRequestDocument;
import eu.unicore.jsdl.extensions.ResourceRequestType;

/**
 * Maps {@link StringSSRSettingType} to the new style resource requirement. 
 *
 */
public class StringSSRSettingToXBeanConverter extends CommonXBeanConverter implements
		JavaToXBeanElementConverter {

	public void convertToXBean(JobDefinitionType jobDef, JSDLElement element) {
		StringSSRSettingType child = (StringSSRSettingType) element;

		ResourceRequestDocument rrd = ResourceRequestDocument.Factory.newInstance();
		ResourceRequestType rr = rrd.addNewResourceRequest();
		rr.setName(child.getName());
		rr.setValue(child.getValue());
		
		XmlCursor parentCursor = getOrCreateResources(jobDef).
						newCursor();
		parentCursor.toEndToken();
		
		XmlCursor childCursor = rrd.newCursor();
		
		childCursor.toChild(0);
		childCursor.copyXml(parentCursor);
	}

	public Class<StringSSRSettingType> getSourceType() {
		return StringSSRSettingType.class;
	}
}
