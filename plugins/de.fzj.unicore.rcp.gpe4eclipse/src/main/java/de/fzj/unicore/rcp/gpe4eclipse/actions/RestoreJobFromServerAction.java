package de.fzj.unicore.rcp.gpe4eclipse.actions;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.gef.commands.Command;
import org.eclipse.ui.PlatformUI;

import com.intel.gpe.clients.api.JobClient;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.commands.RestoreJobFromServerCommand;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.JobNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

public class RestoreJobFromServerAction extends NodeAction {

	public RestoreJobFromServerAction(Node node) {
		super(node);
		setText("Restore Job Description");
		setToolTipText("Restore the job description from the server and display it in a new view.");
		setImageDescriptor(GPEActivator.getImageDescriptor("export_wiz.gif"));
	}

	@Override
	public boolean isCurrentlyAvailable() {
		return getNode() instanceof JobNode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.servicebrowser.actions.ServiceBrowserAction#run()
	 */
	@Override
	public void run() {

		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {

				JobClient jobClient = (JobClient) getNode().getAdapter(
						JobClient.class);
				try {
					jobClient.getStatus();
				} catch (Exception e) {
					GPEActivator
							.log(IStatus.ERROR,
									"Unable to restore the job. The job management service does not respond.",
									e);
				}
				Command cmd = new RestoreJobFromServerCommand(getNode(),
						jobClient);
				cmd.execute();
			}

		});

	}

}