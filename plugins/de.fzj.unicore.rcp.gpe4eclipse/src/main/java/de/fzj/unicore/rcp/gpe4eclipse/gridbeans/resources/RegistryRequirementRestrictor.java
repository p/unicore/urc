package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.unigrids.services.atomic.types.AvailableResourceType;

import com.intel.gpe.gridbeans.parameters.ResourcesParameterValue;

import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;


public class RegistryRequirementRestrictor extends TSRequirementRestrictor {
	private List<ResourceProviderAdapter> properties;
	
	public RegistryRequirementRestrictor(RegistryNode registry) {
		properties = getAllResourceProviders(registry);
	}
	
	private List<ResourceProviderAdapter> getAllResourceProviders(RegistryNode registry) {
		Queue<Node> q = new ConcurrentLinkedQueue<Node>();
		q.add(registry);
		List<ResourceProviderAdapter> ret = new ArrayList<ResourceProviderAdapter>();
		while (!q.isEmpty()) {
			Node next = q.poll();
			for (Node child : next.getChildrenArray()) {
				q.add(child);
			}
			if (!(next instanceof TargetSystemFactoryNode))
				continue;
			TargetSystemFactoryNode node = (TargetSystemFactoryNode) next;
			ret.add(new ResourceProviderAdapter(node.getTargetSystemFactoryProperties()));
		}
		return ret;
	}
	
	@Override
	public void restrictRequirementValues(RequirementList requirementList, ResourcesParameterValue param) {
		AvailableResource[] available = getAvailableResourceArray(properties);
		restrictSiteSpecific(available, requirementList, param);
	}

	protected AvailableResource[] getAvailableResourceArray(List<ResourceProviderAdapter> properties) {
		List<AvailableResourceType> src = new ArrayList<AvailableResourceType>();
		for (ResourceProviderAdapter p: properties) {
			Collections.addAll(src, p.getAvailableResourceArray());
		}
		List<AvailableResource> resources = ResourcesRegistry.toPOJO(src);
		return ResourcesRegistry.unifyResources(resources).toArray(new AvailableResource[0]);
	}

}
