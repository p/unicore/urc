package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources;

import java.util.HashMap;

import com.intel.gpe.clients.api.jsdl.RangeValueType;

public class UnitConverter implements IUnitConverter, RequirementConstants {

	protected static UnitConverter instance;

	/**
	 * HashMap containing factors for each Unit to the base unit of the same
	 * type. E.g. all Kilobytes are first converted into the base unit byte, so
	 * the factor would be 1024.
	 */
	protected HashMap<String, Double> factorMap = new HashMap<String, Double>();

	private UnitConverter() {
		factorMap.put(NO_UNIT, 1d);

		factorMap.put(UNIT_BYTE, 1d);
		factorMap.put(UNIT_KILO_BYTE, KILO);
		factorMap.put(UNIT_MEGA_BYTE, MEGA);
		factorMap.put(UNIT_GIGA_BYTE, GIGA);
		factorMap.put(UNIT_TERA_BYTE, TERA);

		factorMap.put(UNIT_HZ, 1d);
		factorMap.put(UNIT_KILO_HZ, KILO);
		factorMap.put(UNIT_MEGA_HZ, MEGA);
		factorMap.put(UNIT_GIGA_HZ, GIGA);

		factorMap.put(UNIT_MILLI_SECOND, 1d);
		factorMap.put(UNIT_SECOND, 1000d);
		factorMap.put(UNIT_MINUTE, 60 * 1000d);
		factorMap.put(UNIT_HOUR, 60 * 60 * 1000d);
		factorMap.put(UNIT_DAY, 24 * 60 * 60 * 1000d);
	}

	protected Double convertDouble(String sourceUnit, String targetUnit,
			double in) {
		if (Double.isInfinite(in)) {
			return in;
		}
		if (Double.isNaN(in)) {
			return in;
		}
		return in * factorMap.get(sourceUnit) / factorMap.get(targetUnit);
	}

	@SuppressWarnings("unchecked")
	public <T> T convertValue(String sourceUnit, String targetUnit,
			Class<T> valueType, T value) throws Exception {
		if (valueType.equals(Double.class)) {
			if (factorMap.containsKey(sourceUnit)
					&& factorMap.containsKey(targetUnit)) {
				return (T) convertDouble(sourceUnit, targetUnit, (Double) value);
			}
		}
		if (valueType.equals(RangeValueType.class)) {
			RangeValueType in = (RangeValueType) value;
			RangeValueType out = in.clone();
			out.setExpression(in.getExpression());
			if (factorMap.containsKey(sourceUnit)
					&& factorMap.containsKey(targetUnit)) {
				out.setExact(convertDouble(sourceUnit, targetUnit,
						in.getExact()));
				out.setEpsilon(convertDouble(sourceUnit, targetUnit,
						in.getEpsilon()));
				out.setLowerBound(convertDouble(sourceUnit, targetUnit,
						in.getLowerBound()));
				out.setUpperBound(convertDouble(sourceUnit, targetUnit,
						in.getUpperBound()));
				return (T) out;
			}
		}
		throw new Exception("Unable to convert object from unit " + sourceUnit
				+ " to unit " + targetUnit); // cannot deal with this type of
												// object
	}

	public static IUnitConverter getInstance() {
		if (instance == null) {
			instance = new UnitConverter();
		}
		return instance;
	}
}
