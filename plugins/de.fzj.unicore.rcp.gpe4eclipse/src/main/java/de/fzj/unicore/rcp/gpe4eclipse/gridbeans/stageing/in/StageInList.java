/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.in;

import java.util.Set;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;

import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.GridBeanParameterType;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSetSink;
import de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.GPEWorkflowFileSink;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageList;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;

/**
 * @author demuth
 * 
 */
public class StageInList extends StageList {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5375936276713152847L;

	public StageInList(IGridBean gridBean, IAdaptable adaptable) {
		super(gridBean, adaptable);
	}

	@Override
	public IStage createStage() {
		IStageTypeExtensionPoint ext = GPEActivator.getDefault()
				.getStageInTypeRegistry()
				.getDefaultStageType(getGridBeanContext());
		IFileParameterValue value = ext.attachToParameterValue(getAdaptable(),
				null);
		return createStage(null, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageList#createStage
	 * (com.intel.gpe.gridbeans.IGridBeanParameter,
	 * com.intel.gpe.gridbeans.IFileParameterValue)
	 */
	@Override
	public IStage createStage(IGridBeanParameter param, IFileParameterValue file) {
		return new StageIn(param, file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageList#isInputStageList
	 * ()
	 */
	@Override
	public boolean isInputStageList() {
		return true;
	}

	protected void removeStageFromActivity(String id) {
		IActivity activity = getActivity();
		if (activity == null) {
			return;
		}
		// remove the workflowFile from the set of inputs
		IDataSink sink = activity.getDataSinkList().getDataSinkById(id);
		if (sink != null) {
			sink.dispose();
			// fire property change in order to inform references
			// that the output does not exist anymore
			activity.setPropertyValue(IFlowElement.PROP_DATA_SOURCE_LIST,
					activity.getDataSourceList());
		}

	}

	@Override
	protected void removeStagesFromActivity(Set<String> removed) {
		IActivity activity = getActivity();
		if (activity == null) {
			return;
		}

		for (String id : removed) {

			removeStageFromActivity(id);

		}
	}

	@Override
	protected void updateActivity(final IStage stage,
			final IFileParameterValue newValue) {

		IActivity activity = getActivity();
		if (activity == null) {
			return;
		}
		if (activity.getDataSinkList().isDisposed()) {
			return;
		}
		boolean sinkListChanged = false;

		IGridBeanParameter param = stage.getGridBeanParameter();

		QName name = param.getName();
		String id = name.toString();
		IDataSink oldSink = activity.getDataSinkList().getDataSinkById(id);
		if (newValue != null) {
			if (GridBeanParameterType.FILE.equals(param.getType())) {
				GPEWorkflowFileSink sink = null;
				if (oldSink == null) {
					// previously unknown input file => create sink
					sink = new GPEWorkflowFileSink(activity, param,
							newValue.getUniqueId());
					sink.setPropertyValue(IDataSink.PROP_MUST_BE_FILLED, true);
					activity.getDataSinkList().addDataSink(sink);
					sinkListChanged = true;
				} else {
					if (oldSink instanceof GPEWorkflowFileSink) {
						sink = (GPEWorkflowFileSink) oldSink;
						sink.checkFilled();
					} else {
						// parameter type has changed => deal with it
						removeStageFromActivity(oldSink.getId());
						// addActivityNameToFilename(file);
						sink = new GPEWorkflowFileSink(activity, param,
								newValue.getUniqueId());
						sink.setPropertyValue(IDataSink.PROP_MUST_BE_FILLED,
								true);
						activity.getDataSinkList().addDataSink(sink);
						sinkListChanged = true;
					}
				}

			} else if (GridBeanParameterType.FILE_SET.equals(param.getType())) {
				if (oldSink == null) {
					// previously unknown input file set => create sink
					activity.getDataSinkList().addDataSink(
							new GPEWorkflowFileSetSink(activity, param));
					sinkListChanged = true;
				} else if (!(oldSink instanceof GPEWorkflowFileSetSink)) {
					// parameter type has changed => deal with it
					removeStageFromActivity(oldSink.getId());
					activity.getDataSinkList().addDataSink(
							new GPEWorkflowFileSetSink(activity, param));
					sinkListChanged = true;
				}
			}
		} else if (newValue == null) {
			// check whether previously known parameter has been removed
			if (GridBeanParameterType.FILE.equals(param.getType())) {
				removeStageFromActivity(oldSink.getId());
			}

		}
		if (sinkListChanged) {
			activity.setPropertyValue(IFlowElement.PROP_DATA_SINK_LIST,
					activity.getDataSinkList()); // force property change event
		}
	}

}
