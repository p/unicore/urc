/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.parameters;

import java.util.Map;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.clients.api.FileSystem;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.jsdl.gpe.GPEJob;
import com.intel.gpe.clients.api.transfers.IGridFileAddress;
import com.intel.gpe.clients.api.transfers.IGridFileTransfer;
import com.intel.gpe.clients.api.transfers.IProtocol;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameterValue;
import com.intel.gpe.gridbeans.parameters.processing.AbstractProtocolProcessor;
import com.intel.gpe.gridbeans.parameters.processing.ProcessingConstants;

import de.fzj.unicore.rcp.gpe4eclipse.GPE4EclipseConstants;

/**
 * This processor is used to write workflow files that are
 * 
 * @author demuth
 * 
 */
public class OtherStageingsToJSDLProcessor extends AbstractProtocolProcessor {

	public OtherStageingsToJSDLProcessor() {
		processingSteps.add(ProcessingConstants.WRITE_PROCESSED_PARAMS_TO_JSDL);
	}

	public boolean matches(IGridBeanParameter param,
			IGridBeanParameterValue value, Map<String, Object> processorParams) {
		if (!(value instanceof IFileParameterValue)) {
			return false;
		}
		IFileParameterValue file = (IFileParameterValue) value;
		IGridFileAddress address = file.isInputParameter() ? file
				.getProcessedSource() : file.getProcessedTarget();
		IProtocol protocol = address.getProtocol();
		return protocol.equals(GPE4EclipseConstants.OTHER_PROTOCOL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.gridbeans.parameters.IProtocolProcessor#process(com.intel
	 * .gpe.clients.api.jsdl.gpe.GPEJob, int,
	 * com.intel.gpe.gridbeans.IGridBeanParameter,
	 * com.intel.gpe.gridbeans.IGridBeanParameterValue, java.util.Map)
	 */
	public void process(Client client, GPEJob job, int processingStep,
			IGridBeanParameter param, IGridBeanParameterValue value,
			Map<String, Object> processorParams, IProgressListener progress)
			throws Exception {

		if (value instanceof IGridFileTransfer) {
			IGridFileTransfer file = (IGridFileTransfer) value;
			if (param.isInputParameter()) {
				// just write plain internal strings to JSDL
				job.addDataStagingImportElement(file.getProcessedSource()
						.getInternalString(), FileSystem.WORK, file
						.getProcessedTarget().getInternalString());
			} else {
				// just write plain internal strings to JSDL
				job.addDataStagingExportElement(FileSystem.WORK, file
						.getProcessedSource().getInternalString(), file
						.getProcessedTarget().getInternalString());
			}
		}
	}

}
