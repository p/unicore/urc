/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.clients.api.transfers.ProtocolConstants;
import com.intel.gpe.clients.common.transfers.GridFileAddress;
import com.intel.gpe.clients.impl.transfers.U6ProtocolConstants;
import com.intel.gpe.gridbeans.parameters.FileParameterValue;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out.StageOutTypeU6StorageCellEditor;

/**
 * @author demuth
 * 
 */
public class StageOutTypeU6StorageExtension extends
AbstractStageOutTypeExtension {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #attachToParameterValue
	 * (de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor.IAdaptable,
	 * com.intel.gpe.gridbeans.IFileParameterValue)
	 */
	public IFileParameterValue attachToParameterValue(IAdaptable activity,
			IFileParameterValue oldValue) {
		IFileParameterValue newValue;
		GridFileAddress target = new GridFileAddress("", "", "",
				U6ProtocolConstants.UNICORE_SMS_PROTOCOL, false);
		if (oldValue == null) {
			GridFileAddress source = new GridFileAddress(null, "", "",
					ProtocolConstants.JOB_WORKING_DIR_PROTOCOL, false);
			newValue = new FileParameterValue(source, target, true);
		} else {
			try {
				newValue = oldValue.clone();
			} catch (Exception e) {
				GPEActivator
				.log(IStatus.ERROR,
						"Could not clone file address for creating UNICORE output file.",
						e);
				newValue = oldValue;
			}
		}
		newValue.setTarget(target);
		checkSourceChanges(oldValue, newValue);
		return newValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getCellEditor()
	 */
	public CellEditor getCellEditor(Composite parent, IStage stage,
			IAdaptable activity) {
		return new StageOutTypeU6StorageCellEditor(parent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getPriority(de.fzj.unicore.rcp.gpe4eclipse.GridBeanContext)
	 */
	public int getPriority(GridBeanContext context) {
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint
	 * #getType()
	 */
	public QName getType() {
		return U6ProtocolConstants.UNICORE_SMS_QNAME;
	}

	@Override
	public void updateParameterValue(IAdaptable adaptable,
			IFileParameterValue oldValue, IFileParameterValue newValue) {
		super.updateParameterValue(adaptable, oldValue, newValue);
		String oldInWorkdir = oldValue.getSource().getRelativePath();
		String newInWorkdir = newValue.getSource().getRelativePath();
		if (!CollectionUtil.equalOrBothNull(oldInWorkdir, newInWorkdir)) {
			String oldTarget = oldValue.getTarget().getRelativePath();
			if(oldTarget.length()>0)
			{
				int index = oldTarget.length() - oldInWorkdir.length();
				String newTarget = oldTarget.substring(0, index);
				newTarget += newInWorkdir;
				newValue.getTarget().setRelativePath(newTarget);

				// now update displayed String, too
				oldTarget = oldValue.getTarget().getDisplayedString();
				newTarget = oldTarget.substring(0, oldTarget.length()
						- oldInWorkdir.length());
				newTarget += newInWorkdir;
				newValue.getTarget().setDisplayedString(newTarget);
			}
			
		}
	}

}
