/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Status;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;

import com.intel.gpe.clients.all.gridbeans.GridBeanInfo;
import com.thoughtworks.xstream.converters.Converter;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.registry.GridBeanRegistry;
import de.fzj.unicore.rcp.wfeditor.DefaultCreationTool;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.IPaletteEntryExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.PaletteCategory;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.PaletteElement;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;
import de.fzj.unicore.rcp.wfeditor.utils.PackageUtil;

/**
 * @author demuth
 * 
 */
public class WorkflowPaletteEntryExtension implements
IPaletteEntryExtensionPoint {

	public static final PaletteCategory PALETTE_CATEGORY_APPLICATIONS = new PaletteCategory(
			"Applications", 0.5);
	private static Set<Class<?>> annotatedClasses;
	private List<Image> images = new ArrayList<Image>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.extensionpoints.IPartFactoryExtensionPoint
	 * #createEditPart(javax.xml.namespace.QName, org.eclipse.gef.EditPart,
	 * java.lang.Object)
	 */
	public EditPart createEditPart(WFEditor editor, EditPart context,
			Object model) {
		if (model instanceof IFlowElement) {
			IFlowElement element = (IFlowElement) model;
			if (GridBeanActivity.TYPE.equals(element.getType())) {
				return new GridBeanActivityPart((GridBeanActivity) model);
			}
		} else if (model instanceof IDataSink) {
			IDataSink sink = (IDataSink) model;
			if (GPEWorkflowFileSink.TYPE.equals(sink.getSinkType())) {
				return new GPEWorkflowFileSinkPart("File");
			} else if (GPEWorkflowFileSetSink.TYPE.equals(sink.getSinkType())) {
				return new GPEWorkflowFileSinkPart("Fileset");
			} else if (GPEWorkflowVariableSink.TYPE.equals(sink.getSinkType())) {
				return new GPEWorkflowVariableSinkPart();
			}
		}
		else if (model instanceof GridBeanActivityCreationEntry)
		{
			return new GridBeanActivityCreationEntryPart((GridBeanActivityCreationEntry)model);
		}
		return null;
	}

	/**
	 * Perform some cleanup! All created images should be disposed of.
	 */
	public void dispose() {
		for (Image img : images) {
			img.dispose();
		}
	}

	public Set<Converter> getAdditionalConverters() {
		return null;
	}

	/**
	 * contribute activity types to the UNICORE workflow editor
	 */
	public List<PaletteElement> getAdditionalPaletteEntries(WFEditor editor) {
		List<PaletteElement> elements = new ArrayList<PaletteElement>();

		GridBeanRegistry registry = GPEActivator.getDefault()
		.getGridBeanRegistry();
		List<GridBeanInfo> gridbeans = registry.getGridBeans();
		GridBeanActivityFactory factory;
		CombinedTemplateCreationEntry combined;
		ImageDescriptor imageDescr;
		for (GridBeanInfo gridBeanInfo : gridbeans) {
			String appName = gridBeanInfo.getGridBeanName() + " v"
			+ gridBeanInfo.getGridBeanVersion();
			try {

				URL iconUrl = registry.getIconURLFor(gridBeanInfo);
				imageDescr = ImageDescriptor.createFromURL(iconUrl);
				ImageData imgData = imageDescr.getImageData();
				if(imgData == null) throw new FileNotFoundException(iconUrl.toString());
				imageDescr = ImageDescriptor.createFromImageData(imgData.scaledTo(DEFAULT_ICON_SIZE.width,
								DEFAULT_ICON_SIZE.height));
				
				factory = new GridBeanActivityFactory(gridBeanInfo, editor);
				
				combined = new GridBeanActivityCreationEntry(appName, factory, imageDescr,
						imageDescr);
				combined.setToolClass(DefaultCreationTool.class);
				elements.add(new PaletteElement(PALETTE_CATEGORY_APPLICATIONS,
						combined));
			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, "Problem while creating palette entry for application GUI "+appName+". Skipping this plugin.",e);
			}
		}

		return elements;
	}

	public Set<Class<?>> getAllModelClasses() {
		if (annotatedClasses == null) {
			Class<?> clazz = GridBeanActivity.class;
			Package modelPackage = clazz.getPackage();
			try {
				// this is expensive, so don't do it often!
				annotatedClasses = PackageUtil
				.getEclipseClasses(GPEActivator.getDefault(),
						modelPackage.getName(), true);
			} catch (Exception e) {
				GPEActivator.log(Status.ERROR, "Error while initializing palette: "+e.getMessage(), e);
			}
		}
		return annotatedClasses;
	}

	public Map<String, Class<?>> getOldTypeAliases() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.IExecutableExtension#setInitializationData(org
	 * .eclipse.core.runtime.IConfigurationElement, java.lang.String,
	 * java.lang.Object)
	 */
	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
		// TODO Auto-generated method stub

	}

}
