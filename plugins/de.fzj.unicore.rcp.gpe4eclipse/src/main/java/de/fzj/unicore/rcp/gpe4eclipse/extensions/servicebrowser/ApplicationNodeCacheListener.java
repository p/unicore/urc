package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import java.util.ArrayList;
import java.util.List;

import org.apache.xmlbeans.XmlObject;
import org.unigrids.x2006.x04.services.tsf.TargetSystemFactoryPropertiesDocument;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument;

import com.intel.gpe.clients.api.Application;
import com.intel.gpe.clients.impl.tss.ApplicationUtils;

import de.fzj.unicore.rcp.gpe4eclipse.applications.ApplicationRegistry;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeData;

public class ApplicationNodeCacheListener extends TargetSystemNodeCacheListener<Application> {

	protected List<Application> getEntries(XmlObject rpDoc) {
		if ((rpDoc instanceof TargetSystemFactoryPropertiesDocument)) {
			TargetSystemFactoryPropertiesDocument doc = (TargetSystemFactoryPropertiesDocument) rpDoc;
			try {
				return ApplicationUtils.loadApplications(doc
						.getTargetSystemFactoryProperties()
						.getApplicationResourceArray());
			} catch (Exception e) {
				return new ArrayList<Application>();
			}

		} else if (rpDoc instanceof TargetSystemPropertiesDocument) {
			TargetSystemPropertiesDocument doc = (TargetSystemPropertiesDocument) rpDoc;
			try {
				return ApplicationUtils.loadApplications(doc
						.getTargetSystemProperties()
						.getApplicationResourceArray());
			} catch (Exception e) {
				return new ArrayList<Application>();
			}
		}
		return new ArrayList<Application>();
	}



	public void nodeDataRemoved(INodeData nodeData) {
		ApplicationRegistry.getDefaultInstance().remove(nodeData.getURI());
		super.nodeDataRemoved(nodeData);
	}



	@Override
	protected void updateEntries(List<Application> entries, INodeData nodeData) {
		ApplicationRegistry.getDefaultInstance().updateEntries(entries,nodeData.getURI());
	}


}
