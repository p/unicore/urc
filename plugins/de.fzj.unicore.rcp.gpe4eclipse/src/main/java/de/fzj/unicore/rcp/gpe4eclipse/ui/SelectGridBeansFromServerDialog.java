/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

import com.intel.gpe.clients.api.DownloadGridBeanClient;
import com.intel.gpe.clients.api.GridBean;
import com.intel.gpe.util.sets.Pair;

@SuppressWarnings("unchecked")
public class SelectGridBeansFromServerDialog extends ElementListSelectionDialog {

	List<Pair<GridBean, DownloadGridBeanClient>> availableGridBeans;

	public SelectGridBeansFromServerDialog(Shell parentShell,
			List<Pair<GridBean, DownloadGridBeanClient>> availableGridBeans) {
		super(parentShell, new LabelProvider() {
			@Override
			public String getText(Object element) {
				Pair<GridBean, DownloadGridBeanClient> pair = (Pair<GridBean, DownloadGridBeanClient>) element;
				GridBean gb = pair.getM1();
				return gb.getName() + " version " + gb.getVersion()
						+ " (size: " + (gb.getSize() / 1024) + " kB)";
			}
		});
		this.availableGridBeans = availableGridBeans;
		setMultipleSelection(true);
		setTitle("Available applications");
		setMessage("Download new or updated applications:");
	}

	
	public List<Pair<GridBean, DownloadGridBeanClient>> getSelectedGridBeans() {
		Object[] result = getResult();
		if (result == null) {
			return null;
		}
		List<Pair<GridBean, DownloadGridBeanClient>> gridBeans = new ArrayList<Pair<GridBean, DownloadGridBeanClient>>();
		for (Object object : result) {
			gridBeans.add((Pair<GridBean, DownloadGridBeanClient>) object);
		}
		return gridBeans;
	}

	@Override
	public int open() {
		setElements(availableGridBeans.toArray(new Pair[0]));
		return super.open();
	}

}
