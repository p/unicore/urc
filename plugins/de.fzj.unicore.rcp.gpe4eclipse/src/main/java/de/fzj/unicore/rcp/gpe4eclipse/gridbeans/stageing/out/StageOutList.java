/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out;

import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;

import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IStageTypeExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageList;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;

/**
 * @author demuth
 * 
 */
public class StageOutList extends StageList {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6523460714486217102L;

	public StageOutList(IGridBean gridBean, IAdaptable gridBeanActivity) {
		super(gridBean, gridBeanActivity);
	}

	@Override
	public IStage createStage() {

		IStageTypeExtensionPoint type = GPEActivator.getDefault()
				.getStageOutTypeRegistry()
				.getDefaultStageType(getGridBeanContext());
		IFileParameterValue value = type.attachToParameterValue(getAdaptable(),
				null);

		return createStage(null, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageList#createStage
	 * (com.intel.gpe.gridbeans.IGridBeanParameter,
	 * com.intel.gpe.gridbeans.IFileParameterValue)
	 */
	@Override
	public IStage createStage(IGridBeanParameter param, IFileParameterValue file) {
		return new StageOut(param, file);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageList#isInputStageList
	 * ()
	 */
	@Override
	public boolean isInputStageList() {
		return false;
	}

	protected void removeStageFromActivity(WorkflowFile workflowFile) {
		IActivity activity = getActivity();
		if (activity == null) {
			return;
		}
		// remove the workflowFile from the set of outputs
		activity.removeOutputFile(workflowFile);
		workflowFile.dispose();
		// fire property change in order to inform references
		// that the output does not exist anymore
		activity.setPropertyValue(IFlowElement.PROP_DATA_SOURCE_LIST,
				activity.getDataSourceList());

	}

	@Override
	protected void removeStagesFromActivity(Set<String> removed) {
		IActivity activity = getActivity();
		if (activity == null) {
			return;
		}
		WorkflowFile[] outputs = activity.getOutputFiles();
		for (WorkflowFile workflowFile : outputs) {
			if (removed.contains(workflowFile.getId())) {
				removeStageFromActivity(workflowFile);
			}
		}
	}

	@Override
	protected void updateActivity(final IStage stage,
			final IFileParameterValue newValue) {

		IActivity activity = getActivity();
		if (activity == null) {
			return;
		}
		IStageTypeExtensionPoint ext = null;
		if (newValue == null) {
			IDataSource dataSource = activity.getDataSourceList()
					.getDataSourceById(stage.getId());
			if (dataSource != null && (dataSource instanceof WorkflowFile)) {
				WorkflowFile workflowFile = (WorkflowFile) dataSource;
				removeStageFromActivity(workflowFile);
			}
		} else {
			ext = GPEActivator
					.getDefault()
					.getStageOutTypeRegistry()
					.getDefiningExtension(
							newValue.getTarget().getProtocol().getName());
			IDataSource dataSource = activity.getDataSourceList()
					.getDataSourceById(newValue.getUniqueId());
			if (dataSource != null && (dataSource instanceof WorkflowFile)) {
				WorkflowFile workflowFile = (WorkflowFile) dataSource;
				if (ext != null && ext.isWorkflowFileType()) {
					// remove and readd in order to use updated hashcode
					activity.removeOutputFile(workflowFile);
					GridBeanUtils.syncWFFileWithAddress(workflowFile,
							newValue.getTarget());
					workflowFile.getMimeTypes().addAll(newValue.getMimeTypes());
					activity.addOutputFile(workflowFile);

					int previous = activity.getDataSourceList()
							.removeDataSource(workflowFile);
					activity.getDataSourceList().addDataSource(workflowFile,
							previous);
					activity.setPropertyValue(
							IFlowElement.PROP_DATA_SOURCE_LIST,
							activity.getDataSourceList());
					workflowFile.activate();
				} else {
					removeStageFromActivity(workflowFile);
				}

			} else if (ext != null && ext.isWorkflowFileType()) {
				// create and add the workflowFile
				WorkflowFile workflowFile = GridBeanUtils.createWFFileFrom(
						activity, newValue.getUniqueId(), newValue.getTarget());
				workflowFile.getMimeTypes().addAll(newValue.getMimeTypes());
				activity.addOutputFile(workflowFile);
				activity.getDataSourceList().addDataSource(workflowFile);
				activity.setPropertyValue(IFlowElement.PROP_DATA_SOURCE_LIST,
						activity.getDataSourceList());
				workflowFile.activate();
			}
		}
	}

}
