/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing;

import javax.xml.namespace.QName;

import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

/**
 * @author demuth
 * 
 */
public abstract class Stage implements IStage {

	private boolean definedInGridBean = true;

	private IGridBeanParameter gridBeanParameter;

	private IFileParameterValue value;

	private String id;

	/**
	 * type of the source file may be a local file, a remote file, a unique ID
	 * identifying a file within the workflow etc.
	 */
	private QName type;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageIn#
	 * getTargetIdentifier()
	 */
	public IGridBeanParameter getGridBeanParameter() {
		return gridBeanParameter;
	}

	public String getId() {
		return id;
	}

	public QName getType() {
		return type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageIn#getTargetValue
	 * ()
	 */
	public IFileParameterValue getValue() {
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageIn#
	 * isDefinedInGridBean()
	 */
	public boolean isDefinedInGridBean() {
		return definedInGridBean;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageIn#
	 * setDefinedInGridBean(boolean)
	 */
	public void setDefinedInGridBean(boolean defined) {
		definedInGridBean = defined;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageIn#
	 * setTargetIdentifier(com.intel.gpe.gridbeans.IGridBeanParameter)
	 */
	public void setGridBeanParameter(IGridBeanParameter identifier) {
		gridBeanParameter = identifier;

	}

	public void setId(String id) {
		this.id = id;
	}

	public void setType(QName type) {
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStageIn#setTargetValue
	 * (com.intel.gpe.gridbeans.IFileParameterValue)
	 */
	public void setValue(IFileParameterValue value) {
		this.value = value;
	}
}
