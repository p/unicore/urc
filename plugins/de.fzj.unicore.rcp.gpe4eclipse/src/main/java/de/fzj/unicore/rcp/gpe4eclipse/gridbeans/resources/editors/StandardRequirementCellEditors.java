/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.editors;

import javax.xml.namespace.QName;

import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.gpe4eclipse.extensionpoints.IResourceRequirementCellEditorExtensionPoint;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnvRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.BooleanRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.DateTimeRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.EnumRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.RangeValueTypeRequirementValue;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.values.StringRequirementValue;

/**
 * registers the standard CellEditors for Boolean, RangeValue and Enum Types
 * 
 * @author Christian Hohmann
 * 
 */
public class StandardRequirementCellEditors implements
		IResourceRequirementCellEditorExtensionPoint {

	/**
	 * returns the fitting RequirementCellEditor for the given
	 * requirementValueType
	 */
	public RequirementCellEditor getResourceRequirementCellEditor(
			Composite parent, QName requirementValueType) {

		if (BooleanRequirementValue.TYPE.equals(requirementValueType)) {
			return new RequirementCellEditor(BooleanRequirementValue.TYPE,
					new BooleanRequirementCellEditor(parent));
		} else if (EnumRequirementValue.TYPE.equals(requirementValueType)) {
			return new RequirementCellEditor(EnumRequirementValue.TYPE,
					new EnumRequirementCellEditor(parent, new String[0]));
		} else if (RangeValueTypeRequirementValue.TYPE
				.equals(requirementValueType)) {
			return new RequirementCellEditor(
					RangeValueTypeRequirementValue.TYPE,
					new RangeValueRequirementCellEditor(parent));
		} else if (StringRequirementValue.TYPE.equals(requirementValueType)) {
			return new RequirementCellEditor(StringRequirementValue.TYPE,
					new StringRequirementCellEditor(parent));
		} else if (ExecEnvRequirementValue.TYPE.equals(requirementValueType)) {
			return new RequirementCellEditor(ExecEnvRequirementValue.TYPE,
					new ExecEnvCellEditor(parent));
		} else if (DateTimeRequirementValue.TYPE.equals(requirementValueType)) {
			return new RequirementCellEditor(DateTimeRequirementValue.TYPE, new DateTimeRequirementCellEditor(parent));	
		} else {
			return null;
		}
	}
}