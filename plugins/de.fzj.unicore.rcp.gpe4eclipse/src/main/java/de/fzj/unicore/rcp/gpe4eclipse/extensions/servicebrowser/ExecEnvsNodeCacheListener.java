package de.fzj.unicore.rcp.gpe4eclipse.extensions.servicebrowser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.xmlbeans.XmlObject;
import org.unigrids.x2006.x04.services.tsf.TargetSystemFactoryPropertiesDocument;
import org.unigrids.x2006.x04.services.tsf.TargetSystemFactoryPropertiesDocument.TargetSystemFactoryProperties;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument.TargetSystemProperties;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnv;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnvUtils;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecutionEnvironmentRegistry;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeData;
import eu.unicore.jsdl.extensions.ExecutionEnvironmentDescriptionDocument.ExecutionEnvironmentDescription;

public class ExecEnvsNodeCacheListener extends TargetSystemNodeCacheListener<ExecEnv> {


	

	@Override
	protected List<ExecEnv> getEntries(XmlObject rpDoc) {
		ExecutionEnvironmentDescription[] descrs = null;
		if ((rpDoc instanceof TargetSystemFactoryPropertiesDocument)) {
			TargetSystemFactoryPropertiesDocument doc = (TargetSystemFactoryPropertiesDocument) rpDoc;
			TargetSystemFactoryProperties props = doc.getTargetSystemFactoryProperties();
			if (props == null)
				return Collections.emptyList();
			descrs = props.getExecutionEnvironmentDescriptionArray();
		} else if (rpDoc instanceof TargetSystemPropertiesDocument) {
			TargetSystemPropertiesDocument doc = (TargetSystemPropertiesDocument) rpDoc;
			TargetSystemProperties props = doc.getTargetSystemProperties();
			if (props == null)
				return Collections.emptyList();
			descrs = props.getExecutionEnvironmentDescriptionArray();
		}
		
		if (descrs == null)
			return Collections.emptyList();
		List<ExecEnv> result = new ArrayList<ExecEnv>();
		for(ExecutionEnvironmentDescription desc : descrs)
		{
			ExecEnv next = ExecEnvUtils.createExecEnv(desc);
			result.add(next);
		}
		return result;
	}

	
	public void nodeDataRemoved(INodeData nodeData) {
		ExecutionEnvironmentRegistry.getDefaultInstance().remove(nodeData.getURI());
		super.nodeDataRemoved(nodeData);
	}


	@Override
	protected void updateEntries(List<ExecEnv> entries, INodeData nodeData) {
		ExecutionEnvironmentRegistry.getDefaultInstance().updateEntries(entries,nodeData.getURI());
	}
	
	
}
