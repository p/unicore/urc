package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import java.util.Set;

import javax.xml.namespace.QName;

import com.intel.gpe.clients.api.Client;
import com.intel.gpe.gridbeans.IGridBean;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;
import com.intel.gpe.gridbeans.plugins.validators.FileIsReadableAndNotDirectoryValidator;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;

@XStreamAlias("GPEWorkflowFileSink")
@SuppressWarnings("unchecked")
public class GPEWorkflowFileSink extends DataSink {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7845802644252512265L;

	public static final QName TYPE = new QName("http://www.unicore.eu/",
			"GPEFileSink");

	private IGridBeanParameter parameter;
	private String fileId;

	public GPEWorkflowFileSink(IActivity activity,
			IGridBeanParameter parameter, String fileId) {
		super(activity);
		this.parameter = parameter;
		this.fileId = fileId;
		setId(parameter.getName().toString());
		setName(parameter.getDisplayedName());
		IGridBean gb = (IGridBean) activity.getAdapter(IGridBean.class);
		IFileParameterValue value = (IFileParameterValue) gb.get(parameter
				.getName());
		setPropertyValue(PROP_ACCEPTED_DATA_TYPES, value.getMimeTypes());
		checkFilled();
	}

	public void checkFilled() {
		IGridBean gb = (IGridBean) getActivity().getAdapter(IGridBean.class);
		Client client = (Client) getActivity().getAdapter(Client.class);
		if (gb != null && client != null) {
			IFileParameterValue value = (IFileParameterValue) gb.get(parameter
					.getName());
			FileIsReadableAndNotDirectoryValidator validator = new FileIsReadableAndNotDirectoryValidator();
			validator.setClient(client);
			boolean filled = validator.isValid(value, new StringBuffer());
			setPropertyValue(PROP_IS_FILLED, filled);
		}
	}

	public IActivity getActivity() {
		return (IActivity) super.getFlowElement();
	}

	public String getFileId() {
		return fileId;
	}

	@Override
	public Set<String> getIncomingDataTypes() {
		return (Set<String>) getPropertyValue(PROP_ACCEPTED_DATA_TYPES);
	}

	public IGridBeanParameter getParameter() {
		return parameter;
	}

	@Override
	public QName getSinkType() {
		return TYPE;
	}

	public boolean isVisible() {
		IFlowElement element = getFlowElement();
		if (element == null) {
			return false;
		}
		ShowDataFlowsProperty prop = (ShowDataFlowsProperty) element
				.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		if (prop == null || !prop.isShowingSinks()) {
			return false;
		}
		if (getIncomingDataTypes() == null
				|| getIncomingDataTypes().size() == 0) {
			return prop.isShowingFlowsWithDataType("text/any");
		} else {
			return prop.isShowingFlowsWithDataTypes(getIncomingDataTypes());
		}
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public void setParameter(IGridBeanParameter parameter) {
		this.parameter = parameter;
	}

}
