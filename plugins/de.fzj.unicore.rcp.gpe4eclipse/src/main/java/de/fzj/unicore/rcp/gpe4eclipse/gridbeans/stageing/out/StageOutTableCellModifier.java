/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.swt.widgets.TableItem;

import com.intel.gpe.clients.api.jsdl.JSDLJob;
import com.intel.gpe.gridbeans.parameters.IFileParameterValue;
import com.intel.gpe.gridbeans.parameters.IGridBeanParameter;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.StageTypeRegistry;

/**
 * @author demuth
 * 
 */
public class StageOutTableCellModifier implements ICellModifier {

	private StageOutTableViewer stageOutTable;

	public StageOutTableCellModifier(StageOutTableViewer stageOutTable) {
		this.stageOutTable = stageOutTable;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object,
	 * java.lang.String)
	 */
	public boolean canModify(Object element, String property) {
		if (!(element instanceof IStageOut)) {
			return false;
		}
		IStageOut stage = (IStageOut) element;
		if (StageOutTableViewer.ID.equals(property)) {
			return !stage.isDefinedInGridBean()
					&& stageOutTable.getStageOutList().getFileSets().size() > 1;
		} else if (StageOutTableViewer.TARGET_FILE.equals(property)) {

			int column = StageOutTableViewer.getColumnFor(property);
			CellEditor editor = resolveCellEditor(stage);
			if (editor == null) {
				return false;
			}
			stageOutTable.setCellEditor(column, editor);
			return stage.getValue().isTargetModifiable();
		} else if (StageOutTableViewer.SOURCE_FILE.equals(property)) {
			return stage.getValue().isSourceModifiable();
		}
		return true;
	}

	protected StageTypeRegistry getStageTypeRegistry() {
		return GPEActivator.getDefault().getStageOutTypeRegistry();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object,
	 * java.lang.String)
	 */
	public Object getValue(Object element, String property) {

		IStageOut stage = (IStageOut) element;
		Object result = null;
		if (StageOutTableViewer.ID.equals(property)) {
			IGridBeanParameter param = stage.getGridBeanParameter();
			result = param;
		} else if (StageOutTableViewer.TARGET_FILE.equals(property)) {
			result = stage.getValue();
		} else if (StageOutTableViewer.SOURCE_FILE.equals(property)) {
			result = stage.getValue();
		} else if (StageOutTableViewer.TARGET_TYPE.equals(property)) {
			try {
				result = stage.getValue();
			} catch (Exception e) {
				GPEActivator.log(IStatus.WARNING,
						"Could not determine type of stage out!", e);
				return 0;
			}
		}
		else if(StageOutTableViewer.FORCE.equals(property)){
			result = (stage.getValue().getFlags() & JSDLJob.FLAG_OVERWRITE) != 0;
		}
		else if(StageOutTableViewer.DELETE_OUTPUT_FILE.equals(property)){

			result = (stage.getValue().getFlags() & JSDLJob.FLAG_DELETE_ON_TERMINATE) != 0;
		}
		// else if(StageOutTableViewer.BINARY.equals(property)){
		// result = stage.isBinaryContent();
		// }

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object,
	 * java.lang.String, java.lang.Object)
	 */
	public void modify(Object element, String property, Object value) {
		if (value == null) {
			return;
		}
		if (element instanceof TableItem) {
			try {
				StageOutList stageList = stageOutTable.getStageOutList();
				TableItem item = (TableItem) element;
				StageOut stage = (StageOut) item.getData();
				if (StageOutTableViewer.ID.equals(property)) {
					IGridBeanParameter val = (IGridBeanParameter) value;
					stageList.reportStageValueChange(stage, val,
							stage.getValue());
				} else if (StageOutTableViewer.TARGET_FILE.equals(property)) {
					IFileParameterValue val = (IFileParameterValue) value;
					stageList.reportStageValueChange(stage,
							stage.getGridBeanParameter(), val);
				} else if (StageOutTableViewer.SOURCE_FILE.equals(property)) {
					IFileParameterValue val = (IFileParameterValue) value;
					stageList.reportStageValueChange(stage,
							stage.getGridBeanParameter(), val);

				} else if (StageOutTableViewer.TARGET_TYPE.equals(property)) {
					IFileParameterValue val = (IFileParameterValue) value;
					stageList.reportStageValueChange(stage,
							stage.getGridBeanParameter(), val);
					;
				}
				else if (StageOutTableViewer.FORCE.equals(property)) {
					boolean overwrite = (Boolean) value;
					IFileParameterValue val = stage.getValue().clone();
					int flags = val.getFlags();
					flags |= JSDLJob.FLAG_OVERWRITE;
					if(!overwrite) flags ^= JSDLJob.FLAG_OVERWRITE;
					val.setFlags(flags);
					stageList.reportStageValueChange(stage,
							stage.getGridBeanParameter(), val);
					;
				}
				else if (StageOutTableViewer.DELETE_OUTPUT_FILE.equals(property)) {
					
					boolean deleteOnTermination = (Boolean) value;
					IFileParameterValue val = stage.getValue().clone();
					int flags = val.getFlags();
					flags |= JSDLJob.FLAG_DELETE_ON_TERMINATE;
					if (!deleteOnTermination) flags ^= JSDLJob.FLAG_DELETE_ON_TERMINATE;
					val.setFlags(flags);
					stageList.reportStageValueChange(stage, stage.getGridBeanParameter(), val);					
				
				}

			} catch (Exception e) {
				GPEActivator.log(IStatus.WARNING,"Could not change " + property
						+ ": Invalid input.", e);
			}
			stageOutTable.packTable();
		}
	}

	protected CellEditor resolveCellEditor(IStage stage) {
		StageOutList stageList = stageOutTable.getStageOutList();
		return getStageTypeRegistry().getDefiningExtension(stage.getType())
				.getCellEditor(stageOutTable.getTable(), stage,
						stageList.getAdaptable());
	}

}
