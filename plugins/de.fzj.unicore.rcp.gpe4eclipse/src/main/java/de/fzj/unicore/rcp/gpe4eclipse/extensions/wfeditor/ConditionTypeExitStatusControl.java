/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.extensions.wfeditor;

import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.common.utils.ValidityChangeEvent;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.wfeditor.extensions.CommonConditionTypeControl;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType;
import de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.PredecessorTraverser;
import de.fzj.unicore.rcp.wfeditor.ui.IConditionChangeListener;
import de.fzj.unicore.rcp.wfeditor.ui.ValueChangeEvent;

/**
 * @author demuth
 * 
 */
public class ConditionTypeExitStatusControl extends CommonConditionTypeControl {

	private ConditionTypeExitStatus exitStatus;
	private List<IActivity> activities;
	/**
	 * 
	 */
	public ConditionTypeExitStatusControl(Composite parent, Condition value) {
		super(parent, SWT.NONE);
		exitStatus = (ConditionTypeExitStatus) value.getSelectedType();
		exitStatus.setCondition(value);
		setBackground(parent.getBackground());
		GridLayout layout = new GridLayout();
		layout.numColumns = 4;
		this.setLayout(layout);
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		Label label = new Label(this, SWT.NONE);
		label.setText("of Activity");
		label.setLayoutData(data);
		label.setBackground(getBackground());
		controls.add(label);
		initActivitySelection(value);
		initCompareOperator();
		initTextField();

		data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		data.horizontalSpan = 4;
		errorMsg = new Label(this, SWT.NONE);
		errorMsg.setLayoutData(data);
		errorMsg.setForeground(parent.getDisplay()
				.getSystemColor(SWT.COLOR_RED));
		errorMsg.setBackground(getBackground());

		data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		this.setLayoutData(data);
	}

	protected void fireValidityEvent(boolean newValidity) {
		ValidityChangeEvent evt = new ValidityChangeEvent(this, newValidity);
		for (IConditionChangeListener l : listeners) {
			l.validityChanged(evt);
		}
	}

	protected void fireValueChange() {
		ValueChangeEvent evt = new ValueChangeEvent(exitStatus.getCondition());
		for (IConditionChangeListener l : listeners) {
			l.valueChanged(evt);
		}
	}

	protected void initActivitySelection(Condition value) {
		final CCombo activityNames = new CCombo(this, SWT.FLAT | SWT.BORDER
				| SWT.READ_ONLY);
		controls.add(activityNames);
		GridData data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);
		activityNames.setLayoutData(data);
		WorkflowDiagram diagram = value.getDefiningElement().getDiagram();

		IGraphTraverser traverser = new PredecessorTraverser();
		CollectActivitiesVisitor visitor = new CollectActivitiesVisitor();

		try {
			traverser.traverseGraph(diagram, value.getDefiningElement(),
					visitor);
		} catch (Exception e) {
			GPEActivator
					.log(IStatus.ERROR,
							"Unexpected exception while searching for activities with an exit status",
							e);
		}
		activities = visitor.getFoundActivities();
		String[] names = new String[] { "no activities found" };
		int selection = -1;
		if (activities.size() > 0) {

			names = new String[activities.size()];
			int i = 0;
			for (IActivity activity : activities) {
				if (activity.equals(exitStatus.getActivity())) {
					selection = i;
				}
				names[i++] = activity.getName();
			}

		} else {
			exitStatus.setActivity(null);
		}
		activitySelected = selection > -1;
		activityNames.setItems(names);
		activityNames.select(selection);
		activityNames.addSelectionListener(new SelectionListener() {
			private void updateSelection() {
				boolean oldValidity = isValid();
				IActivity oldSelection = exitStatus.getActivity();
				int selection = activityNames.getSelectionIndex();
				activitySelected = selection > -1;
				IActivity newSelection = activities.size() > selection ? activities
						.get(selection) : null;
				exitStatus.setActivity(newSelection);
				if (oldSelection == null ? newSelection != null : !oldSelection
						.equals(newSelection)) {
					fireValueChange();
				}
				if (oldValidity != isValid()) {
					fireValidityEvent(isValid());
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				updateSelection();
			}

			public void widgetSelected(SelectionEvent e) {
				updateSelection();
			}
		});
	}

	protected void initCompareOperator() {
		CCombo compare = new CCombo(this, SWT.FLAT | SWT.BORDER | SWT.READ_ONLY);
		controls.add(compare);
		String[] items = new String[2];
		items[IConditionType.INT_EQUAL] = IConditionType.EQUAL;
		items[IConditionType.INT_UNEQUAL] = IConditionType.UNEQUAL;
		compare.setItems(items);

		GridData data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);
		data.grabExcessHorizontalSpace = false;
		compare.setLayoutData(data);
		compare.select(exitStatus.getComparator());
		compare.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean oldValidity = isValid();
				int oldSelection = exitStatus.getComparator();
				int selection = ((CCombo) e.getSource()).getSelectionIndex();
				exitStatus.setComparator(selection);
				if (oldSelection != selection) {
					fireValueChange();
				}
				if (oldValidity != isValid()) {
					fireValidityEvent(isValid());
				}
			}
		});
	}

	protected void initTextField() {

		Text text = new Text(this, SWT.SINGLE | SWT.BORDER);
		controls.add(text);
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		text.setLayoutData(data);
		String s = String.valueOf(exitStatus.getExitStatus());
		while (s.length() < 4) {
			s = " " + s;
		}
		text.setText(s);
		text.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				boolean oldValidity = isValid();
				String s = ((Text) e.getSource()).getText();
				try {
					exitStatus.setExitStatus(Integer.parseInt(s.trim()));
					valueEntered = true;
					errorMsg.setText("");
				} catch (Exception exc) {
					valueEntered = false;
					errorMsg.setText("Unable to parse input, please enter an integer value!");
					errorMsg.pack();
					pack();
				}
				boolean newValidity = isValid();
				if (oldValidity != newValidity) {
					fireValidityEvent(newValidity);
				}
				fireValueChange();
			}
		});
		
	}
}
