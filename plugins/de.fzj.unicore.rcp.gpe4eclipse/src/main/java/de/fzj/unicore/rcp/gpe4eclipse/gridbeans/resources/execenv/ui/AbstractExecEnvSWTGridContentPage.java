/**
 * 
 */
package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ui;

import javax.xml.namespace.QName;

import org.eclipse.swt.widgets.Composite;

import com.intel.gpe.gridbeans.plugins.IDataControl;
import com.intel.gpe.gridbeans.plugins.swt.ui.arguments.ApplicationParameterPanel;

import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.resources.execenv.ExecEnv;

/**
 * @author sholl
 * 
 *         The abstract implementation of the SWTGrid content page for a
 *         execution environments.
 * 
 */
public abstract class AbstractExecEnvSWTGridContentPage extends
		ApplicationParameterPanel {

	protected ExecEnv model;
	private ExecEnvViewer panel;

	public AbstractExecEnvSWTGridContentPage(Composite parent, ExecEnv o,
			String name, ExecEnvViewer panel) {
		super(parent, o, name);

		this.panel = panel;
	}

	public ExecEnv getModel() {
		return model;
	}

	public ExecEnvViewer getPanel() {
		return panel;
	}

	@Override
	public void removeDataControl(QName name, IDataControl dataControl) {

		panel.getControler().removeDataControl(name, dataControl);
	}

	@Override
	protected void setModel(Object o) {
		this.model = (ExecEnv) o;
	}

	@Override
	public void storeDataControl(QName name, IDataControl dataControl) {

		panel.getControler().linkDataControl(name, dataControl);

	}

}
