/*
 * Copyright (c) 2000-2005, Intel Corporation All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Intel Corporation nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.fzj.unicore.rcp.gpe4eclipse;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.DisplayAccess;

import com.intel.gpe.clients.api.async.AsyncClient;
import com.intel.gpe.clients.api.async.IProgressListener;
import com.intel.gpe.clients.api.async.Request;
import com.intel.gpe.util.observer.IObserver;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

/**
 * Implementation of the AsyncClient interface for Eclipse. Observer callbacks
 * are always executed via PlatformUI.getWorkbench().getDisplay().asyncExec() in
 * order to avoid illegal thread access to Eclipse GUI components.
 * 
 * @author Bastian Demuth
 */
public class SWTAsyncClient implements AsyncClient {

	public SWTAsyncClient() {
	}

	private IStatus callObserver(final Request request,
			final IObserver observer, final Object result, boolean callAsync) {
		if (observer != null) {

			Display d = PlatformUI.getWorkbench().getDisplay();
			if (d.isDisposed()) {
				return Status.CANCEL_STATUS;
			}
			if (callAsync) {
				DisplayAccess.accessDisplayDuringStartup(); // make SURE the
															// observer is
															// called, even
															// during startup of
															// the platform
				d.asyncExec(new Runnable() {
					public void run() {
						observer.observableUpdate(request, result);
					}
				});
			} else {
				observer.observableUpdate(request, result);
			}
		}
		return Status.OK_STATUS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.client2.AsyncClient#executeRequest(com.intel.gpe.client2
	 * .requests.Request, com.intel.gpe.util.observer.IObserver)
	 */
	public void executeRequest(final Request request, final IObserver observer) {
		Job j = new BackgroundJob(request.getName()) {

			
			
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				SWTProgressListener l = new SWTProgressListener(monitor);
				try {
					final Object result = request.perform(l);
					return callObserver(request, observer, result, true);

				} catch (final Throwable e) {
					return callObserver(request, observer, e, true);
				}

			}

		};
		if (PlatformUI.getWorkbench().isStarting()) {

			// During startup scheduled jobs will NOT run!!
			// => perform long running operation in main thread
			// this is ugly but hard to avoid
			// this situation may occur when updating jobs inside
			// previously opened workflows to latest client version
			PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

				public void run() {
					IProgressListener l = new FakeProgressListener();
					try {
						final Object result = request.perform(l);
						callObserver(request, observer, result, false);

					} catch (final Throwable e) {
						callObserver(request, observer, e, false);
					}

				}
			});
		} else {
			j.schedule();
		}
	}

}
