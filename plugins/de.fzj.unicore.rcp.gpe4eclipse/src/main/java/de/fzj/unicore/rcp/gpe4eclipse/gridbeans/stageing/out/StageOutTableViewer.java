package de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.out;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.common.guicomponents.FancyTableViewer;
import de.fzj.unicore.rcp.common.utils.TableColumnSorter;
import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.commands.AddStageOutToModelCommand;
import de.fzj.unicore.rcp.gpe4eclipse.commands.RemoveStageOutFromModelCommand;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.FilenameInJobDirCellEditor;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IDComboBoxCellEditor;
import de.fzj.unicore.rcp.gpe4eclipse.gridbeans.stageing.IStage;


@SuppressWarnings("unchecked")
public class StageOutTableViewer extends FancyTableViewer {

	/**
	 * column headers
	 */
	public static final String ID = "Name",
	SOURCE_FILE = "File(s) in Job Directory",
	TARGET_TYPE = "Destination Type",
	TARGET_FILE = "File(s) at Destination / File ID",
	FORCE = "Overwrite",
	DELETE_OUTPUT_FILE = "Delete on Termination"; 
	// BINARY = "Binary";
	private StageOutList stageOutList;

	/**
	 * The cell editors, one for each column of the table. Note that these are
	 * changed dynamically by the CellModifier being used!
	 */
	private CellEditor[] cellEditors;

	/**
	 * the order of the header names in COLUMN_PROPERTIES determines the order
	 * of columns in the table
	 */
	public static final String[] COLUMN_PROPERTIES = new String[] { ID,
		SOURCE_FILE, TARGET_TYPE, TARGET_FILE, FORCE, DELETE_OUTPUT_FILE };

	/**
	 * List of actions that can be executed on this viewer
	 */
	List<IAction> actions;
	private Action addStageAction;
	private Action removeStageAction;

	private CommandStack commandStack;

	/**
	 * The constructor.
	 * 
	 * @throws IOException
	 */
	public StageOutTableViewer(Composite parent, StageOutList stageOutList,
			CommandStack commandStack) {
		super(parent, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION);

		this.stageOutList = stageOutList;
		this.commandStack = commandStack;
		Table table = createTable();

		// initialize action list
		createActions();

		setUseHashlookup(true);

		// Create the cell editors
		cellEditors = new CellEditor[table.getColumnCount()];



		// ID (Combo Box)
		cellEditors[getColumnFor(ID)] = new IDComboBoxCellEditor(table,
				stageOutList);

		// Target File
		cellEditors[getColumnFor(SOURCE_FILE)] = new FilenameInJobDirCellEditor(
				table, false);

		cellEditors[getColumnFor(TARGET_TYPE)] = new StageOutTypeSelectionCellEditor(
				table, stageOutList.getGridBean(), stageOutList.getAdaptable());

		// cell editor for the target
		// this is just a dummy editor, the real editor to be used
		// may be injected by extensions based on the TARGET_TYPE
		TextCellEditor textEditor = new TextCellEditor(table);
		((Text) textEditor.getControl()).setEditable(true);
		cellEditors[getColumnFor(TARGET_FILE)] = textEditor;

		cellEditors[getColumnFor(FORCE)] = new CheckboxCellEditor(table);
		// cellEditors[getColumnFor(BINARY)] = new CheckboxCellEditor(table);
		
		cellEditors[getColumnFor(DELETE_OUTPUT_FILE)] = new CheckboxCellEditor(table);

		setCellEditors(cellEditors);

		StageOutTableCellModifier modifier = new StageOutTableCellModifier(this);
		setCellModifier(modifier);
		setContentProvider(new StageOutContentProvider());
		setLabelProvider(new StageOutLabelProvider(this));
		setColumnProperties(COLUMN_PROPERTIES);
		setInput(getStageOutList());
		packTable();
		// create context menu for the table
		final MenuManager menuMgr = new MenuManager("Actions");
		menuMgr.setRemoveAllWhenShown(true);

		Menu menu = menuMgr.createContextMenu(getTable());
		getTable().setMenu(menu);
		IMenuListener listener = new IMenuListener() {
			public void menuAboutToShow(IMenuManager m) {

				for (Iterator<IAction> iter = actions.iterator(); iter
				.hasNext();) {
					IAction action = iter.next();
					if (action.isEnabled()) {
						menuMgr.add(action);
					}
				}
			}
		};
		menuMgr.addMenuListener(listener);
	}

	private void createActions() {
		actions = new ArrayList<IAction>();
		addStageAction = new Action() {
			@Override
			public boolean isEnabled() {
				return getStageOutList().getFileSets().size() > 0;
			}

			@Override
			public void run() {
				Command cmd = new AddStageOutToModelCommand(getStageOutList());
				if (commandStack != null) {
					commandStack.execute(cmd);
				} else {
					cmd.execute();
				}
			}
		};
		addStageAction.setText("add File");
		addStageAction
		.setToolTipText("Add a file to be exported after task execution.");
		addStageAction.setImageDescriptor(GPEActivator
				.getImageDescriptor("add.png"));
		actions.add(addStageAction);

		removeStageAction = new Action() {

			@Override
			public boolean isEnabled() {
				// We cannot remove stage ins that are define by the GridBean!
				IStructuredSelection selection = (IStructuredSelection) getSelection();
				List<IStage> stages = selection.toList();
				for (IStage in : stages) {
					if (in.isDefinedInGridBean()) {
						return false;
					}
				}
				return !getSelection().isEmpty();
			}

			@Override
			public void run() {
				IStructuredSelection selection = (IStructuredSelection) getSelection();
				Command cmd = new RemoveStageOutFromModelCommand(
						getStageOutList(), selection.toList());
				if (commandStack != null) {
					commandStack.execute(cmd);
				} else {
					cmd.execute();
				}
			}
		};
		removeStageAction.setText("remove File");
		removeStageAction
		.setToolTipText("Remove a file export that is not needed anymore.");
		removeStageAction.setImageDescriptor(GPEActivator
				.getImageDescriptor("delete.png"));
		actions.add(removeStageAction);
	}

	private Table createTable() {
		Table table = getTable();
		for (int i = 0; i < COLUMN_PROPERTIES.length; i++) {
			TableColumn col = new TableColumn(table, SWT.LEFT, i);
			col.setText(COLUMN_PROPERTIES[i]);
			if (COLUMN_PROPERTIES[i].equals(ID)) {
				col.setToolTipText("An identifier for the file export.");
			} else if (COLUMN_PROPERTIES[i].equals(SOURCE_FILE)) {
				col.setToolTipText("The name of the application's output file(s).");

			} else if (COLUMN_PROPERTIES[i].equals(TARGET_TYPE)) {
				col.setToolTipText("The type of destination to which the file(s) shall be exported. \"None\" if the file(s) shall not be exported.");
			} else if (COLUMN_PROPERTIES[i].equals(TARGET_FILE)) {
				col.setToolTipText("Name of the file at its destination or unique ID that can be used to refer to this file.");
			}

			else if(COLUMN_PROPERTIES[i].equals(FORCE))
			{
				col.setToolTipText("If this is set, file(s) at the destination will be overwritten by this file export.");
			}
			
			 else if(COLUMN_PROPERTIES[i].equals(DELETE_OUTPUT_FILE))
			 {
			 col.setToolTipText("Specify whether the output file should be deleted from storage.");
			 }
			final int index = i;
			new TableColumnSorter(this, col) {
				@Override
				protected int doCompare(Viewer v, Object e1, Object e2) {
					ITableLabelProvider lp = ((ITableLabelProvider) getLabelProvider());
					String t1 = lp.getColumnText(e1, index);
					String t2 = lp.getColumnText(e2, index);
					return t1.compareTo(t2);
				}
			};
		}

		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		return table;

	}

	public List<IAction> getActions() {
		return actions;
	}

	public StageOutList getStageOutList() {
		return stageOutList;
	}

	public void setCellEditor(int column, CellEditor editor) {
		cellEditors[column] = editor;
	}

	public static int getColumnFor(String columnName) {
		for (int i = 0; i < COLUMN_PROPERTIES.length; i++) {
			if (COLUMN_PROPERTIES[i].equals(columnName)) {
				return i;
			}
		}
		return -1;
	}

}