/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.gpe4eclipse.clientstubs;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.unigrids.x2006.x04.services.tss.ApplicationResourceType;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument.TargetSystemProperties;
import org.w3c.dom.Element;

import com.intel.gpe.clients.api.Application;
import com.intel.gpe.clients.api.Job;
import com.intel.gpe.clients.api.JobClient;
import com.intel.gpe.clients.api.StorageClient;
import com.intel.gpe.clients.api.TargetSystemClient;
import com.intel.gpe.clients.api.exceptions.FaultWrapper;
import com.intel.gpe.clients.api.exceptions.GPEInvalidQueryExpressionException;
import com.intel.gpe.clients.api.exceptions.GPEInvalidResourcePropertyQNameException;
import com.intel.gpe.clients.api.exceptions.GPEJobNotSubmittedException;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareRemoteException;
import com.intel.gpe.clients.api.exceptions.GPEMiddlewareServiceException;
import com.intel.gpe.clients.api.exceptions.GPEQueryEvaluationErrorException;
import com.intel.gpe.clients.api.exceptions.GPEResourceUnknownException;
import com.intel.gpe.clients.api.exceptions.GPESecurityException;
import com.intel.gpe.clients.api.exceptions.GPEUnknownQueryExpressionDialectException;
import com.intel.gpe.clients.api.exceptions.GPEUnmarshallingException;
import com.intel.gpe.clients.api.exceptions.GPEWrongJobTypeException;
import com.intel.gpe.clients.common.ApplicationImpl;
import com.intel.gpe.clients.impl.jms.JobClientImpl;
import com.intel.gpe.clients.impl.tss.ApplicationUtils;
import com.intel.gpe.clients.impl.tss.TargetSystemClientImpl;
import com.intel.gpe.gridbeans.jsdl.JSDLJobImpl;
import com.intel.gpe.util.collections.CollectionUtil;

import de.fzj.unicore.rcp.gpe4eclipse.GPEActivator;
import de.fzj.unicore.rcp.gpe4eclipse.utils.GridBeanUtils;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.GroupNode;

/**
 * @author demuth
 * 
 */
public class SWTTargetSystemClient extends TargetSystemClientImpl implements
		IAdaptable {

	NodePath nodePath;

	/**
	 * @param client
	 */
	public SWTTargetSystemClient(NodePath path) {
		super(GPEActivator.getDefault().getClient().getWSRFClientFactory()
				.createWSRFClient(NodeFactory.getEPRForURI(path.last())));
		this.nodePath = path;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || !getClass().equals(o.getClass())) {
			return false;
		}
		SWTTargetSystemClient other = (SWTTargetSystemClient) o;
		return CollectionUtil.equalOrBothNull(nodePath, other.nodePath);
	}

	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class key) {
		if (key.equals(TargetSystemClient.class)) {
			return this;
		} else {
			Node node = NodeFactory.getNodeFor(nodePath);
			Object adapter = node.getAdapter(key);
			return adapter;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.TargetSystemClient#getApplications()
	 */
	@Override
	public List<Application> getApplications()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {

		List<Application> loadApplications = new ArrayList<Application>();
		TargetSystemNode node = (TargetSystemNode) NodeFactory
				.createNode(getEndpointReference());
		try {
			if (node.getTargetSystemProperties() == null) {
				node.refresh(0, false, null);
			}
			TargetSystemProperties targetSystemProperties = node
					.getTargetSystemProperties();
			if (targetSystemProperties != null) {
				ApplicationResourceType[] applicationResourceArray = targetSystemProperties
						.getApplicationResourceArray();
				loadApplications = ApplicationUtils
						.loadApplications(applicationResourceArray);
			}

		} catch (Exception e) {
			GPEActivator
					.log(IStatus.WARNING,
							"Problem while querying target system factory for applications",
							e);
		} finally {
			node.dispose();
		}

		return loadApplications;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#getApplications(java.lang
	 * .String)
	 */
	@Override
	public List<Application> getApplications(String applicationName)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		List<Application> loadApplications = new ArrayList<Application>();
		TargetSystemNode node = (TargetSystemNode) NodeFactory
				.createNode(getEndpointReference());
		try {
			if (node.getTargetSystemProperties() == null) {
				node.refresh(0, false, null);
			}
			TargetSystemProperties targetSystemProperties = node
					.getTargetSystemProperties();
			if (targetSystemProperties != null) {
				ApplicationResourceType[] applicationResourceArray = targetSystemProperties
						.getApplicationResourceArray();
				loadApplications = ApplicationUtils.loadApplication(
						applicationResourceArray, applicationName);
			}

		} catch (Exception e) {
			GPEActivator
					.log(IStatus.WARNING,
							"Problem while querying target system factory for an application",
							e);
		} finally {
			node.dispose();
		}

		return loadApplications;
	}

	@Override
	public String getFullAddress() {
		return nodePath.toString();
	}

	@Override
	public List<JobClient> getJobs()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		TargetSystemNode targetSystemNode = (TargetSystemNode) NodeFactory
				.createNode(getEndpointReference());
		targetSystemNode.updateChildrenFromData(2);
		List<JobClient> result = new ArrayList<JobClient>();
		
		GroupNode groupNode = targetSystemNode.getJobsNode();
		if(groupNode == null) return result;
		Node[] children = groupNode.getChildrenArray();
		for (Node node : children) {
			Object adapter = node.getAdapter(JobClient.class);
			if (adapter != null) {
				result.add((JobClient) adapter);
			}
		}
		targetSystemNode.dispose();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.TargetSystemClient#getName()
	 */
	@Override
	public String getName() throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		Node node = NodeFactory.createNode(getEndpointReference());
		String name = node.getName();
		if (name == null) {
			node.retrieveName();
			name = node.getName();
		}
		node.dispose();
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.WSRPClient#getResourceProperty(javax.xml.namespace
	 * .QName)
	 */
	@Override
	public Element getResourceProperty(QName resourceProperty)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		try {
			return super.getResourceProperty(resourceProperty);
		} catch (GPEInvalidResourcePropertyQNameException e) {
			refreshNode();
			throw e;
		} catch (GPEResourceUnknownException e) {
			refreshNode();
			throw e;
		} catch (GPEUnmarshallingException e) {
			refreshNode();
			throw e;
		} catch (GPESecurityException e) {
			refreshNode();
			throw e;
		} catch (GPEMiddlewareRemoteException e) {
			refreshNode();
			throw e;
		} catch (GPEMiddlewareServiceException e) {
			refreshNode();
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.WSRPClient#getResourcePropertyDocument()
	 */
	@Override
	public Element getResourcePropertyDocument() throws Exception {
		try {
			return super.getResourcePropertyDocument();
		} catch (Exception e) {
			refreshNode();
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#getStorage(java.lang.String)
	 */
	@Override
	public StorageClient getStorage(String type)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		try {
			return super.getStorage(type);
		} catch (GPEInvalidResourcePropertyQNameException e) {
			refreshNode();
			throw e;
		} catch (GPEResourceUnknownException e) {
			refreshNode();
			throw e;
		} catch (GPEUnmarshallingException e) {
			refreshNode();
			throw e;
		} catch (GPESecurityException e) {
			refreshNode();
			throw e;
		} catch (GPEMiddlewareRemoteException e) {
			refreshNode();
			throw e;
		} catch (GPEMiddlewareServiceException e) {
			refreshNode();
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.gpe.clients.api.TargetSystemClient#getStorages()
	 */
	@Override
	public List<StorageClient> getStorages()
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		try {
			return super.getStorages();
		} catch (GPEInvalidResourcePropertyQNameException e) {
			refreshNode();
			throw e;
		} catch (GPEResourceUnknownException e) {
			refreshNode();
			throw e;
		} catch (GPEUnmarshallingException e) {
			refreshNode();
			throw e;
		} catch (GPESecurityException e) {
			refreshNode();
			throw e;
		} catch (GPEMiddlewareRemoteException e) {
			refreshNode();
			throw e;
		} catch (GPEMiddlewareServiceException e) {
			refreshNode();
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.gpe.clients.api.TargetSystemClient#getTextInfo(java.lang.String
	 * )
	 */
	@Override
	public List<String> getTextInfo(String name)
			throws GPEInvalidQueryExpressionException,
			GPEQueryEvaluationErrorException,
			GPEUnknownQueryExpressionDialectException,
			GPEResourceUnknownException,
			GPEInvalidResourcePropertyQNameException, GPESecurityException,
			GPEUnmarshallingException, GPEMiddlewareServiceException,
			GPEMiddlewareRemoteException {
		try {
			return super.getTextInfo(name);
		} catch (GPEInvalidResourcePropertyQNameException e) {
			refreshNode();
			throw e;
		} catch (GPEInvalidQueryExpressionException e) {
			refreshNode();
			throw e;
		} catch (GPEQueryEvaluationErrorException e) {
			refreshNode();
			throw e;
		} catch (GPEUnknownQueryExpressionDialectException e) {
			refreshNode();
			throw e;
		} catch (GPEResourceUnknownException e) {
			refreshNode();
			throw e;
		} catch (GPEUnmarshallingException e) {
			refreshNode();
			throw e;
		} catch (GPESecurityException e) {
			refreshNode();
			throw e;
		} catch (GPEMiddlewareRemoteException e) {
			refreshNode();
			throw e;
		} catch (GPEMiddlewareServiceException e) {
			refreshNode();
			throw e;
		}
	}

	/**
	 * If an error occurs while accessing the server update the service browser.
	 */
	private void refreshNode() {
		Node node = NodeFactory.createNode(getEndpointReference());
		node.refresh(1, false, null);
		node.dispose();
	}

	@Override
	public JobClient submit(Job job, Calendar terminationTime)
			throws GPEWrongJobTypeException, GPEJobNotSubmittedException,
			GPEResourceUnknownException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException, GPESecurityException {
		if (job instanceof JSDLJobImpl) {
			// make sure the target system can be seen in the Grid Browser
			// otherwise we cannot guarantee that the correct security
			// properties are used
			TargetSystemNode targetSystemNode = (TargetSystemNode) NodeFactory
					.revealNode(nodePath, null);
			if (targetSystemNode == null) {
				FaultWrapper wrapper = new FaultWrapper() {
					public String[] getDescriptionAsStrings() {
						return new String[] { "resource unknown" };
					}

					public Throwable getFault() {
						return new Exception();
					}
				};
				throw new GPEResourceUnknownException("Unable to submit to "
						+ getURL() + ": resource does not exist (anymore).",
						wrapper, this);
			}
			JobClientImpl jobClient = (JobClientImpl) super.submit(job,
					terminationTime);

			NodePath path = null;
			try {
				GroupNode jobsNode = targetSystemNode.getOrCreateJobsNode();
				URI uri = new URI(jobClient.getURL());
				jobsNode.lazilyRetrieveChildrenUpTo(uri, null);
				path = jobsNode.getPathToRoot().append(uri);
				if (path == null) {
					throw new GPEMiddlewareServiceException(
							"Unable to find newly submitted job in Grid Browser",
							new Exception(), this);
				}
			} catch (Exception e) {
				throw new GPEMiddlewareServiceException(
						"Unable to find newly submitted job in Grid Browser",
						e, this);
			}
			return new SWTJobClient(path);

		} else {
			throw new GPEWrongJobTypeException(
					"Job instance is incompatible with target system (should be JSDL job)",
					this);
		}
	}

	@Override
	public boolean supportsApplication(String applicationName,
			String applicationVersion)
			throws GPEInvalidResourcePropertyQNameException,
			GPEResourceUnknownException, GPEUnmarshallingException,
			GPESecurityException, GPEMiddlewareRemoteException,
			GPEMiddlewareServiceException {
		if (Application.ANY_APPLICATION_NAME.equals(applicationName)) {
			return true;
		}
		List<Application> apps = getApplications(applicationName);
		return GridBeanUtils.matchApplication(new ApplicationImpl(
				applicationName, applicationVersion), apps);
	}

	@Override
	public String toString() {
		try {
			return getName();
		} catch (Exception e) {
			return getURL();
		}
	}
}
