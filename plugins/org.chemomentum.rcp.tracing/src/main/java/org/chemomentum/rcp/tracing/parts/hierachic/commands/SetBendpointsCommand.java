/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.tracing.parts.hierachic.commands;

import java.util.List;

import org.chemomentum.rcp.tracing.parts.hierachic.HierarchicTraceEdgePart;
import org.eclipse.draw2d.Bendpoint;


public class SetBendpointsCommand
extends org.eclipse.gef.commands.Command
{

	private HierarchicTraceEdgePart traceEdge;
	private List<Bendpoint> oldBends,newBends;
	
	public SetBendpointsCommand(HierarchicTraceEdgePart transition,List<Bendpoint> newBends)
	{
		this.traceEdge = transition;
		this.newBends = newBends;
	}

	public void execute() {
		oldBends = traceEdge.getBendpoints();
		redo();
	}


	public void redo() {
		traceEdge.setBendpoints(newBends);
	}


	public void undo() {
		traceEdge.setBendpoints(oldBends);
	}

}
