/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.tracing.actions;

import java.io.ByteArrayInputStream;
import java.util.Arrays;

import org.chemomentum.common.ws.ITrace;
import org.chemomentum.rcp.tracing.TraceActivator;
import org.chemomentum.rcp.tracing.TraceEditor;
import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.tracer.client.IMessageQuery;
import org.chemomentum.tracer.client.PatternMessageQuery;
import org.chemomentum.tracer.client.TraceQueryDescription;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;


import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.wfeditor.actions.WFEditorAction;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;

/**
 * @author demuth
 *
 */
public class TraceFromWFEditorAction extends WFEditorAction {

	public static String ID = "TracingFromWFEditorAction";
	private WFEditor editor;
	
	public TraceFromWFEditorAction(IWorkbenchPart part)
	{
		super(part);
		this.editor = (WFEditor) part;
		setId(ID);
		setText("Trace Messages");
		setToolTipText("Trace the execution of this workflow.");
		setImageDescriptor(TraceActivator.getImageDescriptor("trace.png"));
	}
	/* (non-Javadoc)
	 * @see de.fzj.unicore.rcp.wfeditor.actions.WFEditorAction#run()
	 */
	@Override
	public void run() {
		try {
			final NodePath tracerAddress = editor.getDiagram().getExecutionData().getTracingService();
			final String workflowId = editor.getDiagram().getExecutionData().getSubmissionID();
			PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable()
			{
				public void run() {
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					try {

						IPath workflowPath = editor.getDiagram().getAbsolutePath();
						IPath myPath = workflowPath.removeFileExtension();
						myPath = myPath.addFileExtension(TracingConstants.TRACE_FILE_EXTENSION);
						IFile file = PathUtils.absolutePathToEclipseFile(myPath);
						if(!file.exists())
						{
							ByteArrayInputStream is = new ByteArrayInputStream(new byte[0]);
							file.create(is, true, null);
						}
						FileEditorInput input = new FileEditorInput(file);
						TraceEditor traceEditor = (TraceEditor) page.openEditor(input,"org.chemomentum.rcp.tracing.editor");
						IMessageQuery query = new PatternMessageQuery(ITrace.MESSAGE_ID, "%"+workflowId+"%");
						TraceQueryDescription queryDescription = new TraceQueryDescription(query,TraceQueryDescription.MODE_RESULT_SET);
						queryDescription.setAttributes(Arrays.asList(TracingConstants.DEFAULT_ATTRIBUTES));
						traceEditor.initialize(tracerAddress,queryDescription);
						traceEditor.refresh();
					} catch (Exception e1) {
						TraceActivator.log("Could not open message trace: "+e1.getMessage(),e1);
					}
				}
				
			});
			
		} catch (Exception e) {
			TraceActivator.log(Status.ERROR, "Could not trace workflow.", e);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.Disposable#dispose()
	 */
	public void dispose() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.ui.actions.UpdateAction#update()
	 */
	public void update() {
		// TODO Auto-generated method stub

	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	@Override
	protected boolean calculateEnabled() {
		try {
			return editor.getDiagram().getExecutionData().getTracingService() != null;
		} catch (Exception e) {
			return false;
		}
	}
	/* (non-Javadoc)
	 * @see de.fzj.unicore.rcp.wfeditor.actions.WFEditorAction#isAddedToContextMenu()
	 */
	@Override
	public boolean isAddedToContextMenu() {
		return true;
	}
	/* (non-Javadoc)
	 * @see de.fzj.unicore.rcp.wfeditor.actions.WFEditorAction#isAddedToLocalToolbar()
	 */
	@Override
	public boolean isAddedToLocalToolbar() {
		return true;
	}

}
