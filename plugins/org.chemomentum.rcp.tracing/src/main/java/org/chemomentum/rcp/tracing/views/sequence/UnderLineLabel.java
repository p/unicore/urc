package org.chemomentum.rcp.tracing.views.sequence;


import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Rectangle;


public class UnderLineLabel extends Label {

	//private Rectangle bounds =new Rectangle(0,0,200,400);
	
	public UnderLineLabel() {
		//setBounds(bounds);
		

	}

	public void paint(Graphics graphics) {
		/*if (UMLPlugin.getDefault().getPreferenceStore().getBoolean(UMLPlugin.PREF_ANTI_ALIAS)) {
			graphics.setAntialias(SWT.ON);
			graphics.setTextAntialias(SWT.ON);
		}*/
		super.paint(graphics);
	}
	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		Rectangle bounds = getBounds();
		
		graphics.drawLine(getTextBounds().x, getTextBounds().bottom() +10, getTextBounds().right(), getTextBounds().bottom() +10);
	}

}
