/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.tracing.retrieval;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.chemomentum.rcp.tracing.TraceActivator;
import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.rcp.tracing.exceptions.TraceModelBuildingException;
import org.chemomentum.rcp.tracing.exceptions.UnsupportedTraceVersionException;
import org.chemomentum.rcp.tracing.model.ModelFactory;
import org.chemomentum.rcp.tracing.model.TraceModel;
import org.chemomentum.rcp.tracing.persistence.TraceSerializer;
import org.chemomentum.tracer.client.PatternMessageQuery;
import org.chemomentum.tracer.client.TraceClient;
import org.chemomentum.tracer.client.TraceQueryDescription;
import org.chemomentum.tracer.xmlbeans.FilterEntriesResponseDocument;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;

import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.uas.util.Pair;

/**
 * This class is used to obtain traced messages from the tracing service and
 * store them in a file. The set of messages can later be updated by calling
 * the {@link #refreshTrace()} method. Messages are stored in a file instead 
 * of returning them in memory since the set of messages can become very large.
 * @author bdemuth
 *
 */
public class TraceRefresher
{

	class SerializingJob extends BackgroundJob {

		boolean firstQuery = true;
		FilterEntriesResponseDocument doc;

		public SerializingJob() {
			super("saving traced messages");
		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {
			try {
				if(firstQuery) 
				{
					serializeMessages(doc, monitor);
					firstQuery = false;
				}
				else appendMessages(doc, monitor);
				return Status.OK_STATUS;
			}
			catch (Exception e) {
				TraceActivator.log(Status.ERROR, "An error occurred while saving message traces from the server: "+e.getMessage(), e); 
				return Status.CANCEL_STATUS;
			}


		}

		public void setDoc(FilterEntriesResponseDocument doc) {
			this.doc = doc;
		}
	}
	private TraceClient client;

	private IPath path;
	private NodePath tracerAddress = null;
	private String viewId;
	private TraceQueryDescription query;
	private Set<String> requiredProperties = new HashSet<String>();


	/**
	 * 
	 * @param tracerNode represents the tracing service to talk to
	 * @param query the query for traced messages
	 * @param path path to the file to store received messages to
	 */
	public TraceRefresher(NodePath tracerNode, TraceQueryDescription query, IPath path)
	{
		this(tracerNode,query,path,TracingConstants.MUST_HAVE_PROPERTIES);
	}

	
	public TraceRefresher(NodePath tracerNode, TraceQueryDescription query, IPath path, Set<String> requiredProperties)
	{
		this(tracerNode,query,path,requiredProperties,TracingConstants.VIEW_ID_HIERARCHIC_GRAPH);
	}
	/**
	 * 
	 * @param tracerNode represents the tracing service to talk to
	 * @param query the query for traced messages
	 * @param path path to the file to store received messages to
	 */
	public TraceRefresher(NodePath tracerNode, TraceQueryDescription query, IPath path, Set<String> requiredProperties, String viewId)
	{
		setTracerAddress(tracerNode);
		this.path = path;
		this.requiredProperties = requiredProperties;
		this.query = query;
		this.viewId = viewId;
	}


	protected void appendMessages(FilterEntriesResponseDocument doc, IProgressMonitor monitor) throws Exception
	{
		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		monitor.beginTask("serializing messages", 2);
		try {
			SubProgressMonitor sub = new SubProgressMonitor(monitor, 1);
			TraceModel tm = convertToModel(doc,sub);
			sub = new SubProgressMonitor(monitor, 1);
			TraceSerializer.addMessages(tm, path, sub);
		} finally {
			monitor.done();
		}
	}

	protected TraceModel convertToModel(FilterEntriesResponseDocument doc, IProgressMonitor monitor) throws Exception
	{
		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		TraceModel result = ModelFactory.getSharedInstance().buildModel(getClient().getServerVersion(),doc,requiredProperties);
		result.setViewID(viewId);
		return result;

	}


	public TraceClient getClient() {
		if(client == null)
		{
			// setup security for communicating with tracer..

			try {
				SecuredNode node = null;
				if(tracerAddress.getLength() > 1) node = (SecuredNode) NodeFactory.revealNode(tracerAddress, null);
				else if(tracerAddress.getLength() == 1) node = (SecuredNode) NodeFactory.createNode(tracerAddress.last()); // backwards compatibility with versions < 6.3.0
				client = null;
				try {
					client = new TraceClient(node.getEpr(),node.getUASSecProps());
				} finally {
					if(tracerAddress.getLength() == 1) node.dispose();
				}
				
				
			} catch (Exception e) {
				TraceActivator.log(Status.ERROR, "Unable to initialize connection to the Tracing service: "+e.getMessage(),e);
			}
		}
		return client;
	}

	public Set<String> getRequiredProperties() {
		return requiredProperties;
	}




	public NodePath getTracerAddress() {
		return tracerAddress;
	}

	public void refreshTrace(IProgressMonitor monitor)
	{
		
		long start = System.currentTimeMillis();

		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		monitor.beginTask("querying tracing service", 100);
		
		try {

			TraceQueryDescription tqd = query;
			Pair<String,String>[] bounds = tqd.getBoundaries();
			String serverVersion = getClient().getServerVersion();
			if(TraceClient.SERVER_VERSION_6_2.compareTo(serverVersion) < 0)
			{
				
				// new server!
				SerializingJob serializingJob = new SerializingJob(); // use background thread to save messages while continuously retrieving messages from the server
				// in order to monitor progress correctly, retrieve trace messages in two steps:
				// first retrieve the first chunk for each boundary that has been set
				// in the course of these queries, we try to find out how many queries are needed in total
				int effort =  2*tqd.getBoundaries().length;
				if(effort == 0) return;
				SubProgressMonitor sub = new SubProgressMonitor(monitor, 20);
				sub.beginTask("", effort);

				int totalNumRequiredQueries = 0;
				int[] numRequiredQueries = new int[tqd.getBoundaries().length];
				long chunkSize = 3000;//TracingConstants.MESSAGE_CHUNK_SIZE;

				for(int i = 0; i < bounds.length; i++)
				{
					Pair<String,String> bound = bounds[i];
					long first = Long.parseLong(bound.getM1());
					long last = Long.parseLong(bound.getM2());
					long requested = last-first+1;
					if(requested < 0) requested = Long.MAX_VALUE;
					tqd = query.clone();
					tqd.removeAllBoundaries();
					tqd.addBoundary(new Pair<String,String>(String.valueOf(first),String.valueOf(first+chunkSize-1)));
					FilterEntriesResponseDocument doc = client.query(tqd);
					sub.worked(1);
					long total = doc.getFilterEntriesResponse().getNumberOfAvailableMessages().longValue();
					requested = Math.min(requested, total);
					int numRequired = (int) Math.ceil(((double)requested)/chunkSize)-1;
					numRequiredQueries[i] = numRequired;
					totalNumRequiredQueries += numRequired;
					
					serializingJob.join();
					serializingJob.setDoc(doc);
					serializingJob.schedule();
					sub.worked(1);

					if(monitor.isCanceled()) return;
				}

				// now do the remaining queries and keep track of the progress
				sub = new SubProgressMonitor(monitor, 80);
				sub.beginTask("querying tracing service", 2*totalNumRequiredQueries);
				for(int i = 0; i < bounds.length; i++)
				{
					Pair<String,String> bound = bounds[i];
					long first = Long.parseLong(bound.getM1());
					long last = Long.parseLong(bound.getM2());
					int numQueries = numRequiredQueries[i];

					for(int j = 1; j <= numQueries; j++)
					{
						tqd = query.clone();
						tqd.removeAllBoundaries();
						long chunkStart = first+j*chunkSize;
						long chunkEnd = first+(j+1)*chunkSize-1;
						tqd.addBoundary(new Pair<String,String>(String.valueOf(chunkStart),String.valueOf(chunkEnd)));
						FilterEntriesResponseDocument doc = client.query(tqd);
						monitor.worked(1);
						serializingJob.join();
						serializingJob.setDoc(doc);
						serializingJob.schedule();
						sub.worked(1);
					}
				}
				serializingJob.join();
			}
			else {
				// old server!
				FilterEntriesResponseDocument doc = client.query(tqd);
				serializeMessages(doc, new SubProgressMonitor(monitor, 50));
			}
			
		} catch (TraceModelBuildingException e1) {
			TraceActivator.log(Status.ERROR, "Unable to interpret messages obtained from server", e1);
		} catch (UnsupportedTraceVersionException e1) {
			TraceActivator.log(Status.ERROR, "Unable to interpret trace server's response. Probably the client is too old for this server.", e1); 
		}
		catch (Exception e) {
			TraceActivator.log(Status.ERROR, "An error occurred while obtaining message traces from the server: "+e.getMessage(), e); 
		}
		finally {
			monitor.done();
			
		}
	}

	protected void serializeMessages(FilterEntriesResponseDocument doc, IProgressMonitor monitor) throws Exception
	{
		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		monitor.beginTask("serializing messages", 2);
		try {
			SubProgressMonitor sub = new SubProgressMonitor(monitor, 1);
			TraceModel tm = convertToModel(doc,sub);
			tm.setTracerAddress(tracerAddress);
			tm.setTraceQueryDescription(query);
			sub = new SubProgressMonitor(monitor, 1);
			TraceSerializer.serialize(tm, path, sub);
		} finally {
			monitor.done();
		}
	}


	public void setQuery(String field, String pattern)
	{
		query = new TraceQueryDescription();
		query.setAttributes(Arrays.asList(TracingConstants.DEFAULT_ATTRIBUTES));
		PatternMessageQuery inner = new PatternMessageQuery(field, pattern);
		query.setQuery(inner);
	}

	public void setQuery(TraceQueryDescription query)
	{
		this.query = query;
	}

	public void setRequiredProperties(Set<String> requiredProperties) {
		this.requiredProperties = requiredProperties;
	}

	public void setTracerAddress(NodePath tracerNode) {
		this.tracerAddress = tracerNode;
		client = null;
	}

	public void setViewId(String viewId) {
		this.viewId = viewId;
	}
	}
