/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.tracing.model;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.xmlbeans.XmlObject;
import org.chemomentum.common.ws.Callback;
import org.chemomentum.common.ws.IServiceOrchestrator;
import org.chemomentum.common.ws.ITrace;
import org.chemomentum.common.ws.IWorkflowFactory;
import org.chemomentum.common.ws.WorkflowManagement;
import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.rcp.tracing.utils.MessageUtils;
import org.chemomentum.tracer.client.AndMessageQuery;
import org.chemomentum.tracer.client.IMessageQuery;
import org.chemomentum.tracer.client.PatternMessageQuery;
import org.chemomentum.tracer.client.TimeMessageQuery;
import org.chemomentum.tracer.client.TraceClient;
import org.chemomentum.tracer.xmlbeans.EntryDocument.Entry;
import org.chemomentum.tracer.xmlbeans.FilterEntriesResponseDocument;
import org.chemomentum.tracer.xmlbeans.TraceDocument.Trace;

import de.fzj.unicore.rcp.common.UAS;

/**
 * @author demuth
 *
 */
public class ModelProducerV6_2 implements IModelProducer{

	public static final String[] MODEL_VERSIONS = new String[]{TraceClient.SERVER_VERSION_6_2,TraceClient.SERVER_VERSION_6_3};


	private ConcurrentHashMap<String,TracedService> _vertices;
	private ConcurrentHashMap<String,TracedMessage> _edges;



	public String[] getModelVersions() {
		return MODEL_VERSIONS;
	}


	
	public TraceModel buildModel(XmlObject xml, Set<String> requiredProperties)
	{		

		FilterEntriesResponseDocument doc = (FilterEntriesResponseDocument) xml;
		Trace trace = doc.getFilterEntriesResponse().getTrace();

		TraceModel graph = new TraceModel();

		_vertices = new ConcurrentHashMap<String,TracedService>();
		_edges = new ConcurrentHashMap<String,TracedMessage>();


		// create all vertices and edges from trace entries
		for(Entry entry:trace.getEntryArray())
		{
			TracedService source = addVertex(graph, entry.getSourceID(),entry.getSourceType());
			TracedService target = addVertex(graph, entry.getTargetID(),entry.getTargetType());
			if(source != null & target != null) addEdge(graph, source,target,entry,requiredProperties);
		}

		return graph;

	}
	



	
	private TracedService addVertex(TraceModel g, String name,String type)
	{

		// only create vertex if it doesn't yet exist
		TracedService n = _vertices.get(name);
		if(n != null) return n;


		String serviceType = "";
		if("client".equalsIgnoreCase(type))
		{ 
			serviceType = TracingConstants.SERVICE_TYPE_CLIENT;
		}
		else if(WorkflowManagement.SERVICE_NAME.equalsIgnoreCase(type))
		{
			name = name.substring(0,name.lastIndexOf("/")+1)+IWorkflowFactory.SERVICE_NAME;
			serviceType = TracingConstants.SERVICE_TYPE_WORKFLOW_ENGINE;
		}
		else if(IWorkflowFactory.SERVICE_NAME.equalsIgnoreCase(type))
		{
			serviceType = TracingConstants.SERVICE_TYPE_WORKFLOW_ENGINE;
			name += "/"+IWorkflowFactory.SERVICE_NAME;
		}
		else if(IServiceOrchestrator.ServiceName.equalsIgnoreCase(type))
		{
			serviceType = TracingConstants.SERVICE_TYPE_ORCHESTRATOR;
		}
		else if(Callback.ServiceName.equalsIgnoreCase(type))
		{
			serviceType = TracingConstants.SERVICE_TYPE_WORKFLOW_ENGINE;
			name = name.substring(0,name.length()-15)+IWorkflowFactory.SERVICE_NAME;
			
		}
		else if(UAS.TSS.equalsIgnoreCase(type))
		{
			serviceType = TracingConstants.SERVICE_TYPE_TARGET_SYSTEM;
		}
		n = new TracedService(name, g, serviceType);
		_vertices.put(name,n);

		return n;
	}
	


	/**
	 * create edge from trace entry
	 */
	
	private TracedMessage addEdge(TraceModel g, TracedService source, TracedService target, Entry entry, Set<String> requiredProperties)
	
	{
		String operation = entry.getOperation();


		// only add edge if it doesn't yet exist
		String key = entry.getMessageId()+entry.getSourceID()+entry.getTargetID()+entry.getOperation()+entry.getTimeStamp().getTimeInMillis();
		TracedMessage e = null;
		if(!_edges.containsKey(key)) {
			String id = entry.getMessageId();
			if(id.startsWith("WA ID: ")) 
			{
				id = id.substring(7);
			}
			e = new TracedMessage(id,g,source,target);
			//			edges for callbacks are inverted in order to obtain nice hierarchical graph
			
			e.setTimestamp(entry.getTimeStamp());
			if(requiredProperties.contains(TracedMessage.PROP_OPERATION)) 
			{
				
				if("Submit".equals(operation)) operation = TracingConstants.OPERATION_SUBMIT_JOB;
				else if("submitWorkflow".equals(operation)) operation = TracingConstants.OPERATION_SUBMIT_WORKFLOW;
				else if("submitWorkAssignment".equals(operation)) operation = TracingConstants.OPERATION_SUBMIT_JOB;
				else if("submittedJobForWA".equals(operation)) operation = TracingConstants.OPERATION_JOB_SUBMITTED;
				else if("finishedJob".equals(operation)) operation = TracingConstants.OPERATION_JOB_FINISHED;
				else if("failedJob".equals(operation)) operation = TracingConstants.OPERATION_JOB_FAILED;
				else if("finishedWA".equals(operation)) operation = TracingConstants.OPERATION_JOB_FINISHED;
				else if("failedWA".equals(operation)) operation = TracingConstants.OPERATION_JOB_FAILED;
				
				e.setOperation(operation);
			}
			if(MessageUtils.isResponse(entry))
			{ 
				e.setResponse(true);
			}
			if(requiredProperties.contains(TracedMessage.PROP_CONTENT))e.setContent(entry.getMessage().toString());
			_edges.put(key,e);
		}
		else
		{
			e = _edges.get(key);
		}
		return e;
	}



	@Override
	public IMessageQuery buildUniqueQuery(TracedMessage msg) {
        IMessageQuery idQuery = new PatternMessageQuery(ITrace.MESSAGE_ID,"%"+msg.getID());
		
		long time = msg.getTimestamp().getTimeInMillis();
		IMessageQuery timestampQuery = new TimeMessageQuery(time-1,time+1);
	
		IMessageQuery sourceQuery = new PatternMessageQuery(ITrace.SOURCE_ID,msg.getSource().getID());
		IMessageQuery targetQuery = new PatternMessageQuery(ITrace.TARGET_ID,msg.getTarget().getID());
		IMessageQuery query = new AndMessageQuery(idQuery, timestampQuery); 
						new AndMessageQuery(timestampQuery,
								new AndMessageQuery(sourceQuery, targetQuery));
		return query;
		
	}
	





}
