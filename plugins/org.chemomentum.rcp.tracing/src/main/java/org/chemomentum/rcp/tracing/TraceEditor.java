package org.chemomentum.rcp.tracing;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.chemomentum.rcp.tracing.actions.RefreshTraceAction;
import org.chemomentum.rcp.tracing.model.TraceModel;
import org.chemomentum.rcp.tracing.parts.TraceRootEditPart;
import org.chemomentum.rcp.tracing.persistence.TraceSerializer;
import org.chemomentum.rcp.tracing.retrieval.TraceRefresher;
import org.chemomentum.tracer.client.TraceQueryDescription;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.MouseWheelHandler;
import org.eclipse.gef.MouseWheelZoomHandler;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.DirectEditAction;
import org.eclipse.gef.ui.parts.GraphicalEditor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;

import de.fzj.unicore.rcp.common.guicomponents.IEditorPartWithActions;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.ui.OverviewContentOutlinePage;




public class TraceEditor extends GraphicalEditor implements IEditorPartWithActions, 
PropertyChangeListener, ITabbedPropertySheetPageContributor{

	public static final String PROPERTY_CONTRIBUTOR_ID = "org.chemomentum.rcp.tracing";
	public static final String ID = "org.chemomentum.rcp.tracing.editor";
	protected TraceViewer graphicalViewer;
	protected TraceRefresher traceRefresher;

	protected TraceModel contents;
	protected TraceQueryDescription query;
	protected IPath path;

	protected ScalableFreeformRootEditPart zoomPanel;

	private Action refreshAction, zoomInAction, zoomOutAction;
	private Shell shell;


	private Map<String,Object> viewData = new HashMap<String, Object>();

	/**
	 * The constructor.
	 */
	public TraceEditor() {
	}


	/**
	 * @see org.eclipse.gef.ui.parts.GraphicalEditor#configureGraphicalViewer()
	 */
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();

		zoomPanel = new TraceRootEditPart();
		getGraphicalViewer().setRootEditPart(zoomPanel);

		double start = 0.1, end = 2.0, stepSize = 0.1;
		int numSteps = (int) ((end-start)/stepSize);
		double[] zoomLevels = new double[numSteps];
		for(int i = 0; i < numSteps; i ++)
		{
			zoomLevels[i] = start+i*stepSize;
		}
		zoomPanel.getZoomManager().setZoomLevels(zoomLevels);
		List<String> zoomContributions = Arrays.asList(new String[] { 
				ZoomManager.FIT_ALL, 
				ZoomManager.FIT_HEIGHT, 
				ZoomManager.FIT_WIDTH });
		zoomPanel.getZoomManager().setZoomLevelContributions(zoomContributions);
		zoomPanel.getZoomManager().setZoomAnimationStyle(ZoomManager.ANIMATE_ZOOM_IN_OUT);
		getGraphicalViewer().setProperty(MouseWheelHandler.KeyGenerator.getKey(SWT.MOD1), 
				MouseWheelZoomHandler.SINGLETON);

		ContextMenuProvider provider =
			new TraceContextMenuProvider(getGraphicalViewer(),getActionRegistry());
		getGraphicalViewer().setContextMenu(provider);
		getSite().registerContextMenu(
				ID, //$NON-NLS-1$
				provider,
				getGraphicalViewer());
		zoomPanel.getZoomManager().setZoom(.5d);
	}

	/**
	 * @see org.eclipse.gef.ui.parts.GraphicalEditor#createActions()
	 */
	@Override
	protected void createActions() {
		super.createActions();
		ActionRegistry registry = getActionRegistry();
		IAction action;

		action = new DirectEditAction((IWorkbenchPart)this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		zoomInAction = new Action("Zoom In") {
			public void run() { 
				zoomIn();
			}
		};
		zoomInAction.setImageDescriptor(WFActivator.getImageDescriptor("zoom_in.png"));
		zoomInAction.setId(WFConstants.ACTION_ZOOM_IN);
		registry.registerAction(zoomInAction);
		getSite().getKeyBindingService().registerAction(zoomInAction);

		zoomOutAction = new Action("Zoom Out") {
			public void run() { 
				zoomOut();
			}
		};
		zoomOutAction.setImageDescriptor(WFActivator.getImageDescriptor("zoom_out.png"));
		zoomOutAction.setId(WFConstants.ACTION_ZOOM_OUT);
		registry.registerAction(zoomOutAction);
		getSite().getKeyBindingService().registerAction(zoomOutAction);

		IAction refreshAction = new RefreshTraceAction(this);
		registry.registerAction(refreshAction);

	}



	protected void createGraphicalViewer(Composite parent) {
		TraceViewer viewer = new TraceViewer();
		setGraphicalViewer(viewer);
		viewer.createControl(parent);
		viewer.getControl().setBackground(ColorConstants.white);
		configureGraphicalViewer();
		hookGraphicalViewer();
		viewer.setEditDomain(getEditDomain());
	}


	public void createPartControl(Composite parent) {
		super.createPartControl(parent);

	}



	/**
	 * @see org.eclipse.ui.ISaveablePart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	public void doSave(IProgressMonitor monitor) {

		try {
			TraceSerializer.serialize(getContents(), path, monitor);
			getCommandStack().markSaveLocation();
		}
		catch (Exception e) {
			TraceActivator.log(Status.ERROR,"Unable to save traced messages to disk: "+e.getMessage(),e);
		}

	}

	/**
	 * @see org.eclipse.ui.ISaveablePart#doSaveAs()
	 */
	public void doSaveAs() {
		//		SaveAsDialog dialog= new SaveAsDialog(getSite().getWorkbenchWindow().getShell());
		//		dialog.setOriginalFile(((IFileEditorInput)getEditorInput()).getFile());
		//		dialog.open();
		//		IPath path= dialog.getResult();

		//		if (path == null)
		//		return;

		//		IWorkspace workspace= ResourcesPlugin.getWorkspace();
		//		final IFile file= workspace.getRoot().getFile(path);

		//		WorkspaceModifyOperation op= new WorkspaceModifyOperation() {
		//		public void execute(final IProgressMonitor monitor) throws CoreException {
		//		try {
		//		ByteArrayOutputStream out = new ByteArrayOutputStream();
		//		createOutputStream(out);
		//		file.create(new ByteArrayInputStream(out.toByteArray()), true, monitor);
		//		out.close();
		//		} 
		//		catch (Exception e) {
		//		e.printStackTrace();
		//		}
		//		}
		//		};

		//		try {
		//		new ProgressMonitorDialog(getSite().getWorkbenchWindow().getShell()).run(false, true, op);
		//		setInput(new FileEditorInput((IFile)file));
		//		getCommandStack().markSaveLocation();
		//		} 
		//		catch (Exception e) {
		//		e.printStackTrace();
		//		} 
	}


	public IAction getAction(String actionID) {
		return getActionRegistry().getAction(actionID);
	}

	public Object getAdapter(Class adapter) {
		if (adapter == ZoomManager.class) {
			return zoomPanel.getZoomManager();
		} else if(adapter == IContentOutlinePage.class) {
			return new OverviewContentOutlinePage(getGraphicalViewer());
		}
		return super.getAdapter(adapter);
	}



	/**
	 * @return the contents
	 */
	public TraceModel getContents() {
		return contents;
	}



	/**
	 * @return the graphicalViewer
	 */
	public TraceViewer getGraphicalViewer() {
		return graphicalViewer;
	}

	/**
	 * @return the traceRefresher
	 */
	public TraceRefresher getTraceRefresher() {
		return traceRefresher;
	}

	public Object getViewData(String viewId)
	{
		return viewData.get(viewId);
	}


	/**
	 * @see org.eclipse.graph.AbstractExample#hookShell()
	 */
	protected void hookShell() {
		//		Composite composite = new Composite(shell, 0);
		//		composite.setLayoutData(new GridData(GridData.FILL_VERTICAL));

		//		composite.setLayout(new GridLayout());	
		//		org.eclipse.swt.widgets.Label nodesLabel 
		//		= new org.eclipse.swt.widgets.Label(composite, SWT.NONE);
		//		nodesLabel.setBounds(0, 0, arg2, arg3)
		//		nodesLabel.setText("nodes label");


	}

	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		DefaultEditDomain defaultEditDomain = new DefaultEditDomain(this);
		setEditDomain(defaultEditDomain);
		super.init(site, input);
	}

	public void initialize(NodePath tracerAddress, TraceQueryDescription query)
	{
		this.query = query;
		TraceRefresher refresher = new TraceRefresher(tracerAddress,query,path);
		setTraceRefresher(refresher);
	}

	@Override
	protected void initializeGraphicalViewer() {


	}

	public void refresh()
	{	
		Job j = new BackgroundJob("Refreshing trace graph...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("refreshing trace graph", 3);
				SubProgressMonitor sub = new SubProgressMonitor(monitor, 2);
				TraceModel model = (TraceModel) getContents();
				if(model != null)
				{
					query = model.getTraceQueryDescription(); // use up-to-date query
					getTraceRefresher().setViewId(model.getViewID());
				}
				getTraceRefresher().setQuery(query);
				getTraceRefresher().refreshTrace(sub);
				try {
					sub = new SubProgressMonitor(monitor, 1);
					model = TraceSerializer.deserialize(path, query, TracingConstants.HARD_MESSAGE_LIMIT, sub);
					model.setTraceQueryDescription(query);
				} catch (Exception e1) {
					TraceActivator.log(Status.ERROR, "Unable to load messages obtained from server", e1);
				}
				final TraceModel newContents = model;
				try {
					getSite().getShell().getDisplay().asyncExec(new Runnable(){
						public void run() {
							setContents(newContents);
							refreshViewer();
						}});

				} catch (NullPointerException e) {
					// do nothing
				}
				return Status.OK_STATUS;
			}

		};
		j.schedule();

	}

	public void setAction(String actionID, IAction action) {
		getActionRegistry().removeAction(action);
		action.setId(actionID); // make sure this is set correctly
		getActionRegistry().registerAction(action);
	}


	/**
	 * @param contents the contents to set
	 */
	public void setContents(TraceModel contents) {
		if(this.contents != null) 
		{
			this.contents.removePropertyChangeListener(this);
		}
		this.contents = contents;
		if(contents != null) contents.addPropertyChangeListener(this);
	}

	protected void recreateViewer()
	{
		getSelectionSynchronizer().removeViewer(getGraphicalViewer());
		Control c = getGraphicalViewer().getControl();
		Composite parent = c.getParent();
		c.dispose();
		createGraphicalViewer(parent);
		parent.layout();

	}

	protected void refreshViewer()
	{
		try {
			getGraphicalViewer().setContents(getContents());
			getGraphicalViewer().refresh();
			getGraphicalViewer().getRootEditPart().refresh();

		} catch (Exception e) {
		
		}
		
	}
	/**
	 * @param graphicalViewer the graphicalViewer to set
	 */
	public void setGraphicalViewer(TraceViewer graphicalViewer) {
		this.graphicalViewer = graphicalViewer;
	}

	protected void setInput(IEditorInput input) {
		super.setInput(input);
		path = ((IFileEditorInput)getEditorInput()).getFile().getLocation();
		File f = path.toFile();
		if(f.exists() && f.length() > 0)
		{
			final IPath p = path;
			Job j = new BackgroundJob("loading trace diagram") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try {
						final TraceModel t = TraceSerializer.deserialize(p, null, TracingConstants.HARD_MESSAGE_LIMIT, monitor);
						initialize(t.getTracerAddress(), t.getTraceQueryDescription());
						getSite().getShell().getDisplay().asyncExec(new Runnable() {
							public void run() {
								setContents(t);
								refreshViewer();
							}
						});
						return Status.OK_STATUS;
					} catch (Exception e) {
						TraceActivator.log(Status.ERROR, "Unable to load traced messages "+e.getMessage(),e);
						return Status.CANCEL_STATUS;
					}

				}
			};
			j.schedule();

		}
	}

	public void setViewData(String viewId, Object data)
	{
		if(data != null) viewData.put(viewId, data);
		else viewData.remove(viewId);
	}

	/**
	 * @param traceRefresher the traceRefresher to set
	 */
	public void setTraceRefresher(TraceRefresher traceRefresher) {
		this.traceRefresher = traceRefresher;
	}


	public void zoomIn()
	{
		zoomPanel.getZoomManager().zoomIn();
	}


	public void zoomOut()
	{
		zoomPanel.getZoomManager().zoomOut();
	}


	public void propertyChange(PropertyChangeEvent evt) {
		if(TraceModel.PROP_VIEW_ID.equals(evt.getPropertyName()))
		{
			refreshViewer();

		}

	}
	
	public String getContributorId()
	{
		return PROPERTY_CONTRIBUTOR_ID;
	}

}