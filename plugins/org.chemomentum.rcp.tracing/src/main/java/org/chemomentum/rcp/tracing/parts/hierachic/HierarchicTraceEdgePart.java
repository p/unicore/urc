/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.tracing.parts.hierachic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.rcp.tracing.model.TraceModel;
import org.chemomentum.rcp.tracing.model.TracedMessage;
import org.chemomentum.rcp.tracing.parts.ITracePart;
import org.chemomentum.rcp.tracing.parts.hierachic.policies.TraceEdgeBendpointEditPolicy;
import org.eclipse.draw2d.AbsoluteBendpoint;
import org.eclipse.draw2d.Bendpoint;
import org.eclipse.draw2d.BendpointConnectionRouter;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Edge;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.draw2d.graph.NodeList;
import org.eclipse.draw2d.graph.Subgraph;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.draw2d.text.PageFlowLayout;
import org.eclipse.draw2d.text.SimpleTextLayout;
import org.eclipse.draw2d.text.TextFlow;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;

import de.fzj.unicore.rcp.wfeditor.model.transitions.SerializableBendpoint;


/**
 * In the hierarchic diagram view, messages with equal source, target,
 * and operation type are merged and represented by a single edit part of this type.
 * @author demuth
 *
 */
public class HierarchicTraceEdgePart extends AbstractConnectionEditPart implements ITracePart {

	List<Bendpoint> bendpoints = new ArrayList<Bendpoint>();

	protected void applyLayout(CompoundDirectedGraph graph, Map map) {
		PolylineConnection conn = (PolylineConnection)getConnectionFigure();
		PolygonDecoration dec = new PolygonDecoration();
		List<Bendpoint> bends = new ArrayList<Bendpoint>();
		conn.setTargetDecoration(dec);
		
		if(isResponse())
		{
			Edge edge = (Edge)map.get(this);
			NodeList nodes = edge.vNodes;
			
			if (nodes != null) {
				for (int i = 0; i < nodes.size(); i++) {
					Node vn = nodes.getNode(i);
					int x = vn.x;
					int y = vn.y;
					bends.add(new AbsoluteBendpoint(x, y + vn.height));
					bends.add(new AbsoluteBendpoint(x, y));
					
				}
			} 
		}
		else
		{		
			Node mid = (Node) map.get(this);
			mid.setPadding(new Insets(10,50,10,10));
			Edge e1 = (Edge) mid.incoming.get(0);
			Edge e2 = (Edge) mid.outgoing.get(0);
			NodeList nodes = e1.vNodes;
			if(nodes == null) nodes = new NodeList();
			if(e2.vNodes != null) nodes.addAll(e2.vNodes);

			for (int i = 0; i < nodes.size(); i++) {
				Node vn = nodes.getNode(i);
				int x = vn.x;
				int y = vn.y;
				bends.add(new AbsoluteBendpoint(x, y));
				bends.add(new AbsoluteBendpoint(x, y + vn.height));
			}
		}
		if(bends.size() > 0) conn.setRoutingConstraint(bends);
		else {
			conn.setRoutingConstraint(Collections.EMPTY_LIST);
		}
		setBendpoints(bends);
	}

	protected boolean isResponse()
	{

		if(getMessages() == null || getMessages().size() == 0) return false;
		return getMessages().get(0).isResponse();
	}
	
	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.CONNECTION_BENDPOINTS_ROLE, new TraceEdgeBendpointEditPolicy());
		//		installEditPolicy(EditPolicy.CONNECTION_ROLE, new TransitionEditPolicy());
	}



	/**
	 * @see org.eclipse.gef.editparts.AbstractConnectionEditPart#createFigure()
	 */
	protected IFigure createFigure() {

		PolylineConnection conn =(PolylineConnection)super.createFigure();
		conn.setConnectionRouter(new BendpointConnectionRouter(){
			public void route(Connection conn) {
				//				GraphAnimation.recordInitialState(conn);
				//				if (!GraphAnimation.playbackState(conn))
				super.route(conn);
			}
		});


		conn.setForegroundColor(ColorConstants.gray);

		List<Bendpoint> bends = getBendpoints();
		if(bends != null) conn.setRoutingConstraint(bends);	


		// label multi-edges with the number of entries they represent
		List<TracedMessage> messages = getMessages();
		String operationName = (String) getMessages().get(0).getPropertyValue(TracedMessage.PROP_OPERATION);
		String labeltext = operationName +" ("+messages.size()+")";
		Label label = new Label(labeltext);
		label.setOpaque(true);
		label.setBackgroundColor(ColorConstants.buttonLightest);
		FontData labelfont = new FontData();
		labelfont.setHeight(14);

		label.setFont(new Font(Display.getCurrent(),labelfont));
		label.setBorder(new LineBorder());

		conn.add(label, new HierarchicTraceEdgeEndpointLocator(conn, false,isResponse()));

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		StringBuffer toolTip = new StringBuffer("");
		for (int i = 0; i < messages.size(); i++) {
			TracedMessage msg = messages.get(i);
			toolTip.append(df.format(msg.getTimestamp().getTime()));
			toolTip.append(": ");
			toolTip.append(msg.getOperation());
			if(i < Math.min(20,messages.size() -1)) toolTip.append("\n");
			else break; // don't create a tooltip for tens of thousands of messages
		}
		Font labelFont = Display.getDefault().getSystemFont();
		TextFlow tf = new TextFlow(toolTip.toString());
		tf.setFont(labelFont);
		FlowPage fp = new FlowPage();
		PageFlowLayout pfl = (PageFlowLayout) fp.getLayoutManager();
		fp.add(tf);
		SimpleTextLayout stl = new SimpleTextLayout(tf);
		stl.setFlowContext(pfl);
		tf.setLayoutManager(stl);
		Panel panel = new Panel();
		panel.add(fp);
		panel.setLayoutManager(new StackLayout());
		panel.setPreferredSize(500,50);
		
		panel.setBorder(new LineBorder(ColorConstants.tooltipForeground, 1));
		panel.setBackgroundColor(ColorConstants.tooltipBackground);

		fp.getLayoutManager().layout(panel);
		panel.setPreferredSize(tf.getPreferredSize());

		conn.setToolTip(panel);

		return conn;

	}

	public PolylineConnection getFigure()
	{
		return (PolylineConnection) super.getFigure();
	}

	public void refresh()
	{
		super.refresh();
		List<Bendpoint> bends = getBendpoints();
		if(bends != null) getFigure().setRoutingConstraint(bends);
	}


	
	public List<TracedMessage> getMessages()
	{
		getParent().getChildren().get(0);
		HierarchicTraceGraphPart parent = (HierarchicTraceGraphPart) getParent().getChildren().get(0);
		return parent.getMessagesWithKey(getModel());
	}

	/**
	 * String identifiers are used as model objects for these edit parts.
	 * The "real" model, i.e. the underlying {@link TracedMessage} objects
	 * can be obtained via the {@link #getMessages()} method. 
	 */
	public String getModel()
	{
		return (String) super.getModel();
	}


	/**
	 * @see org.eclipse.gef.EditPart#setSelected(int)
	 */
	public void setSelected(int value) {
		super.setSelected(value);
		if (value != EditPart.SELECTED_NONE)
			((PolylineConnection)getFigure()).setLineWidth(2);
		else
			((PolylineConnection)getFigure()).setLineWidth(1);
	}

	public void contributeEdgesToGraph(CompoundDirectedGraph graph, Subgraph s, Map map) {
		//		GraphAnimation.recordInitialState(getConnectionFigure());
		Node source = (Node)map.get(getSource());
		Node target = (Node)map.get(getTarget());
		if(source == null || target == null)
		{
			return;
		}
		if(isResponse())
		{
			Edge e = new Edge(this, source, target);
			e.weight = 2;
			e.invert();
			graph.edges.add(e);
			map.put(this, e);
		}
		else 
		{
			Node mid = new Node(this,s);
			mid.width = 10;
			mid.height = 50;
			graph.nodes.add(mid);
			Edge e1 = new Edge(this, source, mid); 			
			e1.weight = 0;
			graph.edges.add(e1);

			Edge e2 = new Edge(this, mid,target); 
			e2.weight = 0;
			graph.edges.add(e2);
			map.put(this, mid);
			
			
		}

	}

	public String getViewId() {
		return TracingConstants.VIEW_ID_HIERARCHIC_GRAPH;
	}

	public TraceModel getTraceModel() {
		return getMessages().get(0).getGraph();
	}


	public List<Bendpoint> getBendpoints() {
		return bendpoints;
	}

	public void insertBendpoint(int index,Bendpoint bp)
	{
		List<Bendpoint> bps = new ArrayList<Bendpoint>(getBendpoints());
		bps.add(index, bp);
		setBendpoints(bps);
	}

	public void removeBendpoint(Bendpoint bp)
	{
		List<Bendpoint> bps = new ArrayList<Bendpoint>(getBendpoints());
		bps.remove(bp);
		setBendpoints(bps);
	}

	public void removeBendpoint(int index)
	{
		List<Bendpoint> bps = new ArrayList<Bendpoint>(getBendpoints());
		bps.remove(index);
		setBendpoints(bps);
	}

	public void setBendpoint(int index, Bendpoint point) {
		List<Bendpoint> bps = new ArrayList<Bendpoint>(getBendpoints());
		bps.set(index, point);
		setBendpoints(bps);
	}

	public void setBendpoints(List<Bendpoint> bendpoints) {
		this.bendpoints = bendpoints;
		refresh();
	}

	public void move(int deltaX, int deltaY) {
		List<Bendpoint> bps = getBendpoints();
		List<Bendpoint> newBps = new ArrayList<Bendpoint>();
		for(Bendpoint b : bps)
		{
			newBps.add(new SerializableBendpoint(b.getLocation().x+deltaX,b.getLocation().y+deltaY));
		}
		setBendpoints(newBps);
	}

}
