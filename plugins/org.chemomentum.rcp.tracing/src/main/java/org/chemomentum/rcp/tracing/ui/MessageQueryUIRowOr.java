package org.chemomentum.rcp.tracing.ui;

import org.chemomentum.tracer.client.IMessageQuery;
import org.chemomentum.tracer.client.OrMessageQuery;
import org.eclipse.swt.widgets.Composite;

public class MessageQueryUIRowOr extends MessageQueryUIRow {
	public static final String TYPE_NAME = "Any of the following";

	
	
	public MessageQueryUIRowOr(int nestingLevel) {
		super(TYPE_NAME, nestingLevel);

	}

	
	@Override
	public void createControls(Composite parent) {
		
		
	}


	@Override
	public IMessageQuery buildQuery() {
		MessageQueryUIRow first = getNestedQueryRows().get(0);
		IMessageQuery firstQuery = first.buildQuery();
		if(getNestedQueryRows().size() == 1) return firstQuery; // no or required
		OrMessageQuery result = new OrMessageQuery(firstQuery, null);
		OrMessageQuery current = result;
		for(int i = 1; i < getNestedQueryRows().size(); i++)
		{
			MessageQueryUIRow row = getNestedQueryRows().get(i);
			IMessageQuery query = row.buildQuery();
			if(i == getNestedQueryRows().size() -1) current.setRight(query);
			else
			{
				OrMessageQuery or = new OrMessageQuery(query, null);
				current.setRight(or);
				current = or;
			}
		}
		return result;
	}
	

}
