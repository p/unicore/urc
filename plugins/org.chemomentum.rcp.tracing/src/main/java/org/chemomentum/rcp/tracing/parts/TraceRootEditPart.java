package org.chemomentum.rcp.tracing.parts;

import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.Layer;
import org.eclipse.draw2d.LayeredPane;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;

import de.fzj.unicore.rcp.wfeditor.ui.WFRootEditPart;

public class TraceRootEditPart extends ScalableFreeformRootEditPart {

	
	protected void createLayers(LayeredPane layeredPane) {
		super.createLayers(layeredPane);
		Layer layer = new FreeformLayer();
		layer.setLayoutManager(new FreeformLayout());
		layeredPane.add(layer, WFRootEditPart.TOOLTIP_LAYER);
	}
}
