/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.tracing.actions;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.chemomentum.common.ws.ITrace;
import org.chemomentum.rcp.servicebrowser.nodes.WorkflowNode;
import org.chemomentum.rcp.tracing.TraceActivator;
import org.chemomentum.rcp.tracing.TraceEditor;
import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.tracer.client.IMessageQuery;
import org.chemomentum.tracer.client.PatternMessageQuery;
import org.chemomentum.tracer.client.TraceQueryDescription;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowUtils;
import de.fzj.unicore.wsrflite.xmlbeans.client.RegistryClient;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * @author demuth
 *
 */
public class TraceWFNodeAction extends NodeAction {


	IContainer targetFolder = null;

	/**
	 * @param node
	 * @param view
	 */
	public TraceWFNodeAction(Node node) {
		super(node);
		setText("Trace Messages");
		setToolTipText("Trace the execution of this workflow.");
		setImageDescriptor(TraceActivator.getImageDescriptor("trace.png"));
	}


	/* (non-Javadoc)
	 * @see de.fzj.unicore.rcp.servicebrowser.actions.ServiceBrowserAction#run()
	 */
	@Override
	public void run() {
		try {
			WorkflowNode wfNode = (WorkflowNode) getNode();
			RegistryNode regNode = null;
			EndpointReferenceType tracerEpr = wfNode.getTracerAddress();
			
			// find registry node
			Node parent = getNode().getParent();
			while(parent != null && !(parent instanceof RegistryNode))
			{
				parent = parent.getParent();
			}
			if(parent == null) 
			{
				TraceActivator.log(Status.ERROR,"Unable to find registry to query for tracer service!");
				return;
			}
			regNode = (RegistryNode) parent;
			if(tracerEpr == null)
			{
				wfNode.refresh(1,false,null);
				tracerEpr = wfNode.getTracerAddress();
				if(tracerEpr == null)
				{
					
					EndpointReferenceType regEpr = regNode.getEpr();
					URL regUrl = new URL(regEpr.getAddress().getStringValue());
					IClientConfiguration sp = ((RegistryNode) parent).getUASSecProps();
					RegistryClient regClient =  new RegistryClient(regUrl.toString(),regEpr,sp);
					List<EndpointReferenceType> tracers = regClient.listServices(ITrace.PORT);
					if(tracers == null || tracers.size() == 0)
					{
						TraceActivator.log(Status.ERROR,"Unable to find registry to query for tracer service!");
						return;
					}
					if(tracers.size() > 1)
					{
						TraceActivator.log(Status.WARNING,"Found more than one tracing service, using first one!");
					}
					tracerEpr = tracers.get(0);
				}
			}
			final String workflowId = wfNode.getWorkflowId();
			final String wfName =  wfNode.getName().split(WorkflowUtils.getSubmittedAtPrefix())[0].trim();


			if(tracerEpr != null)
			{
				
				IFile workflowFile = WorkflowUtils.findSubmittedWorkflowFile(workflowId, 5000, null);
				if(workflowFile != null) targetFolder = workflowFile.getParent();
				if(targetFolder == null)
				{
					// parent folder of submitted workflow not found -> use temp folder
					targetFolder = PathUtils.getTempProject().getFolder("message traces");
					if(!targetFolder.exists()) 
					{
						try {
							((IFolder) targetFolder).create(true, true, null);
						} catch (Exception e) {
							TraceActivator.log(Status.ERROR,"Unable to create folder for storing traced messages!");
							return;
						}
					}
				}
				final NodePath tracerPath = regNode.getPathToRoot().append(new URI(tracerEpr.getAddress().getStringValue()));

				PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable()
				{
					public void run() {
						IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
						try {

							IPath myPath = targetFolder.getLocation();
							myPath = myPath.append(wfName);
							myPath = myPath.addFileExtension(TracingConstants.TRACE_FILE_EXTENSION);
							IFile file = PathUtils.absolutePathToEclipseFile(myPath);
							if(!file.exists())
							{
								ByteArrayInputStream is = new ByteArrayInputStream(new byte[0]);
								file.create(is, true, null);
							}
							FileEditorInput input = new FileEditorInput(file);
							TraceEditor traceEditor = (TraceEditor) page.openEditor(input,"org.chemomentum.rcp.tracing.editor");
							IMessageQuery query = new PatternMessageQuery(ITrace.MESSAGE_ID, "%"+workflowId+"%");
							TraceQueryDescription queryDescription = new TraceQueryDescription(query,TraceQueryDescription.MODE_RESULT_SET);
							queryDescription.setAttributes(Arrays.asList(TracingConstants.DEFAULT_ATTRIBUTES));
							traceEditor.initialize(tracerPath,queryDescription);
							traceEditor.refresh();
						} catch (Exception e1) {
							TraceActivator.log("Could not open message trace: "+e1.getMessage(),e1);
						}
					}

				});
			}

		} catch (Exception e) {
			TraceActivator.log("Could not trace workflow!",e);
		}

	}

}
