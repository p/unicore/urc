/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.tracing;

import org.chemomentum.rcp.tracing.actions.RefreshTraceAction;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.ZoomComboContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.actions.RetargetAction;

import de.fzj.unicore.rcp.common.actions.DirectRetargetAction;
import de.fzj.unicore.rcp.common.actions.FancyEditorActionBarContributor;
import de.fzj.unicore.rcp.common.guicomponents.IEditorPartWithActions;

/**
 * Contributes actions to the Editor.
 * @author Daniel Lee
 */
public class TraceActionBarContributor extends FancyEditorActionBarContributor {

	private ActionRegistry registry = new ActionRegistry();


	public void contributeToToolBar(IToolBarManager toolBarManager) {
		super.contributeToToolBar(toolBarManager);
		toolBarManager.add( new ZoomComboContributionItem(getPage()));
		toolBarManager.update(true);
	}
	

	public void setActiveEditor(IEditorPart editor) {
		super.setActiveEditor(editor);
	}



	/**
	 * manage additional actions per editor instance
	 * @param editor
	 */
	@Override
	protected void buildEditorSpecificActions(IEditorPart edit)
	{
		addEditorSpecificAction(new DirectRetargetAction(RefreshTraceAction.ID,RefreshTraceAction.TEXT,RefreshTraceAction.IMAGE_DESCRIPTOR), true, true);
	}


	@Override
	protected void buildGlobalActions() {
		

	}




	/**
	 * Disposes the contributor. Removes all {@link RetargetAction}s that were {@link
	 * org.eclipse.ui.IPartListener}s on the {@link org.eclipse.ui.IWorkbenchPage} and 
	 * disposes them. Also disposes the action registry.
	 * <P>
	 * Subclasses may extend this method to perform additional cleanup.
	 * @see org.eclipse.ui.part.EditorActionBarContributor#dispose()
	 */
	public void dispose() {
		super.dispose();
		registry.dispose();
		registry = null;
	}

	/**
	 * Retrieves an action from the action registry using the given ID.
	 * @param id the ID of the sought action
	 * @return <code>null</code> or the action if found
	 */
	protected IAction getAction(IEditorPartWithActions editor,String id) {
		IAction result = getActionRegistry().getAction(id);
		if(result != null) return result;
		else return super.getAction(editor, id);
	}

	/**
	 * returns this contributor's ActionRegsitry.
	 * @return the ActionRegistry
	 */
	protected ActionRegistry getActionRegistry() {
		return registry;
	}



}
