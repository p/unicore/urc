package org.chemomentum.rcp.tracing.ui;

import java.util.ArrayList;
import java.util.List;

import org.chemomentum.tracer.client.IMessageQuery;
import org.eclipse.swt.widgets.Composite;

public abstract class MessageQueryUIRow {
	
	private String typeName;

	protected MessageQueryUIRow parentRow = null;
	protected List<MessageQueryUIRow> nestedQueryRows = new ArrayList<MessageQueryUIRow>();

	private int nestingLevel = 0;
	
	

	public MessageQueryUIRow(String queryType, int nestingLevel) {
		super();
		this.typeName = queryType;
		this.nestingLevel = nestingLevel;
	}

	public abstract IMessageQuery buildQuery();
	
	
	public abstract void createControls(Composite parent);
	
	
	public String getTypeName() {
		return typeName;
	}
	
	/**
	 * Used to perform some cleanup
	 */
	public void dispose()
	{
		
	}
	
	public void addNestedQueryRow(MessageQueryUIRow row)
	{
		nestedQueryRows.add(row);
		row.setParentRow(this);
	}
	
	public void addNestedQueryRow(int index,MessageQueryUIRow row)
	{
		nestedQueryRows.add(index,row);
		row.setParentRow(this);
	}
	
	public int removeNestedQueryRow(MessageQueryUIRow row)
	{
		int index = nestedQueryRows.indexOf(row);
		nestedQueryRows.remove(index);
		row.setParentRow(null);
		return index;
	}
	
	public List<MessageQueryUIRow> getNestedQueryRows() {
		return nestedQueryRows;
	}
	public int getNestingLevel() {
		return nestingLevel;
	}
	
	public void setNestedQueryRows(List<MessageQueryUIRow> nestedQueryRows) {
		this.nestedQueryRows = nestedQueryRows;
	}
	
	public void setNestingLevel(int nestingLevel) {
		this.nestingLevel = nestingLevel;
	}

	public MessageQueryUIRow getParentRow() {
		return parentRow;
	}

	public void setParentRow(MessageQueryUIRow parentRow) {
		this.parentRow = parentRow;
	}

}
