package org.chemomentum.rcp.tracing.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.chemomentum.tracer.client.AndMessageQuery;
import org.chemomentum.tracer.client.IMessageQuery;
import org.chemomentum.tracer.client.NotMessageQuery;
import org.chemomentum.tracer.client.OrMessageQuery;
import org.chemomentum.tracer.client.PatternMessageQuery;
import org.chemomentum.tracer.client.TimeMessageQuery;
import org.chemomentum.tracer.xmlbeans.ColumnNames;

public class MessageQueryUIRowFactory {

	private static MessageQueryUIRowFactory instance = null;
	private List<String> rowTypeNames = null;


	public static synchronized MessageQueryUIRowFactory getInstance() {
		if(instance == null) instance = new MessageQueryUIRowFactory();
		return instance;
	}


	public IMessageQuery createDefaultQuery()
	{
		return new PatternMessageQuery(ColumnNames.OPERATION.toString(), "");
	}

	public MessageQueryUIRow changeQueryType(MessageQueryUIRow oldQuery, String type)
	{

		if(MessageQueryUIRowPattern.TYPE_NAME.equals(type))
		{
			return createRowsFor(createDefaultQuery(),oldQuery.getNestingLevel());
		}
		else if(MessageQueryUIRowTime.TYPE_NAME.equals(type))
		{
			Calendar c = Calendar.getInstance();
			c.set(2010, 0, 1, 0, 0, 0);
			return createRowsFor(new TimeMessageQuery(c.getTimeInMillis(),System.currentTimeMillis()),oldQuery.getNestingLevel());
		}
		else if(MessageQueryUIRowOr.TYPE_NAME.equals(type))
		{

			if(oldQuery.getNestedQueryRows().size() > 0)
			{
				MessageQueryUIRow result = createRowsFor(new OrMessageQuery(null, null), oldQuery.getNestingLevel());

				for(MessageQueryUIRow nested : oldQuery.getNestedQueryRows())
				{
					result.addNestedQueryRow(nested);
				}
				return result;
			}
			else
			{
				return createRowsFor(new OrMessageQuery(oldQuery.buildQuery(), createDefaultQuery()), oldQuery.getNestingLevel());
			}
		}
		else if(MessageQueryUIRowAnd.TYPE_NAME.equals(type))
		{
			if(oldQuery.getNestedQueryRows().size() > 0)
			{
				MessageQueryUIRow result = createRowsFor(new AndMessageQuery(null, null), oldQuery.getNestingLevel());

				for(MessageQueryUIRow nested : oldQuery.getNestedQueryRows())
				{
					result.addNestedQueryRow(nested);
				}
				return result;
			}
			else
			{
				return createRowsFor(new AndMessageQuery(oldQuery.buildQuery(), createDefaultQuery()), oldQuery.getNestingLevel());
			}
		}
		return null;
	}

	public MessageQueryUIRow createRowsFor(IMessageQuery query, int nestingLevel)
	{
		query = normalizeQuery(query);
		if(query instanceof PatternMessageQuery)  return new MessageQueryUIRowPattern(query,nestingLevel);
		else if(query instanceof TimeMessageQuery)  return new MessageQueryUIRowTime(query,nestingLevel);
		else if(query instanceof NotMessageQuery)
		{
			NotMessageQuery not = (NotMessageQuery) query;
			if(not.getNegated() instanceof PatternMessageQuery) return new MessageQueryUIRowPattern(query,nestingLevel);
			else if(not.getNegated() instanceof TimeMessageQuery) return new MessageQueryUIRowTime(query,nestingLevel);
		}
		else if(query instanceof OrMessageQuery)
		{
			OrMessageQuery or = (OrMessageQuery) query;
			MessageQueryUIRowOr orRow = new MessageQueryUIRowOr(nestingLevel);
			List<IMessageQuery> leaves = new ArrayList<IMessageQuery>();
			Queue<IMessageQuery> queue = new ConcurrentLinkedQueue<IMessageQuery>();
			if(or.getLeft() != null) queue.add(or.getLeft());
			if(or.getRight() != null) queue.add(or.getRight());
			while(!queue.isEmpty())
			{
				IMessageQuery current = queue.poll();
				if(current instanceof OrMessageQuery) 
				{
					or = (OrMessageQuery) current;
					if(or.getLeft() != null) queue.add(or.getLeft());
					if(or.getRight() != null) queue.add(or.getRight());

				}
				else leaves.add(current);
			}
			for(IMessageQuery leave : leaves)
			{
				MessageQueryUIRow nested = createRowsFor(leave,nestingLevel+1);

				orRow.addNestedQueryRow(nested);

			}
			return orRow;
		}
		else if(query instanceof AndMessageQuery)
		{
			AndMessageQuery and = (AndMessageQuery) query;
			MessageQueryUIRowAnd andRow = null;
			andRow = new MessageQueryUIRowAnd(nestingLevel);
			List<IMessageQuery> leaves = new ArrayList<IMessageQuery>();
			Queue<IMessageQuery> queue = new ConcurrentLinkedQueue<IMessageQuery>();
			if(and.getLeft() != null) queue.add(and.getLeft());
			if(and.getRight() != null) queue.add(and.getRight());
			while(!queue.isEmpty())
			{
				IMessageQuery current = queue.poll();
				if(current instanceof AndMessageQuery) 
				{
					and = (AndMessageQuery) current;
					if(and.getLeft() != null) queue.add(and.getLeft());
					if(and.getRight() != null) queue.add(and.getRight());

				}
				else leaves.add(current);
			}
			for(IMessageQuery leave : leaves)
			{
				int nextNestingLevel = nestingLevel;
				nextNestingLevel++;
				MessageQueryUIRow nested = createRowsFor(leave,nextNestingLevel);
				andRow.addNestedQueryRow(nested);
			}
			return andRow;
		}
		return null;
	}
	/**
	 * Normalize the incoming message query by applying DeMorgan's law
	 * until negations only contain basic queries, i.e. pattern and time
	 * queries and no nested queries. 
	 * @param query
	 * @return
	 */
	protected IMessageQuery normalizeQuery(IMessageQuery query)
	{
		if(query instanceof NotMessageQuery)
		{
			NotMessageQuery not = (NotMessageQuery) query;
			IMessageQuery negated = not.getNegated();
			if(negated instanceof OrMessageQuery)
			{
				OrMessageQuery or = (OrMessageQuery) negated;
				IMessageQuery left = or.getLeft();
				IMessageQuery right = or.getRight();
				// apply DeMorgan's law
				AndMessageQuery and = new AndMessageQuery(new NotMessageQuery(left), new NotMessageQuery(right));
				// normalize recursively
				return normalizeQuery(and);
			}
			else if(negated instanceof AndMessageQuery)
			{
				AndMessageQuery and = (AndMessageQuery) negated;
				IMessageQuery left = and.getLeft();
				IMessageQuery right = and.getRight();
				// apply DeMorgan's law
				OrMessageQuery or = new OrMessageQuery(new NotMessageQuery(left), new NotMessageQuery(right));
				// normalize recursively
				return normalizeQuery(or);
			}
			else return query;
		}
		else return query;
	}

	public List<String> getRowTypeNames()
	{
		if(rowTypeNames == null) rowTypeNames = Arrays.asList(new String[]{MessageQueryUIRowPattern.TYPE_NAME,MessageQueryUIRowTime.TYPE_NAME, MessageQueryUIRowOr.TYPE_NAME,MessageQueryUIRowAnd.TYPE_NAME});
		return rowTypeNames;
	}


}
