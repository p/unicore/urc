/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.tracing.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.ui.views.properties.PropertyDescriptor;

/**
 * @author demuth
 *
 */
public class TracedMessage extends TracedEntity implements Comparable<TracedMessage>{

	private static final long serialVersionUID = 1L;
	
	protected TracedService source, target;
	
	protected boolean response = false;

	/**
	 * Property that holds the message timestamp (Calendar value)
	 */
	public static final String PROP_TIMESTAMP = "timestamp";
	
	/**
	 * Property that holds the message's operation type (String value)
	 */
	public static final String PROP_OPERATION = "operation";
	
	
	/**
	 * Property that holds the message source id (String value)
	 */
	public static final String PROP_SOURCE_ID = "source id";
	
	/**
	 * Property that holds the message source type (String value)
	 */
	public static final String PROP_SOURCE_TYPE = "source type";
	
	/**
	 * Property that holds the message target id (String value)
	 */
	public static final String PROP_TARGET_ID = "target id";
	
	/**
	 * Property that holds the message target type (String value)
	 */
	public static final String PROP_TARGET_TYPE = "target type";
	
	
	/**
	 * Property that holds the message's content (String value)
	 */
	public static final String PROP_CONTENT = "message";
	

	
	public TracedMessage(String id, TraceModel graph, TracedService source, TracedService target)
	{
		super(id,graph);
		this.source = source;
		this.target = target;
		setPropertyValue(PROP_SOURCE_ID, source.getID());
		setPropertyValue(PROP_SOURCE_TYPE, source.getType());
		setPropertyValue(PROP_TARGET_ID, target.getID());
		setPropertyValue(PROP_TARGET_TYPE, target.getType());
		source.addOutgoingMessage(this);
		target.addIncomingMessage(this);
		graph.addMessage(this);
	}





	@Override
	protected void addPropertyDescriptors() {
		super.addPropertyDescriptors();
		PropertyDescriptor descr = new PropertyDescriptor(PROP_OPERATION,PROP_OPERATION); 
		addPropertyDescriptor(descr);
		descr = new PropertyDescriptor(PROP_TIMESTAMP,PROP_TIMESTAMP); 
		descr.setLabelProvider(new LabelProvider()
		{
			public String getText(Object element) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
				return df.format(((Calendar) element).getTime());
			}
		}
		);
		addPropertyDescriptor(descr);
		descr = new PropertyDescriptor(PROP_SOURCE_ID,PROP_SOURCE_ID); 
		addPropertyDescriptor(descr);
		descr = new PropertyDescriptor(PROP_SOURCE_TYPE,PROP_SOURCE_TYPE); 
		addPropertyDescriptor(descr);
		descr = new PropertyDescriptor(PROP_TARGET_ID,PROP_TARGET_ID); 
		addPropertyDescriptor(descr);
		descr = new PropertyDescriptor(PROP_TARGET_TYPE,PROP_TARGET_TYPE); 
		addPropertyDescriptor(descr);
		
	}



	public String getContent() {
		return (String) getPropertyValue(PROP_CONTENT);
	}




	public String getOperation() {
		return (String) getPropertyValue(PROP_OPERATION);
	}





	public TracedService getSource() {
		return source;
	}





	public TracedService getTarget() {
		return target;
	}





	public Calendar getTimestamp() {
		return (Calendar) getPropertyValue(PROP_TIMESTAMP);
	}

	


	
	public boolean isResponse() {
		return response;
	}



	public void setContent(String content) {
		setPropertyValue(PROP_CONTENT, content);
	}



	public void setResponse(boolean inverted) {
		this.response = inverted;
	}



	public void setOperation(String operation) {
		setPropertyValue(PROP_OPERATION, operation);
	}

	public void setSource(TracedService source) {
		if(this.source != null) this.source.removeOutgoingMessage(this);
		this.source = source;
	}

	public void setTarget(TracedService target) {
		if(this.target != null) this.target.removeIncomingMessage(this);
		this.target = target;
	}

	public void setTimestamp(Calendar timestamp) {
		setPropertyValue(PROP_TIMESTAMP, timestamp);
	}





	public int compareTo(TracedMessage o) {
		return getTimestamp().compareTo(o.getTimestamp()); 
	}

	
	public TracedMessage clone() throws CloneNotSupportedException
	{
		TracedMessage result = (TracedMessage) super.clone();
		return result;
	}

	public String toString()
	{
		return getSource().getID()+" -- "+getOperation()+" --> "+getTarget().getID();
	}
	
}
