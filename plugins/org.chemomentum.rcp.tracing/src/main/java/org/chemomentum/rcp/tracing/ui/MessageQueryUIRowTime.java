package org.chemomentum.rcp.tracing.ui;

import java.util.Calendar;

import org.chemomentum.tracer.client.IMessageQuery;
import org.chemomentum.tracer.client.NotMessageQuery;
import org.chemomentum.tracer.client.TimeMessageQuery;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;

public class MessageQueryUIRowTime extends MessageQueryUIRow {

	public static final String TYPE_NAME = "Timestamp";
	
	private static final String IS_BETWEEN = "is between", IS_NOT_BETWEEN = "is not between";
	
	private static final String[] RELATIONS = new String[] {IS_BETWEEN, IS_NOT_BETWEEN};
	
	private TimeMessageQuery query;
	
	private Combo relationCombo;
	private DateTime startDate, startTime, endDate, endTime;
	
	private boolean negated = false;
	
	public MessageQueryUIRowTime(IMessageQuery query, int nestingLevel)
	{
		super(TYPE_NAME,nestingLevel);
		if(query instanceof NotMessageQuery)
		{
			negated = true;
			NotMessageQuery not = (NotMessageQuery) query;
			this.query = (TimeMessageQuery) not.getNegated();
		}
		else this.query = (TimeMessageQuery) query;
	}

	@Override
	public void createControls(Composite parent) {
		
		GridLayout layout = new GridLayout(6, false);
		layout.marginTop = 0;
		layout.marginBottom = 0;
		parent.setLayout(layout);
		relationCombo = new Combo(parent,SWT.READ_ONLY);
		relationCombo.setItems(RELATIONS);
		relationCombo.select(negated ? 1 : 0);
		relationCombo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(query.getStart());

		startDate = new DateTime(parent, SWT.DATE|SWT.DROP_DOWN);
		startDate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		startDate.setDate(c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
		
		startTime = new DateTime(parent, SWT.TIME|SWT.LONG);
		startTime.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		startTime.setHours(c.get(Calendar.HOUR_OF_DAY));
		startTime.setMinutes(c.get(Calendar.MINUTE));
		startTime.setSeconds(c.get(Calendar.SECOND));
		
		Label l2 = new Label(parent,SWT.NONE);
		l2.setText("and");
		l2.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		
		c = Calendar.getInstance();
		c.setTimeInMillis(query.getEnd());
		endDate = new DateTime(parent, SWT.DATE|SWT.DROP_DOWN);
		endDate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		endDate.setDate(c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
		
		endTime = new DateTime(parent, SWT.TIME|SWT.LONG);
		endTime.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		endTime.setHours(c.get(Calendar.HOUR_OF_DAY));
		endTime.setMinutes(c.get(Calendar.MINUTE));
		endTime.setSeconds(c.get(Calendar.SECOND));
	}

	@Override
	public IMessageQuery buildQuery() {
		Calendar c = Calendar.getInstance();
		c.set(startDate.getYear(),startDate.getMonth(), startDate.getDay(), startTime.getHours(), startTime.getMinutes(),startTime.getSeconds());
		query.setStart(c.getTimeInMillis());
		c.set(endDate.getYear(),endDate.getMonth(), endDate.getDay(), endTime.getHours(), endTime.getMinutes(),endTime.getSeconds());
		query.setEnd(c.getTimeInMillis());
		String relation = RELATIONS[relationCombo.getSelectionIndex()];
		negated = IS_NOT_BETWEEN.equals(relation);
		if(negated) return new NotMessageQuery(query);
		else return query;
		
	}
	
}
