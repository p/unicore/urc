package org.chemomentum.rcp.tracing.handlers;

import org.chemomentum.rcp.tracing.TraceEditor;
import org.chemomentum.rcp.tracing.model.TraceModel;
import org.chemomentum.rcp.tracing.ui.MessageQueryDialog;
import org.chemomentum.tracer.client.TraceQueryDescription;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.ui.ISources;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class SetQueryHandler extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public SetQueryHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IEvaluationContext c = (IEvaluationContext) event.getApplicationContext();
		Object editor = c.getVariable(ISources.ACTIVE_EDITOR_NAME);
		if(!(editor instanceof TraceEditor)) return null;
		TraceEditor traceEditor = (TraceEditor) editor;
		TraceModel model = traceEditor.getContents();
		TraceQueryDescription queryDescription = model.getTraceQueryDescription();
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		MessageQueryDialog dlg = new MessageQueryDialog(window.getShell(),queryDescription.clone());
		int result = dlg.open();
		if(Dialog.OK == result)
		{
			TraceQueryDescription query = dlg.getQuery(); 
			model.setTraceQueryDescription(query);
			traceEditor.refresh();
		}
		return null;
	}
}
