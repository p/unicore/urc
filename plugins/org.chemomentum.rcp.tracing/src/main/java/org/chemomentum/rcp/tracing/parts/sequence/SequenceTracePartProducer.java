package org.chemomentum.rcp.tracing.parts.sequence;

import org.chemomentum.rcp.tracing.model.TimeScaleModel;
import org.chemomentum.rcp.tracing.model.TraceModel;
import org.chemomentum.rcp.tracing.model.TracedMessage;
import org.chemomentum.rcp.tracing.model.TracedService;
import org.chemomentum.rcp.tracing.parts.ITracePartProducer;
import org.eclipse.gef.EditPart;

public class SequenceTracePartProducer implements ITracePartProducer{
	public EditPart createEditPart(EditPart context, Object model) {
		
		EditPart editPart=null;
		
		if (model instanceof TraceModel)
		{	
			editPart = new SequenceTraceGraphPart();
			editPart.setModel(model);
		}
		else if (model instanceof TracedMessage) 
		{
			editPart = new SequenceTraceEdgePart();
			editPart.setModel(model);
		}
		else if (model instanceof TimeScaleModel) 
		{
			editPart = new TimeScalePart();
			editPart.setModel(model);
		}
		else if (model instanceof TracedService)
		{
			editPart = new SequenceTraceNodeServicePart();
			editPart.setModel(model);
		}
		
		return editPart;
		
		
	}
}
