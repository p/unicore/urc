package org.chemomentum.rcp.tracing.utils;

import org.chemomentum.tracer.xmlbeans.EntryDocument.Entry;

public class MessageUtils {
	
	/**
	 * Find out whether the given traced entry is a response
	 * to a previous message. This is hackish and should be avoided asap.
	 * @param e
	 * @return
	 */
	public static boolean isResponse(Entry e)
	{
		String operation = e.getOperation();
		return operation.toLowerCase().contains("finished")
				|| operation.toLowerCase().contains("failed") || operation.toLowerCase().contains("submitted");
	}

}
