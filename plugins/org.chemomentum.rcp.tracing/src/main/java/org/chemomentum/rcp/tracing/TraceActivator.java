package org.chemomentum.rcp.tracing;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.fzj.unicore.rcp.logmonitor.LogActivator;

/**
 * The activator class controls the plug-in life cycle
 */
public class TraceActivator extends AbstractUIPlugin implements TracingConstants{

	
	// The plug-in ID
	public static final String PLUGIN_ID = "org.chemomentum.rcp.tracing";
	public final static String ICONS_PATH = "$nl$/icons/";//$NON-NLS-1$

	// The shared instance
	private static TraceActivator plugin;
	
	/**
	 * The constructor
	 */
	public TraceActivator() {
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static TraceActivator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID,ICONS_PATH+path);
	}
	
	public String[] getServiceTypes()
	{
		//TODO make this extensible via an extension point?
		return SERVICE_TYPES;
	}
	
	public ImageDescriptor getImageDescrForService(String serviceType)
	{
		String pathToImage = "";
		//TODO make this extensible via an extension point?
		if(SERVICE_TYPE_CLIENT.equals(serviceType)) pathToImage = "user.gif";
		else if(SERVICE_TYPE_WORKFLOW_ENGINE.equals(serviceType)) pathToImage = "WFEngine.gif";
		else if(SERVICE_TYPE_ORCHESTRATOR.equals(serviceType)) pathToImage = "ServiceOrchestrator.gif";
		else if(SERVICE_TYPE_TARGET_SYSTEM.equals(serviceType)) pathToImage = "TargetSystem.gif";
		return getImageDescriptor(pathToImage);
	}
	
	/** 
	 * Log a message via Eclipse logging
	 * Defaults to INFO Level 
	 * @param msg the message to log
	 */
	public static void log(String msg) {
		log(msg, null);
	}
	/** 
	 * Log a message via Eclipse logging
	 * Defaults to INFO Level 
	 * @param msg the message to log
	 * @param e stacktrace to append
	 */
	public static void log(String msg, Exception e) {
		log(Status.INFO, msg, e, false);
	}
	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, false);
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param e stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, false);
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param forceAlarmUser forces the logging plugin to open a popup; usually, this should NOT be set to true!
	 */
	public static void log(int status, String msg, boolean forceAlarmUser) {
		log(status, msg, null, forceAlarmUser);
	}
	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param e stacktrace to append
	 * @param forceAlarmUser forces the logging plugin to open a popup; usually, this should NOT be set to true!
	 */
	public static void log(int status, String msg, Exception e, boolean forceAlarmUser) {
		IStatus s = new Status(status, PLUGIN_ID, Status.OK, msg, e);
		LogActivator.log(plugin, s,forceAlarmUser);
	}
}
