package org.chemomentum.rcp.tracing.policies;

import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.rcp.tracing.model.commands.SwitchViewCommand;
import org.chemomentum.rcp.tracing.requests.SwitchViewRequest;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;

public class SwitchViewEditPolicy extends TraceEditPolicy {
	
	public Command getCommand(Request request) {
		if(TracingConstants.REQ_SWITCH_VIEW.equals(request.getType()))
		{
			SwitchViewRequest req = (SwitchViewRequest) request;
			return new SwitchViewCommand(getTraceModel(), req.getViewId());
		}
		return null;
	}

}
