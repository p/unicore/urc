/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.tracing.parts.hierachic.commands;

import de.fzj.unicore.rcp.wfeditor.model.transitions.SerializableBendpoint;



public class CreateBendpointCommand 
	extends BendpointCommand 
{

public void execute() {
	SerializableBendpoint bp = new SerializableBendpoint(getLocation().x,getLocation().y);
	getTraceEdge().insertBendpoint(getIndex(), bp);
	super.execute();
}

public void undo() {
	super.undo();
	getTraceEdge().removeBendpoint(getIndex());
}

}


