package org.chemomentum.rcp.tracing.persistence;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Arrays;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.chemomentum.rcp.tracing.TraceActivator;
import org.chemomentum.rcp.tracing.model.TraceModel;
import org.chemomentum.rcp.tracing.model.TracedMessage;
import org.chemomentum.rcp.tracing.model.TracedService;
import org.chemomentum.rcp.tracing.utils.MessageUtils;
import org.chemomentum.tracer.client.QueryToXMLConverter;
import org.chemomentum.tracer.client.TraceQueryDescription;
import org.chemomentum.tracer.client.XMLToQueryConverter;
import org.chemomentum.tracer.xmlbeans.EntryDocument;
import org.chemomentum.tracer.xmlbeans.TraceDiagramDocument;
import org.chemomentum.tracer.xmlbeans.TraceQueryDocument;
import org.chemomentum.tracer.xmlbeans.EntryDocument.Entry;
import org.chemomentum.tracer.xmlbeans.TraceDiagramDocument.TraceDiagram;
import org.chemomentum.tracer.xmlbeans.TraceDocument.Trace;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Status;

import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.logmonitor.utils.ReverseFileReader;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;

public class TraceSerializer {

	/**
	 * Serializes a {@link TraceModel} to a file.
	 * @param trace
	 * @param path the file path where the model should be written
	 * @param progress
	 * @throws Exception
	 */
	public static void serialize(TraceModel trace, IPath path, IProgressMonitor progress) throws Exception
	{
		File f = path.toFile();
		FileOutputStream os = new FileOutputStream(f);
		serialize(trace, new BufferedOutputStream(os),progress);
	}

	/**
	 * Merges all messages in the given {@link TraceModel} with the messages that are already stored
	 * in a file. Any additional properties of the model are ignored, this method ONLY adds the set 
	 * of messages. In case there is no {@link TraceModel} to be found at the given path, this method
	 * simply calls the {@link #serialize(TraceModel, IPath, IProgressMonitor)} method for creating the
	 * file.
	 * @param toMerge
	 * @param path
	 * @param progress
	 * @throws Exception
	 */
	public static void addMessages(TraceModel toAdd, IPath path, IProgressMonitor progress) throws Exception
	{



		try {
			File f = path.toFile();
			if(!f.exists()) serialize(toAdd, path, progress);
			else 
			{
				// in order to append to the XML file, we need to remove the closing tag
				// and readd it later

				// read last lines
				ReverseFileReader reverseReader = new ReverseFileReader(f);
				String last = reverseReader.readLine();
				String lastButOne = reverseReader.readLine();
				String closingTags = lastButOne+"\n"+last;
				reverseReader.close();

				// remove them from file
				RandomAccessFile raf = new RandomAccessFile(f, "rw");
				long length = raf.length();
				raf.setLength(length - closingTags.length());
				raf.close();

				closingTags = "\n"+closingTags;

				progress = ProgressUtils.getNonNullMonitorFor(progress);

				FileOutputStream os = new FileOutputStream(f,true);
				try {


					TracedMessage[] msgs = toAdd.getMessages();
					Arrays.sort(msgs);
					progress.beginTask("adding messages",msgs.length);
					XmlOptions options = new XmlOptions();
					options = options.setSaveNoXmlDecl();
					options = options.setSavePrettyPrint();
					for(TracedMessage msg : msgs)
					{
						EntryDocument entry = msgToEntry(msg);

						entry.save(os,options);
						progress.worked(1);
					}
					os.write(closingTags.getBytes(), 0, closingTags.length());
				} finally{
					os.close();
				}
			}
		} finally {
			progress.done();
		}
		
	}

	/**
	 * Deserialize a {@link TraceModel} from a file. only deserialize messages that match the given
	 * query. This can save a lot of memory if only a subset of stored messages is needed.
	 * @param path
	 * @param query
	 * @param maxNumMessages drop all messages that exceed this number; this is used to put a hard restriction on the memory consumption.
	 * @param progress
	 * @return
	 * @throws Exception
	 */
	public static TraceModel deserialize(IPath path, TraceQueryDescription query, int maxNumMessages, IProgressMonitor progress) throws Exception
	{
		progress = ProgressUtils.getNonNullMonitorFor(progress);
		File f = path.toFile();
		TraceDiagram diagram = TraceDiagramDocument.Factory.parse(f).getTraceDiagram();
		Trace t = diagram.getTrace();
		progress.beginTask("deserializing trace", t.getEntryArray().length);
		TraceModel result = new TraceModel();
		for(Entry e : t.getEntryArray())
		{
			entryToMsg(result, e);
			progress.worked(1);
		}
		result.setTracerAddress(NodePath.parseString(diagram.getTracerAddress()));
		TraceQueryDocument traceQueryDoc = TraceQueryDocument.Factory.newInstance();
		traceQueryDoc.setTraceQuery(diagram.getTraceQuery());
		result.setTraceQueryDescription(XMLToQueryConverter.convert(traceQueryDoc));
		result.setViewID(diagram.getViewId());
		return result;
	}



	private static void serialize(final TraceModel trace, final OutputStream os,IProgressMonitor progress)
	{

		try {
			TracedMessage[] msgs = trace.getMessages();
			progress = ProgressUtils.getNonNullMonitorFor(progress);
			progress.beginTask("Saving traced messages", msgs.length);
			Arrays.sort(msgs);
			TraceDiagramDocument doc = TraceDiagramDocument.Factory.newInstance();
			TraceDiagram diagram = doc.addNewTraceDiagram();
			diagram.setTracerAddress(trace.getTracerAddress().toString());
			diagram.setViewId(trace.getViewID());
			TraceQueryDocument query = QueryToXMLConverter.convert(trace.getTraceQueryDescription());
			diagram.setTraceQuery(query.getTraceQuery());
			Trace t = diagram.addNewTrace();
			int i = 0;
			for(TracedMessage msg : msgs)
			{
				t.addNewEntry();
				t.setEntryArray(i++, msgToEntry(msg).getEntry());
				progress.worked(1);
			}
			XmlOptions options = new XmlOptions();
			options = options.setSavePrettyPrint();
			doc.save(os,options);
		} catch (Exception e) {
			TraceActivator.log(Status.ERROR,"Unable to serialize traced messages: "+e.getMessage(),e);
		}
		finally {
			progress.done();
			if(os != null)
				try {
					os.close();
				} catch (IOException e) {
				}
		}

	}



	private static EntryDocument msgToEntry(TracedMessage msg) throws XmlException
	{
		EntryDocument doc = EntryDocument.Factory.newInstance();
		Entry e = doc.addNewEntry();
		e.setMessageId(msg.getID());
		e.setOperation(msg.getOperation());
		e.setSourceID(msg.getSource().getID());
		e.setSourceType(msg.getSource().getType());
		e.setTargetID(msg.getTarget().getID());
		e.setTargetType(msg.getTarget().getType());
		e.setTimeStamp(msg.getTimestamp());
		String content = msg.getContent();
		if(content != null) 
		{
			XmlObject xml = XmlObject.Factory.parse(content);
			e.setMessage(xml);
		}
		return doc;
	}

	private static void  entryToMsg(TraceModel graph, Entry e) throws XmlException
	{

		TracedService source = graph.getService(e.getSourceID());
		if(source == null) source = new TracedService(e.getSourceID(), graph, e.getSourceType());
		TracedService target = graph.getService(e.getTargetID());
		if(target == null) target = new TracedService(e.getTargetID(), graph, e.getTargetType());
		String id = e.getMessageId();
		if(id.startsWith("WA ID: ")) 
		{
			id = id.substring(7);
		}
		TracedMessage msg = new TracedMessage(id, graph, source, target);
		msg.setOperation(e.getOperation());
		msg.setTimestamp(e.getTimeStamp());
		if(e.getMessage() != null) msg.setContent(e.getMessage().toString());
		msg.setResponse(MessageUtils.isResponse(e)); // TODO this is a hack! Find a proper way to (de-)serialize this boolean!
	}

}
