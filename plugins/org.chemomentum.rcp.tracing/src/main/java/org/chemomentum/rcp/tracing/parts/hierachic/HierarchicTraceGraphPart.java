/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.tracing.parts.hierachic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.rcp.tracing.model.ITraceModelListener;
import org.chemomentum.rcp.tracing.model.TraceModel;
import org.chemomentum.rcp.tracing.model.TracedMessage;
import org.chemomentum.rcp.tracing.model.TracedService;
import org.chemomentum.rcp.tracing.parts.ITracePart;
import org.chemomentum.rcp.tracing.policies.SwitchViewEditPolicy;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.draw2d.graph.Subgraph;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.Request;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

/**
 * @author demuth
 */
public class HierarchicTraceGraphPart extends AbstractGraphicalEditPart implements ITracePart, ITraceModelListener {

	public static final String KEY_SUBGRAPH_CLIENTS = "CL";
	public static final String KEY_SUBGRAPH_ORCHESTRATORS = "SO";
	public static final String KEY_SUBGRAPH_TARGET_SYSTEMS = "TS";
	public static final String KEY_SUBGRAPH_WORKFLOW_ENGINES = "WE";
	
	static final Insets PADDING = new Insets(70, 50, 70, 50);
	static final Insets INNER_PADDING = new Insets(0);
	
	private Map<String,List<TracedMessage>> messageMap = new HashMap<String, List<TracedMessage>>();

	public void setModel(Object o)
	{
		super.setModel(o);
		messagesAdded(getModel().getMessages());
	}
	
	public void activate() {
		super.activate();
		getModel().addTraceModelListener(this);
	}
	
	public void deactivate() {
		super.deactivate();
		getModel().removeTraceModelListener(this);
		messageMap.clear();
	}
	
	public void messagesAdded(TracedMessage[] msgs) {
		for (TracedMessage msg : msgs) {
			String key = getKey(msg);
			List<TracedMessage> list = messageMap.get(key);
			if(list == null) list = new ArrayList<TracedMessage>();
			list.add(msg);
			messageMap.put(key, list);
		}
		
	}

	public void messagesRemoved(TracedMessage[] msgs) {
		for (TracedMessage msg : msgs) {
			String key = getKey(msg);
			List<TracedMessage> list = messageMap.get(key);
			if(list != null)
			{
				list.remove(msg);
			}
			if(list == null || list.size() == 0) messageMap.remove(key);
		}
		
	}

	public void servicesAdded(TracedService[] services) {
		// do nothing
		
	}

	public void servicesRemoved(TracedService[] services) {
		// do nothing
		
	}
	
	protected List getModelChildren() {
		return ((TraceModel) getModel()).getChildren();
	}
	
	protected void createEditPolicies() {
		installEditPolicy(TracingConstants.REQ_SWITCH_VIEW, new SwitchViewEditPolicy());
	}

	protected IFigure createFigure() {
		Figure f = new Figure() {
			public void setBounds(Rectangle rect) {
				int x = bounds.x,
				y = bounds.y;

				boolean resize = (rect.width != bounds.width) || (rect.height != bounds.height),
				translate = (rect.x != x) || (rect.y != y);

				if (isVisible() && (resize || translate))
					erase();
				if (translate) {
					int dx = rect.x - x;
					int dy = rect.y - y;
					primTranslate(dx, dy);
				}
				bounds.width = rect.width;
				bounds.height = rect.height;
				if (resize || translate) {
					fireMoved();
					repaint();
				}
			}
		};
		f.setLayoutManager(new GraphLayoutManager(this));
		return f;
	}
	
	public void contributeNodesToGraph(CompoundDirectedGraph graph, Subgraph s, Map map) {
//		GraphAnimation.recordInitialState(getContentPane());
		Subgraph me = new Subgraph(this, s);
//		me.rowOrder = getActivity().getSortIndex();
		me.outgoingOffset = 5;
		me.incomingOffset = 5;
		IFigure fig = getFigure();
		
		me.innerPadding = INNER_PADDING;
		me.setPadding(PADDING);
		graph.setDefaultPadding(PADDING);
		map.put(this, me);
		graph.nodes.add(me);
		
//		subgraphs = new Subgraph[4];
//		clients = null;
//		wfEngines = null;
//		orchestrators = null;
//		targetSystems = null;
		
		for (Object o : getChildren()) {
			if(o instanceof HierarchicTraceNodePart)
			{
				HierarchicTraceNodePart part = (HierarchicTraceNodePart)o;
				part.contributeNodesToGraph(graph, me, map);
			}
		}
	}
	
	
	public void contributeEdgesToGraph(CompoundDirectedGraph graph, Subgraph s, Map map) {
		for (Object o : getChildren()) {
			if(o instanceof HierarchicTraceNodePart)
			{
				HierarchicTraceNodePart part = (HierarchicTraceNodePart)o;
				part.contributeEdgesToGraph(graph, s, map);
			}
		}
	}
	
	
	protected void applyLayoutToChildren(CompoundDirectedGraph graph, Map map) {
		for (int i = 0; i < getChildren().size(); i++) {
			HierarchicTraceNodePart part = (HierarchicTraceNodePart)getChildren().get(i);
			part.applyLayout(graph, map);
		}
	}

	protected void applyLayout(CompoundDirectedGraph graph, Map map) {
		Node n = (Node)map.get(this);
		getFigure().setBounds(new Rectangle(n.x, n.y, n.width, n.height));
		applyLayoutToChildren(graph, map);
	}

	
	public boolean isSelectable() {
		return false;
	}





	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditPart#getDragTracker(org.eclipse.gef.Request)
	 */
	public DragTracker getDragTracker(Request request) {
		// TODO Check whether the super class method can be called.
		return null;
	}

	
	public HierarchicTraceEdgePart getPartForMessage(TracedMessage msg)
	{
		String key = getKey(msg);
		return (HierarchicTraceEdgePart) getViewer().getEditPartRegistry().get(key);
	}
	
	/**
	 * Returns all messages that share the given key.
	 * @param key
	 * @return
	 */
	public List<TracedMessage> getMessagesWithKey(String key)
	{
		
		List<TracedMessage> result = messageMap.get(key);
		if(result == null) result = new ArrayList<TracedMessage>();
		return result;
	}
	
	public TraceModel getModel() {
		return (TraceModel) super.getModel();
	}
	
	public TraceModel getTraceModel() {
		return getModel();
	}
	
	public String getKey(TracedMessage msg)
	{
		return msg.getSource().getID()+msg.getTarget().getID()+msg.getOperation();
	}
	
	public String getViewId() {
		return TracingConstants.VIEW_ID_HIERARCHIC_GRAPH;
	}

	
}
