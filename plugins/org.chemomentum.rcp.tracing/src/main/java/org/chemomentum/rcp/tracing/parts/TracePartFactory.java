/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.tracing.parts;

import java.util.HashMap;
import java.util.Map;

import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.rcp.tracing.model.TraceModel;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;


/**
 * Used to build the controller layer of our MVC-enabled user interface.
 * In order to implement different views, different controller layers 
 * are used in GEF, each controller layer implementation corresponds 
 * to a different view. Thus, this factory delegates to different 
 * {@link ITracePartProducer} implementations for creating the appropriate
 * controller layer.
 * @author demuth
 *
 */
public class TracePartFactory implements EditPartFactory{

	Map<String, ITracePartProducer> producers = new HashMap<String, ITracePartProducer>();
	Map<String, String> viewNames = new HashMap<String, String>();

	public TracePartFactory()
	{
		iterateOverExtensions();
	}

	public EditPart createEditPart(EditPart context, Object model) {
		String viewId = null;
		if(context == null)
		{
			if(!(model instanceof TraceModel)) return null;
			else viewId = ((TraceModel) model).getViewID();

		}
		else {
			if(!(context instanceof ITracePart)) return null;
			viewId = ((ITracePart) context).getViewId();
		}
		ITracePartProducer producer = getProducer(viewId);
		if(producer == null) 
		{
			viewId = TracingConstants.VIEW_ID_HIERARCHIC_GRAPH; // fallback
			producer = getProducer(viewId);
		}

		return producer.createEditPart(context, model);
	}

	public String[] getAllViewIds()
	{
		return producers.keySet().toArray(new String[producers.size()]);
	}

	public String getNameFor(String viewId)
	{
		ITracePartProducer producer = getProducer(viewId);
		if(producer == null) return null;
		return viewNames.get(viewId);
	}

	private ITracePartProducer getProducer(String viewId)
	{
		if(viewId == null) return null;
		ITracePartProducer producer = producers.get(viewId);

		return producer;
	}

	private void iterateOverExtensions()
	{
		// iterate over extensions and find right extension to create the node
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry.getExtensionPoint(TracingConstants.EXTENSION_POINT_VIEW_TYPES);
		IConfigurationElement[] members = extensionPoint.getConfigurationElements();
		// For each extension
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			IExtension ext = member.getDeclaringExtension();

			try {
				ITracePartProducer producer = (ITracePartProducer) member.createExecutableExtension("partProducer");
				producers.put(ext.getUniqueIdentifier(), producer);
				viewNames.put(ext.getUniqueIdentifier(), ext.getLabel());


			}
			catch (CoreException ex) {
				// do nothing
			}
		}
	}


}
