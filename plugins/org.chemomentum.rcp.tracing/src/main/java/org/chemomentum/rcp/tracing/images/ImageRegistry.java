package org.chemomentum.rcp.tracing.images;

import org.chemomentum.rcp.tracing.TraceActivator;
import org.eclipse.jface.resource.ImageDescriptor;

public class ImageRegistry {

	public static ImageDescriptor getImageDescriptorForService(String typeId)
	{
		if(typeId == null) return null;
		String imagePath = typeId +".gif";
		return TraceActivator.getImageDescriptor(imagePath);
	}

}
