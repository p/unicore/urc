package org.chemomentum.rcp.tracing.ui.contributions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.chemomentum.rcp.tracing.TraceEditor;
import org.chemomentum.rcp.tracing.TraceViewer;
import org.chemomentum.rcp.tracing.handlers.SwitchViewHandler;
import org.chemomentum.rcp.tracing.parts.TracePartFactory;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.CompoundContributionItem;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;

public class SwitchViewContributionItem extends CompoundContributionItem {

	public SwitchViewContributionItem() {

	}

	public SwitchViewContributionItem(String id) {
		super(id);

	}
	

	
	@Override
	protected IContributionItem[] getContributionItems() {
		
		TraceEditor editor = (TraceEditor) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		TraceViewer viewer = editor.getGraphicalViewer();
		// Here's where you would dynamically generate your list
        List<IContributionItem> list = new ArrayList<IContributionItem>();
        TracePartFactory factory = viewer.getEditPartFactory();
		String[] viewIds = factory.getAllViewIds();
		
		
		
		
		
		
		for (int i = 0; i < viewIds.length; i++) {
			Map<String,Object> params = new HashMap<String, Object>();
			String viewId = viewIds[i];
			String viewName = factory.getNameFor(viewId);
			
			params.put(SwitchViewHandler.PARAM_VIEW_ID, viewId);
			CommandContributionItemParameter param = new CommandContributionItemParameter(editor.getSite(),viewId,
	                "org.chemomentum.rcp.tracing.commands.switchViewCommand",
	                params, 
	                null, null, null, 
	                viewName, null,"tooltip", 
	                CommandContributionItem.STYLE_CHECK, null, false);
			CommandContributionItem item = new CommandContributionItem(param);
			list.add(item);
			
		}
       

      
        return list.toArray(new IContributionItem[list.size()]);
	}

}
