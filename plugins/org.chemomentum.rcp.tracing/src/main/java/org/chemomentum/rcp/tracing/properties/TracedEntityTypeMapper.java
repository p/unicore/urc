package org.chemomentum.rcp.tracing.properties;

import org.eclipse.gef.EditPart;
import org.eclipse.ui.views.properties.tabbed.AbstractTypeMapper;

public class TracedEntityTypeMapper extends AbstractTypeMapper {
	 public Class<?> mapType(Object object) {
	        EditPart part = (EditPart) object;
	        return part.getModel().getClass();
	    }
}
