package org.chemomentum.rcp.tracing.model;

/**
 * Interface to be implemented if you are interested in changes within a {@link TraceModel}
 * @author bdemuth
 *
 */
public interface ITraceModelListener {

	
	public void messagesAdded(TracedMessage[] msgs);
	
	public void messagesRemoved(TracedMessage[] msgs);
	
	public void servicesAdded(TracedService[] services);
	
	public void servicesRemoved(TracedService[] services);
}
