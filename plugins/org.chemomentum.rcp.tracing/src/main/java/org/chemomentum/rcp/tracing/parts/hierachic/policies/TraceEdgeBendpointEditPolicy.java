/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.tracing.parts.hierachic.policies;

import org.chemomentum.rcp.tracing.parts.hierachic.HierarchicTraceEdgePart;
import org.chemomentum.rcp.tracing.parts.hierachic.commands.BendpointCommand;
import org.chemomentum.rcp.tracing.parts.hierachic.commands.CreateBendpointCommand;
import org.chemomentum.rcp.tracing.parts.hierachic.commands.DeleteBendpointCommand;
import org.chemomentum.rcp.tracing.parts.hierachic.commands.MoveAllBendpointsCommand;
import org.chemomentum.rcp.tracing.parts.hierachic.commands.MoveBendpointCommand;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.BendpointRequest;
import org.eclipse.gef.requests.ChangeBoundsRequest;


public class TraceEdgeBendpointEditPolicy 
extends org.eclipse.gef.editpolicies.BendpointEditPolicy
{

	
	public boolean understandsRequest(Request req) {
		if(REQ_MOVE.equals(req.getType())) return true;
		return super.understandsRequest(req);
	}
	
	public Command getCommand(Request request) {
		if (REQ_MOVE.equals(request.getType())) {
			return getMoveAllBendpointsCommand((ChangeBoundsRequest)request);
		}
		else return super.getCommand(request);
		
	}
	
	protected Command getCreateBendpointCommand(BendpointRequest request) {
		CreateBendpointCommand com = new CreateBendpointCommand();
		Point p = request.getLocation();
		Connection conn = getConnection();

		conn.translateToRelative(p);

		com.setLocation(p);
		com.setTraceEdge((HierarchicTraceEdgePart)request.getSource());
		com.setIndex(request.getIndex());
		return com;
	}

	protected Command getMoveBendpointCommand(BendpointRequest request) {
		MoveBendpointCommand com = new MoveBendpointCommand();
		Point p = request.getLocation();
		Connection conn = getConnection();
		conn.translateToRelative(p);
		com.setLocation(p);
		com.setTraceEdge((HierarchicTraceEdgePart)request.getSource());
		com.setIndex(request.getIndex());
		return com;
	}
	
	protected Command getMoveAllBendpointsCommand(ChangeBoundsRequest request) {
		HierarchicTraceEdgePart t = (HierarchicTraceEdgePart)getHost();
		boolean hasBendpoints = t.getBendpoints() != null && t.getBendpoints().size() > 0;
		if(!hasBendpoints) return new Command(){}; // do nothing
		MoveAllBendpointsCommand result = new MoveAllBendpointsCommand(t,getDeltaFor(request, (GraphicalEditPart)getHost()));
		return result;
	}

	protected Command getDeleteBendpointCommand(BendpointRequest request) {
		BendpointCommand com = new DeleteBendpointCommand();
		Point p = request.getLocation();
		com.setLocation(p);
		com.setTraceEdge((HierarchicTraceEdgePart)request.getSource());
		com.setIndex(request.getIndex());
		return com;
	}
	
	
	protected Point getDeltaFor(ChangeBoundsRequest request, GraphicalEditPart child) {
		Point ref = child.getFigure().getBounds().getTopLeft();
		Point p = ref.getCopy();
		child.getFigure().translateToAbsolute(p);
		p.translate(request.getMoveDelta());
		child.getFigure().translateToRelative(p);
//		rect.translate(getLayoutOrigin().getNegated());

		return p.translate(ref.getNegated());
	}

}
