/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.tracing.model.commands;

import org.chemomentum.rcp.tracing.model.TraceModel;
import org.eclipse.gef.commands.Command;

/**
 * @author demuth
 */
public class SwitchViewCommand extends Command {

	private TraceModel traceModel;
	private String oldViewId;
	private String viewId;

	


	public SwitchViewCommand(TraceModel traceModel, String viewId) {
		super();
		this.traceModel = traceModel;
		this.viewId = viewId;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	public void execute() {
		oldViewId = traceModel.getViewID();
		redo();
	}

	public void redo() {
		traceModel.setViewID(viewId);
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	public void undo() {
		traceModel.setViewID(oldViewId);
	}

	public TraceModel getTraceModel() {
		return traceModel;
	}

	public void setTraceModel(TraceModel traceModel) {
		this.traceModel = traceModel;
	}

	public String getViewId() {
		return viewId;
	}

	public void setViewId(String viewId) {
		this.viewId = viewId;
	}



}
