/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.tracing.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.chemomentum.rcp.tracing.TraceActivator;
import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.rcp.tracing.parts.TracePartFactory;
import org.chemomentum.tracer.client.TraceQueryDescription;
import org.eclipse.core.runtime.Status;

import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;


/**
 * @author demuth
 *
 */
public class TraceModel extends PropertySource {

	private static final long serialVersionUID = 1L;

	/**
	 * This property holds an identifier for the currently selected view on the model (String value).
	 * When the user wants to see a different view, this value is changed
	 * and our {@link TracePartFactory} is used for creating the associated view.  
	 */
	public static final String PROP_VIEW_ID = "view id";


	/** 
	 * The query that was used to retrieve the messages inside this model ({@link TraceQueryDescription} value).
	 */
	public static final String PROP_TRACE_QUERY = "trace query";

	/**
	 * We put a hard limit on the number of messages that should be kept in memory,
	 * see {@link TracingConstants#HARD_MESSAGE_LIMIT}. In case the query yields
	 * more results, this number will reflect how many messages have been discarded
	 * in order not to exceed the limit (Integer value).
	 */
	public static final String PROP_NUM_OMITTED_MESSAGES = "omitted messages";

	/**
	 * Timestamp stating when the tracing service was queried for the messages
	 * that are included in this model. (Long value, milliseconds since 1970).
	 */
	public static final String PROP_LAST_UPDATED = "last updated";

	private List<TracedService> services = new ArrayList<TracedService>();
	private Map<String,TracedService> serviceMap = new HashMap<String, TracedService>();
	private List<TracedMessage> messages = new ArrayList<TracedMessage>();
	private Map<String,TracedMessage> messageMap = new HashMap<String, TracedMessage>();
	private NodePath tracerAddress = null;
	private String modelVersion;
	
	private List<ITraceModelListener> modelListeners = new ArrayList<ITraceModelListener>();

	public TraceModel()
	{
		setLastUpdated(System.currentTimeMillis());
		setViewID(TracingConstants.VIEW_ID_HIERARCHIC_GRAPH);
		setNumOmittedMessages(0);
		activate();
	}

	public void addMessage(TracedMessage e)
	{
		addMessage(e,true);
	}
	
	protected void addMessage(TracedMessage e, boolean fireEvent)
	{
		synchronized (services) {
			messages.add(e);
			messageMap.put(e.getID(), e);
		}
		if(fireEvent) fireMessagesAdded(new TracedMessage[]{e});

	}

	/**
	 * Adds all messages (and services that don't exist in this model)
	 * from the other model to this model.
	 * @param other
	 */
	public void addMessages(TraceModel other)
	{
		try {
			TracedMessage[] msgs = other.getMessages();
			TracedMessage[] messageClones = new TracedMessage[msgs.length];
			List<TracedService> serviceClones = new ArrayList<TracedService>(); 
			for(int i = 0; i < msgs.length; i++)
			{
				TracedMessage msg = msgs[i];
				TracedMessage clone = msg.clone();
				messageClones[i] = clone;
				clone.setGraph(this);
				addMessage(clone,false);
				TracedService source = getService(clone.getSource().getID());
				if(source == null)
				{
					source = clone.getSource().clone();
					source.setGraph(this);
					addService(source,false);
					serviceClones.add(source);
					clone.setSource(source);
				}
				clone.setSource(source);
				
				TracedService target = getService(clone.getTarget().getID());
				if(target == null)
				{
					target = clone.getTarget().clone();
					target.setGraph(this);
					addService(target,false);
					serviceClones.add(target);
					clone.setTarget(target);
				}
				clone.setTarget(target);
			}
			fireMessagesAdded(messageClones);
			fireServicesAdded(serviceClones.toArray(new TracedService[serviceClones.size()]));
		} catch (Exception e) {
			TraceActivator.log(Status.ERROR, "Unable to merge traced message sets: "+e.getMessage(),e);
		}
	}
	
	public void addTraceModelListener(ITraceModelListener l)
	{
		modelListeners.add(l);
	}
	
	public void removeTraceModelListener(ITraceModelListener l)
	{
		modelListeners.remove(l);
	}

	public void fireMessagesAdded(TracedMessage[] msg)
	{
		for(ITraceModelListener l : modelListeners)
		{
			l.messagesAdded(msg);
		}
	}
	
	public void fireMessagesRemoved(TracedMessage[] msg)
	{
		for(ITraceModelListener l : modelListeners)
		{
			l.messagesRemoved(msg);
		}
	}
	
	public void fireServicesAdded(TracedService[] service)
	{
		for(ITraceModelListener l : modelListeners)
		{
			l.servicesAdded(service);
		}
	}
	
	public void fireServicesremoved(TracedService[] service)
	{
		for(ITraceModelListener l : modelListeners)
		{
			l.servicesRemoved(service);
		}
	}
	
	@Override
	protected void addPropertyDescriptors() {
		// TODO Auto-generated method stub

	}

	public void addService(TracedService n)
	{
		addService(n,true);
	}
		
	protected void addService(TracedService n, boolean fireEvent)
	{
		synchronized (services) {
			services.add(n);
			serviceMap.put(n.getID(), n);
		}
		if(fireEvent) fireServicesAdded(new TracedService[]{n});
	}
	public List<ITracedEntity> getChildren()
	{
		List<ITracedEntity> result = new ArrayList<ITracedEntity>(services);
		return result;
	}

	public long getLastUpdated() {
		return (Long) getPropertyValue(PROP_LAST_UPDATED);
	}

	public TracedMessage getMessage(String id)
	{
		return messageMap.get(id);
	}

	public TracedMessage[] getMessages()
	{
		return messages.toArray(new TracedMessage[messages.size()]);
	}

	public int getNumOmittedMessages() {
		return (Integer) getPropertyValue(PROP_NUM_OMITTED_MESSAGES);
	}

	public TracedService getService(String id)
	{
		return serviceMap.get(id);
	}

	public TracedService[] getServices()
	{
		return services.toArray(new TracedService[services.size()]);
	}

	public TraceQueryDescription getTraceQueryDescription() {
		return (TraceQueryDescription) getPropertyValue(PROP_TRACE_QUERY);
	}

	public String getViewID() {
		return (String) getPropertyValue(PROP_VIEW_ID);
	}

	public void setLastUpdated(long ms) {
		setPropertyValue(PROP_LAST_UPDATED, ms);
	}

	public void setNumOmittedMessages(int num) {
		setPropertyValue(PROP_NUM_OMITTED_MESSAGES,num);
	}

	public void setTraceQueryDescription(TraceQueryDescription TraceQueryDescription) {
		setPropertyValue(PROP_TRACE_QUERY, TraceQueryDescription);
	}

	public NodePath getTracerAddress() {
		return tracerAddress;
	}

	public void setTracerAddress(NodePath tracerEpr) {
		this.tracerAddress = tracerEpr;
	}

	public void setViewID(String viewID) {
		setPropertyValue(PROP_VIEW_ID, viewID);
	}

	public String getModelVersion() {
		return modelVersion;
	}

	public void setModelVersion(String modelVersion) {
		this.modelVersion = modelVersion;
	}

}
