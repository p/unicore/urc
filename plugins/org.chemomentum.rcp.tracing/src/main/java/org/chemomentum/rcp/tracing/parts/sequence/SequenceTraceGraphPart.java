/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.tracing.parts.sequence;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.rcp.tracing.model.TimeScaleModel;
import org.chemomentum.rcp.tracing.model.TraceModel;
import org.chemomentum.rcp.tracing.model.TracedMessage;
import org.chemomentum.rcp.tracing.parts.ITracePart;
import org.chemomentum.rcp.tracing.policies.SwitchViewEditPolicy;
import org.eclipse.core.runtime.Status;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.draw2d.graph.Subgraph;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import de.fzj.unicore.rcp.wfeditor.WFActivator;



/**
 * @author demuth
 */
public class SequenceTraceGraphPart extends AbstractGraphicalEditPart implements ITracePart {

	public static final String KEY_SUBGRAPH_CLIENTS = "CL";
	public static final String KEY_SUBGRAPH_ORCHESTRATORS = "SO";
	public static final String KEY_SUBGRAPH_TARGET_SYSTEMS = "TS";
	public static final String KEY_SUBGRAPH_WORKFLOW_ENGINES = "WE";

	static final Insets PADDING = new Insets(70, 50, 70, 50);
	//static final Insets PADDING = new Insets(0, 0, 0, 0);
	static final Insets INNER_PADDING = new Insets(0);

	private Set<String> labelBounds = new HashSet<String>();

	private TimeScaleModel timeScaleModel;
	private TimeScalePart timescale;



	protected List getModelChildren() {

		List list= new ArrayList(((TraceModel) getModel()).getChildren());
		list.add(0,getTimeScaleModel());
		return list;
	}

	protected TimeScaleModel getTimeScaleModel(){
		if(timeScaleModel==null)
			timeScaleModel = new TimeScaleModel("TIMESCALE",getModel());

		return timeScaleModel;
	}

	protected void createEditPolicies() {
		installEditPolicy(TracingConstants.REQ_SWITCH_VIEW, new SwitchViewEditPolicy());
	}

	protected IFigure createFigure() {
		Figure f = new Figure() {
			
			protected void paintFigure(Graphics graphics) {
				super.paintFigure(graphics);
				labelBounds.clear();
			}
			public void setBounds(Rectangle rect) {
				super.setBounds(rect);
			}
		};
		f.setLayoutManager(new GraphLayoutManager(this));
		return f;
	}

	public void contributeNodesToGraph(CompoundDirectedGraph graph, Subgraph s, Map map) {
		//		GraphAnimation.recordInitialState(getContentPane());
		Subgraph me = new Subgraph(this, s);
		//Subgraph me2 = new Subgraph(this, s);
		//Graph me = new Graph(this, s);

		//		me.rowOrder = getActivity().getSortIndex();
		me.outgoingOffset = 5;
		me.incomingOffset = 5;
		//me2.outgoingOffset = 5;
		//me2.incomingOffset = 5;
		IFigure fig = getFigure();
		//me.setRowConstraint(4);
		me.innerPadding = INNER_PADDING;
		me.setPadding(PADDING);
		//me2.innerPadding = INNER_PADDING;
		//me2.setPadding(PADDING);
		graph.setDefaultPadding(PADDING);
		map.put(this, me);
		//map.put(this, me2);
		graph.nodes.add(me);
		//graph.nodes.add(me2);


		graph.setDirection(PositionConstants.HORIZONTAL);
		if(getTimeScalePart() != null && getTimeScalePart().getInstances() == null) getTimeScalePart().setInstances();

		for (Object o : getChildren()) {
			if(o instanceof SequenceTraceNodePart)
			{
				SequenceTraceNodePart part = (SequenceTraceNodePart)o;
				part.contributeToGraph(graph, me, map);

			}
		}

	}

	public TimeScalePart getTimeScalePart(){
		if(timescale == null)
		{
			for (Object o : getChildren()) {

				if(o instanceof TimeScalePart){
					timescale=(TimeScalePart)o;
					break;
				}
			}
		}
		return timescale;
	}


	public void contributeEdgesToGraph(CompoundDirectedGraph graph, Map map) {
		for (Object o : getChildren()) {
			if(o instanceof SequenceTraceNodePart)

			{
				SequenceTraceNodePart part = (SequenceTraceNodePart)o;
				part.contributeEdgesToGraph(graph, map);
			}
		}
	}


	protected void applyLayoutToChildren(CompoundDirectedGraph graph, Map map) {

		for (int i = 0; i < getChildren().size(); i++) {
			SequenceTraceNodePart part = (SequenceTraceNodePart)getChildren().get(i);
			part.applyLayout(graph, map);
		}

		for (int i = 0; i < getChildren().size(); i++) {
			SequenceTraceNodePart part = (SequenceTraceNodePart)getChildren().get(i);

			for (int j = 0; j < part.getSourceConnections().size(); j++) {
				SequenceTraceEdgePart edge = (SequenceTraceEdgePart) part.getSourceConnections().get(j);
				edge.applyLayout(graph, map);
			}
		}
		getFigure().validate();
		
	}

	protected void applyLayout(CompoundDirectedGraph graph, Map map) {
		Node n = (Node)map.get(this);
		int width = getChildren().size()*200+500;
		int height = timescale == null ? 500 : timescale.getPixelHigh() +500;
		getFigure().setBounds(new Rectangle(0, 0, width, height));
		applyLayoutToChildren(graph, map);
	}


	public boolean isSelectable() {
		return false;
	}





	/* (non-Javadoc)
	 * @see org.eclipse.gef.EditPart#getDragTracker(org.eclipse.gef.Request)
	 */
	public DragTracker getDragTracker(Request request) {
		// TODO Check whether super class method can be called.
		return null;
	}


	public SequenceTraceEdgePart getPartForMessage(TracedMessage msg)
	{
		String key = getKey(msg);
		return (SequenceTraceEdgePart) getViewer().getEditPartRegistry().get(key);
	}

	/**
	 * Returns all messages that share the given key.
	 * @param key
	 * @return
	 */
	public List<TracedMessage> getMessagesWithKey(String key)
	{
		// TODO make this performant by using a HashMap
		// the problem here is to keep the map up-to-date
		List<TracedMessage> result = new ArrayList<TracedMessage>();
		if(key == null) return result;
		for(TracedMessage msg : getModel().getMessages())
		{
			if(key.equals(getKey(msg))) result.add(msg);
		}
		return result;
	}

	public TraceModel getModel() {
		return (TraceModel) super.getModel();
	}

	public TraceModel getTraceModel() {
		return (TraceModel) super.getModel();
	}


	public String getKey(TracedMessage msg)
	{
		return msg.getSource().getID()+msg.getTarget().getID()+msg.getOperation();
	}

	public String getViewId() {
		return TracingConstants.VIEW_ID_SEQUENCE_DIAGRAM;
	}

	/**
	 * Returns a set of String representations of all shown messages' label bounds.
	 * This is used for avoiding to draw multiple labels at the same position.
	 * @return
	 */
	public Set<String> getLabelBounds() {
		return labelBounds;
	}

	public void refreshVisuals()
	{
		try {

			super.refreshVisuals();
			for(Object o : getChildren())
			{
				if(o instanceof SequenceTraceNodePart)
				{
					SequenceTraceNodePart p = (SequenceTraceNodePart) o;
					p.refreshVisuals();
					
				}
			}
			IFigure fig = getContentPane();
			fig.getLayoutManager().layout(fig);
			((GraphicalEditPart) getRoot()).getFigure().invalidate();
			((GraphicalEditPart) getRoot()).getFigure().validate();
			((GraphicalEditPart) getRoot()).getFigure().invalidate();
			((GraphicalEditPart) getRoot()).getFigure().validate();
		} catch (Exception e) {
			WFActivator.log(Status.WARNING,"Could not refresh visuals of "+toString() +"!",e);
		}
	}
}
