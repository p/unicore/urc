package org.chemomentum.rcp.tracing.exceptions;

public class TraceModelBuildingException extends Exception {

	public TraceModelBuildingException() {
		super();
	}

	public TraceModelBuildingException(String message, Throwable cause) {
		super(message, cause);
	}

	public TraceModelBuildingException(String message) {
		super(message);
	}

	public TraceModelBuildingException(Throwable cause) {
		super(cause);
	}



	
}
