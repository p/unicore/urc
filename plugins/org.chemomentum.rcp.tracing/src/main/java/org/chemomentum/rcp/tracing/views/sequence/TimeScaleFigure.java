package org.chemomentum.rcp.tracing.views.sequence;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.CompoundBorder;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;

 /*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * @author sbergmann
 */
public class TimeScaleFigure extends Figure implements IFigure{

	public static final Color INSTANCE_COLOR = new Color(null, 255, 255, 206);

	private Label scalingfactor = null;
	//private Rectangle bounds =new Rectangle(0,0,200,400);
	
	private Label type = null;
	
	public TimeScaleFigure() {
		super();
		//setBounds(bounds);
		setBorder(new CompoundBorder(new LineBorder(ColorConstants.white, 1), new MarginBorder(2)));
	}

	public TimeScaleFigure build() {
		//ToolbarLayout layout = new ToolbarLayout();
		GridLayout layout = new GridLayout();
		//layout.setHorizontal(false);
		layout.numColumns=1;
	    //layout.setStretchMinorAxis(false);
	    //layout.setSpacing(2);
	    //layout.setMinorAlignment(ToolbarLayout.ALIGN_CENTER);
	    setLayoutManager(layout);
	    setOpaque(true);

	    scalingfactor = new Label();
	    //name.setBounds(bounds);
	    scalingfactor.setOpaque(false);
	    setScalingFactor(scalingfactor);

	    
	    setBackgroundColor(ColorConstants.white);

	    UnderLineLabel nameScale=new UnderLineLabel();
	    //nameScale.setBounds(bounds);
	    nameScale.setOpaque(false);
	    nameScale.setLabelAlignment(PositionConstants.LEFT);
	    nameScale.setText("Time Scale");
	   
	    
	    //add name
	    GridData data=new GridData();
	    data.heightHint=100;
	    data.widthHint=200;
	    data.horizontalAlignment = SWT.BEGINNING;
	    data.verticalAlignment = SWT.CENTER;
	    layout.setConstraint(nameScale, data);
	    add(nameScale);

	    //add name for scale factor and direct edit
	    data=new GridData();
	    data.heightHint=30;
	    data.widthHint=200;
	    data.horizontalAlignment = SWT.BEGINNING;
	    data.verticalAlignment = SWT.CENTER;
	    layout.setConstraint(getScalingLabel(), data);
	    getScalingLabel().setLabelAlignment(PositionConstants.LEFT);
	    add(getScalingLabel());
	    
	    
	    return this;
	}
	
	protected void setScalingFactor(Label name) {
		this.scalingfactor = name;
	}
	
	
	public void setInstanceName(String instanceName) {
		this.scalingfactor.setText(instanceName);
	}

	public void setTypeName(String type) {}
	
	public Label getScalingLabel() {
		return this.scalingfactor;
	}
	
	
	public void paint(Graphics graphics) {
		super.paint(graphics);
	}

	

} 