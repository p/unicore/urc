package org.chemomentum.rcp.tracing.ui;

import java.util.ArrayList;
import java.util.List;

import org.chemomentum.tracer.client.IMessageQuery;
import org.chemomentum.tracer.client.NotMessageQuery;
import org.chemomentum.tracer.client.PatternMessageQuery;
import org.chemomentum.tracer.xmlbeans.ColumnNames;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.uas.util.Pair;

public class MessageQueryUIRowPattern extends MessageQueryUIRow {

	public static final String TYPE_NAME = "Field";
	
	private static final String[] FIELD_NAMES = getFieldNames();
	
	private static final String CONTAINS = "contains", DOES_NOT_CONTAIN = "does not contain",  IS = "is", IS_NOT = "is not";
	
	private static final String[] RELATIONS = new String[] {CONTAINS, DOES_NOT_CONTAIN, IS, IS_NOT};
	
	private Combo fieldCombo, relationCombo;
	
	private Text patternText;
	
	private PatternMessageQuery query;
	
	private boolean negated = false;
	
	public MessageQueryUIRowPattern(IMessageQuery query, int nestingLevel)
	{
		super(TYPE_NAME,nestingLevel);
		if(query instanceof NotMessageQuery)
		{
			negated = true;
			NotMessageQuery not = (NotMessageQuery) query;
			this.query = (PatternMessageQuery) not.getNegated();
		}
		else this.query = (PatternMessageQuery) query;
	}

	@Override
	public void createControls(Composite parent) {
		GC gc = new GC(parent);
		int charWidth = gc.getFontMetrics().getAverageCharWidth();
		gc.dispose();
		
		GridLayout layout = new GridLayout(3, false);
		layout.marginTop = 0;
		layout.marginBottom = 0;
		parent.setLayout(layout);
		
		fieldCombo = new Combo(parent,SWT.READ_ONLY);
		fieldCombo.setItems(FIELD_NAMES);
		fieldCombo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		fieldCombo.select(fieldCombo.indexOf(query.getField()));
		
		Pair<String,String> pair = findRelation(query.getPattern());
		String relation = pair.getM1();
		String pattern = pair.getM2();
		relationCombo = new Combo(parent,SWT.READ_ONLY);
		relationCombo.setItems(RELATIONS);
		relationCombo.select(relationCombo.indexOf(relation));
		relationCombo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		
		GridData gd = new GridData(SWT.FILL, SWT.CENTER, true, false);

		int numChars = Math.max(20, pattern.length());
		gd.minimumWidth = charWidth * numChars;
		patternText = new Text(parent,SWT.BORDER);
		patternText.setLayoutData(gd);
		patternText.setText(pattern);
		
	}


	private static final String[] getFieldNames()
	{
		List<String> result = new ArrayList<String>();
		for(int i = 1; i < 100; i++)
		{
			try {
				String s = ColumnNames.Enum.forInt(i).toString();
				if(s == null)
				{
					break;
				}
				else if(s.equalsIgnoreCase("timestamp")) continue;
				else result.add(s);
			} catch (Throwable t) {
				break;
			}
			
		}
		return result.toArray(new String[result.size()]);
	}
	
	/**
	 * Look at the given pattern and try to find out what kind of relation
	 * it expresses by looking at the occurring wildcards.
	 * @param pattern
	 * @return
	 */
	private Pair<String,String> findRelation(String pattern)
	{
		String relation = negated ? IS_NOT : IS;
		if(pattern.trim().length() == 0 || pattern.startsWith("%") && pattern.endsWith("%"))
		{
			if(negated) relation = DOES_NOT_CONTAIN;
			else relation = CONTAINS;
			if(pattern.trim().length() > 0) pattern = pattern.substring(1, pattern.length()-1);
		}
		return new Pair<String,String>(relation,pattern);
		
	}

	@Override
	public IMessageQuery buildQuery() {
		String relation = RELATIONS[relationCombo.getSelectionIndex()];
		negated = IS_NOT.equals(relation) || DOES_NOT_CONTAIN.equals(relation);
		boolean addWildcards = CONTAINS.equals(relation) || DOES_NOT_CONTAIN.equals(relation);
		String pattern = patternText.getText();
		if(addWildcards) pattern = "%"+pattern+"%";
		String field = FIELD_NAMES[fieldCombo.getSelectionIndex()];
		query.setField(field);
		query.setPattern(pattern);
		if(negated) return new NotMessageQuery(query);
		else return query;
		
	}
	
}
