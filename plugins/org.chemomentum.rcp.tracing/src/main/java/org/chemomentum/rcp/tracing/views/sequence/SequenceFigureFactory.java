package org.chemomentum.rcp.tracing.views.sequence;

import org.eclipse.draw2d.RectangleFigure;




/**
 * @author shidat
 *
 */
public class SequenceFigureFactory {

	/**
	 * 
	 * @return
	 */
	public static RectangleFigure getActivationFigure() { 
		/*if (UMLPlugin.getDefault().getPluginPreferences().getBoolean(UMLPlugin.PREF_NEWSTYLE)) {
			return new ActivationFigureEx();
		}*/
		//return new ActivationFigure();
		return new RectangleFigure();
	}
	
	/**
	 *
	 * @return
	 */
	public static TimeScaleFigure getInstanceFigure() {
		/*if (UMLPlugin.getDefault().getPluginPreferences().getBoolean(UMLPlugin.PREF_NEWSTYLE)) {
			return new InstanceFigureEx();
		}*/
		return new TimeScaleFigure();
	}
}
