package org.chemomentum.rcp.tracing.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TimeScaleModel extends TracedEntity {

	private static final long serialVersionUID = 1L;
	
	//scale factors
	private String ONEMILLISECOND="1 unit = 1ms";
	private String TENMILLISECONDS="1 unit = 10ms";
	private String HUNDREDMILLISECONDS="1 unit = 100ms";
	private String HALFSECOND="1 unit = 500ms";
	private String ONESECOND="1 unit = 1s";
	private String HALFMINUTE="1 unit = 30s";
	private String TENSECOND="1 unit = 10s";
	private String FIVESECOND="1 unit = 5s";
	private String ONEMINUTE="1 unit = 1min";
	private String ONEHOUR="1 unit = 1h";
	private String ONEDAY="1 unit = 1d";
	private String ONEWEEK="1 unit = 1w";

	//store scale factor and matching milliseconds
	private HashMap< String, Long> hash=new HashMap<String, Long>();


	//save all factors
	private List<String> list;

	//list with locked scale factors, only unlocked factors should be displayed
	private List<String> locked;

	//selected scale factor
	private String selectedValue=getInitialValue();

	public TimeScaleModel(String id,TraceModel graph) {
		super(id,graph);

		selectedValue=getInitialValue();
		locked=new ArrayList<String>();

		//add factors to list
		list=new ArrayList<String>();
		list.add(ONEMILLISECOND);
		list.add(TENMILLISECONDS);
		list.add(HUNDREDMILLISECONDS);
		list.add(HALFSECOND);
		list.add(ONESECOND);
		list.add(FIVESECOND);
		list.add(TENSECOND);
		list.add(HALFMINUTE);
		list.add(ONEMINUTE);
		list.add(ONEHOUR);
		list.add(ONEDAY);
		list.add(ONEWEEK);


		//add facotrs with matching milliseconds to list
		hash.put(ONEMILLISECOND, new Long(1));
		hash.put(TENMILLISECONDS, new Long(10));
		hash.put(HUNDREDMILLISECONDS, new Long(100));
		hash.put(HALFSECOND, new Long(500));
		hash.put(ONESECOND, new Long(1000));
		hash.put(FIVESECOND, new Long(5000));
		hash.put(ONEMINUTE, new Long(60000));
		hash.put(ONEHOUR, new Long(3600000));
		hash.put(ONEDAY, new Long(86400000));
		hash.put(ONEWEEK, new Long(604800000));
		hash.put(HALFMINUTE, new Long(30000));
		hash.put(TENSECOND, new Long(10000));
	}

	/**
	 * Return the matching milliseconds for scale factor
	 * @param s name of scale factor
	 * @return matching milliseconds
	 */
	public Long getLong(String s){
		Long result = hash.get(s);
		if(result == null) result = -1l;
		return result;
	}

	/**
	 * set selected scale facotr
	 * @param s name of scale factor
	 */
	public void setSelectedValue(String s){
		selectedValue=s;
	}

	/**
	 * Return selected scale factor
	 * @return selected value
	 */
	public String getSelectedValue(){
		return selectedValue;
	}
	
	public Long getLongValue(){
		return getLong(selectedValue);
	}
	
	public void setLongValue(long longValue){
		for(String s : hash.keySet())
		{
			if(longValue == hash.get(s).longValue()) setSelectedValue(s);
		}
	}

	/**
	 * Return initial name of scale factor
	 * @return initial sclae factor
	 */
	public String getInitialValue(){
		return "-1";
	}

	/**
	 * Return all scale factors which are not locked
	 * @return initial sclae factor
	 */
	public List<String> getFactors(){
		List<String> help=list;
		for (int i = 0; i < locked.size(); i++) {
			if(help.contains(locked.get(i))){
				help.remove(locked.get(i));
			}
		}
		return help;
	}
	
}
