/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.tracing.parts.sequence;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;

import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.rcp.tracing.model.TraceModel;
import org.chemomentum.rcp.tracing.model.TracedMessage;
import org.chemomentum.rcp.tracing.parts.ITracePart;
import org.eclipse.draw2d.BendpointConnectionRouter;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.MidpointLocator;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Edge;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.draw2d.text.PageFlowLayout;
import org.eclipse.draw2d.text.SimpleTextLayout;
import org.eclipse.draw2d.text.TextFlow;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.policies.ShowPropertyViewPolicy;

/**
 * In the hierarchic diagram view, messages with equal source, target,
 * and operation type are merged and represented by a single edit part of this type.
 * @author demuth
 *
 */
public class SequenceTraceEdgePart extends AbstractConnectionEditPart implements ITracePart {

	@SuppressWarnings("rawtypes")
	protected void applyLayout(CompoundDirectedGraph graph, Map map) {
		getFigure().invalidate();
		getFigure().validate();
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	protected void createEditPolicies() {
		installEditPolicy(WFConstants.POLICY_OPEN, new ShowPropertyViewPolicy());
		
		//		installEditPolicy(
		//			EditPolicy.CONNECTION_ENDPOINTS_ROLE, new ConnectionEndpointEditPolicy());
		//		installEditPolicy(EditPolicy.CONNECTION_ROLE, new TransitionEditPolicy());
	}

	public void performRequest(Request request) {
		if (REQ_OPEN.equals(request.getType()))
		{
			Command cmd = getCommand(request);
			if(cmd != null && cmd.canExecute()) cmd.execute();
		}
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractConnectionEditPart#createFigure()
	 */
	protected IFigure createFigure() {


		// visually invert backward messages 
		boolean isResponse = getModel().isResponse();

		final PolylineConnection conn = new PolylineConnection()
		{

			// layout tooltip lazily instead of when it is created 
			public IFigure getToolTip() {

				Panel result = (Panel) super.getToolTip();
				if(result == null)
				{
					result = new Panel();
					result.setLayoutManager(new StackLayout());
					setToolTip(result);
					
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					String toolTip = "id: "+getModel().getID()+"\n"
					+"timestamp: "+df.format(getModel().getTimestamp().getTime())+"\n"
					+"source: "+getModel().getSource().getID()+"\n"
					+"target: "+getModel().getTarget().getID();
					Font labelFont = Display.getDefault().getSystemFont();
					TextFlow tf = new TextFlow(toolTip);
					tf.setFont(labelFont);
					FlowPage fp = new FlowPage();
					result.add(fp);
					fp.add(tf);
					PageFlowLayout pfl = (PageFlowLayout) fp.getLayoutManager();
					SimpleTextLayout stl = new SimpleTextLayout(tf);
					stl.setFlowContext(pfl);
					tf.setLayoutManager(stl);
					
					fp.getLayoutManager().layout(result);
					result.setPreferredSize(tf.getSize());
					tf.setForegroundColor(ColorConstants.tooltipForeground);
					
					

					
					result.setBorder(new LineBorder(ColorConstants.tooltipForeground, 1));
					result.setBackgroundColor(ColorConstants.tooltipBackground);
				}
				return result;
			}
		};


		conn.setConnectionRouter(new BendpointConnectionRouter(){
			public void route(Connection conn) {
				//				GraphAnimation.recordInitialState(conn);
				//				if (!GraphAnimation.playbackState(conn))
				super.route(conn);
			}
		});


		conn.setForegroundColor(ColorConstants.gray);
		PolygonDecoration dec = new PolygonDecoration();

		conn.setTargetDecoration(dec);


		String operationName = (String) getModel().getPropertyValue(TracedMessage.PROP_OPERATION);
		SequenceTraceGraphPart edit=(SequenceTraceGraphPart)getParent().getChildren().get(0);
		
		
		String labeltext = " "+operationName+"  ";

		Label label = new Label(labeltext)
		{
			protected void paintFigure(Graphics graphics) {
				Rectangle clip = graphics.getClip(Rectangle.SINGLETON);
				Rectangle myBounds = getBounds();
				Rectangle r = myBounds.getCopy();
				int meshSize = 10;
				r.x = r.x-r.x%meshSize;
				r.y = r.y-r.y%meshSize;
				r.width = r.width-r.width%meshSize;
				r.height = r.height-r.height%meshSize;
				Set<String> labelBounds = getGraphPart().getLabelBounds();
				String myBoundsString = r.toString();

				boolean visible = !labelBounds.contains(myBoundsString) && isEnabled() && isVisible() ;
				if(visible) 
				{
					labelBounds.add(myBoundsString);
					super.paintFigure(graphics);
				}
			}
		};

		label.setOpaque(true);
		label.setBackgroundColor(ColorConstants.buttonLightest);
		FontData labelfont = new FontData();
		labelfont.setHeight(14);

		label.setFont(new Font(Display.getCurrent(),labelfont));
//		label.setBorder(new LineBorder());
		MidpointLocator locator = new MidpointLocator(conn, 0);
		locator.setGap(5);
		locator.setRelativePosition(PositionConstants.NORTH);
		conn.add(label, locator);

		return conn;

	}



	public TracedMessage getModel()
	{
		return (TracedMessage) super.getModel();
	}


	/**
	 * @see org.eclipse.gef.EditPart#setSelected(int)
	 */
	public void setSelected(int value) {
		super.setSelected(value);
		if (value != EditPart.SELECTED_NONE)
			((PolylineConnection)getFigure()).setLineWidth(2);
		else
			((PolylineConnection)getFigure()).setLineWidth(1);
	}

	public void contributeToGraph(CompoundDirectedGraph graph, Map map) {
		//		GraphAnimation.recordInitialState(getConnectionFigure());
		Node source = (Node)map.get(getSource());
		Node target = (Node)map.get(getTarget());
		SequenceTraceNodePart sourcePart = (SequenceTraceNodePart) source.data;
		SequenceTraceNodePart targetPart = (SequenceTraceNodePart) target.data;
		String id = sourcePart.getModelId()+targetPart.getModelId();

		Edge e = (Edge) map.get(id);
		if(e == null)
		{
			e = new Edge(id, source, target);
			boolean isResponse = getModel().isResponse();
			if(isResponse)
			{
				e.invert();
			}
			map.put(id, e);
			graph.edges.add(e);
		}

		e.weight ++;


	}

	public String getViewId() {
		return TracingConstants.VIEW_ID_SEQUENCE_DIAGRAM;
	}

	public TraceModel getTraceModel() {
		// TODO Auto-generated method stub
		return null;
	}

	protected SequenceTraceGraphPart getGraphPart()
	{

		return (SequenceTraceGraphPart) getRoot().getChildren().get(0);
	}


}
