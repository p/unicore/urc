package org.chemomentum.rcp.tracing.ui;

import org.chemomentum.tracer.client.TraceQueryDescription;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class MessageQueryDialog extends Dialog {

	private MessageQueryUIBuilder uiBuilder = new MessageQueryUIBuilder();
	private TraceQueryDescription query;
	public MessageQueryDialog(Shell parentShell, TraceQueryDescription query) {
		super(parentShell);
		this.query = query;
		
	}
	
	public TraceQueryDescription getQuery()
	{
		return query;
	}
	
	public boolean close() {
		query = uiBuilder.buildQuery();
		return super.close();
	}
	
	protected Composite createDialogArea(Composite parent) {
		Composite result = (Composite) super.createDialogArea(parent);
		Label l = new Label(result,SWT.NONE);
		l.setText("Query the tracing service for messages that meet the following criteria:");
		Composite queryParent = new Composite(result,SWT.NONE);
		queryParent.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		uiBuilder.createControls(queryParent, query);
		return result;
	}
	
	public void create()
	{
		super.create();
		uiBuilder.fixLayout();
	}
	
	protected int getShellStyle() {
		return SWT.RESIZE;
	}

	
}
