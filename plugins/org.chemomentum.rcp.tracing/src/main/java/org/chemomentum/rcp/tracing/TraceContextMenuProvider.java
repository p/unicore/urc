/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.tracing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;

import de.fzj.unicore.rcp.wfeditor.actions.ActionComparator;

/**
 * Provides a context menu for the workflow editor.
 * @author Daniel Lee, demuth
 */
public class TraceContextMenuProvider extends ContextMenuProvider {


	private ActionRegistry actionRegistry;

	/**
	 * Creates a new ContextMenuProvider associated with the given viewer and 
	 * action registry.
	 * @param viewer the viewer
	 * @param registry the action registry
	 */
	public TraceContextMenuProvider(EditPartViewer viewer, ActionRegistry registry) {
		super(viewer);

		setActionRegistry(registry);
	}

	/**
	 * @see ContextMenuProvider#buildContextMenu(org.eclipse.jface.action.IMenuManager)
	 */
	public void buildContextMenu(IMenuManager menu) {
		GEFActionConstants.addStandardActionGroups(menu);
		menu.remove(GEFActionConstants.MB_ADDITIONS);
		List<IAction> unsortedActions = new ArrayList<IAction>();
		Iterator iter = getActionRegistry().getActions();
		while (iter.hasNext()) {
			IAction action = (IAction) iter.next();
			action.isEnabled();
			unsortedActions.add(action);		
		}
		
		IAction[] sortedActions = unsortedActions.toArray(new IAction[0]);
		Arrays.sort(sortedActions, new ActionComparator());
		for (int i = 0; i < sortedActions.length; i++) {
			menu.add(sortedActions[i]);
		}

	}

	private ActionRegistry getActionRegistry() {
		return actionRegistry;
	}

	/**
	 * Sets the action registry
	 * @param registry the action registry
	 */
	public void setActionRegistry(ActionRegistry registry) {
		actionRegistry = registry;
	}


}
