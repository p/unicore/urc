/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.tracing.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.xmlbeans.XmlObject;
import org.chemomentum.rcp.tracing.exceptions.TraceModelBuildingException;
import org.chemomentum.rcp.tracing.exceptions.UnsupportedTraceVersionException;
import org.chemomentum.tracer.client.IMessageQuery;

/**
 * Creates the model (graph of plain old java objects) from the message trace.
 * Delegates the task to an implementation of {@link IModelProducer)} which knows
 * how to deal with this particular version of message traces.
 * @author demuth
 *
 */
public class ModelFactory {

	Map<String,IModelProducer> producers = new HashMap<String, IModelProducer>();

	private static ModelFactory instance;

	private ModelFactory()
	{
		addProducer(new ModelProducerV6_2());
	}
	
	public void addProducer(IModelProducer p)
	{
		for(String v : p.getModelVersions())
		{
			producers.put(v, p);
		}
	}

	public TraceModel buildModel(String modelVersion, XmlObject trace, Set<String> requiredProperties) throws TraceModelBuildingException, UnsupportedTraceVersionException
	{		
		synchronized (producers) {
			IModelProducer p = producers.get(modelVersion);
			if(p != null) return p.buildModel(trace,requiredProperties);
			else 
			{
				try {
					// fallback
					p = new ModelProducerV6_2();
					return p.buildModel(trace,requiredProperties);
				} catch (Exception e) {
					throw new UnsupportedTraceVersionException();
				}
				
			}
		}
	}
	
	public IMessageQuery buildUniqueQuery(String modelVersion, TracedMessage msg) throws UnsupportedTraceVersionException
	{
		synchronized (producers) {
			IModelProducer p = producers.get(modelVersion);
			if(p != null) return p.buildUniqueQuery(msg);
			else 
			{
				try {
					// fallback
					p = new ModelProducerV6_2();
					return p.buildUniqueQuery(msg);
				} catch (Exception e) {
					throw new UnsupportedTraceVersionException();
				}
				
			}
		}
	}

	public static synchronized ModelFactory getSharedInstance()
	{
		if(instance == null) instance = new ModelFactory();
		return instance;
	}

}
