package org.chemomentum.rcp.tracing.parts.sequence;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Map;

import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.rcp.tracing.model.TraceModel;
import org.chemomentum.rcp.tracing.parts.ITracePart;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.draw2d.graph.Subgraph;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;


public abstract class SequenceTraceNodePart extends AbstractGraphicalEditPart
implements PropertyChangeListener, ITracePart{

	@Override
	protected IFigure createFigure() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void createEditPolicies() {
		// TODO Auto-generated method stub

	}



	public void refreshVisuals() {

		TimeScalePart timeScale = ((SequenceTraceGraphPart) getParent()).getTimeScalePart();
		if(timeScale == null) return;
		GridLayout layout = (GridLayout) getFigure().getLayoutManager();

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace=false;
		gridData.grabExcessVerticalSpace=false;
		gridData.heightHint = timeScale.getPixelHigh()+10;
		gridData.widthHint=200;

		layout.setConstraint((IFigure) getFigure().getChildren().get(1), gridData);

		getFigure().invalidate();
		getFigure().repaint();
		
	}

	public void propertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub

	}


	public String getViewId() {
		return TracingConstants.VIEW_ID_SEQUENCE_DIAGRAM;
	}


	public TraceModel getTraceModel() {
		return (TraceModel) super.getModel();
	}

	public abstract String getModelId();


	protected void applyLayout(CompoundDirectedGraph graph, Map map) {
		Node n = (Node)map.get(this);
		getFigure().setBounds(new Rectangle(n.x, 50, n.width, n.height));
		refreshVisuals();

	}

	public void contributeEdgesToGraph(CompoundDirectedGraph graph, Map map) {
		List outgoing = getSourceConnections();
		//List outgoing =getModelTargetConnections();
		for (int i = 0; i < outgoing.size(); i++) {
			SequenceTraceEdgePart part = (SequenceTraceEdgePart)getSourceConnections().get(i);
			part.contributeToGraph(graph, map);
		}
		for (int i = 0; i < getChildren().size(); i++) {
			SequenceTraceNodeServicePart child = (SequenceTraceNodeServicePart)children.get(i);
			child.contributeEdgesToGraph(graph, map);
		}
	}




	public void contributeToGraph(CompoundDirectedGraph graph, Subgraph s, Map map)
	{

		Node n = new Node(this, s);
		//	sub.addMember(n);
		n.outgoingOffset = getAnchorOffset();
		n.incomingOffset = getAnchorOffset();
		//n.width = getFigure().getPreferredSize().width;
		//n.height = getFigure().getPreferredSize().height;
		n.width=200;

		TimeScalePart timeP=((SequenceTraceGraphPart)getParent()).getTimeScalePart();

		n.height=timeP == null ? 1200 : timeP.getPixelHigh()+getHeightOffset();
		map.put(this, n);
		graph.nodes.add(n);
	}

	public int getHeightOffset()
	{
		return 230;
	}
	
	int getAnchorOffset()
	{
		return 10;
	}


}
