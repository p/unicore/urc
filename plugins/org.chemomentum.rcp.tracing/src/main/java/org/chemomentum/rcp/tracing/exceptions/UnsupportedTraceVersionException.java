package org.chemomentum.rcp.tracing.exceptions;

public class UnsupportedTraceVersionException extends Exception {

	public UnsupportedTraceVersionException() {
		super("Found an unsupported version in the message trace that was obtained from the tracing service.");
	}

	
	
}
