/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.tracing.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author demuth
 *
 */
public class TracedService extends TracedEntity
{

	private static final long serialVersionUID = 1L;

	protected String type;

	protected List<TracedMessage> incoming = new ArrayList<TracedMessage>();
	protected List<TracedMessage> outgoing = new ArrayList<TracedMessage>();
	
	public TracedService(String iD, TraceModel graph, String type) {
		super(iD,graph);
		this.type = type;
		graph.addService(this);
	}


	

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	

	public TracedMessage[] getIncomingMessages() {
		return incoming.toArray(new TracedMessage[incoming.size()]);
	}

	public void addIncomingMessage(TracedMessage e)
	{
		incoming.add(e);
	}
	
	public void addIncomingMessage(TracedMessage e, int index)
	{
		incoming.add(index,e);
	}
	
	public void removeIncomingMessage(TracedMessage e)
	{
		incoming.remove(e);
	}
	

	public TracedMessage[] getOutgoingMessages() {
		return outgoing.toArray(new TracedMessage[outgoing.size()]);
	}
	
	public void addOutgoingMessage(TracedMessage e)
	{
		outgoing.add(e);
	}
	
	public void addOutgoingMessage(TracedMessage e, int index)
	{
		outgoing.add(index,e);
	}
	
	public void removeOutgoingMessage(TracedMessage e)
	{
		outgoing.remove(e);
	}




	@Override
	protected void addPropertyDescriptors() {
		// TODO Auto-generated method stub
		
	}
	
	public TracedService clone() throws CloneNotSupportedException
	{
		TracedService result = (TracedService) super.clone();
		result.incoming = new ArrayList<TracedMessage>(incoming);
		result.outgoing = new ArrayList<TracedMessage>(outgoing);
		return result;
	}
}
