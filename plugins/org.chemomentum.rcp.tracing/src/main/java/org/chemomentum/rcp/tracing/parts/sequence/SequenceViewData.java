package org.chemomentum.rcp.tracing.parts.sequence;

public class SequenceViewData {
	private long scalingFactor = 0;
	
	public SequenceViewData(long scalingFactor) {
		super();
		this.scalingFactor = scalingFactor;
	}

	public long getScalingFactor() {
		return scalingFactor;
	}
	

	public void setScalingFactor(long scalingFactor) {
		this.scalingFactor = scalingFactor;
	}

}
