/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.tracing.parts.hierachic.commands;

import org.eclipse.draw2d.Bendpoint;

import de.fzj.unicore.rcp.wfeditor.model.transitions.SerializableBendpoint;

public class MoveBendpointCommand
	extends BendpointCommand 
{

private Bendpoint oldBendpoint;

public void execute() {
	Bendpoint bp = new SerializableBendpoint(getLocation().x,getLocation().y);

	setOldBendpoint((Bendpoint)getTraceEdge().getBendpoints().get(getIndex()));
	getTraceEdge().setBendpoint(getIndex(), bp);
	super.execute();
}

protected Bendpoint getOldBendpoint() {
	return oldBendpoint;
}

public void setOldBendpoint(Bendpoint bp) {
	oldBendpoint = bp;
}

public void undo() {
	super.undo();
	getTraceEdge().setBendpoint(getIndex(), getOldBendpoint());
}

}


