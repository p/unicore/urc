package org.chemomentum.rcp.tracing.ui;

import java.util.ArrayList;
import java.util.List;

import org.chemomentum.tracer.client.IMessageQuery;
import org.chemomentum.tracer.client.TraceQueryDescription;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;



public class MessageQueryUIBuilder {

	private static final int MARGIN_TOP = 10, MARGIN_BOTTOM = 10, MARGIN_LEFT = 10, MARGIN_RIGHT = 10, VERTICAL_OFFSET = 10, HORIZONTAL_OFFSET = 5;

	private static final int ROW_PARENT_LIMIT_RIGHT = 90;

	private static final int ADD_REMOVE_BUTTON_SPACE = (100-ROW_PARENT_LIMIT_RIGHT)/2;

	protected Composite parent;

	protected List<MessageQueryUIRow> rows = new ArrayList<MessageQueryUIRow>();
	protected List<Composite> rowParents = new ArrayList<Composite>();
	protected List<Combo> typeCombos = new ArrayList<Combo>();

	protected List<Button> addButtons = new ArrayList<Button>();
	protected List<Button> removeButtons = new ArrayList<Button>();

	protected TraceQueryDescription queryDescription;

	public void createControls(Composite parent, TraceQueryDescription queryDescription)
	{
		this.parent = parent;
		this.queryDescription = queryDescription;
		parent.setLayout(new FormLayout());
		MessageQueryUIRow  row = MessageQueryUIRowFactory.getInstance().createRowsFor(queryDescription.getQuery(), 0);
		int i = 0;
		addRow(i, row, false);

	}




	public int addNewRowAfter(Composite rowParent)
	{
		int index = getIndexForRowParent(rowParent);
		MessageQueryUIRow previous = rows.get(index);

		IMessageQuery defaultQuery = MessageQueryUIRowFactory.getInstance().createDefaultQuery();
		MessageQueryUIRow row = MessageQueryUIRowFactory.getInstance().createRowsFor(defaultQuery, previous.getNestingLevel());
		int result = addRow(index+previous.getNestedQueryRows().size()+1, row,false);
		if(previous.getParentRow() != null)
		{
			previous.getParentRow().addNestedQueryRow(row);
		}
		fixLayout();
		return result;
	}

	public int addRow(int index, final MessageQueryUIRow row)
	{
		return addRow(index, row, true);

	}

	public int addRow(int index, final MessageQueryUIRow row, boolean fixLayout)
	{
		return addRow(index, row, fixLayout, true);
	}

	public int addRow(int index, final MessageQueryUIRow row, boolean fixLayout,boolean recurse)
	{

		int result = 1;
		final Composite rowParent = new Composite(parent, SWT.NONE);

		final Combo typeCombo = new Combo(parent,SWT.READ_ONLY);
		List<String> typeNames = MessageQueryUIRowFactory.getInstance().getRowTypeNames();
		typeCombo.setItems(typeNames.toArray(new String[typeNames.size()]));
		typeCombo.select(typeNames.indexOf(row.getTypeName()));
		typeCombo.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				queryTypeChanged(getIndexForRowParent(rowParent), typeCombo.getItem(typeCombo.getSelectionIndex()));
			}

			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});


		row.createControls(rowParent);

		if(index > 0)
		{
			Button addButton = new Button(parent, SWT.PUSH);

			addButton.setText("+");
			addButton.addSelectionListener(new SelectionListener() {

				public void widgetSelected(SelectionEvent e) {
					addNewRowAfter(rowParent);
				}

				public void widgetDefaultSelected(SelectionEvent e) {
					addNewRowAfter(rowParent);
				}
			});

			Button removeButton = new Button(parent, SWT.PUSH);

			removeButton.setText("-");
			removeButton.addSelectionListener(new SelectionListener() {

				public void widgetSelected(SelectionEvent e) {
					removeRow(rowParent);
				}

				public void widgetDefaultSelected(SelectionEvent e) {
					removeRow(rowParent);	
				}
			});

			addButtons.add(index,addButton);
			removeButtons.add(index,removeButton);
		}
		else 
		{
			addButtons.add(index,null);
			removeButtons.add(index,null);
		}

		rowParents.add(index,rowParent);
		typeCombos.add(index,typeCombo);
		rows.add(index,row);

		if(recurse)
		{
			for(int i = 0; i < row.getNestedQueryRows().size(); i ++)
			{
				result += addRow(index+result, row.getNestedQueryRows().get(i),fixLayout);
			}
		}

		if(fixLayout) fixLayout();
		return result;
	}

	public TraceQueryDescription buildQuery()
	{
		// queries at the top level are connected with a logical conjunction
		MessageQueryUIRow and = new MessageQueryUIRowAnd(0);
		for(MessageQueryUIRow row : rows)
		{
			if(row.getNestingLevel() == 0) and.addNestedQueryRow(row);
		}
		IMessageQuery query = and.buildQuery();
		queryDescription.setQuery(query);
		return queryDescription;
	}

	protected void queryTypeChanged(int index, String newType)
	{
		MessageQueryUIRow oldRow = rows.get(index);
		if(oldRow.getTypeName().equals(newType)) return;
		
		MessageQueryUIRow newRow = MessageQueryUIRowFactory.getInstance().changeQueryType(oldRow, newType);
		boolean recurse = newRow.getNestedQueryRows().size() == 0; // remove nested rows if newRow does not have nested rows anymore, otherwise keep them
		removeRow(index, false, recurse);
		MessageQueryUIRow parentRow = oldRow.getParentRow();
		if(parentRow != null) 
		{
			int childIndex = parentRow.removeNestedQueryRow(oldRow);
			parentRow.addNestedQueryRow(childIndex,newRow); 
		}
		recurse = oldRow.getNestedQueryRows().size() != newRow.getNestedQueryRows().size();
		addRow(index, newRow, false,recurse);

		fixLayout();

	}

	public void fixLayout()
	{
		GC gc = new GC(parent);
		int rowWidth = gc.getFontMetrics().getHeight()+20;
		
		gc.dispose();
		for(int index = 0; index < rows.size(); index ++)
		{
			MessageQueryUIRow row = rows.get(index);
			Control predecessor = index < 1 ? null : typeCombos.get(index-1);
			Control typeCombo = typeCombos.get(index);

			int bottom = MARGIN_TOP+rowWidth*(1+index)+VERTICAL_OFFSET*index;
			FormData formData = new FormData();
			if(predecessor == null)
			{
				formData.top = new FormAttachment(0,MARGIN_TOP);
			}
			else {
				formData.top = new FormAttachment(predecessor,VERTICAL_OFFSET);
			}
			int INDENTATION = 30;
			formData.bottom = new FormAttachment(0,bottom);
			formData.left = new FormAttachment(0,MARGIN_LEFT+row.getNestingLevel()*INDENTATION);
			typeCombo.setLayoutData(formData);

			formData = new FormData();
			if(predecessor == null)
			{
				formData.top = new FormAttachment(0,MARGIN_TOP);
			}
			else {
				formData.top = new FormAttachment(predecessor,VERTICAL_OFFSET);
			}
			formData.bottom = new FormAttachment(0,bottom);
			formData.left = new FormAttachment(typeCombo,HORIZONTAL_OFFSET);
			formData.right = new FormAttachment(ROW_PARENT_LIMIT_RIGHT,-HORIZONTAL_OFFSET);
			Control rowParent = rowParents.get(index);
			rowParent.setLayoutData(formData);



			if(index > 0)
			{
				formData = new FormData();
				if(predecessor == null)
				{
					formData.top = new FormAttachment(0,MARGIN_TOP);
				}
				else {
					formData.top = new FormAttachment(predecessor,VERTICAL_OFFSET);
				}
				formData.bottom = new FormAttachment(0,bottom);
				formData.left = new FormAttachment(ROW_PARENT_LIMIT_RIGHT);
				formData.right = new FormAttachment(ROW_PARENT_LIMIT_RIGHT+ADD_REMOVE_BUTTON_SPACE,-(HORIZONTAL_OFFSET+MARGIN_RIGHT)/2);
				Control addButton = addButtons.get(index);
				addButton.setLayoutData(formData);

				formData = new FormData();
				if(predecessor == null)
				{
					formData.top = new FormAttachment(0,MARGIN_TOP);
				}
				else {
					formData.top = new FormAttachment(predecessor,VERTICAL_OFFSET);
				}
				formData.bottom = new FormAttachment(0,bottom);
				formData.left = new FormAttachment(addButton,HORIZONTAL_OFFSET);
				formData.right = new FormAttachment(100,-MARGIN_RIGHT);
				Control removeButton = removeButtons.get(index);
				removeButton.setLayoutData(formData);
			}
		}

		checkRemoveButtonEnablement();

		int numRows = rows.size();
		int height = MARGIN_TOP+MARGIN_BOTTOM+rowWidth*(numRows+1)+VERTICAL_OFFSET*Math.max(0,numRows)+50;
		int width = Math.max(parent.getShell().getMinimumSize().x, 100);
		parent.getShell().setMinimumSize(width, height);
		parent.layout();
		parent.getShell().pack();
		
		// for some reason, this seems to be necessary twice, otherwise the
		// shell is too small.. seems like an Eclipse bug, but aaanyway...
		parent.layout();
		parent.getShell().pack();
	}

	public int getIndexForRowParent(Composite rowParent)
	{
		for(int i = 0;i< rowParents.size();i++)
		{
			Composite c = rowParents.get(i);
			if(c == rowParent) 
			{
				return i;
			}
		}
		return -1;
	}

	protected void checkRemoveButtonEnablement()
	{
	
		for(int i = 1; i < rows.size(); i++)
		{
			MessageQueryUIRow row = rows.get(i);
			boolean removeEnabled = row.getParentRow().getNestedQueryRows().size() > 1;
			removeButtons.get(i).setEnabled(removeEnabled);
		}
	}

	public void removeRow(Composite rowParent)
	{
		int index = getIndexForRowParent(rowParent);
		MessageQueryUIRow row = rows.get(index);
		MessageQueryUIRow parentRow = row.getParentRow();
		removeRow(index,false,true);
		parentRow.removeNestedQueryRow(row);
		fixLayout();
	}



	public void removeRow(int index)
	{
		removeRow(index, true);
	}

	public void removeRow(int index, boolean fixLayout)
	{
		removeRow(index, fixLayout, true);
	}

	public void removeRow(int index, boolean fixLayout, boolean recurse)
	{
		Combo combo = typeCombos.remove(index);
		Composite rowParent = rowParents.remove(index);
		Button addButton = addButtons.remove(index);
		Button removeButton = removeButtons.remove(index);
		MessageQueryUIRow row = rows.remove(index);
		if(recurse)
		{
			
			for(int i = 0; i < row.getNestedQueryRows().size();i++)
			{
				removeRow(index,false);
			}
		}
		combo.dispose();
		rowParent.dispose();
		if(addButton != null) addButton.dispose();
		if(removeButton != null) removeButton.dispose();


		if(fixLayout) fixLayout();
	}




}
