package org.chemomentum.rcp.tracing.actions;

import org.chemomentum.rcp.tracing.TraceActivator;
import org.chemomentum.rcp.tracing.TraceEditor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;

public class RefreshTraceAction extends Action {

	public static final String ID = TraceActivator.PLUGIN_ID+".editor.refresh";
	public static final String TEXT = "Refresh messages";
	public static final ImageDescriptor IMAGE_DESCRIPTOR = TraceActivator.getImageDescriptor("refresh.png");
	private TraceEditor editor;
	
	public RefreshTraceAction(TraceEditor editor) {
		super(TEXT,IMAGE_DESCRIPTOR);
		this.editor = editor;
		setId(ID);
	}
	
	public void run() 
	{
		editor.refresh();
	}

}
