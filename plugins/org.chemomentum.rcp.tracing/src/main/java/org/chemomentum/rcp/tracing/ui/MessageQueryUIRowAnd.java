package org.chemomentum.rcp.tracing.ui;

import org.chemomentum.tracer.client.AndMessageQuery;
import org.chemomentum.tracer.client.IMessageQuery;
import org.eclipse.swt.widgets.Composite;

public class MessageQueryUIRowAnd extends MessageQueryUIRow {
	public static final String TYPE_NAME = "All of the following";

	
	
	public MessageQueryUIRowAnd(int nestingLevel) {
		super(TYPE_NAME, nestingLevel);

	}

	
	@Override
	public void createControls(Composite parent) {
		
		
	}


	@Override
	public IMessageQuery buildQuery() {
		MessageQueryUIRow first = getNestedQueryRows().get(0);
		IMessageQuery firstQuery = first.buildQuery();
		if(getNestedQueryRows().size() == 1) return firstQuery; // no and required
		AndMessageQuery result = new AndMessageQuery(firstQuery, null);
		AndMessageQuery current = result;
		for(int i = 1; i < getNestedQueryRows().size(); i++)
		{
			MessageQueryUIRow row = getNestedQueryRows().get(i);
			IMessageQuery query = row.buildQuery();
			if(i == getNestedQueryRows().size() -1) current.setRight(query);
			else
			{
				AndMessageQuery and = new AndMessageQuery(query, null);
				current.setRight(and);
				current = and;
			}
		}
		return result;
	}
	

}
