package org.chemomentum.rcp.tracing.parts.hierachic;

import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.rcp.tracing.model.TracedService;

public class FindSubgraphUtil {
	
	public static String findSubgraphKeyFor(TracedService s)
	{
		if(TracingConstants.SERVICE_TYPE_CLIENT.equals(s.getType()))
		{ 
			return HierarchicTraceGraphPart.KEY_SUBGRAPH_CLIENTS;
		}
		else if(TracingConstants.SERVICE_TYPE_ORCHESTRATOR.equals(s.getType())) 
		{ 
			return HierarchicTraceGraphPart.KEY_SUBGRAPH_ORCHESTRATORS;
		}
		else if(TracingConstants.SERVICE_TYPE_TARGET_SYSTEM.equals(s.getType())) 
		{ 
			return HierarchicTraceGraphPart.KEY_SUBGRAPH_TARGET_SYSTEMS;
		}
		else if(TracingConstants.SERVICE_TYPE_WORKFLOW_ENGINE.equals(s.getType())) 
		{ 
			return HierarchicTraceGraphPart.KEY_SUBGRAPH_WORKFLOW_ENGINES;
		}
		return "";
		
	}

}
