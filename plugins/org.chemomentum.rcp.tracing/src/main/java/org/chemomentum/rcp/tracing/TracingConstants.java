package org.chemomentum.rcp.tracing;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.chemomentum.rcp.tracing.model.TracedMessage;
import org.chemomentum.tracer.xmlbeans.ColumnNames;

public interface TracingConstants {
	public static final String EXTENSION_POINT_VIEW_TYPES = "org.chemomentum.rcp.tracing.viewtype";

	
	public static final String REQ_SWITCH_VIEW = "switch view request";
	
	public static final String 
	SERVICE_TYPE_CLIENT = "client",
	SERVICE_TYPE_WORKFLOW_ENGINE = "workflowEngine",
	SERVICE_TYPE_ORCHESTRATOR = "serviceOrchestrator", 
	SERVICE_TYPE_TARGET_SYSTEM = "targetSystem";
	
	public static final String[] SERVICE_TYPES = new String[]{SERVICE_TYPE_CLIENT,SERVICE_TYPE_WORKFLOW_ENGINE,SERVICE_TYPE_ORCHESTRATOR,SERVICE_TYPE_TARGET_SYSTEM}; 
	
	public static final String[] DEFAULT_ATTRIBUTES = new String[]{ColumnNames.SOURCE_ID.toString(),ColumnNames.SOURCE_TYPE.toString(),ColumnNames.TARGET_ID.toString(),ColumnNames.TARGET_TYPE.toString(),ColumnNames.OPERATION.toString(),ColumnNames.USER_ID.toString(),ColumnNames.TIMESTAMP.toString(),ColumnNames.MESSAGE_ID.toString()};
	
	public static final int HARD_MESSAGE_LIMIT = 100000;
	
	public static final int MESSAGE_CHUNK_SIZE = 1000;
	
	public static final String TRACE_FILE_EXTENSION = "trace";
	
	public static final Set<String> MUST_HAVE_PROPERTIES = new HashSet<String>(Arrays.asList(new String[]{TracedMessage.PROP_TIMESTAMP,TracedMessage.PROP_OPERATION}));

	public static final String VIEW_ID_HIERARCHIC_GRAPH = "org.chemomentum.rcp.tracing.parts.hierarchic";
	public static final String VIEW_ID_SEQUENCE_DIAGRAM = "org.chemomentum.rcp.tracing.parts.sequence";
	
	public static final String OPERATION_SUBMIT_WORKFLOW = "submit Workflow";
	public static final String OPERATION_SUBMIT_JOB = "submit Job";
	public static final String OPERATION_JOB_SUBMITTED = "Job submitted";
	public static final String OPERATION_JOB_FINISHED = "Job finished";
	public static final String OPERATION_JOB_FAILED = "Job failed";
	
	public static final String POLICY_SWITCH_VIEW = "switch view policy";
}
