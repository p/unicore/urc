package org.chemomentum.rcp.tracing.handlers;

import java.util.Arrays;
import java.util.List;

import org.chemomentum.common.ws.ITrace;
import org.chemomentum.rcp.tracing.TraceActivator;
import org.chemomentum.rcp.tracing.TraceEditor;
import org.chemomentum.rcp.tracing.model.ModelFactory;
import org.chemomentum.rcp.tracing.model.TracedMessage;
import org.chemomentum.rcp.tracing.parts.hierachic.HierarchicTraceEdgePart;
import org.chemomentum.tracer.client.IMessageQuery;
import org.chemomentum.tracer.client.TraceQueryDescription;
import org.chemomentum.tracer.xmlbeans.EntryDocument.Entry;
import org.chemomentum.tracer.xmlbeans.FilterEntriesResponseDocument;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISources;

import de.fzj.unicore.rcp.logmonitor.TextEditorDialog;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ViewMessageContentHandler extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public ViewMessageContentHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IEvaluationContext c = (IEvaluationContext) event.getApplicationContext();
		Object editor = c.getVariable(ISources.ACTIVE_EDITOR_NAME);
		if(!(editor instanceof TraceEditor)) return null;
		final TraceEditor traceEditor = (TraceEditor) editor;
		final Display d = traceEditor.getGraphicalViewer().getControl().getDisplay();
		final TracedMessage msg = getSelectedMessage(traceEditor);
		if(msg == null) return null;
		BackgroundJob bg = new BackgroundJob("Downloading messsage content.") {
			
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					IMessageQuery query = ModelFactory.getSharedInstance().buildUniqueQuery(msg.getGraph().getModelVersion(), msg);
					TraceQueryDescription queryDescription = new TraceQueryDescription(query,TraceQueryDescription.MODE_RESULT_SET);
					queryDescription.setAttributes(Arrays.asList(ITrace.MESSAGE_ID,ITrace.MESSAGE));
					FilterEntriesResponseDocument respDoc = traceEditor.getTraceRefresher().getClient().query(queryDescription);
					Entry[] entries = respDoc.getFilterEntriesResponse().getTrace().getEntryArray();
					String emptyResponseMsg = "Retrieved empty response from tracing service!";
					if(entries == null || entries.length == 0) throw new Exception(emptyResponseMsg);
					String s = null;
					if(entries.length > 1)
					{
						s = "Retrieved multiple messages!\n";
						for(int i = 0; i < entries.length; i++)
						{
							s+= "Message "+(i+1)+":\n"+entries[i]+"\n\n\n";
						}
					}
					else
					{
						Entry e = entries[0];
						s = e.toString();
					}
					
					if(!d.isDisposed() && !monitor.isCanceled())
					{
						final String toShow = s;
						d.asyncExec(new Runnable() {
							
							@Override
							public void run() {
								TextEditorDialog dlg = new TextEditorDialog(d.getActiveShell(), "Message content",toShow);
								dlg.open();
							}
						});
						
					}	
					
				} catch (Exception e) {
					TraceActivator.log(Status.ERROR, "Unable to retrieve message content from server: "+e.getMessage(), e);
				}
				return Status.OK_STATUS;
			}
		};
		
		bg.schedule();
		
		return null;
	}
	
	private TracedMessage getSelectedMessage(TraceEditor editor)
	{
		List<?> selected = editor.getGraphicalViewer().getSelectedEditParts();
		if(selected == null || selected.size() != 1) return null;
		Object o = selected.get(0);
		if(!(o instanceof EditPart)) return null;
		EditPart part = (EditPart) o;
		if(part instanceof HierarchicTraceEdgePart)
		{
			HierarchicTraceEdgePart hierarchic = (HierarchicTraceEdgePart) part;
			List<TracedMessage> msgs = hierarchic.getMessages();
			if(msgs != null && msgs.size() ==1) return msgs.get(0);
		}
		if(!(part.getModel() instanceof TracedMessage)) return null;
		return (TracedMessage) part.getModel();

	}
	
	public void setEnabled(Object evaluationContext) {
		if(!(evaluationContext instanceof IEvaluationContext))
		{
			setBaseEnabled(false);
			return;
		}
		IEvaluationContext c = (IEvaluationContext) evaluationContext;
		Object editor = c.getVariable(ISources.ACTIVE_EDITOR_NAME);
		if(!(editor instanceof TraceEditor)) 
		{
			setBaseEnabled(false);
			return;
		}
		TraceEditor traceEditor = (TraceEditor) editor;
		setBaseEnabled(getSelectedMessage(traceEditor) != null);
	}
}
