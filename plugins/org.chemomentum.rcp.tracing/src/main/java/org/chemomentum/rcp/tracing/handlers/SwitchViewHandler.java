package org.chemomentum.rcp.tracing.handlers;

import java.util.Map;

import org.chemomentum.rcp.tracing.TraceEditor;
import org.chemomentum.rcp.tracing.TraceViewer;
import org.chemomentum.rcp.tracing.parts.ITracePart;
import org.chemomentum.rcp.tracing.requests.SwitchViewRequest;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.menus.UIElement;

public class SwitchViewHandler extends AbstractHandler implements IElementUpdater {

	/**
	 * The identifier for the view to switch to.
	 */
	public static final String PARAM_VIEW_ID = "org.chemomentum.rcp.tracing.params.viewId";



	public Object execute(ExecutionEvent event) throws ExecutionException {
		IEditorPart editorPart = HandlerUtil.getActiveEditor(event);
		if(!(editorPart instanceof TraceEditor)) return null;
		TraceEditor editor = (TraceEditor) editorPart;
		TraceViewer viewer = editor.getGraphicalViewer();
		ITracePart part = (ITracePart) ((EditPart) viewer.getRootEditPart().getChildren().get(0));
		String selectedId = event.getParameter(PARAM_VIEW_ID);
		Command cmd =part.getCommand(new SwitchViewRequest(selectedId));
		if(cmd != null && cmd.canExecute())
		{
			viewer.getEditDomain().getCommandStack().execute(cmd);
		}

		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		ICommandService service = (ICommandService) window.getService(ICommandService.class);
		service.refreshElements(event.getCommand().getId(), null);
		return "";
	}

	public boolean isEnabled() {
		return true;
	}

	public boolean isHandled() {
		return true;
	}


	@SuppressWarnings("rawtypes")
	public void updateElement(UIElement element, Map parameters) {
		
		IEditorPart editorPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();

		if(!(editorPart instanceof TraceEditor));
		TraceEditor editor = (TraceEditor) editorPart;
		String viewId = editor.getContents().getViewID();
		String elementViewId = (String) parameters.get(PARAM_VIEW_ID);
		
		element.setChecked(elementViewId.equals(viewId));
		


	}

}
