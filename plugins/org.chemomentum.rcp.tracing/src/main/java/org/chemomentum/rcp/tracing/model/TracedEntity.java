package org.chemomentum.rcp.tracing.model;

import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.PropertyDescriptor;

import de.fzj.unicore.rcp.wfeditor.model.PropertySource;

public abstract class TracedEntity extends PropertySource implements ITracedEntity {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Property that holds the entity's id (String value)
	 */
	public static final String PROP_ID = "id";
	
	

	protected TraceModel graph;
	
	public TracedEntity(String iD, TraceModel graph) {
		super();
		setPropertyValue(PROP_ID, iD);
		this.graph = graph;
	}
	
	@Override
	protected void addPropertyDescriptors() {
		IPropertyDescriptor descr = new PropertyDescriptor(PROP_ID,PROP_ID); 
		addPropertyDescriptor(descr);
	}
	

	public String getID() {
		return (String) getPropertyValue(PROP_ID);
	}




	public TraceModel getGraph() {
		return graph;
	}



	public void setGraph(TraceModel graph) {
		this.graph = graph;
	}
	
	public TracedEntity clone() throws CloneNotSupportedException
	{
		TracedEntity result = (TracedEntity) super.clone();
		return result;
	}
	
	public String toString()
	{
		return getID();
	}
}
