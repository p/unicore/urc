/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.tracing.parts.hierachic.commands;

import org.chemomentum.rcp.tracing.parts.hierachic.HierarchicTraceEdgePart;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;

public class MoveAllBendpointsCommand
extends Command 
{

	private HierarchicTraceEdgePart traceEdge;
	private Point delta;

	public MoveAllBendpointsCommand(HierarchicTraceEdgePart transition, Point delta) {
		super();
		this.traceEdge = transition;
		this.delta = delta;
	}

	public void execute() {
		redo();
	}

	public void redo()
	{
		traceEdge.move(delta.x, delta.y);
	}

	public void undo() {

		traceEdge.move(-delta.x, -delta.y);
	}

}


