/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.chemomentum.rcp.tracing.parts.sequence;

import java.beans.PropertyChangeEvent;
import java.util.Arrays;
import java.util.List;

import org.chemomentum.rcp.tracing.images.ImageRegistry;
import org.chemomentum.rcp.tracing.model.TraceModel;
import org.chemomentum.rcp.tracing.model.TracedMessage;
import org.chemomentum.rcp.tracing.model.TracedService;
import org.chemomentum.rcp.tracing.parts.hierachic.HierarchicTraceEdgePart;
import org.chemomentum.rcp.tracing.parts.hierachic.HierarchicTraceGraphPart;
import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ImageFigure;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.draw2d.text.TextFlow;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.tools.DirectEditManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;



/**
 * @author hudsonr
 * Created on Jun 30, 2003
 */
public class SequenceTraceNodeServicePart extends SequenceTraceNodePart implements NodeEditPart
{

	protected DirectEditManager manager;
	protected Image image;

	/**
	 * @see org.eclipse.gef.EditPart#activate()
	 */
	public void activate() {
		super.activate();
		getModel().addPropertyChangeListener(this);
	}

	protected void addTooltip(Figure fig)
	{
		String tooltext = ((TracedService) getModel()).getID();

		Font labelFont = Display.getDefault().getSystemFont();
		TextFlow tf = new TextFlow(tooltext);
		tf.setFont(labelFont);
		FlowPage fp = new FlowPage();
		fp.add(tf);

		Panel panel = new Panel();
		panel.add(fp);
		panel.setLayoutManager(new StackLayout());
		panel.setPreferredSize(500,50);

		fp.getLayoutManager().layout(panel);
		panel.setPreferredSize(tf.getSize());

		fig.setToolTip(panel);
	}

	/*protected void applyLayout(CompoundDirectedGraph graph, Map map) {
		Node n = (Node)map.get(this);
		getFigure().setBounds(new Rectangle(n.x, 50, n.width, n.height));

		for (int i = 0; i < getSourceConnections().size(); i++) {
			SequenceTraceEdgePart edge = (SequenceTraceEdgePart) getSourceConnections().get(i);
			edge.applyLayout(graph, map);
		}
	}*/

	/*public void contributeEdgesToGraph(CompoundDirectedGraph graph, Map map) {
		List outgoing = getSourceConnections();
		for (int i = 0; i < outgoing.size(); i++) {
			SequenceTraceEdgePart part = (SequenceTraceEdgePart)getSourceConnections().get(i);
			part.contributeToGraph(graph, map);
		}
		for (int i = 0; i < getChildren().size(); i++) {
			SequenceTraceNodeServicePart child = (SequenceTraceNodeServicePart)children.get(i);
			child.contributeEdgesToGraph(graph, map);
		}
	}*/

	/*public void contributeToGraph(CompoundDirectedGraph graph, Subgraph s, Map map)
	{
		//	String subKey = FindSubgraphUtil.findSubgraphKeyFor(getModel());
		//	Subgraph sub = (Subgraph) map.get(subKey);
		//	if(sub == null)
		//	{
		//		sub = new Subgraph(subKey);
		//		map.put(subKey, sub);
		//		graph.subgraphs.add(sub);
		//	}



		Node n = new Node(this, s);
		//	sub.addMember(n);
		n.outgoingOffset = getAnchorOffset();
		n.incomingOffset = getAnchorOffset();
		n.width = getFigure().getPreferredSize().width;
		n.height = getFigure().getPreferredSize().height;
		map.put(this, n);
		graph.nodes.add(n);
	}*/

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	protected void createEditPolicies() {
		//	installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new ActivityNodeEditPolicy());

	}

	@Override
	protected IFigure createFigure() {
		Rectangle bounds =new Rectangle(0,0,200,200);
		//Rectangle boundsforFigure = new Rectangle(0,0,200,600);

		Figure panel=new Panel()
		{
			protected void paintChildren(Graphics g)
			{
				super.paintChildren(g);
			}
		};
		//panel.setBounds(bounds);

		GridLayout layout = new GridLayout();
		layout.numColumns=1;

		panel.setLayoutManager(layout);



		ImageDescriptor imageDescr = ImageRegistry.getImageDescriptorForService(getModel().getType());
		image = new Image(null, imageDescr.getImageData().scaledTo(bounds.width, bounds.height));
		Figure imagefig = new ImageFigure(image);
		//imagefig.setBounds(boundsforFigure);



		Figure line = new Figure() {

			public void paint(Graphics graphics) {
				super.paint(graphics);
			}
			protected void paintFigure(Graphics graphics) {

				Rectangle rectangle = getClientArea().getCopy();

				Rectangle clip = graphics.getClip(Rectangle.SINGLETON);
				graphics.setLineStyle(SWT.LINE_CUSTOM);
				graphics.setLineWidth(2);
				graphics.setForegroundColor(ColorConstants.gray);
				graphics.drawLine(rectangle.x+95, clip.y, rectangle.x+95, clip.y+clip.height);


			}
		};

		//line.setBounds(boundsforFigure);

		//Rectangle r=getContentPane().getBounds();
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace=false;
		gridData.grabExcessVerticalSpace=false;
		gridData.heightHint=200;
		gridData.widthHint=200;
		layout.setConstraint(imagefig, gridData);

		panel.add(imagefig);

		gridData = new GridData();
		gridData.grabExcessHorizontalSpace=false;
		gridData.grabExcessVerticalSpace=false;
		gridData.heightHint=1000;
		gridData.widthHint=200;
		layout.setConstraint(line, gridData);

		panel.add(line);  
		//addTooltip();

		
		
		return panel;
	}
	
	public TimeScalePart getTimeScalePart(){
		for (Object o : getParent().getChildren()) {
			if(o instanceof TimeScalePart){
				return (TimeScalePart)o;
			}
		}
		return null;
	}

	/**
	 * @see org.eclipse.gef.EditPart#deactivate()
	 */
	public void deactivate() {
		super.deactivate();
		getModel().removePropertyChangeListener(this);
		if(image != null) image.dispose();
	}

	/*int getAnchorOffset()
	{
		return 10;
	}*/

	/**
	 * Returns the model associated with this EditPart
	 * @return the model
	 */
	public TracedService getModel() {
		return (TracedService)super.getModel();
	}

	public TraceModel getTraceModel() {
		return (TraceModel) super.getModel();
	}
	
	/**
	 * In this view, there is a one-to-many relation between messages
	 * and connection edit parts. In order to deal with this, messages
	 * are represented by ID Strings. All messages with the same ID 
	 * share a common edit part. The Strings are returned here as model
	 * objects as well as in the {@link HierarchicTraceEdgePart#getModel()}
	 * method. In order to obtain the underlying messages, call {@link HierarchicTraceGraphPart#getMessagesWithKey(String)}
	 */
	protected List getModelSourceConnections() {
		TracedMessage[] msgs = getModel().getOutgoingMessages();
		return Arrays.asList(msgs);
	}


	/**
	 * In this view, there is a one-to-many relation between messages
	 * and connection edit parts. In order to deal with this, messages
	 * are represented by ID Strings. All messages with the same ID 
	 * share a common edit part. The Strings are returned here as model
	 * objects as well as in the {@link HierarchicTraceEdgePart#getModel()}
	 * method. In order to obtain the underlying messages, call {@link HierarchicTraceGraphPart#getMessagesWithKey(String)}
	 */
	protected List getModelTargetConnections() {
		TracedMessage[] msgs = getModel().getIncomingMessages();
		return Arrays.asList(msgs);
	}


	public void refresh()
	{
		super.refresh();
	}

	protected void performDirectEdit() {
	}

	/**
	 * @see org.eclipse.gef.EditPart#performRequest(org.eclipse.gef.Request)
	 */
	public void performRequest(Request request) {
		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT)
			performDirectEdit();
	}

	/**
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		//	String prop = evt.getPropertyName();
		//	if (FlowElement.CHILDREN.equals(prop))
		refreshChildren();
		//	else if (FlowElement.INPUTS.equals(prop))
		refreshTargetConnections();


		// Causes Graph to re-layout	
		((GraphicalEditPart)(getViewer().getContents())).getFigure().revalidate();
	}



	/**
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#setFigure(org.eclipse.draw2d.IFigure)
	 */
	protected void setFigure(IFigure figure) {
		figure.getBounds().setSize(0,0);
		super.setFigure(figure);
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#toString()
	 */
	public String toString() {
		return getModel().toString();
	}

	/**
	 * @see NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	public ConnectionAnchor getSourceConnectionAnchor(ConnectionEditPart connection) {
		//return new ChopboxAnchor(getFigure());
		return new TimeAnchor(getFigure(),((SequenceTraceEdgePart)connection));
	}

	/**
	 * @see org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.Request)
	 */
	public ConnectionAnchor getSourceConnectionAnchor(Request request) {
		return new ChopboxAnchor(getFigure());

		//return new TimeAnchor(getFigure(),(TracedMessage)request.getModel());
	}

	/**
	 * @see NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	public ConnectionAnchor getTargetConnectionAnchor(ConnectionEditPart connection) {
		//return new ChopboxAnchor(getFigure());
		return new TimeAnchor(getFigure(),((SequenceTraceEdgePart)connection));
	}

	/**
	 * @see org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.Request)
	 */
	public ConnectionAnchor getTargetConnectionAnchor(Request request) {
		return new ChopboxAnchor(getFigure());
		//return new TimeAnchor(getFigure(),(TracedMessage)connection.getModel());
	}

	@Override
	public String getModelId() {
		if(getModel() == null) return null;
		else return getModel().getID();
	}

}
