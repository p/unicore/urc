package org.chemomentum.rcp.tracing.parts.sequence;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.chemomentum.rcp.tracing.TraceEditor;
import org.chemomentum.rcp.tracing.TraceViewer;
import org.chemomentum.rcp.tracing.TracingConstants;
import org.chemomentum.rcp.tracing.model.TimeScaleModel;
import org.chemomentum.rcp.tracing.model.TracedMessage;
import org.chemomentum.rcp.tracing.model.TracedService;
import org.chemomentum.rcp.tracing.views.sequence.TimeScaleFigure;
import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Edge;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gef.tools.DirectEditManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;

import de.fzj.unicore.rcp.common.guicomponents.StringComboBoxCellEditor;

public class TimeScalePart extends SequenceTraceNodePart implements NodeEditPart
{

	private EntityDirectEditManager directManager = null;
	private Label label;
	//store all instances
	private ArrayList<SequenceTraceNodeServicePart> instances;

	private void performDirectEdit() {

		if (directManager == null) {
			directManager = new EntityDirectEditManager(label,((TimeScaleModel)getModel()).getFactors());
		}
		//set visible
		directManager.show();
	}

	private class EntityDirectEditManager extends DirectEditManager {
		//scale factors
		private List<String> iterations=new ArrayList<String>();
		private Label label;


		/**
		 * @param label
		 * @param iterations list with scale factors
		 */
		public EntityDirectEditManager(Label label,List<String> iterations) {
			super(TimeScalePart.this, StringComboBoxCellEditor.class,
					new EntityCellEditorLocator(label));
			this.label=label;
			this.iterations=iterations;
		}

		/**
		 * initialize combo box with scale factors
		 */
		protected void initCellEditor() {
			StringComboBoxCellEditor editor = (StringComboBoxCellEditor) getCellEditor();
			editor.getComboBox().setEditable(false);
			editor.getComboBox().setBackground(ColorConstants.white);
			editor.setItems(iterations.toArray(new String[0]));
			String initialLabelText = label.getText();
			editor.setValue(initialLabelText);
		}
	}

	private class EntityCellEditorLocator implements CellEditorLocator {
		private Label label;

		public EntityCellEditorLocator(Label label){
			this.label=label;
		}

		public void relocate(CellEditor celleditor) {
			Control c = celleditor.getControl();
			org.eclipse.swt.graphics.Point pref = c.computeSize(-1, -1);
			Rectangle rect = label.getTextBounds().getCopy();
			label.translateToAbsolute(rect);
			c.setBounds(rect.x - 1, rect.y - 1, pref.x + 1, pref.y + 1);
		}
	}

	public void activate() {
		super.activate();
		
		
	}

	Date begin=Calendar.getInstance().getTime(), end=Calendar.getInstance().getTime();

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public long getMillisFromBeginTo(long millis){
		return (long)(210+Math.abs((double)(millis-begin.getTime())/getScalingFactor() * 50));
	}

	public void setScalingFactor(long factor){
		TraceEditor editor = ((TraceViewer) getViewer()).getEditor();
		SequenceViewData data = (SequenceViewData) editor.getViewData(TracingConstants.VIEW_ID_SEQUENCE_DIAGRAM);
		if(data == null) data = new SequenceViewData(factor);
		else data.setScalingFactor(factor);
		editor.setViewData(TracingConstants.VIEW_ID_SEQUENCE_DIAGRAM, data);
		getModel().setLongValue(factor);
	}

	public long getScalingFactor(){
		return getModel().getLongValue();
	}
	
	public TimeScaleModel getModel()
	{
		return (TimeScaleModel) super.getModel();
	}

	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new EntityDirectEditPolicy());
	}

	public void performRequest(Request req) {
		if (req.getType().equals(RequestConstants.REQ_DIRECT_EDIT)
				|| req.getType().equals(RequestConstants.REQ_OPEN)) {
			performDirectEdit();
			return;
		}
		super.performRequest(req);
	}

	public int getHeightOffset()
	{
		return 300;
	}

	protected IFigure createFigure() {

		if(getScalingFactor() == -1)
		{
			TraceEditor editor = ((TraceViewer) getViewer()).getEditor();
			SequenceViewData data = (SequenceViewData) editor.getViewData(TracingConstants.VIEW_ID_SEQUENCE_DIAGRAM);
			if(data == null) data = new SequenceViewData(1000);
			editor.setViewData(TracingConstants.VIEW_ID_SEQUENCE_DIAGRAM, data);
			long scalingFactor = data.getScalingFactor();
			getModel().setLongValue(scalingFactor);
		}

		final int tickWidth = 20;
		final int timeLineOffset = 40;
		final int tickLabelOffset = 3;
		
		//Rectangle bounds =new Rectangle(0,0,200,200);
		//Rectangle boundsforFigure = new Rectangle(0,0,200,600);

		Figure panel=new Panel();
		GridLayout layout = new GridLayout();
		layout.numColumns=1;
		panel.setLayoutManager(layout);



		TimeScaleFigure figure = new TimeScaleFigure().build();
		label=figure.getScalingLabel();
		label.setText(((TimeScaleModel)getModel()).getSelectedValue());


		Figure line = new Figure() {

			public void paint(Graphics graphics) {
				super.paint(graphics);
			}
			protected void paintFigure(Graphics graphics) {
				Rectangle rectangle = getClientArea().getCopy();
				Rectangle clip = graphics.getClip(Rectangle.SINGLETON);
				clip.y-=100;
				clip.height+=200;
				int deltaY = (clip.y-rectangle.y);
				int counter=(int) Math.ceil(deltaY/50.0);
				int offset = (50 - deltaY % 50) % 50 +20;
			

				for (int i = 0; i <= clip.height; i+=50) {
					int y = clip.y+offset+i;
					if(i == 0)
					{
						//draw line
						graphics.setLineStyle(SWT.LINE_DOT);
						graphics.setLineWidth(2);
						graphics.setForegroundColor(ColorConstants.gray);
						int x1 = rectangle.x+timeLineOffset;
						int y1 = Math.max(260,y);
						int x2 = rectangle.x+timeLineOffset;
						int y2 = clip.y+clip.height;
						graphics.drawLine(x1, y1, x2, y2);

					}
					if(counter == 0)
					{
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd\n      hh:mm:ss.SSS");
						String zero = String.valueOf(counter++) +" = "+df.format(getBegin());
						graphics.drawText(zero, rectangle.x+timeLineOffset+tickWidth/2+tickLabelOffset, y-10);
					}
					else graphics.drawText(String.valueOf(counter++), rectangle.x+timeLineOffset+tickWidth/2+tickLabelOffset, y-10);
					graphics.setLineStyle(SWT.LINE_CUSTOM);
					graphics.drawLine(rectangle.x+timeLineOffset-tickWidth/2, y, rectangle.x+timeLineOffset+tickWidth/2, y);
					

				}

			}
		};

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace=false;
		gridData.grabExcessVerticalSpace=false;
		gridData.heightHint=180;
		gridData.widthHint=200;
		gridData.horizontalAlignment = SWT.LEFT;
		layout.setConstraint(figure, gridData);

		panel.add(figure);

		gridData = new GridData();
		gridData.grabExcessHorizontalSpace=false;
		gridData.grabExcessVerticalSpace=false;
		gridData.heightHint=1000;
		gridData.widthHint=200;
		layout.setConstraint(line, gridData);

		panel.add(line);    
		//addTooltip();
		return panel;
	}

	public int getPixelHigh(){
		//if(instances ==null)setInstances();
		return (int)((double)(getEnd().getTime()-getBegin().getTime())/getScalingFactor() *50);
	}


	public ConnectionAnchor getSourceConnectionAnchor(ConnectionEditPart arg0) {
		return new ChopboxAnchor(getFigure());
	}


	public ConnectionAnchor getSourceConnectionAnchor(Request arg0) {
		return new ChopboxAnchor(getFigure());
	}


	public ConnectionAnchor getTargetConnectionAnchor(ConnectionEditPart arg0) {
		return new ChopboxAnchor(getFigure());
	}


	public ConnectionAnchor getTargetConnectionAnchor(Request arg0) {
		return new ChopboxAnchor(getFigure());
	}

	private class EntityDirectEditPolicy extends DirectEditPolicy {

		/**
		 * create command for request
		 * @param request for direct editing
		 * @return Command
		 */
		protected Command getDirectEditCommand(DirectEditRequest request) {
			SetTimescaleCommand command = new SetTimescaleCommand();
			command.setName((String) request.getCellEditor().getValue());
			return command;
		}

		protected void showCurrentEditValue(DirectEditRequest request) {}

		/**
		 * DirectEditCommand
		 */
		private class SetTimescaleCommand extends Command {

			//old scale factor
			private String oldName;
			//new scale factor
			private String newName;
			private TimeScaleModel model;

			/**
			 * change models
			 */
			public void execute() {
				model= getModel();

				long oldfactor=getScalingFactor();
				//set old scale factor
				//oldName = model.getSelectedValue();

				//set new name/scale factor
				model.setSelectedValue(newName);
				label.setText(newName);

				//get matching milliseconds
				long millis=model.getLong(newName);
	
				setScalingFactor(millis);
				// model.getInstances();

				//refresh parent for repaint*/
				//	getFigure().repaint();
				((SequenceTraceGraphPart)getParent()).refresh();
				((SequenceTraceGraphPart)getParent()).getFigure().repaint();
				getRoot().refresh();
			}

			/**
			 * set new name/ scale factor
			 * @param name
			 */
			public void setName(String name) {
				newName = name;
			}

			/**
			 * reverse model changes
			 */
			public void undo() {


			}
		}
	}

	/**
	 * Set a list with all instances
	 * @param hash list with instance names and matching models
	 */
	public void setInstances(){
		//instances=hash;

		List list=getParent().getChildren();
		ArrayList c=new ArrayList();
		List edges=new ArrayList<SequenceTraceEdgePart>();

		for (Object o : list) {
			if(o instanceof SequenceTraceNodeServicePart){
				c.add((SequenceTraceNodeServicePart)o);
				List l=((SequenceTraceNodeServicePart)o).getSourceConnections();
				for (Object obj : l) {
					edges.add((SequenceTraceEdgePart)obj);
				}
			}
		}

		instances=c;

		List liste=getSortedList(c);
		long time=((TracedMessage)liste.get(liste.size()-1)).getTimestamp().getTimeInMillis()-((TracedMessage)liste.get(0)).getTimestamp().getTimeInMillis();

		setBegin(((TracedMessage)liste.get(0)).getTimestamp().getTime());
//		((TimeScaleFigure)getFigure().getChildren().get(0)).getBeginningLabel().setText(getBegin().toString());
		setEnd(((TracedMessage)liste.get(liste.size()-1)).getTimestamp().getTime());

	}

	/**
	 * Return list with all instances
	 * @return list with instance names and matching models
	 */
	public ArrayList<SequenceTraceNodeServicePart> getInstances(){
		return instances;
	}

	public void contributeEdgesToGraph(CompoundDirectedGraph graph, Map map) {

		SequenceTraceGraphPart parent = (SequenceTraceGraphPart) getParent();
		//HashMap<String, SequenceTraceNodeServicePart> hash=getInstances();
		ArrayList c=getInstances();
		//Collection c=hash.values();
		Iterator i=c.iterator();

		while(i.hasNext()){
			SequenceTraceNodeServicePart service=(SequenceTraceNodeServicePart)i.next();
			Node target=(Node)map.get(service);
			Node source=(Node)map.get(this);
			Edge e = new Edge(this, source,target);

			graph.edges.add(e);
			map.put(this.getModel(),e);
		}	
	}


	public List getSortedList(ArrayList<SequenceTraceNodeServicePart> toSort){

		List<TracedMessage> list = new ArrayList<TracedMessage>();
		Iterator<SequenceTraceNodeServicePart> serv=toSort.iterator();

		while(serv.hasNext()){

			TracedService s=((SequenceTraceNodeServicePart)serv.next()).getModel();
			TracedMessage[] msgs=s.getOutgoingMessages();

			for (int i = 0; i < msgs.length; i++) {
				TracedMessage ms=msgs[i];


				boolean found=false;
				for (int j = 0; j < list.size(); j++) {
					if(list.get(j).getTimestamp().getTimeInMillis() > ms.getTimestamp().getTimeInMillis()){
						list.add(j,ms);
						found=true;
						break;
					}
				}
				if(!found)list.add(ms);
			}
		}

		return list;
	}

	public String getModelId() {
		return "time scale";
	}

}
