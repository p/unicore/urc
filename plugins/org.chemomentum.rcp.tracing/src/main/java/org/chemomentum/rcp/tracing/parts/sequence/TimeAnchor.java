package org.chemomentum.rcp.tracing.parts.sequence;

import java.util.Calendar;

import org.eclipse.draw2d.AnchorListener;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;

public class TimeAnchor implements ConnectionAnchor{


	Calendar messageTime;
	SequenceTraceEdgePart messagePart;
	IFigure owner;

	public TimeAnchor(IFigure owner, SequenceTraceEdgePart messageP){
		//super(owner);
		this.owner=owner;
		this.messagePart=messageP;
		this.messageTime=messagePart.getModel().getTimestamp();
	}


	


	public void addAnchorListener(AnchorListener arg0) {
		// TODO Auto-generated method stub
		
	}



	public Point getLocation(Point arg0) {
		if(messageTime != null){

			SequenceTraceGraphPart graphP=(SequenceTraceGraphPart)messagePart.getRoot().getChildren().get(0);
			TimeScalePart timescale=graphP.getTimeScalePart();

			long hoehe=timescale.getMillisFromBeginTo(messageTime.getTimeInMillis());

			Point p=new Point();
			p.setLocation(getReferencePoint().x, this.owner.getBounds().getTop().y+(int)(hoehe));
			//p.setLocation(super.getReferencePoint().x,super.getReferencePoint().y+10); 42483
			getOwner().translateToAbsolute(p);

			return p;
		}
		else{
			return getReferencePoint();
		}
	}



	public IFigure getOwner() {
		return this.owner;
	}



	public Point getReferencePoint() {
		return this.owner.getBounds().getCenter();
	}



	public void removeAnchorListener(AnchorListener arg0) {
		// TODO Auto-generated method stub
		
	}
}
