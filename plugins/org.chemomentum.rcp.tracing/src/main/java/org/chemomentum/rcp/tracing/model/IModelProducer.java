/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.tracing.model;

import java.util.Set;

import org.apache.xmlbeans.XmlObject;
import org.chemomentum.rcp.tracing.exceptions.TraceModelBuildingException;
import org.chemomentum.tracer.client.IMessageQuery;

/**
 * @author demuth
 *
 */
public interface IModelProducer {

	/**
	 * Returns the model versions for which this Producer should be used.
	 * @return
	 */
	public String[] getModelVersions();
	
	/**
	 * Creates the model (graph of plain old java objects) from the message trace.
	 * @param trace the xml document to parse
	 * @param requiredProperties only parse the selected properties from the xml source and add them to the model
	 * @return
	 */
	public TraceModel buildModel(XmlObject trace, Set<String> requiredProperties) throws TraceModelBuildingException;
	
	/**
	 * Used to create a query that can only yield a single result, namely the
	 * original message from which the TracedMessage object was created.
	 * @param msg
	 * @return
	 */
	public IMessageQuery buildUniqueQuery(TracedMessage msg);

}
