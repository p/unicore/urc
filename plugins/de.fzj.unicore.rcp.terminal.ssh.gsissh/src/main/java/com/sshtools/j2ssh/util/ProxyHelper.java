/*
 *  GSI-SSHTools - Java SSH2 API
 *
 *  Copyright (C) 2007 STFC/CCLRC.
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  You may also distribute it and/or modify it under the terms of the
 *  Apache style J2SSH Software License. A copy of which should have
 *  been provided with the distribution.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  License document supplied with your distribution for more details.
 *
 */

/**
 *
 * This class provides some helpful routines for proxy handling
 */

package com.sshtools.j2ssh.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.globus.common.CoGProperties;
import org.globus.gsi.X509Credential;
import org.globus.util.Util;

import com.sshtools.j2ssh.configuration.SshConnectionProperties;

public class ProxyHelper implements Runnable {
	private static Logger log = Logger.getLogger("de.fzj.unicore.rcp.terminal.ssh.gsissh.GSISSHTerminalSession");

    public static boolean proxyExists() {
	String filename = CoGProperties.getDefault().getProxyFile();
	if(filename==null) return false;
	File file = new File(filename);
	return file.exists() && file.isFile();
    }

    public static void saveProxy(X509Credential theProxy, SshConnectionProperties props) {
	String proxyFile = CoGProperties.getDefault().getProxyFile();			 
        OutputStream out = null;
        try {
            File file = Util.createFile(proxyFile);
            if (!Util.setOwnerAccessOnly(proxyFile)) {
                log.warn("Warning: could not set permissions on proxy file.");
		    }
            out = new FileOutputStream(file);
            theProxy.save(out);
	   } catch (Exception e) {
            System.out.println("Failed to save proxy to proxy file: " + e.getMessage());
	    message(props, "Failed to save proxy to proxy file: " + e.getMessage());
        } finally {
            if (out != null) {
                try { 
		    out.close(); 
		} catch(Exception e) {}
            }
        }
    }


    public void run() {
	if(proxyExists()) {
//	    int ret = JOptionPane.showConfirmDialog(null, 
//	    		"You have a proxy certificate stored on a disk which may have been created by this terminal, do you want to delete it?", 
//	    		"GSI-SSHTerm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
	    MessageBox messageBox = new MessageBox(new Shell(), SWT.ERROR
				| SWT.YES | SWT.NO);
		messageBox.setMessage("You have a proxy certificate stored on a disk which may have been created by this terminal, do you want to delete it?");
		messageBox.setText("Destroy Proxy");
		int ret = messageBox.open();
	    if(ret==SWT.YES) destroyProxy();
	}
    }

    private static void message(SshConnectionProperties props, String s) {
    	MessageBox messageBox = new MessageBox(new Shell(), SWT.ICON_ERROR | SWT.OK );		        
    	messageBox.setText("Problem saving proxy");
    	messageBox.setMessage(s);
    	messageBox.open();

    }

    public static void showProxyInfo(java.awt.Component parent) { // NO_UCD
//	String identity="", subject="", issuer="", lifetime="", storage="", type="";
//	CoGProperties cogproperties = CoGProperties.getDefault();
//
//	try {
//	    if (!(new File(cogproperties.getProxyFile())).exists()) {
//		storage = "No local proxy.";
//	    } else {
//		GlobusCredential globuscredential = new GlobusCredential(cogproperties.getProxyFile());
//		globuscredential.verify();
//		storage = "Local File: "+cogproperties.getProxyFile();
//		subject = CertUtil.toGlobusID(globuscredential.getSubject(),true);
//		issuer = CertUtil.toGlobusID(globuscredential.getIssuer(),true);
//		identity = CertUtil.toGlobusID(globuscredential.getIdentityCertificate().getSubjectDN().toString(),true);
//		long seconds = globuscredential.getTimeLeft();
//		long days = seconds /(60*60*24);
//		seconds = (seconds - (days*60*60*24));
//		long hours = seconds / (60*60);
//		seconds = (seconds - (hours*60*60));
//		long mins = seconds / 60;
//		seconds = (seconds - (mins*60));
//		lifetime = days+" days "+hours+" hours "+mins+" minutes "+seconds+" seconds.";
//	        int typeI = globuscredential.getProxyType();
//		type = (typeI == -1) ? "failed to determine certificate type" : CertUtil.getProxyTypeAsString(typeI);
//		if(typeI==GSIConstants.EEC) type = "end entity certificate";
//	    } 
//	} catch(GlobusCredentialException globuscredentialexception) {
//	    if(globuscredentialexception.getMessage().indexOf("Expired") >= 0) {
//		File file = new File(cogproperties.getProxyFile());
//		file.delete();
//		storage = "Expired local proxy.";
//	    } else {
//		storage = "No local proxy (Error).";			
//	    }
//	}
    }

    public static void destroyProxy() {
	String filename = CoGProperties.getDefault().getProxyFile();
	if(filename!=null) {
	    Util.destroy(new File(filename));
	}
    }


    


}
