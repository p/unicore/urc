/*
 *  GSI-SSHTools - Java SSH2 API
 *
 *  Copyright (C) 2005-7 STFC/CCLRC.
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  You may also distribute it and/or modify it under the terms of the
 *  Apache style J2SSH Software License. A copy of which should have
 *  been provided with the distribution.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  License document supplied with your distribution for more details.
 *
 */

package com.sshtools.j2ssh.authentication;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.globus.common.CoGProperties;
import org.globus.gsi.CredentialException;
import org.globus.gsi.GSIConstants;
import org.globus.gsi.X509Credential;
import org.globus.gsi.X509ExtensionSet;
import org.globus.gsi.gssapi.GlobusGSSCredentialImpl;
import org.globus.gsi.util.CertificateLoadUtil;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;

import com.sshtools.common.SshToolsConnectionProfile;
import com.sshtools.j2ssh.configuration.SshConnectionProperties;
import com.sshtools.j2ssh.util.ProxyHelper;

import de.fzj.unicore.rcp.terminal.ssh.gsissh.GSISSHConstants;
import de.fzj.unicore.rcp.terminal.ssh.gsissh.MyBouncyCastleCertProcessingFactory;

public class UserGridCredential {

	private static final Log log = LogFactory.getLog(com.sshtools.j2ssh.authentication.UserGridCredential.class);
	
	private static boolean SAVE_GRID_PROXY_INIT_PROXY=false;
	private static boolean doMyProxy=false;

	// find the certificates zip file and copy contents to $HOME/.globus/certificate where they do not exist
	private static void checkCACertificates(CoGProperties cogproperties) throws IOException {
		// check the directories exist and create if they don't
		String globusDir = System.getProperty("user.home")+"/.globus";
		if (!(new File(globusDir).exists())) {
			boolean success = (new File(globusDir).mkdir());
			if (!success) {
				throw new IOException("Couldn't create directory: "+globusDir);
			}
		}
		String caCertLocations = globusDir + "/certificates";
		File caCertLocationsF = new File(caCertLocations);
		if (!caCertLocationsF.exists()) {
			boolean success = (new File(caCertLocations).mkdir());
			if (!success) {
				throw new IOException("Couldn't create directory: "+caCertLocations);
			}
		}
		if(!caCertLocationsF.isDirectory()) {
			throw new IOException("Location: "+caCertLocations+" is not a directory");
		}
	}


	public static GSSCredential getUserCredential(SshConnectionProperties properties) throws GSSException, IOException {
		SAVE_GRID_PROXY_INIT_PROXY = ((SshToolsConnectionProfile)properties).getApplicationPropertyBoolean(GSISSHConstants.PREF_SAVE_PROXY, SAVE_GRID_PROXY_INIT_PROXY);
		
		CoGProperties cogproperties = CoGProperties.getDefault();
		checkCACertificates(cogproperties);

		int proxyType = GSIConstants.GSI_3_IMPERSONATION_PROXY;
		try {
			String cur = ((SshToolsConnectionProfile)properties).getApplicationProperty(GSISSHConstants.PREF_PROXY_TYPE, Integer.toString(GSIConstants.GSI_3_IMPERSONATION_PROXY));
			proxyType = Integer.parseInt(cur);
		} catch(Exception e) {
			throw new Error("Programming Error", e);
		}
		int lifetimeHours = 12;
		try {
			String cur = ((SshToolsConnectionProfile)properties).getApplicationProperty(GSISSHConstants.PREF_PROXY_LENGTH, "12");
			lifetimeHours = Integer.parseInt(cur);
		} catch(Exception e) {
			throw new Error("Programming Error", e);
		}

		String order = GSISSHConstants.PREF_AUTH_ORDER;
		//	if(GSISSHConstants.PREF_AUTH_ORDER==null||GSISSHConstants.PREF_AUTH_ORDER.equals(""))
		//		order = "param,proxy,cert,other";
		order = "other,proxy";

		if(properties instanceof SshToolsConnectionProfile) {
			SshToolsConnectionProfile profile = (SshToolsConnectionProfile) properties;
			order = profile.getApplicationProperty(GSISSHConstants.PREF_AUTH_ORDER, order);
		}
		String meths[] = order.split("(,|\\s)");

		log.debug("Loading grid proxy.");
		GSSCredential gsscredential = null;


		for(int i=0;i<meths.length && gsscredential == null; i++) {
			String m = meths[i];
			log.debug("Trying method " + m);
			if(meths[i].equals("")) continue;
			if(meths[i].equals("proxy")) {
				gsscredential = loadExistingProxy();
			} else if(meths[i].equals("other")) {
				gsscredential = retrieveProxyFROMJKS(properties,proxyType, lifetimeHours);
			} else {
				errorReport(null,"Method '"+meths[i]+"' is an unknown authentication method, skipping.",null );
			}
		}

		return gsscredential;
	}

	private static void errorReport(final java.awt.Component comp, final String message, final Exception ex) {
		try {
			PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable()
			{

				public void run() {
					Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
					MessageBox messageBox = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK );		        
					messageBox.setText("GSISSH Authentication");
					messageBox.setMessage(message);
					messageBox.open();
				}
			});
		} catch (Exception ex1) {
			log.info("Failed to invoke message box through Messagebox",  ex1);
		}
	}

	private static GSSCredential retrieveProxyFROMJKS(SshConnectionProperties properties, int proxyType, int lifetimeHours)
	throws IOException
	{
		GSSCredential gsscredential = null;
		CoGProperties cogproperties = CoGProperties.getDefault();
		
		Security.insertProviderAt(new BouncyCastleProvider(),1);
		CertificateLoadUtil.setProvider("BC");

		if(!doMyProxy)  {  			
			StringBuffer stringbufferF = new StringBuffer();
			StringBuffer stringbufferP = new StringBuffer();


			KeyStore store = null;
			String  passphrase = stringbufferP.toString();
			File keyfile = new File(stringbufferF.toString());

			String keyfilename = "";

			if(properties instanceof SshToolsConnectionProfile) {
				SshToolsConnectionProfile profile = (SshToolsConnectionProfile) properties;
				keyfilename = profile.getApplicationProperty(GSISSHConstants.PREF_JKS_DEFAULT_FILE, keyfilename);
				passphrase = profile.getApplicationProperty(GSISSHConstants.PREF_JKS_PASS, "");
			}

			keyfile = new File(keyfilename);
			
			BouncyCastleProvider bcprov = new BouncyCastleProvider();
			System.out.println("Using BC prov version "+bcprov.getVersion());
			
			try {
				store = KeyStore.getInstance("JKS");
				FileInputStream in = new FileInputStream(keyfile);
				store.load(in, passphrase.toCharArray());
			}
			catch(Exception exception) {
				throw new IOException("Could not open keystore file",exception);

			}

			Enumeration<String> e = null;
			try {
				e = store.aliases();
			} catch (Exception e2) {
				throw new IOException("Could not retrieve keystore aliases",e2);
			}
			if(!e.hasMoreElements()) {
				MessageBox messageBox = new MessageBox(new Shell(), SWT.ERROR
						| SWT.OK);
				messageBox.setMessage("Could not access your certificate: no certificates found in file.");
				messageBox.setText("GSI-SSHTerm Authentication");
				return null;
			}

			try {
				String alias = null;

				if(properties instanceof SshToolsConnectionProfile) {
					SshToolsConnectionProfile profile = (SshToolsConnectionProfile) properties;
					alias = profile.getApplicationProperty(GSISSHConstants.PREF_JKS_ALIAS, alias);
				}
				if(alias == null) return null;
				java.security.cert.Certificate cert = store.getCertificate(alias);
				Key key = store.getKey(alias,passphrase.toCharArray());

				if(!(cert instanceof X509Certificate)) {
					MessageBox messageBox = new MessageBox(new Shell(), SWT.ICON_ERROR | SWT.OK );		        
					messageBox.setText("GSISSH Authentication");
					messageBox.setMessage("Could not access your certificate: bad certificate type.");
					messageBox.open();
				} 
				if(!(key instanceof PrivateKey)) {
					MessageBox messageBox = new MessageBox(new Shell(), SWT.ICON_ERROR | SWT.OK );		        
					messageBox.setText("GSISSH Authentication");
					messageBox.setMessage("Could not access your certificate: bad certificate type.");
					messageBox.open();
				}	



				MyBouncyCastleCertProcessingFactory factory =
					MyBouncyCastleCertProcessingFactory.getDefault();

				X509Certificate issuerCert = (X509Certificate)cert;

				X509Credential globuscredential = factory.createCredential(new X509Certificate[] {(X509Certificate)issuerCert},
						(PrivateKey)key,
						cogproperties.getProxyStrength(), 
						lifetimeHours * 3600,
						proxyType,
						(X509ExtensionSet)null);


				if(globuscredential != null)
				{
					if(SAVE_GRID_PROXY_INIT_PROXY) {
						ProxyHelper.saveProxy(globuscredential, properties);
					} 
					try {
						//globuscredential.verify();
						gsscredential = new GlobusGSSCredentialImpl(globuscredential, 1);
					} catch(Exception exception1) {
						exception1.printStackTrace();
						StringWriter stringwriter1 = new StringWriter();
						exception1.printStackTrace(new PrintWriter(stringwriter1));
						log.debug(stringwriter1);
						if(exception1.getMessage().indexOf("Expired credentials")>=0) {
							MessageBox messageBox = new MessageBox(new Shell(), SWT.ERROR
									| SWT.OK);
							messageBox.setMessage("Your certificate has expired, please renew your certificate or try another method for authentication.");
							messageBox.setText("GSI-SSHTerm Authentication");
						} else {
							errorReport(properties.getWindow(), "Could not load your certificate", exception1);
						}
					}

				}
				return gsscredential;
			}  catch(Exception exception) {
				if(exception.getMessage().indexOf("Illegal key size")>=0) {
					exception.printStackTrace();
					StringWriter stringwriter = new StringWriter();
					exception.printStackTrace(new PrintWriter(stringwriter));
					log.debug(stringwriter);
					errorReport(properties.getWindow(), "To use this PKCS#12 file you need to install the Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy Files\n (see http://java.sun.com/javase/downloads/index.jsp for Java 6 and http://java.sun.com/javase/downloads/index_jdk5.jsp for Java 5)", exception);	    
				} else if(exception.getMessage().indexOf("wrong password")>=0) {
					exception.printStackTrace();
					StringWriter stringwriter = new StringWriter();
					exception.printStackTrace(new PrintWriter(stringwriter));
					log.debug(stringwriter);
				} else {
					exception.printStackTrace();
					StringWriter stringwriter = new StringWriter();
					exception.printStackTrace(new PrintWriter(stringwriter));
					log.debug(stringwriter);
					errorReport(properties.getWindow(), "Unknown problem while loading your certificate", exception);
				}
			}

		}
		
		return null;
	}

	private static GSSCredential loadExistingProxy()
	throws GSSException
	{
		GlobusGSSCredentialImpl globusgsscredentialimpl = null;
		CoGProperties cogproperties = CoGProperties.getDefault();
		try
		{
			if (!(new File(cogproperties.getProxyFile())).exists()) {
				return null;
			}
			X509Credential globuscredential = new X509Credential(cogproperties.getProxyFile());
			globusgsscredentialimpl = new GlobusGSSCredentialImpl(globuscredential, 1);
			globuscredential.verify();
		}
		catch(CredentialException globuscredentialexception)
		{	
			globuscredentialexception.printStackTrace();
			StringWriter stringwriter = new StringWriter();
			globuscredentialexception.printStackTrace(new PrintWriter(stringwriter));
			log.debug(stringwriter);
			if(globuscredentialexception.getMessage().indexOf("Expired") >= 0)
			{
				File file = new File(cogproperties.getProxyFile());
				file.delete();
				globusgsscredentialimpl = null;
			}
		}
		return globusgsscredentialimpl;
	}

}
