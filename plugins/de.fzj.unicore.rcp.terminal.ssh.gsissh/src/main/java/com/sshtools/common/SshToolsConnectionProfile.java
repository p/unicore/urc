//Changes (c) STFC/CCLRC 2006-2007
/*
 *  SSHTools - Java SSH2 API
 *
 *  Copyright (C) 2002 Lee David Painter.
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  You may also distribute it and/or modify it under the terms of the
 *  Apache style J2SSH Software License. A copy of which should have
 *  been provided with the distribution.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  License document supplied with your distribution for more details.
 *
 */

package com.sshtools.common;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.sshtools.j2ssh.authentication.SshAuthenticationClient;
import com.sshtools.j2ssh.configuration.SshConnectionProperties;
import com.sshtools.j2ssh.forwarding.ForwardingConfiguration;

/**
 *
 *
 * @author $author$
 * @version $Revision: 1.6 $
 */
public class SshToolsConnectionProfile
    extends SshConnectionProperties {

  /**  */
  public static final int DO_NOTHING = 1;

  /**  */
  private static final int START_SHELL = 2;

  /**  */
  public static final int EXECUTE_COMMANDS = 3;
  private Map<String,String> applicationProperties = new HashMap<String,String>();
  private Map<String,String> sftpFavorites = new HashMap<String,String>();
  private Map<String,SshAuthenticationClient> authMethods = 
	  new HashMap<String,SshAuthenticationClient>();
  private boolean requestPseudoTerminal = true;
  private boolean disconnectOnSessionClose = true;
  private int onceAuthenticated = START_SHELL;
  private String executeCommands = "";
  private boolean allowAgentForwarding = false;

  /**
   * Creates a new SshToolsConnectionProfile object.
   */
  public SshToolsConnectionProfile() {
  }

  /**
   *
   *
   * @return
   */
  public Map<String,SshAuthenticationClient> getAuthenticationMethods() {
    return authMethods;
  }

  /**
   *
   *
   * @return
   */
  public boolean requiresPseudoTerminal() {
    return requestPseudoTerminal;
  }

  /**
   *
   *
   * @return
   */
  public boolean disconnectOnSessionClose() {
    return disconnectOnSessionClose;
  }

  /**
   *
   *
   * @param requiresPseudoTerminal
   */
  public void setRequiresPseudoTerminal(boolean requiresPseudoTerminal) {
    this.requestPseudoTerminal = requiresPseudoTerminal;
  }

  /**
   *
   *
   * @param disconnectOnSessionClose
   */
  public void setDisconnectOnSessionClose(boolean disconnectOnSessionClose) {
    this.disconnectOnSessionClose = disconnectOnSessionClose;
  }

  

  /**
   *
   *
   * @param onceAuthenticated
   */
  public void setOnceAuthenticatedCommand(int onceAuthenticated) {
    this.onceAuthenticated = onceAuthenticated;
  }

  /**
   *
   *
   * @return
   */
  public int getOnceAuthenticatedCommand() {
    return onceAuthenticated;
  }

  /**
   *
   *
   * @param executeCommands
   */
  public void setCommandsToExecute(String executeCommands) {
    this.executeCommands = executeCommands;
  }

  /**
   *
   *
   * @return
   */
  public String getCommandsToExecute() {
    return executeCommands;
  }

  /**
   *
   *
   * @param name
   * @param defaultValue
   *
   * @return
   */
  public String getApplicationProperty(String name, String defaultValue) {
    String value = (String) applicationProperties.get(name);

    if (value == null) {
      return defaultValue;
    }
    else {
      return value;
    }
  }

  /**
   *
   *
   * @param name
   * @param defaultValue
   *
   * @return
   */
  public Map<String,String> getSftpFavorites() {
    return sftpFavorites;
  }

  

  

  /**
   *
   *
   * @param name
   * @param defaultValue
   *
   * @return
   */
  public boolean getApplicationPropertyBoolean(String name,
                                               boolean defaultValue) {
    try {
      return new Boolean(getApplicationProperty(name,
                                                String.valueOf(defaultValue))).
          booleanValue();
    }
    catch (NumberFormatException nfe) {
      return defaultValue;
    }
  }

//  /**
//   *
//   *
//   * @param name
//   * @param defaultColor
//   *
//   * @return
//   */
//  public Color getApplicationPropertyColor(String name, Color defaultColor) {
//    return PropertyUtil.stringToColor(getApplicationProperty(name,
//        PropertyUtil.colorToString(defaultColor)));
//  }

  /**
   *
   *
   * @param name
   * @param value
   */
  public void setApplicationProperty(String name, String value) {
    applicationProperties.put(name, value);
  }

  

  /**
   *
   *
   * @param name
   * @param value
   */
  public void setApplicationProperty(String name, boolean value) {
    applicationProperties.put(name, String.valueOf(value));
  }

//  /**
//   *
//   *
//   * @param name
//   * @param value
//   */
//  public void setApplicationProperty(String name, Color value) {
//    applicationProperties.put(name, PropertyUtil.colorToString(value));
//  }

  /**
   * @return
   */
  public Set<String> getApplicationPropertyKeys() {
  	return applicationProperties.keySet();
  }

  

// TODO UCdetector: Remove unused code: 
//   /**
//    *
//    *
//    * @param method
//    */
//   public void addAuthenticationMethod(SshAuthenticationClient method) {
//     if (method != null) {
//       if (!authMethods.containsKey(method.getMethodName())) {
//         authMethods.put(method.getMethodName(), method);
//       }
//     }
//   }

  /**
   *
   *
   * @param config
   */
  @SuppressWarnings("unchecked")
public void addLocalForwarding(ForwardingConfiguration config) {
    if (config != null) {
      localForwardings.put(config.getName(), config);
    }
  }

  /**
   *
   *
   * @param config
   */
  @SuppressWarnings("unchecked")
public void addRemoteForwarding(ForwardingConfiguration config) {
    if (config != null) {
      remoteForwardings.put(config.getName(), config);
    }
  }

  /**
   *
   *
   * @return
   */
  public boolean getAllowAgentForwarding() {
    return allowAgentForwarding;
  }

  /**
   *
   *
   * @param allowAgentForwarding
   */
  public void setAllowAgentForwarding(boolean allowAgentForwarding) {
    this.allowAgentForwarding = allowAgentForwarding;
  }

  


//  /**
//   *
//   *
//   * @param in
//   *
//   * @throws InvalidProfileFileException
//   */
//  private void open(InputStream in) throws InvalidProfileFileException {
//    try {
//      SAXParserFactory saxFactory = SAXParserFactory.newInstance();
//      SAXParser saxParser = saxFactory.newSAXParser();
//      XMLHandler handler = new XMLHandler();
//      saxParser.parse(in, handler);
//      handler = null;
//
//      //            in.close();
//    }
//    catch (IOException ioe) {
//      throw new InvalidProfileFileException("IO error. "
//                                            + ioe.getMessage());
//    }
//    catch (SAXException sax) {
//      throw new InvalidProfileFileException("SAX Error: "
//                                            + sax.getMessage());
//    }
//    catch (ParserConfigurationException pce) {
//      throw new InvalidProfileFileException("SAX Parser Error: "
//                                            + pce.getMessage());
//    }
//    finally {
//    }
//  }


  

  /**
   *
   *
   * @return
   */
  @SuppressWarnings("unchecked")
public String toString() {
    String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    xml += ("<SshToolsConnectionProfile Hostname=\"" + host + "\" Port=\""
            + String.valueOf(port) + "\" Username=\"" + username + "\""
            + " Provider=\"" + getTransportProviderString() + "\">");
    xml += ("   <PreferedCipher Client2Server=\"" + prefEncryption
            + "\" Server2Client=\"" + prefDecryption + "\"/>\n");
    xml += ("   <PreferedMac Client2Server=\"" + prefRecvMac
            + "\" Server2Client=\"" + prefSendMac + "\"/>\n");
    xml += ("   <PreferedCompression Client2Server=\"" + prefRecvComp
            + "\" Server2Client=\"" + prefSendComp + "\"/>\n");
    xml += ("   <PreferedPublicKey Name=\"" + prefPK + "\"/>\n");
    xml += ("   <PreferedKeyExchange Name=\"" + prefKex + "\"/>\n");
    xml += "   <OnceAuthenticated value=\"" + String.valueOf(onceAuthenticated) +
        "\"/>\n";

    if (onceAuthenticated == EXECUTE_COMMANDS) {
      xml += "    <ExecuteCommands>" + executeCommands + "</ExecuteCommands>\n";
    }

    Iterator it = authMethods.entrySet().iterator();
    Map.Entry entry;
    Properties properties;

    while (it.hasNext()) {
      entry = (Map.Entry) it.next();
      xml += ("   <AuthenticationMethod Name=\"" + entry.getKey()
              + "\">\n");

      SshAuthenticationClient auth = (SshAuthenticationClient) entry
          .getValue();
      properties = auth.getPersistableProperties();

      Iterator it2 = properties.entrySet().iterator();

      while (it2.hasNext()) {
        entry = (Map.Entry) it2.next();
        xml += ("      <AuthenticationProperty Name=\""
                + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"/>\n");
      }

      xml += "   </AuthenticationMethod>\n";
    }

    it = applicationProperties.entrySet().iterator();

    while (it.hasNext()) {
      entry = (Map.Entry) it.next();
      xml += ("   <ApplicationProperty Name=\"" + entry.getKey()
              + "\" Value=\"" + entry.getValue() + "\"/>\n");
    }

    // Write the SFTP Favorite entries to file
    it = sftpFavorites.entrySet().iterator();

    while (it.hasNext()) {
      entry = (Map.Entry) it.next();
      xml += ("   <SftpFavorite Name=\"" + entry.getKey()
              + "\" Value=\"" + entry.getValue() + "\"/>\n");
    }

    it = localForwardings.values().iterator();

    xml += "   <ForwardingAutoStart value=\"" +
        String.valueOf(forwardingAutoStart) + "\"/>\n";

    while (it.hasNext()) {
      ForwardingConfiguration config = (ForwardingConfiguration) it.next();
      xml += ("   <LocalPortForwarding Name=\"" + config.getName()
              + "\" AddressToBind=\"" + config.getAddressToBind()
              + "\" PortToBind=\"" + String.valueOf(config.getPortToBind())
              + "\" AddressToConnect=\"" + config.getHostToConnect()
              + "\" PortToConnect=\"" + String.valueOf(config.getPortToConnect())
              + "\"/>\n");
    }

    it = remoteForwardings.values().iterator();

    while (it.hasNext()) {
      ForwardingConfiguration config = (ForwardingConfiguration) it.next();
      xml += ("   <RemotePortForwarding Name=\"" + config.getName()
              + "\" AddressToBind=\"" + config.getAddressToBind()
              + "\" PortToBind=\"" + String.valueOf(config.getPortToBind())
              + "\" AddressToConnect=\"" + config.getHostToConnect()
              + "\" PortToConnect=\"" + String.valueOf(config.getPortToConnect())
              + "\"/>\n");
    }

    xml += "</SshToolsConnectionProfile>";

    return xml;
  }
}
