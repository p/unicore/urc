/*****************************************************************************
 * Copyright (c) 2006, 2007 g-Eclipse Consortium 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for the
 * g-Eclipse project founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributors:
 *    Thomas Koeckerbauer GUP, JKU - initial API and implementation
 *****************************************************************************/

package de.fzj.unicore.rcp.terminal.ssh.gsissh;

import org.eclipse.osgi.util.NLS;

/**
 * Returns the localised messages for this package.
 * 
 * @author Andre Giesler
 */
public class Messages extends NLS {
  private static final String BUNDLE_NAME = "de.fzj.unicore.rcp.terminal.ssh.gsissh.messages"; //$NON-NLS-1$
   
  public static String  GSISSHConnectionInfo_sshTerminal;
  public static String  GSISSHPreferencePage_description;
  public static String  GSISSHPreferencePage_caCertDir;
  
  
  static {
	     NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	  }

  
}
