/*****************************************************************************
 * Copyright (c) 2006, 2007 g-Eclipse Consortium
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for the
 * g-Eclipse project founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributors:
 *    Thomas Koeckerbauer GUP, JKU - initial API and implementation
 *****************************************************************************/

package de.fzj.unicore.rcp.terminal.ssh.gsissh;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.terminal.TerminalConstants;
import de.fzj.unicore.rcp.terminal.UnicoreTerminalPlugin;
import de.fzj.unicore.rcp.terminal.ui.views.TerminalConfigView;

/**
 * Manages the connection information of GSISSH plugin
 * 
 * @author Andre Giesler
 */
class GSISSHConnectionInfo implements GSISSHConstants{
	
	private String siteId;
	private String siteName;
	private String defaultMethod;
	
	private String passphrase;
	private String password;
	
	private boolean canceledPWValue;
	
	private Map<String,String> config;

	GSISSHConnectionInfo( )
	{
		this(null,null,2222);
	}
	
	private GSISSHConnectionInfo( final String username, final String hostname,
				final int port) {
		this.setUsername(username);
		this.setHostname(hostname);
		this.setPort(port);
	}
	
	GSISSHConnectionInfo(Map<String,String> config) {
		this.siteId = config.get(TerminalConstants.ID);
		this.siteName = config.get(TerminalConstants.NAME);
		this.defaultMethod = config.get(TerminalConstants.CONNECTION_TYPE);
		this.config = config;
	}

	public Map<String, String> getConfig() {
		return config;
	}
	
	public void setConfig(Map<String, String> config) {
		if(configChanged(this.config, config))
		{
			GSISSHConfigPersistence.getInstance().writeConfig(siteId, siteName, defaultMethod, config);
			
			PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable()
			{
				public void run() {
					try {
						TerminalConfigView sshConfig = (TerminalConfigView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(TerminalConfigView.ID);
						if(sshConfig != null) sshConfig.refresh();
					} catch (Exception e) {
						UnicoreTerminalPlugin.log(Status.ERROR,"Unable to refresh terminal config view: "+e.getMessage(), e );
					}
				}
			});
		
		}
		this.config = config;
		
	}
	
	private boolean configChanged(Map<String,String> oldConfig, Map<String,String> newConfig)
	{
		if(config.size() != this.config.size()) return true;
		for(String key : newConfig.keySet())
		{
			String oldValue = oldConfig.get(key);
			String newValue = newConfig.get(key);
			boolean equal = newValue == null ? oldValue == null : newValue.equals(oldValue);
			if(!equal) return true;
		}
		return false;
	}	

	public String getPassword() {
		return this.password;
	}
	
	public String getPassphrase() {
		return this.passphrase;
	}
	
	public String getHostname() {
		return config.get(GSISSH_HOST);
	}
	
	public String getProxyType() {
		return config.get(GSISSH_PROXY_TYPE);
	}
	
	public String getDelegType() {
		return config.get(GSISSH_DELEG_TYPE);
	}

	public String getUsername() {
		return config.get(GSISSH_LOGIN);
	}
	
	public String getAlias() {
		return config.get(GSISSH_ALIAS);
	}
	
	public String getSaveProxy() {
		return config.get(GSISSH_SAVED_PROXY);
	}
	
	public String getSaveProxyPath() {
		return config.get(GSISSH_SAVED_PROXY_PATH);
	}
	
	Integer getPort() {
		String s = config.get(GSISSH_PORT);
		if(s == null || NOT_SUPPORTED.equals(s)) return null;
		else return Integer.parseInt(s);		
	}
	
	Integer getProxyLifetime() {
		String s = config.get(GSISSH_PROXY_LIFETIME);
		if(s == null) return 12;
		else return Integer.parseInt(s);		
	}
	
	public void setAlias(String alias) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(alias == null) newConfig.remove(GSISSH_ALIAS);
		else newConfig.put(GSISSH_ALIAS, alias);
		setConfig(newConfig);
	}
	
	public void setSavedProxy(String saveProxy) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(saveProxy == null) newConfig.remove(GSISSH_SAVED_PROXY);
		else newConfig.put(GSISSH_SAVED_PROXY, saveProxy);
		setConfig(newConfig);
	}
	
	public void setSavedProxyPath(String saveProxyPath) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(saveProxyPath == null) newConfig.remove(GSISSH_SAVED_PROXY_PATH);
		else newConfig.put(GSISSH_SAVED_PROXY_PATH, saveProxyPath);
		setConfig(newConfig);
	}
	
	public void setProxyType(String proxyType) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(proxyType == null) newConfig.remove(GSISSH_PROXY_TYPE);
		else newConfig.put(GSISSH_PROXY_TYPE, proxyType);
		setConfig(newConfig);
	}
	
	public void setDelegType(String delegType) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(delegType == null) newConfig.remove(GSISSH_DELEG_TYPE);
		else newConfig.put(GSISSH_DELEG_TYPE, delegType);
		setConfig(newConfig);
	}
	
	public void setHostname(String hostname) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(hostname == null) newConfig.remove(GSISSH_HOST);
		else newConfig.put(GSISSH_HOST, hostname);
		setConfig(newConfig);
	}

	public void setPort(Integer port) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(port == null) newConfig.remove(GSISSH_PORT);
		else newConfig.put(GSISSH_PORT, port.toString());
		setConfig(newConfig);
	}
	
	public void setProxyLifetime(Integer lifetime) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(lifetime == null) newConfig.remove(GSISSH_PROXY_LIFETIME);
		else newConfig.put(GSISSH_PROXY_LIFETIME, lifetime.toString());
		setConfig(newConfig);
	}
	
	public void setUsername(String username) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(username == null) newConfig.remove(GSISSH_LOGIN);
		else newConfig.put(GSISSH_LOGIN, username);
		setConfig(newConfig);
	}

	private boolean isEmpty(String s)
	{
		return s == null || s.trim().length() == 0;
	}	
	
	public boolean promptPort( ) {
		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				String configured = config.get(GSISSH_PORT);
				String port = configured == null ? "2222" : configured;
				InputDialog dlg = new InputDialog(new Shell(),"Port","Please enter the port:",port,new IInputValidator() {				
					public String isValid(String newText) {
						try {
							Integer.parseInt(newText);
							return null;
						} catch (Exception e) {
							return "Input is not an integer number";
						}
					}
				}); 
				dlg.open();
				if ( dlg.getReturnCode() == InputDialog.OK )
					setPort(Integer.parseInt(dlg.getValue()));
				else setPort(null);
				
			}
		} );
	return !isEmpty(config.get(GSISSH_HOST));
}

	public boolean promptUsername( ) {
			Display.getDefault().syncExec( new Runnable() {
				public void run() {
					String configured = config.get(GSISSH_LOGIN);
					String login = configured == null ? "login" : configured;
					Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();
					if (shell == null) shell = new Shell();
					InputDialog dlg = new InputDialog(shell,"User name","Please enter the user name:",login,null); 
					dlg.open();
					String result = dlg.getValue();
					if ( dlg.getReturnCode() == InputDialog.OK && result.trim().length() > 0){
						setUsername(result);
					}						
					else setUsername(null);
				}
			} );
		return !isEmpty(config.get(GSISSH_LOGIN));
	}
	
	public boolean promptHost( ) {
		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				String configured = config.get(GSISSH_HOST);
				String hostname = configured == null ? "hostname" : configured;
				InputDialog dlg = new InputDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(),
						"Host name","Please enter the host name:",hostname,null); 
				dlg.open();
				if ( dlg.getReturnCode() == InputDialog.OK )
					setHostname(dlg.getValue());
				else setHostname(null);
				
			}
		} );
	return !isEmpty(config.get(GSISSH_HOST));
}
	
//	public boolean promptYesNo( final String str ) {
//		final boolean[] result = { false };
//		Display.getDefault().syncExec( new Runnable() {
//			public void run() {
//				result[0] = MessageDialog.openQuestion( null,
//						Messages.getString( "SshShell.sshTerminal" ), //$NON-NLS-1$
//						str );
//			}
//		} );
//		return result[0];
//		//return false;
//	}
	
	public boolean promptPassphrase( final String message ) { // NO_UCD
		if(passphrase != null) return true;

		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				String msg = "Please, enter passphrase for your private key";
				
				HiddenInputDialog dlg = new HiddenInputDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(),"Passphrase",msg,"", null);
				dlg.open();
				if ( dlg.getReturnCode() == InputDialog.OK )
					GSISSHConnectionInfo.this.passphrase = dlg.getValue();
				else
					GSISSHConnectionInfo.this.passphrase = null;
			}
		} );
	return !isEmpty(passphrase);
	}
	
//	public boolean promptPassword( final String message ) {
//		if(password != null) return true;
//
//			Display.getDefault().syncExec( new Runnable() {
//				public void run() {
//					String msg = "Please, enter your password for user "  + getUsername();
//					
//
//					String username = getUsername();
//					if(username != null) 
//					{
//						msg += " for username "+username;
//					}
//					HiddenInputDialog dlg = new HiddenInputDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(),"Password",msg,"", null);
//					dlg.setErrorMessage(msg);
//					dlg.open();
//					if ( dlg.getReturnCode() == InputDialog.OK )
//						GSISSHConnectionInfo.this.password = dlg.getValue();
//					else
//						GSISSHConnectionInfo.this.password = null;
//				}
//			} );
//		
//		return !isEmpty(password);
//	}
	
	

	/**
	 * Returns if the user pushed cancel when queried for password for the ssh session.
	 * @return Returns <code>true</code> if the user pushed cancel when asked for the pw,
	 * <code>true</code> otherwise.
	 */
	public boolean getCanceledPWValue() {
		return this.canceledPWValue;
	}
}
