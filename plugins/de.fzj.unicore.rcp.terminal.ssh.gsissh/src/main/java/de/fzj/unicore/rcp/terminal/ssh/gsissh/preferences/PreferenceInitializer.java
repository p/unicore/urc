package de.fzj.unicore.rcp.terminal.ssh.gsissh.preferences;

import java.io.File;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.globus.common.CoGProperties;

import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.terminal.ssh.gsissh.UnicoreTerminalGSISSHPlugin;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

		
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = UnicoreTerminalGSISSHPlugin.getDefault().getPreferenceStore();
		String certLocations = CoGProperties.getDefault().getCaCertLocations();
		certLocations = store.getString(PreferenceConstants.P_CA_CERT_DIR);
		if(certLocations == null || certLocations.equals(""))
		{
			try {
				IProject project = PathUtils.getTempProject();
				
					IPath p = project.getLocation();
					
					String dirName = p.toString();
					File f = new File(dirName);
					if (!f.isDirectory()) {
						f.mkdir();					
					} 
					
					certLocations = f.getAbsolutePath()+File.separator+"globus_certificates";
					f = new File(certLocations);
					if (!f.isDirectory()) {
						f.mkdir();					
					} 

					//doesn't work, Path is strange after getFolder
//	                folder = project.getFolder(p.toString());	
//	                if(!folder.exists()){
//                	folder.create(true, true, null);
//                	}
	                                
//	                p = p.append(".globus");
//	                folder = project.getFolder(p);	                
//	                if(!folder.exists()){
//	                	folder.create(true, true, null);
//	                }
//	                p = p.append("certificates");
//	                folder = project.getFolder(p);	                
//	                if(!folder.exists()){
//	                	folder.create(true, true, null);
//	                }
//	                
//	                folder = project.getFolder(p);
//	                folder.create(true, true, null);
//	                certLocations = folder.getFullPath().toString();


			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			String userHome = System.getProperty("user.home");
//			File globusDir = new File(userHome,".globus");
//			certLocations = globusDir.getAbsolutePath()+File.separator+"certificates";
		}
	    store.setDefault( PreferenceConstants.P_CA_CERT_DIR,  certLocations);
	    
	    CoGProperties.getDefault().setCaCertLocations(certLocations);
	}

}
