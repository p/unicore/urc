package de.fzj.unicore.rcp.terminal.ssh.gsissh.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {
	/**
	   * Name of the preference field for storing the "CA certificate directory" option.
	   */
	  public static final String P_CA_CERT_DIR = "caCertDir"; //$NON-NLS-1$
}
