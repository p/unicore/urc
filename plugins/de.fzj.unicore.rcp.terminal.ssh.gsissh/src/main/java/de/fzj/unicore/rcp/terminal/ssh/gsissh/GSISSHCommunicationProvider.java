package de.fzj.unicore.rcp.terminal.ssh.gsissh;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;

import de.fzj.unicore.rcp.terminal.TerminalConnection;
import de.fzj.unicore.rcp.terminal.extensionpoints.ICommunicationProvider;

/**
 * The GSISSH plugin
 * 
 * @author Andre Giesler
 */
public class GSISSHCommunicationProvider 
implements ICommunicationProvider

{

	public TerminalConnection establishConnection(Map<String, String> config,
			IProgressMonitor progress) {
		return new GSISSHTerminalSession().establishConnection(config, progress);
	}
	
	
}