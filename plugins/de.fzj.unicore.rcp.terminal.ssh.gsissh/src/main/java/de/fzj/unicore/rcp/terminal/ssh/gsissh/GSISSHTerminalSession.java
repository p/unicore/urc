package de.fzj.unicore.rcp.terminal.ssh.gsissh;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.globus.common.CoGProperties;
import org.globus.gsi.GSIConstants;
import org.globus.gsi.X509Credential;
import org.globus.gsi.X509ExtensionSet;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.sshtools.common.SshToolsConnectionProfile;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.SshEventAdapter;
import com.sshtools.j2ssh.SshException;
import com.sshtools.j2ssh.SshThread;
import com.sshtools.j2ssh.authentication.AuthenticationProtocolState;
import com.sshtools.j2ssh.authentication.GSSAPIKeyexAuthenticationClient;
import com.sshtools.j2ssh.configuration.ConfigurationLoader;
import com.sshtools.j2ssh.connection.Channel;
import com.sshtools.j2ssh.connection.ChannelEventAdapter;
import com.sshtools.j2ssh.connection.ChannelState;
import com.sshtools.j2ssh.io.DynamicBuffer;
import com.sshtools.j2ssh.session.SessionChannelClient;
import com.sshtools.j2ssh.transport.HostKeyVerification;
import com.sshtools.j2ssh.transport.MessageStoreEOFException;
import com.sshtools.j2ssh.transport.TransportProtocol;
import com.sshtools.j2ssh.transport.TransportProtocolException;
import com.sshtools.j2ssh.transport.TransportProtocolState;
import com.sshtools.j2ssh.transport.publickey.SshPublicKey;

import de.fzj.unicore.rcp.common.utils.FileUtils;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.keystore.Certificate;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;
import de.fzj.unicore.rcp.terminal.IBidirectionalConnection;
import de.fzj.unicore.rcp.terminal.ITerminalListener;
import de.fzj.unicore.rcp.terminal.TerminalConnection;
import de.fzj.unicore.rcp.terminal.extensionpoints.ICommunicationProvider;
import de.fzj.unicore.rcp.terminal.ssh.gsissh.preferences.PreferenceConstants;
import eu.unicore.security.canl.AuthnAndTrustProperties;
import eu.unicore.security.canl.CredentialProperties;
import eu.unicore.security.wsutil.client.authn.DelegationSpecification;
import eu.unicore.util.httpclient.ClientProperties;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * Creates a connection to GSISSH server and sets all
 * required connection parameters
 * 
 * @author Andre Giesler
 */
class GSISSHTerminalSession 
implements ICommunicationProvider, ITerminalListener, GSISSHConstants
{

	private Map<String,GSISSHConnectionInfo> cachedConnectionInfos = new HashMap<String, GSISSHConnectionInfo>(); // store connection infos for single sign on

	private static Logger log = Logger.getLogger("de.fzj.unicore.rcp.terminal.ssh.gsissh.GSISSHTerminalSession");

	private Thread thread;

	private int result1 = AuthenticationProtocolState.READY;

	private SessionChannelClient session;

	private DynamicBuffer toServerBuffer;
	private DynamicBuffer fromServerBuffer;

	private final static int BANNER_TIMEOUT = 200;

	/**
	 * A Transport protocol event handler instance that receives notifications
	 * of transport layer events such as Socket timeouts and disconnection.
	 */
	final private SshEventAdapter eventHandler = null;

	/**
	 * The SSH Authentication protocol implementation for this SSH client. The
	 * SSH Authentication protocol runs over the SSH Transport protocol as a
	 * transport protocol service.
	 */
	//private AuthenticationProtocolClient authentication;

	private HostKeyVerification hostKeyVerification;

	private SshToolsConnectionProfile currentConnectionProfile;

	// flag to say user closed connection by closing terminal tab
	private boolean disconnecting = false;

	/**
	 * The SSH Connection protocol implementation for this SSH client. The
	 * connection protocol runs over the SSH Transport protocol as a transport
	 * protocol service and is started by the authentication protocol after a
	 * successful authentication.
	 */
	//private ConnectionProtocol connection;

	/** The current state of the authentication for the current connection. */
	//private int authenticationState = AuthenticationProtocolState.READY;

	/**
	 * Flag indicating whether the forwarding instance is created when the
	 * connection is made.
	 */
	//private boolean useDefaultForwarding = true;

	/** Provides a high level management interface for SSH port forwarding. */
	//private ForwardingClient forwarding;

	/** The SSH Transport protocol implementation for this SSH Client. */
	//private TransportProtocolClient transport;

	private SshClient ssh;

	public GSISSHTerminalSession()
	{
		fromServerBuffer = new DynamicBuffer();
		toServerBuffer = new DynamicBuffer();
	}

	public void windowSizeChanged( final int cols, final int lines, final int xPixels, final int yPixels ) {
	}	

	/**
	 * TODO split into several methods
	 */
	public TerminalConnection establishConnection(Map<String,String> config, IProgressMonitor progress) 
	{
		result1 = AuthenticationProtocolState.READY;

		TerminalConnection result = new TerminalConnection();

		if (System.getProperty("log4j.properties") != null) {
			try {
				Properties properties = new Properties();
				properties.load(ConfigurationLoader.loadFile(System.getProperty(
						"log4j.properties")));

				try {
					Class<?>cls = Class.forName("org.apache.log4j.PropertyConfigurator");
					Object obj = cls.newInstance();
					Method method = cls.getMethod("configure", new Class[] {Properties.class});
					method.invoke(obj, new Object[] {properties});
				}
				catch (Throwable ex) {
				}
			}
			catch (IOException ex) {
				configureBasicLogging();
			}
		}

		try { 
			ConfigurationLoader.initialize(false);//, context);

			String gsisshHost, gsisshUsername, gsisshAlias,
			gsisshProxyType, gsisshDelegType, gsisshSaveProxy,
			gsisshSaveProxyPath;

			Integer gsisshPort, gsisshProxyLifetime;

			GSISSHConnectionInfo info = readConnectionInfo(config);
			gsisshHost = info.getHostname();
			if(gsisshHost == null) 
			{
				boolean okPressed = info.promptHost();
				if(!okPressed) return null;
			}	

			gsisshPort = info.getPort();
			if(gsisshPort == null) 
			{
				boolean okPressed = info.promptPort();
				if(!okPressed) return null;
			}

			gsisshUsername = info.getUsername();
			if(gsisshUsername == null) 
			{
				boolean okPressed = info.promptUsername();
				if(!okPressed) return null;
				config.put(GSISSHConstants.GSISSH_LOGIN, info.getUsername());
			}

			gsisshProxyLifetime = info.getProxyLifetime();
			if(gsisshProxyLifetime == null) 
			{
				gsisshProxyLifetime = 12;
			}

			this.currentConnectionProfile = new SshToolsConnectionProfile();
			currentConnectionProfile.setHost(gsisshHost);
			currentConnectionProfile.setPort(gsisshPort);
			currentConnectionProfile.setUsername(gsisshUsername);

			IClientConfiguration secProps = null;

			TargetSystemNode targetSystem = null;
			String id = config.get(ID);
			Node node = NodeFactory.getNodeFor(NodePath.parseString(id));
			if(node instanceof TargetSystemFactoryNode) 
			{
				TargetSystemFactoryNode targetSystemFactory = (TargetSystemFactoryNode) node;
				if(targetSystemFactory.getChildren().size() == 0) 
				{
					targetSystemFactory.refresh(1,true,null);
					if(targetSystemFactory.getChildren().size() == 0)
						try {
							targetSystemFactory.createTargetSystem();
						} catch (Exception e) {
							//TerminalFromServicebrowserPlugin.log(Status.ERROR,"Error while creating target system for terminal connection: "+e.getStackTrace(),e);
							MessageBox messageBox = new MessageBox(new Shell(), SWT.ICON_QUESTION
									| SWT.OK | SWT.NO);
							messageBox.setMessage("There are currently active forwarding channels!\n\n"
									+
									"Error while creating target system for terminal connection: "+e.getStackTrace());
						}
				}
				if(targetSystemFactory.getChildren().size() == 0) 
				{
					return null;
				}
				targetSystem = (TargetSystemNode) targetSystemFactory.getChildren().get(0);
				secProps = targetSystem.getUASSecProps();
			}
			else if(node instanceof TargetSystemNode) 
			{
				targetSystem = (TargetSystemNode) node;
				secProps = targetSystem.getUASSecProps();
			}
			else
			{
				DelegationSpecification delegate = DelegationSpecification.DO_NOT;
				EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
				epr.addNewAddress().setStringValue("https://urc.anonymous.info");
				secProps = IdentityActivator.getDefault().getClientConfiguration(epr, delegate);
			}

			// Creating globus formatted public CA keys in pre-defined directory 
			IPreferenceStore prefstore = UnicoreTerminalGSISSHPlugin.getDefault().getPreferenceStore();
			String cacertdir = prefstore.getString(PreferenceConstants.P_CA_CERT_DIR );

			File dir = new File(cacertdir);
			FileUtils.rm(dir);
			if(!dir.exists()) 
			{
				if(!dir.mkdirs())
				{

					throw new Exception("Could not create directory for exporting trusted CA certificates: "+dir);
				}
			}

			String praefix = dir.getCanonicalPath() + File.separator;

			for(X509Certificate x509cert: secProps.getValidator().getTrustedIssuers()){
				String hash = CryptoUtils.getSubjectHash(x509cert);
				Certificate cert = new Certificate(x509cert);
				String filename = praefix + hash + ".0" ;
				boolean write = false;
				if(new File(filename).exists()){
					try {
						x509cert.checkValidity();
					} catch (Exception e) {
						write = true;
					}
					InputStream inStream = new FileInputStream(filename);
					CertificateFactory cf = CertificateFactory.getInstance("X.509");
					X509Certificate tmpcert = (X509Certificate)cf.generateCertificate(inStream);
					inStream.close();
					if(!x509cert.equals(tmpcert)){
						write = true;
					}	
				}
				else{
					write = true;
				}
				if(write){
					FileOutputStream fos = null;
					try {
						fos = new FileOutputStream(filename);
						// write out the certificate in ASN1.DER encoding
						// later we may be able how to write PEM encoded format :-)
						fos.write(cert.getCertAsPEMString().getBytes());
						fos.close();
					}
					catch (IOException e) {
						throw new KeyStoreException(e.getMessage());
					}
				}	    		
			}			    	
			
			// TODO this is really ugly
			CredentialProperties credProps=((AuthnAndTrustProperties)((ClientProperties)secProps).getAuthnAndTrustConfiguration())
					.getCredentialProperties();
			String loc=credProps.getValue(CredentialProperties.PROP_LOCATION);
			String password=credProps.getValue(CredentialProperties.PROP_PASSWORD);
			String alias=credProps.getValue(CredentialProperties.PROP_KS_ALIAS);
			
			currentConnectionProfile.setApplicationProperty(GSISSHConstants.PREF_JKS_DEFAULT_FILE, loc);
			currentConnectionProfile.setApplicationProperty(GSISSHConstants.PREF_JKS_PASS, password);

			Security.addProvider(new BouncyCastleProvider()); // TODO ???? 

			gsisshAlias = info.getAlias();
			if(gsisshAlias == null) 
			{
				gsisshAlias = alias;
				info.setAlias(gsisshAlias);
				config.put(GSISSHConstants.GSISSH_ALIAS, info.getAlias());
			}	

			currentConnectionProfile.setApplicationProperty(GSISSHConstants.PREF_JKS_ALIAS, gsisshAlias);

			Key key = secProps.getCredential().getKey();
			X509Certificate cert=secProps.getCredential().getCertificate();
			
			//Bugfix in BouncyCastle lib: instead of
			//BouncyCastleCertProcessingFactory factory =
			//			BouncyCastleCertProcessingFactory.getDefault();

			// fix http://bugzilla.mcs.anl.gov/globus/show_bug.cgi?id=4933
			MyBouncyCastleCertProcessingFactory factory =
					MyBouncyCastleCertProcessingFactory.getDefault();

			CoGProperties cogproperties = CoGProperties.getDefault();
			cogproperties.setProperty(CoGProperties .ENFORCE_SIGNING_POLICY,Boolean.FALSE.toString());
			String cACertsFromCoG = CoGProperties.getDefault().getCaCertLocations();
			String caCertsFromPrefStore = prefstore.getString(PreferenceConstants.P_CA_CERT_DIR );
			if(!cACertsFromCoG.equals(caCertsFromPrefStore)){
				CoGProperties.getDefault().setCaCertLocations(caCertsFromPrefStore);
			}			
			log.info("CAcertsDir = " + CoGProperties.getDefault().getCaCertLocations());


			int proxyType = GSIConstants.GSI_4_IMPERSONATION_PROXY;
			gsisshProxyType = info.getProxyType();
			if(gsisshProxyType != null && gsisshProxyType.equals(GSISSH_PROXY_TYPES[1])) 
			{
				proxyType = GSIConstants.GSI_3_IMPERSONATION_PROXY;
			}
			else if(gsisshProxyType != null && gsisshProxyType.equals(GSISSH_PROXY_TYPES[2])) {
				proxyType = GSIConstants.GSI_2_PROXY;
			}

			currentConnectionProfile.setApplicationProperty(GSISSHConstants.PREF_DELEGATION_TYPE, 
					"full");
			gsisshDelegType = info.getDelegType();
			if(gsisshDelegType != null && gsisshDelegType.equals(GSISSH_DELEG_TYPES[1])) {
				currentConnectionProfile.setApplicationProperty(GSISSHConstants.PREF_DELEGATION_TYPE, 
						"limited");
			}
			else if(gsisshDelegType != null && gsisshDelegType.equals(GSISSH_DELEG_TYPES[2])){
				currentConnectionProfile.setApplicationProperty(GSISSHConstants.PREF_DELEGATION_TYPE, 
						"none");
			}
			info.setDelegType(gsisshDelegType);
			config.put(GSISSHConstants.GSISSH_DELEG_TYPE, info.getDelegType());

			gsisshSaveProxy = info.getSaveProxy();
			if(gsisshSaveProxy != null && gsisshSaveProxy.equals("true")) 
			{
				currentConnectionProfile.setApplicationProperty(GSISSHConstants.PREF_SAVE_PROXY, true);
				gsisshSaveProxyPath = info.getSaveProxyPath();
				if(gsisshSaveProxyPath != null && !gsisshSaveProxyPath.equals("")) 
				{
					CoGProperties.getDefault().setProxyFile(gsisshSaveProxyPath);
				}
			}
			else{
				currentConnectionProfile.setApplicationProperty(GSISSHConstants.PREF_SAVE_PROXY, false);
			}

			X509Credential globuscredential = factory.createCredential(new X509Certificate[] {(X509Certificate)cert},
					(PrivateKey)key,
					cogproperties.getProxyStrength(), 
					gsisshProxyLifetime * 3600,
					proxyType,
					(X509ExtensionSet)null);

			if(globuscredential != null)
			{
				try {
					globuscredential.verify();
				} catch(Exception exception1) {
				}
			}



			disconnecting = false;
			connect(this.currentConnectionProfile, false, new SshEventAdapter() {
				@SuppressWarnings("deprecation")				
				public void onDisconnect(TransportProtocol transport) {				
					if(transport.getState().getValue()!=TransportProtocolState.DISCONNECTED) {
						transport.getState().setLastError(null);
						transport.getState().setDisconnectReason(null);
						return;
					}
					log.info("The connection has disconnected cleaning up");
					if(thread!=null) try { thread.stop(); } catch(Throwable t) {}
					Exception e = transport.getState().getLastError();
					if(e!=null && e instanceof java.io.EOFException && e.getMessage().indexOf("remote host has closed the connection")>=0) {
						if(!disconnecting)log.info("The remote host closed the connection!\n");
						showMessage(IStatus.ERROR,"The remote host closed the connection!\n");
					} else if(e!=null && e instanceof java.net.SocketException) {

						showMessage(IStatus.ERROR,"There was a problem with the connection, it was closed by the operating system.");
					} else if(transport.getState().getDisconnectReason()!=null && !transport.getState().getDisconnectReason().equals("")&& !transport.getState().getDisconnectReason().equals("Terminating connection")) {

						showMessage(IStatus.ERROR,"The remote host has closed the connection:\n" + transport.getState().getDisconnectReason());
					} else if(e!=null) {
						if(!disconnecting) {
							if(e.getMessage()!=null && e.getMessage().indexOf("GSSException")>=0) {
								showMessage(IStatus.ERROR,"There was a problem while authenticating with the remote host",e);
							} else {
								showMessage(IStatus.ERROR,"There was a problem with the connection or with authenticating",e);
							}
						}

						closeConnection(false);
					}
				}
			});

			// wait until authentification process is completed
			if(waitforconnect(progress)){

				authenticationComplete(false);

				InputStream is = fromServerBuffer.getInputStream();
				OutputStream os = toServerBuffer.getOutputStream();

				IBidirectionalConnection connection1 = new UnicoreBidirectionalConnection(this, is,os, this.ssh, getCurrentConnectionProfile());

				result.setConnection(connection1);
				result.setListener(this);
				if(targetSystem!=null){
					result.setName(currentConnectionProfile.getUsername()+"@"+targetSystem.getName());
				}
				else{
					result.setName(currentConnectionProfile.getUsername()+"@"+config.get(NAME));
				}


				return result;
			}

		} catch(SshException se) {
			log.info("authenticationComplete passing on error: "+se);
		} catch(IOException ioe) {
			log.info("authenticationComplete passing on error: "+ioe);
		} catch(Throwable t) {
			log.info("authenticationComplete passing on error: "+t);
			throw new RuntimeException(t);
		}	
		return result;
	}

	private GSISSHConnectionInfo readConnectionInfo(Map<String,String> config)
	{
		try
		{
			String id = config.get(ID);
			GSISSHConnectionInfo result = cachedConnectionInfos.get(id);			
			if(result == null) result = new GSISSHConnectionInfo(config);
			result.setConfig(config);
			cachedConnectionInfos.put(id, result);
			return result;
		}catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}	

	public boolean isConnected() {
		return (session != null) && ssh.isConnected();//&& super.isConnected();
	}

	protected void closeConnection(boolean doDisconnect) {		
		if (doDisconnect) {
			// We should disconnect the session
			try {
				this.fromServerBuffer.close();
				this.toServerBuffer.close();

				if (session != null) {
					session.close();
				}

				if ( (ssh != null) && ssh.isConnected()) {
					Thread.sleep(1000);
					ssh.disconnect();
				}

			}
			catch (Exception se) {
				se.printStackTrace();
				log.info("Disconnect, An unexpected error occured!\n\n" + se.getMessage());
			}
		}


		// Null the session and current properties
		ssh = null;
		session = null;

	}

	private synchronized boolean waitforconnect(IProgressMonitor progress) throws InterruptedException { // wait for outcome
		while(result1!= AuthenticationProtocolState.COMPLETE && !progress.isCanceled()){
			wait(2000);
			if(result1==AuthenticationProtocolState.FAILED){
				return false;
			}
		}
		return true;
	}

	/**
	 *
	 *
	 * @param newProfile
	 *
	 * @return
	 *
	 * @throws IOException
	 */
	private boolean authenticateUser(boolean newProfile) throws IOException {
		try {
			// We should now authenticate
			this.result1 = AuthenticationProtocolState.READY;

			// Our authenticated flag
			boolean authenticated = false;

			// Get the supported authentication methods
			//			java.util.List auths = SshAuthenticationClientFactory
			//			.getSupportedMethods();

			// Get the available methods
			@SuppressWarnings("rawtypes")
			java.util.List supported = null;
			supported = ssh.getAvailableAuthMethods(currentConnectionProfile
					.getUsername());

			if(supported==null) throw new IOException("There are no authentication methods which both the server and client understand.\n Cannot connect.");

			//String b = ssh.getAuthenticationBanner(BANNER_TIMEOUT);
			//System.out.println(b);
			// if terminal emulator available, display SSH_MSG_USERAUTH_BANNER
			//            if (emulation != null) {
			//                OutputStream os = emulation.getTerminalOutputStream();
			//                String b = ssh.getAuthenticationBanner(BANNER_TIMEOUT);
			//                try {
			//                    String[] lines = b.split("\r|\n", -1);
			//                    for (int i = 0; i < lines.length; i++) {
			//                      byte[] bytes = lines[i].getBytes();
			//                      os.write(bytes, 0, bytes.length);
			//                      os.write("\r\n".getBytes(), 0, "\r\n".getBytes().length);
			//                    }
			//                }
			//                catch (Exception e) {
			//                }
			//            }


			if (supported.contains("gssapi-keyex")) {
				GSSAPIKeyexAuthenticationClient aa = new GSSAPIKeyexAuthenticationClient();
				aa.setProperties(getCurrentConnectionProfile());
				aa.setUsername(getCurrentConnectionProfile().getUsername());
				result1 = ssh.authenticate(aa,getCurrentConnectionProfile().getHost());

				if (result1 == AuthenticationProtocolState.COMPLETE) {
					return true;
				}
			}
			else{
				showMessage(IStatus.ERROR,"Server side doesn't support 'gssapi-keyex' authentication. Supported are: " + supported.toString());
			}

			//agiesler: not longer supported, see http://bugzilla.globus.org/globus/show_bug.cgi?id=6911
			//			if (supported.contains("external-keyx")) {
			//				EKEYXAuthenticationClient aa = new EKEYXAuthenticationClient();
			//				aa.setProperties(getCurrentConnectionProfile());
			//				aa.setUsername(getCurrentConnectionProfile().getUsername());
			//				result1 = ssh.authenticate(aa,getCurrentConnectionProfile().getHost());
			//
			//				if (result1 == AuthenticationProtocolState.COMPLETE) {
			//					return true;
			//				}
			//			}

			// Create a list for display that will contain only the
			// supported and available methods
			//java.util.List display = new java.util.ArrayList();

			// Did we receive a banner from the remote computer
			final String banner = ssh.getAuthenticationBanner(BANNER_TIMEOUT);

			if (banner != null) {
				if (!banner.trim().equals("")) {

					log.info("Failed to invoke and wait on BannerDialog");

				}
			}

			//			// Are there any authentication methods within the properties file?
			//			// Iterate through selecting only the supported and available
			//			Iterator it = supported.iterator();
			//
			//			LinkedList allowed = new LinkedList();
			//
			//			while (it.hasNext() && !authenticated) {
			//				Object obj = it.next();
			//
			//				if (auths.contains(obj)) {
			//					display.add(obj);
			//					allowed.add(obj);
			//					//System.out.println(obj);
			//				}
			//			}
			//
			//			// First look to see if we have any authenticaiton methods available
			//			// in the profile properties object as this will overide a manual selection
			//			java.util.Map authMethods = (Map) ( (HashMap) this.currentConnectionProfile
			//					.getAuthenticationMethods())
			//					.clone();
			//			it = authMethods.entrySet().iterator();
			//
			//			//Iterator it2 = null;
			//			//java.util.List selected;
			//
			//			// Loop until the user either cancels or completes
			//			boolean completed = false;
			//			SshAuthenticationClient auth;
			//			Map.Entry entry;
			//			String msg = null;
			//
			//			while (!completed
			//					&&
			//					(ssh.getConnectionState().getValue() !=
			//						TransportProtocolState.DISCONNECTED)) {
			//				auth = null;
			//				// Select an authentication method from the properties file or
			//				// prompt the user to choose
			//				if (it.hasNext()) {
			//					Object obj = it.next();
			//
			//					if (obj instanceof Map.Entry) {
			//						entry = (Map.Entry) obj;
			//						auth = (SshAuthenticationClient) entry.getValue();
			//					}
			//					else if (obj instanceof String) {
			//						auth = SshAuthenticationClientFactory.newInstance( (String) obj, this.currentConnectionProfile);
			//						auth.setUsername(this.currentConnectionProfile.getUsername());
			//					}
			//					else {
			//						closeConnection(true);
			//						throw new IOException(
			//						"Iterator of Map or List of String expected");
			//					}
			//				}
			//				else {
			//					//				    selected = AuthenticationDialog.showAuthenticationDialog(this,
			//					//											     display, ( (msg == null) ? "" : msg));
			//
			//					//				    if (selected.size() > 0) {
			//					//					it = selected.iterator();
			//					//				    }
			//					//				    else {
			//					//					closeConnection(true);
			//					//
			//					//					return false;
			//					//				    }
			//				}
			//				if(auth!=null && !allowed.contains(auth.getMethodName())) auth=null;
			//				if (auth != null) {
			//
			//					if (result1 == AuthenticationProtocolState.FAILED) {
			//						msg = auth.getMethodName()
			//						+ " authentication failed, try again?";
			//						log.info(msg);
			//					}
			//
			//					// If the result returned partial success then continue
			//					if (result1 == AuthenticationProtocolState.PARTIAL) {
			//						// We succeeded so add to the connections authenticaiton
			//						// list and continue on to the next one
			//						this.currentConnectionProfile.addAuthenticationMethod(auth);
			//						msg = auth.getMethodName()
			//						+ " authentication succeeded but another is required";
			//						log.info(msg);
			//					}
			//
			//					if (result1 == AuthenticationProtocolState.COMPLETE) {
			//						authenticated = true;
			//
			//						//If successfull add to the connections list so we can save later
			//						this.currentConnectionProfile.addAuthenticationMethod(auth);
			//
			//						// Set the completed flag
			//						completed = true;
			//						//authenticationComplete(newProfile);
			//					}
			//
			//					if (result1 == AuthenticationProtocolState.CANCELLED) {
			//						ssh.disconnect();
			//
			//						return false;
			//					}
			//				}
			//			}

			// end of while
			return authenticated;
		} catch(EOFException e) {
			throw new IOException("The remote host has closed the connection.\n\nAs you are authenticating this probably means the server has given up authenticating you.");
		} catch(MessageStoreEOFException e) {
			throw new IOException("The remote host has closed the connection.\n\nAs you are authenticating this probably means the server has given up authenticating you.");
		}

	}



	/**
	 *
	 *
	 * @return
	 */
	public HostKeyVerification getHostKeyVerification() {
		return hostKeyVerification;
	}

	private void connect(final SshToolsConnectionProfile profile,
			final boolean newProfile, final  com.sshtools.j2ssh.SshEventAdapter evt) {
		// We need to connect
		ssh = new SshClient();

		// Set the current connection properties
		setCurrentConnectionProfile(profile);

		Runnable r = new Runnable() {

			public void run() {
				// Update the status bar


				//
				try {
					log.info("Connecting to "
							+ currentConnectionProfile.getHost() + " as "
							+ currentConnectionProfile.getUsername() + " at port "
							+ currentConnectionProfile.getPort());

					//SshToolsConnectionProfile cProfile = p;
					//cProfile.setWindow(getContainer().getWindow());
					if(eventHandler!=null) ssh.addEventHandler(eventHandler);
					ssh.connect(currentConnectionProfile,
							(getHostKeyVerification() == null)
							? new SinkHostKeyVerification()
					: getHostKeyVerification());

					if (!authenticateUser(newProfile)) {
						closeConnection(false);
					}
				}
				catch (IOException sshe) {

					ssh = null;
					result1=AuthenticationProtocolState.FAILED;

					if(sshe.getMessage().indexOf("Canceled by user")<0) {

						showMessage(IStatus.ERROR,"Could not establish a connection to host " + currentConnectionProfile.getHost() + ": \n\n " + sshe.getMessage(),sshe);
					}
					closeConnection(false);
				}
				catch (SecurityException se) {
					ssh = null;
					showMessage(IStatus.ERROR,"Thrown SecurityException: "+se.getMessage(),se);
					closeConnection(false);
				} catch(Throwable t) {

					showMessage(IStatus.ERROR,"Thrown error: "+t.getMessage(),t);
				} 
			}

		};

		thread = new SshThread(r,"gsissh  connection", true);
		thread.start();


	}

	public SshToolsConnectionProfile getCurrentConnectionProfile() {
		return currentConnectionProfile;
	}

	public void setCurrentConnectionProfile(SshToolsConnectionProfile profile) {
		currentConnectionProfile = profile;


	}

	private SessionChannelClient createNewSession(boolean addEventListener) throws
	IOException {
		SessionChannelClient session = ssh.openSessionChannel();
		//session.addEventListener(dataListener);
		if (addEventListener) {
			session.addEventListener(new ChannelEventAdapter() {
				public void onChannelClose(Channel channel) {
					if (ssh != null) {
						if (performVerifiedDisconnect(false)) {

							fromServerBuffer.close();
							toServerBuffer.close();

							ssh.disconnect();
							//closeSession();
						}
					}
				}
			});
		}

		// Request a pseudo terminal
		if (currentConnectionProfile.requiresPseudoTerminal()) {
			session.requestPseudoTerminal("vt100",80,24,0,0,"");
		}

		return session;
	}

	private boolean performVerifiedDisconnect(boolean force) {
		// Lets examine the profile to see if we need to close the connection
		SshToolsConnectionProfile profile = getCurrentConnectionProfile();

		if (profile.disconnectOnSessionClose()) {
			// Yes we should, lets ask the user about any forwarding channels
			if (ssh.getForwardingClient().hasActiveConfigurations()) {
				MessageBox messageBox = new MessageBox(new Shell(), SWT.ICON_QUESTION
						| SWT.YES | SWT.NO);
				messageBox.setMessage("There are currently active forwarding channels!\n\n"
						+
						"The profile is configured to disconnect when the session is closed. Closing\n"
						+ "the connection will terminate these forwarding channels with unexpected results.\n\n"
						+ "Do you want to disconnect now?");
				messageBox.setText("Auto disconnect");
				int response = messageBox.open();
				if (response == SWT.YES)
					return true;
				else
					return false;
			}
			else {
				return true;
			}
		}

		return force;
	}

	private void authenticationComplete(boolean newProfile) throws SshException,
	IOException {
		try {
			SshToolsConnectionProfile profile = this.currentConnectionProfile;

			if (profile.getOnceAuthenticatedCommand() !=
					SshToolsConnectionProfile.DO_NOTHING) {
				if (profile.getOnceAuthenticatedCommand() ==
						SshToolsConnectionProfile.EXECUTE_COMMANDS) {
					BufferedReader reader = new BufferedReader(new StringReader(profile
							.getCommandsToExecute() + "\n"));

					String cmd;

					while ( (cmd = reader.readLine()) != null) {
						if (cmd.trim().length() > 0) {
							log.info("Executing " + cmd);
							session = createNewSession(false);

							if (session.executeCommand(cmd)) {
								session.bindInputStream(toServerBuffer.getInputStream());
								session.bindOutputStream(fromServerBuffer.getOutputStream());
								//								session.bindInputStream(new BufferedInputStream(clientToServer));
								//								session.bindOutputStream(new BufferedOutputStream(fromServer));

								try {
									session.getState().waitForState(ChannelState.CHANNEL_CLOSED);
								}
								catch (InterruptedException ex) {
									log.info("The command was interrupted!");
								}
							}
						}
					}

					if (profile.disconnectOnSessionClose()) {
						ssh.disconnect();
					}
				}
				else {
					// Start the users shell
					session = createNewSession(true);

					if (session.startShell()) {
						session.bindInputStream(toServerBuffer.getInputStream());
						session.bindOutputStream(fromServerBuffer.getOutputStream());
						//						session.bindInputStream(clientToServer);
						//						session.bindOutputStream(fromServer);
					}
				}
			}
			log.info("Authentication complete and startup actions performed.");
		} catch(SshException se) {
			log.info("authenticationComplete passing on error: "+se);
			throw se;
		} catch(IOException ioe) {
			log.info("authenticationComplete passing on error: "+ioe);
			throw ioe;

		} catch(Throwable t) {
			log.info("authenticationComplete passing on error: "+t);
			throw new RuntimeException(t);
		}
	}

	public void terminated() {
		log.info("terminated");		
	}

	public void windowTitleChanged(String windowTitle) {
		// TODO Auto-generated method stub

	}

	private static void configureBasicLogging() {
		try {
			//Class cls = Class.forName("org.apache.log4j.BasicConfigurator");
			//Method method = cls.getMethod("configure", (Class) null);
			//method.invoke(null, (Object) null);

			BasicConfigurator.configure();
			//System.out.println(log.getName());
			//System.out.println(logger.getName());
			//			log.setLevel(Level.DEBUG);
			//			Logger.getRootLogger().setLevel(Level.DEBUG);
		}
		catch (Throwable ex2) {
			log.error("fritze2");
		}
	}

	private void showMessage(int status, final String message) {
		if (message != null && message.trim().length() != 0) {
			UnicoreTerminalGSISSHPlugin.log(status, message);
		}
	}

	private void showMessage(int status, final String message, Throwable t ) {
		if (message != null && message.trim().length() != 0) {
			Exception e = null;
			if(t instanceof Exception)
			{
				e = (Exception) t;
			}
			else e = new Exception(t);
			UnicoreTerminalGSISSHPlugin.log(status, message, e);
		}
	}


}

class SinkHostKeyVerification
implements HostKeyVerification {
	public boolean verifyHost(String host, SshPublicKey pk) throws
	TransportProtocolException {
		//    log.warn("Accepting host " + host
		//             + " as host key verification is disabled.");

		return true;
	}
}
