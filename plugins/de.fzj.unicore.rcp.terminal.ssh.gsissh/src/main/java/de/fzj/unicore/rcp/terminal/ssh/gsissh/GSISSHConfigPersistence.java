package de.fzj.unicore.rcp.terminal.ssh.gsissh;

import java.util.Map;

import org.eclipse.ui.IMemento;

import de.fzj.unicore.rcp.terminal.ConfigPersistence;
import de.fzj.unicore.rcp.terminal.ConnectionTypeRegistry;

/**
 * The persistence class of the GSISSH plugin
 * 
 * @author Andre Giesler
 */
public class GSISSHConfigPersistence extends ConfigPersistence implements
		GSISSHConstants {

private static ConfigPersistence instance;
	
	public static ConfigPersistence getInstance()
	{
		if(instance == null)
		{
			instance = new GSISSHConfigPersistence();
		}
		return instance;
	}
	
	@Override
	public boolean readConfig(Map<String, String> config, IMemento memento) {
		IMemento gsissh = getConnectionTypeMemento(CONNECTION_TYPE_ID_GSISSH,memento); 
		if(gsissh == null) return false;
		String gsisshHost = gsissh.getString(GSISSH_HOST);
		String gsisshPort = gsissh.getString(GSISSH_PORT);
		String gsisshLogin = gsissh.getString(GSISSH_LOGIN);
		String gsisshAlias = gsissh.getString(GSISSH_ALIAS);
		String gsisshProxyType = gsissh.getString(GSISSH_PROXY_TYPE);
		String gsisshDelegType = gsissh.getString(GSISSH_DELEG_TYPE);
		String gsisshSavedProxy = gsissh.getString(GSISSH_SAVED_PROXY);
		String gsisshSavedPRoxyPath = gsissh.getString(GSISSH_SAVED_PROXY_PATH);
		String gsisshProxyLifetime = gsissh.getString(GSISSH_PROXY_LIFETIME);
		String supportedString = memento.getString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED);
		Boolean supported = supportedString != null && supportedString.contains(CONNECTION_TYPE_ID_GSISSH);
		config.put(GSISSH_HOST,gsisshHost); 
		config.put(GSISSH_PORT,gsisshPort);
		config.put(GSISSH_LOGIN,gsisshLogin);
		config.put(GSISSH_ALIAS,gsisshAlias);
		config.put(GSISSH_PROXY_TYPE,gsisshProxyType);
		config.put(GSISSH_DELEG_TYPE,gsisshDelegType);
		config.put(GSISSH_PROXY_LIFETIME,gsisshProxyLifetime);
		config.put(GSISSH_SAVED_PROXY,gsisshSavedProxy);
		config.put(GSISSH_SAVED_PROXY_PATH,gsisshSavedPRoxyPath);
		config.put(GSISSH_SUPPORTED, supported.toString());
		return true;
	}

	@Override
	public boolean writeConfig(Map<String, String> config, IMemento memento) {
		String host = config.get(GSISSH_HOST); 
		String port = config.get(GSISSH_PORT);
		String login = config.get(GSISSH_LOGIN);
		String alias = config.get(GSISSH_ALIAS);
		String proxyType = config.get(GSISSH_PROXY_TYPE);
		String delegType = config.get(GSISSH_DELEG_TYPE);
		String proxyLifetime = config.get(GSISSH_PROXY_LIFETIME);
		String savedProxy = config.get(GSISSH_SAVED_PROXY);
		String savedProxyPath = config.get(GSISSH_SAVED_PROXY_PATH);
		String supported = config.get(GSISSH_SUPPORTED);
		String supportedString = memento.getString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED);
		if(supportedString == null) supportedString = "";
		if(Boolean.parseBoolean(supported))
		{
			if(!supportedString.contains(CONNECTION_TYPE_ID_GSISSH))
			{
				if(supportedString.length() > 0) supportedString += " ";
				supportedString += CONNECTION_TYPE_ID_GSISSH;
			}
		}
		else
		{
			if(supportedString.contains(CONNECTION_TYPE_ID_GSISSH))
			{
				supportedString.replaceFirst("\\s*"+CONNECTION_TYPE_ID_GSISSH, "");
			}
		}
		memento.putString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED, supportedString);
		
		IMemento connectionType = getConnectionTypeMemento(CONNECTION_TYPE_ID_GSISSH,memento);
		if(connectionType == null) connectionType = createConnectionTypeMemento(CONNECTION_TYPE_ID_GSISSH,memento);
		if(host != null) connectionType.putString(GSISSH_HOST,host);
		if(login != null) connectionType.putString(GSISSH_LOGIN,login);
		if(port != null) connectionType.putString(GSISSH_PORT,port);
		if(alias != null) connectionType.putString(GSISSH_ALIAS,alias);
		if(proxyType != null) connectionType.putString(GSISSH_PROXY_TYPE,proxyType);
		if(delegType != null) connectionType.putString(GSISSH_DELEG_TYPE,delegType);
		if(proxyLifetime != null) connectionType.putString(GSISSH_PROXY_LIFETIME,proxyLifetime);
		if(savedProxy != null) connectionType.putString(GSISSH_SAVED_PROXY,savedProxy);
		if(savedProxyPath != null) connectionType.putString(GSISSH_SAVED_PROXY_PATH,savedProxyPath);
		connectionType.putString(ATTRIBUTE_CONNECTION_TYPE_NAME, ConnectionTypeRegistry.getInstance().getNameForId(CONNECTION_TYPE_ID_GSISSH));
		return true;
	}

}
