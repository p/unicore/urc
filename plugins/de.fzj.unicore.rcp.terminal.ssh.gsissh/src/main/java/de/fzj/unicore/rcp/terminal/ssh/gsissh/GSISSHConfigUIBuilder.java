package de.fzj.unicore.rcp.terminal.ssh.gsissh;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.globus.common.CoGProperties;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.keystore.KeystoreCredentialController;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.terminal.BasicConfigUIBuilder;
import de.fzj.unicore.rcp.terminal.TerminalConstants;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * The UI class of the GSISSH plugin
 * 
 * @author Andre Giesler
 */
public class GSISSHConfigUIBuilder extends BasicConfigUIBuilder implements
		GSISSHConstants {
	
	Combo aliasCombo,proxyTypeCombo,delegTypeCombo;
	boolean saveProxy = false;
	String path = "";
	String proxyName = "";
	
	List<Control> controls = new ArrayList<Control>();
	Group group;
	Text textSavedProxy;
	Button browseProxyPathButton;
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	protected String getLabelForKey(String key) {
		if(key.equals(GSISSH_HOST)){
			return "Host";
		}
		else if(key.equals(GSISSH_PORT)){
			return "Port";
		}
		else if(key.equals(GSISSH_LOGIN)){
			return "Login";
		}
		else if(key.equals(GSISSH_PASS)){
			return "Passphrase";
		}
		else if(key.equals(GSISSH_PROXY_LIFETIME)){
			return "Proxy Lifetime [hours]";
		}
		else if(key.equals(GSISSH_ALIAS)){
			return "'Alias from client keystore'";
		}
		return key;
	}

	@Override
	protected String[] getRelevantConfigKeys() {
		return new String[]{GSISSH_HOST,GSISSH_PORT,GSISSH_LOGIN};
	}
	
	@Override
	public void buildUI(final Composite parent, TerminalSite site) {
	
		controls.clear();
		Map<String,String> config = site.getConnectionTypeConfigs();
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		layout.verticalSpacing = 10;
		parent.setLayout(layout);
		
		for(String key : getRelevantConfigKeys())
		{
			Label label = new Label(parent, SWT.NONE);
			label.setSize(64, 32);
			label.setText(getLabelForKey(key));
			controls.add(label);
					
			final Text text = new Text(parent, SWT.BORDER);
			final GridData textGD = new GridData();
			text.setLayoutData(textGD);
			String value = config.get(key) == null ? "" : config.get(key);
			text.setText(value);
			textFields.put(key, text);
			controls.add(text);
			final int minX = text.computeSize(-1, -1).x;
			text.addModifyListener(new ModifyListener() {			
				public void modifyText(ModifyEvent e) {
					Point p = text.computeSize(-1, -1);
					textGD.widthHint = Math.max(minX, p.x);
					parent.layout();			
				}
			});
			
			if(key.equals(GSISSH_LOGIN))
				text.setToolTipText("Leave blank, if you cannot choose between different xlogins");
			else if(key.equals(GSISSH_HOST) || key.equals(GSISSH_PORT))
				text.setToolTipText("Value is usually given by UNICORE IDB of the site. Ask Administrator if not. Can be customized.");
		}	
	    
	    group = new Group(parent, SWT.SHADOW_ETCHED_IN);
	    group.setLayout(new GridLayout(2, false));
	    group.setText("Additional Parameters");
	    controls.add(group);
	    GridData groupGridData = new GridData();
		groupGridData.horizontalSpan = 2;
	    group.setLayoutData(groupGridData);

	    Label label = new Label(group, SWT.NONE);
		label.setSize(64, 32);
		label.setText(getLabelForKey(GSISSH_PROXY_LIFETIME));
		controls.add(label);
		
		Text text = new Text(group, SWT.BORDER);
		text.setSize(64, 32);
		String value = config.get(GSISSH_PROXY_LIFETIME) == null || config.get(GSISSH_PROXY_LIFETIME)
			.equals("") ? "12" : config.get(GSISSH_PROXY_LIFETIME);
		text.setText(value);
		textFields.put(GSISSH_PROXY_LIFETIME, text);
		text.setToolTipText("Leave blank, if default (12h) should be used.");
		controls.add(text);
	    
		Label combolable = new Label(group, SWT.NONE);
		combolable.setSize(64, 32);
		combolable.setText("Alias from client keystore");
		controls.add(combolable);
		
		aliasCombo = new Combo(group, SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);
		aliasCombo.setSize(64, 32);
		controls.add(aliasCombo);
		
		String mapped_alias = "";
		String[] segs = site.getId().split( Pattern.quote( "," ) ); 
		String uri = segs[segs.length-1];
		
		if(uri!=null&&!uri.equals("")&&site.getSiteType().equals(TerminalConstants.SITE_TYPE_UNICORE)){
			IClientConfiguration secprops;
			try {
				secprops = ServiceBrowserActivator.getDefault().getUASSecProps(new URI(uri));
				mapped_alias = null; // TODO any way to pass this from IdentityPlugin?
			} catch (URISyntaxException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}		
		}
		KeystoreCredentialController kcc = (KeystoreCredentialController)IdentityActivator.getDefault().getCredential();
		
	    List<String> identityAliases = kcc.getKeystoreManager().getIdentityAliases();
	    ListIterator<String> it = identityAliases.listIterator();
	    while(it.hasNext()){
	    	String id_alias = it.next();
	    	if(aliasCombo.indexOf(id_alias) < 0) { // Not in the list yet. 
	    		aliasCombo.add(id_alias);
	            // Re-sort
	            String[] items = aliasCombo.getItems();
	            Arrays.sort(items);
	            aliasCombo.setItems(items);
	          }
	    }
	    
	    String alias = config.get(GSISSH_ALIAS) == null ? mapped_alias : config.get(GSISSH_ALIAS);
	    
	    if(alias!=null && !alias.equals("")){
	    	aliasCombo.setText(alias);
	    }
	    else{
	    	if(aliasCombo.getItems().length>0){
	    		aliasCombo.select(0);
	    	}
	    }
	    aliasCombo.setToolTipText("Select your Alias, if it is different from the mapped certificate in Security Profiles");
	    
	    Label delegComboLable = new Label(group, SWT.NONE);
	    delegComboLable.setSize(64, 32);
	    delegComboLable.setText("Delegation Type");
	    controls.add(delegComboLable);
	   
		delegTypeCombo = new Combo(group, SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);
		delegTypeCombo.setSize(64, 32);
		delegTypeCombo.setItems(GSISSH_DELEG_TYPES);	    
	    String delegType = config.get(GSISSH_DELEG_TYPE) == null ? "" : config.get(GSISSH_DELEG_TYPE);
	    if(!delegType.equalsIgnoreCase("")){
	    	delegTypeCombo.setText(delegType);
	    }
	    else{
	    	delegTypeCombo.select(0);
	    }
	    delegTypeCombo.setToolTipText("Select the delegation type");
	    controls.add(delegTypeCombo);
	    
	    Label proxyTypeComboLabel = new Label(group, SWT.NONE);
	    proxyTypeComboLabel.setSize(64, 32);
	    proxyTypeComboLabel.setText("Proxy Type");
	    controls.add(proxyTypeComboLabel);
	   
		proxyTypeCombo = new Combo(group, SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);
		proxyTypeCombo.setSize(64, 32);
		proxyTypeCombo.setItems(GSISSH_PROXY_TYPES);	    
	    String proxyType = config.get(GSISSH_PROXY_TYPE) == null ? "" : config.get(GSISSH_PROXY_TYPE);
	    if(!proxyType.equalsIgnoreCase("")){
	    	proxyTypeCombo.setText(proxyType);
	    }	
	    else{
	    	proxyTypeCombo.select(0);
	    }
	    proxyTypeCombo.setToolTipText("Select the Proxy type");
	    controls.add(proxyTypeCombo);
	    
	    GridData gridData = new GridData();
	    gridData.horizontalSpan = 2;
	    gridData.horizontalAlignment = SWT.BEGINNING; 
	    
		final Composite composite = new Composite(group, SWT.NONE);
	    GridLayout gridLayout = new GridLayout();
	    gridLayout.numColumns = 3;
	    composite.setLayout(gridLayout);
	    composite.setLayoutData(gridData);
	    controls.add(composite);
	    
	    final Button checkSavedProxy = new Button(composite, SWT.CHECK );
		checkSavedProxy.setText("Save Proxy");
		checkSavedProxy.setToolTipText("Check if the created Proxy should be saved in the file system.");
		String savedProxy = config.get(GSISSH_SAVED_PROXY) != null && config.get(GSISSH_SAVED_PROXY).equals("true") ? "true" : "false";
		boolean supported = Boolean.parseBoolean(savedProxy);;		
		checkSavedProxy.setSelection(supported);
		controls.add(checkSavedProxy);
		
		
		textSavedProxy = new Text(composite, SWT.BORDER);
		textSavedProxy.setSize(64, 32);
		
		//extract Proxy file name from CoGProperties
		String proxyFile = CoGProperties.getDefault().getProxyFile();
		if(proxyFile != null && !proxyFile.equals("")){
			proxyName = (new File(proxyFile)).getName();
		}
					
		String path = config.get(GSISSH_SAVED_PROXY_PATH) == null ? "" : config.get(GSISSH_SAVED_PROXY_PATH);
		if(path.equals("")){
			proxyFile = CoGProperties.getDefault().getProxyFile();
			if(proxyFile != null && !proxyFile.equals(""))
				path = proxyFile;
		}
		textSavedProxy.setText(path);
		textSavedProxy.setToolTipText("Specify the path where the proxy should be saved by clicking the '...'-button");
		textSavedProxy.setEnabled(checkSavedProxy.getSelection());
		textSavedProxy.setEditable(false);
		
		textFields.put(GSISSH_SAVED_PROXY_PATH, textSavedProxy);
		
		browseProxyPathButton = new Button(composite, SWT.PUSH);

		browseProxyPathButton.setText("...");
		browseProxyPathButton.setBounds(10, 5, 45, 30);
		browseProxyPathButton.setEnabled(checkSavedProxy.getSelection());
		browseProxyPathButton.setToolTipText("Specify the folder where the proxy should be saved");
		browseProxyPathButton.addSelectionListener(new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			public void widgetSelected(SelectionEvent e) {
				if(promptProxyPath(parent, textSavedProxy.getText())){
					if(proxyName == null || proxyName.equals(""))
						textSavedProxy.setText(getPath()+ File.separator + "proxy");
					else
						textSavedProxy.setText(getPath()+ File.separator + proxyName);				
				}
			}		
		});
		
		checkSavedProxy.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent event) {
				boolean enabled = checkSavedProxy.getSelection();
				if(enabled)
				{
					textSavedProxy.setEnabled(true);
					browseProxyPathButton.setEnabled(true);
					saveProxy = true;
				}
				else 
				{
					textSavedProxy.setEnabled(false);
					browseProxyPathButton.setEnabled(false);
					saveProxy = false;
				}
			}
		});
	}
	
	public boolean promptProxyPath(final Composite parent, final String initialpath ) {
		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				DirectoryDialog dlg = new DirectoryDialog(parent.getShell());
				if(initialpath != null && !initialpath.equals("")){
					String path = new File(initialpath).getParent();
					dlg.setFilterPath(path);
				}
		        dlg.setText("Save Proxy to disk Dialog");

		        dlg.setMessage("Select a directory for storing the GSISSH Proxy");

		        String dir = dlg.open();
		        if (dir != null && dir.length()>0){
					setPath(dir);
				}
				else setPath("");
			}
		} );
	return !isEmpty(getPath());
	}
	
	private boolean isEmpty(String s)
	{
		return s == null || s.trim().length() == 0;
	}

	@Override
	public String validateInput(Composite parent, TerminalSite site)
	{
		//super.validateInput(parent, site);
		for(String key : getTextFields().keySet()){
			boolean dirty = true;
			Text text = getTextFields().get(key);
			if(key.equals(GSISSH_PROXY_LIFETIME) || key.equals(GSISSH_LOGIN)){
				dirty=false;
			}
			if(dirty && text.getText().length() == 0) {
				return "Please check input at field "+getLabelForKey(key);
			}
			if(key.equals(GSISSH_PROXY_LIFETIME) || key.equals(GSISSH_PORT)){
				try {
					Integer.parseInt(text.getText());
				} catch (Exception e) {
					text.setFocus();
					text.selectAll();
					return "Input is not an integer number at field " +getLabelForKey(key) + 
						" at tab " + CONNECTION_TYPE_NAME_GSISSH;
				}
			}
			if(aliasCombo.getText().equals("")){
				return "Please set Alias";
			}
		}
		return null;
	}
	
	public void applyConfigurationChanges(Composite parent, TerminalSite site) {
		for(String key : textFields.keySet())
		{
			String value = textFields.get(key).getText();
			site.getConnectionTypeConfigs().put(key, value);
		}
		String value = aliasCombo.getText().equals("") ? "" : aliasCombo.getText();
		site.getConnectionTypeConfigs().put(GSISSH_ALIAS, value);
		
		value = proxyTypeCombo.getText().equals("") ? "" : proxyTypeCombo.getText();
		site.getConnectionTypeConfigs().put(GSISSH_PROXY_TYPE, value);
		
		value = delegTypeCombo.getText().equals("") ? "" : delegTypeCombo.getText();
		site.getConnectionTypeConfigs().put(GSISSH_DELEG_TYPE, value);
		
		if(saveProxy){
			site.getConnectionTypeConfigs().put(GSISSH_SAVED_PROXY, "true");
		}
		else{
			site.getConnectionTypeConfigs().put(GSISSH_SAVED_PROXY, "false");
		}
		
		value = textFields.get(GSISSH_SAVED_PROXY_PATH).getText();
		site.getConnectionTypeConfigs().put(GSISSH_SAVED_PROXY_PATH, value);
		
		textFields.clear();

	}
	
	public void setEnabled(Composite parent, TerminalSite site, boolean enabled)
	{

		
		for(Control c : controls)
		{
			c.setEnabled(enabled);
		}
		textSavedProxy.setEnabled(enabled && saveProxy);
		browseProxyPathButton.setEnabled(enabled && saveProxy);
	}
}
