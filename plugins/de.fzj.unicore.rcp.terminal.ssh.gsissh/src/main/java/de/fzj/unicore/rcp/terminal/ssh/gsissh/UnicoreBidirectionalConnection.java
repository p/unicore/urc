package de.fzj.unicore.rcp.terminal.ssh.gsissh;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.sshtools.common.SshToolsConnectionProfile;
import com.sshtools.j2ssh.SshClient;

import de.fzj.unicore.rcp.terminal.IBidirectionalConnection;

/**
 * Binding streams from GSISSH plugin
 * 
 * @author Andre Giesler
 */
class UnicoreBidirectionalConnection implements IBidirectionalConnection {

	private GSISSHTerminalSession gsisshTermSession;
	private InputStream inputStream;
	private OutputStream outputStream;
	private SshClient ssh;
	private SshToolsConnectionProfile currentConnectionProfile;
	
	protected UnicoreBidirectionalConnection(GSISSHTerminalSession gsisshTermSession, InputStream inputStream,
			OutputStream outputStream, SshClient ssh, SshToolsConnectionProfile currentConnectionProfile) {
		super();
		this.gsisshTermSession = gsisshTermSession;
		this.inputStream = inputStream;
		this.outputStream = outputStream;
		this.ssh = ssh;
		this.currentConnectionProfile = currentConnectionProfile;
	}

	public void close() {
		if (ssh != null) {
	        if (performVerifiedDisconnect(true)) {

	        	try {
					this.inputStream.close();
					this.outputStream.close();	
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        	this.gsisshTermSession.closeConnection(true);	
		    //ssh.disconnect();
		    
	        }
	      }
	}
	
	private boolean performVerifiedDisconnect(boolean force) {
		// Lets examine the profile to see if we need to close the connection
		SshToolsConnectionProfile profile = this.currentConnectionProfile;
		if (profile.disconnectOnSessionClose()) {
			// Yes we should, lets ask the user about any forwarding channels
			if (ssh.getForwardingClient().hasActiveConfigurations()) {
				MessageBox messageBox = new MessageBox(new Shell(), SWT.ICON_QUESTION
						| SWT.YES | SWT.NO);
				messageBox.setMessage("There are currently active forwarding channels!\n\n"
						+
						"The profile is configured to disconnect when the session is closed. Closing\n"
						+ "the connection will terminate these forwarding channels with unexpected results.\n\n"
						+ "Do you want to disconnect now?");
				messageBox.setText("Auto disconnect");
				int response = messageBox.open();
				if (response == SWT.YES)
					return true;
				else
					return false;
			}
		}
		else {
			return true;
		}
		return force;
	}

	public InputStream getInputStream() throws IOException {
		return inputStream;
	}

	public OutputStream getOutputStream() throws IOException {
		return outputStream;
	}

}
