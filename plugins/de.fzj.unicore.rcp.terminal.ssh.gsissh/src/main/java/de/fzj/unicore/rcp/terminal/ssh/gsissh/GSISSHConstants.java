package de.fzj.unicore.rcp.terminal.ssh.gsissh;

import de.fzj.unicore.rcp.terminal.TerminalConstants;

/**
 * Common used constants in GSISSH plugin
 * 
 * @author Andre Giesler
 */
public interface GSISSHConstants extends TerminalConstants {

	public static final String GSISSH_HOST = "gsissh_host", GSISSH_PORT = "gsissh_port", 
	GSISSH_LOGIN = "gsissh_login", GSISSH_PASS = "passphrase", 
	GSISSH_PROXY_LIFETIME = "gsissh_proxy_lifetime", GSISSH_ALIAS="gsissh_alias",
	GSISSH_DELEG_TYPE="gsissh_deleg_type", GSISSH_PROXY_TYPE="gsissh_proxy_type",
	GSISSH_SAVED_PROXY="gsissh_saved_proxy", GSISSH_SAVED_PROXY_PATH="gsissh_saved_proxy_path";

	//Check references when changing order of Strings
	public static final String[] GSISSH_DELEG_TYPES = {"Full", "Limited", "None"};

	//Check references when changing order of Strings
	public static final String[] GSISSH_PROXY_TYPES = {"RFC Impersonation", "Pre-RFC Impersonation", "Legacy"};

	public static final String GSISSH_SUPPORTED = "gsissh_supported";

	public static final String CONNECTION_TYPE_ID_GSISSH = "GSISSH";

	public static final String CONNECTION_TYPE_NAME_GSISSH = "GSISSH";	
	
	public final static String PREF_DELEGATION_TYPE =
		"sshterm.delegationType";
	public final static String PREF_PROXY_TYPE =
		"sshterm.proxyType";
	public final static String PREF_PROXY_LENGTH =
		"sshterm.proxyLength";
		
	public final static String PREF_SAVE_PROXY =
		"sshterm.proxy.save";	

	public final static String PREF_AUTH_ORDER =
		"sshterm.auth.order";
		
	public final static String PREF_JKS_DEFAULT_FILE =
		"sshterm.jks.defaults.file";

	public final static String PREF_JKS_PASS =
		"sshterm.jks.defaults.pass"; 
	
	public final static String PREF_JKS_ALIAS =
			"sshterm.jks.defaults.alias"; 
}
