package de.fzj.unicore.rcp.terminal.ui.tableviewer.providers;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;
import de.fzj.unicore.rcp.terminal.ui.views.TerminalConfigView;

/**
 * 
 * @author Andre Giesler
 * @version $Id$
 * 
 */

public class TerminalSiteLabelProvider extends LabelProvider implements
		ITableLabelProvider {
	
	// We use icons
//	private static final Image CHECKED = AbstractUIPlugin
//			.imageDescriptorFromPlugin("de.fzj.unicore.rcp.terminal",
//					"icons/checked.gif").createImage();
//	private static final Image UNCHECKED = AbstractUIPlugin
//			.imageDescriptorFromPlugin("de.fzj.unicore.rcp.terminal",
//					"icons/unchecked.gif").createImage();
//	private static final Image EDIT = AbstractUIPlugin
//	.imageDescriptorFromPlugin("de.fzj.unicore.rcp.terminal",
//			"icons/edittopic.gif").createImage();
	
	private static final String CHECKED_KEY = "CHECKED";
	  private static final String UNCHECK_KEY = "UNCHECKED";
	  
	  public TerminalSiteLabelProvider(ColumnViewer viewer) {
		    if( JFaceResources.getImageRegistry().getDescriptor(CHECKED_KEY) == null ) {
		      JFaceResources.getImageRegistry().put(UNCHECK_KEY, 
		        makeShot(viewer.getControl(),false));
		      JFaceResources.getImageRegistry().put(CHECKED_KEY,
		        makeShot(viewer.getControl(),true));
		    }
		  }


	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}


	public String getColumnText(Object element, int columnIndex) {
		TerminalSite sshSite = (TerminalSite) element;
		
		if(columnIndex == TerminalConfigView.getColumnFor(TerminalConfigView.NAME)) return sshSite.getName();
		else if(columnIndex == TerminalConfigView.getColumnFor(TerminalConfigView.CONFIG)) return sshSite.getConnectionInfo();
		else if(columnIndex == TerminalConfigView.getColumnFor(TerminalConfigView.CONNECTION_TYPE)) return sshSite.getConnectionType();
		else if(columnIndex == TerminalConfigView.getColumnFor(TerminalConfigView.SITE_TYP)) return sshSite.getSiteType();
		else return "";
		
	}
	
	private Image makeShot(Control control, boolean type) {// Hopefully no platform uses exactly this color because we'll make
		// it transparent in the image.
		Color greenScreen = new Color(control.getDisplay(), 222, 223, 224);

		Shell shell = new Shell(control.getShell(), SWT.NO_TRIM | SWT.NO_BACKGROUND);
		// otherwise we have a default gray color
		shell.setBackground(greenScreen);

		Button button = new Button(shell, SWT.CHECK | SWT.NO_BACKGROUND);
		button.setBackground(greenScreen);
		button.setSelection(type);

		// otherwise an image is located in a corner
		button.setLocation(1, 1);
		Point bsize = button.computeSize(SWT.DEFAULT, SWT.DEFAULT);

		// otherwise an image is stretched by width
		bsize.x = Math.max(bsize.x - 1, bsize.y - 1);
		bsize.y = Math.max(bsize.x - 1, bsize.y - 1);
		button.setSize(bsize);
		shell.setSize(bsize);

		shell.open();
		GC gc = new GC(shell);
		Image image = new Image(control.getDisplay(), bsize.x, bsize.y);
		gc.copyArea(image, 0, 0);
		gc.dispose();
		shell.close();

		ImageData imageData = image.getImageData();
		imageData.transparentPixel = imageData.palette.getPixel(greenScreen
		.getRGB());

		return new Image(control.getDisplay(), imageData);}
	
//	public String getText(Object element) {
//		//System.out.println("gettext");
//		return "auto";
//	}

	


}
