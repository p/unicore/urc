package de.fzj.unicore.rcp.terminal;

public interface TerminalConstants {

	public static final String CONFIG_FILE = "SSHSettings.xml";
	
	public static final String ELEMENT_SSH_SITE_INFO = "SSHSiteInformation";
	public static final String ELEMENT_SSH_SITE = "SSHSite";
	public static final String ELEMENT_CONNECTION_TYPE = "ConnectionType";
	
	public static final String ATTRIBUTE_CONNECTION_TYPE_SUPPORTED = "supported";
	public static final String ATTRIBUTE_CONNECTION_TYPE_ID = "id";
	public static final String ATTRIBUTE_CONNECTION_TYPE_NAME = "name";
	
	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String CONNECTION_TYPE = "connectionType";
	public static final String SITE_TYPE = "siteType";
	public static final String SITE_TYPE_UNICORE ="unicoreTargetSystem";
	public static final String SITE_TYPE_CUSTOM ="CustomTargetSite";
	
	//public static final String[] PLAIN_KEYS = new String[] {"plain_host","plain_port","plain_login"};

	public static final String NOT_SUPPORTED = "Not supported";
	
	public static final String NOTSELECTED = "Not_specified";
	
	public final String NoSSHSupport = "No connection method configured";
	
}
