package de.fzj.unicore.rcp.terminal.ui.tableviewer.providers;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;

/**
 * 
 * @author Andre Giesler
 * @version $Id$
 * 
 */

public class TerminalSiteContentProvider implements IStructuredContentProvider {

	
	public Object[] getElements(Object inputElement) {
		@SuppressWarnings("unchecked")
		List<TerminalSite> sshSites = (List<TerminalSite>) inputElement;
		return sshSites.toArray();
	}

	public void dispose() {
		// TODO Auto-generated method stub
		//System.out.println("input disposed");
	}


	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		//System.out.println("input changed");
	}

}
