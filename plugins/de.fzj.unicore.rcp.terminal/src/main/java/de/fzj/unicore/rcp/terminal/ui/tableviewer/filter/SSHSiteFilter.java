package de.fzj.unicore.rcp.terminal.ui.tableviewer.filter;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;

/**
 * 
 * @author Andre Giesler
 * @version $Id$
 * 
 */

public class SSHSiteFilter extends ViewerFilter {

	private String searchString;

	public void setSearchText(String s) {
		if(s == null) {
			this.searchString = null;
		} else {
			// Search must be a substring of the existing value
			this.searchString = ".*" + s.toLowerCase() + ".*";
		}
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if (searchString == null || searchString.length() == 0) {
			return true;
		}
		TerminalSite p = (TerminalSite) element;
		if (p.getName().toLowerCase().matches(searchString)) {
			return true;
		}
		if (p.getId().toLowerCase().matches(searchString)) {
			return true;
		}

		return false;
	}
}

