package de.fzj.unicore.rcp.terminal.ui.tableviewer.providers;

import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import de.fzj.unicore.rcp.terminal.ui.dialogs.EditSSHSiteDialog;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.ModelProvider;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;


/**
 * 
 * @author Andre Giesler
 * @version $Id$
 * 
 */

class ConDialogCellEditor extends DialogCellEditor {

	private Table table;
	private TableViewer viewer;
	//private String title;

	protected ConDialogCellEditor (Composite parent,String title, ColumnViewer viewer){

		super(parent);
		this.table = (Table)parent;
		this.viewer = (TableViewer)viewer;
		//this.title = title;
	} 

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {

		Object result = editDialog(cellEditorWindow.getShell());
		//System.out.println(result);
		return result; 
	}

	@Override
	protected Button createButton(Composite parent) {
		Button result = new Button(parent, SWT.DOWN);
		result.setText("... Edit"); //$NON-NLS-1$
		return result;
	}


	private  Object editDialog(Shell shell){

		String id ="";
		String name ="";
		String connInfo ="";

		TableItem[] selection = table.getSelection();
		TerminalSite site = (TerminalSite) selection[0].getData();
		connInfo = site.getConnectionInfo();
		id = site.getId();
		name = site.getName();

		EditSSHSiteDialog dialog = new EditSSHSiteDialog(shell, id, name, connInfo, site);

		dialog.open();
		
		if(dialog.getReturnCode()==SWT.OK || dialog.getReturnCode()==0){
			viewer.setInput(ModelProvider.new_Instance().getAllSites());
			viewer.getTable().setSelection(selection[0]);
			viewer.refresh();
		}

		return dialog.getConnInfo();
	} 

}
