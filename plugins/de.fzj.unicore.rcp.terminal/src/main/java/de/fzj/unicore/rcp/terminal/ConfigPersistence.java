package de.fzj.unicore.rcp.terminal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.XMLMemento;

import de.fzj.unicore.rcp.terminal.extensionpoints.IConnectionConfigPersistence;

/**
 * 
 * @author demuth
 */
public abstract class ConfigPersistence implements TerminalConstants, IConnectionConfigPersistence {

	public boolean writeConfig(String id, String name, String connectionType, Map<String,String> config){

		try
		{
			config.put(ID, id);
			config.put(NAME, name);
			config.put(CONNECTION_TYPE, connectionType);

			XMLMemento configMemento = getConfigMemento();
			IMemento site = getSiteMemento(id,configMemento);
			if(site == null) site = configMemento.createChild(ELEMENT_SSH_SITE);

			site.putString(TerminalConstants.ID,id);
			site.putString(TerminalConstants.NAME,name);
			site.putString(TerminalConstants.CONNECTION_TYPE,connectionType);
			site.putString(TerminalConstants.ATTRIBUTE_CONNECTION_TYPE_SUPPORTED, 
					config.get(TerminalConstants.ATTRIBUTE_CONNECTION_TYPE_SUPPORTED));
			return writeConfig(config, site) && saveConfigMemento(configMemento);

		}
		catch ( Exception e ) {
			UnicoreTerminalPlugin.log(Status.ERROR,"Error when serializing SSHData " +
					"properties");
			return false;
		} 

	}

	public abstract boolean writeConfig(Map<String,String> config, IMemento memento);

	/**
	 * Try to read the configuration for the site with the given id and
	 * store it inside the given map. return false iff the read configuration
	 * is invalid
	 * @param config
	 * @param id
	 * @return
	 */
	public Map<String, String> readConfig(String id)
	{
		Map<String, String> config = new HashMap<String, String>();
		
		try {
			IMemento site = getSiteMemento(id);
			if(site != null) 
			{
				readConfig(config, site);
				config.put(ID, id);
				String name = site.getString(NAME);
				if(name == null) name = "";
				config.put(NAME, name);
				config.put(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED, site.getString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED));
			}
		} catch (Exception e) {
			UnicoreTerminalPlugin.log(Status.ERROR,e.getMessage());
		} 
		
		return config;

	}

	/**
	 * Try to read the configuration for the site represented by this memento and
	 * store it inside the given map. return false iff the read configuration
	 * is invalid
	 * @param config
	 * @param memento
	 * @return
	 */
	public abstract boolean readConfig(Map<String, String> config, IMemento memento);


	/**
	 * Creates an XML storage for SSH info, if 
	 * no XML file exists already; return true iff the file exists
	 * afterwards
	 * @throws WorkbenchException 
	 * @throws FileNotFoundException 
	 *
	 */
	static public boolean createXMLMementoFile() { // NO_UCD

		return saveConfigMemento(getConfigMemento());

	}

	public static boolean saveConfigMemento(XMLMemento memento)//, boolean refresh) // NO_UCD
	{
		File savedFile = UnicoreTerminalPlugin.getDefault()
		.getStateLocation().append(CONFIG_FILE).toFile();
		Writer writer;
		try {
			writer = new FileWriter(savedFile);
			memento.save(writer);
//			if(refresh){
//				PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable()
//				{
//					public void run() {
//						try {
//							TerminalConfigView sshConfig = (TerminalConfigView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(TerminalConfigView.ID);
//							if(sshConfig != null) sshConfig.refresh();
//						} catch (Exception e) {
//							UnicoreTerminalPlugin.log(Status.ERROR,"Unable to refresh terminal config view: "+e.getMessage(), e );
//						}
//					}
//				});
//			}

			return true;
		} catch (Exception e1) {
			UnicoreTerminalPlugin.log(Status.ERROR,"Unable to save Terminal Configuration data to XML File");
			
			return false;
		}
	}

	protected static XMLMemento getConfigMemento() {


		File savedFile = UnicoreTerminalPlugin.getDefault()
		.getStateLocation().append(CONFIG_FILE).toFile();

		XMLMemento configMemento = XMLMemento.createWriteRoot(ELEMENT_SSH_SITE_INFO);
		if (savedFile.exists()){
			try {
				Reader reader = new FileReader(savedFile);
				XMLMemento readMemento = XMLMemento.createReadRoot(reader);
				for(IMemento child : readMemento.getChildren(ELEMENT_SSH_SITE))
				{
					configMemento.copyChild(child);
				}
			} catch (Exception e) {
				UnicoreTerminalPlugin.log(Status.ERROR,e.getMessage());
			}

		}

		return configMemento;

	}

	public static XMLMemento getSiteMemento(String id)
	{

		return getSiteMemento(id,getConfigMemento());
	}

	protected static XMLMemento getSiteMemento(String id, XMLMemento configMemento)
	{

		IMemento[] child = configMemento.getChildren(ELEMENT_SSH_SITE);
		for( int i = 0; i < child.length; i++ ) {
			String childId = child[i].getString(TerminalConstants.ID);
			if(id.equals(childId)){
				return (XMLMemento) child[i];
			}                         
		}
		return null;
	}

	private static IMemento[] getAllConnectionTypeMementos(IMemento siteMemento)
	{
		if(siteMemento == null) return new IMemento[0];
		return siteMemento.getChildren(ELEMENT_CONNECTION_TYPE);
	}

	public static IMemento getConnectionTypeMemento(String typeId, IMemento siteMemento)
	{
		if(typeId == null) return null;
		for(IMemento connectionType : siteMemento.getChildren(ELEMENT_CONNECTION_TYPE))
		{
			String id = connectionType.getString(ATTRIBUTE_CONNECTION_TYPE_ID);
			if(typeId.equals(id)) return connectionType; 
		}
		return null;
	}

	public static IMemento createConnectionTypeMemento(String typeId, IMemento siteMemento)
	{
		IMemento result =siteMemento.createChild(ELEMENT_CONNECTION_TYPE);
		result.putString(ATTRIBUTE_CONNECTION_TYPE_ID, typeId);
		return result;
	}

	public static IMemento[] getAllSiteMementos()
	{

		XMLMemento memento = getConfigMemento();
		return memento.getChildren(ELEMENT_SSH_SITE);

	}

	public static void removeSite(String id)
	{
		if(id == null) return;
		XMLMemento old = getConfigMemento();
		XMLMemento newMemento = getConfigMemento();
		for(IMemento child : old.getChildren(ELEMENT_SSH_SITE))
		{
			if(!id.equals(child.getString(ID))) newMemento.copyChild(child);
		}
		saveConfigMemento(newMemento);
	}

	public static void removeSites(Set<String> ids)
	{
		if(ids == null) return;
		XMLMemento old = getConfigMemento();
		XMLMemento newMemento = XMLMemento.createWriteRoot(ELEMENT_SSH_SITE_INFO);
		for(IMemento child : old.getChildren(ELEMENT_SSH_SITE))
		{
			if(!ids.contains(child.getString(ID))) newMemento.copyChild(child);
		}
		saveConfigMemento(newMemento);
	}

	/**
	 * Process the map with site's SSH data, so that it is returned as list
	 * in the need format for the SSH Edit panel
	 *
	 * @param	serverAddress the given NodePath of the site
	 * @return  list of available connection methods
	 */
	public static List<String> getPersistedConnectionTypes(String id){ // NO_UCD

		List<String> ssh_methods = new ArrayList<String>();

		IMemento site = ConfigPersistence.getSiteMemento(id);

		IMemento[] mementos = getAllConnectionTypeMementos(site);
		for (IMemento memento : mementos) {
			if(memento != null){
				ssh_methods.add(memento.getString(ATTRIBUTE_CONNECTION_TYPE_ID));
			}
		}

		if(ssh_methods.isEmpty()){
			ssh_methods.add(TerminalConstants.NOTSELECTED);
		}
		return ssh_methods;
	}
	
	/**
	 * Process the map with site's SSH data, so that it is returned as list
	 * in the need format for the SSH Edit panel
	 *
	 * @param	serverAddress the given NodePath of the site
	 * @return  list of available connection methods
	 */
	public static List<String> getAvailablePersistedConnectionTypes(String id){

		List<String> ssh_methods = new ArrayList<String>();

		IMemento site = ConfigPersistence.getSiteMemento(id);
		
		String availableConnectionTypes = site.getString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED);
		StringTokenizer tokenizer = new StringTokenizer( availableConnectionTypes ); 
		while (tokenizer.hasMoreTokens())ssh_methods.add(tokenizer.nextToken());

		if(ssh_methods.isEmpty()){
			ssh_methods.add(TerminalConstants.NOTSELECTED);
		}
		return ssh_methods;
	}
	

	/**
	 * Process the map with site's SSH data, so that it is returned as list
	 * in the need format for the SSH Edit panel
	 *
	 * @param	serverAddress the given NodePath of the site
	 * @return  list of available connection methods
	 */
	public static List<String> getSupportedConnectionTypes(String id){

		IMemento site = ConfigPersistence.getSiteMemento(id);

		if(site != null && site.getString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED) != null){
			String[] supportedTypes = site.getString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED).split(" ");
			return Arrays.asList(supportedTypes);
		}

		return new ArrayList<String>(0);
	}

	/**
	 * Checks if SSH data of the site is already provided in 
	 * client XML storage  
	 *
	 * @param	serverAddress	The unique NodePath of the site	
	 * @return  true if it exists
	 */
	public static boolean isSiteConfigured(String id){
		return getSiteMemento(id) != null;
	}
	

}
