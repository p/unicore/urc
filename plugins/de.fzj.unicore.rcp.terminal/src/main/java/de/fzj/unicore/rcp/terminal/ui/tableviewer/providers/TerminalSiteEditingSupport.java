package de.fzj.unicore.rcp.terminal.ui.tableviewer.providers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;

import de.fzj.unicore.rcp.terminal.ConnectionType;
import de.fzj.unicore.rcp.terminal.ConnectionTypeRegistry;
import de.fzj.unicore.rcp.terminal.TerminalConstants;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.ModelProvider;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;
import de.fzj.unicore.rcp.terminal.ui.views.TerminalConfigView;

/**
 * 
 * @author Andre Giesler
 * @version $Id$
 * 
 */

public class TerminalSiteEditingSupport extends EditingSupport {
	private CellEditor editor;
	private int column;
	private final String NOTSELECTED = "Not specified";
	
	
	public TerminalSiteEditingSupport(ColumnViewer viewer, int column) {
		super(viewer);
		String[] sshMethod = new String[4];
		sshMethod[0] = NOTSELECTED;
		sshMethod[1] = "PLAIN";
		sshMethod[2] = "SOCKS";
		sshMethod[3] = "GSISSH";
		this.column = column;
		// Create the correct editor based on the column index
		if(column == TerminalConfigView.getColumnFor(TerminalConfigView.NAME)) editor = new TextCellEditor(((TableViewer) viewer).getTable());
		else if(column == TerminalConfigView.getColumnFor(TerminalConfigView.CONFIG)) editor = new ConDialogCellEditor(((TableViewer) viewer).getTable(), "Edit Connection Data", viewer);
		else if(column == TerminalConfigView.getColumnFor(TerminalConfigView.CONNECTION_TYPE)) editor = new ComboBoxCellEditor(((TableViewer) viewer).getTable(),sshMethod);
		else if(column == TerminalConfigView.getColumnFor(TerminalConfigView.SITE_TYP)) editor = new TextCellEditor(((TableViewer) viewer).getTable());
		
	}

	@Override
	protected boolean canEdit(Object element) {
		if(column == TerminalConfigView.getColumnFor(TerminalConfigView.NAME)) return true;
		else if(column == TerminalConfigView.getColumnFor(TerminalConfigView.CONFIG)) return true;
		else if(column == TerminalConfigView.getColumnFor(TerminalConfigView.CONNECTION_TYPE)) return true;
		else if(column == TerminalConfigView.getColumnFor(TerminalConfigView.SITE_TYP)) return false;
		return false;
		
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		TerminalSite sshSite = (TerminalSite) element;
		
		List<String> list = sshSite.getAvailableConnectionTypes();
		String[] elements;
//		if(!list.contains(TerminalConstants.NOTSELECTED)){
//			ArrayList<String> copy = new ArrayList<String>(list);
//			copy.add(0, NOTSELECTED);
//			elements = copy.toArray(new String[copy.size()]);
//		}
//		else{
//			elements = list.toArray(new String[list.size()]);
//		}
		elements = list.toArray(new String[list.size()]);
		
		if(!list.contains(TerminalConstants.NOTSELECTED)){
			ArrayList<String> copy = new ArrayList<String>(list);
			copy.add(0, NOTSELECTED);
			elements = copy.toArray(new String[copy.size()]);
		}
		else{
			elements = list.toArray(new String[list.size()]);
		}
		
		if(editor instanceof ComboBoxCellEditor ){
			((ComboBoxCellEditor)editor).setItems(elements);
		}
		return editor;		
	}
	
	private int getIndexOfSSHMethodsArray(String[] array, String method){
		int i = 0;
		for(i=0;i<array.length;i++){
			if(array[i].equals(method))break;
		}
		return i;
	}

	@Override
	protected Object getValue(Object element) {
		TerminalSite sshSite = (TerminalSite) element;

		List<String> list = sshSite.getAvailableConnectionTypes();
		String[] elements;
//		if(!list.contains(TerminalConstants.NOTSELECTED)){
//			ArrayList<String> copy = new ArrayList<String>(list);
//			copy.add(0, NOTSELECTED);
//			elements = copy.toArray(new String[copy.size()]);
//		}
//		else{
//			elements = list.toArray(new String[list.size()]);
//		}
		elements = list.toArray(new String[list.size()]);

		if(column == TerminalConfigView.getColumnFor(TerminalConfigView.NAME)) return sshSite.getName();
		else if(column == TerminalConfigView.getColumnFor(TerminalConfigView.CONFIG)) return sshSite.getConnectionInfo();
		else if(column == TerminalConfigView.getColumnFor(TerminalConfigView.CONNECTION_TYPE)) 
		{

			if (TerminalConstants.NOTSELECTED.equals(sshSite.getConnectionType())) {
				//return 1;
				return new Integer(getIndexOfSSHMethodsArray(elements,TerminalConstants.NOTSELECTED));
			} 
			else if ("PLAIN".equals(sshSite.getConnectionType())) {
				//return 1;
				return new Integer(getIndexOfSSHMethodsArray(elements,"PLAIN"));
			} 
			else if("SOCKS".equals(sshSite.getConnectionType())) {
				//return 2;
				return new Integer(getIndexOfSSHMethodsArray(elements,"SOCKS"));
			}
			else if("GSISSH".equals(sshSite.getConnectionType())) {
				//return 3;
				return new Integer(getIndexOfSSHMethodsArray(elements,"GSISSH"));
			}
			else{
				return new Integer(0);
			}
		}
		else if(column == TerminalConfigView.getColumnFor(TerminalConfigView.SITE_TYP)) {
			return sshSite.getSiteType();
		}
		return null;
	}

	@Override
	protected void setValue(Object element, Object value) {
		TerminalSite sshSite = (TerminalSite) element;
		
		List<String> list = sshSite.getAvailableConnectionTypes();
		String[] elements;
//		if(!list.contains(TerminalConstants.NOTSELECTED)){
//			ArrayList<String> copy = new ArrayList<String>(list);
//			copy.add(0, NOTSELECTED);
//			elements = copy.toArray(new String[copy.size()]);
//		}
//		else{
//			elements = list.toArray(new String[list.size()]);
//		}
		elements = list.toArray(new String[list.size()]);
		
		boolean dirty = false;
		if(column == TerminalConfigView.getColumnFor(TerminalConfigView.NAME))
		{
			String newValue = String.valueOf(value);
			dirty = !sshSite.getName().equals(newValue);
			sshSite.setName(newValue);
		}
		else if(column == TerminalConfigView.getColumnFor(TerminalConfigView.CONFIG))
		{
			String newValue = String.valueOf(value);
			dirty = !sshSite.getConnectionInfo().equals(newValue);
			sshSite.setConnectionInfo(newValue);
		}
		else if(column == TerminalConfigView.getColumnFor(TerminalConfigView.CONNECTION_TYPE))
		{
			String newValue = elements[(Integer) value];
			dirty = sshSite.getConnectionType() == null ? newValue != null : !sshSite.getConnectionType().equals(newValue);
			sshSite.setConnectionType(newValue);
			String connData = "Connection Type: " + TerminalConstants.NOTSELECTED;
			for(final ConnectionType connectionType : ConnectionTypeRegistry.getInstance().getAllConnectionTypes())
			{
				try {
					if(connectionType.getId().equals(sshSite.getConnectionType())){
						Map<String,String> attributes = connectionType.getUiBuilder().getTooltipInfo(sshSite);
						connData = "Connection Type: " + connectionType.getId() + ", ";
						for(String attr : attributes.keySet()){
							connData += attr + "=" + attributes.get(attr) + ", ";
							
						}
						sshSite.setConnTooltip(connData);
					}
				} catch (Exception e) {
					sshSite.setConnTooltip("");
				}
			}
		}
		else if(column == TerminalConfigView.getColumnFor(TerminalConfigView.SITE_TYP)){
			String newValue = String.valueOf(value);
			dirty = !sshSite.getSiteType().equals(newValue);
			sshSite.setSiteType(newValue);
		}
		
		
		if(dirty) 
		{
			ModelProvider.getInstance().saveSite(sshSite);
			getViewer().update(element, null);
		}
		
	}
	

}
