package de.fzj.unicore.rcp.terminal.ui.tableviewer.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.eclipse.ui.IMemento;

import de.fzj.unicore.rcp.terminal.ConfigPersistence;
import de.fzj.unicore.rcp.terminal.ConnectionType;
import de.fzj.unicore.rcp.terminal.ConnectionTypeRegistry;
import de.fzj.unicore.rcp.terminal.TerminalConstants;

/**
 * 
 * @author Andre Giesler
 * @version $Id$
 * 
 */

public class ModelProvider implements TerminalConstants {

	private static ModelProvider content;

	private List<TerminalSite> allSites;


	private ModelProvider() {
		readAllSites();
	}

	public List<TerminalSite> getAllSites() {
		return allSites;
	}
	
	public HashMap<String, String> getAllSiteIdsNamesMap() {
		HashMap<String, String> idsMap = new HashMap<String, String>();
		ListIterator<TerminalSite> it = allSites.listIterator( allSites.size() );
		while( it.hasPrevious() ){	  
			TerminalSite site = it.previous();
			idsMap.put(site.getId(), site.getName());
		}

		return idsMap;
	}


	
	public static synchronized ModelProvider getInstance() {
		if (content != null) {
			return content;
		} else {
			content = new ModelProvider();
			return content;
		}
		//content = new ModelProvider();
		//return content;
	}

	public static synchronized ModelProvider new_Instance() {		
		content = new ModelProvider();
		return content;
	}



	public List<TerminalSite> readAllSites()
	{

		List<TerminalSite> sshSites = new ArrayList<TerminalSite>();


		for(IMemento site : ConfigPersistence.getAllSiteMementos())
		{
			TerminalSite sshSite = new TerminalSite();
			List<String> list = ConfigPersistence.getSupportedConnectionTypes(site.getString(ID));
			//list.add(0, TerminalConstants.NOTSELECTED);
			ArrayList<String> copy = new ArrayList<String>(list);
			if(!list.contains(TerminalConstants.NOTSELECTED)){				
				copy.add(0, NOTSELECTED);
				sshSite.setAvailableConnectionTypes(copy);
			}
			else{
				sshSite.setAvailableConnectionTypes(ConfigPersistence.getSupportedConnectionTypes(site.getString(ID)));
			}
			//sshSite.setAvailableConnectionTypes(ConfigPersistence.getSupportedConnectionTypes(site.getString(ID)));
			//sshSite.setAvailableConnectionTypes(copy);
			sshSite.setConnectionType(site.getString(CONNECTION_TYPE));
			sshSite.setId(site.getString(ID));
			sshSite.setName(site.getString(NAME));
			sshSite.setSiteType(site.getString(SITE_TYPE));
			sshSite.setConnectionInfo("click to edit");
			// merge all different connection type configs into one big config map
			Map<String,String> config = sshSite.getConnectionTypeConfigs();
			
			for(final ConnectionType connectionType : ConnectionTypeRegistry.getInstance().getAllConnectionTypes())
			{
				Map<String,String> connectionConfig = connectionType.getConfigPersistence().readConfig(sshSite.getId());
				for(String key : connectionConfig.keySet())
				{
					config.put(key, connectionConfig.get(key));
					
					try {
						if(connectionType.getId().equals(sshSite.getConnectionType())){
							Map<String,String> attributes = connectionType.getUiBuilder().getTooltipInfo(sshSite);
							String connData = "Connection Type: " + connectionType.getId() + ", ";
							for(String attr : attributes.keySet()){
								connData += attr + "=" + attributes.get(attr) + ", ";
								
							}
							sshSite.setConnTooltip(connData);
						}
					} catch (Exception e) {
						sshSite.setConnTooltip("");
					}
					
				}
			}
			
			sshSites.add(sshSite);
		}
		allSites = sshSites;
		return sshSites;
	}

// TODO UCdetector: Remove unused code: 
// 	public void removeSite(TerminalSite site){
// 		getInstance().allSites.remove(site);
// 		ConfigPersistence.removeSite(site.getId());
// 		     
// 	}

	public void saveSite(TerminalSite site){
		ConnectionTypeRegistry.writeAllConnectionConfigs(site);
	}


}
