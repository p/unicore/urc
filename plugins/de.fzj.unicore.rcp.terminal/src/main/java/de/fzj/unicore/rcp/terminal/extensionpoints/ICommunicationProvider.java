package de.fzj.unicore.rcp.terminal.extensionpoints;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;

import de.fzj.unicore.rcp.terminal.TerminalConnection;

public interface ICommunicationProvider {

	public TerminalConnection establishConnection(Map<String,String> config, IProgressMonitor progress);
	
}
