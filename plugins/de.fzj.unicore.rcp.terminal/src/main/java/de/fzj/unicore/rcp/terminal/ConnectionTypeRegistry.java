package de.fzj.unicore.rcp.terminal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionDelta;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IRegistryChangeEvent;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.XMLMemento;

import de.fzj.unicore.rcp.terminal.extensionpoints.ICommunicationProvider;
import de.fzj.unicore.rcp.terminal.extensionpoints.IConnectionConfigPersistence;
import de.fzj.unicore.rcp.terminal.extensionpoints.IConnectionConfigUIBuilder;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;

/**
 * Registry of all supported SSH connection types
 * 
 * @author demuth
 * @version $Id$
 * 
 */
public class ConnectionTypeRegistry {

	private static final String ATTRIBUTE_COMMUNICATION_PROVIDER = "communicationProvider";
	private static final String ATTRIBUTE_CONFIG_UI_BUILDER = "configUIBuilder";
	private static final String ATTRIBUTE_CONFIG_PERSISTENCE = "configPersistence";
	private static final String ATTRIBUTE_DESCRIPTION = "description";

	private Map<String,ConnectionType> connectionTypeMap = new HashMap<String, ConnectionType>();

	private List<ConnectionType> connectionTypes = new ArrayList<ConnectionType>();

	private static ConnectionTypeRegistry instance;

	public static synchronized ConnectionTypeRegistry getInstance()
	{
		if(instance == null)
		{
			instance = new ConnectionTypeRegistry();
			instance.updateExtensions();
		}
		return instance;
	}
	
	public ConnectionType getConnectionType(String id)
	{
		return connectionTypeMap.get(id);
	}

	public ICommunicationProvider getCommunicationProvider(String id) // NO_UCD
	{
		return getConnectionType(id).getCommunicationProvider();
	}
	
	public ConnectionType[] getAllConnectionTypes()
	{
		return connectionTypes.toArray(new ConnectionType[connectionTypes.size()]);
	}
	
	public IConnectionConfigUIBuilder getUIBuilder(String id) // NO_UCD
	{
		return getConnectionType(id).getUiBuilder();
	}
	
	public IConnectionConfigPersistence getConfigPersistence(String id) // NO_UCD
	{
		return getConnectionType(id).getConfigPersistence();
	}


	public void updateExtensions() // NO_UCD
	{

		//		manage context menu entries added by extensions
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry.getExtensionPoint(UnicoreTerminalPlugin.EXTENSION_POINT_CONNECTION_TYPES);
		IConfigurationElement[] members = extensionPoint.getConfigurationElements();
		Set<String> obsoleteExtensions = new HashSet<String>(connectionTypeMap.keySet());

		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			addExtension(member);
			String id = getId(member);
			obsoleteExtensions.remove(id);
		}

		for(String id : obsoleteExtensions)
		{
			ConnectionType connType = connectionTypeMap.remove(id);
			connectionTypes.remove(connType);
		}
		

	}

	public void registryChanged(IRegistryChangeEvent event) { // NO_UCD
		IExtensionDelta[] extensionDeltas =
			event.getExtensionDeltas(UnicoreTerminalPlugin.PLUGIN_ID, UnicoreTerminalPlugin.EXTENSION_POINT_CONNECTION_TYPES);

		for(int i = 0; i < extensionDeltas.length; i++) {

			if (extensionDeltas[i].getKind() == IExtensionDelta.ADDED) {

				IExtension extension = extensionDeltas[i].getExtension();
				IConfigurationElement[] members = extension.getConfigurationElements();

				for (int m = 0; m < members.length; m++) {
					IConfigurationElement member = members[m];
					addExtension(member);

				}
			}
			else if(extensionDeltas[i].getKind() == IExtensionDelta.REMOVED) {

				IExtension extension = extensionDeltas[i].getExtension();
				IConfigurationElement[] members = extension.getConfigurationElements();

				for (int m = 0; m < members.length; m++) {
					IConfigurationElement member = members[m];
					String id = getId(member);
					if(!connectionTypeMap.containsKey(id))
					{
						ConnectionType ext = connectionTypeMap.remove(id);
						connectionTypes.remove(ext);
					}
				}

			}
		}
	}
	
	public String getNameForId(String id)
	{
		try{
			return  getConnectionType(id).getName();
		}
		catch(Exception ex){
			return null;
		}
	}
	
	public String getIdForName(String name)
	{
		if(name == null) return null;
		for(ConnectionType t : getAllConnectionTypes())
		{
			if(name.equals(t.getName())) return t.getId();
		}
		return null; // not found
	}
	
	public static void writeAllConnectionConfigs(TerminalSite site)
	{
		ConnectionType[] connectionTypes = getInstance().getAllConnectionTypes();
		for(int i = 0; i < connectionTypes.length; i++)
		{
			ConnectionType connectionType = connectionTypes[i];
			connectionType.getConfigPersistence().writeConfig(site.getId(),site.getName(),site.getConnectionType(),site.getConnectionTypeConfigs());

		}
		XMLMemento configMemento = ConfigPersistence.getConfigMemento();
		IMemento siteMemento = ConfigPersistence.getSiteMemento(site.getId(),configMemento);
		if(siteMemento != null)
		{
			String supported = "";
			if(site.getAvailableConnectionTypes()!=null){
				for(String s : site.getAvailableConnectionTypes())
				{
					if(supported.length() > 0) supported += " ";
					supported += s;
				}
			}			
			if(!supported.contains(TerminalConstants.NOTSELECTED)){
				if(supported.equals(""))supported = TerminalConstants.NOTSELECTED;
				else{
					supported += " " + TerminalConstants.NOTSELECTED;
				}				
			}
			siteMemento.putString(TerminalConstants.ATTRIBUTE_CONNECTION_TYPE_SUPPORTED, supported);
			if(supported.equals(TerminalConstants.NOTSELECTED)){
				siteMemento.putString(TerminalConstants.ATTRIBUTE_CONNECTION_TYPE_SUPPORTED,
						TerminalConstants.NOTSELECTED);
				siteMemento.putString(TerminalConstants.CONNECTION_TYPE,
						TerminalConstants.NOTSELECTED);
			}
			siteMemento.putString(TerminalConstants.SITE_TYPE,
					site.getSiteType());
		}
		ConfigPersistence.saveConfigMemento(configMemento);
	}

	private void addExtension(IConfigurationElement member)
	{
		try {
			String id = getId(member);
			if(id == null) 
			{
				UnicoreTerminalPlugin.log(Status.WARNING,
						"Unable to use connection type extension from plugin "+member.getDeclaringExtension().getContributor().getName()+": "+
						"it does not define a unique id");
			}
			else if(!connectionTypeMap.containsKey(id))
			{
				ICommunicationProvider communicationProvider = (ICommunicationProvider) member.createExecutableExtension(ATTRIBUTE_COMMUNICATION_PROVIDER);
				IConnectionConfigUIBuilder uiBuilder = (IConnectionConfigUIBuilder) member.createExecutableExtension(ATTRIBUTE_CONFIG_UI_BUILDER);
				IConnectionConfigPersistence persistence = (IConnectionConfigPersistence) member.createExecutableExtension(ATTRIBUTE_CONFIG_PERSISTENCE);
				
				String name = member.getDeclaringExtension().getLabel();
				String descr = member.getAttribute(ATTRIBUTE_DESCRIPTION);
				
				ConnectionType connType = new ConnectionType(id, name, descr, communicationProvider, persistence, uiBuilder);
				connectionTypeMap.put(id, connType);
				connectionTypes.add(connType);
			}
		}
		catch (CoreException ex) {

		}
	}
	
	private String getId(IConfigurationElement member)
	{
		return member.getDeclaringExtension().getSimpleIdentifier();
	}
}
