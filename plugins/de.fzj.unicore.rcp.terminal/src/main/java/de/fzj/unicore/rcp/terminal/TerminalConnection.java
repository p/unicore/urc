package de.fzj.unicore.rcp.terminal;

public class TerminalConnection {
	private IBidirectionalConnection connection;
	private ITerminalListener listener;
	private String name = "";
	private String description = "";
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public IBidirectionalConnection getConnection() {
		return connection;
	}
	public void setConnection(IBidirectionalConnection connection) {
		this.connection = connection;
	}
	public ITerminalListener getListener() {
		return listener;
	}
	public void setListener(ITerminalListener listener) {
		this.listener = listener;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
