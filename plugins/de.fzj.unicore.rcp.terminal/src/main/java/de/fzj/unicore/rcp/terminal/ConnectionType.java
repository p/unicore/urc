package de.fzj.unicore.rcp.terminal;

import de.fzj.unicore.rcp.terminal.extensionpoints.ICommunicationProvider;
import de.fzj.unicore.rcp.terminal.extensionpoints.IConnectionConfigPersistence;
import de.fzj.unicore.rcp.terminal.extensionpoints.IConnectionConfigUIBuilder;

/**
 * Managing information of an connection type
 * 
 * @author demuth
 * @version $Id$
 * 
 */
public class ConnectionType {

	private ICommunicationProvider communicationProvider;
	private IConnectionConfigPersistence configPersistence;
	private String description;
	private String id;
	private String name;
	private IConnectionConfigUIBuilder uiBuilder;
	
	
	protected ConnectionType(String id, String name, String description,
			ICommunicationProvider communicationProvider,
			IConnectionConfigPersistence configPersistence,
			IConnectionConfigUIBuilder uiBuilder) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.communicationProvider = communicationProvider;
		this.configPersistence = configPersistence;
		this.uiBuilder = uiBuilder;
	}
	public ICommunicationProvider getCommunicationProvider() {
		return communicationProvider;
	}
	
	
	
	public IConnectionConfigPersistence getConfigPersistence() {
		return configPersistence;
	}
	public String getDescription() {
		return description;
	}
	public String getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public IConnectionConfigUIBuilder getUiBuilder() {
		return uiBuilder;
	}
	public void setCommunicationProvider(
			ICommunicationProvider communicationProvider) {
		this.communicationProvider = communicationProvider;
	}
	public void setConfigPersistence(IConnectionConfigPersistence configPersistence) {
		this.configPersistence = configPersistence;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setUiBuilder(IConnectionConfigUIBuilder uiBuilder) {
		this.uiBuilder = uiBuilder;
	}
}
