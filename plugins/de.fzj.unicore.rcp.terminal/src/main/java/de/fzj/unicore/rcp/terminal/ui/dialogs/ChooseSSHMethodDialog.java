package de.fzj.unicore.rcp.terminal.ui.dialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IMemento;

import de.fzj.unicore.rcp.terminal.ConfigPersistence;
import de.fzj.unicore.rcp.terminal.ConnectionType;
import de.fzj.unicore.rcp.terminal.ConnectionTypeRegistry;
import de.fzj.unicore.rcp.terminal.TerminalConstants;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;

/**
 * Dialog to let user select a default SSH method and configure the connection information
 * 
 * @author Andre Giesler
 */
public class ChooseSSHMethodDialog extends TitleAreaDialog{ 

	private String[] methods;
	private String[] model_items;
	private String nodename;
	private String currentSSHMethod;
	private String siteID;

	private HashMap<String,String> connectionType_Names;

	private Set<String> supportedConnectionTypes = new HashSet<String>();

	private ConnectionType currentConnType = null;
	private TerminalSite currentSite = null;

	final private static String GIVEN = "given";
	final private static String NOT_GIVEN = "not_given";

	private Composite connectionProviderComp;
	private StackLayout stlayout;
	private Button checksupport;

	// Minimum dialog width (in dialog units)
	private static final int MIN_DIALOG_WIDTH = 350;

	// Minimum dialog height (in dialog units)
	private static final int MIN_DIALOG_HEIGHT = 300;


	public String getCurrentSSHMethod() {
		return currentSSHMethod;
	}
	
	public String getCurrentSSHMethodID() {
		return ConnectionTypeRegistry.getInstance().getIdForName(currentSSHMethod);
	}

	//to override default initial size from TitleAreaDialog
	protected Point getInitialSize() {
		Point shellSize = super.getInitialSize();
		return new Point(Math.max(
				convertHorizontalDLUsToPixels(MIN_DIALOG_WIDTH), shellSize.x),
				Math.max(convertVerticalDLUsToPixels(MIN_DIALOG_HEIGHT),
						shellSize.y));
	}

	public ChooseSSHMethodDialog(String siteID, Shell parentShell, List<String> model,
			String node) {
		super(parentShell);

		setShellStyle(SWT.TITLE | SWT.MODELESS | SWT.RESIZE);

		this.siteID = siteID;

		this.nodename = node;

		this.methods = model.toArray(new String[0]);

		//organize SSH methods array
		Arrays.sort(this.methods);
		model_items = new String[methods.length];
		for (int i = 0; i < methods.length; i++) {			
			try {
				model_items[i] = ConnectionTypeRegistry.getInstance().getNameForId(methods[i]);
				if(model_items[i]==null && methods.length==1)model_items[i]=TerminalConstants.NoSSHSupport;
				else if(model_items[i]==null && methods.length>1){
					model_items[i]=TerminalConstants.NOTSELECTED;					
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		//detect given SSH methods
		connectionType_Names = new HashMap<String,String> ();		
		for(final ConnectionType connectionType : ConnectionTypeRegistry.getInstance().getAllConnectionTypes()){
			String connTypeName = connectionType.getName();	
			boolean isgiven = false;
			for (int i = 0; i < model_items.length; i++) {
				if (model_items[i].equals(connTypeName)){
					isgiven = true;				
					break;
				}
			}
			if(isgiven){
				connectionType_Names.put(connectionType.getName(),GIVEN);
			}
			else{
				connectionType_Names.put(connectionType.getName(),NOT_GIVEN);
			}		
		}
	}

	protected Control createContents(Composite parent ) {
		Control contents = super.createContents(parent);
		// Set the title
		setTitle("Terminal connection type");
		// Set the message
//		setMessage("Please, select one of the available Terminal connection methods at Site '"
//				+ this.nodename + "'", IMessageProvider.INFORMATION);

		
		
		return contents;
	}

	@Override
	protected Control createDialogArea(final Composite parent) {		
		this.setHelpAvailable(false);
		ChooseSSHMethodDialog.setDialogHelpAvailable(false);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		layout.makeColumnsEqualWidth = true;
		parent.setLayout(layout);
		Label label1 = new Label(parent, SWT.NONE);
		label1.setText("Select the preferred connection method");

		final Combo combo = new Combo(parent, SWT.READ_ONLY | SWT.DROP_DOWN);

		for ( String elem : this.connectionType_Names.keySet() ){
			//String name = ConnectionTypeRegistry.getInstance().getNameForId(elem);
			combo.add(elem);
		}

		combo.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				if(combo.getSelectionIndex() >= 0)
				{
					currentSSHMethod = combo.getText();
					
					if(connectionType_Names.get(currentSSHMethod).equals(GIVEN)){
						setMessage("Please, select one of the available terminal connection methods at site '" + 
								nodename + "'.",IMessageProvider.INFORMATION);
					}
					else{
						setMessage("Information for connection method '" + currentSSHMethod + "' couldn't be detected at site '" + 
								nodename + "'. Please, edit the configuration manually.", IMessageProvider.INFORMATION);
					}
					String currentSSHMethodID = ConnectionTypeRegistry.getInstance().getIdForName(currentSSHMethod);
					currentConnType = ConnectionTypeRegistry.getInstance().getConnectionType(currentSSHMethodID);
					stlayout.topControl = createConnTypeComposite(connectionProviderComp, currentConnType);
					connectionProviderComp.layout();
					parent.layout();
				}
				else currentSSHMethod = TerminalConstants.NoSSHSupport;
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				if(combo.getSelectionIndex() >= 0 && combo.getSelectionIndex() < methods.length)
				{
					currentSSHMethod = methods[combo.getSelectionIndex()];
					connectionProviderComp = createConnTypeComposite(parent, ConnectionTypeRegistry.getInstance().getConnectionType(currentSSHMethod));
				}
				else currentSSHMethod = TerminalConstants.NoSSHSupport;
			}
		});
		combo.select(0);

		//Setting the correct combo index (a given SSH method if available) 
		for ( String key : this.connectionType_Names.keySet() ){
			if(connectionType_Names.get(key).equals(GIVEN)){
				String name = key;//ConnectionTypeRegistry.getInstance().getNameForId(key);
				for (int i = 0; i<combo.getItems().length; i++){
					if(name.equals(combo.getItem(i))){
						combo.select(i);
						currentSSHMethod = name;
					}
				}
			}
		}
		currentSSHMethod = combo.getText();
		if(connectionType_Names.get(currentSSHMethod).equals(GIVEN)){
			setMessage("Please, select one of the available terminal connection methods at site '" + 
					nodename + "'.",IMessageProvider.INFORMATION);
		}
		else{
			setMessage("Information for connection method '" + currentSSHMethod + "' couldn't be detected at site '" + 
					nodename + "'. Please, edit the configuration manually.",IMessageProvider.INFORMATION);
		}

		if(currentSSHMethod.equals(TerminalConstants.NOTSELECTED)) {
			this.setErrorMessage("There is no connection method provided by Site '" + 
					this.nodename + "'. You have to configure \n a method in the " +
			"'Connection Configuration' view manually.");			
		}

		this.currentConnType = ConnectionTypeRegistry.getInstance().
			getConnectionType(ConnectionTypeRegistry.getInstance().
					getIdForName(currentSSHMethod));

		connectionProviderComp = new Composite(parent, SWT.BORDER);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalSpan = 2;
		connectionProviderComp.setLayoutData(gridData);

		this.stlayout = new StackLayout();
		connectionProviderComp.setLayout(stlayout);				
		stlayout.topControl = createConnTypeComposite(connectionProviderComp, currentConnType);
		connectionProviderComp.layout();
		parent.layout();

		return parent;
	}

	private Composite createConnTypeComposite(Composite parent, final ConnectionType connectionType){

		final Composite composite1 = new Composite(parent, SWT.NONE);
		composite1.setLayout(new GridLayout(1, false));

		GridData gd = new GridData(SWT.FILL,SWT.CENTER,false,false);

		final Label description = new Label(composite1, SWT.NONE);
		description.setLayoutData(gd);
		description.setText(connectionType.getDescription());

		checksupport = new Button(composite1, SWT.CHECK );
		checksupport.setLayoutData(gd);
		checksupport.setText("Supported");
		supportedConnectionTypes.addAll(ConfigPersistence.getSupportedConnectionTypes(siteID));
		boolean supported = supportedConnectionTypes.contains(connectionType.getId());
		checksupport.setSelection(supported);
		final Composite inner = new Composite(composite1, SWT.NONE);
		inner.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));

		final TerminalSite sshSite = new TerminalSite();
		IMemento site = ConfigPersistence.getSiteMemento(siteID);
		List<String> list = ConfigPersistence.getSupportedConnectionTypes(site.getString(TerminalConstants.ID));
		//List<String> list = ConfigPersistence.getAvailablePersistedConnectionTypes(siteID);
		ArrayList<String> copy = new ArrayList<String>(list);
		if(!list.contains(TerminalConstants.NOTSELECTED)){				
			copy.add(0, TerminalConstants.NOTSELECTED);
			sshSite.setAvailableConnectionTypes(copy);
		}
		else{
			sshSite.setAvailableConnectionTypes(ConfigPersistence.getSupportedConnectionTypes(siteID));
			//sshSite.setAvailableConnectionTypes(ConfigPersistence.getAvailablePersistedConnectionTypes(siteID));
		}
		sshSite.setConnectionType(connectionType.getId());//(site.getString(TerminalConstants.CONNECTION_TYPE));
		sshSite.setId(site.getString(TerminalConstants.ID));
		sshSite.setName(site.getString(TerminalConstants.NAME));
		sshSite.setSiteType(site.getString(TerminalConstants.SITE_TYPE));
		sshSite.setConnectionInfo("click to edit");

		// merge all different connection type configs into one big config map
		Map<String,String> config = sshSite.getConnectionTypeConfigs();		
		for(final ConnectionType _connectionType : ConnectionTypeRegistry.getInstance().getAllConnectionTypes())
		{
			Map<String,String> connectionConfig = _connectionType.getConfigPersistence().readConfig(sshSite.getId());
			for(String key : connectionConfig.keySet())
			{
				config.put(key, connectionConfig.get(key));

				try {
					if(_connectionType.getId().equals(sshSite.getConnectionType())){
						Map<String,String> attributes = _connectionType.getUiBuilder().getTooltipInfo(sshSite);
						String connData = "Connection Type: " + _connectionType.getId() + ", ";
						for(String attr : attributes.keySet()){
							connData += attr + "=" + attributes.get(attr) + ", ";

						}
						sshSite.setConnTooltip(connData);
					}
				} catch (Exception e) {
					sshSite.setConnTooltip("");
				}

			}
		}

		this.currentSite = sshSite;

		connectionType.getUiBuilder().buildUI(inner, sshSite);

		checksupport.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent event) {
				boolean enabled = checksupport.getSelection();
				connectionType.getUiBuilder().setEnabled(inner, sshSite, enabled);
				if(enabled)
				{
					supportedConnectionTypes.add(connectionType.getId());
				}
				else 
				{
					supportedConnectionTypes.remove(connectionType.getId());
					if(supportedConnectionTypes.size()==1 && 
							supportedConnectionTypes.contains(TerminalConstants.NOTSELECTED))sshSite.setConnectionType(TerminalConstants.NOTSELECTED);
				}
			}
		});
		connectionType.getUiBuilder().setEnabled(inner, sshSite, checksupport.getSelection());
		return composite1;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {

		createOkButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	private Button createOkButton(Composite parent, int id, String label,
			boolean defaultButton) {
		// increment the number of columns in the button bar
		((GridLayout) parent.getLayout()).numColumns++;
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		button.setData(new Integer(id));
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				buttonPressed(((Integer) event.widget.getData()).intValue());			
			}
		});
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		setButtonLayoutData(button);
		return button;
	}

	private boolean checkInput(ConnectionType connType, TerminalSite site){
		if(this.checksupport.getSelection()){
			//setErrorMessage(null);		
			ConnectionType connectionType = connType;
			//if(!supportedConnectionTypes.contains(connectionType.getId())) return false;
			Composite parent = (Composite) connectionProviderComp;
			Composite inner = (Composite) parent.getChildren()[0];
			String valid = connectionType.getUiBuilder().validateInput(inner, site);
			if(valid != null) 
			{
				//setErrorMessage(valid);
				setMessage(valid, IMessageProvider.ERROR);
				return false;
			}

			return true;
		}
		else{
			//setErrorMessage("Check support first");
			setMessage("Check support first", IMessageProvider.ERROR);
			return false;
		}

	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			if(currentSSHMethod.equals(TerminalConstants.NOTSELECTED)){
				currentSSHMethod = "";
				super.cancelPressed();
			}
			else{
				if(this.currentConnType==null || this.currentSite==null || !checkInput(this.currentConnType, this.currentSite) ) return;
				currentSite.setAvailableConnectionTypes(new ArrayList<String>(supportedConnectionTypes));
				ConnectionType[] connectionTypes = ConnectionTypeRegistry.getInstance().getAllConnectionTypes();

				Composite parent = (Composite) connectionProviderComp;
				//				for(int i =0; i < parent.getChildren().length;i++){
				//					Control control = (Control)parent.getChildren()[i];
				//					System.out.println(control.getData());
				//				}
				Composite inner = (Composite) parent.getChildren()[0];
				currentConnType.getUiBuilder().applyConfigurationChanges(inner, currentSite);

				if(connectionTypes.length<1)currentSite.setConnectionType(TerminalConstants.NOTSELECTED);
				ConnectionTypeRegistry.writeAllConnectionConfigs(currentSite);
				super.okPressed();
			}		
			return;
		}
		else if (buttonId == IDialogConstants.CANCEL_ID) {	
			currentSSHMethod = TerminalConstants.NOTSELECTED;
			super.cancelPressed();		
		}
		else {
			currentSSHMethod = TerminalConstants.NOTSELECTED;
			return;
		}

	}

}
