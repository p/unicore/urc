package de.fzj.unicore.rcp.terminal.extensionpoints;

import java.util.Map;

/**
 * Implementations of this class can be used to persist the configuration for a
 * given connection type. The configuration is passed as a Map<String,String>.
 * When implementing this interface, choose UNIQUE key names in order to avoid 
 * clashes with other configuration maps which are merged with the map returned here.
 * @author bdemuth
 *
 */
public interface IConnectionConfigPersistence {
	
	public Map<String, String> readConfig(String id);
	
	public boolean writeConfig(String id, String name, String connectionType, Map<String,String> config);

}
