package de.fzj.unicore.rcp.terminal.ui.dialogs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import de.fzj.unicore.rcp.terminal.ConfigPersistence;
import de.fzj.unicore.rcp.terminal.ConnectionType;
import de.fzj.unicore.rcp.terminal.ConnectionTypeRegistry;
import de.fzj.unicore.rcp.terminal.TerminalConstants;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;

/**
 * 
 * @author Andre Giesler
 * @version $Id$
 * 
 */

public class EditSSHSiteDialog extends TitleAreaDialog implements TerminalConstants {

	final private String name;	
	private TerminalSite site;	
	private TabFolder tabFolder;    
	//private XMLMemento xmlmemento;
	private String siteId;
	private String connInfo;
	private Set<String> supportedConnectionTypes = new HashSet<String>();

	public String getConnInfo() {
		return connInfo;
	}


	public TerminalSite getSite() {
		return site;
	}
	
//	@Override
//	protected Point getInitialSize() {
//		return new Point(600, 600);
//	}


	public EditSSHSiteDialog(Shell parentShell, String id, String name, String connInfo, TerminalSite site) {
		super(parentShell);

		this.name = name;
		this.siteId = id;
		//this.xmlmemento = ConfigPersistence.getSiteMemento(id);
		this.connInfo = connInfo;
		this.site = site;
		setShellStyle(getShellStyle() | SWT.RESIZE);
		//tabFolder = new TabFolder(parentShell, SWT.NONE);

	}

	@Override
	protected Control createContents(Composite parent) {

		this.setHelpAvailable(false);
		EditSSHSiteDialog.setDialogHelpAvailable(false);

		Control contents = super.createContents(parent);
		setTitle("Edit Connection Information");
		setMessage("Please, edit the connection configuration of selected Site '" + this.name + "'",
				IMessageProvider.INFORMATION);
		return contents;
	}

	@Override
	protected Control createDialogArea(Composite parent) {		
		GridLayout parentlayout = new GridLayout();
		parentlayout.numColumns = 1;
		
		GridData gridData = new GridData();

		parent.setLayout(parentlayout);

		tabFolder = new TabFolder(parent, SWT.FILL);

		gridData = new GridData();

		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		tabFolder.setLayoutData(gridData);

		// Create each tab and set its text, tool tip text,
		// and control
		for(final ConnectionType connectionType : ConnectionTypeRegistry.getInstance().getAllConnectionTypes())
		{
			TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
			tabItem.setText(connectionType.getName());
			if(connectionType.getDescription() != null) tabItem.setToolTipText(connectionType.getDescription());			
			final Composite composite1 = new Composite(tabFolder, SWT.NONE);
			composite1.setLayout(new GridLayout(1, false));
			tabItem.setControl(composite1);
			GridData gd = new GridData(SWT.FILL,SWT.CENTER,false,false);
			final Label description = new Label(composite1, SWT.NONE);
			description.setLayoutData(gd);
			description.setText(connectionType.getDescription());
			
			final Button checksupport = new Button(composite1, SWT.CHECK );
			checksupport.setLayoutData(gd);
			checksupport.setText("Supported by Target Site");
			checksupport.setToolTipText("Enable the connection type manually");
			supportedConnectionTypes.addAll(ConfigPersistence.getSupportedConnectionTypes(siteId));
			boolean supported = supportedConnectionTypes.contains(connectionType.getId());
			
			checksupport.setSelection(supported);
			final Composite inner = new Composite(composite1, SWT.NONE);
			inner.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
			connectionType.getUiBuilder().buildUI(inner, site);
			checksupport.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(org.eclipse.swt.events.SelectionEvent event) {
					boolean enabled = checksupport.getSelection();
					connectionType.getUiBuilder().setEnabled(inner, site, enabled);
					if(enabled)
					{
						supportedConnectionTypes.add(connectionType.getId());
					}
					else 
					{
						supportedConnectionTypes.remove(connectionType.getId());
						if(supportedConnectionTypes.size()==1 && 
								supportedConnectionTypes.contains(NOTSELECTED))site.setConnectionType(NOTSELECTED);
					}

				}
			});
			connectionType.getUiBuilder().setEnabled(inner, site, checksupport.getSelection());
			if(site.getConnectionType().equals(connectionType.getId())){
				tabFolder.setSelection(tabItem);
			}
		}		
		return parent;
	}




	private boolean checkInput(){
		setErrorMessage(null);
		ConnectionType[] connectionTypes = ConnectionTypeRegistry.getInstance().getAllConnectionTypes();
		for(int i = 0; i < connectionTypes.length; i++)
		{
			ConnectionType connectionType = connectionTypes[i];
			if(!supportedConnectionTypes.contains(connectionType.getId())) continue;
			Composite parent = (Composite) tabFolder.getItem(i).getControl();
			Composite inner = (Composite) parent.getChildren()[2];
			String valid = connectionType.getUiBuilder().validateInput(inner, site);
			if(valid != null) 
			{
				setErrorMessage(valid);
				tabFolder.setSelection(tabFolder.getItem(i));
				return false;
			}
		}
		return true;
	}
	
	protected void okPressed(){
		if(!checkInput()) return;
		site.setAvailableConnectionTypes(new ArrayList<String>(supportedConnectionTypes));
		ConnectionType[] connectionTypes = ConnectionTypeRegistry.getInstance().getAllConnectionTypes();
		for(int i = 0; i < connectionTypes.length; i++)
		{
			ConnectionType connectionType = connectionTypes[i];
			Composite parent = (Composite) tabFolder.getItem(i).getControl();
			Composite inner = (Composite) parent.getChildren()[2];
			connectionType.getUiBuilder().applyConfigurationChanges(inner, site);

		}
		if(connectionTypes.length<1)site.setConnectionType(TerminalConstants.NOTSELECTED);
		ConnectionTypeRegistry.writeAllConnectionConfigs(site);
		super.okPressed();

	}
	
	protected void cancelPressed(){
		ConnectionType[] connectionTypes = ConnectionTypeRegistry.getInstance().getAllConnectionTypes();
		for(int i = 0; i < connectionTypes.length; i++)
		{
			ConnectionType connectionType = connectionTypes[i];
			Composite parent = (Composite) tabFolder.getItem(i).getControl();
			Composite inner = (Composite) parent.getChildren()[2];
			connectionType.getUiBuilder().discardConfigurationChanges(inner, site);
		}
		super.cancelPressed();

	}


}

