/*****************************************************************************
 * Copyright (c) 2006, 2007 g-Eclipse Consortium 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for the
 * g-Eclipse project founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributors:
 *    Andre Giesler, JKU - initial API and implementation
 *****************************************************************************/

package de.fzj.unicore.rcp.terminal;

import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.fzj.unicore.rcp.logmonitor.LogActivator;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.terminal.extensionpoints.ICommunicationProvider;
import de.fzj.unicore.rcp.terminal.ui.views.TerminalView;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author demuth, Andre Giesler
 */
public class UnicoreTerminalPlugin extends AbstractUIPlugin {

	/** The plug-in ID */
	public static final String PLUGIN_ID = "de.fzj.unicore.rcp.terminal"; //$NON-NLS-1$ // NO_UCD
	public final static String ICONS_PATH = "$nl$/icons/";//$NON-NLS-1$ // NO_UCD

	public static final String EXTENSION_POINT_CONNECTION_TYPES = PLUGIN_ID+".connectionTypes"; // NO_UCD

	// The shared instance
	private static UnicoreTerminalPlugin plugin;

	


	/**
	 * The constructor
	 */
	public UnicoreTerminalPlugin() {
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start( final BundleContext context ) throws Exception {
		super.start(context);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop( final BundleContext context ) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static UnicoreTerminalPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor( final String path ) {
		return imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH+path);
	}

	public void showTerminalView(final String siteName,
			final List<String> connectionTypeIds,
			final Map<String,String> config)throws PartInitException
	{

		Job j = new BackgroundJob("opening terminal connection")
		{
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					final TerminalConnection conn = createTerminalConnection(siteName, connectionTypeIds, config, monitor);
					if(conn == null || conn.getConnection() == null) return Status.CANCEL_STATUS;
					PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
						public void run() {
							try {
								ITerminalView result = (ITerminalView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(
								TerminalView.ID);						
								result.addTerminal(conn);
							} catch (Exception e) {
								log(Status.ERROR,"Unable to open terminal view",e);
								e.printStackTrace();
							}
						}
					});
				} 
				catch(Exception e)
				{
					log(Status.ERROR,"Unable to create terminal connection",e);
					e.printStackTrace();
				}
				return Status.OK_STATUS;
			}
		};
		j.schedule();


	}

	private TerminalConnection createTerminalConnection(String siteName, List<String> communicationProviderIds, 
			Map<String,String> config, IProgressMonitor progress)throws PartInitException
	{
		int index = 0;
		for(int i=0;i<communicationProviderIds.size();i++){
			if(config.get(TerminalConstants.CONNECTION_TYPE).equals(communicationProviderIds.get(i))){
				index = i;
			}
		}
		ICommunicationProvider provider = ConnectionTypeRegistry.getInstance().getCommunicationProvider(communicationProviderIds.get(index));
		if(provider == null) return null;
		TerminalConnection result = provider.establishConnection(config,progress);
		if(result != null) result.setName(siteName);
		return result;

	}


	/** 
	 * Log a message via Eclipse logging
	 * Defaults to INFO Level 
	 * @param msg the message to log
	 */
	public static void log(String msg) { // NO_UCD
		log(msg, null);
	}
	/** 
	 * Log a message via Eclipse logging
	 * Defaults to INFO Level 
	 * @param msg the message to log
	 * @param e stacktrace to append
	 */
	public static void log(String msg, Exception e) { // NO_UCD
		log(Status.INFO, msg, e, false);
	}
	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, false);
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param e stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, false);
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param forceAlarmUser forces the logging plugin to open a popup; usually, this should NOT be set to true!
	 */
	public static void log(int status, String msg, boolean forceAlarmUser) { // NO_UCD
		log(status, msg, null, forceAlarmUser);
	}
	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param e stacktrace to append
	 * @param forceAlarmUser forces the logging plugin to open a popup; usually, this should NOT be set to true!
	 */
	public static void log(int status, String msg, Exception e, boolean forceAlarmUser) { // NO_UCD
		IStatus s = new Status(status, PLUGIN_ID, Status.OK, msg, e);
		LogActivator.log(plugin, s,forceAlarmUser);
	}
	
	
}
