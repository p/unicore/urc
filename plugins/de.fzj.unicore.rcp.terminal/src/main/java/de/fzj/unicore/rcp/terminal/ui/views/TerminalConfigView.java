package de.fzj.unicore.rcp.terminal.ui.views;

/**
 * Terminal Config View
 * 
 * @author Andre Giesler
 * @version $Id$
 * 
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import de.fzj.unicore.rcp.terminal.ConfigPersistence;
import de.fzj.unicore.rcp.terminal.TerminalConstants;
import de.fzj.unicore.rcp.terminal.UnicoreTerminalPlugin;
import de.fzj.unicore.rcp.terminal.ui.dialogs.CreateTargetSiteDialog;
import de.fzj.unicore.rcp.terminal.ui.dialogs.EditSSHSiteDialog;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.filter.SSHSiteFilter;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.ModelProvider;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.providers.TerminalSiteContentProvider;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.providers.TerminalSiteEditingSupport;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.providers.TerminalSiteLabelProvider;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.sorter.TableSorter;

public class TerminalConfigView extends ViewPart {
	
	public static final String ID = "de.fzj.unicore.rcp.terminal.ui.views.TerminalConfigView";

	public static final String NAME = "Target Site", CONFIG = "Config", CONNECTION_TYPE = "Connection Type", SITE_TYP = "Site Type";
	private static final String[] COLUMNS = {NAME, CONFIG, CONNECTION_TYPE, SITE_TYP};

	private TableViewer viewer;

	private TableSorter tableSorter;

	private SSHSiteFilter filter;
	
	private Point tableCursor;

	public Point getTableCursor() {
		return tableCursor;
	}

	public void setTableCursor(Point tableCursor) {
		this.tableCursor = tableCursor;
	}

	private IPartListener2 partListener2 = new IPartListener2() {
		public void partActivated(IWorkbenchPartReference ref) {
			if (ref.getPart(true) instanceof IEditorPart)
				editorActivated(getViewSite().getPage().getActiveEditor());
		}

		public void partBroughtToTop(IWorkbenchPartReference ref) {
			if (ref.getPart(true) == TerminalConfigView.this){
				editorActivated(getViewSite().getPage().getActiveEditor());
				viewer.setInput(ModelProvider.new_Instance().readAllSites());
				viewer.refresh();
			}
		}

		public void partClosed(IWorkbenchPartReference ref) {
		}

		public void partDeactivated(IWorkbenchPartReference ref) {
		}

		public void partHidden(IWorkbenchPartReference ref) {}

		public void partInputChanged(IWorkbenchPartReference ref) {}

		public void partOpened(IWorkbenchPartReference ref) {
			if (ref.getPart(true) == TerminalConfigView.this) {
				editorActivated(getViewSite().getPage().getActiveEditor());
			}
		}

		public void partVisible(IWorkbenchPartReference ref) {
			if (ref.getPart(true) == TerminalConfigView.this) editorActivated(getViewSite().getPage().getActiveEditor());
		}
	};

	public TerminalConfigView() {
	}

	//This will create the columns for the table
	private void createColumns(final TableViewer viewer) {
		final Table table = viewer.getTable();
		String[] titles = COLUMNS;//, "Update" };
		int[] bounds = { 130, 100, 90, 120};

		for (int i = 0; i < titles.length; i++) {
			final int index = i;
			final TableViewerColumn viewerColumn = new TableViewerColumn(
					viewer, SWT.NONE);
			final TableColumn column = viewerColumn.getColumn();
			column.setText(titles[i]);
			column.setWidth(bounds[i]);
			column.setResizable(true);
			column.setMoveable(true);

			// Disable native tooltip
			viewer.getTable().setToolTipText("");

			// Implement a "fake" tooltip
			final Listener labelListener = new Listener() {
				public void handleEvent(Event event) {
					Label label = (Label) event.widget;
					Shell shell = label.getShell();
					switch (event.type) {
					case SWT.MouseDown:
						Event e = new Event();
						e.item = (TableItem) label.getData("_TABLEITEM");
						// Assuming table is single select, set the selection as if
						// the mouse down event went through to the table
						viewer.getTable().setSelection(new TableItem[] { (TableItem) e.item });
						viewer.getTable().notifyListeners(SWT.Selection, e);
						// fall through
					case SWT.MouseExit:
						shell.dispose();
						break;
					}
				}
			};

			Listener tableListener = new Listener() {
				Shell tip = null;

				Label label = null;

				public void handleEvent(Event event) {
					switch (event.type) {
					case SWT.Dispose:
					case SWT.KeyDown:
					case SWT.MouseMove: {
						if (tip == null)
							break;
						tip.dispose();
						tip = null;
						label = null;
						break;
					}
					case SWT.MouseHover: {
						TableItem item = viewer.getTable().getItem(new Point(event.x, event.y));
						if(item == null) return;
						setTableCursor(new Point(event.x,event.y));
						int column = TerminalConfigView.getColumn(new Point(event.x, event.y), item);
						TerminalSite sshsite = (TerminalSite)item.getData();
						String text = "";
						switch (column){
						case 0:
							String name = sshsite.getName();
							String id = sshsite.getId();
							if(name!=null&&id!=null){
								text = name + "\n Internal id: " + id;
							}						
							break;
						case 1:
							String conninfo = sshsite.getConnTooltip();
							if(conninfo!=null){
								text = conninfo;
								text = text.replaceAll(", ", "\n");
							}					
							break;
						case 2:
							break;
						case 3:
							String sitetype = sshsite.getSiteType();
							if(sitetype!=null){
								if(sitetype.equals(TerminalConstants.SITE_TYPE_UNICORE))
									text = "SSH target site generated by UNICORE Grid Browser";
								else
									text = "SSH target site generated by user";
							}							
							break;
						}
						if (item != null && !text.equals("")) {
							if (tip != null && !tip.isDisposed())
								tip.dispose();
							tip = new Shell(viewer.getTable().getDisplay(), SWT.ON_TOP | SWT.TOOL);
							tip.setLayout(new FillLayout());
							label = new Label(tip, SWT.NONE);
							label.setForeground(viewer.getTable().getDisplay()
									.getSystemColor(SWT.COLOR_INFO_FOREGROUND));
							label.setBackground(viewer.getTable().getDisplay()
									.getSystemColor(SWT.COLOR_INFO_BACKGROUND));
							label.setData("_TABLEITEM", item);
							label.setText(text);
							label.addListener(SWT.MouseExit, labelListener);
							label.addListener(SWT.MouseDown, labelListener);
							Point size = tip.computeSize(SWT.DEFAULT, SWT.DEFAULT);
							Rectangle rect = item.getBounds(column);
							Point pt = viewer.getTable().toDisplay(rect.x, rect.y);
							tip.setBounds(pt.x, pt.y, size.x, size.y);
							if(column<2 || column==3)tip.setVisible(true);
						}
					}
					}
				}
			};
			viewer.getTable().addListener(SWT.Dispose, tableListener);
			viewer.getTable().addListener(SWT.KeyDown, tableListener);
			viewer.getTable().addListener(SWT.MouseMove, tableListener);
			viewer.getTable().addListener(SWT.MouseHover, tableListener);

			column.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					tableSorter.setColumn(index);
					int dir = viewer.getTable().getSortDirection();
					if (viewer.getTable().getSortColumn() == column) {
						dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
					} else {

						dir = SWT.DOWN;
					}
					viewer.getTable().setSortDirection(dir);
					viewer.getTable().setSortColumn(column);
					viewer.refresh();
				}
			});
			
			viewerColumn.setEditingSupport(new TerminalSiteEditingSupport(viewer, i));
		}

		 final Menu menu = new Menu(viewer.getTable());
		 viewer.getTable().setMenu(menu);
		 menu.addMenuListener(new MenuAdapter() {
			 public void menuShown(MenuEvent e) {
				 MenuItem[] items = menu.getItems();
				 for (int i = 0; i < items.length; i++) {
					 items[i].dispose();
				 }

				 final int index = viewer.getTable().getSelectionIndex();
				 if (index == -1) return;
				 MenuItem openItem = new MenuItem(menu, SWT.PUSH);
				 openItem.setText("Open Terminal");
				 Image openimg = UnicoreTerminalPlugin.getImageDescriptor("terminal.png").createImage();
				 openItem.setImage(openimg);
				 openItem.addListener(SWT.Selection, new Listener() {
					 public void handleEvent(Event event) {
						 //ISelection selection = viewer.getSelection();
						 Object o = viewer.getElementAt(index);
						 if(o instanceof TerminalSite)
						 {
							 TerminalSite site = (TerminalSite) o;
							 openTargetSite(site);
						 }
					 }
				 });

				 IWorkbench workbench = PlatformUI.getWorkbench();
				 ISharedImages images = workbench.getSharedImages();
				  
				 MenuItem editMenuItem = new MenuItem(menu, SWT.PUSH);
				 editMenuItem.setText("Edit Config");
				 Image editimg = UnicoreTerminalPlugin.getImageDescriptor("configure-terminal.png").createImage();
				 editMenuItem.setImage(editimg);
				 editMenuItem.addListener(SWT.Selection, new Listener() {
 					 public void handleEvent(Event event) {
 						editTargetSites(null, null, null, null);
 					 }
 				 });
				 
				 MenuItem deleteeMenuItem = new MenuItem(menu, SWT.PUSH);
				 deleteeMenuItem.setText("Delete");
				 Image delimg = UnicoreTerminalPlugin.getImageDescriptor("delete.png").createImage();
				 deleteeMenuItem.setImage(delimg);
				 deleteeMenuItem.addListener(SWT.Selection, new Listener() {
 					 public void handleEvent(Event event) {
 						deleteTargetSites();
 					 }
 				 });
				 
				 MenuItem copyMenuItem = new MenuItem(menu, SWT.PUSH);
				 copyMenuItem.setText("Copy");
				 Image copyimg = images.getImage(ISharedImages.IMG_TOOL_COPY);
				 copyMenuItem.setImage(copyimg);
				 copyMenuItem.addListener(SWT.Selection, new Listener() {
 					 public void handleEvent(Event event) {
 						Object o = viewer.getElementAt(index);
						 if(o instanceof TerminalSite)
						 {
							 final TerminalSite site = (TerminalSite) o;
							 createTargetSite("Copy_" + site.getName(),site);
						 }
 						
 					 }
 				 });
			 }
		 });


		table.setHeaderVisible(true);
		table.setLinesVisible(true);
	}
	
	 
	
//	private void hookDoubleClickCommand() {
//		viewer.addDoubleClickListener(new IDoubleClickListener() {
//			public void doubleClick(DoubleClickEvent event) {
//				try {
//					// Check if the right column is seclected
//					int selectedColumn = -1;
//			        Point pt = getTableCursor();//tab.toDisplay(abs);//.toControl(abs);
//			        
//			        for (int i = 0; i <viewer.getTable().getColumnCount(); i++) {
//			        	System.out.println(viewer.getTable().getBounds().x);
//			        	System.out.println(viewer.getTable().getBounds().y);
//			            TableItem item = viewer.getTable().getItem(pt);			
//			            if (item != null) {			
//			                if (item.getBounds(i).contains(pt)) {			
//			                    selectedColumn = i;			
//			                }			
//			            }			
//			        }
//			          //execute when first column is selected (doesn't work exactly)		
//			        if(selectedColumn==0){
//			        	ISelection selection = viewer.getSelection();
//						if (!selection.isEmpty()){
//							IStructuredSelection sel = (IStructuredSelection) selection;							
//							TableItem[] tableItems = viewer.getTable().getSelection();				
//							Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
//							for(Object o : sel.toArray())
//							{
//								if(o instanceof TerminalSite)
//								{
//									TerminalSite site = (TerminalSite) o;						
//									String uri = site.getId();
//									String name = site.getName();
//									String connInfo = site.getConnectionInfo();
//									EditSSHSiteDialog dialog = new EditSSHSiteDialog(shell, uri, name, connInfo, site );
//									dialog.open();
//
//									if(dialog.getReturnCode()==SWT.OK || dialog.getReturnCode()==0){
//										viewer.setInput(ModelProvider.new_Instance().getAllSites());
//										viewer.getTable().setSelection(tableItems);
//										viewer.refresh();
//									}
//								}
//							}
//						}
//			        }									
//				} catch (Exception ex) {
//					throw new RuntimeException(
//							"Couldn't open Edit dialog");
//				}
//			}
//		});
//	}
	private void editTargetSites(String _uri, String _name, String _connInfo, TerminalSite _site){
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		EditSSHSiteDialog dialog = null;
		TableItem[] tableItems = null;
		if(_uri!=null){
			dialog = new EditSSHSiteDialog(shell, _uri, _name, _connInfo, _site );			
		}
		else{
			ISelection selection = viewer.getSelection();
			if (!selection.isEmpty()){
			
			String uri ="";
			String name ="";
			String connInfo ="";
				IStructuredSelection sel = (IStructuredSelection) selection;
				tableItems = viewer.getTable().getSelection();
				
				for(Object o : sel.toArray())
				{
					if(o instanceof TerminalSite)
					{
						TerminalSite site = (TerminalSite) o;						
						uri = site.getId();
						name = site.getName();
						connInfo = site.getConnectionInfo();
						dialog = new EditSSHSiteDialog(shell, uri, name, connInfo, site );						
					}
				}
			}
			else{
				showMessage("Select at first a target site");
				return;
			}
		}
		dialog.open();

		if(dialog.getReturnCode()==SWT.OK || dialog.getReturnCode()==0){
			viewer.setInput(ModelProvider.new_Instance().getAllSites());
			if(tableItems!=null)viewer.getTable().setSelection(tableItems);
			viewer.refresh();
		}
		
	}
	
	private void createTargetSite(String id, TerminalSite site){
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		CreateTargetSiteDialog dialog = null;
		if(site!=null){
			site.setSiteType(TerminalConstants.SITE_TYPE_CUSTOM);
			dialog = new CreateTargetSiteDialog(shell, id, ModelProvider.getInstance().getAllSiteIdsNamesMap(), site);
			
		}
		else{
			TerminalSite tsite = new TerminalSite();
			tsite.setId(id);
			tsite.setName(id);
			tsite.setSiteType(TerminalConstants.SITE_TYPE_CUSTOM);
			tsite.setConnectionType(TerminalConstants.NOTSELECTED);
			
			dialog = new CreateTargetSiteDialog(shell, id, ModelProvider.getInstance().getAllSiteIdsNamesMap(), tsite);
		}
		dialog.open();

		if(dialog.getReturnCode()==SWT.OK || dialog.getReturnCode()==0){
			viewer.setInput(ModelProvider.new_Instance().getAllSites());
			viewer.refresh();
		}
		
	}
	
	private void openTargetSite(TerminalSite _site){
		 final TerminalSite site = _site;
		 final String id = site.getId();
		 final String name = site.getName();
		 final String connInfo = site.getConnectionInfo();
		 final String defaultMethod = site.getConnectionType();
		 final Map<String,String> config = site.getConnectionTypeConfigs();
		 if(defaultMethod.equals(TerminalConstants.NOTSELECTED)){
			 if(site.getConnectionTypeConfigs().size()<1){
				 //configure Connection Type at first
				 PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable()
				 {
					 public void run() {
						 Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
						 MessageBox messageBox = new MessageBox(shell, SWT.ICON_QUESTION
								 | SWT.YES | SWT.NO);
						 messageBox.setMessage("There is no configured SSH method available " +
								 "for Site '" + name + "'\n Do you want to edit the SSH methods?");
						 messageBox.setText("Exiting Application");
						 int response = messageBox.open();
						 if (response == SWT.YES)
							 editTargetSites(id, name, connInfo, site);
					 }
				 });
			 }
			 else{
				 //choose Connection Type at first
				 showMessage("Select at first a default connection type in 'Terminal Connection Config'");
			 }
		 }
		 else{
			 PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable()
			 {
				 public void run()
				 {
					 try {
						 List<String> connectionTypes = new ArrayList<String>();
						 connectionTypes.addAll(site.getAvailableConnectionTypes());
						 // 'connectionType' key is not in site config
						 config.put(TerminalConstants.CONNECTION_TYPE,
								 defaultMethod);
						 UnicoreTerminalPlugin.getDefault().showTerminalView(name,connectionTypes, config);
					 } catch (Exception e) {
						 UnicoreTerminalPlugin.log(Status.ERROR, "Unable to open terminal connection: "+e.getMessage(), e);
						 showMessage("Unable to open terminal connection: "+e.getMessage());
					 }
				 }
			 });
		 }
	 
	}
	
	private void deleteTargetSites(){
		if(ModelProvider.getInstance().getAllSites().size()>0){	
			IStructuredSelection iselection = (IStructuredSelection)viewer.getSelection();
			if(!iselection.isEmpty()){
				MessageBox messageBox = new MessageBox(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), SWT.YES | SWT.NO);
				messageBox.setText("Confirmation");
				messageBox.setMessage(
				"Are you sure to remove the selected entries?");
				if (messageBox.open() == SWT.YES) {
					Set<String> toRemove = new HashSet<String>();
					for(Object o : iselection.toArray())
					{
						if(o instanceof TerminalSite)
						{
							TerminalSite site = (TerminalSite) o;
							toRemove.add(site.getId());
						}
					}
					ConfigPersistence.removeSites(toRemove);
					viewer.setInput(ModelProvider.new_Instance().readAllSites());
					refresh();
				}								
			}
			else{
				showMessage("Select at first a target site");
			}				
		}			

	}
	
	private void refreshTargetSites(){
		viewer.setInput(ModelProvider.new_Instance().readAllSites());
		refresh();
	}
	
	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {
	
		GridLayout layout = new GridLayout(5, false);
		parent.setLayout(layout);

		//Label fghLabel = new Label(parent, SWT.NONE);
		//fghLabel.setText(" ");
		
		Button newbutton = new Button(parent, SWT.PUSH);
		newbutton.setText("   New   ");
		newbutton.setToolTipText("Create a new custom target site");
		newbutton.addSelectionListener(new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			public void widgetSelected(SelectionEvent e) {
				createTargetSite("MyNewTargetSite", null);
			}
		});
		
		Button openbutton = new Button(parent, SWT.PUSH);
		openbutton.setText(" Open Terminal ");
		openbutton.setToolTipText("Open terminal to selected target site.");
		openbutton.addSelectionListener(new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			public void widgetSelected(SelectionEvent e) {
				ISelection selection = viewer.getSelection();
				if (!selection.isEmpty()){					
					IStructuredSelection sel = (IStructuredSelection) selection;
					for(Object o : sel.toArray())
					{
						if(o instanceof TerminalSite)
						{
							TerminalSite site = (TerminalSite) o;						
							openTargetSite(site);
							break;
						}
					}
				}
				else{
					showMessage("Select at first a target site");
				}

			}
		});
		
		Button editbutton = new Button(parent, SWT.PUSH);
		editbutton.setText(" Edit Config ");
		editbutton.setToolTipText("Get detailed dialog of the target site's connection configuration.");
		editbutton.addSelectionListener(new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			public void widgetSelected(SelectionEvent e) {
				editTargetSites(null, null, null, null);
				}
		});



		Button removebutton = new Button(parent, SWT.PUSH);
		removebutton.setText("   Delete   ");
		removebutton.setToolTipText("Delete target site configuration from client storage.");
		removebutton.addSelectionListener(new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			public void widgetSelected(SelectionEvent e) {
				deleteTargetSites();
			}			
		});

		Button refreshbutton = new Button(parent, SWT.PUSH);
		refreshbutton.setText("   Refresh   ");
		refreshbutton.setToolTipText("Refresh terminal connection data from client storage. \n Is usually done automatically");
		refreshbutton.addSelectionListener(new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			public void widgetSelected(SelectionEvent e) {
				refreshTargetSites();
			}
		});

		Label searchLabel = new Label(parent, SWT.NONE);
		searchLabel.setText("Search: ");
		final Text searchText = new Text(parent, SWT.BORDER | SWT.SEARCH);
		searchText.setToolTipText("Case-insensitive search for target sites in table. Press ESC to clear filter.");

		GridData gridData = new GridData();
		gridData.horizontalSpan = 4;
		gridData.grabExcessHorizontalSpace = true;
		//gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		searchText.setLayoutData(gridData);
		//	searchText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
		//			| GridData.HORIZONTAL_ALIGN_FILL  ));
		searchText.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent ke) {
				if(ke.keyCode == SWT.ESC) {
					filter.setSearchText("");
					searchText.setText("");
				}
				filter.setSearchText(searchText.getText());
				viewer.refresh();
			}

		});


		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FULL_SELECTION);
		createColumns(viewer);
		viewer.setContentProvider(new TerminalSiteContentProvider());
		viewer.setLabelProvider(new TerminalSiteLabelProvider(viewer));
		//viewer.setLabelProvider(new EmulatedNativeCheckBoxLabelProvider());
		//EmulatedNativeCheckBoxLabelProvider

		// Get the content for the viewer, setInput will call getElements in the
		// contentProvider
		viewer.setInput(ModelProvider.getInstance().getAllSites());

		// Make the selection available
		getSite().setSelectionProvider(viewer);

		tableSorter = new TableSorter();
		this.viewer.setSorter(tableSorter);

		filter = new SSHSiteFilter();
		viewer.addFilter(filter);

		// Layout the viewer
		gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 5;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(gridData);

		viewer.getTable().setSelection(0);
		packTable();

		getSite().getPage().addPartListener(partListener2);
		
		//hookDoubleClickCommand();
		
		viewer.getTable().addKeyListener(new KeyAdapter()
		{	
			public void keyPressed(KeyEvent e)
			{
				//String string = "";
 
				//check click together?
//				if ((e.stateMask & SWT.ALT) != 0) {
//					string += "ALT - keyCode = " + e.keyCode;
//					switch(e.character){
//					case 'd' :
//						//deleteTargetSites();
//						break;
//					
//					}
//				}
//				if ((e.stateMask & SWT.CTRL) != 0) string += "CTRL - keyCode = " + e.keyCode;
//				if ((e.stateMask & SWT.SHIFT) != 0) string += "SHIFT - keyCode = " + e.keyCode;
// 
//				if(e.keyCode == SWT.BS)
//				{
//					string += "BACKSPACE - keyCode = " + e.keyCode;
//				}
// 
//				if(e.keyCode == SWT.ESC)
//				{
//					string += "ESCAPE - keyCode = " + e.keyCode;
//				}
// 
//				//check characters 
//				if(e.keyCode >=97 && e.keyCode <=122)
//				{
//					string += " " + e.character + " - keyCode = " + e.keyCode;
//				}
// 
//				//check digit
//				if(e.keyCode >=48 && e.keyCode <=57)
//				{
//					string += " " + e.character + " - keyCode = " + e.keyCode;
//				}
//				
				//check digit
				if(e.keyCode == SWT.DEL)
				{
					//string += " " + e.character + " - keyCode = " + e.keyCode;
					deleteTargetSites();
				}
 
//				if(!string.equals(""))
//					System.out.println (string);
			}
		});


	}




	public void dispose() {
		getSite().getPage().removePartListener(partListener2);
	}

	private void editorActivated(IEditorPart editor) {
		if (getViewSite().getPage().isPartVisible(this))
			return;
	}


	

	private void packTable()
	{
		for( TableColumn col : viewer.getTable().getColumns())
		{
			col.pack();
		}
	}

	public void refresh(){
		viewer.setInput(ModelProvider.new_Instance().readAllSites());
		if(ModelProvider.getInstance().getAllSites().size()>0){
//			TableItem item = viewer.getTable().getItem(0);
//			ISelection sel = new StructuredSelection(item);
//		    viewer.setSelection(sel,true);					    
		    viewer.getTable().setSelection(0);
		}
		viewer.refresh();
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	private static int getColumn( Point pt, TableItem item )
	{
		int columns = item.getParent().getColumnCount();
		for (int i=0; i<columns; i++)
		{
			Rectangle rect = item.getBounds (i);
			if ( pt.x >= rect.x && pt.x < rect.x + rect.width ) return i;
		}
		return -1;
	}

	public static int getColumnFor(String columnName)
	{
		for (int i = 0; i < COLUMNS.length; i++) {
			if(COLUMNS[i].equals(columnName)) return i;
		}
		return -1;
	}
	
	private void showMessage( final String message ) {
		if (message != null && message.trim().length() != 0) {
			Display.getDefault().syncExec( new Runnable() {
				public void run() {
					Shell activeShell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
					MessageDialog.openInformation( activeShell,
							de.fzj.unicore.rcp.terminal.Messages.SSHTerminal_MessageTitle,
							message );
				}
			} );
		}
	}

}