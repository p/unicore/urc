package de.fzj.unicore.rcp.terminal.ui.tableviewer.model;

import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.fzj.unicore.rcp.terminal.extensionpoints.IConnectionConfigPersistence;

/**
 * 
 * @author Andre Giesler
 * @version $Id$
 * 
 */

public class TerminalSite {
	
	private static final String PROP_NAME = "name", PROP_ID = "id", 
	PROP_CONNECTION_TYPE= "connection type", PROP_SITE_TYPE = "site type";
	
	private List<String> availableConnectionTypes;
	private String connectionInfo;
	private String connTooltip;
	public String getConnTooltip() {
		return connTooltip;
	}

	public void setConnTooltip(String connData) {
		this.connTooltip = connData;
	}
	private String connectionType;
	private Map<String,String> connectionTypeConfigs = new HashMap<String, String>();
	private boolean edit;
	private String id;
	private String name;

	private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(
			this);
	private String siteType;
	public TerminalSite(){
	}
	
	
	
// TODO UCdetector: Remove unused code: 
// 	public void addPropertyChangeListener(String propertyName,
// 			PropertyChangeListener listener) {
// 		propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
// 	}

	public List<String> getAvailableConnectionTypes() {
		return availableConnectionTypes;
	}

	public String getConnectionInfo() {
		return connectionInfo;
	}

	public String getConnectionType() {
		return connectionType;
	}
	
	/**
	 * This map can be used by the different connection types for (non-persistently)
	 * storing their configurations. When using this map, be CAREFUL and choose
	 * unique keys in order to avoid key clashes. For persisting configurations
	 * instances of {@link IConnectionConfigPersistence} are used.
	 * @return
	 */
	public Map<String, String> getConnectionTypeConfigs() {
		return connectionTypeConfigs;
	}


	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getSiteType() {
		return siteType;
	}

	
	public boolean isEdit() {
		return edit;
	}
// TODO UCdetector: Remove unused code: 
// 	public void removePropertyChangeListener(PropertyChangeListener listener) {
// 		propertyChangeSupport.removePropertyChangeListener(listener);
// 	}
	
	public void setAvailableConnectionTypes(List<String> sshMethods) {
		this.availableConnectionTypes = sshMethods;
	}
	public void setConnectionInfo(String connInfo) {
		this.connectionInfo = connInfo;
	}
	public void setConnectionType(String sshMethod) {
		propertyChangeSupport.firePropertyChange(PROP_CONNECTION_TYPE, this.connectionType,
				this.connectionType = sshMethod);
	}
	public void setEdit(boolean edit) {
		this.edit = edit;
	}
	
	public void setId(String grid) {
		propertyChangeSupport.firePropertyChange(PROP_ID, this.id,
				this.id = grid);
	}
	public void setName(String name) {
		propertyChangeSupport.firePropertyChange(PROP_NAME, this.name,
				this.name = name);
	}
	public void setSiteType(String siteType) {
		propertyChangeSupport.firePropertyChange(PROP_SITE_TYPE, this.siteType,siteType);
		this.siteType = siteType;
	}
}
