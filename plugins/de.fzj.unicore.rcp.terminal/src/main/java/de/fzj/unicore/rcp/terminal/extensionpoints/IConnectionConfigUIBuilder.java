package de.fzj.unicore.rcp.terminal.extensionpoints;

import java.util.Map;

import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;

public interface IConnectionConfigUIBuilder {
	
	/**
	 * Inside this method, the UI for configuring the given connection should
	 * be created
	 * @param parent
	 * @param site
	 */
	public void buildUI(Composite parent, TerminalSite site);
	
	/**
	 * This method returns the relevant, editable key-value pairs of 
	 * the ConnectionTypes of a TerminalSite, so tooltip informations for the 
	 * TerminalConfigView's table can be created
	 * @param site
	 * @return 
	 */
	public Map<String,String> getTooltipInfo(TerminalSite site);
	
	
	/**
	 * Check user input. Return null if all input is valid or an error description
	 * if the input contains invalid settings;
	 * @return
	 */
	public String validateInput(Composite parent, TerminalSite site);
	
	/**
	 * Enable or disable relevant controls inside the built GUI. This is used
	 * when the user decides to add or remove a given connection type for a given site.
	 * @param parent
	 * @param enabled
	 * @return
	 */
	public void setEnabled(Composite parent, TerminalSite site, boolean enabled);
	
	/**
	 * This gets called when the user chooses to close the configuration dialog.
	 * It should be used for writing data from the UI to the site and possibly
	 * for performing some cleanup.  
	 */
	public void applyConfigurationChanges(Composite parent, TerminalSite site);
	
	/**
	 * This is called when the user decides to discard his configuration changes.
	 * Can be used to perform some cleanup.
	 * @param site
	 */
	public void discardConfigurationChanges(Composite parent, TerminalSite site);
	
	

}
