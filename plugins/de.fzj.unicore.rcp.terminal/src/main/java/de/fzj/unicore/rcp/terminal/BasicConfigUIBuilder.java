package de.fzj.unicore.rcp.terminal;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.terminal.extensionpoints.IConnectionConfigUIBuilder;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;

public abstract class BasicConfigUIBuilder implements IConnectionConfigUIBuilder {

	protected Map<String,Text> textFields = new HashMap<String, Text>();

	public Map<String, Text> getTextFields() {
		return textFields;
	}

	public void setTextFields(Map<String, Text> textFields) {
		this.textFields = textFields;
	}

	public void buildUI(Composite parent, TerminalSite site) {

		Map<String,String> config = site.getConnectionTypeConfigs();

		for(String key : getRelevantConfigKeys())
		{
			Label label = new Label(parent, SWT.NONE);
			label.setSize(64, 32);
			label.setText(getLabelForKey(key));
			Text text = new Text(parent, SWT.BORDER);
			text.setSize(64, 32);
			String value = config.get(key) == null ? "" : config.get(key);
			text.setText(value);
			textFields.put(key, text);
		}


		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		parent.setLayout(layout);

	}
	
	public Map<String,String> getTooltipInfo(TerminalSite site) {

		Map<String,String> config = site.getConnectionTypeConfigs();
		
		Map<String,String> attributes = new HashMap<String,String>();
		for(String key : getRelevantConfigKeys())
		{
			String value = config.get(key) == null ? "" : config.get(key);
			attributes.put(getLabelForKey(key), value);
		}

		return attributes;

	}

	public void applyConfigurationChanges(Composite parent, TerminalSite site) {
		for(String key : textFields.keySet())
		{
			String value = textFields.get(key).getText();
			site.getConnectionTypeConfigs().put(key, value);
		}
		textFields.clear();

	}

	public void discardConfigurationChanges(Composite parent, TerminalSite site) {
		textFields.clear();

	}


	public String validateInput(Composite parent, TerminalSite site)
	{
		for(String key : textFields.keySet()){
			Text text = textFields.get(key);
			if (text.getText().length() == 0) {
				return "Please check input at field "+getLabelForKey(key);
			}
		}
		return null;
	}

	public void setEnabled(Composite parent, TerminalSite site, boolean enabled)
	{
		Control[] controls = parent.getChildren();
		for(Control c : controls){
			c.setEnabled(enabled);		
		}
	}

	/**
	 * Subclasses must override in order to define which of the items in the 
	 * site's configuration are specific to the connection type associated to
	 * this UI builder.
	 * @return
	 */
	protected abstract String[] getRelevantConfigKeys();

	/**
	 * May be used to transform the unique ids of the configuration items
	 * into more human readable Strings that are used for the labels.
	 * @param key
	 * @return
	 */
	protected abstract String getLabelForKey(String key);


}
