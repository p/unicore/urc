package de.fzj.unicore.rcp.terminal.ui.dialogs;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.terminal.ConnectionTypeRegistry;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;

public class CreateTargetSiteDialog extends TitleAreaDialog {

	String proposed_id;
	HashMap<String,String> existing_SiteIds;
	TerminalSite site;
	
	Text text;
	
	public CreateTargetSiteDialog(Shell parentShell, String proposed_id,
			HashMap<String,String> existing_SiteIds, TerminalSite site) {
		super(parentShell);
		
		setShellStyle(SWT.TITLE | SWT.MODELESS | SWT.RESIZE);
		
		this.proposed_id = proposed_id;
		this.existing_SiteIds = existing_SiteIds;
		this.site = site;
	}

	protected Control createContents(Composite parent ) {
		Control contents = super.createContents(parent);
		// Set the title
		setTitle("Create a new SSH TargetSite");
		// Set the message
		setMessage("Enter a unique name for the new target site", IMessageProvider.INFORMATION);

		
		
		return contents;
	}

	@Override
	protected Control createDialogArea(final Composite parent) {		
		this.setHelpAvailable(false);
		CreateTargetSiteDialog.setDialogHelpAvailable(false);
		final Composite area = new Composite(parent, SWT.NULL);
	    final GridLayout gridLayout = new GridLayout();
	    gridLayout.marginWidth = 15;
	    gridLayout.marginHeight = 10;
	    gridLayout.numColumns = 2;
	    area.setLayout(gridLayout);
		Label label1 = new Label(area, SWT.NONE);
		label1.setText("TargetSite ID");
		
		text = new Text(area, SWT.BORDER);
		final GridData textGD = new GridData();
		text.setLayoutData(textGD);
		text.setText(proposed_id);
		text.setFocus();
		text.selectAll();
		
		final int dialogWidth = this.getInitialSize().x;
		final int minX = text.computeSize(-1, -1).x;
		text.addModifyListener(new ModifyListener() {		
			public void modifyText(ModifyEvent e) {
				Point p = text.computeSize(-1, -1);
				int curtextwd = Math.max(minX, p.x);;
				textGD.widthHint = curtextwd;
				//give a little space between max text width and dialog border
				if(dialogWidth>curtextwd + 150){
					parent.layout();
				}							
			}
		});

		return parent;
	}

	protected void createButtonsForButtonBar(Composite parent) {

		createOkButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	private Button createOkButton(Composite parent, int id, String label,
			boolean defaultButton) {
		// increment the number of columns in the button bar
		((GridLayout) parent.getLayout()).numColumns++;
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		button.setData(new Integer(id));
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				buttonPressed(((Integer) event.widget.getData()).intValue());			
			}
		});
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		setButtonLayoutData(button);
		return button;
	}

	private boolean checkInput(String text_value){
		//String id = text_value;
		
		
		for ( Map.Entry<String, String> e : existing_SiteIds.entrySet() ) {
			if(e.getValue().equals(text_value)){
				setMessage("Name " + e.getValue() + " exists already. Please choose another target site name.", IMessageProvider.ERROR);
				text.setFocus();
				text.selectAll();
				
				return false;
			}
		}
		for ( Map.Entry<String, String> e : existing_SiteIds.entrySet() ) {
			if(e.getKey().equals(text_value)){
				setMessage("Name is ok, but the value exists already as internal ID. Please choose another target site name.", IMessageProvider.ERROR);
				text.setFocus();
				text.selectAll();
//				id = e.getKey()+"_1";
//				break;
				return false;
			}
		}
		
		//site.setId(id);
		
		return true;
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			if(checkInput(text.getText())){
				site.setId(text.getText());
				site.setName(text.getText());
				ConnectionTypeRegistry.writeAllConnectionConfigs(site);
				super.okPressed();
			}
		}
		else if (buttonId == IDialogConstants.CANCEL_ID) {	
			super.cancelPressed();		
		}
		else {
			return;
		}

	}
}
