/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.servicebrowser.actions;

import org.chemomentum.common.util.C9MCommonConstants;
import org.chemomentum.common.ws.ILocationManager;
import org.chemomentum.dataManagement.locationManager.QueryLocationRequestDocument;
import org.chemomentum.dataManagement.locationManager.QueryLocationRequestDocument.QueryLocationRequest.Input;
import org.chemomentum.dataManagement.locationManager.QueryLocationResponseDocument;
import org.chemomentum.dataManagement.locationManager.QueryLocationResponseDocument.QueryLocationResponse.Result;
import org.chemomentum.rcp.servicebrowser.Activator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.ui.PlatformUI;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import eu.unicore.security.wsutil.client.UnicoreWSClientFactory;
import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * @author demuth
 * 
 */
public class LMResolveLogicalNameAction extends NodeAction {

	public LMResolveLogicalNameAction(Node node) {
		super(node);
		setText("resolve logical name");
		setToolTipText("Retrieve physical locations of the file with the given logical name.");
		setImageDescriptor(ServiceBrowserActivator.getImageDescriptor(IMAGE));
	}

	@Override
	public void run() {

		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {

			public void run() {
				try {

					String title = "Resolve Logical Name";
					String question = "Please enter the logical name to be resolved.";
					InputDialog id = new InputDialog(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getShell(), title,
							question, "", null);
					id.open();
					if (id.getReturnCode() != Window.OK) {
						return;
					}
					Input input = Input.Factory.newInstance();
					String logicalName = id.getValue();
					if (!logicalName
							.startsWith(C9MCommonConstants.LOGICAL_FILENAME_PREFIX)) {
						logicalName = C9MCommonConstants.LOGICAL_FILENAME_PREFIX
								+ logicalName;
					}
					input.setLogicalName(logicalName);
					Input[] names = { input };
					EndpointReferenceType epr = getNode().getEpr();
					IClientConfiguration secProps = ((SecuredNode) getNode())
							.getUASSecProps();
					ILocationManager lm = new UnicoreWSClientFactory(
							secProps).createPlainWSProxy(
							ILocationManager.class, epr.getAddress()
									.getStringValue());
					QueryLocationRequestDocument request = QueryLocationRequestDocument.Factory
							.newInstance();

					request.addNewQueryLocationRequest().setInputArray(names);
					QueryLocationResponseDocument response = lm
							.queryLocation(request);
					Result[] results = response.getQueryLocationResponse()
							.getResultArray();
					String locations = "";
					if (results != null && results.length > 0) {
						for (int j = 0; j < results.length; j++) {
							locations += results[j].getLogicalName() + "\n";
							String[] physical = results[j]
									.getPhysicalLocationArray();
							for (int i = 0; i < physical.length; i++) {
								locations += (i + 1) + ":" + physical[i] + "\n";
							}
							locations += "\n";
						}
					}
					if (locations.trim().length() == 0) {
						locations = "None";
					}

					title = "Found physical locations for this logical file:";

					MessageDialog md = new MessageDialog(PlatformUI
							.getWorkbench().getActiveWorkbenchWindow()
							.getShell(), title, null, locations, SWT.OK,
							new String[] { "Ok" }, 0);
					md.open();
				} catch (Exception e) {
					Activator.log(IStatus.WARNING,
							"Unable to resolve logical name", e);
				}

			}

		});

	}

}
