/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.servicebrowser.filter;

import java.util.Calendar;

import org.chemomentum.rcp.servicebrowser.nodes.WorkflowNode;
import org.chemomentum.workflow.xmlbeans.StatusType;

import de.fzj.unicore.rcp.servicebrowser.filters.TaskFilterCriteria;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * implements a filter for Workflows by status and workflowname as a regular
 * expression
 * 
 * @author Christian Hohmann
 * @author bdemuth 
 * 
 */
public class WorkflowFilterCriteria extends TaskFilterCriteria {

	final static int int_undefined = 1;
	final static int int_ready = 2;
	final static int int_running = 8;
	final static int int_successful = 16;
	final static int int_failed = 32;
	final static int int_input_needed = 64;
	final static int int_aborted = 128;
	final static int int_all = int_undefined | int_ready | int_running
			| int_successful | int_failed | int_input_needed | int_aborted;
	final static int[] STATUS_ARRAY = new int[] { int_all, int_undefined,
			int_ready, int_running, int_successful, int_failed,
			int_input_needed, int_aborted };
	final static String[] STATUS_STRING_ARRAY = new String[] { "All Workflows",
			"Workflows with undefined status", "Ready Workflows",
			"Running Workflows", "Successful Workflows", "Failed Workflows",
			"Workflows that need user input", "Aborted Workflows" };


	public WorkflowFilterCriteria(int status) {
		this(status, "");
	}

	public WorkflowFilterCriteria(int status, String pattern) {
		this(status, pattern, int_noTime, null, null);
	}
	
	public WorkflowFilterCriteria(int status, String pattern, int timeMode,
			Calendar time1, Calendar time2) {
		super(status, pattern, timeMode, time1, time2);
		
	}

	public WorkflowFilterCriteria(String pattern) {
		this(int_all, pattern);
	}

	protected boolean fitsNodeType(Node node) {
		return WorkflowNode.TYPE.equals(node.getType());
	}
	
	protected boolean fitsTaskStatus(Node node) {
		if (!(node instanceof WorkflowNode)) {
			return false;
		}
		WorkflowNode workflow = (WorkflowNode) node;
		return isSet(getWorkflowStatusCipher(workflow),statusMode);
	}

	@Override
	protected Calendar getSubmissionTime(Node node) {
		if(!(node instanceof WorkflowNode)) return null;
		WorkflowNode job = (WorkflowNode) node;
		return job.getNormalizedSubmissionTime();
	}

	@Override
	protected Calendar getTerminationTime(Node node) {
		if(!(node instanceof WorkflowNode)) return null;
		WorkflowNode job = (WorkflowNode) node;
		return job.getNormalizedTime(job.getTerminationTime());
	}

	protected int getWorkflowStatusCipher(WorkflowNode node) {

		StatusType.Enum status = node.getWorkflowStatus();
		int result = 0;
		if (StatusType.FAILED.equals(status)) {
			result = int_failed;
		} else if (StatusType.READY.equals(status)) {
			result = int_ready;
		} else if (StatusType.RUNNING.equals(status)) {
			result = int_running;
		} else if (StatusType.SUCCESSFUL.equals(status)) {
			result = int_successful;
		} else if (StatusType.UNDEFINED.equals(status)) {
			result = int_undefined;
		} else if (StatusType.USERINPUT_NEEDED.equals(status)) {
			result = int_input_needed;
		} else if (StatusType.ABORTED.equals(status)) {
			result = int_aborted;
		}
		return result;
	} 
	
}