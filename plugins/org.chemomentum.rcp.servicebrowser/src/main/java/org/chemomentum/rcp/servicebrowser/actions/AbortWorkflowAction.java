/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.servicebrowser.actions;

import org.chemomentum.rcp.servicebrowser.Activator;
import org.chemomentum.rcp.servicebrowser.nodes.WorkflowNode;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import de.fzj.unicore.rcp.servicebrowser.actions.AbstractNodeCommandHandler;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * @author demuth
 * 
 */
public class AbortWorkflowAction extends AbstractNodeCommandHandler {

	public AbortWorkflowAction() {
		
	}

	@Override
	public boolean confirmAction(Shell shell) {
		return MessageDialog.openConfirm(shell, "Confirm action", "Do you really want to abort the selected workflow(s)?");
	}


	@Override
	protected IStatus performAction(IProgressMonitor monitor, Node n) {
		monitor.beginTask("aborting", 1);
		try {
			WorkflowNode node = (WorkflowNode) n;
			node.abortWorkflow();
		}
		catch (Exception e) {
			Activator
					.log(IStatus.ERROR, "Could not abort workflow", e);
		}
		finally {
			monitor.done();
		}
		return Status.OK_STATUS;
	}

	@Override
	protected String getJobName() {
		return "aborting workflow(s)";
	}

	@Override
	protected String getTaskName() {
		return "aborting...";
	}


}
