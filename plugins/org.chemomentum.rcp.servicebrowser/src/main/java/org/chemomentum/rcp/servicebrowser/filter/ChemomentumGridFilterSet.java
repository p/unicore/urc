/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.servicebrowser.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.namespace.QName;

import org.chemomentum.rcp.servicebrowser.nodes.WorkflowFactoryNode;
import org.chemomentum.rcp.servicebrowser.nodes.WorkflowNode;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.swt.widgets.Shell;

import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IGridBrowserFilterExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.filters.ConfigurableFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.ContentFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterCriteria;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterMenuCategory;
import de.fzj.unicore.rcp.servicebrowser.filters.GridFilterAction;
import de.fzj.unicore.rcp.servicebrowser.ui.DefaultFilterSet;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceView;

/**
 * this class provides StandardGridFilters that give a basic possibility to
 * filter different criteria.
 * 
 * @author Christian Hohmann
 * 
 */
public class ChemomentumGridFilterSet implements
		IGridBrowserFilterExtensionPoint {
	Shell platformDisplay;

	UserDefinedWorkflowFilterConfiguration wfcTime;

	final int fileAndFolder = 0;
	final int fileOnly = 1;
	final int folderOnly = 2;

	IServiceViewer viewer;
	ServiceView serviceView;
	String nameSpace = "de.fzj.unicore.rcp.servicebrowser.filter";

	private List<GridFilterAction> createWorkflowActions() {

		List<GridFilterAction> result = new ArrayList<GridFilterAction>();
		FilterMenuCategory category = createWorkflowCategory();
		Action action = new Action("Userdefined Workflow Filter",
				IAction.AS_RADIO_BUTTON) {
			@Override
			public void run() {
				if (this.isChecked()) {
					FilterCriteria filter = wfcTime.createTaskFilter();
					if (filter != null) {
						serviceView.activateFilter(this.getId(), filter);
					}
				}
			}
		};
		
		GridFilterAction gfa = new GridFilterAction(action, category, false);
		
		wfcTime = new UserDefinedWorkflowFilterConfiguration();
		DefaultFilterSet dfs = serviceView.getDefaultFilterSet();
		String id = dfs.getSelectedGridFilterId();
		ContentFilter cf = dfs.getFilterController().getFilter(DefaultFilterSet.FILTER_ID_NODE_TYPES);
		if(cf instanceof ConfigurableFilter)
		{
			ConfigurableFilter confFilter = (ConfigurableFilter) cf;
			FilterCriteria crit = confFilter.getGridFilter();
			if(action.getId().equals(id)
					&& (crit instanceof WorkflowFilterCriteria))
			{
				WorkflowFilterCriteria old = (WorkflowFilterCriteria) crit;
				wfcTime.loadConfiguration(old);
			}
		}
		result.add(gfa);

		for (int i = 0; i < WorkflowFilterCriteria.STATUS_ARRAY.length; i++) {
			final int status = WorkflowFilterCriteria.STATUS_ARRAY[i];
			String statusName = WorkflowFilterCriteria.STATUS_STRING_ARRAY[i];
			action = new Action(statusName, IAction.AS_RADIO_BUTTON) {
				@Override
				public void run() {
					if (this.isChecked()) {
						FilterCriteria filter = new WorkflowFilterCriteria(status);
						if (filter != null) {
							serviceView.activateFilter(this.getId(), filter);
						}
					}
				}
			};
			gfa = new GridFilterAction(action, category, false);
			result.add(gfa);
		}

		return result;
	}

	private FilterMenuCategory createWorkflowCategory() {
		return new FilterMenuCategory(
				Collections.singletonList(new QName(nameSpace, "Workflows")));
	}

	private List<GridFilterAction> createWorkflowServiceActions() {

		List<GridFilterAction> result = new ArrayList<GridFilterAction>();
		Action action = new Action("All Workflow Services",
				IAction.AS_RADIO_BUTTON) {
			@Override
			public void run() {
				if (this.isChecked()) {
					serviceView.activateFilter(this.getId(),
							new WorkflowServiceFilterCriteria());
				}
			}
		};
		GridFilterAction gfa = new GridFilterAction(action,
				createWorkflowServiceCategory(), true);
		result.add(gfa);
		return result;
	}

	/*
	 * userdefined Workflow filter
	 */

	private FilterMenuCategory createWorkflowServiceCategory() {
		return new FilterMenuCategory(Collections
				.singletonList(new QName(nameSpace, "Workflow Services")));
	}

	public String[] getDefaultNodeTypes(IServiceViewer viewer) {
		return new String[] { WorkflowFactoryNode.TYPE, WorkflowNode.TYPE };
	}

	public GridFilterAction[] getGridFilterList(IServiceViewer viewer,
			ServiceView serviceView) {
		this.viewer = viewer;
		this.serviceView = serviceView;

		List<GridFilterAction> retval = new ArrayList<GridFilterAction>();
		retval.addAll(createWorkflowActions());
		retval.addAll(createWorkflowServiceActions());

		return retval.toArray(new GridFilterAction[0]);
	}

}