/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.servicebrowser.filter;

import org.chemomentum.common.ws.WorkflowManagement;
import org.chemomentum.rcp.servicebrowser.nodes.WorkflowNode;

import de.fzj.unicore.rcp.servicebrowser.filters.StandardFilterCriteria;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;

/**
 * this class implements filtering ready workflows
 * 
 * @author Christian Hohmann
 * 
 */

public class WorkflowReadyFilterCriteria extends StandardFilterCriteria {

	public WorkflowReadyFilterCriteria() {
	}

	/**
	 * decides if the node is fitting the criteria or not. Caution, the node
	 * might be displayed if a parent of this node is fitting the criteria and
	 * the childnode configuration is configured so
	 */
	@Override
	public boolean fitCriteria(Node node) {

		// first filter level: element is a workflow Node
		if (WorkflowManagement.SERVICE_NAME.equals(node.getType())) {

			// second filter level: correct workflow status
			String workflowstatus = (((WorkflowNode) node).getWorkflowStatus()
					.toString());

			if (!("SUCCESSFUL".equals(workflowstatus))) {
				return false;
			}

			return true;
		}
		return false;
	}
}