package org.chemomentum.rcp.servicebrowser.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.chemomentum.rcp.servicebrowser.Activator;


import de.fzj.unicore.rcp.servicebrowser.actions.AbstractNodeCommandHandler;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import org.chemomentum.rcp.servicebrowser.nodes.WorkflowNode;

/**
 * @author mborcz, rajveer
 * 
 */

public class ResumeWorkflowAction extends AbstractNodeCommandHandler {

	private String [] parameterValues;
	private Map<String, String> newParameters = new HashMap<String, String>();

	public ResumeWorkflowAction() {
	}

	@Override
	public boolean confirmAction(Shell shell) {


		IStructuredSelection objectsSelection = getSelection();

		Object[] objects = objectsSelection.toArray();
		if(objects == null || objects.length != 1 || !(objects[0] instanceof WorkflowNode))
		{
			return false;
		}
		WorkflowNode node = (WorkflowNode) objects[0];
		try {

			// make sure to clear first the old mappings
			newParameters.clear();
			for (Map.Entry<String, String> entry : node.readParameters().entrySet()) {
				newParameters.put(entry.getKey(), entry.getValue());
			}
		} catch (Exception e) {
			Activator.log(IStatus.ERROR, "Error reading workflow parameters", e);
		}
		if (!newParameters.isEmpty()) {

			//opens custom dialog, such that user can edit the parameters
			customDialog cdialog = new customDialog(shell, newParameters);
			cdialog.open();
			// will get null, if no value is changed
			parameterValues = cdialog.getNewParameterValues();

			return cdialog.getReturnCode() == Window.OK;	
		}
		else {
			// opens a simple confirm dialog, if there are no parameters to edit
			return MessageDialog.openConfirm(shell, "Confirm action","Do you really want to resume the selected workflow(s)?");

		}
	}

	@Override
	protected IStatus performAction(IProgressMonitor monitor, Node n) {
		monitor.beginTask("resuming", 1);
		try {
			WorkflowNode node = (WorkflowNode) n;

			// pass only non empty map
			if (!newParameters.isEmpty()) {

				int index = 0;
				for (Map.Entry<String, String> entry : newParameters.entrySet()) {
					// do not change the parameter values, if they are not touched at all
					// otherwise this will overwrite initial values with null
					if (parameterValues[index] != null) {

						entry.setValue(parameterValues[index]);
					}
					index++;
				}				
				node.writeUpdatedParameters(newParameters);
			}
			node.resumeWorkflow();

		} catch (Exception e) {
			Activator.log(IStatus.ERROR, "Could not resume workflow", e);
		} finally {
			monitor.done();
		}
		return Status.OK_STATUS;
	}

	@Override
	protected String getJobName() {
		return "resuming workflow(s)";
	}

	@Override
	protected String getTaskName() {
		return "resuming...";
	}

	// a custom dialog for viewing / updating parameters.
	private static class customDialog extends Dialog{

		private Map<String, String> parameters;
		private static ArrayList<Text> parameterValuesText;
		private String [] values;
		int index = 0;

		protected customDialog(Shell parentShell, Map<String, String> parameters) {
			super(parentShell);	
			this.parameters = parameters;
			parameterValuesText = new ArrayList<Text>(parameters.size());
			this.values = new String[parameters.size()];

		}	
		@Override
		protected Control createDialogArea(Composite parent) {

			Composite container = (Composite) super.createDialogArea(parent);
			container.setLayout(new GridLayout(2, false));

			Label label = new Label(container, SWT.NONE);
			label.setText("Please enter new values of variables, if you want to change them.");
			
			GridData data = new GridData();			
			data.horizontalIndent = 5;	
			data.horizontalAlignment = SWT.LEFT;
			data.horizontalSpan = 2;
			data.grabExcessHorizontalSpace = false;
			data.grabExcessVerticalSpace = true;
			data.verticalAlignment = SWT.CENTER;
			label.setLayoutData(data);
			
			final ScrolledComposite scrolledComposite = new ScrolledComposite(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);

			scrolledComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
			GridData gridD = new GridData(GridData.FILL_BOTH);

			scrolledComposite.setLayoutData(gridD);
			scrolledComposite.setAlwaysShowScrollBars(true);
			
			
			final Composite composite = new Composite(scrolledComposite, SWT.NONE);	
			scrolledComposite.setContent(composite);

			composite.setLayout(new GridLayout(2, false));
			data = new GridData();			
			data.grabExcessHorizontalSpace = true;
			data.grabExcessVerticalSpace = true;			
			data.horizontalIndent = 20;
			data.horizontalAlignment = SWT.CENTER;			
			composite.setLayoutData(data);

			for (Map.Entry<String, String> entry : parameters.entrySet()) {
				Label keylabel = new Label(composite, SWT.NONE);
				keylabel.setText(entry.getKey() + ":");
				GridData gd = new GridData();
				gd.horizontalAlignment = SWT.RIGHT;
				gd.grabExcessHorizontalSpace = true;
				keylabel.setLayoutData(gd);			

				parameterValuesText.add(index, new Text(composite, SWT.BORDER));
				parameterValuesText.get(index).setText(entry.getValue());
				gd = new GridData();
				gd.grabExcessHorizontalSpace = true;
				gd.widthHint = 250;
				gd.horizontalIndent = 20;
				parameterValuesText.get(index).setLayoutData(gd);

				parameterValuesText.get(index).addModifyListener(new ModifyListener() {

					@Override
					public void modifyText(ModifyEvent e) {						
						for (int j = 0; j < values.length; j++) {
							// important to update all values, if any parameter value is changed
							values[j] = parameterValuesText.get(j).getText();
						}						
					}
				});
				
				
				composite.layout();
				composite.setSize(500, 200);

				scrolledComposite.addControlListener(new ControlAdapter() {
					@Override
					public void controlResized(final ControlEvent e) {
						Rectangle r = scrolledComposite.getClientArea();
						Point cSize = composite.computeSize(r.width, SWT.DEFAULT);
						composite.setSize(cSize);
					}
				});				
			
				index++;
			}	
			return container;
		}

		@Override
		protected void configureShell(Shell newShell) {
			super.configureShell(newShell);
			newShell.setText("Resuming workflow(s)");
		}

		@Override
		protected Point getInitialSize() {
			return new Point(600, 400);
		}

		protected String[] getNewParameterValues() {

			return values;
		}
	}
}
