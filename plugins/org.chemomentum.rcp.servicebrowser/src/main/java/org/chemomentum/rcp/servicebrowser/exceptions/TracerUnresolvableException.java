package org.chemomentum.rcp.servicebrowser.exceptions;

public class TracerUnresolvableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1602873597194458386L;

	public TracerUnresolvableException(String message) {
		super(message);
	}

	public TracerUnresolvableException(String message, Throwable cause) {
		super(message, cause);
	}

	public TracerUnresolvableException(Throwable cause) {
		super(cause);
	}

}
