/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

package org.chemomentum.rcp.servicebrowser.filter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.servicebrowser.filters.TaskFilterCriteria;
import de.fzj.unicore.rcp.servicebrowser.filters.TaskFilterConfiguration;

/**
 * provides the configuration GUI for the WorkflowFilter
 * 
 * @author Christian Hohmann
 * 
 */

public class UserDefinedWorkflowFilterConfiguration extends TaskFilterConfiguration {



	private Button checkRunningWorkflow;
	private Button checkFailedWorkflow;
	private Button checkSuccessfulWorkflow;


	@Override
	protected void createStatusButtons(Composite platformDisplay) {
		checkSuccessfulWorkflow = new Button(platformDisplay, SWT.CHECK);
		checkSuccessfulWorkflow.setText("Successful workflows");
		checkSuccessfulWorkflow.setSelection(inStatusMode(WorkflowFilterCriteria.int_successful));
		
		checkFailedWorkflow = new Button(platformDisplay, SWT.CHECK);
		checkFailedWorkflow.setText("Failed workflows");
		checkFailedWorkflow.setSelection(inStatusMode(WorkflowFilterCriteria.int_failed));
		
		checkRunningWorkflow = new Button(platformDisplay, SWT.CHECK);
		checkRunningWorkflow.setText("Running workflows");
		checkRunningWorkflow.setSelection(inStatusMode(WorkflowFilterCriteria.int_running));
	}


	@Override
	protected void deselectAllStatusWidgets() {
		checkFailedWorkflow.setSelection(false);
		checkSuccessfulWorkflow.setSelection(false);
		checkRunningWorkflow.setSelection(false);
	}


	@Override
	protected TaskFilterCriteria doCreateTaskFilter() {
		return new WorkflowFilterCriteria(statusMode, pattern, timeMode, time1, time2);
	}

	

	
	@Override
	protected int getDefaultStatusMode() {
		return WorkflowFilterCriteria.int_all;
	}


	protected void okPressed()
	{
		statusMode = 0;
		statusMode |= checkRunningWorkflow.getSelection() ? WorkflowFilterCriteria.int_running: 0;
		statusMode |= checkSuccessfulWorkflow.getSelection() ? WorkflowFilterCriteria.int_successful: 0;
		statusMode |= checkFailedWorkflow.getSelection() ? WorkflowFilterCriteria.int_failed: 0;

		super.okPressed();
	}
	
	@Override
	protected void selectAllStatusWidgets() {
		checkFailedWorkflow.setSelection(true);
		checkSuccessfulWorkflow.setSelection(true);
		checkRunningWorkflow.setSelection(true);
	}
}