/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.servicebrowser.nodes;

import java.beans.PropertyChangeListener;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;
import org.chemomentum.common.api.workflow.WFClient;
import org.chemomentum.common.impl.workflow.WorkflowSubmissionClient;
import org.chemomentum.common.ws.IWorkflowFactory;
import org.chemomentum.rcp.servicebrowser.Activator;
import org.chemomentum.rcp.servicebrowser.exceptions.TracerUnresolvableException;
import org.chemomentum.workflow.xmlbeans.AccessibleWorkflowReferenceDocument;
import org.chemomentum.workflow.xmlbeans.WorkflowFactoryPropertiesDocument;
import org.chemomentum.workflow.xmlbeans.WorkflowFactoryPropertiesDocument.WorkflowFactoryProperties;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeData;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.RegistryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.GroupNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.ILazyParent;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.ILazyRetriever;
import de.fzj.unicore.rcp.servicebrowser.utils.URIUtils;
import de.fzj.unicore.uas.client.EnumerationClient;

/**
 * @author demuth
 * 
 */
public class WorkflowFactoryNode extends WSRFNode implements ILazyRetriever {

	private static final long serialVersionUID = 3251177251348058963L;

	public static final String DIALECTS = "Dialects";
	public static final String VERSION = "Version";

	public static final String TYPE = IWorkflowFactory.SERVICE_NAME;

	public WorkflowFactoryNode(EndpointReferenceType epr) {
		super(epr);
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		super.addPropertyChangeListener(listener);
	}

	/**
	 * Create the workflow instance but do not submit the workflow yet. The
	 * workflow can be submitted with the returned {@link WFClient}
	 * 
	 * @param workflowWrapper
	 * @return
	 * @throws Exception
	 */
	public WFClient createWorkflow(String workflowName, Calendar terminationTime)
			throws Exception {
		WorkflowSubmissionClient submitWFClient = new WorkflowSubmissionClient(
				getURL().toString(), getEpr(), getUASSecProps());
		return submitWFClient.createWorkflow(workflowName, terminationTime);
	}

	public GroupNode createWorkflowsNode() {
		EndpointReferenceType jobNodeEpr = EndpointReferenceType.Factory
				.newInstance();
		jobNodeEpr.addNewAddress().setStringValue(getWorkflowsNodeURI());
		Utils.addPortType(jobNodeEpr, GroupNode.PORTTYPE);
		GroupNode lazyNode = (GroupNode) NodeFactory.createNode(jobNodeEpr);
		lazyNode.setChildType(WorkflowNode.TYPE);
		return lazyNode;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class key) {
		if (key.getName().equals(TracerNode.class.getName())) {
			try {
				return getTracerNode();
			} catch (Exception e) {
				Activator.log(IStatus.WARNING, "Unable to find tracer.", e);
			}

		} else if (key.getName().equals(RegistryNode.class.getName())) {
			try {
				return getRegistryNode();
			} catch (Exception e) {
				return null;
			}
		}

		return super.getAdapter(key);
	}

	/**
	 * Integer value for sorting nodes in views. Nodes are usually sorted in
	 * ascending order.
	 * 
	 * @return
	 */
	@Override
	public int getCategory() {
		return 200;
	}

	@Override
	public DetailsAdapter getDetails() {
		DetailsAdapter details = super.getDetails();
		Map<String, String> hm = details.getMap();
		String[] supportedDialects = null;
		if (hasCachedResourceProperties()) {
			supportedDialects = getSupportedDialectArray();
		}
		String dialects = "";
		if (supportedDialects != null) {
			for (int i = 0; i < supportedDialects.length; i++) {
				dialects += supportedDialects[i];
				if (i < supportedDialects.length - 1) {
					dialects += ", ";
				}
			}

			hm.put(DIALECTS, dialects);
		}

		String engineVersion = null;
		try {
			engineVersion = getWorkflowFactoryProperties().getVersion();
		} catch (Exception e) {
			// do nothing
		}
		if (engineVersion == null) {
			engineVersion = "1.0.0";
		}
		hm.put(VERSION, engineVersion);
		return details;
	}

	// TODO we use XmlObject here instead of the correct
	// AccessibleWorkflowReferenceDocument due to a ClassCastException at runtime.
	// The classloader of EnumClient does not know the WF classes?!
	private transient EnumerationClient<XmlObject> ec=null;
	
	private synchronized EnumerationClient<XmlObject> getEnumerationClient(
			WorkflowFactoryProperties props) throws Exception {
		if(ec==null){
			if(props != null && props.getAccessibleWorkflowEnumeration() != null)
			{
				ec = new EnumerationClient<XmlObject>(props.getAccessibleWorkflowEnumeration(),
						getUASSecProps(),
						AccessibleWorkflowReferenceDocument.type.getDocumentElementName());
			}
		}
		return ec;

	}

	public LocationManagerNode getLocationManagerNode() throws Exception {
		try {
			Set<String> types = new HashSet<String>();
			types.add(LocationManagerNode.TYPE);
			List<Node> result = getRegistryNode().getChildrenOfTypes(types);
			if (result == null || result.size() == 0) {
				throw new Exception(
						"No location managers in registry. Don't know which one to use.");
			} else if (result.size() == 1) {
				return (LocationManagerNode) result.get(0);
			} else if (result.size() > 1) {
				throw new Exception(
						"Too many location managers in registry. Don't know which one to use.");
			}

		} catch (Exception e) {
			Activator.log(IStatus.WARNING, "Unable to find location manager.",
					e);
		}
		return null;
	}

	public long getNumChildren(ILazyParent n, IProgressMonitor progress) {
		if (!hasCachedResourceProperties()) {
			retrieveProperties(progress);
		}
		WorkflowFactoryProperties props = null;
		long result=-1;
		
		try {
			props = getWorkflowFactoryProperties();
		} catch (Exception e) {
			Activator.log(IStatus.WARNING,
					"Unable to determine number of submitted workflows for "
							+ getName() + ": " + e.getMessage(), e);
		}
		
		EnumerationClient<XmlObject> enumClient = null;
		try {
			enumClient = getEnumerationClient(props);
			if (enumClient != null) {
				result = enumClient.getNumberOfResults();
			}
			
			// careful: 6.4.x workflow service returns zero accessible WFs via enum 
			if(result<=0 && props.getAccessibleWorkflowReferenceArray() != null) {
					result = props.getAccessibleWorkflowReferenceArray().length;
			}
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.WARNING,
					"Unable to retrieve number of jobs for " + getNamePathToRoot(), e);
			
		}
		
		return result;
	}

	@Override
	public SchemaType getSchemaType() {
		return WorkflowFactoryPropertiesDocument.type;
	}

	public String[] getSupportedDialectArray() {
		if (!hasCachedResourceProperties()) {
			return new String[0];
		}

		try {
			WorkflowFactoryProperties wfProps = getWorkflowFactoryProperties();
			if (wfProps == null) {
				return new String[0];
			}
			return wfProps.getSupportedDialectArray();

		} catch (Exception e) {
			e.printStackTrace();

		}

		return new String[0];
	}

	public Set<String> getSupportedDialects() {
		Set<String> dialects = new HashSet<String>();
		String[] supportedDialects = getSupportedDialectArray();
		for (String string : supportedDialects) {
			dialects.add(string);
		}
		return dialects;
	}

	/**
	 * Returns a tracer node for accessing the tracing service that is used by
	 * the workflow system.
	 * 
	 * @return
	 */
	public TracerNode getTracerNode() throws TracerUnresolvableException {

		Set<String> types = new HashSet<String>();
		types.add(TracerNode.TYPE);
		List<Node> tracers = null;
		try {
			tracers = getRegistryNode().getChildrenOfTypes(types);
		} catch (Exception e) {
			throw new TracerUnresolvableException(e);
		}
		if (tracers == null || tracers.size() == 0) {
			throw new TracerUnresolvableException(
					"No tracers in registry. Don't know which one to use.");
		} else if (tracers.size() == 1) {
			return (TracerNode) tracers.get(0);
		} else if (tracers.size() > 1) {
			throw new TracerUnresolvableException(
					"Too many tracers in registry. Don't know which one to use.");
		}


		return null;
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	public WorkflowFactoryProperties getWorkflowFactoryProperties() {
		if (!hasCachedResourceProperties()) {
			return null;
		}
		WorkflowFactoryPropertiesDocument doc = (WorkflowFactoryPropertiesDocument) getCachedResourcePropertyDocument();
		return doc.getWorkflowFactoryProperties();
	}

	public GroupNode getWorkflowsNode() {
		String uri = getWorkflowsNodeURI();
		for (Node n : getChildrenArray()) {
			if (n instanceof GroupNode) {
				if (n.getURI().toString().equals(uri)) {
					return (GroupNode) n;
				}
			}
		}
		return null;
	}

	protected String getWorkflowsNodeURI() {
		return getURI().toString() + "/workflows";
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		super.removePropertyChangeListener(listener);
	}

	/**
	 * create the child nodes of this node
	 */
	@Override
	protected void retrieveChildren() throws Exception {
		INodeData data = getData();
		if (data == null) {
			return;
		}
		List<Node> newChildren = new ArrayList<Node>();
		synchronized (data) {

			if (hasCachedResourceProperties()) {
				// workflows
				GroupNode lazyNode = getWorkflowsNode();
				if (lazyNode == null) {
					lazyNode = createWorkflowsNode();
				}
				// make sure to set the name right even if the "Workflows" node
				// has lost it's properties due to cache corruption
				lazyNode.setNameForAllCopies("Workflows");

				// same for child type
				lazyNode.setChildType(WorkflowNode.TYPE);
				newChildren.add(lazyNode);

			}

			updateChildren(newChildren);
		}
	}

	public List<Node> retrieveChildren(ILazyParent parent, long offset,
			long windowSize, IProgressMonitor progress) {
		List<Node> result = new ArrayList<Node>();
		if (windowSize == 0) {
			return result;
		}
		boolean propertiesUpdated = false;
		if (!hasCachedResourceProperties()) {
			retrieveProperties(null);
			propertiesUpdated = true;
		}
		WorkflowFactoryProperties props = getWorkflowFactoryProperties();
		if (props == null) {
			return result;
		}
		getWorkflowsNode();


		EnumerationClient<XmlObject> enumClient = null;
		try {
			enumClient = getEnumerationClient(props);
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.WARNING,
					"Unable to retrieve workflows for workflow engine " + getNamePathToRoot(), e);
		}

		boolean oldOrBuggyServer = false;

		if (enumClient != null) {
			try {
				long total = enumClient.getNumberOfResults();
				if (total > 0) {
					long start = Math.max(0, total - offset - windowSize);

					// make sure window size is not too large
					windowSize = Math.min(total - offset, windowSize); 

					List<XmlObject> refs = enumClient
							.getResults(start, windowSize);
					// get jobs in reverse order as they appear chronologically
					// in the enum
					for (int i = refs.size() - 1; i >= 0; i--) {
						AccessibleWorkflowReferenceDocument ref = 
								AccessibleWorkflowReferenceDocument.Factory.parse(refs.get(i).newXMLStreamReader());
						EndpointReferenceType epr = ref.getAccessibleWorkflowReference();
						Node n = NodeFactory.createNode(epr);
						result.add(n);
					}
				}
				else{
					// buggy 6.4.x server: accessible wf enumeration shows no results
					oldOrBuggyServer = true;
				}
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.WARNING,
						"Unable to retrieve workflows for workflow engine "
								+ getNamePathToRoot() + ": " + e.getMessage(),
								e);
			}

		}

		if(oldOrBuggyServer) {
			// old server
			if (!propertiesUpdated) {
				retrieveProperties(null);
			}

			WorkflowFactoryProperties wfProps = getWorkflowFactoryProperties();
			if(wfProps == null)
			{
				setState(STATE_FAILED);
				setFailReason("Unable to refresh resource properties.");
			}
			else {
				EndpointReferenceType[] submittedWorkflows = wfProps
						.getAccessibleWorkflowReferenceArray();
				int length=submittedWorkflows.length;
				
				// want to reverse order because latest WF should be on top
				if (length > 0) {
					long start = length - 1 - offset;
					long end = Math.max(0, start - windowSize + 1);
					for (long i = start; i >= end; i--) {
						int index = (int) i;
						EndpointReferenceType epr = submittedWorkflows[index];
						try {
							Node node = NodeFactory.createNode(epr);
							result.add(node);
						} catch (Exception e) {
						}
					}
				}
			}
		}
		return result;
	}

	@Override
	public void retrieveName() {
		URI uri = getURI();
		if (uri != null) {

			setName("Workflow engine @"
					+ URIUtils.extractUnicoreServiceContainerName(uri));
		} else {
			super.retrieveName();
		}
	}
}
