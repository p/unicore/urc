/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.servicebrowser.nodes;

import java.net.URI;

import org.chemomentum.common.ws.ILocationManager;
import org.chemomentum.rcp.servicebrowser.actions.LMResolveLogicalNameAction;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.servicebrowser.nodes.WebServiceNode;
import de.fzj.unicore.rcp.servicebrowser.utils.URIUtils;

/**
 * @author demuth
 * 
 */
public class LocationManagerNode extends WebServiceNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8984141678787524685L;
	public static final String TYPE = ILocationManager.serviceName;

	public LocationManagerNode(EndpointReferenceType epr) {
		super(epr);

	}

	/**
	 * Integer value for sorting nodes in views. Nodes are usually sorted in
	 * ascending order.
	 * 
	 * @return
	 */
	@Override
	public int getCategory() {
		return 1200;
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		availableActions.add(new LMResolveLogicalNameAction(this));
	}

	@Override
	public void retrieveName() {
		URI uri = getURI();
		if (uri != null) {

			setName("Location manager @"
					+ URIUtils.extractUnicoreServiceContainerName(uri));
		} else {
			super.retrieveName();
		}
	}
}
