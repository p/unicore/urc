/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.servicebrowser.nodes;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;
import org.chemomentum.common.impl.workflow.WorkflowManagementClient;
import org.chemomentum.common.ws.WorkflowManagement;
import org.chemomentum.rcp.servicebrowser.Activator;
import org.chemomentum.workflow.xmlbeans.JobReferenceDocument;
import org.chemomentum.workflow.xmlbeans.StatusType;
import org.chemomentum.workflow.xmlbeans.WorkflowResourcePropertiesDocument;
import org.chemomentum.workflow.xmlbeans.WorkflowResourcePropertiesDocument.WorkflowResourceProperties;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;

import org.unigrids.services.atomic.types.GridFileType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.nodes.AbstractFileNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeData;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.StorageNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.ILazyParent;
import de.fzj.unicore.rcp.servicebrowser.nodes.lazy.MoreNode;
import de.fzj.unicore.rcp.servicebrowser.utils.URIUtils;
import de.fzj.unicore.uas.client.EnumerationClient;
import de.fzj.unicore.uas.client.StorageClient;

/**
 * @author demuth, rajveer
 * 
 */
public class WorkflowNode extends WSRFNode implements ILazyParent {

	private static final long serialVersionUID = -5672988721756465564L;

	public static final String PROPERTY_SUBMISSION_TIME = "submission time";
	public static final String PROPERTY_WORKFLOW_STATUS = "workflow status";

	public static final String PROPERTY_CURRENT_CHILD_INDEX = "current child index";

	public static final String TYPE = WorkflowManagement.SERVICE_NAME;

	private static Map<String, String> parameters = new HashMap<String, String>();

	public WorkflowNode(EndpointReferenceType epr) {
		super(epr);
	}

	public void abortWorkflow() throws Exception {
		WorkflowManagementClient client = createWorkflowManagementClient();
		client.abort();
	}

	
	public void resumeWorkflow() throws Exception {
		WorkflowManagementClient client = createWorkflowManagementClient();
		
		// resumes the workflow with updated parameters
		client.resume(parameters);
	}


	// reads parameter if workflow contains any and returns in a map
	public Map<String, String> readParameters() throws Exception {
		WorkflowManagementClient client = createWorkflowManagementClient();
		// clear old mappings if any
		parameters.clear();
		for (Map.Entry<String, String> entryset : client.getParameters().entrySet()) {
			parameters.put(entryset.getKey(), entryset.getValue());
		}			
		return parameters;
	}

	// writes the updated parameters again to the static map
	public void writeUpdatedParameters(Map<String, String> newParameter ) {
		
		parameters.clear();
		for (Map.Entry<String, String> entryset : newParameter.entrySet()) {
			parameters.put(entryset.getKey(), entryset.getValue());
		}			
	}
	
	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
		long newCurrentChildIndex = getLimit() - 1;
		if (getCurrentChildIndex() > newCurrentChildIndex
				&& getMoreNode() == null) {
			addChild(createMoreNode());
		}
		setCurrentChildIndex(newCurrentChildIndex);

	}

	@Override
	public void beforeSerialization() {
		// make sure only the first "chunk" of children gets serialized
		int i = 0;
		for (Node n : getChildrenArray()) {
			if (MoreNode.TYPE.equals(n.getType())) {
				n.setPersistable(true);
			} else {
				n.setPersistable(i < getLimit());
			}
			i++;
		}
		super.beforeSerialization();
	}

	@Override
	public boolean canBeDestroyed() {
		return true;
	}

	@Override
	public int compareTo(Node n) {
		// sort jobs by submission time
		if (n instanceof WorkflowNode) {
			WorkflowNode other = (WorkflowNode) n;
			Calendar c1 = other.getNormalizedSubmissionTime();
			Calendar c2 = getNormalizedSubmissionTime();
			if (c1 != null && c2 != null) {
				return c1.compareTo(c2);
			} else {
				return 0; // don't change order when submission times are not
				// known (yet)
			}

		}
		return super.compareTo(n);
	}

	protected MoreNode createMoreNode() {
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue(getMoreNodeURI());
		Utils.addPortType(epr, MoreNode.PORTTYPE);
		MoreNode moreNode = (MoreNode) NodeFactory.createNode(epr);
		return moreNode;
	}

	public WorkflowManagementClient createWorkflowManagementClient()
			throws Exception {
		return new WorkflowManagementClient(getURL().toString(), getEpr(),
				getUASSecProps());
	}

	public long getCurrentChildIndex() {
		INodeData data = getData();
		if (data == null) {
			return -1;
		}
		Long result = (Long) data.getProperty(PROPERTY_CURRENT_CHILD_INDEX);
		if (result == null) {
			return -1;
		} else {
			return result;
		}
	}

	@Override
	public DetailsAdapter getDetails() {
		DetailsAdapter details = super.getDetails();
		Map<String, String> hm = details.getMap();
		StatusType.Enum status = getWorkflowStatus();
		if (status != null) {
			hm.put("Execution Status", status.toString());
		}
		if (hasCachedResourceProperties()) {
			try {
				hm.put("Submitted at",
						getDefaultDateFormat().format(getSubmissionTime().getTime()));
			} catch (Exception e) {
				// do nothing: server does not provide submission time
			}

		}
		return details;

	}

	// TODO we use XmlObject here instead of the correct
	// JobReferenceDocument due to a ClassCastException at runtime.
	// The classloader of EnumClient does not know the WF classes?!
	private EnumerationClient<XmlObject> getEnumerationClient(
			WorkflowResourceProperties props) throws Exception {
		EnumerationClient<XmlObject> enumClient=null;
		if (props != null && props.isSetJobReferenceEnumeration()
				&& props.getJobReferenceEnumeration() != null) {
			enumClient = new EnumerationClient<XmlObject>(
					props.getJobReferenceEnumeration(), getUASSecProps(),
					JobReferenceDocument.type.getDocumentElementName());
		}

		return enumClient;
	}

	protected int getLimit() {
		return ServiceBrowserActivator
				.getDefault()
				.getPreferenceStore()
				.getInt(ServiceBrowserConstants.P_CHUNK_SIZE_LAZY_CHILD_RETRIEVAL);
	}

	public MoreNode getMoreNode() {
		for (Node n : getChildrenArray()) {
			if (n instanceof MoreNode) {
				return (MoreNode) n;
			}
		}
		return null; // not found
	}

	protected String getMoreNodeURI() {
		return getURI().toString() + "/more";
	}

	public Calendar getNormalizedSubmissionTime() {
		return getNormalizedTime(getSubmissionTime());
	}

	public long getNumLazyChildren(IProgressMonitor progress) {
		if (!hasCachedResourceProperties()) {
			retrieveProperties(progress);
		}
		WorkflowResourceProperties props = getWorkflowProperties();
		if (props == null) {
			return -1;
		}
		EnumerationClient<XmlObject> enumClient = null;
		try {
			enumClient = getEnumerationClient(props);
			if (enumClient != null) {
				return enumClient.getNumberOfResults();
			} else {
				if (props.getJobReferenceArray() != null) {
					return props.getJobReferenceArray().length;
				} else {
					return -1;
				}
			}
		} catch (Exception e) {
			// TODO The e.getMessage part should not be needed
			ServiceBrowserActivator.log(IStatus.WARNING,
					"Unable to retrieve number of jobs for " + getNamePathToRoot(), e);
			return -1;
		}
	}

	@Override
	public SchemaType getSchemaType() {
		return WorkflowResourcePropertiesDocument.type;
	}

	/**
	 * 
	 * @return the submission time of this job
	 */
	public Calendar getSubmissionTime() {
		if (getData() == null) {
			return null;
		}
		Object o = getData().getProperty(PROPERTY_SUBMISSION_TIME);
		if (o != null && o instanceof Calendar) {
			return (Calendar) o;
		} else {
			return null;
		}

	}

	public EndpointReferenceType getTracerAddress() {
		if (getData() == null || getWorkflowProperties() == null) {
			return null;
		}
		return getWorkflowProperties().getTracerAddress();

	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return TYPE;
	}

	public String getWorkflowId() {
		return getURI().getQuery().replace("res=", "");
	}

	protected WorkflowResourceProperties getWorkflowProperties() {
		if (!hasCachedResourceProperties()) {
			return null;
		}
		WorkflowResourcePropertiesDocument doc = (WorkflowResourcePropertiesDocument) getCachedResourcePropertyDocument();
		return doc.getWorkflowResourceProperties();
	}

	public StatusType.Enum getWorkflowStatus() {
		if (getData() == null) {
			return null;
		}
		Object o = getData().getProperty(PROPERTY_WORKFLOW_STATUS);
		if (o != null) {
			try {
				return StatusType.Enum.forString((String) o);
			} catch (Exception e) {
				// do nothing
			}
		}
		return null;
	}

	public IStatus lazilyRetrieveAllChildren(IProgressMonitor progress) {
		progress = ProgressUtils.getNonNullMonitorFor(progress);
		progress.beginTask("", 2);
		try {
			long max = getNumLazyChildren(new SubProgressMonitor(progress, 1));
			return lazilyRetrieveChildrenUpToIndex(max - 1,
					new SubProgressMonitor(progress, 1));
		} finally {
			progress.done();
		}
	}

	public IStatus lazilyRetrieveChildrenUpTo(URI uri, IProgressMonitor progress) {
		String toFind = URIUtils.toNormalizedString(uri);
		updateChildrenFromData();

		int current;
		Node[] children = getChildrenArray();
		for (current = 0; current < children.length; current++) {
			URI childURI = children[current].getURI();
			String compare = URIUtils.toNormalizedString(childURI);
			if (toFind.equals(compare)) {
				return Status.OK_STATUS;
			}
		}
		IStatus s = lazilyRetrieveNextChildren(null);

		while (s.isOK() && getMoreNode() != null) {
			current = 0;
			children = getChildrenArray();
			while (current < children.length) {
				URI childURI = children[current].getURI();
				String compare = URIUtils.toNormalizedString(childURI);
				if (toFind.equals(compare)) {
					return Status.OK_STATUS;
				}
				current++;
			}
			s = lazilyRetrieveNextChildren(progress);
		}
		return s;
	}

	public IStatus lazilyRetrieveChildrenUpToIndex(long index,
			IProgressMonitor progress) {
		INodeData data = getData();
		if (data == null) {
			return Status.CANCEL_STATUS;
		}
		data.setProperty(PROPERTY_CURRENT_CHILD_INDEX, index, this);
		IStatus status = refresh(1, false, progress);
		if (status.isOK()) {
			List<Node> children = getChildren();
			for (Node n : children) {
				if (n.getState() == STATE_NEW) {
					n.refresh(0, false, null);
				}
			}
			setChildren(sortChildren(children));
			return Status.OK_STATUS;
		} else {
			return status;
		}

	}

	public IStatus lazilyRetrieveNextChildren(IProgressMonitor progress) {
		progress = ProgressUtils.getNonNullMonitorFor(progress);
		progress.beginTask("", 5);
		try {
			long offset = getCurrentChildIndex();

			long max = getNumLazyChildren(new SubProgressMonitor(progress, 1));
			if (max < 0) {
				return Status.CANCEL_STATUS;
			}
			offset += getLimit();
			if (offset > max) {
				offset = max;
			}

			return lazilyRetrieveChildrenUpToIndex(offset,
					new SubProgressMonitor(progress, 4));
		} finally {
			progress.done();
		}
	}


	/**
	 * create the child nodes of this node
	 */
	@Override
	protected void retrieveChildren() throws Exception {
		List<Node> newChildren = new ArrayList<Node>();
		if (hasCachedResourceProperties()) {
			WorkflowResourceProperties workflowProps = getWorkflowProperties();
			INodeData data = getData();
			if (data == null) {
				return;
			}
			synchronized (data) {
				long limit = getLimit();

				MoreNode moreNode = getMoreNode();
				long current = getCurrentChildIndex();

				if (current < limit - 1) {

					current = limit - 1;
					putData(PROPERTY_CURRENT_CHILD_INDEX, current);
				}

				long max = getNumLazyChildren(null);
				if (max > current + 1) {
					if (moreNode == null) {
						moreNode = createMoreNode();
					}
					newChildren.add(moreNode);
				} else if (max < current) {
					current = max;
				}

				long numSteps = (long) Math
						.ceil(((double) current + 1) / limit);
				for (long i = 0; i < numSteps; i++) {
					long num = Math.min(limit, current + 1 - i * limit);

					newChildren.addAll(retrieveChildren(i * limit, num, null));
				}

				EndpointReferenceType storageEpr = workflowProps
						.getStorageEPR();
				if (storageEpr == null) // old workflow engine
				{
					storageEpr = workflowProps.getSubmittedWorkflow()
							.getStorageEPR();
				}
				// add working dir node
				try {
					String resId = getResourceId();
					Map<URI, Node> oldChildren = getChildrenMap();
					StorageNode storageNode = (StorageNode) NodeFactory
							.createNode(storageEpr);
					try {
						// use my security properties instead of the storage
						// node's because the storage might not be revealed
						// in the Grid browser
						StorageClient client = new StorageClient(
								storageEpr, getUASSecProps());
						GridFileType gft = client.listProperties(resId);
						AbstractFileNode folderNode = StorageNode
								.createFileNode(oldChildren, storageEpr,
										storageNode.getName(), gft);
						folderNode
						.setNameForAllCopies("working directory of "
								+ getName());
						folderNode.setPersistable(true);
						newChildren.add(folderNode);
					} finally {
						storageNode.dispose();
					}

				} catch (Exception e) {
				}
			}

		}
		updateChildren(newChildren);
	}

	protected List<Node> retrieveChildren(long offset, long windowSize,
			IProgressMonitor progress) {
		List<Node> result = new ArrayList<Node>();
		if (windowSize == 0) {
			return result;
		}
		boolean propertiesUpdated = false;
		if (!hasCachedResourceProperties()) {
			retrieveProperties(null);
			propertiesUpdated = true;
		}
		WorkflowResourceProperties props = getWorkflowProperties();
		if (props == null) {
			return result;
		}

		EnumerationClient<XmlObject> enumClient = null;
		try {
			enumClient = getEnumerationClient(props);
		} catch (Exception e) {
			ServiceBrowserActivator.log(IStatus.WARNING,
					"Unable to retrieve jobs for workflow " + getNamePathToRoot(), e);
		}
		if (enumClient != null) {
			try {
				long total = enumClient.getNumberOfResults();
				if (total > 0) {
					long start = Math.max(0, total - offset - windowSize);
					// make sure window size is not too large
					windowSize = Math.min(total - offset, windowSize); 
					List<XmlObject> refs = enumClient.getResults(start,
							windowSize);
					// get jobs in reverse order as they appear chronologically
					// in the enum
					for (int i = refs.size() - 1; i >= 0; i--) {
						JobReferenceDocument r=JobReferenceDocument.Factory.parse(refs.get(i).newXMLStreamReader());
						Node n = NodeFactory.createNode(r.getJobReference());
						result.add(n);
					}
				}
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.WARNING,
						"Unable to retrieve jobs for workflow "
								+ getNamePathToRoot() + ": " + e.getMessage(),
								e);
			}

		} else {
			// old server
			if (!propertiesUpdated) {
				retrieveProperties(null);
			}

			EndpointReferenceType[] jobEprs = props.getJobReferenceArray();
			if (jobEprs.length > 0) {
				long start = jobEprs.length - 1 - offset;
				long end = Math.max(0, start - windowSize + 1);
				for (long i = start; i >= end; i--) {
					int index = (int) i;
					EndpointReferenceType jobEpr = jobEprs[index];
					try {
						Node node = NodeFactory.createNode(jobEpr);
						result.add(node);

					} catch (Exception e) {
					}
				}
			}
		}
		return result;
	}

	@Override
	public void retrieveName() {

		if (hasCachedResourceProperties()) {
			try {
				WorkflowResourceProperties workflowProps = getWorkflowProperties();
				String wfName = null;
				try {
					wfName = workflowProps.getWorkflowName();
				} catch (NullPointerException e) {
					// do nothing
				}
				if (wfName != null) {
					setName(wfName);
					return;
				}
			} catch (Exception e) {
				ServiceBrowserActivator.log(IStatus.WARNING,
						"Error while setting name of a workflow correctly", e);
			}
		}

		super.retrieveName();
	}

	@Override
	public int retrieveProperties(IProgressMonitor progressMonitor) {
		int result = super.retrieveProperties(progressMonitor);
		if (hasCachedResourceProperties()) {
			WorkflowResourceProperties props = getWorkflowProperties();

			try {

				setWorkflowStatus(props.getWorkflowStatus());
			} catch (Exception e) {
				Activator.log(IStatus.WARNING,
						"Error while retrieving workflow status", e);
			}
			try {
				// store the submission time
				// do not use the xmlbeans Calendar instance! it cannot be
				// deserialized by xstream.
				Calendar cal1 = Calendar.getInstance();
				long serverSubmissionTime = props.getSubmissionTime()
						.getTimeInMillis();
				cal1.setTimeInMillis(serverSubmissionTime);
				setSubmissionTime(cal1);
			} catch (Exception e) {
				// do nothing
			}

		}
		return result;
	}

	protected void setCurrentChildIndex(long index) {
		putData(PROPERTY_CURRENT_CHILD_INDEX, index);
	}

	public void setSubmissionTime(Calendar submissionTime) {
		putData(PROPERTY_SUBMISSION_TIME, submissionTime);

	}

	public void setWorkflowStatus(StatusType.Enum status) {
		putData(PROPERTY_WORKFLOW_STATUS, status.toString());
	}

}
