package de.fzj.unicore.rcp.terminal.ssh.plain;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.terminal.BasicConfigUIBuilder;
import de.fzj.unicore.rcp.terminal.ui.tableviewer.model.TerminalSite;

/**
 * Manages the UI of Plain SSH provider
 * 
 * @author demuth, André Giesler
 */
public class PlainSSHConfigUIBuilder extends BasicConfigUIBuilder implements PlainSSHConstants {

	String path = "";
	String type = "";

	Button passwordButton, interactiveButton, publickeyButton, fileDialogButton;
	Text pubkeyText;
	Label pubkeylabel;
	Group group;
	List<Control> controls = new ArrayList<Control>();

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	protected String[] getRelevantConfigKeys() {
		return new String[]{PLAIN_HOST,PLAIN_PORT,PLAIN_LOGIN};
	}

	protected String[] getRelevantKeyConfigKeys() {
		return new String[]{PLAIN_TYPE,PLAIN_KEY};
	}


	@Override
	protected String getLabelForKey(String key) {
		if(key.equals(PLAIN_HOST)){
			return "Host";
		}
		else if(key.equals(PLAIN_PORT)){
			return "Port";
		}
		else if(key.equals(PLAIN_LOGIN)){
			return "Login";
		}
		else if(key.equals(PLAIN_TYPE)){
			return "Auth-Type";
		}
		else if(key.equals(PLAIN_KEY)){
			return "Private-Key-File";
		}
		else if(key.equals(PLAIN_TYPE_PASS)){
			return "Password";
		}
		else if(key.equals(PLAIN_TYPE_PUBKEY)){
			return "Public-Key";
		}
		else if(key.equals(PLAIN_TYPE_INTERACTIVE)){
			return "Keyboard-Interactive";
		}
		return key;
	}

	@Override
	public String validateInput(Composite parent, TerminalSite site)
	{
		//super.validateInput(parent, site);
		for(String key : getTextFields().keySet()){
			boolean dirty = true;
			Text text = getTextFields().get(key);
			if(key.equals(PLAIN_KEY)){// || key.equals(PLAIN_LOGIN)){// && type.equals(PLAIN_TYPE_PASS)){
				dirty=false;
			}
			if (dirty && text.getText().length() == 0){// && type.equals(PLAIN_TYPE_PUBKEY)) {
				return "Please check input at field "+getLabelForKey(key);
			}
			if (key.equals(PLAIN_PORT)) {
				try {
					Integer.parseInt(text.getText());
				} catch (Exception e) {
					text.setFocus();
					text.selectAll();
					return "Input is not an integer number at field " +getLabelForKey(key) +
					" at tab " + CONNECTION_TYPE_NAME_PLAIN;
				}
			}
		}

		//Validating checkboxes
		if(!this.passwordButton.getSelection() && !this.interactiveButton.getSelection() && 
				!this.publickeyButton.getSelection()){
			String msg = "Please select one of the provided authentication methods: ";
			for(int i = 0; i<PlainSSHConstants.PLAIN_TYPES.length;i++){
				if(i>0)msg += ", ";
				msg += getLabelForKey(PlainSSHConstants.PLAIN_TYPES[i]);
			}
			return msg;
		}	

		//		It's not required to select private key, since the plugin tries 
		//		to find it automatically 
		//		if(this.publickeyButton.getSelection()){
		//			String keypath = "";
		//			Control[] controls = parent.getChildren();
		//			for(Control c : controls){
		//				if(c instanceof Composite){
		//					Composite comp = (Composite)c;
		//					Control[] controls2 = comp.getChildren();
		//					for(Control c2 : controls2){
		//						if(c2 instanceof Group){
		//							Group group = (Group)c2;
		//							Control[] controls3 = group.getChildren();
		//							for(Control c3 : controls3){
		//								if(c3 instanceof Text){
		//									Text text = (Text)c3;
		//									keypath = text.getText();
		//									break;
		//								}
		//							}
		//						}			
		//					}			
		//				}
		//			}
		//			if(keypath.equals("")){
		//				return "Please specify a valid private key path";
		//			}
		//			else{
		//				File file = new File(keypath);
		//				if(!file.exists()){
		//					return "Please specify a valid private key file. File '" + keypath + "' does not exist.";
		//				}
		//			}
		//			
		//		}

		return null;
	}

	@Override
	public Map<String,String> getTooltipInfo(TerminalSite site) {

		Map<String,String> config = site.getConnectionTypeConfigs();

		Map<String,String> attributes = new HashMap<String,String>();
		for(String key : getRelevantConfigKeys())
		{
			String value = config.get(key) == null ? "" : config.get(key);
			attributes.put(getLabelForKey(key), value);
		}

		for(String key : getRelevantKeyConfigKeys())
		{
			String value = config.get(key) == null ? "" : config.get(key);
			attributes.put(getLabelForKey(key), value);
		}

		return attributes;

	}

	@Override
	public void buildUI(final Composite parent, TerminalSite site) {

		controls.clear();
		Map<String,String> config = site.getConnectionTypeConfigs();

		GridLayout layout = new GridLayout();
		layout.verticalSpacing = 10;
		layout.numColumns = 2;
		parent.setLayout(layout);

		for(String key : getRelevantConfigKeys())
		{
			Label label = new Label(parent, SWT.NONE);
			label.setSize(64, 32);
			label.setText(getLabelForKey(key));
			controls.add(label);

			final Text text = new Text(parent, SWT.BORDER);
			final GridData textGD = new GridData();
			text.setLayoutData(textGD);
			text.setSize(64, 32);
			String value = config.get(key) == null ? "" : config.get(key);
			text.setText(value);
			textFields.put(key, text);
			controls.add(text);
			final int minX = text.computeSize(-1, -1).x;
			text.addModifyListener(new ModifyListener() {			
				public void modifyText(ModifyEvent e) {
					Point p = text.computeSize(-1, -1);
					textGD.widthHint = Math.max(minX, p.x);
					parent.layout();			
				}
			});
		}

		GridData gridData = new GridData();
		gridData.horizontalSpan = 2;

		group = new Group(parent, SWT.SHADOW_ETCHED_IN);	
		group.setLayout(new GridLayout(3, false));
		group.setText("Authentication");
		controls.add(group);
	    GridData groupGridData = new GridData();
		groupGridData.horizontalSpan = 2;
	    group.setLayoutData(groupGridData);

		Listener radioGroup = new Listener () {
			public void handleEvent (Event event) {		          
				Control [] children = group.getChildren ();
				for (int j=0; j<children.length; j++) {
					Control child = children [j];
					if (child instanceof Button) {
						Button button = (Button) child;
						if ((button.getStyle () & SWT.RADIO) != 0) button.setSelection (false);
					}
				}

				Button button = (Button) event.widget;
				button.setSelection (true);
				if(button.getText().equals(getLabelForKey(PlainSSHConstants.PLAIN_TYPE_PASS))){
					//type.getConnectionTypeConfigs().put(PLAIN_TYPE, button.getText());
					type = PlainSSHConstants.PLAIN_TYPE_PASS;
					Control[] controls = group.getChildren();
					enableControls(controls, false, false, true);
				}
				else if(button.getText().equals(getLabelForKey(PlainSSHConstants.PLAIN_TYPE_INTERACTIVE))){
					//type.getConnectionTypeConfigs().put(PLAIN_TYPE, button.getText());
					type = PlainSSHConstants.PLAIN_TYPE_INTERACTIVE;
					Control[] controls = group.getChildren();
					enableControls(controls, false, false, true);
				}
				else {
					type = PlainSSHConstants.PLAIN_TYPE_PUBKEY;
					Control[] controls = group.getChildren();
					enableControls(controls, true, false, true);
				}
			}
		};

		gridData = new GridData();
		passwordButton = new Button(group, SWT.RADIO);
		passwordButton.setText(getLabelForKey(PlainSSHConstants.PLAIN_TYPE_PASS));
		passwordButton.setBounds(10, 5, 75, 30);
		passwordButton.addListener(SWT.Selection, radioGroup);
		gridData.horizontalSpan = 3;
		gridData.horizontalAlignment = SWT.FILL;
		passwordButton.setLayoutData(gridData);
		controls.add(passwordButton);

		interactiveButton = new Button(group, SWT.RADIO);
		interactiveButton.setText(getLabelForKey(PlainSSHConstants.PLAIN_TYPE_INTERACTIVE));
		interactiveButton.setBounds(10, 5, 75, 30);
		interactiveButton.addListener(SWT.Selection, radioGroup);
		interactiveButton.setLayoutData(gridData);
		controls.add(interactiveButton);

		publickeyButton = new Button(group, SWT.RADIO);
		publickeyButton.setText(getLabelForKey(PlainSSHConstants.PLAIN_TYPE_PUBKEY));
		publickeyButton.setBounds(10, 5, 75, 30);
		publickeyButton.addListener(SWT.Selection, radioGroup);
		publickeyButton.setLayoutData(gridData);
		controls.add(publickeyButton);

		boolean dirty = false;
		if(config.get(PLAIN_TYPE) == null || config.get(PLAIN_TYPE).equals("")){
			type = "";
		}
		else if(config.get(PLAIN_TYPE).equals(PLAIN_TYPE_PASS)){
			type = PLAIN_TYPE_PASS;
			passwordButton.setSelection(true);
		}
		else if(config.get(PLAIN_TYPE).equals(PLAIN_TYPE_INTERACTIVE)){
			type = PLAIN_TYPE_INTERACTIVE;
			interactiveButton.setSelection(true);
		}
		else if(config.get(PLAIN_TYPE).equals(PLAIN_TYPE_PUBKEY)){
			type = PLAIN_TYPE_PUBKEY;
			publickeyButton.setSelection(true);
			dirty = true;
		}
		else type = "";

		pubkeylabel = new Label(group, SWT.NONE);
		pubkeylabel.setSize(64, 32);
		pubkeylabel.setText(getLabelForKey(PLAIN_KEY));
		pubkeylabel.setEnabled(dirty);
		controls.add(pubkeylabel);
		
		pubkeyText = new Text(group, SWT.BORDER);
		pubkeyText.setSize(128, 32);
		String path = config.get(PLAIN_KEY) == null ? "" : config.get(PLAIN_KEY);
		pubkeyText.setText(path);
		pubkeyText.setToolTipText(pubkeyText.getText());
		pubkeyText.setEnabled(dirty);
		final GridData textGD = new GridData();
		pubkeyText.setLayoutData(textGD);
		final int minX = pubkeyText.computeSize(-1, -1).x;
		pubkeyText.addModifyListener(new ModifyListener() {			
			public void modifyText(ModifyEvent e) {
				Point p = pubkeyText.computeSize(-1, -1);
				textGD.widthHint = Math.max(minX, p.x);
				parent.layout();			
			}
		});
		controls.add(pubkeyText);
		
		gridData = new GridData();

		textFields.put(PLAIN_KEY, pubkeyText);

		fileDialogButton = new Button(group, SWT.PUSH);
		fileDialogButton.setText("...");
		fileDialogButton.setBounds(10, 5, 45, 30);
		fileDialogButton.setEnabled(dirty);
		fileDialogButton.addSelectionListener(new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
			}

			public void widgetSelected(SelectionEvent e) {
				if(promptPrivateKey(pubkeyText.getText())){
					pubkeyText.setText(getPath());
				}
			}		
		});
		controls.add(fileDialogButton);

	}

	private void enableControls(Control[] controls, boolean enable, boolean all, boolean radios){
		for(Control c : controls){
			if(!(c instanceof Button)){
				c.setEnabled(enable);
			}
			else{
				Button cbutton = (Button) c;
				if (!((cbutton.getStyle () & SWT.RADIO) != 0)){
					c.setEnabled(enable);
				}
				else{
					if(all)c.setEnabled(radios);				  
				}
			}	        		  
		}
	}



	public boolean promptPrivateKey( final String message ) {
		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				FileDialog dialog = new FileDialog(new Shell(), SWT.OPEN);
				if(!message.equals("")) {
					File file = new File(message);
					if(file.exists()){
						dialog.setFilterPath(message);
					}
				}
				dialog.setText("Choose your Private Key(e.g. ~/.ssh/id_dsa)");
				String path = dialog.open();
				if (path != null && path.length()>0){
					setPath(path);
				}
				else setPath("");
			}
		} );
		return !isEmpty(getPath());
	}

	private boolean isEmpty(String s)
	{
		return s == null || s.trim().length() == 0;
	}

	public void applyConfigurationChanges(Composite parent, TerminalSite site) {
		for(String key : textFields.keySet())
		{
			String value = textFields.get(key).getText();
			site.getConnectionTypeConfigs().put(key, value);
		}
		site.getConnectionTypeConfigs().put(PLAIN_TYPE, type);
		String value = textFields.get(PLAIN_KEY).getText();
		site.getConnectionTypeConfigs().put(PLAIN_KEY, value);

		textFields.clear();

	}

	public void setEnabled(Composite parent, TerminalSite site, boolean enabled)
	{
		for(Control c : controls)
		{
			c.setEnabled(enabled);
		}
		boolean enable = true;
		if(type.equals(PLAIN_TYPE_PASS) || type.equals(PLAIN_TYPE_INTERACTIVE)){
			enabled = false;
		}
		pubkeylabel.setEnabled(enabled && enable);
		pubkeyText.setEnabled(enabled && enable);
		fileDialogButton.setEnabled(enabled && enable);
	}
}
