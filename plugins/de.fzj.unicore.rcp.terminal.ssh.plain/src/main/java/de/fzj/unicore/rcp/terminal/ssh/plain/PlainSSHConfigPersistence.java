package de.fzj.unicore.rcp.terminal.ssh.plain;

import java.util.Map;

import org.eclipse.ui.IMemento;

import de.fzj.unicore.rcp.terminal.ConfigPersistence;
import de.fzj.unicore.rcp.terminal.ConnectionTypeRegistry;

/**
 * Manages persistence of Plain SSH provider
 * 
 * @author demuth
 * @version $Id$
 * 
 */
public class PlainSSHConfigPersistence extends ConfigPersistence implements PlainSSHConstants {

	private static ConfigPersistence instance;
	
	public static ConfigPersistence getInstance()
	{
		if(instance == null)
		{
			instance = new PlainSSHConfigPersistence();
		}
		return instance;
	}
	
	@Override
	public boolean readConfig(Map<String, String> config, IMemento memento) {
		
		IMemento plain = getConnectionTypeMemento(CONNECTION_TYPE_ID_PLAIN,memento); 
		if(plain == null) return false;
		String sshHost = plain.getString(PLAIN_HOST);
		String sshPort = plain.getString(PLAIN_PORT);
		String sshLogin = plain.getString(PLAIN_LOGIN);
		String sshType = plain.getString(PLAIN_TYPE);
		String sshKey = plain.getString(PLAIN_KEY);
		String supportedString = memento.getString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED);
		Boolean supported = supportedString != null && supportedString.contains(CONNECTION_TYPE_ID_PLAIN);
		config.put(PLAIN_HOST,sshHost); 
		config.put(PLAIN_PORT,sshPort);
		config.put(PLAIN_LOGIN,sshLogin);
		config.put(PLAIN_TYPE,sshType);
		config.put(PLAIN_KEY,sshKey);
		config.put(PLAIN_SUPPORTED, supported.toString());
		return true;
	}
	
	@Override
	public boolean writeConfig(Map<String, String> config,
			IMemento memento) {

		String sshHost = config.get(PLAIN_HOST); 
		String sshPort = config.get(PLAIN_PORT);
		String sshLogin = config.get(PLAIN_LOGIN);
		String sshType = config.get(PLAIN_TYPE);
		String sshKey = config.get(PLAIN_KEY);
		String supported = config.get(PLAIN_SUPPORTED);
		String supportedString = memento.getString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED);
		if(supportedString == null) supportedString = "";
		if(Boolean.parseBoolean(supported))
		{
			if(!supportedString.contains(CONNECTION_TYPE_ID_PLAIN))
			{
				if(supportedString.length() > 0) supportedString += " ";
				supportedString += CONNECTION_TYPE_ID_PLAIN;
			}
		}
		else
		{
			if(supportedString.contains(CONNECTION_TYPE_ID_PLAIN))
			{
				supportedString.replaceFirst("\\s*"+CONNECTION_TYPE_ID_PLAIN, "");
			}
		}
		memento.putString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED, supportedString);
		
		IMemento connectionType = getConnectionTypeMemento(CONNECTION_TYPE_ID_PLAIN,memento);
		if(connectionType == null) connectionType = createConnectionTypeMemento(CONNECTION_TYPE_ID_PLAIN,memento);
		if(sshHost != null) connectionType.putString(PLAIN_HOST,sshHost);
		if(sshLogin != null) connectionType.putString(PLAIN_LOGIN,sshLogin);
		if(sshPort != null) connectionType.putString(PLAIN_PORT,sshPort);
		if(sshType != null) connectionType.putString(PLAIN_TYPE,sshType);
		if(sshKey != null) connectionType.putString(PLAIN_KEY,sshKey);
		connectionType.putString(ATTRIBUTE_CONNECTION_TYPE_NAME, ConnectionTypeRegistry.getInstance().getNameForId(CONNECTION_TYPE_ID_PLAIN));
		return true;

	}

	

}
