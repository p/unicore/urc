package de.fzj.unicore.rcp.terminal.ssh.plain.problems;

import de.fzj.unicore.rcp.logmonitor.problems.ShowHelpPageSolution;

public class ShowInvalidKeyHelpPageSolution extends ShowHelpPageSolution {
	
	
	
	public ShowInvalidKeyHelpPageSolution() {
		super("JSCH_INVALID_PRIVATE_KEY_GEN","JSCH_INVALID_PRIVATE_KEY","Read about unsupported private key formats");
		
	}

	

	@Override
	protected String getHelpPageRef() {
		return "/org.chemomentum.rcp.help.basic/RichClient.html#SUBSUBSECTION_INVALID_KEY_FOR_SSH";
	}

}
