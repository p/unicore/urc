/*****************************************************************************
 * Copyright (c) 2006, 2007 g-Eclipse Consortium
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for the
 * g-Eclipse project founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributors:
 *    Thomas Koeckerbauer GUP, JKU - initial API and implementation
 *****************************************************************************/

package de.fzj.unicore.rcp.terminal.ssh.plain;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;

import de.fzj.unicore.rcp.terminal.TerminalConstants;
import de.fzj.unicore.rcp.terminal.UnicoreTerminalPlugin;
import de.fzj.unicore.rcp.terminal.ui.views.TerminalConfigView;

/**
 * Manages the SSH connection information
 * 
 * @author Andre Giesler
 * @version $Id$
 * 
 */

class SSHConnectionInfo  implements UserInfo, UIKeyboardInteractive, PlainSSHConstants 
 {

	private String siteId;
	private String siteName;
	private String defaultMethod;
	private Map<String,String> config;

	private String passphrase;
	private String password;

	private String[] keyboardInteractiveResult;

	private Shell activeShell = null;

	public Map<String, String> getConfig() {
		return config;
	}

	public String getAuthType() {
		return config.get(PLAIN_TYPE);
	}

	public void setAuthType(String authType) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(authType == null) newConfig.remove(PLAIN_TYPE);
		else newConfig.put(PLAIN_TYPE, authType);
		setConfig(newConfig);
	}

	public String getPrivateKeyPath() {
		return config.get(PLAIN_KEY);
	}

	public void setPrivateKeyPath(String privateKeyPath) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(privateKeyPath == null) newConfig.remove(PLAIN_KEY);
		else newConfig.put(PLAIN_KEY, privateKeyPath);
		setConfig(newConfig);
	}

	SSHConnectionInfo(Map<String,String> config) {
		this.siteId = config.get(TerminalConstants.ID);
		this.siteName = config.get(TerminalConstants.NAME);
		this.defaultMethod = config.get(TerminalConstants.CONNECTION_TYPE);
		this.config = config;

		findCurrentShell();
		

	}

	private void findCurrentShell() {
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable()
		{
			public void run() {
				activeShell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
			} 

		});	

	}

	private boolean configChanged(Map<String,String> oldConfig, Map<String,String> newConfig)
	{
		if(config.size() != this.config.size()) return true;
		for(String key : newConfig.keySet())
		{
			String oldValue = oldConfig.get(key);
			String newValue = newConfig.get(key);
			boolean equal = newValue == null ? oldValue == null : newValue.equals(oldValue);
			if(!equal) return true;
		}
		return false;
	}

	public String getHostname() {
		return config.get(PLAIN_HOST);
	}

	public String getPassphrase() {
		return this.passphrase;
	}

	public String getPassword() {
		return this.password;
	}

	Integer getPort() {
		String s = config.get(PLAIN_PORT);
		if(s == null) return null;
		else return Integer.parseInt(s);
	}

	public String getUsername() {
		return config.get(PLAIN_LOGIN);
	}

	private boolean isEmpty(String s)
	{
		return s == null || s.trim().length() == 0;
	}

	//	public boolean promptPlainType( ) {
	//		Display.getDefault().syncExec( new Runnable() {
	//			public void run() {
	//				String configured = config.get(PLAIN_HOST);
	//				Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();
	//				
	//				if (shell == null) shell = new Shell();
	//				PlainTypeDialog dlg = new PlainTypeDialog(shell,
	//						"Plain SSH Authentication","Please, select the authentication method for host " + configured,"",null); 
	//				dlg.open();
	//				String value = dlg.getValue();
	//				if (value != null && value.length()>0){
	//					if(value.equals(PLAIN_TYPE_PASS)){
	//						setAuthType(PLAIN_TYPE_PASS);
	//					}
	//					else if(value.equals(PLAIN_TYPE_PUBKEY)){
	//						setAuthType(PLAIN_TYPE_PUBKEY);
	//					}
	//					else {
	//						setAuthType(null);
	//					}
	//				}			
	//			}
	//		} );
	//	return !isEmpty(config.get(PLAIN_TYPE));
	//}

	public boolean promptHost( ) {
		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				String configured = config.get(PLAIN_HOST);
				String hostname = configured == null ? Messages.SSHConnectionInfo_PromptHostName : configured;
				InputDialog dlg = new InputDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(),
						Messages.SSHConnectionInfo_TitleHostName,Messages.SSHConnectionInfo_EnterHostName,hostname,null); 
				dlg.open();
				if ( dlg.getReturnCode() == InputDialog.OK )
					setHostname(dlg.getValue());
				else setHostname(null);

			}
		} );
		return !isEmpty(config.get(PLAIN_HOST));
	}

	public String[] promptKeyboardInteractive( final String destination,
			final String name,
			final String instruction,
			final String[] prompt,
			final boolean[] echo ) {
		if(keyboardInteractiveResult!=null)return keyboardInteractiveResult;
		String[] result;		
		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				keyboardInteractiveResult = new String[prompt.length];
				for(int i = 0; i < prompt.length; i++){
					if(echo[i]){
						InputDialog dlg = new InputDialog(activeShell,Messages.SSHConnectionInfo_KeyboardInteractiveTitle,
								Messages.SSHConnectionInfo_KeyboardInteractiveParam + " '" + prompt[i], "", null);
						dlg.open();
						keyboardInteractiveResult[i] = dlg.getValue();
					}
					else{
						HiddenInputDialog dlg = new HiddenInputDialog(activeShell,Messages.SSHConnectionInfo_KeyboardInteractiveTitle,
								Messages.SSHConnectionInfo_KeyboardInteractiveParam + " '" + prompt[i] + "'", "", null);
						dlg.open();
						keyboardInteractiveResult[i] = dlg.getValue();
					}
				}					
			}
		} );
		result = this.keyboardInteractiveResult;
		return result;
	}


	public boolean promptPrivateKey( final String message ) {		
		String privateKey = config.get(PLAIN_KEY);
		if(privateKey != null && !privateKey.equals("")) {
			//this.setPrivateKeyPath(privateKeyPath);
			this.setPrivateKeyPath(this.config.get(PLAIN_KEY));
			return true;
		}
		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				FileDialog dialog = new FileDialog(activeShell, SWT.OPEN);
				dialog.setText(Messages.SSHConnectionInfo_SelectPublicKey);
				String newprivateKey = dialog.open();
				if (newprivateKey == null || newprivateKey.equals("")){	
					newprivateKey = "";
				}
				setPrivateKeyPath(newprivateKey);
			}
		} );
		return !isEmpty(config.get(PLAIN_KEY));
	}

	public boolean promptPassphrase( final String message ) {
		if(passphrase != null) return true;

		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				String msg = Messages.SSHConnectionInfo_EnterPublicKeyPassphrase;
				HiddenInputDialog dlg = new HiddenInputDialog(activeShell,
						Messages.SSHConnectionInfo_PublicKeyPassphrase,msg,"", null);
				dlg.open();
				if ( dlg.getReturnCode() == InputDialog.OK )
					SSHConnectionInfo.this.passphrase = dlg.getValue();
				else
					SSHConnectionInfo.this.passphrase = null;
			}
		} );
		return !isEmpty(passphrase);
	}


	public boolean promptPassword( final String message ) {
		if(password != null) return true;

		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				String msg = Messages.SSHConnectionInfo_PromptPassword;
				String username = getUsername();
				if(username != null) 
				{
					msg += " for username "+username;
				}
				HiddenInputDialog dlg = new HiddenInputDialog(activeShell,
						Messages.SSHConnectionInfo_TitlePassword,msg,"", null);
				dlg.setErrorMessage(msg);
				dlg.open();				    
				if ( dlg.getReturnCode() == InputDialog.OK )
					SSHConnectionInfo.this.password = dlg.getValue();
				else
					SSHConnectionInfo.this.password = null;
			}
		} );

		return !isEmpty(password);
	}

	public boolean promptPort( ) {
		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				String configured = config.get(PLAIN_PORT);
				String port = configured == null ? Messages.SSHConnectionInfo_DefaultPort : configured;
				InputDialog dlg = new InputDialog(activeShell,Messages.SSHConnectionInfo_port,
						Messages.SSHConnectionInfo_EnterPort,port,new IInputValidator() {

					public String isValid(String newText) {
						try {
							Integer.parseInt(newText);
							return null;
						} catch (Exception e) {
							return Messages.SSHConnectionInfo_NoIntegerPort;
						}
					}
				}); 
				dlg.open();
				if ( dlg.getReturnCode() == InputDialog.OK )
					setPort(Integer.parseInt(dlg.getValue()));
				else setPort(null);
			}
		} );
		return !isEmpty(config.get(PLAIN_HOST));
	}

	public boolean promptUsername( ) {
		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				String configured = config.get(PLAIN_LOGIN);
				String login = configured == null ? Messages.SSHConnectionInfo_PromptUserName : configured;
				Shell shell = activeShell;
				if (shell == null) shell = new Shell();
				InputDialog dlg = new InputDialog(shell,Messages.SSHConnectionInfo_userName,
						Messages.SSHConnectionInfo_EnterUserName,login,null); 
				dlg.open();
				String result = dlg.getValue();
				if ( dlg.getReturnCode() == InputDialog.OK && result.trim().length() > 0){
					setUsername(result);
				}						
				else setUsername(null);
			}
		} );
		return !isEmpty(config.get(PLAIN_LOGIN));
	}

	public boolean promptYesNo( final String str ) {
		final boolean[] result = { false };
		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				result[0] = MessageDialog.openQuestion( activeShell,
						Messages.SshShell_sshTerminal,
						str );
			}
		} );
		return result[0];
		//return false;
	}

	public void setConfig(Map<String, String> config) {
		if(configChanged(this.config, config))
		{
			password = null;
			passphrase = null;
			PlainSSHConfigPersistence.getInstance().writeConfig(siteId, siteName, defaultMethod, config);
			PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable()
			{
				public void run() {
					try {
						TerminalConfigView sshConfig = (TerminalConfigView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(TerminalConfigView.ID);
						if(sshConfig != null) sshConfig.refresh();
					} catch (Exception e) {
						UnicoreTerminalPlugin.log(Status.ERROR,"Unable to refresh terminal config view: "+e.getMessage(), e );
					}
				}
			});
		}
		this.config = config;

	}

	public void setHostname(String hostname) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(hostname == null) newConfig.remove(PLAIN_HOST);
		else newConfig.put(PLAIN_HOST, hostname);
		setConfig(newConfig);
	}

	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public void setPort(Integer port) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(port == null) newConfig.remove(PLAIN_PORT);
		else newConfig.put(PLAIN_PORT, port.toString());
		setConfig(newConfig);
	}

	public void setUsername(String username) {
		Map<String,String> newConfig = new HashMap<String, String>(this.config);
		if(username == null) newConfig.remove(PLAIN_LOGIN);
		else newConfig.put(PLAIN_LOGIN, username);
		setConfig(newConfig);
	}

	public void showMessage( final String message ) {
		if (message != null && message.trim().length() != 0) {
			Display.getDefault().syncExec( new Runnable() {
				public void run() {
					MessageDialog.openInformation( activeShell,
							Messages.SshShell_sshTerminal,
							message );
				}
			} );
		}
	}
}
