/*****************************************************************************
 * Copyright (c) 2006, 2007 g-Eclipse Consortium 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for the
 * g-Eclipse project founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributors:
 *    Thomas Koeckerbauer GUP, JKU - initial API and implementation
 *****************************************************************************/

package de.fzj.unicore.rcp.terminal.ssh.plain;

import org.eclipse.osgi.util.NLS;

/**
 * Returns the localised messages for this package.
 * 
 * @author demuth
 */
class Messages extends NLS {
  private static final String BUNDLE_NAME = "de.fzj.unicore.rcp.terminal.ssh.plain.messages"; //$NON-NLS-1$
  
  public static String SshShell_sshTerminal;
  public static String SSHConnectionInfo_userName;
  public static String SSHConnectionInfo_EnterUserName;
  public static String SSHConnectionInfo_PromptUserName;
  public static String SSHConnectionInfo_port;
  public static String SSHConnectionInfo_EnterPort;
  public static String SSHConnectionInfo_DefaultPort;
  public static String SSHConnectionInfo_NoIntegerPort;
  public static String SSHConnectionInfo_password;
  public static String SSHConnectionInfo_PromptPassword;
  public static String SSHConnectionInfo_TitlePassword;
  public static String SSHConnectionInfo_PublicKeyFailed;
  public static String SSHConnectionInfo_PublicKeyPassphrase;
  public static String SSHConnectionInfo_EnterPublicKeyPassphrase;
  public static String SSHConnectionInfo_SelectPublicKey;
  public static String SSHConnectionInfo_KeyboardInteractiveTitle;
  public static String SSHConnectionInfo_KeyboardInteractiveParam;
  public static String SSHConnectionInfo_TitleHostName;
  public static String SSHConnectionInfo_EnterHostName;
  public static String SSHConnectionInfo_PromptHostName;
  
  
  static {
     NLS.initializeMessages(BUNDLE_NAME, Messages.class);
  }


  
}
