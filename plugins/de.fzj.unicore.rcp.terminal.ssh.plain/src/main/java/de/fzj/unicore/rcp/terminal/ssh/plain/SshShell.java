/*****************************************************************************
 * Copyright (c) 2006, 2007 g-Eclipse Consortium 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for the
 * g-Eclipse project founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributors:
 *    Thomas Koeckerbauer GUP, JKU - initial API and implementation
 *****************************************************************************/

package de.fzj.unicore.rcp.terminal.ssh.plain;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.core.runtime.Status;
import org.eclipse.jsch.core.IJSchService;

import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import de.fzj.unicore.rcp.terminal.IBidirectionalConnection;
import de.fzj.unicore.rcp.terminal.ITerminalListener;


/**
 * A terminal factory which allows to open SSH connected terminals.
 * 
 * @author Andre Giesler
 * @version $Id$
 * 
 */

class SshShell implements  ITerminalListener {
	private static final String PREFERRED_AUTHENTICATION = "PreferredAuthentications";
	private static final String AUTHENTICATION_PUBLIC_KEY = "publickey";
	private static final String AUTHENTICATION_PASSWORD = "password";
	private static final String AUTHENTICATION_KEYBOARD = "keyboard-interactive";

	private ChannelShell channel;

	private SSHConnectionInfo userInfo;
	private int preConnectCols = -1;
	private int preConnectLines;
	private int preConnectXPix;
	private int preConnectYPix;

	public void windowSizeChanged( final int cols, final int lines, final int xPixels, final int yPixels ) {
		if ( this.channel.isConnected() ) {
			this.channel.setPtySize( cols, lines, xPixels, yPixels );
		} else {
			this.preConnectCols = cols;
			this.preConnectLines = lines;
			this.preConnectXPix = xPixels;
			this.preConnectYPix = yPixels;
		}
	}



	protected IBidirectionalConnection createConnection(final SSHConnectionInfo sshConnectionInfo, boolean publickey) {
		String authtype = "";
		try {
			IJSchService service = UnicoreTerminalPlainSSHPlugin.getDefault().getJSchService();
			
			this.userInfo = sshConnectionInfo;
			Session session;
			session = service.createSession( this.userInfo.getHostname(),
					this.userInfo.getPort(),
					this.userInfo.getUsername() );

			authtype = userInfo.getAuthType();
			String authMethods = session.getConfig(PREFERRED_AUTHENTICATION);
			session.setUserInfo( this.userInfo );
			
			//Using eclipse prompter
//			GSISSHUserInfoPrompter uiInfoPrompter = new GSISSHUserInfoPrompter(session);
//			session.setUserInfo(uiInfoPrompter);

			// if no auth method is specified try to connect via public key at first
			// if this fails, ask the user for a different public/private key
			// if the second try fails or the user clicks cancel, fallback to other auth methods
			if( authtype==null || "".equals(authtype) ||  PlainSSHConstants.PLAIN_TYPE_PUBKEY.equals(authtype))
			{
				session.setConfig(PREFERRED_AUTHENTICATION, AUTHENTICATION_PUBLIC_KEY);
				try {
					session.connect();
					
					if(!userInfo.getAuthType().equals(PlainSSHConstants.PLAIN_TYPE_PUBKEY)){
						authtype = PlainSSHConstants.PLAIN_TYPE_PUBKEY;
					}					
				} 
				catch (Exception e) {
					if(userInfo.promptPrivateKey(""))
					{
						String keypath = this.userInfo.getPrivateKeyPath();
						
						//as long private key is not verified don't store it
						userInfo.setPrivateKeyPath("");
						
						if(keypath != null && keypath.trim().length() > 0) {
							service.getJSch().addIdentity(keypath);
							
							//now it is verified
							userInfo.setPrivateKeyPath(keypath);
						}					

						session = service.createSession( userInfo.getHostname(),
								userInfo.getPort(),
								userInfo.getUsername() );
						session.setUserInfo(userInfo );
						session.setConfig(PREFERRED_AUTHENTICATION, AUTHENTICATION_PUBLIC_KEY);
						try {
							session.connect();
							if(!PlainSSHConstants.PLAIN_TYPE_PUBKEY.equals(userInfo.getAuthType())){
								authtype = PlainSSHConstants.PLAIN_TYPE_PUBKEY;
							}	
						} catch (Exception e1) {
							// public key authentication failed
							authtype = "";
							
							sshConnectionInfo.showMessage("Public-key authentication failed. " +
									"Cause: \n" + e1.getMessage());
							
//							if(sshConnectionInfo.promptYesNo("Public-key authentication failed. " +
//									"Cause: \n" + e1.getMessage() + "\nTrying password authentication?")){
//								authtype = PlainSSHConstants.PLAIN_TYPE_PASS;
//								session = service.createSession( userInfo.getHostname(),
//										userInfo.getPort(),
//										userInfo.getUsername() );
//								session.setUserInfo(userInfo );
//								session.setConfig(PREFERRED_AUTHENTICATION, AUTHENTICATION_PASSWORD);
//
//								//Using eclipse prompter
//								UserInfoPrompter uiInfoPrompter = new UserInfoPrompter(session);
//								session.setUserInfo(uiInfoPrompter);
//
//								try {
//									session.connect();
//									if(!PlainSSHConstants.PLAIN_TYPE_PASS.equals(userInfo.getAuthType())){
//										authtype = PlainSSHConstants.PLAIN_TYPE_PASS;
//									}	
//								} catch (Exception e2){
//									if(sshConnectionInfo.promptYesNo("Password authentication failed. Cause: \n" + 
//											e1.getMessage() + "\nTrying Keyboard-Interactive authentication?")){
//										authtype = PlainSSHConstants.PLAIN_TYPE_INTERACTIVE;
//										session = service.createSession( userInfo.getHostname(),
//												userInfo.getPort(),
//												userInfo.getUsername() );
//										session.setUserInfo(userInfo );
//										session.setConfig(PREFERRED_AUTHENTICATION, AUTHENTICATION_KEYBOARD);
//
//										//Using eclipse prompter
//										uiInfoPrompter = new UserInfoPrompter(session);
//										session.setUserInfo(uiInfoPrompter);
//
//										try {
//											session.connect();
//											if(!PlainSSHConstants.PLAIN_TYPE_INTERACTIVE.equals(userInfo.getAuthType())){
//												authtype = PlainSSHConstants.PLAIN_TYPE_INTERACTIVE;
//											}	
//										} catch (Exception e3){
//											authtype = "";
//											sshConnectionInfo.showMessage("Keyboard-Interactive authentication failed. Cause: \n" + e3.getMessage());								
//										}
//									}
//								}
//							}
//							else{
//								if(sshConnectionInfo.promptYesNo("Trying Keyboard-Interactive authentication?")){
//									authtype = PlainSSHConstants.PLAIN_TYPE_INTERACTIVE;
//									session = service.createSession( userInfo.getHostname(),
//											userInfo.getPort(),
//											userInfo.getUsername() );
//									session.setUserInfo(userInfo );
//									session.setConfig(PREFERRED_AUTHENTICATION, AUTHENTICATION_KEYBOARD);
//
//									//Using eclipse prompter
//									UserInfoPrompter uiInfoPrompter = new UserInfoPrompter(session);
//									session.setUserInfo(uiInfoPrompter);
//									try {
//										session.connect();
//										if(!PlainSSHConstants.PLAIN_TYPE_INTERACTIVE.equals(userInfo.getAuthType())){
//											authtype = PlainSSHConstants.PLAIN_TYPE_INTERACTIVE;
//										}	
//									} catch (Exception e3){
//										authtype = "";							
//									}
//								}
//							}
						}
					}
					else {
						authtype = "";
						
						//sshConnectionInfo.showMessage("Public-key authentication aborted by user.");
					}
//						authtype = PlainSSHConstants.PLAIN_TYPE_PASS;
//						session = service.createSession( userInfo.getHostname(),
//								userInfo.getPort(),
//								userInfo.getUsername() );
//						session.setUserInfo(userInfo );
//						session.setConfig(PREFERRED_AUTHENTICATION, AUTHENTICATION_PASSWORD);
//
//						//Using eclipse prompter
////						uiInfoPrompter = new GSISSHUserInfoPrompter(session);
////						session.setUserInfo(uiInfoPrompter);
//
//						try {
//							session.connect();
//							if(!PlainSSHConstants.PLAIN_TYPE_PASS.equals(userInfo.getAuthType())){
//								authtype = PlainSSHConstants.PLAIN_TYPE_PASS;
//							}	
//						} catch (Exception e2){
//							sshConnectionInfo.showMessage("Password authentication failed. Cause: \n" + e2.getMessage());
//							
//							
////							if(sshConnectionInfo.promptYesNo("Password authentication failed. Cause: \n" + e2.getMessage() + "\nTrying Keyboard-Interactive authentication?")){
////								authtype = PlainSSHConstants.PLAIN_TYPE_INTERACTIVE;
////								session = service.createSession( userInfo.getHostname(),
////										userInfo.getPort(),
////										userInfo.getUsername() );
////								session.setUserInfo(userInfo );
////								session.setConfig(PREFERRED_AUTHENTICATION, AUTHENTICATION_KEYBOARD);
////
////								//Using eclipse prompter
////								//uiInfoPrompter = new UserInfoPrompter(session);
////								//session.setUserInfo(uiInfoPrompter);
////
////								try {
////									session.connect();
////									if(!PlainSSHConstants.PLAIN_TYPE_INTERACTIVE.equals(userInfo.getAuthType())){
////										authtype = PlainSSHConstants.PLAIN_TYPE_INTERACTIVE;
////									}
////									sshConnectionInfo.showMessage("Password authentication failed, but Keyboard-Interactive " +
////											"authentication was successful. Setting default authentication to Keyboard-Interactive.");
////								} catch (Exception e3){
////									authtype = "";
////									sshConnectionInfo.showMessage("Keyboard-Interactive authentication failed. Cause: \n" + e3.getMessage());					
////								}														
//						}
//					}
				}
			}
			else if(PlainSSHConstants.PLAIN_TYPE_PASS.equals(authtype)){
				session = service.createSession( userInfo.getHostname(),
						userInfo.getPort(),
						userInfo.getUsername() );
				session.setUserInfo(userInfo );
				session.setConfig(PREFERRED_AUTHENTICATION, AUTHENTICATION_PASSWORD);	

				try {
					session.connect();
					if(!PlainSSHConstants.PLAIN_TYPE_PASS.equals(userInfo.getAuthType())){
						authtype = PlainSSHConstants.PLAIN_TYPE_PASS;
					}	
				} catch (Exception e2){

					authtype = PlainSSHConstants.PLAIN_TYPE_INTERACTIVE;
					session = service.createSession( userInfo.getHostname(),
							userInfo.getPort(),
							userInfo.getUsername() );
					session.setUserInfo(userInfo );
					session.setConfig(PREFERRED_AUTHENTICATION, AUTHENTICATION_KEYBOARD);

					//Using eclipse prompter
					//uiInfoPrompter = new UserInfoPrompter(session);
					//session.setUserInfo(uiInfoPrompter);

					try {
						session.connect();
						if(!PlainSSHConstants.PLAIN_TYPE_INTERACTIVE.equals(userInfo.getAuthType())){
							authtype = PlainSSHConstants.PLAIN_TYPE_INTERACTIVE;
						}
						sshConnectionInfo.showMessage("Password authentication failed, but Keyboard-Interactive " +
						"authentication was successful. Setting default authentication to Keyboard-Interactive.");
					} catch (Exception e3){
						authtype = "";
						//sshConnectionInfo.showMessage("Keyboard-Interactive authentication failed. Cause: \n" + e3.getMessage());
						sshConnectionInfo.showMessage("Password authentication failed. Cause: \n" + e2.getMessage());
					}														
				}
				//Using eclipse prompter
//				uiInfoPrompter = new GSISSHUserInfoPrompter(session);
//				session.setUserInfo(uiInfoPrompter);
			}
			else{
				session = service.createSession( userInfo.getHostname(),
						userInfo.getPort(),
						userInfo.getUsername() );
				session.setUserInfo(userInfo );
				session.setConfig(PREFERRED_AUTHENTICATION, AUTHENTICATION_KEYBOARD);

				//Using eclipse prompter
//				uiInfoPrompter = new GSISSHUserInfoPrompter(session);
//				session.setUserInfo(uiInfoPrompter);
			}

			if(authtype.equals(""))return null;

			if(!session.isConnected()) session.connect();	

			userInfo.setAuthType(authtype);
			session.setConfig(PREFERRED_AUTHENTICATION, authMethods);

			final Session s = session;
			final IBidirectionalConnection connection = new IBidirectionalConnection() {
				public void close() {
					SshShell.this.channel.disconnect();
					s.disconnect();
				}
				public InputStream getInputStream() throws IOException {
					return SshShell.this.channel.getInputStream();
				}
				public OutputStream getOutputStream() throws IOException {
					return SshShell.this.channel.getOutputStream();
				}
			};

			this.channel = (ChannelShell) session.openChannel( "shell" ); //$NON-NLS-1$
			this.channel.connect();
			if ( this.preConnectCols != -1 ) {
				windowSizeChanged( this.preConnectCols, this.preConnectLines,
						this.preConnectXPix, this.preConnectYPix );
			}
			return connection;
			//}
		} 
		catch ( final JSchException exception ) {
			UnicoreTerminalPlainSSHPlugin.log(Status.ERROR,"Unable to open a connection: "+exception.getMessage(), exception );
			//sshConnectionInfo.showMessage("Unable to open a connection with authentication method '" +authtype + "': \n" + exception.getMessage());
			return null;
		}
		catch ( final Exception exception ) {
			String errorMessage = "";
			if(exception.getMessage()!=null){
				errorMessage = exception.getMessage();
			}			
			UnicoreTerminalPlainSSHPlugin.log(Status.ERROR,"Unable to open a connection: "+errorMessage, exception );
			//sshConnectionInfo.showMessage("Unable to open a connection with authentication method '" +authtype + "': \n" + errorMessage);
			return null;
		}
	}

	public void windowTitleChanged( final String windowTitle ) {
		//    this.terminal.setDescription( Messages.formatMessage( "SshShell.descriptionWithWinTitle", //$NON-NLS-1$
		//                                  this.userInfo.getUsername(),
		//                                  this.userInfo.getHostname(),
		//                                  windowTitle ) );
	}

	/* (non-Javadoc)
	 * @see de.fzj.unicore.rcp.terminal.ITerminalListener#terminated()
	 */
	public void terminated() {
		// not needed
	}


}
