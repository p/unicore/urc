/*****************************************************************************
 * Copyright (c) 2006, 2007 g-Eclipse Consortium 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for the
 * g-Eclipse project founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributors:
 *    Thomas Koeckerbauer GUP, JKU - initial API and implementation
 *****************************************************************************/

package de.fzj.unicore.rcp.terminal.ssh.plain;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jsch.core.IJSchService;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import de.fzj.unicore.rcp.logmonitor.LogActivator;


/**
 * The activator class controls the plug-in life cycle
 *
 * @author Bastian Demuth, Andre Giesler 
 */
public class UnicoreTerminalPlainSSHPlugin extends AbstractUIPlugin {

  /** The plug-in ID */
  public static final String PLUGIN_ID = "de.fzj.unicore.rcp.terminal.ssh"; //$NON-NLS-1$

  // The shared instance
  private static UnicoreTerminalPlainSSHPlugin plugin;

  private ServiceTracker tracker;

  /**
   * The constructor
   */
  public UnicoreTerminalPlainSSHPlugin() {
    plugin = this;
  }

  /*
   * (non-Javadoc)
   * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
   */
  @Override
  public void start( final BundleContext context ) throws Exception {
    super.start(context);
    this.tracker = new ServiceTracker( getBundle().getBundleContext(),
                                       IJSchService.class.getName(),
                                       null );
    this.tracker.open();
  }

  /*
   * (non-Javadoc)
   * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
   */
  @Override
  public void stop( final BundleContext context ) throws Exception {
    plugin = null;
    this.tracker.close();
    super.stop(context);
  }

  IJSchService getJSchService() {
    return (IJSchService)this.tracker.getService();
  }
  /**
   * Returns the shared instance
   *
   * @return the shared instance
   */
  public static UnicoreTerminalPlainSSHPlugin getDefault() {
    return plugin;
  }

  /** 
	 * Log a message via Eclipse logging
	 * Defaults to INFO Level 
	 * @param msg the message to log
	 */
	public static void log(String msg) {
		log(msg, null);
	}
	/** 
	 * Log a message via Eclipse logging
	 * Defaults to INFO Level 
	 * @param msg the message to log
	 * @param e stacktrace to append
	 */
	public static void log(String msg, Exception e) {
		log(Status.INFO, msg, e, false);
	}
	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, false);
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param e stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, false);
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param forceAlarmUser forces the logging plugin to open a popup; usually, this should NOT be set to true!
	 */
	public static void log(int status, String msg, boolean forceAlarmUser) {
		log(status, msg, null, forceAlarmUser);
	}
	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param e stacktrace to append
	 * @param forceAlarmUser forces the logging plugin to open a popup; usually, this should NOT be set to true!
	 */
	public static void log(int status, String msg, Exception e, boolean forceAlarmUser) {
		IStatus s = new Status(status, PLUGIN_ID, Status.OK, msg, e);
		LogActivator.log(plugin, s,forceAlarmUser);
	}
}
