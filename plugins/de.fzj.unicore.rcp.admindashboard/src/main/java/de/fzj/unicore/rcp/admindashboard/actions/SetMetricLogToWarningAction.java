package de.fzj.unicore.rcp.admindashboard.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.admindashboard.AdminConstants;
import de.fzj.unicore.rcp.admindashboard.ThresholdObserver;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;

public class SetMetricLogToWarningAction extends Action {
	
	private String metricName;
	private SecuredNode callingNode;
	public static final int MIN=-1;
	public static final int MAX=1;
	private int mode;
	
	
	
	public SetMetricLogToWarningAction(int mode, String metricName, SecuredNode callingNode) {		
		this.metricName = metricName;
		this.mode = mode;
		this.callingNode = callingNode;
		if(mode==MIN) setText("MIN threshold Log Level --> SEVERE");
		else if(mode==MAX) setText("MAX threshold Log Level --> SEVERE");
	}
	
	public void run()
	{
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
			public void run() {
				switch(mode) {
					case MIN: ThresholdObserver.getInstance(callingNode).setLogLevelMin(metricName, AdminConstants.METRICS_LOGLEVEL_WARNING); break;
					case MAX: ThresholdObserver.getInstance(callingNode).setLogLevelMax(metricName, AdminConstants.METRICS_LOGLEVEL_WARNING); break;
				}
			}
		});
	}
	
	
}
