package de.fzj.unicore.rcp.admindashboard.widgets.system;

import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public abstract class WatchedMetricsColumnViewerSorter extends ViewerComparator {
		public static final int ASC = 1;
		
		public static final int NONE = 0;
		
		public static final int DESC = -1;
		
		private int direction = 0;
		
		private TableViewerColumn column;
		
		private ColumnViewer viewer;
		
		public WatchedMetricsColumnViewerSorter(ColumnViewer viewer, TableViewerColumn column) {
			this.column = column;
			this.viewer = viewer;
			this.column.getColumn().addSelectionListener(new SelectionAdapter() {

				public void widgetSelected(SelectionEvent e) {
					if( WatchedMetricsColumnViewerSorter.this.viewer.getComparator() != null ) {
						if( WatchedMetricsColumnViewerSorter.this.viewer.getComparator() == WatchedMetricsColumnViewerSorter.this ) {
							int tdirection = WatchedMetricsColumnViewerSorter.this.direction;
							
							if( tdirection == ASC ) {
								setSorter(WatchedMetricsColumnViewerSorter.this, DESC);
							} else if( tdirection == DESC ) {
								setSorter(WatchedMetricsColumnViewerSorter.this, NONE);
							}
						} else {
							setSorter(WatchedMetricsColumnViewerSorter.this, ASC);
						}
					} else {
						setSorter(WatchedMetricsColumnViewerSorter.this, ASC);
					}
				}
			});
		}
		
		public void setSorter(WatchedMetricsColumnViewerSorter sorter, int direction) {
			if( direction == NONE ) {
				column.getColumn().getParent().setSortColumn(null);
				column.getColumn().getParent().setSortDirection(SWT.NONE);
				viewer.setComparator(null);
			} else {
				column.getColumn().getParent().setSortColumn(column.getColumn());
				sorter.direction = direction;
				
				if( direction == ASC ) {
					column.getColumn().getParent().setSortDirection(SWT.DOWN);
				} else {
					column.getColumn().getParent().setSortDirection(SWT.UP);
				}
				
				if( viewer.getComparator() == sorter ) {
					viewer.refresh();
				} else {
					viewer.setComparator(sorter);
				}
				
			}
		}

		public int compare(Viewer viewer, Object e1, Object e2) {
			return direction * doCompare(viewer, e1, e2);
		}
		
		protected abstract int doCompare(Viewer viewer, Object e1, Object e2);
	}
