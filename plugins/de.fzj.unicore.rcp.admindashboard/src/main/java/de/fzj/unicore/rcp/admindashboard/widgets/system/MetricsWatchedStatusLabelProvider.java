package de.fzj.unicore.rcp.admindashboard.widgets.system;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Image;

import de.fzj.unicore.rcp.admindashboard.AdminDashboardActivator;
import de.fzj.unicore.rcp.admindashboard.MetricDataProvider;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;

public class MetricsWatchedStatusLabelProvider extends ColumnLabelProvider {

	private SecuredNode callingNode;	
	
	public MetricsWatchedStatusLabelProvider(SecuredNode caller) {
		callingNode = caller;
	}
	
	@Override
	public Image getImage(Object element) {
		Image image = null;
		String metricName = ((MetricValue) element).getName();
		
		if(MetricDataProvider.getInstance(callingNode).isWatched(metricName)) image = AdminDashboardActivator.getImageDescriptor("unwatchMetric.png").createImage();
		else if(MetricDataProvider.getInstance(callingNode).isUnwatchable(metricName)) image = AdminDashboardActivator.getImageDescriptor("metric_unwatchable.png").createImage();
		else image = AdminDashboardActivator.getImageDescriptor("watchMetric.png").createImage();
		
		return image;
	}
	
	@Override
	public String getText(Object element) {
		return null;
	}

}
