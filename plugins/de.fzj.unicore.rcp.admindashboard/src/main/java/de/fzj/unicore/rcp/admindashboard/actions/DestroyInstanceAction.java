/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.admindashboard.actions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import de.fzj.unicore.rcp.admindashboard.AdminDashboardActivator;
import de.fzj.unicore.rcp.admindashboard.nodes.WSRFParentNode;
import de.fzj.unicore.rcp.admindashboard.util.AdminServiceAdapter;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;

/**
 * @author j.daivandy@fz-juelich.de
 *
 */
public class DestroyInstanceAction extends NodeAction {
	
	private String serviceName;
	
	public DestroyInstanceAction(WSRFNode node)
	{
		super(node);
		setText("destroy UNICORE resource");
		setToolTipText("destroy this UNICORE resource");
		setImageDescriptor(AdminDashboardActivator.getImageDescriptor("deleteInstance.png"));
		
		String address = node.getEpr().getAddress().getStringValue();
		try {
			serviceName = address.split("services/")[1];
			serviceName = serviceName.split("\\?res")[0];			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		
	}

	
	public void run()
	{
		
		Job wsJob = new Job(this.getClass().getName()) {

			@Override
			protected IStatus run(IProgressMonitor arg0) {
				
				try {
					String uid = getNode().getName();
					AdminServiceAdapter.deleteServiceInstance((SecuredNode) getNode(), serviceName, uid);
					
					WSRFParentNode selectedService = (WSRFParentNode) getNode().getParent();
					selectedService.asyncRefresh(1, false);
					return Status.OK_STATUS;
				}
				catch(Exception e) {					
					//AdminDashboardActivator.log(IStatus.ERROR, "Can't delete "+serviceName+" instance " + getNode().getName(), e);
					AdminDashboardActivator.log(IStatus.ERROR, e.getMessage(), true);
					return Status.CANCEL_STATUS;
				}				
			}
			
		};
		
		wsJob.schedule();		
	}

	
	
	public String getConfirmationMessage(){
		return "Do you really want to destroy the selected Grid resources?";
	}

}
