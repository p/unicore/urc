package de.fzj.unicore.rcp.admindashboard.util;

import de.fzj.unicore.rcp.admindashboard.nodes.AdminGridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;

public class AdminUtils {
	
	public static AdminGridNode createNewAdminGridNode()
	{

		AdminGridNode adminToolsNode = new AdminGridNode();
		adminToolsNode.setCache(NodeFactory.getNodeDataCache());
		NodeFactory.getNodeDataCache().incrementNodeCount(adminToolsNode.getEpr());
		return adminToolsNode;

	}

}
