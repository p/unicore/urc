package de.fzj.unicore.rcp.admindashboard.ui;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.admindashboard.AdminDashboardActivator;
import de.fzj.unicore.rcp.admindashboard.actions.MonitorAdminServiceEndpointAction;
import de.fzj.unicore.rcp.admindashboard.nodes.AdminGridNode;
import de.fzj.unicore.rcp.admindashboard.widgets.servicemanager.ServiceManagerTreeViewer;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.ServiceContentProvider;
import de.fzj.unicore.rcp.servicebrowser.actions.RefreshAction;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.IGridBrowserFilterExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.filters.DefaultFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterController;
import de.fzj.unicore.rcp.servicebrowser.filters.FilterMenuCategory;
import de.fzj.unicore.rcp.servicebrowser.filters.GridFilterAction;
import de.fzj.unicore.rcp.servicebrowser.ui.DefaultFilterSet;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceView;

public class AdminDashboardView extends ServiceView {

	public static String ID = "de.fzj.unicore.rcp.admindashboard.ui.admindashboardview";
		
	
	@Override
	public void createPartControl(Composite parent) {
		ServiceManagerTreeViewer treeViewer = new ServiceManagerTreeViewer(parent, SWT.MULTI
				| SWT.H_SCROLL | SWT.V_SCROLL);
		viewer = treeViewer;
		defaultFilter = new DefaultFilter(treeViewer);
		//drillDownAdapter = new DrillDownAdapter(treeViewer);
		FilterController filterController = new FilterController();
		filterController.addViewer(treeViewer);
		defaultFilterSet = new DefaultFilterSet(filterController);
		viewer.setServiceContentProvider(new ServiceContentProvider());		
		
		// TODO
		//treeViewer.setDoubleClickEnabled(false);
		
		AdminGridNode adminGridNode = AdminDashboardActivator.getDefault().getAdminGridNode();
		adminGridNode.setName("AdminGrid");
		adminGridNode.resetAvailableActions();
		viewer.setGridNode(adminGridNode);
		
		

		getActions(filterController);
		loadSettings();

		getSite().setSelectionProvider(treeViewer);

		contributeToActionBars();

	}
	
	@Override
	protected void getActions(FilterController fc) {
		filterMenuCategory = new ArrayList<FilterMenuCategory>();
		filterActions = new ArrayList<Action>();
		filterAsStartupFilter = new ArrayList<Boolean>();

		createNoFilterAction();
		createDefaultFilterAction();

		/*
		 * go over all registered extensions and collect the filters in the two
		 * lists filterActions and filterMenuCategory
		 */
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(ServiceBrowserConstants.GRIDBROWSER_FILTER_EXTENSION_POINT);
		IConfigurationElement[] members = extensionPoint
				.getConfigurationElements();
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			// IExtension extension = member.getDeclaringExtension();
			// String actionName = member.getAttribute(FUNCTION_NAME_ATTRIBUTE);
			try {
				IGridBrowserFilterExtensionPoint ext = (IGridBrowserFilterExtensionPoint) member
						.createExecutableExtension("name");
				GridFilterAction[] gridFilterActions = ext.getGridFilterList(
						viewer, this);
				for (GridFilterAction gfa : gridFilterActions) {
					if (gfa != null) {
						Action currentAction = gfa.getAction();
						currentAction.setId(gfa.getCategory().getMenuPath()
								+ "/" + currentAction.getText());
						filterActions.add(currentAction);
						filterMenuCategory.add(gfa.getCategory());
						filterAsStartupFilter.add(gfa.isUseAsStartupFilter());
					}
					defaultFilter.addDefaultTypes(ext.getDefaultNodeTypes(viewer));
				}

			} catch (CoreException ex) {
				AdminDashboardActivator.log("Could not determine all available actions for service",
								ex);
			}
		}

		adminSwitchAction = new Action("Admin mode", Action.AS_RADIO_BUTTON) {
			public void run() {

			}
		};
		adminSwitchAction.setToolTipText("switch on/off admin functionality");
		adminSwitchAction.setImageDescriptor(AdminDashboardActivator.getImageDescriptor("admin.png"));

		globalRefreshAction = new RefreshAction(viewer.getGridNode());
		globalRefreshAction.setToolTipText("Refresh Grid");
		globalRefreshAction.setViewer(getViewer());

		monitorRegistryAction = new MonitorAdminServiceEndpointAction(getViewer());
	}
	
	
	/**
	 * creates the basic filter that displays all nodes
	 */
	@Override	
	protected void createDefaultFilterAction() {
		List<QName> menuLocation;

		switchViewDefaultAction = new Action("Execution Services and Storages", Action.AS_RADIO_BUTTON) {
			public void run() {
				if (this.isChecked()) {
					// display all nodes
					activateFilter(this.getId(), null);
				}
			}
		};

		/*
		 * empty ArrayList of QName will lead to display the belonging action in
		 * the root of the filter menu
		 */
		menuLocation = new ArrayList<QName>();
		filterMenuCategory.add(new FilterMenuCategory(menuLocation));
		switchViewDefaultAction.setId(switchViewDefaultAction.getText());
		filterActions.add(switchViewDefaultAction);
		filterAsStartupFilter.add(new Boolean(true));
	}
}
