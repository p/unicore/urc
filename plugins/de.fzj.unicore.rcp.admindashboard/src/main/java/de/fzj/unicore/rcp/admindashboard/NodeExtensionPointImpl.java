/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.admindashboard;


import java.net.URI;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.swt.graphics.Image;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.admindashboard.nodes.AdminGridNode;
import de.fzj.unicore.rcp.admindashboard.nodes.AdminServiceNode;
import de.fzj.unicore.rcp.admindashboard.nodes.MetricCategoryNode;
import de.fzj.unicore.rcp.admindashboard.nodes.ServerConfiguratorNode;
import de.fzj.unicore.rcp.admindashboard.nodes.ServiceManagerNode;
import de.fzj.unicore.rcp.admindashboard.nodes.SystemNode;
import de.fzj.unicore.rcp.admindashboard.nodes.WSRFParentNode;
import de.fzj.unicore.rcp.common.utils.Utils;
import de.fzj.unicore.rcp.servicebrowser.Util;
import de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.nodes.FolderNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.uas.util.AddressingUtil;

/**
 * 
 * @author Jason Milad Daivandy
 * 
 */
public class NodeExtensionPointImpl implements INodeExtensionPoint {
	
	private Image adminServiceNodeImage = AdminDashboardActivator.getImageDescriptor("adminServiceNode.png").createImage();
	private Image wsrfParentNodeImage = AdminDashboardActivator.getImageDescriptor("WSRF.png").createImage();
	private Image serviceManagerNodeImage = AdminDashboardActivator.getImageDescriptor("serviceManager.png").createImage();
	private Image serverConfiguratorNodeImage = AdminDashboardActivator.getImageDescriptor("serverConfigurator.png").createImage();
	private Image systemNodeImage = AdminDashboardActivator.getImageDescriptor("serverConfigurator.png").createImage();
	private Image metricCategoryNodeImage = AdminDashboardActivator.getImageDescriptor("serverConfigurator.png").createImage();

	
	public void dispose() {
		wsrfParentNodeImage.dispose();
		serviceManagerNodeImage.dispose();
		serverConfiguratorNodeImage.dispose();
		systemNodeImage.dispose();
		metricCategoryNodeImage.dispose();
	}
	
	public void finalize()
	{
		dispose();
	}
	
	protected String 
	adminGridNodeType = AdminGridNode.TYPE, //AdminGridNode.PORTTYPE.toString(),
	adminServiceNodeType = AdminServiceNode.TYPE, //AdminServiceNode.PORTTYPE.toString(),
	wsrfParentNodeType = WSRFParentNode.TYPE, //WSRFParentNode.PORTTYPE.toString(),
	serviceManagerNodeType = ServiceManagerNode.TYPE, //ServiceManagerNode.PORTTYPE.toString(),
	serverConfiguratorNodeType = ServerConfiguratorNode.TYPE, //ServerConfiguratorNode.PORTTYPE.toString(),
	systemNodeType = SystemNode.TYPE, //SystemNode.PORTTYPE.toString(),
	metricCategoryNodeType = MetricCategoryNode.TYPE; //MetricCategoryNode.PORTTYPE.toString();
	

	public NodeExtensionPointImpl() {

	}


	/* (non-Javadoc)
	 * @see de.fzj.unicore.rcp.servicebrowser.extensionpoints.IServiceExtensionPoint#getImage(de.fzj.unicore.rcp.servicebrowser.serviceNodes.ServiceObject)
	 */
	public Image getImage(Node node) {
		String imageKey = null;
		if (node instanceof WSRFParentNode) return wsrfParentNodeImage;		
		else if (node instanceof AdminServiceNode) return adminServiceNodeImage;
		else if (node instanceof ServiceManagerNode) return serviceManagerNodeImage;
		else if (node instanceof ServerConfiguratorNode) return serverConfiguratorNodeImage;
		else if (node instanceof SystemNode) return systemNodeImage;
		else if (node instanceof MetricCategoryNode) return metricCategoryNodeImage;
		
		else return null;
	}

	/* (non-Javadoc)
	 * @see de.fzj.unicore.rcp.servicebrowser.extensionpoints.IServiceExtensionPoint#getText(de.fzj.unicore.rcp.servicebrowser.serviceNodes.ServiceObject)
	 */
	public String getText(Node node) {
		// use default text for all my nodes: getName()
		return null;
	}

	/* (non-Javadoc)
	 * @see de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeExtensionPoint#getNode(org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public Node getNode(EndpointReferenceType epr) {
		
		if(epr == null) return null;
		
		QName type = Utils.InterfaceNameFromEPR(epr);
		
		if(type == null) return null;
		
		if(AdminGridNode.PORTTYPE.equals(type))
		{
			return new AdminGridNode();
		}
		else if(AdminServiceNode.PORTTYPE.equals(type))
		{
			return new AdminServiceNode(epr);
		}		
		else if(ServiceManagerNode.PORTTYPE.equals(type))
		{
			return new ServiceManagerNode(epr);
		}
		else if(ServerConfiguratorNode.PORTTYPE.equals(type))
		{
			return new ServerConfiguratorNode(epr);
		}
		else if(SystemNode.PORTTYPE.equals(type))
		{
			return new SystemNode(epr);
		}
		else if(MetricCategoryNode.PORTTYPE.equals(type))
		{
			return new MetricCategoryNode(epr);
		}
		else if(WSRFParentNode.PORTTYPE.equals(type))
		{
			return new WSRFParentNode(epr);
		}
		else return null;
	}

	/* (non-Javadoc)
	 * @see de.fzj.unicore.rcp.servicebrowser.extensionpoints.IServiceExtensionPoint#getServiceModel(org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public Node getNode(URI uri) {
		if(uri == null) return null;

		String type = Util.determineTypeOfURI(uri);
		EndpointReferenceType epr = AddressingUtil.newEPR();
		epr.addNewAddress().setStringValue(uri.toString());
		if(adminGridNodeType.equals(type))
		{
			AddressingUtil.addPortType(epr,AdminGridNode.PORTTYPE);
		}
		else if(adminServiceNodeType.equals(type))
		{
			AddressingUtil.addPortType(epr,AdminServiceNode.PORTTYPE);
		}
		else if(serverConfiguratorNodeType.equals(type))
		{
			AddressingUtil.addPortType(epr,ServerConfiguratorNode.PORTTYPE);
		}
		else if(serviceManagerNodeType.equals(type))
		{
			AddressingUtil.addPortType(epr,ServiceManagerNode.PORTTYPE);
		}
		else if(systemNodeType.equals(type))
		{
			AddressingUtil.addPortType(epr,SystemNode.PORTTYPE);
		}
		else if(wsrfParentNodeType.equals(type))
		{
			AddressingUtil.addPortType(epr,FolderNode.getPortType());
		}
		
		else return null;
		
		return getNode(epr);

	}


	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IExecutableExtension#setInitializationData(org.eclipse.core.runtime.IConfigurationElement, java.lang.String, java.lang.Object)
	 */
	public void setInitializationData(IConfigurationElement config, String propertyName, Object data) throws CoreException {
		// TODO Auto-generated method stub

	}

	



}
