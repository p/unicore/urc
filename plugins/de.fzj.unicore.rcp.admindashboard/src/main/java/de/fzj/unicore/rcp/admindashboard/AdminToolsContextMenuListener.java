package de.fzj.unicore.rcp.admindashboard;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchActionConstants;

import de.fzj.unicore.rcp.admindashboard.actions.MonitorAdminServiceEndpointAction;
import de.fzj.unicore.rcp.servicebrowser.ContextMenuListener;
import de.fzj.unicore.rcp.servicebrowser.actions.MultiSelectionAction;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

public class AdminToolsContextMenuListener extends ContextMenuListener {

	private MonitorAdminServiceEndpointAction monitorAdminServiceEndpointAction = new MonitorAdminServiceEndpointAction(null);
		
	public AdminToolsContextMenuListener(IServiceViewer viewer) {
		super(viewer);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void menuAboutToShow(IMenuManager manager) {

		// add new Registry action
		monitorAdminServiceEndpointAction.setViewer(getViewer());
		manager.add(monitorAdminServiceEndpointAction);		
		
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		
		//iterate over all selected nodes and determine the intersection of allowed actions
		IStructuredSelection selection = (IStructuredSelection) getViewer().getSelection();
		List nodeList = selection.toList();
		int numNodes = nodeList.size();
		Node[] nodes = new Node[numNodes];
		nodeList.toArray(nodes);
		getAllNodeActions(nodes);

		Map<String,List<NodeAction>> availableActions = new ConcurrentHashMap<String, List<NodeAction>>();
		for (int i = 0; i < nodes.length; i++) {
			NodeAction[] actions = nodes[i].getAvailableActions().toArray(new NodeAction[0]);
			for (int j = 0; j < actions.length; j++) {
				
				// Check whether this action is available right now.
				if(!actions[j].isCurrentlyAvailable())
				{
					continue;
				}
				
				List<NodeAction> intersectionSoFar = availableActions.get(actions[j].getClassName());

				if(intersectionSoFar == null)				
				{
					if(i == 0)
					{
						intersectionSoFar = new ArrayList<NodeAction>();
						intersectionSoFar.add(actions[j]);
						availableActions.put(actions[j].getClassName(), intersectionSoFar);
					}
				}
				else
				{
					
					// is the action available for all selected nodes up to now?
					if(intersectionSoFar.size() == i) intersectionSoFar.add(actions[j]);

					// no => not in intersection
					else availableActions.remove(actions[j].getClassName());
				}		
			}
		}
		
//		 Create one MultiSelection Action for each action in the intersection and add it to context menu 
		for (Iterator<List<NodeAction>> iter = availableActions.values().iterator(); iter.hasNext();) {
			List<NodeAction> next = iter.next();
			if(next.size() == nodes.length) 
			{
				MultiSelectionAction mult = new MultiSelectionAction(getViewer(), next.toArray(new NodeAction[0]));
				manager.add(mult);
			}
			
		}
	}
	
	@Override
	public void dispose()
	{
		monitorAdminServiceEndpointAction = null;
		setViewer(null);
	}
}
