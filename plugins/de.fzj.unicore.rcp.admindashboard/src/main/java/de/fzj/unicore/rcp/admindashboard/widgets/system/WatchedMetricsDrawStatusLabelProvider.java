package de.fzj.unicore.rcp.admindashboard.widgets.system;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;

import de.fzj.unicore.rcp.admindashboard.AdminDashboardActivator;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;

public class WatchedMetricsDrawStatusLabelProvider extends ColumnLabelProvider {

	private SecuredNode callingNode;
	
	public WatchedMetricsDrawStatusLabelProvider(SecuredNode caller) {
		callingNode = caller;
	}

	@Override
	public Image getImage(Object element) {
		Image image = null;
		String metricName = ((MetricValue) element).getName();		
		
		boolean isDrawn = SystemMetricsDialog.getInstance(callingNode).isDrawn(metricName);
		
		if(!isDrawn) image = AdminDashboardActivator.getImageDescriptor("undrawn.png").createImage();				
		else image = AdminDashboardActivator.getImageDescriptor("drawn.png").createImage();				
		
		return image;
	}

	@Override
	public String getText(Object element) {
		return "";
	}
	
	@Override
	public Color getForeground(Object element) {
		MetricValue mv = (MetricValue) element;				
		Color color = SystemMetricsDialog.getInstance(callingNode).getColor(mv.getName());
		
		return color;
		
	}

	@Override
	public Color getBackground(Object arg0) {
		return null;
	}

}
