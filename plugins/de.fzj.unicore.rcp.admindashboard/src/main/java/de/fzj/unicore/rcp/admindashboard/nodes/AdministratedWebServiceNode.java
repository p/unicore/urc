package de.fzj.unicore.rcp.admindashboard.nodes;

import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.admindashboard.actions.UndeployServiceAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.WebServiceNode;

public class AdministratedWebServiceNode extends WebServiceNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4684571689765910679L;


	public AdministratedWebServiceNode(EndpointReferenceType epr) {
		super(epr);		
	}
	
	
	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		if(!getName().equalsIgnoreCase(AdminServiceNode.TYPE)) {
			availableActions.add(new UndeployServiceAction(this));
		}
				
	}

}
