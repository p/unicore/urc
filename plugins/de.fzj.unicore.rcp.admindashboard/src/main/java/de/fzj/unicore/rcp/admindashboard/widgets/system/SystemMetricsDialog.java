package de.fzj.unicore.rcp.admindashboard.widgets.system;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.bindings.keys.IKeyLookup;
import org.eclipse.jface.bindings.keys.KeyLookupFactory;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TableViewerEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.progress.UIJob;
import org.swtchart.Chart;
import org.swtchart.ILineSeries;
import org.swtchart.ISeries;
import org.swtchart.ISeries.SeriesType;
import org.swtchart.LineStyle;

import de.fzj.unicore.rcp.admindashboard.AdminConstants;
import de.fzj.unicore.rcp.admindashboard.AdminDashboardActivator;
import de.fzj.unicore.rcp.admindashboard.MetricDataProvider;
import de.fzj.unicore.rcp.admindashboard.MetricDataSubscriber;
import de.fzj.unicore.rcp.admindashboard.MetricDataUpdate;
import de.fzj.unicore.rcp.admindashboard.ThresholdObserver;
import de.fzj.unicore.rcp.admindashboard.actions.ClearMetricDataAction;
import de.fzj.unicore.rcp.admindashboard.actions.SetMetricLogToInfoAction;
import de.fzj.unicore.rcp.admindashboard.actions.SetMetricLogToWarningAction;
import de.fzj.unicore.rcp.admindashboard.util.AdminServiceAdapter;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;


public class SystemMetricsDialog extends Dialog implements MetricDataSubscriber {
	
	private Composite theParent;
	private int savedX=-1, savedY=-1;
	
	private String title;
	
	/** Used for mapping one MetricsView dialog to each AdminService instance. */
	private static Map<SecuredNode, SystemMetricsDialog> metricsViews = new HashMap<SecuredNode, SystemMetricsDialog>();
	
	private SecuredNode callingNode;
	private boolean open = true;
	private boolean firstUpdateDone=false;
	
	
	// TAB FOLDER
	private TabFolder tabFolder;
	private TabItem tabMetrics, tabWatchedMetrics;	
	private int selectedTabItem=0;

	
	// TAB: 'Metrics' 
	private Composite metricsContainer;	
	private Label categorySelectionLabel;
	private Composite categorySelectionContainer;
	private Combo categorySelectionDropdown;
	private Button categorySelectionButton;
	private int categoryLastSelected = 0;
	private TableViewer metricTableViewer;
	private List<MetricValue> lastRetrievedMetricValues = new ArrayList<MetricValue>();
	
	
	// TAB: 'Watched Metrics'
	private Composite watchedMetricsContainer;
	private Group chartControls;
	  private Button zoomOuter;
	  private Button viewResetter;
	  private Button zoomInner;
	  private Text refreshInterval_input;
	  private Label refreshInterval_text;
	  private Label refreshInterval_timeUnit;
	  private Button refreshInterval_setter;
	  private Button timeSeriesResetter;
	  
	private Composite theChart;			
	  private Composite verticalScrollers;
		private Button chart_downScroller;
		private Button chart_upScroller;
		
	  private Chart chart;
	  private Composite horizontalScrollers;
		private Button chart_leftScroller;
		private Button chart_rightScroller;
		private boolean chart_manuallyAdjusted=false;
		
	private Composite chartLegend;
	  private TableViewer chartLegend_viewer;
	  	private Button showAller;
	  	private Button cloakAller;	  	
		private List<MetricValue> timeSeries_lastlyWatched = new ArrayList<MetricValue>();	
		private Map<String, Boolean> timeSeriesLegend_contextMenuInfo_MIN = new HashMap<String, Boolean>();
		private Map<String, Boolean> timeSeriesLegend_contextMenuInfo_MAX = new HashMap<String, Boolean>();
		  
	  	  
	
	  // --> time series visualization
	  private Map<String, Color> timeSeries_colorsCurrentlyInUse = new HashMap<String, Color>();		
	  private List<Color> timeSeries_colorsAvailable = new ArrayList<Color>();
	  private Map<String, MetricValue> timeSeries_values = new HashMap<String, MetricValue>();
	  private Map<String, Boolean> timeSeries_visibility = new HashMap<String, Boolean>();
	  private int timeSeries_lowerColorBound=0, timeSeries_upperColorBound=240;
	  private Color timeSeries_selectedColor;
	  private String timeSeries_selected;
	  	  
	  
	
    private SystemMetricsDialog(SecuredNode callingNode) {
    	super(new Shell());    	
        this.title = "Metrics @"+callingNode.getEpr().getAddress().getStringValue().split("services")[0];  
        this.callingNode = callingNode;
        
        setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE | SWT.RESIZE);
        
    	setBlockOnOpen(false);
   	
    	MetricDataProvider.getInstance(callingNode).subscribe(this);
    	createColors();
    }
    
    public static SystemMetricsDialog getInstance(SecuredNode callingNode) {
    	if(!metricsViews.containsKey(callingNode))
    		metricsViews.put(callingNode, new SystemMetricsDialog(callingNode));
    	
    	return metricsViews.get(callingNode);
    }

 
    
    
	protected void buttonPressed(int buttonId) {
		String metric = new String();
		String[] metrics = new String[0];
		
		switch(buttonId) {
			
			case AdminConstants.METRICS_SELECT_CATEGORY: populateRetrievedMetricListTable(categorySelectionDropdown.getText());
								  						 break;
			
			case AdminConstants.METRICS_CHART_LEFT: chart.getAxisSet().getXAxis(0).scrollDown();
							 						chart_manuallyAdjusted=true;
							 						chart.redraw();
							 						break;
							 
			case AdminConstants.METRICS_CHART_RIGHT: chart.getAxisSet().getXAxis(0).scrollUp();
			  				  						 chart_manuallyAdjusted=true; chart.redraw();
			  				  						 break;
			  				  
			case AdminConstants.METRICS_CHART_DOWN: chart.getAxisSet().getYAxis(0).scrollDown();
							 chart_manuallyAdjusted=true;
							 chart.redraw();
							 break;
							 
			case AdminConstants.METRICS_CHART_UP: chart.getAxisSet().getYAxis(0).scrollUp();
						   chart_manuallyAdjusted=true;
						   chart.redraw();
						   break;
						   
			case AdminConstants.METRICS_CHART_ZOOM_IN: chart.getAxisSet().zoomIn();
			 					chart_manuallyAdjusted=true;
			 					chart.redraw();
			 					break;
			 					
			case AdminConstants.METRICS_CHART_ZOOM_OUT: chart.getAxisSet().zoomOut();
			 					 chart_manuallyAdjusted=true;
			 					 chart.redraw();
			 					 break;    		
			 					 
			case AdminConstants.METRICS_CHART_RESET_VIEW: chart.getAxisSet().adjustRange();
			 				   	   chart_manuallyAdjusted=false;
			 				   	   chart.redraw();
			 				   	   break;   			
			
			case AdminConstants.METRICS_DRAW_WATCHED_METRICS: metrics = MetricDataProvider.getInstance(callingNode).getCurrentlyWatchedMetricNames();    		
									   for(String theMetric:metrics) {
												chart.getSeriesSet().getSeries(theMetric).setVisible(true);
												timeSeries_visibility.put(theMetric, true);
										}
									   chart.getAxisSet().adjustRange();
									   chart.redraw();
									   chartLegend_viewer.refresh();
									   break;
			
			case AdminConstants.METRICS_UNDRAW_WATCHED_METRICS: metrics = MetricDataProvider.getInstance(callingNode).getCurrentlyWatchedMetricNames();      		
			   							 for(String theMetric:metrics) {
			   								 chart.getSeriesSet().getSeries(theMetric).setVisible(false);
			   								 timeSeries_visibility.put(theMetric, false);
			   							 }
			   							 chart.getAxisSet().adjustRange();
			   							 chart.redraw();
			   							 chartLegend_viewer.refresh();
			   							 break;
			   							 
			case AdminConstants.METRICS_SET_REFRESH_INTERVAL:						
						int refreshInterval = AdminConstants.METRICS_DEFAULT_POLLING_INTERVAL;
						
						try {
							refreshInterval = Integer.parseInt(refreshInterval_input.getText());
							MetricDataProvider.getInstance(callingNode).setRefreshInterval(refreshInterval);
						}						
						catch(Exception e) {
							refreshInterval_input.setText(Integer.toString(AdminConstants.METRICS_DEFAULT_POLLING_INTERVAL));							
						}
						
						break;
						
			case AdminConstants.METRICS_RESET_TIMESERIES: MetricDataProvider.getInstance(callingNode).resetCollectedData(); break;
			
		}
			
		super.buttonPressed(buttonId);
	}

	protected int getInputTextStyle() {
		return SWT.SINGLE | SWT.BORDER;
	}

	public void setFocus() {
		metricTableViewer.getControl().setFocus();
	}

	protected void configureShell(Shell shell) {
	    super.configureShell(shell);
	    if (title != null) {
			shell.setText(title);
		}
	}

	@Override
	protected Control createContents(Composite parent) {
		Control ctrl = super.createContents(parent);
		
		// remove OK and Cancel buttons; they're unnecessary for this use case and take away space
		getButton(IDialogConstants.OK_ID).setVisible(false);
		getButton(IDialogConstants.CANCEL_ID).setVisible(false);    	
		
		return ctrl;
	}

	/** Persist necessary information of components that are disposed after the dialog is closed. */
	@Override
	public boolean close() {
		
		// remember the currently selected metric category
		categoryLastSelected = categorySelectionDropdown.getSelectionIndex();
		
		// remember visibility stati of metric time series
		Iterator<MetricValue> iter = timeSeries_lastlyWatched.iterator();		
		while(iter.hasNext()) {
			MetricValue mv = iter.next();
			String name = mv.getName();
			boolean visible = chart.getSeriesSet().getSeries(name).isVisible();			
			timeSeries_visibility.put(name, visible);
		}
		
		
		// reset highlighting logic for metric selection in the chart legend		
		timeSeries_selected=null;		
		
		// reset different flags: controls data update threads running in the background
		open=false;		
		firstUpdateDone=false;
		chart_manuallyAdjusted=false;
		
		// get currently selected tab item
		selectedTabItem = tabFolder.getSelectionIndex();
		
		//GridData gd = (GridData) tabFolder.getLayoutData();
		//System.out.println("x="+gd.widthHint+", y="+gd.heightHint);
		savedX=theParent.getBounds().width;
		savedY=theParent.getBounds().height;
		
		
		return super.close();
	}

	@Override
	public int open() {
		open=true;		
		return super.open();
	}

	protected Control createDialogArea(Composite parent) {
		theParent=parent;
		
		tabFolder = new TabFolder(parent, SWT.NONE);
		tabFolder.setLayout(new GridLayout());	
		tabFolder.setLayoutData(new GridData(GridData.FILL_BOTH));
		  	
		tabMetrics = new TabItem(tabFolder, SWT.NONE);
		tabMetrics.setText("Metrics");    	
		tabWatchedMetrics = new TabItem(tabFolder, SWT.NONE);
		tabWatchedMetrics.setText("Watched Metrics");
					
		createMetricsContainer(tabFolder);
		createWatchedMetricsContainer(tabFolder);		
		
		tabMetrics.setControl(metricsContainer);
		tabWatchedMetrics.setControl(watchedMetricsContainer);
		
		tabFolder.setSelection(selectedTabItem);	
	    		
	    applyDialogFont(parent);

	    
	    // restore dimensions previously set/changed by the user
	    if(savedX!=-1 && savedY!=-1) {	    	
	    	theParent.setSize(savedX, savedY);
	    }
			
	    
	    return parent;
	}

	
	
	private void createMetricsContainer(Composite tabFolder) {
		
		metricsContainer = new Group(tabFolder, SWT.NONE);
		GridLayout layout_metricsContainer = new GridLayout(1, false);
		metricsContainer.setLayout(layout_metricsContainer);
		metricsContainer.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true));
		
		addMetricsListMenu();
		addMetricsListTable();
		
		populateCategories();
	}
	
	private void addMetricsListMenu() {		
		categorySelectionContainer = new Composite(metricsContainer, SWT.NONE);		
		categorySelectionContainer.setLayoutData(new GridData());
		categorySelectionContainer.setLayout(new GridLayout(3, false));
		
			categorySelectionLabel = new Label(categorySelectionContainer, SWT.NONE);
			categorySelectionLabel.setText("Category: ");
			GridData ld_categorySelection_label = new GridData();
			categorySelectionLabel.setLayoutData(ld_categorySelection_label);
		
			categorySelectionDropdown = new Combo(categorySelectionContainer, SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);			
			GridData ld_categorySelection_dropDown = new GridData();
			ld_categorySelection_dropDown.widthHint = 300;
			categorySelectionDropdown.setLayoutData(ld_categorySelection_dropDown);
			
			categorySelectionButton = createButton(categorySelectionContainer,  AdminConstants.METRICS_SELECT_CATEGORY, "select", false);
			
			GridData ld_categorySelection_buttonOK = new GridData();
			categorySelectionButton.setLayoutData(ld_categorySelection_buttonOK);
	}
	
	private void addMetricsListTable() {
		metricTableViewer = new TableViewer(metricsContainer, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
		createColumnsForMetricList();		
		metricTableViewer.setContentProvider(new MetricsViewContentProvider());
		metricTableViewer.setInput(lastRetrievedMetricValues);
		metricTableViewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));
	}
	
	private void createColumnsForMetricList() {
				
		TableViewer v = metricTableViewer;
		v.getTable().setHeaderVisible(true);
		v.getTable().setLinesVisible(true);
		
		String[] titles = { "Name", "Value", "Unit", "capture time", "watched" };		
				
		final TableViewerColumn colName = new TableViewerColumn(v, SWT.NONE);
		colName.setLabelProvider(new CommonMetricNameLabelProvider(callingNode, false));
		colName.getColumn().setResizable(true);
		colName.getColumn().setText(titles[0]);
				
		final TableViewerColumn colValue = new TableViewerColumn(v, SWT.NONE);		
		colValue.setLabelProvider(new CommonMetricValueLabelProvider(callingNode, false));
		colValue.getColumn().setResizable(true);		
		colValue.getColumn().setText(titles[1]);
		
		final TableViewerColumn colUnit = new TableViewerColumn(v, SWT.NONE);		
		colUnit.setLabelProvider(new CommonMetricUnitLabelProvider(callingNode, false));
		colUnit.getColumn().setResizable(true);		
		colUnit.getColumn().setText(titles[2]);
		
		final TableViewerColumn colCaptureTime = new TableViewerColumn(v, SWT.NONE);
		colCaptureTime.setLabelProvider(new CommonMetricCaptureTimeLabelProvider(callingNode, false));
		colCaptureTime.getColumn().setToolTipText("Shows the respecetive metric capture time.");
		try {
			colCaptureTime.getColumn().setImage(AdminDashboardActivator.getImageDescriptor("captureTime.png").createImage());
		}
		catch(Exception e) {
			colCaptureTime.getColumn().setText(titles[3]);
		}
		
		
		
		final TableViewerColumn colWatchedStatus = new TableViewerColumn(v, SWT.NONE);
		colWatchedStatus.setLabelProvider(new MetricsWatchedStatusLabelProvider(callingNode));
		colWatchedStatus.setEditingSupport(new WatchedMetricsThresholdEditingSupport(v, callingNode, WatchedMetricsThresholdEditingSupport.MIN));
		colWatchedStatus.getColumn().setToolTipText("Watched metrics are constantly polled for new values and made visible as time series via the 'Watched Metrics' tab.\nYou can watch "+AdminConstants.METRICS_MAXIMUM_WATCHABLE_METRICS+" metrics simultaneously.");
		colWatchedStatus.setEditingSupport(new MetricsViewEditingSupport(v, callingNode));
		try {
			colWatchedStatus.getColumn().setImage(AdminDashboardActivator.getImageDescriptor("watchStatus.png").createImage());
		}
		catch(Exception e) {
			colWatchedStatus.getColumn().setText(titles[4]);
		}
		
		
		
		
		metricsContainer.addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent e) {
				Table table = metricTableViewer.getTable();
				Rectangle area = metricsContainer.getClientArea();
				Point size = table.computeSize(SWT.DEFAULT, SWT.DEFAULT);
				ScrollBar vBar = table.getVerticalBar();
				int width = area.width - table.computeTrim(0,0,0,0).width - vBar.getSize().x;
				if (size.y > area.height + table.getHeaderHeight()) {
					// Subtract the scrollbar width from the total column width
					// if a vertical scrollbar will be required
					Point vBarSize = vBar.getSize();
					width -= vBarSize.x;
				}
				Point oldSize = table.getSize();
				if (oldSize.x > area.width) {
					// table is getting smaller so make the columns 
					// smaller first and then resize the table to
					// match the client area width
					colName.getColumn().setWidth((int) Math.round(width * 0.4));					
					colValue.getColumn().setWidth((int) Math.round(width * 0.15));
					colUnit.getColumn().setWidth((int) Math.round(width * 0.15));
					colCaptureTime.getColumn().setWidth((int) Math.round(width * 0.25));
					colWatchedStatus.getColumn().setWidth((int) Math.round(width * 0.05));
					table.setSize(area.width, area.height);
				} else {
					// table is getting bigger so make the table 
					// bigger first and then make the columns wider
					// to match the client area width
					table.setSize(area.width, area.height);
					colName.getColumn().setWidth((int) Math.round(width * 0.4));					
					colValue.getColumn().setWidth((int) Math.round(width * 0.15));
					colUnit.getColumn().setWidth((int) Math.round(width * 0.15));
					colCaptureTime.getColumn().setWidth((int) Math.round(width * 0.25));
					colWatchedStatus.getColumn().setWidth((int) Math.round(width * 0.05));
					table.setSize(area.width, area.height);
				}
			}
		});
	
		
		
		
		
		// column sorters	
		
		// sort by name
		new SystemMetricsColumnViewerSorter(v, colName) {
			protected int doCompare(Viewer viewer, Object e1, Object e2) {
				MetricValue p1 = (MetricValue) e1;
				MetricValue p2 = (MetricValue) e2;
				
				return p1.getName().compareToIgnoreCase(p2.getName());
			};
		};
		
		// sort by value
		new SystemMetricsColumnViewerSorter(v, colValue) {
			protected int doCompare(Viewer viewer, Object e1, Object e2) {
				MetricValue p1 = (MetricValue) e1;
				MetricValue p2 = (MetricValue) e2;
				
				try {
					double v1 = Double.parseDouble(p1.getValue());
					double v2 = Double.parseDouble(p2.getValue());
					
					return Double.compare(v1, v2);
				}
				catch(NumberFormatException e) {
					//
				}
				
				return 0;
			};
		};
		
		// sort by unit
		new SystemMetricsColumnViewerSorter(v, colUnit) {
			protected int doCompare(Viewer viewer, Object e1, Object e2) {
				MetricValue p1 = (MetricValue) e1;
				MetricValue p2 = (MetricValue) e2;
				
				return p1.getUnits().compareToIgnoreCase(p2.getUnits());
			};
		};
		
		// sort by capture time
		new SystemMetricsColumnViewerSorter(v, colCaptureTime) {
			protected int doCompare(Viewer viewer, Object e1, Object e2) {
				MetricValue p1 = (MetricValue) e1;
				MetricValue p2 = (MetricValue) e2;
				
				double time1 = p1.getTimestamp().getTimeInMillis();
				double time2 = p2.getTimestamp().getTimeInMillis();
				
				return Double.compare(time1, time2);
			};
		};
		
		// sort by watched status
		new SystemMetricsColumnViewerSorter(v, colWatchedStatus) {
			protected int doCompare(Viewer viewer, Object e1, Object e2) {
				MetricValue p1 = (MetricValue) e1;
				MetricValue p2 = (MetricValue) e2;
				
				boolean watched1 = MetricDataProvider.getInstance(callingNode).isWatched(p1.getName());
				boolean watched2 = MetricDataProvider.getInstance(callingNode).isWatched(p2.getName());
				
				if(watched1 && !watched2) return 1;
				else if(!watched1 && watched2) return -1;
				else return 0;
			};
		};
		
		
			
	}

	private void populateCategories() {
		
		
		UIJob wsJob = new UIJob(this.getClass().getName()+": populateCategories()") {
	
			@Override
			public IStatus runInUIThread(IProgressMonitor arg0) {
				
				try {
					String[] categories = AdminServiceAdapter.getMetricCategories(callingNode);
					
					categorySelectionDropdown.add("all");
					categorySelectionDropdown.add("---");
					for(String cat:categories) {
						categorySelectionDropdown.add(cat);
					}
					
					if(categorySelectionDropdown.getItemCount()>0)
						categorySelectionDropdown.select(categoryLastSelected);
					
					return Status.OK_STATUS;
				}
				catch(Exception e) {
					AdminDashboardActivator.log(IStatus.ERROR, "Couldn't retrieve metric categories", e);
					return Status.CANCEL_STATUS;
				}
			}
			
		};
		
		wsJob.schedule();		
	}

	private void populateRetrievedMetricListTable(final String... categories) {		
		UIJob job = new UIJob(this.getClass().getName()+": populateRetrievedMetricListTable(String...)") {
	
			@Override
			public IStatus runInUIThread(IProgressMonitor arg0) {
				try {					
					boolean getAllMetrics = false;
					if(categories[0].equalsIgnoreCase("all")) getAllMetrics = true;
					else if(categories[0].equalsIgnoreCase("---")) return Status.OK_STATUS;
					else if(categories[0].equalsIgnoreCase("")) return Status.OK_STATUS;
					
					MetricValue[] metricValues = AdminServiceAdapter.getMetrics(callingNode, categories, new String[0], getAllMetrics);
					lastRetrievedMetricValues.clear();
					lastRetrievedMetricValues = new ArrayList<MetricValue>();
					
					for(MetricValue mv:metricValues) {
						lastRetrievedMetricValues.add(mv);
						String value = mv.getValue();						
						try {
							Double valueAsDouble = Double.parseDouble(value);							
						}
						catch(NumberFormatException e) {
							MetricDataProvider.getInstance(callingNode).setMetricUnwatchable(mv.getName());
						}
						
						
					}
					
					metricTableViewer.setInput(lastRetrievedMetricValues);
					
					return Status.OK_STATUS;
					
				} catch (Exception e) {
					AdminDashboardActivator.log(IStatus.ERROR, "Couldn't retrieve metric categories", e);
					return Status.CANCEL_STATUS;
				} 
			}			
		};
		
		job.schedule();
		
		
		
	}

	private void createWatchedMetricsContainer(Composite parent) {		
		watchedMetricsContainer = new Group(parent, SWT.NONE);
		watchedMetricsContainer.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true));		
		watchedMetricsContainer.setLayout(new FormLayout());
		
		theChart = new Group(watchedMetricsContainer, SWT.NONE);
		FormData fd = new FormData();
		fd.left = new FormAttachment(0,10);
		fd.right = new FormAttachment(100,-10);
		fd.top = new FormAttachment(0, 10);
		fd.bottom = new FormAttachment(70,0);
		theChart.setLayoutData(fd);
		chartLegend = new Group(watchedMetricsContainer, SWT.NONE);
		fd = new FormData();
		fd.left = new FormAttachment(0,10);
		fd.top = new FormAttachment(theChart,10);
		fd.bottom = new FormAttachment(100,-10);
		fd.right = new FormAttachment(100,-10);
		chartLegend.setLayoutData(fd);
		
		theChart.setLayout(new FormLayout());
		
		chartControls = new Group(theChart, SWT.NONE);
			GridLayout layout_chartControls = new GridLayout(8, false);
			layout_chartControls.horizontalSpacing = 9;
			chartControls.setLayout(layout_chartControls);		
			GridData ld_chartControls = new GridData();
			ld_chartControls.horizontalAlignment = SWT.RIGHT;
			fd = new FormData();
			fd.top = new FormAttachment(0,10);
//			fd.bottom bottom = new FormAttachment()
			fd.right = new FormAttachment(100,-10);

			chartControls.setLayoutData(fd);
			addChartControls();
		
			
			addChart();
			chartLegend.setLayout(new GridLayout(2, false));
			addChartLegend();	

			watchedMetricsContainer.layout(); 
	}
	
	private void addChartControls() {		
		Composite zoomButtons = new Composite(chartControls, SWT.NONE);
		zoomButtons.setLayoutData(new GridData());
		zoomButtons.setLayout(new GridLayout());		
		
			zoomOuter = createButton(zoomButtons, AdminConstants.METRICS_CHART_ZOOM_OUT, "", false);
			zoomOuter.setImage(AdminDashboardActivator.getImageDescriptor("zoom_out.png").createImage());
			zoomOuter.setToolTipText("zoom out");
			zoomOuter.setLayoutData(new GridData());
			
			viewResetter = createButton(zoomButtons, AdminConstants.METRICS_CHART_RESET_VIEW, "", false);
			viewResetter.setImage(AdminDashboardActivator.getImageDescriptor("zoom_reset.png").createImage());
			viewResetter.setToolTipText("reset zoom");
			viewResetter.setLayoutData(new GridData());
			
			zoomInner = createButton(zoomButtons, AdminConstants.METRICS_CHART_ZOOM_IN, "", false);
			zoomInner.setImage(AdminDashboardActivator.getImageDescriptor("zoom_in.png").createImage());
			zoomInner.setToolTipText("zoom in");
			zoomInner.setLayoutData(new GridData());
			
		
		Group inputControls = new Group(chartControls, SWT.NONE);
		inputControls.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		GridLayout layout_inputControls = new GridLayout(3, false);
		layout_inputControls.horizontalSpacing = 5;
		inputControls.setLayout(layout_inputControls);
			
			refreshInterval_text = new Label(inputControls, SWT.NONE);
			refreshInterval_text.setText("Polling Interval");
			refreshInterval_text.setLayoutData(new GridData());
			
			refreshInterval_input = new Text(inputControls, SWT.NONE);
			refreshInterval_input.setEnabled(true);
			refreshInterval_input.setText(Integer.toString(MetricDataProvider.getInstance(callingNode).getRefreshInterval()));
			GridData ld_refreshInterval_input = new GridData();
			ld_refreshInterval_input.widthHint = 30;
			refreshInterval_input.setLayoutData(ld_refreshInterval_input);
					
			refreshInterval_timeUnit = new Label(inputControls, SWT.NONE);
			refreshInterval_timeUnit.setText("sec");
			refreshInterval_timeUnit.setLayoutData(new GridData());
		
			
		Composite inputCommitters = new Composite(chartControls, SWT.NONE);
		inputCommitters.setLayoutData(new GridData());
		inputCommitters.setLayout(new GridLayout());
		
			refreshInterval_setter = createButton(inputCommitters, AdminConstants.METRICS_SET_REFRESH_INTERVAL, "set", false);			
			refreshInterval_setter.setLayoutData(new GridData());
			
			timeSeriesResetter = createButton(inputCommitters, AdminConstants.METRICS_RESET_TIMESERIES, "reset", false);
			timeSeriesResetter.setToolTipText("Resets all time series and flushes their collected data locally.");
			timeSeriesResetter.setLayoutData(new GridData());	
	}
	
	private void addChart() {
		
		// VERTICAL CHART SCROLLERS
		verticalScrollers = new Composite(theChart, SWT.NONE);
		verticalScrollers.setLayout(new GridLayout(1, true));
			chart_upScroller = createButton(verticalScrollers, AdminConstants.METRICS_CHART_UP, "", true);			
			chart_upScroller.setImage(AdminDashboardActivator.getImageDescriptor("chart_up.png").createImage());			
			chart_upScroller.setLayoutData(new GridData(30,30));						
			chart_downScroller = createButton(verticalScrollers, AdminConstants.METRICS_CHART_DOWN, "", false);
			chart_downScroller.setImage(AdminDashboardActivator.getImageDescriptor("chart_down.png").createImage());			
			chart_downScroller.setLayoutData(new GridData(30,30));
		FormData fd = new FormData();
		fd.left = new FormAttachment(0,10);
		fd.right = new FormAttachment(0,50);
		fd.top = new FormAttachment(50,-30);
			verticalScrollers.setLayoutData(fd);
			
		((GridLayout) verticalScrollers.getLayout()).verticalSpacing=GridData.VERTICAL_ALIGN_CENTER;
		((GridLayout) verticalScrollers.getLayout()).numColumns=1;		
						
		// HORIZONTAL CHART SCROLLERS
		horizontalScrollers = new Composite(theChart, SWT.NONE);
		horizontalScrollers.setLayout(new GridLayout());	
		
		
			chart_leftScroller = createButton(horizontalScrollers, AdminConstants.METRICS_CHART_LEFT, "", false);
			chart_leftScroller.setImage(AdminDashboardActivator.getImageDescriptor("chart_left.png").createImage());			
			GridData ld_chart_leftScroller = new GridData(30,30);
			chart_leftScroller.setLayoutData(ld_chart_leftScroller);			
			
			chart_rightScroller = createButton(horizontalScrollers, AdminConstants.METRICS_CHART_RIGHT, "", false);
			chart_rightScroller.setImage(AdminDashboardActivator.getImageDescriptor("chart_right.png").createImage());
			chart_rightScroller.setLayoutData(new GridData(30,30));
			
		((GridLayout) horizontalScrollers.getLayout()).numColumns = 2;
		
		fd = new FormData();
		fd.top = new FormAttachment(100,-50);
		fd.left = new FormAttachment(50,-30);
		fd.bottom = new FormAttachment(100,-10);
		horizontalScrollers.setLayoutData(fd);
			
		// CHART
		boolean chartWasDisposed = false;
		if(chart!=null) chartWasDisposed = chart.isDisposed();
		
		chart = new Chart(theChart, SWT.NONE);
		chart.getTitle().setVisible(false);
		
		fd = new FormData();
		fd.left = new FormAttachment(verticalScrollers,10);
		fd.top = new FormAttachment(chartControls,10);
		fd.bottom = new FormAttachment(horizontalScrollers,-10);
		fd.right = new FormAttachment(100,-10);
		chart.setLayoutData(fd);
		
		Color c = new Color(Display.getCurrent(), new RGB(82, 82, 114));			
		chart.getLegend().setVisible(false);
		chart.getAxisSet().getXAxis(0).getTitle().setVisible(false);
		chart.getAxisSet().getYAxis(0).getTitle().setVisible(false);
		
		chart.getAxisSet().getXAxis(0).getTick().setForeground(c);
		chart.getAxisSet().getYAxis(0).getTick().setForeground(c);
		
		// this dialog is being reopened; re-init time series
		if(chartWasDisposed) {
			MetricDataProvider.getInstance(callingNode).triggerUpdate(this);
			chart.getAxisSet().adjustRange();
			chart.redraw();
		}
		
	}
	
	
	private void addChartLegend() {
		Group chartLegendButtons = new Group(chartLegend, SWT.NONE);
		chartLegendButtons.setLayoutData(new GridData());
		GridLayout layout_chartLegendButtons = new GridLayout(2, true);		
		chartLegendButtons.setLayout(layout_chartLegendButtons);
		
		showAller = createButton(chartLegendButtons, AdminConstants.METRICS_DRAW_WATCHED_METRICS, "all +", false);		
		showAller.setVisible(true);
		GridData arp = new GridData();
		arp.verticalAlignment = SWT.TOP;
		showAller.setLayoutData(arp);
		
		cloakAller = createButton(chartLegendButtons, AdminConstants.METRICS_UNDRAW_WATCHED_METRICS, "all -", false);
		cloakAller.setVisible(true);
		GridData arp2 = new GridData();
		arp2.verticalAlignment = SWT.TOP;
		cloakAller.setLayoutData(arp2);
		
		GridLayout layout_chartLegend = (GridLayout) chartLegend.getLayout();
		layout_chartLegendButtons.numColumns = 2;
		layout_chartLegend.numColumns = 1;
		
		chartLegend_viewer = new TableViewer(chartLegend, SWT.HIDE_SELECTION | SWT.H_SCROLL | SWT.V_SCROLL);
		chartLegend_viewer.getTable().forceFocus();
		chartLegend_viewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));
			
		createColumnsForChartLegend();
		chartLegend_viewer.setContentProvider(new WatchedMetricsContentProvider());
		
		populateChartLegendTable();
	}
	
	
	
	
	private void createColumnsForChartLegend() {
		final TableViewer v = chartLegend_viewer;
		
		createContextMenu(v);
				
		
		v.addSelectionChangedListener(new ISelectionChangedListener(
				) {
			
			public void selectionChanged(SelectionChangedEvent arg0) {
				IStructuredSelection selection = (IStructuredSelection) arg0.getSelection();
		        Object[] objects = selection.toArray();
		        
		        // restore color
		        if(timeSeries_selected!=null) {
		        	ILineSeries penultimatelySelectedSeries = (ILineSeries) chart.getSeriesSet().getSeries(timeSeries_selected);
		        	if(penultimatelySelectedSeries!=null) {
		        		Color color = timeSeries_colorsCurrentlyInUse.get(timeSeries_selected);
		        		penultimatelySelectedSeries.setLineColor(color);
			        	penultimatelySelectedSeries.setLineWidth(2); 
		        	}
		        }
		        
		        
		        if(objects.length>0) {
					MetricValue theMetric = (MetricValue) objects[0];
					
					// log currently selected metric
					timeSeries_selected=theMetric.getName();
					ILineSeries currentlySelectedSeries=null;
					
					if(currentlySelectedSeries!=null) {
						currentlySelectedSeries = (ILineSeries) chart.getSeriesSet().getSeries(timeSeries_selected);										
						
				        // set selection color, thicken line
						currentlySelectedSeries.setLineColor(timeSeries_selectedColor);
						currentlySelectedSeries.setLineWidth(3);			
					}
							
					
					// redraw
					chart.redraw();
		        }
			}
		});
		
		v.getTable().setHeaderVisible(true);
		String[] titles = new String[]{ "Name", "Unit", "Show", "Min THRSH", "Max THRSH"};
				
		final TableViewerColumn colName = new TableViewerColumn(v, SWT.NONE);
		colName.setLabelProvider(new CommonMetricNameLabelProvider(callingNode, true));		
		colName.getColumn().setWidth(230);		
		colName.getColumn().setResizable(true);
		colName.getColumn().setText(titles[0]);
		
		final TableViewerColumn colUnit = new TableViewerColumn(v, SWT.NONE);		
		colUnit.setLabelProvider(new CommonMetricUnitLabelProvider(callingNode, true));		
		colUnit.getColumn().setWidth(150);		
		colUnit.getColumn().setResizable(true);		
		colUnit.getColumn().setText(titles[1]);
				
		final TableViewerColumn colDrawStatus = new TableViewerColumn(v, SWT.NONE);
		colDrawStatus.setLabelProvider(new WatchedMetricsDrawStatusLabelProvider(callingNode));
		
		colDrawStatus.getColumn().setWidth(50);
		colDrawStatus.setEditingSupport(new WatchedMetricsDrawStatusEditingSupport(v, callingNode));
		try {
			colDrawStatus.getColumn().setImage(AdminDashboardActivator.getImageDescriptor("draw.png").createImage());
		}
		catch(Exception e) {
			colDrawStatus.getColumn().setText(titles[2]);
		}
		
		colDrawStatus.getColumn().setToolTipText("(Un-)Plot selected metric time series.");
		
		final TableViewerColumn colThresholdMin = new TableViewerColumn(v, SWT.NONE);		
		colThresholdMin.setLabelProvider(new WatchedMetricsThresholdLabelProvider(callingNode, WatchedMetricsThresholdLabelProvider.MIN));
		colThresholdMin.getColumn().setText(titles[3]);
		colThresholdMin.getColumn().setWidth(100);
		colThresholdMin.setEditingSupport(new WatchedMetricsThresholdEditingSupport(v, callingNode, WatchedMetricsThresholdEditingSupport.MIN));
		colThresholdMin.getColumn().setToolTipText("Issues notification on falling below specified lower threshold.\nNote: notifications only occur on state change.");		
						
		final TableViewerColumn colThresholdMax = new TableViewerColumn(v, SWT.NONE);
		colThresholdMax.setLabelProvider(new WatchedMetricsThresholdLabelProvider(callingNode, WatchedMetricsThresholdLabelProvider.MAX));
		colThresholdMax.getColumn().setText(titles[4]);
		colThresholdMax.getColumn().setWidth(100);
		colThresholdMax.setEditingSupport(new WatchedMetricsThresholdEditingSupport(v, callingNode, WatchedMetricsThresholdEditingSupport.MAX));
		colThresholdMax.getColumn().setToolTipText("Issues notification on exceeding specified upper threshold.\nNote: notifications only occur on state change.");
				
		v.setColumnProperties(titles);		
		
		
		
		chartLegend.addControlListener(new ControlAdapter() {
			public void controlResized(ControlEvent e) {
				Table table = chartLegend_viewer.getTable();
				Rectangle area = chartLegend.getClientArea();
				Point size = table.computeSize(SWT.DEFAULT, SWT.DEFAULT);
				ScrollBar vBar = table.getVerticalBar();
				int width = area.width - table.computeTrim(0,0,0,0).width - vBar.getSize().x;
				if (size.y > area.height + table.getHeaderHeight()) {
					// Subtract the scrollbar width from the total column width
					// if a vertical scrollbar will be required
					Point vBarSize = vBar.getSize();
					width -= vBarSize.x;
				}
				Point oldSize = table.getSize();
				if (oldSize.x > area.width) {
					// table is getting smaller so make the columns 
					// smaller first and then resize the table to
					// match the client area width
					colName.getColumn().setWidth((int) Math.round(width * 0.45));
					colUnit.getColumn().setWidth((int) Math.round(width * 0.2));
					colDrawStatus.getColumn().setWidth((int) Math.round(width * 0.05));
					colThresholdMin.getColumn().setWidth((int) Math.round(width * 0.15));
					colThresholdMax.getColumn().setWidth((int) Math.round(width * 0.15));
					table.setSize(area.width, area.height);
				} else {
					// table is getting bigger so make the table 
					// bigger first and then make the columns wider
					// to match the client area width
					table.setSize(area.width, area.height);
					colName.getColumn().setWidth((int) Math.round(width * 0.45));
					colUnit.getColumn().setWidth((int) Math.round(width * 0.2));
					colDrawStatus.getColumn().setWidth((int) Math.round(width * 0.05));
					colThresholdMin.getColumn().setWidth((int) Math.round(width * 0.15));
					colThresholdMax.getColumn().setWidth((int) Math.round(width * 0.15));
					table.setSize(area.width, area.height);
				}
			}
		});
		
		
		
		
		ColumnViewerEditorActivationStrategy activationSupport = new ColumnViewerEditorActivationStrategy(v) {
			protected boolean isEditorActivationEvent(ColumnViewerEditorActivationEvent event) {
				return event.eventType == ColumnViewerEditorActivationEvent.TRAVERSAL
						|| event.eventType == ColumnViewerEditorActivationEvent.MOUSE_CLICK_SELECTION
						|| event.eventType == ColumnViewerEditorActivationEvent.PROGRAMMATIC
						|| (event.eventType == ColumnViewerEditorActivationEvent.KEY_PRESSED && event.keyCode == KeyLookupFactory
								.getDefault().formalKeyLookup(IKeyLookup.ENTER_NAME));
			}
		};
		activationSupport.setEnableEditorActivationWithKeyboard(true);
		
		TableViewerEditor.create(v, activationSupport, ColumnViewerEditor.TABBING_VERTICAL | ColumnViewerEditor.KEYBOARD_ACTIVATION);
	}

	private void populateChartLegendTable() {
		chartLegend_viewer.setInput(timeSeries_lastlyWatched);		
	}

	private void createContextMenu(final TableViewer v) {
		final MenuManager mgr = new MenuManager();
		mgr.setRemoveAllWhenShown(true);
		
		mgr.addMenuListener(new IMenuListener() {
			
			public void menuAboutToShow(IMenuManager manager) {				
				IStructuredSelection selection = (IStructuredSelection) v.getSelection();
								
				if(!selection.isEmpty()) {
					Object element = selection.getFirstElement();			
					MetricValue mv = (MetricValue) element;
					
					try {
						int logLevelMin = ThresholdObserver.getInstance(callingNode).getLogLevelMin(mv.getName());
						
						if(logLevelMin==AdminConstants.METRICS_LOGLEVEL_INFO)
							mgr.add(new SetMetricLogToWarningAction(SetMetricLogToWarningAction.MIN, mv.getName(), callingNode));
						
						else if(logLevelMin==AdminConstants.METRICS_LOGLEVEL_WARNING)
							mgr.add(new SetMetricLogToInfoAction(SetMetricLogToInfoAction.MIN, mv.getName(), callingNode));
					}
					catch(NullPointerException e) {
						
					}
					
					
					try {
						int logLevelMax = ThresholdObserver.getInstance(callingNode).getLogLevelMax(mv.getName());						
						
						if(logLevelMax==AdminConstants.METRICS_LOGLEVEL_INFO)
							mgr.add(new SetMetricLogToWarningAction(SetMetricLogToWarningAction.MAX, mv.getName(), callingNode));
						
						else if(logLevelMax==AdminConstants.METRICS_LOGLEVEL_WARNING)
							mgr.add(new SetMetricLogToInfoAction(SetMetricLogToWarningAction.MAX, mv.getName(), callingNode));
					}
					catch(Exception e) {
						
					}
					
					mgr.add(new ClearMetricDataAction(callingNode, mv.getName()));
					
				}									
			}
		});
		
		v.getControl().setMenu(mgr.createContextMenu(v.getControl()));
	}

	private void updateTimeSeriesData(String metricName, Date[] xValues, double[] yValues) {
		if(open) {
			boolean visible = false;
			
			if(timeSeries_visibility.containsKey(metricName)) visible = timeSeries_visibility.get(metricName);
			
			ILineSeries series = (ILineSeries) chart.getSeriesSet().getSeries(metricName);
			if(series==null) {
				assignColor(metricName);
				Color color = timeSeries_colorsCurrentlyInUse.get(metricName);				
				series = (ILineSeries) chart.getSeriesSet().createSeries(SeriesType.LINE, metricName);
				series.getLabel().setForeground(color);
				series.getLabel().setVisible(false);				
				series.setLineColor(color);
				series.setLineWidth(2);
				series.setSymbolSize(1);				
			}
				
			series.setVisible(visible);
			
			series.setXDateSeries(xValues);
			series.setYSeries(yValues);
			
			if(!chart_manuallyAdjusted) 
				chart.getAxisSet().adjustRange();
			
			chart.redraw();
		}			
	}

	
	public void adjustChart(String metricName) {
		chart.getAxisSet().adjustRange();
		chart.redraw();
	}

	public void resetCollectedData() {
		ISeries[] series = chart.getSeriesSet().getSeries();
		for(ISeries s:series) {
			s.setXDateSeries(new Date[0]);
			s.setYSeries(new double[0]);
		}
		
		chart.redraw();
	}

	
	public void update(MetricDataUpdate[] updatedValues) {
		if(open) {
			for(MetricDataUpdate update:updatedValues) {
				String metricName = update.getMetricName();
				MetricValue[] values = update.getValues();
				
				Date[] xValues = new Date[values.length];
				double[] yValues = new double[values.length];
				Date date = null;
				for(int i=0; i<values.length; i++) {					
					try {
						date = values[i].getTimestamp().getTime();
					}
					catch(NullPointerException e) {
						e.printStackTrace();
						System.out.println("ARP");
					}
					
					String strValue = values[i].getValue();
					double value = Double.parseDouble(strValue);
					
					xValues[i] = date;
					yValues[i] = value;					
				}
							
				updateTimeSeriesData(metricName, xValues, yValues);
			}
			
			if(!firstUpdateDone) {
				chartLegend_viewer.refresh();
				firstUpdateDone = true;
			}
		}
	}

	
	
	
	/////////////////////////////////////////////////////////////////////////////////
	
	
	
	public void watch(String metricName, String metricUnit) {
		
		// create time series		
		assignColor(metricName);
		Color color = timeSeries_colorsCurrentlyInUse.get(metricName);
		createTimeSeries(metricName, color, 0);
		
		MetricValue mv = MetricValue.Factory.newInstance();
		mv.setName(metricName);
		mv.setUnits(metricUnit);
		mv.setTimestamp(Calendar.getInstance());
		
		timeSeries_values.put(metricName, mv);
		timeSeries_lastlyWatched.add(mv);						
		chartLegend_viewer.add(mv);
		
		showTimeSeries(metricName);
		
		chart.redraw();
		chart.getAxisSet().adjustRange();		
		
		timeSeriesLegend_contextMenuInfo_MIN.put(metricName, true);
		timeSeriesLegend_contextMenuInfo_MAX.put(metricName, true);		
	}
	
	
	private ILineSeries createTimeSeries(String metricName, Color color, int style) {
		// create time series	
		ILineSeries series = (ILineSeries) chart.getSeriesSet().createSeries(SeriesType.LINE, metricName);
		
		series.getLabel().setForeground(color);
		series.getLabel().setVisible(false);
		series.setLineColor(color);
		series.setLineWidth(2);
		series.setSymbolSize(1);
		
		switch(style) {
			case 1: series.setLineStyle(LineStyle.DOT); break;
			default: break;
		}
		
		return series;
	}
	
	
	private void removeTimeSeries(String metricName) {
		ISeries series = chart.getSeriesSet().getSeries(metricName);
		if(series!=null) {
			series.setVisible(false);			
			chart.getSeriesSet().deleteSeries(metricName);
			series =  null;
		}
		
		chart.redraw();
		chart.getAxisSet().adjustRange();
	}
	
	
	
	public void unwatch(String metricName) {
		
		// remove corresponding time series from chart
		removeTimeSeries(metricName);
		
		MetricValue mv = timeSeries_values.remove(metricName);
		timeSeries_lastlyWatched.remove(mv);
		chartLegend_viewer.remove(mv);
		metricTableViewer.refresh();		
		chartLegend_viewer.refresh();
			
		Color color = timeSeries_colorsCurrentlyInUse.remove(metricName);
		timeSeries_colorsAvailable.add(color);
		
		timeSeries_visibility.remove(metricName);
		
		timeSeriesLegend_contextMenuInfo_MIN.remove(metricName);
		timeSeriesLegend_contextMenuInfo_MAX.remove(metricName);
	}
	
	
	
	
	public boolean isDrawn(String metricName) {
		ISeries series = chart.getSeriesSet().getSeries(metricName);
		
		if(series==null) return false;
		
		else {
			boolean visible = series.isVisible();
			return visible;
		}
	}
	

	public void showTimeSeries(final String metricName) {		
		chart.getSeriesSet().getSeries(metricName).setVisible(true);
		timeSeries_visibility.put(metricName, true);
		chart.redraw();			
		chartLegend_viewer.refresh();
	}
	

	public void hideTimeSeries(final String metricName) {
		chart.getSeriesSet().getSeries(metricName).setVisible(false);
		timeSeries_visibility.put(metricName, false);
		chart.redraw();				
		chartLegend_viewer.refresh();
	}	
	
	
	
	
	/////////////////////////////////////////////////////////////////////////////////
	
	
	public Color getColor(String metricName) {
		return timeSeries_colorsCurrentlyInUse.get(metricName);
	}	
	
	private void createColors() {		
		timeSeries_selectedColor = Display.getCurrent().getSystemColor(SWT.COLOR_LIST_SELECTION);
		
		Color color = new Color(Display.getCurrent(), new RGB(0,0,0));
		timeSeries_colorsAvailable.add(color);

		// 30 colors are needed for 30 watchable metrics / time series, but only 29 additional ones required
		int numColors = AdminConstants.METRICS_MAXIMUM_WATCHABLE_METRICS - 1;
		timeSeries_lowerColorBound = timeSeries_lowerColorBound + 8;
		
		for(int i=0; i<numColors; i++) {
			int r = (int) (timeSeries_lowerColorBound + Math.random() * (timeSeries_upperColorBound-timeSeries_lowerColorBound));
			int g = (int) (timeSeries_lowerColorBound + Math.random() * (timeSeries_upperColorBound-timeSeries_lowerColorBound));
			int b = (int) (timeSeries_lowerColorBound + Math.random() * (timeSeries_upperColorBound-timeSeries_lowerColorBound));
			
			// enforce exclusivity of metric selection color
			if(r==timeSeries_selectedColor.getRed() || g==timeSeries_selectedColor.getGreen() || b==timeSeries_selectedColor.getBlue()) {
				i--;
				continue;
			}
			
			color = new Color(Display.getCurrent(), new RGB(r,g,b));			
			timeSeries_colorsAvailable.add(color);
		}		
	}
	
	
	private void assignColor(String metricName) {
		if(!timeSeries_colorsCurrentlyInUse.containsKey(metricName)) {
			Color color = timeSeries_colorsAvailable.remove(0);
			timeSeries_colorsCurrentlyInUse.put(metricName, color);
		}
			
	}
}
