package de.fzj.unicore.rcp.admindashboard;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.progress.UIJob;

import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;

/**
 * Subscribes to MetricDataSubscriber to observe threshold behavior of watched metrics.<br>
 * Issues notifications upon threshold breaches (bilateral).
 * @author j.daivandy@fz-juelich.de
 *
 */
public class ThresholdObserver implements MetricDataSubscriber {
	
	private static Map<SecuredNode, ThresholdObserver> observers = new HashMap<SecuredNode, ThresholdObserver>();
	private SecuredNode callingNode;	
	private static final int DOWN=-1;
	private static final int CONSTANT=0;
	private static final int UP=1;
	
	private ThresholdObserver(SecuredNode callingNode) {
		this.callingNode = callingNode;
		MetricDataProvider.getInstance(callingNode).subscribe(this);
	}
	
	public static ThresholdObserver getInstance(SecuredNode callingNode) {
		if(!observers.containsKey(callingNode)) observers.put(callingNode, new ThresholdObserver(callingNode));
		
		return observers.get(callingNode);
	}
	
	
	private Map<String, Double> minValues = new HashMap<String, Double>();
	private Map<String, Double> maxValues = new HashMap<String, Double>();
	private Map<String, Integer> lastStatesMin = new HashMap<String, Integer>();
	private Map<String, Integer> lastStatesMax = new HashMap<String, Integer>();
	private Map<String, Integer> logLevelsMin = new HashMap<String, Integer>();
	private Map<String, Integer> logLevelsMax = new HashMap<String, Integer>();
	
	
	public void setThresholdMin(String metricName, double thresholdMin) {
		if(!lastStatesMin.containsKey(metricName)) {
			lastStatesMin.put(metricName, CONSTANT);
			logLevelsMin.put(metricName, AdminConstants.METRICS_LOGLEVEL_INFO);
		}
		minValues.put(metricName, thresholdMin);		
	}
	
	public Double getThresholdMin(String metricName) {		
		return minValues.get(metricName);
	}
	
	
	public void setThresholdMax(String metricName, double thresholdMin) {
		if(!lastStatesMax.containsKey(metricName)) {
			lastStatesMax.put(metricName, CONSTANT);
			logLevelsMax.put(metricName, AdminConstants.METRICS_LOGLEVEL_INFO);
		}
		maxValues.put(metricName, thresholdMin);
	}
	
	public Double getThresholdMax(String metricName) {
		return maxValues.get(metricName);
	}

	public void resetCollectedData() {
		
	}

	public void unwatch(String metricName) {
		minValues.remove(metricName);
		maxValues.remove(metricName);		
	}

	public void update(MetricDataUpdate[] updatedValues) {
		for(MetricDataUpdate update:updatedValues) {
			MetricValue[] values = update.getValues();
			double[] yValues = new double[values.length];
			
			for(int i=0; i<values.length; i++) {				
				double v = Double.parseDouble(values[i].getValue());
				yValues[i] = v;
			}
			
			if(yValues.length>0) {				
				double currentValue = yValues[values.length-1];				
				check(update.getMetricName(), currentValue);			
			}
			
		}
		
	}

	public void watch(String metricName, String metricUnit) {
		
	}
	
	private void check(final String metricName, double currentValue) {		
		String msg = null;
		
		if(minValues.get(metricName)!=null) {
			int lastState = lastStatesMin.get(metricName);
			int currentState;
			
			if(currentValue<minValues.get(metricName)) currentState=DOWN;
			else currentState=UP;			
			
			if(lastState!=currentState) {
				if(currentState==UP) msg=metricName + " above MIN threshold (nominal).";
				else if(currentState==DOWN) msg=metricName + " below MIN threshold.\nNo notifications until nominal MIN state has been reached, again.";
				
				int logLevel = logLevelsMin.get(metricName);
				
				switch(logLevel) {
					case AdminConstants.METRICS_LOGLEVEL_INFO: AdminDashboardActivator.log(msg); break;
					case AdminConstants.METRICS_LOGLEVEL_WARNING: {
						final String theMsg = msg;
						UIJob wsJob = new UIJob(Display.getCurrent(), "ThresholdObserver_"+metricName) {

							@Override
							public IStatus runInUIThread(IProgressMonitor arg0) {
								try {
									AdminDashboardActivator.log(IStatus.INFO, theMsg, true);									
									return Status.OK_STATUS;
								}
								catch(Exception e) {
									AdminDashboardActivator.log(IStatus.WARNING, e.getMessage(), true);
									return Status.CANCEL_STATUS;
								}
							}
						};		
						
						wsJob.schedule();
						break;
					}
					default: AdminDashboardActivator.log(msg); break;
				}					
				
				System.out.println(msg);
			}
			
			lastStatesMin.put(metricName, currentState);			
		}
		
		if(maxValues.get(metricName)!=null) {
			int lastState = lastStatesMax.get(metricName);
			int currentState;
			
			if(currentValue<maxValues.get(metricName)) currentState=DOWN;
			else currentState=UP;			
			
			if(lastState!=currentState) {
				if(currentState==UP) msg=metricName + " above MAX threshold.\nNo notifications until nominal MAX state has been reached, again.";
				else if(currentState==DOWN) msg=metricName + " below MAX threshold (nominal).";
				
				
				int logLevel = logLevelsMax.get(metricName);
				
				switch(logLevel) {
					case AdminConstants.METRICS_LOGLEVEL_INFO: AdminDashboardActivator.log(msg); break;
					case AdminConstants.METRICS_LOGLEVEL_WARNING: AdminDashboardActivator.log(IStatus.INFO, msg, true); break;
					default: AdminDashboardActivator.log(msg); break;
				}	
				
				
				System.out.println(msg);
			}
			
			lastStatesMax.put(metricName, currentState);			
		}	
	}
	
	
	public synchronized void deactivateMin(String metricName) {
		minValues.remove(metricName);
		lastStatesMin.remove(metricName);
	}
	
	public boolean isActiveMin(String metricName) {		
		return minValues.containsKey(metricName);
	}
	
	public synchronized void deactivateMax(String metricName) {
		maxValues.remove(metricName);
		lastStatesMax.remove(metricName);
	}
	
	public boolean isActiveMax(String metricName) {		
		return maxValues.containsKey(metricName);
	}
	
	public void setLogLevelMin(String metricName, int level) {
		logLevelsMin.put(metricName, level);
	}
	
	public int getLogLevelMin(String metricName) {
		return logLevelsMin.get(metricName);		
	}
	
	public void setLogLevelMax(String metricName, int level) {
		logLevelsMax.put(metricName, level);
	}
	
	public int getLogLevelMax(String metricName) {
		return logLevelsMax.get(metricName);		
	}
	
	
	

}
