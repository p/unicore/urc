package de.fzj.unicore.rcp.admindashboard.widgets.system;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;

import de.fzj.unicore.rcp.admindashboard.MetricDataProvider;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;

public class MetricsViewEditingSupport extends EditingSupport {
	private CellEditor editor;	
	private SecuredNode callingNode;

	public MetricsViewEditingSupport(TableViewer viewer, SecuredNode caller) {
		super(viewer);
		editor = new CheckboxCellEditor(null, SWT.CHECK | SWT.READ_ONLY);
		callingNode = caller;
	}

	@Override
	protected boolean canEdit(Object element) {
		return true;
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		return editor;
	}

	@Override
	protected Object getValue(Object element) {
		MetricValue metric = (MetricValue) element;		
		return MetricDataProvider.getInstance(callingNode).isWatched(metric.getName());
		
	}

	@Override
	protected void setValue(Object element, Object value) {
		MetricValue metric = (MetricValue) element;
		boolean isWatched = MetricDataProvider.getInstance(callingNode).isWatched(metric.getName());	
		boolean isUnwatchable = MetricDataProvider.getInstance(callingNode).isUnwatchable(metric.getName());
		
		if(isWatched) {
			MetricDataProvider.getInstance(callingNode).unwatch(metric);			
		}
		else if(isUnwatchable) {
			getViewer().refresh();
		}
		else {
			MetricDataProvider.getInstance(callingNode).watch(metric);	
			getViewer().refresh();
		}
			
	
		getViewer().update(element, null);		
	}

}
