package de.fzj.unicore.rcp.admindashboard.widgets.servicemanager;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.admindashboard.AdminDashboardActivator;
import de.fzj.unicore.rcp.admindashboard.AdminToolsContextMenuListener;
import de.fzj.unicore.rcp.admindashboard.nodes.AdminGridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceTreeViewer;

public class ServiceManagerTreeViewer extends ServiceTreeViewer {
	
	public ServiceManagerTreeViewer(Composite parent) {
		super(parent);
	}
	
	public ServiceManagerTreeViewer(Composite parent, int style) {
		super(parent, style);
	}
	
	
	@Override
	protected void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("Available Actions");
		menuMgr.setRemoveAllWhenShown(true);
		contextMenuListener = new AdminToolsContextMenuListener(this);
		menuMgr.addMenuListener(contextMenuListener);

		Menu menu = menuMgr.createContextMenu(getControl());
		getControl().setMenu(menu);
	}
	
	
	@Override
	public void addMonitoringEntry(Node newNode) {
		if(newNode.isTopLevel()) {
			AdminDashboardActivator.log(IStatus.ERROR, "Not adding bookmark: a bookmark for this URI does already exist.");
			return;
		}

		try {
			getGridNode().addBookmark(newNode);
		} catch (Exception e) {
			AdminDashboardActivator.log(IStatus.ERROR, "Not adding bookmark: "+e.getMessage(),e);
		}	

		//persistence
		saveGrid();
		if(!getGridNode().isExpanded())
		{
			PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable()
			{
				public void run() {
					expandToLevel(getGridNode(), 0);
					getGridNode().setExpanded(true);
				}
			});
		}
	}
	
	@Override
	public void saveGrid()
	{
		AdminDashboardActivator.getDefault().saveMonitoredAdminEntries((AdminGridNode) getGridNode());

	}
	
	
	
}
