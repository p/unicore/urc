package de.fzj.unicore.rcp.admindashboard.widgets.system;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Color;

import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;

public class CommonMetricNameLabelProvider extends ColumnLabelProvider {

	private SecuredNode callingNode;
	private boolean watchedMetric=false;
	
	public CommonMetricNameLabelProvider(SecuredNode caller, boolean watched) {
		callingNode = caller;
		watchedMetric = watched;
	}

	
	@Override
	public String getText(Object element) {
		MetricValue mv = (MetricValue) element;
		
		return mv.getName();
	}
	
	@Override
	public Color getForeground(Object element) {
		Color color = null;
		
		if(watchedMetric) {
			MetricValue mv = (MetricValue) element;				
			color = SystemMetricsDialog.getInstance(callingNode).getColor(mv.getName());
		}
		
		else color = super.getForeground(element);
		
		
		return color;		
	}

	@Override
	public Color getBackground(Object arg0) {
		return null;
	}

}
