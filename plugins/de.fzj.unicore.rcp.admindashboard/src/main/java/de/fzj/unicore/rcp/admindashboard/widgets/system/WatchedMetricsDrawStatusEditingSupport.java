package de.fzj.unicore.rcp.admindashboard.widgets.system;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.ui.progress.UIJob;

import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;

public class WatchedMetricsDrawStatusEditingSupport extends EditingSupport {
	private CellEditor editor;	
	private SecuredNode callingNode;
	private Map<String, UIJob> drawJobs = new HashMap<String, UIJob>();	
	private Map<String, UIJob> undrawJobs = new HashMap<String, UIJob>();	

	public WatchedMetricsDrawStatusEditingSupport(ColumnViewer viewer, SecuredNode caller) {
		super(viewer);
		editor = new CheckboxCellEditor(((TableViewer) viewer).getTable(), SWT.CHECK | SWT.READ_ONLY);		
		callingNode = caller;		
	}

	@Override
	protected boolean canEdit(Object element) {
		return true;
	}

	@Override
	protected CellEditor getCellEditor(Object element) {	
		return editor;
	}

	@Override
	protected Object getValue(Object element) {
		Object returnValue = null;
		MetricValue metric = (MetricValue) element;
		returnValue = SystemMetricsDialog.getInstance(callingNode).isDrawn(metric.getName());
	
		return returnValue;
		
	}

	@Override
	protected void setValue(final Object element, Object value) {
		MetricValue metric = (MetricValue) element;
		
		
		boolean isDrawn = SystemMetricsDialog.getInstance(callingNode).isDrawn(metric.getName());		
		final String metricName = metric.getName();
			
		if(isDrawn) {
			UIJob undrawMetricJob = undrawJobs.get(metricName);
			
			if(undrawMetricJob==null) {
				undrawMetricJob = new UIJob("undrawing metric "+metricName+" @"+System.currentTimeMillis()) {

					@Override
					public IStatus runInUIThread(IProgressMonitor arg0) {
						SystemMetricsDialog.getInstance(callingNode).hideTimeSeries(metricName);
						getViewer().update(element, null);

						return Status.OK_STATUS;
					}			
				};
				
				undrawJobs.put(metricName, undrawMetricJob);
			}
			
			undrawMetricJob.schedule();
			
			
			
		}			
		
		else {			
			UIJob drawMetricJob = drawJobs.get(metricName);
			
			if(drawMetricJob==null) {
				drawMetricJob = new UIJob("drawing metric "+metricName+" @"+System.currentTimeMillis()) {

					@Override
					public IStatus runInUIThread(IProgressMonitor arg0) {
						SystemMetricsDialog.getInstance(callingNode).showTimeSeries(metricName);
						getViewer().update(element, null);
						
						//done(Status.OK_STATUS);
						return Status.OK_STATUS;
					}			
				};
				
				drawJobs.put(metricName, drawMetricJob);
			}
			
			drawMetricJob.schedule();
		}
		
	}

}
