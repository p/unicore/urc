/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.admindashboard;


import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.admindashboard.nodes.AdminGridNode;
import de.fzj.unicore.rcp.admindashboard.util.AdminUtils;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.logmonitor.LogActivator;
import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserConstants;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author Jason Milad Daivandy
 */
public class AdminDashboardActivator extends AbstractUIPlugin {

	/** The plug-in ID */
	public static final String PLUGIN_ID = "de.fzj.unicore.rcp.admindashboard";
	public final static String ICONS_PATH = "$nl$/icons/";


	/** The shared instance */
	private static AdminDashboardActivator plugin;
	
	private String adminGridNodeLock = "adminGridNodeLock";
	private AdminGridNode adminGridNode = null;
	

	public static final String ADMIN_MONENTRIES ="Admin_MonitoredEntryName_";
	public static final String ADMIN_MONENTRY_EPRS ="Admin_MonitoredEntryURIs";
	
	

	public AdminDashboardActivator() {
		plugin = this;
	}


	public void start(BundleContext context) throws Exception {
		super.start(context);
		IdentityActivator id = IdentityActivator.getDefault();
		id.init();
		

	}

	
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	public static AdminDashboardActivator getDefault() {
		return plugin;
	}




	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH+path);
	}


	/** 
	 * Log a message via Eclipse logging
	 * Defaults to INFO Level 
	 * @param msg the message to log
	 */
	public static void log(String msg) {
		log(msg, null);
	}
	/** 
	 * Log a message via Eclipse logging
	 * Defaults to INFO Level 
	 * @param msg the message to log
	 * @param e stacktrace to append
	 */
	public static void log(String msg, Exception e) {
		log(Status.INFO, msg, e, getAlarmUser(Status.INFO));
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, getAlarmUser(status));
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param e stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, getAlarmUser(status));
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param alarmUser true means popup should alert user
	 */
	public static void log(int status, String msg, boolean alarmUser) {
		log(status, msg, null, alarmUser);
	}
	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param e stacktrace to append
	 * @param alarmUser true means popup should alert user
	 */
	public static void log(int status, String msg, Exception e, boolean alarmUser) {
		IStatus s = new Status(status, PLUGIN_ID, Status.OK, msg, e);
		//log it
		plugin.getLog().log(s);
		//if user should be alarmed, popup the window
		if(alarmUser) 
			LogActivator.showAlarm(s);
	}

	



	private static boolean getAlarmUser(int status) {
		return LogActivator.getAlarmUser(PLUGIN_ID, status);
	}

	
	public AdminGridNode getAdminGridNode() {

		synchronized (adminGridNodeLock) {
			if(adminGridNode != null) return adminGridNode;
			// deserialize the persisted grid
			// 1. try to load the persisted String
			IDialogSettings s = getDialogSettings();
			adminGridNode = initAdminGridNodeAndBookmarks();

			// create children
			final int numLevels = getPreferenceStore().getInt(ServiceBrowserConstants.P_NUM_LEVELS_ON_STARTUP);
			// refresh Grid!
			if(numLevels > 0)
			{
				Job job = new Job("refreshing Grid") {
					public IStatus run(IProgressMonitor monitor) {
						IStatus status = getAdminGridNode().refresh(numLevels,true,monitor);
						return status;
					}
				};
				job.setUser(false);
				job.schedule();
			}
		}
		return adminGridNode;
	}
	
	
	protected AdminGridNode initAdminGridNodeAndBookmarks()
	{
		AdminGridNode adminGridNode = AdminUtils.createNewAdminGridNode();

		// monitored entries
		IDialogSettings s = getDialogSettings();
		String[] eprs = s.getArray(ADMIN_MONENTRY_EPRS);
		if(eprs != null)
		{
			for(int i=0;i < eprs.length;i++) {
				try {
					String name = s.getArray(ADMIN_MONENTRIES)[i];
					EndpointReferenceType epr = EndpointReferenceType.Factory.parse(eprs[i]);
					Node node = NodeFactory.createNode(epr);
					if(name != null)
					{
						node.setName(name);
					}

					// add to model
					adminGridNode.addBookmark(node);

				} catch (Exception e) {
					log("Could not restore saved Grid root node!",e);
				}
			}
		}
		return adminGridNode;
	}
	
	
	
	
	public void saveMonitoredAdminEntries(AdminGridNode adminGridNode)
	{
		IDialogSettings s = getDialogSettings();
		// save monitored entries
		Node[] entries = adminGridNode.getChildrenArray();//are the monitored entries
		String[] names = new String[entries.length];
		String[] eprs = new String[entries.length];
		for(int i=0;i<entries.length;i++) {
			names[i] = entries[i].getName();
			eprs[i] = entries[i].getEpr().xmlText();
		}
		s.put(ADMIN_MONENTRIES, names);
		s.put(ADMIN_MONENTRY_EPRS, eprs);
	}
	
	
	
}
