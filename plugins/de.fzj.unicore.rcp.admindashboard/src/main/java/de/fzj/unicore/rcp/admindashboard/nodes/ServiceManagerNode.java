/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.admindashboard.nodes;

import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.admindashboard.AdminDashboardActivator;
import de.fzj.unicore.rcp.admindashboard.actions.DeployServiceAction;
import de.fzj.unicore.rcp.admindashboard.util.AdminServiceAdapter;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.uas.util.AddressingUtil;
import de.fzj.unicore.wsrflite.xmlbeans.ServiceEntryDocument.ServiceEntry;


public class ServiceManagerNode extends Node {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3752191108272357307L;
	public static final QName PORTTYPE = new QName("http://www.unicore.fzj.de", ServiceManagerNode.NODE_NAME);
	public static final String TYPE = ServiceManagerNode.NODE_NAME; //PORTTYPE.toString();
	public static final String NODE_NAME = "ServiceManager";
	
	static Map<String, Boolean> serviceTypeMap;

	public ServiceManagerNode(EndpointReferenceType epr) {
		super(epr);		
		serviceTypeMap = new HashMap<String, Boolean>();
		
	}

	public void afterDeserialization()
	{
		super.afterDeserialization();
	}

	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		
		availableActions.add(new DeployServiceAction(this));
	}
	
	@Override
	public void retrieveChildren() {
		removeAllChildren();
		
		Job wsJob = new Job(this.getClass().getName()) {

			@Override
			protected IStatus run(IProgressMonitor arg0) {
				
				try {
					SecuredNode securedNode = (SecuredNode) getParent();
					
					ServiceEntry[] entries = AdminServiceAdapter.getServiceNames(securedNode);
					
					
					String serviceNamespace = null;
					QName servicePort = null;
					EndpointReferenceType epr = null;
					
					boolean isWSRF;
					
					for(ServiceEntry entry:entries) {
						String serviceName = entry.getServiceName();
						serviceNamespace = entry.getTargetNamespace();
						isWSRF = entry.getIsWSRF();
						
						String baseURL = AdminServiceAdapter.getBaseURL(securedNode);
						String serviceURL = baseURL + serviceName;
						
						// a WSRF service needs to be represented by a mock-up node containing its actual WS-resources as child nodes
						if(isWSRF) servicePort = new QName("http://www.fz-juelich.de/unicore/", WSRFParentNode.TYPE);
		 
						else servicePort = new QName(serviceNamespace, serviceName);
						
						serviceTypeMap.put(serviceName, isWSRF);
					
						
						epr = AddressingUtil.newEPR();
						epr.addNewAddress().setStringValue(serviceURL);
						AddressingUtil.addPortType(epr, servicePort);				
						
						Node webServiceNode = NodeFactory.createNode(epr);
			        	addChild(webServiceNode);			        	
			        }
					
					return Status.OK_STATUS;
				}		
		    	catch(Exception e) {		    		
		    		AdminDashboardActivator.log(IStatus.WARNING, "Couldn't retrieve service list.", e);
		    		AdminDashboardActivator.log(IStatus.ERROR, "Couldn't retrieve service list.\n\nCheck connection to \n'"+getParent().getURI()+"'", true);
		    		return Status.CANCEL_STATUS;
		    	}   
			}
			
		};
		
		wsJob.schedule();	
		
		
		 
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		// TODO Auto-generated method stub
		
	}
		
	


}
