/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.admindashboard.nodes;

import java.net.URL;
import java.util.ArrayList;

import javax.xml.namespace.QName;

import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.servicebrowser.actions.EditURIAction;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.actions.RefreshAction;
import de.fzj.unicore.rcp.servicebrowser.actions.RenameAction;
import de.fzj.unicore.rcp.servicebrowser.actions.UnmonitorAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.INodeDataCache;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.WSRFNode;
import de.fzj.unicore.uas.util.AddressingUtil;
import de.fzj.unicore.wsrflite.admin.service.AdminService;


public class AdminServiceNode extends WSRFNode {


	private static final long serialVersionUID = 5447683736650627125L;
	public static final QName PORTTYPE =AdminService.ADMINSERVICE_PORT;
	public static final String TYPE = AdminService.SERVICE_NAME; //PORTTYPE.toString();
	protected String baseURL, baseURLSuffix, bufferedName;

	public AdminServiceNode(EndpointReferenceType epr) {
		super(epr);
		try {
			URL url = new URL(epr.getAddress().getStringValue());
			baseURL = url.getProtocol() + "://" + url.getHost() + ":" + url.getPort();
			baseURLSuffix = " [" + baseURL + "]";
		}
		catch(Exception e) {
			baseURL = "";
			baseURLSuffix = "";
		}
	}

	public void afterDeserialization()
	{
		super.afterDeserialization();
	}

	@Override
	public void resetAvailableActions() {
		availableActions = new ArrayList<NodeAction>();
		
		availableActions.add(new RefreshAction(this));		

		availableActions.add(new EditURIAction(this));
		availableActions.add(new RenameAction(this));
		availableActions.add(new UnmonitorAction(this));
	}
		
	
	
	public String getBaseURL() {
		return baseURL;
	}
	
	
	@Override
	public void setCache(INodeDataCache cache) {
		super.setCache(cache);
		
		addServiceManagerNode();
		addServerConfiguratorNode();
		addSystemNode();
	}
	
	
	@Override
	public void retrieveName() {
		setName(name);
	}
	
	@Override
	public void setName(String name) {
		if(!name.contains(baseURLSuffix)) name = name + baseURLSuffix;
		
		this.name = name;
	}
	
	
	protected void addServiceManagerNode() {
		// create service manager node
		EndpointReferenceType epr = AddressingUtil.newEPR();
		epr.addNewAddress().setStringValue(ServiceManagerNode.NODE_NAME);
		AddressingUtil.addPortType(epr, ServiceManagerNode.PORTTYPE);		
		ServiceManagerNode serviceManagerNode = (ServiceManagerNode) NodeFactory.createNode(epr);

		addChild(serviceManagerNode);
	}
	
	protected void addServerConfiguratorNode() {
		// create server configurator node
		
		
		/* Currently deactivated since it carries no functional child nodes, yet.
		EndpointReferenceType epr = AddressingUtil.newEPR();
		epr.addNewAddress().setStringValue(ServerConfiguratorNode.NODE_NAME);
		AddressingUtil.addPortType(epr, ServerConfiguratorNode.PORTTYPE);		
		ServerConfiguratorNode serverConfiguratorNode = (ServerConfiguratorNode) NodeFactory.createNode(epr);
		addChild(serverConfiguratorNode);
		*/
	}
	
	protected void addSystemNode() {
		// create server configurator node
		EndpointReferenceType epr = AddressingUtil.newEPR();
		epr.addNewAddress().setStringValue(SystemNode.NODE_NAME);
		AddressingUtil.addPortType(epr, SystemNode.PORTTYPE);		
		SystemNode systemNode = (SystemNode) NodeFactory.createNode(epr);
		addChild(systemNode);
	}
	
	@Override
	public void retrieveChildren() {
		removeAllChildren();
		
		addServiceManagerNode();
		addServerConfiguratorNode();
		addSystemNode();
	}


}
