package de.fzj.unicore.rcp.admindashboard;

import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;

/**
 * Groups MetricValue arrays to corresponding metrics for one update call.
 * @author j.daivandy@fz-juelich.de
 *
 */
public class MetricDataUpdate {
	
	private String metricName;
	private String metricUnit;
	private MetricValue[] values;
	
	public MetricDataUpdate(MetricValue[] values) {
		metricName = values[0].getName();
		metricUnit = values[0].getUnits();
		this.values = values;
	}
	
	public String getMetricName() {
		return metricName;
	}
	
	public String getMetricUnit() {
		return metricUnit;
	}
	
	public MetricValue[] getValues() {
		return values;
	}
	
}
