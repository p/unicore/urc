package de.fzj.unicore.rcp.admindashboard.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.namespace.QName;

import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.uas.security.IUASSecurityProperties;
import de.fzj.unicore.uas.util.AddressingUtil;
import de.fzj.unicore.wsrflite.admin.service.AdminService;
import de.fzj.unicore.wsrflite.xmlbeans.DeleteServiceInstanceRequestDocument;
import de.fzj.unicore.wsrflite.xmlbeans.DeployServiceRequestDocument;
import de.fzj.unicore.wsrflite.xmlbeans.GetMetricRequestDocument;
import de.fzj.unicore.wsrflite.xmlbeans.GetMetricsRequestDocument;
import de.fzj.unicore.wsrflite.xmlbeans.GetServiceInstanceRequestDocument;
import de.fzj.unicore.wsrflite.xmlbeans.GetServiceInstanceResponseDocument;
import de.fzj.unicore.wsrflite.xmlbeans.GetServiceInstancesRequestDocument;
import de.fzj.unicore.wsrflite.xmlbeans.GetServiceInstancesResponseDocument;
import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;
import de.fzj.unicore.wsrflite.xmlbeans.ServiceEntryDocument.ServiceEntry;
import de.fzj.unicore.wsrflite.xmlbeans.UndeployServiceRequestDocument;
import de.fzj.unicore.wsrflite.xmlbeans.client.AdminServiceClient;

public class AdminServiceAdapter {
	
	
	public static ServiceEntry[] getServiceNames(SecuredNode node) throws Exception
	{			
		AdminServiceClient adminClient = getAdminServiceClient(node);
		return adminClient.getServiceNames();
	}
	
	
	public static GetServiceInstancesResponseDocument getServiceInstances(SecuredNode node, String serviceName) throws Exception
	{			
		AdminServiceClient adminClient = getAdminServiceClient(node);
		
		GetServiceInstancesRequestDocument req = GetServiceInstancesRequestDocument.Factory.newInstance();
		GetServiceInstancesResponseDocument res = null;
		
		req.addNewGetServiceInstancesRequest().setServiceName(serviceName);		
		res = adminClient.getServiceInstances(req);
		
		return res;			
	}
	
	
	
	public static String getServiceInstanceData(SecuredNode node, String serviceName, String uid) throws Exception
	{			
		AdminServiceClient adminClient = getAdminServiceClient(node);
		
		GetServiceInstanceRequestDocument req = GetServiceInstanceRequestDocument.Factory.newInstance();
		req.addNewGetServiceInstanceRequest().setServiceName(serviceName);
		req.getGetServiceInstanceRequest().setUid(uid);
		
		GetServiceInstanceResponseDocument res = null;
		
		res = adminClient.getServiceInstanceData(req);
		
		res.getGetServiceInstanceResponse().toString();
		
		if(res != null) return res.getGetServiceInstanceResponse().xmlText();
		
		else return null;
	}
	
	
	public static void deleteServiceInstance(SecuredNode node, String serviceName, String uid) throws Exception
	{			
		AdminServiceClient adminClient = getAdminServiceClient(node);
		
		DeleteServiceInstanceRequestDocument req = DeleteServiceInstanceRequestDocument.Factory.newInstance();
		req.addNewDeleteServiceInstanceRequest().setServiceName(serviceName);
		req.getDeleteServiceInstanceRequest().setUid(uid);
		
		adminClient.deleteServiceInstance(req);				
	}
	
	
	
	public static void deployService(SecuredNode node, String serviceName, String iFace, String impl, boolean isPersistent, File jarFile) throws Exception
	{		
		byte[] jarAsByteArray = new byte[(int) jarFile.length()];
		FileInputStream fis = null;
		
		try {
			fis = new FileInputStream(jarFile);
			fis.read(jarAsByteArray);
			fis.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
		AdminServiceClient adminClient = getAdminServiceClient(node);
		
		DeployServiceRequestDocument req = DeployServiceRequestDocument.Factory.newInstance();				
		req.addNewDeployServiceRequest().setServiceName(serviceName);		
		req.getDeployServiceRequest().setJarFile(jarAsByteArray);		
		req.getDeployServiceRequest().setIsPersistent(isPersistent);
		
		adminClient.deployService(req);
	}
	
	
	public static void undeployService(SecuredNode node,String serviceName) throws Exception
	{		
		AdminServiceClient adminClient = getAdminServiceClient(node);
		
		UndeployServiceRequestDocument req = UndeployServiceRequestDocument.Factory.newInstance();
		
		req.addNewUndeployServiceRequest().setServiceName(serviceName);		
		adminClient.undeployService(req);
	}
	
	
	
	
	

	
	public static AdminServiceClient getAdminServiceClient(SecuredNode node) throws Exception {		        
	    //IUASSecurityProperties secProp = IdentityActivator.getDefault().getDefaultUASSecProps();
		IUASSecurityProperties secProp = node.getUASSecProps();
		String baseURL = getBaseURL(node);
		String serviceURL = baseURL + AdminService.SERVICE_NAME;
		
		EndpointReferenceType epr = AddressingUtil.newEPR();
		epr.addNewAddress().setStringValue(serviceURL + "?res=" + AdminService.SINGLETON_ID);
		QName servicePort = new QName(AdminService.ADMINSERVICE_NS, AdminService.SERVICE_NAME);
		AddressingUtil.addPortType(epr, servicePort);
		
		AdminServiceClient adminClient = new AdminServiceClient(serviceURL, epr, secProp);
		
		return adminClient;
	}
	
	
	public static String getBaseURL(SecuredNode node) throws Exception {		        
		String baseURL = node.getEpr().getAddress().getStringValue();		
		baseURL = baseURL.split("services")[0] + "services/";		
		
		return baseURL;
	}
	
	
	
	
	
	public static String[] getMetricCategories(SecuredNode node) throws Exception {		
		AdminServiceClient adminClient = getAdminServiceClient(node);
		
		return adminClient.getMetricCategories();		
	}
	
	
	public static MetricValue[] getMetrics(SecuredNode node, String[] categories, String[] names, boolean getAllMetrics) throws Exception {		
		AdminServiceClient adminClient = getAdminServiceClient(node);
		GetMetricsRequestDocument req = GetMetricsRequestDocument.Factory.newInstance();
		req.addNewGetMetricsRequest();
		
		if(getAllMetrics) req.getGetMetricsRequest().setGetAllMetrics(getAllMetrics);
		
		else {
			req.getGetMetricsRequest().setCategoryArray(categories);		
			req.getGetMetricsRequest().setNameArray(names);
		}
		
		
		return adminClient.getMetrics(req);
	}
	
	
	public static MetricValue getMetric(SecuredNode node, String metricName) throws Exception {		
		AdminServiceClient adminClient = getAdminServiceClient(node);
		GetMetricRequestDocument req = GetMetricRequestDocument.Factory.newInstance();
		req.addNewGetMetricRequest().setName(metricName);
		
		return adminClient.getMetric(req);
	}
	
	
	public static void clearMetricData(SecuredNode node, String metricName) throws Exception {		
		AdminServiceClient adminClient = getAdminServiceClient(node);		
		adminClient.clearMetricData(metricName);		
	}
	
	
	

}
