package de.fzj.unicore.rcp.admindashboard.ui;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.servicebrowser.ui.NewRegistryFrame;
import de.fzj.unicore.uas.impl.registry.MulticastRegistryFinder;
import de.fzj.unicore.uas.util.AddressingUtil;
import de.fzj.unicore.wsrflite.admin.service.AdminService;

public class NewAdminServiceEndpointFrame extends NewRegistryFrame {

	public NewAdminServiceEndpointFrame(Shell parentShell) {
		super(parentShell);
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	protected Control createContents(Composite parent) {
		
		getShell().setText("Add a new AdminServiceEndpoint");
		getShell().setBackgroundMode(SWT.INHERIT_DEFAULT);
		
		Composite control = new Composite(parent,SWT.NONE);
		gridLayout = new GridLayout();
		gridLayout.makeColumnsEqualWidth = false;
		gridLayout.numColumns = 5;
		control.setLayout(gridLayout);

		labelName = new Label(control, SWT.NONE);
		labelName.setText("Name:");

		textName = new Text(control, SWT.BORDER);
		textName.setText("AdminServiceEndpoint");
		GridData gridDataTxt = new GridData(GridData.FILL_HORIZONTAL);
		gridDataTxt.horizontalSpan = 3;
		textName.setLayoutData(gridDataTxt);
		textName.addModifyListener(new ModifyListener(){

			public void modifyText(ModifyEvent e) {
				try {
					String newName = textName.getText();
					if(newName.trim().length() == 0) throw new Exception("Name must not be empty");
					name = newName;
					setErrorMessageName(null);
				} catch (Exception e1) {
					name = null;
					setErrorMessageName(e1.getMessage());
				}
			}
		});
		
		buttonClearName = new Button(control, SWT.NONE);
		buttonClearName.setText("Clear Name");
		GridData gridDataClearName = new GridData();
		gridDataClearName.grabExcessHorizontalSpace = false;
		gridDataClearName.grabExcessVerticalSpace = false;
		gridDataClearName.horizontalAlignment = GridData.FILL;
		buttonClearName.setLayoutData(gridDataClearName);
		buttonClearName
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				textName.setText("");
			}
		});
		labelURL = new Label(control, SWT.NONE);
		labelURL.setText("URL:");

		textURL = new Text(control, SWT.BORDER);
		String defaultUri = "https://localhost:8080/REGISTRY/services/"+AdminService.SERVICE_NAME+"?res="+AdminService.SINGLETON_ID;
		textURL.setText(defaultUri);
		try {
			uri = new URI(defaultUri);
		} catch (URISyntaxException e2) {
		}
		GridData gridDataTxt1 = new GridData(GridData.FILL_HORIZONTAL);
		gridDataTxt1.horizontalSpan = 3;
		textURL.setLayoutData(gridDataTxt1);
		textURL.addModifyListener(new ModifyListener(){

			public void modifyText(ModifyEvent e) {
				try {
					String uriText = textURL.getText();
					if(uriText.trim().length() == 0) throw new Exception("URL must not be empty");
					uri = new URI(uriText);
					setErrorMessageURL(null);
				} catch (Exception e1) {
					uri = null;
					setErrorMessageURL(e1.getMessage());
				}
			}
		});
		
		buttonClearURL = new Button(control, SWT.NONE);
		buttonClearURL.setText("Clear URL");
		GridData gridDataClearURL = new GridData();
		gridDataClearURL.grabExcessHorizontalSpace = false;
		gridDataClearURL.grabExcessVerticalSpace = false;
		gridDataClearURL.horizontalAlignment = GridData.FILL;
		buttonClearURL.setLayoutData(gridDataClearURL);

		buttonClearURL
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				textURL.setText("");
			}
		});
		
		
		new Label(control, SWT.NONE);
		errorLabel = new Label(control,SWT.NONE);
		GridData gridDataErrorLabel = new GridData(GridData.FILL_HORIZONTAL);
		gridDataErrorLabel.horizontalSpan = 3;
		errorLabel.setLayoutData(gridDataErrorLabel);
		Color red = PlatformUI.getWorkbench().getDisplay().getSystemColor( SWT.COLOR_RED);
		errorLabel.setForeground(red);

		
		new Label(control, SWT.NONE);
		
		


		buttonOK = new Button(control, SWT.NONE);
		buttonOK.setText("OK");
		GridData gridDataButton2 = new GridData();
		gridDataButton2.grabExcessHorizontalSpace = false;
		gridDataButton2.grabExcessVerticalSpace = false;
		gridDataButton2.horizontalAlignment = GridData.FILL;
		buttonOK.setLayoutData(gridDataButton2);
		buttonOK.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				okButtonSelected();
			}
		});


		buttonCancel = new Button(control, SWT.NONE);
		buttonCancel.setText("Cancel");
		GridData gridDataButton3 = new GridData();
		gridDataButton3.grabExcessHorizontalSpace = false;
		gridDataButton3.grabExcessVerticalSpace = false;
		buttonCancel.setLayoutData(gridDataButton3);
		buttonCancel
		.addSelectionListener(new org.eclipse.swt.events.SelectionAdapter() {
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
				setReturnCode(CANCEL);
				close();
			}
		});

		
		return control;
	}
	
	@Override
	public EndpointReferenceType getEpr() {
		EndpointReferenceType epr = AddressingUtil.newEPR();
		epr.addNewAddress().setStringValue(uri.toString());		
		AddressingUtil.addPortType(epr, AdminService.ADMINSERVICE_PORT);
		
		return epr;
	}
	
	
	@Override
	protected void refreshMultiDiscover() {
		comboRegistry.removeAll();
		try {
			comboRegistry.setEnabled(true); 
			List<String> registryList = MulticastRegistryFinder.discover();
			if(registryList.size() > 0) {
				for(String reg: registryList) {
					comboRegistry.add(reg);

				}
				comboRegistry.select(0);
				comboBoxSelected();
			} else {
				comboRegistry.add("no AdminServiceEndpoints found in your LAN, please enter a URL manually");
			}
		} catch (Exception e) {
			comboRegistry.add("no AdminServiceEndpoints found in your LAN, please enter a URL manually");
			comboRegistry.select(0);
		}



	}

}
