package de.fzj.unicore.rcp.admindashboard.nodes;

import javax.xml.namespace.QName;

import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.uas.util.AddressingUtil;

public class AdminGridNode extends GridNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8024583618162023223L;
	public static final QName PORTTYPE = new QName("http://www.unicore.fzj.de","AdminGrid");
	public static final String TYPE = AdminGridNode.NODE_NAME; //PORTTYPE.toString();
	private static final String NODE_NAME = "AdminGrid";
	
	public AdminGridNode() {
		super();
		String address = NODE_NAME;

		EndpointReferenceType epr = AddressingUtil.newEPR();
		epr.addNewAddress().setStringValue(address);
		AddressingUtil.addPortType(epr, getPortType());
		setEpr(epr);
	}
	
	
	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();
		
		//availableActions = new ArrayList<NodeAction>();
		
		//availableActions.add(new RefreshAction(this));
		//availableActions.add(new MonitorAdminServiceEndpointAction(this));
		
	}
	

}
