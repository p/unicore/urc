/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.admindashboard.nodes;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.admindashboard.util.AdminServiceAdapter;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.uas.util.AddressingUtil;
import de.fzj.unicore.wsrflite.xmlbeans.GetServiceInstancesResponseDocument;

/**
 * @author demuth
 *
 */
public class WSRFParentNode extends AdministratedWebServiceNode { 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8778963861104492304L;
	public static final String TYPE = "WS-Resource Service";
	public static final QName PORTTYPE = new QName("http://www.fz-juelich.de/unicore/", WSRFParentNode.TYPE);
		

	public WSRFParentNode(EndpointReferenceType epr) {
		super(epr);
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return TYPE;
	} 
	
	
	@Override
	public void retrieveChildren() {
		removeAllChildren();
		
		final EndpointReferenceType epr = getEpr();
		final String serviceURL = epr.getAddress().getStringValue();
		final String serviceName = serviceURL.split("services\\/")[1];
		final WSRFParentNode thisNode = this;
		
		Job wsJob = new Job(this.getClass().getName()) {

			@Override
			protected IStatus run(IProgressMonitor arg0) {
				
				try {
					GetServiceInstancesResponseDocument res = AdminServiceAdapter.getServiceInstances(thisNode, serviceName);
					String[] uids = res.getGetServiceInstancesResponse().getUidArray();
					String serviceNamespace = res.getGetServiceInstancesResponse().getServiceNamespace();
					
					QName servicePort = null;
					EndpointReferenceType theEpr = null;
					
					for(String uid:uids) {				
						theEpr = AddressingUtil.newEPR();
						theEpr.addNewAddress().setStringValue(serviceURL+"?res="+uid);
						servicePort = new QName(serviceNamespace, serviceName);
						AddressingUtil.addPortType(theEpr, servicePort);
						
						Node wsrfNode = NodeFactory.createNode(theEpr);
						wsrfNode.setName(uid);        			
						addChild(wsrfNode);
					}
					
					if(uids.length>0) this.setName(serviceName + " (" + uids.length + ")");
					
					return Status.OK_STATUS;
				}
				catch(Exception e) {
					return Status.CANCEL_STATUS;
				}
			}
			
		};
		
		wsJob.schedule();	
		
		
		
		
		
	}
	
	
	@Override
	public String getLowLevelAsString() {
		return "This is a parent node containing actual WS-resource instances as child nodes.";
	}
	
	@Override
	public void resetAvailableActions() {
		super.resetAvailableActions();		
	}
	
	

	
}
