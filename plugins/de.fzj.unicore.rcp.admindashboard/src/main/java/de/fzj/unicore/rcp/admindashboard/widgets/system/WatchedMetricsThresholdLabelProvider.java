package de.fzj.unicore.rcp.admindashboard.widgets.system;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Color;

import de.fzj.unicore.rcp.admindashboard.ThresholdObserver;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;

public class WatchedMetricsThresholdLabelProvider extends ColumnLabelProvider {

	private SecuredNode callingNode;
	public static final int MIN = 0;
	public static final int MAX = 1;
	private int mode;
	
	public WatchedMetricsThresholdLabelProvider(SecuredNode caller, int mode) {
		callingNode = caller;
		this.mode = mode;
	}

	
	@Override
	public String getText(Object element) {
		MetricValue mv = (MetricValue) element;
		String thresholdString = "n/a";
		Double thresholdValue = null;
		
		switch(mode) {
			case MIN: thresholdValue = ThresholdObserver.getInstance(callingNode).getThresholdMin(mv.getName()); break;
			case MAX: thresholdValue = ThresholdObserver.getInstance(callingNode).getThresholdMax(mv.getName()); break;
		}
		
		if(thresholdValue!=null) thresholdString = thresholdValue.toString();
		
		return thresholdString;
	}
	
	@Override
	public Color getForeground(Object element) {
		MetricValue mv = (MetricValue) element;				
		Color color = SystemMetricsDialog.getInstance(callingNode).getColor(mv.getName());
		
		return color;
		
	}

	@Override
	public Color getBackground(Object arg0) {
		return null;
	}

}
