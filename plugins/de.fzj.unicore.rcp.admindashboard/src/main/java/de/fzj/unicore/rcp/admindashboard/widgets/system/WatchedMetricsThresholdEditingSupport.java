package de.fzj.unicore.rcp.admindashboard.widgets.system;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;

import de.fzj.unicore.rcp.admindashboard.ThresholdObserver;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;

public class WatchedMetricsThresholdEditingSupport extends EditingSupport {
	private CellEditor editor;	
	private SecuredNode callingNode;	
	private int mode;
	public static final int MIN = 0;
	public static final int MAX = 1;

	public WatchedMetricsThresholdEditingSupport(ColumnViewer viewer, SecuredNode caller, int mode) {
		super(viewer);
		editor = new TextCellEditor(((TableViewer) viewer).getTable());		
		callingNode = caller;		
		this.mode = mode;
	}

	@Override
	protected boolean canEdit(Object element) {
		return true;
	}

	@Override
	protected CellEditor getCellEditor(Object element) {	
		return editor;
	}

	@Override
	protected Object getValue(Object element) {
		Object returnValue = null;
		MetricValue metric = (MetricValue) element;
		
		switch(mode) {
			case MIN: returnValue = ThresholdObserver.getInstance(callingNode).getThresholdMin(metric.getName()); break;
			case MAX: returnValue = ThresholdObserver.getInstance(callingNode).getThresholdMax(metric.getName()); break;
		}		
		
		if(returnValue==null) returnValue = "n/a";
		else returnValue = ((Double) returnValue).toString();
		
		return returnValue;
		
	}

	@Override
	protected void setValue(final Object element, Object value) {
		MetricValue metric = (MetricValue) element;
		
		try {
			Double doubleValue = Double.parseDouble((String) value);		
			
			switch(mode) {
				case MIN: ThresholdObserver.getInstance(callingNode).setThresholdMin(metric.getName(), doubleValue); break;
				case MAX: ThresholdObserver.getInstance(callingNode).setThresholdMax(metric.getName(), doubleValue); break;
			}	
			
			getViewer().update(element, null);
		}
		catch(NumberFormatException e) {
			//
		}
	}

}
