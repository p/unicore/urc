package de.fzj.unicore.rcp.admindashboard.widgets.system;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;

public class WatchedMetricsContentProvider implements IStructuredContentProvider {

	
	public Object[] getElements(Object inputElement) {
		@SuppressWarnings("unchecked")
		List<MetricValue> metricValues = (List<MetricValue>) inputElement;
		return metricValues.toArray();
	}

	
	public void dispose() {
	}


	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

}
