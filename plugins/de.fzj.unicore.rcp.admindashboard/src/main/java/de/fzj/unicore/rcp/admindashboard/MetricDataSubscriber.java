package de.fzj.unicore.rcp.admindashboard;

public interface MetricDataSubscriber {
	
	public void update(MetricDataUpdate[] updatedValues);
	
	public void watch(String metricName, String metricUnit);
	
	public void unwatch(String metricName);
	
	public void resetCollectedData();

}
