package de.fzj.unicore.rcp.admindashboard.widgets.servicemanager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.admindashboard.AdminConstants;
import de.fzj.unicore.rcp.admindashboard.AdminDashboardActivator;
import de.fzj.unicore.rcp.admindashboard.util.AdminServiceAdapter;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.wsrflite.utils.deployment.DeploymentDescriptor;
import de.fzj.unicore.wsrflite.utils.deployment.DeploymentDescriptorImpl;
import de.fzj.unicore.wsrflite.xmlbeans.ServiceEntryDocument.ServiceEntry;


public class ServiceManagerDialog extends Dialog {
    private String title,    			   
    			   serviceName,
    			   statusMsg,
    			   pathToJar,
    			   iFace,
    			   impl,
    			   isPersistent;
    
    private SecuredNode callingNode;
    
    private Button buttonDeploy, buttonChooseFile;
    private Text textServiceName, textPathToJar, textIFace, textImpl;   
    
    private FileDialog fileDialog;
    private Combo dropDownPersistence;
	
	private Label labelStatusMsg;
	
	private boolean iFaceSpecified=false,
					implSpecified=false,
					serviceNameSpecified=false,
					nameContainsSpaces=false,
					nameBeginsWithNumber=false,
					serviceNameAvailable=true,
					canceled=false,					
					readyToDeploy=false;
	
	
	private Set<String> occupiedServiceNames = new HashSet<String>();
	   
    public ServiceManagerDialog(Shell parentShell, String dialogTitle, String initialValue, SecuredNode callingNode) {
        super(parentShell);
        this.title = dialogTitle;
        this.statusMsg = "Please enter deployment specifications.";
        this.callingNode = callingNode;
    }
    
    
    protected void buttonPressed(int buttonId) {
        if (buttonId == AdminConstants.SM_DEPLOY_SERVICE) close();
        else if (buttonId == AdminConstants.SM_OPEN_JAR) processServiceJar();
    }

    
    protected void configureShell(Shell shell) {
        super.configureShell(shell);
        if (title != null) {
			shell.setText(title);
		}
    }

    protected void createButtonsForButtonBar(Composite parent) {      
    	textIFace.setFocus();
    }

    protected Control createDialogArea(Composite parent) {    	
    	fileDialog = new FileDialog(new Shell());
    	
    	Composite composite = (Composite) super.createDialogArea(parent);    	         
        GridLayout dialogLayout = new GridLayout();
    	composite.setLayout(dialogLayout);
        composite.setLayoutData(new GridData(440, 265));    	
        
    	Composite entryForm;		// contains all widgets
    	
    	entryForm = new Composite(composite, SWT.NONE);
    	entryForm.setLayoutData(new GridData(440, 265));
    	GridLayout layoutEntryForm = new GridLayout();
    	entryForm.setLayout(layoutEntryForm);
    		
    		// 1. row: text field for entering name of prospective service
    		(new Label(entryForm, SWT.WRAP)).setText("Service Name");
    		textServiceName = new Text(entryForm, getInputTextStyle());
    		textServiceName.setLayoutData(new GridData(250, 18));
    		textServiceName.setText("");    		
    	    		    		
    		(new Label(entryForm, SWT.WRAP)).setText("Jar File");
    		Composite jarSpec = new Composite(entryForm, SWT.WRAP);
    		jarSpec.setLayout(new GridLayout(2, false));
    		GridData layoutDataJarSpec = new GridData(350, 35);
    		layoutDataJarSpec.grabExcessHorizontalSpace=true;    		
    		jarSpec.setLayoutData(layoutDataJarSpec);
            
            textPathToJar = new Text(jarSpec, getInputTextStyle() | SWT.READ_ONLY);
            GridData layoutDataTextPathToJar = new GridData(205, 18);
            layoutDataTextPathToJar.horizontalAlignment = SWT.LEFT;
            textPathToJar.setLayoutData(layoutDataTextPathToJar);            
            textPathToJar.setEnabled(true);
            textPathToJar.setBackground(getShell().getBackground());
    		
            
            // ...and a file selection dialog button
            buttonChooseFile = createButton(jarSpec, AdminConstants.SM_OPEN_JAR, IDialogConstants.OPEN_LABEL, false);
            buttonChooseFile.setText("...");
            GridData layoutDataButtonChooseFile = new GridData(35, 30);
            layoutDataButtonChooseFile.horizontalAlignment = SWT.LEFT;
            buttonChooseFile.setLayoutData(layoutDataButtonChooseFile);
            
            // 3. row: text field for specifying service interface class; grayed-out by default until a jar file has been selected
            (new Label(entryForm, SWT.WRAP)).setText("Interface Class");
            textIFace = new Text(entryForm, getInputTextStyle() | SWT.READ_ONLY);
            textIFace.setLayoutData(new GridData(250, 18));
            textIFace.setEnabled(true);
            textIFace.setBackground(getShell().getBackground());
            
            // 4. row: text field for specifying service implementation class; grayed-out until a jar file has been selected
            (new Label(entryForm, SWT.WRAP)).setText("Implementation Class");            
            textImpl = new Text(entryForm, getInputTextStyle() | SWT.READ_ONLY);
            textImpl.setLayoutData(new GridData(250, 18));
            textImpl.setEnabled(true);
            textImpl.setBackground(getShell().getBackground());
            
            
            /* 5. row: drop-down menu for setting the persistent flag for WSRF services; grayed-out until the service implementation
             * 	  class has been specified; will remain grayed-out if the service isn't WSRF
             */            
            (new Label(entryForm, SWT.WRAP)).setText("Persistent");            
            dropDownPersistence = new Combo(entryForm, getInputTextStyle() | SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);
            dropDownPersistence.setLayoutData(new GridData(75, 18));
            dropDownPersistence.setEnabled(true);
            dropDownPersistence.add("no");
            dropDownPersistence.add("yes");        
            dropDownPersistence.select(0);
            
            
            /* 6. row: deploy button */
            (new Label(entryForm, SWT.WRAP)).setText("");
            GridData layoutData_buttonDeploy = new GridData();
	        layoutData_buttonDeploy.widthHint = 75;
	        layoutData_buttonDeploy.horizontalAlignment = SWT.LEFT;
	        layoutData_buttonDeploy.verticalIndent = 10;
	        buttonDeploy = createButton(entryForm, AdminConstants.SM_DEPLOY_SERVICE, IDialogConstants.OK_LABEL, true);
	        buttonDeploy.setText("deploy");
	        buttonDeploy.setLayoutData(layoutData_buttonDeploy);
	        buttonDeploy.setEnabled(true); 
            
            
            // 7. row: error label            
	        (new Label(entryForm, SWT.WRAP)).setText("");
            GridData layoutData_errorLabel = new GridData(250, 200);            
            layoutData_errorLabel.horizontalAlignment = SWT.LEFT;
            layoutData_errorLabel.verticalIndent = 30;            
            labelStatusMsg = new Label(entryForm, SWT.WRAP);            
            labelStatusMsg.setLayoutData(layoutData_errorLabel);
            Font f = new Font(Display.getCurrent(), "Arial", 9, SWT.NONE);            
            labelStatusMsg.setFont(f);
    	
            
    	layoutEntryForm.numColumns = 2;     
    	dialogLayout.numColumns = 1;        
        
        applyDialogFont(composite);
        
        setEventListeners();
        
        checkOK();
        
        
        return composite;
    }
    
   
    
    private void processServiceJar() {
    	fileDialog.open();
    	
    	pathToJar = fileDialog.getFilterPath() + System.getProperty("file.separator") +fileDialog.getFileName();       
    	textPathToJar.setText(pathToJar);
    	
    	try {
    		File serviceJar = new File(pathToJar);
    		DeploymentDescriptor info = extractDeploymentMetadata(serviceJar);
    	
    		iFace = info.iFace();
    		impl = info.impl();
    		
    		textIFace.setText(iFace);
    		textImpl.setText(impl);        		
    	}
    	catch(Exception e) {
    		AdminDashboardActivator.log(IStatus.ERROR, "'"+pathToJar+"' is no valid service JAR file.\nPlease select a valid one.", true);
    	}
    }
    
    
    private void setEventListeners() {
    	
        textServiceName.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent arg0) {				
				validateServiceName();
			}        	
        });
        
        
        
        textIFace.addModifyListener(new ModifyListener(){

			public void modifyText(ModifyEvent e) {
				if(iFace.trim().length() == 0) iFaceSpecified = false;
				else iFaceSpecified = true;
				checkOK();
			}
		});
        
        
        
        textImpl.addModifyListener(new ModifyListener(){

			public void modifyText(ModifyEvent e) {
				if(impl.trim().length() == 0) implSpecified = false;
				else implSpecified = true;
				
				checkOK();
			}
		});
    }
    

   
    
    private void validateServiceName() {
    	serviceName = textServiceName.getText();
    	
    	// checks if a service name is specified
		if(serviceName.length() > 0) serviceNameSpecified=true;    	
		else serviceNameSpecified=false;
    	
    	
		if(serviceNameSpecified) {
			
			
			// checks if the specified service name contains space characters
	    	if(serviceName.contains(" ")) nameContainsSpaces=true;
	    	else nameContainsSpaces=false; 
	    
	    	// checks if the specified service name begins with a number character
			if(serviceName.startsWith("0") ||
					serviceName.startsWith("1") ||
					serviceName.startsWith("2") ||
					serviceName.startsWith("3") ||
					serviceName.startsWith("4") ||
					serviceName.startsWith("5") ||
					serviceName.startsWith("6") ||
					serviceName.startsWith("7") ||
					serviceName.startsWith("8") ||
					serviceName.startsWith("9"))  {
				
				nameBeginsWithNumber=true;
			}
			else nameBeginsWithNumber=false;
			
			if(occupiedServiceNames.contains(serviceName)) serviceNameAvailable=false;
			else serviceNameAvailable=true;
				
		}
		
		checkOK();
		
		
    }
    
  
    
   
    protected int getInputTextStyle() {
		return SWT.SINGLE | SWT.BORDER;
	}
	
	
	public String getServiceName() {		
		return serviceName;
	}
	
	public String getIFace() {
		return iFace;
	}
	
	public String getImpl() {
		return impl;
	}
	
	public String getIsPersistent() {
		return isPersistent;
	}
	
	public String getPathToJar() {
		return pathToJar;
	}
	
		
	
	private DeploymentDescriptor extractDeploymentMetadata(File jarFile) throws IOException {
		DeploymentDescriptor info = null;
		JarFile jf = new JarFile(jarFile);
		Enumeration<JarEntry> entries = jf.entries();
		
		URL url = jarFile.toURI().toURL();
		ClassLoader jarFileclassLoader = new URLClassLoader(new URL[] { (url) }, this.getClass().getClassLoader());
		ClassLoader oldClassLoader = this.getClass().getClassLoader();
		Thread.currentThread().setContextClassLoader(jarFileclassLoader);	
		
		// browse through all entries in this jar file
		while(entries.hasMoreElements()) {
			JarEntry entry = entries.nextElement();
			String name = entry.getName();
			
			if(name.endsWith(".class")) {
				name = name.replace('/', '.');
				name = name.substring(0, name.length()-6);				
				 
				// test if this jar file entry is a class by trying to load it
				try {
					Class<?> theClass = Class.forName(name, true, jarFileclassLoader);
					
					// the jar file entry is a class... 
					info = theClass.getAnnotation(DeploymentDescriptor.class);
					if(info!=null) break;					
				}
				catch(ClassNotFoundException e) {
					// jar file entry is not a class		
					e.printStackTrace();
				}
			}
		}
		
		Thread.currentThread().setContextClassLoader(oldClassLoader);
		
		// if no DeploymentDescriptor was specified via annotation, read the iface and impl class names from this service jar's manifest
		if(info==null) {
			String iFace = jf.getManifest().getMainAttributes().getValue("iFace");
			String impl = jf.getManifest().getMainAttributes().getValue("impl");
			String b2 = jf.getManifest().getMainAttributes().getValue("isPersistent");
			String initTask = jf.getManifest().getMainAttributes().getValue("initTask");
			
			info = new DeploymentDescriptorImpl(iFace, impl, Boolean.parseBoolean(b2), initTask);
			
			jf.close();			
		}
		
		
		return info;
	}


	


	protected void checkOK()
	{		
		buttonDeploy.setEnabled(false);
		
		StringBuilder sb = new StringBuilder();
		
		if(!serviceNameSpecified && textServiceName.getText().length()==0) sb.append("Please specify a service name.\n");
		if(!serviceNameAvailable && serviceName!=null && serviceName.length()>0) sb.append("Service name '"+serviceName+"' is already taken.\n");		
		if(nameContainsSpaces) sb.append("Space characters not allowed for 'Service Name'.\n");
		if(nameBeginsWithNumber) sb.append("'Service Name' must begin with a letter.\n");
		
		String statusMsg = sb.toString();
		labelStatusMsg.setText(statusMsg);
		
		readyToDeploy = serviceNameSpecified && serviceNameAvailable && iFaceSpecified && implSpecified && !nameContainsSpaces && !nameBeginsWithNumber;
		if(readyToDeploy) labelStatusMsg.setText("");
		
		if(!buttonDeploy.isDisposed()) buttonDeploy.setEnabled(readyToDeploy);
		
		nameContainsSpaces = false;
		nameBeginsWithNumber = false;		
	}
	
	
	@Override
	public int open() {
		Job job = new Job("Service list retriever @"+System.currentTimeMillis()) {

			@Override
			protected IStatus run(IProgressMonitor arg0) {
				try {
					ServiceEntry[] servicesEntries = AdminServiceAdapter.getServiceNames(callingNode);
					for(ServiceEntry entry:servicesEntries) {
						occupiedServiceNames.add(entry.getServiceName());
					}
					
					return Status.OK_STATUS;
					
				} catch (Exception e) {
					return Status.CANCEL_STATUS;
				}
			}
		};
		
		job.schedule();
		
		return super.open();
	}
	
	@Override
	public boolean close() {
		if(!readyToDeploy)
			canceled = true;
		
		return super.close();
	}
	
	public boolean isCanceled() {
		return canceled;
	}
}
