package de.fzj.unicore.rcp.admindashboard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.progress.UIJob;

import de.fzj.unicore.rcp.admindashboard.exceptions.ValueBelowMinimumThresholdException;
import de.fzj.unicore.rcp.admindashboard.util.AdminServiceAdapter;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.wsrflite.xmlbeans.MetricValueDocument.MetricValue;

/**
 * Provides metric data to subscribers.<br>
 * Metric data can either be live data polled from a UNICORE/X or loaded capture profiles. 
 */
public class MetricDataProvider {

	/** Pool of MetricDataProvider Singletons. */
	private static Map<SecuredNode, MetricDataProvider> providers = new HashMap<SecuredNode, MetricDataProvider>();
	
	/** Calling node identifying each Singleton. */
	private SecuredNode callingNode;	
	
	/** Metrics containing non-numerical values can't be watched and plotted. */
	private Set<String> unwatchableMetrics = new HashSet<String>();
	
	/** Contains live metric values polled from a UNICORE/X server. */
	private Map<String, List<MetricValue>> liveValues = new HashMap<String, List<MetricValue>>();	
	
	/** Data subscribers. */
	private Set<MetricDataSubscriber> subscribers = new HashSet<MetricDataSubscriber>();
		
	private Object lock = new Object(), lockReset = new Object();
	
	private int refreshInterval = 10; 
	private Job updater;
	
	private MetricDataProvider(SecuredNode callingNode) {
		this.callingNode = callingNode;
		init();
	}
	
	public static MetricDataProvider getInstance(SecuredNode callingNode) {
		if(!providers.containsKey(callingNode)) providers.put(callingNode, new MetricDataProvider(callingNode));
		
		return providers.get(callingNode);
	}
	
	
	/** Data retrieval polling job. */
	private void init() {
		updater = new Job("subscriber") {
			
			@Override
			public IStatus run(IProgressMonitor monitor) {
				synchronized(lockReset) {
					// reschedule after 10 seconds
					long refresh = (long) (refreshInterval * 1000);
			        schedule(refresh);		        
			        
			        String[] metricNames = new String[0];
			        
			        synchronized(lock) {
			        	metricNames = liveValues.keySet().toArray(new String[liveValues.size()]);
			        }
			        
			        MetricValue[] metrics = new MetricValue[0];
			        
			        if(metricNames.length>0) {
						try {
							metrics = AdminServiceAdapter.getMetrics(callingNode, null, metricNames, false);							
						}
						catch(Exception e) {
							e.printStackTrace();
							AdminDashboardActivator.log(IStatus.ERROR, "Couldn't retrieve new metric values.\nCheck connection to UNICORE/X running at "+callingNode.getURI()+".", true);
						}
						
						for(MetricValue mv:metrics) {
							add(mv);
						}
					}

			        
			        notifyAboutNewValues();
				}
		        
		        
		        return Status.OK_STATUS;
		     }
		};
		
		updater.schedule();
	}
	
	
	/** Convenience method used in polling job. */
	private void add(MetricValue mv) {
		double yValue = 0.0;		
		
		try {
			yValue = Double.parseDouble(mv.getValue());
			liveValues.get(mv.getName()).add(mv);					
		}
		catch(NumberFormatException e) {			
			unwatchableMetrics.add(mv.getName());
			AdminDashboardActivator.log(IStatus.WARNING, "Metric "+mv.getName()+" is unwatchable, since it doesn't supply parseable numeric values.");
		}
	}
	
	
	/** Adds the specified metric to the watch list.
	 * @category EXTERNAL_CONTROLS
	 * */
	public void watch(MetricValue metric) {
		synchronized(lock) {
			if(liveValues.keySet().size()<=AdminConstants.METRICS_MAXIMUM_WATCHABLE_METRICS) {
				liveValues.put(metric.getName(), new ArrayList<MetricValue>());
				liveValues.get(metric.getName()).add(metric);
				
				notifyAboutWatchMetric(metric.getName());
			}
		}
	}
	
	/** Removes the specified metric from watch list.
	 * @category EXTERNAL_CONTROLS
	 * */
	public void unwatch(MetricValue metric) {
		synchronized(lock) {
			liveValues.remove(metric.getName());
			notifyAboutUnwatchMetric(metric.getName());
		}
	}
	
	
	/** Returns list of currently watched metrics.
	 * @category EXTERNAL_STATUS
	 * */
	public String[] getCurrentlyWatchedMetricNames() {
			if(liveValues.size()>0)								
				return liveValues.keySet().toArray(new String[liveValues.size()]);
			
			else return new String[0];
	}
	
	
	
	/** Subscribes a subscriber that is notified of every change/update concerning the watched metrics.
	 * @category EXTERNAL_CONTROLS
	 * */
	public void subscribe(MetricDataSubscriber subscriber) {
		subscribers.add(subscriber);
	}
	
	/**
	 * @category CONVENIENCE
	 */
	private MetricDataUpdate[] getUpdatedValues() {		
		Set<MetricDataUpdate> updates = new HashSet<MetricDataUpdate>();
		String[] metricNames = getCurrentlyWatchedMetricNames();
		
		for(String metricName:metricNames) {
			MetricValue[] mValues = liveValues.get(metricName).toArray(new MetricValue[liveValues.get(metricName).size()]);
			MetricDataUpdate update = new MetricDataUpdate(mValues);
			updates.add(update);
		}
		
		MetricDataUpdate[] updatedValues = updates.toArray(new MetricDataUpdate[updates.size()]);
		
		
		return updatedValues;
	}
	
	
	/** Informs caller if the specified metric is watched.
	 * 
	 * @category EXTERNAL_STATUS
	 * */
	public boolean isWatched(String metricName) {
		return liveValues.containsKey(metricName);
	}
	
	/**
	 * @category EXTERNAL_STATUS
	 */
	public boolean isUnwatchable(String metricName) {
		if(liveValues.keySet().size()>=AdminConstants.METRICS_MAXIMUM_WATCHABLE_METRICS) return true;
		
		return unwatchableMetrics.contains(metricName);
	}
	
	/**
	 * @category EXTERNAL_CONTROLS
	 */
	public void setMetricUnwatchable(String metricName) {
		unwatchableMetrics.add(metricName);
	}
	
	
	/**
	 * 
	 * @category EXTERNAL_CONTROLS
	 */
	public void triggerUpdate(final MetricDataSubscriber subscriber) {
		
		UIJob triggeredUpdate = new UIJob("UI refresher: manually triggered metric data updater @"+System.currentTimeMillis()) {

			@Override
			public IStatus runInUIThread(IProgressMonitor arg0) {				
				MetricDataUpdate[] updatedValues = getUpdatedValues();				
				subscriber.update(updatedValues);				
				
				return Status.OK_STATUS;
			}			
		};
		
		triggeredUpdate.schedule();		
	}
	
	
	/**
	 * @category EXTERNAL_CONTROLS
	 */
	public void setRefreshInterval(int refreshInterval) throws Exception {
		if(refreshInterval>=AdminConstants.METRICS_MINIMUM_POLLING_INTERVAL) {
			this.refreshInterval = refreshInterval;
			updater.cancel();
			updater.schedule();
		}
		else {
			throw new ValueBelowMinimumThresholdException();
		}	 
	}
	
	
	/**
	 * @category EXTERNAL_STATUS
	 */
	public int getRefreshInterval() {
		return refreshInterval;
	}
	
	
	/**
	 * @category EXTERNAL_CONTROLS
	 */
	public void resetCollectedData() {
		synchronized(lockReset) {
			
			Iterator<MetricDataSubscriber> iterSubscribers = subscribers.iterator();
			
			while(iterSubscribers.hasNext()) {
				iterSubscribers.next().resetCollectedData();
			}
			
			Collection<List<MetricValue>> values = liveValues.values();
			Iterator<List<MetricValue>> iterValues = values.iterator();
			
			while(iterValues.hasNext()) {
				iterValues.next().clear();
			}
		}		
	}

	/** Notifies all subscribers about newly watched metric. Used by polling job.
	 * @category SUBSCRIBER_NOTIFICATION
	 *  */
	private synchronized void notifyAboutWatchMetric(final String metricName) {
		UIJob notifier = new UIJob("UI refresher: watch metric @"+System.currentTimeMillis()) {
	
			@Override
			public IStatus runInUIThread(IProgressMonitor arg0) {
				Iterator<MetricDataSubscriber> iter = subscribers.iterator();
				
				while(iter.hasNext()) {
					MetricDataSubscriber subscriber = iter.next();			
					subscriber.watch(metricName, liveValues.get(metricName).get(0).getUnits());
				}				
				
				return Status.OK_STATUS;
			}			
		};
		
		notifier.schedule();		
		
	}

	/** Notifies all subscribers about new values. Used by polling job.
	 * 
	 *  @category SUBSCRIBER_NOTIFICATION
	 *  */
	private synchronized void notifyAboutNewValues() {
		UIJob notifier = new UIJob("UI refresher: metric data updater @"+System.currentTimeMillis()) {
	
			@Override
			public IStatus runInUIThread(IProgressMonitor arg0) {
				
				MetricDataUpdate[] updatedValues = getUpdatedValues();
				Iterator<MetricDataSubscriber> iter = subscribers.iterator();
				
				while(iter.hasNext()) {
					MetricDataSubscriber subscriber = iter.next();			
					subscriber.update(updatedValues);
				}	
				
				return Status.OK_STATUS;
			}			
		};
		
		notifier.schedule();		
	}

	/** Notifies all subscribers about newly unwatched metric. Used by polling job.
	 * @category SUBSCRIBER_NOTIFICATION
	 * */
	private synchronized void notifyAboutUnwatchMetric(final String metricName) {
		UIJob notifier = new UIJob("UI refresher: unwatch metric @"+System.currentTimeMillis()) {
	
			@Override
			public IStatus runInUIThread(IProgressMonitor arg0) {
				Iterator<MetricDataSubscriber> iter = subscribers.iterator();
				
				while(iter.hasNext()) {
					MetricDataSubscriber subscriber = iter.next();			
					subscriber.unwatch(metricName);
				}				
				
				return Status.OK_STATUS;
			}			
		};
		
		notifier.schedule();		
		
	}
	
	
	
}