package de.fzj.unicore.rcp.admindashboard.actions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.window.Window;

import de.fzj.unicore.rcp.admindashboard.AdminDashboardActivator;
import de.fzj.unicore.rcp.admindashboard.ui.NewAdminServiceEndpointFrame;
import de.fzj.unicore.rcp.servicebrowser.actions.MonitorRegistryAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

/**
 * 
 * @author j.daivandy@fz-juelich.de
 *
 */
public class MonitorAdminServiceEndpointAction extends MonitorRegistryAction {

	public MonitorAdminServiceEndpointAction(IServiceViewer viewer) {
		super(viewer);
		
		setText("add AdminService Endpoint");
		setToolTipText("add a new bookmark for an AdminService Endpoint");
		setImageDescriptor(AdminDashboardActivator.getImageDescriptor("registry.png"));
	}
	
	@Override
	protected void popupMonitorRegistry() {
		setEnabled(false);
		try {
			final NewAdminServiceEndpointFrame r = new NewAdminServiceEndpointFrame(getViewer().getControl().getShell());
			int result = r.open();
			
			if(Window.OK == result)
			{
				Job j = new Job("adding AdminService Endpoint")
				{
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						final Node so = NodeFactory.createNode(r.getEpr());
						so.setName(r.getName());
						getViewer().addMonitoringEntry(so);
						return Status.OK_STATUS;
					}

				};
				j.schedule();
			}
		}
		finally {
			setEnabled(true);
		}
	}
	
	
	
	
	

}
