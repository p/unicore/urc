/* -*-mode:java; c-basic-offset:2; indent-tabs-mode:nil -*- */
/*
Copyright (c) 2002-2008 ymnk, JCraft,Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright 
     notice, this list of conditions and the following disclaimer in 
     the documentation and/or other materials provided with the distribution.

  3. The names of the authors may not be used to endorse or promote products
     derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL JCRAFT,
INC. OR ANY CONTRIBUTORS TO THIS SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.fzj.unicore.rcp.terminal.servicebrowser;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Status;
import org.unigrids.services.atomic.types.TextInfoType;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument.TargetSystemProperties;

import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;
import de.fzj.unicore.rcp.terminal.ConnectionType;
import de.fzj.unicore.rcp.terminal.ConnectionTypeRegistry;
import de.fzj.unicore.rcp.terminal.TerminalConstants;
import de.fzj.unicore.rcp.terminal.ssh.plain.PlainSSHConstants;

/**
 * Helper methods   
 * 
 * @author demuth, Andre Giesler
 */
class Util implements ServiceBrowserTerminalConstants {

	/**
	 * Extracts SSH related properties from TargetSystemProperties 
	 *
	 * @return      A map containing SSH properties of the Site
	 */
	public static Map<String,String> extractSSHRelatedProperties(TargetSystemProperties tssProps)
	{
		Map<String,String> config = new HashMap<String, String>();
		TextInfoType[] textInfo = tssProps.getTextInfoArray();
		for (TextInfoType textInfoType : textInfo) {
			String name = textInfoType.getName();
			if(name.startsWith(SSH_INFO_PREFIX)) 
			{
				String value = textInfoType.getValue();
				config.put(name, value);
			}
		}
		return config;
	}



	/**
	 * Returns the TargetSystemProperties (TSS) of the SSHSite
	 *
	 * @param	node the target system node or a target system factory node
	 * @return  the TargetSystemProperties of the site
	 */
	public static TargetSystemProperties getTSSProps(Node node){

		TargetSystemNode targetSystem = null;
		if(node instanceof TargetSystemFactoryNode) 
		{
			TargetSystemFactoryNode targetSystemFactory = (TargetSystemFactoryNode) node;
			if(!targetSystemFactory.hasChildren()) 
			{
				targetSystemFactory.refresh(1,true,null);
				if(!targetSystemFactory.hasChildren())
					try {
						targetSystemFactory.createTargetSystem();
					} catch (Exception e) {
						TerminalFromServicebrowserPlugin.log(Status.ERROR,"Eroor while creating target system for terminal connection: "+e.getStackTrace(),e);
					}
			}
			if(!targetSystemFactory.hasChildren()) 
			{
				return null;
			}
			targetSystem = (TargetSystemNode) targetSystemFactory.getChildrenArray()[0];
		}
		else if(node instanceof TargetSystemNode) 
		{
			targetSystem = (TargetSystemNode) node;
		}
		else return null;

		if(targetSystem.getTargetSystemProperties() == null) targetSystem.refresh(1,false,null);
		return targetSystem.getTargetSystemProperties();

	}


	/**
	 * Get the map with the site's SSH data, so that it is returned as a list
	 *
	 * @param	sshInfomap	the given map with SSH data of all sites
	 * @param	serverAddress the given NodePath of the site
	 * @return  the processed SSH data as a list
	 */
	public static Map<String,String> parseSSHRawData(Map<String,String> rawData){

		Map<String,String> result = ServiceBrowserTerminalConfigPersistence.getInstance().readConfig(rawData.get(ID));

		String sshHost, sshPort, sshType;

		if(rawData.containsKey(SSH_INFO_PLAIN)){
			result.put(PLAIN_SUPPORTED, Boolean.TRUE.toString());
			String plaininfo = rawData.get(SSH_INFO_PLAIN);
			String[] plaininfoArray = plaininfo.split("\\s");
			//System.out.println("plaininfo: " + plaininfo);
			sshHost = plaininfoArray[0].split(":")[0];
			sshPort = plaininfoArray[0].split(":")[1];
			sshType = "";
			if(plaininfoArray.length>1){
				String type = plaininfoArray[1];
				for(int i = 0; i<PlainSSHConstants.PLAIN_TYPES.length; i++){
					if(type.trim().equals(PlainSSHConstants.PLAIN_TYPES[i])){
						sshType = type;
						break;
					}
				}
			}
		}
		else{
			result.put(PLAIN_SUPPORTED, Boolean.FALSE.toString());
			sshHost = TerminalConstants.NOT_SUPPORTED;
			sshPort = TerminalConstants.NOT_SUPPORTED;
			sshType = TerminalConstants.NOT_SUPPORTED;
		}
		if(!result.containsKey(PLAIN_HOST)) result.put(PLAIN_HOST, sshHost); // don't override persisted data!
		if(!result.containsKey(PLAIN_PORT)) result.put(PLAIN_PORT, sshPort);
		if(!result.containsKey(PLAIN_TYPE)) result.put(PLAIN_TYPE, sshType);
		
		
//		if(rawData.containsKey(SSH_INFO_GSISSH)){
//			result.put(GSISSH_SUPPORTED, Boolean.TRUE.toString());
//			String plaininfo = rawData.get(SSH_INFO_GSISSH);
//			String[] plaininfoArray = plaininfo.split("\\s");
//			sshHost = plaininfoArray[0].split(":")[0];
//			sshPort = plaininfoArray[0].split(":")[1];			
//		}
//		else{
//			result.put(GSISSH_SUPPORTED, Boolean.FALSE.toString());
//			sshHost = TerminalConstants.NOT_SUPPORTED;
//			sshPort = TerminalConstants.NOT_SUPPORTED;
//			sshType = TerminalConstants.NOT_SUPPORTED;
//		}
//		if(!result.containsKey(GSISSH_HOST)) result.put(GSISSH_HOST, sshHost); // don't override persisted data!
//		if(!result.containsKey(GSISSH_PORT)) result.put(GSISSH_PORT, sshPort);

		result.put(ID, rawData.get(ID));
		result.put(NAME, rawData.get(NAME));

		//Insert here code for more SSH plugins
		//String sshParam1, sshParam1, ...
		//if(rawData.containsKey(SSH_INFO_XXXPlugin)){
		//...

		return result;

	}

	public static List<String> getConnectionTypes(Map<String,String> config){

		List<String> result = new ArrayList<String>();
		String methods = config.get(SSH_INFO_METHODS);
		if(methods == null || methods.trim().length() == 0) return result;
		String[] methodArray = methods.split("\\s");
		for (String method : methodArray) {
			ConnectionType c = ConnectionTypeRegistry.getInstance().getConnectionType(method);
			if(c != null) result.add(method); // if no suitable id was found, the proposed connection type is not supported by the client
		}

		return result;
	}

}
