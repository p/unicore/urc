/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.terminal.servicebrowser;

import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument.TargetSystemProperties;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.terminal.ConfigPersistence;
import de.fzj.unicore.rcp.terminal.TerminalConstants;
import de.fzj.unicore.rcp.terminal.UnicoreTerminalPlugin;
import de.fzj.unicore.rcp.terminal.ui.views.TerminalConfigView;

/**
 * NodeAction to open Terminal Configuration View
 * 
 * @author demuth
 */
public class ConfigureSSHAction extends NodeAction {
	
	public ConfigureSSHAction(Node node)
	{
		super(node);
		setText("Configure Terminal Connection");
		setToolTipText("Configure Site's SSH method");
		setImageDescriptor(UnicoreTerminalPlugin.getImageDescriptor("configure-terminal.png"));		
	}
	
	public void run()
	{
		//Check if Site SSH Information is already serialized
		String id = getNode().getPathToRoot().toString();
		boolean exists = ConfigPersistence.isSiteConfigured(id);
		if(!exists){
			//			TargetSystemProperties tssprops = Util.getTSSProps(getNode());
			//			Map<String,String> sshInfo = Util.extractSSHRelatedProperties(tssprops);
			//			Map<String,String> parsed = Util.parseSSHRawData(sshInfo);

			TargetSystemProperties tssprops = Util.getTSSProps(getNode());
			Map<String,String> rawData = Util.extractSSHRelatedProperties(tssprops);
			rawData.put(TerminalConstants.ID,id);
			rawData.put(TerminalConstants.NAME, getNode().getNamePathToRoot());
			
			Map<String,String> parsed = Util.parseSSHRawData(rawData);
			parsed.put(Util.DEFAULT_CONNECTION_TYPE, TerminalConstants.NOTSELECTED);
			parsed.put(Util.ATTRIBUTE_CONNECTION_TYPE_SUPPORTED, TerminalConstants.NOTSELECTED);
			ServiceBrowserTerminalConfigPersistence.getInstance().
				writeConfig(id, getNode().getNamePathToRoot(),TerminalConstants.NOTSELECTED, parsed);

			//configPersistence.writeConfig(id, getNode().getNamePathToRoot(),TerminalConstants.NOTSELECTED, rawData);
		}

		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable()
		{
			public void run() {
				TerminalConfigView sshConfig = (TerminalConfigView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(TerminalConfigView.ID);
				if(sshConfig != null) {					
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().bringToTop(sshConfig);
					sshConfig.refresh();
				} else {
					try {
						sshConfig = (TerminalConfigView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(TerminalConfigView.ID);					
						if(sshConfig != null) sshConfig.refresh();
					} catch (PartInitException e) {
						ServiceBrowserActivator.log(IStatus.WARNING,"Cant open Details view", e);
						return;
					}
				}
			}
		});
	}
}
