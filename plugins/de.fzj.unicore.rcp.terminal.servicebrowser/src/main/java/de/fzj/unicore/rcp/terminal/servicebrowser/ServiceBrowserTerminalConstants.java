package de.fzj.unicore.rcp.terminal.servicebrowser;

//import de.fzj.unicore.rcp.terminal.ssh.gsissh.GSISSHConstants;
import de.fzj.unicore.rcp.terminal.ssh.plain.PlainSSHConstants;

/**
 * Constants  
 * 
 * @author demuth, Andre Giesler
 */
interface ServiceBrowserTerminalConstants extends PlainSSHConstants { //, GSISSHConstants {

	public static final String SSH_INFO_PREFIX = "SSHInfo-";
	
	public static final String SSH_INFO_METHODS = SSH_INFO_PREFIX +"METHODS";

	public static final String SSH_INFO_GSISSH = Util.SSH_INFO_PREFIX+"GSISSH";

	public static final String SSH_INFO_PLAIN = Util.SSH_INFO_PREFIX+"PLAIN";

	public static final String SITE_TYPE_UNICORE_TSS = "unicoreTargetSystem";

	public static final String DEFAULT_CONNECTION_TYPE = "default connection type";
}
