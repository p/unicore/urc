/*****************************************************************************
 * Copyright (c) 2006, 2007 g-Eclipse Consortium 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for the
 * g-Eclipse project founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributors:
 *    Thomas Koeckerbauer GUP, JKU - initial API and implementation
 *****************************************************************************/

package de.fzj.unicore.rcp.terminal.servicebrowser;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.fzj.unicore.rcp.logmonitor.LogActivator;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author demuth, Andre Giesler
 */
public class TerminalFromServicebrowserPlugin extends AbstractUIPlugin {

	/** The plug-in ID */
	public static final String PLUGIN_ID = "de.fzj.unicore.rcp.terminal.servicebrowser"; //$NON-NLS-1$
	public final static String ICONS_PATH = "$nl$/icons/";//$NON-NLS-1$

	// The shared instance
	private static TerminalFromServicebrowserPlugin plugin;

	/**
	 * The constructor
	 */
	public TerminalFromServicebrowserPlugin() {
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start( final BundleContext context ) throws Exception {
		super.start(context);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop( final BundleContext context ) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static TerminalFromServicebrowserPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH+path);
	}

	/** 
	 * Log a message via Eclipse logging
	 * Defaults to INFO Level 
	 * @param msg the message to log
	 */
	public static void log(String msg) {
		log(msg, null);
	}
	/** 
	 * Log a message via Eclipse logging
	 * Defaults to INFO Level 
	 * @param msg the message to log
	 * @param e stacktrace to append
	 */
	public static void log(String msg, Exception e) {
		log(Status.INFO, msg, e, false);
	}
	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, false);
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param e stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, false);
	}

	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param forceAlarmUser forces the logging plugin to open a popup; usually, this should NOT be set to true!
	 */
	public static void log(int status, String msg, boolean forceAlarmUser) {
		log(status, msg, null, forceAlarmUser);
	}
	/** 
	 * Log a message via Eclipse logging
	 * @param status the IStatus level to log to
	 * @param msg the message to log
	 * @param e stacktrace to append
	 * @param forceAlarmUser forces the logging plugin to open a popup; usually, this should NOT be set to true!
	 */
	public static void log(int status, String msg, Exception e, boolean forceAlarmUser) {
		IStatus s = new Status(status, PLUGIN_ID, Status.OK, msg, e);
		LogActivator.log(plugin, s,forceAlarmUser);
	}
	
	
}
