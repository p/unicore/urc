package de.fzj.unicore.rcp.terminal.servicebrowser;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;

import de.fzj.unicore.rcp.servicebrowser.extensionpoints.INodeActionExtensionPoint;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;
import de.fzj.unicore.rcp.servicebrowser.ui.IServiceViewer;

/**
 * 
 * @author demuth
 */
public class TerminalNodeActions implements INodeActionExtensionPoint {

	public void addActions(Node node, IServiceViewer viewer) {
		if(TargetSystemNode.TYPE.equals(node.getType()) 
				|| TargetSystemFactoryNode.TYPE.equals(node.getType()))
		{
			node.getAvailableActions().add(new OpenTerminalAction(node));
			node.getAvailableActions().add(new ConfigureSSHAction(node));
		}

	}

	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
	
	}

}
