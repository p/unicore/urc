/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.terminal.servicebrowser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument.TargetSystemProperties;

import de.fzj.unicore.rcp.servicebrowser.ServiceBrowserActivator;
import de.fzj.unicore.rcp.servicebrowser.actions.NodeAction;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.terminal.ConfigPersistence;
import de.fzj.unicore.rcp.terminal.TerminalConstants;
import de.fzj.unicore.rcp.terminal.UnicoreTerminalPlugin;
import de.fzj.unicore.rcp.terminal.ui.views.TerminalConfigView;
import de.fzj.unicore.rcp.terminal.ui.dialogs.ChooseSSHMethodDialog;

/**
 * NodeAction to open Terminal
 * 
 * @author demuth, agiesler
 */
public class OpenTerminalAction extends NodeAction implements ServiceBrowserTerminalConstants {

	public OpenTerminalAction(Node node)
	{
		super(node);
		setText("Open Terminal");
		setToolTipText("Open terminal with default SSH method");
		setImageDescriptor(UnicoreTerminalPlugin.getImageDescriptor("terminal.png"));
	}

	public void run()
	{
		try {
			String nodeName = getNode().getNamePathToRoot();
			Node node = getNode();
			NodePath nodePath = node.getPathToRoot();
			String id = nodePath.toString();
			//Check if the site's SSH-Info is already provided in the client's XML storage
			String default_method;
			ServiceBrowserTerminalConfigPersistence configPersistence = ServiceBrowserTerminalConfigPersistence.getInstance();
			List<String> connectionTypes = new ArrayList<String>();
			boolean isConfigured = ServiceBrowserTerminalConfigPersistence.isSiteConfigured(id);	
			if(!isConfigured){
				TargetSystemProperties tssprops = Util.getTSSProps(node);
				if(tssprops == null) 
				{
					node.refresh(1, true, null);
					tssprops = Util.getTSSProps(node);
				}
				
				Map<String,String> rawData = Util.extractSSHRelatedProperties(tssprops);
				rawData.put(ID,id);
				rawData.put(NAME, nodeName);

				Map<String,String> parsed = Util.parseSSHRawData(rawData);
				parsed.put(Util.DEFAULT_CONNECTION_TYPE, TerminalConstants.NOTSELECTED);
				parsed.put(Util.ATTRIBUTE_CONNECTION_TYPE_SUPPORTED, TerminalConstants.NOTSELECTED);
				ServiceBrowserTerminalConfigPersistence.getInstance().
				writeConfig(id, getNode().getNamePathToRoot(),TerminalConstants.NOTSELECTED, parsed);

				List<String> ssh_methods = Util.getConnectionTypes(rawData);

				if(ssh_methods.size() > 1)
				{	
					runSSHDefaultMethodDialog(rawData, ssh_methods, nodePath);
				}
				else if(ssh_methods.size() == 1)
				{
					default_method = ssh_methods.get(0);
					connectionTypes.add(default_method);
					//showTerminalView(id, rawData, ssh_methods, default_method);
					runSSHDefaultMethodDialog(rawData, ssh_methods, nodePath);
				}
				else {
					default_method = TerminalConstants.NOTSELECTED;
					ssh_methods.add(default_method);


					Map<String,String> config = configPersistence.readConfig(id);					
					runSSHDefaultMethodDialog(config, ssh_methods, nodePath);

				}
			}
			else{
				default_method = configPersistence.getDefaultConnectionType(id);			

				//List<String> ssh_methods = ConfigPersistence.getPersistedConnectionTypes(id);
				List<String> ssh_methods = ConfigPersistence.getAvailablePersistedConnectionTypes(id);
				Map<String,String> config = configPersistence.readConfig(id);
				// Check if default method has already been set
				if(default_method == null || default_method.equals("") || 
						default_method.equals(TerminalConstants.NOTSELECTED)){				
					runSSHDefaultMethodDialog(config, ssh_methods, nodePath);
				}

				else{

					String name = config.get(NAME);
					if(name == null) name = nodeName;
					configPersistence.writeConfig(id, nodeName, default_method, config);
					connectionTypes.add(default_method);

					UnicoreTerminalPlugin.getDefault().showTerminalView(name,connectionTypes, config);
				}
			}
		} catch (Exception e) {
			TerminalFromServicebrowserPlugin.log(Status.ERROR, "Unable to open terminal connection: "+e.getMessage(), e);
		}
	}

	private void runSSHDefaultMethodDialog(final Map<String,String> rawData, final List<String> methods, final NodePath nodePath){
		final List<String> ssh_methods;
		if(methods.size()>1 && methods.contains(TerminalConstants.NOTSELECTED)){
			List<String> copy = new ArrayList<String>(methods);
			copy.remove(TerminalConstants.NOTSELECTED);
			ssh_methods = copy;
		}
		else{
			ssh_methods = methods;
		}

		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable()
		{

			public void run() {
				Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

				ChooseSSHMethodDialog dialog = new ChooseSSHMethodDialog(nodePath.toString(), shell, ssh_methods, getNode().getName());

				if(dialog.open() == Dialog.CANCEL) {
					return;
				}
				String default_method = dialog.getCurrentSSHMethodID();

				if(!ssh_methods.contains(default_method)){
					ssh_methods.add(default_method);
				}

				if(ssh_methods.contains(TerminalConstants.NOTSELECTED)){
					ssh_methods.remove(TerminalConstants.NOTSELECTED);
				}
				
				if(default_method != null && default_method.trim().length() > 0 && !default_method.equals(TerminalConstants.NOTSELECTED)){

					List<String> providers = new ArrayList<String>();
					providers.add(default_method);
					refreshAndShowTerminalConfigView();
					showTerminalView(nodePath.toString(), rawData, ssh_methods, default_method);
				}
				else{
					refreshAndShowTerminalConfigView();
				}
			}
		});
	}

	private void refreshAndShowTerminalConfigView(){
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable()
		{
			public void run() {
				TerminalConfigView sshConfig = (TerminalConfigView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(TerminalConfigView.ID);
				if(sshConfig != null) {					
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().bringToTop(sshConfig);
					sshConfig.refresh();
				} else {
					try {
						sshConfig = (TerminalConfigView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(TerminalConfigView.ID);					
						if(sshConfig != null) sshConfig.refresh();
					} catch (PartInitException e) {
						ServiceBrowserActivator.log(IStatus.WARNING,"Cant open Details view", e);
						return;
					}
				}
			}
		});	
	}

	private void refreshTerminalConfigView(){
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable()
		{
			public void run() {
				TerminalConfigView sshConfig = (TerminalConfigView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(TerminalConfigView.ID);
				if(sshConfig != null) {					
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().bringToTop(sshConfig);
					sshConfig.refresh();
				} 
			}
		});	
	}

	private void showTerminalView(String siteId,Map<String,String> rawData, List<String> methods, String default_method)
	{
		try {
			Map<String,String> parsed = Util.parseSSHRawData(rawData);
			parsed.put(Util.DEFAULT_CONNECTION_TYPE, default_method);
			Boolean plainSupported = methods.contains(CONNECTION_TYPE_ID_PLAIN);
			parsed.put(PLAIN_SUPPORTED,plainSupported.toString()); 
			
//			Boolean gsisshSupported = methods.contains(CONNECTION_TYPE_ID_GSISSH);
//			parsed.put(GSISSH_SUPPORTED,gsisshSupported.toString());

			ServiceBrowserTerminalConfigPersistence.getInstance().
			writeConfig(siteId, getNode().getNamePathToRoot(),default_method, parsed);

			UnicoreTerminalPlugin.getDefault().showTerminalView(parsed.get(NAME),methods, parsed);

			refreshTerminalConfigView();

		} catch (Exception e) {
			TerminalFromServicebrowserPlugin.log("Unable to open terminal view: "+e.getMessage(),e);
		}
	}
}
