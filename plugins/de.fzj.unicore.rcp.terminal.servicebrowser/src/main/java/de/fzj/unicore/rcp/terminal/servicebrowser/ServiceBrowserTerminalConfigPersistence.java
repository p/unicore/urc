package de.fzj.unicore.rcp.terminal.servicebrowser;

import java.util.Map;

import org.eclipse.ui.IMemento;

import de.fzj.unicore.rcp.terminal.ConfigPersistence;
import de.fzj.unicore.rcp.terminal.TerminalConstants;
//import de.fzj.unicore.rcp.terminal.ssh.gsissh.GSISSHConfigPersistence;
import de.fzj.unicore.rcp.terminal.ssh.plain.PlainSSHConfigPersistence;

/**
 * Persistence Management
 * 
 * @author demuth, Andre Giesler
 */
class ServiceBrowserTerminalConfigPersistence extends ConfigPersistence implements ServiceBrowserTerminalConstants {

	
private static ServiceBrowserTerminalConfigPersistence instance;
	
	protected static ServiceBrowserTerminalConfigPersistence getInstance()
	{
		if(instance == null)
		{
			instance = new ServiceBrowserTerminalConfigPersistence();
		}
		return instance;
	}
	
	@Override
	public boolean readConfig(Map<String, String> config, IMemento memento) {
		if(memento == null) return false;
		String defaultConnection = memento.getString(TerminalConstants.CONNECTION_TYPE);
		config.put(DEFAULT_CONNECTION_TYPE,defaultConnection); 
		if(getConnectionTypeMemento(CONNECTION_TYPE_ID_PLAIN,memento) != null)
		{
			PlainSSHConfigPersistence.getInstance().readConfig(config, memento);
		}
//		if(getConnectionTypeMemento(CONNECTION_TYPE_ID_GSISSH,memento) != null)
//		{
//			GSISSHConfigPersistence.getInstance().readConfig(config, memento);
//		}
		
		// *** Extend ***
		// read other SSH configs
		// *** Extend ***
		
		return true;
	}
	
	@Override
	public boolean writeConfig(Map<String, String> config,
			IMemento memento) {
		memento.putString(TerminalConstants.SITE_TYPE,SITE_TYPE_UNICORE_TSS);
		String defaultConnection = config.get(DEFAULT_CONNECTION_TYPE);
		if(defaultConnection == null) defaultConnection = TerminalConstants.NOTSELECTED;
		memento.putString(TerminalConstants.CONNECTION_TYPE,defaultConnection);
		String plainSupported = config.get(PLAIN_SUPPORTED);
		if(Boolean.parseBoolean(plainSupported))
		{
			PlainSSHConfigPersistence.getInstance().writeConfig(config, memento);
		}
//		String gsisshSupported = config.get(GSISSH_SUPPORTED);
//		if(Boolean.parseBoolean(gsisshSupported))
//		{
//			GSISSHConfigPersistence.getInstance().writeConfig(config, memento);
//		}
		
		// *** Extend ***
		// write other SSH configs
		// *** Extend ***
		
		return true;

	}
	

	
	/**
	 * Detects the default SSH connection method 
	 *
	 * @param	serverAddress	The unique NodePath of the site	
	 * @return  the default method in String format
	 */
	protected String getDefaultConnectionType(String id){
		Map<String,String> config = getInstance().readConfig(id);
		return config.get(DEFAULT_CONNECTION_TYPE);
	}
	

}
