package de.fzj.unicore.rcp.common.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.mapper.MapperWrapper;

/**
 * Utility for configuring XStream properly for (de-)serializing an extensible
 * object hierarchy. This involves iterating over plugin extensions.
 * 
 * @author bdemuth
 * 
 */
@SuppressWarnings("rawtypes")
public abstract class XStreamConfigurator {

	private Set<Class<?>> annotatedClasses = new HashSet<Class<?>>();
	private Set<Converter> converters = new HashSet<Converter>();
	private Map<String, Class> oldTypeAliases = new HashMap<String, Class>();

	private XStream serializer = null;
	private XStream deserializer = null;


	protected XStreamConfigurator() {
		iterateOverExtensions();
	}

	protected ClassLoader getClassLoader()
	{
		return new DelegatingExtensionClassloader(
				getClass().getClassLoader(), getExtensionIDs());
	}

	protected abstract String[] getExtensionIDs();



	protected XStream createXStream() {
		XStream xstream = new XStream() {

			@Override
			protected MapperWrapper wrapMapper(MapperWrapper next) {
				return new MapperWrapper(next) {
					/**
					 * Make deserialization more robust: don't throw an
					 * exception when a field has been removed
					 */

					@Override
					public boolean shouldSerializeMember(Class definedIn,
							String fieldName) {
						if(!super.shouldSerializeMember(definedIn, fieldName))
						{
							return false;
						}
						Class c = definedIn;
						while(c != null)
						{
							try {
								c.getDeclaredField(fieldName);
								return true;
							} catch (Exception e) {
								// do nothing, just continue
							}
							c = c.getSuperclass();
						}
						return false;
					}

				};
			}
		};

		xstream.registerConverter(new BackwardsCompatibleURIConverter());

		xstream.setClassLoader(getClassLoader());
		for (Class clazz : annotatedClasses) {
			xstream.processAnnotations(clazz);
		}
		for (Converter c : converters) {
			xstream.registerConverter(c);
		}

		return xstream;
	}

	public XStream getXStreamForDeserialization() {
		if (deserializer == null) {
			deserializer = createXStream();
			for (String key : oldTypeAliases.keySet()) {
				deserializer.alias(key, oldTypeAliases.get(key));
			}
		}
		return deserializer;
	}

	public XStream getXStreamForSerialization() {
		if (serializer == null) {
			serializer = createXStream();
		}
		return serializer;
	}

	/**
	 * New model object types are injected by the palette entry extension point.
	 * Iterate over the associated extensions and register all annotated classes
	 * and converters!
	 */
	protected void iterateOverExtensions() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		for(String extensionId : getExtensionIDs())
		{
			IExtensionPoint extensionPoint = registry
			.getExtensionPoint(extensionId);
			IConfigurationElement[] members = extensionPoint
			.getConfigurationElements();
			for (int m = 0; m < members.length; m++) {
				IConfigurationElement member = members[m];
				try {

					Class[] classes = getClasses(extensionId,member);
					if (classes != null) {
						for (Class clazz : classes) {
							registerAnnotatedClass(clazz);
						}
					}
					Converter[] converters = getConverters(extensionId,member);
					if (converters != null) {
						for (Converter c : converters) {
							registerConverter(c);
						}
					}

					Map<String, Class<?>> typeAliases = getOldTypeAliases(extensionId,member);
					if (typeAliases != null) {
						this.oldTypeAliases.putAll(typeAliases);
					}

				} catch (Exception ex) {
					log(IStatus.WARNING, "Could not determine all extensions for extension point "+extensionId,
							ex);
				}
			}
		}
	}

	/**
	 * Can be overriden by subclasses to register additional classes with
	 * XStream annotations, contributed by extensions.
	 * @param extensionId
	 * @param member
	 * @return
	 */
	protected Class[] getClasses(String extensionId, IConfigurationElement member)
	{
		return null;
	}

	/**
	 * Can be overriden by subclasses to register additional XStream converters,
	 * contributed by extensions.
	 * @param extensionId
	 * @param member
	 * @return
	 */
	protected Converter[] getConverters(String extensionId, IConfigurationElement member)
	{
		return null;
	}

	/**
	 * Can be overriden by subclasses to register obsolete aliases that must be
	 * bound to their current types explicitly 
	 * @param member
	 * @return
	 */
	protected Map<String, Class<?>> getOldTypeAliases(String extensionId, IConfigurationElement member)
	{
		return null;
	}

	protected abstract void log(int status, String msg, Exception ex);

	public void registerAnnotatedClass(Class<?> clazz) {
		annotatedClasses.add(clazz);
	}

	public void registerConverter(Converter c) {
		converters.add(c);
	}


}
