package de.fzj.unicore.rcp.common.actions;

public interface UnicoreCommonActionConstants {

	public static final String ACTION_SUBMIT = "submit";
	public static final String ACTION_SET_TERMINATION_TIME = "set termination time";
	public static final String ACTION_FETCH_OUTCOMES = "fetch outcomes";
}
