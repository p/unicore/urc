/*
 * SetterminationTimeFrame.java
 *
 * Created on August 31, 2006, 4:49 PM
 */

package de.fzj.unicore.rcp.common.guicomponents;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.common.Constants;

/**
 * 
 * @author demuth
 * @author bjoernh
 */
public class SelectTimeAndPropertiesDialog extends MessageDialog implements
		SelectionListener {

	DateTime dateControl, timeControl;
	Date initialSelection;
	Date minTime = null, maxTime = null, selectedTime = null;
	String errorMessage;

	/**
	 * Error message label widget.
	 */
	private Text errorMessageText;
	private Group grpTerminationTime;
	private Group grpProperties;
	private Map<String, String> properties;

	private HashMap<String, Text> property2Field;

	public SelectTimeAndPropertiesDialog(Shell parentShell, String message,
			Map<String, String> _properties) {
		super(parentShell, "Choose date and time", null, message,
				MessageDialog.NONE, new String[] { IDialogConstants.OK_LABEL,
						IDialogConstants.CANCEL_LABEL }, 0);
		setShellStyle(SWT.DIALOG_TRIM | SWT.MAX | SWT.RESIZE
				| SWT.APPLICATION_MODAL);
		this.properties = _properties;
	}

	/**
	 * Notifies that this dialog's button with the given id has been pressed.
	 * <p>
	 * The <code>Dialog</code> implementation of this framework method calls
	 * <code>okPressed</code> if the ok button is the pressed, and
	 * <code>cancelPressed</code> if the cancel button is the pressed. All other
	 * button presses are ignored. Subclasses may override to handle other
	 * buttons, but should call <code>super.buttonPressed</code> if the default
	 * handling of the ok and cancel buttons is desired.
	 * </p>
	 * <p>
	 * Copied from {@link Dialog} by bjoernh, because {@link MessageDialog}
	 * overrides useful default behavior.
	 * </p>
	 * 
	 * @param buttonId
	 *            the id of the button that was pressed (see
	 *            <code>IDialogConstants.*_ID</code> constants)
	 */
	protected void buttonPressed(int buttonId) {
		if (IDialogConstants.OK_ID == buttonId) {
			okPressed();
		} else if (IDialogConstants.CANCEL_ID == buttonId) {
			cancelPressed();
		}
	}

	@Override
	protected void cancelPressed() {
		selectedTime = null;
		super.cancelPressed();
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		for (String property : properties.keySet()) {
			properties.put(property, property2Field.get(property).getText());
		}
		super.okPressed();
	}

	@Override
	protected void configureShell(Shell newShell) {
		newShell.setMinimumSize(new Point(500, 400));
		newShell.setText("Calendar");
		super.configureShell(newShell);
	}

	@Override
	public Control createCustomArea(Composite parent) {
		Composite area = new Composite(parent, SWT.NONE);
		area.setLayout(new GridLayout(2, false));
		area.setLayoutData(new GridData(GridData.FILL_BOTH));

		grpProperties = new Group(area, SWT.NONE);
		GridData gd_grpProperties = new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1);
		gd_grpProperties.widthHint = 312;
		grpProperties.setLayoutData(gd_grpProperties);
		grpProperties.setLayout(new GridLayout(2, false));
		grpProperties.setText("Properties");

		property2Field = new HashMap<String, Text>();
		for (String property : properties.keySet()) {
			createPropertyInput(area, property);
		}

		grpTerminationTime = new Group(area, SWT.NONE);
		grpTerminationTime.setText("Termination Time");
		grpTerminationTime.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL,
				false, true, 1, 1));
		grpTerminationTime.setLayout(new GridLayout(1, false));
		dateControl = new DateTime(grpTerminationTime, SWT.BORDER
				| SWT.CALENDAR | SWT.LONG);
		dateControl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false,
				false, 1, 1));
		dateControl.addSelectionListener(this);
		timeControl = new DateTime(grpTerminationTime, SWT.BORDER | SWT.TIME
				| SWT.LONG);
		timeControl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false,
				false, 1, 1));
		timeControl.addSelectionListener(this);

		errorMessageText = new Text(area, SWT.READ_ONLY | SWT.WRAP);
		errorMessageText.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true,
				true, 2, 1));
		errorMessageText.setForeground(errorMessageText.getDisplay()
				.getSystemColor(SWT.COLOR_RED));
		errorMessageText.setBackground(errorMessageText.getDisplay()
				.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		if (initialSelection != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(initialSelection);
			dateControl.setDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH),
					c.get(Calendar.DATE));
			
			timeControl.setHours(c.get(Calendar.HOUR_OF_DAY));
			
			timeControl.setMinutes(c.get(Calendar.MINUTE));
			timeControl.setSeconds(c.get(Calendar.SECOND));
		}
		getShell().pack();
		return area;
	}

	/**
	 * @param area
	 * @param property
	 */
	private void createPropertyInput(Composite area, String property) {
		Label lblLabel = new Label(grpProperties, SWT.NONE);
		lblLabel.setText(property);

		Text txtValue = new Text(grpProperties, SWT.BORDER);
		txtValue.setText(properties.get(property));
		txtValue.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false,
				1, 1));
		property2Field.put(property, txtValue);
	}

	/**
	 * Returns the selected time or null if the user pressed Cancel
	 * 
	 * @return
	 */
	public Date getSelectedTime() {
		return selectedTime;
	}

	public void setErrorMessage(String errorMessage) {

		this.errorMessage = errorMessage;
		if (errorMessageText != null && !errorMessageText.isDisposed()) {
			Point shellSize = getShell().getSize();
			int oldSpace = shellSize.y;
			errorMessageText
					.setText(errorMessage == null ? " \n " : errorMessage); //$NON-NLS-1$
			// Disable the error message text control if there is no error, or
			// no error text (empty or whitespace only). Hide it also to avoid
			// color change.
			// See https://bugs.eclipse.org/bugs/show_bug.cgi?id=130281
			boolean hasError = errorMessage != null
					&& (StringConverter.removeWhiteSpaces(errorMessage))
							.length() > 0;
			errorMessageText.setEnabled(hasError);
			errorMessageText.setVisible(hasError);

			errorMessageText.getParent().update();
			errorMessageText.getParent().layout();
			int newSpace = getShell().computeSize(-1, -1).y;
			if (hasError && newSpace > oldSpace) {

				getShell().setSize(shellSize.x,
						shellSize.y + newSpace - oldSpace);
			}
			// Access the ok button by id, in case clients have overridden
			// button creation.
			// See https://bugs.eclipse.org/bugs/show_bug.cgi?id=113643
			Control button = getButton(IDialogConstants.OK_ID);
			if (button != null) {
				button.setEnabled(errorMessage == null);
			}
		}
	}

	public void setInitialSelection(Date initialSelection) {
		this.initialSelection = initialSelection;
	}

	public void setMaxTime(Date maxTime) {
		this.maxTime = maxTime;
	}

	public void setMinTime(Date minTime) {
		this.minTime = minTime;
	}

	protected void validateSelection() {
		selectedTime = null;
		Calendar c = Calendar.getInstance();
		c.set(dateControl.getYear(), dateControl.getMonth(),
				dateControl.getDay(), timeControl.getHours(),
				timeControl.getMinutes(), timeControl.getSeconds());
		Date newTime = c.getTime();
		if (minTime != null && newTime.before(minTime)) {
			setErrorMessage("Invalid date. Please select a date after "
					+ Constants.getDefaultDateFormat().format(minTime) + ".");
		} else if (maxTime != null && newTime.after(maxTime)) {
			setErrorMessage("Invalid date. Please select a date before "
					+ Constants.getDefaultDateFormat().format(maxTime) + ".");
		} else {
			setErrorMessage(null);
			selectedTime = newTime;
		}
	}

	public void widgetSelected(SelectionEvent e) {
		if (e.getSource().equals(timeControl)
				|| e.getSource().equals(dateControl)) {
			validateSelection();
		}
	}

	/**
	 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		widgetSelected(e);
	}
}
