package de.fzj.unicore.rcp.common.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.ggf.schemas.jsdl.x2005.x11.jsdl.ExactType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.RangeValueType;

class IntervalEvent implements Comparable<IntervalEvent> {
	public static final int TYPE_INTERVAL_START = 0, TYPE_INTERVAL_END = 1;

	public Double time;
	public Interval interval;
	public int type;

	IntervalEvent(double time, Interval interval, int type) {
		this.time = time;
		this.interval = interval;
		this.type = type;
	}

	public int compareTo(IntervalEvent e) {
		int d = time.compareTo(e.time);
		if (d == 0) {
			if (type == TYPE_INTERVAL_START) {
				if (e.type == TYPE_INTERVAL_START) {
					if (!interval.containsStart() && e.interval.containsStart()) {
						return 1;
					}
				} else if (e.type == TYPE_INTERVAL_END) {
					if (!interval.containsStart() || !e.interval.containsEnd()) {
						return 1;
					}
				}
			} else if (type == TYPE_INTERVAL_END) {
				if (e.type == TYPE_INTERVAL_START) {
					if (interval.containsEnd() && e.interval.containsStart()) {
						return 1;
					}
				} else if (e.type == TYPE_INTERVAL_END) {
					if (!interval.containsEnd() || e.interval.containsEnd()) {
						return 1;
					}
				}
			}
			return -1;
		} else {
			return d;
		}

	}

	@Override
	public String toString() {
		String type = this.type == TYPE_INTERVAL_START ? "start of interval"
				: "end of interval";
		return "Event: " + type + " " + interval.toString();
	}

}

public class RangeValueUtils {

	public static double EPSILON = 1E-12;

	public static List<Interval> createIntervalsFromRangeValueType(
			RangeValueType r) {
		List<Interval> result = new ArrayList<Interval>();
		ExactType[] exacts = r.getExactArray();
		if (exacts != null) {
			for (ExactType exact : exacts) {
				double mid = exact.getDoubleValue();
				Interval i;
				if (exact.isSetEpsilon()) {
					double epsilon = exact.getEpsilon();
					i = new Interval(mid - epsilon, mid + epsilon);
				} else {
					i = new Interval(mid, mid);
				}
				result.add(i);
			}
		}

		RangeType[] ranges = r.getRangeArray();
		if (ranges != null) {
			for (RangeType range : ranges) {
				double start = range.getLowerBound().getDoubleValue();
				boolean containsStart = !range.getLowerBound()
						.getExclusiveBound();
				double end = range.getUpperBound().getDoubleValue();
				boolean containsEnd = !range.getUpperBound()
						.getExclusiveBound();
				Interval i = new Interval(start, end, containsStart,
						containsEnd);
				result.add(i);
			}
		}
		if (r.isSetLowerBoundedRange()) {
			double start = r.getLowerBoundedRange().getDoubleValue();
			boolean containsStart = !r.getLowerBoundedRange()
					.isSetExclusiveBound();
			result.add(new Interval(start, Double.POSITIVE_INFINITY,
					containsStart, false));
		}
		if (r.isSetUpperBoundedRange()) {
			double end = r.getUpperBoundedRange().getDoubleValue();
			boolean containsEnd = !r.getUpperBoundedRange()
					.isSetExclusiveBound();
			result.add(new Interval(Double.NEGATIVE_INFINITY, end, false,
					containsEnd));
		}
		return result;
	}

	public static boolean fitsInto(RangeValueType a, RangeValueType b) {
		// a = transformExactIntoLowerBounds(a);
		return testIntersection(a, b);
	}

	public static double getMaximum(RangeValueType r) {
		double result = Double.NEGATIVE_INFINITY;
		List<Interval> intervals = createIntervalsFromRangeValueType(r);
		for (Interval interval : intervals) {
			if (interval.getEnd() > result) {
				result = interval.getEnd();
			}
		}
		return result;
	}

	public static double getMinimum(RangeValueType r) {
		double result = Double.POSITIVE_INFINITY;
		List<Interval> intervals = createIntervalsFromRangeValueType(r);
		for (Interval interval : intervals) {
			if (interval.getStart() < result) {
				result = interval.getStart();
			}
		}
		return result;
	}

	/**
	 * Merge all intersecting intervals in order to obtain a list of disjoint
	 * intervals.
	 * 
	 * @param intervals
	 * @return
	 */
	public static List<Interval> mergeIntervals(List<Interval> intervals) {
		List<Interval> result = new ArrayList<Interval>();

		List<IntervalEvent> events = new ArrayList<IntervalEvent>();

		for (Interval interval : intervals) {
			IntervalEvent start = new IntervalEvent(interval.getStart(),
					interval, IntervalEvent.TYPE_INTERVAL_START);
			events.add(start);
			IntervalEvent end = new IntervalEvent(interval.getEnd(), interval,
					IntervalEvent.TYPE_INTERVAL_END);
			events.add(end);
		}

		Collections.sort(events);
		Interval current = null;
		int numIntervalsOnSweepLine = 0;
		for (IntervalEvent e : events) {
			switch (e.type) {
			case IntervalEvent.TYPE_INTERVAL_START:
				if (numIntervalsOnSweepLine == 0) {
					current = new Interval(e.interval.getStart(),
							Double.POSITIVE_INFINITY,
							e.interval.containsStart(), false);
				}
				numIntervalsOnSweepLine++;
				break;
			case IntervalEvent.TYPE_INTERVAL_END:
				numIntervalsOnSweepLine--;
				if (numIntervalsOnSweepLine == 0) {
					current.setEnd(e.interval.getEnd());
					current.setContainsEnd(e.interval.containsEnd());
					result.add(current);
				}
			}
		}
		return result;
	}

	/**
	 * Returns true iff there is at least one intersection between an interval
	 * from list a and an interval from list b. The prerequisite is that all
	 * intervals in each of the lists are disjoint. This can be enforced by
	 * using {@link #mergeIntervals(List)}..
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static boolean testIntersection(List<Interval> a, List<Interval> b) {

		List<IntervalEvent> events = new ArrayList<IntervalEvent>();

		List<Interval> intervals = new ArrayList<Interval>(a);
		intervals.addAll(b);
		for (Interval interval : intervals) {
			IntervalEvent start = new IntervalEvent(interval.getStart(),
					interval, IntervalEvent.TYPE_INTERVAL_START);
			events.add(start);
			IntervalEvent end = new IntervalEvent(interval.getEnd(), interval,
					IntervalEvent.TYPE_INTERVAL_END);
			events.add(end);
		}

		Collections.sort(events);
		int numIntervalsOnSweepLine = 0;
		for (IntervalEvent e : events) {
			switch (e.type) {
			case IntervalEvent.TYPE_INTERVAL_START:
				numIntervalsOnSweepLine++;
				break;
			case IntervalEvent.TYPE_INTERVAL_END:
				numIntervalsOnSweepLine--;
			}
			if (numIntervalsOnSweepLine > 1) {
				return true;
			}
		}
		return false;
	}

	public static boolean testIntersection(RangeValueType a, RangeValueType b) {
		List<Interval> aIntervals = createIntervalsFromRangeValueType(a);
		List<Interval> bIntervals = createIntervalsFromRangeValueType(b);
		// make intervals disjoint
		aIntervals = mergeIntervals(aIntervals);
		bIntervals = mergeIntervals(bIntervals);
		return testIntersection(aIntervals, bIntervals);
	}

	public static RangeValueType transformExactIntoLowerBounds(RangeValueType r) {
		RangeValueType result = (RangeValueType) r.copy();
		List<ExactType> newExacts = new ArrayList<ExactType>();
		List<RangeType> newLowerBounded = new ArrayList<RangeType>();

		ExactType[] exacts = r.getExactArray();
		if (exacts != null) {
			for (ExactType exact : exacts) {

				if (exact.isSetEpsilon()) {
					newExacts.add(exact);
				} else {
					double lower = exact.getDoubleValue();
					RangeType range = RangeType.Factory.newInstance();
					range.addNewLowerBound().setDoubleValue(lower);
					range.addNewUpperBound().setDoubleValue(
							Double.POSITIVE_INFINITY);
					newLowerBounded.add(range);
				}
			}
			result.setExactArray(newExacts.toArray(new ExactType[0]));
			RangeType[] ranges = result.getRangeArray();
			for (RangeType rangeType : ranges) {
				newLowerBounded.add(0, rangeType);
			}
			result.setRangeArray(newLowerBounded.toArray(new RangeType[newLowerBounded.size()]));
		}
		return result;
	}

}
