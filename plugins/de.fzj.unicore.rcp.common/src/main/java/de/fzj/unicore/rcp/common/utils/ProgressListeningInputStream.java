package de.fzj.unicore.rcp.common.utils;

import java.io.IOException;
import java.io.InputStream;
import de.fzj.unicore.uas.fts.ProgressListener;

public class ProgressListeningInputStream extends InputStream {

	private InputStream delegate;
	private ProgressListener<Long> listener;
	

	public ProgressListeningInputStream(InputStream delegate,
			ProgressListener<Long> listener) {
		super();
		this.delegate = delegate;
		this.listener = listener;
	}

	
	public int available() throws IOException {
		return delegate.available();
	}
	
	
	public void close() throws IOException {
		delegate.close();
	}

	
	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}



	public int hashCode() {
		return delegate.hashCode();
	}

	public void mark(int readlimit) {
		delegate.mark(readlimit);
	}


	public boolean markSupported() {
		return delegate.markSupported();
	}


	private void notifyProgress(long bytes) throws ProgressListener.CancelledException
	{
		if(listener != null) 
		{
			if(listener.isCancelled()) throw new ProgressListener.CancelledException("Data transfer was cancelled by the user!");
			listener.notifyProgress(bytes);
			
		}
	}


	public int read() throws IOException {
		int result = delegate.read();
		if(result > 0) notifyProgress(result);
		return result;
	}


	public int read(byte[] b) throws IOException {
		
		int result = delegate.read(b);
		if(result > 0) notifyProgress(result);
		return result;
	}


	public int read(byte[] b, int off, int len) throws IOException {
		int result = delegate.read(b,off,len);
		if(result > 0) notifyProgress(result);
		return result;
	}


	public void reset() throws IOException {
		delegate.reset();
	}


	public long skip(long n) throws IOException {
		return delegate.skip(n);
	}


	public String toString() {
		return delegate.toString();
	}
	
	
}
