package de.fzj.unicore.rcp.common.problems;

import de.fzj.unicore.rcp.logmonitor.problems.GraphicalSolution;
import eu.unicore.problemutil.Problem;

/**
 * @deprecated 2014-04-28 ProjectMetadataRestorer used Eclipse internal classes
 *             The problem for which this is a solution hardly ever occurred.
 * @author bjoernh
 *
 *         28.04.2015 14:37:15
 *
 */
public class RestoreProjectMetadataSolution extends GraphicalSolution {

	public RestoreProjectMetadataSolution() {
		super("RESTORE_CORRUPTED_PROJECT_METADATA",
				"PROJECT_METADATA_CORRUPTED",
				"Try to restore missing workspace metadata.");
	}

	@Override
	public boolean solve(String message, Problem[] problems, String details)
			throws Exception {
		// new ProjectMetadataRestorer().restore();
		return false;
	}

}
