/**
 * (c) Copyright Mirasol Op'nWorks Inc. 2002, 2003. 
 * http://www.opnworks.com
 * Created on Apr 2, 2003 by lgauthier@opnworks.com
 */

package de.fzj.unicore.rcp.common.detailsView;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * Label provider for the Viewer
 * 
 * @see org.eclipse.jface.viewers.LabelProvider
 */
public class DetailLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	// Names of images used to represent checkboxes
	public static final String CHECKED_IMAGE = "checked";
	public static final String UNCHECKED_IMAGE = "unchecked";

	/**
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
	 *      int)
	 */
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	/**
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
	 *      int)
	 */
	public String getColumnText(Object element, int columnIndex) {
		String result = "";
		KeyValue kv = (KeyValue) element;
		switch (columnIndex) {
		case 0:
			result = kv.getKey();
			break;
		case 1:
			result = kv.getValue();
			// result = StringUtils.convertNewLines(result);
			break;
		default:
			break;
		}
		return result;
	}

}
