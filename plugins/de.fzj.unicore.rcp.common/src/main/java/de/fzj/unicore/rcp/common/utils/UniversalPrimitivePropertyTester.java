package de.fzj.unicore.rcp.common.utils;

import java.lang.reflect.Method;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.IStatus;

import de.fzj.unicore.rcp.common.UnicoreCommonActivator;

public class UniversalPrimitivePropertyTester extends PropertyTester {

	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		Method m;
		try {
			m = receiver.getClass().getMethod(property);
			
			Object result = m.invoke(receiver, args);
			if(expectedValue == null)
			{
				return result == null;
			}
			else if(result == null) return false;
			return expectedValue.toString().equalsIgnoreCase(result.toString());
		} catch (Exception e) {
			UnicoreCommonActivator.log(IStatus.ERROR, "Problem while checking property "+property+" of object "+receiver+": "+e.getMessage(), e);
			return false;
		}
	}

}
