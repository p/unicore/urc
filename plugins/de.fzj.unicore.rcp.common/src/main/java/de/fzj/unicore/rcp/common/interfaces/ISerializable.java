package de.fzj.unicore.rcp.common.interfaces;

import java.io.Serializable;

/**
 * This interface is used to mark objects that can be serialized. It provides a
 * number of hooks that are called before and after serialization and after
 * deserialization.
 * 
 * @author bdemuth
 * 
 */
public interface ISerializable extends Serializable {

	/**
	 * Implementing classes can implement this in order to execute code before
	 * the object is used after it has been deserialized.
	 */
	public void afterDeserialization();

	/**
	 * This is called right after serializing the object and can be used to
	 * return the object to its original state (before
	 * {@link #beforeSerialization()} was called).
	 */
	public void afterSerialization();

	/**
	 * This is called right before serializing the object (e.g. for saving it to
	 * the disk) and can be used to clean up.
	 * 
	 */
	public void beforeSerialization();

	/**
	 * This method is used to update elements to the latest model version. It
	 * provides a hook for fixing old serialized values without the need to
	 * write an XStream {@link Converter} (of course, converters might still be
	 * necessary if the serialized XML deviates too much). It is preferred over
	 * writeReplace(), as it more explicitly states the old and new version.
	 * Also, it is called in a much more orderly fashion: a child element may
	 * assume that its parent element has already been converted to the latest
	 * version.
	 * 
	 * This method should be called before {@link #afterDeserialization()} is
	 * called.
	 * 
	 * @param oldVersion
	 * @param currentVersion
	 */
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion);
}
