/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.common.detailsView;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.TextViewer;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerEditor;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.console.actions.TextViewerAction;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.texteditor.FindReplaceAction;

import de.fzj.unicore.rcp.common.adapters.DetailsAdapter;
import de.fzj.unicore.rcp.common.adapters.LowLevelInfo;
import de.fzj.unicore.rcp.common.properties.IPropertySource;
import de.fzj.unicore.rcp.common.utils.StringUtils;

public class DetailsView extends ViewPart implements ISelectionListener,
		PropertyChangeListener {

	/**
	 * make sure that the view is being updated by the SWT thread in order to
	 * avoid illegal thread access to GUI components
	 * 
	 * @author demuth
	 * 
	 */
	private class InputUpdater implements Runnable {
		IAdaptable adaptable;

		public InputUpdater(IAdaptable adaptable) {
			this.adaptable = adaptable;
		}

		public void run() {
			// 1st: try to get Details for table stuff
			DetailsAdapter adapter = (DetailsAdapter) adaptable
					.getAdapter(DetailsAdapter.class);
			if (adapter == null) {
				return;
			}
			Map<String, String> h = adapter.getMap();
			// 2nd: try to get LowLevel Info for the TextArea
			LowLevelInfo s = (LowLevelInfo) adaptable
					.getAdapter(LowLevelInfo.class);
			// update guis
			if (h == null && s == null) {
				return;
			}

			if (h == null) {
				keyValueList = new KeyValueList();
				tableViewer.setInput(keyValueList);
			} else {
				keyValueList = new KeyValueList(h);
				tableViewer.setInput(keyValueList);
			}

			if (s == null) {
				lowLevelInfo.getDocument().set("no low level information");
			} else {

				String infos = s.getLowLevelInfo();

				infos = StringUtils.convertNewLines(infos);

				lowLevelInfo.getDocument().set(infos);

				if ("".equals(s.getLowLevelInfo())) {
					lowLevelInfo.getDocument()
							.set("no low level information !");
					// lowLevelInfo.getControl().setToolTipText(s.getName());
				}
			}
			pack();
		}
	}

	/**
	 * InnerClass that acts as a proxy for the ExampleTaskList providing content
	 * for the Table. It implements the ITaskListViewer interface since it must
	 * register changeListeners with the ExampleTaskList
	 */
	class UserContentProvider implements IStructuredContentProvider,
			IKeyValueListViewer {
		/*
		 * (non-Javadoc)
		 * 
		 * @see ITaskListViewer#addTask(ExampleTask)
		 */
		public void addKeyValue(KeyValue kv) {
			tableViewer.add(kv);
		}

		public void dispose() {
			keyValueList.removeChangeListener(this);
		}

		// Return the tasks as an array of Objects
		public Object[] getElements(Object parent) {
			return keyValueList.getKeyValues().toArray();
		}

		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
			if (newInput != null) {
				((KeyValueList) newInput).addChangeListener(this);
			}
			if (oldInput != null) {
				((KeyValueList) oldInput).removeChangeListener(this);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see ITaskListViewer#removeTask(ExampleTask)
		 */
		public void removeKeyValue(KeyValue kv) {
			tableViewer.remove(kv);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see ITaskListViewer#updateTask(ExampleTask)
		 */
		public void updateKeyValue(KeyValue u) {
			tableViewer.update(u, null);
		}
	}

	public static String ID = "de.fzj.unicore.details";
	private Table table;
	private TableViewer tableViewer;
	private TabFolder tabFolder;
	private TextViewer lowLevelInfo;
	private CellEditor valueEditor;
	private IPropertySource model;

	private Map<String, IAction> lowLevelActions;
	private MenuManager lowLevelMenuManager;

	private KeyValueList keyValueList;

	// Set column names
	private String[] columnNames = new String[] { "Key", "Value", };

	protected void createLowLevelActions() {
		IActionBars actionBars = getViewSite().getActionBars();
		TextViewerAction action = new TextViewerAction(lowLevelInfo,
				ITextOperationTarget.SELECT_ALL);
		action.configureAction("Select &All", "", "");
		action.setActionDefinitionId(ActionFactory.SELECT_ALL.getCommandId());
		lowLevelActions = new HashMap<String, IAction>();
		lowLevelActions.put(ActionFactory.SELECT_ALL.getId(), action);
		actionBars.setGlobalActionHandler(ActionFactory.SELECT_ALL.getId(),
				action);

		final TextViewerAction copyAction = new TextViewerAction(lowLevelInfo,
				ITextOperationTarget.COPY);
		copyAction.configureAction("&Copy", "", "");
		copyAction.setImageDescriptor(PlatformUI.getWorkbench()
				.getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_TOOL_COPY));
		copyAction.setActionDefinitionId(ActionFactory.COPY.getCommandId());
		lowLevelActions.put(ActionFactory.COPY.getId(), copyAction);
		actionBars.setGlobalActionHandler(ActionFactory.COPY.getId(),
				copyAction);
		lowLevelInfo
				.addSelectionChangedListener(new ISelectionChangedListener() {

					public void selectionChanged(SelectionChangedEvent event) {
						copyAction.update();
					}
				});

		ResourceBundle bundle = LowLevelInfoBundleMessages.getBundle();
		FindReplaceAction fraction = new FindReplaceAction(
				bundle,
				"find_replace_action_", getSite().getShell(), lowLevelInfo.getFindReplaceTarget()); //$NON-NLS-1$
		fraction.setActionDefinitionId(ActionFactory.FIND.getCommandId());
		lowLevelActions.put(ActionFactory.FIND.getId(), fraction);
		actionBars.setGlobalActionHandler(ActionFactory.FIND.getId(), fraction);
	}

	/**
	 * Create a new shell, add the widgets, open the shell
	 * 
	 * @return the shell that was created
	 */
	@Override
	public void createPartControl(Composite parent) {
		tabFolder = new TabFolder(parent, SWT.BOTTOM);
		TabItem detailsTabItem = new TabItem(tabFolder, SWT.NONE);
		detailsTabItem.setText("Details");

		createTable(tabFolder);

		// Create and setup the TableViewer
		createTableViewer();
		tableViewer.setContentProvider(new UserContentProvider());
		tableViewer.setLabelProvider(new DetailLabelProvider());
		// The input for the table viewer is the instance of ExampleTaskList
		keyValueList = new KeyValueList();
		tableViewer.setInput(keyValueList);

		detailsTabItem.setControl(tableViewer.getControl());

		TabItem lowLevelTabItem = new TabItem(tabFolder, SWT.NONE);
		lowLevelTabItem.setText("Low Level Information");
		lowLevelInfo = new TextViewer(tabFolder, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL);
		final StyledText styledText = lowLevelInfo.getTextWidget();
		styledText.setWordWrap(false);
		styledText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent event) {
				if (event.keyCode == 'p' && (event.stateMask & SWT.CTRL) != 0) {
					styledText.print();
				}
			}
		});
		Document doc = new Document();
		doc.set("uninitialized.");
		lowLevelInfo.setDocument(doc);
		lowLevelInfo.setEditable(false);
		lowLevelTabItem.setControl(lowLevelInfo.getControl());
		String id = "#ContextMenu"; //$NON-NLS-1$
		lowLevelMenuManager = new MenuManager("#ContextMenu", id); //$NON-NLS-1$
		lowLevelMenuManager.setRemoveAllWhenShown(true);
		lowLevelMenuManager.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager m) {
				lowLevelContextMenuAboutToShow(m);
			}
		});
		Menu menu = lowLevelMenuManager.createContextMenu(lowLevelInfo
				.getControl());
		lowLevelInfo.getControl().setMenu(menu);

		createLowLevelActions();

		getSite().registerContextMenu(id, lowLevelMenuManager, lowLevelInfo);

		getViewSite().getPage().addSelectionListener(this);
		this.selectionChanged(getViewSite().getPage().getActivePart(),
				getViewSite().getPage().getSelection());

	}

	/**
	 * Create the Table
	 */
	private void createTable(Composite parent) {
		int style = SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.FULL_SELECTION;

		table = new Table(parent, style);
		table.setLayout(new GridLayout());
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
	}

	/**
	 * Create the TableViewer
	 */
	private void createTableViewer() {

		tableViewer = new TableViewer(table);
		tableViewer.setUseHashlookup(true);

		// 0th column Key
		final TableColumn column0 = new TableColumn(table, SWT.LEFT, 0);
		column0.setText(columnNames[0]);
		column0.setWidth(200);

		// 1st column Value
		final TableColumn column1 = new TableColumn(table, SWT.LEFT, 1);
		column1.setText(columnNames[1]);
		column1.setWidth(300);

		tableViewer.setColumnProperties(columnNames);

		// Create the cell editors
		CellEditor[] editors = new CellEditor[table.getColumnCount()];

		// Column 0
		TextCellEditor keyEditor = new TextCellEditor(table);
		((Text) keyEditor.getControl()).setEditable(false);
		editors[0] = keyEditor;
		valueEditor = new ValueCellEditor(table);

		editors[1] = valueEditor;
		tableViewer.setCellEditors(editors);
		tableViewer.setCellModifier(new DetailsCellModifier(this));

		TableViewerEditor.create(tableViewer,
				new ColumnViewerEditorActivationStrategy(tableViewer) {
					/**
					 * @see org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy#isEditorActivationEvent(org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent)
					 */
					@Override
					protected boolean isEditorActivationEvent(
							ColumnViewerEditorActivationEvent event) {
						return event.eventType == ColumnViewerEditorActivationEvent.MOUSE_DOUBLE_CLICK_SELECTION;
					}
				}, ColumnViewerEditor.KEYBOARD_ACTIVATION
						| ColumnViewerEditor.TABBING_HORIZONTAL
						| ColumnViewerEditor.TABBING_VERTICAL
						| ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR);
	}

	@Override
	public void dispose() {
		if (lowLevelMenuManager != null) {
			lowLevelMenuManager.dispose();
		}
		getViewSite().getPage().removeSelectionListener(this);
		if (model != null) {
			model.removePropertyChangeListener(this);
		}
	}

	/**
	 * Return the column names in a collection
	 * 
	 * @return List containing column names
	 */
	public java.util.List<String> getColumnNames() {
		return Arrays.asList(columnNames);
	}

	/**
	 * @return currently selected item
	 */
	public ISelection getSelection() {
		return tableViewer.getSelection();
	}

	protected void lowLevelContextMenuAboutToShow(IMenuManager menuManager) {
		IDocument doc = lowLevelInfo.getDocument();
		if (doc == null) {
			return;
		}

		menuManager.add(lowLevelActions.get(ActionFactory.COPY.getId()));
		menuManager.add(lowLevelActions.get(ActionFactory.SELECT_ALL.getId()));

		menuManager.add(new Separator("FIND")); //$NON-NLS-1$
		menuManager.add(lowLevelActions.get(ActionFactory.FIND.getId()));

		menuManager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	public void pack() {
		table.getColumn(0).pack();
		table.getColumn(1).pack();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent arg0) {
		PlatformUI.getWorkbench().getDisplay()
				.asyncExec(new InputUpdater((IAdaptable) arg0.getSource()));
	}

	public void selectionChanged(IWorkbenchPart part, ISelection selection) {

		if (selection instanceof IStructuredSelection) {
			Object first = ((IStructuredSelection) selection).getFirstElement();
			if (first instanceof IAdaptable) { // --> try to get Infos

				new InputUpdater((IAdaptable) first).run();
				// stop listening on old model and start listening on new model
				if (model != null) {
					model.removePropertyChangeListener(this);
				}
				model = null;
				if (first instanceof IPropertySource) {
					model = (IPropertySource) first;
					model.addPropertyChangeListener(this);
				}
			}

		}

	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		tabFolder.setFocus();
	}

}