package de.fzj.unicore.rcp.common.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

public class DirectoryWatcher extends BackgroundJob {

	private File[] watched;
	private long updateInterval;
	private Map<File,Long> cached = new HashMap<File, Long>();
	private List<DirectoryListener> listeners = new ArrayList<DirectoryListener>();

	public DirectoryWatcher(String name, long updateInterval, File ... watched) {
		super(name);
		this.updateInterval = updateInterval;
		setWatched(watched);
		setSystem(true);


	}

	protected void initCache()
	{
		synchronized (cached) {
			cached.clear();
			if(watched == null) 
			{
				return;
			}
			for(File dir : watched)
			{
				if(dir == null) continue;
				List<File> current = Arrays.asList(dir.listFiles());

				for(File f : current)
				{
					try {
						long lastModified = f.lastModified();
						cached.put(f, lastModified);
					} catch (Exception e) {
						// file seems to be inaccessible
						// TODO ignore?
					}
				}
			}
		}
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {

		try {

			if(monitor.isCanceled()) return Status.CANCEL_STATUS;
			List<File> added = new ArrayList<File>();
			Set<File> found = new HashSet<File>();
			List<File> deleted = new ArrayList<File>();
			List<File> modified = new ArrayList<File>();

			synchronized (cached) {
				if(watched != null)
				{
					for(File dir : watched)
					{
						if(dir == null) continue;
						File[] listed = dir.listFiles();
						if(listed == null || listed.length == 0) continue;
						List<File> current = Arrays.asList(listed);

						for(File f : current)
						{
							if(monitor.isCanceled())
							{
								return Status.CANCEL_STATUS;
							}
							found.add(f);
							try {
								long lastModified = f.lastModified();
								Long oldModified = cached.get(f);
								if(oldModified == null)
								{
									added.add(f);
									cached.put(f, lastModified);
								}
								else if(oldModified != lastModified)
								{
									modified.add(f);
									cached.put(f, lastModified);
								}
							} catch (Exception e) {
								// file seems to be inaccessible
								// TODO ignore?
							}
						}
					}
				}

				//everything in cached that is not in found has been deleted
				for(Iterator<File> it = cached.keySet().iterator();it.hasNext();)
				{
					File f = it.next();
					if(!found.contains(f))
					{
						deleted.add(f);
						it.remove();
					}
				}
			}

			if(!added.isEmpty() || !deleted.isEmpty() || !modified.isEmpty()){

				notifyListeners(added.toArray(new File[added.size()]), deleted.toArray(new File[deleted.size()]), modified.toArray(new File[modified.size()]));
			}

		} finally {
			if(!monitor.isCanceled()) 
			{
				schedule(getUpdateInterval());
			}
		}

		return Status.OK_STATUS;

	}





	public void addDirectoryListener(DirectoryListener l)
	{
		if(!listeners.contains(l))
		{
			listeners.add(l);
		}
	}

	public void removeDirectoryListener(DirectoryListener l)
	{
		listeners.remove(l);
	}



	protected void notifyListeners(File[] added, File[] deleted, File[] modified)
	{
		for(DirectoryListener l : listeners)
		{
			try {
				l.contentChanged(added, deleted,modified);
			} catch (Exception e) {
				UnicoreCommonActivator.log(Status.WARNING, "Directory listener could not be notified: "+e.getMessage(),e);
			}
		}
	}



	public long getUpdateInterval() {
		return updateInterval;
	}

	public void setUpdateInterval(long updateInterval) {
		this.updateInterval = updateInterval;
	}

	public File[] getWatched() {
		return watched;
	}

	public void setWatched(File[] watched) {
		this.watched = watched;
		initCache();
	}

}
