package de.fzj.unicore.rcp.common.guicomponents;

import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;

public interface IEditorPartWithSettableInput extends IEditorPart {
	public void setInput(IEditorInput input);
}
