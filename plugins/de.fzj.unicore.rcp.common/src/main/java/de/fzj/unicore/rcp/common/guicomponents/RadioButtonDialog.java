package de.fzj.unicore.rcp.common.guicomponents;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class RadioButtonDialog extends Dialog {

	protected String[] options;
	protected String selectedOption;
	protected String title;

	protected RadioButtonDialog(Shell parentShell, String title,
			String[] options) {
		super(parentShell);
		this.options = options;
		this.title = title;
	}

	@Override
	protected void configureShell(Shell newShell) {
		newShell.setText(title);
		super.configureShell(newShell);
	}

	@Override
	public Control createDialogArea(Composite c) {

		Composite area = (Composite) super.createDialogArea(c);
		area.setLayout(new GridLayout(1, true));
		for (int i = 0; i < options.length; i++) {
			final Button radio = new Button(area, SWT.RADIO);
			radio.setText(options[i]);
			radio.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(SelectionEvent e) {
				}

				public void widgetSelected(SelectionEvent e) {
					selectedOption = radio.getText();
				}
			});
			if (i == 0) {
				radio.setSelection(true);
			}
		}
		selectedOption = options[0];
		area.pack();
		return area;
	}

	public String getSelectedOption() {
		return selectedOption;
	}

}
