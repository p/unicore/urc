/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.common.detailsView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class KeyValueList {

	private ArrayList<KeyValue> keyValues = new ArrayList<KeyValue>();
	private Set<IKeyValueListViewer> changeListeners = new HashSet<IKeyValueListViewer>();

	/**
	 * Creates an empty UserList Used upon start without connection to a xUUDB
	 */
	public KeyValueList() {
		// empty
	}

	/**
	 * Creates an UserList filled with users of defined xUUDB defined by props
	 * 
	 * @param props
	 *            the connection properties for the xUUDB
	 * @throws Exception
	 *             if parsing of props or connection to xUUDB (retrieve of list)
	 *             fails
	 */
	public KeyValueList(Map<String, String> h) {

		for (String s : h.keySet()) {
			keyValues.add(new KeyValue(s, h.get(s)));

		}
	}

	public void addChangeListener(IKeyValueListViewer viewer) {
		changeListeners.add(viewer);
	}

	/**
	 * Return the collection of Users
	 */
	public ArrayList<KeyValue> getKeyValues() {
		return keyValues;
	}

	public void removeChangeListener(IKeyValueListViewer viewer) {
		changeListeners.remove(viewer);
	}

	/**
	 * Remove a User from the xUUDB
	 * 
	 * @param User
	 */
	public void removeKeyValue(KeyValue kv) {
		// check if user is inserted (thus is not new) and delete from DB
		keyValues.remove(kv); // remove from local model
		Iterator<IKeyValueListViewer> iterator = changeListeners.iterator();
		while (iterator.hasNext()) {
			iterator.next().removeKeyValue(kv);
		}
	}
}
