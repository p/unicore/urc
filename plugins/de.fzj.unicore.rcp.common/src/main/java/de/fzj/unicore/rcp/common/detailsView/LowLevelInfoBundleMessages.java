package de.fzj.unicore.rcp.common.detailsView;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class LowLevelInfoBundleMessages {

	private static final String BUNDLE_NAME = "de.fzj.unicore.rcp.common.detailsView.LowLevelInfoBundleMessages"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private LowLevelInfoBundleMessages() {
	}

	public static ResourceBundle getBundle() {
		return RESOURCE_BUNDLE;
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
