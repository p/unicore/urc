/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.common.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlCursor.TokenType;
import org.w3.x2005.x08.addressing.EndpointReferenceType;
import org.w3.x2005.x08.addressing.MetadataType;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.wsrflite.xmlbeans.WSUtilities;

/**
 * @author demuth
 * 
 */
public class Utils {

	public static XmlObject[] extractAnyElements(XmlObject source, QName q)
			throws XmlException {
		List<XmlObject> results = new ArrayList<XmlObject>();
		XmlCursor cursor = null;
		try {
			if (source != null) {
				// XmlObject o = XmlObject.Factory.parse(source.toString());
				cursor = source.newCursor();

				boolean hasMore = skipToElement(cursor, q);

				while (hasMore) {
					XmlObject next = cursor.getObject();
					QName name = cursor.getName();

					if (q.getNamespaceURI().equals(name.getNamespaceURI())
							&& q.getLocalPart().equals(name.getLocalPart())) {
						results.add(next);
					}
					hasMore = cursor.toNextSibling();
				}

			}
		} finally {
			if (cursor != null) {
				cursor.dispose();
			}
		}
		return results.toArray(new XmlObject[results.size()]);
	}

	/**
	 * Exctract the InterfaceName from the EPRs Metadata element
	 * 
	 * @param epr
	 *            to parse
	 * @return the QName of the implemented interface or null if none could be
	 *         determined
	 */
	public static QName InterfaceNameFromEPR(EndpointReferenceType epr) {

		XmlObject interfaceName;
		try {
			interfaceName = extractAnyElements(epr.getMetadata(),
					Constants.INTERFACE_NAME)[0];
			XmlCursor n = interfaceName.newCursor();

			n.toFirstContentToken();

			String localPart = n.getTextValue().substring(
					n.getTextValue().indexOf(":") + 1);
			String prefix = n.getTextValue().substring(0,
					n.getTextValue().indexOf(":"));
			n.toParent();
			String namespace = n.namespaceForPrefix(prefix);
			n.dispose();
			return new QName(namespace, localPart);

		} catch (Exception e) {
			return null;
		}
	}

	protected static boolean skipToElement(XmlCursor cursor, QName name) {
		// walk through element tree in prefix order (root first, then children
		// from left to right)
		if (name.equals(cursor.getName())) {
			return true;
		}
		boolean hasMoreChildren = true;
		int i = 0;
		while (hasMoreChildren) {
			hasMoreChildren = cursor.toChild(i++);
			if (hasMoreChildren) {
				boolean foundInChild = skipToElement(cursor, name);
				if (foundInChild) {
					return true;
				}
				cursor.toParent();
			}
		}
		return false;
	}
	

	/**
	 * add interface name of a service to the metadata of the epr
	 */
	public static void addPortType(EndpointReferenceType epr, QName portType) {
		MetadataType meta = epr.getMetadata();
		if (meta == null) {
			meta = epr.addNewMetadata();
		}
		XmlCursor n = meta.newCursor();
		n.toFirstContentToken();
		n.beginElement(Constants.INTERFACE_NAME);
		n.insertNamespace("x", portType.getNamespaceURI());
		n.insertChars("x:" + portType.getLocalPart());
		n.dispose();
	}

	/**
	 * add the server DN to the metadata of the epr
	 */
	public static void addServerIdentity(EndpointReferenceType epr, String dn) {
		MetadataType meta = epr.getMetadata();
		if (meta == null) {
			meta = epr.addNewMetadata();
		}
		XmlCursor n = meta.newCursor();
		n.toFirstContentToken();
		n.beginElement(Constants.SERVER_NAME);
		n.insertChars(dn);
		n.dispose();
	}

	public static List<XmlObject> extractElements(XmlObject source, QName name)
			throws IOException, XmlException {
		List<XmlObject> res = new ArrayList<XmlObject>();
		XmlCursor cursor = source.newCursor();
		while (skipToElement(cursor, name)) {
			XmlObject o = XmlObject.Factory.parse(cursor.newReader());
			res.add(o);
		}
		cursor.dispose();
		return res;
	}

	/**
	 * Extract the InterfaceName (i.e. port tye from the EPRs Metadata element
	 * 
	 * @param epr
	 *            to parse
	 * @return the QName of the implemented interface or null if none could be
	 *         determined
	 */
	public static QName extractInterfaceName(EndpointReferenceType epr) {
		try {
			XmlCursor n = epr.newCursor();
			WSUtilities.skipToElement(n, Constants.INTERFACE_NAME);
			String localPart = n.getTextValue().substring(
					n.getTextValue().indexOf(":") + 1);
			String prefix = n.getTextValue().substring(0,
					n.getTextValue().indexOf(":"));
			String namespace = n.namespaceForPrefix(prefix);
			n.dispose();
			return new QName(namespace, localPart);
		} catch (Exception e) {
			return null;
		}
	}

	public static String extractServerIDFromEPR(EndpointReferenceType epr) {
		try {
			if (epr == null || epr.getMetadata() == null) {
				return null;
			}
			MetadataType meta = epr.getMetadata();
			XmlObject[] o = WSUtilities.extractAnyElements(meta,
					Constants.SERVER_NAME);
			if (o.length == 0) {
				return null;
			}
			XmlCursor c = WSUtilities.extractAnyElements(meta,
					Constants.SERVER_NAME)[0].newCursor();
			while (c.toNextToken() != TokenType.TEXT) {
			}
			String res = c.getChars();
			c.dispose();
			return res;
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * create a new EPR
	 * 
	 * @return {@link EndpointReferenceType}
	 */
	public static EndpointReferenceType newEPR() {
		EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
		return epr;
	}


	public static String UIDFromEPR(EndpointReferenceType epr) {
		String[] tokens = epr.getAddress().getStringValue().split("=");
		String resid = null;
		if (tokens.length > 1) {
			resid = tokens[1];
		}
		return resid;
	}

}
