package de.fzj.unicore.rcp.common.utils;

import java.io.IOException;
import java.io.OutputStream;

import de.fzj.unicore.uas.fts.ProgressListener;

public class ProgressListeningOutputStream extends OutputStream {

	private OutputStream delegate;
	private ProgressListener<Long> listener;
	

	public ProgressListeningOutputStream(OutputStream delegate,
			ProgressListener<Long> listener) {
		super();
		this.delegate = delegate;
		this.listener = listener;
	}

	
	private void notifyProgress(long bytes) throws ProgressListener.CancelledException
	{
		if(listener != null) 
		{
			if(listener.isCancelled()) throw new ProgressListener.CancelledException("Data transfer was cancelled by the user!");
			listener.notifyProgress(bytes);
			
		}
	}
	
	public void write(int b) throws IOException {
		delegate.write(b);
		notifyProgress(1);
	}

	public int hashCode() {
		return delegate.hashCode();
	}

	public void write(byte[] b) throws IOException {
		delegate.write(b);
		if(b != null) notifyProgress(b.length);
	}

	public void write(byte[] b, int off, int len) throws IOException {
		delegate.write(b, off, len);
		if(b != null) notifyProgress(len);
	}

	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}

	public void flush() throws IOException {
		delegate.flush();
	}

	public void close() throws IOException {
		delegate.close();
	}

	public String toString() {
		return delegate.toString();
	}
	
	
}
