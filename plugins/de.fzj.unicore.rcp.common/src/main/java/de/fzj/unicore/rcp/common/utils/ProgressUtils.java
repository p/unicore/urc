package de.fzj.unicore.rcp.common.utils;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;

public class ProgressUtils {
	
	public static final Double KILO = 1024d;
	public static final Double MEGA = KILO * 1024;
	public static final Double GIGA = MEGA * 1024;
	public static final Double TERA = GIGA * 1024;
	public static final Double PETA = TERA * 1024;

	public static IProgressMonitor getNonNullMonitorFor(IProgressMonitor monitor) {
		if (monitor != null) {
			return monitor;
		} else {
			return new NullProgressMonitor();
		}
	}
	
	
	/**
	 * Often it is difficult to transform long based effort into integer based
	 * values. This method, in conjunction with {@link #calculateEffort(long, long)}
	 * can be used to simplify this task.
	 * @param longEffort
	 * @return A reasonable integer value that can be used for an 
	 * {@link IProgressMonitor};
	 */
	public static int calculateEffort(long longEffort) {
		 // small values
		if (longEffort < MEGA) {
			return (int) longEffort;
		} else if(longEffort < GIGA)
		{
			// medium sized values
			return (int) Math.ceil(longEffort / KILO);	
		} else if(longEffort < TERA)
		{
			// large values
			return (int) Math.ceil(longEffort / MEGA);
		} else if(longEffort < PETA) {
			// huge values
			return (int) Math.ceil(longEffort / GIGA);
		}
		// default
		return (int) Math.ceil(longEffort / TERA);
	}

	/**
	 * Often it is difficult to transform long based effort into integer based
	 * values. This method, in conjunction with {@link #calculateEffort(long)}
	 * can be used to simplify this task. 
	 * @param longEffort
	 * @return An integer value that should be fed into 
	 * {@link IProgressMonitor#worked(int)} when the monitor has been created
	 * with a total effort that was returned by {@link #calculateEffort(long)}.
	 */
	public static int calculateEffort(long effort, long totalEffort) {
		int total = calculateEffort(totalEffort);
		double fraction = ((double) effort) / totalEffort;
		return (int) (fraction * total);
	}

}
