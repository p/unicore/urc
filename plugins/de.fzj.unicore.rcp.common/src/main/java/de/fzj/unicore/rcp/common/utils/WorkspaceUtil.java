package de.fzj.unicore.rcp.common.utils;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.URIException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.URIUtil;
import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.ide.ChooseWorkspaceData;
import org.eclipse.ui.internal.ide.ChooseWorkspaceDialog;
import org.eclipse.ui.internal.ide.IDEWorkbenchMessages;
import org.eclipse.ui.internal.ide.IDEWorkbenchPlugin;
import org.eclipse.ui.internal.ide.StatusUtil;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

/**
 * The "main program" for the Eclipse IDE.
 * 
 * @since 3.0
 */
public class WorkspaceUtil {

	/**
	 * The name of the folder containing metadata information for the workspace.
	 */
	public static final String METADATA_FOLDER = ".metadata"; //$NON-NLS-1$

	private static final String VERSION_FILENAME = "version.ini"; //$NON-NLS-1$

	private static final String WORKSPACE_VERSION_KEY = "org.eclipse.core.runtime"; //$NON-NLS-1$

	private static final String WORKSPACE_VERSION_VALUE = "1"; //$NON-NLS-1$

	private static final String PROP_EXIT_CODE = "eclipse.exitcode"; //$NON-NLS-1$


	/**
	 * Eclipse's default way of dealing with workspaces has some drawbacks. If
	 * an RCP application is not started from its installation directory and
	 * Eclipse cannot find the default workspace, it will just create a new
	 * workspace folder in the current working directory. This method tries to
	 * provide a more reasonable handling of workspaces. It lets the Client fall
	 * back to the workspace folder in the installation directory if no
	 * workspace has been set. Moreover, it stores the argument to the -data
	 * option persistently if it is used. For persisting workspace settings, the
	 * file ".settings/org.eclipse.ui.ide.prefs" in the configuration area is
	 * used, as usual.
	 */
	public static boolean fixWorkspace(Shell shell) {

		Location instanceLoc = Platform.getInstanceLocation();
		if (dataArgumentSet()) {
			storeWorkspaceLocation(getDataArgument());
		}


		String workspace = null;
		if (getIDEPrefsFile().exists()) {
			ChooseWorkspaceData cwd = new ChooseWorkspaceData("");
			if (cwd.readPersistedData()) {
				if (cwd.getRecentWorkspaces() != null
						&& cwd.getRecentWorkspaces().length > 0
						&& cwd.getRecentWorkspaces()[0] != null) {
					workspace = cwd.getRecentWorkspaces()[0];
				}
			}

		}
		if (workspace == null) {

			IPath workspacePath = new Path("workspace");
			workspacePath = fixInstallationRelativePath(workspacePath);
			System.out.println("No workspace location specified. Using "
							+ workspacePath.toOSString());
			workspace = workspacePath.toOSString();

		}


		URI desiredURL = null;
		try {
			File f = new File(workspace);
			if(!f.exists())
			{
				if(f.mkdirs()) desiredURL = toFileURI(f.getAbsolutePath());
				else desiredURL = null;
			}
			else desiredURL = toFileURI(f.getAbsolutePath());
		} catch (Exception e1) {

		} 
		if(shell==null) shell = new Shell();
		// should should set the icon and message for this shell to be the 
		// same as the chooser dialog - this will be the guy that lives in
		// the task bar and without these calls you'd have the default icon 
		// with no message.
		shell.setText(ChooseWorkspaceDialog.getWindowTitle());
		shell.setImages(Dialog.getDefaultImages());

		if(!isValid(shell, desiredURL))
		{
			MessageDialog
			.openError(
					shell,
					IDEWorkbenchMessages.IDEApplication_workspaceInvalidTitle,
					IDEWorkbenchMessages.IDEApplication_workspaceInvalidMessage);
			// still invalid => prompt and set
			ChooseWorkspaceData launchData = new ChooseWorkspaceData(instanceLoc
					.getDefault());
			desiredURL = promptForWorkspace(shell, launchData, true);
		}
		else if(isLocked(desiredURL))
		{
			// by this point it has been determined that the workspace is
			// already in use -- force the user to choose again
			MessageDialog.openError(shell, IDEWorkbenchMessages.IDEApplication_workspaceInUseTitle, 
					IDEWorkbenchMessages.IDEApplication_workspaceInUseMessage);
			// still invalid => prompt and set
			ChooseWorkspaceData launchData = new ChooseWorkspaceData(instanceLoc
					.getDefault());
			desiredURL = promptForWorkspace(shell, launchData, true);
		}


		if (desiredURL == null) {
			return false;
		}
		
		URL unencoded = null;
		try {
			// BasicLocation can't deal with encoded special chars correctly
			unencoded = new URL(URIUtil.toUnencodedString(desiredURL));
			if(instanceLoc.isSet())
			{
				if(equivalent(instanceLoc.getURL(),desiredURL.toURL()) || equivalent(instanceLoc.getURL(),unencoded))
				{
					return true;
				}
			}

		}
		catch (Exception e) {
			return false;
		}

		try {
			// the operation will fail if the url is not a valid
			// instance data area, so other checking is unneeded
			instanceLoc.release();

			if (instanceLoc.set(unencoded, true)) {
				writeWorkspaceVersion();
				File f = new File(desiredURL);
				storeWorkspaceLocation(f.getAbsolutePath());
				return true;
			}
		} 
		catch (Exception e) {
			MessageDialog
			.openError(
					shell,
					IDEWorkbenchMessages.IDEApplication_workspaceCannotBeSetTitle,
					IDEWorkbenchMessages.IDEApplication_workspaceCannotBeSetMessage);

		}


		return false;

	}

	private static boolean equivalent(URL uri1, URL uri2)
	{
		if(uri1 == null) return uri2 == null;
		if(uri2 == null) return false;
		
		String s1 = normalizePath(uri1.getFile());
		String s2 = normalizePath(uri2.getFile());
		return s1.equals(s2);
	}

	private static String normalizePath(String path)
	{
		Stack<String> nPath = new Stack<String>();
		String []ret = path.split("/");
		for (String s : ret)
		{
			if (s == null || s.length() == 0)
				continue;

			if (s.equals(".."))
			{
				if (!nPath.empty())
					nPath.pop();
			} else if (!s.equals("."))
			{
				nPath.push(s);
			}
		}

		ret = new String[nPath.size()];

		nPath.toArray(ret);
		String result = "";
		for (int i = 0; i < ret.length; i++) {
			result+="/"+ret[i];
		}
		return result;
	}

	public static IPath fixInstallationRelativePath(IPath relative) {

		if (relative.isAbsolute()) {
			return relative; // this is not a relative path..
		}
		File installArea;
		try {
			String prop = System.getProperty("osgi.install.area");
			//if(!prop.startsWith("file:")) prop = "file:"+prop;
		
			installArea = new File(toFileURI(prop));
			IPath installationPath = new Path(installArea.getAbsolutePath());
			if (installationPath.append(relative).toFile().exists()) {
				// file seems to be there as expected
				return installationPath.append(relative);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return relative; // couldn't find the file..
	}

	private static URI toFileURI(String path) throws URIException, URISyntaxException
	{
		URI temp = null;
		if(path.toLowerCase().startsWith("file:"))
		{
			path = URIUtils.encodeAll(path);
			temp = new URI(path);
		}
		else
		{
			File f = new File(path);
			temp = f.toURI();
		}
		return temp;
	}
	
	
	private static boolean dataArgumentSet() {
		String commands = System.getProperty("eclipse.commands");
		// use DOTALL in order not to match newlines
		Pattern p = Pattern.compile(".*\\n\\s*-data\\s*\\n.*", Pattern.DOTALL); 
		Matcher m = p.matcher(commands);
		return m.matches();

	}


	private static String getDataArgument() {
		String commands = System.getProperty("eclipse.commands");
		int index = commands.indexOf("-data");
		commands = commands.substring(index + 6);

		return commands.split("\\n")[0];
	}

	/**
	 * Returns the file ".settings/org.eclipse.ui.ide.prefs" in the
	 * configuration area of Eclipse (usually, the configuration area is located
	 * in the"configuration" folder in the installation directory.
	 * 
	 * @return
	 */
	private static File getIDEPrefsFile() {
		String config = System.getProperty("osgi.configuration.area");
		config += ".settings";
		URI uri;
		try {
			uri = toFileURI(config);
			File parent = new File(uri);
			return new File(parent, "org.eclipse.ui.ide.prefs");
		} catch (Exception e) {
			e.printStackTrace();
			return new File("");
		}
	}


	/**
	 * Creates the display used by the application.
	 * 
	 * @return the display used by the application
	 */
	protected Display createDisplay() {
		return PlatformUI.createDisplay();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IExecutableExtension#setInitializationData(org.eclipse.core.runtime.IConfigurationElement, java.lang.String, java.lang.Object)
	 */
	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) {
		// There is nothing to do for IDEApplication
	}

	/**
	 *

	/**
	 * Open a workspace selection dialog on the argument shell, populating the
	 * argument data with the user's selection. Perform first level validation
	 * on the selection by comparing the version information. This method does
	 * not examine the runtime state (e.g., is the workspace already locked?).
	 * 
	 * @param shell
	 * @param launchData
	 * @param force
	 *            setting to true makes the dialog open regardless of the
	 *            showDialog value
	 * @return An URL storing the selected workspace or null if the user has
	 *         canceled the launch operation.
	 */
	private static URI promptForWorkspace(Shell shell, ChooseWorkspaceData launchData,
			boolean force) {
		URI url = null;
		do {
			// okay to use the shell now - this is the splash shell
			new ChooseWorkspaceDialog(shell, launchData, true,false).prompt(force);
			String instancePath = launchData.getSelection();
			if (instancePath == null) {
				return null;
			}

			// the dialog is not forced on the first iteration, but is on every
			// subsequent one -- if there was an error then the user needs to be
			// allowed to fix it
			force = true;

			// 70576: don't accept empty input
			if (instancePath.length() <= 0) {
				MessageDialog
				.openError(
						shell,
						IDEWorkbenchMessages.IDEApplication_workspaceEmptyTitle,
						IDEWorkbenchMessages.IDEApplication_workspaceEmptyMessage);
				continue;
			}

			// create the workspace if it does not already exist
			File workspace = new File(instancePath);
			if (!workspace.exists()) {
				workspace.mkdir();
			}

			try {
				url = toFileURI(workspace.getAbsolutePath());
			} catch (Exception e) {
				MessageDialog
				.openError(
						shell,
						IDEWorkbenchMessages.IDEApplication_workspaceInvalidTitle,
						IDEWorkbenchMessages.IDEApplication_workspaceInvalidMessage);
				continue;
			}
			
			if(!isValid(shell, url))
			{
				MessageDialog
				.openError(
						shell,
						IDEWorkbenchMessages.IDEApplication_workspaceInvalidTitle,
						IDEWorkbenchMessages.IDEApplication_workspaceInvalidMessage);
			}
			else if(isLocked(url))
			{
				// by this point it has been determined that the workspace is
				// already in use -- force the user to choose again
				MessageDialog.openError(shell, IDEWorkbenchMessages.IDEApplication_workspaceInUseTitle, 
						IDEWorkbenchMessages.IDEApplication_workspaceInUseMessage);
			}
			else
			{
				break;
			}
		} while (true);

		return url;
	}

	/**
	 * Return true if the argument directory is ok to use as a workspace and
	 * false otherwise. A version check will be performed, and a confirmation
	 * box may be displayed on the argument shell if an older version is
	 * detected.
	 * 
	 * @return true if the argument URL is ok to use as a workspace and false
	 *         otherwise.
	 */
	private static boolean isValid(Shell shell, URI url) {
		// a null url is not a valid workspace
		if (url == null) {
			return false;
		}

		File f;
		try {
	
			f = new File(url);
			if(!f.isDirectory() || !f.canRead() || !f.canWrite()) return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}




		String version = readWorkspaceVersion(url);

		// if the version could not be read, then there is not any existing
		// workspace data to trample, e.g., perhaps its a new directory that
		// is just starting to be used as a workspace
		if (version == null) {
			return true;
		}

		final int ide_version = Integer.parseInt(WORKSPACE_VERSION_VALUE);
		int workspace_version = Integer.parseInt(version);

		// equality test is required since any version difference (newer
		// or older) may result in data being trampled
		if (workspace_version == ide_version) {
			return true;
		}

		// At this point workspace has been detected to be from a version
		// other than the current ide version -- find out if the user wants
		// to use it anyhow.
		String title = "Different Workspace Version";
		String message = "";
		try {
			message = NLS
					.bind("Workspace ''{0}'' was written with a different version"
							+ " of the product and will be updated. Updating the"
							+ " workspace can make it incompatible with older"
							+ " versions of the product.\n\nAre you sure you"
							+ " want to continue with this workspace?",
							url.toURL().getFile());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		MessageBox mbox = new MessageBox(shell, SWT.OK | SWT.CANCEL
				| SWT.ICON_WARNING | SWT.APPLICATION_MODAL);
		mbox.setText(title);
		mbox.setMessage(message);
		return mbox.open() == SWT.OK;
	}

	// TODO need to fix this or get rid of WSUtil entirely.
	private static boolean isLocked(URI uri) {
		// what we're about to do does not work with encoded URLs
		URL url;
		try {
			url = uri.toURL();
		} catch (MalformedURLException e) {
			return true;
		} catch (Exception e) {
			// ??
		}
		return false;
	}

	/**
	 * Look at the argument URL for the workspace's version information. Return
	 * that version if found and null otherwise.
	 */
	private static String readWorkspaceVersion(URI workspace) {
		File versionFile = getVersionFile(workspace, false);
		if (versionFile == null || !versionFile.exists()) {
			return null;
		}

		try {
			// Although the version file is not spec'ed to be a Java properties
			// file, it happens to follow the same format currently, so using
			// Properties to read it is convenient.
			Properties props = new Properties();
			FileInputStream is = new FileInputStream(versionFile);
			try {
				props.load(is);
			} finally {
				is.close();
			}

			return props.getProperty(WORKSPACE_VERSION_KEY);
		} catch (IOException e) {
			IDEWorkbenchPlugin.log("Could not read version file", new Status( //$NON-NLS-1$
					IStatus.ERROR, IDEWorkbenchPlugin.IDE_WORKBENCH,
					IStatus.ERROR,
					e.getMessage() == null ? "" : e.getMessage(), //$NON-NLS-1$, 
							e));
			return null;
		}
	}

	/**
	 * Write the version of the metadata into a known file overwriting any
	 * existing file contents. Writing the version file isn't really crucial,
	 * so the function is silent about failure
	 */
	private static void writeWorkspaceVersion() {
		Location instanceLoc = Platform.getInstanceLocation();
		if (instanceLoc == null || instanceLoc.isReadOnly()) {
			return;
		}
		OutputStream output = null;
		try {
			File versionFile = getVersionFile(instanceLoc.getURL().toURI(), true);
			if (versionFile == null) {
				return;
			}

			String versionLine = WORKSPACE_VERSION_KEY + '='
			+ WORKSPACE_VERSION_VALUE;

			output = new FileOutputStream(versionFile);
			output.write(versionLine.getBytes("UTF-8")); //$NON-NLS-1$
		} catch (Exception e) {
			IDEWorkbenchPlugin.log("Could not write version file", //$NON-NLS-1$
					StatusUtil.newStatus(IStatus.ERROR, e.getMessage(), e));
		} finally {
			try {
				if (output != null) {
					output.close();
				}
			} catch (IOException e) {
				// do nothing
			}
		}
	}

	/**
	 * The version file is stored in the metadata area of the workspace. This
	 * method returns an URL to the file or null if the directory or file does
	 * not exist (and the create parameter is false).
	 * 
	 * @param create
	 *            If the directory and file does not exist this parameter
	 *            controls whether it will be created.
	 * @return An url to the file or null if the version file does not exist or
	 *         could not be created.
	 */
	private static File getVersionFile(URI workspaceUrl, boolean create) {
		if (workspaceUrl == null) {
			return null;
		}

		try {
		
			// make sure the directory exists
			File metaDir = new File(new File(workspaceUrl), METADATA_FOLDER);
			if (!metaDir.exists() && (!create || !metaDir.mkdir())) {
				return null;
			}

			// make sure the file exists
			File versionFile = new File(metaDir, VERSION_FILENAME);
			if (!versionFile.exists()
					&& (!create || !versionFile.createNewFile())) {
				return null;
			}

			return versionFile;
		} catch (Exception e) {
			// cannot log because instance area has not been set
			return null;
		}
	}


	/**
	 * Persist a workspace location in the file
	 * ".settings/org.eclipse.ui.ide.prefs" in the configuration area, as
	 * Eclipse does.
	 * 
	 * @param location
	 */
	private static void storeWorkspaceLocation(String location) {
		// 1. get config pref node
		Preferences node = new ConfigurationScope()
		.getNode(IDEWorkbenchPlugin.IDE_WORKBENCH);
		String newLocs = "";
		String old = node.get(IDE.Preferences.RECENT_WORKSPACES,"");


		node.putBoolean(IDE.Preferences.SHOW_WORKSPACE_SELECTION_DIALOG, false);

		// 2. number of workspaces to remember
		int numWorkspaces = node.getInt(IDE.Preferences.MAX_RECENT_WORKSPACES, -1);
		if(numWorkspaces == -1)
		{
			numWorkspaces = 5;
			node.putInt(IDE.Preferences.MAX_RECENT_WORKSPACES, numWorkspaces);
		}


		// 3. store values of recent workspaces into array
		String[] tokens = old.split("\n");
		int num = 0;
		if(tokens.length > 0 && tokens[0].trim().length() > 0)
		{
			for(int i = 0; i < tokens.length;i++)
			{
				if(!location.equals(tokens[i]))
				{		

					newLocs+=tokens[i];
					num++;
					if(num < numWorkspaces-1 && i < tokens.length - 1) newLocs+="\n";
					if(num >= numWorkspaces-1) break;	
				}
			}
			newLocs=location+"\n"+newLocs;
		}
		else newLocs = location;
		node.put(IDE.Preferences.RECENT_WORKSPACES, newLocs);

		// 4. store the protocol version used to encode the list
		node.putInt(IDE.Preferences.RECENT_WORKSPACES_PROTOCOL, 3);

		// 5. store the node
		try {
			node.flush();
		} catch (BackingStoreException e) {
			// do nothing
		}
	}

}

