package de.fzj.unicore.rcp.common.guicomponents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.common.guicomponents.IListElement.INameChangeListener;

public abstract class GenericListEditor<T extends IListElement> implements
		INameChangeListener {

	/**
	 * Gap between label and control.
	 */
	protected static final int HORIZONTAL_GAP = 8;

	/**
	 * The list widget; <code>null</code> if none (before creation or after
	 * disposal).
	 */
	private List list;

	private java.util.List<T> internalList;

	private Map<String, T> idsToElements = new HashMap<String, T>();

	private java.util.List<IListListener<T>> listeners;

	private Composite control;

	/**
	 * The button box containing the Add, Remove, Up, and Down buttons;
	 * <code>null</code> if none (before creation or after disposal).
	 */
	private Composite buttonBox;

	/**
	 * The Add button.
	 */
	private Button addButton;

	/**
	 * The Remove button.
	 */
	private Button removeButton;

	/**
	 * The Up button.
	 */
	private Button upButton;

	/**
	 * The Down button.
	 */
	private Button downButton;

	/**
	 * The Rename button.
	 */
	private Button renameButton;

	/**
	 * The selection listener.
	 */
	private SelectionListener selectionListener;

	/**
	 * This array specifies which buttons to create
	 */
	private boolean[] buttonsToCreate;

	private boolean addingAllowed = true;

	private boolean listeningToNameChanges = true;

	private int selectionIndex = -1;

	public GenericListEditor(Composite parent) {

		this(parent, new ArrayList<T>());
	}

	public GenericListEditor(Composite parent, java.util.List<T> initialElements) {
		this(parent, initialElements, new boolean[] { true, true, true, true,
				true });
	}

	public GenericListEditor(Composite parent,
			java.util.List<T> initialElements, boolean[] buttonsToCreate) {

		internalList = initialElements;
		listeners = new ArrayList<IListListener<T>>();
		this.buttonsToCreate = buttonsToCreate;
		createControl(parent);

	}

	public void addListListener(IListListener<T> l) {
		listeners.add(l);
	}

	/**
	 * Notifies that the Add button has been pressed.
	 */
	private void addPressed() {
		T element = createNewListElement();
		String input = element.getName();

		int index = list.getSelectionIndex() + 1;
		if (index == 0) {
			index = list.getItemCount();
		}
		list.add(input, index);
		internalList.add(index, element);
		idsToElements.put(element.getId(), element);
		list.setSelection(index);
		element.addNameChangeListener(this);
		// list.pack();
		// parent.pack();
		for (IListListener<T> l : listeners) {
			l.elementAdded(element);
		}
		selectionChanged();
	}

	/*
	 * (non-Javadoc) Method declared on FieldEditor.
	 */
	protected void adjustForNumColumns(int numColumns) {
		((GridData) list.getLayoutData()).horizontalSpan = numColumns - 1;
	}

	private void checkButtonsEnabled() {
		boolean overallEnabled = control.isEnabled();
		int size = list.getItemCount();

		boolean selectionEmpty = getSelectedElement() == null;
		if (addButton != null) {
			addButton.setEnabled(overallEnabled && isAddingAllowed());
		}
		if (removeButton != null) {
			boolean enabled = overallEnabled && !selectionEmpty
					&& getSelectedElement().canBeRemoved();
			removeButton.setEnabled(enabled);
		}
		if (renameButton != null) {
			renameButton.setEnabled(overallEnabled && !selectionEmpty);
		}
		if (upButton != null) {
			upButton.setEnabled(overallEnabled && size > 1
					&& selectionIndex > 0);
		}
		if (downButton != null) {
			downButton.setEnabled(overallEnabled && size > 1
					&& selectionIndex >= 0 && selectionIndex < size - 1);
		}
	}

	/**
	 * Checks if the given parent is the current parent of the supplied control;
	 * throws an (unchecked) exception if they are not correctly related.
	 * 
	 * @param control
	 *            the control
	 * @param parent
	 *            the parent control
	 */
	protected void checkParent(Control control, Composite parent) {
		Assert.isTrue(control.getParent() == parent, "Different parents");//$NON-NLS-1$
	}

	/**
	 * Returns the number of pixels corresponding to the given number of
	 * horizontal dialog units.
	 * <p>
	 * Clients may call this framework method, but should not override it.
	 * </p>
	 * 
	 * @param control
	 *            the control being sized
	 * @param dlus
	 *            the number of horizontal dialog units
	 * @return the number of pixels
	 */
	protected int convertHorizontalDLUsToPixels(Control control, int dlus) {
		GC gc = new GC(control);
		gc.setFont(control.getFont());
		FontMetrics metrics = gc.getFontMetrics();
		gc.dispose();
		return Dialog.convertHorizontalDLUsToPixels(metrics, dlus);
	}

	/**
	 * Returns the number of pixels corresponding to the given number of
	 * vertical dialog units.
	 * <p>
	 * Clients may call this framework method, but should not override it.
	 * </p>
	 * 
	 * @param control
	 *            the control being sized
	 * @param dlus
	 *            the number of vertical dialog units
	 * @return the number of pixels
	 */
	protected int convertVerticalDLUsToPixels(Control control, int dlus) {
		GC gc = new GC(control);
		gc.setFont(control.getFont());
		FontMetrics metrics = gc.getFontMetrics();
		gc.dispose();
		return Dialog.convertVerticalDLUsToPixels(metrics, dlus);
	}

	/**
	 * Creates the Add, Remove, Up, and Down button in the given button box.
	 * 
	 * @param box
	 *            the box for the buttons
	 */
	protected void createButtons(Composite box) {
		if (buttonsToCreate[0]) {
			addButton = createPushButton(box, "Add");
		}
		if (buttonsToCreate[1]) {
			removeButton = createPushButton(box, "Remove");
		}
		if (buttonsToCreate[2]) {
			upButton = createPushButton(box, "Move up");
		}
		if (buttonsToCreate[3]) {
			downButton = createPushButton(box, "Move down");
		}
		if (buttonsToCreate[4]) {
			renameButton = createPushButton(box, "Rename");
		}
	}

	/**
	 * Creates this field editor's main control containing all of its basic
	 * controls.
	 * 
	 * @param parent
	 *            the parent control
	 */
	protected void createControl(Composite parent) {
		control = new Composite(parent, SWT.NONE) {
			@Override
			public void setEnabled(boolean enabled) {
				if (list != null) {
					list.setEnabled(enabled);
				}
				if (addButton != null) {
					addButton.setEnabled(enabled);
				}
				if (removeButton != null) {
					removeButton.setEnabled(enabled);
				}
				if (upButton != null) {
					upButton.setEnabled(enabled);
				}
				if (downButton != null) {
					downButton.setEnabled(enabled);
				}
				if (renameButton != null) {
					renameButton.setEnabled(enabled);
				}

				super.setEnabled(enabled);
				// now verify if enablement is ok and disable if its not
				checkButtonsEnabled();

			}
		};
		GridLayout layout = new GridLayout();
		parent.setLayout(layout);
		control.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true));

		layout = new GridLayout();
		layout.numColumns = getNumberOfColumns();
		layout.marginWidth = 0;
		layout.marginHeight = 0;
		layout.horizontalSpacing = HORIZONTAL_GAP;
		control.setLayout(layout);
		doFillIntoGrid(control, layout.numColumns);
	}

	/**
	 * Returns this field editor's list control.
	 * 
	 * @param parent
	 *            the parent control
	 * @return the list control
	 */
	protected List createListControl(Composite parent) {
		if (list == null) {
			list = new List(parent, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL
					| SWT.H_SCROLL);
			list.setFont(parent.getFont());
			selectionIndex = list.getSelectionIndex();
			list.addSelectionListener(getSelectionListener());
		} else {
			checkParent(list, parent);
		}
		return list;
	}

	/**
	 * Method for creating new elements when the add button is pressed. Must be
	 * overriden by subclasses
	 * 
	 * @return
	 */
	protected abstract T createNewListElement();

	/**
	 * Helper method to create a push button.
	 * 
	 * @param parent
	 *            the parent control
	 * @param key
	 *            the resource name used to supply the button's label text
	 * @return Button
	 */
	private Button createPushButton(Composite parent, String key) {
		Button button = new Button(parent, SWT.PUSH);
		button.setText(JFaceResources.getString(key));
		button.setFont(parent.getFont());
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		int widthHint = convertHorizontalDLUsToPixels(button,
				IDialogConstants.BUTTON_WIDTH);
		data.widthHint = Math.max(widthHint,
				button.computeSize(SWT.DEFAULT, SWT.DEFAULT, true).x);
		button.setLayoutData(data);
		button.addSelectionListener(getSelectionListener());
		return button;
	}

	/**
	 * Creates a selection listener.
	 */
	public void createSelectionListener() {
		selectionListener = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				Widget widget = event.widget;
				if (widget == addButton) {
					addPressed();
				} else if (widget == removeButton) {
					removePressed();
				} else if (widget == upButton) {
					upPressed();
				} else if (widget == downButton) {
					downPressed();
				} else if (widget == renameButton) {
					renamePressed();
				} else if (widget == list) {
					selectionChanged();
				}
				checkButtonsEnabled();
			}
		};
	}

	/*
	 * (non-Javadoc) Method declared on FieldEditor.
	 */
	protected void doFillIntoGrid(Composite parent, int numColumns) {
		GridData gd;

		list = createListControl(parent);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.verticalAlignment = GridData.FILL;
		gd.horizontalSpan = numColumns - 1;
		gd.grabExcessHorizontalSpace = true;
		gd.widthHint = 100;
		list.setLayoutData(gd);

		buttonBox = getButtonBoxControl(parent);
		gd = new GridData();
		gd.verticalAlignment = GridData.BEGINNING;
		buttonBox.setLayoutData(gd);
	}

	/**
	 * Notifies that the Down button has been pressed.
	 */
	private void downPressed() {
		swap(false);
	}

	/**
	 * Returns this field editor's button box containing the Add, Remove, Up,
	 * and Down button.
	 * 
	 * @param parent
	 *            the parent control
	 * @return the button box
	 */
	public Composite getButtonBoxControl(Composite parent) {
		if (buttonBox == null) {
			buttonBox = new Composite(parent, SWT.NULL);
			GridLayout layout = new GridLayout();
			layout.marginWidth = 0;
			buttonBox.setLayout(layout);
			createButtons(buttonBox);

		} else {
			checkParent(buttonBox, parent);
		}

		selectionChanged();
		return buttonBox;
	}

	public Composite getControl() {
		return control;
	}

	public T getElementById(String id) {
		return idsToElements.get(id);
	}

	public List getListControl() {
		return list;
	}

	/*
	 * (non-Javadoc) Method declared on FieldEditor.
	 */
	public int getNumberOfColumns() {
		return 2;
	}

	public T getSelectedElement() {
		if (selectionIndex >= 0 && selectionIndex < internalList.size()) {
			return internalList.get(selectionIndex);
		} else {
			return null;
		}
	}

	/**
	 * Returns this field editor's selection listener. The listener is created
	 * if nessessary.
	 * 
	 * @return the selection listener
	 */
	private SelectionListener getSelectionListener() {
		if (selectionListener == null) {
			createSelectionListener();
		}
		return selectionListener;
	}

	public boolean isAddingAllowed() {
		return addingAllowed;
	}

	public void nameChanged(String sourceId, String oldName, String newName) {
		if (!listeningToNameChanges) {
			return;
		}
		T source = getElementById(sourceId);
		int index = internalList.indexOf(source);
		list.remove(index);
		list.add(newName, index);

		for (IListListener<T> l : listeners) {
			l.elementChanged(source);
		}
		list.select(index);
	}

	public void removeListListener(IListListener<T> l) {
		listeners.remove(l);
	}

	/**
	 * Notifies that the Remove button has been pressed.
	 */
	private void removePressed() {
		int index = list.getSelectionIndex();
		if (index >= 0) {
			list.remove(index);
			T element = internalList.remove(index);
			idsToElements.remove(element);
			element.removeNameChangeListener(this);
			for (IListListener<T> l : listeners) {
				l.elementRemoved(element);
			}
			if (list.getSelectionIndex() == -1 && list.getItemCount() > 0) {
				int select = Math.min(index - 1, list.getItemCount() - 1);
				select = Math.max(select, 0);
				list.select(select);
			}
			// list.pack();
			// parent.pack();
			selectionChanged();
		}
	}

	/**
	 * Notifies that the Rename button has been pressed.
	 */
	private void renamePressed() {
		Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();
		final T element = getSelectedElement();
		InputDialog dialog = new InputDialog(shell, "Rename",
				"Please enter a new name.", element.getName(),
				new IInputValidator() {
					public String isValid(String newText) {
						return element.nameValid(newText);
					}
				});
		if (Window.OK == dialog.open()) {

			String newName = dialog.getValue();
			int index = internalList.indexOf(element);
			list.remove(index);
			listeningToNameChanges = false;
			element.setName(newName);
			listeningToNameChanges = true;
			list.add(newName, index);

			list.select(index);
			for (IListListener<T> l : listeners) {
				l.elementChanged(element);
			}

		}

	}

	/**
	 * Notifies that the list selection has changed.
	 */
	private void selectionChanged() {
		selectionIndex = list.getSelectionIndex();

		T selected = null;
		if (selectionIndex >= 0) {
			selected = internalList.get(selectionIndex);
		}
		for (IListListener<T> l : listeners) {
			l.selectionChanged(selected);
		}
		checkButtonsEnabled();
	}

	public void setAddingAllowed(boolean addingAllowed) {
		this.addingAllowed = addingAllowed;
		checkButtonsEnabled();
	}

	public void setElements(java.util.List<T> elements) {
		for (T t : internalList) {
			t.removeNameChangeListener(this);
		}
		idsToElements.clear();
		this.internalList = elements;
		list.removeAll();
		for (T t : elements) {
			list.add(t.getName());
			idsToElements.put(t.getId(), t);
			t.addNameChangeListener(this);
		}
		list.select(0);
		selectionIndex = 0;
	}

	public void setEnabled(boolean enabled) {
		getControl().setEnabled(enabled);
	}

	/*
	 * (non-Javadoc) Method declared on FieldEditor.
	 */
	public void setFocus() {
		if (list != null) {
			list.setFocus();
		}
	}

	/**
	 * Moves the currently selected item up or down.
	 * 
	 * @param up
	 *            <code>true</code> if the item should move up, and
	 *            <code>false</code> if it should move down
	 */
	private void swap(boolean up) {
		int index = list.getSelectionIndex();
		int target = up ? index - 1 : index + 1;

		if (index >= 0) {
			String[] selection = list.getSelection();
			Assert.isTrue(selection.length == 1);
			list.remove(index);
			T e = internalList.remove(index);
			list.add(selection[0], target);
			internalList.add(target, e);
			list.setSelection(target);
		}
		selectionChanged();
	}

	/**
	 * Notifies that the Up button has been pressed.
	 */
	private void upPressed() {
		swap(true);
	}

}
