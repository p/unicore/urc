package de.fzj.unicore.rcp.common.internal.utils;

import java.util.Map;
import java.util.UUID;

import org.unigrids.services.atomic.types.ProtocolType;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.uas.client.UFTPFileTransferClient;

public class FiletransferParameterProvider implements
de.fzj.unicore.uas.FiletransferParameterProvider {


	public void provideParameters(Map<String, String> params, String protocol) {
		if (ProtocolType.UFTP.toString().equals(protocol)) {


			// setting uftp.streams
			Integer uftpStreams = UnicoreCommonActivator.getDefault()
			.getPreferenceStore().getInt(Constants.P_UFTP_STREAM_NUMBER);
			params.put(UFTPFileTransferClient.PARAM_STREAMS, uftpStreams.toString());

			// setting uftp.client.host
			String uftpClientHost = UnicoreCommonActivator.getDefault()
			.getPreferenceStore()
			.getDefaultString(Constants.P_UFTP_CLIENT_HOST);
			if (!uftpClientHost.equals(""))
			{
				params.put(UFTPFileTransferClient.PARAM_CLIENT_HOST, uftpClientHost);
			}

			// setting uftp.encryption
			Boolean uftpEnableEncryption = UnicoreCommonActivator.getDefault()
				.getPreferenceStore()
				.getBoolean(Constants.P_UFTP_ENABLE_ENCRYPTION);
			params.put(UFTPFileTransferClient.PARAM_ENABLE_ENCRYPTION, uftpEnableEncryption.toString());

			//add unique secret
			if(params.get(UFTPFileTransferClient.PARAM_SECRET)==null){
				params.put(UFTPFileTransferClient.PARAM_SECRET, UUID.randomUUID().toString());
			}
		}
	}


}
