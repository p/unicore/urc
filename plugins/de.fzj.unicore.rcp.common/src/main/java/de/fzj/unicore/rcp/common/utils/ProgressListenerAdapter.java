package de.fzj.unicore.rcp.common.utils;

import java.util.Formatter;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.uas.fts.ProgressListener;

public class ProgressListenerAdapter implements ProgressListener<Long> {

	private IProgressMonitor monitor;
	private long totalSize, newWorked = 0, lastTimeStamp,
			workedSinceLastTimeStamp, idleInterval;
	private int oldWorked = 0;
	private Job updateTransferRateJob;
	private String path;

	public ProgressListenerAdapter(IProgressMonitor progress, long totalSize) {
		this(progress, totalSize, "");
	}

	public ProgressListenerAdapter(IProgressMonitor progress, long totalSize,
			String path) {
		this.monitor = ProgressUtils.getNonNullMonitorFor(progress);
		this.totalSize = totalSize;
		this.path = path;
	}

	public boolean isCancelled() {
		return monitor.isCanceled();
	}

	public void notifyProgress(Long newValue) {

		workedSinceLastTimeStamp += newValue;

		newWorked += newValue;
		int totalWorked = (int) (100.0 * (newWorked / (double) totalSize));
		int diff = totalWorked - oldWorked;
		monitor.worked(diff);
		oldWorked += diff;

		if (updateTransferRateJob == null) {
			updateTransferRateJob = new BackgroundJob("monitoring file transfer rate") {
				
				

				@Override
				protected IStatus run(IProgressMonitor mon) {
					if (oldWorked >= 100) {
						return Status.OK_STATUS;
					}
					long passed = System.currentTimeMillis() - lastTimeStamp;
					double bytePerSec = workedSinceLastTimeStamp * 1000d
							/ passed;

					if (newWorked < totalSize) {
						monitor.setTaskName("Transferring file "
								+ path
								+ " at "
								+ FileUtils
										.humanReadableFileSize((long) bytePerSec)
								+ "/sec, ETE: " + formatDuration((int) ((totalSize-newWorked)/bytePerSec)));
					} else {
						monitor.setTaskName("File transfer finished");
					}
					lastTimeStamp = System.currentTimeMillis();
					if (workedSinceLastTimeStamp > 0) {
						idleInterval = 0;
					} else {
						idleInterval += passed;
					}
					workedSinceLastTimeStamp = 0;
					if (!isCancelled() && (newWorked < totalSize)
							&& idleInterval < 5 * 60 * 1000) {
						schedule(3000);
						return Status.OK_STATUS;
					} else {
						return Status.CANCEL_STATUS;
					}

				}
			};
			updateTransferRateJob.setSystem(true);
			updateTransferRateJob.schedule(500);
			lastTimeStamp = System.currentTimeMillis();
		}
	}

	/**
	 * Format duration in seconds as either
	 * <ul>
	 * <li>hh:mm:ss</li>
	 * <li>mm:ss</li>
	 * <li>ss</li>
	 * </ul>
	 * 
	 * Durations less than a minute will be suffixed with "s" to be explicit
	 * about the unit of time.
	 * 
	 * @param i the duration in seconds
	 * @return a formatted String acc. to above rules.
	 */
	private String formatDuration(int i) {
		if (i < 60) {
			return "" + i + "s";
		}
		final int h = i / 3600;
		final int r = i % 3600;
		final int m = r / 60;
		final int s = r % 60;

		final StringBuilder sb = new StringBuilder();
		final Formatter f = new Formatter(sb);
		try {
			if (h > 0) {
				f.format("%02d:%02d:%02d", h, m, s);
			} else {
				f.format("%02d:%02d", m, s);
			}
		} finally {
			f.close();
		}
		return sb.toString();
	}
}
