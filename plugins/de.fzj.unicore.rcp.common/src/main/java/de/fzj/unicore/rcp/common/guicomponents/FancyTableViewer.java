/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.common.guicomponents;

import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

/**
 * A basic TableViewer implementation that adds some nice features
 * 
 * @author demuth
 * 
 */
public class FancyTableViewer extends TableViewer implements ControlListener {

	public static int RESIZE_POLICY_PACK = 0;
	public static int RESIZE_POLICY_EQUAL_WIDTH = 1;

	private int resizePolicy = RESIZE_POLICY_PACK;
	/**
	 * If the user manually changes column widths, save them in order to stick
	 * to them even though packTable() is called.
	 */
	protected int[] colWidths;

	private boolean addedToTable = false;
	
	private boolean enabled = true;

	/**
	 * Set to true temporarily while the column widths are equalized. This is
	 * used in order to ignore events that have been produced by myself.
	 */
	private boolean equalizingColumnWidths = false;
	
	private Color oldFG, oldBG = null;
	
	private Menu oldMenu = null;

	public FancyTableViewer(Composite parent) {
		super(parent);
	}

	/**
	 * @param parent
	 * @param style
	 */
	public FancyTableViewer(Composite parent, int style) {
		super(parent, style);
	}

	public FancyTableViewer(Table table) {
		super(table);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.events.ControlListener#controlMoved(org.eclipse.swt.events
	 * .ControlEvent)
	 */
	public void controlMoved(ControlEvent e) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.events.ControlListener#controlResized(org.eclipse.swt
	 * .events.ControlEvent)
	 */
	public void controlResized(ControlEvent e) {

		if (colWidths == null || equalizingColumnWidths) {
			return;
		}
		Table table = getTable();
		if (e.getSource() instanceof TableColumn) {

			// save column widths for restoring them later
			TableColumn col = (TableColumn) e.getSource();
			TableColumn[] cols = table.getColumns();
			for (int i = 0; i < cols.length; i++) {
				if (col.equals(cols[i])) {
					colWidths[i] = col.getWidth();
				}
			}
		} else {
			// whole table has been resized
			if (resizePolicy == RESIZE_POLICY_EQUAL_WIDTH) {
				equalizeColumnWidths();
			}
		}

	}

	protected void equalizeColumnWidths() {
		equalizingColumnWidths = true;
		Table table = getTable();
		Rectangle area = table.getParent().getClientArea();
		Point preferredSize = table.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		int width = area.width - 2 * table.getBorderWidth();
		if (preferredSize.y > area.height + table.getHeaderHeight()) {
			// Subtract the scrollbar width from the total column width
			// if a vertical scrollbar will be required
			Point vBarSize = table.getVerticalBar().getSize();
			width -= vBarSize.x;
		}
		Point oldSize = table.getSize();
		if (oldSize.x > area.width) {
			// table is getting smaller so make the columns
			// smaller first and then resize the table to
			// match the client area width
			TableColumn[] cols = table.getColumns();
			for (TableColumn col : cols) {
				col.setWidth(width / cols.length + 1);
			}
			table.setSize(area.width, area.height);
		} else {
			// table is getting bigger so make the table
			// bigger first and then make the columns wider
			// to match the client area width
			table.setSize(area.width, area.height);
			TableColumn[] cols = table.getColumns();
			for (TableColumn col : cols) {
				col.setWidth(width / cols.length + 1);
			}
		}
		equalizingColumnWidths = false;
	}

	public int getResizePolicy() {
		return resizePolicy;
	}

	public void packTable() {
		if (!addedToTable) {
			getTable().getParent().addControlListener(this);
			addedToTable = true;
		}

		if (colWidths == null) {
			// initialize widths
			// -1 means that the user has not manually changed
			// the width so far
			colWidths = new int[getTable().getColumnCount()];
			for (int i = 0; i < colWidths.length; i++) {
				colWidths[i] = -1;
			}
		}

		int i = 0;
		for (TableColumn col : getTable().getColumns()) {
			col.removeControlListener(this);
			if (colWidths[i++] == -1) {
				col.pack();
			}
			col.addControlListener(this);
		}

	}

	public void setResizePolicy(int resizePolicy) {
		this.resizePolicy = resizePolicy;
	}
	
	@Override
	public ICellModifier getCellModifier()
	{
		if(isEnabled()) return super.getCellModifier();
		else
		{
			return new ICellModifier() {
				
				@Override
				public void modify(Object element, String property, Object value) {
					// do nothing
				}
				@Override
				public Object getValue(Object element, String property) {
					return null;
				}
				
				@Override
				public boolean canModify(Object element, String property) {
					// disallow modification when disabled
					return false;
				}
			};
		}
	}
	
	/**
	 * Disabling a table is ugly, cause scrollbars get deactivated. This fancy
	 * class allows for making the table look disabled while preserving the
	 * ability to scroll 
	 * @param enabled
	 */
	public void setEnabled(boolean enabled)
	{
		if(this.enabled == enabled) return;
		this.enabled = enabled;
		
		Table t = getTable();
		if(t != null)
		{
			
			Display d = t.getDisplay();
			Color fg = null, bg = null;
			if(enabled)
			{
				fg = oldFG;
				bg = oldBG;
				t.setMenu(oldMenu);
				
			}
			else {
				oldFG = t.getForeground();
				oldBG = t.getBackground();
				oldMenu = t.getMenu();
				t.setMenu(null);
				fg = d.getSystemColor(SWT.COLOR_TITLE_INACTIVE_FOREGROUND);
				bg = d.getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND);
			}
			t.setForeground(fg);
			t.setBackground(bg);
			
		}
	}

	public ViewerCell getCell(Point point) {
		if(isEnabled())
		{
			return super.getCell(point);
		}
		else return null;
	}
	public boolean isEnabled() {
		return enabled;
	}
}
