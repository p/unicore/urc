package de.fzj.unicore.rcp.common.utils;

import java.lang.ref.WeakReference;

public class HashableWeakReference<T> extends WeakReference<T> {

	int hashCode;

	public HashableWeakReference(T referent) {
		super(referent);
		hashCode = referent == null ? 0 : referent.hashCode();
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		T referent = get();
		if(obj instanceof HashableWeakReference) obj = ((HashableWeakReference) obj).get();
		return referent == null ? obj == null : referent.equals(obj);
	}

	@Override
	public String toString() {
		T referent = get();
		return referent == null ? "null" : referent.toString();
	}
}