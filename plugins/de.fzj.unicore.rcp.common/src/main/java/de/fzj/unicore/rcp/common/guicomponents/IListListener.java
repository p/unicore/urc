package de.fzj.unicore.rcp.common.guicomponents;

public interface IListListener<T extends IListElement> {
	public void elementAdded(T newElement);

	public void elementChanged(T changedElement);

	public void elementRemoved(T removedElement);

	public void selectionChanged(T newSelection);
}
