package de.fzj.unicore.rcp.common.guicomponents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.IAction;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.CommandNotMappedException;
import org.eclipse.ui.actions.ContributedAction;
import org.eclipse.ui.part.EditorPart;

public abstract class EditorPartWithActions extends EditorPart implements
		IEditorPartWithActions {

	/**
	 * Tag used in xml configuration files to specify editor action
	 * contributions. Current value: <code>editorContribution</code>
	 * 
	 * @since 2.0
	 */
	private static final String TAG_CONTRIBUTION_TYPE = "editorContribution"; //$NON-NLS-1$

	/** The actions registered with the editor. */
	private Map<String, IAction> fActions = new HashMap<String, IAction>(10);

	/**
	 * Returns the action with the given action id that has been contributed via
	 * XML to this editor. The lookup honors the dependencies of plug-ins.
	 * 
	 * @param actionID
	 *            the action id to look up
	 * @return the action that has been contributed
	 * @since 2.0
	 */
	private IAction findContributedAction(String actionID) {
		List<IConfigurationElement> actions = new ArrayList<IConfigurationElement>();
		IConfigurationElement[] elements = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(PlatformUI.PLUGIN_ID,
						"editorActions"); //$NON-NLS-1$
		for (int i = 0; i < elements.length; i++) {
			IConfigurationElement element = elements[i];
			if (TAG_CONTRIBUTION_TYPE.equals(element.getName())) {
				if (!getSite().getId().equals(element.getAttribute("targetID"))) {
					continue;
				}

				IConfigurationElement[] children = element
						.getChildren("action"); //$NON-NLS-1$
				for (int j = 0; j < children.length; j++) {
					IConfigurationElement child = children[j];
					if (actionID.equals(child.getAttribute("actionID"))) {
						actions.add(child);
					}
				}
			}
		}
		int actionSize = actions.size();
		if (actionSize > 0) {
			try {
				// TODO sort the actions
				return new ContributedAction(getSite(), actions.get(0));
			} catch (CommandNotMappedException e) {
				// out of luck, no command action mapping
			}
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.common.guicomponents.IEditorPartWithActions#getAction
	 * (java.lang.String)
	 */
	public IAction getAction(String actionID) {
		Assert.isNotNull(actionID);
		IAction action = fActions.get(actionID);

		if (action == null) {
			action = findContributedAction(actionID);
			if (action != null) {
				setAction(actionID, action);
			}
		}

		return action;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.common.guicomponents.IEditorPartWithActions#setAction
	 * (java.lang.String, org.eclipse.jface.action.IAction)
	 */
	public void setAction(String actionID, IAction action) {
		Assert.isNotNull(actionID);
		if (action == null) {
			fActions.remove(actionID);
		} else {
			fActions.put(actionID, action);
		}
	}
}
