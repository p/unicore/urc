package de.fzj.unicore.rcp.common.utils;

import java.util.HashSet;
import java.util.Set;

/**
 * This class that is used in conjunction with the {@link HandlerOrderingUtil}.
 * It allows to resolve dependencies between different handlers to obtain an
 * order in which the handlers should be invoked. This class is supposed to be
 * reusable in different contexts. It is only a wrapper around the "actual"
 * handler that is represented by the {@link #userData} object.
 * 
 * @author bdemuth
 * 
 */
public class Handler {

	protected String id;
	protected Set<String> before = new HashSet<String>();
	protected Set<String> after = new HashSet<String>();
	protected Object userData = null;

	public Handler(String id, Object userData) {
		super();
		this.id = id;
		this.userData = userData;
	}

	public Set<String> getAfter() {
		return after;
	}

	public Set<String> getBefore() {
		return before;
	}

	public String getId() {
		return id;
	}

	public Object getUserData() {
		return userData;
	}

	public void setAfter(Set<String> after) {
		this.after = after;
	}

	public void setBefore(Set<String> before) {
		this.before = before;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUserData(Object userData) {
		this.userData = userData;
	}
}
