package de.fzj.unicore.rcp.common.utils;

import java.io.File;
import java.io.IOException;

public class FileUtils {

	/**
	 * Create a String for human readable file size.
	 * 
	 * Default to base 1024 rather than SI units (base 1000).
	 * 
	 * @param bytes
	 * @return
	 */
	public static String humanReadableFileSize(long bytes) {
		return humanReadableFileSize(bytes, false);
	}

	/**
	 * Create a String for human readable file size.
	 * Credits: http://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java
	 * 
	 * @param bytes
	 * @param si whether to output si units
	 * @return
	 */
	public static String humanReadableFileSize(long bytes, boolean si) {
		    int unit = si ? 1000 : 1024;
		    if (bytes < unit) return bytes + " B";
		    int exp = (int) (Math.log(bytes) / Math.log(unit));
		    String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
		    return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}

	/**
	 * Recursive delete
	 * 
	 * @param f
	 * @throws IOException
	 */
	public static void rm(File f) throws IOException {
		if (f.isDirectory()) {
			File[] children = f.listFiles();
			if (children != null) {
				for (File child : children) {
					rm(child);
				}
			}
		}
		if (!f.delete()) {
			throw new IOException("Could not delete " + f.toString() + ".");
		}
	}
}
