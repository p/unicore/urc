package de.fzj.unicore.rcp.common.utils;

import java.net.URI;
import java.net.URISyntaxException;

import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * This class is used to circumvent backwards compatibility problems with
 * xstream 1.3, where URIs where serialized differently
 * @author bdemuth
 *
 */
public class BackwardsCompatibleURIConverter  implements Converter {

	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class type) {
		return type.equals(URI.class);
	}


	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		writer.setValue(source.toString());
	}


	@Override
	public Object unmarshal(HierarchicalStreamReader reader,
			UnmarshallingContext context) {
		if(reader.hasMoreChildren())
		{
			reader.moveDown();
			reader.moveDown();
			reader.moveDown();
			String s = reader.getValue();
			try {
				URI uri = new URI(s);
				reader.moveUp();
				reader.moveUp();
				reader.moveUp();
				return uri;
			} catch (URISyntaxException e) {
				throw new ConversionException(e);
			}
		}
		else {
			try {
				return new URI(reader.getValue());
			} catch (URISyntaxException e) {
				throw new ConversionException(e);
			}
		}
	}

}
