/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.common.guicomponents;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.FileEditorInput;

/**
 * @author demuth
 * 
 */
public class EditorInputChangedListener implements IResourceChangeListener,
		IPartListener {

	private IEditorPartWithSettableInput editor;

	public EditorInputChangedListener(IEditorPartWithSettableInput editor) {
		this.editor = editor;
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
		editor.getSite().getPage().addPartListener(this);
	}

	private void closeEditor() {
		Display.getDefault().syncExec(new Runnable() {
			public void run() {
				editor.getSite().getPage().closeEditor(editor, true);
			}
		});
	}

	public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
		editor.getSite().getPage().removePartListener(this);
	}

	public void partActivated(IWorkbenchPart part) {

	}

	public void partBroughtToTop(IWorkbenchPart part) {

	}

	public void partClosed(IWorkbenchPart part) {
		if (part == editor) {
			dispose();
		}
	}

	public void partDeactivated(IWorkbenchPart part) {

	}

	public void partOpened(IWorkbenchPart part) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.resources.IResourceChangeListener#resourceChanged(org
	 * .eclipse.core.resources.IResourceChangeEvent)
	 */
	public void resourceChanged(IResourceChangeEvent event) {
		IFile file = (IFile) editor.getEditorInput().getAdapter(IFile.class);
		if (file == null) {
			return;
		}

		if ((event.getType() == IResourceChangeEvent.PRE_DELETE || event
				.getType() == IResourceChangeEvent.PRE_CLOSE)
				&& event.getResource().equals(file.getProject())) {
			// closeEditor(); this is wrong... this event is also fired when a
			// project is renamed!
		} else {
			IResourceDelta delta = null;
			if (event.getDelta() != null) {
				delta = event.getDelta().findMember(file.getFullPath());
			}
			if (delta == null) {
				return;
			}
			Display display = editor.getSite().getShell().getDisplay();
			if (delta.getKind() == IResourceDelta.REMOVED) {

				if ((IResourceDelta.MOVED_TO & delta.getFlags()) == 0) { // if
																			// the
																			// file
																			// was
																			// deleted
					// NOTE: The case where an open, unsaved file is deleted is
					// being handled by the
					// PartListener added to the Workbench in the initialize()
					// method.
					display.asyncExec(new Runnable() {
						public void run() {
							closeEditor();
						}
					});
				} else { // else if it was moved or renamed
					final IFile newFile = ResourcesPlugin.getWorkspace()
							.getRoot().getFile(delta.getMovedToPath());
					display.asyncExec(new Runnable() {
						public void run() {
							editor.setInput(new FileEditorInput(newFile));
						}
					});
				}
			} else if (delta.getKind() == IResourceDelta.CHANGED) {
				// the file was overwritten somehow (could have been replaced by
				// another
				// version in the repository)
				// don't do anything here automatically as changing the editor
				// input is dangerous

				// final IFile newFile =
				// ResourcesPlugin.getWorkspace().getRoot()
				// .getFile(delta.getFullPath());
				// display.asyncExec(new Runnable() {
				// public void run() {
				// editor.setInput(new FileEditorInput(newFile));
				// }
				// });

			}
		}
	}

}
