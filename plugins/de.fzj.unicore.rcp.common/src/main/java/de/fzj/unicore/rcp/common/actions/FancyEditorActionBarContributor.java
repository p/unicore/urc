package de.fzj.unicore.rcp.common.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IContributionManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.actions.RetargetAction;
import org.eclipse.ui.part.EditorActionBarContributor;

import de.fzj.unicore.rcp.common.guicomponents.IEditorPartWithActions;

public abstract class FancyEditorActionBarContributor extends
		EditorActionBarContributor {

	protected List<RetargetAction> actions = new ArrayList<RetargetAction>();
	/**
	 * The active editor part.
	 */
	private IEditorPartWithActions activeEditor;
	protected Map<RetargetAction, Boolean> addToContextMenu = new HashMap<RetargetAction, Boolean>();
	protected Map<RetargetAction, Boolean> addToToolbar = new HashMap<RetargetAction, Boolean>();
	protected List<String> globalActionKeys = new ArrayList<String>();
	protected Map<RetargetAction, Boolean> isGlobal = new HashMap<RetargetAction, Boolean>();
	protected IMenuManager menu;
	private Map<String, RetargetAction> retargetActions = new HashMap<String, RetargetAction>();
	protected IToolBarManager toolBarManager;

	public FancyEditorActionBarContributor() {
		super();
	}

	protected void addEditorSpecificAction(RetargetAction action,
			boolean addToToolbar, boolean addToContextMenu) {
		if (getRetargetAction(action.getId()) != null) {
			return; // do not override any global actions
		}
		addRetargetAction(action, false, addToToolbar, addToContextMenu);
	}

	protected void addGlobalAction(RetargetAction action, boolean addToToolbar,
			boolean addToContextMenu) {
		addRetargetAction(action, true, addToToolbar, addToContextMenu);
	}

	/**
	 * Indicates the existence of a global action identified by the specified
	 * key. This global action is defined outside the scope of this contributor,
	 * such as the Workbench's undo action, or an action provided by a workbench
	 * ActionSet. The list of global action keys is used whenever the active
	 * editor is changed ({@link #setActiveEditor(IEditorPart)}). Keys provided
	 * here will result in corresponding actions being obtained from the active
	 * editor, and those actions will be registered with the ActionBars for this
	 * contributor. The editor's action handler and the global action must have
	 * the same key.
	 * 
	 * @param key
	 *            the key identifying the global action
	 */
	protected void addGlobalActionKey(String key) {
		globalActionKeys.add(key);
	}

	protected void addGlobalKeys() {
		IActionBars bars = getActionBars();
		for (int i = 0; i < globalActionKeys.size(); i++) {
			String id = globalActionKeys.get(i);
			bars.setGlobalActionHandler(id, getAction(activeEditor, id));
		}
	}

	/**
	 * The <code>item</code> is
	 * {@link IContributionManager#add(IContributionItem) added} to
	 * <code>menu</code> if no item with the same id currently exists. If there
	 * already is an contribution item with the same id, the new item gets
	 * {@link IContributionManager#insertAfter(String, IContributionItem)
	 * inserted after} it.
	 * 
	 * @param menu
	 *            the contribution manager
	 * @param item
	 *            the contribution item
	 * @since 3.2
	 */
	private void addOrInsert(IContributionManager menu, IContributionItem item) {
		String id = item.getId();
		if (menu.find(id) == null) {
			menu.add(item);
		} else {
			menu.insertAfter(id, item);
		}
	}

	protected void addRetargetAction(RetargetAction action, boolean global,
			boolean addToToolbar, boolean addToContextMenu) {

		IActionBars actionBars = getActionBars();
		IAction actionHandler = getAction(activeEditor, action.getId());
		if (actionHandler == null) {
			actionHandler = action.getActionHandler();
		}
		actionBars.setGlobalActionHandler(action.getId(), actionHandler);
		getPage().addPartListener(action);
		retargetActions.put(action.getId(), action);
		actions.add(action);
		this.addToToolbar.put(action, addToToolbar);
		this.addToContextMenu.put(action, addToContextMenu);
		this.isGlobal.put(action, global);
		// addGlobalActionKey(action.getId());
	}

	/**
	 * Override this in subclasses to define the set of actions that are
	 * specifically available for the given editor instance. Actions must be
	 * added with
	 * {@link #addEditorSpecificAction(RetargetAction, boolean, boolean)}
	 */
	protected abstract void buildEditorSpecificActions(IEditorPart editor);

	/**
	 * Override this in subclasses to define the set of actions that are
	 * available for all editor instances for which this contributor is used.
	 * Actions must be added with
	 * {@link #addGlobalAction(RetargetAction, boolean, boolean)}
	 */
	protected abstract void buildGlobalActions();

	@Override
	public void contributeToMenu(IMenuManager menu) {
		this.menu = menu;
		IMenuManager editMenu = menu
				.findMenuUsingPath(IWorkbenchActionConstants.M_EDIT);
		if (editMenu != null) {
			for (RetargetAction action : getActionsForContextMenu()) {
				editMenu.remove(action.getId());
				editMenu.add(action);
			}
			addOrInsert(editMenu, new Separator(
					IWorkbenchActionConstants.MB_ADDITIONS));
		}

	}

	@Override
	public void contributeToToolBar(IToolBarManager toolBarManager) {
		toolBarManager.removeAll();
		this.toolBarManager = toolBarManager;
		for (RetargetAction action : getActionsForToolbar()) {
			toolBarManager.remove(action.getId());
			toolBarManager.add(action);
		}
		toolBarManager.update(true);
	}

	/**
	 * @see org.eclipse.gef.ui.actions.ActionBarContributor#declareGlobalActionKeys()
	 */
	protected void declareGlobalActionKeys() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		activeEditor = null;
		removeAllRetargetActions();
		super.dispose();
	}

	/**
	 * Returns the action registered with the given editor.
	 * 
	 * @param editor
	 *            the editor, or <code>null</code>
	 * @param actionId
	 *            the action id
	 * @return the action, or <code>null</code> if none
	 */
	protected IAction getAction(IEditorPartWithActions editor, String actionId) {
		return (editor == null || actionId == null ? null : editor
				.getAction(actionId));
	}

	protected List<RetargetAction> getActionsForContextMenu() {
		List<RetargetAction> result = new ArrayList<RetargetAction>();
		for (RetargetAction retargetAction : actions) {
			if (isAddedToContextMenu(retargetAction)) {
				result.add(retargetAction);
			}
		}
		return result;
	}

	protected List<RetargetAction> getActionsForToolbar() {
		List<RetargetAction> result = new ArrayList<RetargetAction>();
		for (RetargetAction retargetAction : actions) {
			if (isAddedToToolbar(retargetAction)) {
				result.add(retargetAction);
			}
		}
		return result;
	}

	/**
	 * Returns the active editor part.
	 * 
	 * @return the active editor part
	 */
	protected final IEditorPart getActiveEditorPart() {
		return activeEditor;
	}

	protected RetargetAction[] getAllActions() {
		return retargetActions.values().toArray(
				new RetargetAction[retargetActions.size()]);
	}

	protected RetargetAction getRetargetAction(String id) {
		return retargetActions.get(id);
	}

	@Override
	public void init(IActionBars bars) {
		super.init(bars);
		buildGlobalActions();
	}

	protected boolean isAddedToContextMenu(RetargetAction action) {
		Boolean result = addToContextMenu.get(action);
		if (result == null) {
			return true;
		} else {
			return result;
		}
	}

	protected boolean isAddedToToolbar(RetargetAction action) {
		Boolean result = addToToolbar.get(action);
		if (result == null) {
			return true;
		} else {
			return result;
		}
	}

	protected boolean isGlobal(RetargetAction action) {
		Boolean result = isGlobal.get(action);
		if (result == null) {
			return true;
		} else {
			return result;
		}
	}

	protected void removeAllRetargetActions() {

		for (RetargetAction action : getAllActions()) {
			removeRetargetAction(action);
		}

		// submitAction.setAction(getAction(activeEditor,
		// UnicoreCommonActionConstants.ACTION_SUBMIT));
	}

	protected void removeEditorSpecificAction(RetargetAction action) {
		if (getRetargetAction(action.getId()) != null) {
			return; // do not remove any global actions
		}
		removeRetargetAction(action);
	}

	/**
	 * Cleans up old editor specific actions. Must be called before new editor
	 * specific actions are built.
	 */
	protected void removeEditorSpecificActions() {
		for (RetargetAction action : getAllActions()) {
			if (!isGlobal(action)) {
				removeRetargetAction(action);
			}
		}
	}

	protected void removeGlobalAction(RetargetAction action) {
		removeRetargetAction(action);
	}

	protected void removeGlobalActionKey(String key) {
		globalActionKeys.remove(key);
	}

	protected void removeRetargetAction(RetargetAction action) {

		IActionBars actionBars = getActionBars();
		actionBars.setGlobalActionHandler(action.getId(), null);
		actions.remove(action);
		getPage().removePartListener(action);
		retargetActions.remove(action.getId());
		action.dispose();
		this.addToToolbar.remove(action);
		this.addToContextMenu.remove(action);
		this.isGlobal.remove(action);
		// removeGlobalActionKey(action.getId());
	}

	/**
	 * The <code>BasicTextEditorActionContributor</code> implementation of this
	 * <code>IEditorActionBarContributor</code> method installs the global
	 * action handler for the given text editor by calling a private helper
	 * method.
	 * <p>
	 * Subclasses may extend.
	 * </p>
	 * 
	 * @param part
	 *            {@inheritDoc}
	 */
	@Override
	public void setActiveEditor(IEditorPart part) {
		if (activeEditor == part) {
			return;
		}
		Assert.isTrue(part == null || part instanceof IEditorPartWithActions);
		removeEditorSpecificActions();
		activeEditor = (IEditorPartWithActions) part;
		buildEditorSpecificActions(part);

		// refresh menu and toolbar
		contributeToMenu(menu);
		contributeToToolBar(toolBarManager);
		updateActionHandlers();
	}

	protected void updateActionHandlers() {
		for (RetargetAction action : getAllActions()) {
			IActionBars actionBars = getActionBars();
			IAction actionHandler = getAction(activeEditor, action.getId());
			if (actionHandler != null) {
				// workaround for Eclipse (?) bug that leaves actions disabled
				boolean oldEnabled = actionHandler.isEnabled();
				actionHandler.setEnabled(!oldEnabled);
				actionHandler.setEnabled(oldEnabled);
			}
			actionBars.setGlobalActionHandler(action.getId(), actionHandler);
		}
	}

}