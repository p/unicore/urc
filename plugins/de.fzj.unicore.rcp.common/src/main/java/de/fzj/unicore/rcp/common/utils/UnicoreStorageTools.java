/*********************************************************************************
 * Copyright (c) 2008 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

package de.fzj.unicore.rcp.common.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.util.URIUtil;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.unigrids.services.atomic.types.GridFileType;
import org.unigrids.services.atomic.types.ProtocolType;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.uas.client.FileTransferClient;
import de.fzj.unicore.uas.client.StorageClient;
import de.fzj.unicore.uas.client.UFTPConstants;
import de.fzj.unicore.uas.fts.FiletransferOptions;
import de.fzj.unicore.uas.fts.ProgressListener;
import de.fzj.unicore.uas.util.Pair;
import de.fzj.unicore.wsrflite.xmlbeans.BaseFault;

/**
 * This class provides file tools to copy, move, download, and upload files from
 * and to UNICORE6 storages
 * 
 * @author demuth
 * 
 */
public class UnicoreStorageTools {

	/**
	 * Flag constant (bit mask value 1) indicating that the operation should
	 * overwrite remote or local files if they exist.
	 */
	public static final int FORCE = 0x1;

	/**
	 * Flag constant (bit mask value 2) indicating that the operation should
	 * append content to the local or remote file in case they exist.
	 */
	public static final int APPEND = 0x2;

	/**
	 * Flag constant (bit mask value 2) indicating that the operation should
	 * rename target files automatically in case they<s<s exist.
	 */
	public static final int RENAME = 0x4;

	private static IProgressMonitor beginSubTask(IProgressMonitor monitor,
			String name, int work) {
		if (monitor == null) {
			return null;
		}
		monitor.subTask(name);
		return new SubProgressMonitor(monitor, work,
				SubProgressMonitor.PREPEND_MAIN_LABEL_TO_SUBTASK);
	}

	public static int calculateEffort(long bytes) {
		return ProgressUtils.calculateEffort(bytes);
	}

	public static int calculateEffort(long bytes, long totalBytes) {
		return ProgressUtils.calculateEffort(bytes, totalBytes);
	}

	/**
	 * Recursively gather all files and directories that need to be copied
	 * 
	 * @param sourceFolder
	 * @param storage
	 * @param targetFolder
	 * @param secMgr
	 * @param progress
	 * @return the accumulated file sizes
	 * @throws Exception
	 */
	private static long collectFiles(List<Pair<File, String>> collection,
			File sourceFolder, String targetFolder, Map<File, Long> fileSizes,
			boolean accumulateFileSizes, IProgressMonitor progress)
	throws Exception {
		{
			progress = ProgressUtils.getNonNullMonitorFor(progress);
			String parentPath = sourceFolder.getAbsolutePath();
			long result = 1;
			if (collection != null) {
				collection.add(new Pair<File, String>(sourceFolder,
						targetFolder));
			}

			for (File child : sourceFolder.listFiles()) {
				if (progress.isCanceled()) {
					return -1;
				}
				String relative = child.getAbsolutePath().substring(
						parentPath.length() + 1);
				if (collection != null) {
					collection.add(new Pair<File, String>(child, targetFolder
							+ File.separator + relative));
				}
				long fileSize = 0;
				if (child.isDirectory()) {
					long accumulatedFileSize = collectFiles(collection, child,
							targetFolder + File.separator + relative,
							fileSizes, accumulateFileSizes, null);
					fileSize = accumulateFileSizes ? accumulatedFileSize : 1;
					result += accumulatedFileSize;
				} else {
					fileSize = child.length();
					result += fileSize;
				}

				if (fileSizes != null) {
					fileSizes.put(child, fileSize);
				}
			}
			if (fileSizes != null && fileSizes.get(sourceFolder) == null) {
				fileSizes.put(sourceFolder, result);
			}
			return result;
		}
	}

	public static long collectFileSizes(File sourceFolder,
			Map<File, Long> fileSizes, boolean accumulateFileSizes,
			IProgressMonitor progress) throws Exception {
		{
			progress = ProgressUtils.getNonNullMonitorFor(progress);
			long result = 1;
			for (File child : sourceFolder.listFiles()) {
				if (progress.isCanceled()) {
					return -1;
				}
				long fileSize = 0;
				if (child.isDirectory()) {
					long accumulatedFileSize = collectFileSizes(child,
							fileSizes, accumulateFileSizes, null);
					fileSize = accumulateFileSizes ? accumulatedFileSize : 1;
					result += accumulatedFileSize;
				} else {
					fileSize = child.length();
					result += fileSize;
				}

				if (fileSizes != null) {
					fileSizes.put(child, fileSize);
				}
			}
			if (fileSizes != null && fileSizes.get(sourceFolder) == null) {
				fileSizes.put(sourceFolder, result);
			}
			return result;
		}
	}

	private static long collectRemoteFiles(
			List<Pair<GridFileType, String>> collection,
			GridFileType sourceFolder, StorageClient sms, String targetFolder,
			Map<String, Long> fileSizes, boolean accumulateFileSizes,
			IProgressMonitor progress) throws Exception {
		progress = ProgressUtils.getNonNullMonitorFor(progress);
		long result = 1;
		if (collection != null) {
			collection.add(new Pair<GridFileType, String>(sourceFolder,
					targetFolder));
		}
		String path = sourceFolder.getPath();
		path = URIUtil.encode(path,
				org.apache.commons.httpclient.URI.allowed_fragment); // escape
		// special
		// characters
		// in
		// filenames!
		for (GridFileType child : sms.listDirectory(path)) {
			if (progress.isCanceled()) {
				return result;
			}
			String relative = child.getPath().substring(
					sourceFolder.getPath().length() + 1);
			String target = targetFolder + "/" + relative;
			long fileSize = 0;
			if (child.getIsDirectory()) {
				long accumulatedFileSize = collectRemoteFiles(collection,
						child, sms, target, fileSizes, accumulateFileSizes,
						null);
				fileSize = accumulateFileSizes ? accumulatedFileSize : 1;
				result += accumulatedFileSize;
			} else {
				if (collection != null) {
					collection
					.add(new Pair<GridFileType, String>(child, target));
				}
				fileSize = child.getSize();
				result += child.getSize();
			}
			if (fileSizes != null) {
				fileSizes.put(child.getPath(), fileSize);
			}
		}
		if (fileSizes != null && fileSizes.get(sourceFolder) == null) {
			fileSizes.put(sourceFolder.getPath(), result);
		}
		return result;
	}

	public static void createFolder(StorageClient storage, File sourceFolder,
			String targetFolder, IProgressMonitor progress) throws Exception {
		progress = ProgressUtils.getNonNullMonitorFor(progress);
		progress.beginTask("", 1);
		try {
			//			// escape special characters in filenames!
			//			targetFolder = URIUtil.encode(targetFolder,
			//					org.apache.commons.httpclient.URI.allowed_fragment); 

			boolean targetExists = false;
			// create target folder if it does not exist already
			try {
				targetExists = storage.listProperties(targetFolder) != null;
			} catch (Exception e) {
				// do nothing
			}
			if (!targetExists) {
				storage.createDirectory(targetFolder);
			}
			progress.worked(1);
		} finally {
			progress.done();
		}

	}

	public static long diskUsage(StorageClient storageClient,
			String remotePath, IProgressMonitor monitor) throws Exception {
		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		try {
			// escape special characters in filenames!
			remotePath = URIUtil.encode(remotePath,
					org.apache.commons.httpclient.URI.allowed_fragment);
			monitor.beginTask("computing size for remote file or folder", 5);
			GridFileType gft = storageClient.listProperties(remotePath);
			monitor.worked(1);
			if (gft.getIsDirectory()) {
				SubProgressMonitor sub = new SubProgressMonitor(monitor, 4);
				return collectRemoteFiles(null, gft, storageClient, "", null,
						true, sub);
			} else {
				return gft.getSize();
			}
		} finally {
			monitor.done();
		}
	}

	public static void download(StorageClient storageClient, String remotePath,
			OutputStream stream, long bytes, IProgressMonitor monitor)
	throws Exception {
		download(storageClient, remotePath, stream, bytes, RENAME, monitor);
	}

	public static void download(StorageClient storageClient, String remotePath,
			OutputStream stream, long bytes, int mode, IProgressMonitor monitor)
	throws Exception {
		download(storageClient, remotePath, stream, bytes, mode, null, monitor);
	}

	public static void download(StorageClient storageClient, String remotePath,
			OutputStream stream, long bytes, int mode, String protocol, IProgressMonitor monitor)
	throws Exception {
		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		int work = 100;
		monitor.beginTask("downloading data", work);
		try {
			// escape special characters in filenames!
			remotePath = URIUtil.encode(remotePath,
					org.apache.commons.httpclient.URI.allowed_fragment); 

			ProtocolType.Enum prot = protocol == null ? null : ProtocolType.Enum.forString(protocol);
			if(prot == null)
			{
				ProtocolType.Enum[] prots = getPreferredProtocolOrder(bytes);
				prot = prots.length == 1 ? prots[0] : storageClient.findSupportedProtocol(prots);
			}
			Map<String,String> config = new HashMap<String, String>();
			FileTransferClient c = storageClient.getExport(remotePath, config, prot);
			ProgressListener<Long> l = new ProgressListenerAdapter(monitor,
					bytes, remotePath);
			if (c instanceof FiletransferOptions.IMonitorable) {
				((FiletransferOptions.IMonitorable) c)
				.setProgressListener(l);
				stream = new BufferedOutputStream(stream);
			}
			else
			{
				stream = new BufferedOutputStream(new ProgressListeningOutputStream(stream, l));
			}
			c.readAllData(stream);

		} 

		finally {
			try {
				if (stream != null) {
					stream.close();
				}
			} catch (Exception e) {
				// do nothing
			}

			monitor.done();

		}
	}

	public static void downloadFile(StorageClient storage, String remotePath,
			File targetFile, IProgressMonitor progress) throws Exception {
		downloadFile(storage, remotePath, targetFile, RENAME, progress);
	}

	public static void downloadFile(StorageClient storage, String remotePath,
			File targetFile, int mode, IProgressMonitor progress) throws Exception {
		downloadFile(storage, remotePath, targetFile, mode, null, progress);
	}

	public static void downloadFile(StorageClient storage, String remotePath,
			File targetFile, int mode, String protocol, IProgressMonitor progress)
	throws Exception {
		OutputStream os;
		// escape special characters in filenames!
		remotePath = URIUtil.encode(remotePath,
				org.apache.commons.httpclient.URI.allowed_fragment); 
		
		GridFileType gft = storage.listProperties(remotePath);
		if (gft == null) {
			throw new Exception("Remote file " + remotePath
					+ " not found on storage");
		}
		if (targetFile.exists()) {
			if (isSet(mode, FORCE)) {
				boolean append = isSet(mode, APPEND);
				os = new FileOutputStream(targetFile, append);
			} else if (isSet(mode, RENAME)) {
				targetFile = findUniquePath(targetFile);
				os = new FileOutputStream(targetFile, false);
			} else {
				return;
			}

		} else {
			if (!targetFile.getParentFile().exists()) {
				targetFile.getParentFile().mkdirs();
			}
			targetFile.createNewFile();
			os = new FileOutputStream(targetFile);
		}

		download(storage, remotePath, os, gft.getSize(), mode, protocol, progress);
	}


	public static void downloadFolder(StorageClient storage, String remotePath,
			File targetFile, IProgressMonitor progress) throws Exception {
		downloadFolder(storage, remotePath, targetFile, FORCE, progress);
	}

	public static void downloadFolder(StorageClient storage, String remotePath,
			File targetFile, int mode, IProgressMonitor progress) throws Exception {
		downloadFolder(storage, remotePath, targetFile, mode, null, progress);
	}


	public static void downloadFolder(StorageClient storage, String remotePath,
			File targetFile, int mode, String protocol, IProgressMonitor progress)
	throws Exception {
		progress = ProgressUtils.getNonNullMonitorFor(progress);
		progress.beginTask("downloading folder", 100);
		try {
			List<Pair<GridFileType, String>> collection = new ArrayList<Pair<GridFileType, String>>();
			Map<String, Long> fileSizes = new HashMap<String, Long>();
			IProgressMonitor sub = beginSubTask(progress,
					"computing download size", 20);
			remotePath = URIUtil.encode(remotePath,
					org.apache.commons.httpclient.URI.allowed_fragment); // escape
			// special
			// characters
			// in
			// filenames!
			GridFileType remoteProps = storage.listProperties(remotePath);
			long folderSize = collectRemoteFiles(collection, remoteProps,
					storage, targetFile.getAbsolutePath(), fileSizes, false,
					sub);
			sub.done();
			sub = beginSubTask(progress, "transferring", 80);
			sub.beginTask("", calculateEffort(folderSize));
			try {
				IProgressMonitor subProgress;
				for (Pair<GridFileType, String> pair : collection) {
					if (progress.isCanceled()) {
						return;
					}
					GridFileType currentSource = pair.getM1();
					String currentTarget = pair.getM2();
					File localFile = new File(currentTarget);
					if (currentSource.getIsDirectory()) {
						if (!localFile.exists() && !localFile.mkdirs()) {
							throw new Exception("Unable to create directory "
									+ localFile);
						}
						sub.worked(calculateEffort(1, folderSize));

					} else {
						subProgress = beginSubTask(
								sub,
								"transferring file",
								calculateEffort(
										fileSizes.get(currentSource.getPath()),
										folderSize));
						download(storage, currentSource.getPath(),
								new FileOutputStream(localFile),
								currentSource.getSize(), mode, protocol, subProgress);
					}
				}
			} finally {
				sub.done();
			}
		} finally {
			progress.done();
		}
	}



	public static File findUniquePath(File f) throws BaseFault {

		File parent = f.getParentFile();
		Set<String> existing = new HashSet<String>();
		String[] files = parent.list();
		for (String file : files) {
			existing.add(file);
		}
		String parentPath = parent.getAbsolutePath();
		String result = f.getName();

		String regExp = "(.*)\\((\\d+)\\)$";
		Pattern pattern = Pattern.compile(regExp);
		while (existing.contains(parentPath + File.separator + result)) {
			Matcher matcher = pattern.matcher(result);

			if (matcher.matches()) {
				String number = matcher.group(2);
				int next = Integer.parseInt(number) + 1;
				result = matcher.group(1) + "(" + next + ")";
			} else {
				result += "(2)";
			}
		}

		return new File(parent, result);

	}

	public static String findUniquePath(StorageClient sc, String path)
	throws BaseFault {
		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		if (path.endsWith("/") && path.length() > 1) {
			path = path.substring(0, path.length() - 2);
		}
		int index = path.lastIndexOf("/");
		String parent = path.substring(0, index);
		Set<String> existing = new HashSet<String>();
		GridFileType[] files = sc.listDirectory(parent);
		for (GridFileType gridFileType : files) {
			existing.add(gridFileType.getPath());
		}
		String result = path;
		String regExp = "(.*)\\((\\d+)\\)$";
		Pattern pattern = Pattern.compile(regExp);
		while (existing.contains(result)) {
			Matcher matcher = pattern.matcher(result);

			if (matcher.matches()) {
				String number = matcher.group(2);
				int next = Integer.parseInt(number) + 1;
				result = matcher.group(1) + "(" + next + ")";
			} else {
				result += "(2)";
			}
		}

		return result;

	}

	public static String getStorageAddress(String fileAddress) throws Exception {
		int index = fileAddress.indexOf("#");
		if (index > 0) {
			String result = fileAddress.substring(0, index);
			if (!result.startsWith("http")) {
				result = result.substring(result.indexOf(":") + 1); // remove
				// file
				// transfer
				// protocol
			}
			return result;

		} else {
			throw new Exception("Invalid UNICORE 6 file address: "
					+ fileAddress);
		}
	}

	public static boolean isSet(int flags, int mask) {
		return (flags & mask) == mask;
	}

	public static String parentFromFullPath(String path) {
		return path.substring(0, Math.max(0, path.lastIndexOf("/")));
	}


	public static void upload(StorageClient storageClient, InputStream stream,
			long bytes, String remotePath, IProgressMonitor monitor)
	throws Exception {
		upload(storageClient, stream, bytes, remotePath, RENAME, monitor);
	}

	public static void upload(StorageClient storageClient, InputStream stream,
			long bytes, String remotePath, int mode, IProgressMonitor monitor)
	throws Exception {
		upload(storageClient, stream, bytes, remotePath, mode, null, monitor);
	}

	public static void upload(StorageClient storageClient, InputStream stream,
			long bytes, String remotePath, int mode, String protocol, IProgressMonitor monitor)
	throws Exception {
		//		// escape special characters in filenames!
		//		remotePath = URIUtil.encode(remotePath,
		//				org.apache.commons.httpclient.URI.allowed_fragment); 
		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		int work = 100;
		monitor.beginTask("uploading data", work);
		try {

			/*
			 * if file already does exist, it must be deleted before upload is
			 * possible
			 */
			work--;
			GridFileType gft = null;
			try {
				gft = storageClient.listProperties(remotePath);
				if (gft != null) {
					if (isSet(mode, FORCE)) {
						storageClient.delete(remotePath);
					} else if (isSet(mode, RENAME)) {
						remotePath = findUniquePath(storageClient, remotePath);
					} else {
						throw new Exception("Unable to upload file "
								+ remotePath
								+ ": The file already exists on the storage.");
					}
				}
			} catch (FileNotFoundException e) {
				// the file doesn't exist => everything ok
			}

			monitor.worked(1);
			/*
			 * upload the data
			 */
			IProgressMonitor sub = beginSubTask(monitor, "", 99);
			FileTransferClient c = null;
			try {
				ProtocolType.Enum prot = protocol == null ? null : ProtocolType.Enum.forString(protocol);
				if(prot == null)
				{
					ProtocolType.Enum[] prots = getPreferredProtocolOrder(bytes);
					prot = prots.length == 1 ? prots[0] : storageClient.findSupportedProtocol(prots);
				}
				Map<String,String> config = createExtraParameters();
				boolean append = isSet(mode, APPEND);
				c = storageClient.getImport(remotePath, append, config, prot);
				ProgressListener<Long> l = new ProgressListenerAdapter(sub,
						bytes, remotePath);
				sub.beginTask("", 100);
				if (c instanceof FiletransferOptions.IMonitorable) {

					((FiletransferOptions.IMonitorable)c).setProgressListener(l);
					stream = new BufferedInputStream(stream);
				} else {
					stream = new ProgressListeningInputStream(new BufferedInputStream(stream),l);
				}
				c.writeAllData(stream);
			} finally {
				sub.done();
				if (c != null) {
					c.destroy();
				}
			}

		} finally {
			monitor.done();
		}
	}



	/**
	 * @return
	 */
	private static Map<String, String> createExtraParameters() {
		Map<String, String> parameters = new HashMap<String, String>();

		IPreferenceStore prefs = UnicoreCommonActivator.getDefault().getPreferenceStore();

		parameters.put(UFTPConstants.PARAM_STREAMS,
				Integer.toString(prefs.getInt(Constants.P_UFTP_STREAM_NUMBER)));
		String uftpClientHost = prefs.getString(Constants.P_UFTP_CLIENT_HOST);
		if (!uftpClientHost.equals("")) {
			parameters.put(UFTPConstants.PARAM_CLIENT_HOST, uftpClientHost);
		}

		parameters.put(UFTPConstants.PARAM_ENABLE_ENCRYPTION,
				Boolean.toString(prefs.getBoolean(Constants.P_UFTP_ENABLE_ENCRYPTION)));
		
		parameters.put(UFTPConstants.PARAM_ENABLE_COMPRESSION,
				Boolean.toString(prefs.getBoolean(Constants.P_UFTP_ENABLE_COMPRESSION)));
		
		//add unique secret
		if(parameters.get(UFTPConstants.PARAM_SECRET)==null){
			parameters.put(UFTPConstants.PARAM_SECRET, UUID.randomUUID().toString());
		}

		return parameters;
	}

	public static void uploadFile(StorageClient storage, File source,
			String targetFile, IProgressMonitor progress) throws Exception {
		uploadFile(storage, source, targetFile, RENAME, progress);
	}

	public static void uploadFile(StorageClient storage, File source,
			String targetFile, int mode, IProgressMonitor progress) throws Exception {
		uploadFile(storage, source, targetFile, mode, null, progress);
	}

	public static void uploadFile(StorageClient storage, File source,
			String targetFile,int mode, String protocol, IProgressMonitor progress) throws Exception {
		upload(storage, new FileInputStream(source), source.length(),
				targetFile, mode, protocol, progress);
	}



	public static void uploadFolder(StorageClient storage, File source,
			String target, int mode, IProgressMonitor progress)
	throws Exception {
		uploadFolder(storage, source, target, mode, null, progress);
	}

	public static void uploadFolder(StorageClient storage, File source,
			String target, int mode, String protocol, IProgressMonitor progress)
	throws Exception {

		progress = ProgressUtils.getNonNullMonitorFor(progress);
		try {
			//			// escape special characters in filenames!
			//			target = URIUtil.encode(target,
			//					org.apache.commons.httpclient.URI.allowed_fragment); 
			List<Pair<File, String>> collection = new ArrayList<Pair<File, String>>();
			Map<File, Long> fileSizes = new HashMap<File, Long>();
			long folderSize = collectFiles(collection, source, target,
					fileSizes, false, null);
			progress.beginTask("uploading folder", calculateEffort(folderSize));
			IProgressMonitor subProgress;
			for (Pair<File, String> pair : collection) {
				if (progress.isCanceled()) {
					return;
				}
				File currentSource = pair.getM1();
				String currentTarget = pair.getM2();

				if (currentSource.isDirectory()) {
					subProgress = beginSubTask(progress, "creating folder",
							calculateEffort(1, folderSize));
					createFolder(storage, currentSource, currentTarget,
							subProgress);
				} else {
					subProgress = beginSubTask(
							progress,
							"transferring file",
							calculateEffort(fileSizes.get(currentSource),
									folderSize));
					uploadFile(storage, currentSource, currentTarget, mode, 
							protocol, subProgress);
				}
			}
		} finally {
			progress.done();
		}
	}

	public static void uploadFolder(StorageClient storage, File source,
			String target, IProgressMonitor progress) throws Exception {
		uploadFolder(storage, source, target, RENAME, progress);
	}

	public static ProtocolType.Enum[] getPreferredProtocolOrder(long fileSize)
	{
		ProtocolType.Enum[] prots = null;
		String protocol = UnicoreCommonActivator.getDefault()
		.getDefaultFileTransferProtocol();
		if(Constants.FILE_TRANSFER_PROTOCOL_AUTO[1].equals(protocol))
		{
			if(fileSize > 1024*1024)
			{
				// for files larger than 1 MB negotiate a fast protocol
				prots = new ProtocolType.Enum[]{ProtocolType.UFTP,ProtocolType.UDT,ProtocolType.BFT,ProtocolType.RBYTEIO};
			}
			else
			{
				// for small files prefer a simple protocol with small overhead (but allow others as well)
				prots = new ProtocolType.Enum[]{ProtocolType.BFT, ProtocolType.RBYTEIO, ProtocolType.UDT, ProtocolType.UFTP};
			}
		}
		else {
			// use preferred protocol plus BFT and ByteIO as fallback
			prots = new ProtocolType.Enum[]{ProtocolType.Enum.forString(protocol),ProtocolType.BFT,ProtocolType.RBYTEIO};
		}
		return prots;
	}


	public static ProtocolType.Enum[] getPreferredProtocolOrder()
	{
		return getPreferredProtocolOrder(10*1024*1024); // assume 10MB by default
	}




}
