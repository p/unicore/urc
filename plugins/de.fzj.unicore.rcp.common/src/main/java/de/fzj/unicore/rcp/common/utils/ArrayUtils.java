package de.fzj.unicore.rcp.common.utils;

import java.lang.reflect.Array;

public class ArrayUtils {

	public static <T> boolean contains(T[] array, T test) {
		if(array==null || array.length==0)
			return false;
		
		for(T s: array){
			if(s!=null && s.equals(test))
				return true;
		}
		return false;
	}

	public static String[] add(String[] array, String... added) {
		if(array == null && added == null) return new String[0];
		return concat(array,added);
	}
	
	/**
	 * Returns a new array that is a concatenation of the two input arrays.
	 * Caution: 
	 * @param <T>
	 * @param array
	 * @param toAppend
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] concat(T first, T... toAppend) {
		if(toAppend == null && first == null) return null;
		if(first == null) 
		{	
			T[] result = (T[]) Array.newInstance(toAppend.getClass().getComponentType(), toAppend.length);
			System.arraycopy(toAppend, 0, result, 0, toAppend.length);
			return result;
		}
		if(toAppend == null) toAppend = (T[]) createEmptyArray(first.getClass());
		
		T[] newArray = (T[]) Array.newInstance(first.getClass(), toAppend.length+1);
		newArray[0] = first;
		if(toAppend.length > 0) System.arraycopy(toAppend, 0, newArray, 1, toAppend.length);
		return newArray;
	}
	
	/**
	 * Returns a new array that is a concatenation of the two input arrays.
	 * 
	 * @param <T>
	 * @param array
	 * @param toAppend
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] concat(T[] array, T... toAppend) {
		if(toAppend == null && array == null) return null;
		if(toAppend == null) toAppend = (T[]) createEmptyArray(array.getClass().getComponentType());
		if (array == null) array = (T[]) createEmptyArray(toAppend.getClass().getComponentType());
		
		int arrayLength = array.length;
		T[] newArray = (T[]) Array.newInstance(array.getClass().getComponentType(), arrayLength + toAppend.length);
		if(array.length > 0) System.arraycopy(array, 0, newArray, 0, arrayLength);
		if(toAppend.length > 0) System.arraycopy(toAppend, 0, newArray, arrayLength, toAppend.length);
		return newArray;
	}
	
	@SuppressWarnings("unchecked")
	private static <C> C[] createEmptyArray(Class<C> newType)
	{
		return ((Object)newType == (Object)Object[].class)
        ? (C[]) new Object[0]
        : (C[]) Array.newInstance(newType, 0);
	}

}
