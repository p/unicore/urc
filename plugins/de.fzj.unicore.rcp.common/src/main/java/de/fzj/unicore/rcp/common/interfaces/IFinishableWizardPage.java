package de.fzj.unicore.rcp.common.interfaces;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.IWizardPage;

/**
 * "Normal" wizard pages lack the ability to perform some code when the wizard's
 * {@link IWizard#performFinish()} method is called. Instances of this interface
 * provide this method which is needed for building extensible wizards.
 * 
 * @author bdemuth
 * 
 */
public interface IFinishableWizardPage extends IWizardPage {

	/**
	 * Perform steps that are required when the user presses the wizard's finish
	 * button, i.e. when the user confirms the action. Return true if all
	 * required steps could be performed and false if some error occurred.
	 * Returning false will prevent the wizard from completing its job!
	 * 
	 * @return
	 */
	public boolean finish();

}
