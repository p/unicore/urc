package de.fzj.unicore.rcp.common.utils;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;


/**
 * Menu manager that sorts its items between separators when creating a context
 * menu. Note that this alters the normal order that is created e.g. by calling
 * {@link #insertBefore(String, org.eclipse.jface.action.IAction)} or 
 * {@link #insertAfter(String, org.eclipse.jface.action.IAction)}.
 * @author bdemuth
 *
 */
public class SortingMenuManager extends MenuManager {


	protected Comparator<MenuItem> comparator;


	public SortingMenuManager() {
		super();
	}



	public SortingMenuManager(String text, ImageDescriptor image, String id) {
		super(text, image, id);
	}



	public SortingMenuManager(String text, String id) {
		super(text, id);
	}



	public SortingMenuManager(String text) {
		super(text);
	}





	protected void update(boolean force, boolean recursive) {

		if(isDirty() || force)
		{
			Menu tempMenu = new Menu(getMenu().getParent());
			IContributionItem[] items = getItems();
			removeAll();
			for(Item item : getMenuItems())
			{
				item.dispose();
			}
			
			// init stuff
	
			if(comparator == null) comparator = createComparator();
			final Map<IContributionItem,MenuItem> map = new HashMap<IContributionItem, MenuItem>();

			for(int i = 0; i < items.length; i++)
			{
				IContributionItem ci = items[i];
				
				ci.fill(tempMenu, i);
				MenuItem newItem = tempMenu.getItem(i);
				map.put(ci, newItem);

				int j = i;
				if(!ci.isSeparator() && j > 0)
				{

					IContributionItem previous = items[j-1];
					MenuItem previousItem = map.get(previous);
					boolean smaller = comparator.compare(newItem,previousItem) < 0;
					
					while(j > 0 &&  !previous.isSeparator() && smaller)
					{
						// swap the two
						items[j] = previous;
						items[j-1] = ci;
						
						j --;
						if(j > 0)
						{
							previous = items[j-1];
							previousItem = map.get(previous);
							smaller = comparator.compare(newItem,previousItem) < 0;
						}
					}
				}
			}
			for(int i = 0; i < items.length; i++)
			{
				add(items[i]);
			}

			// cleanup
			tempMenu.dispose();
			tempMenu = null;

		}
		super.update(force,recursive);


	}


	protected Comparator<MenuItem> createComparator()
	{
		return new Comparator<MenuItem>() {

			@Override
			public int compare(MenuItem o1, MenuItem o2) {
				return o1.getText().compareToIgnoreCase(o2.getText());
			}
		};
	}

	public void setComparator(Comparator<MenuItem> comparator) {
		this.comparator = comparator;
	}

}
