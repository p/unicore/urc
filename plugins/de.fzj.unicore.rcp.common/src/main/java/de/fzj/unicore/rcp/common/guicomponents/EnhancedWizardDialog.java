package de.fzj.unicore.rcp.common.guicomponents;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

/**
 * This Wizard dialog allows for programatically calling
 * {@link #finishPressed()} and {@link #cancelPressed()}.
 * 
 * @author bdemuth
 * 
 */
public class EnhancedWizardDialog extends WizardDialog {

	public EnhancedWizardDialog(Shell parentShell, IWizard newWizard) {
		super(parentShell, newWizard);
	}

	@Override
	public void cancelPressed() {
		super.cancelPressed();
	}

	@Override
	public void finishPressed() {
		super.finishPressed();
	}

}
