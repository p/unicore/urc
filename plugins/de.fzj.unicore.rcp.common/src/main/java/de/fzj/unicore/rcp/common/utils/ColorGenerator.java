package de.fzj.unicore.rcp.common.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.eclipse.swt.graphics.RGB;

public class ColorGenerator {

	class RandomExponentialDistribution {
		private Random rand;
		private double mean;

		/**
		 * Create a new generator of random numbers with exponential
		 * distribution of specified mean that will use <code>randgen</code> as
		 * its source of random numbers.
		 * 
		 * @param mean
		 *            the mean of the exponential distribution
		 * @param randgen
		 *            a Random object to be used by the generator.
		 */
		RandomExponentialDistribution(double mean, Random randgen) {
			this.mean = mean;
			rand = randgen;
		}

		/**
		 * Return a random number with exponential distribution.
		 */
		public double nextDouble() {
			double val;

			do {
				val = rand.nextDouble();
			} while (val == 0.0);

			return mean * (-Math.log(val));
		}
	}

	public static final RGB RED = new RGB(255, 0, 0);
	public static final RGB GREEN = new RGB(0, 255, 0);
	public static final RGB BLUE = new RGB(0, 0, 255);

	private static final int[] sectorBoundaries = new int[] { 0, 24, 50, 70,
			180, 250 };

	/**
	 * Size of memory of the generator. 0 means no memory.
	 */
	private int memorySize;

	private long seed = 1;

	/**
	 * Data structure for remembering colors which were generated in the past.
	 */
	private List<RGB> memory;

	/**
	 * How strict is the metric judging how similar are colors in the memory.
	 * For 10-12, it can generate many different colors, with 15, it's only
	 * about 80-90.
	 */
	public static final int METRICBOUND = 12;

	/**
	 * The colorspace is divided in {@link #numSectors} different parts. When
	 * creating new colors, we cycle through these sectors in order to obtain
	 * different colors. Cycling is equivalent to incrementing and applying a
	 * modulo to this value.
	 */
	private int sector = 3; // start with green

	/**
	 * A constructor with no parameters, such a generator creates pastel colors,
	 * but gives no guarantee that they won't be too similar.
	 */
	public ColorGenerator() {
		this(System.currentTimeMillis());
	}

	public ColorGenerator(int memorysize, long seed) {
		this(memorysize, seed, new LinkedList<RGB>());
	}

	/**
	 * A constructor with two parameter, giving the size of memory and a
	 * (pseudo) random seed. By using the same seed, the same sequence of colors
	 * is achieved.
	 */
	public ColorGenerator(int memorysize, long seed, List<RGB> memory) {
		this.seed = seed;
		memorySize = memorysize;
		this.memory = memory;
	}

	public ColorGenerator(long seed) {
		this(100, seed);
	}

	private double colorDistanceFromMemory(RGB c) {
		double min = Double.MAX_VALUE;
		for (RGB color : memory) {
			min = Math.min(min, colorDistance(c, color));
		}
		return min;
	}

	private double colorPurity(RGB c) {
		double redDist = colorDistance(c, RED);
		double greenDist = colorDistance(c, GREEN);
		double blueDist = colorDistance(c, BLUE);
		double minDist = Math.min(redDist, Math.min(greenDist, blueDist));
		return Math.abs(800 - minDist);
	}

	private RGB createColor(Random rand) {

		int sectorStart = sectorBoundaries[sector];
		int sectorEnd = sectorBoundaries[sector + 1];
		if (sector == 0 && rand.nextBoolean()) {
			// due to the cyclic nature of the hue, we combine low hue
			// values and high hue values to one big color range
			int numSectors = sectorBoundaries.length - 1;
			sectorStart = sectorBoundaries[numSectors];
			sectorEnd = 360;
		}

		int offset = rand.nextInt(sectorEnd - sectorStart + 1);
		float hue = sectorStart + offset;

		// for brightness and saturation, tend to use higher values
		RandomExponentialDistribution exp = new RandomExponentialDistribution(
				.7, rand);
		float saturation = (float) Math.min(.99, .5 + exp.nextDouble() / 3);
		float brightness = (float) Math.min(.99, .5 + exp.nextDouble() / 3);

		return new RGB(hue, saturation, brightness);
	}

	/**
	 * Generate a color for object o. Uses the hashcode of o in order to create
	 * a color for o.
	 * 
	 * @param o
	 * @return
	 */
	public RGB nextColor(Object o) {
		RGB result = null;
		long newSeed = seed * o.hashCode();
		Random rand = new Random(newSeed);
		int numSectors = sectorBoundaries.length - 1;
		int maxAttempts = 50;

		if (memorySize == 0)// No memory - just generates colors
		{

			return createColor(rand);

		} else // There is memory for colors
		{

			double maxRanking = -1;
			for (int i = 0; i < maxAttempts; i++) {
				RGB c = createColor(rand);
				// calculate the ranking
				// at first we'd like to get "pure" colors, later on it is more
				// important to get colors that differ from the colors in our
				// memory
				double weight = 1 / (Math.sqrt(memory.size()) + 1);
				double ranking = (1 - weight) * colorDistanceFromMemory(c)
						+ weight * colorPurity(c);
				if (ranking > maxRanking) {
					maxRanking = ranking;
					result = c;
				}

			}
		}

		sector++;
		sector %= numSectors;

		memory.add(result); // Because we added the color to it.
		if (memory.size() > memorySize) // If the memory is too full, we remove
										// the oldest inserted color.
		{
			memory.remove(0);
		}

		return result;
	}

	/**
	 * Computes the "distance" between two colors, trying to measure how
	 * different the colors appear to the human eye. This is a difficult task,
	 * suffering from the fact that color perception is subjective.
	 * 
	 * @param c1
	 * @param c2
	 * @return
	 */
	public static double colorDistance(RGB c1, RGB c2) {
		long rmean = ((long) c1.red + (long) c2.red) / 2;
		long r = (long) c1.red - (long) c2.red;
		long g = (long) c1.green - (long) c2.green;
		long b = (long) c1.blue - (long) c2.blue;
		return Math.sqrt((((512 + rmean) * r * r) >> 8) + 4 * g * g
				+ (((767 - rmean) * b * b) >> 8));
	}
}
