package de.fzj.unicore.rcp.common.guicomponents;

import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.widgets.Composite;

public class IntegerCellEditor extends TextCellEditor {

	public IntegerCellEditor(Composite parent) {
		super(parent);
		setValidator(new IntegerCellEditorValidator());
	}

	@Override
	protected Object doGetValue() {
		return Integer.parseInt((String) super.doGetValue());
	}

	@Override
	protected void doSetValue(Object value) {
		Integer i = (Integer) value;
		super.doSetValue(String.valueOf(i));
	}

}
