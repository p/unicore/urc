/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.common;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.fzj.unicore.rcp.common.utils.DelegatingExtensionClassloader;
import de.fzj.unicore.rcp.common.utils.WorkspaceUtil;
import de.fzj.unicore.rcp.logmonitor.LogActivator;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author Bastian Demuth, Valentina Huber
 */
public class UnicoreCommonActivator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "de.fzj.unicore.rcp.common";
	public static final String ICONS_PATH = "$nl$/icons/"; //$NON-NLS-1$
	// The shared instance
	private static UnicoreCommonActivator plugin;

	private DelegatingExtensionClassloader xmlBeansClassloader;

	public static final int LEVEL_BEGINNER = 1, LEVEL_ADVANCED = 50,
			LEVEL_EXPERT = 100;

	public static final String[][] GRID_DETAIL_LEVELS = new String[][] {
		{ "beginner", String.valueOf(LEVEL_BEGINNER) },
		{ "advanced", String.valueOf(LEVEL_ADVANCED) },
		{ "expert", String.valueOf(LEVEL_EXPERT) } };

	/**
	 * The constructor
	 */
	public UnicoreCommonActivator() {
		plugin = this;
	}



	/**
	 * @return the file transport protocol that shall be used by default
	 */
	public String getDefaultFileTransferProtocol() {
		return getPreferenceStore().getString(
				Constants.P_DEFAULT_FILE_TRANSFER_PROTOCOL);
	}

	/**
	 * @return the gridDetailLevel
	 */
	public int getGridDetailLevel() {
		try {

			return Integer.parseInt(getPreferenceStore().getString(
					Constants.P_GRID_DETAIL_LEVEL));
		} catch (Exception e) {
			return LEVEL_BEGINNER;
		}
	}



	public DelegatingExtensionClassloader getXmlBeansClassloader() {
		if (xmlBeansClassloader == null) {
			xmlBeansClassloader = new DelegatingExtensionClassloader(getClass()
					.getClassLoader(),
					"de.fzj.unicore.rcp.common.xmlBeansClasses");
		}

		return xmlBeansClassloader;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		WorkspaceUtil.fixWorkspace(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		// storeWorkspaceLocation();
		super.stop(context);

	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static UnicoreCommonActivator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		if (imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH + path) == null) {
			log(IStatus.WARNING, "Can't find image " + path);
		}
		return imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH + path);

	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param forceAlarmUser
	 *            forces the logging plugin to open a popup; usually, this
	 *            should NOT be set to true!
	 */
	public static void log(int status, String msg, boolean forceAlarmUser) {
		log(status, msg, null, forceAlarmUser);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 * @param forceAlarmUser
	 *            forces the logging plugin to open a popup; usually, this
	 *            should NOT be set to true!
	 */
	public static void log(int status, String msg, Exception e,
			boolean forceAlarmUser) {
		IStatus s = new Status(status, PLUGIN_ID, IStatus.OK, msg, e);
		LogActivator.log(plugin, s, forceAlarmUser);
	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 */
	public static void log(String msg) {
		log(msg, null);
	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(String msg, Exception e) {
		log(IStatus.INFO, msg, e, false);
	}

}
