package de.fzj.unicore.rcp.common.detailsView;

import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class ValueCellEditor extends TextCellEditor {
	

	public ValueCellEditor(Composite parent) {
		super(parent,SWT.MULTI | SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL);
		((Text) getControl()).setEditable(false);
	}

	@Override
	protected void doSetFocus() {
		super.doSetFocus();
	}

}
