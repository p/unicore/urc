package de.fzj.unicore.rcp.common.guicomponents;

import org.eclipse.jface.action.IAction;
import org.eclipse.ui.IEditorPart;

public interface IEditorPartWithActions extends IEditorPart {

	public abstract IAction getAction(String actionID);

	public abstract void setAction(String actionID, IAction action);

}