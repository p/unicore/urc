package de.fzj.unicore.rcp.common.utils;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

/**
 * This classloader will first look at the provided classloader and then iterate
 * over the extensions for the given extension point ID in order to find a given
 * class there. This is useful in order to simulate Buddy classloading without
 * causing deadlocks (Eclipse's registered Buddy classloading causes deadlocks).
 * 
 * @author bdemuth
 * 
 */
public class DelegatingExtensionClassloader extends ClassLoader {

	private String[] extensionPointIds;
	private Bundle[] extendingPlugins;


	public DelegatingExtensionClassloader(ClassLoader ownClassLoader,
			String... extensionPointIds) {
		super(ownClassLoader);
		this.extensionPointIds = extensionPointIds;
		// initialize yourself and start all bundles in order to get ready
		// doing this lazily causes problems with Jaxb deserialization
		// as the bundles did not have a chance to add their packages to the
		// context
		// in time
		getExtendingPlugins();
	}

	@Override
	public Class<?> findClass(String name) throws ClassNotFoundException {
		for (Bundle bundle : getExtendingPlugins()) {
			try {
				return bundle.loadClass(name);
			} catch (Throwable t) {
				// do nothing
			}
		}
		throw new ClassNotFoundException(
				"The class "
						+ name
						+ " could neither be found by this plugin's class loader nor in any of the extending plugins.");
	}

	private Bundle[] getExtendingPlugins() {
		if (extendingPlugins == null) {
			loadExtensions();
		}
		return extendingPlugins;
	}

	@Override
	public URL getResource(String name) {
		URL result = super.getResource(name);
		if (result == null) {
			for (Bundle bundle : getExtendingPlugins()) {
				result = bundle.getResource(name);
				if (result != null) {
					return result;
				}
			}
		}
		return result;
	}

	private void loadExtensions() {
		Set<Bundle> result = new HashSet<Bundle>();
		for (String extensionPointId : extensionPointIds) {
			// iterate over extensions and find extending plugins whose
			// classloaders
			// should be queried, too
			IExtensionRegistry registry = Platform.getExtensionRegistry();
			IExtensionPoint extensionPoint = registry
					.getExtensionPoint(extensionPointId);
			IExtension[] extensions = extensionPoint.getExtensions();
			for (IExtension iExtension : extensions) {
				String bundleId = iExtension.getContributor().getName();
				if(bundleId != null) {
					Bundle b = Platform.getBundle(bundleId);
					if(b != null) {
						result.add(b);
						try {
							b.loadClass("noname");
						} catch (Exception e) {
							// do nothing
						}
					}
				}
			}
		}

		extendingPlugins = result.toArray(new Bundle[result.size()]);
	}

}
