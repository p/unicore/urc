package de.fzj.unicore.rcp.common.detailsView;

public interface IKeyValueListViewer {

	/**
	 * Update the view to reflect the fact that a task was added to the task
	 * list
	 * 
	 * @param task
	 */
	public void addKeyValue(KeyValue kv);

	/**
	 * Update the view to reflect the fact that a task was removed from the task
	 * list
	 * 
	 * @param task
	 */
	public void removeKeyValue(KeyValue kv);

	/**
	 * Update the view to reflect the fact that one of the tasks was modified
	 * 
	 * @param task
	 */
	public void updateKeyValue(KeyValue kv);
}
