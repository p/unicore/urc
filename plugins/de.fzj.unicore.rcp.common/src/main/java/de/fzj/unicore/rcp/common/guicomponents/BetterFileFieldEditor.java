package de.fzj.unicore.rcp.common.guicomponents;

import java.io.File;

import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.widgets.Composite;

/**
 * A field editor for a file path type preference. A standard file dialog
 * appears when the user presses the change button.
 */
public class BetterFileFieldEditor extends FileFieldEditor {

	/**
	 * Indicates whether the path must be absolute; <code>false</code> by
	 * default.
	 */
	protected boolean enforceAbsolute = false;

	protected boolean enforceExistence = false;

	/**
	 * List of legal file extension suffixes, or <code>null</code> for system
	 * defaults.
	 */
	protected String[] extensions = null;

	/**
	 * Creates a file field editor.
	 * 
	 * @param name
	 *            the name of the preference this field editor works on
	 * @param labelText
	 *            the label text of the field editor
	 * @param enforceAbsolute
	 *            <code>true</code> if the file path must be absolute, and
	 *            <code>false</code> otherwise
	 * @param parent
	 *            the parent of the field editor's control
	 */
	public BetterFileFieldEditor(String name, String labelText,
			boolean enforceExistence, boolean enforceAbsolute, Composite parent) {
		init(name, labelText);
		this.enforceAbsolute = enforceAbsolute;
		this.enforceExistence = enforceExistence;
		setErrorMessage(JFaceResources
				.getString("FileFieldEditor.errorMessage"));//$NON-NLS-1$
		setChangeButtonText(JFaceResources.getString("openBrowse"));//$NON-NLS-1$
		setValidateStrategy(VALIDATE_ON_KEY_STROKE);
		createControl(parent);
	}

	/**
	 * Creates a file field editor.
	 * 
	 * @param name
	 *            the name of the preference this field editor works on
	 * @param labelText
	 *            the label text of the field editor
	 * @param parent
	 *            the parent of the field editor's control
	 */
	public BetterFileFieldEditor(String name, String labelText, Composite parent) {
		this(name, labelText, false, false, parent);
	}

	/*
	 * (non-Javadoc) Method declared on StringFieldEditor. Checks whether the
	 * text input field specifies an existing file.
	 */
	@Override
	protected boolean checkState() {

		String msg = null;

		String path = getTextControl().getText();
		if (path != null) {
			path = path.trim();
		} else {
			path = "";//$NON-NLS-1$
		}
		if (path.length() == 0) {
			if (!isEmptyStringAllowed()) {
				msg = getErrorMessage();
			}
		} else {
			File file = new File(path);

			if (enforceAbsolute && !file.isAbsolute()) {
				msg = JFaceResources.getString("FileFieldEditor.errorMessage2");//$NON-NLS-1$
			}

			boolean exists = file.isFile();
			if (!exists && enforceExistence) {
				msg = getErrorMessage();
			}

		}

		boolean matchesExtension = false;
		String validExtensions = "";
		if (msg == null && extensions != null) {
			for (int i = 0; i < extensions.length; i++) {
				String ext = extensions[i];
				// TODO this is a hack
				ext = ext.replaceFirst("\\*", "");
				validExtensions += ext;
				if (i < extensions.length - 1) {
					validExtensions += ",";
				}
				if (".*".equals(ext.toLowerCase())
						|| path.toLowerCase().endsWith(ext.toLowerCase())) {
					matchesExtension = true;
					break;
				}

			}
			if (!matchesExtension) {
				msg = "Please select a file with one of the following extensions: "
						+ validExtensions;
			}
		}

		if (msg != null) { // error
			showErrorMessage(msg);
			return false;
		}

		// OK!
		clearErrorMessage();
		return true;
	}

	@Override
	public void setFileExtensions(String[] extensions) {
		super.setFileExtensions(extensions);
		this.extensions = extensions;
	}

}
