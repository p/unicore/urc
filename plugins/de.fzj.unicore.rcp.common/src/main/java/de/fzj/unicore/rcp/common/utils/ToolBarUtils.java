package de.fzj.unicore.rcp.common.utils;

import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.SubContributionItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class ToolBarUtils {

	public static void setToolItemText(ToolBar toolBar) {
		ToolItem[] items = toolBar.getItems();
		for (int i = 0; i < items.length; i++) {
			IContributionItem item = (IContributionItem) items[i].getData();

			if (!(item.isGroupMarker() || item.isSeparator())) {
				if (item instanceof ActionContributionItem) {
					ActionContributionItem actionItem = (ActionContributionItem) item;
					items[i].setText(actionItem.getAction().getText());
				} else if (item instanceof SubContributionItem) {
					items[i].setText(((ActionContributionItem) ((SubContributionItem) item)
							.getInnerItem()).getAction().getText());
				}
			}
		}
	}
}
