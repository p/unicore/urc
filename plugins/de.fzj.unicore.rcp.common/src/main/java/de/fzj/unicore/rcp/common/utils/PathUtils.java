package de.fzj.unicore.rcp.common.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.httpclient.util.URIUtil;
import org.apache.tools.ant.util.FileUtils;
import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.ide.undo.CreateProjectOperation;

public class PathUtils {

	public static IFile absolutePathToEclipseFile(IPath path) {
		IPath rootPath = ResourcesPlugin.getWorkspace().getRoot().getLocation();
		if (rootPath.isPrefixOf(path)) {
			IPath relative = makeRelativeTo(rootPath, path);
			return ResourcesPlugin.getWorkspace().getRoot().getFile(relative);
		} else {
			// file is contained in a linked project
			IProject[] projects = ResourcesPlugin.getWorkspace().getRoot()
			.getProjects();
			for (IProject project : projects) {
				if (project.getLocation().isPrefixOf(path)) {
					IPath relative = makeRelativeTo(project.getLocation(), path);
					return project.getFile(relative);
				}
			}
		}
		return null;
	}

	public static IFolder absolutePathToEclipseFolder(IPath path) {
		IPath rootPath = ResourcesPlugin.getWorkspace().getRoot().getLocation();
		if (rootPath.isPrefixOf(path)) {
			IPath relative = makeRelativeTo(rootPath, path);
			return ResourcesPlugin.getWorkspace().getRoot().getFolder(relative);
		} else {
			// file is contained in a linked project
			IProject[] projects = ResourcesPlugin.getWorkspace().getRoot()
			.getProjects();
			for (IProject project : projects) {
				if (project.getLocation().isPrefixOf(path)) {
					IPath relative = makeRelativeTo(project.getLocation(), path);
					return project.getFolder(relative);
				}
			}
		}
		return null;
	}

	/**
	 * Copy a file from an absolute source path to an absolute target path
	 * 
	 * @param source
	 * @param target
	 * @throws IOException
	 */
	public static void copy(IPath source, IPath target, boolean overwrite)
	throws IOException {
		FileUtils.getFileUtils().copyFile(source.toFile(), target.toFile(),
				null, overwrite);
		try {
			IFile file = absolutePathToEclipseFile(target);
			file.refreshLocal(0, null);
		} catch (Exception e) {
		}
	}

	public static void createFile(IFile file, IProgressMonitor progress)
	throws Exception {
		File f = file.getLocation().toFile();
		if (!f.exists()) {
			if (!f.getParentFile().exists()) {
				f.getParentFile().mkdirs();
			}
			f.createNewFile();

		} else {
			throw new IOException("File " + f.toString() + " already exists.");
		}
		if (file.getProject() != null) {
			file.getProject().refreshLocal(
					file.getProjectRelativePath().segmentCount(), progress);
		}
	}

	/**
	 * Tries to identify the project that contains the file with the given URI
	 * and returns a project relative path or null if the project cannot be
	 * identified or the file does not exist.
	 * 
	 * @param uri
	 * @return
	 */
	public static IPath extractProjectRelativePath(URI uri) {
		if (uri == null || !new File(uri).exists()) {
			return null;
		}
		IProject[] projects = ResourcesPlugin.getWorkspace().getRoot()
		.getProjects();
		for (IProject project : projects) {
			IPath p = project.getLocation();
			File f = new File(uri);
			IPath absolute = new Path(f.getAbsolutePath());
			if (p.isPrefixOf(absolute)) {
				return makeRelativeTo(p, absolute);
			}
		}
		return null;
	}

	/**
	 * This returns a project relative path for the given URI or null if the
	 * file does not exist.
	 * 
	 * @param uri
	 * @return
	 */
	public static IPath extractProjectRelativePath(URI uri, IProject project) {
		if (uri == null || !new File(uri).exists()) {
			return null;
		}

		IPath p = project.getLocation();
		File f = new File(uri);
		IPath absolute = new Path(f.getAbsolutePath());
		if (p.isPrefixOf(absolute)) {
			return makeRelativeTo(p, absolute);
		}

		return null;
	}

	/**
	 * Tries to identify the project that contains the file with the given URI
	 * and returns a workspace relative path or null if the project cannot be
	 * identified or the file does not exist.
	 * 
	 * @param uri
	 * @return
	 */
	public static IPath extractWorkspaceRelativePath(URI uri) {
		if (uri == null) {
			return null;
		}
		File f = new File(uri);
		if (f.exists()) {
			// try to match project paths against prefixes of the absolute path
			IProject[] projects = ResourcesPlugin.getWorkspace().getRoot()
			.getProjects();
			for (IProject project : projects) {
				IPath p = project.getLocation();
				p = p.removeLastSegments(1);

				IPath absolute = new Path(f.getAbsolutePath());
				if (p.isPrefixOf(absolute)) {
					return makeRelativeTo(p, absolute);
				}
			}
		}
		return null;
	}

	/**
	 * Method for fixing paths that are supposed to be relative to the
	 * installation directory by default (like the workspace or the
	 * "applications" directory). This is necessary if eclipse is not started
	 * from within the installation directory.
	 * 
	 * @param relative
	 * @return
	 */
	public static IPath fixInstallationRelativePath(IPath relative) {

		if (relative.isAbsolute()) {
			return relative; // this is not a relative path..
		}
		File installArea;
		try {
			installArea = new File(new URI(URIUtil.encode(
					System.getProperty("osgi.install.area"),
					org.apache.commons.httpclient.URI.allowed_fragment)));
			IPath installationPath = new Path(installArea.getAbsolutePath());
			if (installationPath.append(relative).toFile().exists()) {
				// file seems to be there as expected
				return installationPath.append(relative);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return relative; // couldn't find the file..
	}

	public static IFile getTempFile(IPath relative, IProgressMonitor progress)
	throws Exception {
		ProgressUtils.getNonNullMonitorFor(progress);
		IProject tempProject = null;
		IFile result = null;
		tempProject = getTempProject();
		if (tempProject == null) {
			throw new IOException(
			"Unable to find or create project for temporary UNICORE files");
		}
		result = tempProject.getFile(relative);
		return result;
	}

	public static IProject getTempProject() throws Exception {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IProject project = root.getProject("UNICORE temporary Files");
		if (!project.exists()) {
			final IProjectDescription description = workspace
			.newProjectDescription(project.getName());

			CreateProjectOperation op = new CreateProjectOperation(description,
					"New Project");

			op.execute(null, null);
		}
		return project;
	}

	public static IPath makeRelativeTo(IPath base, IPath absolute) {
		// can't make relative if devices are not equal
		if (absolute.getDevice() != base.getDevice()
				&& (absolute.getDevice() == null || !absolute.getDevice()
						.equalsIgnoreCase(base.getDevice()))) {
			return absolute;
		}
		int commonLength = absolute.matchingFirstSegments(base);
		final int differenceLength = base.segmentCount() - commonLength;
		if (differenceLength > 0) {
			return absolute;
		}
		final int newSegmentLength = differenceLength + absolute.segmentCount()
		- commonLength;
		if (newSegmentLength == 0) {
			return Path.EMPTY;
		}
		String[] newSegments = new String[newSegmentLength];

		// add parent references for each segment different from the base
		Arrays.fill(newSegments, 0, differenceLength, ".."); //$NON-NLS-1$
		// append the segments of this path not in common with the base
		System.arraycopy(absolute.segments(), commonLength, newSegments,
				differenceLength, newSegmentLength - differenceLength);
		IPath result = new Path(null, newSegments[0]);
		for (int i = 1; i < newSegments.length; i++) {
			result = result.append(newSegments[i]);
		}
		return result;
	}

	public static IEditorPart openEditor(IWorkbenchPage page, IPath fileAddress)
	throws PartInitException, FileNotFoundException {
		File fileToOpen = fileAddress.toFile();

		if (fileToOpen.exists() && fileToOpen.isFile()) {
			IFileStore fileStore = EFS.getLocalFileSystem().getStore(
					fileAddress);
			return IDE.openEditorOnFileStore(page, fileStore);
		} else {
			throw new FileNotFoundException("Unable to open the file at"
					+ fileAddress);
		}
	}

	public static String[] parseMultiplePathString(String stringList) {
		StringTokenizer st = new StringTokenizer(stringList, File.pathSeparator
				+ "\n\r");//$NON-NLS-1$
		List<Object> v = new ArrayList<Object>();
		while (st.hasMoreElements()) {
			v.add(st.nextElement());
		}
		return (String[]) v.toArray(new String[v.size()]);
	}

	public static File pathToFile(IPath path, IProject parentProject) {
		return new File(parentProject.getFile(path).getLocationURI());
	}

	/**
	 * Try to refresh the folder at the given path This is a best effort
	 * service. If the folder can't be found or refreshing fails, this method
	 * won't throw an exception.
	 * 
	 * @param path
	 * @param depth
	 * @param monitor
	 */
	public static void refreshEclipseFolder(IPath path, int depth,
			IProgressMonitor monitor) {
		try {
			monitor = ProgressUtils.getNonNullMonitorFor(monitor);
			IPath rootPath = ResourcesPlugin.getWorkspace().getRoot()
			.getLocation();
			if (rootPath.isPrefixOf(path)) {
				IPath relative = makeRelativeTo(rootPath, path);
				if (relative.segmentCount() == 1) {
					// should be a project
					IProject project = ResourcesPlugin.getWorkspace().getRoot()
					.getProject(relative.lastSegment());
					if (project != null) {
						project.refreshLocal(depth, monitor);
					}
				} else {
					IFolder folder = ResourcesPlugin.getWorkspace().getRoot()
					.getFolder(relative);
					if (folder != null) {
						folder.refreshLocal(depth, monitor);
					}
				}
			} else {
				// file is contained in a linked project
				IProject[] projects = ResourcesPlugin.getWorkspace().getRoot()
				.getProjects();
				for (IProject project : projects) {
					if (project.getLocation().equals(path)) {
						project.refreshLocal(depth, monitor);
						break;
					} else if (project.getLocation().isPrefixOf(path)) {
						IPath relative = makeRelativeTo(project.getLocation(),
								path);
						IFolder folder = project.getFolder(relative);
						if (folder != null) {
							folder.refreshLocal(depth, monitor);
						}
					}
				}
			}
		} catch (Throwable t) {
			// ignore
		}
	}
}
