package de.fzj.unicore.rcp.common.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;

/**
 * This class represents a UNICORE preference page that is contributed to the
 * Preferences dialog.
 * 
 * @author Bastian Demuth, Valentina Huber
 * @version $Id: CommonPreferencePage.java,v 1.1 2007/10/02 09:01:10 vhuber Exp
 *          $
 */

public class CommonPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	Group proxyGroup;

	Composite proxyHostParent;
	StringFieldEditor proxyHostEditor;
	Composite proxyPortParent;
	IntegerFieldEditor proxyPortEditor;
	Composite proxyUserParent;
	StringFieldEditor proxyUserNameEditor;
	Composite proxyPasswdParent;
	StringFieldEditor proxyPasswdEditor;
	Composite nonProxyHostsParent;
	StringFieldEditor nonProxyHostsEditor;

	public CommonPreferencePage() {
		super(GRID);
		setPreferenceStore(UnicoreCommonActivator.getDefault()
				.getPreferenceStore());
		setDescription("Preferences for the UNICORE rich client framework.");
	}

	@Override
	protected Control createContents(Composite parent) {
		Control result = super.createContents(parent);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		proxyGroup.setLayoutData(gd);
		gd.horizontalSpan = ((GridLayout) getFieldEditorParent().getLayout()).numColumns;

		proxyHostEditor.getLabelControl(proxyHostParent).setLayoutData(
				new GridData(GridData.BEGINNING));
		proxyHostEditor.getTextControl(proxyHostParent).setLayoutData(
				new GridData(GridData.FILL_HORIZONTAL));

		proxyPortEditor.getLabelControl(proxyPortParent).setLayoutData(
				new GridData(GridData.BEGINNING));
		proxyPortEditor.getTextControl(proxyPortParent).setLayoutData(
				new GridData(GridData.FILL_HORIZONTAL));

		proxyUserNameEditor.getLabelControl(proxyUserParent).setLayoutData(
				new GridData(GridData.BEGINNING));
		proxyUserNameEditor.getTextControl(proxyUserParent).setLayoutData(
				new GridData(GridData.FILL_HORIZONTAL));

		proxyPasswdEditor.getLabelControl(proxyPasswdParent).setLayoutData(
				new GridData(GridData.BEGINNING));
		proxyPasswdEditor.getTextControl(proxyPasswdParent).setLayoutData(
				new GridData(GridData.FILL_HORIZONTAL));
		applyDialogFont(parent);
		// nonProxyHostsEditor.getLabelControl(nonProxyHostsParent).setLayoutData(new
		// GridData(GridData.BEGINNING));
		// nonProxyHostsEditor.getTextControl(nonProxyHostsParent).setLayoutData(new
		// GridData(GridData.FILL_HORIZONTAL));
		return result;
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		// grid detail level
		addField(new RadioGroupFieldEditor(Constants.P_GRID_DETAIL_LEVEL,
				"&User expertise level:",
				UnicoreCommonActivator.GRID_DETAIL_LEVELS.length,
				UnicoreCommonActivator.GRID_DETAIL_LEVELS,
				getFieldEditorParent()));

		ComboFieldEditor fileTransferEditor = new ComboFieldEditor(
				Constants.P_DEFAULT_FILE_TRANSFER_PROTOCOL,
				"Default &File transfer protocol:",
				Constants.KNOWN_TRANSFER_PROTOCOLS, getFieldEditorParent());
		addField(fileTransferEditor);

		IntegerFieldEditor uftpStreamsEditor = new IntegerFieldEditor(
				Constants.P_UFTP_STREAM_NUMBER,
				"&Number of parallel streams for UFTP transfers:",
				getFieldEditorParent());
		addField(uftpStreamsEditor);

		StringFieldEditor uftpClientHostEditor = new StringFieldEditor(
				Constants.P_UFTP_CLIENT_HOST,
				"Public name of client host for UFTP transfers:",
				getFieldEditorParent());
		addField(uftpClientHostEditor);

		BooleanFieldEditor uftpEnableEncryptionEditor = new BooleanFieldEditor(
				Constants.P_UFTP_ENABLE_ENCRYPTION,
				"Use encryption during UFTP transfers",
				getFieldEditorParent());
		addField(uftpEnableEncryptionEditor);

		BooleanFieldEditor uftpEnableCompressionEditor = new BooleanFieldEditor(
				Constants.P_UFTP_ENABLE_COMPRESSION,
				"Use compression during UFTP transfers",
				getFieldEditorParent());
		addField(uftpEnableCompressionEditor);

		IntegerFieldEditor connectionTimeoutEditor = new IntegerFieldEditor(
				Constants.P_CONNECTION_TIMEOUT,
				"Connection &timeout for service calls (ms):",
				getFieldEditorParent());
		addField(connectionTimeoutEditor);
			
		proxyGroup = new Group(getFieldEditorParent(), SWT.NONE);
		proxyGroup.setText("HTTP Proxy settings");
		proxyGroup.setLayout(new GridLayout(2, false));

		proxyHostParent = new Composite(proxyGroup, SWT.NONE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		proxyHostParent.setLayoutData(gd);
		proxyHostEditor = new StringFieldEditor(Constants.P_PROXY_HOST,
				"Host:", proxyHostParent);
		addField(proxyHostEditor);

		proxyPortParent = new Composite(proxyGroup, SWT.NONE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		proxyPortParent.setLayoutData(gd);
		proxyPortEditor = new IntegerFieldEditor(Constants.P_PROXY_PORT,
				"Port:", proxyPortParent);
		addField(proxyPortEditor);

		proxyUserParent = new Composite(proxyGroup, SWT.NONE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		proxyUserParent.setLayoutData(gd);
		proxyUserNameEditor = new StringFieldEditor(Constants.P_PROXY_USERNAME,
				"Username:", proxyUserParent);
		addField(proxyUserNameEditor);

		proxyPasswdParent = new Composite(proxyGroup, SWT.NONE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		proxyPasswdParent.setLayoutData(gd);
		proxyPasswdEditor = new StringFieldEditor(Constants.P_PROXY_PASSWD,
				"Password:", proxyPasswdParent);
		proxyPasswdEditor.getTextControl(proxyPasswdParent).setEchoChar('*');
		addField(proxyPasswdEditor);
		//
		// nonProxyHostsParent = new Composite(proxyGroup,SWT.NONE);
		// gd = new GridData(GridData.FILL_HORIZONTAL);
		// gd.horizontalSpan = 2;
		// nonProxyHostsParent.setLayoutData(gd);
		// nonProxyHostsEditor = new
		// StringFieldEditor(Constants.P_PROXY_NON_HOSTS,"No Proxy for:",nonProxyHostsParent);
		// addField(nonProxyHostsEditor);
	}

	public void init(IWorkbench workbench) {
		// TODO Auto-generated method stub

	}

}
