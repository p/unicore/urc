package de.fzj.unicore.rcp.common.utils;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

public class BackgroundWorker extends BackgroundJob {

	boolean runAgain = false;
	int delay = 0;
	Queue<Runnable> work = new ConcurrentLinkedQueue<Runnable>();
	
	public BackgroundWorker(String name) {
		this(name,true,100);
	}
	
	public BackgroundWorker(String name, boolean system, int delay) {
		super("");
		setSystem(system);
		this.delay = delay;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		runAgain = false;
		while(!work.isEmpty())
		{
			Runnable r = work.poll();
			if(r != null) r.run();
		}
		if(runAgain) schedule(delay);
		return Status.OK_STATUS;
	}
	
	public void scheduleWork(Runnable r)
	{
		work.add(r);
		if (getState() == org.eclipse.core.runtime.jobs.Job.NONE) {
			schedule(delay);
		} else {
			runAgain = true;
		}
	}

}
