package de.fzj.unicore.rcp.common.utils;

public interface IValidityChangeListener {

	/**
	 * Called when the validity of a GUI component or model object has changed
	 * (e.g. when the user enters an invalid value).
	 * 
	 * @param e
	 */
	public void validityChanged(ValidityChangeEvent e);

}
