package de.fzj.unicore.rcp.common.utils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.jar.JarFile;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

/**
 * This classloader will first look at the provided classloader and then iterate
 * over the extensions for the given extension point ID in order to find a given
 * class there. This is useful in order to simulate Buddy classloading without
 * causing deadlocks (Eclipse's registered Buddy classloading causes deadlocks).
 * 
 * @author bdemuth
 * 
 */
public class DelegatingExtensionURLClassloader extends URLClassLoader {

	private String[] extensionPointIds;
	private Bundle[] extendingPlugins;

	protected HashSet<String> setJarFileNames2Close = new HashSet<String>();

	public DelegatingExtensionURLClassloader(URL[] urls, ClassLoader parent,
			String... extensionPointIds) {
		super(urls, parent);
		this.extensionPointIds = extensionPointIds;
		// initialize yourself and start all bundles in order to get ready
		// doing this lazily causes problems with Jaxb deserialization
		// as the bundles did not have a chance to add their packages to the
		// context
		// in time
		getExtendingPlugins();
	}

	public DelegatingExtensionURLClassloader(URL[] urls, String extensionPointId) {
		this(urls, null, new String[] { extensionPointId });
	}

	@Override
	public Class<?> findClass(String name) throws ClassNotFoundException {
		try {
			return super.findClass(name);
		} catch (ClassNotFoundException e) {
			for (Bundle bundle : getExtendingPlugins()) {
				try {
					return bundle.loadClass(name);
				} catch (Throwable t) {
					// do nothing
				}
			}
		}
		throw new ClassNotFoundException(
				"The class "
				+ name
				+ " could neither be found by this plugin's class loader nor in any of the extending plugins.");
	}

	private Bundle[] getExtendingPlugins() {
		if (extendingPlugins == null) {
			loadExtensions();
		}
		return extendingPlugins;
	}


	@Override
	public URL getResource(String name) {
		URL result = super.getResource(name);
		if (result == null) {
			for (Bundle bundle : getExtendingPlugins()) {
				result = bundle.getResource(name);
				if (result != null) {
					return result;
				}
			}
		}
		return result;
	}

	private void loadExtensions() {
		Set<Bundle> result = new HashSet<Bundle>();
		for (String extensionPointId : extensionPointIds) {
			// iterate over extensions and find extending plugins whose
			// classloaders
			// should be queried, too
			IExtensionRegistry registry = Platform.getExtensionRegistry();
			IExtensionPoint extensionPoint = registry
			.getExtensionPoint(extensionPointId);
			IConfigurationElement[] members = extensionPoint
			.getConfigurationElements();

			// For each extension
			for (int m = 0; m < members.length; m++) {
				IConfigurationElement member = members[m];
				Bundle b = null;
				String id = member.getDeclaringExtension().getContributor()
				.getName();
				if (id != null) {
					b = Platform.getBundle(id);
				}
				if (b != null) {
					result.add(b);
					try {
						// activate bundle by trying to load a class
						b.loadClass("blaaaaaaa");
					} catch (Exception e) {
						// do nothing
					}
				}
			}
		}

		extendingPlugins = result.toArray(new Bundle[result.size()]);
	}


	public void close() {
		setJarFileNames2Close.clear();
		closeClassLoader(this);
		finalizeNativeLibs(this);
		cleanupJarFileFactory();
	}

	/**
	 * cleanup jar file factory cache
	 */
	@SuppressWarnings({ "nls", "rawtypes"})
	public boolean cleanupJarFileFactory()
	{
		boolean res = false;
		Class<?> classJarURLConnection = null;
		try {
			classJarURLConnection = classForName("sun.net.www.protocol.jar.JarURLConnection");
		} catch (ClassNotFoundException e) {
			//ignore
		}
		if (classJarURLConnection == null) {
			return res;
		}
		Field f = null;
		try {
			f = classJarURLConnection.getDeclaredField("factory");
		} catch (NoSuchFieldException e) {
			//ignore
		}
		if (f == null) {
			return res;
		}
		f.setAccessible(true);
		Object obj = null;
		try {
			obj = f.get(null);
		} catch (IllegalAccessException e) {
			//ignore
		}
		if (obj == null) {
			return res;
		}
		Class<?> classJarFileFactory = obj.getClass();
		//
		HashMap fileCache = null;
		try {
			f = classJarFileFactory.getDeclaredField("fileCache");
			f.setAccessible(true);
			obj = f.get(null);
			if (obj instanceof HashMap) {
				fileCache = (HashMap)obj;
			}
		} catch (NoSuchFieldException e) {
		} catch (IllegalAccessException e) {
			//ignore
		}
		HashMap urlCache = null;
		try {
			f = classJarFileFactory.getDeclaredField("urlCache");
			f.setAccessible(true);
			obj = f.get(null);
			if (obj instanceof HashMap) {
				urlCache = (HashMap)obj;
			}
		} catch (NoSuchFieldException e) {
		} catch (IllegalAccessException e) {
			//ignore
		}
		if (urlCache != null) {
			HashMap urlCacheTmp = (HashMap)urlCache.clone();
			Iterator it = urlCacheTmp.keySet().iterator();
			while (it.hasNext()) {
				obj = it.next();
				if (!(obj instanceof JarFile)) {
					continue;
				}
				JarFile jarFile = (JarFile)obj;
				if (setJarFileNames2Close.contains(jarFile.getName())) {
					try {
						jarFile.close();
					} catch (IOException e) {
						//ignore
					}
					if (fileCache != null) {
						fileCache.remove(urlCache.get(jarFile));
					}
					urlCache.remove(jarFile);
				}
			}
			res = true;
		} else if (fileCache != null) {
			// urlCache := null
			HashMap fileCacheTmp = (HashMap)fileCache.clone();
			Iterator it = fileCacheTmp.keySet().iterator();
			while (it.hasNext()) {
				Object key = it.next();
				obj = fileCache.get(key);
				if (!(obj instanceof JarFile)) {
					continue;
				}
				JarFile jarFile = (JarFile)obj;
				if (setJarFileNames2Close.contains(jarFile.getName())) {
					try {
						jarFile.close();
					} catch (IOException e) {
						//ignore
					}
					fileCache.remove(key);
				}
			}
			res = true;
		}
		setJarFileNames2Close.clear();
		return res;
	}

	/**
	 * close jar files of cl
	 * @param cl
	 * @return
	 */
	@SuppressWarnings( { "nls" })
	public boolean closeClassLoader(ClassLoader cl) {
		boolean res = false;
		if (cl == null) {
			return res;
		}
		Class<?> classURLClassLoader = URLClassLoader.class;
		Field f = null;
		try {
			f = classURLClassLoader.getDeclaredField("ucp");
		} catch (NoSuchFieldException e1) {
			//ignore
		}
		if (f != null) {
			f.setAccessible(true);
			Object obj = null;
			try {
				obj = f.get(cl);
			} catch (IllegalAccessException e1) {
				//ignore
			}
			if (obj != null) {
				final Object ucp = obj;
				f = null;
				try {
					f = ucp.getClass().getDeclaredField("loaders");
				} catch (NoSuchFieldException e1) {
					//ignore
				}
				if (f != null) {
					f.setAccessible(true);
					List<?> loaders = null;
					try {
						loaders = (List<?>) f.get(ucp);
						res = true;
					} catch (IllegalAccessException e1) {
						//ignore
					}
					for (int i = 0; loaders != null && i < loaders.size(); i++) {
						obj = loaders.get(i);
						f = null;
						try {
							f = obj.getClass().getDeclaredField("jar");
						} catch (NoSuchFieldException e) {
							//ignore
						}
						if (f != null) {
							f.setAccessible(true);
							try {
								obj = f.get(obj);
							} catch (IllegalAccessException e1) {
								// ignore
							}
							if (obj instanceof JarFile) {
								final JarFile jarFile = (JarFile)obj;
								setJarFileNames2Close.add(jarFile.getName());
								//try {
								//	jarFile.getManifest().clear();
								//} catch (IOException e) {
								//	// ignore
								//}
								try {
									jarFile.close();
								} catch (IOException e) {
									// ignore
								}
							}
						}
					}
				}
			}
		}
		return res;
	}

	/**
	 * finalize native libraries
	 * @param cl
	 * @return
	 */
	@SuppressWarnings({ "nls" })
	public boolean finalizeNativeLibs(ClassLoader cl) {
		boolean res = false;
		Class<?> classClassLoader = ClassLoader.class;
		java.lang.reflect.Field nativeLibraries = null;
		try {
			nativeLibraries = classClassLoader.getDeclaredField("nativeLibraries");
		} catch (NoSuchFieldException e1) {
			//ignore
		}
		if (nativeLibraries == null) {
			return res;
		}
		nativeLibraries.setAccessible(true);
		Object obj = null;
		try {
			obj = nativeLibraries.get(cl);
		} catch (IllegalAccessException e1) {
			//ignore
		}
		if (!(obj instanceof Vector)) {
			return res;
		}
		res = true;
		Vector<?> java_lang_ClassLoader_NativeLibrary = (Vector<?>)obj;
		for (Object lib : java_lang_ClassLoader_NativeLibrary) {
			java.lang.reflect.Method finalize = null;
			try {
				finalize = lib.getClass().getDeclaredMethod("finalize", new Class[0]);
			} catch (NoSuchMethodException e) {
				//ignore
			}
			if (finalize != null) {
				finalize.setAccessible(true);
				try {
					finalize.invoke(lib, new Object[0]);
				} catch (IllegalAccessException e) {
				} catch (InvocationTargetException e) {
					//ignore
				}
			}
		}
		return res;
	}

	private static Class<?> classForName(String name) throws ClassNotFoundException {
		try {
			ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
			if ( contextClassLoader != null ) {
				return contextClassLoader.loadClass(name);
			}
		}
		catch ( Throwable t ) {
		}
		return Class.forName( name );
	}
}
