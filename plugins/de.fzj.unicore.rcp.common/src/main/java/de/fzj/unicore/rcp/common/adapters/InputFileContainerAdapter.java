package de.fzj.unicore.rcp.common.adapters;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;

/**
 * Adapter for obtaining information about a location where local input files
 * for jobs and workflows should be stored. Can be returned by
 * {@link IAdaptable} classes in order to convey such information.
 * 
 * @author bdemuth
 * 
 */
public class InputFileContainerAdapter {

	private IPath path;

	public InputFileContainerAdapter(IPath path) {
		super();
		this.path = path;
	}

	public IPath getPath() {
		return path;
	}

	public void setPath(IPath path) {
		this.path = path;
	}

}
