package de.fzj.unicore.rcp.common.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = UnicoreCommonActivator.getDefault()
		.getPreferenceStore();
		store.setDefault(Constants.P_BOOLEAN, true);
		store.setDefault(Constants.P_CHOICE, "choice2");
		store.setDefault(Constants.P_STRING, "Default value");
		store.setDefault(Constants.P_GRID_DETAIL_LEVEL,
				UnicoreCommonActivator.LEVEL_BEGINNER);
		store.setDefault(Constants.P_CONNECTION_TIMEOUT, 20000);
		store.setDefault(Constants.P_PROXY_PORT, 80);
		// store.setDefault(Constants.P_PROXY_NON_HOSTS, "localhost 127.0.0.1"
		// );
		store.setDefault(Constants.P_DEFAULT_FILE_TRANSFER_PROTOCOL,
				Constants.FILE_TRANSFER_PROTOCOL_BFT[1]);
		store.setDefault(Constants.P_UFTP_STREAM_NUMBER,1);

		String defaultUftpClientHost = ""; //getFullyQualifiedHostName();

		store.setDefault(Constants.P_UFTP_CLIENT_HOST, defaultUftpClientHost);
		store.setDefault(Constants.P_UFTP_ENABLE_ENCRYPTION, false);
		store.setDefault(Constants.P_UFTP_ENABLE_COMPRESSION, false);
	}

}
