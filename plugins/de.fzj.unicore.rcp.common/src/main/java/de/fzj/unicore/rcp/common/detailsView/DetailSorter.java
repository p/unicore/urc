/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.common.detailsView;

import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;

import org.eclipse.jface.viewers.ContentViewer;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;

public class DetailSorter extends ViewerSorter {
	// Criteria that the instance uses
	private int criteria;
	/**
	 * Constructor argument values that indicate to sort items
	 */
	public final static int UNSORT = 0;
	public final static int KEY = 1;
	public final static int VALUE = 2;

	/**
	 * Creates a new viewer sorter, which uses the given collator to sort
	 * strings.
	 * 
	 * @param collator
	 *            the collator to use to sort strings
	 */
	public DetailSorter(Collator collator) {
		super(collator);
		this.criteria = 0; // sort by xlogin by default
	}

	/**
	 * Creates a new viewer sorter, which uses the default collator to sort
	 * strings.
	 */
	public DetailSorter(int criteria) {
		this(Collator.getInstance());
		this.criteria = criteria;
	}

	/**
	 * Returns the category of the given element. The category is a number used
	 * to allocate elements to bins; the bins are arranged in ascending numeric
	 * order. The elements within a bin are arranged via a second level sort
	 * criterion.
	 * <p>
	 * The default implementation of this framework method returns
	 * <code>0</code>. Subclasses may reimplement this method to provide
	 * non-trivial categorization.
	 * </p>
	 * 
	 * @param element
	 *            the element
	 * @return the category
	 */
	@Override
	public int category(Object element) {
		return 0;
	}

	/*
	 * (non-Javadoc) Method declared on ViewerSorter.
	 */
	@Override
	public int compare(Viewer viewer, Object o1, Object o2) {

		KeyValue kv1 = (KeyValue) o1;
		KeyValue kv2 = (KeyValue) o2;

		switch (criteria) {

		case UNSORT:
			return 0;
		case KEY:
			return compareString(kv1.getKey(), kv2.getKey());
		case VALUE:
			return compareString(kv1.getValue(), kv2.getValue());
		default:
			return 0;
		}
	}

	/**
	 * Returns a negative, zero, or positive number depending on whether the
	 * first element is less than, equal to, or greater than the second element.
	 * <p>
	 * The default implementation of this method is based on comparing the
	 * elements' categories as computed by the <code>category</code> framework
	 * method. Elements within the same category are further subjected to a case
	 * insensitive compare of their label strings, either as computed by the
	 * content viewer's label provider, or their <code>toString</code> values in
	 * other cases. Subclasses may override.
	 * </p>
	 * 
	 * @param viewer
	 *            the viewer
	 * @param e1
	 *            the first element
	 * @param e2
	 *            the second element
	 * @return a negative number if the first element is less than the second
	 *         element; the value <code>0</code> if the first element is equal
	 *         to the second element; and a positive number if the first element
	 *         is greater than the second element
	 */
	public int compareOLD(Viewer viewer, Object e1, Object e2) {

		int cat1 = category(e1);
		int cat2 = category(e2);

		if (cat1 != cat2) {
			return cat1 - cat2;
		}

		// cat1 == cat2

		String name1;
		String name2;

		if (viewer == null || !(viewer instanceof ContentViewer)) {
			name1 = e1.toString();
			name2 = e2.toString();
		} else {
			IBaseLabelProvider prov = ((ContentViewer) viewer)
					.getLabelProvider();
			if (prov instanceof DetailLabelProvider) {
				DetailLabelProvider lprov = (DetailLabelProvider) prov;
				name1 = lprov.getText(e1);
				name2 = lprov.getText(e2);
			} else {
				name1 = e1.toString();
				name2 = e2.toString();
			}
		}
		if (name1 == null) {
			name1 = "";//$NON-NLS-1$
		}
		if (name2 == null) {
			name2 = "";//$NON-NLS-1$
		}
		return collator.compare(name1, name2);
	}

	private int compareString(String kv1String, String kv2String) {
		return collator.compare(kv1String, kv2String);
	}

	/**
	 * Returns the collator used to sort strings.
	 * 
	 * @return the collator used to sort strings
	 */
	@Override
	public Collator getCollator() {
		return collator;
	}

	/**
	 * Returns whether this viewer sorter would be affected by a change to the
	 * given property of the given element.
	 * 
	 * @param element
	 *            the element
	 * @param property
	 *            the property
	 * @return <code>true</code> if the sorting would be affected, and
	 *         <code>false</code> if it would be unaffected
	 */
	@Override
	public boolean isSorterProperty(Object element, String property) {
		return false;
	}

	/**
	 * Sorts the given elements in-place, modifying the given array.
	 * <p>
	 * The default implementation of this method uses the java.util.Arrays#sort
	 * algorithm on the given array, calling <code>compare</code> to compare
	 * elements.
	 * </p>
	 * 
	 * @param viewer
	 *            the viewer
	 * @param elements
	 *            the elements to sort
	 */
	@Override
	public void sort(final Viewer viewer, Object[] elements) {
		Arrays.sort(elements, new Comparator<Object>() {
			public int compare(Object a, Object b) {
				return DetailSorter.this.compare(viewer, a, b);
			}
		});
	}
}
