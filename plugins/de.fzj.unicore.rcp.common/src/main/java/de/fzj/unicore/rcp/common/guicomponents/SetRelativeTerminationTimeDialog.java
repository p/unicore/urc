/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.common.guicomponents;

import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.common.UnicoreCommonActivator;

/**
 * @author Valentina Huber
 * @version $Id: SetTerminationTimeDialog.java, v. 1.0 Nov 5, 2007 3:20:07 PM
 *          v.huber Exp $
 */
public class SetRelativeTerminationTimeDialog extends Dialog {

	class NullOrIntegerValidator implements IInputValidator {
		private final Pattern INTEGER_PATTERN = Pattern
				.compile("[\\s]*[0-9]*[\\s]*");

		public String isValid(String newText) {
			if (newText != null
					&& !INTEGER_PATTERN.matcher(newText.trim()).matches()) {
				return "'" + newText + "' is not a valid integer.";
			}
			return null;
		}

	}

	String title, message;

	/** Ok button widget. */
	private Button okButton;

	/** Error message string. */
	private String errorMessage;

	/** Error message label widget. */
	private Text errorMessageText;

	private Text daysText, hoursText;
	private int initialTime = -1;

	private int terminationTime;

	IInputValidator integerValidator = new NullOrIntegerValidator();

	private Image termTimeImg = UnicoreCommonActivator.getImageDescriptor(
			"clock_edit.png").createImage();

	/**
	 * Creates an input dialog with OK and Cancel buttons. Note that the dialog
	 * will have no visual representation (no widgets) until it is told to open.
	 * <p>
	 * Note that the <code>open</code> method blocks for input dialogs.
	 * </p>
	 * 
	 * @param parentShell
	 *            the parent shell, or <code>null</code> to create a top-level
	 *            shell
	 * @param dialogTitle
	 *            the dialog title, or <code>null</code> if none
	 * @param dialogMessage
	 *            the dialog message, or <code>null</code> if none
	 * @param initialValue
	 *            the initial input value, or <code>null</code> if none
	 *            (equivalent to the empty string)
	 */
	public SetRelativeTerminationTimeDialog(Shell parentShell,
			String dialogTitle, String message, Integer initialValue) {

		super(parentShell);
		setShellStyle(SWT.DIALOG_TRIM | SWT.RESIZE | SWT.MAX
				| SWT.APPLICATION_MODAL);
		title = dialogTitle;
		this.message = message;
		initialTime = initialValue.intValue();
	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			validateInput();
			if (errorMessage == null || errorMessage.trim().length() == 0) {
				terminationTime = Integer.parseInt(daysText.getText().trim())
						* 24 + Integer.parseInt(hoursText.getText().trim());
				if (terminationTime < 1) {
					setErrorMessage("Termination time should be >= 1 hours");
					return;
				}
			} else {
				return;
			}
		} else {
			terminationTime = -1;
		}
		super.buttonPressed(buttonId);
	}

	@Override
	public boolean close() {
		boolean b = super.close();
		termTimeImg.dispose();
		termTimeImg = null;
		return b;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets
	 * .Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		if (title != null) {
			shell.setText(title);
		}
		shell.setImage(termTimeImg);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse
	 * .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons by default
		okButton = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		// create composite

		Composite composite = (Composite) super.createDialogArea(parent);
		applyDialogFont(composite);
		GridLayout gridLayout = (GridLayout) composite.getLayout();
		gridLayout.numColumns = 5;

		Label messageLabel = new Label(composite, SWT.RIGHT);
		messageLabel.setText(message);
		GridData messageData = new GridData();
		messageLabel.setLayoutData(messageData);

		daysText = new Text(composite, SWT.SINGLE | SWT.BORDER);
		GridData textData = new GridData(GridData.GRAB_HORIZONTAL
				| GridData.FILL_HORIZONTAL);
		textData.widthHint = 100;
		daysText.setLayoutData(textData);

		Label daysLabel = new Label(composite, SWT.LEFT);
		GridData lbData = new GridData();
		daysLabel.setText("days"); //$NON-NLS-1$
		daysLabel.setLayoutData(lbData);

		hoursText = new Text(composite, SWT.SINGLE | SWT.BORDER);
		hoursText.setLayoutData(textData);

		Label hoursLabel = new Label(composite, SWT.LEFT);
		hoursLabel.setText("hours"); //$NON-NLS-1$
		hoursLabel.setLayoutData(lbData);

		if (initialTime != -1) {
			daysText.setText(String.valueOf(initialTime / 24));
			hoursText.setText(String.valueOf(initialTime % 24));
		}

		GridData errData = new GridData(GridData.FILL_HORIZONTAL);
		errData.horizontalSpan = 5;
		errorMessageText = new Text(composite, SWT.READ_ONLY);
		errorMessageText.setBackground(errorMessageText.getDisplay()
				.getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
		errorMessageText.setForeground(errorMessageText.getDisplay()
				.getSystemColor(SWT.COLOR_RED));
		errorMessageText.setLayoutData(errData);

		// Set the error message text
		// See https://bugs.eclipse.org/bugs/show_bug.cgi?id=66292
		setErrorMessage(errorMessage);

		applyDialogFont(composite);

		return composite;
	}

	/**
	 * Returns the ok button.
	 * 
	 * @return the ok button
	 */
	protected Button getOkButton() {
		return okButton;
	}

	/**
	 * Gets the termination time (in hours)
	 * 
	 * @return The termination time (in hours)
	 */
	public int getTerminationTime() {
		return terminationTime;
	}

	/**
	 * Sets or clears the error message.
	 * 
	 * @param errorMessage
	 *            the error message, or <code>null</code> to clear
	 * @since 3.0
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
		if (errorMessageText != null && !errorMessageText.isDisposed()) {
			errorMessageText.setText(errorMessage == null ? "" : errorMessage); //$NON-NLS-1$
			errorMessageText.getParent().update();
		}
	}

	/**
	 * Validates the input.
	 * <p>
	 * The default implementation of this framework method delegates the request
	 * to the supplied input validator object; if it finds the input invalid,
	 * the error message is displayed in the dialog's message line. This hook
	 * method is called whenever the text changes in the input field.
	 * </p>
	 */
	protected void validateInput() {
		errorMessage = null;
		String daysString = daysText.getText().trim();
		String hoursString = hoursText.getText().trim();
		if (daysString.length() == 0 && hoursString.length() == 0) {
			errorMessage = "Please specify the termination time";
		} else {
			if (daysString.length() != 0) {
				errorMessage = integerValidator.isValid(daysString);
			}
			if (errorMessage == null && hoursString.length() != 0) {
				errorMessage = integerValidator.isValid(hoursString);
			}
		}
		// Bug 16256: important not to treat "" (blank error) the same as null
		// (no error)
		setErrorMessage(errorMessage);
	}

}
