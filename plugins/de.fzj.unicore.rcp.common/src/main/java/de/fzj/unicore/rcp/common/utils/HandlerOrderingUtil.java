package de.fzj.unicore.rcp.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This util class can be used to bring a set of handlers in correct order. The
 * order is determined by looking at the Handler sets returned by
 * {@link Handler#getBefore()} and {@link Handler#getAfter()}. This is a nice
 * mechanism to obtain a valid order just by specifying dependencies.
 * 
 * @author bdemuth
 * 
 */
public class HandlerOrderingUtil {

	private List<Handler> handlers = new ArrayList<Handler>();

	public List<Handler> getHandlers() {
		return handlers;
	}

	public void insertHandler(Handler handler) {
		if (handlers.size() == 0) {
			handlers.add(handler);
			return;
		}

		int begin = -1;
		int end = handlers.size();

		Set<String> before = handler.getBefore();
		Set<String> after = handler.getAfter();

		for (int i = 0; i < handlers.size(); i++) {
			Handler cmp = handlers.get(i);

			if (before.contains(cmp.getId())) {
				if (i < end) {
					end = i;
				}
			}

			if (cmp.getBefore().contains(handler.getId())) {
				if (i > begin) {
					begin = i;
				}
			}

			if (after.contains(cmp.getId())) {
				if (i > begin) {
					begin = i;
				}
			}

			if (cmp.getAfter().contains(handler.getId())) {
				if (i < end) {
					end = i;
				}
			}
		}

		if (end < begin + 1) {
			throw new IllegalStateException("Invalid ordering for handler "
					+ handler.getId());
		}

		handlers.add(begin + 1, handler);
	}

}
