/*******************************************************************************
 * Copyright (c) 2000, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package de.fzj.unicore.rcp.common.actions;

import org.eclipse.ui.IEditorPart;

import de.fzj.unicore.rcp.common.UnicoreCommonActivator;

/**
 * Manages the installation and removal of global actions for the same type of
 * editors.
 * <p>
 * If instantiated and used as-is, this contributor connects to all of the
 * workbench defined global editor actions the corresponding actions of the
 * current editor. Subclasses may override the following methods:
 * <ul>
 * <li><code>contributeToMenu</code> - extend to contribute to menu</li>
 * <li><code>contributeToToolBar</code> - reimplement to contribute to tool bar</li>
 * <li><code>contributeToStatusLine</code> - reimplement to contribute to status
 * line</li>
 * <li><code>setActiveEditor</code> - extend to react to editor changes</li>
 * </ul>
 * </p>
 * 
 * @see org.eclipse.ui.texteditor.ITextEditorActionConstants
 */
public class SubmittableEditorActionBarContributor extends
		FancyEditorActionBarContributor {

	/**
	 * Creates an empty editor action bar contributor. The action bars are
	 * furnished later via the <code>init</code> method.
	 * 
	 * @see org.eclipse.ui.IEditorActionBarContributor#init(org.eclipse.ui.IActionBars,
	 *      org.eclipse.ui.IWorkbenchPage)
	 */
	public SubmittableEditorActionBarContributor() {

	}

	@Override
	protected void buildEditorSpecificActions(IEditorPart editor) {
		// do nothing
		// subclasses should override to add actions that are only available for
		// certain editor instances
		// this is called after setActiveEditor is called
	}

	@Override
	protected void buildGlobalActions() {
		addGlobalAction(new DirectRetargetAction(
				UnicoreCommonActionConstants.ACTION_SUBMIT, "Submit",
				UnicoreCommonActivator.getImageDescriptor("run.gif")), true,
				true);
		addGlobalAction(
				new DirectRetargetAction(
						UnicoreCommonActionConstants.ACTION_FETCH_OUTCOMES,
						"Fetch output files",
						UnicoreCommonActivator
								.getImageDescriptor("fetchOutput.gif")), true,
				true);
		addGlobalAction(
				new DirectRetargetAction(
						UnicoreCommonActionConstants.ACTION_SET_TERMINATION_TIME,
						"Set Termination Time", UnicoreCommonActivator
								.getImageDescriptor("clock_edit.png")), true,
				true);
	}

}
