package de.fzj.unicore.rcp.common.utils.swing;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;

/**
 * Depending on the AWT implementation, calling
 * {@link SwingUtilities#invokeAndWait(Runnable)} from the SWT main thread can
 * cause deadlocks. This class tries to workaround this issue.
 * 
 * @author bdemuth
 * 
 */
public class SafeSwingUtilities {

	class SwingJob extends BackgroundJob {
		boolean finished = false;
		Runnable wrapped;
		InterruptedException e1;
		InvocationTargetException e2;

		public SwingJob(Runnable wrapped) {
			super("");
			this.wrapped = wrapped;
		}

		
		@Override
		protected IStatus run(IProgressMonitor monitor) {
			try {
				SwingUtilities.invokeAndWait(wrapped);
			} catch (InterruptedException e) {
				e1 = e;
			} catch (InvocationTargetException e) {
				e2 = e;
			} finally {
				finished = true;
			}
			return Status.OK_STATUS;
		}

	}

	public static void invokeAndWait(Runnable r) throws InterruptedException,
			InvocationTargetException {
		if (r == null) {
			return;
		}
		Display d = null;
		try {
			d = PlatformUI.getWorkbench().getDisplay();
		} catch (Throwable t) {
			// do nothing
		}
		if (d == null) {
			SwingUtilities.invokeAndWait(r);
		}
		if (d.isDisposed()) {
			return;
		}
		boolean isMainThread = Thread.currentThread().equals(d.getThread());
		if (!isMainThread) {
			// no problem here
			SwingUtilities.invokeAndWait(r);
		} else {
			SwingJob sj = new SafeSwingUtilities().new SwingJob(r);
			sj.schedule();
			while (!sj.finished) {
				boolean sleep = !d.readAndDispatch();
				if (sleep) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
					}
				}
			}
			if (sj.e1 != null) {
				throw sj.e1;
			}
			if (sj.e2 != null) {
				throw sj.e2;
			}
		}
	}

}
