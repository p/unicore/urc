package de.fzj.unicore.rcp.common;

import java.text.SimpleDateFormat;

import javax.xml.namespace.QName;

import de.fzj.unicore.rcp.logmonitor.utils.FileDialogUtils;

/**
 * The common constants.
 * 
 * @author Bastian Demuth, Valentina Huber
 * 
 */
public class Constants {

	/**
	 * The current version of the client. Can be used to check for necessary
	 * updates from older model objects.
	 */
	public static final String CURRENT_CLIENT_VERSION = "7.0.0";

	/** the keystore that is used by the client must be in one of these formats */
	public static final String[] REUSABLE_KEYSTORE_FORMATS = { "*.jks", "*.*",
			"*" };

	/** keystores in these formats can be imported */
	public static final String[] IMPORTABLE_KEYSTORE_FORMATS = { "*.jks",
			"*.jceks", "*.p12", "*.pem", "*.*", "*" };

	/** If a keystore is created, use this format */
	public static final String DEFAULT_KEYSTORE_FORMAT = ".jks";

	/** Store a certificate public key in this format */
	public static final String PUBLIC_KEY_FORMAT = ".pem";

	/** public key formats to be filtered in the filechooser */
	public static final String[] PUBLIC_KEY_FORMATS = { "*.pem", "*.cer",
			"*.crt", "*.der", "*.*", "*" };

	/** Formats for signing replies from CAs */
	public static final String[] CSR_REPLY_FORMAT = { "*.pem", "*.crt", "*.*",
			"*" };

	/** Store a certificate request in this format */
	public static final String CSR_FORMAT = ".csr";

	/** certificate request formats to be filtered in the filechooser */
	public static final String[] CSR_FORMATS = { "*.csr", "*.*", "*" };

	public static final String DEFAULT_DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
	
	public static final SimpleDateFormat getDefaultDateFormat() {
		return new SimpleDateFormat(DEFAULT_DATE_FORMAT_PATTERN);
	}

	/** Store jobs in this format */
	public static final String JOB_FORMAT = "job";

	/** public key formats to be filtered in the filechooser */
	public static final String[] JOB_FORMATS = { "*.job", "*.*", "*" };
	
	/**
	 * Constant that can be used in combination with {@link FileDialogUtils} to
	 * remember a directory for downloading files to.
	 */
	public static final String FILE_DIALOG_ID_DOWNLOAD_DIR = "de.fzj.unicore.rcp.common.downloaddir";
	
	/**
	 * Constant that can be used in combination with {@link FileDialogUtils} to
	 * remember a directory for uploading files from.
	 */
	public static final String FILE_DIALOG_ID_UPLOAD_DIR = "de.fzj.unicore.rcp.common.uploaddir";

	// Constant definitions for plug-in preferences
	public static final String PREFERENCE_PAGE_ID = "de.fzj.unicore.rcp.common.preferences.CommonPreferencePage";

	public static final String P_PATH = "pathPreference";

	public static final String P_BOOLEAN = "booleanPreference";

	public static final String P_CHOICE = "choicePreference";

	public static final String P_STRING = "stringPreference";

	public static final String P_GRID_DETAIL_LEVEL = "grid detail level";

	public static final String P_CONNECTION_TIMEOUT = "connection timeout";

	public static final String P_PROXY_HOST = "proxy host";

	public static final String P_PROXY_PORT = "proxy port";

	public static final String P_PROXY_USERNAME = "proxy username";

	public static final String P_PROXY_PASSWD = "proxy passwd";

	public static final String P_PROXY_NON_HOSTS = "proxy non hosts";

	public static final String P_DEFAULT_FILE_TRANSFER_PROTOCOL = "file transfer protocol";

	public static final String P_UFTP_STREAM_NUMBER = "uftp stream number";

	public static final String P_UFTP_CLIENT_HOST = "uftp client host";

	public static final String P_UFTP_ENABLE_ENCRYPTION = "uftp encryption";
	
	public static final String P_UFTP_ENABLE_COMPRESSION = "uftp compression";

	public static final String[] FILE_TRANSFER_PROTOCOL_RBYTEIO = {
			"OGSA Random Access ByteIO (RBYTEIO)", "RBYTEIO" };

	public static final String[] FILE_TRANSFER_PROTOCOL_SBYTEIO = {
			"OGSA Streamable ByteIO", "SBYTEIO" };

	public static final String[] FILE_TRANSFER_PROTOCOL_UDT = {
			"UDP based Data Transfer (UDT)", "UDT" };

	public static final String[] FILE_TRANSFER_PROTOCOL_BFT = {
			"Baseline file transfer (HTTP)", "BFT" };
	
	public static final String[] FILE_TRANSFER_PROTOCOL_UFTP = {
		"Parallel FTP streams (UFTP)", "UFTP" };
	
	public static final String[] FILE_TRANSFER_PROTOCOL_AUTO = {
		"Automatic", "U6" };

	public static final String[][] KNOWN_TRANSFER_PROTOCOLS = {
			FILE_TRANSFER_PROTOCOL_AUTO,
			FILE_TRANSFER_PROTOCOL_BFT, FILE_TRANSFER_PROTOCOL_UFTP, FILE_TRANSFER_PROTOCOL_RBYTEIO,
			FILE_TRANSFER_PROTOCOL_UDT };

	/** Used to store the path of the keystore */
	public static String P_KEYSTORE_PATH = "keystore_path";

	public static final int DEFAULT_TERMINATION_TIME = 720; // hours

	/**
	 * String that is used to separate iteration counters in nested workflow
	 * loops from each other.
	 */
	public static final String ITERATION_ID_SEPERATOR = ":::";

	public static final String CURRENT_TOTAL_ITERATOR = "${CURRENT_TOTAL_ITERATOR}";

	public static final String DATE_FORMAT_WITHOUT_COLON_PATTERN = "yyyy-MM-dd HH-mm-ss"; 
	/**
	 * @deprecated Not thread safe, so a new instance should be used every time.
	 * @see #getDateFormatWithoutColon()
	 */
	public static SimpleDateFormat DATE_FORMAT_WITHOUT_COLON = new SimpleDateFormat(
			DATE_FORMAT_WITHOUT_COLON_PATTERN);
	public static SimpleDateFormat getDateFormatWithoutColon() {
		return new SimpleDateFormat(DATE_FORMAT_WITHOUT_COLON_PATTERN);
	}

	public static final String UNIGRIDS_TYPES_NS = "http://unigrids.org/2006/04/types";

	public final static QName EPR_METADATA = new QName(
			"http://www.w3.org/2005/08/addressing", "Metadata");

	public final static QName INTERFACE_NAME = new QName(
			"http://www.w3.org/2005/08/addressing/metadata", "InterfaceName");

	public final static QName SERVER_NAME = new QName(
			"http://www.unicore.eu/unicore6", "ServerIdentity");


}
