package de.fzj.unicore.rcp.common.guicomponents;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract base class for implementing list elements for the
 * {@link GenericListEditor}. Should be subclassed in order to obtain functional
 * list elements.
 * 
 * @author bdemuth
 * 
 */
public abstract class AbstractListElement implements IListElement {

	protected List<INameChangeListener> listeners = new ArrayList<INameChangeListener>(
			1);
	protected String name;

	public void addNameChangeListener(INameChangeListener l) {
		listeners.add(l);
	}

	public boolean canBeRemoved() {

		return true;
	}

	public String getName() {
		return name;
	}

	public String nameValid(String name) {
		return null;
	}

	public void removeNameChangeListener(INameChangeListener l) {
		listeners.remove(l);
	}

	/**
	 * When overriding this, call super.setName(String) in order to inform the
	 * listeners.
	 */
	public void setName(String name) {
		String oldName = this.name;
		for (INameChangeListener l : listeners) {
			l.nameChanged(getId(), oldName, name);
		}
		this.name = name;

	}

}
