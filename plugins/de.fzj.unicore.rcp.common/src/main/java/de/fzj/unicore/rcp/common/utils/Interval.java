package de.fzj.unicore.rcp.common.utils;

public class Interval {

	private double start, end;

	private boolean containsStart = true, containsEnd = true;

	public Interval(double start, double end) {
		this(start, end, true, true);
	}

	public Interval(double start, double end, boolean containsStart,
			boolean containsEnd) {
		this.start = start;
		this.end = end;
		this.containsStart = containsStart;
		this.containsEnd = containsEnd;
	}

	public boolean containsEnd() {
		return containsEnd;
	}

	public boolean containsStart() {
		return containsStart;
	}

	public double getEnd() {
		return end;
	}

	public double getStart() {
		return start;
	}

	public boolean intersects(Interval i) {
		return (getStart() < i.getEnd() || getStart() == i.getEnd()
				&& containsStart() && i.containsEnd())
				&& (getEnd() > i.getStart() || getEnd() == i.getStart()
						&& containsEnd() && i.containsStart());
	}

	public void setContainsEnd(boolean containsEnd) {
		this.containsEnd = containsEnd;
	}

	public void setContainsStart(boolean containsStart) {
		this.containsStart = containsStart;
	}

	public void setEnd(double end) {
		this.end = end;
	}

	public void setStart(double start) {
		this.start = start;
	}

	@Override
	public String toString() {
		String opening = containsStart() ? "[" : "(";
		String closing = containsEnd() ? "]" : ")";
		return opening + getStart() + "," + getEnd() + closing;
	}

}
