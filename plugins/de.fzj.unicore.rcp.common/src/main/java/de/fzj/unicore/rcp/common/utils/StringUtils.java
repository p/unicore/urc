package de.fzj.unicore.rcp.common.utils;

import java.util.Arrays;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	public static String convertNewLines(String inputStr) {
		String newLine = System.getProperty("line.separator");
		return inputStr.replaceAll("\\r?\\n", newLine);
	}

	public static String cutOffDigits(String s) {
		// cut off all digits at the end of the input String
		Pattern p = Pattern.compile("\\d+\\z");
		Matcher m = p.matcher(s);
		return m.replaceAll("");
	}



	public static String longestCommonPrefix(String... strings){
		if(strings == null || strings.length == 0) return "";
		if(strings.length == 1) return strings[0];
		String[] strings2 = new String[strings.length];
		System.arraycopy(strings, 0, strings2, 0, strings.length);
		strings = strings2;
		Arrays.sort(strings);
		String first = strings[0];
		String last = strings[strings.length-1];
		String prefix = first;
		int index = first.length();
		while(index >= 0 && !last.startsWith(prefix)){
			prefix= first.substring(0, index--);
		}
		return prefix;
	}

	public static String longestCommonSuffix(String... strings){
		if(strings == null || strings.length == 0) return "";
		if(strings.length == 1) return strings[0];
		String[] strings2 = new String[strings.length];
		System.arraycopy(strings, 0, strings2, 0, strings.length);
		strings = strings2;
		Arrays.sort(strings, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				int len1 = o1.length();
				int len2 = o2.length();
			
				char v1[] = o1.toCharArray();
				char v2[] = o2.toCharArray();
				
				int i = len1-1;
				int j = len2-1;
				
				while (i >= 0 && j >= 0) {
					char c1 = v1[i];
					char c2 = v2[j];
					if (c1 != c2) {
						return c2 - c1;
					}
					i--;
					j--;
				}
				return len2 - len1;
			}
		});
		String first = strings[0];
		String last = strings[strings.length-1];
		String suffix = last;
		int lastLength = last.length();
		int index = 1;
		while(index < lastLength+1 && !first.endsWith(suffix)){
			suffix= last.substring(index, lastLength);
			index++;
		}
		return suffix;
	}
}