package de.fzj.unicore.rcp.common.guicomponents;

import org.eclipse.jface.viewers.ICellEditorValidator;

public class IntegerCellEditorValidator implements ICellEditorValidator {

	public String isValid(Object value) {
		if (!(value instanceof String)) {
			return "Value must be of type String";
		}
		String s = (String) value;
		try {
			Integer.parseInt(s);
			return null;
		} catch (Exception e) {
			return "Please enter an integer value";
		}
	}

}
