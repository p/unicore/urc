package de.fzj.unicore.rcp.common.utils;

import java.io.File;

public interface DirectoryListener {

	public abstract void contentChanged(File[] added, File[] deleted, File[] modified) ;
}
