package de.fzj.unicore.rcp.common.guicomponents;

/**
 * Interface for elements that can be organized by a {@link GenericListEditor}
 * 
 * @author bdemuth
 * 
 */
public interface IListElement {

	/**
	 * Interface for listening to name changes on this list element. Used for
	 * updating the {@link GenericListEditor}.
	 * 
	 * @author bdemuth
	 * 
	 */
	public interface INameChangeListener {
		public void nameChanged(String sourceId, String oldName, String newName);
	}

	public void addNameChangeListener(INameChangeListener l);

	public boolean canBeRemoved();

	/**
	 * A unique id for identifying this element. No two elements in the list are
	 * allowed to share the same id!
	 * 
	 * @return
	 */
	public String getId();

	public String getName();

	/**
	 * Check whether the list element's name can be set to the given value. If
	 * so, return null. If not, return a String describing why the name is
	 * invalid.
	 * 
	 * @param name
	 *            The name to be checked
	 * @return
	 */
	public String nameValid(String name);

	public void removeNameChangeListener(INameChangeListener l);

	public void setName(String name);
}
