package de.fzj.unicore.rcp.common.problems;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.logmonitor.problems.ShowPreferencePageSolution;

public class CheckConnectionTimeoutSolution extends ShowPreferencePageSolution {

	public CheckConnectionTimeoutSolution() {
		super("CHECK_CONN_TIMEOUT_VALUE", "CONN_TIMED_OUT",
				"Increase the value of the connection timeout...");
	}

	@Override
	protected String getPreferencePageId() {
		return Constants.PREFERENCE_PAGE_ID;
	}

}
