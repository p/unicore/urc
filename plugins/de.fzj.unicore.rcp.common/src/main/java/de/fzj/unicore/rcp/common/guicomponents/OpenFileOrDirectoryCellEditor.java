/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * DISCLAIMER
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************************/

package de.fzj.unicore.rcp.common.guicomponents;

import java.io.File;

import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;

public class OpenFileOrDirectoryCellEditor extends DialogCellEditor {

	private String filterPath = null;
	private String fileName = null;
	private String[] filterExtensions = {};

	private static final String FILE = "Files", DIR = "Directory";

	private static final String[] OPTIONS = new String[] { FILE, DIR };
	private String title;
	private boolean allowDirectories = true;
	private boolean returningDirectories = false;
	private boolean allowMultiple = false;
	

	/** A CellEditor, opening a FileBrowser for selecting a File */
	public OpenFileOrDirectoryCellEditor(Composite parent) {
		super(parent);
	}

	/**
	 * A CellEditor, opening a FileBrowser for selecting a File
	 * 
	 * @param parent
	 * @param filterPath
	 *            the system dependent path to the default directory for
	 *            selecting a file
	 */
	public OpenFileOrDirectoryCellEditor(Composite parent, String filterPath) {
		super(parent);
		setFilterPath(filterPath);
	}

	public String getFileName() {
		return fileName;
	}

	public String[] getFilterExtensions() {
		return filterExtensions;
	}

	public String getFilterPath() {
		return filterPath;
	}

	public String getTitle() {
		return title;
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		String[] filenames = null;
		RadioButtonDialog radioButtonDialog = null;

		if (allowDirectories) {
			radioButtonDialog = new RadioButtonDialog(
					cellEditorWindow.getShell(), "Please choose", OPTIONS);
			radioButtonDialog.open();
			if (radioButtonDialog.getReturnCode() == Window.CANCEL) {
				return null;
			}
		}

		String dialogTitle = getTitle();

		if (!allowDirectories
				|| FILE.equals(radioButtonDialog.getSelectedOption())) {
			if (dialogTitle == null) {
				if (allowMultiple) {
					dialogTitle = "Please select files.";
				} else {
					dialogTitle = "Please select a file.";
				}
			}
			int multi = allowMultiple ? SWT.MULTI : SWT.SINGLE;
			// Create file selection dialog
			FileDialog dialog = new FileDialog(cellEditorWindow.getShell(),
					SWT.OPEN | multi);
			if (getFileName() != null) {
				dialog.setFileName(getFileName());
			}
			if (getFilterPath() != null) {
				dialog.setFilterPath(getFilterPath());
			}
			dialog.setFilterExtensions(filterExtensions);
			dialog.setText(dialogTitle);
			dialog.open();
			filenames = dialog.getFileNames();
			if (filenames != null) {
				for (int i = 0; i < filenames.length; i++) {
					filenames[i] = dialog.getFilterPath() + File.separator
							+ filenames[i];
				}
			}
		} else if (DIR.equals(radioButtonDialog.getSelectedOption())) {
			returningDirectories = true;
			if (dialogTitle == null) {
				dialogTitle = "Please select a directory.";
			}
			// // Create dir selection dialog
			DirectoryDialog dialog = new DirectoryDialog(
					cellEditorWindow.getShell(), SWT.OPEN);
			if (getFilterPath() != null) {
				dialog.setFilterPath(getFilterPath());
			}
			dialog.setText(dialogTitle);
			String dir = dialog.open();
			if (dir == null) {
				filenames = null;
			} else {
				filenames = new String[] { dir };
			}
		}
		// Indicate if file name is valid
		setValueValid(filenames != null);
		return filenames;
	}

	protected void setAllowDirectories(boolean allowDirectories) {
		this.allowDirectories = allowDirectories;
	}

	public void setAllowMultiple(boolean allowMultiple) {
		this.allowMultiple = allowMultiple;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFilterExtensions(String[] filterExtensions) {
		this.filterExtensions = filterExtensions;
	}

	public void setFilterPath(String filterPath) {
		this.filterPath = filterPath;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isReturningDirectories() {
		return returningDirectories;
	}

}
