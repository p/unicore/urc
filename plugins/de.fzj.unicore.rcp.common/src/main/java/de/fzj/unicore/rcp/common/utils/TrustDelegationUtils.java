package de.fzj.unicore.rcp.common.utils;

import javax.security.auth.x500.X500Principal;

import org.w3.x2005.x08.addressing.EndpointReferenceType;

import eu.unicore.util.httpclient.IClientConfiguration;

/**
 * @deprecated this should not be needed anymore, as the IdentityActivator
 * already adds the TD settings
 */
public class TrustDelegationUtils {

	@Deprecated
	public static IClientConfiguration addTrustDelegationTo(
			IClientConfiguration sp, EndpointReferenceType serverEpr) {
		String serverID = Utils.extractServerIDFromEPR(serverEpr);
		return addTrustDelegationTo(sp, serverID);
	}

	@Deprecated
	public static IClientConfiguration addTrustDelegationTo(
			IClientConfiguration sp, String serverID) {
		sp = sp.clone();
		sp.getETDSettings().setExtendTrustDelegation(true);
		sp.getETDSettings().setIssuerCertificateChain(sp.getCredential().getCertificateChain());
		sp.getETDSettings().setReceiver(new X500Principal(serverID));
		return sp;
	}

}
