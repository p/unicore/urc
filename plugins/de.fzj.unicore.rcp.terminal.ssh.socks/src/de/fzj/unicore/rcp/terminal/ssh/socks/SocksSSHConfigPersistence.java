package de.fzj.unicore.rcp.terminal.ssh.socks;

import java.util.Map;

import org.eclipse.ui.IMemento;

import de.fzj.unicore.rcp.terminal.ConfigPersistence;
import de.fzj.unicore.rcp.terminal.ConnectionTypeRegistry;
import de.fzj.unicore.rcp.terminal.TerminalConstants;

public class SocksSSHConfigPersistence extends ConfigPersistence implements SocksSSHConstants {

	private static ConfigPersistence instance;

	public static ConfigPersistence getInstance()
	{
		if(instance == null)
		{
			instance = new SocksSSHConfigPersistence();
		}
		return instance;
	}
	
	@Override
	public boolean readConfig(Map<String, String> config, IMemento memento) {
		IMemento socks = ConfigPersistence.getConnectionTypeMemento(CONNECTION_TYPE_ID_SOCKS,memento); 
		if(socks == null) return false;
		String proxyHost = socks.getString(SOCKS_PROXY_HOST);
		String proxyPort = socks.getString(SOCKS_PROXY_PORT);
		String backendHost = socks.getString(SOCKS_BACKEND_HOST);
		String backendPort = socks.getString(SOCKS_BACKEND_PORT);
		String supportedString = memento.getString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED);
		Boolean supported = supportedString != null && supportedString.contains(CONNECTION_TYPE_ID_SOCKS);
		config.put(SOCKS_PROXY_HOST,proxyHost); 
		config.put(SOCKS_PROXY_PORT,proxyPort);
		config.put(SOCKS_BACKEND_HOST,backendHost);
		config.put(SOCKS_BACKEND_PORT, backendPort);
		config.put(SOCKS_SUPPORTED, supported.toString());
		return true;
	}

	public boolean writeConfig(Map<String,String> config, IMemento memento){
		

		IMemento connectionType = getConnectionTypeMemento(CONNECTION_TYPE_ID_SOCKS,memento);
		if(connectionType == null) connectionType = createConnectionTypeMemento(CONNECTION_TYPE_ID_SOCKS,memento);
		String proxyHost = null; //TODO deal with those correctly
		String proxyPort = null;
		String backendHost = null;
		String backendPort = null;
		String supported = config.get(SOCKS_SUPPORTED);
		String supportedString = memento.getString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED);
		if(supportedString == null) supportedString = "";
		if(Boolean.parseBoolean(supported))
		{
			if(!supportedString.contains(CONNECTION_TYPE_ID_SOCKS))
			{
				if(supportedString.length() > 0) supportedString += " ";
				supportedString += CONNECTION_TYPE_ID_SOCKS;
			}
		}
		else
		{
			if(supportedString.contains(CONNECTION_TYPE_ID_SOCKS))
			{
				supportedString.replaceFirst("\\s*"+CONNECTION_TYPE_ID_SOCKS, "");
			}
		}
		memento.putString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED, supportedString);
		
		connectionType.putString(SOCKS_PROXY_HOST,proxyHost);
		connectionType.putString(SOCKS_PROXY_PORT,String.valueOf(proxyPort));
		connectionType.putString(SOCKS_BACKEND_HOST,backendHost);
		connectionType.putString(SOCKS_BACKEND_PORT,String.valueOf(backendPort));
		connectionType.putString(ATTRIBUTE_CONNECTION_TYPE_SUPPORTED,supported);
		connectionType.putString(ATTRIBUTE_CONNECTION_TYPE_NAME, ConnectionTypeRegistry.getInstance().getNameForId(CONNECTION_TYPE_ID_SOCKS));
		return true;
	}
}
