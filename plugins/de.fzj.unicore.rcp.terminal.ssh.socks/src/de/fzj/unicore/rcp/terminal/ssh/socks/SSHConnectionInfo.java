/*****************************************************************************
 * Copyright (c) 2006, 2007 g-Eclipse Consortium
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Initial development of the original code was made for the
 * g-Eclipse project founded by European Union
 * project number: FP6-IST-034327  http://www.geclipse.eu/
 *
 * Contributors:
 *    Thomas Koeckerbauer GUP, JKU - initial API and implementation
 *****************************************************************************/

package de.fzj.unicore.rcp.terminal.ssh.socks;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;


class SSHConnectionInfo implements UserInfo, UIKeyboardInteractive {
	String passphrase;
	String passwd;
	boolean canceledPWValue;
	private String user;
	private String host;
	private int port;
	private boolean promptPasswd;
	private boolean promptPassphrase;
	private boolean passwordInteractiveUsed;
	private String[] keyboardInteractiveResult;

	SSHConnectionInfo( )
	{
		this(null,null,null,null,22);
	}
	
	SSHConnectionInfo( final String username, final String hostname,
			final String password, final String passphrase,
			final int portNumber ) {
		this.user = username;
		this.host = hostname;
		this.passwd = password;
		this.promptPasswd = !( this.passwd != null && this.passwd.length() != 0 );
		this.passphrase = passphrase;
		this.promptPassphrase = !( this.passphrase != null && this.passphrase.length() != 0 );
		this.port = portNumber;
		this.canceledPWValue = false;
	}

	public String getPassword() {
		this.promptPasswd = true;
		return this.passwd;
	}

	String getUsername() {
		return this.user;
	}

	String getHostname() {
		return this.host;
	}

	int getPort() {
		return this.port;
	}

	public boolean promptYesNo( final String str ) {
		final boolean[] result = { false };
		Display.getDefault().syncExec( new Runnable() {
			public void run() {
				result[0] = MessageDialog.openQuestion( null,
						Messages.getString( "SshShell.sshTerminal" ), //$NON-NLS-1$
						str );
			}
		} );
		return result[0];
	}

	public String getPassphrase() {
		this.promptPassphrase = true;
		return this.passphrase;
	}

	public boolean promptPassphrase( final String message ) {

		return false;
	}

	public boolean promptPassword( final String message ) {
		final int[] result = { Window.OK };

		if ( this.promptPasswd ) {
			Display.getDefault().syncExec( new Runnable() {
				public void run() {
					HiddenInputDialog dlg = new HiddenInputDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(),"Password","Please enter your password","321", null);
					dlg.open();
					if ( dlg.getReturnCode() == InputDialog.OK )
						SSHConnectionInfo.this.passwd = dlg.getValue();
					else
						SSHConnectionInfo.this.canceledPWValue = true;
				}
			} );
		}
		return !canceledPWValue;
	}
	
	public boolean promptHost( ) {
			Display.getDefault().syncExec( new Runnable() {
				public void run() {
					String hostname = SSHConnectionInfo.this.host == null ? "hostname" : SSHConnectionInfo.this.host;
					InputDialog dlg = new InputDialog(new Shell(),"Host name","Please enter the host name:",hostname,null); 
					dlg.open();
					if ( dlg.getReturnCode() == InputDialog.OK )
						SSHConnectionInfo.this.host = dlg.getValue();
					else SSHConnectionInfo.this.host = null;
					
				}
			} );
		return host != null;
	}

	public boolean promptUsername( ) {
			Display.getDefault().syncExec( new Runnable() {
				public void run() {
					InputDialog dlg = new InputDialog(new Shell(),"User name","Please enter the user name:","chemomentum",null); 
					dlg.open();
					if ( dlg.getReturnCode() == InputDialog.OK )
						SSHConnectionInfo.this.user = dlg.getValue();
					else SSHConnectionInfo.this.user = null;
				}
			} );
		return user != null;
	}
	
	public void showMessage( final String message ) {
		if (message != null && message.trim().length() != 0) {
			Display.getDefault().syncExec( new Runnable() {
				public void run() {
					MessageDialog.openInformation( null,
							Messages.getString( "SshShell.sshTerminal" ), //$NON-NLS-1$
							message );
				}
			} );
		}
	}

	/**
	 * Returns if the user pushed cancel when queried for password for the ssh session.
	 * @return Returns <code>true</code> if the user pushed cancel when asked for the pw,
	 * <code>true</code> otherwise.
	 */
	public boolean getCanceledPWValue() {
		return this.canceledPWValue;
	}

	public String[] promptKeyboardInteractive( final String destination,
			final String name,
			final String instruction,
			final String[] prompt,
			final boolean[] echo ) {
		String[] result;
		if ( this.passwordInteractiveUsed ) {
			Display.getDefault().syncExec( new Runnable() {
				public void run() {
					HiddenInputDialog dlg = new HiddenInputDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(),"Password","Please enter your password","321", null);
			          dlg.open();
					keyboardInteractiveResult = new String[]{dlg.getValue()};
				}
			} );
			result = this.keyboardInteractiveResult;
		} else {
			this.passwordInteractiveUsed = true;
			result = new String[]{this.passwd};
		}
		return result;
	}
}
