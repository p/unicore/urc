package de.fzj.unicore.rcp.terminal.ssh.socks;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import de.fzj.unicore.rcp.terminal.IBidirectionalConnection;

public class UnicoreBidirectionalConnection implements IBidirectionalConnection {

	private InputStream inputStream;
	private OutputStream outputStream;
	
	public UnicoreBidirectionalConnection(InputStream inputStream,
			OutputStream outputStream) {
		super();
		this.inputStream = inputStream;
		this.outputStream = outputStream;
	}

	public void close() {

	}

	public InputStream getInputStream() throws IOException {
		return inputStream;
	}

	public OutputStream getOutputStream() throws IOException {
		return outputStream;
	}

}
