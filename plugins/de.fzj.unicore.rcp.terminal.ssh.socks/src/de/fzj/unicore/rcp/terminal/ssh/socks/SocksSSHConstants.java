package de.fzj.unicore.rcp.terminal.ssh.socks;

import de.fzj.unicore.rcp.terminal.TerminalConstants;

public interface SocksSSHConstants extends TerminalConstants {
	public static final String CONNECTION_TYPE_ID_SOCKS = "SOCKS";
	public static final String SOCKS_PROXY_HOST = "proxyHost";
	public static final String SOCKS_PROXY_PORT = "proxyPort";
	public static final String SOCKS_BACKEND_HOST = "backendHost";
	public static final String SOCKS_BACKEND_PORT = "backendPort";
	
	public static final String SOCKS_SUPPORTED = "socks_supported";

}
