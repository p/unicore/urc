/* -*-mode:java; c-basic-offset:2; indent-tabs-mode:nil -*- */
/*
Copyright (c) 2002-2008 ymnk, JCraft,Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright 
     notice, this list of conditions and the following disclaimer in 
     the documentation and/or other materials provided with the distribution.

  3. The names of the authors may not be used to endorse or promote products
     derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL JCRAFT,
INC. OR ANY CONTRIBUTORS TO THIS SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 This file depends on following documents,
   - RFC 1928  SOCKS Protocol Verseion 5  
   - RFC 1929  Username/Password Authentication for SOCKS V5. 
 */

package de.fzj.unicore.rcp.terminal.ssh.socks;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.X509Certificate;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.openssl.PEMWriter;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SocketFactory;

public class UnicoreProxySOCKS5 implements com.jcraft.jsch.Proxy{
	private static int DEFAULTPORT=1080;
	private String proxy_host;
	private int proxy_port;
	private InputStream in;
	private OutputStream out;
	private Socket socket;
	private String user;
	private String passwd;
	private String myKey;  
	private X509Certificate cert;
	private PrivateKey privateKey;
	private Session session;

	
	public UnicoreProxySOCKS5(String proxy_host, X509Certificate cert, PrivateKey privateKey, Session session){
		int port=DEFAULTPORT;
		String host=proxy_host;
		if(proxy_host.indexOf(':')!=-1){
			try{
				host=proxy_host.substring(0, proxy_host.indexOf(':'));
				port=Integer.parseInt(proxy_host.substring(proxy_host.indexOf(':')+1));
			}
			catch(Exception e){
			}
		}
		this.proxy_host=host;
		this.proxy_port=port;
		this.privateKey = privateKey;
		this.cert = cert;
		this.session = session;
	}
	
	
	public void setUserPasswd(String user, String passwd){
		this.user=user;
		this.passwd=passwd;
	}
	public String getUsername() {
		return this.user;
	}



	public void connect(SocketFactory socket_factory, String host, int port, int timeout) throws JSchException{

		
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

		System.out.println( 
				"Beginning of sock5 connect to "+host+" port "+port);
		System.out.println("Proxy is  "+proxy_host+" port "+proxy_port);

		try{
			if(socket_factory==null){
				socket=Util.createSocket(proxy_host, proxy_port, timeout);
				//socket=new Socket(proxy_host, proxy_port);    
				in=socket.getInputStream();
				out=socket.getOutputStream();
			}
			else{
				socket=socket_factory.createSocket(proxy_host, proxy_port);
				in=socket_factory.getInputStream(socket);
				out=socket_factory.getOutputStream(socket);
			}
			if(timeout>0){
				socket.setSoTimeout(timeout);
			}
			socket.setTcpNoDelay(true);

			System.out.println("sock5 connect succeed");


			byte[] buf=new byte[1024];
			int index=0;

			/*
                   +----+----------+----------+
                   |VER | NMETHODS | METHODS  |
                   +----+----------+----------+
                   | 1  |    1     | 1 to 255 |
                   +----+----------+----------+

   The VER field is set to X'05' for this version of the protocol.  The
   NMETHODS field contains the number of method identifier octets that
   appear in the METHODS field.

   The values currently defined for METHOD are:

          o  X'00' NO AUTHENTICATION REQUIRED
          o  X'01' GSSAPI
          o  X'02' USERNAME/PASSWORD
          o  X'03' to X'7F' IANA ASSIGNED
          o  X'80' to X'FE' RESERVED FOR PRIVATE METHODS
          o  X'FF' NO ACCEPTABLE METHODS
			 */

			buf[index++]=5;

			buf[index++]=3;
			buf[index++]=0;           // NO AUTHENTICATION REQUIRED
			buf[index++]=2;           // USERNAME/PASSWORD
			buf[index++]=42;           // USERNAME/PASSWORD
			out.write(buf, 0, index);

			/*
    The server selects from one of the methods given in METHODS, and
    sends a METHOD selection message:

                         +----+--------+
                         |VER | METHOD |
                         +----+--------+
                         | 1  |   1    |
                         +----+--------+
			 */
			/*in.read(buf, 0, 2);
      System.out.println("awaiting version");*/
			fill(in, buf, 2);
			/*System.out.println("version received");       
			 */
			boolean check=false;
			switch((buf[1])&0xff){
			case 0:                // NO AUTHENTICATION REQUIRED
				check=true;
				break;
			case 2:                // USERNAME/PASSWORD
				if(user==null || passwd==null)break;

				/*
   Once the SOCKS V5 server has started, and the client has selected the
   Username/Password Authentication protocol, the Username/Password
   subnegotiation begins.  This begins with the client producing a
   Username/Password request:

           +----+------+----------+------+----------+
           |VER | ULEN |  UNAME   | PLEN |  PASSWD  |
           +----+------+----------+------+----------+
           | 1  |  1   | 1 to 255 |  1   | 1 to 255 |
           +----+------+----------+------+----------+

   The VER field contains the current version of the subnegotiation,
   which is X'01'. The ULEN field contains the length of the UNAME field
   that follows. The UNAME field contains the username as known to the
   source operating system. The PLEN field contains the length of the
   PASSWD field that follows. The PASSWD field contains the password
   association with the given UNAME.
				 */
				index=0;
				buf[index++]=1;
				buf[index++]=(byte)(user.length());
				System.arraycopy(user.getBytes(), 0, buf, index, user.length());
				index+=user.length();
				buf[index++]=(byte)(passwd.length());
				System.arraycopy(passwd.getBytes(), 0, buf, index, passwd.length());
				index+=passwd.length();

				out.write(buf, 0, index);

				/*
   The server verifies the supplied UNAME and PASSWD, and sends the
   following response:

                        +----+--------+
                        |VER | STATUS |
                        +----+--------+
                        | 1  |   1    |
                        +----+--------+

   A STATUS field of X'00' indicates success. If the server returns a
   `failure' (STATUS value other than X'00') status, it MUST close the
   connection.
				 */
				//in.read(buf, 0, 2);
				fill(in, buf, 2);
				if(buf[1]==0)
					check=true;
				break;
			case 42:
				String KEYREQUEST = "KEYREQUEST";
				String key = "";
				byte [] encodedkey = null;

				Integer KEYLEN = 63;
				

				System.out.println("loading certificate and Requesting key from server\n");


				index=0;			
				buf[index++]=0;
				System.arraycopy(KEYREQUEST.getBytes(), 0, buf, index, KEYREQUEST.length());
				index = index + KEYREQUEST.length(); 
				out.write(buf, 0, index);
				
				System.out.println("awaiting key\n");

				fill(in, buf, KEYLEN+1);
				byte help[] = new byte[KEYLEN] ;
				System.arraycopy(buf, 1, help, 0, KEYLEN);
				key = new String(help);
				myKey = key;


				System.out.println("scramble key " + key + "Keylen " + KEYLEN );

				encodedkey = encodekey(key, privateKey);
				StringWriter writer = new StringWriter();
				PEMWriter pemWriter = new PEMWriter(writer);
				pemWriter.writeObject(cert);
				pemWriter.flush();
				writer.flush();
				byte[] certData = writer.toString().getBytes();
				int size = calcsize(certData.length, user, encodedkey.length);


				System.out.println("send size " + size);


				index=0;			
				buf[index++]= 1;
				buf[index++]=(byte)(size&0xff);	
				buf[index++]=(byte)(size>>8 & 0xff);
				buf[index++]=(byte)(size>>16 & 0xff);       
				buf[index++]=(byte)(size>>>24);

				out.write(buf ,0 , 5);

				System.out.println("awaiting available remote buffersize");

				fill(in, buf, 5);

				//TODO: insert here routine for looping if remote buffersize is exceeded


				System.out.println("packing x509 certificate and encodedkey and their sizes");


				buf = makeCertPaket(certData, encodedkey, user, size);
				

				System.out.println("Send " +  buf.length + "bytes x509 certificate and encodedkey and their sizes");


				out.write(buf, 0, buf.length);

				//System.out.println("awaiting unamelen ");	        

				System.out.println("awaiting uname ");


				in.read(buf);
				int unamelen = buf[1];
				byte unamehelp[] = new byte[unamelen] ;
				System.arraycopy(buf, 2, unamehelp, 0, unamelen);			


				System.out.println("received " + unamelen + " "+ user);


				if (user.length()<1) user = new String(unamehelp);

				System.out.println("awaiting access granted for" + user );


				fill(in, buf, 2);
				//System.out.println("answer read" );	        


				System.out.println("answer read" );


				if(buf[1]==0) check = true;
				else check = false;


				if (check == true)  { 
					System.out.println("access granted for" + user + ":-) ");
				} else {
					System.out.println("access denied for" + user + ":-( ");
				}


				break;
			default:
			}

			if(!check){
				try{ socket.close(); }
				catch(Exception eee){
				}
				throw new JSchException("fail in SOCKS5 proxy, Socks not denied access");
			}

			/*
      The SOCKS request is formed as follows:

        +----+-----+-------+------+----------+----------+
        |VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |
        +----+-----+-------+------+----------+----------+
        | 1  |  1  | X'00' |  1   | Variable |    2     |
        +----+-----+-------+------+----------+----------+

      Where:

      o  VER    protocol version: X'05'
      o  CMD
         o  CONNECT X'01'
         o  BIND X'02'
         o  UDP ASSOCIATE X'03'
      o  RSV    RESERVED
         o  ATYP   address type of following address
         o  IP V4 address: X'01'
         o  DOMAINNAME: X'03'
         o  IP V6 address: X'04'
      o  DST.ADDR       desired destination address
      o  DST.PORT desired destination port in network octet
         order
			 */

			index=0;
			buf[index++]=5;
			buf[index++]=1;       // CONNECT
			buf[index++]=0;

			byte[] hostb=host.getBytes();
			int len=hostb.length;
			buf[index++]=3;      // DOMAINNAME
			buf[index++]=(byte)(len);
			System.arraycopy(hostb, 0, buf, index, len);
			index+=len;
			buf[index++]=(byte)(port>>>8);
			buf[index++]=(byte)(port&0xff);

			out.write(buf, 0, index);

			/*
   The SOCKS request information is sent by the client as soon as it has
   established a connection to the SOCKS server, and completed the
   authentication negotiations.  The server evaluates the request, and
   returns a reply formed as follows:

        +----+-----+-------+------+----------+----------+
        |VER | REP |  RSV  | ATYP | BND.ADDR | BND.PORT |
        +----+-----+-------+------+----------+----------+
        | 1  |  1  | X'00' |  1   | Variable |    2     |
        +----+-----+-------+------+----------+----------+

   Where:

   o  VER    protocol version: X'05'
   o  REP    Reply field:
      o  X'00' succeeded
      o  X'01' general SOCKS server failure
      o  X'02' connection not allowed by ruleset
      o  X'03' Network unreachable
      o  X'04' Host unreachable
      o  X'05' Connection refused
      o  X'06' TTL expired
      o  X'07' Command not supported
      o  X'08' Address type not supported
      o  X'09' to X'FF' unassigned
    o  RSV    RESERVED
    o  ATYP   address type of following address
      o  IP V4 address: X'01'
      o  DOMAINNAME: X'03'
      o  IP V6 address: X'04'
    o  BND.ADDR       server bound address
    o  BND.PORT       server bound port in network octet order
			 */

			//in.read(buf, 0, 4);
			fill(in, buf, 4);

			if(buf[1]!=0){
				try{ socket.close(); }
				catch(Exception eee){
				}
				throw new JSchException("ProxySOCKS5: server returns "+buf[1]);
			}

			switch(buf[3]&0xff){
			case 1:
				//in.read(buf, 0, 6);
				fill(in, buf, 6);
				break;
			case 3:
				//in.read(buf, 0, 1);
				fill(in, buf, 1);
				//in.read(buf, 0, buf[0]+2);
				fill(in, buf, (buf[0]&0xff)+2);
				break;
			case 4:
				//in.read(buf, 0, 18);
				fill(in, buf, 18);
				break;
			default:
			}
			
			Method m = session.getClass().getDeclaredMethod("setUserName", String.class);
			m.setAccessible(true);
			m.invoke(session, getUsername());
			session.setPassword(getPassword());
		}
		catch(RuntimeException e){
			e.printStackTrace();
			throw e;
		}
		catch(Exception e){
			try{ if(socket!=null)socket.close(); }
			catch(Exception eee){
			}
			String message="ProxySOCKS5: "+e.toString();
			if(e instanceof Throwable)
				throw new JSchException(message, (Throwable)e);
			throw new JSchException(message);
		}

		
	}
	
	
	public InputStream getInputStream(){ return in; }
	public OutputStream getOutputStream(){ return out; }
	public Socket getSocket(){ return socket; }
	public String getPassword(){return myKey;}
	public void close(){
		try{
			if(in!=null)in.close();
			if(out!=null)out.close();
			if(socket!=null)socket.close();
		}
		catch(Exception e){
		}
		in=null;
		out=null;
		socket=null;
	}
	public static int getDefaultPort(){
		return DEFAULTPORT;
	}
	private void fill(InputStream in, byte[] buf, int len) throws JSchException, IOException{
		int s=0;
		while(s<len){
			int i=in.read(buf, s, len-s);
			if(i<=0){
				throw new JSchException("ProxySOCKS5: stream is closed");
			}
			s+=i;
		}
	}

	byte [] encodekey(String key, PrivateKey privateKey) {  

		byte [] encryptedBytes = null;
		try {

			System.out.println("private key extracted");


			javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance(privateKey.getAlgorithm());

			System.out.println("cipher created");

			cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, privateKey);

			System.out.println("cipher initilized, now starting encrypt key.");	 	     

			encryptedBytes = cipher.doFinal(key.getBytes());

		} catch (BadPaddingException e) {

			System.err.println("Password verification failed " + e.getMessage());	 	     

			return null;
		} catch (NoSuchAlgorithmException e) {

			System.err.println("Password verification failed " + e.getMessage());	 	     

			return null;
		} catch (NoSuchPaddingException e) {

			System.err.println("Password verification failed " + e.getMessage());	 	     

			return null;
		} catch (InvalidKeyException e) {

			System.err.println("Password verification failed " + e.getMessage());	 	     

			return null;
		} catch (IllegalBlockSizeException e) {

			System.err.println("Password verification failed " + e.getMessage());	 	     

			return null;
		}
		return encryptedBytes;
	}   

	int calcsize(int certsize, String username, int encodedkeysize) {
		int u_intsize = 4;
		
		// Status | size |encoded key | size | Certificate 
		return 1 + encodedkeysize + certsize + username.length() + u_intsize + u_intsize + u_intsize;		

	}
	/**
	 * Create the authentication Paket.
	 * @param certfilename , where the certificate ca be loaded from.
	 * @param encodedkey , received which was encoded with the certificate.
	 * @param username, the username presetted when xlogin from shall not be used.
	 * @param paksize, expected answerpaket size.
	 * @return byte array which contains data to send.
	 */
	byte [] makeCertPaket(byte[] certData, byte [] encodedkey, String username, int paksize ) {
		int certsize = 0;
		int encodedkeysize = 0;
		int usernamesize = 0;
		byte [] myBuff = new byte[paksize];
		int index = 0;	  
  

		encodedkeysize = encodedkey.length;
		myBuff[index++] = 1; 

		myBuff[index++]=(byte)(encodedkeysize&0xff);
		myBuff[index++]=(byte)(encodedkeysize>>8 & 0xff);
		myBuff[index++]=(byte)(encodedkeysize>>16 &0xff);
		myBuff[index++]=(byte)(encodedkeysize>>>24);

		System.arraycopy(encodedkey, 0, myBuff, index, encodedkey.length);
		index = index + encodedkey.length;
		certsize = certData.length;
		
		myBuff[index++]=(byte)(certsize&0xff);
		myBuff[index++]=(byte)(certsize>>8 &0xff);
		myBuff[index++]=(byte)(certsize>>16 &0xff);
		myBuff[index++]=(byte)(certsize>>>24);
		
		
		System.arraycopy(certData, 0, myBuff, index, certData.length);
		if (myBuff.length != paksize) { return null;}

		usernamesize = (int) username.length();
		index+=certData.length;
		myBuff[index++]=(byte)(usernamesize&0xff);
		myBuff[index++]=(byte)(usernamesize>>8 &0xff);
		myBuff[index++]=(byte)(usernamesize>>16 &0xff);
		myBuff[index++]=(byte)(usernamesize>>>24);
		System.arraycopy(username.getBytes(), 0, myBuff, index, username.length());	  
		return myBuff;
	} 
	
	

}
