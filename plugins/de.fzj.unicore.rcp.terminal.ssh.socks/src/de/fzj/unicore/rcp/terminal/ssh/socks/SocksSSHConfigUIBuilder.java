package de.fzj.unicore.rcp.terminal.ssh.socks;

import de.fzj.unicore.rcp.terminal.BasicConfigUIBuilder;

public class SocksSSHConfigUIBuilder extends BasicConfigUIBuilder implements SocksSSHConstants {

	@Override
	protected String[] getRelevantConfigKeys() {
		return new String[]{SOCKS_PROXY_HOST,SOCKS_PROXY_PORT,SOCKS_BACKEND_HOST, SOCKS_BACKEND_PORT};
	}

	@Override
	protected String getLabelForKey(String key) {
		return key;
	}

	

	
}
