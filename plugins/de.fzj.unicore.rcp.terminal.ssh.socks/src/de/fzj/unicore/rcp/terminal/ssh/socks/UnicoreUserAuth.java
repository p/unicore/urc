package de.fzj.unicore.rcp.terminal.ssh.socks;

import com.jcraft.jsch.Buffer;
import com.jcraft.jsch.Packet;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserAuth;
import com.jcraft.jsch.UserInfo;


public class UnicoreUserAuth extends UserAuth {

	static final int SSH_MSG_SERVICE_REQUEST=                 5;
	private static final int SSH_MSG_SERVICE_ACCEPT=                  6;
	private final int SSH_MSG_USERAUTH_PASSWD_CHANGEREQ=60;
	
	private String methods=null;
	protected UnicoreProxySOCKS5 proxy;
	protected Packet packet;
	protected Buffer buf;


	public UnicoreUserAuth() {
		super();
	}

	public boolean start(Session session,UserInfo userInfo) throws Exception{
		buf = new Buffer();
		this.packet= new Packet(buf);
		this.proxy = ((UnicoreUserInfo) userInfo).getProxy();
		String username = proxy.getUsername();

		byte[] password=proxy.getPassword().getBytes();
	    String dest=username+"@"+session.getHost();
	    if(session.getPort()!=22){
	      dest+=(":"+session.getPort());
	    }

	    try{

	    while(true){
	      if(password==null){
		if(userinfo==null){
		  //throw new JSchException("USERAUTH fail");
		  return false;
		}
		if(!userinfo.promptPassword("Password for "+dest)){
		  throw new Exception("password");
		  //break;
		}

		String _password=userinfo.getPassword();
		if(_password==null){
		  throw new Exception("password");
		  //break;
		}
	        password=Util.str2byte(_password);
	      }

	      byte[] _username=null;
	      _username=Util.str2byte(username);

	      // send
	      // byte      SSH_MSG_USERAUTH_REQUEST(50)
	      // string    user name
	      // string    service name ("ssh-connection")
	      // string    "password"
	      // boolen    FALSE
	      // string    plaintext password (ISO-10646 UTF-8)
	      packet.reset();
	      buf.putByte((byte)SSH_MSG_USERAUTH_REQUEST);
	      buf.putString(_username);
	      buf.putString("ssh-connection".getBytes());
	      buf.putString("password".getBytes());
	      buf.putByte((byte)0);
	      buf.putString(password);
	      session.write(packet);

	      loop:
	      while(true){
		buf=session.read(buf);
	        int command=getCommand(buf)&0xff;

		if(command==SSH_MSG_USERAUTH_SUCCESS){
		  return true;
		}
		if(command==SSH_MSG_USERAUTH_BANNER){
		  buf.getInt(); buf.getByte(); buf.getByte();
		  byte[] _message=buf.getString();
		  byte[] lang=buf.getString();
	          String message=Util.byte2str(_message);
		  if(userinfo!=null){
		    userinfo.showMessage(message);
		  }
		  continue loop;
		}
		if(command==SSH_MSG_USERAUTH_PASSWD_CHANGEREQ){
		  buf.getInt(); buf.getByte(); buf.getByte(); 
		  byte[] instruction=buf.getString();
		  byte[] tag=buf.getString();
		  if(userinfo==null || 
	             !(userinfo instanceof UIKeyboardInteractive)){
	            if(userinfo!=null){
	              userinfo.showMessage("Password must be changed.");
	            }
	            return false;
	          }

	          UIKeyboardInteractive kbi=(UIKeyboardInteractive)userinfo;
	          String[] response;
	          String name="Password Change Required";
	          String[] prompt={"New Password: "};
	          boolean[] echo={false};
	          response=kbi.promptKeyboardInteractive(dest,
	                                                 name,
	                                                 new String(instruction),
	                                                 prompt,
	                                                 echo);
	          if(response==null){
	            throw new Exception("password");
	          }

	          byte[] newpassword=response[0].getBytes();

	          // send
	          // byte      SSH_MSG_USERAUTH_REQUEST(50)
	          // string    user name
	          // string    service name ("ssh-connection")
	          // string    "password"
	          // boolen    TRUE
	          // string    plaintext old password (ISO-10646 UTF-8)
	          // string    plaintext new password (ISO-10646 UTF-8)
	          packet.reset();
	          buf.putByte((byte)SSH_MSG_USERAUTH_REQUEST);
	          buf.putString(_username);
	          buf.putString("ssh-connection".getBytes());
	          buf.putString("password".getBytes());
	          buf.putByte((byte)1);
	          buf.putString(password);
	          buf.putString(newpassword);
	          Util.bzero(newpassword);
	          response=null;
	          session.write(packet);
		  continue loop;
	        }
		if(command==SSH_MSG_USERAUTH_FAILURE){
		  buf.getInt(); buf.getByte(); buf.getByte(); 
		  byte[] foo=buf.getString();
		  int partial_success=buf.getByte();
		  //System.err.println(new String(foo)+
		  //		 " partial_success:"+(partial_success!=0));
		  if(partial_success!=0){
		    throw new Exception(new String(foo));
		  }
		  break;
		}
		else{
	          //System.err.println("USERAUTH fail ("+buf.getCommand()+")");
//		  throw new JSchException("USERAUTH fail ("+buf.getCommand()+")");
		  return false;
		}
	      }

	      if(password!=null){
	        Util.bzero(password);
	        password=null;
	      }

	    }

	    }
	    finally{
	      if(password!=null){
	        Util.bzero(password);
	        password=null;
	      }
	    }

	    //throw new JSchException("USERAUTH fail");
	    //return false;
	  }
	
	String getMethods(){
		return methods;
	}

	protected int getCommand(Buffer buf)
	{
		int offset = buf.getOffSet();
		buf.setOffSet(5);
		int result = buf.getByte();
		buf.setOffSet(offset);
		return result;
	}

}
