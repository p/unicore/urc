package de.fzj.unicore.rcp.terminal.ssh.socks;

import java.io.File;
import java.net.URI;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.unigrids.x2006.x04.services.tsf.TargetSystemFactoryPropertiesDocument.TargetSystemFactoryProperties;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument.TargetSystemProperties;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.KeyStoreManager;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.SecuredNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;
import de.fzj.unicore.rcp.terminal.IBidirectionalConnection;
import de.fzj.unicore.rcp.terminal.TerminalConnection;
import de.fzj.unicore.rcp.terminal.TerminalConstants;
import de.fzj.unicore.rcp.terminal.extensionpoints.ICommunicationProvider;
import de.fzj.unicore.uas.security.IUASSecurityProperties;

public class UnicoreSSHCommunicationProvider implements
ICommunicationProvider, TerminalConstants {

	public static final String SSH_INFO_SOCKS = Util.SSH_INFO_PREFIX+"SOCKS";

	private static Logger logger = Logger.getLogger(UnicoreSSHCommunicationProvider.class);


	private static JSch jsch;

	public TerminalConnection establishConnection(Map<String,String> connInfo, IProgressMonitor progress ) {
		try {
			String id = connInfo.get(ID);
			NodePath serverAddress = NodePath.parseString(id);
			SecuredNode n = (SecuredNode) NodeFactory.getNodeFor(serverAddress);
			TargetSystemProperties tssProps = Util.getTSSProps(n);
			String proxyHost, backendHost = null;
			int proxyPort,backendPort;

			if(connInfo == null || connInfo.isEmpty()){
				//System.out.println(tssProps.toString());
				Map<String,String> sshInfo = Util.extractSSHRelatedProperties(tssProps);
				String socksInfo = sshInfo.get(SSH_INFO_SOCKS);
				System.out.println("socksInfo: " + socksInfo);
				String[] proxy_backend = socksInfo.split("\\s");

				proxyHost = proxy_backend[0].split(":")[0];
				proxyPort = Integer.parseInt(proxy_backend[0].split(":")[1]);

				backendHost = proxy_backend[1].split(":")[0];
				backendPort = Integer.parseInt(proxy_backend[1].split(":")[1]);
			}
			else{
				proxyHost = connInfo.get("proxyHost");
				proxyPort = Integer.parseInt(connInfo.get("proxyPort"));
				backendHost = connInfo.get("proxyHost");
				backendPort = Integer.parseInt(connInfo.get("backendPort"));
			}


			Session session= getJsch().getSession("", backendHost);

			IUASSecurityProperties secProps = n.getUASSecProps();

			KeyStoreManager manager = IdentityActivator.getDefault().getKeystoreManager();
			String alias = secProps.getKeystoreAlias();
			String password = secProps.getKeystorePassword();


			X509Certificate cert = secProps.getPublicKey();
			cert.getPublicKey().getEncoded();
			PrivateKey privateKey = (PrivateKey) manager.getKeyEntry(alias, password.toCharArray());

			UnicoreProxySOCKS5 ps5 = new UnicoreProxySOCKS5(proxyHost,cert,privateKey,session);
			ps5.setUserPasswd("", password);     
			session.setProxy(ps5);      

			// username and password will be given via UserInfo interface.
			UserInfo ui=new UnicoreUserInfo(ps5);
			session.setUserInfo(ui);

			session.connect();
			progress.worked(60);


			Channel channel=session.openChannel("shell");
			channel.connect();
			progress.worked(20);

			IBidirectionalConnection connection = new UnicoreBidirectionalConnection(channel.getInputStream(),channel.getOutputStream());
			TerminalConnection result = new TerminalConnection();
			result.setConnection(connection);

			result.setName(ps5.getUsername()+"@"+n.getName());
			result.setDescription("UNICORE SSH connection established.");
			return result;
		}
		catch(Exception e)
		{
			UnicoreTerminalSSHPlugin.log(Status.ERROR, "Unable to create SOCKS SSH connection", e);
			return null;
		}
		finally {
			if(progress != null) progress.done();
		}
	}

	public static JSch getJsch() {
		if(jsch == null)
		{
			jsch = new JSch();
			File hostFile = UnicoreTerminalSSHPlugin.getDefault().getStateLocation().append("known_hosts").toFile();
			try {
				if(!hostFile.exists()) hostFile.createNewFile();
				jsch.setKnownHosts(hostFile.getAbsolutePath());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return jsch;
	}


}
