/* -*-mode:java; c-basic-offset:2; indent-tabs-mode:nil -*- */
/*
Copyright (c) 2002-2008 ymnk, JCraft,Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright 
     notice, this list of conditions and the following disclaimer in 
     the documentation and/or other materials provided with the distribution.

  3. The names of the authors may not be used to endorse or promote products
     derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL JCRAFT,
INC. OR ANY CONTRIBUTORS TO THIS SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.fzj.unicore.rcp.terminal.ssh.socks;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.ui.IMemento;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.XMLMemento;
import org.unigrids.services.atomic.types.TextInfoType;
import org.unigrids.x2006.x04.services.tss.TargetSystemPropertiesDocument.TargetSystemProperties;

import com.jcraft.jsch.JSchException;

import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemFactoryNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.TargetSystemNode;
import de.fzj.unicore.rcp.terminal.TerminalConstants;
import de.fzj.unicore.rcp.terminal.UnicoreTerminalPlugin;

class Util implements TerminalConstants  // inherit some constants
{
	public static final String SSH_INFO_PREFIX = "SSHInfo-";

	
	public static TargetSystemProperties getTSSProps(Node node){

		TargetSystemNode targetSystem = null;
		if(node instanceof TargetSystemFactoryNode) 
		{
			TargetSystemFactoryNode targetSystemFactory = (TargetSystemFactoryNode) node;
			if(targetSystemFactory.getChildren().size() == 0) return null;
			targetSystem = (TargetSystemNode) targetSystemFactory.getChildren().get(0);
		}
		else if(node instanceof TargetSystemNode) 
		{
			targetSystem = (TargetSystemNode) node;
		}
		else return null;

		return targetSystem.getTargetSystemProperties();

	}
	
	public static Map<String,String> extractSSHRelatedProperties(TargetSystemProperties tssProps)
	{
		Map<String,String> sshInfo = new HashMap<String, String>();
		TextInfoType[] textInfo = tssProps.getTextInfoArray();
		for (TextInfoType textInfoType : textInfo) {
			String name = textInfoType.getName();
			if(name.startsWith(SSH_INFO_PREFIX)) sshInfo.put(name, textInfoType.getValue());
		}
		return sshInfo;
	}


	public static void saveConfig(Map<String,String> config, 
			NodePath serverAddress, String name){

		


		//here begins the part where data is written 
		//to XML storage file
		File savedFile = UnicoreTerminalPlugin.getDefault()
		.getStateLocation().append("SSHSettings.xml").toFile();
		System.out.println(savedFile.getAbsolutePath());

		try
		{
			Reader reader = new FileReader(savedFile);
			XMLMemento root;
			if (savedFile.exists()){
				root = XMLMemento.createReadRoot(reader);
			}
			else{
				root = XMLMemento.createWriteRoot("SSHSiteInformation");
			}

			root.getChildren("SSHSiteInformation");

			IMemento sSHSite = null;

			IMemento[] child = root.getChildren(ELEMENT_SSH_SITE);
			
			for( int i = 0; i < child.length; i++ ) {
				String id = child[i].getString(TerminalConstants.ID);
				if(id.equals(serverAddress.toString())){
					sSHSite = child[i];
					break;
				}                         
			}


			//			IMemento gsissh = sSHSite.createChild("GSISSH");
			//			gsissh.putString("gsi_supported",String.valueOf(gsissh_support));
			//			gsissh.putString("host",gsisshHost);
			//			gsissh.putString("port",String.valueOf(gsisshPort));



			Writer writer;
			try {
				writer = new FileWriter(savedFile);
				root.save(writer);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		catch ( IOException e ) {
			System.out.println("Error when serializing SSHData " +
			"properties" );
		} catch (WorkbenchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	static Socket createSocket(String host, int port, int timeout) throws JSchException{
		Socket socket=null;
		if(timeout==0){
			try{
				socket=new Socket(host, port);
				return socket;
			}
			catch(Exception e){
				String message=e.toString();
				if(e instanceof Throwable)
					throw new JSchException(message, (Throwable)e);
				throw new JSchException(message);
			}
		}
		final String _host=host;
		final int _port=port;
		final Socket[] sockp=new Socket[1];
		final Exception[] ee=new Exception[1];
		String message="";
		Thread tmp=new Thread(new Runnable(){
			public void run(){
				sockp[0]=null;
				try{
					sockp[0]=new Socket(_host, _port);
				}
				catch(Exception e){
					ee[0]=e;
					if(sockp[0]!=null && sockp[0].isConnected()){
						try{
							sockp[0].close();
						}
						catch(Exception eee){}
					}
					sockp[0]=null;
				}
			}
		});
		tmp.setName("Opening Socket "+host);
		tmp.start();
		try{ 
			tmp.join(timeout);
			message="timeout: ";
		}
		catch(java.lang.InterruptedException eee){
		}
		if(sockp[0]!=null && sockp[0].isConnected()){
			socket=sockp[0];
		}
		else{
			message+="socket is not established";
			if(ee[0]!=null){
				message=ee[0].toString();
			}
			tmp.interrupt();
			tmp=null;
			throw new JSchException(message);
		}
		return socket;
	} 

	static byte[] str2byte(String str, String encoding){
		if(str==null) 
			return null;
		try{ return str.getBytes(encoding); }
		catch(java.io.UnsupportedEncodingException e){
			return str.getBytes();
		}
	}

	static byte[] str2byte(String str){
		return str2byte(str, "UTF-8");
	}

	static String byte2str(byte[] str, String encoding){
		try{ return new String(str, encoding); }
		catch(java.io.UnsupportedEncodingException e){
			return new String(str);
		}
	}

	static String byte2str(byte[] str){
		return byte2str(str, "UTF-8");
	}

	static void bzero(byte[] foo){
		if(foo==null)
			return;
		for(int i=0; i<foo.length; i++)
			foo[i]=0;
	}

}
