<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="de.fzj.unicore.rcp.wfeditor" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appInfo>
         <meta.schema plugin="de.fzj.unicore.rcp.wfeditor" id="DataFlowExtensionPoint" name="DataFlowExtensionPoint"/>
      </appInfo>
      <documentation>
         [Enter description of this extension point.]
      </documentation>
   </annotation>

   <include schemaLocation="schema://org.eclipse.core.expressions/schema/expressionLanguage.exsd"/>

   <element name="extension">
      <annotation>
         <appInfo>
            <meta.element />
         </appInfo>
      </annotation>
      <complexType>
         <sequence>
            <element ref="assembler" minOccurs="0" maxOccurs="unbounded"/>
            <element ref="dataTypeMapping" minOccurs="0" maxOccurs="unbounded"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="canConnect">
      <annotation>
         <documentation>
            &lt;p&gt;Contains a core expression used by the &lt;code&gt;DataFlowFactory&lt;/code&gt; to determine whether this data flow assembler can create a data flow for the given data source and data sink.&lt;/p&gt;
         </documentation>
      </annotation>
      <complexType>
         <choice>
            <element ref="not"/>
            <element ref="and"/>
            <element ref="or"/>
            <element ref="instanceof"/>
            <element ref="test"/>
            <element ref="systemTest"/>
            <element ref="equals"/>
            <element ref="count"/>
            <element ref="with"/>
            <element ref="resolve"/>
            <element ref="adapt"/>
            <element ref="iterate"/>
            <element ref="reference"/>
         </choice>
      </complexType>
   </element>

   <element name="assembler">
      <annotation>
         <documentation>
            &lt;p&gt;Used when creating an &lt;code&gt;IExecutableExtension&lt;/code&gt; with a named parameter, or more than one.&lt;/p&gt;
         </documentation>
      </annotation>
      <complexType>
         <sequence>
            <element ref="canConnect"/>
         </sequence>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  &lt;p&gt;The assembler class that imlements &lt;code&gt;de.fzj.unicore.rcp.wfeditor.extensionpoints.IDataFlowAssembler&lt;/code&gt;.&lt;/p&gt;
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":de.fzj.unicore.rcp.wfeditor.extensionpoints.IDataFlowAssembler"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="dataTypeMapping">
      <annotation>
         <documentation>
            Define a mapping between a human readable name and an internal data type identifier which may identify a primitive type (e.g. &quot;Integer&quot;, &quot;String&quot;) or a complex type like &quot;File&quot; or &quot;XML&quot;. The internal identifier can be a nominal String that has to be matched exactly or a regular expression. For example, in order to identify files, the regular expression &quot;.*/.*&quot; is used: the data types for files are MIME types and these have the form &quot;type/subtype&quot;. The defined mapping is used in order to show/hide the different types of data sources, data sinks, and data flows when using the data flow creation tool. The human readable name is displayed in the palette and the internal identifier is used for matchmaking when deciding whether to show a certain source, sink or flow.
         </documentation>
      </annotation>
      <complexType>
         <sequence>
            <element ref="before" minOccurs="0" maxOccurs="unbounded"/>
            <element ref="after" minOccurs="0" maxOccurs="unbounded"/>
         </sequence>
         <attribute name="name" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
          <attribute name="pluralName" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         
         <attribute name="internalId" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="isRegExp" type="boolean">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="enabledByDefault" type="boolean">
            <annotation>
               <documentation>
                  States whether data sources/sinks/flows with this data type should be shown by default when the data flow creation tool is selected.
               </documentation>
            </annotation>
         </attribute>
              </complexType>
   </element>

   <element name="before">
      <annotation>
         <documentation>
            Used to specify the order in which the data type mapping names appear in the workflow editor&apos;s palette, below the data flow creation tool.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="mappingName" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="after">
      <complexType>
         <attribute name="mappingName" type="string" use="required">
            <annotation>
               <documentation>
                  Used to specify the order in which the data type mapping names appear in the workflow editor&apos;s palette, below the data flow creation tool.
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         [Enter the first release in which this extension point appears.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         [Enter extension point usage example here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="apiinfo"/>
      </appInfo>
      <documentation>
         [Enter API information here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         [Enter information about supplied implementation of this extension point.]
      </documentation>
   </annotation>


</schema>
