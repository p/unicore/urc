/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.ui.actions.Clipboard;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

/**
 * @author demuth
 */
public class PasteCommand extends Command {

	private StructuredActivity parent;
	private List<IFlowElement> elements;
	private Map<String, IActivity> copiedActivities;

	private CompoundCommand undoCommand;
	private Command relayoutCommand;
	private EditPartViewer viewer;

	private Dimension diff;
	public static Boolean executing = false;

	private boolean undone = false;

	ISelection oldSelection;
	ISelection newSelection;

	public PasteCommand(StructuredActivity parent) {
		super();
		this.parent = parent;
	}

	/**
	 * Cdalculate a vector to be added to all pasted elements' locations
	 * 
	 * @param elements
	 * @return
	 */
	protected Dimension calculateDiffToMouse(List<IFlowElement> elements) {

		Dimension diff = null;
		if (getViewer() != null) {
			Control editorControl = getViewer().getControl();
			Display d = editorControl.getDisplay();
			org.eclipse.swt.graphics.Point swtPoint = d.getCursorLocation();
			Control cursorControl = d.getCursorControl();
			if (cursorControl == editorControl) {
				swtPoint = editorControl.toControl(swtPoint);
				Point pasteLocation = new Point(swtPoint.x, swtPoint.y);
				GraphicalEditPart diagramPart = (GraphicalEditPart) getViewer()
						.getEditPartRegistry().get(parent.getDiagram());
				diagramPart.getFigure().translateToRelative(pasteLocation);
				Rectangle bounding = null;
				for (IFlowElement element : elements) {

					if (element instanceof IActivity) {
						IActivity act = (IActivity) element;
						Rectangle r = new Rectangle(act.getLocation(),
								act.getSize());
						bounding = bounding == null ? r : bounding.union(r);
					}
				}
				diff = pasteLocation.getDifference(bounding.getTop());
			}
		}
		return diff;
	}

	@Override
	public boolean canExecute() {
		// always claim we can execute
		// do this due to a bug (?) that
		// causes the paste command to be unavailable at times
		// inform the user about paste failure later in case the clipboard
		// elements
		// can't be pasted
		return true;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		if (viewer == null || executing) {
			return;
		}
		List<EditPart> selection = new ArrayList<EditPart>();
		executing = true;
		try {
			Object clipboard = Clipboard.getDefault().getContents();

			if (!(clipboard instanceof IFlowElement[])) {
				WFActivator
						.log(IStatus.ERROR,
								"Unable to paste the clipboard contents into this diagram.");
			} else if (!getParent().canAddChildren()) {
				WFActivator
						.log(IStatus.ERROR,
								"Unable to paste the clipboard contents into this element.");
			} else {
				elements = Arrays.asList((IFlowElement[]) clipboard);

				// check whether elements can be pasted
				for (IFlowElement element : elements) {
					if (element instanceof IActivity
							&& !parent.canAddChild((IActivity) element)) {
						WFActivator
								.log(IStatus.ERROR,
										"Unable to paste the clipboard contents into this element.");
						return;
					}

				}

				// get mouse location: if possible, paste under mouse
				diff = calculateDiffToMouse(elements);

				copiedActivities = new HashMap<String, IActivity>();
				List<Command> undoCommandsInReverseOrder = new ArrayList<Command>();
				Map<Object, Object> mapOfCopies = new HashMap<Object, Object>();
				Set<IFlowElement> toBeCopied = new HashSet<IFlowElement>(
						elements);

				int copyFlags = ICopyable.FLAG_NONE;
				for (IFlowElement element : elements) {
					if (element.isCut()) {
						copyFlags = ICopyable.FLAG_CUT;
					}
					IFlowElement copy = CloneUtils.getFromMapOrCopy(element,
							toBeCopied, mapOfCopies, copyFlags);
					if (copy == null) {
						continue;
					}
					if (copy.getDiagram() != parent.getDiagram()) {
						copy.setDiagram(parent.getDiagram()); // pasted from
																// different
																// diagram
					}
					if (element.isCut()) {
						copy.setCut(false);
						SetPropertyCommand setCutCommand = new SetPropertyCommand(
								element, IFlowElement.PROP_CUT, false);
						setCutCommand.execute();
						undoCommandsInReverseOrder.add(new ReverseCommand(
								setCutCommand));
					}

					if (element instanceof IActivity) {
						IActivity activity = (IActivity) copy;
						if (diff != null) {
							activity.move(diff.width, diff.height);
						}
						copiedActivities.put(element.getID(), activity);
						getParent().addChild(activity);

						DeleteCommand del = new DeleteCommand();
						del.setParent(getParent());
						del.setChild(activity);
						undoCommandsInReverseOrder.add(del);
					}

					if (getViewer() != null) {
						EditPart part = (EditPart) getViewer()
								.getEditPartRegistry().get(copy);
						if (part != null) {
							selection.add(part);
						}
					}

				}

				// now insert copied objects into the diagram
				// this needs to be done in the correct order
				// => sort with binary insertion sort algorithm
				List<ICopyable> keys = new ArrayList<ICopyable>();
				for (Object o : mapOfCopies.keySet()) {
					if (o instanceof ICopyable) {
						ICopyable copyable = (ICopyable) o;
						insertCopyable(keys, copyable);
					}
				}

				for (ICopyable key : keys) {
					key.insertCopyIntoDiagram((ICopyable) mapOfCopies.get(key),
							parent, toBeCopied, mapOfCopies, copyFlags);
				}

				if (getViewer() != null) {

					relayoutCommand = new AdjustLayoutCommand(copiedActivities
							.values().iterator().next(), viewer);
					relayoutCommand.execute();
					undoCommandsInReverseOrder.add(new ReverseCommand(
							relayoutCommand));
				}

				undoCommand = new CompoundCommand();
				for (int i = undoCommandsInReverseOrder.size() - 1; i >= 0; i--) {
					undoCommand.add(undoCommandsInReverseOrder.get(i));
				}

			}

		} finally {
			executing = false;
		}
		if (getViewer() != null) {
			IStructuredSelection s = new StructuredSelection(selection);
			oldSelection = getViewer().getSelection();
			getViewer().setSelection(s);
			newSelection = s;
		}
	}

	/**
	 * Returns the StructuredActivity that is the parent
	 * 
	 * @return the parent
	 */
	public StructuredActivity getParent() {
		return parent;
	}

	public EditPartViewer getViewer() {
		return viewer;
	}

	private void insertCopyable(List<ICopyable> list, ICopyable copyable) {
		if (list.size() == 0) {
			list.add(copyable);
			return;
		}
		int low = 0;
		int high = -1;
		for (int i = 0; i < list.size(); i++) {
			int cmp = compare(copyable, list.get(i));
			if (cmp > 0) {
				low = i;
			} else if (cmp < 0 && high == -1) {
				high = i;
			}

		}
		list.add(low + 1, copyable);
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void redo() {
		if (elements == null || viewer == null || executing) {
			return;
		}
		executing = true;
		try {
			if (undoCommand != null) {
				undoCommand.undo();
			}
			if (newSelection != null) {
				viewer.setSelection(newSelection);
			}
		} finally {
			executing = false;
		}

	}

	/**
	 * Sets the parent to the passed StructuredActiivty
	 * 
	 * @param newParent
	 *            the parent
	 */
	public void setParent(StructuredActivity newParent) {
		parent = newParent;
	}

	public void setViewer(EditPartViewer viewer) {
		this.viewer = viewer;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		if (undoCommand != null) {
			if (!undone) {
				undoCommand.execute();
				undone = true;
			} else {
				undoCommand.redo();
			}
		}
		if (oldSelection != null) {
			viewer.setSelection(oldSelection);
		}
	}

	private static int compare(ICopyable c1, ICopyable c2) {

		Set<Class<?>> after = c1.insertAfter();
		for (Class<?> clazz : after) {
			if (clazz.isAssignableFrom(c2.getClass())) {
				return 1;
			}
		}
		Set<Class<?>> before = c1.insertBefore();
		for (Class<?> clazz : before) {
			if (clazz.isAssignableFrom(c2.getClass())) {
				return -1;
			}
		}
		after = c2.insertAfter();
		for (Class<?> clazz : after) {
			if (clazz.isAssignableFrom(c1.getClass())) {
				return -1;
			}
		}
		before = c2.insertBefore();
		for (Class<?> clazz : before) {
			if (clazz.isAssignableFrom(c1.getClass())) {
				return 1;
			}
		}
		return 0;

	}

}
