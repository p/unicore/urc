/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import java.util.List;

import org.eclipse.draw2d.Bendpoint;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.BendpointRequest;
import org.eclipse.gef.requests.ChangeBoundsRequest;

import de.fzj.unicore.rcp.wfeditor.model.commands.BendpointCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.CreateBendpointCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.DeleteBendpointCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.MoveAllBendpointsCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.MoveBendpointCommand;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;

public class TransitionBendpointEditPolicy extends
		org.eclipse.gef.editpolicies.BendpointEditPolicy {

	@Override
	public Command getCommand(Request request) {
		if (REQ_MOVE.equals(request.getType())) {
			return getMoveAllBendpointsCommand((ChangeBoundsRequest) request);
		} else {
			return super.getCommand(request);
		}

	}

	@Override
	protected Command getCreateBendpointCommand(BendpointRequest request) {
		CreateBendpointCommand com = new CreateBendpointCommand();
		Point p = request.getLocation();
		Connection conn = getConnection();

		conn.translateToRelative(p);

		com.setLocation(p);
		com.setTransition((Transition) request.getSource().getModel());
		com.setIndex(request.getIndex());
		return com;
	}

	@Override
	protected Command getDeleteBendpointCommand(BendpointRequest request) {
		BendpointCommand com = new DeleteBendpointCommand();
		Point p = request.getLocation();
		com.setLocation(p);
		com.setTransition((Transition) request.getSource().getModel());
		com.setIndex(request.getIndex());
		return com;
	}

	protected Point getDeltaFor(ChangeBoundsRequest request,
			GraphicalEditPart child) {
		Point ref = child.getFigure().getBounds().getTopLeft();
		Point p = ref.getCopy();
		child.getFigure().translateToAbsolute(p);
		p.translate(request.getMoveDelta());
		child.getFigure().translateToRelative(p);
		// rect.translate(getLayoutOrigin().getNegated());

		return p.translate(ref.getNegated());
	}

	protected Command getMoveAllBendpointsCommand(ChangeBoundsRequest request) {
		Transition t = (Transition) getHost().getModel();
		boolean hasBendpoints = t.getBendpoints() != null
				&& t.getBendpoints().size() > 0;
		if (!hasBendpoints) {
			return new Command() {
			}; // do nothing
		}
		MoveAllBendpointsCommand result = new MoveAllBendpointsCommand(t,
				getDeltaFor(request, (GraphicalEditPart) getHost()));
		return result;
	}

	@Override
	protected Command getMoveBendpointCommand(BendpointRequest request) {
		MoveBendpointCommand com = new MoveBendpointCommand();
		Point p = request.getLocation();
		Connection conn = getConnection();
		conn.translateToRelative(p);
		com.setLocation(p);
		com.setTransition((Transition) request.getSource().getModel());
		com.setIndex(request.getIndex());
		return com;
	}

	@Override
	public boolean understandsRequest(Request req) {
		if (REQ_MOVE.equals(req.getType())) {
			return true;
		}
		return super.understandsRequest(req);
	}
	
	@Override
	protected void showMoveBendpointFeedback(BendpointRequest request) {
		// there seems to be a bug in the parent class leading to an IndexOutOfBoundsException
		// try to work around
		
		PointList points = getConnection().getPoints();
		int bpIndex = -1;
		List bendPoints = (List) getConnection().getRoutingConstraint();
		if(request.getIndex() >= bendPoints.size())
		{
			bendPoints.add(null);
			showCreateBendpointFeedback(request);
		}
		Point bp = ((Bendpoint) bendPoints.get(request.getIndex()))
				.getLocation();

		int smallestDistance = -1;

		for (int i = 0; i < points.size(); i++) {
			if (smallestDistance == -1
					|| points.getPoint(i).getDistance2(bp) < smallestDistance) {
				bpIndex = i;
				smallestDistance = points.getPoint(i).getDistance2(bp);
				if (smallestDistance == 0)
					break;
			}
		}
		if(bpIndex > 0 && bpIndex < points.size()-1) super.showMoveBendpointFeedback(request);
	}

}
