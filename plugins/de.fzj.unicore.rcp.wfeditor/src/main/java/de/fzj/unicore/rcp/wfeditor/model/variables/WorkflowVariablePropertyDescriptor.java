package de.fzj.unicore.rcp.wfeditor.model.variables;

import org.eclipse.ui.views.properties.PropertyDescriptor;

public class WorkflowVariablePropertyDescriptor extends PropertyDescriptor {

	protected WorkflowVariable variable;

	WorkflowVariablePropertyDescriptor(WorkflowVariable v) {
		super(v.getId(), v.getName());
		this.variable = v;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof WorkflowVariablePropertyDescriptor)) {
			return false;
		} else {
			return ((WorkflowVariablePropertyDescriptor) o).variable.getId() == variable
					.getId();
		}
	}

	@Override
	public String getDisplayName() {
		return variable.getName();
	}
}
