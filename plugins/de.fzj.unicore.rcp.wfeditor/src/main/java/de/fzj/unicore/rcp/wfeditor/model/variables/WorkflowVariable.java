/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.variables;

import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.eclipse.ui.views.properties.TextPropertyDescriptor;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IDisposableWithUndo;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSource;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.model.structures.WhileActivity;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;
import de.fzj.unicore.rcp.wfeditor.utils.PropertyMapComparator;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowUtils;

/**
 * @author demuth
 * 
 */
@XStreamAlias("WorkflowVariable")
public class WorkflowVariable extends DataSource implements IDataSource,
		IDisposableWithUndo, ICopyable {

	public static final QName TYPE = new QName("http://www.unicore.eu/",
			"WorkflowVariable");

	/**
	 * 
	 */
	private static final long serialVersionUID = 9183655788469605426L;

	/**
	 * Types that may be declared for workflow variables. Caution: floats are
	 * only named "Float" for better usability, internally, they are modeled as
	 * doubles!
	 */
	public static final String VARIABLE_TYPE_INTEGER = "Integer",
			VARIABLE_TYPE_STRING = "String", VARIABLE_TYPE_FLOAT = "Float";

	public static final String[] VARIABLE_TYPES = { VARIABLE_TYPE_INTEGER,
			VARIABLE_TYPE_STRING, VARIABLE_TYPE_FLOAT };

	public static final String SYMBOL_FOR_INTEGER = "1,2..";

	public static final String SYMBOL_FOR_STRING = "abc";

	public static final String SYMBOL_FOR_FLOAT = "3.14";

	public static final String[] SYMBOLS = { SYMBOL_FOR_INTEGER,
			SYMBOL_FOR_STRING, SYMBOL_FOR_FLOAT };

	// keys defining property names
	public static String PROP_PROTOCOL = "protocol", PROP_ID = "id",
			PROP_NAME = "Name", PROP_INITIAL_VALUE = "Initial value",
			PROP_TYPE = "type", PROP_FLOW_ELEMENT = "element";

	public WorkflowVariable() {
	}

	public WorkflowVariable(IFlowElement flowElement) {
		this(flowElement.getDiagram().getUniqueVariableName("Variable"),
				VARIABLE_TYPES[0], "0", flowElement);
	}

	public WorkflowVariable(String name, String type, String initialValue,
			IFlowElement flowElement) {
		setId(createRandomId());
		setName(name);
		setType(type);
		setInitialValue(initialValue);
		setFlowElement(flowElement);

	}

	@Override
	public void activate() {
		super.activate();
		if (getDiagram() != null && getDiagram().getVariable(getName()) != this) {
			getDiagram().addVariable(this);
		}
	}

	@Override
	public void addPersistentPropertyChangeListener(PropertyChangeListener l) {
		super.addPersistentPropertyChangeListener(l);
	}

	@Override
	protected void addPropertyDescriptors() {
		addPropertyDescriptor(new TextPropertyDescriptor(PROP_NAME, PROP_NAME));
		addPropertyDescriptor(new TextPropertyDescriptor(PROP_INITIAL_VALUE,
				PROP_INITIAL_VALUE));
	}

	@Override
	public void dispose() {
		super.dispose();

		if (getDiagram() != null) {
			getDiagram().removeVariable(this);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof WorkflowVariable)) {
			return false;
		}
		WorkflowVariable other = (WorkflowVariable) o;
		if (this == other) {
			return true;
		}
		return PropertyMapComparator.equal(getProperties(),
				other.getProperties());

	}

	@Override
	public WorkflowVariable getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		WorkflowVariable copy = (WorkflowVariable) super.getCopy(toBeCopied,
				mapOfCopies, copyFlags);
		copy.setFlowElement(CloneUtils.getFromMapOrCopy(getFlowElement(),
				toBeCopied, mapOfCopies, copyFlags));
		if ((copyFlags & ICopyable.FLAG_CUT) != 0) {
			// keep our name and id
			// do NOT use setName or setPropertyValue here, as this will
			// result in a failed validity check
			copy.properties.put(PROP_NAME, getName());
			copy.setId(getId());
		} else {
			copy.setName(copy.getFlowElement().getDiagram()
					.getUniqueVariableName("Variable"));
			copy.setId(createRandomId());
		}

		return copy;
	}

	public WorkflowDiagram getDiagram() {
		if (getFlowElement() != null) {
			return getFlowElement().getDiagram();
		}
		return null;
	}

	@Override
	public IFlowElement getFlowElement() {
		return (IFlowElement) getPropertyValue(PROP_FLOW_ELEMENT);
	}

	/**
	 * A unique ID for referencing this Object.
	 */
	@Override
	public String getId() {
		return (String) getPropertyValue(PROP_ID);
	}

	public String getInitialValue() {
		return (String) getPropertyValue(PROP_INITIAL_VALUE);
	}

	@Override
	public String getName() {
		return (String) getPropertyValue(PROP_NAME);
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_INITIAL_VALUE);
		propertiesToCopy.add(PROP_PROTOCOL);
		propertiesToCopy.add(PROP_TYPE);
		return propertiesToCopy;
	}

	public QName getProtocol() {
		return (QName) getPropertyValue(PROP_PROTOCOL);
	}

	@Override
	public QName getSourceType() {
		return TYPE;
	}

	public String getType() {
		return (String) getPropertyValue(PROP_TYPE);
	}

	@Override
	public int hashCode() {
		return PropertyMapComparator.hashCode(getProperties());
	}

	public boolean isVisible() {
		IFlowElement element = getFlowElement();
		if (element == null) {
			return false;
		}
		ShowDataFlowsProperty prop = (ShowDataFlowsProperty) element
				.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		if (prop == null) {
			return false;
		}
		if (!prop.isShowingSources()) {
			return false;
		}
		return getOutgoingDataTypes().size() == 0
				|| prop.isShowingFlowsWithDataTypes(getOutgoingDataTypes());
	}

	/**
	 * Check whether the variable'S name can be set to the given value. If so,
	 * return null. If not, return a String describing why the name is invalid.
	 * 
	 * @param name
	 *            The name to be checked
	 * @return
	 */
	public String nameValid(String name) {
		String check = WorkflowUtils.isAllowedAsVariableName(name);
		if (check != null) {
			return "Unable to change variable name: " + check;
		}
		if (getDiagram() == null || name.equals(getName())
				|| getDiagram().isUniqueVariableName(name)) {
			return null;
		}
		return "A variable with this name already exists.";
	}

	private Object readResolve() {
		return this;
	}

	@Override
	public void removePersistentPropertyChangeListener(PropertyChangeListener l) {
		super.removePersistentPropertyChangeListener(l);
	}

	@Override
	public void setFlowElement(IFlowElement element) {
		setPropertyValue(PROP_FLOW_ELEMENT, element);
	}

	public void setId(String id) {
		setPropertyValue(PROP_ID, id);
	}

	public void setInitialValue(String initialValue) {
		setPropertyValue(PROP_INITIAL_VALUE, initialValue);
	}

	@Override
	public void setName(String name) {

		setPropertyValue(PROP_NAME, name);
	}

	@Override
	public void setPropertyValue(Object key, Object value) {
		if (PROP_TYPE.equals(key)) {
			Set<String> dataTypes = getOutgoingDataTypes();
			if (dataTypes == null) {
				dataTypes = new HashSet<String>();
			} else {
				dataTypes.clear();
			}
			dataTypes.add((String) value);
			super.setPropertyValue(PROP_PROVIDED_DATA_TYPES, dataTypes);
		}
		if (getDiagram() != null && PROP_NAME.equals(key)) {
			String newName = (String) value;
			getDiagram().changeVariableName(this, getName(), newName);
			super.setPropertyValue(key, value);
		} else {
			super.setPropertyValue(key, value);
		}
	}

	public void setProtocol(QName protocol) {
		setPropertyValue(PROP_PROTOCOL, protocol);
	}

	public void setType(String type) {
		setPropertyValue(PROP_TYPE, type);
	}

	@Override
	public String toString() {
		return "";
	}

	@Override
	public void undoDispose() {

		if (getDiagram() != null) {
			getDiagram().addVariable(this);
		}
		super.undoDispose();
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		if (updatedToCurrentModelVersion) {
			return; // don't do this twice!
		}
		super.updateToCurrentModelVersion(oldVersion, currentVersion);
		if (oldVersion.compareTo("6.4.0") < 0) {
			if (getFlowElement() instanceof WhileActivity) {
				// this was a bug prior to 6.4.0
				WhileActivity act = (WhileActivity) getFlowElement();
				if (act.getIterationCounter() == this) {
					setFlowElement(act.getStartActivity());
				}
			}
			setPropertyValue(PROP_TYPE, getType()); // initialize data types
													// correctly
			getFlowElement().getDataSourceList().addDataSource(this);

		}
	}

	public static int getIndexFromTypeName(String typeName) {
		for (int i = 0; i < VARIABLE_TYPES.length; i++) {
			if (VARIABLE_TYPES[i].equals(typeName)) {
				return i;
			}
		}
		return -1;
	}

	public static String getSymbolFromTypeName(String typeName) {
		return SYMBOLS[getIndexFromTypeName(typeName)];
	}

}
