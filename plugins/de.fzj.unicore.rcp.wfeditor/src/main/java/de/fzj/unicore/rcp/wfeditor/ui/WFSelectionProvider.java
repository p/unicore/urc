package de.fzj.unicore.rcp.wfeditor.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.GraphicalViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;


public class WFSelectionProvider implements ISelectionProvider, ISelectionChangedListener {
	
	private GraphicalViewer[] viewers;
	private ISelection currentSelection = new StructuredSelection();

	public WFSelectionProvider(GraphicalViewer... viewers) {
		super();
		this.viewers = viewers;
		for(GraphicalViewer v : viewers)
		{
			v.addSelectionChangedListener(this);
		}
	}
	
	
	private List<ISelectionChangedListener> fListeners= new ArrayList<ISelectionChangedListener>(5);

	public void addSelectionChangedListener(ISelectionChangedListener listener) {
		fListeners.add(listener);
	}

	public ISelection getSelection() {
		return currentSelection;
	}

	public void removeSelectionChangedListener(ISelectionChangedListener listener) {
		fListeners.remove(listener);
	}

	public void setSelection(ISelection selection) {
		viewers[0].setSelection(selection);
	}

	public void selectionChanged(SelectionChangedEvent event) {
		// forward to my listeners
		SelectionChangedEvent wrappedEvent= new SelectionChangedEvent(this, event.getSelection());
		currentSelection = wrappedEvent.getSelection();
		for (Iterator<ISelectionChangedListener> listeners= fListeners.iterator(); listeners.hasNext();) {
			ISelectionChangedListener listener= listeners.next();
			listener.selectionChanged(wrappedEvent);
		}
	}

	
}
