package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;

public class TransitionTargetDecoration extends PolygonDecoration {

	GraphicalEditPart targetPart;

	public TransitionTargetDecoration(GraphicalEditPart targetPart) {
		super();
		this.targetPart = targetPart;
	}

	@Override
	public void paintFigure(Graphics graphics) {
		Rectangle bounds = new Rectangle(getBounds());
		if (targetPart != null && targetPart.getFigure() != null
				&& targetPart.getFigure().getParent() != null) {
			bounds = bounds.intersect(targetPart.getFigure().getParent()
					.getClientArea());
			Rectangle originalClip = new Rectangle();
			graphics.getClip(originalClip);
			graphics.clipRect(bounds);
			super.paintFigure(graphics);
			graphics.clipRect(originalClip);
		} else {
			super.paintFigure(graphics);
		}

	}

	public void setTargetPart(GraphicalEditPart targetPart) {
		this.targetPart = targetPart;
	}
}
