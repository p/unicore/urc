package de.fzj.unicore.rcp.wfeditor.model.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

@XStreamAlias("DataSource")
@SuppressWarnings("unchecked")
public abstract class DataSource extends PropertySource implements IDataSource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7237110566305573380L;
	private transient Map<String, IDataFlow> flowMap = new HashMap<String, IDataFlow>();
	public static final QName TYPE = new QName("http://www.unicore.eu/",
			"DataSource");

	private transient IDataFlow[] disposedFlows = null;
	private DataSourceList parentList = null;

	public DataSource() {
		this(new HashSet<String>());
	}

	public DataSource(Set<String> dataTypes) {
		setPropertyValue(PROP_PROVIDED_DATA_TYPES, dataTypes);
		activate();
	}

	public void addOutgoingFlow(IDataFlow flow) {
		addOutgoingFlow(flow, -1);
	}

	public void addOutgoingFlow(IDataFlow flow, int index) {
		if (flow == null || getOutgoingFlowById(flow.getId()) != null) {
			return;
		}
		List<IDataFlow> flows = (List<IDataFlow>) getPropertyValue(PROP_DATA_FLOWS);
		if (flows == null) {
			flows = new ArrayList<IDataFlow>();
		}
		if (index < 0) {
			index = flows.size() + index;
		}
		flows.add(flow);
		getFlowMap().put(flow.getId(), flow);
		setPropertyValue(PROP_DATA_FLOWS, flows);

	}

	@Override
	protected void addPropertyDescriptors() {

	}

	public int compareTo(IDataSource o) {
		int result = getSourceType().getLocalPart().compareTo(
				o.getSourceType().getLocalPart());
		if (result != 0) {
			return result;
		}
		if (getName() == null) {
			return 0;
		} else {
			return getName().compareTo(o.getName());
		}
	}

	@Override
	public void dispose() {
		super.dispose();
		synchronized (properties) {
			disposedFlows = getOutgoingFlows();
			for (IDataFlow flow : disposedFlows) {
				if (flow.isConnected()) {
					flow.disconnect();
				}
			}
			if (getParentList() != null) {
				getParentList().removeDataSource(this);
			}
		}
	}

	@Override
	public DataSource getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		DataSource copy = (DataSource) super.getCopy(toBeCopied, mapOfCopies,
				copyFlags);
		copy.setFlowElement(CloneUtils.getFromMapOrCopy(getFlowElement(),
				toBeCopied, mapOfCopies, copyFlags));
		copy.setParentSourceList(CloneUtils.getFromMapOrCopy(getParentList(),
				toBeCopied, mapOfCopies, copyFlags));
		copy.setPropertyValue(PROP_PROVIDED_DATA_TYPES, new HashSet<String>(
				getOutgoingDataTypes()));
		copy.flowMap = null;
		for (IDataFlow flow : getOutgoingFlows()) {
			CloneUtils.getFromMapOrCopy(flow,
					toBeCopied, mapOfCopies, copyFlags);

			// do NOT add flows to this source or the sink here!!
			// this leads to adding the copied flow to the diagram
			// immediately which is NOT desired!

		}
		return copy;
	}

	public abstract IFlowElement getFlowElement();

	private Map<String, IDataFlow> getFlowMap() {
		if (flowMap == null) {
			restoreFlowMap();
		}
		return flowMap;
	}

	public abstract String getId();

	public String getName() {
		return (String) getPropertyValue(PROP_NAME);
	}

	public Set<String> getOutgoingDataTypes() {
		return (Set<String>) getPropertyValue(PROP_PROVIDED_DATA_TYPES);
	}

	public IDataFlow getOutgoingFlowById(String id) {
		return getFlowMap().get(id);
	}

	public IDataFlow[] getOutgoingFlows() {
		List<IDataFlow> flows = (List<IDataFlow>) getPropertyValue(PROP_DATA_FLOWS);
		if (flows == null) {
			return new IDataFlow[0];
		} else {
			return flows.toArray(new IDataFlow[flows.size()]);
		}
	}

	public DataSourceList getParentList() {
		return parentList;
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_NAME);
		return propertiesToCopy;
	}

	public QName getSourceType() {
		return TYPE;
	}

	public boolean hasOutgoingFlow(IDataFlow flow) {
		return getFlowMap().values().contains(flow);
	}

	private Object readResolve() {
		return this;
	}

	public int removeOutgoingFlow(IDataFlow flow) {
		List<IDataFlow> flows = (List<IDataFlow>) getPropertyValue(PROP_DATA_FLOWS);
		int result = flows == null ? -1 : flows.indexOf(flow);
		if (result >= 0) {
			flows.remove(result);
			getFlowMap().remove(flow.getId());
			setPropertyValue(PROP_DATA_FLOWS, flows);
		}
		return result;
	}

	private void restoreFlowMap() {
		flowMap = new HashMap<String, IDataFlow>();
		for (IDataFlow flow : getOutgoingFlows()) {
			flowMap.put(flow.getId(), flow);
		}
	}

	public abstract void setFlowElement(IFlowElement element);

	public void setName(String name) {
		setPropertyValue(PROP_NAME, name);
	}

	public void setParentSourceList(DataSourceList parentList) {
		this.parentList = parentList;
	}

	@Override
	public void setState(int state) {
		super.setState(state);
	}

	@Override
	public void undoDispose() {
		super.undoDispose();

		for (IDataFlow flow : disposedFlows) {
			if (!flow.isConnected()) {
				flow.reconnect();
			}
		}
		if (getParentList() != null) {
			getParentList().addDataSource(this);
		}

	}

}
