/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.ui;

import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import de.fzj.unicore.rcp.common.utils.ValidityChangeEvent;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.extensions.CommonConditionTypeControl;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.conditions.CollectVariablesVisitor;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.model.conditions.ConditionTypeVariableComparison;
import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.PredecessorTraverser;

/**
 * @author demuth
 * 
 */
public class ConditionTypeVariableComparisonControl extends CommonConditionTypeControl {

	private boolean variableSelected = false;
	private ConditionTypeVariableComparison comparison;
	private CCombo compare;
	private List<WorkflowVariable> variables;
	/**
	 * 
	 */
	public ConditionTypeVariableComparisonControl(Composite parent,
			Condition value) {
		super(parent, SWT.NONE);
		setBackground(parent.getBackground());
		comparison = (ConditionTypeVariableComparison) value.getSelectedType();
		comparison.setCondition(value);
		GridLayout layout = new GridLayout();
		layout.numColumns = 4;
		this.setLayout(layout);
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		Label label = new Label(this, SWT.NONE);
		label.setText("of Variable");
		label.setBackground(getBackground());
		label.setLayoutData(data);
		controls.add(label);

		initVariableSelection(value);
		initCompareOperator();
		initTextField();

		errorMsg = new Label(this, SWT.NONE);
		data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);
		data.horizontalSpan = 4;
		errorMsg.setLayoutData(data);
		errorMsg.setForeground(parent.getDisplay()
				.getSystemColor(SWT.COLOR_RED));
		errorMsg.setBackground(getBackground());
		controls.add(errorMsg);
		data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);
		this.setLayoutData(data);
		pack();
	}

	protected void fireValidityEvent(boolean newValidity) {
		ValidityChangeEvent evt = new ValidityChangeEvent(this, newValidity);
		for (IConditionChangeListener l : listeners) {
			l.validityChanged(evt);
		}
	}

	protected void fireValueChange() {
		ValueChangeEvent evt = new ValueChangeEvent(comparison.getCondition());
		for (IConditionChangeListener l : listeners) {
			l.valueChanged(evt);
		}
	}

	protected void initCompareOperator() {
		compare = new CCombo(this, SWT.FLAT | SWT.BORDER | SWT.READ_ONLY);
		String[] items = null;
		if (comparison.getVariable() != null
				&& WorkflowVariable.VARIABLE_TYPE_STRING.equals(comparison
						.getVariable().getType())) {
			items = IConditionType.STRING_COMPARATORS;
		} else {
			items = IConditionType.COMPARATORS;
		}
		compare.setItems(items);

		GridData data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);
		data.grabExcessHorizontalSpace = false;
		compare.setLayoutData(data);
		compare.select(comparison.getComparator());
		compare.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean oldValidity = isValid();
				int oldSelection = comparison.getComparator();
				int selection = ((CCombo) e.getSource()).getSelectionIndex();
				comparison.setComparator(selection);
				if (oldSelection != selection) {
					fireValueChange();
				}
				if (oldValidity != isValid()) {
					fireValidityEvent(isValid());
				}
			}
		});
		controls.add(compare);
	}

	protected void initTextField() {

		Text text = new Text(this, SWT.SINGLE | SWT.BORDER);
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		text.setLayoutData(data);
		String s = String.valueOf(comparison.getValue());
		while (s.length() < 4) {
			s = " " + s;
		}
		text.setText(s);
		text.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				boolean oldValidity = isValid();
				String s = ((Text) e.getSource()).getText();
				s = s.trim();
				if (WorkflowVariable.VARIABLE_TYPE_INTEGER.equals(comparison
						.getVariable())) {
					try {
						Integer.parseInt(s.trim());
						if (!s.equals(comparison.getValue())) {
							comparison.setValue(s);
						}
						valueEntered = true;
						errorMsg.setText("");
					} catch (Exception exc) {
						valueEntered = false;
						errorMsg.setText("Unable to parse input, please enter an integer value!");
						errorMsg.pack();
					}
				} else {
					if (!s.equals(comparison.getValue())) {
						comparison.setValue(s);
					}
					valueEntered = true;
					errorMsg.setText("");
				}
				fireValueChange();
				boolean newValidity = isValid();
				if (oldValidity != newValidity) {
					fireValidityEvent(newValidity);
				}

			}
		});
		controls.add(text);
	}

	protected void initVariableSelection(Condition value) {
		final CCombo variableNames = new CCombo(this, SWT.FLAT | SWT.BORDER
				| SWT.READ_ONLY);
		GridData data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);
		variableNames.setLayoutData(data);
		WorkflowDiagram diagram = value.getDefiningElement().getDiagram();

		IGraphTraverser traverser = new PredecessorTraverser();
		CollectVariablesVisitor visitor = new CollectVariablesVisitor();

		try {
			traverser.traverseGraph(diagram, value.getDefiningElement(),
					visitor);
		} catch (Exception e) {
			WFActivator
					.log(IStatus.ERROR,
							"Unexpected exception while searching for workflow variables",
							e);
		}
		variables = visitor.getFound();
		String[] names = new String[] { "no variables found" };
		int selection = -1;
		if (variables.size() > 0) {

			names = new String[variables.size()];
			int i = 0;
			for (WorkflowVariable variable : variables) {
				if (variable.equals(comparison.getVariable())) {
					selection = i;
				}
				names[i++] = variable.getName();
			}
			if (comparison.getVariable() == null) {
				selection = -1;
			}

		} else {
			comparison.setVariable(null);
			selection = -1;
		}
		variableSelected = selection > -1;
		variableNames.setItems(names);
		variableNames.select(selection);
		controls.add(variableNames);
		variableNames.addSelectionListener(new SelectionListener() {
			private void updateSelection() {
				boolean oldValidity = isValid();
				WorkflowVariable oldSelection = comparison.getVariable();
				int selection = variableNames.getSelectionIndex();
				variableSelected = selection > -1 && selection < variables.size();
				WorkflowVariable newSelection = variableSelected ? variables
						.get(selection) : null;
				comparison.setVariable(newSelection);
				boolean selectionChanged = oldSelection == null ? newSelection != null
						: !oldSelection.equals(newSelection);
				String[] oldItems = compare.getItems();
				if (selectionChanged) {
					String[] items = null;
					if (WorkflowVariable.VARIABLE_TYPE_STRING
							.equals(newSelection.getType())) {
						items = IConditionType.STRING_COMPARATORS;
					} else {
						items = IConditionType.COMPARATORS;
					}
					if (oldItems == null || oldItems.length != items.length) {
						compare.setItems(items);
						compare.select(0);
						comparison.setComparator(0);
					}
					fireValueChange();
					if (oldValidity != isValid()) {
						fireValidityEvent(isValid());
					}
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				updateSelection();
			}

			public void widgetSelected(SelectionEvent e) {
				updateSelection();
			}
		});
	}

	public boolean isValid() {
		return variableSelected && valueEntered;
	}

}
