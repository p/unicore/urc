/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.CompoundDirectedGraphLayout;
import org.eclipse.draw2d.graph.Subgraph;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.layouts.IAutomaticGraphLayout;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.parts.StructuredActivityPart;

/**
 * @author demuth
 * 
 */
public class ComputeAndApplyLayoutCommand extends Command {

	private StructuredActivity container;
	private EditPartViewer viewer;
	private Command internalCommand;
	private int flags;

	public ComputeAndApplyLayoutCommand(StructuredActivity container,
			EditPartViewer viewer) {
		this.container = container;
		this.viewer = viewer;
	}

	@Override
	public void execute() {
		Object o = viewer.getEditPartRegistry().get(container);
		if (o instanceof StructuredActivityPart) {
			StructuredActivityPart part = (StructuredActivityPart) o;
			CompoundCommand resetInnerPaddingsCmd = new CompoundCommand();
			boolean resetInnerPaddings = (flags & IAutomaticGraphLayout.FLAG_RESET_INNER_PADDINGS) != 0;
			if(resetInnerPaddings)
			{
				Queue<StructuredActivity> q = new ConcurrentLinkedQueue<StructuredActivity>();
				q.add(part.getModel());
				
				while(!q.isEmpty())
				{
					StructuredActivity next = q.poll();
					resetInnerPaddingsCmd.add(new SetInnerPaddingCommand(next, null));
					for(IActivity a : next.getChildren())
					{
						if(a instanceof StructuredActivity) q.add((StructuredActivity) a);
					}
				}
				resetInnerPaddingsCmd.execute();
			}
			CompoundDirectedGraph graph = new CompoundDirectedGraph();
			graph.setDefaultPadding(WFConstants.BETWEEN_ACTIVITY_PADDING);
			graph.setMargin(WFConstants.ZERO_INSETS);
			Map<Object,Object> partsToNodes = new HashMap<Object,Object>();
			Subgraph sub = new Subgraph("a");
			graph.nodes.add(sub);
			part.contributeNodesToGraph(graph, sub, partsToNodes);
			part.contributeEdgesToGraph(graph, sub, partsToNodes);
			CompoundDirectedGraphLayout layout = new CompoundDirectedGraphLayout();
			graph.setDirection(part.getModel()
					.getDirection());
			layout.visit(graph);

			CompoundCommand applyLayouts = new CompoundCommand();
			part.applyLayout(graph, partsToNodes, applyLayouts);
			applyLayouts.execute();
			if(resetInnerPaddings)
			{
				internalCommand = resetInnerPaddingsCmd.chain(applyLayouts);
			}
			else internalCommand = applyLayouts;
		}
	}

	public StructuredActivity getContainer() {
		return container;
	}

	@Override
	public void redo() {
		if (internalCommand != null) {
			internalCommand.redo();
		}
	}

	public void setContainer(StructuredActivity container) {
		this.container = container;
	}

	@Override
	public void undo() {
		if (internalCommand != null) {
			internalCommand.undo();
		}
	}

	public void setFlags(int flags) {
		this.flags = flags;
	}

}
