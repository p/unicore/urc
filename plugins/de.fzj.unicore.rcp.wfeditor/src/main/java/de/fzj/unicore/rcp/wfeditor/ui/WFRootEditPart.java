package de.fzj.unicore.rcp.wfeditor.ui;

import java.io.BufferedInputStream;

import org.apache.tools.ant.filters.StringInputStream;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayeredPane;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.Layer;
import org.eclipse.draw2d.LayeredPane;
import org.eclipse.draw2d.ScalableFreeformLayeredPane;
import org.eclipse.draw2d.Viewport;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;

import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.DiagramView;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.tools.WFMarqueeDragTracker;
import de.fzj.unicore.rcp.wfeditor.utils.XStreamConfigurator;

public class WFRootEditPart extends ScalableFreeformRootEditPart {

	/**
	 * Identifies the layer containing the Labels for activities and
	 * transitions.
	 */
	public static String LABEL_LAYER = "Label Layer"; //$NON-NLS-1$

	/**
	 * Identifies the layer containing data sources, data sinks and data flows.
	 */
	public static String DATA_FLOW_LAYER = "Data flow Layer"; //$NON-NLS-1$

	/**
	 * Identifies the layer containing tooltips.
	 */
	public static String TOOLTIP_LAYER = "Tooltip Layer"; //$NON-NLS-1$

	@Override
	protected void createLayers(LayeredPane layeredPane) {
		super.createLayers(layeredPane);
		Layer layer = new FreeformLayer();
		layer.setLayoutManager(new FreeformLayout());
		layeredPane.add(layer, TOOLTIP_LAYER);
	}

	/**
	 * Creates a layered pane and the layers that should be printed.
	 */
	@Override
	protected LayeredPane createPrintableLayers() {
		FreeformLayeredPane pane = new FreeformLayeredPane();

		Layer primary = new FreeformLayer();
		primary.setLayoutManager(new FreeformLayout());
		pane.add(primary, PRIMARY_LAYER);

		ConnectionLayer connectionLayer = new ConnectionLayer();
		connectionLayer.setPreferredSize(new Dimension(5, 5));
		pane.add(connectionLayer, CONNECTION_LAYER);

		Layer layer = new FreeformLayer();
		layer.setLayoutManager(new FreeformLayout());
		pane.add(layer, DATA_FLOW_LAYER);

		layer = new FreeformLayer();
		layer.setLayoutManager(new FreeformLayout());
		pane.add(layer, LABEL_LAYER);

		return pane;
	}

	@Override
	protected ScalableFreeformLayeredPane createScaledLayers() {
		ScalableFreeformLayeredPane layers = super.createScaledLayers();
		return layers;
	}

	public WorkflowDiagram getDiagram() {
		return (WorkflowDiagram) getContents().getModel();
	}

	@Override
	public DragTracker getDragTracker(Request req) {

		DragTracker result = new WFMarqueeDragTracker();
		// result.setMarqueeBehavior(MarqueeDragTracker.BEHAVIOR_CONNECTIONS_TOUCHED);
		return result;
	}

	public void loadViewInfoForDiagram() {
		try {
			IPath path = getViewInfoPathForDiagramPath(getDiagram()
					.getAbsolutePath());
			if (path == null) {
				return;
			}
			IFile file = PathUtils.absolutePathToEclipseFile(path);
			if (!file.exists()) {
				return;
			}
			DiagramView view = (DiagramView) XStreamConfigurator.getInstance()
					.getXStreamForDeserialization().fromXML(file.getContents());

			Viewport vp = (Viewport) getFigure();
			getZoomManager().setZoom(view.getZoomFactor());

			vp.getHorizontalRangeModel().setMinimum(view.getHorizontalMin());
			vp.getHorizontalRangeModel().setMaximum(view.getHorizontalMax());

			vp.getVerticalRangeModel().setMinimum(view.getVerticalMin());
			vp.getVerticalRangeModel().setMaximum(view.getVerticalMax());
			vp.setViewLocation(view.getXLocation(), view.getYLocation());
		} catch (Exception e) {
			// do nothing
			WFActivator
					.log(IStatus.WARNING,
							"Unable to restore saved information about the user's view on the diagram.",
							e);
		}
	}

	public void saveViewInfoForDiagram() {
		try {
			IPath path = getViewInfoPathForDiagramPath(getDiagram()
					.getAbsolutePath());
			if (path == null) {
				return;
			}
			Viewport vp = (Viewport) getFigure();
			int x = vp.getHorizontalRangeModel().getValue();
			int y = vp.getVerticalRangeModel().getValue();
			int horMin = vp.getHorizontalRangeModel().getMinimum();
			int horMax = vp.getHorizontalRangeModel().getMaximum();
			int vertMin = vp.getVerticalRangeModel().getMinimum();
			int vertMax = vp.getVerticalRangeModel().getMaximum();
			double zoom = getZoomManager().getZoom();
			DiagramView view = new DiagramView(x, y, horMin, horMax, vertMin,
					vertMax, zoom);
			String s = XStreamConfigurator.getInstance()
					.getXStreamForSerialization().toXML(view);
			IFile file = PathUtils.absolutePathToEclipseFile(path);
			if (!file.exists()) {
				file.create(new BufferedInputStream(new StringInputStream(s)),
						true, null);
			} else {
				file.setContents(new BufferedInputStream(new StringInputStream(
						s)), true, false, null);
			}
		} catch (Exception e) {
			// do nothing
		}
	}

	@Override
	public void setContents(EditPart part) {
		super.setContents(part);
		if (part != null && getDiagram() != null) {
			loadViewInfoForDiagram();
		}
	}

	public static IPath getViewInfoPathForDiagramPath(IPath path) {
		if (path == null) {
			return null;
		}
		path = path.removeFileExtension();
		return path.addFileExtension(WFConstants.FILE_EXTENSION_DIAGRAM_VIEW);
	}

}
