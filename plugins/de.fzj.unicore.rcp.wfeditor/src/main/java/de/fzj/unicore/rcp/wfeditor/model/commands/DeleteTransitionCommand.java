/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;

/**
 * Handles the deletion of connections between Activities.
 * 
 * @author Daniel Lee
 */
public class DeleteTransitionCommand extends Command {

	private IActivity source;
	private IActivity target;
	private Transition transition;

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		if (source != null) {
			source.removeOutput(transition);
		}
		if (target != null) {
			target.removeInput(transition);
		}
		transition.dispose();
	}

	/**
	 * Sets the transition
	 * 
	 * @param transition
	 *            the transition
	 */
	public void setTransition(Transition transition) {
		this.transition = transition;
		source = transition.source;
		target = transition.target;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		transition.source = source;
		transition.target = target;
		if (source != null) {
			source.addOutput(transition);
		}
		if (target != null) {
			target.addInput(transition);
		}
		if (transition.isDisposed()) {
			transition.undoDispose();
		}
	}

}
