/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.submission;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.namespace.QName;

/**
 * @author demuth
 * 
 */
public class ConverterRegistry {

	Map<QName, Map<QName, IConverter>> converters;

	public ConverterRegistry() {
		converters = new ConcurrentHashMap<QName, Map<QName, IConverter>>();
	}

	/**
	 * return Converter that converts between the model and the workflow
	 * language of the given types or null if no appropriate converter exists
	 * 
	 * @param modelType
	 * @param wfLanguageType
	 * @return
	 */
	public IConverter getConverter(QName modelType, QName wfLanguageType) {
		Map<QName, IConverter> sameModelType = converters.get(modelType);
		if (sameModelType == null) {
			return null;
		}
		return sameModelType.get(wfLanguageType);
	}

	/**
	 * Determine all workflow language types to which a model of the given type
	 * can be converted.
	 * 
	 * @param modelType
	 * @return
	 */
	public Set<QName> getPossibleTargets(QName modelType) {
		Set<QName> result = new HashSet<QName>();
		Map<QName, IConverter> sameModelType = converters.get(modelType);
		if (sameModelType != null) {
			for (Iterator<IConverter> iter = sameModelType.values().iterator(); iter
					.hasNext();) {
				IConverter converter = iter.next();
				result.add(converter.getTargetType());
			}
		}
		return result;
	}

	/**
	 * Register a Converter. Caution this may overwrite another converter
	 * without warning.
	 * 
	 * @param c
	 */
	public void registerConverter(IConverter c) {
		Map<QName, IConverter> sameModelType = converters.get(c.getModelType());
		if (sameModelType == null) {
			sameModelType = new ConcurrentHashMap<QName, IConverter>();
			converters.put(c.getModelType(), sameModelType);
		}
		sameModelType.put(c.getTargetType(), c);
	}

	public void unregisterConverter(IConverter c) {
		Map<QName, IConverter> sameModelType = converters.get(c.getModelType());
		if (sameModelType == null) {
			return;
		}
		sameModelType.remove(c.getTargetType());
	}
}
