package de.fzj.unicore.rcp.wfeditor.extensions;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.wfeditor.extensionpoints.IConditionTypeExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType;
import de.fzj.unicore.rcp.wfeditor.ui.IConditionChangeListener;
import de.fzj.unicore.rcp.wfeditor.ui.IConditionTypeControlProducer;

public class ConditionTypeFreeFormExtension implements
		IConditionTypeExtensionPoint {
	
	private class FreeFormConditionControlProducer extends CommonControlProducer {
		@Override
		public CommonConditionTypeControl getControl(Composite parent,
				Condition condition) {
			control = new ConditionTypeFreeFormControl(parent, condition);
			parent.pack();
			for (IConditionChangeListener l : listeners) {
				control.addValidityChangeListener(l);
			}
			listeners.clear();
			return control;
		}
	}

	static final String ID = "de.fzj.unicore.rcp.wfeditor.extensions.conditionFreeForm";

	@Override
	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
		// TODO Auto-generated method stub

	}

	@Override
	public IConditionType createNewConditionType() {
		return new ConditionTypeFreeForm();
	}

	@Override
	public IConditionTypeControlProducer getControlProducer() {
		// TODO consider to allow this only above UnicoreCommonActivator.LEVEL_ADVANCED 
		return new FreeFormConditionControlProducer();
	}

	@Override
	public String getExtensionId() {
		return ID;
	}

}
