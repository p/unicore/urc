/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.gef.requests.GroupRequest;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.commands.RemoveIllegalTransitionsCommand;

/**
 * @author Daniel Lee
 */
public class RemoveIllegalTransitionsEditPolicy extends AbstractEditPolicy {

	@Override
	public Command getCommand(Request request) {
		if (WFConstants.REQ_REMOVE_ILLEGAL_TRANSITIONS
				.equals(request.getType())) {
			CompoundCommand result = new CompoundCommand();
			GroupRequest group = (GroupRequest) request;
			List<?> toCheck = group.getEditParts();
			for (int i = 0; i < toCheck.size(); i++) {
				EditPart part = (EditPart) toCheck.get(i);
				if (part.getModel() instanceof IActivity) {
					result.add(new RemoveIllegalTransitionsCommand(
							(IActivity) part.getModel()));
				}

			}
			return result;
		}
		return null;
	}

}
