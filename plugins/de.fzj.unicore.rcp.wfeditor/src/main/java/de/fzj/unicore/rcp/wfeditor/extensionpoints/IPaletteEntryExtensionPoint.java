/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.extensionpoints;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gef.EditPart;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.converters.Converter;

import de.fzj.unicore.rcp.wfeditor.parts.PartFactory;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;

/**
 * Extension point for adding entries to the palette of the workflow editor.
 * These entries represent new Types of activities, transitions or tools. They
 * are used to edit the underlying graph model of the workflow, so they create
 * elements of this model. This extension is also used to create EditParts
 * (controllers) for these elements.
 * 
 * @author demuth
 * 
 */
public interface IPaletteEntryExtensionPoint extends IExecutableExtension {

	public static final PaletteCategory PALETTE_CATEGORY_TOOLS = new PaletteCategory(
			"Tools", 0.0);
	public static final PaletteCategory PALETTE_CATEGORY_WORKFLOW_STRUCTURES = new PaletteCategory(
			"Structures", 1.0);
	public static final PaletteCategory PALETTE_CATEGORY_TRANSITIONS = new PaletteCategory(
			"Transitions", 2.0);
	public static final PaletteCategory PALETTE_CATEGORY_VARIABLES = new PaletteCategory(
			"Variables", 3.0);

	public static final Dimension DEFAULT_ICON_SIZE = new Dimension(40, 40);

	/**
	 * Create an EditPart for the given model element. This is being called from
	 * {@link PartFactory}
	 * 
	 * @param editor
	 *            reference to the workflow editor
	 * @param context
	 * @param model
	 *            the elemnt of the model for which an EditPart shall be created
	 * @return
	 */
	public EditPart createEditPart(WFEditor editor, EditPart context,
			Object model);

	/**
	 * Perform some cleanup. Especially clean up all images that have been
	 * created!
	 */
	public void dispose();

	/**
	 * Set of additional converters that are necessary for (de-) serializing the
	 * model objects with xstream.
	 * 
	 * @return
	 */
	public Set<Converter> getAdditionalConverters();

	/**
	 * List of new elements to be added to the palette of the editor.
	 * 
	 * @return
	 */
	public List<PaletteElement> getAdditionalPaletteEntries(WFEditor editor);

	/**
	 * Set of all classes that might be used in the model object graph of the
	 * newly introduced workflow elements. This is necessary for
	 * (de-)serializing the annotated workflow model with xstream.
	 * 
	 * @return
	 */
	public Set<Class<?>> getAllModelClasses();

	/**
	 * This method allows to specify old (deprecated) element names that were
	 * used to serialize model objects but do not match the current classes'
	 * {@link XStreamAlias} annotations anymore. By providing these mappings,
	 * the old elements can be de-serialized to new Model objects. When
	 * serializing the new model objects, their current {@link XStreamAlias}
	 * annotation is used, which will unify the resulting XML.
	 * 
	 * @return
	 */
	public Map<String, Class<?>> getOldTypeAliases();

}
