package de.fzj.unicore.rcp.wfeditor.utils;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import com.thoughtworks.xstream.XStream;

import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;

@SuppressWarnings("unchecked")
public class CloneUtils {

	public static <T> T clone(T orig) throws CloneNotSupportedException {
		if (orig instanceof Cloneable) {
			try {
				Method cloneMethod = orig.getClass().getMethod("clone");
				cloneMethod.setAccessible(true);
				return (T) cloneMethod.invoke(orig);
			} catch (Exception e) {
				throw new CloneNotSupportedException();
			}
		} else {
			throw new CloneNotSupportedException();
		}
	}

	/**
	 * Returns a copy of the object, or null if the object cannot be serialized.
	 */
	public static <T> T deepClone(T orig) {
		T result = null;
		try {
			XStream xstream = XStreamConfigurator.getInstance()
					.getXStreamForSerialization();
			String s = xstream.toXML(orig);
			result = (T) xstream.fromXML(s);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Returns a copy of the object, or null if the object cannot be serialized.
	 */
	
	public static <T> T deepClone(T orig, ClassLoader classLoader) {
		T result = null;
		try {
			XStream xstream = XStreamConfigurator.getInstance()
					.getXStreamForSerialization();
			xstream.setClassLoader(classLoader);
			String s = xstream.toXML(orig);
			result = (T) xstream.fromXML(s);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static <T extends ICopyable> T getFromMapOrCopy(T toCopyNow,
			Set<IFlowElement> toBeCopied, Map<Object, Object> mapOfCopies,
			int copyFlags) {
		if (toCopyNow == null) {
			return null;
		} else if (mapOfCopies.get(toCopyNow) != null) {
			return (T) mapOfCopies.get(toCopyNow);
		} else if (toCopyNow instanceof IFlowElement) {
			// do NOT copy flow elements that are not supposed to be copied

			if (isBeingCopied((IFlowElement) toCopyNow, toBeCopied)) {
				T result = (T) toCopyNow.getCopy(toBeCopied, mapOfCopies,
						copyFlags);
				if (result != null) {
					mapOfCopies.put(toCopyNow, result);
				}
				return result;
			} else {
				return toCopyNow;
			}
		} else {
			T result = (T) toCopyNow
					.getCopy(toBeCopied, mapOfCopies, copyFlags);
			if (result != null) {
				mapOfCopies.put(toCopyNow, result);
			}
			return result;
		}
	}

	/**
	 * This method checks, whether the given element or one of its parents is
	 * contained in the given set of flow elements to be copied.
	 * 
	 * @param element
	 * @param toBeCopied
	 * @return
	 */
	public static boolean isBeingCopied(IFlowElement element,
			Set<IFlowElement> toBeCopied) {
		if (element == null) {
			return false;
		}
		IFlowElement check = element;
		while (!toBeCopied.contains(check) && check.getParent() != null) {
			// check whether one of the parents of the current element should be
			// copied
			// => this implies that the current element should be copied, too
			check = check.getParent();
		}
		return toBeCopied.contains(check);
	}

}
