/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.structures;

import javax.xml.namespace.QName;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.ui.views.properties.PropertyDescriptor;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.guicomponents.BooleanPropertyDescriptor;

/**
 * 
 * @author demuth
 */
@XStreamAlias("ContainerActivity")
public class ContainerActivity extends CollapsibleActivity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 675155363817681778L;
	public static QName TYPE = new QName("http://www.unicore.eu/",
			"ContainerActivity");
	public static final String COBROKER = "COBROKER";

	/**
	 * 
	 */
	public ContainerActivity() {
		super();
		setPropertyValue(COBROKER, false);
	}

	/**
	 * @see de.fzj.unicore.rcp.wfeditor.model.activities.Activity#addPropertyDescriptors()
	 */
	@Override
	protected void addPropertyDescriptors() {
		super.addPropertyDescriptors();
		PropertyDescriptor cobroDescriptor = new BooleanPropertyDescriptor(
				COBROKER, COBROKER);
		cobroDescriptor.setCategory("Brokering");
		addPropertyDescriptor(cobroDescriptor);
	}
	
	public Insets getDefaultInnerPadding() {
		return new Insets(10, 10, 10, 10);
	}
	
	@Override
	public QName getType() {
		return TYPE;
	}

	@Override
	public void init() {
		super.init();
		if (getName() == null || getName().trim().length() == 0) {
			setName(getDiagram().getUniqueActivityName("Group"));
		}
//		setSize(WFConstants.DEFAULT_EMPTY_CONTAINER_SIZE.getCopy());
	}

	@Override
	public void setSize(Dimension d) {
		super.setSize(d);
	}

}
