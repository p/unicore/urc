/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.variables;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.PropertyDescriptor;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IDisposableWithUndo;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.ui.WorkflowVariableSelectionCellEditor;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;
import de.fzj.unicore.rcp.wfeditor.utils.PropertyMapComparator;

/**
 * @author demuth
 * 
 */
@XStreamAlias("WorkflowVariableModifier")
public class WorkflowVariableModifier extends PropertySource implements
		PropertyChangeListener, IDisposableWithUndo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9183655788469605426L;

	// keys defining property names
	public static final String PROP_ID = "id", PROP_NAME = "Name",
			PROP_VARIABLE_NAME = "Variable name",
			PROP_INTERNAL_EXPRESSION = "Internal expression",
			PROP_DISPLAYED_EXPRESSION = "Expression", PROP_TYPE = "type",
			PROP_FLOW_ELEMENT = "element", PROP_DESCRIPTION = "Description",
			PROP_DISPOSED = "disposed",
			PROP_DATA_SOURCE_AND_SINK_VISIBLE = "dataSourceAndSinkVisible";

	public static final String DEFAULT_NAME = "Modifier";

	private boolean disposable = false;

	private WorkflowVariable variable;

	private VariableModifierDataSource dataSource;

	private VariableModifierDataSink dataSink;

	public WorkflowVariableModifier() {

	}

	public WorkflowVariableModifier(IFlowElement flowElement) {
		this(DEFAULT_NAME, flowElement);
	}

	public WorkflowVariableModifier(String name, IFlowElement flowElement) {
		setId(createRandomId());
		setName(name);
		setFlowElement(flowElement);
		setDisplayedExpression("");
		dataSink = new VariableModifierDataSink(this);
		dataSource = new VariableModifierDataSource(this);
	}

	@Override
	public void activate() {
		super.activate();
		if (getVariable() != null) {
			// in versions previous to 6.4.0, we used to listen on variables
			// ourselves; after that, the data flow from the variable to
			// ourselves does this dirty job
			getVariable().removePersistentPropertyChangeListener(this);
		}
	}

	@Override
	protected void addPropertyDescriptors() {
		addPropertyDescriptor(new TextPropertyDescriptor(PROP_NAME, PROP_NAME));
		addPropertyDescriptor(new PropertyDescriptor(PROP_VARIABLE_NAME,
				PROP_VARIABLE_NAME) {
			@Override
			public CellEditor createPropertyEditor(Composite parent) {
				CellEditor editor = new WorkflowVariableSelectionCellEditor(
						parent, getFlowElement());
				return editor;
			}
		});
		addPropertyDescriptor(new TextPropertyDescriptor(
				PROP_DISPLAYED_EXPRESSION, PROP_DISPLAYED_EXPRESSION));
		addPropertyDescriptor(new TextPropertyDescriptor(PROP_DESCRIPTION,
				PROP_DESCRIPTION));
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();

		// make sure data sink and source are created even when dealing with old
		// modifiers
		getDataSink();
		getDataSource();
	}

	@Override
	public void dispose() {
		super.dispose();
		if (!getDataSink().isDisposed()) {
			getDataSink().dispose();
		}
		if (!getDataSource().isDisposed()) {
			getDataSource().dispose();
		}

	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof WorkflowVariableModifier)) {
			return false;
		}
		WorkflowVariableModifier other = (WorkflowVariableModifier) o;
		if (this == other) {
			return true;
		}
		return PropertyMapComparator.equal(getProperties(),
				other.getProperties());

	}

	@Override
	public WorkflowVariableModifier getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		WorkflowVariableModifier copy = (WorkflowVariableModifier) super
				.getCopy(toBeCopied, mapOfCopies, copyFlags);
		copy.setFlowElement(CloneUtils.getFromMapOrCopy(getFlowElement(),
				toBeCopied, mapOfCopies, copyFlags));
		copy.setId(createRandomId());
		copy.setName(copy.getFlowElement().getDiagram()
				.getUniqueElementName(DEFAULT_NAME));
		copy.dataSource = CloneUtils.getFromMapOrCopy(dataSource, toBeCopied,
				mapOfCopies, copyFlags);
		copy.dataSink = CloneUtils.getFromMapOrCopy(dataSink, toBeCopied,
				mapOfCopies, copyFlags);
		return copy;
	}

	public VariableModifierDataSink getDataSink() {
		if (dataSink == null) {
			dataSink = new VariableModifierDataSink(this);
		}
		return dataSink;
	}

	public VariableModifierDataSource getDataSource() {
		if (dataSource == null) {
			dataSource = new VariableModifierDataSource(this);
		}
		return dataSource;
	}

	public String getDescription() {
		return (String) getPropertyValue(PROP_DESCRIPTION);
	}

	public String getDisplayedExpression() {
		Object displayed = getPropertyValue(PROP_DISPLAYED_EXPRESSION);
		if (displayed == null) {
			return getInternalExpression();
		} else {
			return (String) displayed;
		}
	}

	public IFlowElement getFlowElement() {
		return (IFlowElement) getPropertyValue(PROP_FLOW_ELEMENT);
	}

	/**
	 * A unique ID for referencing this Object.
	 */
	public String getId() {
		return (String) getPropertyValue(PROP_ID);
	}

	public String getInternalExpression() {
		Object internal = getPropertyValue(PROP_INTERNAL_EXPRESSION);
		if (internal == null) {
			return getDisplayedExpression();
		} else {
			return (String) internal;
		}
	}

	public String getName() {
		return (String) getPropertyValue(PROP_NAME);
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_DESCRIPTION);
		propertiesToCopy.add(PROP_DISPLAYED_EXPRESSION);
		propertiesToCopy.add(PROP_DISPOSED);
		propertiesToCopy.add(PROP_INTERNAL_EXPRESSION);
		propertiesToCopy.add(PROP_TYPE);
		propertiesToCopy.add(PROP_DATA_SOURCE_AND_SINK_VISIBLE);
		return propertiesToCopy;
	}

	public String getType() {
		return (String) getPropertyValue(PROP_TYPE);
	}

	/**
	 * Tries to return the variable being modified by this modifier. Caution,
	 * the result may be null!
	 * 
	 * @return
	 */
	public WorkflowVariable getVariable() {
		if (getFlowElement() != null && getFlowElement().getDiagram() != null) {
			return getFlowElement().getDiagram().getVariable(getVariableName());
		}
		return null;
	}

	public String getVariableName() {
		return (String) getPropertyValue(PROP_VARIABLE_NAME);
	}

	@Override
	public int hashCode() {
		return PropertyMapComparator.hashCode(getProperties());
	}

	public boolean isDataSourceAndSinkVisible() {
		Boolean result = (Boolean) getPropertyValue(PROP_DATA_SOURCE_AND_SINK_VISIBLE);
		return result == null ? true : result; // should be visible by default
	}

	@Override
	public boolean isDisposable() {
		return disposable;
	}

	@Override
	public boolean isDisposed() {
		return super.isDisposed();
	}

	public void propertyChange(PropertyChangeEvent evt) {
		// in versions previous to 6.4.0, we used to listen on variables
		// ourselves; after that, the data flow from the variable to
		// ourselves does this dirty job
	}

	/**
	 * Modifiers can be hidden explicitly so that their sources and sinks are
	 * not displayed in the data flow editing mode. This leads to hiding
	 * attached data flows, too.
	 * 
	 * @param visible
	 */
	public void setDataSourceAndSinkVisible(boolean visible) {
		setPropertyValue(PROP_DATA_SOURCE_AND_SINK_VISIBLE, visible);
	}

	public void setDescription(String descr) {
		setPropertyValue(PROP_DESCRIPTION, descr);
	}

	public void setDisplayedExpression(String expression) {
		setPropertyValue(PROP_DISPLAYED_EXPRESSION, expression);
	}

	@Override
	public void setDisposable(boolean disposable) {
		this.disposable = disposable;
	}

	public void setFlowElement(IFlowElement element) {
		setPropertyValue(PROP_FLOW_ELEMENT, element);
	}

	public void setId(String id) {
		setPropertyValue(PROP_ID, id);
	}

	public void setInternalExpression(String expression) {
		setPropertyValue(PROP_INTERNAL_EXPRESSION, expression);
		// validate();
	}

	public void setName(String name) {
		setPropertyValue(PROP_NAME, name);
	}

	@Override
	public void setPropertyValue(Object key, Object value) {
		if (PROP_VARIABLE_NAME.equals(key)) {
			String oldValue = getVariableName();
			String newValue = (String) value;
			properties.put(key, value); // don't use super.setPropertyValue
										// here, cause the fired event will
										// trigger actions that should not be
										// triggered yet
			variable = getVariable();
			if (variable == null && !getDataSource().isDisposed()) {
				getDataSource().dispose();
			}
			if (variable != null && getDataSource().isDisposed()) {
				getDataSource().undoDispose();
			}
			firePropertyChange(PROP_VARIABLE_NAME, oldValue, newValue);
			if (oldValue != null && newValue != null) {
				// try to automatically fix the expression
				String newInternal = getInternalExpression().replaceAll(
						"\\b" + oldValue + "\\b", newValue);
				setInternalExpression(newInternal);
				String newDisplayed = getDisplayedExpression().replaceAll(
						"\\b" + oldValue + "\\b", newValue);
				setDisplayedExpression(newDisplayed);
			}

		} else {
			super.setPropertyValue(key, value);
		}
		IFlowElement flowElement = getFlowElement();
		if (flowElement != null) {
			// make sure an event is triggered on the parent element
			// informing listeners of modifier change
			WorkflowVariableModifierList list = flowElement
					.getVariableModifierList();
			flowElement.setPropertyValue(IFlowElement.PROP_VARIABLE_MODIFIERS,
					list);
		}

	}

	public void setVariableName(String name) {
		setPropertyValue(PROP_VARIABLE_NAME, name);
	}

	@Override
	public String toString() {
		return "";
	}

	@Override
	public void undoDispose() {
		super.undoDispose();
		if (getDataSink().isDisposed()) {
			getDataSink().undoDispose();
		}
		if (getDataSource().isDisposed()) {
			getDataSource().undoDispose();
		}
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		if (updatedToCurrentModelVersion) {
			return; // don't do this twice!
		}
		super.updateToCurrentModelVersion(oldVersion, currentVersion);
		if (oldVersion.compareTo("6.4.0") < 0) {
			getFlowElement().getDataSourceList().addDataSource(getDataSource());
			getFlowElement().getDataSinkList().addDataSink(getDataSink());
			if (getVariable() != null) {
				VariableToModifierDataFlow flow = new VariableToModifierDataFlow(
						getVariable(), getDataSink());
				flow.connect();
			}
		}
	}

	// private void validate()
	// {
	// ValidationResult old = (ValidationResult)
	// getPropertyValue(PROP_VALIDATION_RESULT);
	// ValidationError error = new
	// ValidationError("The entered expression does not contain the name of the selected variable!");
	// if(getInternalExpression().contains(getVariableName()))
	// old.removeError(error);
	// else old.addError(error);
	// setPropertyValue(PROP_VALIDATION_RESULT, old);
	// }
}
