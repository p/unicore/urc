package de.fzj.unicore.rcp.wfeditor.utils;

import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.core.JVM;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.IPaletteEntryExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.model.serialization.DomConverter;
import de.fzj.unicore.rcp.wfeditor.model.serialization.WFDiagramConverter;

/**
 * Utility for configuring XStream properly for (de-)serializing the workflow
 * diagram. Since the diagram's model is extensible by additional Eclipse
 * plugins, this involves iterating over plugin extensions.
 * 
 * @author bdemuth
 * 
 */
@SuppressWarnings("rawtypes")
public class XStreamConfigurator extends de.fzj.unicore.rcp.common.utils.XStreamConfigurator{


	private static XStreamConfigurator instance;

	protected XStreamConfigurator() {
		super();
	}

	protected XStream createXStream() {
		XStream xstream = super.createXStream();
		xstream.registerConverter(new DomConverter());
		xstream.registerConverter(new WFDiagramConverter(xstream.getMapper(),
				new JVM().bestReflectionProvider()));

		return xstream;
	}

	public static XStreamConfigurator getInstance() {
		if (instance == null) {
			instance = new XStreamConfigurator();
		}
		return instance;
	}

	@Override
	protected String[] getExtensionIDs() {
		return new String[] {
				WFConstants.EXTENSION_POINT_PALETTE_ENTRY,
				WFConstants.EXTENSION_POINT_MODEL_CONVERTER };
	}

	@Override
	protected Class[] getClasses(String extensionId,
			IConfigurationElement member) {
		if(!WFConstants.EXTENSION_POINT_PALETTE_ENTRY.equals(extensionId))
		{
			return null;
		}
		IPaletteEntryExtensionPoint ep = createExecutableExtension(member);
		if(ep != null)
		{
			return ep.getAllModelClasses().toArray(new Class[0]);
		}
		return null;
	}

	@Override
	protected Converter[] getConverters(String extensionId,
			IConfigurationElement member) {
		if(!WFConstants.EXTENSION_POINT_PALETTE_ENTRY.equals(extensionId))
		{
			return null;
		}
		IPaletteEntryExtensionPoint ep = createExecutableExtension(member);
		if(ep != null)
		{
			Set<Converter> converters = ep.getAdditionalConverters();
			if(converters != null)
			{
				return converters.toArray(new Converter[converters.size()]);
			}
		}
		return null;
	}

	@Override
	protected Map<String, Class<?>> getOldTypeAliases(String extensionId,
			IConfigurationElement member) {
		if(!WFConstants.EXTENSION_POINT_PALETTE_ENTRY.equals(extensionId))
		{
			return null;
		}
		IPaletteEntryExtensionPoint ep = createExecutableExtension(member);
		if(ep != null)
		{
			try {
				return ep.getOldTypeAliases();	
			} catch (AbstractMethodError ame) {
				// do nothing
			}
		}
		return null;
	}

	@Override
	protected void log(int status,String msg, Exception ex) {
		WFActivator.log(status,msg,ex);
	}

	protected IPaletteEntryExtensionPoint createExecutableExtension(IConfigurationElement member)
	{
		try {
			return (IPaletteEntryExtensionPoint) member
			.createExecutableExtension("name");
		} catch (CoreException e) {
			return null;
		}
	}
}
