/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.common.guicomponents.DynamicWizard;
import de.fzj.unicore.rcp.common.interfaces.IFinishableWizardPage;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.ISubmissionWizardPageFactory;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.submission.ISubmitter;
import de.fzj.unicore.rcp.wfeditor.submission.SubmitterRegistry;

/**
 * Wizard for submitting a workflow
 */
public class WFSubmissionWizard extends DynamicWizard {

	protected WorkflowDiagram workflowDiagram;

	protected WFSubmissionWizardPage1 page1;

	protected List<ISubmitter> submitters;

	protected ISubmitter selectedSubmitter;

	protected Map<ISubmissionWizardPageFactory, IWizardPage[]> contributedPages = new HashMap<ISubmissionWizardPageFactory, IWizardPage[]>();

	protected Node selectedNode;

	/**
	 * Runnable containing the actions to be performed during submission Setting
	 * this from the outside makes the submission wizard more reusable
	 */
	protected IRunnableWithProgress submissionRunnable;

	protected String taskName = "submitting workflow";

	/**
	 * Creates a wizard for submitting a workflow
	 */

	public WFSubmissionWizard(WorkflowDiagram diagram,
			IRunnableWithProgress submissionRunnable) {
		setWindowTitle("Submit workflow");
		setNeedsProgressMonitor(true);
		setWorkflowDiagram(diagram);

		setForcePreviousAndNextButtons(true);
		page1 = new WFSubmissionWizardPage1();
		this.submissionRunnable = submissionRunnable;
	}

	/*
	 * (non-Javadoc) Method declared on IWizard.
	 */
	@Override
	public void addPages() {
		super.addPages();

		page1.setWorkflowDiagram(getWorkflowDiagram());

		addPage(page1);

	}

	@Override
	public void createPageControls(Composite pageContainer) {
		super.createPageControls(pageContainer);

	}

	@Override
	public void dispose() {
		super.dispose();
		for (ISubmissionWizardPageFactory factory : contributedPages.keySet()) {
			factory.dispose();
		}
		contributedPages.clear();
		if (submitters != null) {
			submitters.clear();
		}
	}

	public WFSubmissionWizardPage1 getPage1() {
		return page1;
	}

	public Node getSelectedNode() {
		return selectedNode;
	}

	public ISubmitter getSelectedSubmitter() {
		return selectedSubmitter;
	}

	public ISubmitter getSuitableSubmitter() {
		if (submitters != null) {
			for (ISubmitter submitter : submitters) {
				if (submitter
						.canSubmit(getSelectedNode(), getWorkflowDiagram())) {
					return submitter;
				}
			}
		}
		return null;
	}

	public WorkflowDiagram getWorkflowDiagram() {
		return workflowDiagram;
	}

	/*
	 * (non-Javadoc) Method declared on BasicNewResourceWizard.
	 */
	protected void initializeDefaultPageImageDescriptor() {
		// TODO image does not exist
		ImageDescriptor desc = WFActivator
				.imageDescriptorFromPlugin(WFActivator.PLUGIN_ID,
						WFActivator.ICONS_PATH + "wizban/newprj_wiz.png");

		setDefaultPageImageDescriptor(desc);
	}

	@Override
	public boolean performCancel() {
		Job j = new BackgroundJob("cancelling") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.setCanceled(true); // always run the runnable but let it
											// know that submission was canceled
											// => perform cleanup
				try {
					submissionRunnable.run(monitor);
					return Status.OK_STATUS;
				} catch (Exception e) {
					WFActivator.log(
							"Error while submitting workflow: "
									+ e.getMessage(), e);
					return Status.CANCEL_STATUS;
				}

			}
		};
		j.schedule();
		return super.performCancel();
	}

	/*
	 * (non-Javadoc) Method declared on IWizard.
	 */
	@Override
	public boolean performFinish() {

		boolean success = true;
		for (IWizardPage page : getPages()) {
			if (page instanceof IFinishableWizardPage) {
				success = success && ((IFinishableWizardPage) page).finish();
				if (!success) {
					break;
				}
			}
		}
		final boolean cancel = !success;
		Job j = new BackgroundJob(taskName) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				if (cancel) {
					monitor.setCanceled(true); // always run the runnable but
												// let it know that submission
												// was canceled => perform
												// cleanup
				}
				try {
					submissionRunnable.run(monitor);
					return Status.OK_STATUS;
				} catch (Exception e) {
					WFActivator.log(
							"Error while submitting workflow: "
									+ e.getMessage(), e);
					return Status.CANCEL_STATUS;
				}

			}
		};

		j.schedule();
		return success;
	}

	public void setSelectedNode(Node selectedNode) {
		ISubmitter oldSubmitter = getSelectedSubmitter();
		this.selectedNode = selectedNode;
		ISubmitter newSubmitter = getSuitableSubmitter();
		selectedSubmitter = newSubmitter;
		if (newSubmitter != null) {
			updateContributedPages();

		} else if (oldSubmitter != null) {
			while (getPageCount() > 1) {
				removePage(getPageCount() - 1);
			}
		}
		if (getContainer().getCurrentPage() != null) {
			getContainer().updateButtons();
		}
	}

	public void setSubmissionRunnable(IRunnableWithProgress submissionRunnable) {
		this.submissionRunnable = submissionRunnable;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	protected void setWorkflowDiagram(WorkflowDiagram workflowDiagram) {
		this.workflowDiagram = workflowDiagram;
		if (workflowDiagram != null) {
			SubmitterRegistry submitterRegistry = WFActivator.getDefault()
					.getSubmitterRegistry();
			this.submitters = submitterRegistry
					.getSuitableSubmitters(workflowDiagram);
		}
	}

	public void updateContributedPages() {
		while (getPageCount() > 1) {
			removePage(getPageCount() - 1);
		}
		if (selectedSubmitter == null) {
			return;
		}
		SubmitterRegistry submitterRegistry = WFActivator.getDefault()
				.getSubmitterRegistry();
		ISubmissionWizardPageFactory factory = submitterRegistry
				.getWizardPageFactory(selectedSubmitter);
		if (factory != null) {
			IWizardPage[] pages = contributedPages.get(factory);
			if (pages == null) {
				pages = factory.createPages(this, getWorkflowDiagram(),
						getSelectedNode(), selectedSubmitter);
			} else {
				pages = factory.updatePages(pages, this, getWorkflowDiagram(),
						getSelectedNode(), selectedSubmitter);
			}

			if (pages != null) {
				contributedPages.put(factory, pages);
				for (IWizardPage page : pages) {
					addPage(page);
				}
			} else {
				contributedPages.remove(factory);
			}
		}
		if (getContainer().getCurrentPage() != null) {
			getContainer().updateButtons();
		}
	}

}
