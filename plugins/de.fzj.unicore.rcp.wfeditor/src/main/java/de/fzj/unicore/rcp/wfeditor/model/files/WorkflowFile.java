/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.files;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.StateConstants;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSource;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

/**
 * @author demuth
 * 
 */
@XStreamAlias("WorkflowFile")
@SuppressWarnings("unchecked")
public class WorkflowFile extends DataSource implements IDataSource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3462750422572338386L;

	// keys defining property names
	public static String PROP_PROTOCOL = "protocol",
			PROP_PARENT_DIR_ADDRESS = "parent dir address",
			PROP_RELATIVE_PATH = "relative path",
			PROP_DISPLAYED_STRING = "displayed_string",
			PROP_USING_WILDCARDS = "using_wildcards",
			PROP_EXCLUDES = "excludes", PROP_MIME_TYPES = "mime_types";

	public static final Set<String> WORKFLOW_FILE_PROPERTIES = new HashSet<String>(
			Arrays.asList(new String[] { PROP_PROTOCOL,
					PROP_PARENT_DIR_ADDRESS, PROP_RELATIVE_PATH,
					PROP_DISPLAYED_STRING, PROP_USING_WILDCARDS, PROP_EXCLUDES,
					PROP_MIME_TYPES }));

	public static final QName TYPE = new QName("http://www.unicore.eu/",
			"WorkflowFile");

	/**
	 * A unique ID for referencing this Object.
	 */
	private String id;

	/**
	 * The activity that defines this workflow file.
	 */
	private IActivity activity;

	/**
	 * A reference to the workflow diagram
	 */
	private WorkflowDiagram diagram;

	public WorkflowFile(WorkflowDiagram _diagram, String _fileId, IActivity _activity) {
		super();
		this.diagram = _diagram;
		this.id = _fileId;
		this.activity = _activity;
		setPropertyValue(PROP_EXCLUDES, new ArrayList<String>());
		setPropertyValue(PROP_MIME_TYPES, new HashSet<String>());
	}

	@Override
	protected void addPropertyDescriptors() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof WorkflowFile)) {
			return false;
		}
		WorkflowFile other = (WorkflowFile) o;
		return this == other
				|| ((getProtocol() == null ? other.getProtocol() == null
						: getProtocol().equals(other.getProtocol()))
						&& (getActivity() == null ? other.getActivity() == null
								: getActivity().equals(other.getActivity()))
						&& (getParentDirAddress() == null ? other
								.getParentDirAddress() == null
								: getParentDirAddress().equals(
										other.getParentDirAddress()))
						&& (getRelativePath() == null ? other.getRelativePath() == null
								: getRelativePath().equals(
										other.getRelativePath()))
						&& (getExcludes() == null ? other.getExcludes() == null
								: getExcludes().equals(other.getExcludes()))
						&& (getMimeTypes() == null ? other.getMimeTypes() == null
								: getMimeTypes().equals(other.getMimeTypes()))
						&& (isUsingWildcards() == other.isUsingWildcards()) && (getId() == null ? other
						.getId() == null : getId().equals(other.getId())));
	}

	public IActivity getActivity() {
		return activity;
	}

	@Override
	public WorkflowFile getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		WorkflowFile copy = (WorkflowFile) super.getCopy(toBeCopied,
				mapOfCopies, copyFlags);
		copy.setFlowElement(CloneUtils.getFromMapOrCopy(getFlowElement(),
				toBeCopied, mapOfCopies, copyFlags));
		copy.setDiagram(copy.getFlowElement().getDiagram());
		copy.setPropertyValue(PROP_MIME_TYPES, new HashSet<String>(
				getMimeTypes()));
		return copy;
	}

	public WorkflowDiagram getDiagram() {
		return diagram;
	}

	public String getDisplayedString() {
		return (String) getPropertyValue(PROP_DISPLAYED_STRING);
	}

	public List<String> getExcludes() {
		return (List<String>) getPropertyValue(PROP_EXCLUDES);
	}

	@Override
	public IFlowElement getFlowElement() {
		return getActivity();
	}

	public String getFullDisplayedName() {

		IActivity a1 = getActivity();
		return a1.getName() + WFConstants.DISPLAYED_STRING_SEPERATOR
				+ getDisplayedString();
	}
	
	@Override
	public String getId() {
		return id;
	}

	public Set<String> getMimeTypes() {
		return (Set<String>) getPropertyValue(PROP_MIME_TYPES);
	}

	@Override
	public Set<String> getOutgoingDataTypes() {
		return getMimeTypes();
	}

	public String getParentDirAddress() {
		return (String) getPropertyValue(PROP_PARENT_DIR_ADDRESS);
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_DISPLAYED_STRING);
		propertiesToCopy.add(PROP_EXCLUDES);
		propertiesToCopy.add(PROP_PARENT_DIR_ADDRESS);
		propertiesToCopy.add(PROP_RELATIVE_PATH);
		propertiesToCopy.add(PROP_USING_WILDCARDS);
		propertiesToCopy.add(PROP_PROTOCOL);
		return propertiesToCopy;
	}

	public QName getProtocol() {
		return (QName) getPropertyValue(PROP_PROTOCOL);
	}

	public String getRelativePath() {
		return (String) getPropertyValue(PROP_RELATIVE_PATH);
	}

	@Override
	public QName getSourceType() {
		return TYPE;
	}

	@Override
	public int hashCode() {
		List<Object> relevantData = new ArrayList<Object>();
		relevantData.add(getProtocol());
		relevantData.add(getActivity());
		relevantData.add(getParentDirAddress());
		relevantData.add(getRelativePath());
		relevantData.add(getId());

		for (String s : getExcludes()) {
			relevantData.add(s);
		}

		for (String s : getMimeTypes()) {
			relevantData.add(s);
		}

		relevantData.add(isUsingWildcards());
		int hashCode = 1;
		Iterator<?> i = relevantData.iterator();
		while (i.hasNext()) {
			Object obj = i.next();
			hashCode = 31 * hashCode + (obj == null ? 0 : obj.hashCode());
		}
		return hashCode;
	}

	@Override
	public boolean isDisposed() {
		return getState() == StateConstants.STATE_DISPOSED;
	}

	public boolean isUsingWildcards() {
		Object prop = getPropertyValue(PROP_USING_WILDCARDS);
		return prop == null ? false : (Boolean) prop;
	}

	public boolean isVisible() {
		IFlowElement element = getFlowElement();
		if (element == null) {
			return false;
		}
		ShowDataFlowsProperty prop = (ShowDataFlowsProperty) element
				.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		if (prop == null) {
			return false;
		}
		if (!prop.isShowingSources()) {
			return false;
		}
		if (getOutgoingDataTypes().size() == 0) {
			return prop.isShowingFlowsWithDataType("text/any");
		} else {
			return prop.isShowingFlowsWithDataTypes(getOutgoingDataTypes());
		}
	}

	public void setActivity(IActivity activity) {
		this.activity = activity;
	}

	public void setDiagram(WorkflowDiagram diagram) {
		this.diagram = diagram;
	}

	public void setDisplayedString(String displayedString) {
		setPropertyValue(PROP_DISPLAYED_STRING, displayedString);
	}

	public void setDisposed(boolean disposed) {
		setState(StateConstants.STATE_DISPOSED);
	}

	public void setExcludes(List<String> excludes) {
		setPropertyValue(PROP_EXCLUDES, excludes);
	}

	@Override
	public void setFlowElement(IFlowElement element) {
		if (!(element instanceof IActivity)) {
			throw new RuntimeException(
					"Parent element of a workflow file must be an activity!");
		}
		setActivity((IActivity) element);

	}

	public void setId(String id) {
		this.id = id;
	}

	public void setMimeTypes(Set<String> mimeTypes) {
		setPropertyValue(PROP_MIME_TYPES, mimeTypes);
		setPropertyValue(PROP_PROVIDED_DATA_TYPES, mimeTypes);
	}

	public void setParentDirAddress(String address) {
		setPropertyValue(PROP_PARENT_DIR_ADDRESS, address);
	}

	public void setProtocol(QName protocol) {
		setPropertyValue(PROP_PROTOCOL, protocol);
	}

	public void setRelativePath(String relativePath) {
		setPropertyValue(PROP_RELATIVE_PATH, relativePath);
		setName(relativePath);
	}

	public void setUsingWildcards(boolean isUsingWildcards) {
		setPropertyValue(PROP_USING_WILDCARDS, isUsingWildcards);
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {

		if (updatedToCurrentModelVersion) {
			return; // don't do this twice!
		}
		super.updateToCurrentModelVersion(oldVersion, currentVersion);
		if (oldVersion.compareTo("6.4.0") < 0) {
			setName(getRelativePath());
		}

	}

}
