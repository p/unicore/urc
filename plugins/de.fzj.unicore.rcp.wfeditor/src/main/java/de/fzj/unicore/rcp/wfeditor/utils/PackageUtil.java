package de.fzj.unicore.rcp.wfeditor.utils;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.Plugin;

/**
 * This class is able to return all classes in a given package. It may not work
 * for the default package, which you shouldn't use anyway.
 * 
 */
public class PackageUtil {

	public static Set<Class<?>> getEclipseClasses(Plugin plugin,
			String packageName, boolean includeSubPackages) throws Exception {
		Set<Class<?>> classes = new HashSet<Class<?>>();
		String path = packageName.replace('.', '/');
		Enumeration<?> enu = plugin.getBundle().findEntries("", "*.class", true);
		while (enu != null && enu.hasMoreElements()) {
			String s = enu.nextElement().toString();
			int index = s.indexOf(path);
			if (index >= 0) {
				String classname = s.substring(index, s.length() - 6);
				classname = classname.replace("/", ".");
				Class<?> clazz = plugin.getBundle().loadClass(classname);
				classes.add(clazz);
			}
		}
		return classes;
	}

}
