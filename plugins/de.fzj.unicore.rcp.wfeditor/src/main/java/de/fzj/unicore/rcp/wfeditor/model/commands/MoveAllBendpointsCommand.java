/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;

public class MoveAllBendpointsCommand extends Command {

	private Transition transition;
	private Point delta;

	public MoveAllBendpointsCommand(Transition transition, Point delta) {
		super();
		this.transition = transition;
		this.delta = delta;
	}

	@Override
	public void execute() {
		redo();
	}

	@Override
	public void redo() {
		transition.move(delta.x, delta.y);
	}

	@Override
	public void undo() {

		transition.move(-delta.x, -delta.y);
	}

}
