/*******************************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.borders;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Image;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;

/**
 * This border knows about collapsability. Specifically, it has icons for expand
 * and collapse, does hit testing for these icons, has an image and label to use
 * when collapsed, and does complete rendering when collapsed to look similar to
 * a leaf border (except with the addition of an expand icon).
 * 
 * It does not do rendering for the expanded state. The rationale is that things
 * should look similar when collapsed (like a leaf node) but may have different
 * appearances when expanded.
 */
public class CollapsibleBorder extends DrawerBorder {

	// Our parent figure. Used for relative location calculation.
	protected IFigure parentFigure;

	// Whether or not we are collapsed.
	private boolean collapsed = false;

	// The images for collapsed (+) and expanded (-).
	protected Image collapseImage, expandImage;
	// The width and height of the expanded image. Collapsed should be the same.
	protected int expandImageWidth, expandImageHeight;
	// The bounds of the "+/-" icon at the top of the figure
	protected Rectangle rectExpandCollapse;
	// The bounds of the rectangle surrounding the children when expanded.
	// Computed to take into account drawers.
	private Rectangle expandedBounds;
	// We keep this round rect around to paint.
	private RoundedRectangle roundRect;

	// The calculated rectangle for the collapsed border. This is null
	// when not collapsed, so subclasses shouldn't assume it's there.
	protected Rectangle collapsedBounds;

	// The calculated bounds of the rectangle to draw when collapsed
	protected Rectangle collapsedRectangle;

	// The vertical margin between the border and the image/text
	protected int topMargin;
	protected int bottomMargin = 3;

	// space between image and label
	protected int spacing = 5;
	protected int borderWidth = 1;
	protected int margin = 11;

	// space between the inside of the border and the contents
	protected Insets innerPadding = new Insets(3, 3, 3, 3);

	public CollapsibleBorder(IFigure parentFigure) {
		this.parentFigure = parentFigure;

		this.expandImage = WFActivator.getDefault().getImageRegistry()
				.get(WFConstants.IMAGE_EXPAND);
		this.collapseImage = WFActivator.getDefault().getImageRegistry()
				.get(WFConstants.IMAGE_COLLAPSE);

		this.expandImageHeight = expandImage.getBounds().height;
		this.expandImageWidth = expandImage.getBounds().width;

		this.topMargin = this.expandImageHeight + 15;

		this.roundRect = new RoundedRectangle();
		this.roundRect.setOpaque(true);
		this.roundRect.setCornerDimensions(new Dimension(WFConstants.ARC_WIDTH,
				WFConstants.ARC_WIDTH));
	}

	protected void calculate(IFigure figure) {
		// Calculate the coordinates for collapsed state, even though
		// we may currently be expanded.
		if (collapsedBounds == null) {
			collapsedBounds = figure.getBounds().getCopy();
			this.collapsedRectangle = collapsedBounds.getCopy();

			// Leave room on the left and right for the drawer.
			collapsedRectangle.x += DRAWER_WIDTH;
			collapsedRectangle.width -= DRAWER_WIDTH * 2;

			// bounds of the figure that we are given
			Rectangle figureBounds = figure.getBounds().getCopy();

			// area for plus/minus buttons
			rectExpandCollapse = new Rectangle(figureBounds.x
					+ figureBounds.width / 2 - expandImageWidth / 2,
					figureBounds.y + 1, expandImageWidth, expandImageHeight);

			// calculate the size of the round rectangle surrounding the
			// children,
			// taking into account arc size and drawer width
			this.expandedBounds = figureBounds.getCopy();

			expandedBounds.width -= 1;

			expandedBounds.y += expandImageHeight - 1;
			expandedBounds.height -= expandImageHeight;
		}

	}

	/**
	 * Subclasses should call this paint method unless there is a very good
	 * reason for overriding its behaviour. To affect where various things
	 * appear, override the calculate() method.
	 */
	protected void doPaint(IFigure figure, Graphics graphics, Insets insets) {
		// Remember the clipping rectangle
		Rectangle oldClip = new Rectangle();
		oldClip = graphics.getClip(oldClip);

		if (isCollapsed()) {
			graphics.drawImage(expandImage, rectExpandCollapse.getLocation());
		} else {
			graphics.drawImage(collapseImage, rectExpandCollapse.getLocation());
			// Paint the round rectangle around the lower part of the figure
			// (surrounding the
			// children figures).
			graphics.drawRoundRectangle(expandedBounds, WFConstants.ARC_WIDTH,
					WFConstants.ARC_WIDTH);
		}

	}

	/**
	 * We only know about the gradient rectangle when we are collapsed. TODO: Do
	 * we need a gradient rectangle when collapsed?
	 */
	protected Rectangle getGradientRect() {
		return new Rectangle(0, 0, 0, 0);
	}

	public Insets getInsets(IFigure figure) {

		calculate(figure);
		Insets result = new Insets(innerPadding);
		result.top += rectExpandCollapse.height;
		return result;

	}

	public Rectangle getRectExpandCollapse() {
		return rectExpandCollapse;
	}

	@Override
	public Image getTopImage() {
		return null;
	}

	/**
	 * Throw away computed values that determine the layout
	 */
	public void invalidate() {
		// rectCollapsed = null;
		collapsedBounds = null;
	}

	public boolean isCollapsed() {
		return collapsed;
	}

	/**
	 * Tests whether the given point is inside the collapsed image. The
	 * superclass only knows where the collapsed image is located; if subclasses
	 * want to do hit testing on the expanded image, they may override but may
	 * want to call super if we are collapsed.
	 */
	public boolean isPointInCollapseImage(int x, int y) {
		Point p = new Point(x, y);
		parentFigure.translateToRelative(p);
		Rectangle rect = rectExpandCollapse.getCopy();
		rect.expand(new Insets(1, 1, 1, 1));
		return rect.contains(p);
	}

	/**
	 * This method first invalidates and recalculates any bounds, and then
	 * paints the top and bottom drawers, as well as any markers contained
	 * within them.
	 */
	public final void paint(IFigure figure, Graphics graphics, Insets insets) {
		invalidate();
		calculate(figure);
		doPaint(figure, graphics, insets);
	}

	public void setCollapsed(boolean showCollapse) {
		this.collapsed = showCollapse;
	}

	public void setRectExpandCollapse(Rectangle rectExpandCollapse) {
		this.rectExpandCollapse = rectExpandCollapse;
	}
}
