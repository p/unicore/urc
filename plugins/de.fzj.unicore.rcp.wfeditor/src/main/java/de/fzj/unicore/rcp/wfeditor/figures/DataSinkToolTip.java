package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.TextUtilities;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.swt.graphics.Font;

import de.fzj.unicore.rcp.wfeditor.ui.WFTooltipHelper;

public class DataSinkToolTip extends Figure implements IComplexToolTip {

	private DataSinkFigure hoverSource;
	private EditPartViewer viewer;
	private int animationCounter = 0;
	private static final int ANIMATION_END = 20;

	public DataSinkToolTip() {
		setOpaque(true);

	}

	@SuppressWarnings("unchecked")
	public boolean createTooltip(EditPartViewer viewer, IFigure tooltipLayer,
			IFigure hoverSource, int eventX, int eventY) {

		this.hoverSource = (DataSinkFigure) hoverSource;
		this.viewer = viewer;
		Rectangle parentBounds = hoverSource.getBounds();
		Rectangle bounds = new Rectangle(parentBounds.x, parentBounds.y, 50, 50);
		hoverSource.translateToAbsolute(bounds);
		tooltipLayer.translateToRelative(bounds);
		setBounds(bounds);
		// use the parent's edit part for editing!
		Object parentPart = viewer.getVisualPartMap().get(hoverSource);
		if (parentPart != null) {
			viewer.getVisualPartMap().put(this, parentPart);
		}
		return true;
	}

	public void hide(IFigure tooltipLayer, IFigure hoverSource) {
		viewer.getVisualPartMap().remove(this);
	}

	@Override
	protected void paintFigure(Graphics graphics) {

		String label = hoverSource == null ? null : hoverSource
				.getDataTypeSymbol();
		if (label != null) {
			Rectangle parentBounds = hoverSource.getBounds().getCopy();
			hoverSource.translateToAbsolute(parentBounds);
			translateToRelative(parentBounds);
			Font oldFont = graphics.getFont();
			graphics.setFont(getFont());

			IFigure visible = ((GraphicalEditPart) viewer.getRootEditPart())
					.getFigure();
			Rectangle outerBounds = visible.getClientArea().getCopy();
			visible.translateToAbsolute(outerBounds);

			int charHeight = graphics.getFontMetrics().getHeight();
			String[] text = hoverSource.getToolTipText();
			int numLines = text.length;
			int maxLength = 0;
			for (String s : text) {
				int length = TextUtilities.INSTANCE.getTextExtents(s,
						graphics.getFont()).width;
				if (length > maxLength) {
					maxLength = length;
				}
			}

			int lineOffset = 3;
			int width = 1 + (int) (((double) animationCounter) / ANIMATION_END * (maxLength + 20));
			int height = 1 + (int) (((double) animationCounter) / ANIMATION_END * (numLines
					* charHeight + (numLines - 1) * lineOffset + 10));

			Rectangle bounds = null;
			if (animationCounter == ANIMATION_END) {
				bounds = getBounds();
				bounds = WFTooltipHelper.computeToolTipLocation(outerBounds,
						bounds);
			} else {
				bounds = new Rectangle(0, 0, width, height);
				bounds.x = parentBounds.right();
				bounds.y = parentBounds.getCenter().y - height / 2;
				bounds = WFTooltipHelper.computeToolTipLocation(outerBounds,
						bounds);
				setBounds(bounds);
				animationCounter++;
			}
			super.paintFigure(graphics);
			Point center = bounds.getCenter();
			int x = Math.max(bounds.x + 5, center.x - width / 2);
			for (int i = 0; i < text.length; i++) {
				String s = text[i];
				int y = bounds.y + 2 + i * (charHeight + lineOffset);
				graphics.drawText(s, x, y);
			}

			graphics.setFont(oldFont);
			graphics.drawRectangle(bounds.getCropped(new Insets(1, 1, 1, 1)));
		}

	}

	public boolean refreshTooltip(IFigure tooltipLayer, IFigure hoverSource,
			int eventX, int eventY) {
		Rectangle parentBounds = hoverSource.getBounds().getCopy();
		hoverSource.translateToAbsolute(parentBounds);
		translateToRelative(parentBounds);
		return parentBounds.contains(new Point(eventX, eventY));
	}
}
