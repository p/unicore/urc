/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.borders;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.TitleBarBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;

import de.fzj.unicore.rcp.common.utils.ColorUtils;

/**
 * @author demuth
 * 
 */
public class TitleBarBorderWithImage extends TitleBarBorder {
	
	private Image image = null;
	
	private int VERTICAL_SPACE = 2;
	private int HORIZONTAL_SPACE = 2;
	
	public TitleBarBorderWithImage() {
		this(null);
	}




	public TitleBarBorderWithImage(String s) {
		super(s);
		setTextColor(ColorUtils.getColor(SWT.COLOR_TITLE_FOREGROUND));
	}




	public void paint(IFigure figure, Graphics g, Insets insets) {
		tempRect.setBounds(getPaintRectangle(figure, insets));
		Rectangle rec = tempRect;
		Insets padding = getPadding();
		rec.height = Math.min(rec.height, getTextExtents(figure).height
				+ padding.getHeight());
		g.clipRect(rec);
		g.setBackgroundColor(ColorUtils.getColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT));
		g.setForegroundColor(ColorUtils.getColor(SWT.COLOR_TITLE_BACKGROUND));
		g.fillGradient(rec, false);

		int x = rec.x + padding.left;
		int y = rec.y + padding.top;

		int textWidth = getTextExtents(figure).width;
		int freeSpace = rec.width - padding.getWidth() - textWidth;
		if(image != null) 
		{
			if(image.isDisposed()) return;
			freeSpace -= image.getBounds().width;
		}
		
		if (getTextAlignment() == PositionConstants.CENTER)
			freeSpace /= 2;
		if (getTextAlignment() != PositionConstants.LEFT)
			x += freeSpace;

		g.setFont(getFont(figure));
		g.setForegroundColor(getTextColor());
		g.drawString(getLabel(), x, y);
		
		if(image != null)
		{
			x = rec.right()-padding.right-image.getBounds().width;
			g.drawImage(image, x, y);
		}
	}
	
	protected Insets calculateInsets(IFigure figure) {
		return super.calculateInsets(figure);
	}
	
	public Dimension getPreferredSize(IFigure fig) {
		Dimension result = super.getPreferredSize(fig).getCopy();
		if(image != null) 
		{
			result.width += image.getBounds().width+HORIZONTAL_SPACE;
			result.height = Math.max(result.height,image.getBounds().height+VERTICAL_SPACE);
			
		}
		else result.width += result.height+5;
		return result;
	}


	public Image getImage() {
		return image;
	}


	public void setImage(Image image) {
		this.image = image;
	}

}
