package de.fzj.unicore.rcp.wfeditor.parts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ManhattanConnectionRouter;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.figures.DataSinkListFigure;
import de.fzj.unicore.rcp.wfeditor.figures.RoundedPolylineConnection;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSinkList;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.policies.ShowDataSinksAndSourcesEditPolicy;
import de.fzj.unicore.rcp.wfeditor.ui.WFRootEditPart;

public class DataSinkListPart extends AbstractWFEditPart implements
		PropertyChangeListener {

	protected boolean visible = false;

	private static final int ARROW_PANEL_HEIGHT = 14;

	/**
	 * A figure that is used to depict the data flow from the sinks to the
	 * sinks' parent activity.
	 */
	protected IFigure arrowPanel;

	protected FlowLayout layout;

	public DataSinkListPart() {
		super();
	}

	@Override
	public void activate() {
		super.activate();
		ShowDataFlowsProperty prop = (ShowDataFlowsProperty) getActivity()
				.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		visible = prop != null && prop.isShowingSources()
				&& prop.isShowingAnyDataTypeAtAll();
		getFigure().setVisible(visible);
		refreshChildren();
		refreshVisuals();
		getActivity().addPropertyChangeListener(this);
		getModel().addPropertyChangeListener(this);
	}

	protected Rectangle computeBounds(int numSinks) {
		Point location = getActivity().getLocation();
		Dimension size = getActivity().getSize();
		int y = location.y - WFConstants.DEFAULT_DATA_CONNECTOR_HEIGHT
				- ARROW_PANEL_HEIGHT;
		if (numSinks == 0) {
			return new Rectangle(location.x + size.width / 2, y, 0, 0);
		} else {
			int totalChildrenWidth = 0;
			for (Object o : getChildren()) {
				if (o instanceof GraphicalEditPart) {
					GraphicalEditPart e = (GraphicalEditPart) o;
					totalChildrenWidth += e.getFigure().getPreferredSize().width;
				}
			}

			int totalWidth = totalChildrenWidth
					+ getBetweenSinksPadding(numSinks) * (numSinks - 1);
			int totalX = location.x + size.width / 2 - totalWidth / 2;
			int totalHeight = WFConstants.DEFAULT_DATA_CONNECTOR_HEIGHT
					+ ARROW_PANEL_HEIGHT;
			return new Rectangle(totalX, y, totalWidth, totalHeight);
		}

	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(WFConstants.POLICY_SHOW_DATA_SINKS_AND_SOURCES,
				new ShowDataSinksAndSourcesEditPolicy(getParent()));
	}

	@Override
	protected IFigure createFigure() {
		IFigure result = new DataSinkListFigure(getParent());
		layout = new FlowLayout();
		layout.setHorizontal(true);
		layout.setMajorSpacing(0);
		result.setLayoutManager(layout);
		return result;
	}

	@Override
	public void deactivate() {
		super.deactivate();
		getActivity().removePropertyChangeListener(this);
		getModel().removePropertyChangeListener(this);
	}

	protected IActivity getActivity() {

		return getParent().getModel();
	}

	protected int getBetweenSinksPadding(int numSinks) {
		return WFConstants.DEFAULT_BETWEEN_SINKS_PADDING;
	}

	@Override
	public DataSinkList getModel() {
		return (DataSinkList) super.getModel();
	}

	@Override
	protected List<IDataSink> getModelChildren() {
		List<IDataSink> result = new ArrayList<IDataSink>();
		if (visible) {
			for (IDataSink sink : getModel().getDataSinks()) {
				if (sink.isVisible()) {
					result.add(sink);
				}
			}
			Collections.sort(result);
		}
		return result;
	}

	@Override
	public ActivityPart getParent() {
		return (ActivityPart) super.getParent();
	}

	public void propertyChange(final PropertyChangeEvent evt) {
		Display d = PlatformUI.getWorkbench().getDisplay();
		if (!d.isDisposed()) {
			d.asyncExec(new Runnable() {
				public void run() {
					if (!isActive()) {
						return; // we might have become disposed of in the
								// meantime
					}
					if (evt.getSource() == getActivity()) {
						if (IFlowElement.PROP_SHOW_DATA_FLOWS.equals(evt
								.getPropertyName())) {
							ShowDataFlowsProperty prop = (ShowDataFlowsProperty) evt
									.getNewValue();
							visible = prop == null ? false : prop
									.isShowingSinks()
									&& prop.isShowingAnyDataTypeAtAll();
							refreshChildren();
							refreshVisuals();

							getFigure().setVisible(visible);
						} else {
							refreshChildren();
							refreshVisuals();
						}
					} else if (evt.getSource() == getModel()) {
						refreshChildren();
						refreshVisuals();
					}
				}
			});
		}
	}

	@Override
	public void refresh() {
		refreshChildren(); // we need to refresh children first before
							// refreshing our visuals
		refreshVisuals();
	}

	@Override
	protected void refreshVisuals() {
		if (getFigure() != null && getFigure().getParent() != null) {
			int numChildren = getChildren().size();
			Rectangle bounds = computeBounds(numChildren);

			layout.setMinorSpacing(getBetweenSinksPadding(numChildren));
			getFigure().getParent().setConstraint(getFigure(), bounds);

			if (arrowPanel != null) {
				getFigure().remove(arrowPanel);
			}
			arrowPanel = new Panel();
			arrowPanel.setPreferredSize(new Dimension(bounds.width,
					ARROW_PANEL_HEIGHT));
			getFigure().add(arrowPanel);
			int i = 0;
			for (Object o : getChildren()) {
				if (o instanceof GraphicalEditPart) {
					GraphicalEditPart part = (GraphicalEditPart) o;

					RoundedPolylineConnection conn = new RoundedPolylineConnection();
					arrowPanel.add(conn);
					conn.setConnectionRouter(new ManhattanConnectionRouter());
					conn.setSourceAnchor(new BottomAnchor(part.getFigure(), -1));
					conn.setTargetAnchor(new TopAnchor(getParent().getFigure(),
							-1));
					if (i == numChildren / 2) {
						conn.setTargetDecoration(new PolygonDecoration());
					}
					i++;
				}
			}
		}
		super.refreshVisuals();
		for (Object o : getChildren()) {
			if (o instanceof EditPart) {
				EditPart part = (EditPart) o;
				part.refresh();
			}
		}
	}

	@Override
	public void removeNotify() {
		IFigure dataLayer = getLayer(WFRootEditPart.DATA_FLOW_LAYER);
		if (dataLayer.getChildren().contains(getFigure())) {
			dataLayer.remove(getFigure());
		}
		super.removeNotify();
	}
}
