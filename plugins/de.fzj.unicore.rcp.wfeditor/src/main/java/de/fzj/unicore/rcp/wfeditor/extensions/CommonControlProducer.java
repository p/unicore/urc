package de.fzj.unicore.rcp.wfeditor.extensions;

import java.util.HashSet;
import java.util.Set;

import de.fzj.unicore.rcp.wfeditor.ui.IConditionChangeListener;
import de.fzj.unicore.rcp.wfeditor.ui.IConditionTypeControlProducer;

/**
 * Producer for {@link CommonConditionTypeControl}
 * 
 * This class captures commonalities among {@link CommonConditionTypeControl}.
 * It is declared abstract, because some methods must be implemented for the
 * specific sub-types.
 * 
 * @author bjoernh
 * 
 */
public abstract class CommonControlProducer implements
		IConditionTypeControlProducer {

	protected CommonConditionTypeControl control;
	protected Set<IConditionChangeListener> listeners = new HashSet<IConditionChangeListener>();

	public void addConditionChangeListener(IConditionChangeListener l) {
		listeners.add(l);

	}

	public boolean isInputValid() {
		if (control == null) {
			return false;
		}
		return control.isValid();
	}

	public void removeConditionChangeListener(IConditionChangeListener l) {
		listeners.remove(l);
	}

}
