/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.conditions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.IConditionTypeExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.model.structures.RepeatUntilActivity;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

/**
 * @author demuth
 * 
 *         An object that represents a conditional expression that appears
 *         somewhere in the workflow. There are different types of conditions
 *         that are capable of comparing different values against different
 *         expressions. New types may be introduced through an extension point.
 */
@XStreamAlias("Condition")
public class Condition extends PropertySource {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3062029067652985319L;

	/**
	 * A condition is always stored in a property source. This is the key for
	 * accessing the condition in the parent property source.
	 */
	public static String PROP_CONDITION = "condition";

	/**
	 * A key for storing a String value inside this condition that is prepended
	 * to the String returned by {@link #getDisplayedExpression()}. This can be
	 * used to indicate the semantics of the condition, e.g. the
	 * {@link RepeatUntilActivity} uses it to indicate that the condition is
	 * actually negated.
	 * 
	 */
	public static String PROP_DISPLAYED_PREFIX = "prefix";

	private IConditionalElement parent;
	private IConditionType selectedType = null;

	private IFlowElement definingElement;
	private List<IConditionType> availableTypes = new ArrayList<IConditionType>();

	/**
	 * The element in the workflow to which the condition belongs.
	 * 
	 * @param definingElement
	 * @param parent
	 */
	public Condition(IFlowElement definingElement, IConditionalElement parent) {
		this.definingElement = definingElement;
		this.parent = parent;

	}

	@Override
	protected void addPropertyDescriptors() {

	}

	@Override
	public void dispose() {
		super.dispose();
		for (IConditionType type : availableTypes) {
			type.dispose();
		}
	}

	public String[] getAvailableTypeNames() {
		String[] result = new String[getAvailableTypes().size()];
		int i = 0;
		for (IConditionType type : getAvailableTypes()) {
			result[i++] = type.getTypeName();
		}
		return result;
	}

	public List<IConditionType> getAvailableTypes() {
		availableTypes = updateAvailableTypes();
		return availableTypes;
	}

	public IConditionalElement getConditionalElement() {
		return parent;
	}

	@Override
	public Condition getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		Condition copy = (Condition) super.getCopy(toBeCopied, mapOfCopies,
				copyFlags);
		mapOfCopies.put(this, copy);
		copy.setDefiningElement(CloneUtils.getFromMapOrCopy(
				getDefiningElement(), toBeCopied, mapOfCopies, copyFlags));

		copy.setConditionalElement(CloneUtils.getFromMapOrCopy(
				getConditionalElement(), toBeCopied, mapOfCopies, copyFlags));

		List<IConditionType> availableTypeCopies = new ArrayList<IConditionType>(
				availableTypes.size());
		boolean typeFound = false;
		for (IConditionType type : getAvailableTypes()) {
			IConditionType typeCopy = CloneUtils.getFromMapOrCopy(type,
					toBeCopied, mapOfCopies, copyFlags);
			availableTypeCopies.add(typeCopy);
			if (type == selectedType) {
				typeFound = true;
				copy.selectedType = typeCopy;
			}
		}
		if (!typeFound) {
			copy.selectedType = null;
		}
		copy.availableTypes = availableTypeCopies;
		return copy;
	}

	public IFlowElement getDefiningElement() {
		return definingElement;
	}

	public String getDisplayedExpression() {
		if (getSelectedType() == null) {
			return "no condition";
		}
		String prefix = (String) getPropertyValue(PROP_DISPLAYED_PREFIX);
		if (prefix == null) {
			prefix = "";
		}
		return prefix + getSelectedType().getDisplayedExpression();
	}

	public String getInternalExpression() throws Exception {
		return getSelectedType().getInternalExpression();
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_DISPLAYED_PREFIX);
		return propertiesToCopy;
	}

	public IConditionType getSelectedType() {
		if (selectedType == null) {
			setSelectedType(getAvailableTypes().get(0));
		}
		return selectedType;
	}

	private Object readResolve() {
		return this;
	}

	public boolean selectType(String typeName) {
		if (typeName == null) {
			return false;
		}
		IConditionType type = null;
		for (IConditionType current : getAvailableTypes()) {
			if (typeName.equals(current.getTypeName())) {
				type = current;
			}
		}
		if (type != null) {
			this.selectedType = type;
		}
		return type != null;
	}

	public void setConditionalElement(IConditionalElement parent) {
		this.parent = parent;
	}

	public void setDefiningElement(IFlowElement definingElement) {
		this.definingElement = definingElement;
	}

	public void setSelectedType(IConditionType type) {
		this.selectedType = type;
		if (availableTypes == null || availableTypes.size() == 0) {
			availableTypes = updateAvailableTypes();
		}
	}

	@Override
	public String toString() {
		return getDisplayedExpression();
	}

	@Override
	public void undoDispose() {
		super.undoDispose();
		for (IConditionType type : availableTypes) {
			if (type.isDisposed()) {
				type.undoDispose();
			}
		}

	}

	public List<IConditionType> updateAvailableTypes() {
		List<IConditionType> newAvailableTypes = new ArrayList<IConditionType>();
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(WFConstants.EXTENSION_POINT_CONDITION_TYPE);
		IConfigurationElement[] members = extensionPoint
				.getConfigurationElements();

		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			try {
				String id = member.getDeclaringExtension()
						.getUniqueIdentifier();
				IConditionType type = null;
				if (selectedType != null
						&& selectedType.getExtensionId().equals(id)) {
					type = selectedType;
				}
				if (type == null) {
					// reuse existing type if available
					for (IConditionType existingType : availableTypes) {
						if (existingType.getExtensionId().equals(id)) {
							type = existingType;
							break;
						}
					}
				}
				if (type == null) {
					// type not found. create a new instance
					IConditionTypeExtensionPoint ext = (IConditionTypeExtensionPoint) member
							.createExecutableExtension("name");
					type = ext.createNewConditionType();
				}
				newAvailableTypes.add(type);
			} catch (CoreException ex) {
				WFActivator
						.log("Could not determine all available types of conditions",
								ex);
			}
		}
		return newAvailableTypes;
	}

}
