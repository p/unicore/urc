/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.actions;

import java.util.List;

import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.DeleteRetargetAction;
import org.eclipse.gef.ui.actions.RedoRetargetAction;
import org.eclipse.gef.ui.actions.UndoRetargetAction;
import org.eclipse.gef.ui.actions.ZoomComboContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.actions.RetargetAction;

import de.fzj.unicore.rcp.common.actions.SubmittableEditorActionBarContributor;
import de.fzj.unicore.rcp.common.guicomponents.IEditorPartWithActions;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;

/**
 * Contributes actions to the Editor.
 * 
 * @author Daniel Lee
 */
public class FlowActionBarContributor extends
		SubmittableEditorActionBarContributor {

	private ActionRegistry registry = new ActionRegistry();

	/**
	 * manage additional actions per editor instance
	 * 
	 * @param editor
	 */
	@Override
	protected void buildEditorSpecificActions(IEditorPart edit) {
		WFEditor editor = (WFEditor) edit;

		List<WFEditorAction> actions = editor.getWfEditorActions();
		for (WFEditorAction action : actions) {

			WFEditorRetargetAction retargetAction = new WFEditorRetargetAction(
					action);
			addEditorSpecificAction(retargetAction,
					retargetAction.isAddedToLocalToolbar(),
					retargetAction.isAddedToContextMenu());

		}

	}

	@Override
	protected void buildGlobalActions() {
		super.buildGlobalActions();
		RetargetAction undo = new UndoRetargetAction();
		RetargetAction redo = new RedoRetargetAction();

		RetargetAction delete = new DeleteRetargetAction();

		addGlobalAction(undo, true, true);
		addGlobalAction(redo, true, true);
		addGlobalAction(delete, true, true);

	}

	@Override
	public void contributeToToolBar(IToolBarManager toolBarManager) {
		super.contributeToToolBar(toolBarManager);
		toolBarManager.add(new ZoomComboContributionItem(getPage()));
		toolBarManager.update(true);
	}

	/**
	 * Disposes the contributor. Removes all {@link RetargetAction}s that were
	 * {@link org.eclipse.ui.IPartListener}s on the
	 * {@link org.eclipse.ui.IWorkbenchPage} and disposes them. Also disposes
	 * the action registry.
	 * <P>
	 * Subclasses may extend this method to perform additional cleanup.
	 * 
	 * @see org.eclipse.ui.part.EditorActionBarContributor#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();
		registry.dispose();
		registry = null;
	}

	/**
	 * Retrieves an action from the action registry using the given ID.
	 * 
	 * @param id
	 *            the ID of the sought action
	 * @return <code>null</code> or the action if found
	 */
	@Override
	protected IAction getAction(IEditorPartWithActions editor, String id) {
		IAction result = getActionRegistry().getAction(id);
		if (result != null) {
			return result;
		} else {
			return super.getAction(editor, id);
		}
	}

	/**
	 * returns this contributor's ActionRegsitry.
	 * 
	 * @return the ActionRegistry
	 */
	protected ActionRegistry getActionRegistry() {
		return registry;
	}

	@Override
	public void setActiveEditor(IEditorPart editor) {
		super.setActiveEditor(editor);
	}

}
