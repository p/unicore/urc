/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gef.tools.CreationTool;
import org.eclipse.gef.tools.SelectionTool;
import org.eclipse.gef.ui.palette.editparts.PaletteEditPart;
import org.eclipse.swt.events.MouseEvent;

/**
 * @author demuth
 * 
 */
public class DefaultCreationTool extends CreationTool {

	private EditPart sourcePart = null; 
	
	@Override
	public void mouseHover(MouseEvent me, EditPartViewer viewer) {
		if (getCurrentViewer() != null) {
			EditPart editPart = getCurrentViewer()
					.findObjectAtExcluding(getLocation(), getExclusionSet());
			if (editPart instanceof PaletteEditPart && editPart != sourcePart) {
				// the user is hovering over another palette entry and might
				// want to use it!
				eraseTargetFeedback();
				unlockTargetEditPart();
				EditDomain domain = getCurrentViewer().getEditDomain();
				SelectionTool selectionTool = new SelectionTool();
				domain.setActiveTool(selectionTool);
				setState(STATE_TERMINAL);
				handleFinished();
				selectionTool.activate();
				selectionTool.mouseMove(me, viewer);
				return;
			}
		}
		super.mouseHover(me, viewer);
	}

	public void activate() {
		super.activate();
	}
	
	protected boolean updateTargetUnderMouse() {
		if (sourcePart == null) {
			sourcePart = getCurrentViewer()
					.findObjectAtExcluding(getLocation(), getExclusionSet());
			
		}
		return super.updateTargetUnderMouse();
	}
	
	protected void handleFinished() {
		sourcePart = null;
		super.handleFinished();
		
	}


	/**
	 * Sets the location (and size if the user is performing size-on-drop) of
	 * the request.
	 * 
	 * @see org.eclipse.gef.tools.TargetingTool#updateTargetRequest()
	 */
	@Override
	protected void updateTargetRequest() {

		CreateRequest req = getCreateRequest();
		if (isInState(STATE_DRAG_IN_PROGRESS)) {
			Point loq = getStartLocation();
			Rectangle bounds = new Rectangle(loq, loq);
			bounds.union(loq.getTranslated(getDragMoveDelta()));
			// req.setSize(bounds.getSize());
			req.setSize(null);
			req.setLocation(bounds.getLocation());
			req.getExtendedData().clear();
			// if (!getCurrentInput().isAltKeyDown() && helper != null) {
			// PrecisionRectangle baseRect = new PrecisionRectangle(bounds);
			// PrecisionRectangle result = baseRect.getPreciseCopy();
			// helper.snapRectangle(req, PositionConstants.NSEW,
			// baseRect, result);
			// req.setLocation(result.getLocation());
			// req.setSize(result.getSize());
			// }
		} else {
			req.setSize(null);
			req.setLocation(getLocation());
		}

	}

}
