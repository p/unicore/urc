package de.fzj.unicore.rcp.wfeditor.ui;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;

import com.thoughtworks.xstream.XStream;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.utils.XStreamConfigurator;

public class WFExportWizard extends WFSubmissionWizard implements IExportWizard {

	public static final String UPLOAD_INPUT_FILES = "upload workflow input files";

	private boolean uploadingFiles;

	public WFExportWizard() {
		this(null);

	}

	public WFExportWizard(WorkflowDiagram diagram) {
		super(diagram, null);
		setWindowTitle("Export Workflow");
		setTaskName("exporting workflow");

	}

	@Override
	public void addPages() {

		if (getWorkflowDiagram() == null) {
			WizardPage page = new WizardPage("") {

				public void createControl(Composite parent) {
					Label errorLabel = new Label(parent, SWT.NONE);
					errorLabel.setText("Selected file is not a workflow!");
					setControl(errorLabel);
				}
			};
			page.setDescription("Invalid selection");
			addPage(page);

		} else {
			super.addPages();
		}

	}

	@Override
	public void createPageControls(Composite pageContainer) {

		if (getWorkflowDiagram() != null) {

			Action checkBox = new Action("upload input files",
					IAction.AS_CHECK_BOX) {
				@Override
				public void setChecked(boolean checked) {
					String path = "checked.png";
					if (!checked) {
						path = "un" + path;
					}
					setImageDescriptor(WFActivator.getImageDescriptor(path));
					super.setChecked(checked);
					updateContributedPages();
					uploadingFiles = checked;
				}

			};

			checkBox.setChecked(false);
			checkBox.setId(UPLOAD_INPUT_FILES);
			ActionContributionItem item = new ActionContributionItem(checkBox);
			getPage1().getLowerToolBarManager().insert(0, item);
			super.createPageControls(pageContainer);
		}

	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		List<?> selectedResources = computeSelectedResources(selection);
		if (selectedResources.size() == 0) {
			return;
		} else if (selectedResources.size() == 1) {
			IFile file = (IFile) ((IAdaptable) selectedResources.get(0))
					.getAdapter(IFile.class);

			try {
				InputStream is = file.getContents(false);
				if (is.available() > 0) {
					XStream xstream = XStreamConfigurator.getInstance()
							.getXStreamForDeserialization();
					WorkflowDiagram diagram = (WorkflowDiagram) xstream
							.fromXML(new InputStreamReader(
									new BufferedInputStream(is)));
					is.close();

					diagram.setFilename(file.getName());
					IProject project = file.getProject();
					String parentFolder = file.getParent()
							.getProjectRelativePath().toPortableString();
					diagram.setParentFolder(parentFolder);
					diagram.setProject(project);

					// diagram has been deserialized
					// try to inform it now.

					diagram.afterDeserialization();
					diagram.activate();
					for (IActivity a : diagram.getActivities().values()) {
						a.afterDeserialization();
						a.activate();
						for (Transition t : a.getOutgoingTransitions()) {
							t.afterDeserialization();
							t.activate();
						}

					}

					diagram.loadExecutionData();
					setWorkflowDiagram(diagram);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	protected void initSubmissionRunnable() {
		if (submissionRunnable == null) {
			submissionRunnable = new WFExportRunnable();
		}
		if (submissionRunnable instanceof WFExportRunnable) {
			WFExportRunnable exportRunnable = (WFExportRunnable) submissionRunnable;
			exportRunnable.setSelectedNode(getSelectedNode());
			exportRunnable.setSelectedSubmitter(getSelectedSubmitter());
			exportRunnable.setWorkflowDiagram(getWorkflowDiagram());
			exportRunnable.setUploadingFiles(uploadingFiles);
		}
	}

	@Override
	public boolean performCancel() {
		initSubmissionRunnable();
		return super.performCancel();
	}

	@Override
	public boolean performFinish() {
		initSubmissionRunnable();
		return super.performFinish();
	}
	
	private static List computeSelectedResources(
			IStructuredSelection originalSelection) {
		List resources = null;
		for (Iterator e = originalSelection.iterator(); e.hasNext();) {
			Object next = e.next();
			Object resource = null;
			if (next instanceof IResource) {
				resource = next;
			} else if (next instanceof IAdaptable) {
				resource = ((IAdaptable) next).getAdapter(IResource.class);
			}
			if (resource != null) {
				if (resources == null) {
					// lazy init to avoid creating empty lists
					// assume selection contains mostly resources most times
					resources = new ArrayList(originalSelection.size());
				}
				resources.add(resource);
			}
		}
		if (resources == null) {
			return Collections.EMPTY_LIST;
		}
		return resources;

	}
}
