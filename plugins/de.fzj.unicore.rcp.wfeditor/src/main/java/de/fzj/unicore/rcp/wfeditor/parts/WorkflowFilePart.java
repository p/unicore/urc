package de.fzj.unicore.rcp.wfeditor.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.figures.DataSourceFigure;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowFileUtils;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowUtils;

public class WorkflowFilePart extends DataSourcePart implements
		IPropertyChangeListener {

	Color mimeTypeColor = null;
	String mimeType = null;

	@Override
	public void activate() {
		super.activate();

	}

	@Override
	protected IFigure createFigure() {
		DataSourceFigure result = (DataSourceFigure) super.createFigure();

		return result;
	}

	@Override
	public void deactivate() {

		if (mimeTypeColor != null) {
			WFActivator.getDefault().getPreferenceStore()
					.removePropertyChangeListener(this);
			mimeTypeColor.dispose();
		}
		super.deactivate();
	}

	@Override
	public WorkflowFile getModel() {
		return (WorkflowFile) super.getModel();
	}

	public void propertyChange(PropertyChangeEvent event) {
		if (event.getProperty().equals(
				WFConstants.P_MIME_COLOR_PREFIX + mimeType)) {
			RGB rgb = (RGB) event.getNewValue();
			if (rgb.equals(mimeTypeColor.getRGB())) {
				return;
			}
			Color temp = mimeTypeColor;
			mimeTypeColor = new Color(temp.getDevice(), rgb);
			((DataSourceFigure) getFigure()).setDataTypeColor(mimeTypeColor);
			temp.dispose();
			getFigure().repaint();
		}
	}

	@Override
	protected void refreshVisuals() {

		super.refreshVisuals();
		if (!(getFigure() instanceof DataSourceFigure)) {
			return;
		}
		DataSourceFigure figure = (DataSourceFigure) getFigure();
		String symbol = getModel().getDisplayedString();
		String typeName = "any";
		if (getModel().getOutgoingDataTypes() != null
				&& getModel().getOutgoingDataTypes().size() > 0) {
			int i = 0;
			for (String mime : getModel().getOutgoingDataTypes()) {

				RGB rgb = WorkflowFileUtils.getColorForMimeType(mime);
				if (rgb == null) {
					rgb = WorkflowFileUtils.getMimeColorGenerator().nextColor(
							mime);
					WorkflowFileUtils.registerColorForMimeType(mime, rgb);
				}
				if (mimeTypeColor == null) {
					mimeTypeColor = new Color(getViewer().getControl()
							.getDisplay(), rgb);
					mimeType = mime;
					figure.setDataTypeColor(mimeTypeColor);
					WFActivator.getDefault().getPreferenceStore()
							.addPropertyChangeListener(this);
				}

				if (i == 0) {
					typeName = mime;

				}

				i++;
			}

		}
		figure.setDataTypeSymbol(symbol);
		int width = WorkflowUtils.getPreferredTextDimension(
				figure.getDataTypeSymbol(), figure.getFont()).width;
		figure.setPreferredSize(width, figure.getPreferredSize().height);
		figure.setToolTipText(new String[] {
				"File: " + getModel().getDisplayedString(), "Type: " + typeName });

	}
}
