package de.fzj.unicore.rcp.wfeditor.ui;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.ui.palette.PaletteEditPartFactory;

import de.fzj.unicore.rcp.wfeditor.parts.DataFlowCreationToolEntryPart;
import de.fzj.unicore.rcp.wfeditor.tools.DataFlowCreationToolEntry;

public class WFPaletteEditPartFactory extends PaletteEditPartFactory {

	Map<Class<?>, Class<?>> modelToPartMap = new HashMap<Class<?>, Class<?>>();
	WFEditor editor;
	
	public WFPaletteEditPartFactory(WFEditor editor) {
		registerPalettePart(DataFlowCreationToolEntry.class,
				DataFlowCreationToolEntryPart.class);
		this.editor = editor;
	}

	@Override
	public EditPart createEditPart(EditPart parentEditPart, Object model) {

		if (modelToPartMap.containsKey(model.getClass())) {
			Class<?> clazz = modelToPartMap.get(model.getClass());
			try {
				Constructor<?> constructor = clazz.getConstructor();
				EditPart result = (EditPart) constructor.newInstance();
				result.setModel(model);
				result.setParent(parentEditPart);
				return result;
			} catch (Exception e) {
				// do nothing
			}
		}
			
		if(editor.getRootPanel() != null)
		{
			// ty to fallback to the main viewer's edit part factory!
			// this makes this factory extensible
			EditPartFactory viewerFactory = editor.getRootPanel().getViewer().getEditPartFactory();
			EditPart result = viewerFactory.createEditPart(parentEditPart, model);
			if(result != null) 
			{
				return result;
			}
		}
		return super.createEditPart(parentEditPart, model);
	}

	/**
	 * This method can be used to register new part types for palette entries.
	 * The registered part classes must have a default constructor!!
	 * 
	 * @param modelClass
	 * @param partClass
	 */
	public void registerPalettePart(Class<?> modelClass, Class<?> partClass) {
		modelToPartMap.put(modelClass, partClass);
	}
}
