package de.fzj.unicore.rcp.wfeditor.utils;

import java.util.Iterator;
import java.util.Map;

public class PropertyMapComparator {

	public static boolean equal(Map<?, ?> m1, Map<?, ?> m2) {
		Iterator<?> i = m1.keySet().iterator();
		while (i.hasNext()) {
			Object key = i.next();
			Object o1 = m1.get(key);
			Object o2 = m2.get(key);
			boolean equal = o1 == null ? o2 == null : o1.equals(o2);
			if (!equal) {
				return false;
			}
		}
		return true;
	}

	public static int hashCode(Map<?, ?> m) {

		int hashCode = 1;
		Iterator<?> i = m.values().iterator();
		while (i.hasNext()) {
			Object obj = i.next();
			hashCode = 31 * hashCode + (obj == null ? 0 : obj.hashCode());
		}
		return hashCode;
	}

}
