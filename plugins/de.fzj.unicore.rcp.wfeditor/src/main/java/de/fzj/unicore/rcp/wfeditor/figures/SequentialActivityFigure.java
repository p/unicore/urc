/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

/**
 * @author hudsonr
 */
public class SequentialActivityFigure extends StructuredActivityFigure {

	static final MarginBorder MARGIN_BORDER = new MarginBorder(0, 8, 0, 0);

	static final PointList ARROW = new PointList(3);
	{
		ARROW.addPoint(0, 0);
		ARROW.addPoint(10, 0);
		ARROW.addPoint(5, 5);
	}

	/**
	 * @param header
	 * @param footer
	 */
	public SequentialActivityFigure(Rectangle bounds) {
		super(bounds);
		setBorder(MARGIN_BORDER);
		setOpaque(true);
	}

	@Override
	public void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		Color oldBG = graphics.getBackgroundColor();
		graphics.setBackgroundColor(ColorConstants.button);
		Rectangle r = getBounds();
		graphics.fillRectangle(r);
		graphics.setBackgroundColor(oldBG);
		graphics.fillRectangle(new Rectangle(r.x + 13, r.y + 3, r.width - 26,
				r.height - 6));
		// graphics.fillRectangle(r.x + 13, r.y + 10, 8, r.height - 18);
		// Point topRight = r.getTopRight();
		//
		// graphics.fillRectangle(topRight.x - 13, topRight.y + 10, 8, r.height
		// - 18);
		// graphics.fillPolygon(ARROW);
		// graphics.drawPolygon(ARROW);
	}

}
