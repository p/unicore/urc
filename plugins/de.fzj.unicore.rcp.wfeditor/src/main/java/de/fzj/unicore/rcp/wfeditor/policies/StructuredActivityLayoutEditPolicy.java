/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.draw2d.graph.Subgraph;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.layouts.IAutomaticGraphLayout;
import de.fzj.unicore.rcp.wfeditor.model.activities.Activity;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.commands.AddAndAssignSourceCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.AddCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.AdjustLayoutCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.AutomaticLayoutCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.CreateCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.DeleteTransitionCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.DisplacePeersCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.MoveActivityCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.SetConstraintCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.SetInnerPaddingCommand;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.parts.StructuredActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.WorkflowDiagramPart;
import de.fzj.unicore.rcp.wfeditor.requests.RemoveIllegalTransitionsRequest;
import de.fzj.unicore.rcp.wfeditor.requests.ResizeContainerRequest;

/**
 * @author Bastian Demuth
 */
public class StructuredActivityLayoutEditPolicy extends XYLayoutEditPolicy {

	/**
	 * Flag that can be set in {@link CreateRequest#getExtendedData()} in order
	 * to omit the layout adjustment command when generating a
	 * {@link CreateCommand}. This is useful when some other policy delegates
	 * the creation to this policy but wants to take actions BEFORE the layout
	 * adjustment step (like relocating activities).
	 */
	public static final String OMIT_LAYOUT_ADJUSTMENT_COMMAND_DURING_CREATION = "omit layout adjustment command";

	protected boolean canAddChildren() {
		return getModel().canAddChildren();
	}

	protected Command createAddCommand(EditPart child) {
		return null;
	}

	protected Command createAddCommand(IActivity child,
			StructuredActivity parent, Rectangle constraint) {
		CompoundCommand result = new CompoundCommand();
		AddCommand add = new AddCommand();
		add.setConstraints(constraint);
		add.setChild(child);
		add.setParent(parent);
		result.add(add);
		Command adjustLayoutCommand = null;

		if (!(getHost() instanceof WorkflowDiagramPart)) {
			adjustLayoutCommand = new AdjustLayoutCommand(child, getHost()
					.getViewer());
			result.add(adjustLayoutCommand);
		} else {
			result.add(new DisplacePeersCommand(child, getHost().getViewer()));
		}

		return result;
	}

	@Override
	protected Command createChangeConstraintCommand(
			ChangeBoundsRequest request, EditPart child, Object constraint) {
		SetConstraintCommand cmd = new SetConstraintCommand();
		IActivity activity = (IActivity) child.getModel();
		cmd.setPart(activity);
		Rectangle r = (Rectangle) constraint;

		cmd.setLocation(r);
		Command result = cmd;

		if(child instanceof StructuredActivityPart)
		{
			StructuredActivityPart structure = (StructuredActivityPart) child;
			
			IFigure structureFigure = structure.getFigure();
			Rectangle outer = r.getCopy().getCropped(structureFigure.getInsets());
			Insets i = new Insets(WFConstants.INNER_PADDING);
			CompoundDirectedGraph graph = new CompoundDirectedGraph();
			graph.setDefaultPadding(WFConstants.BETWEEN_ACTIVITY_PADDING);
			graph.setMargin(WFConstants.ZERO_INSETS);
			Map<Object,Object> partsToNodes = new HashMap<Object,Object>();
			Subgraph sub = new Subgraph("a");
			graph.nodes.add(sub);
			structure.contributeNodesToGraph(graph, sub, partsToNodes);
			for(Object key : partsToNodes.keySet())
			{
				
				if(key != structure)
				{
					Node n = (Node) partsToNodes.get(key);
					Rectangle inner = new Rectangle(n.x,n.y,n.width,n.height);
					int diff = inner.getTop().y-outer.getTop().y;
					if(diff < i.top) i.top = diff;
					diff = outer.getBottom().y-inner.getBottom().y;
					if(diff < i.bottom) i.bottom = diff;
					diff = inner.getLeft().x-outer.getLeft().x;
					if(diff < i.left) i.left = diff;
					diff = outer.getRight().x-inner.getRight().x;
					if(diff < i.right) i.right = diff;
				}
			}
			
			SetInnerPaddingCommand pad = new SetInnerPaddingCommand(structure.getModel(), i);
			result = result.chain(pad);
			

		}
		AdjustLayoutCommand adjust = new AdjustLayoutCommand(activity, getHost().getViewer());
		return result.chain(adjust);
//		ResizeContainerRequest resizeReq = new ResizeContainerRequest();
//		Command resizeCmd = getHost().getCommand(resizeReq);
//		if (resizeCmd != null) {
//			return cmd.chain(resizeCmd);
//		} else {
//			return result;
//		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editpolicies.ConstrainedLayoutEditPolicy#
	 * createChangeConstraintCommand(org.eclipse.gef.EditPart, java.lang.Object)
	 */
	@Override
	protected Command createChangeConstraintCommand(EditPart child,
			Object constraint) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see org.eclipse.gef.editpolicies.OrderedLayoutEditPolicy#createChildEditPolicy(org.eclipse.gef.EditPart)
	 */
	@Override
	protected EditPolicy createChildEditPolicy(EditPart child) {
		return new WFResizableEditPolicy();
	}

	//	private Command createMoveChildCommand(IActivity child) {
	//
	//		Rectangle constraint = new Rectangle(child.getLocation(),
	//				child.getSize());
	//		IActivity myModel = ((IActivity) getHost().getModel());
	//		Rectangle myConstraint = new Rectangle(myModel.getLocation(),
	//				myModel.getSize());
	//		Point target = myConstraint.getBottom();
	//		target.translate(0, WFConstants.BETWEEN_ACTIVITY_PADDING.bottom);
	//		Dimension delta = target.getDifference(constraint.getTop());
	//		constraint.translate(delta.width, delta.height);
	//		MoveActivityCommand cmd = new MoveActivityCommand(child,
	//				constraint.getTopLeft());
	//		return cmd;
	//	}

	@Override
	protected Command getAddCommand(Request req) {
		ChangeBoundsRequest request = (ChangeBoundsRequest) req;
		List<?> editParts = request.getEditParts();
		CompoundCommand result = new CompoundCommand();
		for (int i = 0; i < editParts.size(); i++) {
			GraphicalEditPart child = (GraphicalEditPart) editParts.get(i);
			if (!(child.getModel() instanceof IActivity)) {
				continue;
			}
			IActivity activity = (IActivity) child.getModel();
			Rectangle r = null;

			if (child.getParent() != null) // test if child has already been
				// added to the diagram, this is not
				// true if the add command is
				// created from a CreateCommand
			{
				r = child.getFigure().getBounds().getCopy();
				child.getFigure().translateToAbsolute(r);
				r = request.getTransformedRectangle(r);

				// convert this figure to relative
				getLayoutContainer().translateToRelative(r);
				getLayoutContainer().translateFromParent(r);
				r.translate(getLayoutOrigin().getNegated());
				r = (Rectangle) getConstraintFor(r);
			} else {
				r = new Rectangle(activity.getLocation(), activity.getSize());
				r = request.getTransformedRectangle(r);
			}
			if (canAddChildren()) {
				Command addCmd = createAddCommand(activity, getModel(), r);
				result.add(addCmd);
			} else if (getModel().getParent().canAddChildren()) {

				r = new Rectangle(activity.getLocation(), activity.getSize()); // don't
				// move
				// the
				// activity
				Command addCmd = createAddCommand(activity, getModel()
						.getParent(), r);
				result.add(addCmd);
			}
		}

		if (result.canExecute()) {
			RemoveIllegalTransitionsRequest removeTransitionsReq = new RemoveIllegalTransitionsRequest();
			removeTransitionsReq.setEditParts(request.getEditParts());
			Command removeTransitionCommand = getHost().getCommand(
					removeTransitionsReq);
			if (removeTransitionCommand != null) {
				result.add(removeTransitionCommand);
			}

			if (!canAddChildren() && getModel().getParent().canAddChildren()) {
				for (int i = 0; i < editParts.size(); i++) {
					GraphicalEditPart child = (GraphicalEditPart) editParts
					.get(i);
					if (!(child.getModel() instanceof IActivity)) {
						continue;
					}
					IActivity activity = (IActivity) child.getModel();
					for (Transition t : activity.getIncomingTransitions()) {
						EditPart part = (EditPart) getHost().getViewer()
						.getEditPartRegistry().get(t.source);
						if (part != null
								&& part.getSelected() == EditPart.SELECTED_NONE) {
							DeleteTransitionCommand deleteTransitions = new DeleteTransitionCommand();
							deleteTransitions.setTransition(t);
							result.add(deleteTransitions);
						}
					}
					for (Transition t : activity.getOutgoingTransitions()) {
						EditPart part = (EditPart) getHost().getViewer()
						.getEditPartRegistry().get(t.source);
						if (part != null
								&& part.getSelected() == EditPart.SELECTED_NONE) {
							DeleteTransitionCommand deleteTransitions = new DeleteTransitionCommand();
							deleteTransitions.setTransition(t);
							result.add(deleteTransitions);
						}
					}
					AddAndAssignSourceCommand assignSourceCmd = new AddAndAssignSourceCommand();
					assignSourceCmd.setChild(activity);
					assignSourceCmd.setSource(getModel());
					result.add(assignSourceCmd);
				}
			}
		}
		return result.unwrap();
	}

	protected Command getCreateBelowCommand(CreateRequest request) {
		AddAndAssignSourceCommand cmd = new AddAndAssignSourceCommand();

		Activity child = (Activity) request.getNewObject();
		cmd.setChild(child);
		cmd.setSource((IActivity) getHost().getModel());

		Command result = cmd;
		Command createCommand = getHost().getParent().getCommand(request);
		if (createCommand != null) {
			result = result.chain(createCommand);
		}
		// result = result.chain(createMoveChildCommand(child));
		Command resizeParentCommand = getHost().getParent().getCommand(
				new ResizeContainerRequest());
		if (resizeParentCommand != null) {
			result = result.chain(resizeParentCommand);
		}

		return result;
	}

	/**
	 * @see LayoutEditPolicy#getCreateCommand(org.eclipse.gef.requests.CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(CreateRequest request) {
		if (!canAddChildren()) {
			return getCreateBelowCommand(request);
		}
		CompoundCommand result = new CompoundCommand();
		CreateCommand createCommand = new CreateCommand();
		Activity child = (Activity) request.getNewObject();

		createCommand.setParent(getHost());
		createCommand.setChild(child);
		try {
			Rectangle constraint = ((Rectangle) getConstraintFor(request))
			.getCopy();
			if (constraint.isEmpty()) {
				constraint.setSize(child.getSize());
				// with the following lines, activities are created horizontally
				// centered under the mouse
				Dimension dim = constraint.getTopLeft().getDifference(
						constraint.getCenter());
				constraint.translate(dim.width, 0);
			}

			createCommand.setLocation(constraint);
			result.add(createCommand);
			AddCommand addCmd = new AddCommand();
			addCmd.setChild(child);
			addCmd.setParent(getModel());
			result.add(addCmd);

			if (child instanceof StructuredActivity) {
				AutomaticLayoutCommand layout = new AutomaticLayoutCommand(
						(StructuredActivity) child, getHost().getViewer());
				// use defaults for layouting but do not reset inner paddings
				// resetting inner paddings wipes out the default inner padding
				// values set by the structured activities during creation
				layout.setFlags(IAutomaticGraphLayout.FLAG_DEFAULT ^ IAutomaticGraphLayout.FLAG_RESET_INNER_PADDINGS);
				result.add(layout);
			}

			if (request.getExtendedData().get(
					OMIT_LAYOUT_ADJUSTMENT_COMMAND_DURING_CREATION) == null) {
				result.add(new AdjustLayoutCommand(child, getHost().getViewer()));

			} else {
				request.getExtendedData().remove(
						OMIT_LAYOUT_ADJUSTMENT_COMMAND_DURING_CREATION);
			}

			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected Point getLayoutOrigin() {
		return new Point(0, 0);
	}

	protected StructuredActivity getModel() {
		return (StructuredActivity) getHost().getModel();
	}

	/**
	 * @see org.eclipse.gef.editpolicies.LayoutEditPolicy#getMoveChildrenCommand(org.eclipse.gef.Request)
	 */
	@Override
	protected Command getMoveChildrenCommand(Request req) {

		ChangeBoundsRequest request = (ChangeBoundsRequest) req;
		CompoundCommand move = new CompoundCommand();
		Command cmd;
		GraphicalEditPart child;
		List<?> children = request.getEditParts();

		for (int i = 0; i < children.size(); i++) {

			child = (GraphicalEditPart) children.get(i);
			if (!isMyChild(child)) {
				continue;
			}
			Rectangle rect = (Rectangle) translateToModelConstraint(getConstraintFor(
					request, child));
			Point newPos = rect.getTopLeft();
			IActivity activity = (IActivity) child.getModel();
			cmd = new MoveActivityCommand(activity, newPos);
			move.add(cmd);
			move.add(new AdjustLayoutCommand(activity, getHost().getViewer()));
		}
		Command result = move;
		return result;
	}

	/**
	 * Return null since the parent will produce a command via
	 * getMoveChildrenCommand().
	 * 
	 * @param request
	 *            the change bounds request
	 * @return the command contribution to the request
	 */
	protected Command getMoveCommand(ChangeBoundsRequest request) {
		return null;
	}

	protected boolean isMyChild(EditPart child) {
		return getHost().getChildren().contains(child);
	}

}
