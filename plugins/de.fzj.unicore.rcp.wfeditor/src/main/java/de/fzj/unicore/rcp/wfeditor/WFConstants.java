/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.actions.ActionFactory;

/**
 * @author demuth
 * 
 */
public class WFConstants {

	/**
	 * ID for the DataFlowExtensionPoint. Value:
	 * <code>"de.fzj.unicore.rcp.wfeditor.DataFlowExtensionPoint"</code>
	 */
	public static final String EXTENSION_POINT_DATA_FLOWS = "de.fzj.unicore.rcp.wfeditor.DataFlowExtensionPoint";//$NON-NLS-1$

	/**
	 * ID for the PaletteEntryExtensionPoint. Value:
	 * <code>"de.fzj.unicore.rcp.wfeditor.PaletteEntryExtensionPoint"</code>
	 */
	public static final String EXTENSION_POINT_PALETTE_ENTRY = "de.fzj.unicore.rcp.wfeditor.PaletteEntryExtensionPoint";//$NON-NLS-1$

	/**
	 * ID for the ModelConverterExtensionPoint. Value:
	 * <code>"de.fzj.unicore.rcp.wfeditor.ModelConverterExtensionPoint"</code>
	 */
	public static final String EXTENSION_POINT_MODEL_CONVERTER = "de.fzj.unicore.rcp.wfeditor.ModelConverterExtensionPoint";//$NON-NLS-1$

	/**
	 * ID for the EditorActionsExtensionPoint. Value:
	 * <code>"de.fzj.unicore.rcp.wfeditor.EditorActionsExtensionPoint"</code>
	 */
	public static final String EXTENSION_POINT_EDITOR_ACTIONS = "de.fzj.unicore.rcp.wfeditor.EditorActionsExtensionPoint";//$NON-NLS-1$

	/**
	 * ID for the Model2WFConverterExtensionPoint. Value:
	 * <code>"de.fzj.unicore.rcp.wfeditor.Model2WFConverterExtensionPoint"</code>
	 */
	public static final String EXTENSION_POINT_MODEL_2_WF_CONVERTER = "de.fzj.unicore.rcp.wfeditor.Model2WFConverterExtensionPoint";//$NON-NLS-1$

	/**
	 * ID for the CondtionTypeExtensionPoint. Value:
	 * <code>"de.fzj.unicore.rcp.wfeditor.ConditionTypeExtensionPoint"</code>
	 */
	public static final String EXTENSION_POINT_CONDITION_TYPE = "de.fzj.unicore.rcp.wfeditor.ConditionTypesExtensionPoint";//$NON-NLS-1$

	/**
	 * ID for the DiagramTypeExtensionPoint. Value:
	 * <code>"de.fzj.unicore.rcp.wfeditor.DiagramTypeExtensionPoint"</code>
	 */
	public static final String EXTENSION_POINT_DIAGRAM_TYPE = "de.fzj.unicore.rcp.wfeditor.DiagramTypeExtensionPoint";//$NON-NLS-1$

	/**
	 * ID for the SubmitterTypeExtensionPoint. Value:
	 * <code>"de.fzj.unicore.rcp.wfeditor.SubmitterTypeExtensionPoint"</code>
	 */
	public static final String EXTENSION_POINT_SUBMITTER = "de.fzj.unicore.rcp.wfeditor.SubmitterExtensionPoint";//$NON-NLS-1$

	/**
	 * Zoom in action id. Value:
	 * <code>"de.fzj.unicore.rcp.wfeditor.actions.zoom_in"</code>
	 */
	public static final String ACTION_ZOOM_IN = "de.fzj.unicore.rcp.wfeditor.actions.ZoomIn";//$NON-NLS-1$

	/**
	 * Zoom out action id. Value:
	 * <code>"de.fzj.unicore.rcp.wfeditor.actions.zoom_out"</code>
	 */
	public static final String ACTION_ZOOM_OUT = "de.fzj.unicore.rcp.wfeditor.actions.ZoomOut";//$NON-NLS-1$

	/**
	 * Layout diagram action id. Value:
	 * <code>"de.fzj.unicore.rcp.wfeditor.actions.layoutDiagram"</code>
	 */
	public static final String ACTION_LAYOUT_DIAGRAM = "de.fzj.unicore.rcp.wfeditor.actions.LayoutDiagram";//$NON-NLS-1$

	/**
	 * Edit policy that is responsible for resizing a container automatically.
	 */
	public static final String POLICY_AUTO_RESIZE_CONTAINER = "policy auto resize container"; //$NON-NLS-1$

	/**
	 * Edit policy that is responsible for "opening" a flow element e.g. when it
	 * gets double clicked. This will usually open a view (e.g. the
	 * PropertySheet view).
	 */
	public static final String POLICY_OPEN = "openFlowElement";

	/**
	 * Edit policy that is responsible for copying the selected element(s) to
	 * the clipboard.
	 */
	public static final String POLICY_COPY = "copyFlowElement";

	/**
	 * Edit policy that is responsible for cutting the selected element(s) from
	 * the diagram and keeping them in the clipboard.
	 */
	public static final String POLICY_CUT = "cutFlowElement";

	/**
	 * Edit policy that is responsible for pasting elements from the clipboard
	 * into the diagram.
	 */
	public static final String POLICY_PASTE = "pasteFlowElement";

	/**
	 * Edit policy that is responsible for removing transitions that are not
	 * valid anymore
	 */
	public static final String POLICY_REMOVE_ILLEGAL_TRANSITIONS = "removeIllegalTransitions";

	/**
	 * Edit policy that is responsible for showing data sinks and sources inside
	 * the workflow diagram
	 */
	public static final String POLICY_SHOW_DATA_SINKS_AND_SOURCES = "showDataSinksAndSources"; //$NON-NLS-1$

	/**
	 * Constant used to create a request indicating that a container shall be
	 * auto-resized due to a change in a child's constraint.
	 */
	public static final String REQ_AUTO_RESIZE_CONTAINER = "auto resize container"; //$NON-NLS-1$

	/**
	 * Constant used to create a request indicating that the selected element(s)
	 * shall be copied to the clipboard
	 */
	public static final String REQ_COPY = ActionFactory.COPY.getId();

	public static final String REQ_CUT = ActionFactory.CUT.getId();

	public static final String REQ_PASTE = ActionFactory.PASTE.getId();

	public static final String REQ_REMOVE_ILLEGAL_TRANSITIONS = "remove illegal transitions"; //$NON-NLS-1$

	/**
	 * Generic request for setting properties in model objects
	 */
	public static final String REQ_SET_PROPERTY = "set property request"; //$NON-NLS-1$

	/**
	 * Generic request for setting properties in model objects
	 */
	public static final String REQ_SHOW_DATA_SINKS_AND_SOURCES = "show data sinks and sources request"; //$NON-NLS-1$

	public static final String IMAGE_COLLAPSE = "collapse.gif";
	public static final String IMAGE_EXPAND = "expand.gif";
	public static final String IMAGE_ENABLED_BREAKPOINT = "breakpoint_enabled.gif";
	public static final String IMAGE_DISABLED_BREAKPOINT = "breakpoint_disabled.gif";

	public static final String IMAGE_DATA_SINK_PROBLEM = "data sink problem";

	public static final String IMAGE_WORFKLOW_FILE = "workflow_file.png";
	public static final String IMAGE_WORFKLOW_VARIABLE_INTEGER_SOURCE = "workflow_variable_integer_source.png";
	public static final String IMAGE_WORFKLOW_VARIABLE_STRING_SOURCE = "workflow_variable_string_source.png";

	public static final String IMAGE_WORFKLOW_VARIABLE_INTEGER_SINK = "workflow_variable_integer_sink.png";
	public static final String IMAGE_WORFKLOW_VARIABLE_STRING_SINK = "workflow_variable_string_sink.png";

	/**
	 * The default space that should be kept between activities and the border
	 * of subgraphs.
	 */
	public static Insets INNER_PADDING = new Insets(45, 15, 35, 15);

	/**
	 * The default space that should be kept between activities and the border
	 * of the workflow diagram.
	 */
	public static Insets INNER_DIAGRAM_PADDING = new Insets(45, 75, 35, 15);

	public static final Insets ZERO_INSETS = new Insets(0, 0, 0, 0);

	public static final Dimension DEFAULT_EMPTY_CONTAINER_SIZE = new Dimension(
			64, 64);

	public static final int DEFAULT_ACTIVITY_LABEL_HEIGHT = 15;

	public static final int DEFAULT_ACTIVITY_LABEL_WIDTH = 100;

	public static final int DEFAULT_DATA_CONNECTOR_FONT_SIZE = 9;

	public static final int DEFAULT_ENLARGED_DATA_CONNECTOR_FONT_SIZE = 10;

	public static final int DEFAULT_DATA_CONNECTOR_HEIGHT = 20;

	public static final int DEFAULT_DATA_CONNECTOR_WIDTH = 35;

	public static final int DEFAULT_BETWEEN_SOURCES_PADDING = 6;

	public static final int DEFAULT_BETWEEN_SINKS_PADDING = 6;

	/**
	 * The default space that should be kept between activities
	 */
	public static Insets BETWEEN_ACTIVITY_PADDING = new Insets(
			25 + DEFAULT_DATA_CONNECTOR_HEIGHT, 15,
			25 + DEFAULT_DATA_CONNECTOR_HEIGHT, 15);
	
	/**
	 * The minimal space that should be kept between activities
	 */
	public static Insets MINIMAL_BETWEEN_ACTIVITY_PADDING = new Insets(
			10, 5,
			10, 5);

	public static final Dimension DEFAULT_SIMPLE_ACTIVITY_SIZE = new Dimension(
			64, 64);

	public static final Color DEFAULT_TRANSITION_COLOR = ColorConstants.black;

	/**
	 * Constants defining where to create a connection anchor
	 */
	public static final int ANCHOR_TOP = 0;

	public static final int ANCHOR_BOTTOM = 1;

	public static final int ANCHOR_LEFT = 2;

	public static final int ANCHOR_RIGHT = 3;

	public static final int ANCHOR_TOP_INNER = 4;

	public static final int ANCHOR_BOTTOM_INNER = 5;

	public static final Integer CENTER = new Integer(PositionConstants.CENTER);
	public static final Integer TOP = new Integer(PositionConstants.TOP);
	public static final Integer BOTTOM = new Integer(PositionConstants.BOTTOM);
	public static final Integer LEFT = new Integer(PositionConstants.LEFT);
	public static final Integer RIGHT = new Integer(PositionConstants.RIGHT);
	public static final Integer TOP_LEFT = new Integer(PositionConstants.TOP
			| PositionConstants.LEFT);
	public static final Integer TOP_RIGHT = new Integer(PositionConstants.TOP
			| PositionConstants.RIGHT);
	public static final Integer BOTTOM_LEFT = new Integer(
			PositionConstants.BOTTOM | PositionConstants.LEFT);
	public static final Integer BOTTOM_RIGHT = new Integer(
			PositionConstants.BOTTOM | PositionConstants.RIGHT);

	public static final int ARC_WIDTH = 7;

	public static final int DEFAULT_PALETTE_SIZE = 150;
	public static final String PREF_PALETTE_SIZE = "palette size";
	
	/**
	 * Separator that is inserted into the displayed string of input workflow
	 * files between the source activity's name and the displayed string of the
	 * file in the source activity
	 */
	public static final String DISPLAYED_STRING_SEPERATOR = ": ";

	/**
	 * Key for the termination time preference for workflows. Used by the
	 * associated preference page.
	 * @deprecated not setting termination time by default, rely on server default
	 */
	@Deprecated
	public static final String P_WORKFLOW_TERMINATION_TIME = "workflow_termination_time";

	public static final String P_WORKFLOW_POLLING_INTERVAL = "workflow_polling_interval";

	public static final String P_KNOWN_MIME_TYPES = "mime_types";

	public static final String P_MIME_COLOR_PREFIX = "mimeTypeColor ";

	public static final String MIME_TYPE_SEPARATOR = ",";

	/**
	 * Name of the directory where job files for workflows are stored
	 */
	public static final String WORKFLOW_JOB_DIR = "jobs";

	/**
	 * File extension for DiagramView files that contain information about the
	 * user's view on the diagram (e.g. zoom level)
	 */
	public static final String FILE_EXTENSION_DIAGRAM_VIEW = "view";

	/**
	 * Key for storing the model version in xstream's UnmarshallingContext. Can
	 * be used to determine which unmarshalling procedure shall be used.
	 */
	public static final String XSTREAM_MODEL_VERSION = "xstream wfdiagram modelversion";

	public static final String[] ILLEGAL_CHARS_IN_ACTIVITY_NAMES = new String[] {
			"(", ")", "%", "!", "<", ">", "&", "$", "/", "\\", "?", "_" };

	public static final String[] ILLEGAL_CHARS_IN_VARIABLE_NAMES = new String[] {
			"(", ")", "%", "!", "<", ">", "&", "$", "/", "\\", "?" };

	/**
	 * In loops, execution state strings can be appended to iteration ids in
	 * order to show the user the overall state of each iteration at one glance.
	 * This regexp should be used to extract either the original iteration id or
	 * the execution state from such an "extended" iteration id.
	 */
	public static final String REGEXP_ITERATION_EXECUTION_STATE = "(.*) \\(([a-zA-Z]+)\\)$";
}
