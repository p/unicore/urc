/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.ui;

import java.util.List;

import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.common.guicomponents.StringComboBoxCellEditor;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowVariableUtils;

/**
 * CellEditor that allows selection of a single workflow variable
 * 
 * @author demuth
 * 
 */
public class WorkflowVariableSelectionCellEditor extends
		StringComboBoxCellEditor {

	IFlowElement element;
	String selected;
	private List<WorkflowVariable> available;

	public WorkflowVariableSelectionCellEditor(Composite parent,
			IFlowElement element) {
		super(parent);
		this.element = element;
	}

	/**
	 * returns an object of RangeValueTypeResourceRequirementValue
	 */
	@Override
	protected String doGetValue() {
		int selection = getComboBox().getSelectionIndex();
		if (available.size() > 0) {
			selected = available.get(selection).getName();
			return selected;
		} else {
			return null;
		}
	}

	@Override
	protected void doSetValue(Object value) {
		this.selected = (String) value;
		initVariableSelection();
	}

	protected void initVariableSelection() {

		available = WorkflowVariableUtils.collectWorkflowVariables(element);
		String[] names = WorkflowVariableUtils.extractVariableNames(available);
		int selection = 0;
		if (available.size() > 0) {
			int i = 0;
			for (WorkflowVariable variable : available) {
				if (variable.getName().equals(selected)) {
					selection = i;
				}
			}
			selected = names[selection];
		}
		setItems(names);
		getComboBox().select(selection);
	}

}
