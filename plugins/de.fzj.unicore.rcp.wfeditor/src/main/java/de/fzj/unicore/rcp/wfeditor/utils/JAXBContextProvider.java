/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.utils;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;

/**
 * @author demuth
 * 
 */
public class JAXBContextProvider {
	private static JAXBContext defaultContext;
	private static List<Package> packages;

	public static void addPackage(Package pack) {
		if (packages == null) {
			initContext();
		}
		if (!packages.contains(pack)) {
			packages.add(pack);
		}
		rebuildContext();
	}

	public static JAXBContext getContext(ClassLoader classLoader) {
		if (packages == null) {
			packages = initPackages();
		}
		// build JAXBContext (e.g. used for serializing jobs)
		try {
			String allPackages = "";
			int i = 1;
			for (Package pack : packages) {
				allPackages += pack.getName();
				if (i < packages.size()) {
					allPackages += ":";
				}
				i++;
			}

			return JAXBContext.newInstance(allPackages, classLoader);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Returns the default JAXBContext (e.g. used for serializing jobs)
	 */
	public static JAXBContext getDefaultContext() {
		if (defaultContext == null) {
			initContext();
		}
		return defaultContext;
	}

	private static void initContext() {
		if (packages == null) {
			packages = initPackages();
		}
		rebuildContext();
	}

	private static List<Package> initPackages() {
		List<Package> packages = new ArrayList<Package>();
		packages.add(WorkflowDiagram.class.getPackage());
		return packages;
	}

	private static void rebuildContext() {
		// build JAXBContext (e.g. used for serializing jobs)
		try {
			String allPackages = "";
			int i = 1;
			for (Package pack : packages) {
				allPackages += pack.getName();
				if (i < packages.size()) {
					allPackages += ":";
				}
				i++;
			}

			defaultContext = JAXBContext.newInstance(allPackages,
					JAXBContextProvider.class.getClassLoader());
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void removePackage(Package pack) {
		if (packages == null) {
			initContext();
		}
		if (packages.contains(pack)) {
			packages.remove(pack);
		}
		rebuildContext();
	}

	public static void setDefaultContext(JAXBContext defaultContext) {
		JAXBContextProvider.defaultContext = defaultContext;
	}

}
