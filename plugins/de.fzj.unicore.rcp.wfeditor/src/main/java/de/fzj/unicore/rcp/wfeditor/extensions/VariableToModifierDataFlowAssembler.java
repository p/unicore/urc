package de.fzj.unicore.rcp.wfeditor.extensions;

import java.util.HashSet;
import java.util.Set;

import de.fzj.unicore.rcp.wfeditor.extensionpoints.IDataFlowAssembler;
import de.fzj.unicore.rcp.wfeditor.model.commands.DataFlowAssemblyCommand;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.variables.VariableModifierDataSink;
import de.fzj.unicore.rcp.wfeditor.model.variables.VariableToModifierDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifier;

/**
 * Class that can be used to connect a {@link WorkflowVariable} with a
 * {@link WorkflowVariableModifier} via drag & drop. Used in conjunction with a
 * {@link VariableModifierDataSink}.
 * 
 * @author bdemuth
 * 
 */
public class VariableToModifierDataFlowAssembler implements IDataFlowAssembler {

	public DataFlowAssemblyCommand getAssemblyCommand(final IDataSource source,
			final IDataSink sink) throws Exception {

		if (source.getOutgoingDataTypes() != null
				&& sink.getIncomingDataTypes() != null
				&& !source.getOutgoingDataTypes().isEmpty()
				&& !sink.getIncomingDataTypes().isEmpty()) {
			Set<String> validTypes = new HashSet<String>(
					sink.getIncomingDataTypes());
			validTypes.retainAll(source.getOutgoingDataTypes());
			if (validTypes.isEmpty()) {
				return null;
			}
		}

		return new DataFlowAssemblyCommand() {
			IDataFlow oldLinker;

			@Override
			protected IDataFlow assembleDataFlow() {

				WorkflowVariable variable = (WorkflowVariable) source;

				if (sink instanceof VariableModifierDataSink) {
					VariableModifierDataSink variableSink = (VariableModifierDataSink) sink;

					oldLinker = getOldLinker(variableSink);
					if (oldLinker != null) {
						oldLinker.disconnect();
					}

					VariableToModifierDataFlow flow = new VariableToModifierDataFlow(
							variable, variableSink);
					flow.connect();
					return flow;
				}

				return null;

			}

			protected IDataFlow getOldLinker(IDataSink sink) {
				if (sink.getIncomingFlows() != null
						&& sink.getIncomingFlows().length > 0) {
					return sink.getIncomingFlows()[0];
				}
				return null;
			}

			@Override
			public void redo() {
				if (oldLinker != null) {
					oldLinker.disconnect();
				}
				super.redo();

			}

			@Override
			public void undo() {
				super.undo();
				if (oldLinker != null) {
					oldLinker.reconnect();
				}
			}

		};

	}

}
