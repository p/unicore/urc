package de.fzj.unicore.rcp.wfeditor.properties;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import de.fzj.unicore.rcp.common.guicomponents.AbstractListElement;
import de.fzj.unicore.rcp.common.guicomponents.GenericListEditor;
import de.fzj.unicore.rcp.common.guicomponents.IListListener;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.variables.IElementWithVariableModifiers;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifier;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifierList;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowVariableUtils;

public class VariableModifiersSection extends AbstractSection {

	private class WorkflowVariableModifierWrapper extends AbstractListElement {

		private WorkflowVariableModifier modifier;

		public WorkflowVariableModifierWrapper(WorkflowVariableModifier modifier) {
			this.modifier = modifier;
		}

		@Override
		public boolean canBeRemoved() {
			return getModifier().isDisposable();
		}

		public String getId() {
			return modifier.getId();
		}

		public WorkflowVariableModifier getModifier() {
			return modifier;
		}

		@Override
		public String getName() {
			return getModifier().getName();
		}

		@Override
		public String nameValid(String name) {
			return null;
		}

		@Override
		public void setName(String name) {
			super.setName(name);
			getModifier().setName(name);

		}
	}

	protected org.eclipse.swt.widgets.List modifiers;
	protected GenericListEditor<WorkflowVariableModifierWrapper> modifiersEditor;
	protected CCombo variableSelectionCombo;

	protected Text expressionText;

	protected boolean allowAdding = true;

	protected WorkflowVariableModifierList workflowVariableModifierList;
	protected ModifyListener modifyListener;
	protected SelectionListener selectionListener;

	protected IListListener<WorkflowVariableModifierWrapper> listListener;

	protected void addListeners() {
		modifiersEditor.addListListener(listListener);
		variableSelectionCombo.addSelectionListener(selectionListener);
		expressionText.addModifyListener(modifyListener);
	}

	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ITabbedPropertySection#createControls(org.eclipse.swt.widgets.Composite,
	 *      org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage)
	 */
	@Override
	public void createControls(Composite parent,
			TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);
		Composite flatForm = getWidgetFactory().createFlatFormComposite(parent);
		FormData data;
		Group composite = getWidgetFactory().createGroup(flatForm,
				"Variable Modifiers");
		data = new FormData();
		data.left = new FormAttachment(0, ITabbedPropertyConstants.HSPACE);
		data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(0, ITabbedPropertyConstants.VSPACE);
		data.bottom = new FormAttachment(100, -ITabbedPropertyConstants.VSPACE);
		composite.setLayoutData(data);
		composite.setLayout(new FormLayout());
		controls.add(composite);
		int vertOffset = 3 * ITabbedPropertyConstants.VSPACE;

		Composite listParent = getWidgetFactory().createComposite(composite);

		data = new FormData();
		data.left = new FormAttachment(0, 3);
		data.right = new FormAttachment(50, ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(0, vertOffset);
		listParent.setLayoutData(data);

		modifiersEditor = new GenericListEditor<WorkflowVariableModifierWrapper>(
				listParent, new ArrayList<WorkflowVariableModifierWrapper>(),
				new boolean[] { true, true, false, false, true }) {
			@Override
			protected WorkflowVariableModifierWrapper createNewListElement() {
				WorkflowVariableModifier modifier = new WorkflowVariableModifier(
						element);
				modifier.activate();
				modifier.setDisposable(true);
				return new WorkflowVariableModifierWrapper(modifier);
			}
		};
		controls.add(modifiersEditor.getControl());

		Label modifiedVariableLabel = getWidgetFactory().createLabel(composite,
				"Modified variable:");
		data = new FormData();
		data.left = new FormAttachment(listParent,
				2 * ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(0, vertOffset);
		modifiedVariableLabel.setLayoutData(data);
		controls.add(modifiedVariableLabel);

		variableSelectionCombo = getWidgetFactory().createCCombo(composite);
		variableSelectionCombo.setBackground(null);

		data = new FormData();
		data.left = new FormAttachment(listParent,
				2 * ITabbedPropertyConstants.HSPACE);
		data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(modifiedVariableLabel,
				ITabbedPropertyConstants.VSPACE);
		variableSelectionCombo.setLayoutData(data);
		controls.add(variableSelectionCombo);

		Label initialValueLabel = getWidgetFactory().createLabel(composite,
				"Expression:");
		data = new FormData();
		data.left = new FormAttachment(listParent,
				2 * ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(variableSelectionCombo,
				ITabbedPropertyConstants.VSPACE);
		initialValueLabel.setLayoutData(data);
		controls.add(initialValueLabel);

		expressionText = getWidgetFactory().createText(composite, "",
				SWT.BORDER | SWT.LEFT);
		expressionText.setSize(100, expressionText.getSize().y);
		data = new FormData();
		data.left = new FormAttachment(listParent,
				2 * ITabbedPropertyConstants.HSPACE);
		data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(initialValueLabel,
				ITabbedPropertyConstants.VSPACE);
		expressionText.setLayoutData(data);
		expressionText
				.setToolTipText("Expression for assigning a new value to the selected variable.\nThe syntax to be used is based on Java.\n"
						+ "Examples:\n"
						+ "Variable1 ++;\n"
						+ "Variable1 = Variable1+5;");
		controls.add(expressionText);
		createListeners();
	}

	protected void createListeners() {
		modifyListener = new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				String value = expressionText.getText();
				WorkflowVariableModifier modifier = getSelectedModifier();
				if (modifier != null
						&& !value.equals(modifier.getInternalExpression())) {
					modifier.setInternalExpression(value);
					modifier.setDisplayedExpression(value);
					updateElement();
				}
			}
		};

		selectionListener = new SelectionListener() {

			private void updateTargetVariable() {
				String selected = variableSelectionCombo
						.getItem(variableSelectionCombo.getSelectionIndex());
				if (WorkflowVariableUtils.NO_VARIABLE_SELECTED.equals(selected)) {
					selected = null;
				}
				WorkflowVariableModifier modifier = getSelectedModifier();
				if (modifier != null) {
					WorkflowVariableUtils.linkModifierToVariable(modifier,
							selected);
					updateElement();
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				updateTargetVariable();
			}

			public void widgetSelected(SelectionEvent e) {
				updateTargetVariable();
			}
		};

		listListener = new IListListener<WorkflowVariableModifierWrapper>() {

			public void elementAdded(WorkflowVariableModifierWrapper newElement) {
				workflowVariableModifierList.addModifier(newElement
						.getModifier());

			}

			public void elementChanged(
					WorkflowVariableModifierWrapper changedElement) {

			}

			public void elementRemoved(
					WorkflowVariableModifierWrapper removedElement) {
				workflowVariableModifierList.removeModifier(removedElement
						.getModifier());
				removedElement.getModifier().dispose();

			}

			public void selectionChanged(
					WorkflowVariableModifierWrapper newSelection) {
				WorkflowVariableModifier newVar = null;
				if (newSelection != null) {
					newVar = newSelection.getModifier();
				}
				modifierSelectionChanged(newVar);
			}
		};
	}

	@Override
	public void dispose() {
		removeListeners();
	}

	public WorkflowVariableModifier getSelectedModifier() {
		WorkflowVariableModifierWrapper wrapper = modifiersEditor
				.getSelectedElement();
		if (wrapper != null) {
			return wrapper.getModifier();

		}
		return null;
	}

	protected void initModifiers() {
		if (element == null || modifiersEditor == null) {
			return;
		}
		removeListeners();
		modifiersEditor.setAddingAllowed(allowAdding);
		workflowVariableModifierList = element.getVariableModifierList();
		List<WorkflowVariableModifier> modifiers = workflowVariableModifierList
				.getModifiers();
		List<WorkflowVariableModifierWrapper> wrappers = new ArrayList<WorkflowVariableModifierWrapper>();
		for (WorkflowVariableModifier modifier : modifiers) {
			wrappers.add(new WorkflowVariableModifierWrapper(modifier));
		}
		modifiersEditor.setElements(wrappers);

		updateAvailableVarNames();
		modifierSelectionChanged(getSelectedModifier());

		addListeners();

	}

	public void modifierSelectionChanged(WorkflowVariableModifier selected) {
		if (getSelectedModifier() != null) {
			getSelectedModifier().removePropertyChangeListener(this);
		}
		if (selected == null) {
			variableSelectionCombo.select(0);
			expressionText.setText("");
		} else {
			selected.addPropertyChangeListener(this);
			int index = 0;
			String[] varNames = variableSelectionCombo.getItems();
			for (String string : varNames) {
				if (string.equals(getSelectedModifier().getVariableName())) {
					break;
				}
				index++;
			}
			if (index >= varNames.length) {
				index = 0;
			}
			variableSelectionCombo.select(index);
			expressionText.setText(selected.getDisplayedExpression());
		}
		variableSelectionCombo.setEnabled(selected != null && canEditModel());
		expressionText.setEnabled(selected != null && canEditModel());
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (IFlowElement.PROP_DECLARED_VARIABLES.equals(evt.getPropertyName())) {
			initModifiers();
		} else if (WorkflowVariableModifier.PROP_DISPLAYED_EXPRESSION
				.equals(evt.getPropertyName())) {
			String newValue = (String) evt.getNewValue();
			if (!expressionText.getText().equals(newValue)) {
				expressionText.removeModifyListener(modifyListener);
				expressionText.setText(newValue);
				expressionText.addModifyListener(modifyListener);
			}
		} else if (WorkflowVariableModifier.PROP_VARIABLE_NAME.equals(evt
				.getPropertyName())) {
			String newValue = (String) evt.getNewValue();
			updateAvailableVarNames();
			String[] varNames = variableSelectionCombo.getItems();
			int index = 0;
			for (String string : varNames) {
				if (string.equals(newValue)) {
					break;
				}
				index++;
			}
			if (index >= varNames.length) {
				index = 0;
			}
			variableSelectionCombo.select(index);
		}

	}

	protected void removeListeners() {
		if (getSelectedModifier() != null) {
			getSelectedModifier().removePropertyChangeListener(this);
		}
		if (modifiersEditor != null) {
			modifiersEditor.removeListListener(listListener);
		}
		if (variableSelectionCombo != null
				&& !variableSelectionCombo.isDisposed()) {
			variableSelectionCombo.removeSelectionListener(selectionListener);
		}
		if (expressionText != null && !expressionText.isDisposed()) {
			expressionText.removeModifyListener(modifyListener);
		}
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
		IElementWithVariableModifiers e = (IElementWithVariableModifiers) element;
		allowAdding = e.allowsAddingModifiers();
		initModifiers();
		updateEnabled();
	}

	protected void updateAvailableVarNames() {
		Collection<WorkflowVariable> vars = element.getDiagram()
				.getVariables().values();
		String[] varNames = WorkflowVariableUtils.extractVariableNames(vars);
		variableSelectionCombo.setItems(varNames);
	}

	public void updateElement() {
		element.removePropertyChangeListener(this);
		element.setVariableModifierList(workflowVariableModifierList);
		element.addPropertyChangeListener(this);
		element.getDiagram().setDirty(true);
	}
}
