package de.fzj.unicore.rcp.wfeditor.model.data;

import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import de.fzj.unicore.rcp.common.interfaces.ISerializable;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.IPropertySourceWithListeners;

/**
 * A sink for data that can be passed from a data source inside the workflow
 * e.g. file or workflow variable
 * 
 * @author bdemuth
 * 
 */
public interface IDataSink extends IPropertySourceWithListeners, ICopyable,
		ISerializable, Comparable<IDataSink> {

	public static final String PROP_NAME = "name";

	public static final String PROP_ACCEPTED_DATA_TYPES = "accepted data types property";

	public static final String PROP_DATA_FLOWS = "data flows";

	/**
	 * Key for Boolean property stating whether this sink has to be filled by
	 * some data flow(s). This can be used for marking the sink when it still
	 * needs to be filled.
	 */
	public static final String PROP_MUST_BE_FILLED = "must be filled";

	/**
	 * Key for Boolean property stating whether this sink is filled by some data
	 * flow(s).
	 */
	public static final String PROP_IS_FILLED = "filled";

	public void addIncomingFlow(IDataFlow flow);

	/**
	 * Add a data flow to the list of flows at the given index. Negative indexes
	 * can be used to append at the end of the list: -1 adds as last element, -2
	 * adds as last but one element, etc.
	 * 
	 * @param flow
	 * @param index
	 */
	public void addIncomingFlow(IDataFlow flow, int index);

	public IDataSink getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags);

	/**
	 * The parent activity that provides the data source.
	 * 
	 * @return
	 */
	public IFlowElement getFlowElement();

	public String getId();

	/**
	 * A set of identifiers for types the data may have. This may be used to
	 * refer to MIME types or basic data types (integer, double, string...).
	 * Such identifiers can be used to make a match between data sources and
	 * data sinks (if the intersection of the type sets is not empty, the data
	 * may be exchanged).
	 * 
	 * @return
	 */
	public Set<String> getIncomingDataTypes();

	/**
	 * Returns the flow with the given id if there is an incoming flow with this
	 * id, else returns null
	 * 
	 * @param id
	 * @return
	 */
	public IDataFlow getIncomingFlowById(String id);

	public IDataFlow[] getIncomingFlows();

	public String getName();

	public QName getSinkType();

	/**
	 * Returns true iff the given flow is referenced by this data sink, i.e. the
	 * flow ends at this sink.
	 * 
	 * @param flow
	 * @return
	 */
	public boolean hasIncomingFlow(IDataFlow flow);

	public boolean isVisible();

	/**
	 * Remove the given flow from the list of flows.
	 * 
	 * @param flow
	 * @return the index of the removed element or -1 if the element was not
	 *         found
	 */
	public int removeIncomingFlow(IDataFlow flow);

	public void setFlowElement(IFlowElement element);

	/**
	 * The {@link DataSinkList} that holds and manages this data sink.
	 * 
	 * @param parentList
	 */
	public void setParentSinkList(DataSinkList parentList);

}
