package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPartViewer;

import de.fzj.unicore.rcp.wfeditor.ui.WFTooltipHelper;

/**
 * This interface can be used in conjunction with {@link WFTooltipHelper} in
 * order to create complex adaptable tooltips that position themselves.
 * 
 * @author bdemuth
 * 
 */
public interface IComplexToolTip extends IFigure {

	/**
	 * Complex tooltips compute their position themselves. This method is called
	 * shortly before the tooltip is shown. Apart from computing the tooltip's
	 * position and size, this method can also take the mouse position into
	 * account in order to change the tooltip's appearance.
	 * 
	 * @param eventX
	 * @param eventY
	 * @return
	 */
	public boolean createTooltip(EditPartViewer viewer, IFigure tooltipLayer,
			IFigure hoverSource, int eventX, int eventY);

	public void hide(IFigure tooltipLayer, IFigure hoverSource);

	/**
	 * This method is used to infrom the tooltip that the mouse position has
	 * changed.
	 * 
	 * @param tooltipLayer
	 * @param eventX
	 * @param eventY
	 * @return
	 */
	public boolean refreshTooltip(IFigure tooltipLayer, IFigure hoverSource,
			int eventX, int eventY);

}
