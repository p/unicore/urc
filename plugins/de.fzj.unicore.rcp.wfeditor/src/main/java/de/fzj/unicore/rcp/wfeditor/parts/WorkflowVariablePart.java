package de.fzj.unicore.rcp.wfeditor.parts;

import org.eclipse.draw2d.IFigure;

import de.fzj.unicore.rcp.wfeditor.figures.DataSourceFigure;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;

public class WorkflowVariablePart extends DataSourcePart {

	@Override
	protected IFigure createFigure() {
		DataSourceFigure result = (DataSourceFigure) super.createFigure();
		return result;
	}

	@Override
	public WorkflowVariable getModel() {
		return (WorkflowVariable) super.getModel();
	}

	@Override
	protected void refreshVisuals() {

		super.refreshVisuals();
		if (!(getFigure() instanceof DataSourceFigure)) {
			return;
		}
		DataSourceFigure figure = (DataSourceFigure) getFigure();

		String label = WorkflowVariable.getSymbolFromTypeName(getModel()
				.getType());
		figure.setToolTipText(new String[] {
				"Variable: " + getModel().getName(),
				"Type: " + getModel().getType() });
		figure.setDataTypeSymbol(label);
	}

}
