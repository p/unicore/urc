package de.fzj.unicore.rcp.wfeditor.model.structures;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifier;

@XStreamAlias("LoopActivity")
public abstract class LoopActivity extends CollapsibleActivity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3099840536151610002L;

	public LoopActivity() {
		setCanAddChildren(false);
	}

	public abstract StructuredActivity getBody();

	public abstract WorkflowVariable getIterationCounter();

	public abstract WorkflowVariableModifier getIterationIncrementer();

	@Override
	public boolean isLoop() {
		return true;
	}
}
