/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.activities;

import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.variables.IElementWithVariableModifiers;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifier;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifierList;

/**
 * @author demuth
 * 
 */
@XStreamAlias("VariableModifierActivity")
public class VariableModifierActivity extends Activity implements
		IElementWithVariableModifiers {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4428386379724564106L;
	public static QName TYPE = new QName("http://www.unicore.eu/",
			"ModifyVariableActivity");

	public VariableModifierActivity() {
		WorkflowVariableModifier m = new WorkflowVariableModifier(this);
		getVariableModifierList().addModifier(m);
		getDataSinkList().addDataSink(m.getDataSink());
		getDataSourceList().addDataSource(m.getDataSource());
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();

		// make sure data sink and source are created even when dealing with old
		// modifiers
		if (getDataSinkList().getDataSinks().length == 0) {
			WorkflowVariableModifierList list = getVariableModifierList();
			if (list.getModifiers().size() > 0) {
				WorkflowVariableModifier m = list.getModifiers().get(0);
				getDataSinkList().addDataSink(m.getDataSink());
				getDataSourceList().addDataSource(m.getDataSource());
			}
		}

	}

	public boolean allowsAddingModifiers() {
		return false;
	}

	@Override
	public Activity getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		return super.getCopy(toBeCopied, mapOfCopies, copyFlags);
	}

	public WorkflowVariableModifier getModifier() {
		return getVariableModifierList().getModifiers().get(0);
	}

	@Override
	public QName getType() {
		return TYPE;
	}

	@Override
	public void setDiagram(WorkflowDiagram diagram) {
		super.setDiagram(diagram);
		setName(getDiagram().getUniqueActivityName("Modifier"));
	}

}
