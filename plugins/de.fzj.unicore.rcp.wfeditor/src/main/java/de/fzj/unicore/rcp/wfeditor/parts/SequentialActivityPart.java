/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import de.fzj.unicore.rcp.wfeditor.figures.StructuredActivityFigure;

/**
 * @author hudsonr Created on Jul 18, 2003
 */
public class SequentialActivityPart extends StructuredActivityPart {

	/**
	 * @see de.fzj.unicore.rcp.wfeditor.parts.ActivityPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		// installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new
		// ActivityNodeEditPolicy());
		// installEditPolicy(EditPolicy.COMPONENT_ROLE, new
		// ActivityEditPolicy());
		// installEditPolicy(
		// EditPolicy.SELECTION_FEEDBACK_ROLE,
		// new ActivityContainerHighlightEditPolicy());
		// ResizeContainerPolicy resize = new ResizeContainerPolicy();
		// installEditPolicy(WFConstants.POLICY_AUTO_RESIZE_CONTAINER, resize);
		// installEditPolicy(EditPolicy.CONTAINER_ROLE, new
		// ActivityContainerEditPolicy());
		// installEditPolicy(EditPolicy.LAYOUT_ROLE, new
		// FixedOrderedLayoutEditPolicy());
		// installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new
		// StructuredActivityDirectEditPolicy());
		// installEditPolicy("resize",new WFResizableEditPolicy());
	}

	/**
	 * @see de.fzj.unicore.rcp.wfeditor.parts.StructuredActivityPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		Point loc = getModel().getLocation();
		Dimension size = getModel().getSize();
		Rectangle r = new Rectangle(loc, size);
		IFigure fig = new StructuredActivityFigure(r);
		fig.setBounds(r);
		return fig;
	}

	@Override
	public IFigure getFigure() {
		return super.getFigure();
		// if (figure == null)
		// {
		// WFOrderedLayout layout = new WFOrderedLayout(false);
		// layout.setMajorSpacing(WFConstants.BETWEEN_ACTIVITY_PADDING.bottom);
		// layout.setStretchMinorAxis(false);
		// layout.setMajorAlignment(FlowLayout.ALIGN_CENTER);
		// layout.setMinorAlignment(FlowLayout.ALIGN_CENTER);

		// setFigure(createFigure());
		// if (figure instanceof StructuredActivityFigure)
		// ((StructuredActivityFigure)figure).getContents().setLayoutManager(layout);
		// else figure.setLayoutManager(layout);
		// }
		// return figure;
	}

	/**
	 * @see ActivityPart#contributeEdgesToGraph(org.eclipse.graph.CompoundDirectedGraph,
	 *      java.util.Map)
	 */
	// public void contributeEdgesToGraph(CompoundDirectedGraph graph, Map map)
	// {
	// super.contributeEdgesToGraph(graph, map);
	// Node node, prev = null;
	// EditPart a;
	// List members = getChildren();
	// for (int n = 0; n < members.size(); n++) {
	// a = (EditPart)members.get(n);
	// node = (Node)map.get(a);
	// if (prev != null) {
	// Edge e = new Edge(prev, node);
	// e.weight = 50;
	// graph.edges.add(e);
	// }
	// prev = node;
	// }
	// }

}
