package de.fzj.unicore.rcp.wfeditor.properties;

import org.eclipse.ui.views.properties.tabbed.AbstractTypeMapper;

import de.fzj.unicore.rcp.wfeditor.parts.ActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.TransitionPart;


public class FlowElementTypeMapper extends AbstractTypeMapper {
	
	@Override
	@SuppressWarnings("rawtypes")
	public Class mapType(Object object) {
		if (object instanceof ActivityPart) {
			return ((ActivityPart) object).getModel().getClass();
		} else if (object instanceof TransitionPart) {
			return ((TransitionPart) object).getTransition().getClass();
		} else {
			return super.mapType(object);
		}
	}
}
