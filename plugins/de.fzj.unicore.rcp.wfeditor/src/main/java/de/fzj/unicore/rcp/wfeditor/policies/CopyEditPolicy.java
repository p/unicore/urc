/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.gef.requests.GroupRequest;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.commands.CopyCommand;

public class CopyEditPolicy extends AbstractEditPolicy {

	@Override
	public Command getCommand(Request request) {
		if (WFConstants.REQ_COPY.equals(request.getType())) {
			GroupRequest group = (GroupRequest) request;
			List<?> toCopy = group.getEditParts();
			List<IFlowElement> elements = new ArrayList<IFlowElement>();
			for (int i = 0; i < toCopy.size(); i++) {
				EditPart part = (EditPart) toCopy.get(i);
				if (part.getModel() instanceof IFlowElement) {
					elements.add((IFlowElement) part.getModel());
				}

			}
			return new CopyCommand(elements.toArray(new IFlowElement[elements
					.size()]));
		}
		return null;
	}

}
