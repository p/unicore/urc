/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.IPropertySourceWithListeners;

/**
 * @author demuth
 */
public class SetPropertyCommand extends Command {

	private IPropertySourceWithListeners element;
	private Object propertyKey;
	private Object oldValue;
	private Object newValue;

	public SetPropertyCommand(IPropertySourceWithListeners element,
			Object propertyKey, Object propertyValue) {
		super();
		this.element = element;
		this.propertyKey = propertyKey;
		this.newValue = propertyValue;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldValue = element.getPropertyValue(propertyKey);
		redo();
	}

	@Override
	public void redo() {
		element.setPropertyValue(propertyKey, newValue);
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		element.setPropertyValue(propertyKey, oldValue);
	}

}
