/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.PropertyDescriptor;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.StringUtils;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSinkList;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSourceList;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionState;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateDescriptor;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableList;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifierList;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowUtils;

@XStreamAlias("FlowElement")
public abstract class FlowElement extends PropertySource implements
IFlowElement {

	static final long serialVersionUID = 1;

	public static QName TYPE = new QName("http://www.unicore.eu/",
	"FlowElement");

	private String ID = UUID.randomUUID().toString();

	private String name = "";

	/**
	 * Reference to the top level model object.
	 */
	protected WorkflowDiagram diagram;

	private boolean deletable = true;

	private StructuredActivity parent;

	private boolean boundToParent = false;

	protected transient boolean iterationManuallyChanged = false;

	private transient Map<Object, Object> unserializedPropertyMap;

	/**
	 * the command stack of the editor used for executing commands from within
	 * the model.
	 */
	protected transient CommandStack commandStack;

	public FlowElement() {
		addPropertyDescriptors();
		setExecutionStateDescriptor(new ExecutionStateDescriptor());
		setCurrentIterationId(ExecutionStateConstants.DEFAULT_ITERATION_ID);
		setPropertyValue(PROP_DECLARED_VARIABLES, new WorkflowVariableList());
		setPropertyValue(PROP_VARIABLE_MODIFIERS,
				new WorkflowVariableModifierList());
		setPropertyValue(PROP_SHOW_DATA_FLOWS, new ShowDataFlowsProperty());
	}

	@Override
	public void activate() {
		super.activate();

		if (getVariableList() != null) {
			getVariableList().activate();
		}
		if (getVariableModifierList() != null) {
			getVariableModifierList().activate();
		}
		getDataSinkList().activate();
		getDataSourceList().activate();

		if (parent != null) {
			// inherit the property from our parent that tells us whether to
			// show data flows instead of control flows
			ShowDataFlowsProperty prop = (ShowDataFlowsProperty) parent
			.getPropertyValue(PROP_SHOW_DATA_FLOWS);
			if (prop != null) {
				setPropertyValue(PROP_SHOW_DATA_FLOWS, prop.clone());
			}
		}
	}

	@Override
	protected void addPropertyDescriptors() {
		IPropertyDescriptor descr = new PropertyDescriptor(
				PROP_DECLARED_VARIABLES, PROP_DECLARED_VARIABLES);
		addPropertyDescriptor(descr);
		descr = new PropertyDescriptor(PROP_VARIABLE_MODIFIERS,
				PROP_VARIABLE_MODIFIERS);
		addPropertyDescriptor(descr);
		descr = new TextPropertyDescriptor(PROP_NAME, "Name");
		addPropertyDescriptor(descr);
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
		getVariableList().afterDeserialization();
		getVariableModifierList().afterDeserialization();
	}

	@Override
	public void afterSerialization() {
		super.afterSerialization();
		getVariableList().afterSerialization();
		getVariableModifierList().afterSerialization();
		setPropertyValue(PROP_CUT, getUnserializedPropertyMap()
				.remove(PROP_CUT));
		setPropertyValue(PROP_SHOW_DATA_FLOWS, getUnserializedPropertyMap()
				.remove(PROP_SHOW_DATA_FLOWS));

	}

	public void afterSubmission() {
		// do nothing by default
	}

	@Override
	public void beforeSerialization() {
		super.beforeSerialization();
		getVariableList().beforeSerialization();
		getVariableModifierList().beforeSerialization();
		// don't to serialize this
		getUnserializedPropertyMap().put(PROP_CUT, removeProperty(PROP_CUT));

		ShowDataFlowsProperty prop = (ShowDataFlowsProperty) getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		if (prop != null) {
			getUnserializedPropertyMap()
			.put(PROP_SHOW_DATA_FLOWS, prop.clone());
			prop.setShowingFlows(false);
		}

	}

	public void beforeSubmission() {
		// stop showing data flows
		setPropertyValue(PROP_SHOW_DATA_FLOWS, new ShowDataFlowsProperty()); 
	}

	protected String canSetNameTo(String name)
	{
		if(getDiagram() == null)
		{
			return "This element is not part of a workflow.";
		}
		if(!getDiagram().isUniqueElementName(name)) return "Another workflow part with the requested name already exists.";
		return WorkflowUtils.isAllowedAsActivityName(name);
	}

	public void changeCurrentExecutionState(int cipher, String description) {
		ExecutionStateDescriptor state = getExecutionStateDescriptor().clone();
		ExecutionState currentState = state.getState(getCurrentIterationId());
		currentState.setCipher(cipher);
		currentState.setDetailedDescription(description);
		setExecutionStateDescriptor(state);

	}

	@Override
	public FlowElement clone() throws CloneNotSupportedException

	{
		FlowElement result = (FlowElement) super.clone();
		result.ID = UUID.randomUUID().toString();
		return result;
	}

	@Override
	public void dispose() {
		super.dispose();
		if (getVariableList() != null) {
			getVariableList().dispose();
		}
		if (getVariableModifierList() != null) {
			getVariableModifierList().dispose();
		}
		if (!getDataSinkList().isDisposed()) {
			getDataSinkList().dispose();
		}
		if (!getDataSourceList().isDisposed()) {
			getDataSourceList().dispose();
		}
		if (getDiagram() != null) {
			getDiagram().addDisposedElement(this);
		}
	}

	protected void fireStructureChange(Object prop, Object child) {
		firePropertyChange(prop.toString(), null, child);
	}

	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class adapter) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.IActivity#getCommandStack()
	 */
	public CommandStack getCommandStack() {
		if (commandStack == null && getDiagram() != null) {
			commandStack = getDiagram().getCommandStack();
		}
		return commandStack;
	}

	@Override
	public FlowElement getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		if (mapOfCopies.get(this) != null) {
			return (FlowElement) mapOfCopies.get(this); // never copy the same
			// element twice
		}

		FlowElement copy = (FlowElement) super.getCopy(toBeCopied, mapOfCopies,
				copyFlags);

		if ((copyFlags & ICopyable.FLAG_CUT) != 0) {
			// copy may keep our name
			// do NOT use setName or setPropertyValue here, as this will
			// result in a failed validity check
			copy.name = getName();
			// copy may keep our id
			copy.ID = ID;
		} else {
			// need a new name
			String myRawName = StringUtils.cutOffDigits(getName());
			copy.setName(getDiagram().getUniqueElementName(myRawName));
		}

		copy.setDiagram(getDiagram());
		copy.parent = null;

		return copy;

	}

	public ExecutionState getCurrentExecutionState() {
		if (getExecutionStateDescriptor() == null) {
			return null;
		}
		return getExecutionStateDescriptor().getState(getCurrentIterationId());
	}

	public int getCurrentExecutionStateCipher() {
		if (getCurrentExecutionState() == null) {
			return ExecutionStateConstants.STATE_UNSUBMITTED;
		}
		return getCurrentExecutionState().getCipher();
	}

	public String getCurrentExecutionStateDescription() {
		if (getCurrentExecutionState() == null) {
			return "unsubmitted";
		}
		return getCurrentExecutionState().getDetailedDescription();
	}

	public String getCurrentExecutionStateName() {
		return ExecutionStateConstants
		.getStringForState(getCurrentExecutionStateCipher());

	}

	public String getCurrentIterationId() {
		String result = (String) getPropertyValue(PROP_CURRENT_ITERATION_ID);
		WorkflowDiagram diagram = getDiagram();
		if (diagram != null) {
			ExecutionData executionData = diagram.getExecutionData();
			if (executionData != null) {
				String iterationId = executionData.getIterationId(getID());
				if (iterationId != null) {
					result = iterationId;
				}
			}
		}

		if (result == null && getParent() != null) {
			result = getParent().getCurrentIterationId();
		}
		return result;
	}

	public DataSinkList getDataSinkList() {
		DataSinkList list = (DataSinkList) getPropertyValue(PROP_DATA_SINK_LIST);
		if (list == null) {
			list = new DataSinkList();
			setPropertyValue(PROP_DATA_SINK_LIST, list);
		}
		return list;
	}

	public DataSourceList getDataSourceList() {
		DataSourceList list = (DataSourceList) getPropertyValue(PROP_DATA_SOURCE_LIST);
		if (list == null) {
			list = new DataSourceList();
			setPropertyValue(PROP_DATA_SOURCE_LIST, list);
		}
		return list;
	}

	public WorkflowDiagram getDiagram() {
		return diagram;
	}

	public ExecutionStateDescriptor getExecutionStateDescriptor() {
		WorkflowDiagram diagram = getDiagram();
		if (diagram != null) {
			ExecutionData executionData = diagram.getExecutionData();
			if (executionData != null) {
				ExecutionStateDescriptor state = executionData
				.getStateDescriptor(getID());
				if (state != null) {
					return state;
				}
			}
		}

		return (ExecutionStateDescriptor) getPropertyValue(PROP_EXECUTION_STATE);
	}

	public String getID() {
		return ID;
	}

	public String getName() {
		return name;
	}

	public StructuredActivity getParent() {
		return parent;
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_SHOW_DATA_FLOWS);
		propertiesToCopy.add(PROP_CURRENT_ITERATION_ID);
		propertiesToCopy.add(PROP_ITERATOR_NAME);
		propertiesToCopy.add(PROP_DATA_SINK_LIST);
		propertiesToCopy.add(PROP_DATA_SOURCE_LIST);
		propertiesToCopy.add(PROP_DECLARED_VARIABLES);
		propertiesToCopy.add(PROP_VARIABLE_MODIFIERS);
		propertiesToCopy.add(PROP_EXECUTION_STATE);
		return propertiesToCopy;
	}

	@Override
	public Object getPropertyValue(String propName) {
		if (PROP_NAME.equals(propName)) {
			return getName();
		} else {
			return super.getPropertyValue(propName);
		}
	}

	public QName getType() {
		return TYPE;
	}

	public String getUnresolvedIterationId() {
		String result = "";
		if (getParent() != null) {
			result = getParent().getUnresolvedIterationId();
		}
		String iterator = (String) getPropertyValue(PROP_ITERATOR_NAME);
		if (iterator != null) {
			result += Constants.ITERATION_ID_SEPERATOR + "${" + iterator + "}";
		}
		return result;
	}

	/**
	 * This map can be used to store properties that should not be serialized.
	 * These properties should be moved to this map in
	 * {@link FlowElement#beforeSerialization()} and moved back to the
	 * properties map in {@link #afterSerialization()}.
	 * 
	 * @return
	 */
	protected Map<Object, Object> getUnserializedPropertyMap() {
		if (unserializedPropertyMap == null) {
			unserializedPropertyMap = new HashMap<Object, Object>();
		}
		return unserializedPropertyMap;
	}

	public WorkflowVariableList getVariableList() {
		return (WorkflowVariableList) getPropertyValue(PROP_DECLARED_VARIABLES);
	}

	public WorkflowVariableModifierList getVariableModifierList() {
		return (WorkflowVariableModifierList) getPropertyValue(PROP_VARIABLE_MODIFIERS);
	}

	public void init() {
		// do nothing by default
		// this can be overridden by subclasses in order to initialize the
		// element's structure
	}

	public boolean isBoundToParent() {
		return boundToParent;
	}

	public boolean isCut() {
		Boolean result = (Boolean) getPropertyValue(PROP_CUT);
		if (result == null) {
			return false;
		} else {
			return result;
		}
	}

	public boolean isDeletable() {
		return deletable;
	}

	public boolean isIterationManuallyChanged() {
		return iterationManuallyChanged;
	}

	private void readObject(ObjectInputStream in) throws IOException,
	ClassNotFoundException {
		in.defaultReadObject();
	}

	public boolean readyForSubmission() {
		return true;
	}

	public void setBoundToParent(boolean boundToParent) {
		this.boundToParent = boundToParent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.model.IActivity#setCommandStack(org.eclipse
	 * .gef.commands.CommandStack)
	 */
	public void setCommandStack(CommandStack commandStack) {
		this.commandStack = commandStack;
	}

	public void setCurrentIterationId(String id) {
		setCurrentIterationId(id, false);
	}

	public void setCurrentIterationId(final String id, boolean manual) {
		if (manual) {
			setIterationManuallyChanged(true);
		}
		if (manual || !isIterationManuallyChanged()) {

			WorkflowDiagram diagram = getDiagram();
			if (diagram != null) {
				ExecutionData executionData = diagram.getExecutionData();
				if (executionData != null) {
					executionData.setIterationId(getID(), id);
				}
			}
			setPropertyValue(PROP_CURRENT_ITERATION_ID, id);
		}

	}

	public void setCut(boolean cut) {
		setPropertyValue(PROP_CUT, cut);
	}

	public void setDataSinks(DataSinkList list) {
		setPropertyValue(PROP_DATA_SINK_LIST, list);
	}

	public void setDataSources(DataSourceList list) {
		setPropertyValue(PROP_DATA_SOURCE_LIST, list);
	}

	public void setDeletable(boolean deletable) {
		this.deletable = deletable;
	}

	public void setDiagram(WorkflowDiagram diagram) {
		// do not produce any side effects here!
		this.diagram = diagram;
	}

	public void setExecutionStateDescriptor(final ExecutionStateDescriptor state) {
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
			public void run() {

				WorkflowDiagram diagram = getDiagram();
				if (diagram != null) {
					ExecutionData executionData = diagram.getExecutionData();
					if (executionData != null) {
						executionData.setStateDescriptor(getID(), state);
					}
				}
				setPropertyValue(PROP_EXECUTION_STATE, state);
			}
		});
	}

	public void setIterationManuallyChanged(boolean iterationManuallyChanged) {
		this.iterationManuallyChanged = iterationManuallyChanged;
	}

	public boolean setName(String s) {
		if (getName() != null && getName().equals(s)) {
			return true;
		}
		String check = canSetNameTo(s);
		if (check == null) {
			Object old = name;
			name = s;
			firePropertyChange(PROP_NAME, old, s);
			return true;
		} else {

			WFActivator.log(IStatus.ERROR,
					"Unable to change element name: " + check);
			return false;

		}
	}

	public void setParent(StructuredActivity parent) {
		StructuredActivity oldParent = this.parent;
		this.parent = parent;
		firePropertyChange(PARENT, oldParent, parent);

	}

	@Override
	public void setPropertyValue(Object key, Object value) {
		if (PROP_NAME.equals(key)) {
			setName((String) value);
		} else {
			super.setPropertyValue(key, value);
		}

	}

	public void setVariableList(WorkflowVariableList list) {
		setPropertyValue(PROP_DECLARED_VARIABLES, list);
	}

	public void setVariableModifierList(WorkflowVariableModifierList list) {
		setPropertyValue(PROP_VARIABLE_MODIFIERS, list);
	}

	@Override
	public void undoDispose() {
		super.undoDispose();
		if (getVariableList() != null) {
			getVariableList().undoDispose();
		}
		if (getVariableModifierList() != null) {
			getVariableModifierList().undoDispose();
		}
		if (getDataSinkList().isDisposed()) {
			getDataSinkList().undoDispose();
		}
		if (getDataSourceList().isDisposed()) {
			getDataSourceList().undoDispose();
		}
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		if (updatedToCurrentModelVersion) {
			return; // don't do this twice!
		}
		super.updateToCurrentModelVersion(oldVersion, currentVersion);

		getDataSinkList().updateToCurrentModelVersion(oldVersion,
				currentVersion);
		getDataSourceList().updateToCurrentModelVersion(oldVersion,
				currentVersion);
		getVariableList().updateToCurrentModelVersion(oldVersion,
				currentVersion);
		getVariableModifierList().updateToCurrentModelVersion(oldVersion,
				currentVersion);
	}

}
