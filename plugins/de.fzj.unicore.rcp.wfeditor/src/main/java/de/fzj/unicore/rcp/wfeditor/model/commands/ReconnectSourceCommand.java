/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.utils.TransitionUtils;

/**
 * Command that handles the reconnection of source Activities.
 * 
 * @author Daniel Lee
 */
public class ReconnectSourceCommand extends Command {

	/** source Activity **/
	protected IActivity source;
	/** target Activity **/
	protected IActivity target;
	/** transition between source and target **/
	protected Transition transition;
	/** previous source prior to command execution **/
	protected IActivity oldSource;

	/**
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return TransitionUtils.isAllowed(source, target)
				&& !transition.isBoundToParent();

	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		if (source != null) {
			oldSource.removeOutput(transition);
			transition.source = source;
			source.addOutput(transition);
		}
	}

	/**
	 * Returns the source Activity associated with this command
	 * 
	 * @return the source Activity
	 */
	public IActivity getSource() {
		return source;
	}

	/**
	 * Returns the target Activity associated with this command
	 * 
	 * @return the target Activity
	 */
	public IActivity getTarget() {
		return target;
	}

	/**
	 * Returns the Transition associated with this command
	 * 
	 * @return the Transition
	 */
	public Transition getTransition() {
		return transition;
	}

	/**
	 * Sets the source Activity associated with this command
	 * 
	 * @param activity
	 *            the source Activity
	 */
	public void setSource(IActivity activity) {
		source = activity;
	}

	/**
	 * Sets the target Activity assoicated with this command
	 * 
	 * @param activity
	 *            the target Activity
	 */
	public void setTarget(IActivity activity) {
		target = activity;
	}

	/**
	 * Sets the transition associated with this
	 * 
	 * @param trans
	 *            the transition
	 */
	public void setTransition(Transition trans) {
		transition = trans;
		target = trans.target;
		oldSource = trans.source;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		source.removeOutput(transition);
		transition.source = oldSource;
		oldSource.addOutput(transition);
	}

}
