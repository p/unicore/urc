package de.fzj.unicore.rcp.wfeditor.extensionpoints;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.wizard.IWizardPage;

import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.submission.ISubmitter;
import de.fzj.unicore.rcp.wfeditor.ui.WFSubmissionWizard;

/**
 * Implementing classes are used to instantiate Wizard Pages.
 * 
 * @author bdemuth
 * 
 */
public interface ISubmissionWizardPageFactory {

	/**
	 * Create the workflow engine specific pages that should be displayed in the
	 * {@link WFSubmissionWizard} after the first page. Do NOT return null in
	 * case you don't want to contribute any pages. Instead return an empty
	 * array.
	 * 
	 * @param wizard
	 * @param model
	 * @param submissionService
	 * @param submitter
	 * @return
	 */
	public IWizardPage[] createPages(WFSubmissionWizard wizard,
			WorkflowDiagram model, IAdaptable submissionService,
			ISubmitter submitter);

	/**
	 * This is called when the wizard gets disposed. It gives implementing
	 * classes a chance to cleanup behind them before the wizard may be opened
	 * the next time.
	 */
	public void dispose();

	/**
	 * When the service selection on the first wizard page changes, this method
	 * is called in order to give the implementing class a chance to apply
	 * modifications to the wizard pages if necessary. Pages can also be added
	 * or removed. Again, do NOT return null, instead use an empty array!
	 * 
	 * @param oldPages
	 * @param wizard
	 * @param model
	 * @param submissionService
	 * @param submitter
	 * @return
	 */
	public IWizardPage[] updatePages(IWizardPage[] oldPages,
			WFSubmissionWizard wizard, WorkflowDiagram model,
			IAdaptable submissionService, ISubmitter submitter);
}
