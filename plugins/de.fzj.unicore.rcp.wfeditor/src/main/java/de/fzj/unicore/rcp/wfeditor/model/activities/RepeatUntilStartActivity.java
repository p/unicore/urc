package de.fzj.unicore.rcp.wfeditor.model.activities;

import javax.xml.namespace.QName;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.variables.IElementWithVariableDeclarations;

@XStreamAlias("RepeatUntilStartActivity")
public class RepeatUntilStartActivity extends SimpleActivity implements
		IElementWithVariableDeclarations {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8838447838932082042L;

	public static QName TYPE = new QName("http://www.unicore.eu/",
	"RepeatUntilStartActivity");
	
	public RepeatUntilStartActivity() {
		super();

	}
	
	@Override
	public QName getType() {
		return TYPE;
	}

	public boolean allowsAddingVariableDeclarations() {
		return false;
	}

}
