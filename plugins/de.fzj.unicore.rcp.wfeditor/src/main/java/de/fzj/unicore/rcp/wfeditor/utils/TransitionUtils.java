package de.fzj.unicore.rcp.wfeditor.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.StartActivity;
import de.fzj.unicore.rcp.wfeditor.model.commands.ConnectionCreateCommand;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.traversal.BottomUpDAGTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.CountElementsVisitor;
import de.fzj.unicore.rcp.wfeditor.traversal.IElementVisitor;
import de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.TopDownDAGTraverser;

public class TransitionUtils {

	class LookForControlFlowVisitor implements IElementVisitor {
		IActivity source;
		boolean foundControlFlow = false;

		public LookForControlFlowVisitor(IActivity source) {
			this.source = source;
		}

		public boolean isDone() {
			return foundControlFlow;
		}

		public void visit(IFlowElement element) throws Exception {
			if (element == source) {
				foundControlFlow = true;
			}
		}
	}

	class TopDownDAGTraverserWithHypotheticalTransition extends
			TopDownDAGTraverser {

		IActivity source, target;

		public TopDownDAGTraverserWithHypotheticalTransition(IActivity source,
				IActivity target) {
			super();
			this.source = source;
			this.target = target;
		}

		@Override
		protected int calculateInDegree(IActivity activity) {
			int inDegree = 0;
			for (Transition t : activity.getIncomingTransitions()) {
				if (t != null && !t.isClosingLoop()) {
					inDegree++; // don't count loop closing transitions!
				}
			}
			if (activity == target) {
				inDegree++;
			}
			return inDegree;
		}

		@Override
		public void updateNeighboursInDegrees(IActivity activity,
				Map<String, Integer> inDegrees, Queue<String> q) {
			List<Transition> transitions = activity.getOutgoingTransitions();
			for (Transition transition : transitions) {
				if (transition.isClosingLoop()) {
					continue;
				}
				String id = transition.target.getID();
				try {
					int inDegree = inDegrees.get(id);
					inDegree--;
					inDegrees.put(id, inDegree);
					// whenever a node reaches in-degree 0, it is ready for
					// being traversed (visited)
					if (inDegrees.get(id) == 0) {
						q.offer(id);
					}
				} catch (Exception e) {
					// do nothing
				}
			}
			if (activity == source) {
				String id = target.getID();
				try {
					int inDegree = inDegrees.get(id);
					inDegree--;
					inDegrees.put(id, inDegree);
					// whenever a node reaches in-degree 0, it is ready for
					// being traversed (visited)
					if (inDegrees.get(id) == 0) {
						q.offer(id);
					}
				} catch (Exception e) {
					// do nothing
				}
			}
		}
	}

	/**
	 * Checks whether there is a path along the transitions of the diagram from
	 * the source (or one of its parents) to the target (or one of its parents).
	 * If no such path exists, this method returns an instance of
	 * {@link Command} that can be executed for establishing such a path.
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static Command ensureControlFlowBetween(IActivity source,
			IActivity target) {

		if (wouldCloseLoop(source, target)) {
			return null;
		}
		// create control flow if it doesn't exist
		List<IActivity> sourceParents = new ArrayList<IActivity>();
		IActivity current = source;

		while (current != null) {
			sourceParents.add(current);
			current = current.getParent();
		}
		List<IActivity> targetParents = new ArrayList<IActivity>();
		current = target;
		while (current != null) {
			targetParents.add(current);
			current = current.getParent();
		}

		int i = sourceParents.size() - 1, j = targetParents.size() - 1;
		while (sourceParents.get(i) == targetParents.get(j) && i > 0 && j > 0) {
			if (i > 0) {
				i--;
			}
			if (j > 0) {
				j--;
			}
		}
		IActivity sourceParent = sourceParents.get(i);
		IActivity targetParent = targetParents.get(j);

		BottomUpDAGTraverser traverser = new BottomUpDAGTraverser();
		LookForControlFlowVisitor visitor = new TransitionUtils().new LookForControlFlowVisitor(
				source);
		try {
			traverser.traverseGraph(targetParent.getParent(), targetParent,
					visitor);
		} catch (Exception e) {
			return null;
		}
		if (!visitor.foundControlFlow) {
			ConnectionCreateCommand createdTransitionCommand = new ConnectionCreateCommand();
			createdTransitionCommand.setSource(sourceParent);
			createdTransitionCommand.setTarget(targetParent);
			if (createdTransitionCommand.canExecute()) {
				return createdTransitionCommand;
			}

		}

		return null;
	}

	public static boolean existsAlready(IActivity source, IActivity target) {
		// Check if connection exists already
		List<Transition> transistions = source.getOutgoingTransitions();
		for (int i = 0; i < transistions.size(); i++) {
			if (transistions.get(i).target.equals(target)) {
				return true;
			}
		}
		return false;
	}

	public static List<Transition> getNonLoopIncomingTransitions(
			IActivity activity) {
		List<Transition> result = new ArrayList<Transition>();
		for (Transition t : activity.getIncomingTransitions()) {
			if (!t.isClosingLoop()) {
				result.add(t);
			}
		}
		return result;
	}

	public static List<Transition> getNonLoopOutgoingTransitions(
			IActivity activity) {
		List<Transition> result = new ArrayList<Transition>();
		for (Transition t : activity.getOutgoingTransitions()) {
			if (!t.isClosingLoop()) {
				result.add(t);
			}
		}
		return result;
	}

	/**
	 * Checks whether a new transition with the given source and target can be
	 * established. Self-loops, transitions with identical source target and
	 * transitions crossing subworkflow borders are not allowed.
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static boolean isAllowed(IActivity source, IActivity target) {
		return sourceAndTargetValid(source, target)
				&& shareSameParent(source, target)
				&& !isSelfLoop(source, target)
				&& !existsAlready(source, target)
				&& (source.getParent() == null || source.getParent()
						.canAddChildren());
	}

	public static boolean isAllowedIgnoreParent(IActivity source,
			IActivity target) {
		return sourceAndTargetValid(source, target)
				&& !isSelfLoop(source, target)
				&& !existsAlready(source, target);
	}

	public static boolean isOkToKeep(Transition t) {
		IActivity source = t.source;
		IActivity target = t.target;
		return sourceAndTargetValid(source, target)
				&& shareSameParent(source, target)
				&& !isSelfLoop(source, target);
	}

	public static boolean isSelfLoop(IActivity source, IActivity target) {
		return target == null ? source == null : target.equals(source);
	}

	public static boolean lineContainsPoint(Point p1, Point p2, Point p) {
		int tolerance = 7;
		Rectangle rect = Rectangle.SINGLETON;
		rect.setSize(0, 0);
		rect.setLocation(p1.x, p1.y);
		rect.union(p2.x, p2.y);
		rect.expand(tolerance, tolerance);
		if (!rect.contains(p.x, p.y)) {
			return false;
		}

		int v1x, v1y, v2x, v2y;
		int numerator, denominator;
		double result = 0.0;

		if (p1.x != p2.x && p1.y != p2.y) {

			v1x = p2.x - p1.x;
			v1y = p2.y - p1.y;
			v2x = p.x - p1.x;
			v2y = p.y - p1.y;

			numerator = v2x * v1y - v1x * v2y;
			denominator = v1x * v1x + v1y * v1y;

			result = ((numerator << 10) / denominator * numerator) >> 10;
		}

		// if it is the same point, and it passes the bounding box test,
		// the result is always true.
		return result <= tolerance * tolerance;
	}

	public static boolean shareSameParent(IActivity source, IActivity target) {
		return source.getParent() == null ? target.getParent() == null : source
				.getParent().equals(target.getParent());
	}

	public static boolean sourceAndTargetValid(IActivity source,
			IActivity target) {
		return source != null && target != null
				&& !(target instanceof StartActivity);
	}

	/**
	 * Returns true if a new Transition between source and target activity would
	 * create a loop in the control flow graph. This method only works correctly
	 * if source and target activity are children of the same subgraph.
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static boolean wouldCloseLoop(IActivity source, IActivity target) {
		IGraphTraverser traverser = new TransitionUtils().new TopDownDAGTraverserWithHypotheticalTransition(
				source, target);
		CountElementsVisitor counter = new CountElementsVisitor();
		try {
			traverser.traverseGraph(source.getParent(), source, counter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return source.getParent().getChildren().size() != counter
				.getElementCount();
	}

}
