/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.utils.TransitionUtils;

/**
 * @author Daniel Lee
 */
public class ConnectionCreateCommand extends Command {

	/** The Transistion between source and target Activities **/
	protected Transition transition;
	/** The source Activity **/
	protected IActivity source;
	/** The target Activity **/
	protected IActivity target;

	/**
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return TransitionUtils.isAllowed(getSource(), getTarget());
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		transition = new Transition(source, target);
	}

	/**
	 * Returns the source Activity
	 * 
	 * @return the source
	 */
	public IActivity getSource() {
		return source;
	}

	/**
	 * Returns the target Activity
	 * 
	 * @return the target
	 */
	public IActivity getTarget() {
		return target;
	}

	/**
	 * Returns the Transition between the source and target Activities
	 * 
	 * @return the transition
	 */
	public Transition getTransition() {
		return transition;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		source.addOutput(transition);
		target.addInput(transition);
	}

	/**
	 * Sets the source Activity
	 * 
	 * @param activity
	 *            the source Activity
	 */
	public void setSource(IActivity activity) {
		source = activity;
	}

	/**
	 * Sets the target Activity
	 * 
	 * @param activity
	 *            the target
	 */
	public void setTarget(IActivity activity) {
		target = activity;
	}

	/**
	 * Sets the Transistion between the source and target Activities
	 * 
	 * @param transition
	 *            the transistion
	 */
	public void setTransition(Transition transition) {
		this.transition = transition;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		source.removeOutput(transition);
		target.removeInput(transition);
	}

}
