/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;

import de.fzj.unicore.rcp.wfeditor.extensionpoints.IPaletteEntryExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.PaletteCategory;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.PaletteElement;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;

/**
 * Handles the creation of the palette for the Flow Editor.
 * 
 * @author demuth
 */
public class PaletteFactory {

	// the editor for which a palette is being built
	private WFEditor editor;

	private Map<PaletteCategory, PaletteContainer> categories = new HashMap<PaletteCategory, PaletteContainer>();
	private List<IPaletteEntryExtensionPoint> extensions = new ArrayList<IPaletteEntryExtensionPoint>();

	public PaletteFactory(WFEditor editor) {

		this.editor = editor;
	}

	private void addElements(List<PaletteElement> elements) {
		for (PaletteElement paletteElement : elements) {
			PaletteCategory key = paletteElement.getCategory();
			PaletteContainer container = categories.get(key);
			if (container == null) {
				container = new PaletteDrawer(key.getName());
				categories.put(key, container);
			}
			container.add(paletteElement.getEntry());
		}
	}

	private List<PaletteContainer> createCategories(PaletteRoot root) {

		if(extensions.isEmpty())
		{
			iterateOverExtensions();
		}

		for(IPaletteEntryExtensionPoint ext : extensions)
		{
			List<PaletteElement> elements = ext
			.getAdditionalPaletteEntries(getEditor());
			addElements(elements);
		}

		List<PaletteContainer> result = new ArrayList<PaletteContainer>();

		List<PaletteCategory> sorted = new ArrayList<PaletteCategory>(
				categories.keySet());
		Collections.sort(sorted);
		for (PaletteCategory c : sorted) {
			PaletteContainer cont = categories.get(c);
			if (!result.contains(cont)) {
				result.add(cont);
			}
		}
		return result;
	}

	/**
	 * Creates the PaletteRoot and adds all Palette elements.
	 * 
	 * @return the root
	 */
	public PaletteRoot createPalette() {
		categories.clear();
		PaletteRoot flowPalette = new PaletteRoot();
		flowPalette.addAll(createCategories(flowPalette));
		return flowPalette;
	}

	/**
	 * Perform some cleanup!
	 */
	public void dispose() {
		for (IPaletteEntryExtensionPoint ext : extensions) {
			ext.dispose();
		}
	}

	public WFEditor getEditor() {
		return editor;
	}

	private void iterateOverExtensions() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
		.getExtensionPoint(WFConstants.EXTENSION_POINT_PALETTE_ENTRY);
		IConfigurationElement[] members = extensionPoint
		.getConfigurationElements();
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			try {
				IPaletteEntryExtensionPoint ext = (IPaletteEntryExtensionPoint) member
				.createExecutableExtension("name");

				extensions.add(ext);

			} catch (CoreException ex) {
				WFActivator.log(
						"Could not determine all available palette entries for the extension "
						+ member.getDeclaringExtension()
						.getUniqueIdentifier(), ex);
			}
		}
	}

	public void setEditor(WFEditor editor) {
		this.editor = editor;
	}

}
