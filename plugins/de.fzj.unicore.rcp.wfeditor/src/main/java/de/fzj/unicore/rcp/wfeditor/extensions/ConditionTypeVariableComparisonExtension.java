/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.extensions;


import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.swt.widgets.Composite;

import de.fzj.unicore.rcp.wfeditor.extensionpoints.IConditionTypeExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.model.conditions.ConditionTypeVariableComparison;
import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType;
import de.fzj.unicore.rcp.wfeditor.ui.ConditionTypeVariableComparisonControl;
import de.fzj.unicore.rcp.wfeditor.ui.IConditionChangeListener;
import de.fzj.unicore.rcp.wfeditor.ui.IConditionTypeControlProducer;

/**
 * @author demuth
 * 
 */
public class ConditionTypeVariableComparisonExtension implements
		IConditionTypeExtensionPoint {

	private class VariableComparisonControlProducer extends CommonControlProducer {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * de.fzj.unicore.rcp.wfeditor.extensionpoints.IConditionTypeExtensionPoint
		 * #getControl(org.eclipse.swt.widgets.Composite,
		 * de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType)
		 */
		public CommonConditionTypeControl getControl(Composite parent, Condition condition) {
			control = new ConditionTypeVariableComparisonControl(parent,
					condition);
			parent.pack();
			for (IConditionChangeListener l : listeners) {
				control.addValidityChangeListener(l);
			}
			listeners.clear();
			return control;
		}
	}

	public static final String ID = "de.fzj.unicore.rcp.wfeditor.extensions.conditionVariableComparison";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.extensionpoints.IConditionTypeExtensionPoint
	 * #createNewConditionType()
	 */
	public IConditionType createNewConditionType() {
		return new ConditionTypeVariableComparison();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.extensionpoints.IConditionTypeExtensionPoint
	 * #getControlProducer()
	 */
	public IConditionTypeControlProducer getControlProducer() {
		return new VariableComparisonControlProducer();
	}

	public String getExtensionId() {
		return ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.IExecutableExtension#setInitializationData(org
	 * .eclipse.core.runtime.IConfigurationElement, java.lang.String,
	 * java.lang.Object)
	 */
	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {
		// TODO Auto-generated method stub

	}
}
