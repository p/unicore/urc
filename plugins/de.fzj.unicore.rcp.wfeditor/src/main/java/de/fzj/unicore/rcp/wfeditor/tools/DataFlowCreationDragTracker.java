package de.fzj.unicore.rcp.wfeditor.tools;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;

public class DataFlowCreationDragTracker extends ConnectionCreationDragTracker {

	public DataFlowCreationDragTracker(EditPart sourceEditPart) {
		super(sourceEditPart);
	}

	@Override
	protected boolean handleButtonUp(int button) {
		EditPart targetPart = getTargetEditPart();
		EditPart sourcePart = getConnectionSource();
		EditPartViewer viewer = getCurrentViewer();
		if (viewer != null && targetPart != null && targetPart == sourcePart) {
			viewer.select(targetPart);

		}
		return super.handleButtonUp(button);
	}

}
