package de.fzj.unicore.rcp.wfeditor.model.activities;

import javax.xml.namespace.QName;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.variables.IElementWithVariableDeclarations;

@XStreamAlias("WhileStartActivity")
public class WhileStartActivity extends SimpleActivity implements
IElementWithVariableDeclarations {

	public static QName TYPE = new QName("http://www.unicore.eu/",
	"WhileStartActivity");



	@Override
	public QName getType() {
		return TYPE;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8666638196271540311L;

	public boolean allowsAddingVariableDeclarations() {
		return false;
	}



	private Object readResolve() {
		return this;
	}
}
