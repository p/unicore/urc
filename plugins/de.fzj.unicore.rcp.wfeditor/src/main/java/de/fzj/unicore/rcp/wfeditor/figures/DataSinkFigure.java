package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ImageFigure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowUtils;

public class DataSinkFigure extends Figure {

	private Color dataTypeColor;
	private String dataTypeSymbol;
	/**
	 * Lines of text to be displayed inside the tooltip figure.
	 */
	private String[] toolTipText;

	private Font toolTipFont;
	private IFigure problemFigure = null;

	private boolean mustBeFilled = false;

	public DataSinkFigure() {
		setLayoutManager(new XYLayout());
	}

	public Dimension calculatePreferredSize() {
		String text = getDataTypeSymbol();

		Dimension d = WorkflowUtils.getPreferredTextDimension(text, getFont());
		d.height = WFConstants.DEFAULT_DATA_CONNECTOR_HEIGHT;
		d.width += 5;
		return d;
	}

	public Color getDataTypeColor() {
		return dataTypeColor;
	}

	public String getDataTypeSymbol() {
		return dataTypeSymbol;
	}

	@Override
	public IFigure getToolTip() {
		IFigure result = new DataSinkToolTip();
		result.setFont(toolTipFont);
		return result;
	}

	public Font getToolTipFont() {
		return toolTipFont;
	}

	public String[] getToolTipText() {
		return toolTipText;
	}

	@Override
	protected void paintFigure(Graphics graphics) {

		Rectangle bounds = getBounds();
		if (dataTypeSymbol != null) {
			Font oldFont = graphics.getFont();
			graphics.setFont(getFont());

			int charWidth = graphics.getFontMetrics().getAverageCharWidth();
			int charHeight = graphics.getFontMetrics().getHeight();
			int labelWidth = charWidth * dataTypeSymbol.length();
			Point center = bounds.getCenter();

			if (getDataTypeColor() != null) {
				graphics.setBackgroundColor(getDataTypeColor());
				graphics.fillRectangle(bounds.getCropped(new Insets(1, 4,
						center.y - bounds.y + charHeight / 2 - 1, 2)));
			}
			String text = dataTypeSymbol;
			int x = center.x - labelWidth / 2 + 1;
			if (mustBeFilled) {
				x -= 6;
			}
			x = Math.max(bounds.x + 1, x);
			graphics.drawText(text, x, center.y - charHeight / 2);
			graphics.setFont(oldFont);

		}
		graphics.drawRectangle(bounds.getCropped(new Insets(1, 1, 2, 1)));
	}

	public void setDataTypeColor(Color dataTypeColor) {
		this.dataTypeColor = dataTypeColor;
	}

	public void setDataTypeSymbol(String label) {
		this.dataTypeSymbol = label;
	}

	/**
	 * Tell the figure whether the data sink must yet be filled by some data
	 * flow(s) before the workflow can be submitted. Such a data flow is invalid
	 * and should be marked accordingly
	 */
	public void setMustBeFilled(boolean mustBeFilled) {
		this.mustBeFilled = mustBeFilled;
		if (mustBeFilled) {
			if (problemFigure == null) {
				Dimension d = calculatePreferredSize();
				int height;
				int width;
				Image problemImage = WFActivator.getDefault()
						.getImageRegistry()
						.get(WFConstants.IMAGE_DATA_SINK_PROBLEM);
				if (problemImage == null) {
					height = (int) (d.height * 3.0 / 4.0);
					Image img = PlatformUI.getWorkbench().getDisplay()
							.getSystemImage(SWT.ICON_WARNING);
					ImageData id = img.getImageData();
					width = (int) (id.width * ((double) height) / id.height);
					id = id.scaledTo(width, height);
					problemImage = new Image(getFont().getDevice(), id);
					WFActivator
							.getDefault()
							.getImageRegistry()
							.put(WFConstants.IMAGE_DATA_SINK_PROBLEM,
									problemImage);
				} else {
					width = problemImage.getImageData().width;
					height = problemImage.getImageData().height;
				}
				problemFigure = new ImageFigure(problemImage);
				add(problemFigure);
				setConstraint(problemFigure, new Rectangle(d.width - width,
						d.height - height, width, height));
			}
		}
		if (problemFigure != null) {
			problemFigure.setVisible(mustBeFilled);
		}
	}

	public void setToolTipFont(Font tooltipFont) {
		this.toolTipFont = tooltipFont;
	}

	public void setToolTipText(String[] toolTipText) {
		this.toolTipText = toolTipText;
	}

}
