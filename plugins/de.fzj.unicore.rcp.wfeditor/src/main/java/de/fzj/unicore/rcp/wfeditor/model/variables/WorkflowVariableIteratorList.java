package de.fzj.unicore.rcp.wfeditor.model.variables;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.interfaces.ISerializable;
import de.fzj.unicore.rcp.wfeditor.model.IDisposableWithUndo;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

@XStreamAlias("WorkflowVariableIteratorList")
public class WorkflowVariableIteratorList extends PropertySource implements
		IDisposableWithUndo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4003291711594299244L;
	private List<WorkflowVariableIterator> iterators;

	public WorkflowVariableIteratorList() {

	}

	@Override
	public void activate() {
		super.activate();
		for (WorkflowVariableIterator var : getIterators()) {
			var.activate();
		}
	}

	public void addIterator(WorkflowVariableIterator v) {
		getIterators().add(v);
		setPropertyValue(v.getId(), v);
	}

	@Override
	protected void addPropertyDescriptors() {
		getPropertyDescriptorList().clear();
		// for (WorkflowVariableIterator current : getIterators()) {
		// addPropertyDescriptor(new
		// WorkflowVariablePropertyDescriptor(current));
		// }
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
		for (ISerializable current : getIterators()) {
			current.afterDeserialization();
		}
	}

	@Override
	public void afterSerialization() {
		super.afterSerialization();
		for (ISerializable current : getIterators()) {
			current.afterSerialization();
		}
	}

	@Override
	public void beforeSerialization() {
		super.beforeSerialization();
		for (ISerializable current : getIterators()) {
			current.beforeSerialization();
		}
	}

	@Override
	public void dispose() {
		super.dispose();
		for (WorkflowVariableIterator var : getIterators()) {
			if (!var.isDisposed()) {
				var.dispose();
			}
		}

	}

	@Override
	public WorkflowVariableIteratorList getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		WorkflowVariableIteratorList copy = (WorkflowVariableIteratorList) super
				.getCopy(toBeCopied, mapOfCopies, copyFlags);
		copy.iterators = new ArrayList<WorkflowVariableIterator>();
		for (WorkflowVariableIterator mod : getIterators()) {
			WorkflowVariableIterator newIt = CloneUtils.getFromMapOrCopy(mod,
					toBeCopied, mapOfCopies, copyFlags);
			copy.addIterator(newIt);
		}
		return copy;
	}

	public List<WorkflowVariableIterator> getIterators() {
		if (iterators == null) {
			iterators = new ArrayList<WorkflowVariableIterator>();
		}
		return iterators;
	}

	public void removeIterator(WorkflowVariableIterator v) {
		getIterators().remove(v);
		removeProperty(v.getId());
	}

	@Override
	public String toString() {
		return "";
	}

	@Override
	public void undoDispose() {
		super.undoDispose();
		for (WorkflowVariableIterator var : getIterators()) {
			if (var.isDisposed()) {
				var.undoDispose();
			}
		}

	}
}
