/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.activities;

import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;

/**
 * @author demuth
 * 
 */
public interface IActivity extends IFlowElement {

	/**
	 * The key for the activities location property ({@link Point} value)
	 */
	public static final String PROP_LOCATION = "location";

	/**
	 * The key for the activities size property ({@link Dimension} value)
	 */
	public static final String PROP_SIZE = "size";

	/**
	 * The key for the output files property
	 */
	public static final String PROP_OUTPUT_FILES = "output files";

	/**
	 * Called when an Activity gets first added to the diagram. Run some init
	 * code. Take care of adding the activity to the diagrams map of activities!
	 * 
	 */
	public void activate();

	/**
	 * Adds an incoming transition
	 * 
	 * @param transition
	 */
	public void addInput(Transition transition);

	/**
	 * Adds an incoming transition which should be placed at the specified index
	 * 
	 * @param transition
	 * @param index
	 */
	public void addInput(Transition transition, int index);

	/**
	 * Adds an outgoing transition
	 * 
	 * @param transtition
	 */
	public void addOutput(Transition transtition);

	/**
	 * Adds an outgoing transition which should be placed at the specified index
	 * 
	 * @param transition
	 * @param index
	 */
	public void addOutput(Transition transtition, int index);

	public void addOutputFile(WorkflowFile f);

	/**
	 * Restore original state as it was before beforeSaveAs was called. The
	 * reasoning is that beforeSave as may require some changes in links to
	 * external files which have to be undone.
	 * 
	 * @param originalLocation
	 *            absolute path to the original folder of the saved diagram
	 * @param recursive
	 *            migrate the activity and all its sub-activities
	 * @param copyLocalInputFiles
	 *            tells the activity to copy locally available data to the
	 *            target as well in order to make the copy more self-contained
	 * @param copyRemoteInputFiles
	 *            tells the activity to copy remote data to the target as well
	 *            in order to make the copy even more self-contained
	 * @param monitor
	 * @throws Exception
	 */
	public void afterSaveAs(IPath originalLocation, boolean recursive,
			boolean copyLocalInputFiles, boolean copyRemoteInputFiles,
			IProgressMonitor monitor) throws Exception;

	/**
	 * * Asks the activity to copy its data to the given location. This is
	 * called when a functional copy of the workflow needs to be assembled, e.g.
	 * when the workflow is submitted or wrapped up in a zip file.
	 * 
	 * @param newLocation
	 *            absolute path to the destination folder of the saved diagram
	 * @param recursive
	 *            migrate the activity and all its sub-activities
	 * @param copyLocalInputFiles
	 *            tells the activity to copy locally available data to the
	 *            target as well in order to make the copy more self-contained
	 * @param copyRemoteInputFiles
	 *            tells the activity to copy remote data to the target as well
	 *            in order to make the copy even more self-contained
	 * @param monitor
	 */
	public void beforeSaveAs(IPath newLocation, boolean recursive,
			boolean copyLocalInputFiles, boolean copyRemoteInputFiles,
			IProgressMonitor monitor) throws Exception;

	/**
	 * Returns a list of all incoming transitions
	 * 
	 * @return
	 */
	public List<Transition> getIncomingTransitions();

	/**
	 * Get the coordinates of the upper left corner of the activity in the
	 * workflow diagram
	 * 
	 * @return
	 */
	public Point getLocation();

	/**
	 * Returns a list of all outgoing transitions
	 * 
	 * @return
	 */
	public List<Transition> getOutgoingTransitions();

	/**
	 * Returns a list of output files that are produced by the activity during
	 * workflow execution and can be referenced by other activities
	 * 
	 * @return
	 */
	public WorkflowFile[] getOutputFiles();

	/**
	 * Returns the absolute size of the activity in the diagram
	 * 
	 * @return
	 */
	public Dimension getSize();

	public int getSortIndex();

	/**
	 * Move the activity and its children
	 * 
	 * @param deltaX
	 * @param deltaY
	 */
	public void move(int deltaX, int deltaY);

	public int removeInput(Transition transition);

	public int removeOutput(Transition transition);

	public void removeOutputFile(WorkflowFile f);

	/**
	 * Set the activity's location. This does not influence the activity's
	 * children!
	 * 
	 * @param location
	 */
	public void setLocation(Point location);

	public void setSize(Dimension size);

	public void setSortIndex(int i);

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString();

}