package de.fzj.unicore.rcp.wfeditor.model.variables;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.eclipse.gef.commands.Command;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;
import de.fzj.unicore.rcp.wfeditor.utils.TransitionUtils;

@XStreamAlias("VariableToModifierDataFlow")
public class VariableToModifierDataFlow extends PropertySource implements
		IDataFlow, PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7233022035247075402L;
	private WorkflowVariable source;
	private VariableModifierDataSink sink;
	private String id;
	private transient int sinkIndex, sourceIndex = -1;
	private transient Command createTransitionCommand;

	public VariableToModifierDataFlow(WorkflowVariable source,
			VariableModifierDataSink sink) {
		super();
		this.source = source;
		this.sink = sink;
		id = createRandomId();
		sink.getModifier().setVariableName(source.getName());
	}

	@Override
	protected void addPropertyDescriptors() {
		// TODO Auto-generated method stub

	}

	protected void addToSourceAndSink(int sourceIndex, int sinkIndex) {
		if (!getDataSource().hasOutgoingFlow(this)) {
			getDataSource().addOutgoingFlow(this, sourceIndex);
		}
		if (!getDataSink().hasIncomingFlow(this)) {
			getDataSink().addIncomingFlow(this, sinkIndex);
		}
	}

	public void connect() {
		reconnect();
	}

	public void disconnect() {
		sink.getModifier().setVariableName(null);
		sinkIndex = sink.removeIncomingFlow(this);
		sourceIndex = source.removeOutgoingFlow(this);
		source.removePersistentPropertyChangeListener(this);

	}

	@Override
	public ICopyable getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		IDataFlow copy = null;

		WorkflowVariable source = this.source;
		IFlowElement sourceParent = source.getFlowElement();
		VariableModifierDataSink sink = this.sink;
		IFlowElement sinkParent = sink.getFlowElement();
		boolean isCut = (copyFlags & ICopyable.FLAG_CUT) != 0;

		if (!isCut) {
			// this is not a cut => if the sink's parent element is not copied
			// we shouldn't be copied either!
			if (!CloneUtils.isBeingCopied(sinkParent, toBeCopied)) {
				return null;
			}
		}

		boolean copySourceParent = CloneUtils.isBeingCopied(sourceParent,
				toBeCopied);
		boolean copySinkParent = CloneUtils.isBeingCopied(sinkParent,
				toBeCopied);

		if (copySourceParent) {
			sourceParent = CloneUtils.getFromMapOrCopy(sourceParent,
					toBeCopied, mapOfCopies, copyFlags);
			source = CloneUtils.getFromMapOrCopy(source, toBeCopied,
					mapOfCopies, copyFlags);
		}
		if (copySinkParent) {
			sinkParent = CloneUtils.getFromMapOrCopy(sinkParent, toBeCopied,
					mapOfCopies, copyFlags);
			sink = CloneUtils.getFromMapOrCopy(sink, toBeCopied, mapOfCopies,
					copyFlags);
			sink.getModifier().setVariableName(source.getName());
		}
		if (mapOfCopies.get(this) == null) {
			copy = new VariableToModifierDataFlow(source, sink);

			// link to object subgraph that is being copied
			// do NOT link to the diagram itself (the whole object graph)
			if (copySourceParent) {
				if (!source.hasOutgoingFlow(copy)) {
					source.addOutgoingFlow(copy);
				}
			}
			if (copySinkParent) {
				if (!sink.hasIncomingFlow(copy)) {
					sink.addIncomingFlow(copy);
				}
			}
		} else {
			return (ICopyable) mapOfCopies.get(this);
		}
		return copy;
	}

	public IDataSink getDataSink() {
		return sink;
	}

	public IDataSource getDataSource() {
		return source;
	}

	public String getId() {
		return id;
	}

	public QName getType() {
		return IDataFlow.TYPE;
	}

	@Override
	public Set<Class<?>> insertAfter() {
		Set<Class<?>> result = super.insertAfter();
		result.add(IActivity.class);
		result.add(Transition.class);
		return result;
	}

	@Override
	public void insertCopyIntoDiagram(ICopyable c, StructuredActivity parent,
			Set<IFlowElement> toBeCopied, Map<Object, Object> mapOfCopies,
			int copyFlags) {
		// only fully connect to the diagram after pasting
		VariableToModifierDataFlow copy = (VariableToModifierDataFlow) c;
		copy.connect();
		if (copy.source.getFlowElement() instanceof IActivity
				&& copy.sink.getFlowElement() instanceof IActivity) {
			IActivity source = (IActivity) copy.source.getFlowElement();
			IActivity target = (IActivity) copy.sink.getFlowElement();
			copy.createTransitionCommand = TransitionUtils
					.ensureControlFlowBetween(source, target);
			if (copy.createTransitionCommand != null) {
				copy.createTransitionCommand.execute();
			}
		}
	}

	public boolean isConnected() {
		return sink != null && sink.hasIncomingFlow(this);

	}

	public boolean isVisible() {
		return source != null && sink != null && source.isVisible()
				&& sink.isVisible();
	}

	public void propertyChange(PropertyChangeEvent evt) {
		if (WorkflowVariable.PROP_NAME.equals(evt.getPropertyName())) {
			String newValue = (String) evt.getNewValue();
			sink.getModifier().setVariableName(newValue);
		} else if (WorkflowVariable.PROP_TYPE.equals(evt.getPropertyName())) {
			String newValue = (String) evt.getNewValue();
			sink.getModifier().setPropertyValue(
					WorkflowVariableModifier.PROP_TYPE, newValue);
		}
	}

	public void reconnect() {
		if (source.isDisposed() || source.getFlowElement() == null
				|| source.getFlowElement().isDisposed() || sink == null
				|| sink.isDisposed() || sink.getFlowElement() == null
				|| sink.getFlowElement().isDisposed()) {
			return; // try to avoid restoring invalid links
		}
		addToSourceAndSink(sourceIndex, sinkIndex);
		sink.getModifier().setVariableName(source.getName());
		source.addPersistentPropertyChangeListener(this);
	}

	protected void removeFromSourceAndSink() {
		if (getDataSource().hasOutgoingFlow(this)) {
			sourceIndex = getDataSource().removeOutgoingFlow(this);
		}
		if (getDataSink().hasIncomingFlow(this)) {
			sinkIndex = getDataSink().removeIncomingFlow(this);
		}
	}

}
