package de.fzj.unicore.rcp.wfeditor.model;

import java.beans.PropertyChangeEvent;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.commands.CommandStackListener;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.StartActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.serialization.WFDiagramConverter;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateDescriptor;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.utils.JAXBContextProvider;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowUtils;
import de.fzj.unicore.rcp.wfeditor.utils.XStreamConfigurator;

/**
 * 
 * @author Bastian Demuth
 */
@XStreamAlias("WorkflowDiagram")
public class WorkflowDiagram extends StructuredActivity {

	private static final long serialVersionUID = 1580107457524117972L;

	public static final String CURRENT_MODEL_VERSION = Constants.CURRENT_CLIENT_VERSION;

	public static QName TYPE = new QName("http://www.unicore.eu/",
			"WorkflowDiagram");
	/**
	 * The property defining the workflow's termination time which is an Integer
	 * corresponding to the time to be added to the submission time (in hours)
	 */
	public static final String PROP_TERMINATION_TIME = "workflow termination time";

	/**
	 * The Boolean property stating whether the diagram can be submitted right
	 * now
	 */
	public static final String PROP_READY_FOR_SUBMISSION = "ready for submission";

	/**
	 * This property holds a data object of type {@link DiagramView} that
	 * represents the user's view on the diagram when it was closed. Note that
	 * this object only gets updated shortly before closing the diagram!
	 */
	public static final String PROP_DIAGRAM_VIEW = "diagram view";

	/**
	 * Possible values for this property
	 */
	public static Set<Integer> editableStates;

	/**
	 * Key for dirty flag that might be set independently of the Command stack's
	 * dirty state and claims that the diagram has changed (and might be saved)
	 */
	public static final String PROP_DIRTY = "dirty";

	/**
	 * Keep track of the model version. This allows for adjustments of the
	 * deserialization process of old diagram models, which should improve
	 * backward compatibility. This field is manually written to the output XML
	 * as an attribute by {@link WFDiagramConverter} therefore it is marked to
	 * be omitted by default XStream converters.
	 */
	@XStreamOmitField
	public String modelVersion = CURRENT_MODEL_VERSION;

	/**
	 * The name for storing the diagram to a file
	 */
	private String filename;

	private transient IProject project;

	private transient CommandStackListener commandStackListener;

	/**
	 * An object that describes the context in which the diagram was created /
	 * is used. It is used for different purposes, e.g. finding a suitable
	 * converter which transforms the diagram into an (XML) workflow document.
	 */
	private DiagramContext context;

	/**
	 * A map containing all activities within this diagram. The activities' IDs
	 * are used as keys.
	 */
	private Map<String, IActivity> activities;

	private transient Map<String, Transition> transitions;

	/**
	 * A Set containing disposed Disposables that need to be cleaned up when
	 * saving this diagram.
	 */
	private transient Set<IDisposableWithUndo> disposedElements;

	/**
	 * A map containing all workflow variables defined within this diagram. The
	 * variables' names are used as keys.
	 */
	private Map<String, WorkflowVariable> variables;

	private transient ExecutionData executionData;

	// parent folder of this workflow file, relative
	// to the parent project
	private String parentFolder;

	/**
	 * 
	 */
	public WorkflowDiagram() {
		super();
		activities = new HashMap<String, IActivity>();
		setPropertyValue(PROP_DIRTY, false);
		setDiagram(this);
		createStartActivity();
	}

	@Override
	public void activate() {

		super.activate();

	}

	/**
	 * Add an activity to the list of known activities.
	 * 
	 * @param activity
	 * @return
	 */
	public void addActivity(IActivity activity) {
		if (activity == null || activities.containsKey(activity.getName())) {
			return;
		}
		activity.addPersistentPropertyChangeListener(this);
		activities.put(activity.getName(), activity);
	}

	public void addDisposedElement(IDisposableWithUndo disposable) {
		getDisposedElements().add(disposable);
	}

	public void addTransition(Transition t) {
		if (t == null || getTransitions().containsKey(t.getName())) {
			return;
		}
		getTransitions().put(t.getName(), t);
		t.addPropertyChangeListener(this);
	}

	public void addVariable(WorkflowVariable variable) {
		if (variable == null || variables.containsKey(variable.getName())) {
			return;
		}
		getVariables().put(variable.getName(), variable);
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
	}

	@Override
	public void afterSaveAs(IPath originalLocation, boolean recursive,
			boolean copyLocalInputFiles, boolean copyRemoteInputFiles,
			IProgressMonitor monitor) throws Exception {
		IPath relative = PathUtils.makeRelativeTo(getProject().getLocation(),
				originalLocation);
		String parent = relative.toPortableString();
		if (parent.trim().length() == 0) {
			parent = null;
		}
		setParentFolder(parent);
		super.afterSaveAs(originalLocation, recursive, copyLocalInputFiles,
				copyRemoteInputFiles, monitor);
	}

	@Override
	public void afterSubmission() {
		super.afterSubmission();
		changeCurrentExecutionState(ExecutionStateConstants.STATE_RUNNING,
				"running");
		saveExecutionData();

	}

	@Override
	public void beforeSaveAs(IPath newLocation, boolean recursive,
			boolean copyLocalInputFiles, boolean copyRemoteInputFiles,
			IProgressMonitor monitor) throws Exception {
		IPath relative = PathUtils.makeRelativeTo(getProject().getLocation(),
				newLocation);
		setParentFolder(relative.toPortableString());
		super.beforeSaveAs(newLocation, recursive, copyLocalInputFiles,
				copyRemoteInputFiles, monitor);

	}

	@Override
	public void beforeSubmission() {
		super.beforeSubmission();
		createExecutionData();
		changeCurrentExecutionState(ExecutionStateConstants.STATE_SUBMITTING,
				"submitting");
	}

	@Override
	public void changeCurrentExecutionState(int cipher, String description) {
		super.changeCurrentExecutionState(cipher, description);
		checkReadyForSubmission();

	}

	public void changeVariableName(WorkflowVariable variable, String oldName,
			String newName) {
		getVariables().remove(oldName);
		getVariables().put(newName, variable);
	}

	public void checkReadyForSubmission() {
		Boolean oldProperty = (Boolean) getPropertyValue(PROP_READY_FOR_SUBMISSION);
		boolean oldReady = oldProperty == null ? true : oldProperty;
		boolean newReady = true;
		int state = getCurrentExecutionStateCipher();
		if (state != ExecutionStateConstants.STATE_UNSUBMITTED) {
			newReady = false;
		}
		if (newReady) {
			for (Iterator<IActivity> iter = activities.values().iterator(); iter
					.hasNext();) {
				IActivity activity = iter.next();
				if (!activity.readyForSubmission()) {
					newReady = false;
					break;
				}
			}
		}
		if (oldReady != newReady) {
			setPropertyValue(PROP_READY_FOR_SUBMISSION, newReady);
		}
	}

	public void cleanUpDisposedElements(IProgressMonitor progress) {
		IDisposableWithUndo[] disposed = getDisposedElements().toArray(
				new IDisposableWithUndo[getDisposedElements().size()]);
		if (disposed.length == 0) {
			return;
		}
		if (progress != null) {
			progress.beginTask("cleaning up deleted workflow elements",
					disposed.length);
		}
		try {
			for (IDisposableWithUndo disposable : disposed) {
				SubProgressMonitor sub = null;
				if (progress != null) {
					new SubProgressMonitor(progress, 1);
				}
				disposable.finalDispose(sub);
			}

		} finally {
			getDisposedElements().clear();
			if (progress != null) {
				progress.done();
			}
		}
	}

	public void createExecutionData() {
		executionData = new ExecutionData();
		String parentFolder = getParentFolder();
		String filename = getFilename();
		if (parentFolder != null && filename != null) {
			IPath myPath = Path.fromPortableString(parentFolder);
			myPath = myPath.append(filename);
			executionData.setWorkflowFilePath(myPath.toPortableString());
		}

	}

	public void createStartActivity() {
		StartActivity s = new StartActivity();
		setStartActivity(s);
		s.setDiagram(this);
		
		s.setLocation(new Point(120, WFConstants.INNER_DIAGRAM_PADDING.top));
		addChild(s);
		s.init();
	}

	@Override
	public void dispose() {
		if (getCommandStack() != null && commandStackListener != null) {
			getCommandStack().removeCommandStackListener(commandStackListener);
		}
		super.dispose();
	}

	public void doSaveAs(IPath newLocation, boolean recursive,
			boolean copyLocalInputFiles, boolean copyRemoteInputFiles,
			IProgressMonitor monitor) throws Exception {
		File targetDir = newLocation.toFile();
		if (targetDir.exists() && targetDir.isDirectory()
				&& targetDir.canWrite()) {
			File target = new File(targetDir, getFilename());
			beforeSerialization();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(target)));

			XStream xstream = XStreamConfigurator.getInstance()
					.getXStreamForSerialization();
			String s = xstream.toXML(diagram);
			afterSerialization();
			writer.write(s);
			writer.close();
		}
		getProject().refreshLocal(Integer.MAX_VALUE, null);
	}

	/**
	 * Returns an absolute path to the diagram file.
	 * 
	 * @return
	 */
	public IPath getAbsolutePath() {
		try {

			if (getParentFolder() == null
					|| getParentFolder().trim().length() == 0) {
				return getProject().getFile(getFilename()).getLocation();
			} else {
				IFolder parentFolder = getProject().getFolder(
						Path.fromPortableString(getParentFolder()));

				return parentFolder.getLocation().append(getFilename());
			}
		} catch (NullPointerException e) {
			return null;
		}
	}

	public Map<String, IActivity> getActivities() {
		return activities;
	}

	public IActivity getActivity(String id) {
		for (IActivity activity : activities.values()) {
			if (activity.getID().equals(id)) {
				return activity;
			}
		}
		return null;
	}

	public IActivity getActivityByName(String name) {
		return activities.get(name);
	}

	/**
	 * The activity diagram is used to access the command stack of the editor
	 * from within other activities. This way, commands can be called from
	 * within the model.
	 */
	@Override
	public CommandStack getCommandStack() {
		return commandStack;
	}

	public DiagramContext getContext() {
		return context;
	}

	protected Set<IDisposableWithUndo> getDisposedElements() {
		if (disposedElements == null) {
			disposedElements = new HashSet<IDisposableWithUndo>();
		}
		return disposedElements;
	}

	/**
	 * Get the information describing the workflow's execution state or null if
	 * the workflow has not been submitted yet.
	 * 
	 * @return
	 */
	public ExecutionData getExecutionData() {
		return executionData;
	}

	public String getFilename() {
		return filename;
	}

	public JAXBContext getJaxbContext() {
		return JAXBContextProvider.getDefaultContext();
	}

	public String getParentFolder() {
		return parentFolder;
	}

	public IProject getProject() {
		return project;
	}

	public Transition getTransitionByName(String name) {
		return getTransitions().get(name);
	}

	protected Map<String, Transition> getTransitions() {
		if (transitions == null) {
			transitions = new HashMap<String, Transition>();
		}
		return transitions;
	}

	@Override
	public QName getType() {
		return TYPE;
	}

	/**
	 * Returns an activity name that is unique within this diagram and starts
	 * with the given prefix. Finding a unique name is done by iteratively
	 * appending larger numbers to the prefix.
	 * 
	 * @param prefix
	 * @return
	 */
	public String getUniqueActivityName(String prefix) {
		return getUniqueElementName(prefix);
	}

	/**
	 * Returns a name that is unique for elements within this diagram and starts
	 * with the given prefix. Finding a unique name is done by iteratively
	 * appending larger numbers to the prefix.
	 * 
	 * @param prefix
	 * @return
	 */
	public String getUniqueElementName(String prefix) {
		try {
			int totalNumNames = getActivities().size()
					+ getTransitions().size();
			if (totalNumNames == 0) {
				return prefix + 1;
			}
			for (int number = 1; number <= totalNumNames + 1; number++) {
				String current = prefix + number;
				if (!getActivities().containsKey(current)
						&& !getTransitions().containsKey(current)) {
					return prefix + number;
				}
			}

		} catch (Exception e) {
			WFActivator.log(IStatus.ERROR,
					"Unable to determine a unique element name ", e);
		}
		return "no name";
	}

	public String getUniqueVariableName(String prefix) {
		try {
			Set<String> variableNames = new HashSet<String>();
			for (WorkflowVariable variable : getVariables().values()) {
				variableNames.add(variable.getName());
			}
			if (variableNames.size() == 0) {
				return prefix + 1;
			}
			for (int number = 1; number <= variableNames.size() + 1; number++) {
				if (!(variableNames.contains(prefix + number))) {
					return prefix + number;
				}
			}

		} catch (Exception e) {
			WFActivator.log(IStatus.ERROR,
					"Unable to determine a unique variable name ", e);
		}
		return "no name";
	}

	public WorkflowVariable getVariable(String name) {
		return getVariables().get(name);
	}

	public Map<String, WorkflowVariable> getVariables() {
		if (variables == null) {
			variables = new HashMap<String, WorkflowVariable>();
		}
		return variables;
	}

	/**
	 * This method returns true if the workflow has been submitted and has parts
	 * that are still being executed
	 * 
	 * @return
	 */
	public boolean hasRunningActivities() {
		if (getCurrentExecutionStateCipher() < ExecutionStateConstants.STATE_NOT_STARTED) {
			return false;
		} else if (getCurrentExecutionStateCipher() == ExecutionStateConstants.STATE_RUNNING) {
			return true;
		}

		for (Iterator<IActivity> iter = activities.values().iterator(); iter
				.hasNext();) {
			IActivity activity = iter.next();
			if (activity.getCurrentExecutionStateCipher() == ExecutionStateConstants.STATE_RUNNING) {
				return true;
			}
		}
		return false;
	}

	public boolean isDirty() {
		Boolean dirty = (Boolean) getPropertyValue(PROP_DIRTY);
		if (dirty == null) {
			return false;
		} else {
			return dirty;
		}
	}

	public boolean isEditable() {
		return getEditableStates().contains(getCurrentExecutionStateCipher());
	}

	public boolean isUniqueActivityName(String name) {
		return !getActivities().containsKey(name);

	}

	public boolean isUniqueElementName(String name) {
		return !getActivities().containsKey(name)
				&& !getTransitions().containsKey(name);
	}

	public boolean isUniqueVariableName(String name) {
		return !getVariables().containsKey(name);
	}

	public void loadExecutionData() {
		String executionFolderPath = getParentFolder();
		if (executionFolderPath == null
				|| executionFolderPath.trim().length() == 0) {
			return;
		}
		IFolder executionFolder = getProject().getFolder(
				Path.fromPortableString(executionFolderPath));
		IFile executionDataFile = executionFolder.getFile(WorkflowUtils
				.getExecutionDataFilename());
		if (!executionDataFile.exists()) {
			return;
		}
		try {
			Unmarshaller unmarshaller = getJaxbContext().createUnmarshaller();
			ExecutionData executionData = (ExecutionData) unmarshaller
					.unmarshal(executionDataFile.getContents());
			setExecutionData(executionData);
		} catch (Exception e) {
			WFActivator.log(IStatus.ERROR,
					"Error while loading workflow execution data", e);
		}

		// update activities visuals after restoring their states!
		for (Iterator<IActivity> iter = activities.values().iterator(); iter
				.hasNext();) {
			IActivity activity = iter.next();
			activity.setExecutionStateDescriptor(activity
					.getExecutionStateDescriptor());
			String iterationId = activity.getCurrentIterationId();
			if (!ExecutionStateConstants.DEFAULT_ITERATION_ID
					.equals(iterationId)) {
				activity.setCurrentIterationId(iterationId);
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource() != this) {

			if (PROP_NAME.equals(evt.getPropertyName())) {

				if (evt.getSource() instanceof IActivity) {
					IActivity act = (IActivity) evt.getSource();
					// the name of an activity has changed!
					activities.remove(evt.getOldValue());
					activities.put((String) evt.getNewValue(), act);
				} else if (evt.getSource() instanceof Transition) {
					Transition t = (Transition) evt.getSource();
					// the name of a transition has changed!
					transitions.remove(evt.getOldValue());
					transitions.put((String) evt.getNewValue(), t);
				}

			}
		}
		super.propertyChange(evt);
	}

	@Override
	public boolean readyForSubmission() {
		Boolean property = (Boolean) getPropertyValue(PROP_READY_FOR_SUBMISSION);
		return property == null ? true : property;
	}

	public void removeActivity(IActivity activity) {
		if (activity == null) {
			return;
		}
		activity.removePersistentPropertyChangeListener(this);
		activities.remove(activity.getName());
	}

	public void removeDisposedElement(IDisposableWithUndo disposable) {
		getDisposedElements().remove(disposable);
	}

	public void removeTransition(Transition transition) {
		if (transition == null) {
			return;
		}
		transition.removePersistentPropertyChangeListener(this);
		transitions.remove(transition.getName());
	}

	public void removeVariable(WorkflowVariable variable) {
		if (variable == null) {
			return;
		}
		getVariables().remove(variable.getName());
	}

	public void resetExecutionData() {
		setExecutionData(null);
		parentFolder = null;

	}

	public void saveExecutionData() {
		if (getExecutionData() != null) {
			if (!getProject().exists()) {
				return; // this may happen a few times when the project is
						// renamed
			}
			synchronized (getExecutionData()) {
				IFolder executionFolder = getProject().getFolder(
						Path.fromPortableString(getParentFolder()));
				if (executionFolder != null) {
					if (!executionFolder.exists()) {
						try {
							executionFolder.create(true, true, null);
						} catch (CoreException e) {
							WFActivator
									.log(IStatus.ERROR,
											"Unable to create folder for workflow execution data.",
											e);
						}
					}

					String filename = getFilename();
					if (filename != null) {
						IPath myPath = executionFolder.getLocation().append(
								filename);
						executionData.setWorkflowFilePath(myPath
								.toPortableString());
					}

					IFile executionDataFile = executionFolder
							.getFile(WorkflowUtils.getExecutionDataFilename());
					try {

						Marshaller marshaller = getJaxbContext()
								.createMarshaller();
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						marshaller.marshal(getExecutionData(), out);
						String s = out.toString();
						if (executionDataFile.exists()) {
							executionDataFile.delete(true, null);
						}
						executionDataFile.create(
								new ByteArrayInputStream(s.getBytes()), true,
								null);

					} catch (Exception e) {
						WFActivator
								.log(IStatus.ERROR,
										"Error while saving workflow execution data",
										e);
					}
				}
			}
		}

	}

	@Override
	public void setCommandStack(CommandStack commandStack) {

		if (commandStackListener == null) {
			commandStackListener = new CommandStackListener() {
				public void commandStackChanged(EventObject event) {
					checkReadyForSubmission();
				}
			};
		} else if (this.commandStack != null) {
			this.commandStack.removeCommandStackListener(commandStackListener);
		}
		// whenever a command is executed: check whether the diagram is still
		// ready for submission
		if (commandStack != null) {
			commandStack.addCommandStackListener(commandStackListener);
		}
		super.setCommandStack(commandStack);
	}

	public void setContext(DiagramContext context) {
		this.context = context;
	}

	/**
	 * Allow other objects to set a dirty flag on the diagram even without
	 * executing a command.
	 * 
	 * @param dirty
	 */
	public void setDirty(boolean dirty) {
		setPropertyValue(PROP_DIRTY, dirty);
	}

	public void setExecutionData(ExecutionData executionData) {
		this.executionData = executionData;
		checkReadyForSubmission();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.IActivity#setFinished()
	 */
	@Override
	public void setExecutionStateDescriptor(
			ExecutionStateDescriptor stateDescriptor) {
		super.setExecutionStateDescriptor(stateDescriptor);
		if (getCurrentExecutionState() == null) {
			return;
		}
		int state = getCurrentExecutionState().getCipher();
		if (getStartActivity() != null) {
			if (state == ExecutionStateConstants.STATE_SUBMITTING) {
				getStartActivity().changeCurrentExecutionState(state,
						"submitting");
			} else if (state == ExecutionStateConstants.STATE_FAILED) {
				String descr = getCurrentExecutionState()
						.getDetailedDescription();
				int startExecutionCipher = getStartActivity()
						.getCurrentExecutionStateCipher();
				if (startExecutionCipher != ExecutionStateConstants.STATE_FAILED
						&& startExecutionCipher != ExecutionStateConstants.STATE_SUCCESSFUL) {
					descr = "failed during submission";
				}
				getStartActivity().changeCurrentExecutionState(state, descr);
			} else if (state == ExecutionStateConstants.STATE_RUNNING) {
				getStartActivity().changeCurrentExecutionState(
						ExecutionStateConstants.STATE_SUCCESSFUL,
						"workflow started");
			}
		}

	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public void setParentFolder(String parentFolder) {
		this.parentFolder = parentFolder;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	@Override
	public void setPropertyValue(Object propName, Object val) {
		super.setPropertyValue(propName, val);
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		if (updatedToCurrentModelVersion) {
			return; // don't do this twice!
		}
		super.updateToCurrentModelVersion(oldVersion, currentVersion);
		if ("6.4.0".compareTo(oldVersion) > 0) {
			// in former times, IDs were used to store activities, now we use
			// human readable names, which must be unique anyway
			Map<String, IActivity> activities = new HashMap<String, IActivity>();
			for (IActivity act : getActivities().values()) {
				activities.put(act.getName(), act);
				for (Transition t : act.getOutgoingTransitions()) {
					if (t.getName() == null || t.getName().trim().length() == 0) {
						t.setName(getUniqueElementName(Transition.DEFAULT_TRANSITION_NAME));
					}
				}

			}
			this.activities = activities;
		}

		// handle transitions and data flows after activities:
		// they connect different activities and might rely on their updated
		// properties
		Set<Transition> transitions = new HashSet<Transition>();
		Set<IDataFlow> dataFlows = new HashSet<IDataFlow>();
		for (IActivity act : getActivities().values()) {
			for (Transition t : act.getOutgoingTransitions()) {
				transitions.add(t);
			}
			for (IDataSource source : act.getDataSourceList().getDataSources()) {
				for (IDataFlow flow : source.getOutgoingFlows()) {
					dataFlows.add(flow);
				}
			}
		}
		for (Transition t : transitions) {
			t.updateToCurrentModelVersion(oldVersion, currentVersion);
		}

		for (IDataFlow f : dataFlows) {
			f.updateToCurrentModelVersion(oldVersion, currentVersion);
		}

	}

	public static Set<Integer> getEditableStates() {
		if (editableStates == null) {
			editableStates = new HashSet<Integer>();
			editableStates.add(ExecutionStateConstants.STATE_UNSUBMITTED);
		}
		return editableStates;
	}

	public static void setEditableStates(Set<Integer> editableStates) {
		WorkflowDiagram.editableStates = editableStates;
	}
}
