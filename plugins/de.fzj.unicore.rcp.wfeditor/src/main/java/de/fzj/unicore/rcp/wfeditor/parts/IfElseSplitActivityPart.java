package de.fzj.unicore.rcp.wfeditor.parts;

import org.eclipse.jface.resource.ImageDescriptor;

import de.fzj.unicore.rcp.wfeditor.WFActivator;

public class IfElseSplitActivityPart extends SimpleActivityPart {

	@Override
	public ImageDescriptor getImageDescriptor() {
		return WFActivator.getImageDescriptor("if.png");
	}

	

}
