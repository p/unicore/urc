/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.conditions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.extensions.ConditionTypeTimeComparisonExtension;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

/**
 * @author demuth
 * 
 */
@XStreamAlias("ConditionTypeTimeComparison")
public class ConditionTypeTimeComparison implements IConditionType {

	public static final String TYPE_NAME = "Current time";

	public static final String[] TIME_COMPARATORS = new String[] { "before",
			"after" };

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm");
	private int comparator = 0;
	/**
	 * The value to which the current time is compared when evaluating the
	 * condition
	 */
	private Long value = System.currentTimeMillis() + 1000 * 60 * 60 * 24; // set
																			// this
																			// to
																			// now
																			// +
																			// 1
																			// day
	private Condition condition;
	private transient boolean disposed = false;

	public ConditionTypeTimeComparison() {

	}

	@Override
	public ConditionTypeTimeComparison clone()
			throws CloneNotSupportedException {
		ConditionTypeTimeComparison clone = (ConditionTypeTimeComparison) super
				.clone();
		return clone;
	}

	public void dispose() {
		disposed = true;
	}

	public void finalDispose(IProgressMonitor progress) {
		// do nothing
	}

	public int getComparator() {
		return comparator;
	}

	public Condition getCondition() {
		return condition;
	}

	public IConditionType getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		ConditionTypeTimeComparison result;
		try {
			result = (ConditionTypeTimeComparison) super.clone();
		} catch (CloneNotSupportedException e) {
			WFActivator.log(IStatus.ERROR, "Unable to copy condition of type '"
					+ TYPE_NAME + "'", e);
			return null;
		}
		Condition newCondition = CloneUtils.getFromMapOrCopy(condition,
				toBeCopied, mapOfCopies, copyFlags);
		result.setCondition(newCondition);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType#
	 * getDisplayedExpression()
	 */
	public String getDisplayedExpression() {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(getValue());
		return "current time is " + TIME_COMPARATORS[comparator] + "\n"
				+ Constants.getDefaultDateFormat().format(c.getTime());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType#getExtensionId
	 * ()
	 */
	public String getExtensionId() {
		return ConditionTypeTimeComparisonExtension.ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType#
	 * getInternalExpression()
	 */
	public String getInternalExpression() throws Exception {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(getValue());
		return TIME_COMPARATORS[getComparator()] + "(\""
				+ DATE_FORMAT.format(c.getTime()) + "\")";

	}

	public String getTypeName() {
		return TYPE_NAME;
	}

	public Long getValue() {
		return value;
	}

	public Set<Class<?>> insertAfter() {
		return new HashSet<Class<?>>(); // we don't care
	}

	public Set<Class<?>> insertBefore() {
		return new HashSet<Class<?>>(); // we don't care
	}

	public void insertCopyIntoDiagram(ICopyable copy,
			StructuredActivity parent, Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
	}

	public boolean isDisposable() {
		return true;
	}

	public boolean isDisposed() {
		return disposed;
	}

	public void setComparator(int comparator) {
		this.comparator = comparator;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	public void setValue(long value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return getDisplayedExpression();
	}

	public void undoDispose() {
		disposed = false;

	}

}
