/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.actions;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.UnicoreCommonActivator;
import de.fzj.unicore.rcp.common.actions.UnicoreCommonActionConstants;
import de.fzj.unicore.rcp.common.guicomponents.SetRelativeTerminationTimeDialog;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;

/**
 * @author demuth
 * 
 */
public class SetTerminationTimeAction extends WFEditorAction {

	public static String ID = UnicoreCommonActionConstants.ACTION_SET_TERMINATION_TIME;
	public static ImageDescriptor IMAGE = UnicoreCommonActivator
			.getImageDescriptor("clock_edit.png");
	public static String TEXT = "Set Termination Time";

	private WFEditor editor;

	public SetTerminationTimeAction(IWorkbenchPart part) {
		super(part);
		this.editor = (WFEditor) part;
		setImageDescriptor(IMAGE);
		setId(ID);
		setText(TEXT);
		setToolTipText("Set the time at which the submitted workflow will be disposed of by the server.");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	@Override
	protected boolean calculateEnabled() {
		return getWorkflowDiagram().getCurrentExecutionStateCipher() == ExecutionStateConstants.STATE_UNSUBMITTED;
	}


	protected WorkflowDiagram getWorkflowDiagram() {
		return editor.getDiagram();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.actions.WFEditorAction#isAddedToContextMenu()
	 */
	@Override
	public boolean isAddedToContextMenu() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.actions.WFEditorAction#run()
	 */
	@Override
	public void run() {
	
		Integer oldTT = (Integer) getWorkflowDiagram().getPropertyValue(
				WorkflowDiagram.PROP_TERMINATION_TIME);
		if (oldTT == null) {
			oldTT = Constants.DEFAULT_TERMINATION_TIME;
		}
		Shell s = null;
		try {
			s = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		} catch (Exception e) {

		}
		if (s == null) {
			s = new Shell();
		}
		SetRelativeTerminationTimeDialog d = new SetRelativeTerminationTimeDialog(
				s, "Set Termination Time",
				"Enter the lifetime interval for your workflow:", oldTT);
		d.open();
		if (d.getReturnCode() == Window.OK) {
			int terminationTime = d.getTerminationTime();
			getWorkflowDiagram().setPropertyValue(
					WorkflowDiagram.PROP_TERMINATION_TIME, terminationTime);
			getWorkflowDiagram().setDirty(true);
		}

	}

}
