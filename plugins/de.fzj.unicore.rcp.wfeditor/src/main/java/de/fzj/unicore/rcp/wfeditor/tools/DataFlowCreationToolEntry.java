package de.fzj.unicore.rcp.wfeditor.tools;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.requests.CreationFactory;
import org.eclipse.jface.resource.ImageDescriptor;

public class DataFlowCreationToolEntry extends ConnectionCreationToolEntry {

	protected DataFlowCreationTool tool;

	public DataFlowCreationToolEntry(String label, String shortDesc,
			CreationFactory factory, ImageDescriptor iconSmall,
			ImageDescriptor iconLarge) {
		super(label, shortDesc, factory, iconSmall, iconLarge);
		setToolClass(DataFlowCreationTool.class);
	}

	@Override
	public Tool createTool() {
		tool = (DataFlowCreationTool) super.createTool();
		return tool;
	}

	@Override
	public void setToolProperty(Object key, Object value) {
		super.setToolProperty(key, value);
		if (tool != null) {
			tool.setProperties(getToolProperties());
		}
	}
}
