package de.fzj.unicore.rcp.wfeditor.ui;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.FreeformViewport;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MouseListener;
import org.eclipse.draw2d.PopUpHelper;
import org.eclipse.draw2d.ScrollPane;
import org.eclipse.draw2d.TitleBarBorder;
import org.eclipse.draw2d.ToolTipHelper;
import org.eclipse.draw2d.Viewport;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Control;

import de.fzj.unicore.rcp.wfeditor.figures.IComplexToolTip;

/**
 * We use our own tooltip helper in order to customize where and how long to
 * display tooltips.
 * 
 * @author bdemuth
 * 
 */
public class WFTooltipHelper extends ToolTipHelper {

	// private Timer timer;
	private IFigure currentTipSource;

	private IFigure currentTip;
	private IComplexToolTip complexToolTip;

	protected boolean tipShowing;
	private IFigure tooltipLayer = null;
	private EditPartViewer viewer;

	private boolean focusedOnTooltip;

	private MouseListener mouseListener;
	private KeyListener keyListener;
	private ScrollPane toolTipScrollPane;

	public WFTooltipHelper(Control c, ScalableFreeformRootEditPart root) {
		super(c);
		tooltipLayer = root.getLayer(WFRootEditPart.TOOLTIP_LAYER);
		viewer = root.getViewer();
		hookListeners(root);
	}

	protected void hookListeners(final GraphicalEditPart root)
	{
		
		mouseListener = new MouseListener() {

			public void mousePressed(org.eclipse.draw2d.MouseEvent me) {
				org.eclipse.draw2d.geometry.Point mouseLoc = me.getLocation().getCopy(); 
				if(isFocusedOnTooltip())
				{
					toolTipScrollPane.translateToRelative(mouseLoc);
					if(!toolTipScrollPane.containsPoint(mouseLoc))
					{
						setFocusedOnTooltip(false);
						hide();
					}
				}
				else
				{
					if(isShowing())
					{

						currentTip.translateToRelative(mouseLoc);
						boolean overTooltip = currentTip.containsPoint(mouseLoc);
						if(overTooltip)
						{
							setFocusedOnTooltip(true);
						}
					}
				}
			}

			public void mouseReleased(org.eclipse.draw2d.MouseEvent me) {

			}

			public void mouseDoubleClicked(org.eclipse.draw2d.MouseEvent me) {

			}
		};
		root.getFigure().addMouseListener(mouseListener);

		keyListener = new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent ke) {
				if(SWT.TAB == ke.character || SWT.CR == ke.character)
				{
					if(!isFocusedOnTooltip()) setFocusedOnTooltip(true);
				}
				else if(SWT.ESC == ke.character)
				{
					if(isFocusedOnTooltip())
					{
						setFocusedOnTooltip(false);
						hide();
					}
				}
			}


		};
		viewer.getControl().addKeyListener(keyListener);

	}
	
	protected void unhookListeners(GraphicalEditPart root)
	{
		Control control = viewer.getControl();
		if(control != null && !control.isDisposed())
		{
			
			if(mouseListener != null)
			{
				root.getFigure().removeMouseListener(mouseListener);
				mouseListener = null;
			}
			if(keyListener != null)
			{
				control.removeKeyListener(keyListener);
				keyListener = null;
			}
			
		}
	}

	/**
	 * Sets the LightWeightSystem's contents to the passed tooltip, and displays
	 * the tip. The tip will be displayed only if the tip source is different
	 * than the previously viewed tip source. (i.e. The cursor has moved off of
	 * the previous tooltip source figure.)
	 * <p>
	 * The tooltip will be painted directly below the cursor if possible,
	 * otherwise it will be painted directly above cursor.
	 * 
	 * @param hoverSource
	 *            the figure over which the hover event was fired
	 * @param tip
	 *            the tooltip to be displayed
	 * @param eventX
	 *            the x coordinate of the hover event
	 * @param eventY
	 *            the y coordinate of the hover event
	 * @since 2.0
	 */
	@Override
	public void displayToolTipNear(IFigure hoverSource, IFigure tip,
			int eventX, int eventY) {
		if(isFocusedOnTooltip()) 
		{
			// the user has focused on the tooltip and is scrolling it
			return;
		}

		if (tip != null && hoverSource != currentTipSource) {
			currentTipSource = hoverSource;
			currentTip = tip;

			if (tip instanceof IComplexToolTip) {
				complexToolTip = (IComplexToolTip) tip;
				org.eclipse.draw2d.geometry.Point relative = translateToFigure(
						control, tip, eventX, eventY);
				boolean showTooltip = complexToolTip.createTooltip(viewer,
						tooltipLayer, hoverSource, relative.x, relative.y);
				if (showTooltip) {
					show();
				} else {
					hide();
				}

			} else {

				show();
				Rectangle constraints = computeToolTipLocation(control,
						tooltipLayer, hoverSource, tip, eventX, eventY);

				tooltipLayer.setConstraint(tip, constraints);
			}

		}
	}

	/**
	 * Disposes of the tooltip's shell and kills the timer.
	 * 
	 * @see PopUpHelper#dispose()
	 */
	@Override
	public void dispose() {
		unhookListeners((GraphicalEditPart) viewer.getRootEditPart());
		if (isShowing()) {
			// timer.cancel();
			hide();
		}

		getShell().dispose();
	}

	public IFigure getTooltipLayer() {
		return tooltipLayer;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void hide() {
		if (currentTip instanceof IComplexToolTip) {
			IComplexToolTip complexToolTip = (IComplexToolTip) currentTip;
			complexToolTip.hide(tooltipLayer, currentTipSource);
		}
		currentTip = null;
		currentTipSource = null;
		complexToolTip = null;
		tooltipLayer.setVisible(false);
		// make sure all tooltips are cleaned up
		for (Object o : tooltipLayer.getChildren().toArray(
				new IFigure[tooltipLayer.getChildren().size()])) {
			IFigure fig = (IFigure) o;
			tooltipLayer.remove(fig);
		}

		tipShowing = false;

	}

	/**
	 * @see PopUpHelper#hookShellListeners()
	 */
	@Override
	protected void hookShellListeners() {

	}

	@Override
	public boolean isShowing() {
		return tipShowing;
	}

	@Override
	protected void show() {
		tooltipLayer.setVisible(true);
		tipShowing = true;
		tooltipLayer.add(currentTip);

	}

	//	private Rectangle translateToDisplay(IFigure figure, Rectangle r) {
	//		figure.translateToAbsolute(r);
	//		Point loc = control.toDisplay(r.x, r.y);
	//		org.eclipse.draw2d.geometry.Point bottomRight = r.getBottomRight();
	//		Point bottomRightInDisplay = control.toDisplay(bottomRight.x,
	//				bottomRight.y);
	//		return new Rectangle(loc.x, loc.y, bottomRightInDisplay.x - loc.x,
	//				bottomRightInDisplay.y - loc.y);
	//	}

	/**
	 * Displays the hover source's tooltip if a tooltip of another source is
	 * currently being displayed.
	 * 
	 * @param figureUnderMouse
	 *            the figure over which the cursor was when called
	 * @param tip
	 *            the tooltip to be displayed
	 * @param eventX
	 *            the x coordinate of the cursor
	 * @param eventY
	 *            the y coordinate of the cursor
	 * @since 2.0
	 */
	@Override
	public void updateToolTip(IFigure figureUnderMouse, IFigure tip,
			int eventX, int eventY) {
		if(isFocusedOnTooltip()) return;
		/*
		 * If the cursor is not on any Figures, it has been moved off of the
		 * control.
		 */
		if (figureUnderMouse == null) {
			if (isShowing()) {

				if (complexToolTip != null) {
					org.eclipse.draw2d.geometry.Point relative = translateToFigure(
							control, currentTip, eventX, eventY);
					boolean showTooltip = complexToolTip.refreshTooltip(
							tooltipLayer, currentTipSource, relative.x,
							relative.y);
					if (!showTooltip) {
						hide();
					}

				}
				// timer.cancel();
			}
		}
		// Makes tooltip appear without a hover event if a tip is currently
		// being displayed
		if (isShowing() && figureUnderMouse != currentTipSource) {
			if (complexToolTip != null) {
				org.eclipse.draw2d.geometry.Point relative = translateToFigure(
						control, currentTip, eventX, eventY);
				if (!complexToolTip.refreshTooltip(tooltipLayer,
						currentTipSource, relative.x, relative.y)) {
					hide();
					// displayToolTipNear(figureUnderMouse, tip, eventX,
					// eventY);
				}

			} else {
				hide();
				displayToolTipNear(figureUnderMouse, tip, eventX, eventY);
			}

		} else if (!isShowing() && figureUnderMouse != currentTipSource) {
			currentTipSource = null;
		}
	}

	/**
	 * Similar to
	 * {@link #computeToolTipLocation(IFigure, IFigure, IFigure, int, int)} but
	 * this method assumes that the mouse loction needs to be translated to the
	 * parent control first.
	 * 
	 * @param control
	 * @param tooltipLayer
	 * @param tooltipSource
	 * @param tooltip
	 * @param mouseX
	 * @param mouseY
	 * @return
	 */
	public static Rectangle computeToolTipLocation(Control control,
			IFigure tooltipLayer, IFigure tooltipSource, IFigure tooltip,
			int mouseX, int mouseY) {
		Point p = control.toControl(mouseX, mouseY);
		return computeToolTipLocation(tooltipLayer, tooltipSource, tooltip,
				p.x, p.y);
	}

	/**
	 * Computes the location of a tooltip that should be displayed inside the
	 * tooltip layer figure. The location is relative to the tooltip layer. The
	 * result is meant to be used as a constraint with
	 * {@link IFigure#setConstraint(IFigure, Object)}, called on the tooltip
	 * layer figure.
	 * 
	 * @param tooltipLayer
	 * @param tooltipSource
	 * @param tooltip
	 * @param mouseX
	 * @param mouseY
	 * @return
	 */
	public static Rectangle computeToolTipLocation(IFigure tooltipLayer,
			IFigure tooltipSource, IFigure tooltip, int mouseX, int mouseY) {
		org.eclipse.draw2d.geometry.Point displayPoint = new org.eclipse.draw2d.geometry.Point(
				mouseX + 25, mouseY);
		Dimension dim = tooltip.getPreferredSize().getCopy();
		tooltip.translateToAbsolute(dim);
		Rectangle inner = new Rectangle(displayPoint, dim);
		IFigure parent = getTopParent(tooltipLayer);
		Rectangle outer = parent.getBounds().getCopy();
		parent.translateToAbsolute(outer);
		inner = computeToolTipLocation(outer, inner);

		tooltipLayer.translateToRelative(inner);
		return inner;
	}

	protected static IFigure getTopParent(IFigure f)
	{
		IFigure parent = f.getParent();
		while (parent.getParent() != null
				&& !(parent instanceof FreeformViewport)) {
			parent = parent.getParent();
		}
		return parent;
	}

	public static Rectangle computeToolTipLocation(Rectangle outer,
			Rectangle inner) {
		inner = inner.getCopy();

		if(inner.width > outer.width/1.5) inner.width = (int) (outer.width/1.5);
		if(inner.height > outer.height/1.5) inner.height = (int) (outer.height/1.5);

		// in case the inner rectangle does not fit inside the outer rectangle:
		// move it
		int rightDist = outer.x + outer.width - (inner.x + inner.width);
		if (rightDist < 0) {
			inner.x += rightDist;
		}
		int downDist = outer.y + outer.height - (inner.y + inner.height);
		if (downDist < 0) {
			inner.y += downDist;
		}
		int leftDist = inner.x - outer.x;
		if (leftDist < 0) {
			inner.x -= leftDist;
		}
		int upDist = inner.y - outer.y;
		if (upDist < 0) {
			inner.y -= upDist;
		}
		return inner;
	}

	private static org.eclipse.draw2d.geometry.Point translateToFigure(
			Control control, IFigure fig, int x, int y) {
		Point p = control.toControl(x, y);
		return translateToFigure(fig, p.x, p.y);
	}

	private static org.eclipse.draw2d.geometry.Point translateToFigure(
			IFigure fig, int x, int y) {
		org.eclipse.draw2d.geometry.Point result = new org.eclipse.draw2d.geometry.Point(
				x, y);

		fig.translateToRelative(result);
		return result;
	}

	protected boolean isFocusedOnTooltip() {
		return focusedOnTooltip;
	}

	protected void setFocusedOnTooltip(boolean focusedOnTooltip) {
		boolean old = this.focusedOnTooltip;
		if(old == focusedOnTooltip) return;
		if(!isShowing()) return;

		if(focusedOnTooltip)
		{
			// don't do this for complex tooltips!
			boolean doFocus = complexToolTip == null;


			if(doFocus)
			{

				toolTipScrollPane = new ScrollPane();
				Border b = new TitleBarBorder("Viewing tooltip - press Esc to exit");
				toolTipScrollPane.setBorder(b);

				tooltipLayer.remove(currentTip);

				toolTipScrollPane.setViewport(new Viewport(true));
				toolTipScrollPane.setContents(currentTip);
				tooltipLayer.add(toolTipScrollPane);

				Dimension borderSize = b.getPreferredSize(toolTipScrollPane);
				Rectangle r = currentTip.getBounds();
				toolTipScrollPane.setPreferredSize(r.width,r.height);
				r.width = Math.max(r.width, borderSize.width);
				r.height = Math.max(r.height, borderSize.height);
				r.expand(6, 6);
				toolTipScrollPane.setBounds(r);
			}
		}
		else if(toolTipScrollPane != null)
		{
			toolTipScrollPane.removeAll();
			tooltipLayer.remove(toolTipScrollPane);
			tooltipLayer.add(currentTip);
			toolTipScrollPane = null;
		}

		this.focusedOnTooltip = toolTipScrollPane != null;
	}

}
