/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.parts.ActivityPart;

public class ShowDataSinksAndSourcesEditPolicy extends AbstractEditPolicy {

	private ActivityPart part;

	public ShowDataSinksAndSourcesEditPolicy(ActivityPart part) {
		super();
		this.part = part;
	}

	@Override
	public Command getCommand(Request request) {
		// if(WFConstants.REQ_SHOW_DATA_SINKS_AND_SOURCES.equals(request.getType()))
		// {
		// ShowDataSinksAndSourcesRequest req = (ShowDataSinksAndSourcesRequest)
		// request;
		// boolean show = req.shouldShow();
		// CompoundCommand result = new CompoundCommand();
		// result.add(new ShowDataSinksCommand(part.getActivity(), show));
		// result.add(new ShowDataSourcesCommand(part.getActivity(), show));
		//
		// if(req.getPartInCenter() == getHost())
		// {
		// result.add(new ShowIncomingDataFlowsCommand(part.getActivity(),
		// show));
		// result.add(new ShowOutgoingDataFlowsCommand(part.getActivity(),
		// show));
		// // also show all connected data flows
		// for(IDataSource source :
		// part.getActivity().getDataSourceList().getDataSources())
		// {
		// for(IDataFlow flow : source.getFlows())
		// {
		// Map<Object,EditPart> partRegistry =
		// getHost().getViewer().getEditPartRegistry();
		// EditPart sinkPart = partRegistry.get(flow.getDataSink());
		// Command cmd = sinkPart.getCommand(request);
		// if(cmd != null) result.add(cmd);
		// }
		// }
		//
		// for(IDataSink sink :
		// part.getActivity().getDataSinkList().getDataSinks())
		// {
		// for(IDataFlow flow : sink.getFlows())
		// {
		// Map<Object,EditPart> partRegistry =
		// getHost().getViewer().getEditPartRegistry();
		// EditPart sourcePart = partRegistry.get(flow.getDataSource());
		// Command cmd = sourcePart.getCommand(request);
		// if(cmd != null) result.add(cmd);
		// }
		// }
		// }
		// return result;
		// }
		return null;
	}

	@Override
	public EditPart getTargetEditPart(Request request) {
		return getHost();
	}

	@Override
	public boolean understandsRequest(Request request) {
		return WFConstants.REQ_SHOW_DATA_SINKS_AND_SOURCES.equals(request
				.getType());
	}

}
