package de.fzj.unicore.rcp.wfeditor.extensions;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridData;

import de.fzj.unicore.rcp.common.utils.ValidityChangeEvent;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.ui.IConditionChangeListener;
import de.fzj.unicore.rcp.wfeditor.ui.ValueChangeEvent;

public class ConditionTypeFreeFormControl extends CommonConditionTypeControl {
	private Text conditionText;
	private ConditionTypeFreeForm freeForm;

	public ConditionTypeFreeFormControl(Composite parent, Condition condition) {
		super(parent, SWT.NONE);
		setLayout(new GridLayout(3, false));
		
		Label lblCondition = new Label(this, SWT.NONE);
		lblCondition.setText("Condition:");
		
		conditionText = new Text(this, SWT.BORDER);
		conditionText.setToolTipText("See UNICORE Workflow System manual for help:\nhttp://www.unicore.eu/documentation/manuals/unicore6/files/workflow/workflow-manual.html#_transitions_and_conditions");
		conditionText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		this.setBackground(parent.getBackground());
		lblCondition.setBackground(parent.getBackground());
		conditionText.setBackground(parent.getBackground());
		
		errorMsg = new Label(this, SWT.NONE);
		GridData data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);
		data.horizontalSpan = 4;
		errorMsg.setLayoutData(data);
		errorMsg.setForeground(parent.getDisplay()
				.getSystemColor(SWT.COLOR_RED));
		errorMsg.setBackground(getBackground());
		controls.add(errorMsg);
		
		initialize(condition);
	}

	private void initialize(Condition condition) {
		freeForm = (ConditionTypeFreeForm) condition.getSelectedType();
		freeForm.setCondition(condition);
		try {
			conditionText.setText(condition.getInternalExpression());
		} catch (Exception e) {
			conditionText.setText(condition.getDisplayedExpression());
		}
		
		conditionText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent ev) {
				boolean oldValidity = isValid();
				String s = ((Text) ev.getSource()).getText();
				try {
					freeForm.setInternalExpression(s);
					valueEntered = true;
					errorMsg.setText("");
				} catch (Exception e) {
					valueEntered = false;
					errorMsg.setText("Unable to use this free form string as a condition.");
					errorMsg.pack();
					pack();
				}
				boolean newValidity = isValid();
				if(oldValidity != newValidity) {
					fireValidityEvent(newValidity);
				}
				fireValueChange();
			}

		});
		
	}
	
	private void fireValueChange() {
		ValueChangeEvent ev = new ValueChangeEvent(freeForm.getCondition());
		for (IConditionChangeListener l : listeners) {
			l.valueChanged(ev);
		}
	}

	private void fireValidityEvent(boolean newValidity) {
		ValidityChangeEvent ev = new ValidityChangeEvent(this, newValidity);
		for(IConditionChangeListener l : listeners) {
			l.validityChanged(ev);
		}
	}
	

	
	
}
