/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.fzj.unicore.rcp.logmonitor.LogActivator;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.IModelConverterExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.monitoring.MonitorerRegistry;
import de.fzj.unicore.rcp.wfeditor.submission.ConverterRegistry;
import de.fzj.unicore.rcp.wfeditor.submission.IConverter;
import de.fzj.unicore.rcp.wfeditor.submission.SubmitterRegistry;

/**
 * Plugin class used by the flow editor.
 * 
 * @author Bastian Demuth, Valentina Huber
 */
public class WFActivator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "de.fzj.unicore.rcp.wfeditor";

	// path to icons
	public final static String ICONS_PATH = "$nl$/icons/";//$NON-NLS-1$

	// shared plugin instance returned by "getDefault()"
	private static WFActivator plugin;

	private SubmitterRegistry submitterRegistry;

	private ConverterRegistry converterRegistry;

	private MonitorerRegistry monitorerRegistry;

	private DiagramTypeRegistry diagramTypeRegistry;

	/**
	 * Creates a new WFActivator with the given descriptor
	 * 
	 * @param descriptor
	 *            the descriptor
	 */
	public WFActivator() {
		super();
		plugin = this;
		converterRegistry = new ConverterRegistry();
		monitorerRegistry = new MonitorerRegistry();
		diagramTypeRegistry = new DiagramTypeRegistry();
	}

	public ConverterRegistry getConverterRegistry() {
		return converterRegistry;
	}

	public DiagramTypeRegistry getDiagramTypeRegistry() {
		return diagramTypeRegistry;
	}

	public MonitorerRegistry getMonitorerRegistry() {
		return monitorerRegistry;
	}

	public SubmitterRegistry getSubmitterRegistry() {
		if (submitterRegistry == null) {
			submitterRegistry = new SubmitterRegistry();
		}
		return submitterRegistry;
	}

	private void registerConverters() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(WFConstants.EXTENSION_POINT_MODEL_CONVERTER);
		IConfigurationElement[] members = extensionPoint
				.getConfigurationElements();
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			try {
				IModelConverterExtensionPoint ext = (IModelConverterExtensionPoint) member
						.createExecutableExtension("name");
				List<IConverter> converters = ext.getConverters();
				for (IConverter converter : converters) {
					getConverterRegistry().registerConverter(converter);
				}

			} catch (CoreException ex) {
				WFActivator
						.log("Could not determine all available actions for service",
								ex);
			}
		}
	}

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		getPreferenceStore().setDefault(WFConstants.PREF_PALETTE_SIZE,
				WFConstants.DEFAULT_PALETTE_SIZE);
		registerConverters();

	}

	/**
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#initializeImageRegistry(org.eclipse.jface.resource.ImageRegistry)
	 */
	@Override
	protected void initializeImageRegistry(ImageRegistry reg) {
		reg.put(WFConstants.IMAGE_COLLAPSE,
				getImageDescriptor(WFConstants.IMAGE_COLLAPSE));
		reg.put(WFConstants.IMAGE_EXPAND,
				getImageDescriptor(WFConstants.IMAGE_EXPAND));
		reg.put(WFConstants.IMAGE_ENABLED_BREAKPOINT,
				getImageDescriptor(WFConstants.IMAGE_ENABLED_BREAKPOINT));
		reg.put(WFConstants.IMAGE_DISABLED_BREAKPOINT,
				getImageDescriptor(WFConstants.IMAGE_DISABLED_BREAKPOINT));
		reg.put(WFConstants.IMAGE_WORFKLOW_FILE,
				getImageDescriptor(WFConstants.IMAGE_WORFKLOW_FILE));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static WFActivator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, ICONS_PATH + path);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 */
	public static void log(int status, String msg) {
		log(status, msg, null, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param forceAlarmUser
	 *            forces the logging plugin to open a popup; usually, this
	 *            should NOT be set to true!
	 */
	public static void log(int status, String msg, boolean forceAlarmUser) {
		log(status, msg, null, forceAlarmUser);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(int status, String msg, Exception e) {
		log(status, msg, e, false);
	}

	/**
	 * Log a message via Eclipse logging
	 * 
	 * @param status
	 *            the IStatus level to log to
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 * @param forceAlarmUser
	 *            forces the logging plugin to open a popup; usually, this
	 *            should NOT be set to true!
	 */
	public static void log(int status, String msg, Exception e,
			boolean forceAlarmUser) {
		IStatus s = new Status(status, PLUGIN_ID, IStatus.OK, msg, e);
		LogActivator.log(plugin, s, forceAlarmUser);
	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 */
	public static void log(String msg) {
		log(msg, null);
	}

	/**
	 * Log a message via Eclipse logging Defaults to INFO Level
	 * 
	 * @param msg
	 *            the message to log
	 * @param e
	 *            stacktrace to append
	 */
	public static void log(String msg, Exception e) {
		log(IStatus.INFO, msg, e, false);
	}

}
