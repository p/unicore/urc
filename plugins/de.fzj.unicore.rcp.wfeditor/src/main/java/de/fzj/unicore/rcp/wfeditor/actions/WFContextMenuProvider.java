/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.ui.IWorkbenchActionConstants;

/**
 * Provides a context menu for the workflow editor.
 * 
 * @author Daniel Lee, demuth
 */
public class WFContextMenuProvider extends ContextMenuProvider {

	// the name of the domain for which a workflow editor context menu is
	// supposed to be built
	private QName contentDomain;

	private ActionRegistry actionRegistry;

	/**
	 * Creates a new FlowContextMenuProvider associated with the given viewer
	 * and action registry.
	 * 
	 * @param viewer
	 *            the viewer
	 * @param registry
	 *            the action registry
	 */
	public WFContextMenuProvider(QName contentDomain, EditPartViewer viewer,
			ActionRegistry registry) {
		super(viewer);
		setContentDomain(contentDomain);
		setActionRegistry(registry);
	}

	/**
	 * @see ContextMenuProvider#buildContextMenu(org.eclipse.jface.action.IMenuManager)
	 */
	@Override
	public void buildContextMenu(IMenuManager menu) {
		GEFActionConstants.addStandardActionGroups(menu);
		menu.remove(IWorkbenchActionConstants.MB_ADDITIONS);
		List<IAction> actions = new ArrayList<IAction>();
		Iterator<?> iter = getActionRegistry().getActions();
		while (iter.hasNext()) {
			IAction action = (IAction) iter.next();
			action.isEnabled();
			actions.add(action);

		}

		// sort and add to menu
		Collections.sort(actions, new ActionComparator());
		for (IAction action : actions) {
			menu.add(action);
		}

	}

	private ActionRegistry getActionRegistry() {
		return actionRegistry;
	}

	public QName getContentDomain() {
		return contentDomain;
	}

	/**
	 * Sets the action registry
	 * 
	 * @param registry
	 *            the action registry
	 */
	public void setActionRegistry(ActionRegistry registry) {
		actionRegistry = registry;
	}

	public void setContentDomain(QName contentDomain) {
		this.contentDomain = contentDomain;
	}

}
