package de.fzj.unicore.rcp.wfeditor.model.variables;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;

/**
 * Interface to be implemented by workflow elements which declare workflow
 * variable iterators (defining an iteration over certain variable values). Used
 * for displaying the associated section in the tabbed properties view;
 * 
 * @author bdemuth
 * 
 */
public interface IElementWithVariableIterators extends IFlowElement {

	public static final String PROP_ITERATOR_LIST = "workflow variable iterator list";

	/**
	 * Key for Boolean property that indicates whether the variable iterators
	 * will be used during workflow execution. If the variable iterators are not
	 * used, they won't contribute to the resulting workflow and the controls
	 * for setting up the iterators are disabled.
	 */
	public static final String PROP_ITERATORS_USED = "variable iterators used";

	public boolean allowsAddingIterators();

	public boolean areIteratorsUsed();

	/**
	 * Get a list containing all variable iterators that have been declared by
	 * this element.
	 * 
	 * @return
	 */
	public WorkflowVariableIteratorList getVariableIteratorList();

	public void setUseIterators(boolean active);

	public void setVariableIteratorList(WorkflowVariableIteratorList list);
}
