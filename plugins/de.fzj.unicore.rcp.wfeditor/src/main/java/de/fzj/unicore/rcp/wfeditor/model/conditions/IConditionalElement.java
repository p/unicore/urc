package de.fzj.unicore.rcp.wfeditor.model.conditions;

import java.util.Map;
import java.util.Set;

import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.IPropertySourceWithListeners;

/**
 * This interface is used for identifying property sources that contain a
 * condition as a property.
 * 
 * @author bdemuth
 * 
 */
public interface IConditionalElement extends IPropertySourceWithListeners,
		ICopyable {

	public IConditionalElement getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags);
}
