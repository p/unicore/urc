package de.fzj.unicore.rcp.wfeditor.model.variables;

/**
 * Marker interface to be implemented by workflow elements which declare
 * workflow variable modifiers. Used for displaying the associated section in
 * the tabbed properties view;
 * 
 * @author bdemuth
 * 
 */
public interface IElementWithVariableModifiers {

	public boolean allowsAddingModifiers();
}
