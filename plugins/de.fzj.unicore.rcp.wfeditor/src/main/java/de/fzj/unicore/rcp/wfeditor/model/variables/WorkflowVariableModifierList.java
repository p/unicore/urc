package de.fzj.unicore.rcp.wfeditor.model.variables;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.interfaces.ISerializable;
import de.fzj.unicore.rcp.wfeditor.model.IDisposableWithUndo;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

@XStreamAlias("WorkflowVariableModifierList")
public class WorkflowVariableModifierList extends PropertySource implements
		IDisposableWithUndo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4070160769675333630L;
	private List<WorkflowVariableModifier> modifiers;

	public WorkflowVariableModifierList() {

	}

	@Override
	public void activate() {
		super.activate();
		for (WorkflowVariableModifier mod : getModifiers()) {
			mod.activate();
		}
	}

	public void addModifier(WorkflowVariableModifier v) {
		getModifiers().add(v);
		setPropertyValue(v.getId(), v);
		addPropertyDescriptor(new WorkflowVariableModifierPropertyDescriptor(v));
	}

	@Override
	protected void addPropertyDescriptors() {
		getPropertyDescriptorList().clear();
		for (WorkflowVariableModifier current : getModifiers()) {
			addPropertyDescriptor(new WorkflowVariableModifierPropertyDescriptor(
					current));
		}
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
		for (ISerializable current : getModifiers()) {
			current.afterDeserialization();
		}
	}

	@Override
	public void afterSerialization() {
		super.afterSerialization();
		for (ISerializable current : getModifiers()) {
			current.afterSerialization();
		}
	}

	@Override
	public void beforeSerialization() {
		super.beforeSerialization();
		for (ISerializable current : getModifiers()) {
			current.beforeSerialization();
		}
	}

	@Override
	public WorkflowVariableModifierList clone()
			throws CloneNotSupportedException {
		WorkflowVariableModifierList clone = (WorkflowVariableModifierList) super
				.clone();
		return clone;
	}

	@Override
	public void dispose() {
		for (WorkflowVariableModifier mod : getModifiers()) {
			if (!mod.isDisposed()) {
				mod.dispose();
			}
		}
	}

	@Override
	public WorkflowVariableModifierList getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		WorkflowVariableModifierList copy = (WorkflowVariableModifierList) super
				.getCopy(toBeCopied, mapOfCopies, copyFlags);
		copy.modifiers = new ArrayList<WorkflowVariableModifier>();
		for (WorkflowVariableModifier mod : getModifiers()) {
			WorkflowVariableModifier newMod = CloneUtils.getFromMapOrCopy(mod,
					toBeCopied, mapOfCopies, copyFlags);
			copy.addModifier(newMod);
		}
		return copy;
	}

	public List<WorkflowVariableModifier> getModifiers() {
		if (modifiers == null) {
			modifiers = new ArrayList<WorkflowVariableModifier>();
		}
		return modifiers;
	}

	public void removeModifier(WorkflowVariableModifier v) {
		getModifiers().remove(v);
		removeProperty(v.getId());
		removePropertyDescriptor(new WorkflowVariableModifierPropertyDescriptor(
				v));
	}

	@Override
	public String toString() {
		return "";
	}

	@Override
	public void undoDispose() {
		for (WorkflowVariableModifier mod : getModifiers()) {
			if (mod.isDisposed()) {
				mod.undoDispose();
			}
		}
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		if (updatedToCurrentModelVersion) {
			return; // don't do this twice!
		}
		super.updateToCurrentModelVersion(oldVersion, currentVersion);
		for (WorkflowVariableModifier mod : getModifiers()) {
			mod.updateToCurrentModelVersion(oldVersion, currentVersion);
		}
	}
}
