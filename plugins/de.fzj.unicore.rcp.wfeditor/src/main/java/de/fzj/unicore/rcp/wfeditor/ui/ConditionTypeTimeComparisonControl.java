/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.ui;

import java.util.Calendar;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.ValidityChangeEvent;
import de.fzj.unicore.rcp.wfeditor.extensions.CommonConditionTypeControl;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.model.conditions.ConditionTypeTimeComparison;

/**
 * @author demuth
 * 
 */
public class ConditionTypeTimeComparisonControl extends CommonConditionTypeControl {

	private ConditionTypeTimeComparison comparison;
	private CCombo compare;
	private DateTime dateControl, timeControl;
	private Calendar selectedTime;
	/**
	 * 
	 */
	public ConditionTypeTimeComparisonControl(Composite parent,
			Condition value) {
		super(parent, SWT.NONE);
		setBackground(parent.getBackground());
		comparison = (ConditionTypeTimeComparison) value.getSelectedType();
		comparison.setCondition(value);
		GridLayout layout = new GridLayout();
		layout.numColumns = 4;
		this.setLayout(layout);
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		Label label = new Label(this, SWT.NONE);
		label.setText("is");
		label.setBackground(getBackground());
		label.setLayoutData(data);
		controls.add(label);

		initCompareOperator();
		initTimePicker();

		data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);
		data.horizontalSpan = 4;
		errorMsg.setLayoutData(data);
		errorMsg.setForeground(parent.getDisplay()
				.getSystemColor(SWT.COLOR_RED));
		errorMsg.setBackground(getBackground());

		data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);
		this.setLayoutData(data);
		pack();
	}

	protected void fireValidityEvent(boolean newValidity) {
		ValidityChangeEvent evt = new ValidityChangeEvent(this, newValidity);
		for (IConditionChangeListener l : listeners) {
			l.validityChanged(evt);
		}
	}

	protected void fireValueChange() {
		ValueChangeEvent evt = new ValueChangeEvent(comparison.getCondition());
		for (IConditionChangeListener l : listeners) {
			l.valueChanged(evt);
		}
	}

	protected void initCompareOperator() {
		compare = new CCombo(this, SWT.FLAT | SWT.BORDER | SWT.READ_ONLY);
		String[] items = ConditionTypeTimeComparison.TIME_COMPARATORS;
		compare.setItems(items);

		GridData data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);
		data.grabExcessHorizontalSpace = false;
		compare.setLayoutData(data);
		compare.select(comparison.getComparator());
		compare.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean oldValidity = isValid();
				int oldSelection = comparison.getComparator();
				int selection = ((CCombo) e.getSource()).getSelectionIndex();
				comparison.setComparator(selection);
				if (oldSelection != selection) {
					fireValueChange();
				}
				if (oldValidity != isValid()) {
					fireValidityEvent(isValid());
				}
			}
		});
		controls.add(compare);
	}

	protected void initTimePicker() {

		selectedTime = Calendar.getInstance();
		selectedTime.setTimeInMillis(comparison.getValue());
		dateControl = new DateTime(this, SWT.BORDER | SWT.DATE );
		dateControl.setDay(selectedTime.get(Calendar.DAY_OF_MONTH));
		dateControl.setMonth(selectedTime.get(Calendar.MONTH));
		dateControl.setYear(selectedTime.get(Calendar.YEAR));
		
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		dateControl.setLayoutData(data);
		dateControl.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				timeChanged();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				timeChanged();
			}

		});
		timeControl = new DateTime(this, SWT.BORDER | SWT.TIME | SWT.LONG);
		timeControl.setHours(selectedTime.get(Calendar.HOUR_OF_DAY));
		timeControl.setMinutes(selectedTime.get(Calendar.MINUTE));
		timeControl.setSeconds(selectedTime.get(Calendar.SECOND));
		data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		timeControl.setLayoutData(data);
		timeControl.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				timeChanged();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				timeChanged();
			}

		});
		errorMsg = new Label(this, SWT.NONE);
		data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);
		data.horizontalSpan = 4;
		errorMsg.setLayoutData(data);
		errorMsg.setForeground(getParent().getDisplay()
				.getSystemColor(SWT.COLOR_RED));
		errorMsg.setBackground(getBackground());
		updateErrorMsg();
		controls.add(errorMsg);
		controls.add(dateControl);
		controls.add(timeControl);
	}



	private void timeChanged()
	{
		boolean oldValidity = isValid();
		selectedTime = Calendar.getInstance();
		selectedTime.set(dateControl.getYear(), dateControl.getMonth(),
				dateControl.getDay(), timeControl.getHours(),
				timeControl.getMinutes(), timeControl.getSeconds());
		if(isValid())
		{
			comparison.setValue(selectedTime.getTimeInMillis());
		}
		fireValueChange();
		boolean newValidity = isValid();
		if (oldValidity != newValidity) {
			fireValidityEvent(newValidity);
			updateErrorMsg();
		}

	}

	private void updateErrorMsg()
	{
		if(isValid())
		{
			errorMsg.setVisible(false);

		}
		else
		{
			errorMsg.setText("Please enter a valid date after "+Constants.getDefaultDateFormat().format(Calendar.getInstance().getTime()));
			errorMsg.setVisible(true);
		}
		layout();
	}

	public boolean isValid() {
		return valueEntered && selectedTime != null && selectedTime.getTimeInMillis() > System.currentTimeMillis();
	}

}
