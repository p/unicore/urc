/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.layouts;

import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * An ordered Layout with a single row. Should be used with an EditPolicy that
 * automatically resizes the parent figure if the row of children doesn't fit
 * in.
 * 
 * @author demuth
 * 
 */
public class WFOrderedLayout extends FlowLayout {

	/**
	 * Holds the necessary information for layout calculations.
	 */
	class WorkingData {
		int rowHeight, rowWidth, rowCount, rowX, rowY, maxWidth;
		Rectangle bounds[], area;
		IFigure row[];
	}

	private WorkingData data = null;

	/**
	 * Constructs a FlowLayout with horizontal orientation.
	 */
	public WFOrderedLayout() {

	}

	/**
	 * Constructs a FlowLayout whose orientation is given in the input.
	 * 
	 * @param isHorizontal
	 *            <code>true</code> if the layout should be horizontal
	 */
	public WFOrderedLayout(boolean isHorizontal) {
		setHorizontal(isHorizontal);
	}

	/**
	 * @see org.eclipse.draw2d.AbstractLayout#calculatePreferredSize(IFigure,
	 *      int, int)
	 */
	@Override
	protected Dimension calculatePreferredSize(IFigure container, int wHint,
			int hHint) {
		// Subtract out the insets from the hints
		if (wHint > -1) {
			wHint = Math.max(0, wHint - container.getInsets().getWidth());
		}
		if (hHint > -1) {
			hHint = Math.max(0, hHint - container.getInsets().getHeight());
		}

		// Figure out the new hint that we are interested in based on the
		// orientation
		// Ignore the other hint (by setting it to -1). NOTE: The children of
		// the
		// parent figure will then be asked to ignore that hint as well.
		int maxWidth;
		if (isHorizontal()) {
			maxWidth = wHint;
			hHint = -1;
		} else {
			maxWidth = hHint;
			wHint = -1;
		}
		if (maxWidth < 0) {
			maxWidth = Integer.MAX_VALUE;
		}

		// The preferred dimension that is to be calculated and returned
		Dimension prefSize = new Dimension();

		List<?> children = container.getChildren();
		int width = 0;
		int height = 0;
		IFigure child;
		Dimension childSize;

		// Build the sizes for each row, and update prefSize accordingly
		for (int i = 0; i < children.size(); i++) {
			child = (IFigure) children.get(i);
			childSize = transposer.t(getChildSize(child, wHint, hHint));
			if (i == 0) {
				width = childSize.width;
				height = childSize.height;
			} else if (width + childSize.width + getMinorSpacing() > maxWidth) {
				// The current row is full, start a new row.
				prefSize.height += height + getMajorSpacing();
				prefSize.width = Math.max(prefSize.width, width);
				width = childSize.width;
				height = childSize.height;
			} else {
				// The current row can fit another child.
				width += childSize.width + getMinorSpacing();
				height = Math.max(height, childSize.height);
			}
		}

		// Flush out the last row's data
		prefSize.height += height;
		prefSize.width = Math.max(prefSize.width, width);

		// Transpose the dimension back, and compensate for the border.
		prefSize = transposer.t(prefSize);
		prefSize.width += container.getInsets().getWidth();
		prefSize.height += container.getInsets().getHeight();
		prefSize.union(getBorderPreferredSize(container));

		return prefSize;
	}

	/**
	 * Initializes the state of row data, which is internal to the layout
	 * process.
	 */
	@Override
	protected void initRow() {
		data.rowX = 0;
		data.rowHeight = 0;
		data.rowWidth = 0;
		data.rowCount = 0;
	}

	/**
	 * Initializes state data for laying out children, based on the Figure given
	 * as input.
	 * 
	 * @param parent
	 *            the parent figure
	 * @since 2.0
	 */
	@Override
	protected void initVariables(IFigure parent) {
		data.row = new IFigure[parent.getChildren().size()];
		data.bounds = new Rectangle[data.row.length];
		data.maxWidth = data.area.width;
	}

	/**
	 * @see org.eclipse.draw2d.LayoutManager#layout(IFigure)
	 */
	@Override
	public void layout(IFigure parent) {
		data = new WorkingData();
		Rectangle relativeArea = parent.getClientArea();
		data.area = transposer.t(relativeArea);

		Iterator<?> iterator = parent.getChildren().iterator();
		int dx;

		// Calculate the hints to be passed to children
		int wHint = -1;
		int hHint = -1;
		if (isHorizontal()) {
			wHint = parent.getClientArea().width;
		} else {
			hHint = parent.getClientArea().height;
		}

		initVariables(parent);
		initRow();
		int i = 0;
		while (iterator.hasNext()) {
			IFigure f = (IFigure) iterator.next();
			Dimension pref = transposer.t(getChildSize(f, wHint, hHint));
			Rectangle r = new Rectangle(0, 0, pref.width, pref.height);

			if (data.rowCount > 0) {
				if (data.rowWidth + pref.width > data.maxWidth) {
					layoutRow(parent);
				}
			}
			r.x = data.rowX;
			r.y = data.rowY;
			dx = r.width + getMinorSpacing();
			data.rowX += dx;
			data.rowWidth += dx;
			data.rowHeight = Math.max(data.area.height, r.height);
			data.row[data.rowCount] = f;
			data.bounds[data.rowCount] = r;
			data.rowCount++;
			i++;
		}
		if (data.rowCount != 0) {
			layoutRow(parent);
		}
		data = null;
	}

	/**
	 * Layouts one row of components. This is done based on the layout's
	 * orientation, minor alignment and major alignment.
	 * 
	 * @param parent
	 *            the parent figure
	 * @since 2.0
	 */
	@Override
	protected void layoutRow(IFigure parent) {
		int majorAdjustment = 0;
		int minorAdjustment = 0;
		int correctMajorAlignment = majorAlignment;
		int correctMinorAlignment = minorAlignment;

		majorAdjustment = data.area.width - data.rowWidth;

		switch (correctMajorAlignment) {
		case ALIGN_LEFTTOP:
			majorAdjustment = 0;
			break;
		case ALIGN_CENTER:
			majorAdjustment /= 2;
			break;
		case ALIGN_RIGHTBOTTOM:
			break;
		}

		for (int j = 0; j < data.rowCount; j++) {
			if (fill) {
				data.bounds[j].height = data.rowHeight;
			} else {
				minorAdjustment = data.rowHeight - data.bounds[j].height;
				switch (correctMinorAlignment) {
				case ALIGN_LEFTTOP:
					minorAdjustment = 0;
					break;
				case ALIGN_CENTER:
					minorAdjustment /= 2;
					break;
				case ALIGN_RIGHTBOTTOM:
					break;
				}
				data.bounds[j].y += minorAdjustment;
			}
			data.bounds[j].x += majorAdjustment;

			setBoundsOfChild(parent, data.row[j], transposer.t(data.bounds[j]));
		}
		data.rowY += getMajorSpacing() + data.rowHeight;
		initRow();
	}

}
