package de.fzj.unicore.rcp.wfeditor.model.variables;

import org.eclipse.ui.views.properties.PropertyDescriptor;

public class WorkflowVariableModifierPropertyDescriptor extends
		PropertyDescriptor {

	protected WorkflowVariableModifier modifier;

	WorkflowVariableModifierPropertyDescriptor(WorkflowVariableModifier v) {
		super(v.getId(), v.getName());
		this.modifier = v;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof WorkflowVariableModifierPropertyDescriptor)) {
			return false;
		} else {
			return ((WorkflowVariableModifierPropertyDescriptor) o).modifier
					.getId() == modifier.getId();
		}
	}

	@Override
	public String getDisplayName() {
		return modifier.getName();
	}
}
