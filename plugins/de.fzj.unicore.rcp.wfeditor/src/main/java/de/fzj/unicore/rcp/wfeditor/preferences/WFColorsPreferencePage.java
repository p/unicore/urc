package de.fzj.unicore.rcp.wfeditor.preferences;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;

public class WFColorsPreferencePage extends PreferencePage implements
		IWorkbenchPreferencePage {

	private Map<String, ColorFieldEditor> colorEditors;

	/*
	 * @see PreferencePage#createContents(Composite)
	 */
	@Override
	protected Control createContents(Composite parent) {
		colorEditors = new HashMap<String, ColorFieldEditor>();
		Composite entryTable = new Composite(parent, SWT.NULL);

		// Create a data that takes up the extra space in the dialog .
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.grabExcessHorizontalSpace = true;
		entryTable.setLayoutData(data);

		GridLayout layout = new GridLayout();
		entryTable.setLayout(layout);

		Group colorComposite = new Group(entryTable, SWT.NONE);
		colorComposite.setText("Workflow file connectors:");
		colorComposite.setLayout(new GridLayout());

		// Create a data that takes up the extra space in the dialog.
		colorComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		String s = getPreferenceStore().getString(
				WFConstants.P_KNOWN_MIME_TYPES);
		for (String mime : s.split(WFConstants.MIME_TYPE_SEPARATOR)) {
			if (s.length() == 0) {
				continue;
			}
			ColorFieldEditor colorEditor = new ColorFieldEditor(
					WFConstants.P_MIME_COLOR_PREFIX + mime, "MIME type \""
							+ mime + "\":", colorComposite);

			// Set the editor up to use this page
			colorEditor.setPage(this);
			colorEditor.setPreferenceStore(getPreferenceStore());
			colorEditor.load();
			colorEditors.put(mime, colorEditor);
		}
		applyDialogFont(entryTable);
		return entryTable;
	}

	/*
	 * @see IWorkbenchPreferencePage#init(IWorkbench)
	 */
	public void init(IWorkbench workbench) {
		// Initialize the preference store we wish to use
		setPreferenceStore(WFActivator.getDefault().getPreferenceStore());
	}

	/**
	 * Performs special processing when this page's Restore Defaults button has
	 * been pressed. Sets the contents of the color field to the default value
	 * in the preference store.
	 */
	@Override
	protected void performDefaults() {
		for (ColorFieldEditor colorEditor : colorEditors.values()) {
			colorEditor.loadDefault();
		}
	}

	/**
	 * Method declared on IPreferencePage. Save the color preference to the
	 * preference store.
	 */
	@Override
	public boolean performOk() {
		for (ColorFieldEditor colorEditor : colorEditors.values()) {
			colorEditor.store();
		}
		return super.performOk();
	}

}
