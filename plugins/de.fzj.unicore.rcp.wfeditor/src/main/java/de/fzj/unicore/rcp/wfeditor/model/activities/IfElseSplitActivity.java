package de.fzj.unicore.rcp.wfeditor.model.activities;

import javax.xml.namespace.QName;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("IfElseSplitActivity")
public class IfElseSplitActivity extends SimpleActivity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6967701676491346491L;

	public static QName TYPE = new QName("http://www.unicore.eu/",
	"IfElseSplitActivity");
	
	@Override
	public QName getType() {
		return TYPE;
	}
}
