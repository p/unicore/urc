/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;

/**
 * Command to rename Activities.
 * 
 * @author Daniel Lee
 */
public class SetIterationIdCommand extends Command {

	private IFlowElement model;
	private String newIterationId, oldIterationId;

	public SetIterationIdCommand(IFlowElement model, String newIterationId) {
		this.model = model;
		this.newIterationId = newIterationId;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldIterationId = model.getCurrentIterationId();
		redo();
	}

	@Override
	public void redo() {
		model.setCurrentIterationId(newIterationId, true);
		StructuredActivity parent = model.getParent();
		while (parent != null) {
			parent.setIterationManuallyChanged(true);
			// no need to undo this: messing with the iteration ID means not
			// auto-updating for the user anymore
			parent = parent.getParent();
		}

	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		model.setCurrentIterationId(oldIterationId, true);
	}

}
