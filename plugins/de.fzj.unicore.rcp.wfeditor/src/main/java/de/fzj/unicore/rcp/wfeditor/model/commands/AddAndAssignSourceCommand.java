/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.utils.TransitionUtils;

/**
 * @author demuth
 */
public class AddAndAssignSourceCommand extends Command {

	private IActivity child;
	private IActivity source;
	private Transition transition;

	public AddAndAssignSourceCommand() {
	}

	/**
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return source.getParent().canAddChildren()
				&& TransitionUtils.isAllowedIgnoreParent(source, child);
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		if (child.getIncomingTransitions().size() == 0) // only add transitions
														// to the starting
														// activities
		{
			transition = new Transition(source, child);
		}
	}

	/**
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		if (transition != null) {
			source.addOutput(transition);
			child.addInput(transition);
		}
	}

	/**
	 * Sets the Activity to create
	 * 
	 * @param activity
	 *            the Activity to create
	 */
	public void setChild(IActivity activity) {
		child = activity;
	}

	/**
	 * Sets the source to the passed activity
	 * 
	 * @param activity
	 *            the source
	 */
	public void setSource(IActivity activity) {
		source = activity;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		source.removeOutput(transition);
		child.removeInput(transition);
	}

}
