/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.IPaletteEntryExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;

/**
 * @author hudsonr, demuth Created on Jul 16, 2003
 */
public class PartFactory implements EditPartFactory {

	private WFEditor editor;

	public PartFactory(WFEditor editor) {
		this.editor = editor;
	}

	/**
	 * Iterate over all PartFactory extensions and allow them to instantiate the
	 * EditPart. The first EditPart returned by an extension is being returned.
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart,
	 *      java.lang.Object)
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		EditPart part = null;

		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(WFConstants.EXTENSION_POINT_PALETTE_ENTRY);
		IConfigurationElement[] members = extensionPoint
				.getConfigurationElements();
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			try {
				IPaletteEntryExtensionPoint ext = (IPaletteEntryExtensionPoint) member
						.createExecutableExtension("name");
				part = ext.createEditPart(getEditor(), context, model);
				if (part != null) {
					part.setModel(model);
					return part;
				}
			} catch (CoreException ex) {
				WFActivator
						.log("Could not determine all available actions for service",
								ex);
			}
		}

		return null;
	}

	public WFEditor getEditor() {
		return editor;
	}

	public void setEditor(WFEditor editor) {
		this.editor = editor;
	}

}
