package de.fzj.unicore.rcp.wfeditor.layouts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.AbstractRouter;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Vector;

/**
 * Provides a {@link Connection} with an orthogonal route between the
 * Connection's source and target anchors.
 */
public final class DataFlowConnectionRouter extends AbstractRouter {

	private class ReservedInfo {
		public List<Integer> reservedRows = new ArrayList<Integer>(2);
		public List<Integer> reservedCols = new ArrayList<Integer>(2);
	}

	private Map<Integer,Integer> rowsUsed = new HashMap<Integer,Integer>();

	private Map<Integer,Integer> colsUsed = new HashMap<Integer,Integer>();
	// private Hashtable offsets = new Hashtable(7);

	private Map<Connection,ReservedInfo> reservedInfo = new HashMap<Connection, ReservedInfo>();

	private static Vector UP = new Vector(0, -1), DOWN = new Vector(0, 1),
			LEFT = new Vector(-1, 0), RIGHT = new Vector(1, 0);

	private int getColumnNear(Connection connection, int r, int n, int x) {
		int min = Math.min(n, x), max = Math.max(n, x);
		if (min > r) {
			max = min;
			min = r - (min - r);
		}
		if (max < r) {
			min = max;
			max = r + (r - max);
		}
		int proximity = 0;
		int direction = -1;
		if (r % 2 == 1) {
			r--;
		}
		Integer i;
		while (proximity < r) {
			i = new Integer(r + proximity * direction);
			if (!colsUsed.containsKey(i)) {
				colsUsed.put(i, i);
				reserveColumn(connection, i);
				return i.intValue();
			}
			int j = i.intValue();
			if (j <= min) {
				return j + 2;
			}
			if (j >= max) {
				return j - 2;
			}
			if (direction == 1) {
				direction = -1;
			} else {
				direction = 1;
				proximity += 2;
			}
		}
		return r;
	}

	protected Vector getEndDirection(Connection conn) {
		Point start = getStartPoint(conn);
		Point end = getEndPoint(conn);
		int dx = end.x - start.x;
		int dy = end.y - start.y;
		if (Math.abs(dy) < 10) {
			if (dx > 0) {
				return LEFT;
			} else {
				return RIGHT;
			}
		} else {
			if (dy > 0) {
				return UP;
			} else {
				return DOWN;
			}
		}
	}

	protected int getRowNear(Connection connection, int r, int n, int x) {
		int min = Math.min(n, x), max = Math.max(n, x);
		if (min > r) {
			max = min;
			min = r - (min - r);
		}
		if (max < r) {
			min = max;
			max = r + (r - max);
		}

		int proximity = 0;
		int direction = -1;
		if (r % 2 == 1) {
			r--;
		}
		Integer i;
		while (proximity < r) {
			i = new Integer(r + proximity * direction);
			if (!rowsUsed.containsKey(i)) {
				rowsUsed.put(i, i);
				reserveRow(connection, i);
				return i.intValue();
			}
			int j = i.intValue();
			if (j <= min) {
				return j + 2;
			}
			if (j >= max) {
				return j - 2;
			}
			if (direction == 1) {
				direction = -1;
			} else {
				direction = 1;
				proximity += 2;
			}
		}
		return r;
	}

	protected Vector getStartDirection(Connection conn) {

		Point start = getStartPoint(conn);
		Point end = getEndPoint(conn);
		int dx = end.x - start.x;
		int dy = end.y - start.y;
		if (Math.abs(dy) < 10) {
			if (dx > 0) {
				return RIGHT;
			} else {
				return LEFT;
			}
		} else {
			if (dy > 0) {
				return DOWN;
			} else {
				return UP;
			}
		}
	}

	/**
	 * @see ConnectionRouter#invalidate(Connection)
	 */
	@Override
	public void invalidate(Connection connection) {
		removeReservedLines(connection);
	}

	protected void processPositions(Vector start, Vector end, List<Integer> positions,
			boolean horizontal, Connection conn) {
		removeReservedLines(conn);

		int pos[] = new int[positions.size() + 2];
		if (horizontal) {
			pos[0] = (int) start.x;
		} else {
			pos[0] = (int) start.y;
		}
		int i;
		for (i = 0; i < positions.size(); i++) {
			pos[i + 1] = positions.get(i);
		}
		if (horizontal == (positions.size() % 2 == 1)) {
			pos[++i] = (int) end.x;
		} else {
			pos[++i] = (int) end.y;
		}

		PointList points = new PointList();
		points.addPoint(new Point(start.x, start.y));
		Point p;
		int current, prev, min, max;
		boolean adjust;
		for (i = 2; i < pos.length - 1; i++) {
			horizontal = !horizontal;
			prev = pos[i - 1];
			current = pos[i];

			adjust = (i != pos.length - 2);
			if (horizontal) {
				if (adjust) {
					min = pos[i - 2];
					max = pos[i + 2];
					pos[i] = current = getRowNear(conn, current, min, max);
				}
				p = new Point(prev, current);
			} else {
				if (adjust) {
					min = pos[i - 2];
					max = pos[i + 2];
					pos[i] = current = getColumnNear(conn, current, min, max);
				}
				p = new Point(current, prev);
			}
			points.addPoint(p);
		}
		points.addPoint(new Point(end.x, end.y));
		conn.setPoints(points);
	}

	/**
	 * @see ConnectionRouter#remove(Connection)
	 */
	@Override
	public void remove(Connection connection) {
		removeReservedLines(connection);
	}

	protected void removeReservedLines(Connection connection) {
		ReservedInfo rInfo = (ReservedInfo) reservedInfo.get(connection);
		if (rInfo == null) {
			return;
		}

		for (int i = 0; i < rInfo.reservedRows.size(); i++) {
			rowsUsed.remove(rInfo.reservedRows.get(i));
		}
		for (int i = 0; i < rInfo.reservedCols.size(); i++) {
			colsUsed.remove(rInfo.reservedCols.get(i));
		}
		reservedInfo.remove(connection);
	}

	protected void reserveColumn(Connection connection, Integer column) {
		ReservedInfo info = (ReservedInfo) reservedInfo.get(connection);
		if (info == null) {
			info = new ReservedInfo();
			reservedInfo.put(connection, info);
		}
		info.reservedCols.add(column);
	}

	protected void reserveRow(Connection connection, Integer row) {
		ReservedInfo info = (ReservedInfo) reservedInfo.get(connection);
		if (info == null) {
			info = new ReservedInfo();
			reservedInfo.put(connection, info);
		}
		info.reservedRows.add(row);
	}

	/**
	 * @see ConnectionRouter#route(Connection)
	 */
	public void route(Connection conn) {
		if ((conn.getSourceAnchor() == null)
				|| (conn.getTargetAnchor() == null)) {
			return;
		}
		int i;
		Point startPoint = getStartPoint(conn);
		conn.translateToRelative(startPoint);
		Point endPoint = getEndPoint(conn);
		conn.translateToRelative(endPoint);

		Vector start = new Vector(startPoint.x, startPoint.y);
		Vector end = new Vector(endPoint.x, endPoint.y);
		Vector average = start.getAveraged(end);

		Vector direction = new Vector(start, end);
		Vector startNormal = getStartDirection(conn);
		Vector endNormal = getEndDirection(conn);

		List<Integer> positions = new ArrayList<Integer>(5);
		boolean horizontal = startNormal.isHorizontal();
		if (horizontal) {
			positions.add((int) start.y);
		} else {
			positions.add((int) start.x);
		}
		horizontal = !horizontal;
		int step = 10;
		if (startNormal.getDotProduct(endNormal) == 0) {
			if ((startNormal.getDotProduct(direction) >= 0)
					&& (endNormal.getDotProduct(direction) <= 0)) {
				// 0
			} else {
				// 2
				if (startNormal.getDotProduct(direction) < 0) {
					i = (int) startNormal.getDotProduct(start
							.getAdded(startNormal.getMultiplied(step)));
				} else {
					if (horizontal) {
						i = (int) average.y;
					} else {
						i = (int) average.x;
					}
				}
				positions.add(new Integer(i));
				horizontal = !horizontal;

				if (endNormal.getDotProduct(direction) > 0) {
					i = (int) endNormal.getDotProduct(end.getAdded(endNormal
							.getMultiplied(step)));
				} else {
					if (horizontal) {
						i = (int) average.y;
					} else {
						i = (int) average.x;
					}
				}
				positions.add(new Integer(i));
				horizontal = !horizontal;
			}
		} else {
			if (startNormal.getDotProduct(endNormal) > 0) {
				// 1
				if (startNormal.getDotProduct(direction) >= 0) {
					i = (int) startNormal.getDotProduct(start
							.getAdded(startNormal.getMultiplied(step)));
				} else {
					i = (int) endNormal.getDotProduct(end.getAdded(endNormal
							.getMultiplied(step)));
				}
				positions.add(new Integer(i));
				horizontal = !horizontal;
			} else {
				// 3 or 1
				if (startNormal.getDotProduct(direction) < 0) {
					i = (int) startNormal.getDotProduct(start
							.getAdded(startNormal.getMultiplied(step)));
					positions.add(new Integer(i));
					horizontal = !horizontal;
				}

				if (horizontal) {
					i = (int) average.y;
				} else {
					i = (int) average.x;
				}
				positions.add(new Integer(i));
				horizontal = !horizontal;

				if (startNormal.getDotProduct(direction) < 0) {
					i = (int) endNormal.getDotProduct(end.getAdded(endNormal
							.getMultiplied(step)));
					positions.add(new Integer(i));
					horizontal = !horizontal;
				}
			}
		}
		if (horizontal) {
			positions.add((int) end.y);
		} else {
			positions.add((int) end.x);
		}

		processPositions(start, end, positions, startNormal.isHorizontal(),
				conn);
	}

}
