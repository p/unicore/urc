package de.fzj.unicore.rcp.wfeditor.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.swt.graphics.RGB;

import de.fzj.unicore.rcp.common.utils.ColorGenerator;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.files.CollectWFFilesVisitor;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFileComparator;
import de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.PredecessorTraverser;

public class WorkflowFileUtils {


	private static ColorGenerator mimeColorGenerator;

	public static List<WorkflowFile> collectWorkflowFiles(IFlowElement element) {

		List<WorkflowFile> files = new ArrayList<WorkflowFile>();
		if (element.getDiagram().containsCycles()) {
			WFActivator
					.log(IStatus.ERROR,
							"Cannot refresh available workflow files, the workflow contains a cycle!");
		} else {
			try {

				IGraphTraverser traverser = new PredecessorTraverser();
				CollectWFFilesVisitor visitor = new CollectWFFilesVisitor();

				traverser.traverseGraph(element.getDiagram(), element, visitor);
				Set<WorkflowFile> found = visitor.getFoundFiles();

				for (WorkflowFile workflowFile : found) {
					IActivity act = workflowFile.getActivity();
					// do not allow importing your own output files!
					if (element.equals(act)) {
						continue;
					}
					files.add(workflowFile);
				}
			} catch (Exception e) {
				WFActivator.log(IStatus.ERROR,
						"Cannot refresh available workflow files:", e);
			}
		}
		WorkflowFileComparator cmp = new WorkflowFileComparator();
		Collections.sort(files, cmp);
		return files;
	}

	public static RGB getColorForMimeType(String mime) {

		IPreferenceStore prefs = getPreferenceStore();
		String s = prefs.getString(WFConstants.P_KNOWN_MIME_TYPES);
		String[] knownTypes = s.split(WFConstants.MIME_TYPE_SEPARATOR);
		boolean known = false;
		for (String string : knownTypes) {
			if (string.equals(mime)) {
				known = true;
				break;
			}
		}
		if (known == false) {
			return null;
		}
		return PreferenceConverter.getColor(prefs,
				WFConstants.P_MIME_COLOR_PREFIX + mime);
	}

	public static WorkflowFile getFileWithId(String id, IActivity act) {
		if (id == null) {
			return null;
		}
		for (WorkflowFile file : act.getOutputFiles()) {
			if (id.equals(file.getId())) {
				return file;
			}
		}

		return null;
	}

	public static String getFullDisplayedName(WorkflowFile f) {

		return f.getFullDisplayedName();
	}

	public static ColorGenerator getMimeColorGenerator() {
		if (mimeColorGenerator == null) {
			initColorGenerator();
		}
		return mimeColorGenerator;
	}

	private static IPreferenceStore getPreferenceStore() {

		return WFActivator.getDefault().getPreferenceStore();
	}

	private static void initColorGenerator() {

		IPreferenceStore prefs = getPreferenceStore();
		String s = prefs.getString(WFConstants.P_KNOWN_MIME_TYPES);
		String[] knownTypes = s.split(WFConstants.MIME_TYPE_SEPARATOR);
		List<RGB> usedColors = new LinkedList<RGB>();
		for (String mime : knownTypes) {
			RGB rgb = PreferenceConverter.getColor(prefs,
					WFConstants.P_MIME_COLOR_PREFIX + mime);
			usedColors.add(rgb);
		}
		mimeColorGenerator = new ColorGenerator(1000, 1, usedColors);
	}

	public static void registerColorForMimeType(String mime, RGB color) {

		IPreferenceStore prefs = getPreferenceStore();
		String s = prefs.getString(WFConstants.P_KNOWN_MIME_TYPES);
		String[] knownTypes = s.split(WFConstants.MIME_TYPE_SEPARATOR);
		boolean known = false;
		for (String string : knownTypes) {
			if (string.equals(mime)) {
				known = true;
				break;
			}
		}
		if (!known) {
			if (s.length() > 0) {
				s += WFConstants.MIME_TYPE_SEPARATOR;
			}
			s += mime;
			prefs.setValue(WFConstants.P_KNOWN_MIME_TYPES, s);
		}

		PreferenceConverter.setValue(prefs, WFConstants.P_MIME_COLOR_PREFIX
				+ mime, color);

	}

}
