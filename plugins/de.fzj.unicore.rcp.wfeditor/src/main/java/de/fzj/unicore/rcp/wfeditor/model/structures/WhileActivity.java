/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.structures;

import java.beans.PropertyChangeEvent;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.SimpleActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.VariableModifierActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.WhileStartActivity;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.model.conditions.ConditionTypeVariableComparison;
import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType;
import de.fzj.unicore.rcp.wfeditor.model.transitions.ConditionalTransition;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableList;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifier;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifierList;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowVariableUtils;

/**
 * @author demuth
 * 
 */
@XStreamAlias("WhileActivity")
public class WhileActivity extends LoopActivity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1217782094231417562L;

	public static QName TYPE = new QName("http://www.unicore.eu/",
			"WhileActivity");

	/**
	 * Property for storing the iteration counter modifier activity.
	 */
	private static String PROP_MODIFIER = "while activity variable modifier";

	protected WhileStartActivity splitActivity;
	protected StructuredActivity body;
	protected ConditionalTransition conditionalTransition;

	public WhileActivity() {
	}

	@Override
	public void activate() {
		super.activate();
		WorkflowVariableModifierList modifierList = ((IFlowElement) getPropertyValue(PROP_MODIFIER))
				.getVariableModifierList();
		WorkflowVariableModifier iterationIncrementer = modifierList
				.getModifiers().get(0);
		iterationIncrementer.setDataSourceAndSinkVisible(false);
		WorkflowVariable iterationCounter = getSplitActivity()
				.getVariableList().getVariables().get(0);
		if (getDiagram().getVariable(iterationCounter.getName()) == null) {
			getDiagram().addVariable(iterationCounter);
		}
		if (iterationIncrementer.getVariable() == null) {
			WorkflowVariableUtils.linkModifierToVariable(iterationIncrementer,
					iterationCounter.getName());
		}
	}

	@Override
	public WhileActivity clone() throws CloneNotSupportedException {
		WhileActivity result = (WhileActivity) super.clone();
		result.body = null;
		result.conditionalTransition = null;
		result.splitActivity = null;
		return result;
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	public StructuredActivity getBody() {
		return body;
	}

	public Condition getCondition() {
		return conditionalTransition.getCondition();
	}

	@Override
	public WhileActivity getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		WhileActivity result = (WhileActivity) super.getCopy(toBeCopied,
				mapOfCopies, copyFlags);
		result.splitActivity = CloneUtils.getFromMapOrCopy(splitActivity,
				toBeCopied, mapOfCopies, copyFlags);
		result.body = CloneUtils.getFromMapOrCopy(body, toBeCopied,
				mapOfCopies, copyFlags);
		result.conditionalTransition = CloneUtils.getFromMapOrCopy(
				conditionalTransition, toBeCopied, mapOfCopies, copyFlags);
		return result;
	}


	@Override
	public WorkflowVariable getIterationCounter() {
		SimpleActivity splitActivity = getSplitActivity();
		if (splitActivity == null) {
			return null;
		}
		WorkflowVariableList list = splitActivity.getVariableList();
		if (list.getVariables().size() == 0) {
			return null;
		}
		return list.getVariables().get(0);
	}

	@Override
	public WorkflowVariableModifier getIterationIncrementer() {
		WorkflowVariableModifierList list = ((IFlowElement) getPropertyValue(PROP_MODIFIER))
				.getVariableModifierList();
		if (list.getModifiers().size() == 0) {
			return null;
		}
		return list.getModifiers().get(0);
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_MODIFIER);
		return propertiesToCopy;
	}

	public SimpleActivity getSplitActivity() {
		return splitActivity;
	}

	@Override
	public QName getType() {
		return TYPE;
	}

	@Override
	public void init() {
		super.init();
		splitActivity = new WhileStartActivity();
		this.addChild(getSplitActivity());
		setStartActivity(splitActivity);
		splitActivity.setDeletable(false);
		splitActivity.setBoundToParent(true);

		StructuredActivity body = new ContainerActivity();

		setBody(body);
		this.addChild(body);
		body.setDeletable(false);
		body.setBoundToParent(true);

		VariableModifierActivity modifier = new VariableModifierActivity();
		this.addChild(modifier);
		modifier.setDeletable(false);
		modifier.setBoundToParent(true);
		setPropertyValue(PROP_MODIFIER, modifier);

		// now create transitions
		conditionalTransition = new ConditionalTransition(splitActivity, body);
		conditionalTransition.setDeletable(false);
		conditionalTransition.setBoundToParent(true);

		Transition toModifier = new Transition(body, modifier);
		toModifier.setDeletable(false);
		toModifier.setBoundToParent(true);

		Transition closingLoop = new Transition(modifier, splitActivity);
		closingLoop.setPropertyValue(Transition.PROP_IS_CLOSING_LOOP, true);
		closingLoop.setDeletable(false);
		closingLoop.setBoundToParent(true);

		WorkflowDiagram diagram = getDiagram();
		getSplitActivity().setDiagram(diagram);
		getBody().setDiagram(diagram);
		modifier.setDiagram(diagram);
		initNamesAndVariables();

		getSplitActivity().init();
		getBody().init();
		conditionalTransition.init();
	}

	protected void initNamesAndVariables() {
		setName(getDiagram().getUniqueActivityName("WhileActivity"));
		getSplitActivity().setName(getDiagram().getUniqueActivityName("While"));
		getBody().setName(getDiagram().getUniqueActivityName("WhileBody"));

		WorkflowVariable iterationCounter = new WorkflowVariable(getName()
				+ "_Iteration_Counter", WorkflowVariable.VARIABLE_TYPE_INTEGER,
				"1", getStartActivity());
		getSplitActivity().getVariableList().addVariable(iterationCounter);
		getSplitActivity().getDataSourceList().addDataSource(iterationCounter);
		iterationCounter.addPersistentPropertyChangeListener(this);

		WorkflowVariableModifierList modifierList = ((IFlowElement) getPropertyValue(PROP_MODIFIER))
				.getVariableModifierList();
		WorkflowVariableModifier iterationIncrementer = modifierList
				.getModifiers().get(0);
		iterationIncrementer.setName(getName() + "_Iteration_Modifier");
		iterationIncrementer
				.setPropertyValue(
						WorkflowVariableModifier.PROP_DESCRIPTION,
						"This expression is evaluated after each iteration in order to update the iteration counter");
		iterationIncrementer.setDisplayedExpression(iterationCounter.getName()
				+ " ++;");

		setPropertyValue(PROP_ITERATOR_NAME, iterationCounter.getName());

		Condition c = getCondition();
		c.selectType(ConditionTypeVariableComparison.TYPE_NAME);
		ConditionTypeVariableComparison type = (ConditionTypeVariableComparison) c
				.getSelectedType();
		type.setComparator(IConditionType.INT_SMALLER_EQUAL);
		type.setValue("5");
		type.setVariable(getIterationCounter());
		type.setCondition(c);

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (getIterationCounter() != null
				&& getIterationCounter().equals(evt.getSource())
				&& WorkflowVariable.PROP_NAME.equals(evt.getPropertyName())) {
			setPropertyValue(PROP_ITERATOR_NAME, getIterationCounter()
					.getName());
		} else {
			super.propertyChange(evt);
		}
	}

	public void setBody(StructuredActivity ifBody) {
		this.body = ifBody;
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		if (updatedToCurrentModelVersion) {
			return; // don't do this twice!
		}
		super.updateToCurrentModelVersion(oldVersion, currentVersion);

	}

}
