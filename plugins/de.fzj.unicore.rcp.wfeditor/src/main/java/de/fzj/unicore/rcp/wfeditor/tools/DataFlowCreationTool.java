package de.fzj.unicore.rcp.wfeditor.tools;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.tools.SelectionTool;
import org.eclipse.ui.IEditorPart;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;

/**
 * This tool can be used to create, select and delete data flows.
 */
public class DataFlowCreationTool extends SelectionTool {

	/**
	 * Use tool property with this key for displaying different types of
	 * sources, sinks and dataflows, while hiding others. The corresponding
	 * value tells us what to show and what to hide
	 */
	public static final String PROP_SHOW_DATA_FLOWS = IFlowElement.PROP_SHOW_DATA_FLOWS;

	/**
	 * The current value for the {@link #PROP_SHOW_DATA_FLOWS} property.
	 */
	protected ShowDataFlowsProperty showDataFlowsProperty;

	protected boolean reactivating;

	@Override
	public void activate() {
		super.activate();
		if (!reactivating) {

			IEditorPart part = ((DefaultEditDomain) getDomain())
					.getEditorPart();
			if (part instanceof WFEditor) {

				WFEditor editor = (WFEditor) part;

				showSelectedDataFlows();
				// hide other connections
				IFigure connectionLayer = editor.getRootPanel().getLayer(
						LayerConstants.CONNECTION_LAYER);
				connectionLayer.setVisible(false);
			}
		}
	}

	@Override
	protected void applyProperty(Object key, Object value) {
		if (PROP_SHOW_DATA_FLOWS.equals(key)) {
			ShowDataFlowsProperty newValue = (ShowDataFlowsProperty) value;
			if (isActive()) {
				if (showDataFlowsProperty == null ? newValue != null
						: !showDataFlowsProperty.equals(newValue)) {
					showDataFlowsProperty = newValue;
					showSelectedDataFlows();
				}
			} else {
				showDataFlowsProperty = newValue;
			}

		}
		super.applyProperty(key, value);
	}

	@Override
	public void deactivate() {
		if (!reactivating) {
			hideDataFlows();
			IEditorPart part = ((DefaultEditDomain) getDomain())
					.getEditorPart();
			if (part instanceof WFEditor) {

				WFEditor editor = (WFEditor) part;
				// show other connections
				IFigure connectionLayer = editor.getRootPanel().getLayer(
						LayerConstants.CONNECTION_LAYER);
				connectionLayer.setVisible(true);
			}
		}

		super.deactivate();
	}

	protected WorkflowDiagram getDiagram() {
		IEditorPart part = ((DefaultEditDomain) getDomain()).getEditorPart();
		if (part instanceof WFEditor) {

			WFEditor editor = (WFEditor) part;
			WorkflowDiagram diagram = editor.getDiagram();
			return diagram;
		}
		return null;
	}

	protected void hideDataFlows() {

		WorkflowDiagram diagram = getDiagram();
		if (diagram != null) {
			for (IActivity act : diagram.getActivities().values()) {
				ShowDataFlowsProperty prop = (ShowDataFlowsProperty) act
						.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
				if (prop == null) {
					prop = new ShowDataFlowsProperty();
				}
				prop.setShowingFlows(false);
				act.setPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS, prop);
			}
			ShowDataFlowsProperty prop = (ShowDataFlowsProperty) diagram
					.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
			if (prop == null) {
				prop = new ShowDataFlowsProperty();
			}
			prop.setShowingFlows(false);
			diagram.setPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS, prop);

		}
	}

	@Override
	public void reactivate() {
		reactivating = true;
		super.reactivate();
		reactivating = false;
	}

	protected void showSelectedDataFlows() {

		WorkflowDiagram diagram = getDiagram();
		if (diagram != null) {
			for (IActivity act : diagram.getActivities().values()) {
				act.setPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS,
						showDataFlowsProperty.clone());
			}
			diagram.setPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS,
					showDataFlowsProperty.clone());
		}
	}

}
