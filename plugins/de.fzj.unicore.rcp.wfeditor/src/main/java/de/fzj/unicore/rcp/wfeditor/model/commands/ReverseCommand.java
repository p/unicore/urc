package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.gef.commands.Command;

/**
 * Command that executes another command in reverse order, i.e. it calls undo
 * while redoing and redo while undoing
 * 
 * @author bdemuth
 * 
 */
public class ReverseCommand extends Command {

	private Command wrapped;

	public ReverseCommand(Command wrapped) {
		super();
		this.wrapped = wrapped;
	}

	@Override
	public boolean canExecute() {
		return wrapped.canUndo();
	}

	@Override
	public boolean canUndo() {
		return true;
	}

	@Override
	public void dispose() {
		wrapped.dispose();
	}

	@Override
	public void execute() {
		wrapped.undo();
	}

	@Override
	public void redo() {
		wrapped.undo();
	}

	@Override
	public void undo() {
		wrapped.redo();
	}

}
