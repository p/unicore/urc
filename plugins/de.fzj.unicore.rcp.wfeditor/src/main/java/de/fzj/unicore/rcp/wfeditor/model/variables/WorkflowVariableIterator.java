package de.fzj.unicore.rcp.wfeditor.model.variables;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.model.conditions.ConditionTypeVariableComparison;
import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType;
import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionalElement;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowVariableUtils;

/**
 * @author demuth
 * 
 */
@XStreamAlias("WorkflowVariableIterator")
public class WorkflowVariableIterator extends PropertySource implements
		IConditionalElement, PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4609163432337108704L;

	// keys defining property names
	public static String PROP_ID = "id", PROP_NAME = "Name",
			PROP_FLOW_ELEMENT = "element",
			PROP_ITERATOR_VARIABLE = "Iterator variable",
			PROP_ITERATOR_MODIFIER = "Iterator modifier",
			PROP_ITERATOR_TERMINATE_CONDITION = "condition";

	public static final String DEFAULT_NAME = "Modifier";

	public WorkflowVariableIterator(IFlowElement flowElement) {
		setId(createRandomId());
		setFlowElement(flowElement);
		setVariable(new WorkflowVariable(flowElement));
		getVariable().setType(WorkflowVariable.VARIABLE_TYPE_INTEGER);
		setModifier(new WorkflowVariableModifier(flowElement));
		String expression = getVariable().getName() + "++;";
		getModifier().setInternalExpression(expression);
		getModifier().setDisplayedExpression(expression);
		getModifier().setDataSourceAndSinkVisible(false);
		setTerminateCondition(new Condition(flowElement, this));
		ConditionTypeVariableComparison conditionType = new ConditionTypeVariableComparison();
		conditionType.setComparator(IConditionType.INT_SMALLER);
		conditionType.setVariable(getVariable());
		conditionType.setCondition(getTerminateCondition());
		conditionType.setValue("5");
		getTerminateCondition().setSelectedType(conditionType);
		getVariable().addPersistentPropertyChangeListener(this);
		setName(getVariable().getName());
	}

	public WorkflowVariableIterator(String name, IFlowElement flowElement) {
		this(flowElement);
		setName(name);

	}

	@Override
	public void activate() {
		super.activate();
		updateElement();
		getVariable().activate();
		WorkflowVariableUtils.linkModifierToVariable(getModifier(),
				getVariable().getName());
		getModifier().activate();
		getTerminateCondition().activate();
	}

	@Override
	protected void addPropertyDescriptors() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		super.dispose();
		updateElement();
		getVariable().dispose();
		getModifier().dispose();
		getTerminateCondition().dispose();
	}

	@Override
	public WorkflowVariableIterator getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		WorkflowVariableIterator copy = (WorkflowVariableIterator) super
				.getCopy(toBeCopied, mapOfCopies, copyFlags);
		copy.setFlowElement(CloneUtils.getFromMapOrCopy(getFlowElement(),
				toBeCopied, mapOfCopies, copyFlags));
		copy.setId(createRandomId());
		return copy;
	}

	public IFlowElement getFlowElement() {
		return (IFlowElement) getPropertyValue(PROP_FLOW_ELEMENT);
	}

	/**
	 * A unique ID for referencing this Object.
	 */
	public String getId() {
		return (String) getPropertyValue(PROP_ID);
	}

	public WorkflowVariableModifier getModifier() {
		return (WorkflowVariableModifier) getPropertyValue(PROP_ITERATOR_MODIFIER);
	}

	public String getName() {
		return (String) getPropertyValue(PROP_NAME);
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_NAME);
		propertiesToCopy.add(PROP_ITERATOR_MODIFIER);
		propertiesToCopy.add(PROP_ITERATOR_TERMINATE_CONDITION);
		propertiesToCopy.add(PROP_ITERATOR_VARIABLE);
		return propertiesToCopy;
	}

	public Condition getTerminateCondition() {
		return (Condition) getPropertyValue(PROP_ITERATOR_TERMINATE_CONDITION);
	}

	public WorkflowVariable getVariable() {
		return (WorkflowVariable) getPropertyValue(PROP_ITERATOR_VARIABLE);
	}

	public void propertyChange(PropertyChangeEvent evt) {
		updateElement();
	}

	public void setFlowElement(IFlowElement element) {
		setPropertyValue(PROP_FLOW_ELEMENT, element);
	}

	public void setId(String id) {
		setPropertyValue(PROP_ID, id);
	}

	public void setModifier(WorkflowVariableModifier modifier) {
		setPropertyValue(PROP_ITERATOR_MODIFIER, modifier);
	}

	public void setName(String name) {

		setPropertyValue(PROP_NAME, name);
	}

	public void setTerminateCondition(Condition terminateCondition) {
		setPropertyValue(PROP_ITERATOR_TERMINATE_CONDITION, terminateCondition);
	}

	public void setVariable(WorkflowVariable variable) {
		setPropertyValue(PROP_ITERATOR_VARIABLE, variable);
	}

	@Override
	public void undoDispose() {
		super.undoDispose();
		if (getTerminateCondition().isDisposed()) {
			getTerminateCondition().undoDispose();
		}
		if (getModifier().isDisposed()) {
			getModifier().undoDispose();
		}
		if (getVariable().isDisposed()) {
			getVariable().undoDispose();
		}
		updateElement();

	}

	protected void updateElement() {
		// force update and inform listeners that the variable has been changed
		getFlowElement().setPropertyValue(IFlowElement.PROP_DECLARED_VARIABLES,
				getFlowElement().getVariableList());
	}
}
