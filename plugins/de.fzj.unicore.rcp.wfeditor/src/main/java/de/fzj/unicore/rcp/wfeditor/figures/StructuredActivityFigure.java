/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.graphics.Image;

import de.fzj.unicore.rcp.wfeditor.borders.TitleBarBorderWithImage;

/**
 * 
 * @author hudsonr Created on Jul 23, 2003
 */
public class StructuredActivityFigure extends Figure implements IActivityFigure {

	/**
	 * Small figure that is added over the actualFigure for visualizing the
	 * state of the activity. TODO
	 */
	private Image stateImage;
	private TitleBarBorderWithImage titleBar;
	private String label;
			
	public StructuredActivityFigure(Rectangle bounds) {
		this("",bounds);

	}
	
	public StructuredActivityFigure(String title, Rectangle bounds) {
		if(title != null) setLabel(title);
		setPreferredSize(bounds.getSize());
		setBounds(bounds);
		setBorder(createBorder());

	}



	public Border createBorder() {
		titleBar = new TitleBarBorderWithImage();
		titleBar.setFont(JFaceResources.getTextFont());
		titleBar.setLabel(getLabel());
		//		return new LineBorder(1);
		return titleBar;
	}

	// public IFigure getContents() {
	// return contents;
	// }

	/**
	 * @see org.eclipse.draw2d.Figure#getPreferredSize(int, int)
	 */
	// public Dimension getPreferredSize(int wHint, int hHint) {
	// Dimension dim = new Dimension();
	// dim.width = getContents().getPreferredSize().width;
	// dim.width += getInsets().getWidth();
	// dim.height =
	// getContents().getPreferredSize().height+getInsets().getHeight();
	// return dim;
	// }

	// public void setBounds(Rectangle rect) {
	// super.setBounds(rect);
	// rect = getClientArea(rect);
	//
	// contents.setLocation(header.getBounds().getBottomLeft());
	// contents.setSize(rect.width,rect.height-header.getBounds().height);
	// }

	public void setSelected(boolean value) {
	}

	/**
	 * Set the image for the figure. Replaces the old actual figure
	 * 
	 * @param imageDescr
	 */
	public void setStateImage(ImageDescriptor imageDescr) {
		if(stateImage != null)
		{
			stateImage.dispose();
			stateImage = null;
		}
		if(imageDescr != null && titleBar != null)
		{
			int titleBarHeight = titleBar.getInsets(this).top;
			
			stateImage = new Image(null,
					imageDescr.getImageData().scaledTo(titleBarHeight-2,titleBarHeight-2));
			titleBar.setImage(stateImage);
		}
		invalidate();
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
		if(titleBar != null)
		{
			titleBar.setLabel(label);
			invalidate();
		}
	}

}
