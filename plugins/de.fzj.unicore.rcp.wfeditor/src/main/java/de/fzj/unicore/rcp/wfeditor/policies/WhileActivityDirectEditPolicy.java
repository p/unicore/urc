/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.draw2d.Label;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.figures.WhileActivityFigure;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.commands.SetIterationIdCommand;
import de.fzj.unicore.rcp.wfeditor.parts.WhileActivityPart;

/**
 * 
 * @author demuth
 */
public class WhileActivityDirectEditPolicy extends ActivityDirectEditPolicy {

	@Override
	protected Command getDirectEditCommand(DirectEditRequest request) {
		String suffix = (String) request.getCellEditor().getValue();
		// cut off execution state summary
		Pattern pattern = Pattern
				.compile(WFConstants.REGEXP_ITERATION_EXECUTION_STATE);
		Matcher m = pattern.matcher(suffix);
		if (m.matches()) {
			suffix = m.group(1);
		}
		WhileActivityPart part = (WhileActivityPart) getHost();
		IFlowElement model = part.getModel().getBody();
		String prefix = model.getParent().getCurrentIterationId();
		String newValue;
		if (prefix.trim().length() > 0) {
			newValue = prefix + Constants.ITERATION_ID_SEPERATOR + suffix;
		} else {
			newValue = suffix;
		}
		SetIterationIdCommand cmd = new SetIterationIdCommand(model, newValue);
		return cmd;
	}

	@Override
	protected void revertOldEditValue(DirectEditRequest request) {

		Label label = ((WhileActivityFigure) getHostFigure())
				.getIterationLabel();
		WhileActivityPart part = (WhileActivityPart) getHost();
		IFlowElement model = part.getModel().getBody();
		String[] iterationIds = model.getCurrentIterationId().split(
				Constants.ITERATION_ID_SEPERATOR);
		String iterationId = iterationIds[iterationIds.length - 1];
		label.setText(iterationId);

		super.revertOldEditValue(request);
	}

	/**
	 * @see DirectEditPolicy#showCurrentEditValue(org.eclipse.gef.requests.DirectEditRequest)
	 */
	@Override
	protected void showCurrentEditValue(DirectEditRequest request) {
		String value = (String) request.getCellEditor().getValue();
		((WhileActivityFigure) getHostFigure()).getIterationLabel().setText(
				value);
		// hack to prevent async layout from placing the cell editor twice.
		getHostFigure().getUpdateManager().performUpdate();
	}

	@Override
	public boolean understandsRequest(Request r) {
		return false;
	}

}
