package de.fzj.unicore.rcp.wfeditor.policies;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.DropRequest;
import org.eclipse.gef.requests.ReconnectRequest;

import de.fzj.unicore.rcp.wfeditor.figures.RoundedPolylineConnection;
import de.fzj.unicore.rcp.wfeditor.layouts.DataFlowConnectionRouter;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.commands.CreateDataFlowCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.DeleteDataFlowCommand;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.parts.StructuredActivityPart;
import de.fzj.unicore.rcp.wfeditor.ui.WFRootEditPart;

public class DragDataEditPolicy extends GraphicalNodeEditPolicy {

	@Override
	protected Connection createDummyConnection(Request req) {
		return new RoundedPolylineConnection();
	}

	@Override
	protected Command getConnectionCompleteCommand(
			CreateConnectionRequest request) {

		if (request.getStartCommand() instanceof CreateDataFlowCommand
				&& (request.getTargetEditPart().getModel() instanceof IDataSink)) {
			CompoundCommand result = new CompoundCommand();
			CreateDataFlowCommand cmd = (CreateDataFlowCommand) request
					.getStartCommand();
			IDataSource source = (IDataSource) request.getSourceEditPart()
					.getModel();
			cmd.setSource(source);
			IDataSink sink = (IDataSink) request.getTargetEditPart().getModel();
			cmd.setSink(sink);
			result.add(cmd);
			return result;
		}
		return null;
	}

	@Override
	protected Command getConnectionCreateCommand(CreateConnectionRequest request) {
		CreateDataFlowCommand cmd = new CreateDataFlowCommand();
		request.setStartCommand(cmd);
		return cmd;
	}

	@Override
	protected ConnectionRouter getDummyConnectionRouter(
			CreateConnectionRequest request) {
		Object o = getHost().getModel();
		if (o instanceof IFlowElement) {
			WorkflowDiagram d = ((IFlowElement) o).getDiagram();
			Object part = getHost().getViewer().getEditPartRegistry().get(d);
			if (part instanceof StructuredActivityPart) {
				return ((StructuredActivityPart) part)
						.getConnectionRouter(WFRootEditPart.DATA_FLOW_LAYER);
			}
		}
		return new DataFlowConnectionRouter();
	}

	@Override
	protected Command getReconnectSourceCommand(ReconnectRequest request) {

		Object source = getHost().getModel();
		Object target = request.getConnectionEditPart().getTarget().getModel();
		Object connection = request.getConnectionEditPart().getModel();
		if (connection instanceof IDataFlow && source instanceof IDataSource
				&& target instanceof IDataSink) {
			CompoundCommand result = new CompoundCommand();
			DeleteDataFlowCommand deleteCmd = new DeleteDataFlowCommand();
			deleteCmd.setDataFlow((IDataFlow) connection);
			result.add(deleteCmd);
			CreateDataFlowCommand cmd = new CreateDataFlowCommand();
			;
			cmd.setSource((IDataSource) source);
			cmd.setSink((IDataSink) target);
			result.add(cmd);
			return result;
		}
		return null;
	}

	@Override
	protected Command getReconnectTargetCommand(ReconnectRequest request) {
		Object source = request.getConnectionEditPart().getSource().getModel();
		Object target = getHost().getModel();
		Object connection = request.getConnectionEditPart().getModel();
		if (connection instanceof IDataFlow && source instanceof IDataSource
				&& target instanceof IDataSink) {
			CompoundCommand result = new CompoundCommand();
			DeleteDataFlowCommand deleteCmd = new DeleteDataFlowCommand();
			deleteCmd.setDataFlow((IDataFlow) connection);
			result.add(deleteCmd);
			CreateDataFlowCommand cmd = new CreateDataFlowCommand();
			;
			cmd.setSource((IDataSource) source);
			cmd.setSink((IDataSink) target);
			result.add(cmd);
			return result;
		}
		return null;
	}

	@Override
	protected ConnectionAnchor getTargetConnectionAnchor(
			CreateConnectionRequest request) {
		EditPart target = request.getTargetEditPart();
		return (target instanceof NodeEditPart && target.getModel() instanceof IDataSink) ? ((NodeEditPart) target)
				.getTargetConnectionAnchor(request) : null;
	}

	@Override
	public EditPart getTargetEditPart(Request request) {
		if (REQ_CONNECTION_START.equals(request.getType())
				|| REQ_CONNECTION_END.equals(request.getType())) {
			return getHost();
		}
		return null;
	}

	@Override
	public void showSourceFeedback(Request request) {
		if (REQ_CONNECTION_END.equals(request.getType())) {
			if (request instanceof CreateConnectionRequest) {
				CreateConnectionRequest req = (CreateConnectionRequest) request;
				if (req.getSourceEditPart() == req.getTargetEditPart()) {
					return; // can't drag data to self
				}
			}
			showCreationFeedback((CreateConnectionRequest) request);
		}
	}

	/**
	 * Calls {@link #showTargetConnectionFeedback(DropRequest)} when
	 * appropriate.
	 * 
	 * @see org.eclipse.gef.EditPolicy#showTargetFeedback(Request)
	 */
	@Override
	public void showTargetFeedback(Request request) {
		super.showTargetFeedback(request);
	}

	@Override
	public boolean understandsRequest(Request request) {
		return REQ_CONNECTION_START.equals(request.getType())
				|| REQ_CONNECTION_END.equals(request.getType());
	}

}
