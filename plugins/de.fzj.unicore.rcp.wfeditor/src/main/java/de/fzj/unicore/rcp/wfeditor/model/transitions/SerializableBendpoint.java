package de.fzj.unicore.rcp.wfeditor.model.transitions;

import org.eclipse.draw2d.Bendpoint;
import org.eclipse.draw2d.geometry.Point;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Bendpoint")
public class SerializableBendpoint implements Bendpoint {

	private int x, y;
	private transient Point location;

	/**
	 * Creates a new SerializableBendpoint at the Point (x,y).
	 * 
	 * @param x
	 *            The X coordinate
	 * @param y
	 *            The Y coordinate
	 * @since 2.0
	 */
	public SerializableBendpoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * @see org.eclipse.draw2d.Bendpoint#getLocation()
	 */
	public Point getLocation() {
		if (location == null) {
			location = new Point(x, y);
		}
		return location;
	}

	@Override
	public String toString() {
		return getLocation().toString();
	}

}
