package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

public class ArrowFigure extends Figure {

	@Override
	protected void paintFigure(Graphics graphics) {

		Rectangle bounds = getBounds();

		Color c = ColorConstants.green;
		graphics.setBackgroundColor(c);
		int triangleHeight = bounds.height / 2;
		double sideOffset = (bounds.width / 3);
		int rectX = (int) (bounds.x + sideOffset);
		int rectY = bounds.y;
		int rectWidth = (int) (bounds.width - 2 * sideOffset);
		int rectHeight = bounds.height - triangleHeight;
		// Rectangle r = new Rectangle(rectX,rectY,rectWidth,rectHeight);
		// graphics.fillRectangle(r);
		// graphics.setForegroundColor(ColorConstants.black);
		// graphics.drawRectangle(getBounds());
		int[] arrowPoints = new int[] { rectX - bounds.width / 4,
				rectY + rectHeight - 1, rectX, rectY + rectHeight / 2, rectX,
				rectY, rectX + rectWidth, rectY, rectX + rectWidth,
				rectY + rectHeight / 2, rectX + rectWidth + bounds.width / 4,
				rectY + rectHeight - 1, rectX + rectWidth / 2,
				rectY + rectHeight + triangleHeight };
		graphics.fillPolygon(arrowPoints);
		graphics.drawPolygon(arrowPoints);
		// graphics.setForegroundColor(ColorConstants.red);
		// graphics.drawRectangle(bounds);
	}

}
