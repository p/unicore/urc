/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;

/**
 * Command to rename Activities.
 * 
 * @author Daniel Lee
 */
public class RenameFlowElementCommand extends Command {

	private IFlowElement element;
	private String name, oldName;

	private boolean renameSuccessful = false;

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		renameSuccessful = element.setName(name);
	}

	/**
	 * Sets the source Activity
	 * 
	 * @param activity
	 *            the source Activity
	 */
	public void setElement(IFlowElement element) {
		this.element = element;
	}

	/**
	 * Sets the new Activity name
	 * 
	 * @param string
	 *            the new name
	 */
	public void setName(String string) {
		name = string;
	}

	/**
	 * Sets the old Activity name
	 * 
	 * @param string
	 *            the old name
	 */
	public void setOldName(String string) {
		oldName = string;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		element.setName(oldName);
		renameSuccessful = false;
	}

	public boolean wasRenameSuccessful() {
		return renameSuccessful;
	}

}
