/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LayoutManager;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.draw2d.graph.Subgraph;
import org.eclipse.draw2d.text.FlowPage;
import org.eclipse.draw2d.text.TextFlow;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.requests.DirectEditRequest;
import org.eclipse.gef.tools.DirectEditManager;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.properties.IPropertySource;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.figures.ActivityFigure;
import de.fzj.unicore.rcp.wfeditor.figures.CenteredConnectionAnchor;
import de.fzj.unicore.rcp.wfeditor.figures.IActivityFigure;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.commands.SetConstraintCommand;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.policies.ActivityEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.ActivityNodeEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.ActivitySourceEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.CopyEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.CutEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.PasteEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.RemoveIllegalTransitionsEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.ShowDataSinksAndSourcesEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.ShowPropertyViewPolicy;
import de.fzj.unicore.rcp.wfeditor.requests.WFEditorRequest;
import de.fzj.unicore.rcp.wfeditor.ui.WFGraphicalViewer;
import de.fzj.unicore.rcp.wfeditor.ui.WFRootEditPart;
import de.fzj.unicore.rcp.wfeditor.utils.WFDragEditPartsTracker;

/**
 * @author hudsonr Created on Jun 30, 2003
 */
public abstract class ActivityPart extends AbstractWFEditPart implements
PropertyChangeListener, NodeEditPart, IFlowElementPart {

	protected DirectEditManager manager;

	/**
	 * @see org.eclipse.gef.EditPart#activate()
	 */
	@Override
	public void activate() {
		refresh();
		getModel().addPropertyChangeListener(this);

		if (!getModel().isDisposed()) {
			getModel().activate();
		} else {
			getModel().undoDispose();
		}
		super.activate();
	}
	@Override
	public IFigure getFigure() {
		boolean hadNoFigure = figure == null;
		IFigure result = super.getFigure();
		if (hadNoFigure && result instanceof IActivityFigure) {
			IActivityFigure fig = (IActivityFigure) result;
			fig.setLabel(getModel().getName());
		}
		return result;
	}



	public void updateToolTip() {

		String toolTip = "";
		if (getToolTipText() != null) {
			toolTip = getToolTipText();
		}
		TextFlow tf = new TextFlow(toolTip);
		tf.setForegroundColor(ColorConstants.tooltipForeground);
		Font systemFont = Display.getDefault().getSystemFont();
		tf.setFont(systemFont);
		FlowPage fp = new FlowPage();
		fp.add(tf);
		Panel panel = new Panel();
		panel.setBorder(new LineBorder(ColorConstants.tooltipForeground, 1));
		panel.add(fp);
		panel.setLayoutManager(new StackLayout());
		panel.setPreferredSize(200, 50);
		panel.setBackgroundColor(ColorConstants.tooltipBackground);

		fp.getLayoutManager().layout(panel);
		
		Dimension preferred = tf.getSize();
		// if(preferred.height > 500) preferred.height = 500;
		// if(preferred.width> 500) preferred.width = 500;
		tf.setSize(preferred);
		panel.setPreferredSize(preferred.width+5,preferred.height+5);
		getFigure().setToolTip(panel);
	}

	protected String getToolTipText() {
		IActivity activity = getModel();
		if(activity == null) return null;
		String result = activity.getCurrentExecutionStateDescription();
		if (result == null) {
			result = activity.getCurrentExecutionStateName();
		}
		return result;
	}

	@Override
	protected void addChildVisual(EditPart childEditPart, int index) {
		boolean isDataSourceList = childEditPart instanceof DataSourceListPart;
		boolean isDataSinkList = childEditPart instanceof DataSinkListPart;
		if (isDataSourceList || isDataSinkList) {
			IFigure dataLayer = getLayer(WFRootEditPart.DATA_FLOW_LAYER);
			IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
			ShowDataFlowsProperty prop = (ShowDataFlowsProperty) getModel()
			.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
			boolean showChild = prop != null
			&& prop.isShowingAnyDataTypeAtAll()
			&& (isDataSourceList ? prop.isShowingSources() : prop
					.isShowingSinks());
			child.setVisible(showChild);
			dataLayer.add(child);
		} else {
			IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
			getContentPane().add(child);
		}
	}

	public void afterDeserialization() {
		
		updateToolTip();

	}

	public void afterSerialization() {
		
	}

	protected boolean allowsDirectEdit(Point requestLoc) {
		return getModel().getDiagram().isEditable();
	}

	public void applyLayout(CompoundDirectedGraph graph, Map<?, ?> map,
			CompoundCommand cmd) {
		Node n = (Node) map.get(this);
		SetConstraintCommand myCmd = new SetConstraintCommand();
		myCmd.setPart(getModel());
		myCmd.setLocation(new Point(n.x, n.y));
		myCmd.setSize(new Dimension(n.width, n.height));
		cmd.add(myCmd);

		for (int i = 0; i < getSourceConnections().size(); i++) {
			TransitionPart trans = (TransitionPart) getSourceConnections().get(
					i);
			trans.applyGraphResults(graph, map, cmd);
		}
	}

	public void beforeSerialization() {
		
	}

	public void contributeEdgesToGraph(CompoundDirectedGraph graph, Subgraph s,
			Map<?, ?> map) {
		List<?> outgoing = getSourceConnections();
		for (int i = 0; i < outgoing.size(); i++) {
			TransitionPart part = (TransitionPart) getSourceConnections()
			.get(i);
			part.contributeToGraph(graph, s, map);
		}
		Object o = map.get(this);
		if (o instanceof Subgraph) {
			for (int i = 0; i < getChildren().size(); i++) {
				ActivityPart child = (ActivityPart) children.get(i);
				child.contributeEdgesToGraph(graph, (Subgraph) o, map);
			}
		}
	}

	public abstract void contributeNodesToGraph(CompoundDirectedGraph graph,
			Subgraph s, Map<?, ?> map);

	@Override
	protected EditPart createChild(Object model) {
		// Make sure the edit part doesn't exist, yet.
		// If one exists, reuse it.
		// This is a bit of a hack that is introduced cause in our editor,
		// the part already gets created by the Create command.
		EditPart existing = (EditPart) getViewer().getEditPartRegistry().get(
				model);
		if (existing != null) {
			return existing;
		} else {
			return getViewer().getEditPartFactory().createEditPart(this, model);
		}
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE,
				new ActivityNodeEditPolicy());
		installEditPolicy(EditPolicy.CONTAINER_ROLE,
				new ActivitySourceEditPolicy());
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new ActivityEditPolicy());
		installEditPolicy(WFConstants.POLICY_OPEN, new ShowPropertyViewPolicy());
		installEditPolicy(WFConstants.POLICY_COPY, new CopyEditPolicy());
		installEditPolicy(WFConstants.POLICY_CUT, new CutEditPolicy());
		installEditPolicy(WFConstants.POLICY_PASTE, new PasteEditPolicy());
		installEditPolicy(WFConstants.POLICY_REMOVE_ILLEGAL_TRANSITIONS,
				new RemoveIllegalTransitionsEditPolicy());
		installEditPolicy(WFConstants.POLICY_SHOW_DATA_SINKS_AND_SOURCES,
				new ShowDataSinksAndSourcesEditPolicy(this));
		// installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new
		// ActivityDirectEditPolicy());
	}

	/**
	 * @see org.eclipse.gef.EditPart#deactivate()
	 */
	@Override
	public void deactivate() {
		super.deactivate();
		getModel().removePropertyChangeListener(this);
		if (getFigure() instanceof ActivityFigure) {
			((ActivityFigure) getFigure()).dispose();
		}
	}

	/**
	 * Returns the Activity model associated with this EditPart
	 * 
	 * @return the Activity model
	 */
	public IActivity getModel() {
		return (IActivity) super.getModel();
	}

	/**
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class adapter) {
		if (adapter == IPropertySource.class) {
			return getModel();
		}
		return super.getAdapter(adapter);
	}

	protected abstract int getAnchorOffset();

	@Override
	public Command getCommand(Request request) {
		Command cmd = null;
		// Disable editing completely if workflow diagram disallows editing
		WorkflowDiagram diagram = getModel().getDiagram();

		if (diagram == null
				|| diagram.isEditable()
				|| REQ_OPEN.equals(request.getType())
				|| REQ_DIRECT_EDIT.equals(request.getType())
				|| ((request instanceof WFEditorRequest) && ((WFEditorRequest) request)
						.isExecutable(diagram))) {
			cmd = super.getCommand(request);
		} else {
			cmd = null;
		}
		return cmd;

	}

	/**
	 * Get an anchor at the given location for this edit part.
	 * 
	 * This must be called after the figure for this edit part has been created.
	 * 
	 * @param location
	 * @return
	 */
	public ConnectionAnchor getConnectionAnchor(int location) {
		return new CenteredConnectionAnchor(getFigure(), location, 0);
	}

	@Override
	public DragTracker getDragTracker(Request request) {
		return new WFDragEditPartsTracker(this);
	}

	@Override
	protected List<PropertySource> getModelChildren() {
		List<PropertySource> result = new ArrayList<PropertySource>();
		result.add(getModel().getDataSourceList());
		result.add(getModel().getDataSinkList());
		return result;
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#getModelSourceConnections()
	 */
	@Override
	protected List<?> getModelSourceConnections() {
		return getModel().getOutgoingTransitions();
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#getModelTargetConnections()
	 */
	@Override
	protected List<?> getModelTargetConnections() {
		return getModel().getIncomingTransitions();
	}

	/**
	 * @see NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	public ConnectionAnchor getSourceConnectionAnchor(
			ConnectionEditPart connection) {
		return new ChopboxAnchor(getFigure());
	}

	/**
	 * @see org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.Request)
	 */
	public ConnectionAnchor getSourceConnectionAnchor(Request request) {
		return new ChopboxAnchor(getFigure());
	}

	/**
	 * @see NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	public ConnectionAnchor getTargetConnectionAnchor(
			ConnectionEditPart connection) {
		return new ChopboxAnchor(getFigure());
	}

	/**
	 * @see org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.Request)
	 */
	public ConnectionAnchor getTargetConnectionAnchor(Request request) {
		return new ChopboxAnchor(getFigure());
	}

	@Override
	public WFGraphicalViewer getViewer() {
		try {
			return (WFGraphicalViewer) super.getViewer();
		} catch (NullPointerException e) {
			return null;
		}
	}

	protected void performDirectEdit() {

	}

	/**
	 * @see org.eclipse.gef.EditPart#performRequest(org.eclipse.gef.Request)
	 */
	@Override
	public void performRequest(Request request) {

		boolean directEdit = REQ_DIRECT_EDIT.equals(request.getType())
		&& (!(request instanceof DirectEditRequest) || allowsDirectEdit(((DirectEditRequest) request)
				.getLocation().getCopy()));
		if (directEdit) {
			performDirectEdit();
		} else if (REQ_OPEN.equals(request.getType())) {
			Command cmd = getCommand(request);
			if (cmd != null && cmd.canExecute()) {
				cmd.execute();
			}
		}
	}

	/**
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();

		if (IFlowElement.CHILDREN.equals(prop)) {
			refreshChildren();
		} else if (IFlowElement.INPUTS.equals(prop)) {
			refreshTargetConnections();
		} else if (IFlowElement.OUTPUTS.equals(prop)) {
			refreshSourceConnections();
		} else {
			Display d = PlatformUI.getWorkbench().getDisplay();
			if (!d.isDisposed()) {
				d.asyncExec(new Runnable() {
					public void run() {

						if (IFlowElement.PROP_NAME.equals(prop)) {
							updateLabelInFigure((String) evt.getNewValue());
						} else if (IFlowElement.PROP_VARIABLE_MODIFIERS
								.equals(prop)
								|| IFlowElement.PROP_DECLARED_VARIABLES
								.equals(prop)) {
							refreshChildren();
						}
						else if (IFlowElement.PROP_EXECUTION_STATE.equals(prop)
								|| IFlowElement.PROP_CURRENT_ITERATION_ID.equals(prop)) {
							updateToolTip();
						}

						refreshVisuals();
						if (getParent() != null) {
							// Causes parent to re-layout
							try {
								((GraphicalEditPart) getParent()).getFigure()
								.revalidate();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				});
			}
		}

	}

	/**
	 * Updates the visual aspect of this.
	 */
	@Override
	protected void refreshVisuals() {
		if (getParent() == null || getModel() == null) {
			return;
		}

		visualizeState();
		visualizeBreakpoint();
		updateBounds();
		getFigure().repaint();
	}

	@Override
	protected void removeChildVisual(EditPart childEditPart) {
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#toString()
	 */
	@Override
	public String toString() {
		return getModel().toString();

	}

	protected void updateLabelInFigure(String label) {
		if (getFigure() instanceof IActivityFigure) {
			((IActivityFigure) getFigure()).setLabel(label);
		}
	}

	protected abstract void visualizeBreakpoint();


	protected void updateBounds()
	{
		Point loc = getModel().getLocation();
		Dimension size = getModel().getSize();
		Rectangle r = new Rectangle(loc, size);
		GraphicalEditPart parent = (GraphicalEditPart) getParent();
		LayoutManager layouter = parent.getFigure().getLayoutManager();
		if(layouter != null)
		{
			Object o = layouter.getConstraint(getFigure());
			if(!r.equals(o))
			{
				parent.setLayoutConstraint(this,
						getFigure(), r);
				layouter.layout(parent.getFigure());
			}
		}
	}

	protected void visualizeState() {
		IFigure figure = getFigure();
		if(figure instanceof IActivityFigure)
		{
			IActivityFigure activityFigure = (IActivityFigure) figure;
			switch (getModel().getCurrentExecutionStateCipher()) {
			case ExecutionStateConstants.STATE_SUBMITTING:
				activityFigure.setStateImage(WFActivator
						.getImageDescriptor("hourglass.png"));
				break;
			case ExecutionStateConstants.STATE_RUNNING:
				activityFigure.setStateImage(WFActivator
						.getImageDescriptor("running.png"));
				break;
			case ExecutionStateConstants.STATE_FAILED:
				activityFigure.setStateImage(WFActivator
						.getImageDescriptor("failed.png"));
				break;
			case ExecutionStateConstants.STATE_SUCCESSFUL:
				activityFigure.setStateImage(WFActivator
						.getImageDescriptor("successful.png"));
				break;

			default:
				activityFigure.setStateImage(null);
				break;
			}
		}
	}

}
