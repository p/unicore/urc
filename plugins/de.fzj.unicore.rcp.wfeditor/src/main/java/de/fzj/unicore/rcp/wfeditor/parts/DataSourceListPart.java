package de.fzj.unicore.rcp.wfeditor.parts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ManhattanConnectionRouter;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.figures.DataSourceListFigure;
import de.fzj.unicore.rcp.wfeditor.figures.RoundedPolylineConnection;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSourceList;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.policies.ShowDataSinksAndSourcesEditPolicy;
import de.fzj.unicore.rcp.wfeditor.ui.WFRootEditPart;

public class DataSourceListPart extends AbstractWFEditPart implements
		PropertyChangeListener {

	private boolean visible = false;

	private static final int ARROW_PANEL_HEIGHT = 14;

	/**
	 * A figure that is used to depict the data flow from the sources' parent
	 * activity to the sources.
	 */
	protected IFigure arrowPanel;

	protected FlowLayout layout;

	public DataSourceListPart() {
		super();
	}

	@Override
	public void activate() {
		super.activate();
		ShowDataFlowsProperty prop = (ShowDataFlowsProperty) getActivity()
				.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		visible = prop != null && prop.isShowingSources()
				&& prop.isShowingAnyDataTypeAtAll();
		getFigure().setVisible(visible);
		refreshChildren();
		refreshVisuals();
		getActivity().addPropertyChangeListener(this);
		getModel().addPropertyChangeListener(this);
	}

	protected Rectangle computeBounds(int numSources) {
		Point location = getActivity().getLocation();
		Dimension size = getActivity().getSize();
		int y = location.y + size.height;
		if (numSources == 0) {
			return new Rectangle(location.x + size.width / 2, y, 0, 0);
		}

		else {
			int totalChildrenWidth = 0;
			for (Object o : getChildren()) {
				if (o instanceof GraphicalEditPart) {
					GraphicalEditPart e = (GraphicalEditPart) o;
					totalChildrenWidth += e.getFigure().getPreferredSize().width;
				}
			}

			int totalWidth = totalChildrenWidth
					+ getBetweenSourcesPadding(numSources) * (numSources - 1);
			int totalX = location.x + size.width / 2 - totalWidth / 2;
			int totalHeight = WFConstants.DEFAULT_DATA_CONNECTOR_HEIGHT
					+ ARROW_PANEL_HEIGHT;
			return new Rectangle(totalX, y, totalWidth, totalHeight);
		}

	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(WFConstants.POLICY_SHOW_DATA_SINKS_AND_SOURCES,
				new ShowDataSinksAndSourcesEditPolicy(getParent()));
	}

	@Override
	protected IFigure createFigure() {
		IFigure result = new DataSourceListFigure(getParent());
		layout = new FlowLayout();
		layout.setHorizontal(true);
		layout.setMajorSpacing(0);
		result.setLayoutManager(layout);
		return result;
	}

	@Override
	public void deactivate() {
		super.deactivate();
		getActivity().removePropertyChangeListener(this);
		getModel().removePropertyChangeListener(this);
	}

	protected IActivity getActivity() {

		return getParent().getModel();
	}

	protected int getBetweenSourcesPadding(int numSinks) {
		return WFConstants.DEFAULT_BETWEEN_SOURCES_PADDING;
	}

	@Override
	public DataSourceList getModel() {
		return (DataSourceList) super.getModel();
	}

	@Override
	protected List<IDataSource> getModelChildren() {
		List<IDataSource> result = new ArrayList<IDataSource>();
		if (visible) {
			for (IDataSource source : getModel().getDataSources()) {
				if (source.isVisible()) {
					result.add(source);
				}
			}
			Collections.sort(result);
		}
		return result;
	}

	@Override
	public ActivityPart getParent() {
		return (ActivityPart) super.getParent();
	}

	public void propertyChange(final PropertyChangeEvent evt) {
		Display d = PlatformUI.getWorkbench().getDisplay();
		if (!d.isDisposed()) {
			d.asyncExec(new Runnable() {
				public void run() {
					if (!isActive()) {
						return; // we might have become disposed of in the
								// meantime
					}
					if (evt.getSource() == getActivity()) {
						if (IFlowElement.PROP_SHOW_DATA_FLOWS.equals(evt
								.getPropertyName())) {
							ShowDataFlowsProperty prop = (ShowDataFlowsProperty) evt
									.getNewValue();
							visible = prop == null ? false : prop
									.isShowingSources()
									&& prop.isShowingAnyDataTypeAtAll();

							refreshChildren();
							refreshVisuals();

							getFigure().setVisible(visible);

						} else {
							refreshChildren();
							refreshVisuals();
						}
					} else if (evt.getSource() == getModel()) {
						refreshChildren();
						refreshVisuals();
					}
				}
			});
		}
	}

	@Override
	public void refresh() {
		refreshChildren(); // we need to refresh children first before
							// refreshing our visuals
		refreshVisuals();
	}

	@Override
	protected void refreshVisuals() {
		if (getFigure() != null && getFigure().getParent() != null) {
			int numChildren = getChildren().size();
			Rectangle bounds = computeBounds(numChildren);
			layout.setMinorSpacing(getBetweenSourcesPadding(numChildren));
			getFigure().getParent().setConstraint(getFigure(), bounds);
			if (arrowPanel != null) {
				getFigure().remove(arrowPanel);
			}
			arrowPanel = new Panel();
			arrowPanel.setPreferredSize(new Dimension(bounds.width,
					ARROW_PANEL_HEIGHT));
			getFigure().add(arrowPanel, 0);

			for (Object o : getChildren()) {

				if (o instanceof GraphicalEditPart) {

					GraphicalEditPart part = (GraphicalEditPart) o;
					RoundedPolylineConnection conn = new RoundedPolylineConnection();
					arrowPanel.add(conn);
					conn.setConnectionRouter(new ManhattanConnectionRouter());
					conn.setSourceAnchor(new TopAnchor(getParent().getFigure(),
							-1));
					conn.setTargetAnchor(new BottomAnchor(part.getFigure(), -1));
					conn.setTargetDecoration(new PolygonDecoration());
				}
			}
		}
		super.refreshVisuals();
		for (Object o : getChildren()) {
			if (o instanceof EditPart) {
				EditPart part = (EditPart) o;
				part.refresh();
			}
		}
	}

	@Override
	public void removeNotify() {
		IFigure dataLayer = getLayer(WFRootEditPart.DATA_FLOW_LAYER);
		if (dataLayer.getChildren().contains(getFigure())) {
			dataLayer.remove(getFigure());
		}
		super.removeNotify();
	}

}
