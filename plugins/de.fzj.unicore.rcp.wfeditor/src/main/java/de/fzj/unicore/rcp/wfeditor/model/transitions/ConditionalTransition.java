/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.transitions;

import java.util.Map;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionalElement;

/**
 * @author demuth
 * 
 */
@XStreamAlias("ConditionalTransition")
public class ConditionalTransition extends Transition implements
		IConditionalElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7264067810418528203L;

	/**
	 * @param source
	 * @param target
	 */
	public ConditionalTransition(IActivity source, IActivity target) {
		super(source, target);
		setCondition(new Condition(this, this));
	}

	@Override
	protected void addPropertyDescriptors() {
		super.addPropertyDescriptors();
	}

	@Override
	public void dispose() {
		super.dispose();
		getCondition().dispose();
	}

	/**
	 * Convenience method for retrieving the condition.
	 */
	public Condition getCondition() {
		return (Condition) getPropertyValue(Condition.PROP_CONDITION);
	}

	@Override
	public ConditionalTransition getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		return (ConditionalTransition) super.getCopy(toBeCopied, mapOfCopies,
				copyFlags);
	}

	@Override
	public String getLabelText() {
		return getCondition().getDisplayedExpression();
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(Condition.PROP_CONDITION);
		return propertiesToCopy;
	}

	private Object readResolve() {
		return this;
	}

	/**
	 * Convenience method for setting the condition.
	 */
	public void setCondition(Condition condition) {
		setPropertyValue(Condition.PROP_CONDITION, condition);
	}

	public void setPropertyValue(String key, Object value) {
		super.setPropertyValue(key, value);
	}

	@Override
	public void undoDispose() {
		super.undoDispose();
		if (getCondition().isDisposed()) {
			getCondition().undoDispose();
		}
	}

	private Object writeReplace() {
		return this;
	}
}
