/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.draw2d.Bendpoint;

import de.fzj.unicore.rcp.wfeditor.model.transitions.SerializableBendpoint;

public class MoveBendpointCommand extends BendpointCommand {

	private Bendpoint oldBendpoint;

	@Override
	public void execute() {
		Bendpoint bp = new SerializableBendpoint(getLocation().x,
				getLocation().y);

		setOldBendpoint(getTransition().getBendpoints().get(getIndex()));
		getTransition().setBendpoint(getIndex(), bp);
		super.execute();
	}

	protected Bendpoint getOldBendpoint() {
		return oldBendpoint;
	}

	public void setOldBendpoint(Bendpoint bp) {
		oldBendpoint = bp;
	}

	@Override
	public void undo() {
		super.undo();
		getTransition().setBendpoint(getIndex(), getOldBendpoint());
	}

}
