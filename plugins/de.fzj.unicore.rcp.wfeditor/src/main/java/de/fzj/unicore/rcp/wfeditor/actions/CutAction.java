package de.fzj.unicore.rcp.wfeditor.actions;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.requests.GroupRequest;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;

import de.fzj.unicore.rcp.wfeditor.WFConstants;

public class CutAction extends WFEditorAction {

	public CutAction(IWorkbenchPart part) {
		super(part);

	}

	public CutAction(IWorkbenchPart part, int style) {
		super(part, style);

	}

	@Override
	protected boolean calculateEnabled() {
		Command cmd = createCutCommand(getSelectedObjects());
		if (cmd == null) {
			return false;
		}
		return cmd.canExecute();
	}

	public Command createCutCommand(List<?> objects) {
		if (objects.isEmpty()) {
			return null;
		}
		if (!(objects.get(0) instanceof EditPart)) {
			return null;
		}

		GroupRequest req = new GroupRequest(WFConstants.REQ_CUT);
		req.setEditParts(objects);

		CompoundCommand compoundCmd = new CompoundCommand();
		for (int i = 0; i < objects.size(); i++) {
			EditPart object = (EditPart) objects.get(i);
			Command cmd = object.getCommand(req);
			if (cmd != null) {
				compoundCmd.add(cmd);
			}
		}

		return compoundCmd;
	}

	@Override
	protected void init() {
		setId(ActionFactory.CUT.getId());
		setText("Cut");
		setToolTipText("Cut selected elements from the diagram");
		ISharedImages sharedImages = PlatformUI.getWorkbench()
				.getSharedImages();
		setImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_CUT));
		setDisabledImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_CUT));
		setEnabled(false);
	}

	/**
	 * Performs the cut to clipboard action on the selected objects.
	 */
	@Override
	public void run() {
		execute(createCutCommand(getSelectedObjects()));
	}

}
