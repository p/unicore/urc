/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.structures;

import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.eclipse.draw2d.geometry.Dimension;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IfElseSplitActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.SimpleActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.ConditionalTransition;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

/**
 * @author demuth
 * 
 */
@XStreamAlias("IfElseActivity")
public class IfElseActivity extends CollapsibleActivity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1846085315478012001L;

	public static QName TYPE = new QName("http://www.unicore.eu/",
			"IfElseActivity");

	protected IfElseSplitActivity splitActivity;
	protected StructuredActivity ifBody, elseBody;

	public IfElseActivity() {
		setCanAddChildren(false);
		setSize(new Dimension(300, 300));

		splitActivity = new IfElseSplitActivity();

		setStartActivity(splitActivity);
		this.addChild(getSplitActivity());
		splitActivity.setDeletable(false);
		splitActivity.setBoundToParent(true);

		StructuredActivity ifBody = new ContainerActivity();
		setIfBody(ifBody);
		this.addChild(ifBody);
		ifBody.setDeletable(false);
		ifBody.setBoundToParent(true);

		StructuredActivity elseBody = new ContainerActivity();
		setElseBody(elseBody);
		this.addChild(elseBody);
		elseBody.setDeletable(false);
		elseBody.setBoundToParent(true);

		// now create transitions
		ConditionalTransition t1 = new ConditionalTransition(splitActivity,
				ifBody);
		t1.setDeletable(false);
		t1.setBoundToParent(true);

		Transition t2 = new Transition(splitActivity, elseBody);
		t2.setLabelText("  else  ");
		t2.setDeletable(false);
		t2.setBoundToParent(true);
		initStructure();
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public IfElseActivity getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		IfElseActivity result = (IfElseActivity) super.getCopy(toBeCopied,
				mapOfCopies, copyFlags);
		result.splitActivity = CloneUtils.getFromMapOrCopy(splitActivity,
				toBeCopied, mapOfCopies, copyFlags);
		result.ifBody = CloneUtils.getFromMapOrCopy(ifBody, toBeCopied,
				mapOfCopies, copyFlags);
		result.elseBody = CloneUtils.getFromMapOrCopy(elseBody, toBeCopied,
				mapOfCopies, copyFlags);
		return result;
	}

	public StructuredActivity getElseBody() {
		return elseBody;
	}

	public StructuredActivity getIfBody() {
		return ifBody;
	}

	public SimpleActivity getSplitActivity() {
		return splitActivity;
	}

	@Override
	public QName getType() {
		return TYPE;
	}

	@Override
	public void init() {
		super.init();
		WorkflowDiagram diagram = getDiagram();
		getSplitActivity().setDiagram(diagram);
		getIfBody().setDiagram(diagram);
		getElseBody().setDiagram(diagram);
		setName(getDiagram().getUniqueActivityName("IfActivity"));
		getSplitActivity().setName(
				getDiagram().getUniqueActivityName("IfSplit"));
		getIfBody().setName(getDiagram().getUniqueActivityName("IfBody"));
		getElseBody().setName(getDiagram().getUniqueActivityName("ElseBody"));
		getSplitActivity().init();
		getIfBody().init();
		getElseBody().init();
	}

	protected void initStructure() {

	}

	public void setElseBody(StructuredActivity elseBody) {
		this.elseBody = elseBody;
	}

	public void setIfBody(StructuredActivity ifBody) {
		this.ifBody = ifBody;
	}

	@Override
	public void setSize(Dimension size) {
		super.setSize(size);
	}

	@Override
	public void undoDispose() {
		super.undoDispose();
	}

}
