package de.fzj.unicore.rcp.wfeditor;

import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.PlatformUI;

public class WFColorConstants {

	public static final Color TRANSITION_LABEL_BACKGROUND = new Color(
			PlatformUI.getWorkbench().getDisplay(), 230, 230, 230);
	public static final Color CONTAINER_HIGHLIGHT = new Color(null, 200, 200,
			240);
}
