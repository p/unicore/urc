/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.gef.requests.ReconnectRequest;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.commands.ConnectionCreateCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.ReconnectSourceCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.ReconnectTargetCommand;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.parts.ActivityPart;

/**
 * 
 * Created on Jul 17, 2003
 */
public class ActivityNodeEditPolicy extends GraphicalNodeEditPolicy {

	/**
	 * Returns the model associated with the EditPart on which this EditPolicy
	 * is installed
	 * 
	 * @return the model
	 */
	protected IActivity getActivity() {
		return (IActivity) getHost().getModel();
	}

	/**
	 * Returns the ActivityPart on which this EditPolicy is installed
	 * 
	 * @return the
	 */
	protected ActivityPart getActivityPart() {
		return (ActivityPart) getHost();
	}

	/**
	 * @see GraphicalNodeEditPolicy#getConnectionCompleteCommand(CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCompleteCommand(
			CreateConnectionRequest request) {
		if (request.getStartCommand() instanceof ConnectionCreateCommand) {
			ConnectionCreateCommand cmd = (ConnectionCreateCommand) request
					.getStartCommand();
			cmd.setTarget(getActivity());
			return cmd;
		} else {
			return null;
		}
	}

	/**
	 * @see GraphicalNodeEditPolicy#getConnectionCreateCommand(CreateConnectionRequest)
	 */
	@Override
	protected Command getConnectionCreateCommand(CreateConnectionRequest request) {
		ConnectionCreateCommand cmd = new ConnectionCreateCommand();
		cmd.setSource(getActivity());
		request.setStartCommand(cmd);
		return cmd;
	}

	/**
	 * @see GraphicalNodeEditPolicy#getReconnectSourceCommand(ReconnectRequest)
	 */
	@Override
	protected Command getReconnectSourceCommand(ReconnectRequest request) {
		ReconnectSourceCommand cmd = new ReconnectSourceCommand();
		Object model = request.getConnectionEditPart().getModel();
		if (!(model instanceof Transition)) {
			return null;
		}
		cmd.setTransition((Transition) model);
		cmd.setSource(getActivity());
		return cmd;
	}

	/**
	 * @see GraphicalNodeEditPolicy#getReconnectTargetCommand(ReconnectRequest)
	 */
	@Override
	protected Command getReconnectTargetCommand(ReconnectRequest request) {
		ReconnectTargetCommand cmd = new ReconnectTargetCommand();
		Object model = request.getConnectionEditPart().getModel();
		if (!(model instanceof Transition)) {
			return null;
		}
		cmd.setTransition((Transition) model);
		cmd.setTarget(getActivity());
		return cmd;
	}

}
