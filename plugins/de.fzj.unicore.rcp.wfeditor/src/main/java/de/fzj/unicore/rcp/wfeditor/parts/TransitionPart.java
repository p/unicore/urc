/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.Bendpoint;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Locator;
import org.eclipse.draw2d.MidpointLocator;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Edge;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.draw2d.graph.Subgraph;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.gef.editpolicies.ConnectionEndpointEditPolicy;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.properties.IPropertySource;

import de.fzj.unicore.rcp.wfeditor.WFColorConstants;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.figures.TransitionFigure;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.policies.ShowPropertyViewPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.TransitionBendpointEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.TransitionEditPolicy;
import de.fzj.unicore.rcp.wfeditor.requests.WFEditorRequest;
import de.fzj.unicore.rcp.wfeditor.ui.WFGraphicalViewer;
import de.fzj.unicore.rcp.wfeditor.utils.WFDragEditPartsTracker;

/**
 * @author hudsonr Created on Jul 16, 2003
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class TransitionPart extends AbstractConnectionEditPart implements
		PropertyChangeListener, IFlowElementPart {

	protected Label label;
	protected Locator labelLocator;

	@Override
	public void activate() {
		super.activate();
		if (!getTransition().isDisposed()) {
			getTransition().activate();
		} else {
			getTransition().undoDispose();
		}
		getTransition().addPropertyChangeListener(this);
	}

	public void afterDeserialization() {

	}

	public void afterSerialization() {

	}

	protected void applyGraphResults(CompoundDirectedGraph graph, Map map,
			CompoundCommand cmd) {
		// after auto-layout: remove all bendpoints
		// transitions are later routed with connection routers
		getTransition().setBendpoints(new ArrayList<Bendpoint>());
		

	}

	public void beforeSerialization() {

	}

	public void contributeToGraph(CompoundDirectedGraph graph, Subgraph s,
			Map map) {
		Node source = (Node) map.get(getSource());
		Node target = (Node) map.get(getTarget());
		if (source != null && target != null) {

			Edge e = new Edge(this, source, target);
			int weight = 1;
			if (getTransition().isClosingLoop()) {
				// invert edge in order to place the last node in the loop below
				// the first node in the loop
				e.invert();
				weight = 0;
			}
			e.weight = weight;
			graph.edges.add(e);
			map.put(this, e);

		}
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE,
				new ConnectionEndpointEditPolicy());
		installEditPolicy(EditPolicy.CONNECTION_ROLE,
				new TransitionEditPolicy());
		installEditPolicy(WFConstants.POLICY_OPEN, new ShowPropertyViewPolicy());
		installEditPolicy(EditPolicy.CONNECTION_BENDPOINTS_ROLE,
				new TransitionBendpointEditPolicy());
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractConnectionEditPart#createFigure()
	 */
	@Override
	protected TransitionFigure createFigure() {

		TransitionFigure conn = new TransitionFigure();
		label = createLabel();
		if (label != null) {
			conn.add(label);
		}
		return conn;

	}

	protected Label createLabel() {
		String labelText = getTransition().getLabelText();
		if (labelText != null && labelText.trim().length() > 0) {
			Label l = new Label(labelText);
			l.setOpaque(true);
			l.setBackgroundColor(WFColorConstants.TRANSITION_LABEL_BACKGROUND);

			return l;
		}
		return null;
	}

	protected Locator createLabelLocator() {
		return new MidpointLocator(getConnectionFigure(), 0);
	}

	@Override
	public void deactivate() {
		super.deactivate();
		getTransition().removePropertyChangeListener(this);
	}

	/**
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@Override
	public Object getAdapter(Class adapter) {
		if (adapter == IPropertySource.class) {
			return getTransition();
		}
		return super.getAdapter(adapter);
	}

	@Override
	public Command getCommand(Request request) {
		Command cmd = null;
		// Disable editing completely if workflow diagram disallows editing
		WorkflowDiagram diagram = getTransition().getDiagram();

		if (diagram == null
				|| diagram.isEditable()
				|| REQ_OPEN.equals(request.getType())
				|| REQ_DIRECT_EDIT.equals(request.getType())
				|| ((request instanceof WFEditorRequest) && ((WFEditorRequest) request)
						.isExecutable(diagram))) {
			cmd = super.getCommand(request);
		} else {
			cmd = null;
		}
		return cmd;

	}

	@Override
	public DragTracker getDragTracker(Request request) {
		return new WFDragEditPartsTracker(this);
	}

	@Override
	public TransitionFigure getFigure() {
		return (TransitionFigure) super.getFigure();
	}

	protected Locator getLabelLocator() {
		if (labelLocator == null) {
			labelLocator = createLabelLocator();
		}
		return labelLocator;
	}

	@Override
	public EditPart getTargetEditPart(Request r) {
		return super.getTargetEditPart(r);
	}

	public Transition getTransition() {
		return (Transition) getModel();
	}

	@Override
	public WFGraphicalViewer getViewer() {
		try {
			return (WFGraphicalViewer) super.getViewer();
		} catch (NullPointerException e) {
			return null;
		}
	}

	@Override
	public void performRequest(Request request) {
		if (REQ_OPEN.equals(request.getType())) {
			Command cmd = getCommand(request);
			if (cmd != null && cmd.canExecute()) {
				cmd.execute();
			}
		}
	}

	public void propertyChange(final PropertyChangeEvent evt) {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				refresh();

			}
		});
	}

	@Override
	public void refresh() {
		super.refresh();
		Connection conn = getConnectionFigure();
		if (label != null) {
			conn.remove(label);
		}
//		int direction = PositionConstants.SOUTH;
//		try {
//			direction = (getTransition().source.getParent()).getDirection();
//		} catch (NullPointerException e) {
//			// TODO this happens when undoing a deletion!!
//
//		}
		List<Bendpoint> bends = getTransition().getBendpoints();
		if (bends != null) {
			getConnectionFigure().setRoutingConstraint(new ArrayList<Bendpoint>(bends));
		}

		label = createLabel();
		if (label != null) {
			conn.add(label, getLabelLocator());
		}

	}

	/**
	 * @see org.eclipse.gef.EditPart#setSelected(int)
	 */
	@Override
	public void setSelected(int value) {
		super.setSelected(value);
		if (value != EditPart.SELECTED_NONE) {
			((PolylineConnection) getFigure()).setLineWidth(2);
		} else {
			((PolylineConnection) getFigure()).setLineWidth(1);
		}
	}

	@Override
	public void setTarget(EditPart editPart) {

		try {
			GraphicalEditPart targetPart = (GraphicalEditPart) editPart;
			getFigure().setTargetPart(targetPart);

		} catch (NullPointerException e) {
			getFigure().setTargetPart(null);
		}
		super.setTarget(editPart);
	}

}
