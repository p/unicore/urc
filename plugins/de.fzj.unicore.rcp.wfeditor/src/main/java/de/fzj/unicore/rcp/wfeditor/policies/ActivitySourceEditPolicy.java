/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.gef.editpolicies.ContainerEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gef.requests.GroupRequest;
import org.eclipse.swt.graphics.Color;

import de.fzj.unicore.rcp.wfeditor.WFColorConstants;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.activities.Activity;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.commands.AddAndAssignSourceCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.AdjustLayoutCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.DeleteTransitionCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.MoveActivityCommand;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;

/**
 * @author Daniel Lee, demuth
 */
@SuppressWarnings({"unchecked"})
public class ActivitySourceEditPolicy extends ContainerEditPolicy {

	private Color revertColor;
	private static Color highLightColor = WFColorConstants.CONTAINER_HIGHLIGHT;

	private Command createMoveChildCommand(IActivity child) {

		Rectangle constraint = new Rectangle(child.getLocation(),
				child.getSize());
		IActivity myModel = ((IActivity) getHost().getModel());
		Rectangle myConstraint = new Rectangle(myModel.getLocation(),
				myModel.getSize());
		Point target = myConstraint.getBottom();
		target.translate(0, WFConstants.BETWEEN_ACTIVITY_PADDING.bottom
				+ WFConstants.BETWEEN_ACTIVITY_PADDING.top);
		Dimension delta = target.getDifference(constraint.getTop());
		constraint.translate(delta.width, delta.height);
		MoveActivityCommand cmd = new MoveActivityCommand(child,
				constraint.getTopLeft());
		return cmd;
	}

	/**
	 * @see org.eclipse.gef.EditPolicy#eraseTargetFeedback(org.eclipse.gef.Request)
	 */
	@Override
	public void eraseTargetFeedback(Request request) {
		if (revertColor != null) {
			setBackground(revertColor);
			revertColor = null;
		}
	}

	/**
	 * @see org.eclipse.gef.editpolicies.ContainerEditPolicy#getAddCommand(org.eclipse.gef.requests.GroupRequest)
	 */
	@Override
	protected Command getAddCommand(GroupRequest request) {
		CompoundCommand cmd = new CompoundCommand();

		EditPart[] parts = (EditPart[]) request.getEditParts().toArray(
				new EditPart[0]);
		if (parts.length == 0) {
			return null;
		}
		EditPartViewer viewer = parts[0].getViewer();
		if (viewer == null) {
			return null;
		}
		for (int i = 0; i < parts.length; i++) {
			if (!(parts[i].getModel() instanceof IActivity)) {
				continue;
			}
			IActivity child = ((Activity) parts[i].getModel());
			for (Transition t : child.getIncomingTransitions()) {
				EditPart part = (EditPart) viewer.getEditPartRegistry().get(
						t.source);
				if (part != null
						&& part.getSelected() == EditPart.SELECTED_NONE) {
					DeleteTransitionCommand deleteTransitions = new DeleteTransitionCommand();
					deleteTransitions.setTransition(t);
					cmd.add(deleteTransitions);
				}
			}
			for (Transition t : child.getOutgoingTransitions()) {
				EditPart part = (EditPart) viewer.getEditPartRegistry().get(
						t.source);
				if (part != null
						&& part.getSelected() == EditPart.SELECTED_NONE) {
					DeleteTransitionCommand deleteTransitions = new DeleteTransitionCommand();
					deleteTransitions.setTransition(t);
					cmd.add(deleteTransitions);
				}
			}
			AddAndAssignSourceCommand add = new AddAndAssignSourceCommand();
			add.setSource((IActivity) getHost().getModel());
			add.setChild(child);
			cmd.add(add);
			ChangeBoundsRequest addReq = new ChangeBoundsRequest();
			addReq.setType(RequestConstants.REQ_ADD);
			addReq.setEditParts(parts[i]);
			cmd.add(createMoveChildCommand(child));
			Command addCommand = getHost().getParent().getCommand(addReq);
			if (addCommand != null) {
				cmd.add(addCommand);
			}

		}

		return cmd;
	}

	private Color getBackground() {
		return getFigure().getBackgroundColor();
	}

	/**
	 * @see ContainerEditPolicy#getCreateCommand(org.eclipse.gef.requests.CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(CreateRequest request) {

		request.getExtendedData()
				.put(StructuredActivityLayoutEditPolicy.OMIT_LAYOUT_ADJUSTMENT_COMMAND_DURING_CREATION,
						true);

		Command createCommand = getHost().getParent().getCommand(request);
		if (createCommand == null) {
			return null;
		}
		CompoundCommand result = new CompoundCommand();
		result.add(createCommand);
		AddAndAssignSourceCommand cmd = new AddAndAssignSourceCommand();

		Activity child = (Activity) request.getNewObject();
		cmd.setChild(child);
		cmd.setSource((IActivity) getHost().getModel());
		result.add(cmd);
		result.add(createMoveChildCommand(child));
		Command adjustLayoutCommand = new AdjustLayoutCommand(child, getHost()
				.getViewer());
		if (adjustLayoutCommand != null) {
			result.add(adjustLayoutCommand);
		}
		return result;
	}

	private IFigure getFigure() {
		return ((GraphicalEditPart) getHost()).getFigure();
	}

	/**
	 * @see AbstractEditPolicy#getTargetEditPart(org.eclipse.gef.Request)
	 */
	@Override
	public EditPart getTargetEditPart(Request request) {
		if (REQ_CREATE.equals(request.getType())) {
			return getHost();
		}
		if (REQ_ADD.equals(request.getType())) {
			return getHost();
		}
		if (REQ_MOVE.equals(request.getType())) {
			return getHost();
		}
		return super.getTargetEditPart(request);
	}

	private void setBackground(Color c) {
		getFigure().setBackgroundColor(c);
	}

	/**
	 * Changes the background color of the container to the highlight color
	 */
	protected void showHighlight() {
		if (revertColor == null) {
			revertColor = getBackground();
			setBackground(highLightColor);
		}
	}

	/**
	 * @see org.eclipse.gef.EditPolicy#showTargetFeedback(org.eclipse.gef.Request)
	 */
	@Override
	public void showTargetFeedback(Request request) {
		if (request.getType().equals(RequestConstants.REQ_CREATE)
				|| request.getType().equals(RequestConstants.REQ_ADD)) {
			showHighlight();
		}
	}

}
