/**
 * This code has been taken from
 * http://stackoverflow.com/questions/22670665/create-miniature-outline-view-from-gef-editor
 */
package de.fzj.unicore.rcp.wfeditor.ui;

import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.draw2d.Viewport;
import org.eclipse.draw2d.parts.ScrollableThumbnail;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.ui.parts.ContentOutlinePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

public class OverviewContentOutlinePage extends ContentOutlinePage {

private ScrollableThumbnail thumbnail;
  private DisposeListener disposeListener;
  private GraphicalViewer viewer;
  private SashForm sash;

  public OverviewContentOutlinePage(GraphicalViewer viewer) {
    super(viewer);
    this.viewer = viewer;
  }

  @Override
  public void createControl(Composite parent) {
	sash = new SashForm(parent, SWT.VERTICAL);
	Canvas canvas = new Canvas(sash, SWT.BORDER);
    LightweightSystem lws = new LightweightSystem(canvas);
    thumbnail = new ScrollableThumbnail(
    (Viewport) ((ScalableFreeformRootEditPart) viewer.getRootEditPart()).getFigure());
    thumbnail.setSource(((ScalableFreeformRootEditPart) viewer.getRootEditPart()).getLayer(LayerConstants.PRINTABLE_LAYERS));
    lws.setContents(thumbnail);

    disposeListener = new DisposeListener() {
      @Override
      public void widgetDisposed(DisposeEvent e) {
         if (thumbnail != null) {
             thumbnail.deactivate();
              thumbnail = null;
         }
      }
    };

    viewer.getControl().addDisposeListener(disposeListener);
  }

  /**
	 * @see org.eclipse.gef.ui.parts.ContentOutlinePage#getControl()
	 */
	@Override
	public Control getControl() {
		return sash;
	}

  @Override
  public void dispose() {
    if(viewer != null) {
      if (viewer.getControl() != null && !viewer.getControl().isDisposed())
        viewer.getControl().removeDisposeListener(disposeListener);
      super.dispose();
    }
  }
}
