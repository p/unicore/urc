/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.structures;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Insets;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.Activity;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.traversal.CountElementsVisitor;
import de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.TopDownDAGTraverser;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

/**
 * A Structured activity is an activity whose execution is determined by some
 * internal structure.
 * 
 * @author demuth
 */
@XStreamAlias("StructuredActivity")
public class StructuredActivity extends Activity implements
		PropertyChangeListener {

	/**
	 * Key for direction property that tells the diagram whether the graph
	 * should be layouted in North-South or West-East direction. Can be either
	 * {@link PositionConstants#SOUTH} or {@link PositionConstants#EAST}.
	 */
	public static final String PROP_DIRECTION = "direction";

	public static QName TYPE = new QName("http://www.unicore.eu/",
			"StructuredActivity");

	static final long serialVersionUID = 1;

	private transient boolean containsCycles = false;

	private boolean canAddChildren = true;

	/**
	 * Mark whether an update for this value is needed
	 */
	private transient boolean containsCyclesDirty = false;

	protected List<IActivity> children = new ArrayList<IActivity>();
	
	private Insets innerPadding;
	
	/**
	 * The activity where the execution of this structured activity starts.
	 */
	private IActivity startActivity;

	public void addChild(IActivity child) {
		addChild(child, -1);
	}

	public void addChild(IActivity child, int index) {
		if (index >= 0) {
			children.add(index, child);
		} else {
			children.add(child);
		}
		child.setParent(this);
		for (Transition t : child.getOutgoingTransitions()) {
			t.setParent(this);
		}
		updateChildIndices();
		fireStructureChange(CHILDREN, child);
		child.addPersistentPropertyChangeListener(this);

	}

	@Override
	public void afterSaveAs(IPath newLocation, boolean recursive,
			boolean copyLocalInputFiles, boolean copyRemoteInputFiles,
			IProgressMonitor monitor) throws Exception {
		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		monitor.beginTask("", 1 + getChildren().size());
		try {
			SubProgressMonitor sub1 = new SubProgressMonitor(monitor, 1);
			super.afterSaveAs(newLocation, recursive, copyLocalInputFiles,
					copyRemoteInputFiles, sub1);
			if (recursive) {
				for (IActivity child : getChildren()) {
					SubProgressMonitor sub2 = new SubProgressMonitor(monitor, 1);
					child.afterSaveAs(newLocation, recursive,
							copyLocalInputFiles, copyRemoteInputFiles, sub2);
				}
			}
		} finally {
			monitor.done();
		}
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization(false);
		for(IActivity child : getChildren())
		{
			child.afterDeserialization();
		}
		for (Transition t : getOutgoingTransitions()) {
			t.afterDeserialization();
		}
	}



	@Override
	public void afterSerialization() {
		super.afterSerialization(false);
		for(IActivity child : getChildren())
		{
			child.afterSerialization();
		}
		
		for (Transition t : getOutgoingTransitions()) {
			t.afterSerialization();
		}
		
	}
	
	@Override
	public void afterSubmission() {
		super.afterSubmission();
		List<IActivity> children = getChildren();
		for (IActivity child : children) {
			child.afterSubmission();
		}
	}
	

	@Override
	public void beforeSerialization() {
		super.beforeSerialization(false);
		
		for(IActivity child : getChildren())
		{
			child.beforeSerialization();
		}
		
		for (Transition t : getOutgoingTransitions()) {
			t.beforeSerialization();
		}

	}

	@Override
	public void beforeSaveAs(IPath newLocation, boolean recursive,
			boolean copyLocalInputFiles, boolean copyRemoteInputFiles,
			IProgressMonitor monitor) throws Exception {
		monitor = ProgressUtils.getNonNullMonitorFor(monitor);
		monitor.beginTask("", 1 + getChildren().size());
		try {
			SubProgressMonitor sub1 = new SubProgressMonitor(monitor, 1);
			if (recursive) {
				for (IActivity child : getChildren()) {
					SubProgressMonitor sub2 = new SubProgressMonitor(monitor, 1);
					child.beforeSaveAs(newLocation, recursive,
							copyLocalInputFiles, copyRemoteInputFiles, sub2);
				}
			}
			super.beforeSaveAs(newLocation, recursive, copyLocalInputFiles,
					copyRemoteInputFiles, sub1);
		} finally {
			monitor.done();
		}
	}

	@Override
	public void beforeSubmission() {
		super.beforeSubmission();
		List<IActivity> children = getChildren();
		for (IActivity child : children) {
			child.beforeSubmission();
		}
	}

	public boolean canAddChild(IActivity child) {
		return !child.isBoundToParent();
	}

	public boolean canAddChildren() {
		return canAddChildren;
	}

	@Override
	public StructuredActivity clone() throws CloneNotSupportedException {
		StructuredActivity result = (StructuredActivity) super.clone();
		result.children = new ArrayList<IActivity>();
		result.startActivity = null;
		return result;
	}

	public boolean containsCycles() {
		if (containsCyclesDirty) {
			updateContainsCycles();
		}
		if (containsCycles) {
			return true;
		} else {
			// check children
			for (IActivity child : getChildren()) {
				if ((child instanceof StructuredActivity)) {
					boolean childContainsCycle = ((StructuredActivity) child)
							.containsCycles();
					if (childContainsCycle) {
						return true;
					}
				}
			}
		}
		return false;
	}

	protected void copyChildren(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		StructuredActivity result = (StructuredActivity) mapOfCopies.get(this);

		for (IActivity child : getChildren()) {
			IActivity copy = CloneUtils.getFromMapOrCopy(child, toBeCopied,
					mapOfCopies, copyFlags);
			result.addChild(copy);
			for (Transition t : child.getOutgoingTransitions()) {
				CloneUtils.getFromMapOrCopy(t, toBeCopied,
						mapOfCopies, copyFlags);
			}
		}
	}

	@Override
	public void dispose() {
		for (IActivity child : getChildren()) {
			child.dispose();
		}
		super.dispose();
	}

	public List<IActivity> getChildren() {
		return children;
	}

	@Override
	public StructuredActivity getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		StructuredActivity result = (StructuredActivity) super.getCopy(
				toBeCopied, mapOfCopies, copyFlags);
		result.startActivity = CloneUtils.getFromMapOrCopy(startActivity,
				toBeCopied, mapOfCopies, copyFlags);
		copyChildren(toBeCopied, mapOfCopies, copyFlags);

		return result;
	}

	/**
	 * Obtain direction property that tells the diagram whether the graph should
	 * be layouted in North-South or West-East direction. Can be either
	 * {@link PositionConstants#SOUTH} or {@link PositionConstants#EAST}.
	 */
	public int getDirection() {
		Integer result = (Integer) getPropertyValue(PROP_DIRECTION);
		if (result == null) {
			return PositionConstants.SOUTH;
		} else {
			return result;
		}
	}

	
	public Insets getDefaultInnerPadding() {
		return new Insets(80, 15, 15, 15);
	}
	
	public Insets getInnerPadding() {
		return innerPadding;
	}

	public IActivity getStartActivity() {
		return startActivity;
	}

	@Override
	public QName getType() {
		return TYPE;
	}

	public boolean isLoop() {
		return false;
	}

	@Override
	public void move(int deltaX, int deltaY) {
		super.move(deltaX, deltaY);
		List<IActivity> children = getChildren();
		for (IActivity child : children) {
			child.move(deltaX, deltaY);
			for (Transition t : child.getOutgoingTransitions()) {
				t.move(deltaX, deltaY);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		if (INPUTS.equals(evt.getPropertyName())
				|| OUTPUTS.equals(evt.getPropertyName())) {
			// the topology of this subgraph has changed!
			containsCyclesDirty = true;
		}
	}

	/**
	 * This is executed when an object of this type gets deserialized
	 * 
	 * @return
	 */
	protected Object readResolve() {
		containsCyclesDirty = true;
		return this;
	}

	public void removeChild(IActivity child) {
		children.remove(child);
		updateChildIndices();
		fireStructureChange(CHILDREN, child);
		child.removePersistentPropertyChangeListener(this);
		child.setParent(null);
	}

	public void setCanAddChildren(boolean canAddChildren) {
		this.canAddChildren = canAddChildren;
	}

	@Override
	public void setCurrentIterationId(String id, boolean manual) {
		String myOld = getCurrentIterationId();
		List<IActivity> children = getChildren();
		if (children != null) {
			for (IActivity child : children) {
				String childsOld = child.getCurrentIterationId();
				String newId = childsOld;

				if (ExecutionStateConstants.DEFAULT_ITERATION_ID
						.equals(childsOld)
						&& !ExecutionStateConstants.DEFAULT_ITERATION_ID
								.equals(id)
						|| ExecutionStateConstants.DEFAULT_ITERATION_ID
								.equals(myOld)
						|| myOld.trim().length() == childsOld.trim().length()) {
					newId = id;
				} else {
					newId = childsOld.replaceFirst(myOld, id);
				}

				boolean equal = newId == null ? childsOld == null : newId
						.equals(childsOld);
				if (!equal) {
					child.setCurrentIterationId(newId, manual);
				}
			}
		}
		super.setCurrentIterationId(id, manual);

	}

	@Override
	public void setDiagram(WorkflowDiagram diagram) {
		super.setDiagram(diagram);
		for (IActivity child : getChildren().toArray(new Activity[0])) {
			child.setDiagram(diagram);
		}
	}

	public void setInnerPadding(Insets innerPadding) {
		this.innerPadding = innerPadding;
	}

	public void setStartActivity(IActivity activity) {
		this.startActivity = activity;
	}

	@Override
	public void undoDispose() {
		// must undispose ourselves before children are undisposed
		// otherwise events property change events caused by changes in the
		// children will not be fired
		super.undoDispose();
		for (IActivity child : getChildren()) {
			if (child.isDisposed()) {
				child.undoDispose();
			}
		}

	}

	protected void updateChildIndices() {
		List<IActivity> children = getChildren();
		int index = 0;
		for (IActivity activity : children) {
			activity.setSortIndex(index++);
		}
	}

	protected void updateContainsCycles() {
		IGraphTraverser traverser = new TopDownDAGTraverser();
		CountElementsVisitor counter = new CountElementsVisitor();
		try {
			traverser.traverseGraph(this, getStartActivity(), counter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		containsCycles = getChildren().size() != counter.getElementCount();
		containsCyclesDirty = false;
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		super.updateToCurrentModelVersion(oldVersion, currentVersion);
		List<IActivity> children = getChildren();
		for (IActivity child : children) {
			child.updateToCurrentModelVersion(oldVersion, currentVersion);
		}
	}

}
