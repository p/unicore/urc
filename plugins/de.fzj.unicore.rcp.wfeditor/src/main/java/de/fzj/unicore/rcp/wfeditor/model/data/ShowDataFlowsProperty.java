package de.fzj.unicore.rcp.wfeditor.model.data;

import java.util.HashSet;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ShowDataFlowsProperty")
public class ShowDataFlowsProperty implements Cloneable {

	private Set<String> shownDataTypes = new HashSet<String>();
	private Set<String> regExps = new HashSet<String>();

	private boolean showingSinks = false;

	private boolean showingIncoming = false;
	private boolean showingOutgoing = false;

	private boolean showingSources = false;

	public ShowDataFlowsProperty() {

	}

	public ShowDataFlowsProperty(boolean showingSources, boolean showingSinks,
			boolean showingIncoming, boolean showingOutgoing) {
		super();

		this.showingSources = showingSources;
		this.showingSinks = showingSinks;
		this.showingIncoming = showingIncoming;
		this.showingOutgoing = showingOutgoing;
	}

	@Override
	public ShowDataFlowsProperty clone() {
		try {
			ShowDataFlowsProperty result = (ShowDataFlowsProperty) super
					.clone();
			result.regExps = new HashSet<String>(regExps);
			result.shownDataTypes = new HashSet<String>(shownDataTypes);
			return result;
		} catch (CloneNotSupportedException e) {
			return null; // should never happen
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ShowDataFlowsProperty)) {
			return false;
		}
		ShowDataFlowsProperty other = (ShowDataFlowsProperty) obj;
		if (showingIncoming != other.showingIncoming) {
			return false;
		}
		if (showingOutgoing != other.showingOutgoing) {
			return false;
		}
		if (showingSinks != other.showingSinks) {
			return false;
		}
		if (showingSources != other.showingSources) {
			return false;
		}
		if (regExps.size() != other.regExps.size()
				|| !regExps.containsAll((other.regExps))) {
			return false;
		}
		if (shownDataTypes.size() != other.shownDataTypes.size()
				|| !shownDataTypes.containsAll((other.shownDataTypes))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (showingIncoming ? 1231 : 1237);
		result = prime * result + (showingOutgoing ? 1231 : 1237);
		result = prime * result + (showingSinks ? 1231 : 1237);
		result = prime * result + (showingSources ? 1231 : 1237);
		result = prime * result
				+ ((shownDataTypes == null) ? 0 : shownDataTypes.hashCode());
		if (shownDataTypes != null) {
			for (String s : shownDataTypes) {
				result = prime * result + s.hashCode();
			}
		}
		return result;
	}

	/**
	 * Use this in order to hide flows of all types. This does NOT influence
	 * general settings like {@link #isShowingIncoming()},
	 * {@link #isShowingOutgoing()}, {@link #isShowingSources())}, or
	 * {@link #isShowingSinks()} but resets all information related to data
	 * types.
	 */
	public void hideFlowsOfAllTypes() {
		shownDataTypes.clear();
		regExps.clear();
	}

	public void hideFlowsWithDataType(String dataType, boolean isRegExp) {
		if (isRegExp) {
			regExps.remove(dataType);
		} else {
			shownDataTypes.remove(dataType);
		}
	}

	public boolean isShowingAnyDataTypeAtAll() {
		return (regExps != null && regExps.size() > 0)
				|| (shownDataTypes != null && shownDataTypes.size() > 0);
	}

	/**
	 * Convenience method to check whether all sources, sinks, incoming and
	 * outgoing flows are shown.
	 * 
	 * @return
	 */
	public boolean isShowingFlows() {
		return showingSources && showingSinks && showingIncoming
				&& showingOutgoing;
	}

	public boolean isShowingFlowsWithDataType(String dataType) {
		if (shownDataTypes.contains(dataType)) {
			return true;
		} else {
			for (String exp : regExps) {
				if (exp.equals(dataType) || dataType.matches(exp)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isShowingFlowsWithDataTypes(Set<String> dataTypes) {
		for (String dataType : dataTypes) {
			if (isShowingFlowsWithDataType(dataType)) {
				return true;
			}
		}
		return false;
	}

	public boolean isShowingIncoming() {
		return showingIncoming;
	}

	public boolean isShowingOutgoing() {
		return showingOutgoing;
	}

	public boolean isShowingSinks() {
		return showingSinks;
	}

	public boolean isShowingSources() {
		return showingSources;
	}

	/**
	 * Convenience method for showing/hiding sources, sinks, incoming and
	 * outgoing data flows. Does not influence the setting regarding data types.
	 * 
	 * @param show
	 */
	public void setShowingFlows(boolean show) {
		setShowingIncoming(show);
		setShowingOutgoing(show);
		setShowingSinks(show);
		setShowingSources(show);
	}

	public void setShowingIncoming(boolean showIncoming) {
		this.showingIncoming = showIncoming;
	}

	public void setShowingOutgoing(boolean showOutgoing) {
		this.showingOutgoing = showOutgoing;
	}

	public void setShowingSinks(boolean showingSinks) {
		this.showingSinks = showingSinks;
	}

	public void setShowingSources(boolean showingSources) {
		this.showingSources = showingSources;
	}

	/**
	 * Use this method to control the visualization of data flows on a per-data
	 * type level. The given String can either correspond to an exact data type
	 * or to a whole group of data types. This is done by using regular
	 * expressions that are evaluated against a data type when
	 * {@link #isShowingFlowsWithDataType(String)} gets called.
	 * 
	 * @param dataType
	 */
	public void showFlowsWithDataType(String dataType, boolean isRegExp) {
		if (isRegExp) {
			regExps.add(dataType);
		} else {
			shownDataTypes.add(dataType);
		}
	}

}
