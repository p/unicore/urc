package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.ConnectionLocator;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;

public class SmartConnectionLocator extends ConnectionLocator {

	int direction = PositionConstants.SOUTH;
	IFigure label;

	/**
	 * 
	 * 
	 * @param c
	 *            Connection associated with this locator
	 * @since 2.0
	 */
	public SmartConnectionLocator(Connection c, IFigure label) {
		this(c, label, PositionConstants.SOUTH);

	}

	public SmartConnectionLocator(Connection c, IFigure label, int direction) {
		super(c);
		this.direction = direction;
		this.label = label;
	}

	@Override
	protected Rectangle getNewBounds(Dimension size, Point center) {
		return super.getNewBounds(size, center);
	}

	@Override
	protected Point getReferencePoint() {
		Point result = new Point(0, 0);
		PointList points = getConnection().getPoints();
		Point first = points.getFirstPoint();
		Point last = points.getLastPoint();
		Dimension labelBounds = label.getPreferredSize();
		// getConnection().translateToRelative(labelBounds);
		int labelAlign = labelBounds.width < labelBounds.height ? -1 : 1;
		int directionSign = (direction & PositionConstants.SOUTH) == 0 ? -1 : 1;

		switch (labelAlign * directionSign) {
		case 1:
			int y = (first.y + last.y) / 2 - labelBounds.height / 2; // vertically
																		// place
																		// label
																		// in
																		// the
																		// middle
																		// of
																		// the
																		// edge
			for (int i = 0; i < points.size() - 1; i++) {
				Point current = points.getPoint(i);
				Point next = points.getPoint(i + 1);
				if (current.y <= y && y <= next.y) {
					int x = (current.x + next.x) / 2;// horizontally place label
														// in the middle of the
														// current segment
					result.x = x;
					result.y = y;
					break;
				}
			}
			break;

		case -1:
			int x = (first.x + last.x) / 2; // horizontally place label in the
											// middle of the edge
			int left = x - labelBounds.width / 2;
			int right = x + labelBounds.width / 2;
			y = Integer.MAX_VALUE;
			for (int i = 0; i < points.size() - 1; i++) {
				Point current = points.getPoint(i);
				Point next = points.getPoint(i + 1);
				if (next.x < current.x) {
					// swap
					Point temp = current;
					current = next;
					next = temp;
				}
				if (current.x <= right && next.x >= left) // these segments
															// interfere with
															// the vertical
															// placement
				{
					int min;
					int deltaY = (next.y - current.y);
					int deltaX = (next.x - current.x);
					double slope = deltaX == 0 ? 0 : (double) deltaY / deltaX;
					double b = current.y - slope * current.x;

					int start = Math.max(left, current.x);
					int end = Math.min(right, next.x);

					if (slope == 0) {
						min = next.y;
					} else if (slope > 0) {
						min = (int) (slope * start + b);
					} else {
						min = (int) (slope * end + b);
					}
					if (min < y) {
						y = min;
					}
				}
			}
			result.x = x;
			result.y = y - labelBounds.height / 2;
			break;
		}
		getConnection().translateToAbsolute(result);
		return result;
	}

}
