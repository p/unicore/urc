package de.fzj.unicore.rcp.wfeditor.utils;

import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.dnd.TemplateTransferDropTargetListener;

import de.fzj.unicore.rcp.wfeditor.DefaultCreationTool;

public class WFTransferDropTargetListener extends
		TemplateTransferDropTargetListener {

	public WFTransferDropTargetListener(EditPartViewer viewer) {
		super(viewer);
	}

	/**
	 * Deselect any selected tool after dropping a new element keeping the
	 * element creation tool selected feels weird CAUTION: even if you switch
	 * this off, the {@link DefaultCreationTool} will usually deselect itself
	 * cause its mouseHover method is called and the editpart under the mouse is
	 * the palette root's edit part (WHYYY?!)
	 */
	@Override
	protected void handleDrop() {
		super.handleDrop();
		if (getTargetEditPart() != null) {

			getViewer().getEditDomain().loadDefaultTool();

		}
	}

}
