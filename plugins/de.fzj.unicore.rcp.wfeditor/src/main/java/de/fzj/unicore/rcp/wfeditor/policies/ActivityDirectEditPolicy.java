/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import org.eclipse.draw2d.Label;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;

import de.fzj.unicore.rcp.wfeditor.figures.ActivityFigure;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.commands.RenameFlowElementCommand;
import de.fzj.unicore.rcp.wfeditor.parts.ActivityPart;

/**
 * EditPolicy for the direct editing of Activity names.
 * 
 * @author Daniel Lee
 */
public class ActivityDirectEditPolicy extends DirectEditPolicy {

	/**
	 * @see DirectEditPolicy#getDirectEditCommand(org.eclipse.gef.requests.DirectEditRequest)
	 */
	@Override
	protected Command getDirectEditCommand(DirectEditRequest request) {
		RenameFlowElementCommand cmd = new RenameFlowElementCommand();
		cmd.setElement((IActivity) getHost().getModel());
		cmd.setOldName(((IActivity) getHost().getModel()).getName());
		cmd.setName((String) request.getCellEditor().getValue());
		return cmd;
	}

	@Override
	protected void revertOldEditValue(DirectEditRequest request) {
		if (getHostFigure() instanceof ActivityFigure) {
			Label label = ((ActivityFigure) getHostFigure()).getLabelFigure();
			label.setText(((IActivity) getHost().getModel()).getName());
		}

		super.revertOldEditValue(request);
	}

	/**
	 * @see DirectEditPolicy#showCurrentEditValue(org.eclipse.gef.requests.DirectEditRequest)
	 */
	@Override
	protected void showCurrentEditValue(DirectEditRequest request) {
		Label label = ((ActivityFigure) getHostFigure()).getLabelFigure();
		String value = (String) request.getCellEditor().getValue();
		label.setText(value);
		// hack to prevent async layout from placing the cell editor twice.
		// getHostFigure().getUpdateManager().performUpdate();
	}

	@Override
	public boolean understandsRequest(Request request) {
		WorkflowDiagram diagram = ((ActivityPart) getHost()).getModel()
				.getDiagram();
		return diagram == null || diagram.isEditable();

	}

}
