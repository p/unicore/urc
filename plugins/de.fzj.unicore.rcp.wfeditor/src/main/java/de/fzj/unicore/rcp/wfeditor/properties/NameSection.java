package de.fzj.unicore.rcp.wfeditor.properties;

import java.beans.PropertyChangeEvent;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.commands.RenameFlowElementCommand;

public class NameSection extends AbstractSection {

	Text nameText;

	private void changeElementNameTo(String name) {
		String text = nameText.getText();
		if (element != null && !element.getName().equals(text)) {
			element.removePropertyChangeListener(this);
			RenameFlowElementCommand cmd = new RenameFlowElementCommand();
			cmd.setElement(element);
			cmd.setName(text);
			element.getCommandStack().execute(cmd);
			if (!cmd.wasRenameSuccessful()) {
				nameText.setText(element.getName());
			}
			element.addPropertyChangeListener(this);
		}
	}

	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ITabbedPropertySection#createControls(org.eclipse.swt.widgets.Composite,
	 *      org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage)
	 */
	@Override
	public void createControls(Composite parent,
			TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);
		Composite composite = getWidgetFactory()
				.createFlatFormComposite(parent);
		FormData data;

		nameText = getWidgetFactory().createText(composite, ""); //$NON-NLS-1$
		nameText.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				changeElementNameTo(nameText.getText());
			}

			public void widgetSelected(SelectionEvent e) {
				changeElementNameTo(nameText.getText());
			}
		});

		nameText.addFocusListener(new FocusListener() {

			public void focusGained(FocusEvent e) {

			}

			public void focusLost(FocusEvent e) {
				changeElementNameTo(nameText.getText());

			}
		});

		data = new FormData();
		data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
		data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(0, 0);
		nameText.setLayoutData(data);
		controls.add(nameText);

		CLabel nameLabel = getWidgetFactory().createCLabel(composite, "Name:"); //$NON-NLS-1$
		data = new FormData();
		data.left = new FormAttachment(0, 0);
		data.right = new FormAttachment(nameText,
				-ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(nameText, 0, SWT.CENTER);
		nameLabel.setLayoutData(data);
		controls.add(nameLabel);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (IFlowElement.PROP_NAME.equals(evt.getPropertyName())
				&& !nameText.isDisposed()) {
			nameText.setText(element.getName());
		}
	}

	/*
	 * @see
	 * org.eclipse.ui.views.properties.tabbed.view.ITabbedPropertySection#refresh
	 * ()
	 */
	@Override
	public void refresh() {
		if (!nameText.isDisposed()) {
			nameText.setText(element.getName());
		}
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
		updateEnabled();
	}
}
