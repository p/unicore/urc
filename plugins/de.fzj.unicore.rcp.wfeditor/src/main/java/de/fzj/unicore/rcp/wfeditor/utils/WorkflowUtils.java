package de.fzj.unicore.rcp.wfeditor.utils;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.Calendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.graphics.Font;

import com.thoughtworks.xstream.XStream;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.ExecutionData;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;

public class WorkflowUtils {

	/**
	 * Try to find the local file that represents a previously submitted
	 * workflow. This can only be done heuristically at the moment and might
	 * fail, e.g. when the user has deleted that file already. When it fails,
	 * we'll return null. In order not to search indefinetely, one may give a
	 * timeout and a progress monitor.
	 * 
	 * @param wfNode
	 * @param timeout
	 *            ms until aborting the search or -1 if we should go on forever
	 * @return
	 */
	@SuppressWarnings("unused")
	public static IFile findSubmittedWorkflowFile(String submissionId,
			long timeout, IProgressMonitor progress) {
		try {
			progress = ProgressUtils.getNonNullMonitorFor(progress);

			// try to find folder containing the submitted workflow (may not
			// exist)
			JAXBContext ctxt = JAXBContextProvider.getDefaultContext();
			Unmarshaller unmarshaller = ctxt.createUnmarshaller();
			long startTime = System.currentTimeMillis(); // don't check for too
															// long, finding the
															// folder is a
															// best-effort
															// service
			for (IProject p : ResourcesPlugin.getWorkspace().getRoot()
					.getProjects()) {
				IFolder f = p.getFolder(getSubmittedFolderName());
				if (f.exists()) {
					for (IResource r : f.members()) {
						if (r instanceof IFolder) {
							IFolder submittedFolder = (IFolder) r;
							IFile executionDataFile = submittedFolder
									.getFile(getExecutionDataFilename());
							if (executionDataFile != null) {
								try {
									ExecutionData ed = (ExecutionData) unmarshaller
											.unmarshal(executionDataFile
													.getContents());
									if (submissionId.equals(ed
											.getSubmissionID())) {
										String wfFilePath = ed
												.getWorkflowFilePath();
										if (wfFilePath != null) {
											return PathUtils
													.absolutePathToEclipseFile(Path
															.fromPortableString(wfFilePath));
										} else {
											XStream xstream = XStreamConfigurator
													.getInstance()
													.getXStreamForDeserialization();
											// try to find suitable wf file by
											// deserializing with XStream
											// this might be EXPENSIVE..
											// however, we should give it a try,
											// as we seem to have found the
											// right folder
											// note that we cannot rely on the
											// file extension, as this might
											// vary with different workflow
											// diagram types

											for (IResource potentialWFFile : submittedFolder
													.members()) {
												try {
													if (potentialWFFile instanceof IFile) {
														IFile file = (IFile) potentialWFFile;
														InputStream is = file
																.getContents();
														WorkflowDiagram diagram = (WorkflowDiagram) xstream
																.fromXML(new InputStreamReader(
																		new BufferedInputStream(
																				is)));
														return file;
													}

												} catch (Throwable t) {
													// do nothing and try next
													// file
												}
											}
										}

									}
								} catch (Exception e) {
									// do nothing
								}
								// we shouldn't do this indefinitely any more
								// and write to a temp file instead
								if (progress.isCanceled()
										|| System.currentTimeMillis()
												- startTime > timeout) {
									break;
								}
							}
						}
					}
				}

				// we shouldn't do this indefinitely any more
				// and write to a temp file instead
				if (progress.isCanceled()
						|| System.currentTimeMillis() - startTime > timeout) {
					break;
				}
			}
			return null; // not found

		} catch (Exception e) {
			WFActivator
					.log(IStatus.WARNING,
							"Unable to find local file for previously submitted workflow. ",
							e);
			return null;
		}
	}

	public static String getExecutionDataFilename() {
		return "ExecutionData.xml";
	}

	public static Dimension getPreferredTextDimension(String text, Font f) {
		Label l = new Label();
		l.setFont(f);
		l.setText(text);
		Dimension d = l.getPreferredSize();
		d.width += 10;
		return d;
	}

	public static String getSubmittedAtPrefix() {
		return "submitted at ";
	}

	public static String getSubmittedFolderName() {
		return "submitted";
	}

	public static IPath getSubmittedPath() {
		DateFormat df = Constants.DATE_FORMAT_WITHOUT_COLON;
		return new Path(getSubmittedFolderName() + "/" + getSubmittedAtPrefix()
				+ df.format(Calendar.getInstance().getTime()));
	}

	/**
	 * Check whether the given name is allowed for a part of the workflow. Some
	 * characters are not allowed since the workflow must be serializable in
	 * XML. If the name is allowed, return null, if not, return a description
	 * stating which characters are not allowed.
	 * 
	 * @param name
	 * @return
	 */
	public static String isAllowedAsActivityName(String name) {
		String result = null;
		String[] illegal = WFConstants.ILLEGAL_CHARS_IN_ACTIVITY_NAMES;
		for (String s : illegal) {
			if (name.contains(s)) {
				if (result == null) {
					result = "The provided name contains illegal characters: ";
				}
				result = result + s + ", ";
			}
		}
		// cut off the last "," that is always added
		if (result != null) {
			result = result.substring(0, result.length() - 2);
		}
		return result;
	}

	/**
	 * Check whether the given name is allowed for a workflow variable. Some
	 * characters are not allowed since the workflow must be serializable in
	 * XML. If the name is allowed, return null, if not, return a description
	 * stating which characters are not allowed.
	 * 
	 * @param name
	 * @return
	 */
	public static String isAllowedAsVariableName(String name) {
		String result = null;
		String[] illegal = WFConstants.ILLEGAL_CHARS_IN_VARIABLE_NAMES;
		for (String s : illegal) {
			if (name.contains(s)) {
				if (result == null) {
					result = "The provided name contains illegal characters: ";
				}
				result = result + s + ", ";
			}
		}
		// cut off the last "," that is always added
		if (result != null) {
			result = result.substring(0, result.length() - 2);
		}
		return result;
	}
}
