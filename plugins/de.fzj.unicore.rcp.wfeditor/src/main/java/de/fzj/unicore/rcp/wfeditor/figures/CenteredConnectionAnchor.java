/*******************************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.AbstractConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PrecisionPoint;
import org.eclipse.draw2d.geometry.Rectangle;

import de.fzj.unicore.rcp.wfeditor.WFConstants;

/**
 * FixedConnectionAnchor is an anchor at either the top, bottom, left or right
 * of the node, centered along the edge.
 */
public class CenteredConnectionAnchor extends AbstractConnectionAnchor {

	private int location;
	private int inset;
	private int offset = 0;

	public CenteredConnectionAnchor(IFigure owner, int location, int inset) {
		super(owner);
		this.location = location;
		this.inset = inset;
	}

	public CenteredConnectionAnchor(IFigure owner, int location, int inset,
			int offset) {
		this(owner, location, inset);
		this.offset = offset;
	}

	public Point getLocation(Point reference) {
		Rectangle r = getOwner().getBounds();
		int x, y;
		switch (location) {
		case WFConstants.ANCHOR_TOP:
			x = r.right() - r.width / 2 + offset - 1;
			y = r.y + inset;
			x = ConnectionXCoordHistory.getInstance().adjustX(x);
			break;
		case WFConstants.ANCHOR_BOTTOM:
			x = r.right() - r.width / 2 + offset - 1;
			y = r.bottom() - inset;
			x = ConnectionXCoordHistory.getInstance().adjustX(x);
			break;
		case WFConstants.ANCHOR_LEFT:
			x = r.x + inset;
			y = r.bottom() - r.height / 2 + offset;
			break;
		case WFConstants.ANCHOR_RIGHT:
			x = r.right() - inset;
			y = r.bottom() - r.height / 2 + offset;
			break;
		case WFConstants.ANCHOR_TOP_INNER:
			x = r.right() - r.width / 2 + offset - 1;
			y = r.y + inset;
			x = ConnectionXCoordHistory.getInstance().adjustX(x);
			break;
		case WFConstants.ANCHOR_BOTTOM_INNER:
			x = r.right() - r.width / 2 + offset - 1;
			// TODO: GIGANTIC hack. 11 is the offset from the bottom of a
			// sequence to the
			// top of its lower +/- expansion arrow.
			y = r.bottom() - 11 - inset;
			x = ConnectionXCoordHistory.getInstance().adjustX(x);
			break;
		default:
			// Something went wrong. Attach the anchor to the middle
			x = r.right() - r.width / 2;
			y = r.bottom() - r.height / 2;
		}
		Point p = new PrecisionPoint(x, y);
		getOwner().translateToAbsolute(p);
		return p;
	}

	@Override
	public Point getReferencePoint() {
		return getLocation(null);
	}

}
