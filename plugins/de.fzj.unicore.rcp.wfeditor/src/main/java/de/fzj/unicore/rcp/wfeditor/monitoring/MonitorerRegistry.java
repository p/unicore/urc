/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.monitoring;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;

import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;

/**
 * @author demuth
 * 
 */
public class MonitorerRegistry implements IMonitorer {

	List<IMonitorer> monitorers = new ArrayList<IMonitorer>();
	Map<String, IMonitorer> activeMonitorers = new HashMap<String, IMonitorer>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.monitoring.IMonitorer#canMonitor(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram,
	 * org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public boolean canMonitor(WorkflowDiagram model, NodePath serviceAddress) {
		return findSuitableMonitorers(model, serviceAddress).size() > 0;
	}

	private List<IMonitorer> findSuitableMonitorers(WorkflowDiagram model,
			NodePath serviceAddress) {
		synchronized (monitorers) {
			List<IMonitorer> suitable = new ArrayList<IMonitorer>();
			for (IMonitorer monitorer : monitorers) {
				if (monitorer.canMonitor(model, serviceAddress)) {
					suitable.add(monitorer);
				}
			}
			return suitable;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.monitoring.IMonitorer#monitor(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram,
	 * org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public void monitor(WorkflowDiagram model, NodePath monitoringService)
			throws Exception {
		List<IMonitorer> suitable = findSuitableMonitorers(model,
				monitoringService);
		for (IMonitorer monitorer : suitable) {
			if (monitorer.canMonitor(model, monitoringService)) {
				try {

					monitorer.monitor(model, monitoringService);
					activeMonitorers.put(
							model.getID() + monitoringService.toString(),
							monitorer);
				} catch (Exception e) {
					WFActivator.log(IStatus.WARNING,
							"Monitoring could not be started.", e);
				}
			}
		}

	}

	public void registerMonitorer(IMonitorer c) {
		monitorers.add(c);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.monitoring.IMonitorer#stopMonitoring(de.fzj
	 * .unicore.rcp.wfeditor.model.WorkflowDiagram,
	 * org.w3.x2005.x08.addressing.EndpointReferenceType)
	 */
	public void stopMonitoring(WorkflowDiagram model, NodePath monitoringService)
			throws Exception {

		IMonitorer monitorer = activeMonitorers.remove(model.getID()
				+ monitoringService.toString());
		if (monitorer == null) {
			return;
		}
		try {
			monitorer.stopMonitoring(model, monitoringService);
		} catch (Exception e) {
			WFActivator.log(
					"Workflow execution monitorer could not be stopped!", e);
		}

	}

	public void unregisterMonitorer(IMonitorer c) {
		monitorers.remove(c);
	}

}
