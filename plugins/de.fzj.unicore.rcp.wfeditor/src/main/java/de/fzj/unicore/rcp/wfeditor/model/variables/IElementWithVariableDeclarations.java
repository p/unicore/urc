package de.fzj.unicore.rcp.wfeditor.model.variables;

/**
 * Marker interface to be implemented by workflow elements which declare
 * workflow variables. Used for displaying the associated section in the tabbed
 * properties view;
 * 
 * @author bdemuth
 * 
 */
public interface IElementWithVariableDeclarations {
	public boolean allowsAddingVariableDeclarations();
}
