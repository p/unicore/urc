package de.fzj.unicore.rcp.wfeditor.ui;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.ui.parts.DomainEventDispatcher;
import org.eclipse.swt.widgets.Control;

public class WFEventDispatcher extends DomainEventDispatcher {

	private WFTooltipHelper toolTipHelper;
	protected IFigure hoverSource;
	protected boolean editorCaptured = false;

	public WFEventDispatcher(EditDomain d, EditPartViewer v) {
		super(d, v);
	}


	
	@Override
	public void dispatchMouseHover(org.eclipse.swt.events.MouseEvent me) {
		super.dispatchMouseHover(me);
		boolean oldVisible = getToolTipHelper().getTooltipLayer().isVisible();
		// do not ask the tooltip for a tooltip!
		toolTipHelper.getTooltipLayer().setVisible(false); 
		updateFigureUnderCursor(me);
		if (oldVisible) {
			toolTipHelper.getTooltipLayer().setVisible(true);
		}
		// continue forwarding events to the tooltip helper even when
		// we've locked on a target (e.g. after starting a drag)
		if (editorCaptured && hoverSource != null) {
			toolTipHelper = getToolTipHelper();
			IFigure tip = hoverSource.getToolTip();
			Control control = (Control) me.getSource();
			org.eclipse.swt.graphics.Point absolute;
			absolute = control.toDisplay(new org.eclipse.swt.graphics.Point(
					me.x, me.y));
			toolTipHelper.displayToolTipNear(hoverSource, tip, absolute.x,
					absolute.y);
		}
	}

	@Override
	public void dispatchMouseMoved(org.eclipse.swt.events.MouseEvent me) {
		super.dispatchMouseMoved(me);
		if (editorCaptured) {
			Control control = (Control) me.getSource();
			org.eclipse.swt.graphics.Point absolute;
			absolute = control.toDisplay(new org.eclipse.swt.graphics.Point(
					me.x, me.y));
			toolTipHelper = getToolTipHelper();
			IFigure tooltip = hoverSource == null ? null : hoverSource
					.getToolTip();
			toolTipHelper.updateToolTip(hoverSource, tooltip, absolute.x,
					absolute.y);
		}

	}

	/**
	 * Returns the ToolTipHelper used to display tooltips on hover events.
	 * 
	 * @return the ToolTipHelper
	 */
	@Override
	protected WFTooltipHelper getToolTipHelper() {
		if (toolTipHelper == null) {
			toolTipHelper = new WFTooltipHelper(control,
					(ScalableFreeformRootEditPart) viewer.getRootEditPart());
		}
		return toolTipHelper;
	}
	
	protected void updateHoverSource(org.eclipse.swt.events.MouseEvent me) {
		
		boolean oldVisible = getToolTipHelper().getTooltipLayer().isVisible();
		// do not ask the tooltip for a tooltip!
		getToolTipHelper().getTooltipLayer().setVisible(false);
		IFigure f = getRoot().findFigureAt(me.x, me.y);
		if (f != null) {
			boolean sourceFound = false;
			Figure source = (Figure) f;
			while (!sourceFound && source.getParent() != null) {
				if (source.getToolTip() != null)
					sourceFound = true;
				else
					source = (Figure) source.getParent();
			}
			setHoverSource(source, me);
		} else {
			setHoverSource(null, me);
		}
		if (oldVisible) {
			getToolTipHelper().getTooltipLayer().setVisible(true);
		}
	}

	@Override
	protected void setHoverSource(Figure figure,
			org.eclipse.swt.events.MouseEvent me) {
		hoverSource = figure;
		if (figure != null) {
			super.setHoverSource(figure, me);
		} else {
			Control control = (Control) me.getSource();
			org.eclipse.swt.graphics.Point absolute;
			absolute = control.toDisplay(new org.eclipse.swt.graphics.Point(
					me.x, me.y));
			toolTipHelper = getToolTipHelper();
			toolTipHelper.updateToolTip(null, null, absolute.x, absolute.y);
		}
	}

	@Override
	public void setRouteEventsToEditor(boolean value) {
		super.setRouteEventsToEditor(value);
		this.editorCaptured = value;
	}

}
