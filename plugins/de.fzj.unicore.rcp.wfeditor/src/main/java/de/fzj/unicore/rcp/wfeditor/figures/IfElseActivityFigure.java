/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.geometry.Rectangle;

/**
 * @author hudsonr
 */
public class IfElseActivityFigure extends CollapsibleFigure {

	boolean selected;

	public IfElseActivityFigure(Rectangle bounds) {
		super(bounds, null);
		setOpaque(true);

	}

	@Override
	public void setBounds(Rectangle rect) {
		super.setBounds(rect);
	}

}
