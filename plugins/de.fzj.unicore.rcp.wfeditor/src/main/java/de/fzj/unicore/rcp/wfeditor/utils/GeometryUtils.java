package de.fzj.unicore.rcp.wfeditor.utils;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Ray;
import org.eclipse.draw2d.geometry.Rectangle;

public class GeometryUtils {

	public static final double EPSILON = 5e-8d;

	/** The point is on the line */
	public final static int ORIENTATION_COLLINEAR = 1;

	/** The point lies to the left the line */
	public final static int ORIENTATION_LEFT = 2;

	/** The point lies to the right the line */
	public final static int ORIENTATION_RIGHT = 3;

	/**
	 * Orientation cannot be determined because the two points defining the line
	 * are identical
	 */
	public final static int ORIENTATION_UNDEFINED = 4;

	/**
	 * The point is on the line and lies before the segment defined by the two
	 * points
	 */
	public static final int LIES_BEFORE = 11;

	/**
	 * The point is on the line and lies on the segment defined by the two
	 * points
	 */
	public static final int LIES_ON = 12;

	/**
	 * The point is on the line and lies behind the segment defined by the two
	 * points
	 */
	public static final int LIES_BEHIND = 13;

	private static int compareX(Point p1, Point p2) {
		if (p1.x < p2.x) {
			return -1;
		}
		if (p1.x > p2.x) {
			return 1;
		}
		// p1.x == p2.x
		if (p1.y < p2.y) {
			return -1;
		}
		if (p1.y > p2.y) {
			return 1;
		}
		return 0;
	}

	/**
	 * Returns the edge of the given rectangle is closest to the given point.
	 * 
	 * @param boundary
	 *            rectangle to test
	 * @param toTest
	 *            point to compare
	 * @return one of PositionConstants.LEFT, PositionConstants.RIGHT,
	 *         PositionConstants.TOP, or PositionConstants.BOTTOM
	 * 
	 * @since 3.0
	 */
	public static int getClosestSide(Rectangle boundary, Point toTest) {

		int[] sides = new int[] { PositionConstants.LEFT,
				PositionConstants.RIGHT, PositionConstants.TOP,
				PositionConstants.BOTTOM };

		int closestSide = PositionConstants.LEFT;
		double closestDistance = Double.MAX_VALUE;

		for (int idx = 0; idx < sides.length; idx++) {
			int side = sides[idx];

			double distance = Math.abs(getDistanceFromEdge(boundary, toTest,
					side));

			if (distance < closestDistance) {
				closestDistance = distance;
				closestSide = side;
			}
		}

		return closestSide;
	}

	/**
	 * Returns the distance between a point and a line segment (given as source
	 * and target point).
	 * 
	 * @param s1
	 * @param s2
	 * @param p
	 * @return
	 */
	public static double getDistanceFrom(Point s1, Point s2, Point p) {
		if (s1.equals(s2)) {
			// Segment is just a point
			return s1.getDistance(p);
		}

		Point orthogonalSource = p;
		Point orthogonalTarget = new Point(p.x + s2.y - s1.y, p.y - s2.x + s1.x);
		InspectResult inspect = inspectBasicLine(orthogonalSource,
				orthogonalTarget, s1, s2);

		if (inspect.orderOnThis == LIES_ON) {
			return inspect.intersectionPoint.getDistance(p);
		} else {
			return Math.min(s1.getDistance(p), s2.getDistance(p));
		}
	}

	/**
	 * Returns the distance from the point to the nearest edge of the given
	 * rectangle. Returns negative values if the point lies outside the
	 * rectangle.
	 * 
	 * @param boundary
	 *            rectangle to test
	 * @param toTest
	 *            point to test
	 * @return the distance between the given point and the nearest edge of the
	 *         rectangle. Returns negative values for points inside the
	 *         rectangle and positive values for points outside the rectangle.
	 * @since 3.1
	 */
	public static double getDistanceFrom(Rectangle boundary, Point toTest) {
		int side = getClosestSide(boundary, toTest);
		return getDistanceFromEdge(boundary, toTest, side);
	}

	/**
	 * Returns the distance of the given point from a particular side of the
	 * given rectangle.
	 * 
	 * @param rectangle
	 *            a bounding rectangle
	 * @param p
	 *            a point to test
	 * @param edgeOfInterest
	 *            side of the rectangle to test against
	 * @return the distance of the given point from the given edge of the
	 *         rectangle
	 * @since 3.0
	 */
	public static double getDistanceFromEdge(Rectangle rectangle, Point p,
			int edgeOfInterest) {
		Point s1 = null, s2 = null;
		switch (edgeOfInterest) {
		case PositionConstants.TOP:
			s1 = rectangle.getTopLeft();
			s2 = rectangle.getTopRight();
			break;
		case PositionConstants.BOTTOM:
			s1 = rectangle.getBottomLeft();
			s2 = rectangle.getBottomRight();
			break;
		case PositionConstants.LEFT:
			s1 = rectangle.getTopLeft();
			s2 = rectangle.getBottomLeft();
			break;
		case PositionConstants.RIGHT:
			s1 = rectangle.getTopRight();
			s2 = rectangle.getBottomRight();
			break;
		}
		return getDistanceFrom(s1, s2, p);
	}

	public static int getDistantSide(Rectangle boundary, Point toTest) {

		int[] sides = new int[] { PositionConstants.LEFT,
				PositionConstants.RIGHT, PositionConstants.TOP,
				PositionConstants.BOTTOM };

		int closestSide = PositionConstants.LEFT;
		double maxDistance = Double.MIN_VALUE;

		for (int idx = 0; idx < sides.length; idx++) {
			int side = sides[idx];

			double distance = getMaxDistanceFromEdge(boundary, toTest, side);

			if (distance < maxDistance) {
				maxDistance = distance;
				closestSide = side;
			}
		}

		return closestSide;
	}

	/**
	 * Returns the maximal distance from the test point to any point on the
	 * edges of the given rectangle.
	 * 
	 * @param boundary
	 *            rectangle to test
	 * @param toTest
	 *            point to test
	 * @return the distance between the given point and the nearest edge of the
	 *         rectangle.
	 * @since 3.1
	 */
	public static double getMaxDistanceFrom(Rectangle boundary, Point toTest) {
		return Math.sqrt(max(boundary.getTopLeft().getDistance2(toTest),
				boundary.getTopRight().getDistance2(toTest), boundary
						.getBottomLeft().getDistance2(toTest), boundary
						.getBottomRight().getDistance2(toTest)));
	}

	public static double getMaxDistanceFromEdge(Rectangle rectangle,
			Point testPoint, int edgeOfInterest) {
		switch (edgeOfInterest) {
		case PositionConstants.TOP:
			return Math.max(rectangle.getTopLeft().getDistance(testPoint),
					rectangle.getTopRight().getDistance(testPoint));
		case PositionConstants.BOTTOM:
			return Math.max(rectangle.getBottomLeft().getDistance(testPoint),
					rectangle.getBottomRight().getDistance(testPoint));
		case PositionConstants.LEFT:
			return Math.max(rectangle.getTopLeft().getDistance(testPoint),
					rectangle.getBottomLeft().getDistance(testPoint));
		case PositionConstants.RIGHT:
			return Math.max(rectangle.getTopRight().getDistance(testPoint),
					rectangle.getBottomRight().getDistance(testPoint));
		}
		return 0;
	}

	public static boolean inSegment(Point p, Point segmentSource,
			Point segmentTarget) {
		return ((isCollinear(p, segmentSource, segmentTarget)) && (inspectCollinearPoint(
				p, segmentSource, segmentTarget) == LIES_ON));

	}

	private static InspectResult inspectBasicLine(Point source1, Point target1,
			Point source2, Point target2) {
		double x1 = source1.x;
		double x2 = target1.x;
		double x3 = source2.x;
		double x4 = target2.x;

		double y1 = source1.y;
		double y2 = target1.y;
		double y3 = source2.y;
		double y4 = target2.y;

		double dx_1_2 = x2 - x1;
		double dy_1_2 = y2 - y1;
		double dx_3_4 = x4 - x3;
		double dy_3_4 = y4 - y3;

		InspectResult result = new InspectResult();

		double det = (dx_1_2 * dy_3_4) - (dy_1_2 * dx_3_4);

		double bound_det = (Math.abs(x2) + Math.abs(x1))
				* (Math.abs(y4) + Math.abs(y3)) + (Math.abs(y2) + Math.abs(y1))
				* (Math.abs(x4) + Math.abs(x3));

		if (Math.abs(det) <= bound_det * EPSILON) {
			result.parallel = true;
		} else {
			result.parallel = false;

			Point pt = null;
			if (inSegment(source1, source2, target2)) {
				pt = source1;
			} else if (inSegment(target1, source2, target2)) {
				pt = target1;
			} else if (inSegment(source2, source1, target1)) {
				pt = source2;
			} else if (inSegment(target2, source1, target1)) {
				pt = target2;
			}

			if (pt != null) {

				result.intersectionPoint = new Point(pt);

				result.orderOnThis = LIES_ON;
				result.orderOnParam = LIES_ON;

				return result;
			}

			double dx_1_3 = x3 - x1;
			double dy_1_3 = y3 - y1;

			double lambda = ((dx_1_3 * dy_3_4) - (dy_1_3 * dx_3_4)) / det;
			double mue = ((dx_1_3 * dy_1_2) - (dy_1_3 * dx_1_2)) / det;

			double new_x;
			double new_y;

			if ((dx_3_4 == 0) || (mue == 0)) {
				new_x = x3;
			} else {
				new_x = x1 + (lambda * dx_1_2);
			}

			if ((dy_3_4 == 0) || (mue == 0)) {
				new_y = y3;
			} else {
				new_y = y1 + (lambda * dy_1_2);
			}

			result.intersectionPoint = new Point(new_x, new_y);

			result.orderOnThis = inspectCollinearPoint(
					result.intersectionPoint, source2, target2);
			result.orderOnParam = inspectCollinearPoint(
					result.intersectionPoint, source1, target1);

		} // if

		return (result);

	}

	private static int inspectCollinearPoint(Point p, Point s1, Point s2) {
		float delta_x = s2.x - s1.x;
		float delta_y = s2.y - s1.y;
		int output_position = LIES_BEHIND;

		if (Math.abs(delta_x) >= Math.abs(delta_y)) {
			float x_abs = Math.abs(p.x);

			if (delta_x > 0) {
				if (p.x < s1.x - (x_abs + Math.abs(s1.x)) * EPSILON) {
					output_position = LIES_BEFORE;
				} else if (p.x <= s2.x + (x_abs + Math.abs(s2.x)) * EPSILON) {
					output_position = LIES_ON;
				}
			} else {
				if (p.x > s1.x + (x_abs + Math.abs(s1.x)) * EPSILON) {
					output_position = LIES_BEFORE;
				} else if (p.x >= s2.x - (x_abs + Math.abs(s2.x)) * EPSILON) {
					output_position = LIES_ON;
				}
			}
		} else {
			float y_abs = Math.abs(p.y);

			if (delta_y > 0) {
				if (p.y < s1.y - (y_abs + Math.abs(s1.y)) * EPSILON) {
					output_position = LIES_BEFORE;
				} else if (p.y <= s2.y + (y_abs + Math.abs(s2.y)) * EPSILON) {
					output_position = LIES_ON;
				}
			} else {
				if (p.y > s1.y + (y_abs + Math.abs(s1.y)) * EPSILON) {
					output_position = LIES_BEFORE;
				} else if (p.y >= s2.y - (y_abs + Math.abs(s2.y)) * EPSILON) {
					output_position = LIES_ON;
				}
			}
		}

		return (output_position);

	}

	private static int inspectPointCollinearWithRay(Point start, Ray direction,
			Point input_point) {
		Point _target = new Point(start.x + direction.x, start.y + direction.y);
		int output_position = LIES_ON;

		if (compareX(start, _target) < 0) {
			if (compareX(input_point, start) < 0) {
				output_position = LIES_BEFORE;
			}
		} else {

			if (compareX(input_point, start) > 0) {
				output_position = LIES_BEFORE;
			}
		}

		return (output_position);

	}

	public static boolean isCollinear(Point p, Point s1, Point s2) {
		return (orientation(p, s1, s2) == ORIENTATION_COLLINEAR);

	}

	public static double max(double... values) {
		double result = Double.MIN_VALUE;
		for (double d : values) {
			if (d > result) {
				result = d;
			}
		}
		return result;
	}

	public static int orientation(Point p, Point s1, Point s2) {
		int output_orientation;

		if (s1.equals(s2)) {
			if (p.equals(s1)) {
				output_orientation = ORIENTATION_COLLINEAR;
			} else {
				output_orientation = ORIENTATION_UNDEFINED;
			} // if
		} else {
			double area = (((s1.x - p.x) * (s2.y - p.y)) - ((s1.y - p.y) * (s2.x - p.x)));
			double bound = (((Math.abs(s1.x) + Math.abs(p.x)) * (Math.abs(s2.y) + Math
					.abs(p.y))) + ((Math.abs(s1.y) + Math.abs(p.y)) * (Math
					.abs(s2.x) + Math.abs(p.x))));

			if (Math.abs(area) <= bound * EPSILON) {
				output_orientation = ORIENTATION_COLLINEAR;
			} else {
				if (area < 0) {
					output_orientation = ORIENTATION_RIGHT;
				} else {
					output_orientation = ORIENTATION_LEFT;
				} // if
			} // if
		} // if

		return (output_orientation);

	}

	public static Point rayRectangleIntersection(Point rayStart, Ray direction,
			Rectangle r) {
		Point result = raySegmentIntersection(rayStart, direction,
				r.getTopLeft(), r.getTopRight());
		if (result != null) {
			return result;
		}
		result = raySegmentIntersection(rayStart, direction, r.getTopRight(),
				r.getBottomRight());
		if (result != null) {
			return result;
		}
		result = raySegmentIntersection(rayStart, direction,
				r.getBottomRight(), r.getBottomLeft());
		if (result != null) {
			return result;
		}
		result = raySegmentIntersection(rayStart, direction, r.getTopLeft(),
				r.getBottomLeft());
		return result;
	}

	/**
	 * Returns the intersection of a ray with a line segment. The ray is defined
	 * by a starting point and a direction, the segment is defined by providing
	 * source and target point. Returns null if the ray does not intersect with
	 * the segment. In case the segment lies on the ray, the end point of the
	 * segment which is further away from the ray's starting point is returned.
	 * 
	 * @param rayStart
	 * @param direction
	 * @param segmentSource
	 * @param segmentTarget
	 * @return
	 */
	public static Point raySegmentIntersection(Point rayStart, Ray direction,
			Point segmentSource, Point segmentTarget) {
		Point rayMid = new Point(rayStart.x + direction.x, rayStart.y
				+ direction.y);
		InspectResult prepare = inspectBasicLine(rayStart, rayMid,
				segmentSource, segmentTarget);

		if (prepare.parallel) {
			if (segmentSource.equals(segmentTarget)) {

				if (isCollinear(segmentSource, rayStart, rayMid)
						&& inspectCollinearPoint(segmentSource, rayStart,
								rayMid) == LIES_ON) {
					return segmentSource.getCopy();
				} else {
					return null;
				} // if
			} else {
				if (isCollinear(rayStart, segmentSource, segmentTarget)) {

					boolean is_source_on_ray = (inspectPointCollinearWithRay(
							rayStart, direction, segmentSource) == LIES_ON);
					boolean is_target_on_ray = (inspectPointCollinearWithRay(
							rayStart, direction, segmentTarget) == LIES_ON);

					if (is_source_on_ray || is_target_on_ray) {
						Point new_source;
						Point new_target;

						if (is_source_on_ray && is_target_on_ray) {
							new_source = segmentSource.getCopy();
							new_target = segmentTarget.getCopy();
						} else if (is_source_on_ray) {
							new_source = segmentSource.getCopy();
							new_target = rayStart.getCopy();
						} else {
							new_source = rayStart.getCopy();
							new_target = segmentTarget.getCopy();
						}

						if (new_source.equals(new_target)) {
							return new_source;
						} else {
							if (rayStart.getDistance2(new_source) > rayStart
									.getDistance2(new_target)) {
								return new_source;
							} else {
								return new_target;
							}
						}
					} else {
						return null;
					} // if
				} else {
					return null;
				}
			}
		} else {
			if ((prepare.orderOnThis == LIES_ON)
					&& (prepare.orderOnParam != LIES_BEFORE)) {
				return prepare.intersectionPoint;
			} else {
				return null;
			}
		}

	}

}
