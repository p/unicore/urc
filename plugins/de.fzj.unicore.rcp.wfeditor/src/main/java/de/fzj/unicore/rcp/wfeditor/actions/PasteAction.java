package de.fzj.unicore.rcp.wfeditor.actions;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.GroupRequest;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;

import de.fzj.unicore.rcp.wfeditor.WFConstants;

public class PasteAction extends WFEditorAction {

	public PasteAction(IWorkbenchPart part) {
		super(part);

	}

	public PasteAction(IWorkbenchPart part, int style) {
		super(part, style);

	}

	@Override
	protected boolean calculateEnabled() {
		Command cmd = createPasteCommand(getSelectedObjects());
		if (cmd == null) {
			return false;
		}
		return cmd.canExecute();
	}

	public Command createPasteCommand(List<?> objects) {
		if (objects == null || objects.isEmpty()) {
			return null;
		}
		if (!(objects.get(0) instanceof EditPart)) {
			return null;
		}

		GroupRequest req = new GroupRequest(WFConstants.REQ_PASTE);
		req.setEditParts(objects);

		Command cmd = null;
		for (int i = 0; i < objects.size(); i++) {
			EditPart object = (EditPart) objects.get(i);
			cmd = object.getCommand(req);
			if (cmd != null) {
				return cmd;
			}
		}
		return null;

	}

	@Override
	protected void init() {
		setId(ActionFactory.PASTE.getId());
		setText("Paste");
		setToolTipText("Paste elements from clipboard");
		ISharedImages sharedImages = PlatformUI.getWorkbench()
				.getSharedImages();
		setImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_PASTE));
		setDisabledImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_PASTE_DISABLED));
		setEnabled(false);
	}

	/**
	 * Performs the paste from clipboard action on the selected objects.
	 */
	@Override
	public void run() {
		execute(createPasteCommand(getSelectedObjects()));
	}

}
