/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.properties;

import java.util.List;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import de.fzj.unicore.rcp.common.utils.ValidityChangeEvent;
import de.fzj.unicore.rcp.wfeditor.extensions.CommonConditionTypeControl;
import de.fzj.unicore.rcp.wfeditor.model.IPropertySourceWithListeners;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType;
import de.fzj.unicore.rcp.wfeditor.ui.ConditionTypeControlFactory;
import de.fzj.unicore.rcp.wfeditor.ui.IConditionChangeListener;
import de.fzj.unicore.rcp.wfeditor.ui.IConditionTypeControlProducer;
import de.fzj.unicore.rcp.wfeditor.ui.ValueChangeEvent;

/**
 * @author demuth
 * 
 */
public class ConditionSection extends AbstractSection implements
		IConditionChangeListener {

	private IConditionTypeControlProducer controlProducer;
	private Condition condition;
	private Composite composite;
	private CCombo conditionTypeCombo;

	private CommonConditionTypeControl conditionTypeControl;

	public boolean checkInputValid() {
		return controlProducer != null && controlProducer.isInputValid();

	}

	@Override
	public void createControls(Composite parent,
			TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);

		composite = getWidgetFactory().createComposite(parent);

		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);

		Label typeLabel = new Label(composite, SWT.NONE);
		typeLabel.setText("Condition type:");
		typeLabel.setBackground(composite.getBackground());
		controls.add(new Label(composite, SWT.NONE));

		conditionTypeCombo = getWidgetFactory().createCCombo(composite);
		GridData data = new GridData(SWT.BEGINNING, SWT.TOP, false, false);

		conditionTypeCombo.setBackground(null);
		conditionTypeCombo.setLayoutData(data);
		controls.add(conditionTypeCombo);

	}

	@Override
	public void dispose() {
		if (controlProducer != null) {
			controlProducer.removeConditionChangeListener(this);
		}
	}

	private Condition getCondition() {
		return condition;
	}

	private IPropertySourceWithListeners getConditionalElement() {
		return element;
	}

	protected int getSelectedTypeIndex(Condition condition) {
		IConditionType type = condition.getSelectedType();
		if (type == null) {
			return 0;
		}
		List<IConditionType> types = condition.getAvailableTypes();
		for (int i = 0; i < types.size(); i++) {
			if (types.get(i).getTypeName().equals(type.getTypeName())) {
				return i;
			}
		}
		return 0;
	}

	protected void init() {
		if (getConditionalElement() == null) {
			return;
		}
		condition = (Condition) getConditionalElement().getPropertyValue(
				Condition.PROP_CONDITION);
		initConditionTypeCombo();
	}

	protected void initConditionTypeCombo() {
		if (element == null || conditionTypeCombo == null
				|| getCondition() == null) {
			return;
		}
		Condition condition = getCondition();
		conditionTypeCombo.setItems(condition.getAvailableTypeNames());
		conditionTypeCombo.select(getSelectedTypeIndex(condition));

		conditionTypeCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int index = ((CCombo) e.getSource()).getSelectionIndex();
				setSelectedTypeIndex(getCondition(), index);
				conditionTypeControl.dispose();
				if (controlProducer != null) {
					controlProducer
							.removeConditionChangeListener(ConditionSection.this);
				}
				controlProducer = ConditionTypeControlFactory
						.getControlProducer(getCondition());
				controlProducer
						.addConditionChangeListener(ConditionSection.this);
				if (conditionTypeControl != null) {
					controls.remove(conditionTypeControl);
				}
				conditionTypeControl = controlProducer.getControl(composite,
						getCondition());
				controls.add(conditionTypeControl);
				conditionTypeControl.pack();
				composite.layout();
				if (!checkInputValid()) {
					return;
				}
				updateParent();
				element.getDiagram().setDirty(true);
			}
		});
		if (conditionTypeControl != null) {
			conditionTypeControl.dispose();
		}
		if (controlProducer != null) {
			controlProducer.removeConditionChangeListener(this);
		}

		controlProducer = ConditionTypeControlFactory
				.getControlProducer(condition);
		controlProducer.addConditionChangeListener(this);
		if (conditionTypeControl != null) {
			controls.remove(conditionTypeControl);
		}
		conditionTypeControl = controlProducer.getControl(composite, condition);
		controls.add(conditionTypeControl);
		composite.layout();

	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
		init();
		updateEnabled();
	}

	protected void setSelectedTypeIndex(Condition condition, int index) {
		List<IConditionType> types = condition.getAvailableTypes();
		IConditionType type = types.get(index);
		condition.setSelectedType(type);
		updateParent();
	}

	public void updateParent() {
		// the condition has changed => upda098te the parent
		getConditionalElement().setPropertyValue(Condition.PROP_CONDITION,
				getCondition());
		element.getDiagram().setDirty(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.ui.IValidityChangeListener#validityChanged
	 * (boolean)
	 */
	public void validityChanged(ValidityChangeEvent e) {

	}

	public void valueChanged(ValueChangeEvent e) {
		if (!checkInputValid()) {
			return;
		}
		updateParent();
	}

}
