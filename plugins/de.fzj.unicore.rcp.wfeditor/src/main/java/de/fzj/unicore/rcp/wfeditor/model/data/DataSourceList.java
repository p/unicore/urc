package de.fzj.unicore.rcp.wfeditor.model.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

@XStreamAlias("DataSourceList")
@SuppressWarnings("unchecked")
public class DataSourceList extends PropertySource {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3124802025782156862L;

	/**
	 * The key for the data sources property. The value is a list containing
	 * {@link IDataSource} objects
	 */
	public static final String PROP_DATA_SOURCES = "data sources";

	private transient Map<String, IDataSource> map = new HashMap<String, IDataSource>();

	private transient IDataSource[] disposedSources = null;

	public DataSourceList() {
		activate();
	}

	public void addDataSource(IDataSource source) {
		addDataSource(source, -1);
	}

	public void addDataSource(IDataSource source, int index) {
		if (getMap().containsKey(source.getId())) {
			return; // was added before
		}
		List<IDataSource> sources = (List<IDataSource>) getPropertyValue(PROP_DATA_SOURCES);
		if (sources == null) {
			sources = new ArrayList<IDataSource>();
		}
		if (index < 0) {
			index = sources.size() + index;
		}
		getMap().put(source.getId(), source);
		sources.add(source);
		source.setParentSourceList(this);
		setPropertyValue(PROP_DATA_SOURCES, sources);
	}

	@Override
	protected void addPropertyDescriptors() {
		// TODO Auto-generated method stub

	}

	public void clear() {
		List<IDataSource> sources = (List<IDataSource>) getPropertyValue(PROP_DATA_SOURCES);
		if (sources != null && sources.size() > 0) {
			getMap().clear();
			sources.clear();
			setPropertyValue(PROP_DATA_SOURCES, sources);
		}
	}

	@Override
	public void dispose() {
		super.dispose();
		disposedSources = getDataSources();
		for (IDataSource source : disposedSources) {
			if (!source.isDisposed()) {
				source.dispose();
			}
		}
	}

	@Override
	public DataSourceList getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		DataSourceList copy = (DataSourceList) super.getCopy(toBeCopied,
				mapOfCopies, copyFlags);
		copy.map = null;
		for (IDataSource source : getDataSources()) {
			IDataSource newSource = CloneUtils.getFromMapOrCopy(source,
					toBeCopied, mapOfCopies, copyFlags);
			copy.addDataSource(newSource);
		}
		return copy;
	}

	public IDataSource getDataSourceById(String id) {
		return getMap().get(id);
	}

	public IDataSource[] getDataSources() {
		List<IDataSource> sources = (List<IDataSource>) getPropertyValue(PROP_DATA_SOURCES);
		if (sources == null) {
			return new IDataSource[0];
		} else {
			return sources.toArray(new IDataSource[sources.size()]);
		}
	}

	private Map<String, IDataSource> getMap() {
		if (map == null) {
			restoreMap();
		}
		return map;
	}

	public int indexOf(IDataSource source) {
		List<IDataSource> sources = (List<IDataSource>) getPropertyValue(PROP_DATA_SOURCES);
		if (sources == null) {
			return -1;
		}
		return sources.indexOf(source);
	}

	public int removeDataSource(IDataSource source) {
		List<IDataSource> sources = (List<IDataSource>) getPropertyValue(PROP_DATA_SOURCES);
		int result = sources == null ? -1 : sources.indexOf(source);
		if (result >= 0) {
			getMap().remove(source.getId());
			sources.remove(source);
			setPropertyValue(PROP_DATA_SOURCES, sources);
		}
		return result;
	}

	public int removeDataSource(String id) {
		IDataSource source = getDataSourceById(id);
		return removeDataSource(source);
	}

	private void restoreMap() {
		map = new HashMap<String, IDataSource>();
		for (IDataSource sink : getDataSources()) {
			map.put(sink.getId(), sink);
		}
	}

	public int size() {
		return getMap().size();
	}

	@Override
	public void undoDispose() {
		if (disposedSources != null) {
			for (IDataSource source : disposedSources) {
				if (source.isDisposed()) // do not undispose twice!
				{
					source.undoDispose();
				}
			}
		}
		disposedSources = null;
		super.undoDispose();
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		if (updatedToCurrentModelVersion) {
			return; // don't do this twice!
		}
		super.updateToCurrentModelVersion(oldVersion, currentVersion);
		for (IDataSource source : getDataSources()) {
			source.updateToCurrentModelVersion(oldVersion, currentVersion);
		}
	}
}
