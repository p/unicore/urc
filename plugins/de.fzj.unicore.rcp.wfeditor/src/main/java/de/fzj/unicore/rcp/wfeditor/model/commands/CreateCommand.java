/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;

/**
 * @author Daniel Lee
 */
public class CreateCommand extends Command {

	private EditPart parent;
	private IActivity child;

	// location where to create the node
	private Rectangle rect;

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void execute() {
		if (rect != null) {
			setChildBounds(rect);
		}
		EditPart childPart = parent.getViewer().getEditPartFactory()
				.createEditPart(parent, child);
		parent.getViewer().getEditPartRegistry().put(child, childPart);

	}

	private Insets getInsets() {
		return new Insets(0, 0, 0, 0);
	}

	@Override
	public void redo() {

		if (child.isDisposed()) {
			child.undoDispose();
		}
	}

	/**
	 * Sets the Activity to create
	 * 
	 * @param activity
	 *            the Activity to create
	 */
	public void setChild(IActivity activity) {
		child = activity;
		if (child.getName() == null) {
			child.setName("Activity " + (parent.getChildren().size() + 1));
		}
	}

	private void setChildBounds(Rectangle rect) {
		Insets expansion = getInsets();
		if (!rect.isEmpty()) {
			rect.expand(expansion);
		} else {
			rect.x -= expansion.left;
			rect.y -= expansion.top;
		}
		Point oldBounds = child.getLocation();
		Dimension diff = rect.getLocation().getDifference(oldBounds);
		child.move(diff.width, diff.height);
		if (!rect.isEmpty()) {
			child.setSize(rect.getSize());
		}
	}

	public void setLocation(Rectangle r) {
		rect = r;
	}

	/**
	 * Sets the parent WorkflowDiagram
	 * 
	 * @param sa
	 *            the parent
	 */
	public void setParent(EditPart parent) {
		this.parent = parent;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {

		child.dispose();
	}

}
