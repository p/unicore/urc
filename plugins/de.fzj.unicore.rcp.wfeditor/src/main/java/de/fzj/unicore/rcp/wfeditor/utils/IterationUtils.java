package de.fzj.unicore.rcp.wfeditor.utils;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;

public class IterationUtils {

	public static String fixIterationId(WorkflowDiagram model, String iteration) {

		String result = iteration;
		// cut off the last part of the iteration string since this part should
		// be constant
		int index = iteration.lastIndexOf(Constants.ITERATION_ID_SEPERATOR);
		if (index > 0) {
			result = iteration.substring(0, index);
		}
		return result;
	}

}
