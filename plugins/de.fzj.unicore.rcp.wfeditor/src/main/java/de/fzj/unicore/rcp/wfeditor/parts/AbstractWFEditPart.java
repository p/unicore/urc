package de.fzj.unicore.rcp.wfeditor.parts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

public abstract class AbstractWFEditPart extends AbstractGraphicalEditPart {

	/**
	 * The corresponding method in our superclass reuses parts when two model
	 * objects are equal, but it does NOT correct the reference to the model
	 * object in the reused part, which is EVIL.
	 */
	@Override
	protected void refreshChildren() {
		super.refreshChildren();
		int i;
		EditPart editPart;
		Object model;

		Map<Object, EditPart> modelToEditPart = new HashMap<Object, EditPart>();
		List<?> children = getChildren();

		for (i = 0; i < children.size(); i++) {
			editPart = (EditPart) children.get(i);
			modelToEditPart.put(editPart.getModel(), editPart);
		}

		List<?> modelObjects = getModelChildren();

		for (i = 0; i < modelObjects.size(); i++) {
			model = modelObjects.get(i);
			EditPart part = (EditPart) children.get(i);
			// Do a quick check to see if editPart[i] == model[i]
			if (i < children.size() && part.getModel() == model) {
				continue;
			}
			part.setModel(model);

		}
	}
}
