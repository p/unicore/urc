package de.fzj.unicore.rcp.wfeditor.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;

/**
 * Class used to initialize default preference values.
 */
public class WFPreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = WFActivator.getDefault().getPreferenceStore();
//		store.setDefault(WFConstants.P_WORKFLOW_TERMINATION_TIME,
//				Constants.DEFAULT_TERMINATION_TIME);
		store.setDefault(WFConstants.P_WORKFLOW_POLLING_INTERVAL, 5);
	}

}
