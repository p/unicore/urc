/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.utils.StringUtils;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.FlowElement;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFileList;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

@XStreamAlias("Activity")
public abstract class Activity extends FlowElement implements IActivity {

	static final long serialVersionUID = 1;
	private List<Transition> inputs = new ArrayList<Transition>();
	private List<Transition> outputs = new ArrayList<Transition>();

	/**
	 * The index of the activity referring to it's parent's list of activities.
	 */
	private int sortIndex;

	/**
	 * location of the node in the graph diagram
	 */
	protected Point location = new Point(0, 0);
	protected Dimension size = WFConstants.DEFAULT_SIMPLE_ACTIVITY_SIZE
	.getCopy();

	public Activity() {
		setPropertyValue(PROP_OUTPUT_FILES, new WorkflowFileList());
	}

	public Activity(String s) {
		this();
		setName(s);
	}

	@Override
	public void activate() {
		super.activate();
		if (getDiagram() != null && !getDiagram().equals(this)) {
			IActivity act = getDiagram().getActivityByName(getName());
			if (act == null) {
				getDiagram().addActivity(this);
			} else {
				if (act != this) {
					String myRawName = StringUtils.cutOffDigits(getName());
					setName(getDiagram().getUniqueActivityName(myRawName));
					getDiagram().addActivity(this);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.model.IActivity#addInput(de.fzj.unicore.rcp
	 * .wfeditor.model.Transition)
	 */
	public void addInput(Transition transition) {
		addInput(transition, inputs.size());
	}

	public void addInput(Transition transition, int index) {
		if (inputs.contains(transition)) {
			return; // never add the same transition twice!
		}
		inputs.add(index, transition);
		fireStructureChange(INPUTS, transition);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.model.IActivity#addOutput(de.fzj.unicore.
	 * rcp.wfeditor.model.Transition)
	 */
	public void addOutput(Transition transition) {
		addOutput(transition, outputs.size());
	}

	public void addOutput(Transition transition, int index) {
		if (outputs.contains(transition)) {
			return; // never add the same transition twice!
		}
		outputs.add(index, transition);
		fireStructureChange(OUTPUTS, transition);
	}

	public void addOutputFile(WorkflowFile f) {
		WorkflowFileList list = (WorkflowFileList) getPropertyValue(PROP_OUTPUT_FILES);
		list.addFile(f);
		setPropertyValue(PROP_OUTPUT_FILES, list);
		// only add file file workflow file list, NOT to the data source list
		// do this separately, otherwise, it's a nasty side effect
	}

	@Override
	protected void addPropertyDescriptors() {
		super.addPropertyDescriptors();
	}

	@Override
	public void afterDeserialization() {
		afterDeserialization(true);
	}

	protected void afterDeserialization(boolean doTransitions) {
		super.afterDeserialization();
		if(doTransitions)
		{
			for (Transition t : getOutgoingTransitions()) {
				t.afterDeserialization();
			}
		}
	}

	public void afterSaveAs(IPath newLocation, boolean recursive,
			boolean copyLocalInputFiles, boolean copyRemoteInputFiles,
			IProgressMonitor monitor) throws Exception {
		// do nothing by default

	}

	@Override
	public void afterSerialization() {
		afterSerialization(true);
	}

	protected void afterSerialization(boolean doTransitions) {
		super.afterSerialization();
		if(doTransitions)
		{
			for (Transition t : getOutgoingTransitions()) {
				t.afterSerialization();
			}
		}
	}

	public void beforeSaveAs(IPath newLocation, boolean recursive,
			boolean copyLocalInputFiles, boolean copyRemoteInputFiles,
			IProgressMonitor monitor) throws Exception {
		// do nothing by default

	}

	@Override
	public void beforeSerialization() {
		beforeSerialization(true);

	}

	protected void beforeSerialization(boolean doTransitions) {
		super.beforeSerialization();
		if(doTransitions)
		{
			for (Transition t : getOutgoingTransitions()) {
				t.beforeSerialization();
			}
		}

	}

	@Override
	public Activity clone() throws CloneNotSupportedException

	{
		Activity result = (Activity) super.clone();
		// deep clone size and location
		result.size = new Dimension(size);
		result.location = new Point(location.x, location.y);

		// new lists of inputs and outputs
		result.inputs = new ArrayList<Transition>();
		result.outputs = new ArrayList<Transition>();
		return result;
	}

	/**
	 * Take care of removing the activity from the diagrams map of activities!
	 */
	@Override
	public void dispose() {
		for (WorkflowFile wf : getOutputFiles()) {
			wf.dispose();
		}
		if (getDiagram() != null) {
			getDiagram().removeActivity(this);
		}
		super.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class adapter) {
		if (IActivity.class.equals(adapter)) {
			return this;
		} else {
			return super.getAdapter(adapter);
		}
	}

	@Override
	public Activity getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		try {
			Activity result = (Activity) super.getCopy(toBeCopied, mapOfCopies,
					copyFlags);

			// duplicate incoming transitions if they haven't been duplicated
			// before
			for (Transition t : getIncomingTransitions()) {
				CloneUtils.getFromMapOrCopy(t, toBeCopied, mapOfCopies,
						copyFlags);
			}

			// duplicate incoming transitions if they haven't been duplicated
			// before
			// note that it does not suffice to duplicate incoming transitions
			// only,
			// as not all transitions connect activities to be copied
			for (Transition t : getOutgoingTransitions()) {
				CloneUtils.getFromMapOrCopy(t, toBeCopied, mapOfCopies,
						copyFlags);
			}

			return result;
		} catch (Exception e) {
			WFActivator.log(IStatus.ERROR,
					"Could not create copy of the element " + getName(), e);
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.IActivity#getIncomingTransitions()
	 */
	public List<Transition> getIncomingTransitions() {
		return inputs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.IActivity#getLocation()
	 */
	public Point getLocation() {
		return location;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.IActivity#getOutgoingTransitions()
	 */
	public List<Transition> getOutgoingTransitions() {
		return outputs;
	}

	public WorkflowFile[] getOutputFiles() {
		return ((WorkflowFileList) getPropertyValue(PROP_OUTPUT_FILES))
		.getFiles();
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_OUTPUT_FILES);

		return propertiesToCopy;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.IActivity#getSize()
	 */
	public Dimension getSize() {
		return size;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.IActivity#getSortIndex()
	 */
	public int getSortIndex() {
		return sortIndex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.IActivity#move(int, int)
	 */
	public void move(int deltaX, int deltaY) {
		if (deltaX == 0 && deltaY == 0) {
			return;
		}
		Point location = getLocation();
		location = location.getTranslated(deltaX, deltaY);
		setLocation(location);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.model.IActivity#removeInput(de.fzj.unicore
	 * .rcp.wfeditor.model.Transition)
	 */
	public int removeInput(Transition transition) {
		int result = inputs.indexOf(transition);
		inputs.remove(transition);
		fireStructureChange(INPUTS, transition);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.model.IActivity#removeOutput(de.fzj.unicore
	 * .rcp.wfeditor.model.Transition)
	 */
	public int removeOutput(Transition transition) {
		int result = outputs.indexOf(transition);
		outputs.remove(transition);
		fireStructureChange(OUTPUTS, transition);
		return result;
	}

	public void removeOutputFile(WorkflowFile f) {
		WorkflowFileList list = (WorkflowFileList) getPropertyValue(PROP_OUTPUT_FILES);
		list.removeFile(f);
		setPropertyValue(PROP_OUTPUT_FILES, list);
		// only remove file from workflow file list, NOT from the data source
		// list
		// do this separately, otherwise, it's a nasty side effect

	}

	@Override
	public void setDiagram(WorkflowDiagram diagram) {
		super.setDiagram(diagram);
		for (Transition t : getOutgoingTransitions()) {
			t.setDiagram(diagram);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.model.IActivity#setLocation(org.eclipse.draw2d
	 * .geometry.Point)
	 */
	public void setLocation(Point location) {
		if (this.location.equals(location)) {
			return;
		}
		Object oldValue = this.location;
		this.location = location;
		firePropertyChange(PROP_LOCATION, oldValue, location);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.model.IActivity#setSize(org.eclipse.draw2d
	 * .geometry.Dimension)
	 */
	public void setSize(Dimension size) {
		if (size == null || this.size.equals(size)) {
			return;
		}
		Object oldValue = this.size;
		this.size = size;
		firePropertyChange(PROP_SIZE, oldValue, size);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.IActivity#setSortIndex(int)
	 */
	public void setSortIndex(int i) {
		sortIndex = i;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.IActivity#toString()
	 */
	@Override
	public String toString() {
		String className = getClass().getName();
		className = className.substring(className.lastIndexOf('.') + 1);
		return className + "(" + getName() + ")";
	}

	@Override
	public void undoDispose() {
		if (getDiagram() != null) {
			getDiagram().addActivity(this);
		}
		super.undoDispose();
		for (WorkflowFile wf : getOutputFiles()) {
			if (wf.isDisposed()) {
				wf.undoDispose();
			}
		}

	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {

		if (updatedToCurrentModelVersion) {
			return; // don't do this twice!
		}
		super.updateToCurrentModelVersion(oldVersion, currentVersion);
		if (oldVersion.compareTo("6.4.0") < 0) {
			// updating workflow files like this is only necessary for these
			// versions
			// for a newer version, the files would already be updated when
			// all data sources are updated
			for (WorkflowFile f : getOutputFiles()) {
				f.updateToCurrentModelVersion(oldVersion, currentVersion);
			}
		}

	}

}
