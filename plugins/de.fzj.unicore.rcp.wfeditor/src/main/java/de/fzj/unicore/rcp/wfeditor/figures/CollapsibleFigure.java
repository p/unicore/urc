/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.CompoundBorder;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;

import de.fzj.unicore.rcp.wfeditor.borders.CollapsibleBorder;

/**
 * A collapsable container figure has a collapsable border, as well as an image
 * and text for the border.
 */
public class CollapsibleFigure extends StructuredActivityFigure implements
		ICollapsibleFigure {

	protected CollapsibleBorder border;

	public CollapsibleFigure(Rectangle bounds, String title) {
		super(title, bounds);
		
	}

	@Override
	public Border createBorder() {
		Border superBorder = super.createBorder();
		border = new CollapsibleBorder(this);
		return new CompoundBorder(border, superBorder);
	}

	public Rectangle getRectExpandCollapse() {
		return border.getRectExpandCollapse();
	}

	@Override
	public void invalidate() {
		if (border != null) {
			border.invalidate();
		}
		super.invalidate();
	}

	public boolean isCollapsed() {
		return border.isCollapsed();
	}

	public boolean isPointInCollapseImage(int i, int j) {
		return border.isPointInCollapseImage(i, j);
	}

	@Override
	public void paint(Graphics graphics) {
		super.paint(graphics);
	}

	@Override
	public void setBounds(Rectangle bounds) {
		super.setBounds(bounds);
	}

	public void setCollapsed(boolean collapsed) {
		border.setCollapsed(collapsed);
		invalidate();
	}

	public void setRectExpandCollapse(Rectangle rectExpandCollapse) {
		border.setRectExpandCollapse(rectExpandCollapse);
	}

}
