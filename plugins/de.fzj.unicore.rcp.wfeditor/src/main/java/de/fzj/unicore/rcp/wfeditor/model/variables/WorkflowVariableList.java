package de.fzj.unicore.rcp.wfeditor.model.variables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.interfaces.ISerializable;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IDisposableWithUndo;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

@XStreamAlias("WorkflowVariableList")
public class WorkflowVariableList extends PropertySource implements
		IDisposableWithUndo, ICopyable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5334583634394677379L;
	private List<WorkflowVariable> variables;
	private transient Map<String, WorkflowVariable> map = new HashMap<String, WorkflowVariable>();

	public WorkflowVariableList() {

	}

	@Override
	public void activate() {
		super.activate();
		for (WorkflowVariable var : getVariables()) {
			var.activate();
		}
	}

	@Override
	protected void addPropertyDescriptors() {
		getPropertyDescriptorList().clear();
		for (WorkflowVariable current : getVariables()) {
			addPropertyDescriptor(new WorkflowVariablePropertyDescriptor(
					current));
		}
	}

	public void addVariable(WorkflowVariable v) {
		getVariables().add(v);
		getMap().put(v.getId(), v);
		setPropertyValue(v.getId(), v);
		addPropertyDescriptor(new WorkflowVariablePropertyDescriptor(v));
	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
		for (ISerializable current : getVariables()) {
			current.afterDeserialization();
		}
	}

	@Override
	public void afterSerialization() {
		super.afterSerialization();
		for (ISerializable current : getVariables()) {
			current.afterSerialization();
		}
	}

	@Override
	public void beforeSerialization() {
		super.beforeSerialization();
		for (ISerializable current : getVariables()) {
			current.beforeSerialization();
		}
	}

	@Override
	public void dispose() {
		for (WorkflowVariable var : getVariables()) {
			if (!var.isDisposed()) {
				var.dispose();
			}
		}
	}

	@Override
	public WorkflowVariableList getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		WorkflowVariableList copy = (WorkflowVariableList) super.getCopy(
				toBeCopied, mapOfCopies, copyFlags);
		copy.variables = new ArrayList<WorkflowVariable>();
		for (WorkflowVariable var : getVariables()) {
			WorkflowVariable newVar = CloneUtils.getFromMapOrCopy(var,
					toBeCopied, mapOfCopies, copyFlags);
			copy.addVariable(newVar);
		}
		return copy;
	}

	private Map<String, WorkflowVariable> getMap() {
		if (map == null) {
			restoreMap();
		}
		return map;
	}

	public WorkflowVariable getVariableById(String id) {
		return getMap().get(id);
	}
	
	public WorkflowVariable getVariableByName(String _name) {
		for (WorkflowVariable workflowVariable : variables) {
			if(workflowVariable.getName().equals(_name)) {
				return workflowVariable;
			}
		}
		return null;
	}

	public List<WorkflowVariable> getVariables() {
		if (variables == null) {
			variables = new ArrayList<WorkflowVariable>();
		}
		return variables;
	}

	public void removeVariable(WorkflowVariable v) {
		getVariables().remove(v);
		getMap().remove(v.getId());
		removeProperty(v.getId());
		removePropertyDescriptor(new WorkflowVariablePropertyDescriptor(v));
	}

	private void restoreMap() {
		map = new HashMap<String, WorkflowVariable>();
		for (WorkflowVariable var : getVariables()) {
			map.put(var.getId(), var);
		}
	}

	@Override
	public String toString() {
		return "";
	}

	@Override
	public void undoDispose() {
		for (WorkflowVariable var : getVariables()) {
			if (var.isDisposed()) {
				var.undoDispose();
			}
		}
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		if (updatedToCurrentModelVersion) {
			return; // don't do this twice!
		}
		super.updateToCurrentModelVersion(oldVersion, currentVersion);
		for (WorkflowVariable var : getVariables()) {
			var.updateToCurrentModelVersion(oldVersion, currentVersion);
		}
	}
}
