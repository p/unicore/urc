package de.fzj.unicore.rcp.wfeditor.model.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.expressions.ElementHandler;
import org.eclipse.core.expressions.EvaluationContext;
import org.eclipse.core.expressions.EvaluationResult;
import org.eclipse.core.expressions.Expression;
import org.eclipse.core.expressions.ExpressionConverter;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.common.utils.Handler;
import de.fzj.unicore.rcp.common.utils.HandlerOrderingUtil;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.IDataFlowAssembler;
import de.fzj.unicore.rcp.wfeditor.model.commands.DataFlowAssemblyCommand;
import de.fzj.unicore.uas.util.Pair;

/**
 * This class is used in conjunction with the DataFlowAssembler extension point
 * to create a data flow between an {@link IDataSource} and an {@link IDataSink}
 * . A data flow assembler extension may define an expression to be evaluated on
 * instances of this class in order to decide whether it is capable to create
 * the flow between this data source and sink.
 * 
 * @author bdemuth
 * 
 */
public class DataFlowFactory {

	private static final String ELEMENT_CAN_CONNECT = "canConnect";
	private static final String ELEMENT_ASSEMBLER = "assembler";

	private static final String ELEMENT_DATA_TYPE_MAPPING = "dataTypeMapping";
	private static final String ELEMENT_BEFORE = "before";
	private static final String ELEMENT_AFTER = "after";

	private static final String ATTRIBUTE_NAME = "name";
	private static final String ATTRIBUTE_PLURAL_NAME = "pluralName";
	private static final String ATTRIBUTE_INTERNAL_IDENTIFIER = "internalId";
	private static final String ATTRIBUTE_IS_REGEXP = "isRegExp";
	private static final String ATTRIBUTE_ENABLED_BY_DEFAULT = "enabledByDefault";
	private static final String ATTRIBUTE_MAPPING_NAME = "mappingName";

	private List<Pair<Expression, IConfigurationElement>> extensions = new ArrayList<Pair<Expression, IConfigurationElement>>();

	private ShowDataFlowsProperty defaultShowDataFlowsProperty;
	private Map<String, String> dataTypeMappings = new HashMap<String, String>();
	private Map<String, Boolean> isRegexpMap = new HashMap<String, Boolean>();
	private Map<String, String> pluralNameMap = new HashMap<String, String>();
	private String[] dataTypeNames;

	private static DataFlowFactory instance = null;

	private DataFlowFactory() {
		super();
		iterateOverExtensions();
	}

	private boolean alreadyConnected(IDataSource source, IDataSink sink) {
		for (IDataFlow flow : source.getOutgoingFlows()) {
			if (sink.getIncomingFlowById(flow.getId()) != null) {
				return true;
			}
		}
		return false;

	}

	public boolean canCreateDataFlow(IDataSource source, IDataSink sink) {
		if (getMatchingExtension(source, sink) == null
				|| alreadyConnected(source, sink)) {
			return false;
		}
		Command cmd;
		try {
			cmd = getDataFlowAssemblyCommand(source, sink);
			return cmd != null && cmd.canExecute();
		} catch (Exception e) {
			return false;
		}
	}

	public DataFlowAssemblyCommand getDataFlowAssemblyCommand(
			IDataSource source, IDataSink sink) throws Exception {
		IConfigurationElement ext = getMatchingExtension(source, sink);
		IDataFlowAssembler assembler = (IDataFlowAssembler) ext
				.createExecutableExtension("class");
		return assembler.getAssemblyCommand(source, sink);

	}

	public String getDataTypeId(String dataTypeName) {
		return dataTypeMappings.get(dataTypeName);
	}

	public String getDataTypeName(String dataType) {
		if (dataType == null) {
			return null;
		}
		for (String name : dataTypeMappings.keySet()) {
			String id = dataTypeMappings.get(name);
			boolean isRegexp = isRegexp(id);
			if (id.equals(dataType) || isRegexp && dataType.matches(id)) {
				return name;
			}
		}
		return null;
	}

	public String[] getDataTypeNames() {
		return dataTypeNames;
	}

	public ShowDataFlowsProperty getDefaultShowDataFlowsProperty() {
		return defaultShowDataFlowsProperty.clone();
	}

	private IConfigurationElement getMatchingExtension(IDataSource source,
			IDataSink sink) {
		EvaluationContext context = new EvaluationContext(null, source);
		context.setAllowPluginActivation(true);
		context.addVariable("source", source);
		context.addVariable("sink", sink);
		for (Pair<Expression, IConfigurationElement> ext : extensions) {
			try {

				EvaluationResult result = ext.getM1().evaluate(context);
				if (EvaluationResult.TRUE.equals(result)) {
					return ext.getM2();
				}
			} catch (CoreException e) {
				// do nothing
				e.printStackTrace();
			}
		}
		return null;
	}

	public String getPluralName(String dataTypeName) {
		return pluralNameMap.get(dataTypeName);
	}

	public boolean isRegexp(String dataTypeId) {
		return isRegexpMap.get(dataTypeId);
	}

	private void iterateOverExtensions() {
		defaultShowDataFlowsProperty = new ShowDataFlowsProperty(false, false,
				false, false);
		HandlerOrderingUtil sorter = new HandlerOrderingUtil();

		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(WFConstants.EXTENSION_POINT_DATA_FLOWS);
		IConfigurationElement[] members = extensionPoint
				.getConfigurationElements();
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			if (ELEMENT_ASSEMBLER.equals(member.getName())) {
				try {
					IConfigurationElement exprElement = member
							.getChildren(ELEMENT_CAN_CONNECT)[0].getChildren()[0];
					Expression expr = ElementHandler.getDefault().create(
							ExpressionConverter.getDefault(), exprElement);
					extensions.add(new Pair<Expression, IConfigurationElement>(
							expr, member));

				} catch (CoreException ex) {
					WFActivator.log("Could not add data flow assembler "
							+ member.getDeclaringExtension()
									.getUniqueIdentifier(), ex);
				}
			} else if (ELEMENT_DATA_TYPE_MAPPING.equals(member.getName())) {
				try {
					String name = member.getAttribute(ATTRIBUTE_NAME);
					String internalId = member
							.getAttribute(ATTRIBUTE_INTERNAL_IDENTIFIER);
					String isRegexpString = member
							.getAttribute(ATTRIBUTE_IS_REGEXP);
					boolean isRegexp = false;
					try {
						isRegexp = Boolean.parseBoolean(isRegexpString);
					} catch (Exception e) {
						// do nothing
					}
					String enabledByDefaultString = member
							.getAttribute(ATTRIBUTE_ENABLED_BY_DEFAULT);
					boolean enabledByDefault = false;
					try {
						enabledByDefault = Boolean
								.parseBoolean(enabledByDefaultString);
					} catch (Exception e) {
						// do nothing
					}
					if (enabledByDefault) {
						defaultShowDataFlowsProperty.showFlowsWithDataType(
								internalId, isRegexp);
					}
					dataTypeMappings.put(name, internalId);
					isRegexpMap.put(internalId, isRegexp);
					String pluralName = member
							.getAttribute(ATTRIBUTE_PLURAL_NAME);
					pluralNameMap.put(name, pluralName);
					Handler h = new Handler(name, name);
					Set<String> before = new HashSet<String>();
					IConfigurationElement[] beforeElements = member
							.getChildren(ELEMENT_BEFORE);
					for (IConfigurationElement beforeElement : beforeElements) {
						String mappingName = beforeElement
								.getAttribute(ATTRIBUTE_MAPPING_NAME);
						before.add(mappingName);
					}
					h.setBefore(before);

					Set<String> after = new HashSet<String>();
					IConfigurationElement[] afterElements = member
							.getChildren(ELEMENT_AFTER);
					for (IConfigurationElement afterElement : afterElements) {
						String mappingName = afterElement
								.getAttribute(ATTRIBUTE_MAPPING_NAME);
						after.add(mappingName);
					}
					h.setAfter(after);
					sorter.insertHandler(h);
				} catch (Exception ex) {
					WFActivator.log("Could not add data flow assembler "
							+ member.getDeclaringExtension()
									.getUniqueIdentifier(), ex);
				}

				dataTypeNames = new String[sorter.getHandlers().size()];
				for (int i = 0; i < dataTypeNames.length; i++) {
					Handler h = sorter.getHandlers().get(i);
					dataTypeNames[i] = h.getId();
				}
			}
		}
	}

	public static DataFlowFactory getInstance() {
		if (instance == null) {
			instance = new DataFlowFactory();
		}
		return instance;
	}

}
