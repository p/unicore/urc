package de.fzj.unicore.rcp.wfeditor.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ValidationError")
public class ValidationError {

	private String description;

	public ValidationError(String description) {
		super();
		this.description = description;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ValidationError other = (ValidationError) obj;
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		return true;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return getDescription();
	}

}
