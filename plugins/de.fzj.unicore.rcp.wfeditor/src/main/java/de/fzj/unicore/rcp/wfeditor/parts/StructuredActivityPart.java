/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Subgraph;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.figures.StructuredActivityFigure;
import de.fzj.unicore.rcp.wfeditor.layouts.AutomaticGraphLayout;
import de.fzj.unicore.rcp.wfeditor.layouts.DataFlowConnectionRouter;
import de.fzj.unicore.rcp.wfeditor.layouts.GraphAnimation;
import de.fzj.unicore.rcp.wfeditor.layouts.IAutomaticGraphLayout;
import de.fzj.unicore.rcp.wfeditor.layouts.StructuredActivityLayout;
import de.fzj.unicore.rcp.wfeditor.layouts.WFConnectionRouter;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.commands.MoveActivityCommand;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.policies.ActivityContainerEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.ActivityContainerHighlightEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.ActivityEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.ActivityNodeEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.CopyEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.CutEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.PasteEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.RemoveIllegalTransitionsEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.ResizeContainerPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.ShowPropertyViewPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.StructuredActivityLayoutEditPolicy;
import de.fzj.unicore.rcp.wfeditor.ui.WFRootEditPart;

/**
 * @author hudsonr, demuth
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public abstract class StructuredActivityPart extends ActivityPart implements
NodeEditPart {

	protected Map<Object, ConnectionRouter> connectionRouterMap = new HashMap<Object, ConnectionRouter>();

	@Override
	public void activate() {
		super.activate();

	}

	@Override
	protected void addChild(EditPart child, int index) {
		super.addChild(child, index);

	}

	@Override
	protected boolean allowsDirectEdit(Point requestLoc) {
		// IFigure header = ((StructuredActivityFigure)getFigure()).getHeader();
		// header.translateToRelative(requestLoc);
		// if (header.containsPoint(requestLoc))
		// return true;
		return false;
	}

	protected void applyChildrenLayouts(CompoundDirectedGraph graph, Map map,
			CompoundCommand cmd) {
		for (int i = 0; i < getChildren().size(); i++) {
			ActivityPart part = (ActivityPart) getChildren().get(i);
			part.applyLayout(graph, map, cmd);
		}
	}

	/**
	 * Save location and size changes to model after this part's figure has been
	 * layouted.
	 */
	@Override
	public void applyLayout(CompoundDirectedGraph graph, Map map,
			CompoundCommand cmd) {
		boolean isTopLevel = map.get(getParent()) == null;
		Point oldLocation = null;
		if (isTopLevel) {
			// we are top level! => the layout algorithm has placed us in the
			// wrong location
			// as it always places us at the origin instead of keeping our
			// original location
			oldLocation = getModel().getLocation();
		}
		applyOwnLayout(graph, map, cmd);
		applyChildrenLayouts(graph, map, cmd);
		if (isTopLevel) {
			cmd.add(new MoveActivityCommand(getModel(),
					oldLocation));
		}
	}

	protected void applyOwnLayout(CompoundDirectedGraph graph, Map map,
			CompoundCommand cmd) {
		Subgraph n = (Subgraph) map.get(this);
		if ((getModel().getDirection() & PositionConstants.EAST) != 0) {

			int temp = n.x;
			n.x = n.y;
			n.y = temp;
			temp = n.width;
			n.width = n.height;
			n.height = temp;
		}

		super.applyLayout(graph, map, cmd);

	}


	@Override
	public void contributeNodesToGraph(CompoundDirectedGraph graph, Subgraph s,
			Map map) {
		GraphAnimation.recordInitialState(getContentPane());
		Subgraph me = new Subgraph(this, s);
		me.outgoingOffset = 5;
		me.incomingOffset = 5;
		IActivity act = getModel();
		me.width = act.getSize().width;
		me.height = act.getSize().height;
		me.x = act.getLocation().x;
		me.y = act.getLocation().y;
		me.innerPadding = new Insets(getInnerPadding());

		me.setPadding(getPadding());
		map.put(this, me);
		graph.nodes.add(me);
		if(getChildren().size() == 0)
		{
			// empty containers' sizes can only be influenced via inner padding
			
			Dimension dim = WFConstants.DEFAULT_EMPTY_CONTAINER_SIZE;
			me.innerPadding = new Insets(dim.height/2,dim.width/2,dim.height/2,dim.width/2);
			IFigure f = getFigure();
			if(f.getBorder() != null)
			{
				Insets i = f.getBorder().getInsets(f);
				me.innerPadding.add(i);
			}
			
		}
		else
		{
			for (int i = 0; i < getChildren().size(); i++) {
				ActivityPart activity = (ActivityPart) getChildren().get(i);
				activity.contributeNodesToGraph(graph, me, map);
			}
		}
	}

	/**
	 * @see de.fzj.unicore.rcp.wfeditor.parts.ActivityPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE,
				new ActivityNodeEditPolicy());
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new ActivityEditPolicy());
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE,
				new ActivityContainerHighlightEditPolicy());
		installEditPolicy(EditPolicy.CONTAINER_ROLE,
				new ActivityContainerEditPolicy());
		ResizeContainerPolicy resize = new ResizeContainerPolicy();
		installEditPolicy(WFConstants.POLICY_AUTO_RESIZE_CONTAINER, resize);
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new StructuredActivityLayoutEditPolicy());
		// installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new
		// StructuredActivityDirectEditPolicy());
		installEditPolicy(WFConstants.POLICY_OPEN, new ShowPropertyViewPolicy());

		installEditPolicy(WFConstants.POLICY_COPY, new CopyEditPolicy());
		installEditPolicy(WFConstants.POLICY_CUT, new CutEditPolicy());
		installEditPolicy(WFConstants.POLICY_PASTE, new PasteEditPolicy());
		installEditPolicy(WFConstants.REQ_REMOVE_ILLEGAL_TRANSITIONS,
				new RemoveIllegalTransitionsEditPolicy());
	}

	@Override
	protected int getAnchorOffset() {
		return -1;
	}

	public Command getAutomaticLayoutCommand(EditPartViewer viewer, int flags) {
		AutomaticGraphLayout layout = new AutomaticGraphLayout(getModel(),viewer);
		layout.setFlags(flags);
		return layout.getApplyLayoutCommand();
	}

	public Command getAutomaticLayoutCommand(EditPartViewer viewer) {
		return getAutomaticLayoutCommand(viewer,IAutomaticGraphLayout.FLAG_DEFAULT);
	}

	/**
	 * Retrieves this structure's connection router for the given layer. It
	 * usually makes sense to share a connection router for all connections
	 * inside a structure because connections must be routed simultaneously in
	 * order to minimize intersections. Connections in different layers however
	 * might be routed independently.
	 * 
	 * @return
	 */
	public ConnectionRouter getConnectionRouter(Object layerID) {
		ConnectionRouter connectionRouter = connectionRouterMap.get(layerID);
		if (connectionRouter == null) {
			if (WFRootEditPart.DATA_FLOW_LAYER.equals(layerID)) {

				connectionRouter = new DataFlowConnectionRouter();
			} else {
				// fallback
				connectionRouter = new WFConnectionRouter(getFigure());
			}
			connectionRouterMap.put(layerID, connectionRouter);
		}
		return connectionRouter;
	}

	@Override
	public IFigure getContentPane() {
		// if (getFigure() instanceof StructuredActivityFigure)
		// return ((StructuredActivityFigure)getFigure()).getContents();
		return getFigure();
	}

	@Override
	public IFigure getFigure() {

		boolean hadNoFigure = figure == null;
		IFigure result = super.getFigure();
		if (hadNoFigure && figure.getLayoutManager() == null) {
			getContentPane().setLayoutManager(
					new StructuredActivityLayout());
		}
		return result;
	}

	/**
	 * padding between the border of the structured activity's figure and the
	 * child figures
	 * 
	 * @return
	 */
	public Insets getInnerPadding() {
		Insets result = getModel().getInnerPadding();
		if(result == null)
		{
			result = getModel().getDefaultInnerPadding();
		}
		if(result != null) return result;
		return WFConstants.INNER_PADDING;
	}


	@Override
	protected List getModelChildren() {
		return getModel().getChildren();
	}

	/**
	 * padding between the child figures
	 * 
	 * @return
	 */
	public Insets getPadding() {
		return WFConstants.BETWEEN_ACTIVITY_PADDING;
	}

	public StructuredActivity getModel() {
		return (StructuredActivity) super.getModel();
	}

	public void performAutomaticLayout() {

		new AutomaticGraphLayout(getModel(),getViewer()).layout(getFigure());

	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	@Override
	public void refreshVisuals() {
		try {
			super.refreshVisuals();
			IFigure fig = getContentPane();
			fig.getLayoutManager().layout(fig);
		} catch (Exception e) {
			WFActivator.log(IStatus.WARNING, "Could not refresh visuals of "
					+ toString() + "!", e);
		}

	}

	/**
	 * @see org.eclipse.gef.EditPart#setSelected(int)
	 */
	@Override
	public void setSelected(int value) {
		super.setSelected(value);
		if (getFigure() instanceof StructuredActivityFigure) {
			StructuredActivityFigure sf = (StructuredActivityFigure) getFigure();
			sf.setSelected(value != SELECTED_NONE);
		}

	}

	@Override
	protected void visualizeBreakpoint() {

	}

	@Override
	protected void visualizeState() {
		IFigure fig = getFigure();
		if (fig instanceof StructuredActivityFigure) {
			StructuredActivityFigure activityFigure = (StructuredActivityFigure) fig;
			switch (getModel().getCurrentExecutionStateCipher()) {
			case ExecutionStateConstants.STATE_RUNNING:
				activityFigure.setStateImage(WFActivator
						.getImageDescriptor("running.png"));
				break;
			case ExecutionStateConstants.STATE_FAILED:
				activityFigure.setStateImage(WFActivator
						.getImageDescriptor("failed.png"));
				break;
			case ExecutionStateConstants.STATE_SUCCESSFUL:
				activityFigure.setStateImage(WFActivator
						.getImageDescriptor("successful.png"));
				break;

			default:
				activityFigure.setStateImage(null);
				break;
			}

		}
	}

}
