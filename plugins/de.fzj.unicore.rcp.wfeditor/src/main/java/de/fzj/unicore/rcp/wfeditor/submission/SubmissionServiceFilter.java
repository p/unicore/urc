package de.fzj.unicore.rcp.wfeditor.submission;

import de.fzj.unicore.rcp.servicebrowser.filters.ContentFilter;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;

/**
 * Base class for service filters filtering out everything but suitable services
 * for workflow submission.
 * 
 * @author bdemuth
 * 
 */
public abstract class SubmissionServiceFilter extends ContentFilter {

	private WorkflowDiagram workflowDiagram;
	
	/**
	 * 
	 */
	public SubmissionServiceFilter(String _id) {
		super(_id);
	}

	public WorkflowDiagram getWorkflowDiagram() {
		return workflowDiagram;
	}

	public void setWorkflowDiagram(WorkflowDiagram workflowDiagram) {
		this.workflowDiagram = workflowDiagram;
	}
}
