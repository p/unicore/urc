package de.fzj.unicore.rcp.wfeditor.extensions;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;


/**
 * An object representing a free form string that is copied into the
 * workflow description literally.
 * 
 * @author bjoernh
 *
 */
public class ConditionTypeFreeForm implements IConditionType, PropertyChangeListener {

	private static final String TYPE_NAME = "Free form condition";
	private boolean disposed = false;
	private Condition condition;
	private String internalExpression;
	
	public ConditionTypeFreeForm() {
		internalExpression = "";
	}

	@Override
	public ConditionTypeFreeForm clone() throws CloneNotSupportedException {
		ConditionTypeFreeForm clone = (ConditionTypeFreeForm) super.clone();
		
		return clone;
	}
	
	@Override
	public void finalDispose(IProgressMonitor progress) {
		// do nothing

	}

	@Override
	public boolean isDisposable() {
		return true;
	}

	@Override
	public boolean isDisposed() {
		return disposed ;
	}

	@Override
	public void undoDispose() {
		disposed = false;
	}

	@Override
	public void dispose() {
		disposed = true;
	}

	@Override
	public Set<Class<?>> insertAfter() {
		return new HashSet<Class<?>>();
	}

	@Override
	public Set<Class<?>> insertBefore() {
		return new HashSet<Class<?>>();
	}

	@Override
	public void insertCopyIntoDiagram(ICopyable copy,
			StructuredActivity parent, Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		// TODO addPersistendPropertyChangeListener(this) to potential fields
	}

	@Override
	public Condition getCondition() {
		return condition;
	}

	@Override
	public IConditionType getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		ConditionTypeFreeForm result;
		try {
			result = (ConditionTypeFreeForm) super.clone();
		} catch (CloneNotSupportedException e) {
			WFActivator.log(IStatus.ERROR,
					"Unable to copy condition of type 'free form'", e);
			return null;
		}
		Condition newCondition = CloneUtils.getFromMapOrCopy(condition,
				toBeCopied, mapOfCopies, copyFlags);
		result.setCondition(newCondition);
		return result;
	}

	@Override
	public String getDisplayedExpression() {
		return internalExpression;
	}

	@Override
	public String getExtensionId() {
		return ConditionTypeFreeFormExtension.ID;
	}

	@Override
	public String getInternalExpression() throws Exception {
		return internalExpression;
	}

	@Override
	public String getTypeName() {
		return TYPE_NAME;
	}

	@Override
	public void setCondition(Condition condition) {
		this.condition = condition;

	}

	public void setInternalExpression(String internalExpression) {
		this.internalExpression = internalExpression;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		return;
	}

}
