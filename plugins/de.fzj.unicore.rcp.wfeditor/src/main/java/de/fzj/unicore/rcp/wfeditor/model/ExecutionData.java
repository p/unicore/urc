/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.core.runtime.IStatus;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateDescriptor;

/**
 * @author demuth
 * 
 */
@XmlRootElement
public class ExecutionData {

	/**
	 * The address of the Grid service that was used for submitting the
	 * workflow. Only set after the workflow is submitted.
	 */
	private String submissionServiceString;

	/**
	 * The address of the Grid service that is used for monitoring the execution
	 * of the workflow. Only set after the workflow is submitted.
	 */
	private String monitoringServiceString;

	/**
	 * The address of the Grid service that is used for tracing the execution of
	 * the workflow. Only set after the workflow is submitted.
	 */
	private String tracingServiceString;

	/**
	 * The unique ID that identifies the submitted instance of the workflow.
	 * This will usually be obtained from a server during workflow submission.
	 */
	private String submissionID;

	/**
	 * An OS-independent path to the workflow file belonging to this
	 * ExecutionData object
	 */
	private String workflowFilePath;

	private Map<String, ExecutionStateDescriptor> elementStates = new HashMap<String, ExecutionStateDescriptor>();

	private Map<String, String> elementIterationIds = new HashMap<String, String>();

	public Map<String, String> getElementIterationIds() {
		return elementIterationIds;
	}

	public Map<String, ExecutionStateDescriptor> getElementStates() {
		return elementStates;
	}

	public String getIterationId(String elementId) {
		String result = elementIterationIds.get(elementId);
		return result;
	}

	@XmlTransient
	public NodePath getMonitoringService() {
		if (getMonitoringServiceString() == null) {
			return null;
		}
		try {
			return NodePath.parseString(getMonitoringServiceString());
		} catch (Exception e) {
			try { // keep this for backwards compatibility with versions < 6.3.0
				EndpointReferenceType epr = EndpointReferenceType.Factory
						.parse(getMonitoringServiceString());
				URI uri = new URI(epr.getAddress().getStringValue());
				return new NodePath(uri);
			} catch (Exception e2) {
				WFActivator
						.log(IStatus.ERROR,
								"Problem while deserialising the service address for monitoring the workflow.",
								e);
				return null;
			}

		}

	}

	public String getMonitoringServiceString() {
		return monitoringServiceString;
	}

	public ExecutionStateDescriptor getStateDescriptor(String id) {
		ExecutionStateDescriptor result = elementStates.get(id);
		if (result == null) {
			ExecutionStateDescriptor sd = new ExecutionStateDescriptor();
			elementStates.put(id, sd);
			return sd;
		} else {
			return result;
		}
	}

	public String getSubmissionID() {
		return submissionID;
	}

	@XmlTransient
	public NodePath getSubmissionService() {
		if (getSubmissionServiceString() == null) {
			return null;
		}
		try {
			return NodePath.parseString(getSubmissionServiceString());
		} catch (Exception e) {
			try { // keep this for backwards compatibility with versions < 6.3.0
				EndpointReferenceType epr = EndpointReferenceType.Factory
						.parse(getSubmissionServiceString());
				URI uri = new URI(epr.getAddress().getStringValue());
				return new NodePath(uri);
			} catch (Exception e2) {
				WFActivator
						.log(IStatus.ERROR,
								"Problem while deserialising the address of the service to which this workflow was submitted.",
								e);
				return null;
			}

		}
	}

	public String getSubmissionServiceString() {
		return submissionServiceString;
	}

	@XmlTransient
	public NodePath getTracingService() {
		if (getTracingServiceString() == null) {
			return null;
		}
		try {
			return NodePath.parseString(getTracingServiceString());
		} catch (Exception e) {
			try { // keep this for backwards compatibility with versions < 6.3.0
				EndpointReferenceType epr = EndpointReferenceType.Factory
						.parse(getTracingServiceString());
				URI uri = new URI(epr.getAddress().getStringValue());
				return new NodePath(uri);
			} catch (Exception e2) {
				WFActivator
						.log(IStatus.ERROR,
								"Problem while deserialising the service address for tracing the workflow.",
								e);
				return null;
			}

		}

	}

	public String getTracingServiceString() {
		return tracingServiceString;
	}

	public String getWorkflowFilePath() {
		return workflowFilePath;
	}

	public void setElementIterationIds(Map<String, String> elementIterationIds) {
		this.elementIterationIds = elementIterationIds;
	}

	public void setElementStates(
			Map<String, ExecutionStateDescriptor> elementStates) {
		this.elementStates = elementStates;
	}

	public void setIterationId(String elementId, String iterationId) {
		elementIterationIds.put(elementId, iterationId);
	}

	public void setMonitoringService(NodePath monitoringService) {
		monitoringServiceString = monitoringService.toString();
	}

	public void setMonitoringServiceString(String monitoringServiceString) {
		this.monitoringServiceString = monitoringServiceString;
	}

	public void setStateDescriptor(String id, ExecutionStateDescriptor sd) {
		elementStates.put(id, sd);
	}

	public void setSubmissionID(String submissionID) {
		this.submissionID = submissionID;
	}

	public void setSubmissionService(NodePath submissionService) {
		submissionServiceString = submissionService.toString();
	}

	public void setSubmissionServiceString(String submissionServiceString) {
		this.submissionServiceString = submissionServiceString;
	}

	public void setTracingService(NodePath tracingService) {
		tracingServiceString = tracingService.toString();
	}

	public void setTracingServiceString(String tracingServiceString) {
		this.tracingServiceString = tracingServiceString;
	}

	public void setWorkflowFilePath(String workflowFilePath) {
		this.workflowFilePath = workflowFilePath;
	}

}
