/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.wfeditor.WFActivator;

/**
 * @author demuth
 */
public class ShowPropertyViewCommand extends Command {

	public ShowPropertyViewCommand() {

	}

	@Override
	public void execute() {
		try {
			PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.showView("org.eclipse.ui.views.PropertySheet", null,
							IWorkbenchPage.VIEW_ACTIVATE);
		} catch (PartInitException e) {
			WFActivator
					.log(IStatus.ERROR, "Could not open properties view.", e);
		}

	}
}