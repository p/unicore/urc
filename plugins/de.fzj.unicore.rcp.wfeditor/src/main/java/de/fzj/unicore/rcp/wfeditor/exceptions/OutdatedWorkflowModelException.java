package de.fzj.unicore.rcp.wfeditor.exceptions;

/**
 * This class is merely used for identifying the situation where an outdated
 * workflow has been found and converted to the current client version. It will
 * only be used for displaying a message to the user. There is no need to worry
 * about this type of exception anywhere in the code.
 * 
 * @author bdemuth
 * 
 */
public class OutdatedWorkflowModelException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6986970327941571624L;

}
