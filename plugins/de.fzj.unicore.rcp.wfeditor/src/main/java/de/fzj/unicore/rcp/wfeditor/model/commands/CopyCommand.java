/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.ui.actions.Clipboard;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

/**
 * @author demuth
 */
public class CopyCommand extends Command {

	private IFlowElement[] elements;
	private Object oldFromClipboard;
	private CompoundCommand childCommands;

	public CopyCommand(IFlowElement[] elements) {
		super();
		this.elements = elements;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {

		childCommands = new CompoundCommand();
		if (elements != null && elements.length > 0) {
			Set<IFlowElement> toBeCopied = new HashSet<IFlowElement>(
					Arrays.asList(elements));
			List<IFlowElement> l = new ArrayList<IFlowElement>(elements.length);
			for (IFlowElement e : elements) {
				if (e.isDeletable()
						&& !CloneUtils.isBeingCopied(e.getParent(), toBeCopied)) {
					l.add(e);
				}
			}
			elements = l.toArray(new IFlowElement[l.size()]);

			IFlowElement first = elements[0];
			for (IFlowElement e : first.getDiagram().getActivities().values()) {
				SetPropertyCommand setCutCommand = new SetPropertyCommand(e,
						IFlowElement.PROP_CUT, false);
				childCommands.add(setCutCommand);
			}
		}
		childCommands.execute();

		try {
			oldFromClipboard = Clipboard.getDefault().getContents();
		} catch (Exception e) {
			// ignore
		}
		Clipboard.getDefault().setContents(elements);
	}

	@Override
	public void redo() {
		try {
			oldFromClipboard = Clipboard.getDefault().getContents();
			childCommands.redo();
		} catch (Exception e) {
			// ignore
		}
		Clipboard.getDefault().setContents(elements);
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {

		if (oldFromClipboard != null) {
			Clipboard.getDefault().setContents(oldFromClipboard);
		} else {
			org.eclipse.swt.dnd.Clipboard cb = new org.eclipse.swt.dnd.Clipboard(
					null);
			cb.clearContents();
			cb.dispose();
		}
		childCommands.undo();
	}

}
