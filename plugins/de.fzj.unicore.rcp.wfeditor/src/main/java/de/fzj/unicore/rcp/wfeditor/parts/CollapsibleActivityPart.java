/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LayoutManager;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.draw2d.graph.Subgraph;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.swt.graphics.Image;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.borders.DrawerBorder;
import de.fzj.unicore.rcp.wfeditor.figures.CollapsibleFigure;
import de.fzj.unicore.rcp.wfeditor.figures.IActivityFigure;
import de.fzj.unicore.rcp.wfeditor.figures.ICollapsibleFigure;
import de.fzj.unicore.rcp.wfeditor.layouts.WFDecorationLayout;
import de.fzj.unicore.rcp.wfeditor.model.commands.AdjustLayoutCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.MoveActivityCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.ReverseCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.SetCollapsedCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.SetConstraintCommand;
import de.fzj.unicore.rcp.wfeditor.model.structures.CollapsibleActivity;
import de.fzj.unicore.rcp.wfeditor.utils.WFDragEditPartsTracker;

/**
 * Subclasses of CollapsableEditPart represent BPEL activities which can be
 * graphically represented in an expanded or collapsed manner.
 * 
 * In the collapsed state, this class will render the node. In the expanded
 * state, the subclass will be expected to render the node.
 */
public abstract class CollapsibleActivityPart extends StructuredActivityPart {

	public class CollapsableDecorationLayout extends WFDecorationLayout {
		private int borderImageWidth;

		public CollapsableDecorationLayout(int borderImageWidth) {
			this.borderImageWidth = borderImageWidth;
		}

		@Override
		protected Point calculateLocation(int locationHint, IFigure container,
				Dimension childDimension) {
			Rectangle area = container.getClientArea();
			switch (locationHint) {
			case PositionConstants.CENTER:
				// Center
				return new Point(area.x + area.width / 2 - childDimension.width
						/ 2, area.y + area.height / 2 - childDimension.height
						/ 2);
			case PositionConstants.TOP:
				// Top Center
				return new Point(area.x + area.width / 2 - childDimension.width
						/ 2, area.y + borderImageWidth / 2);
			case PositionConstants.BOTTOM:
				// Bottom Center
				return new Point(area.x + area.width / 2 - childDimension.width
						/ 2, area.y + area.height - childDimension.height);
			case PositionConstants.LEFT: {
				// Center Left
				int x = area.x + DrawerBorder.DRAWER_WIDTH;
				int y = area.y + (area.height / 2) - (childDimension.width / 2);
				if (!isCollapsed()) {
					y += DrawerBorder.DRAWER_HALF_HEIGHT / 2;
				}
				return new Point(x, y);
			}
			case PositionConstants.RIGHT: {
				// Center Right
				int x = area.x + area.width - childDimension.width
				- DrawerBorder.DRAWER_WIDTH;
				int y = area.y + (area.height / 2) - (childDimension.width / 2);
				if (!isCollapsed()) {
					y += DrawerBorder.DRAWER_HALF_HEIGHT / 2;
				}
				return new Point(x, y);
			}
			case PositionConstants.TOP | PositionConstants.LEFT: {
				// Top Left
				int x = area.x + DrawerBorder.DRAWER_WIDTH;
				int y = area.y + (borderImageWidth / 2);
				if (!isCollapsed()) {
					y += (collapsedLabel.getSize().height / 2)
					- (WFConstants.ARC_WIDTH / 2);
				}
				return new Point(x, y);
			}
			case PositionConstants.TOP | PositionConstants.RIGHT: {
				// Top Right
				int x = area.x + area.width - childDimension.width
				- DrawerBorder.DRAWER_WIDTH;
				int y = area.y + (borderImageWidth / 2);
				if (!isCollapsed()) {
					y += (collapsedLabel.getSize().height / 2)
					- (WFConstants.ARC_WIDTH / 2);
				} else {
					x -= 1; // 1 is a magic number
				}
				return new Point(x, y);
			}
			case PositionConstants.BOTTOM | PositionConstants.LEFT: {
				// Bottom Left
				int x = area.x + DrawerBorder.DRAWER_WIDTH;
				int y = area.y + area.height - WFConstants.ARC_WIDTH;
				if (!isCollapsed()) {
					y -= DrawerBorder.DRAWER_HALF_HEIGHT / 2;
				} else {
					y -= DrawerBorder.DRAWER_HALF_HEIGHT;
				}
				return new Point(x, y);
			}
			case PositionConstants.BOTTOM | PositionConstants.RIGHT: {
				// Bottom Right
				int x = area.x + area.width - childDimension.width
				- DrawerBorder.DRAWER_WIDTH;
				int y = area.y + area.height - WFConstants.ARC_WIDTH;
				if (!isCollapsed()) {
					y -= DrawerBorder.DRAWER_HALF_HEIGHT / 2;
				} else {
					y -= DrawerBorder.DRAWER_HALF_HEIGHT;
					x -= 1; // 1 is a magic number
				}
				return new Point(x, y);
			}
			default:
				return new Point(area.x, area.y);
			}
		}
	}


	// The primary image for the edit part
	protected Image image;

	// The images in the top and bottom drawer, if any
	protected Image topImage, bottomImage;

	// The label to display when collapsed. Subclasses may also use it
	// when expanded if desired.
	protected Label collapsedLabel;

	protected LayoutManager expandedLayout;

	private Command lastCollapseCommand = null;

	public CollapsibleActivityPart() {

	}

	@Override
	public void activate() {
		super.activate();
	}

	@Override
	protected void addChild(EditPart child, int index) {
		super.addChild(child, index);
		// CompoundCommand result = new CompoundCommand();
		// Command resizeParentCommand = getCommand(new
		// ResizeContainerRequest());
		// if(resizeParentCommand != null) result.add(resizeParentCommand);
		// Command autoLayoutCommand = getAutomaticLayoutCommand();
		// if(autoLayoutCommand != null) result.add(autoLayoutCommand);
		// getActivity().getCommandStack().execute(result);

	}

	// subclasses can override this to change edit policies, etc.
	protected void collapseNotify(boolean collapsed) {
	}

	protected void configureCollapsedFigure(ICollapsibleFigure collapsible) {

		IFigure contents = collapsible;
		// save expanded layout for restoring it later
		this.expandedLayout = contents.getLayoutManager();

		FlowLayout layout = new FlowLayout();
		layout.setMajorAlignment(FlowLayout.ALIGN_CENTER);
		contents.setLayoutManager(layout);
		IFigure collapsedFigure = getCollapsedFigure();
		Dimension d = new Dimension(WFConstants.DEFAULT_SIMPLE_ACTIVITY_SIZE.width,WFConstants.DEFAULT_SIMPLE_ACTIVITY_SIZE.height);
		collapsedFigure.setBounds(new Rectangle(new Point(0,0),d));
		contents.add(collapsedFigure);

	}

	// protected DrawerBorder getDrawerBorder() {
	// Border border = getContentPane().getBorder();
	// if (border instanceof DrawerBorder) return (DrawerBorder)border;
	// return null;
	// }

	/**
	 * Configure the given figure for expanded mode. Subclasses should do work
	 * in here to set border and layout information for expanded mode.
	 */
	protected abstract void configureExpandedFigure(ICollapsibleFigure figure);



	@Override
	public void applyLayout(CompoundDirectedGraph graph, Map map,
			CompoundCommand cmd) {
		if(isCollapsed())
		{
			Node n = (Node) map.get(this);
			SetConstraintCommand myCmd = new SetConstraintCommand();
			myCmd.setPart(getModel());
			myCmd.setLocation(getModel().getLocation());
			myCmd.setSize(new Dimension(n.width, n.height));
			cmd.add(myCmd);

			// instead of setting the location and recursively calling applyLayout
			// on the children, we need to move the activity in order to move
			// the children as well
			MoveActivityCommand move = new MoveActivityCommand(getModel(), new Point(n.x,n.y));
			cmd.add(move);

			for (int i = 0; i < getSourceConnections().size(); i++) {
				TransitionPart trans = (TransitionPart) getSourceConnections().get(
						i);
				trans.applyGraphResults(graph, map, cmd);
			}
		}
		else super.applyLayout(graph, map, cmd);
	}

	@Override
	public void contributeNodesToGraph(CompoundDirectedGraph graph, Subgraph s,
			Map map) {
		if(isCollapsed())
		{
			Node n = new Node(this, s);
			n.outgoingOffset = getAnchorOffset();
			n.incomingOffset = getAnchorOffset();
			IFigure figure = getFigure();

			Rectangle bounds = figure.getBounds().getCopy();

			// account for the difference between where the figure is and where it
			// should be
			Rectangle r = new Rectangle(getModel().getLocation(),getModel().getSize());
			Dimension diff = r.getLocation().getDifference(bounds.getLocation());			
			n.x = r.x;
			n.y = r.y;
			n.width = r.width;
			n.height = r.height;

			map.put(this, n);
			graph.nodes.add(n);
		}
		else super.contributeNodesToGraph(graph, s, map);

	}

	@Override
	protected abstract ICollapsibleFigure createFigure();

	protected abstract IFigure getCollapsedFigure();

	public Label getCollapsedLabel() {
		return collapsedLabel;
	}

	/**
	 * Convenience method
	 * 
	 * @return
	 */
	public CollapsibleActivity getModel() {
		return (CollapsibleActivity) super.getModel();
	}

	@Override
	public DragTracker getDragTracker(Request request) {
		return new WFDragEditPartsTracker(this) {
			@Override
			protected boolean handleButtonDown(int button) {
				// TODO: do this via a request?
				if (isPointInCollapseIcon(getLocation())) {
					setCollapsed(!isCollapsed());
					return true;
				}

				return super.handleButtonDown(button);
			}

			@Override
			protected boolean handleDoubleClick(int button) {
				return super.handleDoubleClick(button);
			}
		};
	}

	/**
	 * Return a list of the model children that should be displayed while the
	 * node is in expanded mode. Subclasses may override.
	 */
	protected List<?> getExpandedChildren() {
		return super.getModelChildren();
	}

	@Override
	public ICollapsibleFigure getFigure() {
		boolean hadNoFigure = figure == null;
		ICollapsibleFigure result = (ICollapsibleFigure) super.getFigure();
		if (hadNoFigure) {
			result.setCollapsed(isCollapsed());
			initializeLabels();
			updateFigure();
		}
		return result;
	}

	/**
	 * Return the image that was created for the receiver. This image
	 * corresponds to the image descriptor which the subclass provided in
	 * getImageDescriptor().
	 * 
	 * This image may be used by subclasses for the lifetime of this edit part.
	 * TODO: Get rid of this. Subclasses should handle the lifecycle.
	 */
	protected Image getImage() {
		return image;
	}

	/**
	 * Return an image which should be displayed in this node while collapsed.
	 * Subclasses may override.
	 */
	protected Image getImg() {

		return null;
	}

	/**
	 * Return a string which should be displayed in this node while collapsed.
	 */
	protected String getLabel() {
		return "bla";
	}

	public Label getLabelFigure() {
		return collapsedLabel;
	}

	@Override
	protected List<?> getModelChildren() {
		if (isCollapsed()) {
			return Collections.EMPTY_LIST;
		}

		return getExpandedChildren();
	}

	/**
	 * A CollapsableEditPart has extra responsibilities for source and target
	 * connections, because it must show flow links for any activities which are
	 * inside it but not visible (in the case that the CollapsableEditPart is
	 * collapsed).
	 */
	@Override
	protected List<?> getModelSourceConnections() {
		final List<?> result = super.getModelSourceConnections();

		return result;
	}

	/**
	 * A CollapsibleActivityPart has extra responsibilities for source and
	 * target connections, because it must show flow links for any activities
	 * which are inside it but not visible (in the case that the
	 * CollapsableEditPart is collapsed).
	 */
	@Override
	protected List<?> getModelTargetConnections() {
		final List<?> result = super.getModelTargetConnections();
		// if (isCollapsed()) {
		// BPELUtil.visitModelDepthFirst(getActivity(), new IModelVisitor() {
		// public boolean visit(Object modelObject) {
		// if (modelObject instanceof Activity) {
		// List targets = ((Activity)modelObject).getTargets();
		// Iterator it = targets.iterator();
		// while (it.hasNext()) {
		// Target target = (Target)it.next();
		// Source source = target.getLink().getSource();
		// Activity sourceActivity = source.getActivity();
		// // If the source is not contained in this model object, add the
		// // connection.
		// if (!containsModel(sourceActivity)) {
		// result.add(target.getLink());
		// }
		// }
		// }
		// return true;
		// }
		// });
		// }
		return result;
	}

	/**
	 * @see NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(
			ConnectionEditPart connection) {
		if ((getModel().getDirection() & PositionConstants.SOUTH) != 0) {
			return new TopAnchor(getFigure(), -1);
		} else {
			return new ChopboxAnchor(getFigure());
		}
	}

	/**
	 * @see org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.Request)
	 */
	@Override
	public ConnectionAnchor getTargetConnectionAnchor(Request request) {
		if ((getModel().getDirection() & PositionConstants.SOUTH) != 0) {
			return new TopAnchor(getFigure(), -1);
		} else {
			return new ChopboxAnchor(getFigure());
		}
	}

	protected void initializeLabels() {
		this.collapsedLabel = new Label(getLabel());

		// Get Image from registry
		this.image = getImg();
	}

	protected boolean isCollapsable() {
		return true;
	}

	public boolean isCollapsed() {
		return getModel().isCollapsed();
	}



	protected boolean isPointInCollapseIcon(Point point) {
		return getFigure().isPointInCollapseImage(point.x, point.y);
	}

	/**
	 * Recompute the anchors for incoming and outgoing connections. Called after
	 * collapsing/expanding.
	 */
	protected void refreshConnectionVisuals() {
		refreshSourceConnectionVisuals();
		refreshTargetConnectionVisuals();
	}

	protected void refreshDrawerImages() {

		if (topImage != null) {
			topImage.dispose();
			topImage = null;
		}
		if (bottomImage != null) {
			bottomImage.dispose();
			bottomImage = null;
		}
		// IMarkerHolder holder = BPELUtil.adapt(getActivity(),
		// IMarkerHolder.class);

		// int topMarkerPriority = Integer.MIN_VALUE;
		// int bottomMarkerPriority = Integer.MIN_VALUE;
		// IMarker topMarker = null;
		// IMarker bottomMarker = null;

		// for (IMarker marker : holder.getMarkers(getActivity())) {

		// if
		// (marker.getAttribute(IModelMarkerConstants.DECORATION_MARKER_VISIBLE_ATTR,
		// true) == false) {
		// continue;
		// }

		// String value =
		// marker.getAttribute(IModelMarkerConstants.DECORATION_GRAPHICAL_MARKER_ANCHOR_POINT_ATTR,EMPTY_STRING);

		// if (value.equals(WFConstants.MARKER_ANCHORPOINT_DRAWER_TOP)) {
		// int priority =
		// marker.getAttribute(IModelMarkerConstants.DECORATION_MARKER_PRIORITY_ATTR,
		// 0);
		// if (priority > topMarkerPriority) {
		// topMarkerPriority = priority;
		// topImage = BPELUtil.getImage(marker);
		// topMarker = marker;
		// }
		// } else if
		// (value.equals(WFConstants.MARKER_ANCHORPOINT_DRAWER_BOTTOM)) {
		// int priority =
		// marker.getAttribute(IModelMarkerConstants.DECORATION_MARKER_PRIORITY_ATTR,
		// 0);
		// if (priority > bottomMarkerPriority) {
		// bottomMarkerPriority = priority;
		// bottomImage = BPELUtil.getImage(marker);
		// bottomMarker = marker;
		// }
		// }
		// }

		// border.setTopImage(topImage);
		// border.setBottomImage(bottomImage);
		// border.setTopMarker(topMarker);
		// border.setBottomMarker(bottomMarker);
	}

	protected void refreshSourceConnectionVisuals() {
		List<?> editParts = getSourceConnections();
		for (int i = 0; i < editParts.size(); i++) {
			ConnectionEditPart editPart = (ConnectionEditPart) editParts.get(i);
			editPart.refresh();
		}
	}

	protected void refreshTargetConnectionVisuals() {
		List<?> editParts = getTargetConnections();
		for (int i = 0; i < editParts.size(); i++) {
			ConnectionEditPart editPart = (ConnectionEditPart) editParts.get(i);
			editPart.refresh();
		}
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	@Override
	public void refreshVisuals() {
		super.refreshVisuals();

		if(isCollapsed() != getFigure().isCollapsed())
		{
			updateFigure();
		}
	}


	protected void updateBounds()
	{
		if(isCollapsed()) 
		{
			Rectangle r = getModel().getCollapsedBounds();
			GraphicalEditPart parent = (GraphicalEditPart) getParent();
			LayoutManager layouter = parent.getFigure().getLayoutManager();
			if(layouter != null)
			{
				Object o = layouter.getConstraint(getFigure());
				if(!r.equals(o))
				{
					parent.setLayoutConstraint(this,
							getFigure(), r);
					layouter.layout(parent.getFigure());
				}
			}
		}
		else{
			super.updateBounds();
		}

	}

	protected Rectangle computeCollapsedBounds()
	{
		int x = WFConstants.DEFAULT_SIMPLE_ACTIVITY_SIZE.width + 2;
		int y = WFConstants.DEFAULT_SIMPLE_ACTIVITY_SIZE.height + 2;

		IFigure subgraph = getFigure();
		// allow for adjusting collapsed image figure vertically
		y += subgraph.getInsets().top;
		Rectangle r = new Rectangle(0, 0, x, y);

		if(getFigure().getBorder() != null)
		{
			Dimension d = getFigure().getBorder().getPreferredSize(getFigure());
			int xDiff = Math.max(0,d.width - r.width);
			int yDiff = Math.max(0,d.height - r.height);
			r.x -= xDiff/2;
			r.width+=xDiff;
			r.y -= yDiff/2;
			r.height+= yDiff;
		}

		Rectangle expandedRect = new Rectangle(getModel()
				.getLocation(), getModel().getSize());
		Dimension diff = expandedRect.getTop().getDifference(
				r.getTop());
		r.translate(diff.width, diff.height);


		return r;
	}

	/**
	 * This should only be called as reaction to user input as it causes
	 * re-layouting of the collapsible subgraph.
	 * 
	 * @param collapsed
	 */
	public void setCollapsed(boolean collapsed) {
		if (!isCollapsable()) {
			return;
		}

		if (isCollapsed() == collapsed) {
			return;
		}
		CommandStack stack = getModel().getCommandStack();
		Object[] commands = stack.getCommands();
		if(commands.length > 0 && commands[commands.length-1] == lastCollapseCommand && lastCollapseCommand.canUndo())
		{
			stack.execute(new ReverseCommand(lastCollapseCommand));
			lastCollapseCommand = null;
			return;
		}


		// set bounds correctly
		Rectangle collapsedRect;
		if (collapsed) {
			Rectangle expandedRect = new Rectangle(getModel()
					.getLocation(), getModel().getSize());

			collapsedRect = computeCollapsedBounds();

			// save expanded bounds for restoring them later
			getModel().setExpandedBounds(expandedRect);

			getModel().setCollapsedBounds(collapsedRect);

		}

		SetCollapsedCommand collapse = new SetCollapsedCommand(getModel(), collapsed);
		AdjustLayoutCommand adjust = new AdjustLayoutCommand(getModel(), getViewer());
		lastCollapseCommand = collapse.chain(adjust);
		getModel().getCommandStack().execute(lastCollapseCommand);
	}

	@Override
	protected void unregisterVisuals() {
		this.image = null;
		// this.editPartMarkerDecorator = null;

		this.topImage = null;
		this.bottomImage = null;
		super.unregisterVisuals();
	}

	@SuppressWarnings("unchecked")
	protected void updateFigure() {

		ICollapsibleFigure collapsible = (CollapsibleFigure) getFigure();
		collapsible.setCollapsed(isCollapsed());
		IFigure contents = collapsible;

		if (isCollapsed()) {

			// First refresh children, which removes all model children's
			// figures
			refreshChildren();

			// Manually remove the rest of the children
			IFigure[] children = (IFigure[]) contents.getChildren().toArray(
					new IFigure[0]);
			for (int i = 0; i < children.length; i++) {
				contents.remove(children[i]);
			}

			// Now restore the collapsed children, border and layout
			configureCollapsedFigure(collapsible);
		} else {

			// Manually remove the children
			IFigure[] children = (IFigure[]) contents.getChildren().toArray(
					new IFigure[0]);
			for (int i = 0; i < children.length; i++) {
				contents.remove(children[i]);
			}
			configureExpandedFigure(collapsible);
			// Now restore the expanded children, border and layout
			if (expandedLayout != null) {
				contents.setLayoutManager(expandedLayout);
			}

			refreshChildren();
		}

		refreshSourceConnections();
		refreshTargetConnections();
		refreshConnectionVisuals();

		// Switching collapsed states may have changed the border, which is
		// responsible for drawing the drawer markers. Refresh these markers
		// now.
		refreshDrawerImages();
		// Force a repaint, as the drawer images may have changed.
		getFigure().repaint();
	}

	@Override
	protected void updateLabelInFigure(String label) {
		if (getCollapsedFigure() instanceof IActivityFigure) {
			((IActivityFigure) getCollapsedFigure()).setLabel(label);
			getCollapsedFigure().repaint();
		}
		super.updateLabelInFigure(label);
	}
}
