package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.parts.ActivityPart;

public class DataSinkListFigure extends Figure {

	private ActivityPart part;

	public DataSinkListFigure(ActivityPart part) {
		this.part = part;

		setOpaque(true);
		setBackgroundColor(ColorConstants.white);

	}

	@Override
	public boolean containsPoint(int x, int y) {
		return getBounds().contains(x, y);
	}

	public IActivity getActivity() {
		return part.getModel();
	}

	@Override
	public void paint(Graphics graphics) {
		super.paint(graphics);
	}

	// public boolean refreshTooltip(IFigure hoverSource, IFigure tooltipLayer,
	// int eventX, int eventY) {
	//
	// Point location = getActivity().getLocation();
	// Dimension size = getActivity().getSize();
	// int x1 = location.x;
	// int x2 = location.x+size.width;
	// int y1 = location.y;
	// int y2 = location.y+size.height;
	// for(Object child : getChildren())
	// {
	// IFigure fig = (IFigure) child;
	// Object o = getLayoutManager().getConstraint(fig);
	// if(o instanceof Rectangle)
	// {
	// Rectangle r = (Rectangle) o;
	// if(r.x < x1) x1 = r.x;
	// if(r.x+r.width > x2) x2 = r.x+r.width;
	//
	// if(r.y < y1) y1 = r.y;
	// if(r.y+r.height > y2) y2 = r.y+r.height;
	// }
	// }
	//
	// Rectangle bounds = new Rectangle(x1,y1,x2-x1,y2-y1);
	// bounds.expand(15, 15);
	// if(hoverSource.equals(part.getFigure()) &&
	// !bounds.contains(eventX,eventY))
	// {
	// return false;
	// }
	//
	//
	// Point p = new Point(eventX, eventY);
	// IDataSource[] inputData = getInputData();
	//
	// tooltipLayer.setConstraint(this, bounds);
	// return true;
	// }

}
