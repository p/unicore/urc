package de.fzj.unicore.rcp.wfeditor.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;

/**
 * This class represents a UNICORE Workflow preference page that is contributed
 * to the UNICORE Preferences dialog.
 * 
 * @author Valentina Huber
 * @version $Id: GridBrowserPreferencePage.java,v 1.1 2007/10/02 09:01:10 vhuber
 *          Exp $
 */
public class WFPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public WFPreferencePage() {
		super(GRID);
		setPreferenceStore(WFActivator.getDefault().getPreferenceStore());
		setDescription("Preferences for UNICORE workflows.");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

//		IntegerFieldEditor terminationTime = new IntegerFieldEditor(
//				WFConstants.P_WORKFLOW_TERMINATION_TIME,
//				"&Default lifetime for workflows (hours):",
//				getFieldEditorParent());
//		terminationTime.setValidRange(1, Integer.MAX_VALUE);
//		addField(terminationTime);

		IntegerFieldEditor pollingTime = new IntegerFieldEditor(
				WFConstants.P_WORKFLOW_POLLING_INTERVAL,
				"Time between polling workflow status (seconds):",
				getFieldEditorParent());
		pollingTime.setValidRange(1, Integer.MAX_VALUE);
		addField(pollingTime);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}

}