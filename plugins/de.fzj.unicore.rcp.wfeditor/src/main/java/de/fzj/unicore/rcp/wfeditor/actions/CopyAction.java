package de.fzj.unicore.rcp.wfeditor.actions;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.requests.GroupRequest;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;

import de.fzj.unicore.rcp.wfeditor.WFConstants;

public class CopyAction extends WFEditorAction {

	public CopyAction(IWorkbenchPart part) {
		super(part);

	}

	public CopyAction(IWorkbenchPart part, int style) {
		super(part, style);

	}

	@Override
	protected boolean calculateEnabled() {
		Command cmd = createCopyCommand(getSelectedObjects());
		if (cmd == null) {
			return false;
		}
		return cmd.canExecute();
	}

	public Command createCopyCommand(List<?> objects) {
		if (objects.isEmpty()) {
			return null;
		}
		if (!(objects.get(0) instanceof EditPart)) {
			return null;
		}

		GroupRequest req = new GroupRequest(WFConstants.REQ_COPY);
		req.setEditParts(objects);

		CompoundCommand compoundCmd = new CompoundCommand();
		for (int i = 0; i < objects.size(); i++) {
			EditPart object = (EditPart) objects.get(i);
			Command cmd = object.getCommand(req);
			if (cmd != null) {
				compoundCmd.add(cmd);
			}
		}

		return compoundCmd;
	}

	@Override
	protected void init() {
		setId(ActionFactory.COPY.getId());
		setText("Copy");
		setToolTipText("Copy selected elements to clipboard");
		ISharedImages sharedImages = PlatformUI.getWorkbench()
				.getSharedImages();
		setImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_COPY));
		setDisabledImageDescriptor(sharedImages
				.getImageDescriptor(ISharedImages.IMG_TOOL_COPY_DISABLED));
		setEnabled(false);
	}

	/**
	 * Performs the copy to clipboard action on the selected objects.
	 */
	@Override
	public void run() {
		Command cmd = createCopyCommand(getSelectedObjects());
		// don't execute this via the command stack..
		// we don't want to get the editor dirty and undoing this is rather
		// pointless
		if (cmd.canExecute()) {
			cmd.execute();
		}
	}

}
