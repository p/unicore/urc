package de.fzj.unicore.rcp.wfeditor.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;

import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.submission.ISubmitter;
import de.fzj.unicore.rcp.wfeditor.submission.SubmitterRegistry;

public class WFExportRunnable implements IRunnableWithProgress {
	private Node selectedNode;
	private ISubmitter selectedSubmitter;
	private WorkflowDiagram workflowDiagram;
	private boolean uploadingFiles;

	protected Node getSelectedNode() {
		return selectedNode;
	}

	protected ISubmitter getSelectedSubmitter() {
		return selectedSubmitter;
	}

	protected WorkflowDiagram getWorkflowDiagram() {
		return workflowDiagram;
	}

	protected boolean isUploadingFiles() {
		return uploadingFiles;
	}

	public void run(IProgressMonitor progress)
			throws InvocationTargetException, InterruptedException {
		if (!progress.isCanceled()) {

			try {
				final SubmitterRegistry submitterRegistry = WFActivator
						.getDefault().getSubmitterRegistry();
				if (!submitterRegistry.canDealWith(getWorkflowDiagram())) {
					progress.setCanceled(true);
					return;
				}
				final Node node = getSelectedNode();
				final ISubmitter submitter = getSelectedSubmitter();
				if (node == null || submitter == null) {
					progress.setCanceled(true);
					return;
				}

				XmlObject workflow = null;

				progress.beginTask("exporting workflow", 100);
				// editor.doSave(new SubProgressMonitor(progress,30));
				workflow = submitter.createWorkflowDescription(node,
						getWorkflowDiagram(), isUploadingFiles(),
						new SubProgressMonitor(progress, 70));
				saveWorkflow(workflow);

			} catch (Exception e) {
				WFActivator
						.log(IStatus.WARNING, "Could not export workflow", e);

			} finally {
				progress.done();
			}
		}
	}

	protected void saveWorkflow(final XmlObject workflow) {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {

			public void run() {

				if (workflow != null) {
					Shell shell = PlatformUI.getWorkbench().getDisplay()
							.getActiveShell();
					if (shell == null) {
						shell = new Shell(PlatformUI.getWorkbench()
								.getDisplay());
					}
					FileDialog dialog = new FileDialog(shell, SWT.SAVE);
					dialog.setText("Choose a file for saving the exported workflow");
					final String fileName = dialog.open();
					Job job = new BackgroundJob("saving exported workflow to file") {

						@Override
						protected IStatus run(IProgressMonitor monitor) {
							try {
								saveWorkflow(workflow, fileName);
							} catch (IOException e) {
								WFActivator.log(IStatus.ERROR,
										"Unable to export workflow, could not write to file: "
												+ fileName, e);
							}
							return Status.OK_STATUS;
						}
					};
					job.schedule();
				}

			}
		});
	}

	protected void saveWorkflow(XmlObject workflow, String fileName)
			throws FileNotFoundException, IOException {
		if (fileName == null) {
			return;
		}
		XmlOptions saveOptions = new XmlOptions();
		saveOptions.setSavePrettyPrint();
		saveOptions.setSavePrettyPrintIndent(4);
		OutputStream os = new FileOutputStream(new File(fileName));
		try {
			workflow.save(os, saveOptions);
		} finally {
			os.close();
		}
		
	}

	protected void setSelectedNode(Node selectedNode) {
		this.selectedNode = selectedNode;
	}

	protected void setSelectedSubmitter(ISubmitter selectedSubmitter) {
		this.selectedSubmitter = selectedSubmitter;
	}

	protected void setUploadingFiles(boolean uploadFiles) {
		this.uploadingFiles = uploadFiles;
	}

	protected void setWorkflowDiagram(WorkflowDiagram workflowDiagram) {
		this.workflowDiagram = workflowDiagram;
	}

}
