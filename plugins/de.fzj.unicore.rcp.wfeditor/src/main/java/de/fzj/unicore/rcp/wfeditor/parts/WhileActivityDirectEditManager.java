/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import java.util.List;

import org.eclipse.draw2d.Label;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gef.tools.DirectEditManager;

import de.fzj.unicore.rcp.common.guicomponents.StringComboBoxCellEditor;

/**
 * DirectEditManager for Activities
 * 
 * @author Daniel Lee
 */
@SuppressWarnings({"rawtypes"})
public class WhileActivityDirectEditManager extends DirectEditManager {

	protected Label activityLabel;
	protected List<String> iterations;
	protected String currentIteration;

	/**
	 * Creates a new ActivityDirectEditManager with the given attributes.
	 * 
	 * @param source
	 *            the source EditPart
	 * @param editorType
	 *            type of editor
	 * @param locator
	 *            the CellEditorLocator
	 */
	public WhileActivityDirectEditManager(GraphicalEditPart source,
			Class editorType, CellEditorLocator locator, Label label,
			List<String> iterations, String currentIteration) {
		super(source, editorType, locator);
		activityLabel = label;
		this.iterations = iterations;
		this.currentIteration = currentIteration;
	}

	/**
	 * @see org.eclipse.gef.tools.DirectEditManager#initCellEditor()
	 */
	@Override
	protected void initCellEditor() {

		StringComboBoxCellEditor editor = (StringComboBoxCellEditor) getCellEditor();
		editor.setItems(iterations.toArray(new String[0]));
		editor.setValue(currentIteration);
		String initialLabelText = activityLabel.getText();
		editor.setValue(initialLabelText);

	}

}
