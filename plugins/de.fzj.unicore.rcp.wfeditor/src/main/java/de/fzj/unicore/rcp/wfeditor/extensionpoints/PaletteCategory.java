package de.fzj.unicore.rcp.wfeditor.extensionpoints;

public class PaletteCategory implements Comparable<PaletteCategory> {
	/**
	 * Name of the category as displayed in the palette.
	 */
	private String name;
	/**
	 * Value for sorting the categories in the palette.
	 */
	private Double index;

	public PaletteCategory(String name, Double index) {
		this.name = name;
		this.index = index;
	}

	public int compareTo(PaletteCategory o) {
		return getIndex().compareTo(o.getIndex());
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof PaletteCategory)) {
			return false;
		}
		return ((PaletteCategory) o).name.equals(name);
	}

	protected Double getIndex() {
		return index;
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

}
