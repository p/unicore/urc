package de.fzj.unicore.rcp.wfeditor.parts;

import org.eclipse.draw2d.IFigure;

import de.fzj.unicore.rcp.wfeditor.figures.ICollapsibleFigure;

public abstract class SimpleCollapsibleActivityPart extends
		CollapsibleActivityPart {

	@Override
	protected void configureExpandedFigure(ICollapsibleFigure figure) {

	}

	@Override
	protected abstract IFigure getCollapsedFigure();

	
}
