/*******************************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.commands.UnexecutableCommand;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.tools.DragEditPartsTracker;
import org.eclipse.jface.viewers.StructuredSelection;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.parts.TransitionPart;

/**
 * Overridden DragEditPartsTracker to provide more intelligent behavior when
 * dragging children that don't all have the same parent.
 */
public class WFDragEditPartsTracker extends DragEditPartsTracker {

	protected List<IFigure> exclusionSet;

	public WFDragEditPartsTracker(EditPart sourceEditPart) {
		super(sourceEditPart);
	}

	@Override
	public void deactivate() {
		super.deactivate();
		exclusionSet = null;
	}

	@Override
	protected void eraseSourceFeedback() {
		super.eraseSourceFeedback();
	}

	@Override
	protected Command getCommand() {

		CompoundCommand moveCommands = new CompoundCommand();
		CompoundCommand orphanCommands = new CompoundCommand();
		CompoundCommand addCommands = new CompoundCommand();

		Iterator<?> iter = getOperationSet().iterator();
		while (iter.hasNext()) {
			EditPart editPart = (EditPart) iter.next();
			Request request = getTargetRequest();
			Object originalType = request.getType();
			boolean isTransition = (editPart.getModel() instanceof Transition);

			// make sure the request only has this particular editpart.
			((ChangeBoundsRequest) request).setEditParts(editPart);

			boolean targetIsChildOfSource = false;
			EditPart parent = getTargetEditPart().getParent();
			while (parent != null) {
				if (parent == editPart) {
					targetIsChildOfSource = true;
					break;
				}
				parent = parent.getParent();
			}

			boolean boundToParent = false;
			if (editPart.getModel() instanceof IFlowElement) {
				IFlowElement model = (IFlowElement) editPart.getModel();
				boundToParent = model.isBoundToParent();
			}

			boolean isMove = targetIsChildOfSource || boundToParent
					|| editPart.getParent() == getTargetEditPart()
					|| editPart == getTargetEditPart() || isTransition;

			if (isMove) {

				// if the transition's parent is selected as well, it will take
				// care of the move
				if (isTransition) {
					TransitionPart tp = (TransitionPart) editPart;
					if (getOperationSet().contains(tp.getTarget().getParent())) {
						continue;
					}
				}

				request.setType(REQ_MOVE);
				moveCommands.add(editPart.getCommand(request));
			} else {
				if (getTargetEditPart() == null) {
					return UnexecutableCommand.INSTANCE;
				}
				request.setType(REQ_ORPHAN);
				Command orphanCommand = editPart.getCommand(request);
				request.setType(REQ_ADD);
				Command addCommand = getTargetEditPart().getCommand(request);
				if (addCommand == null || orphanCommand == null) {
					return UnexecutableCommand.INSTANCE;
				}
				orphanCommands.add(orphanCommand);
				addCommands.add(addCommand);

			}
			request.setType(originalType);

		}

		CompoundCommand command = new CompoundCommand();
		command.setDebugLabel("Drag Object Tracker"); //$NON-NLS-1$
		if (!moveCommands.isEmpty()) {
			command.add(moveCommands);
		}
		if (!orphanCommands.isEmpty()) {
			command.add(orphanCommands);
		}
		if (!addCommands.isEmpty()) {
			command.add(addCommands);
		}

		return command;
	}

	@Override
	protected Collection<IFigure> getExclusionSet() {
		if (exclusionSet == null) {
			List<?> set = getOperationSet();
			exclusionSet = new ArrayList<IFigure>(set.size() + 1);
			for (int i = 0; i < set.size(); i++) {
				GraphicalEditPart editpart = (GraphicalEditPart) set.get(i);
				exclusionSet.add(editpart.getFigure());
			}
		}
		return exclusionSet;
	}

	@Override
	protected void performDrag() {
		// try to preserve the selection!
		EditPartViewer viewer = null;
		if (!getOperationSet().isEmpty()) {
			viewer = ((EditPart) getOperationSet().get(0)).getViewer();
		}
		List<Object> models = new ArrayList<Object>();
		if (viewer != null) {
			Iterator<?> iter = getOperationSet().iterator();
			while (iter.hasNext()) {
				EditPart editPart = (EditPart) iter.next();
				Object model = editPart.getModel();
				if (model != null) {
					models.add(model);
				}
			}
		}
		super.performDrag();
		if (viewer != null && viewer.getEditPartRegistry() != null) {
			List<EditPart> selection = new ArrayList<EditPart>(models.size());
			for (Object model : models) {
				EditPart replacement = (EditPart) viewer.getEditPartRegistry()
						.get(model);
				if (replacement != null) {
					selection.add(replacement);
				}
			}
			viewer.setSelection(new StructuredSelection(selection));
		}

	}
}
