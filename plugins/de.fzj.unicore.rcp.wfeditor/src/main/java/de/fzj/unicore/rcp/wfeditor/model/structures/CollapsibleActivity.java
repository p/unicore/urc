/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.structures;

import java.util.Set;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author demuth
 * 
 */
@XStreamAlias("CollapsibleActivity")
public class CollapsibleActivity extends StructuredActivity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4995235326449671608L;

	/**
	 * The key for the collapsed property
	 */
	public static final String PROP_COLLAPSED = "collapsed";

	private Rectangle collapsedBounds, expandedBounds;

	private Dimension difference;

	public Rectangle getCollapsedBounds() {
		return collapsedBounds;
	}

	public Rectangle getExpandedBounds() {
		return expandedBounds;
	}
	
	@Override
	public void afterDeserialization()
	{
		super.afterDeserialization();
		if(difference == null && expandedBounds != null && collapsedBounds != null)
		{
			// backwards compatiblity
			difference = expandedBounds.getLocation().getDifference(collapsedBounds.getLocation());
		}
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_COLLAPSED);
		return propertiesToCopy;
	}

	public boolean isCollapsed() {
		Boolean collapsed = (Boolean) getPropertyValue(PROP_COLLAPSED);
		if (collapsed == null) {
			return false;
		} else {
			return collapsed;
		}
	}

	@Override
	public void setLocation(Point location) {
		if(isCollapsed())
		{
			if(collapsedBounds != null) collapsedBounds.setLocation(location);
			if(expandedBounds != null) expandedBounds.setLocation(location.getTranslated(difference));
		}
		else
		{
			if(collapsedBounds != null) collapsedBounds.setLocation(location.getTranslated(difference.getNegated()));
			if(expandedBounds != null) expandedBounds.setLocation(location);
		}
		super.setLocation(location);

	}

	@Override
	public void setSize(Dimension size) {
		if(isCollapsed())
		{
			if(collapsedBounds != null) collapsedBounds.setSize(size);
		}
		else if(expandedBounds != null) 
		{
			expandedBounds.setSize(size);
		}
		super.setSize(size);

	}


	@Override
	public void move(int dx, int dy) {

		super.move(dx, dy);

	}

	/**
	 * Convenience method for accessing the "collapsed" property which is true
	 * iff the CollapsibleActivity is collapsed
	 * 
	 * @param dirty
	 */
	public void setCollapsed(boolean collapsed) {
		if (collapsed) {
			super.setSize(getCollapsedBounds().getSize());
			super.setLocation(getCollapsedBounds().getTopLeft());
		} else {
			super.setSize(getExpandedBounds().getSize());
			super.setLocation(getExpandedBounds().getTopLeft());
		}
		setPropertyValue(PROP_COLLAPSED, collapsed);
	}

	public void setCollapsedBounds(Rectangle collapsedBounds) {
		this.collapsedBounds = collapsedBounds;
		if(collapsedBounds != null)
		{
			if (isCollapsed()) {
				super.setSize(collapsedBounds.getSize());
				super.setLocation(collapsedBounds.getTopLeft());
			}
			if(expandedBounds != null)
			{
				difference = expandedBounds.getLocation().getDifference(collapsedBounds.getLocation());
			}
		}
	}

	public void setExpandedBounds(Rectangle expandedBounds) {
		this.expandedBounds = expandedBounds;
		if(expandedBounds != null)
		{
			if (!isCollapsed()) {
				super.setSize(expandedBounds.getSize());
				super.setLocation(expandedBounds.getTopLeft());
			}
			if(collapsedBounds != null)
			{
				difference = expandedBounds.getLocation().getDifference(collapsedBounds.getLocation());
			}
		}
	}
}
