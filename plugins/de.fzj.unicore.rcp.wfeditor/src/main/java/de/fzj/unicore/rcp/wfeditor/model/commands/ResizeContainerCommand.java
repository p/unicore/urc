/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import java.util.List;

import org.eclipse.draw2d.Bendpoint;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ChangeBoundsRequest;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.parts.StructuredActivityPart;

/**
 * @author demuth
 * 
 */
public class ResizeContainerCommand extends Command {

	private StructuredActivity container;
	private Command internalCommand;
	private EditPartViewer viewer;

	public ResizeContainerCommand(StructuredActivity container,
			EditPartViewer viewer) {
		this.container = container;
		this.viewer = viewer;
	}

	@Override
	public void execute() {

		// container.refresh();

		// automatically resize the host's figure to contain all its children
		Rectangle newcontainerBounds = calculateNewContainerBounds(container,
				viewer);

		SetConstraintCommand cmd = new SetConstraintCommand();
		cmd.setPart(container);
		cmd.setLocation(newcontainerBounds);
		internalCommand = cmd;
		internalCommand.execute();
	}

	public StructuredActivity getContainer() {
		return container;
	}

	@Override
	public void redo() {
		internalCommand.redo();
	}

	public void setContainer(StructuredActivity container) {
		this.container = container;
	}

	@Override
	public void undo() {
		internalCommand.undo();
	}

	public static Rectangle calculateNewContainerBounds(
			List<IActivity> allChildren, Rectangle oldContainerBounds,
			Insets inner, Insets borderWidth, EditPartViewer viewer) {
		Rectangle newcontainerBounds = null;
		for (IActivity nextChild : allChildren) {
			Rectangle childBounds = AdjustLayoutCommand.getBoundsForLayouting(
					nextChild, viewer);
			if (newcontainerBounds == null) {
				newcontainerBounds = childBounds;
			} else {
				newcontainerBounds.union(childBounds);
			}
			for (Transition t : nextChild.getOutgoingTransitions()) {

				List<Bendpoint> bendpoints = t.getBendpoints();
				if (bendpoints != null) {
					for (Bendpoint bendpoint : bendpoints) {
						newcontainerBounds.union(bendpoint.getLocation());
					}
				}
			}

		}
		if (newcontainerBounds == null) {
			int innerX = inner == null ? 0 : inner.left + inner.right;
			int innerY = inner == null ? 0 : inner.top + inner.bottom;
			Dimension dim = WFConstants.DEFAULT_EMPTY_CONTAINER_SIZE;
			newcontainerBounds = new Rectangle(0, 0,
					dim.width-innerX,dim.height-innerY);
			Dimension diff = oldContainerBounds.getCenter().getDifference(
					newcontainerBounds.getCenter());
			newcontainerBounds.translate(diff.width, diff.height);
		}
		
		return calculateNewContainerBounds(newcontainerBounds, inner,
				borderWidth);
	}

	public static Rectangle calculateNewContainerBounds(
			Rectangle childrenBoundsUnion, Insets inner, Insets borderWidth) {

		Insets padding = inner.getAdded(borderWidth);
		Rectangle newcontainerBounds = childrenBoundsUnion.getCopy();
		newcontainerBounds.width += padding.left + padding.right;
		newcontainerBounds.height += padding.top + padding.bottom;
		newcontainerBounds.x -= padding.left;
		newcontainerBounds.y -= padding.top;
		return newcontainerBounds;
	}

	public static Rectangle calculateNewContainerBounds(
			StructuredActivity container, EditPartViewer viewer) {

		Insets padding = WFConstants.INNER_PADDING;
		Insets borderWidth = WFConstants.ZERO_INSETS;
		List<IActivity> allChildren = container.getChildren();
		Object part = viewer.getEditPartRegistry().get(container);
		IFigure containerFigure = null;
		if (part instanceof StructuredActivityPart) {
			StructuredActivityPart graphicalPart = (StructuredActivityPart) part;
			containerFigure = graphicalPart.getFigure();
			padding = graphicalPart.getInnerPadding();
			borderWidth = containerFigure.getInsets();
		}

		Rectangle oldcontainerBounds = new Rectangle(container.getLocation(),
				container.getSize());
		Rectangle newcontainerBounds = calculateNewContainerBounds(allChildren,
				oldcontainerBounds, padding, borderWidth, viewer);
		if (containerFigure != null) {
			containerFigure.translateToParent(newcontainerBounds);
		}
		return newcontainerBounds;
	}

	protected static ChangeBoundsRequest createChangeBoundsRequest(int direction) {
		ChangeBoundsRequest resizeRequest = new ChangeBoundsRequest();
		resizeRequest.setType(RequestConstants.REQ_RESIZE);
		resizeRequest.setCenteredResize(false);
		resizeRequest.setResizeDirection(direction);
		return resizeRequest;
	}

}
