package de.fzj.unicore.rcp.wfeditor.properties;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import de.fzj.unicore.rcp.common.guicomponents.AbstractListElement;
import de.fzj.unicore.rcp.common.guicomponents.GenericListEditor;
import de.fzj.unicore.rcp.common.guicomponents.IListListener;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.variables.IElementWithVariableDeclarations;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableList;

public class VariablesSection extends AbstractSection {
	private class WorkflowVariableWrapper extends AbstractListElement {

		private WorkflowVariable variable;

		public WorkflowVariableWrapper(WorkflowVariable variable) {
			this.variable = variable;
		}

		@Override
		public boolean canBeRemoved() {
			return getVariable().isDisposable();
		}

		public String getId() {
			return getVariable().getId();
		}

		@Override
		public String getName() {
			return getVariable().getName();
		}

		public WorkflowVariable getVariable() {
			return variable;
		}

		@Override
		public String nameValid(String name) {
			return getVariable().nameValid(name);
		}

		@Override
		public void setName(String name) {
			super.setName(name);
			getVariable().setName(name);
			updateElement();
		}
	}

	protected org.eclipse.swt.widgets.List variables;
	protected GenericListEditor<WorkflowVariableWrapper> variablesEditor;
	protected CCombo variableTypeCombo;

	protected Text initialValueText;

	protected boolean allowAdding = true;

	protected WorkflowVariableList workflowVariableList;
	protected ModifyListener modifyListener;
	protected SelectionListener selectionListener;
	protected IListListener<WorkflowVariableWrapper> listListener;

	protected Composite parent;

	protected void addListeners() {
		variablesEditor.addListListener(listListener);
		variableTypeCombo.addSelectionListener(selectionListener);
		initialValueText.addModifyListener(modifyListener);
	}

	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ITabbedPropertySection#createControls(org.eclipse.swt.widgets.Composite,
	 *      org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage)
	 */
	@Override
	public void createControls(Composite parent,
			TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);
		setParent(parent);
		FormData data;
		Composite flatForm = getWidgetFactory().createFlatFormComposite(parent);

		Group composite = getWidgetFactory().createGroup(flatForm, "Variables");
		data = new FormData();
		data.left = new FormAttachment(0, ITabbedPropertyConstants.HSPACE);
		data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(0, ITabbedPropertyConstants.VSPACE);
		data.bottom = new FormAttachment(100, -ITabbedPropertyConstants.VSPACE);
		composite.setLayoutData(data);
		composite.setLayout(new FormLayout());
		int vertOffset = 3 * ITabbedPropertyConstants.VSPACE;
		controls.add(composite);

		Composite listParent = getWidgetFactory().createComposite(composite);

		data = new FormData();
		data.left = new FormAttachment(0, 3);
		data.right = new FormAttachment(50, ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(0, vertOffset);
		listParent.setLayoutData(data);

		variablesEditor = new GenericListEditor<WorkflowVariableWrapper>(
				listParent, new ArrayList<WorkflowVariableWrapper>(),
				new boolean[] { true, true, false, false, true }) {
			@Override
			protected WorkflowVariableWrapper createNewListElement() {
				WorkflowVariable variable = new WorkflowVariable(element);
				variable.activate();
				variable.setDisposable(true);
				return new WorkflowVariableWrapper(variable);
			}
		};
		controls.add(variablesEditor.getControl());

		Label variableTypeLabel = getWidgetFactory().createLabel(composite,
				"Variable Type:");
		data = new FormData();
		data.left = new FormAttachment(listParent,
				2 * ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(0, vertOffset);
		variableTypeLabel.setLayoutData(data);
		controls.add(variableTypeLabel);

		variableTypeCombo = getWidgetFactory().createCCombo(composite);
		variableTypeCombo.setBackground(null);
		data = new FormData();
		data.left = new FormAttachment(listParent,
				2 * ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(variableTypeLabel,
				ITabbedPropertyConstants.VSPACE);
		variableTypeCombo.setLayoutData(data);
		variableTypeCombo.setItems(WorkflowVariable.VARIABLE_TYPES);
		controls.add(variableTypeCombo);

		Label initialValueLabel = getWidgetFactory().createLabel(composite,
				"Initial Value:");
		data = new FormData();
		data.left = new FormAttachment(variableTypeLabel,
				ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(0, vertOffset);
		initialValueLabel.setLayoutData(data);
		controls.add(initialValueLabel);

		initialValueText = getWidgetFactory().createText(composite, "",
				SWT.BORDER | SWT.LEFT);
		initialValueText.setSize(100, initialValueText.getSize().y);
		data = new FormData();
		data.left = new FormAttachment(variableTypeLabel,
				ITabbedPropertyConstants.HSPACE);
		data.right = new FormAttachment(100, -ITabbedPropertyConstants.HSPACE);
		data.top = new FormAttachment(initialValueLabel,
				ITabbedPropertyConstants.VSPACE);
		initialValueText.setLayoutData(data);
		controls.add(initialValueText);

		createListeners();
	}

	protected void createListeners() {
		selectionListener = new SelectionListener() {
			private void updateVariableType() {
				String selected = variableTypeCombo.getItem(variableTypeCombo
						.getSelectionIndex());
				WorkflowVariable var = getSelectedVariable();
				if (var != null) {
					var.setType(selected);
					updateElement();
				}

			}

			public void widgetDefaultSelected(SelectionEvent e) {
				updateVariableType();
			}

			public void widgetSelected(SelectionEvent e) {
				updateVariableType();
			}

		};
		listListener = new IListListener<WorkflowVariableWrapper>() {
			public void elementAdded(WorkflowVariableWrapper newElement) {
				workflowVariableList.addVariable(newElement.getVariable());
				element.getDataSourceList().addDataSource(
						newElement.getVariable());

			}

			public void elementChanged(WorkflowVariableWrapper changedElement) {

			}

			public void elementRemoved(WorkflowVariableWrapper removedElement) {
				workflowVariableList.removeVariable(removedElement
						.getVariable());
				removedElement.getVariable().dispose();

			}

			public void selectionChanged(WorkflowVariableWrapper newSelection) {
				WorkflowVariable newVar = null;
				if (newSelection != null) {
					newVar = newSelection.getVariable();
				}
				variableSelectionChanged(newVar);
			}
		};

		modifyListener = new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				String value = initialValueText.getText();
				WorkflowVariable var = getSelectedVariable();
				if (var != null && !value.equals(var.getInitialValue())) {
					var.setInitialValue(value);
					updateElement();
				}

			}
		};

	}

	@Override
	public void dispose() {
		removeListeners();
	}

	public Composite getParent() {
		return parent;
	}

	public WorkflowVariable getSelectedVariable() {
		WorkflowVariableWrapper wrapper = variablesEditor.getSelectedElement();
		if (wrapper != null) {
			return wrapper.getVariable();

		}
		return null;
	}

	protected void initVariables() {
		if (element == null || variablesEditor == null) {
			return;
		}
		removeListeners();
		variablesEditor.setAddingAllowed(allowAdding);
		workflowVariableList = element.getVariableList();
		List<WorkflowVariable> vars = workflowVariableList.getVariables();
		List<WorkflowVariableWrapper> wrappers = new ArrayList<WorkflowVariableWrapper>();
		for (WorkflowVariable workflowVariable : vars) {
			wrappers.add(new WorkflowVariableWrapper(workflowVariable));
		}
		variablesEditor.setElements(wrappers);
		variableSelectionChanged(getSelectedVariable());
		addListeners();

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (IFlowElement.PROP_DECLARED_VARIABLES.equals(evt.getPropertyName())) {
			initVariables();
		}
	}

	protected void removeListeners() {
		if (variablesEditor != null) {
			variablesEditor.removeListListener(listListener);
		}
		if (variableTypeCombo != null && !variableTypeCombo.isDisposed()) {
			variableTypeCombo.removeSelectionListener(selectionListener);
		}
		if (initialValueText != null && !initialValueText.isDisposed()) {
			initialValueText.removeModifyListener(modifyListener);
		}
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
		IElementWithVariableDeclarations e = (IElementWithVariableDeclarations) element;
		allowAdding = e.allowsAddingVariableDeclarations();
		// variables.removeAll();
		initVariables();
		updateEnabled();
	}

	public void setParent(Composite parent) {
		this.parent = parent;
	}

	public void updateElement() {
		element.removePropertyChangeListener(this);
		element.setVariableList(workflowVariableList);
		element.addPropertyChangeListener(this);
		element.getDiagram().setDirty(true);
	}

	public void variableSelectionChanged(WorkflowVariable selected) {
		if (selected == null) {
			variableTypeCombo.select(0);
			initialValueText.setText("");
		} else {
			variableTypeCombo.select(WorkflowVariable
					.getIndexFromTypeName(selected.getType()));
			initialValueText.setText(selected.getInitialValue());
		}
		variableTypeCombo.setEnabled(selected != null && canEditModel());
		initialValueText.setEnabled(selected != null && canEditModel());
	}

}
