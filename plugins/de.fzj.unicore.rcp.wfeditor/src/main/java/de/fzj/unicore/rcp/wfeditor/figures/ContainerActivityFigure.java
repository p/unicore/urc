/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.geometry.Rectangle;

import de.fzj.unicore.rcp.wfeditor.borders.CollapsibleBorder;

/**
 * @author hudsonr
 */
public class ContainerActivityFigure extends CollapsibleFigure {

	boolean selected;

	public ContainerActivityFigure(Rectangle bounds) {
		super(bounds, null);
		setOpaque(true);

	}

	public Border createBorder() {
		return border = new CollapsibleBorder(this);

	}

}
