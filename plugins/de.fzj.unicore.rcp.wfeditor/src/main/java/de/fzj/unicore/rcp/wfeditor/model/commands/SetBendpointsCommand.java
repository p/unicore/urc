/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import java.util.List;

import org.eclipse.draw2d.Bendpoint;

import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;

public class SetBendpointsCommand extends org.eclipse.gef.commands.Command {

	private Transition transition;
	private List<Bendpoint> oldBends, newBends;

	public SetBendpointsCommand(Transition transition, List<Bendpoint> newBends) {
		this.transition = transition;
		this.newBends = newBends;
	}

	@Override
	public void execute() {
		oldBends = transition.getBendpoints();
		redo();
	}

	@Override
	public void redo() {
		transition.setBendpoints(newBends);
	}

	@Override
	public void undo() {
		transition.setBendpoints(oldBends);
	}

}
