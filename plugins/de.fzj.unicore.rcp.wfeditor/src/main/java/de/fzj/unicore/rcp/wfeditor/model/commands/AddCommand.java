/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;

/**
 * AddCommand
 * 
 * @author Daniel Lee
 */
public class AddCommand extends Command {

	private IActivity child;
	private WorkflowDiagram childDiagram;
	private StructuredActivity parent;
	private int index = -1;

	private Rectangle constraints;
	private Rectangle oldConstraints;

	public AddCommand() {
		super();
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		redo();
	}

	public Rectangle getConstraints() {
		return constraints;
	}

	public int getIndex() {
		return index;
	}

	/**
	 * Returns the StructuredActivity that is the parent
	 * 
	 * @return the parent
	 */
	public StructuredActivity getParent() {
		return parent;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		if (constraints != null) {
			oldConstraints = new Rectangle(child.getLocation(), child.getSize());
			Dimension diff = constraints.getLocation().getDifference(
					oldConstraints.getLocation());
			child.move(diff.width, diff.height);
			child.setSize(constraints.getSize());
		}
		if (parent.getDiagram() != child.getDiagram()) {
			// moved element from one diagram to another
			childDiagram = child.getDiagram();
			child.setDiagram(parent.getDiagram());
		}
		if (getIndex() != -1) {
			parent.addChild(child, getIndex());
		} else {
			parent.addChild(child);
		}

	}

	/**
	 * Sets the child to the passed Activity
	 * 
	 * @param subpart
	 *            the child
	 */
	public void setChild(IActivity newChild) {
		child = newChild;
	}

	public void setConstraints(Rectangle constraints) {
		this.constraints = constraints;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * Sets the parent to the passed StructuredActiivty
	 * 
	 * @param newParent
	 *            the parent
	 */
	public void setParent(StructuredActivity newParent) {
		parent = newParent;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		if (oldConstraints != null) {
			Dimension diff = oldConstraints.getLocation().getDifference(
					constraints.getLocation());
			child.move(diff.width, diff.height);
			child.setSize(oldConstraints.getSize());
		}
		parent.removeChild(child);
		if (childDiagram != null) {
			child.setDiagram(childDiagram);
			childDiagram = null;
		}
	}

}
