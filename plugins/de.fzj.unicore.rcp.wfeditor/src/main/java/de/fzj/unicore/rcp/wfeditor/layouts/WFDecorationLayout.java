/*******************************************************************************
 * Copyright (c) 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.layouts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.geometry.Dimension;

public abstract class WFDecorationLayout extends DecorationLayout {

	public interface AnchorMotionListener {
		public void anchorEntered(int position);
	}

	class MouseMotionAdapter implements MouseMotionListener {
		int listenerAnchor;

		public MouseMotionAdapter(int anchor) {
			this.listenerAnchor = anchor;
		}

		public void mouseDragged(MouseEvent me) {
		}

		public void mouseEntered(MouseEvent me) {
			// fire listeners for the anchor point of the given mouse event
			fireMarkerMotionListeners(listenerAnchor);
		}

		public void mouseExited(MouseEvent me) {
		}

		public void mouseHover(MouseEvent me) {
		}

		public void mouseMoved(MouseEvent me) {
		}
	}

	private List<AnchorMotionListener> listeners = new ArrayList<AnchorMotionListener>();

	public void addAnchorMotionListener(AnchorMotionListener listener) {
		listeners.add(listener);
	}

	@Override
	protected Dimension calculatePreferredSize(IFigure container, int wHint,
			int hHint) {
		return new Dimension(0, 0);
	}

	private void fireMarkerMotionListeners(int anchor) {
		Iterator<AnchorMotionListener> it = listeners.iterator();
		while (it.hasNext()) {
			AnchorMotionListener listener = (AnchorMotionListener) it.next();
			listener.anchorEntered(anchor);
		}
	}

	public void removeAnchorMotionListener(AnchorMotionListener listener) {
		listeners.remove(listener);
	}

	@Override
	public void setConstraint(IFigure child, Object constraint) {
		super.setConstraint(child, constraint);
		child.addMouseMotionListener(new MouseMotionAdapter(
				((Integer) constraint).intValue()));
	}
}
