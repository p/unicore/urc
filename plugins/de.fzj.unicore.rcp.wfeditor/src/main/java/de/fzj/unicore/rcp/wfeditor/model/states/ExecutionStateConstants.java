/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.states;

/**
 * @author demuth
 * 
 */
public class ExecutionStateConstants {

	/**
	 * Possible values for states of activities
	 */
	public static final int STATE_UNSUBMITTED = 4;
	public static final String STRING_STATE_UNSUBMITTED = "unsubmitted";

	public static final int STATE_SUBMITTING = 5;
	public static final String STRING_STATE_SUBMITTING = "submitting";

	public static final int STATE_NOT_STARTED = 7;

	public static final int STATE_RUNNING = 10;
	public static final String STRING_STATE_RUNNING = "running";

	public static final int STATE_SUCCESSFUL = 20;
	public static final String STRING_STATE_SUCCESSFUL = "finished";

	public static final int STATE_FAILED = 30;
	public static final String STRING_STATE_FAILED = "failed";

	public static final int STATE_UNKNOWN = 35;
	public static final String STRING_STATE_UNKNOWN = "unknown";

	public static final int STATE_ABORTED = 40;
	public static final String STRING_STATE_ABORTED = "aborted";

	public static final int STATE_WAITING_FOR_USER_INPUT = 50;
	public static final String STRING_STATE_WAITING_FOR_USER_INPUT = "waiting for user input";

	public static final String DEFAULT_ITERATION_ID = "";

	public static String getStringForState(int state) {
		switch (state) {
		case STATE_UNSUBMITTED:
			return STRING_STATE_UNSUBMITTED;
		case STATE_SUBMITTING:
			return STRING_STATE_SUBMITTING;
		case STATE_RUNNING:
			return STRING_STATE_RUNNING;
		case STATE_SUCCESSFUL:
			return STRING_STATE_SUCCESSFUL;
		case STATE_FAILED:
			return STRING_STATE_FAILED;
		case STATE_UNKNOWN:
			return STRING_STATE_UNKNOWN;
		case STATE_ABORTED:
			return STRING_STATE_ABORTED;
		case STATE_WAITING_FOR_USER_INPUT:
			return STRING_STATE_WAITING_FOR_USER_INPUT;
		}
		return "unknown state";
	}
}
