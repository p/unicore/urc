package de.fzj.unicore.rcp.wfeditor.tools;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.Handle;
import org.eclipse.gef.requests.SelectionRequest;
import org.eclipse.gef.tools.SelectionTool;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;

/**
 * This tool can be used to create, select and delete data flows.
 */
public class ControlFlowCreationTool extends SelectionTool {

	protected EditPart sourceEditPart = null;

	public ControlFlowCreationTool() {
		super();
		setUnloadWhenFinished(false);
	}

	@Override
	protected boolean handleButtonDown(int button) {

		if (!stateTransition(STATE_INITIAL, STATE_DRAG)) {
			resetHover();
			return true;
		}
		resetHover();
		EditPartViewer viewer = getCurrentViewer();
		Point p = getLocation();

		if (getDragTracker() != null) {
			getDragTracker().deactivate();
		}

		if (viewer instanceof GraphicalViewer) {
			Handle handle = ((GraphicalViewer) viewer).findHandleAt(p);
			if (handle != null) {
				setDragTracker(handle.getDragTracker());
				return true;
			}
		}
		updateTargetRequest();
		((SelectionRequest) getTargetRequest()).setLastButtonPressed(button);
		updateTargetUnderMouse();
		sourceEditPart = null;
		EditPart editpart = getTargetEditPart();
		if (editpart != null) {
			if (editpart.getModel() instanceof IActivity) {
				ConnectionCreationDragTracker dragTracker = new ConnectionCreationDragTracker(
						editpart);
				dragTracker.setUnloadWhenFinished(false);
				setDragTracker(dragTracker);
				sourceEditPart = editpart;
			} else {
				setDragTracker(editpart.getDragTracker(getTargetRequest()));
			}

			lockTargetEditPart(editpart);
			return true;
		}
		return false;
	}

	@Override
	protected boolean handleButtonUp(int button) {

		boolean result = super.handleButtonUp(button);
		if (sourceEditPart != null && getTargetEditPart() == sourceEditPart) {
			getCurrentViewer().select(sourceEditPart);
		}
		sourceEditPart = null;
		return result;
	}

}
