/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;

/**
 * Handles the deletion of Activities.
 * 
 * @author Daniel Lee
 */
public class DeleteCommand extends Command {

	private IActivity child;
	private StructuredActivity parent;
	private int index = -1;
	private List<Transition> connections = new ArrayList<Transition>();
	private boolean deleteTransitions = true;

	private void collectConnections(IActivity a) {
		if (a instanceof StructuredActivity) {
			List<IActivity> children = ((StructuredActivity) a).getChildren();
			for (IActivity act : children) {
				collectConnections(act);
			}
		}
		for (Transition t : a.getIncomingTransitions()) {
			if (!connections.contains(t)) {
				connections.add(t);
			}
		}
		for (Transition t : a.getOutgoingTransitions()) {
			if (!connections.contains(t)) {
				connections.add(t);
			}
		}
	}

	private void deleteConnections(IActivity a) {

		for (int i = 0; i < connections.size(); i++) {
			Transition t = connections.get(i);
			// do NOT dispose transitions that have been disposed by someone
			// else
			// this may happen when source AND target of the transition have
			// been deleted
			if (!t.isDisposed()) {
				t.source.removeOutput(t);
				t.target.removeInput(t);
				t.dispose();
			}
		}
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		if (deleteTransitions) {
			collectConnections(child);
		}
		redo();
	}

	/**
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		if (deleteTransitions) {
			deleteConnections(child);
		}
		index = parent.getChildren().indexOf(child);
		parent.removeChild(child);
		child.dispose();
	}

	private void restoreConnections() {
		for (int i = 0; i < connections.size(); i++) {
			Transition t = connections.get(i);
			// do NOT un-dispose transitions that have been undisposed by
			// someone else
			// this may happen when source AND target of the transition have
			// been un-deleted
			if (t.isDisposed()) {
				t.target.addInput(t);
				t.source.addOutput(t);
				if (t.isDisposed()) {
					t.undoDispose();
				}
			}
		}
	}

	/**
	 * Sets the child to the passed Activity
	 * 
	 * @param a
	 *            the child
	 */
	public void setChild(IActivity a) {
		child = a;
	}

	public void setDeleteTransitions(boolean deleteTransitions) {
		this.deleteTransitions = deleteTransitions;
	}

	/**
	 * Sets the parent to the passed StructuredActivity
	 * 
	 * @param sa
	 *            the parent
	 */
	public void setParent(StructuredActivity sa) {
		parent = sa;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		parent.addChild(child, index);
		if (deleteTransitions) {
			restoreConnections();
		}
		if (child.isDisposed()) {
			child.undoDispose();
		}
	}

}
