/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

/**
 * @author demuth
 */
public class RepeatUntilActivityFigure extends CollapsibleFigure {

	private Label iterationLabel, iterationLabelDescription;
	private String selectedIteration;

	private boolean selected;

	public RepeatUntilActivityFigure(Rectangle bounds) {
		super(bounds, null);
		Font font = Display.getDefault().getSystemFont();
		iterationLabel = new Label("0");
		iterationLabel.setFont(font);
		iterationLabelDescription = new Label("Iteration: ");
		iterationLabelDescription.setFont(font);

	}


	public Label getIterationLabel() {
		return iterationLabel;
	}

	public Label getIterationLabelDescription() {
		return iterationLabelDescription;
	}

	public String getSelectedIteration() {
		return selectedIteration;
	}

	public boolean isSelected() {
		return selected;
	}

	@Override
	public void setBounds(Rectangle rect) {
		super.setBounds(rect);
		if (iterationLabel != null) {
			Dimension size = iterationLabel.getPreferredSize();
			int x = rect.right() - size.width - getInsets().right - 5;
			int y = rect.y + getInsets().top;
			Rectangle iterationLabelBounds = new Rectangle(x, y, size.width,
					size.height);
			iterationLabel.setBounds(iterationLabelBounds);

			size = iterationLabelDescription.getPreferredSize();
			x = iterationLabelBounds.x - size.width;
			Rectangle iterationLabelDescriptionBounds = new Rectangle(x, y,
					size.width, size.height);
			iterationLabelDescription
					.setBounds(iterationLabelDescriptionBounds);
		}

	}

	@Override
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public void setSelectedIteration(String selectedIteration) {
		this.selectedIteration = selectedIteration;
		getIterationLabel().setText(selectedIteration);
	}

}
