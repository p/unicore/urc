package de.fzj.unicore.rcp.wfeditor.extensions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import de.fzj.unicore.rcp.wfeditor.ui.IConditionChangeListener;

/**
 * An intermediate sub-class of {@link Composite} capturing the commonalities
 * among ConditionTypeControls
 * 
 * @author bjoernh
 * 
 */
public abstract class CommonConditionTypeControl extends Composite {

	protected List<Control> controls = new ArrayList<Control>();
	protected Label errorMsg;
	protected Set<IConditionChangeListener> listeners = new HashSet<IConditionChangeListener>();
	protected boolean valueEntered = true;
	protected boolean activitySelected = false;

	public CommonConditionTypeControl(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		for (Control c : controls) {
			c.setEnabled(enabled);
		}
	}

	public void addValidityChangeListener(IConditionChangeListener l) {
		listeners.add(l);

	}

	public void removeValidityChangeListener(IConditionChangeListener l) {
		listeners.remove(l);

	}

	public boolean isValid() {
		return activitySelected && valueEntered;
	}

}
