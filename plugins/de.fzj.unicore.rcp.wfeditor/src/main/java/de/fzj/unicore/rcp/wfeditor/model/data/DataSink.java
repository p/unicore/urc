package de.fzj.unicore.rcp.wfeditor.model.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.xml.namespace.QName;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

@XStreamAlias("DataSink")
@SuppressWarnings("unchecked")
public abstract class DataSink extends PropertySource implements IDataSink {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6528089953701601009L;
	private IFlowElement flowElement;
	private String id;

	private DataSinkList parentList;

	private transient IDataFlow[] disposedFlows = null;

	private transient Map<String, IDataFlow> flowMap = new HashMap<String, IDataFlow>();
	public static final QName TYPE = new QName("http://www.unicore.eu/",
			"DataSink");

	public DataSink(IFlowElement element) {
		this(element, new HashSet<String>());
	}

	public DataSink(IFlowElement element, Set<String> dataTypes) {
		this.flowElement = element;
		this.id = UUID.randomUUID().toString();
		setPropertyValue(PROP_ACCEPTED_DATA_TYPES, dataTypes);
		setPropertyValue(PROP_MUST_BE_FILLED, false);
		setPropertyValue(PROP_IS_FILLED, false);
		activate();
	}

	public void addIncomingFlow(IDataFlow flow) {
		addIncomingFlow(flow, -1);
	}

	public void addIncomingFlow(IDataFlow flow, int index) {
		if (flow == null || getIncomingFlowById(flow.getId()) != null) {
			return;
		}
		List<IDataFlow> flows = (List<IDataFlow>) getPropertyValue(PROP_DATA_FLOWS);
		if (flows == null) {
			flows = new ArrayList<IDataFlow>();
		}
		if (index < 0) {
			index = flows.size() + index;
		}
		flows.add(flow);
		getFlowMap().put(flow.getId(), flow);
		setPropertyValue(PROP_DATA_FLOWS, flows);
	}

	@Override
	protected void addPropertyDescriptors() {

	}

	public int compareTo(IDataSink o) {
		int result = getSinkType().getLocalPart().compareTo(
				o.getSinkType().getLocalPart());
		if (result != 0) {
			return result;
		} else {
			return getName().compareTo(o.getName());
		}
	}

	@Override
	public void dispose() {
		super.dispose();
		disposedFlows = getIncomingFlows();
		for (IDataFlow flow : disposedFlows) {
			if (flow.isConnected()) {
				flow.disconnect();
			}
		}
		if (getParentList() != null) {
			getParentList().removeDataSink(this);
		}

	}

	@Override
	public DataSink getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		DataSink copy = (DataSink) super.getCopy(toBeCopied, mapOfCopies,
				copyFlags);
		copy.setFlowElement(CloneUtils.getFromMapOrCopy(getFlowElement(),
				toBeCopied, mapOfCopies, copyFlags));
		copy.setParentSinkList(CloneUtils.getFromMapOrCopy(getParentList(),
				toBeCopied, mapOfCopies, copyFlags));
		if (getIncomingDataTypes() != null) {
			copy.setPropertyValue(PROP_ACCEPTED_DATA_TYPES,
					new HashSet<String>(getIncomingDataTypes()));
		}
		copy.flowMap = null;
		for (IDataFlow flow : getIncomingFlows()) {
			CloneUtils.getFromMapOrCopy(flow,
					toBeCopied, mapOfCopies, copyFlags);
			// do NOT add flows to this sink or source here!!
			// this leads to adding the copied flow to the diagram
			// immediately which is NOT desired!

		}
		return copy;
	}

	public IFlowElement getFlowElement() {
		return flowElement;
	}

	private Map<String, IDataFlow> getFlowMap() {
		if (flowMap == null) {
			restoreFlowMap();
		}
		return flowMap;
	}

	public String getId() {
		return id;
	}

	public Set<String> getIncomingDataTypes() {
		return (Set<String>) getPropertyValue(PROP_ACCEPTED_DATA_TYPES);
	}

	public IDataFlow getIncomingFlowById(String id) {
		return getFlowMap().get(id);
	}

	public IDataFlow[] getIncomingFlows() {
		List<IDataFlow> flows = (List<IDataFlow>) getPropertyValue(PROP_DATA_FLOWS);
		if (flows == null) {
			return new IDataFlow[0];
		} else {
			return flows.toArray(new IDataFlow[flows.size()]);
		}
	}

	public String getName() {
		return (String) getPropertyValue(PROP_NAME);
	}

	public DataSinkList getParentList() {
		return parentList;
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_NAME);
		propertiesToCopy.add(PROP_MUST_BE_FILLED);
		propertiesToCopy.add(PROP_IS_FILLED);
		return propertiesToCopy;
	}

	public QName getSinkType() {
		return DataSink.TYPE;
	}

	public boolean hasIncomingFlow(IDataFlow flow) {
		return getFlowMap().values().contains(flow);
	}

	private Object readResolve() {
		return this;
	}

	public int removeIncomingFlow(IDataFlow flow) {
		List<IDataFlow> flows = (List<IDataFlow>) getPropertyValue(PROP_DATA_FLOWS);
		int result = flows == null ? -1 : flows.indexOf(flow);
		if (result >= 0) {
			flows.remove(result);
			getFlowMap().remove(flow.getId());
			setPropertyValue(PROP_DATA_FLOWS, flows);
		}
		return result;
	}

	private void restoreFlowMap() {
		flowMap = new HashMap<String, IDataFlow>();
		for (IDataFlow flow : getIncomingFlows()) {
			flowMap.put(flow.getId(), flow);
		}
	}

	public void setFlowElement(IFlowElement flowElement) {
		this.flowElement = flowElement;
	}

	protected void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		setPropertyValue(PROP_NAME, name);
	}

	public void setParentSinkList(DataSinkList parentList) {
		this.parentList = parentList;
	}

	@Override
	public void undoDispose() {
		super.undoDispose();
		for (IDataFlow flow : disposedFlows) {
			if (!flow.isConnected()) {
				flow.reconnect();
			}
		}
		if (getParentList() != null) {
			getParentList().addDataSink(this);
		}
	}

}
