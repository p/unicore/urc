/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ComponentEditPolicy;
import org.eclipse.gef.requests.GroupRequest;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.commands.DeleteCommand;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;

/**
 * @author Daniel Lee
 */
public class ActivityEditPolicy extends ComponentEditPolicy {

	/**
	 * @see ComponentEditPolicy#createDeleteCommand(org.eclipse.gef.requests.GroupRequest)
	 */
	@Override
	protected Command createDeleteCommand(GroupRequest deleteRequest) {
		Object model = getHost().getModel();
		if (!(model instanceof IActivity)) {
			return null;
		}
		IActivity child = (IActivity) model;
		EditPart host = getHost();
		if (!child.isDeletable() || host == null || !host.isActive()) {
			return null;
		}
		EditPart parent = host.getParent();
		StructuredActivity struct = (StructuredActivity) parent.getModel();
		DeleteCommand deleteCmd = new DeleteCommand();
		deleteCmd.setParent(struct);
		deleteCmd.setChild(child);
		return deleteCmd;
	}

}
