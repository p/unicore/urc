/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.transitions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.eclipse.draw2d.Bendpoint;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.utils.StringUtils;
import de.fzj.unicore.rcp.wfeditor.model.FlowElement;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;
import de.fzj.unicore.rcp.wfeditor.utils.TransitionUtils;

/**
 * @author demuth
 */
@XStreamAlias("Transition")
public class Transition extends FlowElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6202597327081722892L;

	public static final String DEFAULT_TRANSITION_NAME = "Transition";

	/**
	 * Property for storing a list of serializable bendpoints i.e. coordinates
	 * where the transition should be bent when drawn.
	 */
	public static final String PROP_BENDPOINTS = "bendpoints";

	public static final String PROP_LABEL_TEXT = "label";

	public static QName TYPE = new QName("http://www.unicore.eu/", "Transition");

	/**
	 * This property is used for special transitions that close loops in the
	 * workflow. These transitions must be marked in order to be ignored by
	 * certain graph traversal algorithms.
	 */
	public static final String PROP_IS_CLOSING_LOOP = "closing loop";

	public IActivity source, target;

	/**
	 * This constructor should not be used by anyone except the
	 * {@link FlowElement#getCopy(Set, Map, int)} method.
	 */
	public Transition() {
		
	}

	public Transition(IActivity source, IActivity target) {
		this.source = source;
		this.target = target;

		if (source != null) {
			setDiagram(source.getDiagram());
			source.addOutput(this);

		}
		if (target != null) {
			if (getDiagram() != null) {
				setDiagram(target.getDiagram());
			}
			target.addInput(this);

		}

		if (source != null && source.getParent() != null) {
			setParent(source.getParent());
		} else if (target != null && target.getParent() != null) {
			setParent(target.getParent());
		}
	}

	@Override
	public void activate() {
		super.activate();
		if (source != null && source.getParent() != null) {
			setParent(source.getParent());
		} else if (target != null && target.getParent() != null) {
			setParent(target.getParent());
		}

		if (diagram != null) {
			String oldName = getName();
			String name = oldName;

			if (name == null || name.trim().length() == 0) {
				name = "Transition1";
			}

			Transition t = getDiagram().getTransitionByName(name);
			if (t == null) {
				getDiagram().addTransition(this);
			} else {
				if (t != this) {
					String myRawName = StringUtils.cutOffDigits(name);
					name = diagram.getUniqueElementName(myRawName);
					diagram.addTransition(this);
				}
			}
			if (!name.equals(oldName)) {
				setName(name);
			}
		}
	}

	@Override
	public void dispose() {
		if (getDiagram() != null) {
			getDiagram().removeTransition(this);
		}
		super.dispose();
	}

	@SuppressWarnings("unchecked")
	public List<Bendpoint> getBendpoints() {
		List<Bendpoint> result = (List<Bendpoint>) getPropertyValue(PROP_BENDPOINTS);
		if (result == null) {
			result = new ArrayList<Bendpoint>();
			setPropertyValue(PROP_BENDPOINTS, result);
		}
		return result;
	}

	@Override
	public Transition getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		Transition copy = null;
		IActivity copySource = source;
		IActivity copyTarget = target;
		boolean sourceBeingCopied = CloneUtils
				.isBeingCopied(source, toBeCopied);
		boolean targetBeingCopied = CloneUtils
				.isBeingCopied(target, toBeCopied);
		if (sourceBeingCopied) {
			copySource = CloneUtils.getFromMapOrCopy(source, toBeCopied,
					mapOfCopies, copyFlags);
		}
		if (targetBeingCopied) {
			// transition is inside the subgraph to be copied
			copyTarget = CloneUtils.getFromMapOrCopy(target, toBeCopied,
					mapOfCopies, copyFlags);

		}
		if (mapOfCopies.get(this) == null) {
			// duplicate transition only if it is valid
			if (TransitionUtils.isAllowedIgnoreParent(copySource, copyTarget)) {
				copy = (Transition) super.getCopy(toBeCopied, mapOfCopies,
						copyFlags);
				copy.source = copySource;
				copy.target = copyTarget;
				if (sourceBeingCopied) {
					copySource.addOutput(copy);
				}
				if (targetBeingCopied) {
					copyTarget.addInput(copy);
				}
			}
			return copy;
		} else {
			return (Transition) mapOfCopies.get(this);
		}

	}

	public String getLabelText() {
		String result = (String) getPropertyValue(PROP_LABEL_TEXT);
		if (result == null) {
			result = "";
		}
		return result;
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_IS_CLOSING_LOOP);
		return propertiesToCopy;
	}

	@Override
	public QName getType() {
		return TYPE;
	}

	@Override
	public Set<Class<?>> insertAfter() {
		Set<Class<?>> result = super.insertAfter();
		result.add(IActivity.class);
		return result;
	}

	public void insertBendpoint(int index, Bendpoint bp) {
		List<Bendpoint> bps = new ArrayList<Bendpoint>(getBendpoints());
		bps.add(index, bp);
		setBendpoints(bps);
	}

	@Override
	public void insertCopyIntoDiagram(ICopyable c, StructuredActivity parent,
			Set<IFlowElement> toBeCopied, Map<Object, Object> mapOfCopies,
			int copyFlags) {
		Transition copy = (Transition) c;
		boolean sourceRefersToCopy = copy.source.getOutgoingTransitions()
				.contains(copy);
		boolean targetRefersToCopy = copy.target.getIncomingTransitions()
				.contains(copy);
		if (!TransitionUtils.isOkToKeep(copy)) {
			// we can't check for parent relationship in the getCopy method
			// however, we can do this check here and dispose ourselves if it
			// fails
			copy.dispose();
			if (sourceRefersToCopy) {
				copy.source.removeOutput(copy);
			}
			if (targetRefersToCopy) {
				copy.target.removeInput(copy);
			}
		} else {
			if (!sourceRefersToCopy) {
				copy.source.addOutput(copy);
			}
			if (!targetRefersToCopy) {
				copy.target.addInput(copy);
			}
		}
	}

	/**
	 * Returns true for special transitions that close loops in the workflow.
	 * These transitions must be marked in order to be ignored by certain graph
	 * traversal algorithms.
	 */
	public boolean isClosingLoop() {
		Boolean result = (Boolean) getPropertyValue(PROP_IS_CLOSING_LOOP);
		if (result == null) {
			return false;
		} else {
			return result;
		}
	}

	public void move(int deltaX, int deltaY) {
		List<Bendpoint> bps = getBendpoints();
		List<Bendpoint> newBps = new ArrayList<Bendpoint>();
		for (Bendpoint b : bps) {
			newBps.add(new SerializableBendpoint(b.getLocation().x + deltaX, b
					.getLocation().y + deltaY));
		}
		setBendpoints(newBps);
	}

	public void removeBendpoint(Bendpoint bp) {
		List<Bendpoint> bps = new ArrayList<Bendpoint>(getBendpoints());
		bps.remove(bp);
		setBendpoints(bps);
	}

	public void removeBendpoint(int index) {
		List<Bendpoint> bps = new ArrayList<Bendpoint>(getBendpoints());
		bps.remove(index);
		setBendpoints(bps);
	}

	public void setBendpoint(int index, Bendpoint point) {
		List<Bendpoint> bps = new ArrayList<Bendpoint>(getBendpoints());
		bps.set(index, point);
		setBendpoints(bps);
	}

	public void setBendpoints(List<Bendpoint> bendpoints) {
		setPropertyValue(PROP_BENDPOINTS, bendpoints);
	}

	@Override
	public void setDiagram(WorkflowDiagram diagram) {
		super.setDiagram(diagram);

	}

	public void setLabelText(String label) {
		setPropertyValue(PROP_LABEL_TEXT, label);
	}

	@Override
	public String toString() {
		return source + "-->" + target;
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		super.updateToCurrentModelVersion(oldVersion, currentVersion);

	}

}
