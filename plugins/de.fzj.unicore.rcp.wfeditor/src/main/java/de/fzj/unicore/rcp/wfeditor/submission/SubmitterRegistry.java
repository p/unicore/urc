/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.submission;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;

import de.fzj.unicore.rcp.servicebrowser.filters.ContentFilter;
import de.fzj.unicore.rcp.servicebrowser.filters.DisjunctiveContentFilter;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.ISubmissionWizardPageFactory;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;

/**
 * Shared instance where new Submitters can be registered by plugins. Used to
 * find a suitable submitter.
 * 
 * @author demuth
 * 
 */
public class SubmitterRegistry {

	List<ISubmitter> submitters;
	Map<Class<?>, SubmissionServiceFilter> filters;
	Map<Class<?>, Integer> maxDepths;
	Map<Class<?>, ISubmissionWizardPageFactory> wizardPageFactories;

	/**
	 * 
	 */
	public SubmitterRegistry() {
		submitters = Collections.synchronizedList(new ArrayList<ISubmitter>());
		filters = new ConcurrentHashMap<Class<?>, SubmissionServiceFilter>();
		maxDepths = new ConcurrentHashMap<Class<?>, Integer>();
		wizardPageFactories = new ConcurrentHashMap<Class<?>, ISubmissionWizardPageFactory>();
		iterateOverExtensions();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.ISubmitter#canSubmit(de.fzj.unicore
	 * .rcp.wfeditor.model.WorkflowDiagram)
	 */
	public boolean canDealWith(WorkflowDiagram model) {
		List<ISubmitter> suitable = findSuitableSubmitters(model);
		return suitable.size() > 0;
	}

	private List<ISubmitter> findSuitableSubmitters(WorkflowDiagram model) {
		synchronized (submitters) {
			List<ISubmitter> suitable = new ArrayList<ISubmitter>();
			for (ISubmitter submitter : submitters) {
				if (submitter.canDealWith(model)) {
					suitable.add(submitter);
				}
			}
			return suitable;
		}
	}

	public int getMaxServiceDepth(WorkflowDiagram model) {
		int result = 0;
		List<ISubmitter> suitable = findSuitableSubmitters(model);
		for (ISubmitter submitter : suitable) {
			int depth = maxDepths.get(submitter.getClass());
			if (result < depth) {
				result = depth;
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.submission.ISubmitter#getServiceFilters(de
	 * .fzj.unicore.rcp.wfeditor.model.WorkflowDiagram)
	 */
	public List<ContentFilter> getServiceFilters(WorkflowDiagram model) {
		List<ContentFilter> filters = new ArrayList<ContentFilter>();
		List<ISubmitter> suitable = findSuitableSubmitters(model);
		for (ISubmitter submitter : suitable) {
			SubmissionServiceFilter cf;
			try {
				cf = this.filters.get(submitter.getClass()).getClass()
						.newInstance();
				cf.setWorkflowDiagram(model);
				if (cf != null) {
					filters.add(cf);
				}
			} catch (Exception e) {
				WFActivator
						.log(IStatus.WARNING,
								"Unable to instantiate filter for determining suitable workflow services.",
								e);
			}

		}
		List<ContentFilter> result = new ArrayList<ContentFilter>();
		if (filters.size() > 0) {
			DisjunctiveContentFilter disjunctive = new DisjunctiveContentFilter(
					"submissionFilter", filters);
			result.add(disjunctive);
		}
		return result;
	}

	public List<ISubmitter> getSuitableSubmitters(WorkflowDiagram model) {
		synchronized (submitters) {
			List<ISubmitter> suitable = new ArrayList<ISubmitter>();
			for (ISubmitter submitter : submitters) {
				if (submitter.canDealWith(model)) {
					try {
						suitable.add(submitter.getClass().newInstance());
					} catch (Exception e) {
						// do nothing
					}
				}
			}
			return suitable;
		}
	}

	public ISubmissionWizardPageFactory getWizardPageFactory(
			ISubmitter submitter) {
		return wizardPageFactories.get(submitter.getClass());
	}

	private void iterateOverExtensions() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(WFConstants.EXTENSION_POINT_SUBMITTER);
		IConfigurationElement[] members = extensionPoint
				.getConfigurationElements();
		submitters.clear();
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			try {
				ISubmitter sub = (ISubmitter) member
						.createExecutableExtension("class");
				registerSubmitter(sub);
				Class<? extends ISubmitter> c = sub.getClass();
				try {
					ISubmissionWizardPageFactory factory = (ISubmissionWizardPageFactory) member
							.createExecutableExtension("wizardPageFactory");
					wizardPageFactories.put(c, factory);
				} catch (Exception e) {
					// do nothing
				}

				try {
					int depth = Integer.parseInt(member
							.getAttribute("maxDepthInGridBrowser"));
					maxDepths.put(c, depth);
				} catch (Exception e) {
					maxDepths.put(c, 5); // fallback to 5
				}
				try {
					SubmissionServiceFilter filter = (SubmissionServiceFilter) member
							.createExecutableExtension("serviceFilter");
					if (filter != null) {
						filters.put(c, filter);
					}
				} catch (Exception e) {
					// do nothing
				}

			} catch (CoreException ex) {
				WFActivator.log(
						"Could not enable the workflow submitter for extension "
								+ member.getDeclaringExtension()
										.getUniqueIdentifier(), ex);
			}

		}
	}

	private void registerSubmitter(ISubmitter c) {
		submitters.add(c);
	}


}
