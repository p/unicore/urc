/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.extensionpoints;

import org.eclipse.core.runtime.IExecutableExtension;

import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType;
import de.fzj.unicore.rcp.wfeditor.ui.IConditionTypeControlProducer;

/**
 * Extension point for creating EditParts (controllers) for elements of the
 * graph model. The types of graph elements and their EditParts are being
 * defined by additional plugins. This is done by using this extension point and
 * the PaletteEntry extension point of the workflow editor.
 * 
 * @author demuth
 * 
 */
public interface IConditionTypeExtensionPoint extends IExecutableExtension {

	/**
	 * A model object that represent the newly introduced condition type.
	 * Example condition types are workflow variable comparison and job exit
	 * status comparison condition types.
	 * 
	 * @return
	 */
	public IConditionType createNewConditionType();

	/**
	 * This method is used for creating a control that can be used for editing
	 * instances of the newly introduced condition type.
	 * 
	 * @return
	 */
	public IConditionTypeControlProducer getControlProducer();

	/**
	 * This extension's unique identifier. It is used for finding the right
	 * extension for creating the {@link IConditionTypeControlProducer} for
	 * editing a particular {@link IConditionType} model object.
	 * 
	 * @return
	 */
	public String getExtensionId();

}
