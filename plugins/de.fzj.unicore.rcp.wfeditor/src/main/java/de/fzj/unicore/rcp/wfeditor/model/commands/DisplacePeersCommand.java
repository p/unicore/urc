/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Ray;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.utils.GeometryUtils;

/**
 * @author demuth
 * 
 */
@SuppressWarnings("deprecation")
public class DisplacePeersCommand extends Command {

	class SweepEvent implements Comparable<SweepEvent> {

		int type;
		Double time;
		Rectangle rect;

		public SweepEvent(int type, double time, Rectangle rect) {
			super();
			this.type = type;
			this.time = time;
			this.rect = rect;
		}

		public int compareTo(SweepEvent o) {
			return time.compareTo(o.time);
		}

	}

	private static final int EVENT_TYPE_START = 0, EVENT_TYPE_END = 1;

	private IActivity resized;

	EditPartViewer viewer;

	private CompoundCommand internalCommand;

	public DisplacePeersCommand(IActivity resized, EditPartViewer viewer) {
		this.resized = resized;
		this.viewer = viewer;
	}

	@Override
	public void execute() {
		internalCommand = new CompoundCommand();
		Map<IActivity, Rectangle> map = new HashMap<IActivity, Rectangle>();

		StructuredActivity parent = resized.getParent();
		Rectangle[] allNodes = new Rectangle[parent.getChildren().size()];

		Rectangle center = null;
		for (int i = 0; i < allNodes.length; i++) {
			IActivity current = parent.getChildren().get(i);

			Rectangle r = AdjustLayoutCommand.getBoundsForLayouting(current,
					viewer);
			if (current == resized) {
				center = r;
			}
			map.put(current, r);
			allNodes[i] = r;
			r.expand(WFConstants.MINIMAL_BETWEEN_ACTIVITY_PADDING);
		}
		if (allNodes.length > 1) {

			radialSweep(center, allNodes);

		}

		for (IActivity act : map.keySet()) {
			Rectangle r = map.get(act);
			r.crop(WFConstants.MINIMAL_BETWEEN_ACTIVITY_PADDING);
			MoveActivityCommand cmd = new MoveActivityCommand(act,
					r.getLocation());
			internalCommand.add(cmd);
		}

		internalCommand.execute();
	}

	
	private void radialSweep(Rectangle centerRect, Rectangle[] rectangles) {

		Point center = centerRect.getTop();

		PriorityQueue<SweepEvent> events = new PriorityQueue<SweepEvent>();
		Set<Rectangle> currentRectangles = new HashSet<Rectangle>();
		for (Rectangle current : rectangles) {
			double time = current.contains(center) ? 0 : GeometryUtils
					.getDistanceFrom(current, center);
			if (current == centerRect) {
				events.add(new SweepEvent(EVENT_TYPE_START, -1, current)); // make
																			// sure
																			// the
																			// center
																			// rect
																			// gets
																			// added
																			// first!
			} else {
				events.add(new SweepEvent(EVENT_TYPE_START, time, current));
			}

		}

		while (!events.isEmpty()) {
			SweepEvent e = events.poll();
			Rectangle current = e.rect;
			switch (e.type) {
			case EVENT_TYPE_START:

				double time = current.contains(center) ? 0 : GeometryUtils
						.getDistanceFrom(current, center);
				if (current != centerRect && time != e.time) {
					continue; // old event => ignore
				}

				double push = -1;
				Ray direction = new Ray(center, current.getTop());
				if (direction.length() < GeometryUtils.EPSILON) {
					direction = new Ray(1, 0);
				}
				if (current != centerRect) {
					// find out whether the rectangle should be moved along the
					// vector
					// connecting the center of the radial sweep and the center
					// of the rectangle
					for (Rectangle r : currentRectangles) {
						if (r.intersects(current)) {
							double localPush = Double.MAX_VALUE;
							// find the minimum value that is sufficient to push
							// the current rectangle out of the intersecting
							// rectangle
							if (direction.x > 0) {
								localPush = Math.min(localPush,
										(r.x + r.width - current.x)
												/ ((double) direction.x));
							} else if (direction.x < 0) {
								localPush = Math.min(localPush, (r.x
										- current.x - current.width)
										/ ((double) direction.x));
							}
							if (direction.y > 0) {
								localPush = Math.min(localPush,
										(r.y + r.height - current.y)
												/ ((double) direction.y));
							} else if (direction.y < 0) {
								localPush = Math.min(localPush, (r.y
										- current.y - current.height)
										/ ((double) direction.y));
							}

							push = Math.max(push, localPush);
						}
					}
				}
				if (push > 0) {

					current.x += (int) (push * direction.x);
					if (direction.x != 0) {
						current.x += direction.x / Math.abs(direction.x); // push
																			// one
																			// unit
																			// further
																			// to
																			// make
																			// sure
																			// the
																			// intersection
																			// is
																			// gone
					}
					current.y += (int) (push * direction.y);
					if (direction.y != 0) {
						current.y += direction.y / Math.abs(direction.y);
					}
					time = current.contains(center) ? 0 : GeometryUtils
							.getDistanceFrom(current, center);
					events.add(new SweepEvent(EVENT_TYPE_START, time, e.rect));
				} else {
					currentRectangles.add(e.rect);
					time = Math.abs(GeometryUtils.getMaxDistanceFrom(e.rect,
							center));
					events.add(new SweepEvent(EVENT_TYPE_END, time, e.rect));
				}
				break;
			case EVENT_TYPE_END:
				currentRectangles.remove(e.rect);
				break;

			}
		}
	}

	@Override
	public void redo() {
		internalCommand.redo();
	}

	@Override
	public void undo() {
		internalCommand.undo();
	}

}
