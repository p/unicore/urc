package de.fzj.unicore.rcp.wfeditor.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.interfaces.ISerializable;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

@XStreamAlias("PropertySource")
public abstract class PropertySource implements IPropertySourceWithListeners,
		ISerializable, Cloneable, ICopyable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1570979446319067270L;
	protected transient List<PropertyChangeListener> transientListeners = new ArrayList<PropertyChangeListener>();
	protected List<PropertyChangeListener> listeners = new ArrayList<PropertyChangeListener>();

	protected transient List<IPropertyDescriptor> descriptors = new ArrayList<IPropertyDescriptor>();

	protected Map<Object, Object> properties;

	protected boolean disposable = false;

	private transient int stateBeforeSerialization = 0;

	protected transient boolean updatedToCurrentModelVersion = false;

	public PropertySource() {
		properties = new HashMap<Object, Object>();
		setState(StateConstants.STATE_CREATED);
		// setPropertyValue(PROP_VALIDATION_RESULT, new ValidationResult());
		addPropertyDescriptors();

	}

	public void activate() {
		setState(StateConstants.STATE_ACTIVE);
	}

	public void addPersistentPropertyChangeListener(PropertyChangeListener l) {
		if (getPersistentListeners().contains(l)) {
			return;
		}
		getPersistentListeners().add(l);
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		if (getListeners().contains(l)) {
			return;
		}
		getListeners().add(l);
	}

	protected void addPropertyDescriptor(IPropertyDescriptor descriptor) {
		// do not add the descriptor if there already is one for the same
		// property!
		for (IPropertyDescriptor d : getPropertyDescriptorList()) {
			if (d.getId().equals(descriptor.getId())) {
				return;
			}
		}
		getPropertyDescriptorList().add(descriptor);
	}

	protected abstract void addPropertyDescriptors();

	public void afterDeserialization() {
		addPropertyDescriptors();
	}

	public void afterSerialization() {
		setState(stateBeforeSerialization);
	}

	public void beforeSerialization() {
		stateBeforeSerialization = getState();
		setState(StateConstants.STATE_SERIALIZED);
	}

	/**
	 * Do a "flat" clone, i.e. do NOT recursively clone properties
	 */
	@Override
	public PropertySource clone() throws CloneNotSupportedException {
		PropertySource clone = (PropertySource) super.clone();
		// do not clone listeners!
		clone.listeners = new ArrayList<PropertyChangeListener>();
		clone.transientListeners = new ArrayList<PropertyChangeListener>();
		// do not clone properties!
		clone.properties = new HashMap<Object, Object>();

		return clone;
	}

	public void clonePropertyValuesFrom(PropertySource other) {
		clonePropertyValuesFrom(other, other.getProperties().keySet(), true);
	}

	public void clonePropertyValuesFrom(PropertySource other,
			Set<Object> propertyKeys, boolean clearOwnProperties) {
		if (clearOwnProperties) {
			properties = new HashMap<Object, Object>(propertyKeys.size());
		}
		properties.put(PROP_STATE, other.getState());
		// perform a deep clone on the properties
		for (Object key : propertyKeys) {

			Object value = other.getPropertyValue(key);
			Object oldValue = getPropertyValue(key);
			Object newKey = key;
			Object newValue = value;
			// now try to clone key and value
			try {
				newKey = CloneUtils.clone(key);
			} catch (CloneNotSupportedException e) {
				// do nothing
			}
			try {
				if (value != null) {
					newValue = CloneUtils.clone(value);
				}
			} catch (CloneNotSupportedException e) {
				// do nothing
			}
			properties.put(newKey, newValue);
			if (!(oldValue == null ? newValue == null : oldValue
					.equals(newValue))) {
				firePropertyChange(key, oldValue, newValue);
			}
		}
	}

	public void copyPropertyValuesFrom(PropertySource other,
			Set<Object> propertyKeys, Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		for (Object key : propertyKeys) {
			Object newKey;
			// now try to clone key
			try {
				newKey = CloneUtils.clone(key);
			} catch (CloneNotSupportedException e) {
				newKey = key;
			}

			Object value = other.getPropertyValue(key);
			Object newValue = value;
			if (value instanceof ICopyable) {
				newValue = CloneUtils.getFromMapOrCopy((ICopyable) value,
						toBeCopied, mapOfCopies, copyFlags);
			} else if (value instanceof Cloneable) {
				try {
					newValue = CloneUtils.clone(value);
					mapOfCopies.put(newKey, newValue);
				} catch (CloneNotSupportedException e) {
					// do nothing
				}
			}
			setPropertyValue(newKey, newValue);
		}
	}

	protected String createRandomId() {
		return UUID.randomUUID().toString();
	}

	public void dispose() {
		setState(StateConstants.STATE_DISPOSED);
	}

	public void finalDispose(IProgressMonitor progress) {
		// do nothing by default
	}

	protected void firePropertyChange(Object prop, Object old, Object newValue) {
		firePropertyChange(prop.toString(), old, newValue);
	}

	protected void firePropertyChange(PropertyChangeEvent evt) {
		try {
			String prop = evt.getPropertyName();
			Object newValue = evt.getNewValue();
			// fire event only if we are active or if we have just changed our
			// state to DISPOSED or ACTIVE
			if (isActive()
					|| PROP_STATE.equals(prop)
					&& (StateConstants.STATE_DISPOSED == (Integer) newValue || StateConstants.STATE_ACTIVE == (Integer) newValue)) {

				for (PropertyChangeListener target : getListeners()) {
					target.propertyChange(evt);
				}
				for (PropertyChangeListener target : getPersistentListeners()) {
					target.propertyChange(evt);
				}
			}
		} catch (Exception e) {
			WFActivator.log("Unable to notify listeners of property change: " + evt.getPropertyName() + " ( "
					+ evt.getOldValue() + " => " + evt.getNewValue() + ")", e);
		}
	}

	protected void firePropertyChange(String name, Object oldValue,
			Object newValue) {
		PropertyChangeEvent evt = new PropertyChangeEvent(this, name, oldValue,
				newValue);
		firePropertyChange(evt);
	}

	public ICopyable getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		if (mapOfCopies.get(this) != null) {
			return (ICopyable) mapOfCopies.get(this);
		}
		PropertySource copy;
		try {
			copy = clone();
		} catch (CloneNotSupportedException e1) {
			WFActivator.log(
					"Unable to copy selected element(s): " + e1.getMessage(),
					e1);
			return null;
		}

		mapOfCopies.put(this, copy);
		copy.properties.put(PROP_STATE, getState());
		copy.copyPropertyValuesFrom(this, getPropertiesToCopy(), toBeCopied,
				mapOfCopies, copyFlags);

		return copy;
	}

	public Object getEditableValue() {
		return this;
	}

	public List<PropertyChangeListener> getListeners() {
		if (transientListeners == null) {
			transientListeners = new ArrayList<PropertyChangeListener>();
		}
		return transientListeners;
	}

	public List<PropertyChangeListener> getPersistentListeners() {
		return listeners;
	}

	public Map<Object, Object> getProperties() {
		return properties;
	}

	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = new HashSet<Object>();
		propertiesToCopy.add(PROP_VALIDATION_RESULT);
		propertiesToCopy.add(PROP_STATE);
		return propertiesToCopy;
	}

	public List<IPropertyDescriptor> getPropertyDescriptorList() {
		if (descriptors == null) {
			descriptors = new ArrayList<IPropertyDescriptor>();
		}
		return descriptors;
	}

	public IPropertyDescriptor[] getPropertyDescriptors() {
		return getPropertyDescriptorList().toArray(new IPropertyDescriptor[0]);
	}

	public Object getPropertyValue(Object propName) {
		return getPropertyValue((String) propName);
	}

	public Object getPropertyValue(String propName) {
		return properties.get(propName);
	}

	public int getState() {
		return (Integer) getPropertyValue(PROP_STATE);
	}

	public Set<Class<?>> insertAfter() {
		return new HashSet<Class<?>>(); // we don't care
	}

	public Set<Class<?>> insertBefore() {
		return new HashSet<Class<?>>(); // we don't care
	}

	public void insertCopyIntoDiagram(ICopyable copy,
			StructuredActivity parent, Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		// do nothing by default

	}

	public boolean isActive() {
		int state = getState();
		return StateConstants.STATE_ACTIVE == state;
	}

	public boolean isAlreadyListening(PropertyChangeListener l) {
		return transientListeners != null && transientListeners.contains(l);
	}

	public boolean isAlreadyListeningPersistently(PropertyChangeListener l) {
		return listeners != null && listeners.contains(l);
	}

	public boolean isDisposable() {
		return disposable;
	}

	public boolean isDisposed() {
		int state = getState();
		return StateConstants.STATE_DISPOSED == state;
	}

	public boolean isPropertySet(Object propName) {
		return getPropertyValue(propName) != null;
	}

	public void removeAllPersistentPropertyChangeListeners() {
		getPersistentListeners().clear();

	}

	public void removeAllPropertyChangeListeners() {
		getListeners().clear();

	}

	public void removePersistentPropertyChangeListener(PropertyChangeListener l) {
		getPersistentListeners().remove(l);
	}

	/**
	 * Completely removes a property and returns the current value for
	 * convenience.
	 * 
	 * @param propName
	 * @return
	 */
	public Object removeProperty(Object propName) {
		Object old = properties.get(propName);
		properties.remove(propName);
		if (old != null) {
			firePropertyChange(propName, old, null);
		}
		return old;
	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		getListeners().remove(l);
	}

	protected void removePropertyDescriptor(IPropertyDescriptor descriptor) {
		getPropertyDescriptorList().remove(descriptor);
	}

	public void resetPropertyValue(Object propName) {
	}

	public void setDisposable(boolean disposable) {
		this.disposable = disposable;
	}

	public void setPropertyValue(Object propName, Object val) {
		Object old = properties.get(propName);
		if (val != null) {
			properties.put(propName, val);
		} else {
			properties.remove(propName);
		}
		firePropertyChange(propName, old, val);
	}

	public void setState(int state) {
		Object oldState = getPropertyValue(PROP_STATE);
		if (oldState != null && getState() == state) {
			return;
		}
		setPropertyValue(PROP_STATE, state);
	}

	public void undoDispose() {
		setState(StateConstants.STATE_ACTIVE);
	}

	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		// do nothing by default
		updatedToCurrentModelVersion = true;
	}
}
