package de.fzj.unicore.rcp.wfeditor.extensionpoints;

import de.fzj.unicore.rcp.wfeditor.model.commands.DataFlowAssemblyCommand;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;

public interface IDataFlowAssembler {

	public DataFlowAssemblyCommand getAssemblyCommand(IDataSource source,
			IDataSink sink) throws Exception;
}
