package de.fzj.unicore.rcp.wfeditor.model;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.services.IDisposable;

/**
 * An object that can be disposed of (and performs some cleanup when disposed),
 * but also provides a method for undoing the disposal. This obviously implies
 * that the changes made in {@link IDisposable#dispose()} must not be
 * irreversible.
 * 
 * @author bdemuth
 * 
 */
public interface IDisposableWithUndo extends IDisposable {

	/**
	 * Perform some final clean up when the Disposable is REALLY REALLY not
	 * needed anymore. The contract is that undoDispose() will NEVER be called
	 * after calling this.
	 * 
	 * @return
	 */
	public void finalDispose(IProgressMonitor progress);

	public boolean isDisposable();

	public boolean isDisposed();

	/**
	 * Undo the actions performed in dispose!
	 * 
	 */
	public void undoDispose();

}
