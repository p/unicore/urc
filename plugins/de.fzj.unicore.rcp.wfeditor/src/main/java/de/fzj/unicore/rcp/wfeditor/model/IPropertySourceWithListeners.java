package de.fzj.unicore.rcp.wfeditor.model;

import java.beans.PropertyChangeListener;
import java.util.List;

import org.eclipse.ui.views.properties.IPropertySource;

/**
 * An object with properties that are being observed by listeners. Listeners can
 * either be transient or persisted with the property source and restored during
 * deserialisation. Objects of this type have a lifecycle: they become activated
 * (and they do not send any events to registered listeners before being
 * activated) and may be disposed.
 * 
 * @author bdemuth
 * 
 */
public interface IPropertySourceWithListeners extends IPropertySource,
		IDisposableWithUndo, IActivatable {

	/**
	 * The key for the state property (see {@link StateConstants} for possible
	 * values)
	 */
	public static final String PROP_STATE = "state";

	/**
	 * The key for a property with a ValidationResult value that reflects
	 * whether this part of the workflow is valid for submission or not.
	 * Additional validation checks might be performed during submission to a
	 * particular workflow service. These checks might abort the submission of
	 * the workflow.
	 */
	public static final String PROP_VALIDATION_RESULT = "Validation Result";

	/**
	 * Returns true iff the given listener has already been added
	 * 
	 * @param l
	 */
	public void addPersistentPropertyChangeListener(PropertyChangeListener l);

	public void addPropertyChangeListener(PropertyChangeListener l);

	public List<PropertyChangeListener> getListeners();

	public List<PropertyChangeListener> getPersistentListeners();

	public int getState();

	public boolean isAlreadyListening(PropertyChangeListener l);

	/**
	 * Returns true iff the given listener has already been added persistently
	 * 
	 * @param l
	 */
	public boolean isAlreadyListeningPersistently(PropertyChangeListener l);

	public void removeAllPersistentPropertyChangeListeners();

	public void removeAllPropertyChangeListeners();

	public void removePersistentPropertyChangeListener(PropertyChangeListener l);

	public void removePropertyChangeListener(PropertyChangeListener l);

	public void setState(int state);
}
