package de.fzj.unicore.rcp.wfeditor.properties;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.gef.EditPart;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;

public abstract class AbstractSection extends AbstractPropertySection implements
		PropertyChangeListener {

	protected IFlowElement element;

	/**
	 * List of controls that shall automatically be enabled/disabled depending
	 * on whether the diagram can be edited or not. Subclasses should add their
	 * controls here.
	 */
	protected List<Control> controls = new ArrayList<Control>();

	@Override
	public void aboutToBeHidden() {
		super.aboutToBeHidden();
		if (element != null) {
			element.removePropertyChangeListener(this);
		}
	}

	@Override
	public void aboutToBeShown() {
	}

	protected boolean canEditModel() {
		if (element == null) {
			return false;
		} else {
			return element.getDiagram().isEditable();
		}
	}

	/**
	 * Empty default implementation. Some subclasses are happy with this, others are not.
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {

	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		if (element != null) {
			element.removePropertyChangeListener(this);
		}
		super.setInput(part, selection);
		Assert.isTrue(selection instanceof IStructuredSelection);
		Object input = ((IStructuredSelection) selection).getFirstElement();
		Assert.isTrue(input instanceof EditPart);
		Object model = ((EditPart) input).getModel();
		Assert.isTrue(model instanceof IFlowElement);
		this.element = (IFlowElement) model;
		if (element != null) {
			element.addPropertyChangeListener(this);
		}
	}

	protected void updateEnabled() {
		for (Control c : controls) {
			c.setEnabled(canEditModel());
		}
	}

}
