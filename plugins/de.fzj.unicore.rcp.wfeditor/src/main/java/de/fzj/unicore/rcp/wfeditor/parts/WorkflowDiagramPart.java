/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import java.util.EventObject;
import java.util.Map;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.CommandStackListener;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.figures.WorkflowDiagramFigure;
import de.fzj.unicore.rcp.wfeditor.layouts.GraphAnimation;
import de.fzj.unicore.rcp.wfeditor.policies.ActivityContainerEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.PasteEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.RemoveIllegalTransitionsEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.StructuredActivityLayoutEditPolicy;

/**
 * @author demuth
 */
@SuppressWarnings({"rawtypes"})
public class WorkflowDiagramPart extends StructuredActivityPart {

	CommandStackListener stackListener = new CommandStackListener() {
		public void commandStackChanged(EventObject event) {
			if (!GraphAnimation.captureLayout(getFigure())) {
				return;
			}
			while (GraphAnimation.step()) {
				getFigure().getUpdateManager().performUpdate();
			}
			GraphAnimation.end();
		}
	};

	/**
	 * @see de.fzj.unicore.rcp.wfeditor.parts.ActivityPart#activate()
	 */
	@Override
	public void activate() {
		super.activate();
		getViewer().getEditDomain().getCommandStack()
				.addCommandStackListener(stackListener);

	}

	@Override
	protected void applyOwnLayout(CompoundDirectedGraph graph, Map map,
			CompoundCommand cmd) {
		super.applyOwnLayout(graph, map, cmd);
	}

	protected void applyOwnResults(CompoundDirectedGraph graph, Map map) {
	}

	/**
	 * @see de.fzj.unicore.rcp.wfeditor.parts.ActivityPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.NODE_ROLE, null);
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, null);
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, null);
		installEditPolicy(EditPolicy.COMPONENT_ROLE,
				new RootComponentEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE,
				new StructuredActivityLayoutEditPolicy());
		installEditPolicy(EditPolicy.CONTAINER_ROLE,
				new ActivityContainerEditPolicy());
		installEditPolicy(WFConstants.POLICY_PASTE, new PasteEditPolicy());
		installEditPolicy(WFConstants.REQ_REMOVE_ILLEGAL_TRANSITIONS,
				new RemoveIllegalTransitionsEditPolicy());
	}

	@Override
	protected IFigure createFigure() {
		FreeformLayout layout = new FreeformLayout();

		Figure f = new WorkflowDiagramFigure(this);

		f.setLayoutManager(layout);
		return f;
	}

	/**
	 * @see de.fzj.unicore.rcp.wfeditor.parts.ActivityPart#deactivate()
	 */
	@Override
	public void deactivate() {
		getViewer().getEditDomain().getCommandStack()
				.removeCommandStackListener(stackListener);
		super.deactivate();
	}

	@Override
	public Insets getInnerPadding() {
		return WFConstants.INNER_DIAGRAM_PADDING;
	}

	@Override
	public Insets getPadding() {
		return super.getPadding();
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#isSelectable()
	 */
	@Override
	public boolean isSelectable() {
		return false;
	}

	/**
	 * @see de.fzj.unicore.rcp.wfeditor.parts.StructuredActivityPart#refreshVisuals()
	 */
	@Override
	public void refreshVisuals() {
		super.refreshVisuals();

	}

	@Override
	public void setParent(EditPart parent) {
		super.setParent(parent);
	}
	
	protected void updateBounds()
	{
		// the size of the workflow diagram does not change
		// => do nothing
	}

}
