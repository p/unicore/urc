package de.fzj.unicore.rcp.wfeditor.ui;

import org.eclipse.gef.EditDomain;
import org.eclipse.gef.ui.parts.DomainEventDispatcher;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.swt.events.DisposeEvent;

public class WFGraphicalViewer extends ScrollingGraphicalViewer {

	private DomainEventDispatcher eventDispatcher;
	private WFEditor editor;

	public WFGraphicalViewer(WFEditor editor) {
		super();
		this.editor = editor;
	}

	public WFEditor getEditor() {
		return editor;
	}

	@Override
	protected DomainEventDispatcher getEventDispatcher() {
		return eventDispatcher;
	}

	@Override
	protected void handleDispose(DisposeEvent e) {
		super.handleDispose(e);
		getVisualPartMap().clear();
	}

	@Override
	public void setEditDomain(EditDomain domain) {
		super.setEditDomain(domain);
		// Set the new event dispatcher, even if the new domain is null. This
		// will dispose
		// the old event dispatcher.
		getLightweightSystem().setEventDispatcher(
				eventDispatcher = new WFEventDispatcher(domain, this));
	}
}
