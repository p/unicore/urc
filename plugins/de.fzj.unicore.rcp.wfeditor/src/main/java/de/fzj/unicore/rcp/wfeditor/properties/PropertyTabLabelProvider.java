/******************************************************************************
 * Copyright (c) 2004, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    IBM Corporation - initial API and implementation 
 ****************************************************************************/

package de.fzj.unicore.rcp.wfeditor.properties;

import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.jface.viewers.DecoratingLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;

/**
 * Label provider that delegates to the Icon Service and the Parser Service for
 * the images and text it provides.
 * 
 * @author ldamus
 * 
 */
public class PropertyTabLabelProvider extends DecoratingLabelProvider {

	/**
	 * A label provider which uses the icon and parser service to get the
	 * labels.
	 */
	private static class MyLabelProvider extends LabelProvider {

		private Image img = WFActivator.getImageDescriptor(
				"export_workflow.gif").createImage();

		@Override
		public void dispose() {
			super.dispose();
			img.dispose();
		}

		@Override
		public Image getImage(Object element) {

			return img;
		}

		@Override
		public String getText(Object element) {

			if ((element instanceof IStructuredSelection)) {
				IStructuredSelection ss = (IStructuredSelection) element;
				if (ss.size() == 1) {
					element = ss.getFirstElement();
				}
			}

			if (element instanceof GraphicalEditPart) {
				Object o = ((GraphicalEditPart) element).getModel();
				if (o instanceof IFlowElement) {
					return ((IFlowElement) o).getName();
				}
			}
			return null;
		}
	}

	public PropertyTabLabelProvider() {
		super(new MyLabelProvider(), PlatformUI.getWorkbench()
				.getDecoratorManager().getLabelDecorator());

	}

}
