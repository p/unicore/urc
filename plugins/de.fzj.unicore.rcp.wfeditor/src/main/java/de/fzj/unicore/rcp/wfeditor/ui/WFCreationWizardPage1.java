/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.ui;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.ide.undo.CreateFileOperation;
import org.eclipse.ui.ide.undo.WorkspaceUndoUtil;

import de.fzj.unicore.rcp.wfeditor.WFActivator;

/**
 * FlowWizardPage1
 * 
 * @author Daniel Lee
 */
public class WFCreationWizardPage1 extends WizardNewProjectCreationPage {

	private IWorkbench workbench;

	public WFCreationWizardPage1(IWorkbench aWorkbench) {
		super("sampleFlowPage1");
		this.setTitle("Create a workflow project");
		this.setDescription("Create a new workflow project resource");
		this.setImageDescriptor(ImageDescriptor.createFromFile(
				WFActivator.class, "images/flowbanner.gif"));
		this.workbench = aWorkbench;
	}

	@Override
	public void createControl(Composite parent) {
		super.createControl(parent);
		validatePage();
	}

	public void createProjectFile(final IFile newFile) {
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) {
				CreateFileOperation cfo = new CreateFileOperation(newFile,
						null, null, "New File");
				try {
					PlatformUI
							.getWorkbench()
							.getOperationSupport()
							.getOperationHistory()
							.execute(
									cfo,
									monitor,
									WorkspaceUndoUtil
											.getUIInfoAdapter(getShell()));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		try {
			getContainer().run(true, true, op);
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean finish() {

		IFile newFile = getProjectHandle().getFile(getProjectName() + ".flow");
		createProjectFile(newFile);

		// Since the file resource was created fine, open it for editing
		try {
			IWorkbenchWindow dwindow = workbench.getActiveWorkbenchWindow();
			IWorkbenchPage page = dwindow.getActivePage();
			if (page != null) {
				IDE.openEditor(page, newFile, true);
			}
		} catch (org.eclipse.ui.PartInitException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
