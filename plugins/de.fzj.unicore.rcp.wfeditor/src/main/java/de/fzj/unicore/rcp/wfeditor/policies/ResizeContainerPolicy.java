/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editpolicies.GraphicalEditPolicy;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.commands.AdjustLayoutCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.ResizeContainerCommand;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;

/**
 * @author demuth
 * 
 */
public class ResizeContainerPolicy extends GraphicalEditPolicy {

	protected boolean autoResizing = true;

	/**
	 * Returns <code>null</code> by default. <code>null</code> is used to
	 * indicate that the EditPolicy does not contribute to the specified
	 * <code>Request</code>.
	 * 
	 * @see org.eclipse.gef.EditPolicy#getCommand(Request)
	 */
	@Override
	public Command getCommand(Request request) {
		if (!WFConstants.REQ_AUTO_RESIZE_CONTAINER.equals(request.getType())
				|| !isAutoResizing()) {
			return null;
		}
		// Command result = new ResizeContainerCommand((StructuredActivity)
		// getHost().getModel());
		CompoundCommand result = new CompoundCommand();
		result.add(new ResizeContainerCommand((StructuredActivity) getHost()
				.getModel(), getHost().getViewer()));
		result.add(new AdjustLayoutCommand((IActivity) getHost().getModel(),
				getHost().getViewer()));
		return result;
	}

	public boolean isAutoResizing() {
		return autoResizing;
	}

	public void setAutoResizing(boolean autoResizing) {
		this.autoResizing = autoResizing;
	}

	@Override
	public boolean understandsRequest(Request request) {
		return WFConstants.REQ_AUTO_RESIZE_CONTAINER.equals(request.getType());
	}

}
