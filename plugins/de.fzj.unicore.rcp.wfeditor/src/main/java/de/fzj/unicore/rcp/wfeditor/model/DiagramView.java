package de.fzj.unicore.rcp.wfeditor.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Class for persisting all data that needs to restore the view of a workflow
 * diagram as it was when the diagram was closed
 * 
 * @author bdemuth
 * 
 */
@XStreamAlias("DiagramView")
public class DiagramView {
	private int xLocation, yLocation;
	private int horizontalMin, horizontalMax;
	private int verticalMin, verticalMax;
	private double zoomFactor;

	public DiagramView(int xLocation, int yLocation, int xLocationMin,
			int xLocationMax, int yLocationMin, int yLocationMax,
			double zoomFactor) {
		super();
		this.xLocation = xLocation;
		this.yLocation = yLocation;
		this.horizontalMin = xLocationMin;
		this.horizontalMax = xLocationMax;
		this.verticalMin = yLocationMin;
		verticalMax = yLocationMax;
		this.zoomFactor = zoomFactor;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DiagramView)) {
			return false;
		}
		DiagramView other = (DiagramView) obj;
		if (verticalMax != other.verticalMax) {
			return false;
		}
		if (xLocation != other.xLocation) {
			return false;
		}
		if (horizontalMax != other.horizontalMax) {
			return false;
		}
		if (horizontalMin != other.horizontalMin) {
			return false;
		}
		if (yLocation != other.yLocation) {
			return false;
		}
		if (verticalMin != other.verticalMin) {
			return false;
		}
		if (Double.doubleToLongBits(zoomFactor) != Double
				.doubleToLongBits(other.zoomFactor)) {
			return false;
		}
		return true;
	}

	public int getHorizontalMax() {
		return horizontalMax;
	}

	public int getHorizontalMin() {
		return horizontalMin;
	}

	public int getVerticalMax() {
		return verticalMax;
	}

	public int getVerticalMin() {
		return verticalMin;
	}

	public int getxLocation() {
		return xLocation;
	}

	public int getXLocation() {
		return xLocation;
	}

	public int getyLocation() {
		return yLocation;
	}

	public int getYLocation() {
		return yLocation;
	}

	public double getZoomFactor() {
		return zoomFactor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + verticalMax;
		result = prime * result + xLocation;
		result = prime * result + horizontalMax;
		result = prime * result + horizontalMin;
		result = prime * result + yLocation;
		result = prime * result + verticalMin;
		long temp;
		temp = Double.doubleToLongBits(zoomFactor);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	public void setHorizontalMax(int xLocationMax) {
		this.horizontalMax = xLocationMax;
	}

	public void setHorizontalMin(int xLocationMin) {
		this.horizontalMin = xLocationMin;
	}

	public void setVerticalMax(int yLocationMax) {
		verticalMax = yLocationMax;
	}

	public void setVerticalMin(int yLocationMin) {
		this.verticalMin = yLocationMin;
	}

	public void setxLocation(int xLocation) {
		this.xLocation = xLocation;
	}

	public void setXLocation(int xLocation) {
		this.xLocation = xLocation;
	}

	public void setyLocation(int yLocation) {
		this.yLocation = yLocation;
	}

	public void setYLocation(int yLocation) {
		this.yLocation = yLocation;
	}

	public void setZoomFactor(double zoomFactor) {
		this.zoomFactor = zoomFactor;
	}

}
