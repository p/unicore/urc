/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;

/**
 * SplitTransitionCommand
 * 
 * @author Daniel Lee
 */
public class SplitTransitionCommand extends Command {


	private Command createCommand;

	private IActivity oldSource, oldTarget;
	private int oldTargetIndex;
	private Transition transition;

	private IActivity activity;
	private Transition newOutgoingTransition;

	public SplitTransitionCommand(Command createCommand) {
		this.createCommand = createCommand;
	}

	@Override
	public boolean canExecute() {
		return activity != oldSource && activity != oldTarget
				&& createCommand.canExecute();
	}

	@Override
	public boolean canUndo() {
		return createCommand.canUndo();
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {

		newOutgoingTransition = new Transition(activity, oldTarget);
		oldTargetIndex = oldTarget.removeInput(transition);
		transition.target = activity;
		activity.addInput(transition);
		createCommand.execute();
	}

	/**
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {

		activity.undoDispose();
		oldTarget.addInput(newOutgoingTransition, oldTargetIndex);
		activity.addOutput(newOutgoingTransition);
		oldTarget.removeInput(transition);
		transition.target = activity;
		activity.addInput(transition);
		createCommand.redo();
	}

	/**
	 * Sets the Activity to be added.
	 * 
	 * @param activity
	 *            the new activity
	 */
	public void setActivity(IActivity activity) {
		this.activity = activity;
	}

	/**
	 * Sets the transition to be "split".
	 * 
	 * @param transition
	 *            the transition to be "split".
	 */
	public void setTransition(Transition transition) {
		this.transition = transition;
		oldSource = transition.source;
		oldTarget = transition.target;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {

		oldTarget.removeInput(newOutgoingTransition);
		activity.removeInput(transition);
		activity.removeOutput(newOutgoingTransition);
		activity.dispose();
		oldTarget.addInput(transition, oldTargetIndex);
		transition.target = oldTarget;
		createCommand.undo();
	}

}
