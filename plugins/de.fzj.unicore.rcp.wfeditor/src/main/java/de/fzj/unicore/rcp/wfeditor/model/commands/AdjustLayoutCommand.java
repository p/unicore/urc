/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.draw2d.graph.Subgraph;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.parts.ActivityPart;

/**
 * This command should be used when an element has been enlarged or added to the
 * graph. It tries to remove overlaps without changing the original layout too
 * much.
 * 
 * @author demuth
 * 
 */
public class AdjustLayoutCommand extends Command {

	private IActivity modifiedOrAdded;

	private EditPartViewer viewer;

	private CompoundCommand internalCommand;

	public AdjustLayoutCommand(IActivity modifiedOrAdded, EditPartViewer viewer) {
		this.modifiedOrAdded = modifiedOrAdded;
		this.viewer = viewer;

	}

	@Override
	public void execute() {
		internalCommand = new CompoundCommand();

		StructuredActivity parent = modifiedOrAdded.getParent();
		IActivity current = modifiedOrAdded;
		while (parent != null) {
			DisplacePeersCommand displaceCommand = new DisplacePeersCommand(
					current, viewer);
			internalCommand.add(displaceCommand);

			if (!(parent == current.getDiagram())) {
				ResizeContainerCommand resizeCmd = new ResizeContainerCommand(
						parent, viewer);
				internalCommand.add(resizeCmd);
				parent = parent.getParent();
				current = current.getParent();
			} else {
				
				parent = null;
			}
		}

		internalCommand.execute();
	}

	@Override
	public void redo() {
		internalCommand.redo();
	}

	@Override
	public void undo() {
		internalCommand.undo();
	}

	@SuppressWarnings("unchecked")
	public static Rectangle getBoundsForLayouting(IActivity act,
			EditPartViewer viewer) {
		Rectangle result = null;
		EditPart part = (EditPart) viewer.getEditPartRegistry().get(act);
		if (part instanceof ActivityPart) {
			ActivityPart activityPart = (ActivityPart) part;
			CompoundDirectedGraph graph = new CompoundDirectedGraph();
			graph.setDefaultPadding(WFConstants.BETWEEN_ACTIVITY_PADDING);
			graph.setMargin(WFConstants.ZERO_INSETS);
			Map<?, ?> partsToNodes = new HashMap<Object, Object>();
			Subgraph sub = new Subgraph("a");
			graph.nodes.add(sub);
			activityPart.contributeNodesToGraph(graph, sub, partsToNodes);
			Object o = partsToNodes.get(part);
			if (o instanceof Node) {
				Node n = (Node) o;
				result = new Rectangle(n.x, n.y, n.width, n.height);
			}

		}

		if (result == null) {
			result = new Rectangle(act.getLocation(), act.getSize());
		}
		return result;
	}

}
