package de.fzj.unicore.rcp.wfeditor.layouts;

import org.eclipse.draw2d.LayoutManager;

public interface IAutomaticGraphLayout extends LayoutManager {
	
	
	/**
	 * Flag indicating that inner paddings that have been modified by the user
	 * should be discarded.
	 */
	public int FLAG_RESET_INNER_PADDINGS = 1;
	
	/**
	 * Flag indicating that default behaviour should be used.
	 */
	public int FLAG_DEFAULT = FLAG_RESET_INNER_PADDINGS;

}
