package de.fzj.unicore.rcp.wfeditor.model.conditions;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;

@XStreamAlias("Breakpoint")
public class Breakpoint {

	private Condition condition;
	private IActivity activity;
	private boolean enabled = true;
	private boolean holdAllThreads = true;
	private int hitCount;

	public IActivity getActivity() {
		return activity;
	}

	public Condition getCondition() {
		return condition;
	}

	public int getHitCount() {
		return hitCount;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public boolean isHoldAllThreads() {
		return holdAllThreads;
	}

	public void setActivity(IActivity activity) {
		this.activity = activity;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setHitCount(int hitCount) {
		this.hitCount = hitCount;
	}

	public void setHoldAllThreads(boolean holdAllThreads) {
		this.holdAllThreads = holdAllThreads;
	}
}
