package de.fzj.unicore.rcp.wfeditor.layouts;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.draw2d.AbstractRouter;
import org.eclipse.draw2d.Bendpoint;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.FigureListener;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LayoutListener;
import org.eclipse.draw2d.ShortestPathConnectionRouter;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.PrecisionPoint;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.Path;
import org.eclipse.draw2d.graph.ShortestPathRouter;

/**
 * This class does shortest path routing. It is very similar to the
 * {@link ShortestPathConnectionRouter}. However, since the logic of the
 * {@link ShortestPathConnectionRouter} cannot easily be modified by subclasses,
 * this is a re-write. The main difference is that this router adds artificial
 * obstacles along the edges of the container in order to avoid connections that
 * overlap their parent container.
 * 
 * @author bdemuth
 * 
 */
public class WFConnectionRouter extends AbstractRouter {

	private class LayoutTracker extends LayoutListener.Stub {
		@Override
		public void postLayout(IFigure container) {
			processLayout();
		}

		@Override
		public void remove(IFigure child) {
			removeChild(child);
		}

		@Override
		public void setConstraint(IFigure child, Object constraint) {
			addChild(child);
		}
	}

	private Map<Connection,Object> constraintMap = new HashMap<Connection, Object>();
	private Map<IFigure,Rectangle> figuresToBounds;
	private Map<Connection,Path> connectionToPaths;
	protected boolean isDirty;
	protected ShortestPathRouter algorithm = new ShortestPathRouter();
	private IFigure container;
	private Set<Connection> staleConnections = new HashSet<Connection>();
	private LayoutListener listener = new LayoutTracker();


	private FigureListener figureListener = new FigureListener() {
		public void figureMoved(IFigure source) {
			Rectangle newBounds = source.getBounds().getCopy();
			if (algorithm.updateObstacle(
					(Rectangle) figuresToBounds.get(source), newBounds)) {
				queueSomeRouting();
				isDirty = true;
			}

			figuresToBounds.put(source, newBounds);
		}
	};
	private boolean ignoreInvalidate;

	/**
	 * Creates a new shortest path router with the given container. The
	 * container contains all the figure's which will be treated as obstacles
	 * for the connections to avoid. Any time a child of the container moves,
	 * one or more connections will be revalidated to process the new obstacle
	 * locations. The connections being routed must not be contained within the
	 * container.
	 * 
	 * @param container
	 *            the container
	 */
	public WFConnectionRouter(IFigure container) {
		isDirty = false;
		algorithm = new ShortestPathRouter();
		this.container = container;
		setSpacing(20);
	}

	protected void addChild(IFigure child) {
		if (connectionToPaths == null) {
			return;
		}
		if (figuresToBounds.containsKey(child)) {
			return;
		}
		Rectangle bounds = child.getBounds().getCopy();
		algorithm.addObstacle(bounds);
		figuresToBounds.put(child, bounds);
		child.addFigureListener(figureListener);
		isDirty = true;
	}

	/**
	 * Returns true if the given connection is routed by this router, false
	 * otherwise
	 * 
	 * @param conn
	 *            Connection whose router is questioned
	 * @return true if this is the router used for conn
	 * @since 3.5
	 */
	public boolean containsConnection(Connection conn) {
		return connectionToPaths != null && connectionToPaths.containsKey(conn);
	}

	/**
	 * Gets the constraint for the given {@link Connection}. The constraint is
	 * the paths list of bend points for this connection.
	 * 
	 * @param connection
	 *            The connection whose constraint we are retrieving
	 * @return The constraint
	 */
	@Override
	public Object getConstraint(Connection connection) {
		return constraintMap.get(connection);
	}

	/**
	 * @return the container which contains connections routed by this router
	 * @since 3.5
	 */
	public IFigure getContainer() {
		return container;
	}

	/**
	 * @return All connection paths after routing dirty paths. Some of the paths
	 *         that were not dirty may change as well, as a consequence of new
	 *         routings.
	 * @since 3.5
	 */
	@SuppressWarnings("unchecked")
	public List<Path> getPathsAfterRouting() {
		if (isDirty) {
			processStaleConnections();
			isDirty = false;
			List<Path> all = algorithm.solve();
			return all;

		}
		return null;
	}

	/**
	 * Returns the default spacing maintained on either side of a connection.
	 * The default value is 4.
	 * 
	 * @return the connection spacing
	 * @since 3.2
	 */
	public int getSpacing() {
		return algorithm.getSpacing();
	}

	/**
	 * @return true if there are connections routed by this router, false
	 *         otherwise
	 * @since 3.5
	 */
	public boolean hasMoreConnections() {
		return connectionToPaths != null && !connectionToPaths.isEmpty();
	}

	private void hookAll() {
		figuresToBounds = new HashMap<IFigure, Rectangle>();
		for (int i = 0; i < container.getChildren().size(); i++) {
			addChild((IFigure) container.getChildren().get(i));
			// attempt to force algorithm to keep some distance to the parent's
			// border
			// however, this may cause infinite revalidate() loops!!
			// (the parent bounds grow => connections are re-routed => the
			// parent bounds shrink
			// => connections are re-routed => the parent bounds grow ...
			// Rectangle r = container.getBounds();
			// parentTop = new Rectangle(r.x,r.y,r.width,1);
			// algorithm.addObstacle(parentTop);
			// parentLeft = new Rectangle(r.x,r.y+1,1,r.height-2);
			// algorithm.addObstacle(parentLeft);
			// parentRight = new Rectangle(r.x+r.width,r.y+1,1,r.height-2);
			// algorithm.addObstacle(parentRight);
			// parentBottom = new Rectangle(r.x,r.y+r.height,r.width,1);
			// algorithm.addObstacle(parentBottom);
		}

		container.addLayoutListener(listener);
	}

	/**
	 * @see ConnectionRouter#invalidate(Connection)
	 */
	@Override
	public void invalidate(Connection connection) {
		if (ignoreInvalidate) {
			return;
		}
		staleConnections.add(connection);
		isDirty = true;
	}

	/**
	 * Returns the value indicating if the router is dirty, i.e. if there are
	 * any outstanding connections that need to be routed
	 * 
	 * @return true if there are connections to be routed, false otherwise
	 * @since 3.5
	 */
	public boolean isDirty() {
		return isDirty;
	}

	private void processLayout() {
		if (staleConnections.isEmpty()) {
			return;
		}
		((Connection) staleConnections.iterator().next()).revalidate();
	}

	private void processStaleConnections() {
		Iterator<Connection> iter = staleConnections.iterator();
		if (iter.hasNext() && connectionToPaths == null) {
			connectionToPaths = new HashMap<Connection, Path>();
			hookAll();
		}

		while (iter.hasNext()) {
			Connection conn = (Connection) iter.next();

			Path path = (Path) connectionToPaths.get(conn);
			if (path == null) {
				path = new Path(conn);

				connectionToPaths.put(conn, path);
				algorithm.addPath(path);
			}

			List<?> constraint = (List<?>) getConstraint(conn);
			if (constraint == null) {
				constraint = Collections.EMPTY_LIST;
			}

			ConnectionAnchor sourceAnchor = conn.getSourceAnchor();
			ConnectionAnchor targetAnchor = conn.getTargetAnchor();
			Point start = null;
			Point end = null;
			if (sourceAnchor != null && targetAnchor != null) {
				start = sourceAnchor.getReferencePoint().getCopy();
				end = targetAnchor.getReferencePoint().getCopy();
			} else {
				start = new Point(0, 0);
				end = new Point(0, 0);
			}
			container.translateToRelative(start);
			container.translateToRelative(end);

			path.setStartPoint(start);
			path.setEndPoint(end);

			if (!constraint.isEmpty()) {
				PointList bends = new PointList(constraint.size());
				for (int i = 0; i < constraint.size(); i++) {
					Bendpoint bp = (Bendpoint) constraint.get(i);
					bends.addPoint(bp.getLocation());
				}
				path.setBendPoints(bends);
			} else {
				path.setBendPoints(null);
			}

			isDirty |= path.isDirty;
		}
		staleConnections.clear();
	}

	void queueSomeRouting() {
		if (connectionToPaths == null || connectionToPaths.isEmpty()) {
			return;
		}
		try {
			ignoreInvalidate = true;
			((Connection) connectionToPaths.keySet().iterator().next())
					.revalidate();
		} finally {
			ignoreInvalidate = false;
		}
	}

	/**
	 * @see ConnectionRouter#remove(Connection)
	 */
	@Override
	public void remove(Connection connection) {
		staleConnections.remove(connection);
		constraintMap.remove(connection);
		if (connectionToPaths == null) {
			return;
		}
		Path path = (Path) connectionToPaths.remove(connection);
		algorithm.removePath(path);
		isDirty = true;
		if (connectionToPaths.isEmpty()) {
			unhookAll();
			connectionToPaths = null;
		} else {
			// Make sure one of the remaining is revalidated so that we can
			// re-route again.
			queueSomeRouting();
		}
	}

	void removeChild(IFigure child) {
		if (connectionToPaths == null) {
			return;
		}
		Rectangle bounds = child.getBounds().getCopy();
		boolean change = algorithm.removeObstacle(bounds);
		figuresToBounds.remove(child);
		child.removeFigureListener(figureListener);
		if (change) {
			isDirty = true;
			queueSomeRouting();
		}
	}

	/**
	 * @see ConnectionRouter#route(Connection)
	 */
	@SuppressWarnings("unchecked")
	public void route(Connection conn) {
		if (isDirty) {
			ignoreInvalidate = true;
			processStaleConnections();

			// Rectangle r = container.getBounds();
			// Rectangle newParentTop = new Rectangle(r.x,r.y,r.width,1);
			// algorithm.updateObstacle(parentTop, newParentTop);
			// parentTop = newParentTop;
			//
			// Rectangle newParentLeft = new Rectangle(r.x,r.y+1,1,r.height-2);
			// algorithm.updateObstacle(parentLeft, newParentLeft);
			// parentLeft = newParentLeft;
			//
			// Rectangle newParentRight = new
			// Rectangle(r.x+r.width,r.y+1,1,r.height-2);
			// algorithm.updateObstacle(parentRight, newParentRight);
			// parentRight = newParentRight;
			//
			// Rectangle newParentBottom = new
			// Rectangle(r.x,r.y+r.height,r.width,1);
			// algorithm.updateObstacle(parentBottom, newParentBottom);
			// parentBottom = newParentBottom;

			isDirty = false;
			List<Path> updated = algorithm.solve();
			Connection current;
			for (int i = 0; i < updated.size(); i++) {
				Path path = updated.get(i);
				current = (Connection) path.data;
				current.revalidate();

				PointList points = path.getPoints().getCopy();
				Point ref1, ref2, start, end;
				ref1 = new PrecisionPoint(points.getPoint(1));
				ref2 = new PrecisionPoint(points.getPoint(points.size() - 2));
				current.translateToAbsolute(ref1);
				current.translateToAbsolute(ref2);

				start = current.getSourceAnchor().getLocation(ref1).getCopy();
				end = current.getTargetAnchor().getLocation(ref2).getCopy();

				current.translateToRelative(start);
				current.translateToRelative(end);
				points.setPoint(start, 0);
				points.setPoint(end, points.size() - 1);

				current.setPoints(points);
			}
			ignoreInvalidate = false;
		}
	}

	/**
	 * @see ConnectionRouter#setConstraint(Connection, Object)
	 */
	@Override
	public void setConstraint(Connection connection, Object constraint) {
		// Connection.setConstraint() already calls revalidate, so we know that
		// a
		// route() call will follow.
		staleConnections.add(connection);
		constraintMap.put(connection, constraint);
		isDirty = true;
	}

	/**
	 * Sets the value indicating if connection invalidation should be ignored.
	 * 
	 * @param b
	 *            true if invalidation should be skipped, false otherwise
	 * @since 3.5
	 */
	public void setIgnoreInvalidate(boolean b) {
		ignoreInvalidate = b;
	}

	/**
	 * Sets the default space that should be maintained on either side of a
	 * connection. This causes the connections to be separated from each other
	 * and from the obstacles. The default value is 20.
	 * 
	 * @param spacing
	 *            the connection spacing
	 * @since 3.2
	 */
	public void setSpacing(int spacing) {
		algorithm.setSpacing(spacing);
	}

	/**
	 * Returns the value indicating if connection invalidation should be
	 * ignored.
	 * 
	 * @return true if invalidation should be skipped, false otherwise
	 * @since 3.5
	 */
	public boolean shouldIgnoreInvalidate() {
		return ignoreInvalidate;
	}

	private void unhookAll() {
		container.removeLayoutListener(listener);
		if (figuresToBounds != null) {
			Iterator<IFigure> figureItr = figuresToBounds.keySet().iterator();
			while (figureItr.hasNext()) {
				// Must use iterator's remove to avoid concurrent modification
				IFigure child = figureItr.next();
				figureItr.remove();
				removeChild(child);
			}
			figuresToBounds = null;
		}
	}

}
