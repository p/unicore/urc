package de.fzj.unicore.rcp.wfeditor.parts;

import de.fzj.unicore.rcp.wfeditor.figures.DataSinkFigure;
import de.fzj.unicore.rcp.wfeditor.model.variables.VariableModifierDataSink;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;

public class VariableModifierDataSinkPart extends DataSinkPart {

	@Override
	public VariableModifierDataSink getModel() {
		return (VariableModifierDataSink) super.getModel();
	}

	@Override
	protected void refreshVisuals() {

		if (!(getFigure() instanceof DataSinkFigure)) {
			return;
		}
		DataSinkFigure figure = (DataSinkFigure) getFigure();

		WorkflowVariable var = getModel().getModifiedVariable();

		if (var == null) {
			figure.setToolTipText(new String[] { "Drag & drop a workflow variable data source here." });
			figure.setDataTypeSymbol("?");
		} else {
			String varName = var.getName();
			String typeName = var.getType();
			String symbol = WorkflowVariable.getSymbolFromTypeName(typeName);
			figure.setToolTipText(new String[] {
					"Selected variable: " + varName, "Type: " + typeName });
			figure.setDataTypeSymbol(symbol);
		}
		super.refreshVisuals();
	}

}
