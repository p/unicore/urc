package de.fzj.unicore.rcp.wfeditor.model.variables;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.extensions.VariableToModifierDataFlowAssembler;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;

/**
 * Class that can be used to connect a {@link WorkflowVariable} with a
 * {@link WorkflowVariableModifier} via drag & drop. Used in conjunction with a
 * {@link VariableToModifierDataFlowAssembler}
 * 
 * @author bdemuth
 * 
 */
@XStreamAlias("VariableModifierDataSink")
public class VariableModifierDataSink extends DataSink implements
		PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2550196010026905878L;

	public static final String PROP_VARIABLE_MODIFIER = "variable modifier";

	public static final QName TYPE = new QName("http://www.unicore.eu/",
			"VariableModifierDataSink");

	public VariableModifierDataSink(WorkflowVariableModifier modifier) {
		super(modifier.getFlowElement());
		Set<String> dataTypes = new HashSet<String>();
		dataTypes.addAll(Arrays.asList(WorkflowVariable.VARIABLE_TYPES));
		setPropertyValue(PROP_ACCEPTED_DATA_TYPES, dataTypes);
		setModifier(modifier);
		modifier.addPersistentPropertyChangeListener(this);
	}

	public WorkflowVariable getModifiedVariable() {
		return getModifier().getVariable();

	}

	public WorkflowVariableModifier getModifier() {
		return (WorkflowVariableModifier) getPropertyValue(PROP_VARIABLE_MODIFIER);
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_VARIABLE_MODIFIER);
		return propertiesToCopy;
	}

	@Override
	public QName getSinkType() {
		return TYPE;
	}

	public boolean isVisible() {
		IFlowElement element = getFlowElement();
		if (element == null) {
			return false;
		}
		if (!getModifier().isDataSourceAndSinkVisible()) {
			return false;
		}
		ShowDataFlowsProperty prop = (ShowDataFlowsProperty) element
				.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		if (prop == null || !prop.isShowingSinks()) {
			return false;
		}

		WorkflowVariable var = getModifiedVariable();
		if (var == null) {
			return true;
		} else {
			return prop.isShowingFlowsWithDataType(var.getType());
		}
	}

	public void propertyChange(PropertyChangeEvent evt) {

		firePropertyChange(PROP_VARIABLE_MODIFIER, getModifier(), getModifier());
	}

	protected void setModifier(WorkflowVariableModifier modifier) {
		setPropertyValue(PROP_VARIABLE_MODIFIER, modifier);
	}
}
