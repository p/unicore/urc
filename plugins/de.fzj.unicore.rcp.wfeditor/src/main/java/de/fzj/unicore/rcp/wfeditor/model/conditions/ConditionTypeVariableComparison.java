/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.conditions;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.extensions.ConditionTypeVariableComparisonExtension;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.IPropertySourceWithListeners;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

/**
 * @author demuth
 * 
 */
@XStreamAlias("ConditionTypeVariableComparison")
public class ConditionTypeVariableComparison implements IConditionType,
		PropertyChangeListener {

	public static final String TYPE_NAME = "Variable Value";

	private int comparator = INT_EQUAL;
	/**
	 * The value to which the variable value is compared when evaluating the
	 * condition
	 */
	private String value = "0";
	private WorkflowVariable variable;
	private transient WorkflowVariable disposedVariable;
	private Condition condition;
	private transient boolean disposed = false;

	public ConditionTypeVariableComparison() {

	}

	@Override
	public ConditionTypeVariableComparison clone()
			throws CloneNotSupportedException {
		ConditionTypeVariableComparison clone = (ConditionTypeVariableComparison) super
				.clone();
		clone.disposedVariable = null;
		return clone;
	}

	public void dispose() {
		disposed = true;
		// perform some cleanup
		// must be called when this is not needed anymore!
		if (getVariable() != null) {
			getVariable().removePersistentPropertyChangeListener(this);
		}
		if (disposedVariable != null) {
			disposedVariable.removePersistentPropertyChangeListener(this);
		}
	}

	public void finalDispose(IProgressMonitor progress) {
		// do nothing
	}

	public int getComparator() {
		return comparator;
	}

	public Condition getCondition() {
		return condition;
	}

	public IConditionType getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		ConditionTypeVariableComparison result;
		try {
			result = (ConditionTypeVariableComparison) super.clone();
		} catch (CloneNotSupportedException e) {
			WFActivator
					.log(IStatus.ERROR,
							"Unable to copy condition of type 'variable comparison'",
							e);
			return null;
		}
		Condition newCondition = CloneUtils.getFromMapOrCopy(condition,
				toBeCopied, mapOfCopies, copyFlags);
		result.setCondition(newCondition);
		if (variable != null
				&& CloneUtils.isBeingCopied(variable.getFlowElement(),
						toBeCopied)) {
			result.variable = CloneUtils.getFromMapOrCopy(variable, toBeCopied,
					mapOfCopies, copyFlags);
		} else {
			result.variable = variable;
		}
		result.disposedVariable = null;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType#
	 * getDisplayedExpression()
	 */
	public String getDisplayedExpression() {
		String variableName = "?";
		if (getVariable() != null) {
			variableName = getVariable().getName();
		}
		return variableName + " " + COMPARATORS[comparator] + " " + value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType#getExtensionId
	 * ()
	 */
	public String getExtensionId() {
		return ConditionTypeVariableComparisonExtension.ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType#
	 * getInternalExpression()
	 */
	public String getInternalExpression() throws Exception {
		if (getVariable() == null) {
			throw new Exception("Invalid condition " + getDisplayedExpression()
					+ ". Please set a workflow variable for comparison.");
		}
		if (WorkflowVariable.VARIABLE_TYPE_INTEGER.equals(getVariable()
				.getType()) || WorkflowVariable.VARIABLE_TYPE_FLOAT.equals(getVariable()
						.getType())) {
			return "eval(" + getVariable().getName() + COMPARATORS[comparator]
					+ value + ")";
		} else if (WorkflowVariable.VARIABLE_TYPE_STRING.equals(getVariable()
				.getType())) {
			if (comparator == INT_EQUAL) {
				return "eval(" + getVariable().getName() + ".equals(\""
						+ value.trim() + "\"))";
			} else if (comparator == INT_UNEQUAL) {
				return "eval(!" + getVariable().getName() + ".equals(\""
						+ value.trim() + "\"))";
			} else {
				throw new Exception("Invalid comparator for variable type "
						+ WorkflowVariable.VARIABLE_TYPE_STRING + ": "
						+ COMPARATORS[comparator]);
			}
		} else {
			throw new Exception("Invalid condition: Unknown variable type "
					+ getVariable().getType());
		}
	}

	public String getTypeName() {
		return TYPE_NAME;
	}

	public String getValue() {
		return value;
	}

	public WorkflowVariable getVariable() {
		return variable;
	}

	public Set<Class<?>> insertAfter() {
		return new HashSet<Class<?>>(); // we don't care
	}

	public Set<Class<?>> insertBefore() {
		return new HashSet<Class<?>>(); // we don't care
	}

	public void insertCopyIntoDiagram(ICopyable copy,
			StructuredActivity parent, Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		ConditionTypeVariableComparison result = (ConditionTypeVariableComparison) copy;
		if (result.variable != null) {
			result.variable.addPersistentPropertyChangeListener(result);
		}

	}

	public boolean isDisposable() {
		return true;
	}

	public boolean isDisposed() {
		return disposed;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		WorkflowVariable var = (WorkflowVariable) evt.getSource();
		if (WorkflowVariable.PROP_NAME.equals(evt.getPropertyName())) {
			updateCondition();
		} else if (IPropertySourceWithListeners.PROP_STATE.equals(evt
				.getPropertyName())) {
			if (var.isDisposed()) {
				// target variable has been deleted
				disposedVariable = variable;
				variable = null;
				updateCondition();
			} else if (disposedVariable != null && disposedVariable.isActive()) {
				// deletion has been undone
				variable = disposedVariable;
				disposedVariable = null;
				updateCondition();
			}

		}

	}

	private Object readResolve() {
		return this;
	}

	public void setComparator(int comparator) {
		this.comparator = comparator;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setVariable(WorkflowVariable variable) {
		if (this.variable != null) {
			this.variable.removePersistentPropertyChangeListener(this);
		}
		this.variable = variable;
		if (disposedVariable != null) {
			disposedVariable.removePersistentPropertyChangeListener(this);
		}
		disposedVariable = null;
		if (this.variable != null) {
			this.variable.addPersistentPropertyChangeListener(this);
		}
	}

	@Override
	public String toString() {
		return getDisplayedExpression();
	}

	public void undoDispose() {
		disposed = false;
		if (getVariable() != null) {
			getVariable().addPersistentPropertyChangeListener(this);
		}
		if (disposedVariable != null) {
			disposedVariable.addPersistentPropertyChangeListener(this);
		}

	}

	private void updateCondition() {
		Condition condition = getCondition();
		IPropertySourceWithListeners el = condition.getConditionalElement();
		el.setPropertyValue(Condition.PROP_CONDITION, condition);
	}
}
