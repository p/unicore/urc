package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.extensionpoints.IDataFlowAssembler;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;

/**
 * Abstract template class that must be used by {@link IDataFlowAssembler}
 * implementations for creating data flows and undoing the creation.
 * 
 * @author bdemuth
 * 
 */
public abstract class DataFlowAssemblyCommand extends Command {

	private IDataFlow dataFlow;

	protected abstract IDataFlow assembleDataFlow();

	@Override
	public void execute() {
		dataFlow = assembleDataFlow();
	}

	public IDataFlow getDataFlow() {
		return dataFlow;
	}

	@Override
	public void redo() {
		if (dataFlow != null) {
			dataFlow.reconnect();
		}
	}

	@Override
	public void undo() {
		if (dataFlow != null) {
			dataFlow.disconnect();
		}
	}
}
