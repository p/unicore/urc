package de.fzj.unicore.rcp.wfeditor.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.extensions.VariableToModifierDataFlowAssembler;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.conditions.CollectVariablesVisitor;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifier;
import de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser;
import de.fzj.unicore.rcp.wfeditor.traversal.PredecessorTraverser;

public class WorkflowVariableUtils {

	public static final String NO_VARIABLE_SELECTED = "None";

	public static List<WorkflowVariable> collectWorkflowVariables(
			IFlowElement element) {

		WorkflowDiagram diagram = element.getDiagram();

		IGraphTraverser traverser = new PredecessorTraverser();
		CollectVariablesVisitor visitor = new CollectVariablesVisitor();

		try {
			traverser.traverseGraph(diagram, element, visitor);
		} catch (Exception e) {
			WFActivator
					.log(IStatus.ERROR,
							"Unexpected exception while searching for workflow variables",
							e);
		}
		return visitor.getFound();
	}

	public static String[] extractVariableNames(
			Collection<WorkflowVariable> variables) {
		String[] names = new String[variables.size() + 1];
		names[0] = NO_VARIABLE_SELECTED;
		int i = 1;
		for (WorkflowVariable variable : variables) {
			names[i++] = variable.getName();
		}
		if (variables.size() > 0) {
			Arrays.sort(names, 1, variables.size());
		}

		return names;
	}

	public static void linkModifierToVariable(
			WorkflowVariableModifier modifier, String variableName) {
		if (variableName == null) {
			// unlink the modifier from its variable
			IDataSink sink = modifier.getDataSink();
			if (sink.getIncomingFlows() != null
					&& sink.getIncomingFlows().length > 0) {
				sink.getIncomingFlows()[0].disconnect();
			}
			modifier.setVariableName(null);
		} else {
			WorkflowVariable var = modifier.getFlowElement().getDiagram()
					.getVariable(variableName);
			if (var == null) {
				WFActivator
						.log(IStatus.ERROR,
								"Failed attempt to link variable with name "
										+ variableName
										+ " to a variable modifier. The variable could not be found.");
			}
			try {
				Command cmd = new VariableToModifierDataFlowAssembler()
						.getAssemblyCommand(var, modifier.getDataSink());
				cmd.execute();
			} catch (Exception e) {
				WFActivator.log(IStatus.ERROR,
						"Failed attempt to link variable with name "
								+ variableName + " to a variable modifier.", e);
			}
		}

	}

}
