/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.commands.SubmitWorkflowCommand;
import de.fzj.unicore.rcp.wfeditor.ui.WFRootEditPart;

public class SubmitWorkflowPolicy extends AbstractEditPolicy {

	@Override
	public Command getCommand(Request request) {
		if (REQ_OPEN.equals(request.getType())
				&& getHost().getModel() instanceof IFlowElement) {
			WorkflowDiagram diagram = ((IFlowElement) getHost().getModel())
					.getDiagram();
			((WFRootEditPart) getHost().getRoot()).saveViewInfoForDiagram();
			return new SubmitWorkflowCommand(diagram);
		}
		return null;
	}

}
