package de.fzj.unicore.rcp.wfeditor.model.serialization;

import org.eclipse.core.runtime.IStatus;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.converters.reflection.SerializableConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.mapper.Mapper;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;

public class WFDiagramConverter extends SerializableConverter {

	public WFDiagramConverter(Mapper mapper,
			ReflectionProvider reflectionProvider) {
		super(mapper, reflectionProvider);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class clazz) {
		return WorkflowDiagram.class.isAssignableFrom(clazz);
	}

	@Override
	public void marshal(Object original, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		WorkflowDiagram diagram = (WorkflowDiagram) original;
		writer.addAttribute("modelVersion", diagram.modelVersion);
		super.marshal(original, writer, context);
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader,
			UnmarshallingContext context) {
		String modelVersion = reader.getAttribute("modelVersion");
		if (modelVersion == null) {
			modelVersion = "6.1.4";
			WFActivator.log(IStatus.WARNING,
					"Unable to read model version of workflow diagram, assuming version "
							+ modelVersion);
		}

		context.put(WFConstants.XSTREAM_MODEL_VERSION, modelVersion);
		WorkflowDiagram diagram = (WorkflowDiagram) super.unmarshal(reader,
				context);
		diagram.modelVersion = modelVersion;
		return diagram;
	}

}
