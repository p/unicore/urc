package de.fzj.unicore.rcp.wfeditor.model.files;

import java.util.Comparator;

public class WorkflowFileComparator implements
Comparator<WorkflowFile> {

public WorkflowFileComparator() {

}

/*
* (non-Javadoc)
* 
* @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
*/
public int compare(WorkflowFile o1, WorkflowFile o2) {

String s1 = o1.getFullDisplayedName();
String s2 = o2.getFullDisplayedName();
return s1.compareTo(s2);
}

}
