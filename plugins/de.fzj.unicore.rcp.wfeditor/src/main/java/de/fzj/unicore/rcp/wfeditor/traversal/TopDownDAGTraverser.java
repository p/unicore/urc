/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.traversal;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;

/**
 * 
 * Traverses all top level activities (nodes) of a subgraph. Does so by walking
 * through the topological levels of the subgraph one by one. If the subgraph
 * contains cycles, no node that is part of a cycle will be traversed!
 * 
 * @author demuth
 * 
 */
public class TopDownDAGTraverser implements IGraphTraverser {

	protected int calculateInDegree(IActivity activity) {
		int inDegree = 0;
		for (Transition t : activity.getIncomingTransitions()) {
			if (t != null && !t.isClosingLoop()) {
				inDegree++; // don't count loop closing transitions!
			}
		}
		return inDegree;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser#traverseGraph(de
	 * .fzj.unicore.rcp.wfeditor.model.IFlowElement,
	 * de.fzj.unicore.rcp.wfeditor.traversal.IElementVisitor)
	 */
	public void traverseGraph(StructuredActivity subgraph,
			IFlowElement startingElement, IElementVisitor visitor)
			throws Exception {

		Map<String, Integer> inDegrees = new HashMap<String, Integer>();
		Map<String, IActivity> activities = new HashMap<String, IActivity>();

		List<IActivity> activityList = subgraph.getChildren();
		for (IActivity activity : activityList) {
			activities.put(activity.getID(), activity);
		}
		if (activities.size() == 0) {
			return;
		}
		Queue<String> q = new ArrayBlockingQueue<String>(activities.size());

		// store the in-degrees of all activities (nodes)
		// start traversing the nodes with in-degree 0
		for (IActivity activity : activities.values()) {
			int inDegree = calculateInDegree(activity);
			inDegrees.put(activity.getID(), inDegree);
			if (inDegree == 0) {
				q.add(activity.getID());
			}
		}

		while (!q.isEmpty() && !visitor.isDone()) {
			IActivity activity = activities.get(q.poll());
			visitor.visit(activity);
			updateNeighboursInDegrees(activity, inDegrees, q);
		}
	}

	public void updateNeighboursInDegrees(IActivity activity,
			Map<String, Integer> inDegrees, Queue<String> q) {
		List<Transition> transitions = activity.getOutgoingTransitions();
		for (Transition transition : transitions) {
			if (transition.isClosingLoop()) {
				continue;
			}
			String id = transition.target.getID();
			try {
				int inDegree = inDegrees.get(id);
				inDegree--;
				inDegrees.put(id, inDegree);
				// whenever a node reaches in-degree 0, it is ready for being
				// traversed (visited)
				if (inDegrees.get(id) == 0) {
					q.offer(id);
				}
			} catch (Exception e) {
				// do nothing
			}
		}
	}

}
