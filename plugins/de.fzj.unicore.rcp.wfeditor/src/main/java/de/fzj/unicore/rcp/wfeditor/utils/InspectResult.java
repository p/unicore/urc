package de.fzj.unicore.rcp.wfeditor.utils;

import org.eclipse.draw2d.geometry.Point;

public class InspectResult {

	public boolean parallel;

	/**
	 * 
	 * 
	 * @see LIES_BEFORE
	 * @see LIES_ON
	 * @see LIES_BEHIND
	 */
	public int orderOnThis;

	/**
	 * 
	 * @see LIES_BEFORE
	 * @see LIES_ON
	 * @see #LIES_BEHIND
	 */
	public int orderOnParam;

	public Point intersectionPoint;

	public InspectResult() {
	};
}
