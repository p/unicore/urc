package de.fzj.unicore.rcp.wfeditor.actions;

import java.util.List;

import org.eclipse.ui.IWorkbenchPart;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.parts.ActivityPart;

public class RemoveBreakpointAction extends WFEditorAction {

	public static String ID = "RemoveBreakpointAction";

	/**
	 * Constructor for Action1.
	 */
	public RemoveBreakpointAction(IWorkbenchPart part) {
		super(part);

		setId(ID);
		setText("Remove Breakpoint(s)");
		setToolTipText("Remove all breakpoints from the selected activities.");
	}

	@Override
	protected boolean calculateEnabled() {
		List<?> selection = getSelectedObjects();
		for (Object o : selection) {
			if (!(o instanceof ActivityPart)) {
				return false;
			}
			if (((ActivityPart) o).getModel().getPropertyValue(
					IFlowElement.PROP_BREAKPOINT) == null) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isAddedToContextMenu() {
		return true;
	}

	@Override
	public boolean isAddedToLocalToolbar() {
		return false;
	}

	@Override
	public void run() {
		List<?> selection = getSelectedObjects();
		for (Object o : selection) {
			((ActivityPart) o).getModel().setPropertyValue(
					IFlowElement.PROP_BREAKPOINT, null);
		}

	}

}
