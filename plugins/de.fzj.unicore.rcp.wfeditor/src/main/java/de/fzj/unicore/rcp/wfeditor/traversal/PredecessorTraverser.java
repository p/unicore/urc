/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.traversal;

import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;

/**
 * 
 * Traverses all activities from which there is a path of transitions to the
 * starting element. If these activities are structured activities, their
 * children are being visited as well.
 * 
 * @author demuth
 * 
 */
public class PredecessorTraverser implements IGraphTraverser {

	private Set<String> parents = new HashSet<String>();

	private boolean hasOutgoingTransitions(IActivity activity) {
		for (Transition t : activity.getOutgoingTransitions()) {
			if (!t.isClosingLoop()) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.traversal.IGraphTraverser#traverseGraph(de
	 * .fzj.unicore.rcp.wfeditor.model.IFlowElement,
	 * de.fzj.unicore.rcp.wfeditor.traversal.IElementVisitor)
	 */
	public void traverseGraph(StructuredActivity subgraph,
			IFlowElement startingElement, IElementVisitor visitor)
			throws Exception {
		Set<String> visited = new HashSet<String>();

		if (subgraph.containsCycles()) {
			return;
		}
		if (startingElement instanceof Transition) {
			startingElement = ((Transition) startingElement).source;
		}
		IActivity startAct = (IActivity) startingElement;
		StructuredActivity parent = startAct.getParent();
		while (parent != null) {
			parents.add(parent.getID());
			parent = parent.getParent();
		}

		Queue<IActivity> q = new ConcurrentLinkedQueue<IActivity>();
		q.offer(startAct);

		while (!q.isEmpty() && !visitor.isDone()) {
			IActivity activity = q.poll();
			List<Transition> incoming = activity.getIncomingTransitions();
			for (Transition t : incoming) {
				if (t.isClosingLoop()) {
					continue;
				}
				q.offer(t.source);
			}
			parent = activity.getParent();
			if (parent != null) {
				boolean reachedTopLevel = parent.equals(subgraph);
				if (!reachedTopLevel && !visited.contains(parent.getID())) {
					q.offer(parent);
				}
			}

			// visit children of structured activities, too
			// - skip them if they are a parent of the starting activity
			if (activity instanceof StructuredActivity
					&& !parents.contains(activity.getID())) {
				StructuredActivity structured = (StructuredActivity) activity;
				List<IActivity> children = structured.getChildren();
				for (IActivity child : children) {
					// start from bottom activities
					if (!hasOutgoingTransitions(child)) {
						q.offer(child);
					}
				}
			}
			if (!visited.contains(activity.getID())) {
				visitor.visit(activity);
			}
			visited.add(activity.getID());
		}
		
		// visit the diagram as well..
		WorkflowDiagram diagram = startingElement.getDiagram();
		if(diagram != null) visitor.visit(diagram);
	}

}
