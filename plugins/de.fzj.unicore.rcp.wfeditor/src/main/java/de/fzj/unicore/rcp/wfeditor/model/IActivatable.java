package de.fzj.unicore.rcp.wfeditor.model;

public interface IActivatable {

	public void activate();

	public boolean isActive();
}
