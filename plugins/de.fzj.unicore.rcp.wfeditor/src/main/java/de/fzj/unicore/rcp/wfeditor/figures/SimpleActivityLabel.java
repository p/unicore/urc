/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * A label that is drawn in the label layer and may exceed it's activity figure,
 * but not the parent of it's activity figure
 * 
 * @author bdemuth
 * 
 */
public class SimpleActivityLabel extends Label {
	private IFigure activityFigure;

	public SimpleActivityLabel(IFigure activityFigure, String label) {
		super(label);
		this.activityFigure = activityFigure;
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		Rectangle bounds = new Rectangle(getBounds());
		if (activityFigure != null && activityFigure.getParent() != null) {
			bounds = bounds.intersect(activityFigure.getParent()
					.getClientArea());
	
			Rectangle originalClip = new Rectangle();
			graphics.getClip(originalClip);
			graphics.clipRect(bounds);
			super.paintFigure(graphics);
			graphics.clipRect(originalClip);
		} else {
			super.paintFigure(graphics);
		}

	}
	
	@Override
	protected Dimension calculateTextSize() {
		Dimension result = getTextUtilities().getTextExtents(getText(), getFont());
		result.width += 10;
		return result;
	}

}
