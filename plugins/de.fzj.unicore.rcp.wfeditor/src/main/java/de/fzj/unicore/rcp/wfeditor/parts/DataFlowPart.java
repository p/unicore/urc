/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.Polyline;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;
import org.eclipse.gef.editpolicies.ConnectionEndpointEditPolicy;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.figures.RoundedPolylineConnection;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.policies.DataFlowEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.ShowPropertyViewPolicy;
import de.fzj.unicore.rcp.wfeditor.requests.WFEditorRequest;
import de.fzj.unicore.rcp.wfeditor.ui.WFGraphicalViewer;
import de.fzj.unicore.rcp.wfeditor.ui.WFRootEditPart;
import de.fzj.unicore.rcp.wfeditor.utils.WFDragEditPartsTracker;

/**
 * @author hudsonr Created on Jul 16, 2003
 */
public class DataFlowPart extends AbstractConnectionEditPart implements
		IFlowElementPart {

	@Override
	public void activate() {
		super.activate();
		figure.setVisible(getModel().isVisible());

	}

	@Override
	protected void activateFigure() {
		IFigure dataLayer = getLayer(WFRootEditPart.DATA_FLOW_LAYER);

		IFigure child = getFigure();

		dataLayer.add(child);

	}

	public void afterDeserialization() {

	}

	public void afterSerialization() {

	}

	public void beforeSerialization() {

	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE,
				new ConnectionEndpointEditPolicy());
		installEditPolicy(EditPolicy.CONNECTION_ROLE, new DataFlowEditPolicy());
		installEditPolicy(WFConstants.POLICY_OPEN, new ShowPropertyViewPolicy());
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractConnectionEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		RoundedPolylineConnection result = new RoundedPolylineConnection();

		PolygonDecoration deco = new PolygonDecoration();
		result.setTargetDecoration(deco);

		// data flows can pass the borders of structured activities
		// => share a global connection router
		WorkflowDiagram diagram = getDataFlow().getDataSink().getFlowElement()
				.getDiagram();
		StructuredActivityPart part = (StructuredActivityPart) getViewer()
				.getEditPartRegistry().get(diagram);
		ConnectionRouter router = part
				.getConnectionRouter(WFRootEditPart.DATA_FLOW_LAYER);
		result.setConnectionRouter(router);
		return result;

	}

	@Override
	public void deactivate() {
		super.deactivate();
	}

	@Override
	protected void deactivateFigure() {
		IFigure dataLayer = getLayer(WFRootEditPart.DATA_FLOW_LAYER);
		IFigure child = getFigure();
		if (child != null && child.getParent() == dataLayer) {
			dataLayer.remove(child);
		}
		getConnectionFigure().setSourceAnchor(null);
		getConnectionFigure().setTargetAnchor(null);
	}

	@Override
	public Command getCommand(Request request) {
		Command cmd = null;
		// Disable editing completely if workflow diagram disallows editing
		WorkflowDiagram diagram = null;
		try {
			diagram = getModel().getDataSink().getFlowElement().getDiagram();
		} catch (NullPointerException e) {
			// do nothing
		}

		if (diagram == null
				|| diagram.isEditable()
				|| REQ_OPEN.equals(request.getType())
				|| REQ_DIRECT_EDIT.equals(request.getType())
				|| ((request instanceof WFEditorRequest) && ((WFEditorRequest) request)
						.isExecutable(diagram))) {
			cmd = super.getCommand(request);
		} else {
			cmd = null;
		}
		return cmd;

	}

	public IDataFlow getDataFlow() {
		return getModel();
	}

	@Override
	public DragTracker getDragTracker(Request request) {
		return new WFDragEditPartsTracker(this);
	}

	@Override
	public IDataFlow getModel() {
		return (IDataFlow) super.getModel();
	}

	@Override
	public WFGraphicalViewer getViewer() {
		try {
			return (WFGraphicalViewer) super.getViewer();
		} catch (NullPointerException e) {
			return null;
		}
	}

	@Override
	public void performRequest(Request request) {
		if (REQ_OPEN.equals(request.getType())) {
			Command cmd = getCommand(request);
			if (cmd != null && cmd.canExecute()) {
				cmd.execute();
			}
		}
	}

	/**
	 * @see org.eclipse.gef.EditPart#setSelected(int)
	 */
	@Override
	public void setSelected(int value) {
		super.setSelected(value);
		if (value != EditPart.SELECTED_NONE) {
			((Polyline) getFigure()).setLineWidth(2);
		} else {
			((Polyline) getFigure()).setLineWidth(1);
		}
	}

}
