package de.fzj.unicore.rcp.wfeditor.actions;

import java.util.List;

import org.eclipse.ui.IWorkbenchPart;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Breakpoint;
import de.fzj.unicore.rcp.wfeditor.parts.ActivityPart;

public class AddBreakpointAction extends WFEditorAction {

	public static String ID = "AddBreakpointAction";

	/**
	 * Constructor for Action1.
	 */
	public AddBreakpointAction(IWorkbenchPart part) {
		super(part);

		setImageDescriptor(WFActivator
				.getImageDescriptor("breakpoint_enabled.gif"));
		setId(ID);
		setText("Add Breakpoint(s)");
		setToolTipText("Add breakpoints to the selected activities that will suspend the workflow right before the activity gets executed.");
	}

	@Override
	protected boolean calculateEnabled() {
		List<?> selection = getSelectedObjects();
		for (Object o : selection) {
			if (!(o instanceof ActivityPart)) {
				return false;
			}
			if (((ActivityPart) o).getModel().getPropertyValue(
					IFlowElement.PROP_BREAKPOINT) != null) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isAddedToContextMenu() {
		return true;
	}

	@Override
	public boolean isAddedToLocalToolbar() {
		return false;
	}

	@Override
	public void run() {
		List<?> selection = getSelectedObjects();
		for (Object o : selection) {
			Breakpoint b = new Breakpoint();
			((ActivityPart) o).getModel().setPropertyValue(
					IFlowElement.PROP_BREAKPOINT, b);
		}

	}

}
