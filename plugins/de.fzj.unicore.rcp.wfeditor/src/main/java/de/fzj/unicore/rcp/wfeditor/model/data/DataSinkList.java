package de.fzj.unicore.rcp.wfeditor.model.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

@XStreamAlias("DataSinkList")
@SuppressWarnings("unchecked")
public class DataSinkList extends PropertySource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5746037266885983063L;

	/**
	 * The key for the data sinks property. The value is a list containing
	 * {@link IDataSink} objects
	 */
	public static final String PROP_DATA_SINKS = "data sinks";

	private transient Map<String, IDataSink> map = new HashMap<String, IDataSink>();

	private transient IDataSink[] disposedSinks = null;

	public DataSinkList() {
		activate();
	}

	public void addDataSink(IDataSink sink) {
		addDataSink(sink, -1);
	}

	public void addDataSink(IDataSink sink, int index) {

		if (getMap().containsKey(sink.getId())) {
			return; // was added before
		}
		List<IDataSink> sinks = (List<IDataSink>) getPropertyValue(PROP_DATA_SINKS);
		if (sinks == null) {
			sinks = new ArrayList<IDataSink>();
		}
		if (index < 0) {
			index = sinks.size() + index;
		}
		getMap().put(sink.getId(), sink);
		sinks.add(sink);
		sink.setParentSinkList(this);
		setPropertyValue(PROP_DATA_SINKS, sinks);
	}

	@Override
	protected void addPropertyDescriptors() {
		// TODO Auto-generated method stub

	}

	public void clear() {
		List<IDataSink> sinks = (List<IDataSink>) getPropertyValue(PROP_DATA_SINKS);
		if (sinks != null && sinks.size() > 0) {
			getMap().clear();
			sinks.clear();
			setPropertyValue(PROP_DATA_SINKS, sinks);
		}
	}

	@Override
	public void dispose() {
		super.dispose();
		disposedSinks = getDataSinks();
		for (IDataSink sink : disposedSinks) {
			if (!sink.isDisposed()) {
				sink.dispose();
			}
		}
	}

	@Override
	public DataSinkList getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		DataSinkList copy = (DataSinkList) super.getCopy(toBeCopied,
				mapOfCopies, copyFlags);
		copy.map = null;
		for (IDataSink sink : getDataSinks()) {
			IDataSink newSink = CloneUtils.getFromMapOrCopy(sink, toBeCopied,
					mapOfCopies, copyFlags);
			copy.addDataSink(newSink);
		}
		return copy;
	}

	public IDataSink getDataSinkById(String id) {
		return getMap().get(id);
	}

	public IDataSink[] getDataSinks() {
		List<IDataSink> sinks = (List<IDataSink>) getPropertyValue(PROP_DATA_SINKS);
		if (sinks == null) {
			return new IDataSink[0];
		} else {
			return sinks.toArray(new IDataSink[sinks.size()]);
		}
	}

	private Map<String, IDataSink> getMap() {
		if (map == null) {
			restoreMap();
		}
		return map;
	}

	public int indexOf(IDataSink sink) {
		List<IDataSink> sinks = (List<IDataSink>) getPropertyValue(PROP_DATA_SINKS);
		if (sinks == null) {
			return -1;
		}
		return sinks.indexOf(sink);
	}

	public int removeDataSink(IDataSink sink) {
		List<IDataSink> sinks = (List<IDataSink>) getPropertyValue(PROP_DATA_SINKS);
		int result = sinks == null ? -1 : sinks.indexOf(sink);
		if (result >= 0) {
			getMap().remove(sink.getId());
			sinks.remove(sink);
			setPropertyValue(PROP_DATA_SINKS, sinks);
		}
		return result;
	}

	public int removeDataSink(String id) {
		IDataSink sink = getDataSinkById(id);
		return removeDataSink(sink);
	}

	private void restoreMap() {
		map = new HashMap<String, IDataSink>();
		for (IDataSink sink : getDataSinks()) {
			map.put(sink.getId(), sink);
		}
	}

	public int size() {
		return getMap().size();
	}

	@Override
	public void undoDispose() {
		if (disposedSinks != null) {
			for (IDataSink sink : disposedSinks) {
				if (sink.isDisposed()) // do not undispose twice!
				{
					sink.undoDispose();
				}
			}
		}
		disposedSinks = null;
		super.undoDispose(); // this MUST be called after sinks are being
								// undisposed, otherwise the GridBeanActivity
								// will re-add sinks according to the
								// GridBeanModel
	}

	@Override
	public void updateToCurrentModelVersion(String oldVersion,
			String currentVersion) {
		if (updatedToCurrentModelVersion) {
			return; // don't do this twice!
		}
		super.updateToCurrentModelVersion(oldVersion, currentVersion);
		for (IDataSink sink : getDataSinks()) {
			sink.updateToCurrentModelVersion(oldVersion, currentVersion);
		}
	}
}
