package de.fzj.unicore.rcp.wfeditor.model.files;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.common.interfaces.ISerializable;
import de.fzj.unicore.rcp.wfeditor.model.IDisposableWithUndo;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.PropertySource;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

@XStreamAlias("WorkflowFileList")
public class WorkflowFileList extends PropertySource implements
		IDisposableWithUndo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2776399744065123566L;
	private List<WorkflowFile> files;

	public WorkflowFileList() {
		files = new ArrayList<WorkflowFile>();
	}

	@Override
	public void activate() {
		super.activate();
		for (WorkflowFile var : getFiles()) {
			var.activate();
		}
	}

	public void addFile(WorkflowFile f) {
		files.add(f);
		setPropertyValue(f.getId(), f);
	}

	public void addFiles(Collection<WorkflowFile> coll) {
		for (WorkflowFile f : coll) {
			addFile(f);
		}
	}

	@Override
	protected void addPropertyDescriptors() {

	}

	@Override
	public void afterDeserialization() {
		super.afterDeserialization();
		for (ISerializable current : getFiles()) {
			current.afterDeserialization();
		}
	}

	@Override
	public void afterSerialization() {
		super.afterSerialization();
		for (ISerializable current : getFiles()) {
			current.afterSerialization();
		}
	}

	@Override
	public void beforeSerialization() {
		super.beforeSerialization();
		for (ISerializable current : getFiles()) {
			current.beforeSerialization();
		}
	}

	@Override
	public void dispose() {
		for (WorkflowFile var : getFiles()) {
			var.dispose();
		}
	}

	@Override
	public WorkflowFileList getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags) {
		WorkflowFileList copy = (WorkflowFileList) super.getCopy(toBeCopied,
				mapOfCopies, copyFlags);
		copy.files = new ArrayList<WorkflowFile>();
		for (WorkflowFile file : getFiles()) {
			WorkflowFile newFile = CloneUtils.getFromMapOrCopy(file,
					toBeCopied, mapOfCopies, copyFlags);
			copy.addFile(newFile);
		}
		return copy;
	}

	public WorkflowFile getFileAtIndex(int index) {
		return files.get(index);
	}

	public WorkflowFile[] getFiles() {
		return files.toArray(new WorkflowFile[0]);
	}

	public void removeFile(WorkflowFile f) {
		files.remove(f);
		removeProperty(f.getId());
	}

	public WorkflowFile removeFileAtIndex(int index) {
		WorkflowFile f = files.remove(index);
		removeProperty(f.getId());
		return f;
	}

	public WorkflowFile[] removeFilesAtIndices(int[] indices) {
		WorkflowFile[] result = new WorkflowFile[indices.length];
		if (indices.length == 0) {
			return result;
		}
		Arrays.sort(indices);
		// walk through indices to be removed in descending order
		// this way no indices have to be adjusted
		for (int i = indices.length - 1; i >= 0; i--) {
			int index = indices[i];
			result[i] = removeFileAtIndex(index);
		}
		return result;
	}

	public void sort() {
		WorkflowFileComparator cmp = new WorkflowFileComparator();
		Collections.sort(files, cmp);
	}

	@Override
	public String toString() {
		return "";
	}

	@Override
	public void undoDispose() {
		for (WorkflowFile var : getFiles()) {
			var.undoDispose();
		}
	}
}
