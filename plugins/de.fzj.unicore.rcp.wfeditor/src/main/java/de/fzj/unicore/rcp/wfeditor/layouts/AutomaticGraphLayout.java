/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.layouts;

import java.util.List;

import org.eclipse.draw2d.AbstractLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.commands.ComputeAndApplyLayoutCommand;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;

public class AutomaticGraphLayout extends AbstractLayout implements IAutomaticGraphLayout{

	private StructuredActivity structuredActivity;
	private EditPartViewer viewer;
	private int flags = 0;


	public AutomaticGraphLayout(StructuredActivity structuredActivity,
			EditPartViewer viewer) {
		super();
		this.structuredActivity = structuredActivity;
		this.viewer = viewer;
	}


	@SuppressWarnings("unchecked")
	@Override
	protected Dimension calculatePreferredSize(IFigure container, int wHint,
			int hHint) {
		container.validate();
		List<IFigure> children = container.getChildren();
		Rectangle result = new Rectangle().setLocation(container
				.getClientArea().getLocation());
		for (int i = 0; i < children.size(); i++) {
			result.union(children.get(i).getBounds());
		}
		result.resize(container.getInsets().getWidth(), container.getInsets()
				.getHeight());
		return result.getSize();
	}


	public Command getApplyLayoutCommand() {
		ComputeAndApplyLayoutCommand result = new ComputeAndApplyLayoutCommand(structuredActivity, viewer);
		result.setFlags(flags);
		return result;
	}

	public void layout(IFigure container) {
		GraphAnimation.recordInitialState(container);
		if (GraphAnimation.playbackState(container)) {
			return;
		}

		Command cmd = getApplyLayoutCommand();
		structuredActivity.getDiagram().getCommandStack()
				.execute(cmd);
	}


	public void setFlags(int flags) {
		this.flags = flags;
	}
	

}
