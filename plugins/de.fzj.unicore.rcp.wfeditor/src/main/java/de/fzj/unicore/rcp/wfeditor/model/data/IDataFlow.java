package de.fzj.unicore.rcp.wfeditor.model.data;

import javax.xml.namespace.QName;

import de.fzj.unicore.rcp.common.interfaces.ISerializable;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;

public interface IDataFlow extends ICopyable, ISerializable {

	public static final QName TYPE = new QName("http://www.unicore.eu/",
			"DataFlow");

	/**
	 * Disconnect this data flow and remove it from the diagram.
	 */
	public void disconnect();

	public IDataSink getDataSink();

	public IDataSource getDataSource();

	public String getId();

	public QName getType();

	public boolean isConnected();

	public boolean isVisible();

	/**
	 * Undo the actions taken in disconnect
	 */
	public void reconnect();

}
