package de.fzj.unicore.rcp.wfeditor.exceptions;

/**
 * This class is merely used for identifying the situation where a workflow has
 * been found that has been created by a newer client than the current client.
 * 
 * @author bdemuth
 * 
 */
public class NewerWorkflowModelException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6986970327941571624L;

}
