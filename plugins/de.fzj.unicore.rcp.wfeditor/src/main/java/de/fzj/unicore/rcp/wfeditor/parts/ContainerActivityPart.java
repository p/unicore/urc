/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.resource.ImageDescriptor;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.figures.ActivityFigure;
import de.fzj.unicore.rcp.wfeditor.figures.ContainerActivityFigure;
import de.fzj.unicore.rcp.wfeditor.figures.ICollapsibleFigure;

/**
 * @author hudsonr
 */
public class ContainerActivityPart extends SimpleCollapsibleActivityPart {

	private IFigure collapsedFigure;

	protected IFigure createActualFigure() {
		return null;
	}

	@Override
	protected ICollapsibleFigure createFigure() {
		Point loc = getModel().getLocation();
		Dimension size = getModel().getSize();
		Rectangle r = new Rectangle(loc, size);
		ICollapsibleFigure fig = new ContainerActivityFigure(r);
		fig.setBounds(r);
		return fig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.parts.CollapsibleActivityPart#getCollapsedFigure
	 * ()
	 */
	@Override
	protected IFigure getCollapsedFigure() {
		if (collapsedFigure == null) {
			ImageDescriptor desc = WFActivator
					.getImageDescriptor("container.png");
			
			collapsedFigure = new ActivityFigure(this, desc);
//			 Image img = new Image(null,
//			 desc.getImageData().scaledTo(WFConstants.DEFAULT_SIMPLE_ACTIVITY_SIZE.width,
//			 WFConstants.DEFAULT_SIMPLE_ACTIVITY_SIZE.height));
//			 collapsedFigure = new ImageFigure(img);
		}
		return collapsedFigure;
	}

}
