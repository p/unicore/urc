/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPolicy;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.guicomponents.StringComboBoxCellEditor;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.figures.ActivityFigure;
import de.fzj.unicore.rcp.wfeditor.figures.ICollapsibleFigure;
import de.fzj.unicore.rcp.wfeditor.figures.WhileActivityFigure;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionState;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateDescriptor;
import de.fzj.unicore.rcp.wfeditor.model.structures.WhileActivity;
import de.fzj.unicore.rcp.wfeditor.policies.WhileActivityDirectEditPolicy;
import de.fzj.unicore.rcp.wfeditor.utils.IterationUtils;

/**
 * @author demuth
 * 
 */
public class WhileActivityPart extends SimpleCollapsibleActivityPart {

	protected ActivityFigure collapsedFigure = null;

	protected List<String> iterations = new ArrayList<String>();

	@Override
	public void activate() {
		super.activate();
		updateIterationIdVisuals();
		// need to listen to state changes in the while loop's body:
		// additional iterations will be set there and lead to updating
		// of the combo box for selecting iterations
		getModel().getBody().addPropertyChangeListener(this);

	}

	@Override
	protected boolean allowsDirectEdit(Point requestLoc) {
		Point p1 = requestLoc.getCopy();
		Point p2 = requestLoc.getCopy();
		IFigure label1 = getFigure().getIterationLabel();
		IFigure label2 = getFigure().getIterationLabelDescription();
		label1.translateToRelative(p1);
		label2.translateToRelative(p2);
		return label1.containsPoint(p1) || label2.containsPoint(p2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.parts.CollapsibleActivityPart#
	 * configureExpandedFigure(org.eclipse.draw2d.IFigure)
	 */
	@Override
	protected void configureExpandedFigure(ICollapsibleFigure collapsible) {
		collapsible.add(getFigure().getIterationLabelDescription());
		collapsible.add(getFigure().getIterationLabel());

	}

	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		removeEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE);
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new WhileActivityDirectEditPolicy());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.parts.CollapsibleActivityPart#createFigure()
	 */
	@Override
	protected WhileActivityFigure createFigure() {
		Point loc = getModel().getLocation();
		Dimension size = getModel().getSize();
		Rectangle r = new Rectangle(loc, size);
		WhileActivityFigure fig = new WhileActivityFigure(r);
		return fig;
	}

	@Override
	public void deactivate() {
		super.deactivate();
		getModel().getBody().removePropertyChangeListener(this);
	}

	private String fixIterationId(String iteration) {
		return IterationUtils.fixIterationId(getModel().getDiagram(),
				iteration);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.parts.CollapsibleActivityPart#getCollapsedFigure
	 * ()
	 */
	@Override
	protected ActivityFigure getCollapsedFigure() {
		if (collapsedFigure == null) {
			collapsedFigure = new ActivityFigure(this,
					WFActivator.getImageDescriptor("while.png"));
		}
		return collapsedFigure;
	}

	@Override
	public WhileActivityFigure getFigure() {
		return (WhileActivityFigure) super.getFigure();
	}

	

	public List<String> getIterations() {
		return iterations;
	}

	private String getIterationVisual(String iterationId, ExecutionState state) {
		String[] iterations = iterationId
		.split(Constants.ITERATION_ID_SEPERATOR);
		String iteration = iterations[iterations.length - 1];
		if(state != null)
		{
			// append execution state to items in combo box so user sees the state
			// at first glance
			String stateString = ExecutionStateConstants.getStringForState(state
					.getCipher());
			iteration += " (" + stateString + ")";
		}
		return iteration;
	}

	public WhileActivity getModel() {
		return (WhileActivity) super.getModel();
	}

	@Override
	public void performDirectEdit() {

		Label l = getFigure().getIterationLabel();
		String iteratonId = getModel().getBody()
		.getCurrentIterationId();
		ExecutionState state = getModel().getBody()
		.getCurrentExecutionState();
		String currentIteration = getIterationVisual(iteratonId, state);
		manager = new WhileActivityDirectEditManager(this,
				StringComboBoxCellEditor.class,
				new ActivityCellEditorLocator(l), l, getIterations(),
				currentIteration);

		manager.show();

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (getModel().getBody().equals(evt.getSource())) {

			if (IFlowElement.PROP_EXECUTION_STATE.equals(evt.getPropertyName())
					|| IFlowElement.PROP_CURRENT_ITERATION_ID.equals(evt
							.getPropertyName())) {
				PlatformUI.getWorkbench().getDisplay()
				.asyncExec(new Runnable() {
					public void run() {
						updateIterationIdVisuals();
					}
				});
			}

		} else {
			super.propertyChange(evt);
		}
	}

	public void setIterations(List<String> iterations) {
		this.iterations = iterations;
	}

	protected void updateIterationIdVisuals() {
		String childIteratonId = getModel().getBody()
		.getCurrentIterationId();
		String parentIterationId = getModel().getCurrentIterationId();
		parentIterationId = fixIterationId(parentIterationId);
		if(parentIterationId.equals(childIteratonId) || childIteratonId.endsWith(Constants.ITERATION_ID_SEPERATOR))
		{
			// this should not happen! however, it happened with empty loops
			// due to a server-side bug prior to 6.4
			getFigure().setSelectedIteration("");
			return;
		}

		List<String> iterationStrings = new ArrayList<String>();
		ExecutionStateDescriptor sd = getModel().getBody()
		.getExecutionStateDescriptor();
		synchronized (sd) {

			Map<String, ExecutionState> stateMap = sd.getStates();
			Iterator<String> it = stateMap.keySet().iterator();
			while (it.hasNext()) {
				String s = it.next();
				// only iterations within the current iteration of the parent
				// can be selected
				if (!s.startsWith(parentIterationId) || s.trim().length() == 0) {
					continue;
				}
				String iteration = getIterationVisual(s, stateMap.get(s));
				iterationStrings.add(iteration);
			}
			Collections.sort(iterationStrings);

			setIterations(iterationStrings);

			String[] iterations = childIteratonId
			.split(Constants.ITERATION_ID_SEPERATOR);
			String selectedIteration = iterations[iterations.length - 1];
			getFigure().setSelectedIteration(selectedIteration);
		}
	}

}
