/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.ui;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import de.fzj.unicore.rcp.common.utils.ValidityChangeEvent;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.conditions.Condition;
import de.fzj.unicore.rcp.wfeditor.model.conditions.IConditionType;

/**
 * @author demuth
 * 
 */
public class ConditionDialog extends Dialog implements IConditionChangeListener {

	private Condition result;
	private Condition temp;
	private IConditionTypeControlProducer controlProducer;

	
	private Shell sShell;
	private Map<Object, Object> mapOfCopies = new HashMap<Object, Object>();
	private Set<IFlowElement> toBeCopied = new HashSet<IFlowElement>();
	private int copyFlags = ICopyable.FLAG_NONE;
	int selectedTypeIndex = 0;

	private Control conditionTypeControl;


	public ConditionDialog(Shell parent, String title, Condition value) {
		super(parent);
		this.result = value;

		temp = value.getCopy(toBeCopied, mapOfCopies, copyFlags);
	}

	public void checkInputValid() {
		boolean valid = controlProducer != null
				&& controlProducer.isInputValid();
		getButton(IDialogConstants.OK_ID).setEnabled(valid);
	}

	@Override
	protected Control createButtonBar(Composite parent) {
		Control result = super.createButtonBar(parent);
		checkInputValid();
		return result;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		// create a composite with standard margins and spacing
		final Composite composite = (Composite) super.createDialogArea(parent);

		GridLayout layout = (GridLayout) composite.getLayout();
		layout.numColumns = 2;

		Label typeLabel = new Label(composite, SWT.NONE);
		typeLabel.setText("Condition type:");
		new Label(composite, SWT.NONE);

		Combo combo = new Combo(composite, SWT.READ_ONLY);
		GridData data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);

		combo.setLayoutData(data);
		combo.setItems(temp.getAvailableTypeNames());
		selectedTypeIndex = getSelectedTypeIndex(temp);
		combo.select(selectedTypeIndex);

		final ConditionDialog dialog = this;
		combo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int index = ((Combo) e.getSource()).getSelectionIndex();
				if (selectedTypeIndex != index) {
					setSelectedTypeIndex(temp, index);
					conditionTypeControl.dispose();
					if (controlProducer != null) {
						controlProducer.removeConditionChangeListener(dialog);
					}
					controlProducer = ConditionTypeControlFactory
							.getControlProducer(temp);
					controlProducer.addConditionChangeListener(dialog);
					conditionTypeControl = controlProducer.getControl(
							composite, temp);
					getShell().pack();
					checkInputValid();
				}
			}
		});
		controlProducer = ConditionTypeControlFactory.getControlProducer(temp);

		controlProducer.addConditionChangeListener(this);
		conditionTypeControl = controlProducer.getControl(composite, temp);

		return composite;
	}

	public boolean close() {
		super.close();
		sShell.close();
		sShell.dispose();
		return true;
	}

	public Condition getResult() {
		return result;
	}

	protected int getSelectedTypeIndex(Condition condition) {
		IConditionType type = condition.getSelectedType();
		if (type == null) {
			return 0;
		}
		List<IConditionType> types = condition.getAvailableTypes();
		for (int i = 0; i < types.size(); i++) {
			if (types.get(i).getTypeName().equals(type.getTypeName())) {
				return i;
			}
		}
		return 0;
	}

	@Override
	protected void okPressed() {
		if (!temp.equals(result)) {
			for (Object o : mapOfCopies.keySet()) {
				if (o instanceof ICopyable) {
					ICopyable copyable = (ICopyable) o;
					copyable.insertCopyIntoDiagram((ICopyable) mapOfCopies
							.get(o), result.getDefiningElement().getParent(),
							toBeCopied, mapOfCopies, copyFlags);
				}
			}
			// old condition value will be
			// replaced and must be disposed of
			result.dispose();
			result = temp;
		}
		super.okPressed();
	}

	public void setResult(Condition result) {
		this.result = result;
	}

	protected void setSelectedTypeIndex(Condition condition, int index) {
		List<IConditionType> types = condition.getAvailableTypes();
		IConditionType type = types.get(index);
		condition.setSelectedType(type);
		selectedTypeIndex = index;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.ui.IValidityChangeListener#validityChanged
	 * (boolean)
	 */
	public void validityChanged(ValidityChangeEvent e) {
		getButton(IDialogConstants.OK_ID).setEnabled(e.getNewValidity());

	}

	public void valueChanged(ValueChangeEvent e) {
		// ignore: we only look at the value when the OK button is pressed
	}

}
