package de.fzj.unicore.rcp.wfeditor.utils;

public class MimeTypeUtils {

	/**
	 * Create a symbol that represents a certain MIME type with few characters.
	 * 
	 * @param mime
	 * @return
	 */
	public static String toSymbol(String mime) {
		if ("text/plain".equals(mime)) {
			return "text"; // treat plain text differently, as .plain looks
							// stupid
		}
		String subtype = mime.split("/")[1];
		return "." + subtype;
	}
}
