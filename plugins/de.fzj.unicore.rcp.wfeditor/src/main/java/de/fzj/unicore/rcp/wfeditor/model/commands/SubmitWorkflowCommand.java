/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodePath;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.monitoring.MonitorerRegistry;
import de.fzj.unicore.rcp.wfeditor.submission.ISubmitter;
import de.fzj.unicore.rcp.wfeditor.submission.SubmitterRegistry;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;
import de.fzj.unicore.rcp.wfeditor.ui.WFRootEditPart;
import de.fzj.unicore.rcp.wfeditor.ui.WFSubmissionWizard;
import de.fzj.unicore.rcp.wfeditor.utils.WorkflowUtils;

/**
 * Command that wraps the logic for submitting workflows.
 * 
 * @author demuth
 */
public class SubmitWorkflowCommand extends Command {

	private class SubmissionRunnable implements IRunnableWithProgress {
		WFSubmissionWizard wizard;

		public SubmissionRunnable() {

		}

		public void run(final IProgressMonitor progress) {

			try {
				if (progress.isCanceled()) {
					return;
				}
				final Node node = wizard.getSelectedNode();
				final ISubmitter submitter = wizard.getSelectedSubmitter();
				if (node == null || submitter == null) {
					return;
				}

				NodePath address = null;
				try {
					IPath viewSource = WFRootEditPart
							.getViewInfoPathForDiagramPath(workflowDiagram
									.getAbsolutePath());
					String parent = getWorkflowDiagram().getParentFolder();
					if (parent == null) {
						parent = "";
					}
					IPath relativeOldPath = Path.fromPortableString(parent)
							.append(getWorkflowDiagram().getFilename());
					IPath oldPath = getWorkflowDiagram().getProject()
							.getLocation().append(relativeOldPath);
					IPath submittedPath = WorkflowUtils.getSubmittedPath();
					int i = 1;
					IFolder execFolder;
					do {
						IPath current = submittedPath.uptoSegment(i++);
						execFolder = getWorkflowDiagram().getProject()
								.getFolder(current);
						if (!execFolder.exists()) {
							execFolder.create(true, true,
									new SubProgressMonitor(progress,
											5 / submittedPath.segmentCount()));
						}
					} while (i <= submittedPath.segmentCount());

					getWorkflowDiagram().beforeSaveAs(execFolder.getLocation(),
							true, true, false, null);
					getWorkflowDiagram().doSaveAs(execFolder.getLocation(),
							true, true, false, null);
					getWorkflowDiagram().afterSaveAs(
							oldPath.removeLastSegments(1), true, true, false,
							null);

					final IFile copy = execFolder.getFile(getWorkflowDiagram()
							.getFilename());

					// try to use the same view on the submitted diagram that
					// the user had on the original diagram
					try {

						IPath viewTarget = WFRootEditPart
								.getViewInfoPathForDiagramPath(copy
										.getLocation());
						PathUtils.copy(viewSource, viewTarget, true);
					} catch (Exception e) {
						e.printStackTrace();
						// this is optional so stop whining if it fails
					}

					PlatformUI.getWorkbench().getDisplay()
							.syncExec(new Runnable() {

								public void run() {
									IWorkbenchWindow window = PlatformUI
											.getWorkbench()
											.getActiveWorkbenchWindow();
									IEditorInput input = new FileEditorInput(
											copy);
									IWorkbenchPage page = window
											.getActivePage();
									if (page != null) {
										IEditorDescriptor desc = PlatformUI
												.getWorkbench()
												.getEditorRegistry()
												.getDefaultEditor(
														getWorkflowDiagram()
																.getFilename());
										String editorId = "org.eclipse.ui.DefaultTextEditor";
										if (desc != null) {
											editorId = desc.getId();
										}
										try {
											submittedEditor = (WFEditor) page
													.openEditor(input, editorId);
										} catch (Exception e) {
											e.printStackTrace();
										}

									} else {

									}
								}

							});

					submittedEditor.getDiagram().beforeSubmission();

					if (progress.isCanceled()) {
						address = null;
					} else {
						try {
							address = submitter.submit(node,
									submittedEditor.getDiagram(),
									new SubProgressMonitor(progress, 85));
						} catch (Exception e) {
							WFActivator.log(
									IStatus.ERROR,
									"Submission failed: "
											+ e.getLocalizedMessage(), e);
							submittedEditor
									.getDiagram()
									.changeCurrentExecutionState(
											ExecutionStateConstants.STATE_FAILED,
											"submission failed");
							address = null;
						}
					}

					if (address == null || progress.isCanceled()) {
						PlatformUI.getWorkbench().getDisplay()
								.asyncExec(new Runnable() {
									public void run() {
										submittedEditor
												.getSite()
												.getPage()
												.closeEditor(submittedEditor,
														false);
									}
								});
						return;
					}

					submittedEditor.doSave(new SubProgressMonitor(progress, 5),
							copy);
					submittedEditor.getDiagram().getProject()
							.refreshLocal(5, null);
					submittedEditor.getDiagram().getExecutionData()
							.setMonitoringService(address);
					submittedEditor.getDiagram().afterSubmission();

				} catch (Exception e) {
					WFActivator.log(IStatus.WARNING,
							"Could not submit workflow", e);
					submittedEditor.getDiagram().changeCurrentExecutionState(
							ExecutionStateConstants.STATE_FAILED,
							"submission failed");
					return;
				}

				try {
					// start monitoring
					MonitorerRegistry monitorerRegistry = WFActivator
							.getDefault().getMonitorerRegistry();
					monitorerRegistry.monitor(submittedEditor.getDiagram(),
							address);

				} catch (Exception e) {
					WFActivator.log(IStatus.WARNING,
							"Could not monitor workflow", e);
					return;
				}

			} finally {
				PlatformUI.getWorkbench().getDisplay()
						.asyncExec(new Runnable() {
							public void run() {
								submissionOver();
							}
						});
			}
		}

		public void setWizard(WFSubmissionWizard wizard) {
			this.wizard = wizard;
		}

	}

	private WorkflowDiagram workflowDiagram;

	private WFEditor submittedEditor;

	public SubmitWorkflowCommand(WorkflowDiagram diagram) {
		this.workflowDiagram = diagram;
	}

	@Override
	public boolean canExecute() {
		return workflowDiagram != null && workflowDiagram.readyForSubmission();
	}

	@Override
	public void execute() {
		try {

			final SubmitterRegistry submitterRegistry = WFActivator
					.getDefault().getSubmitterRegistry();

			getWorkflowDiagram().setPropertyValue(
					WorkflowDiagram.PROP_READY_FOR_SUBMISSION, false);

			if (!submitterRegistry.canDealWith(getWorkflowDiagram())) {
				submissionOver();
				return;
			}
			Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getShell();
			if (shell == null) {
				shell = new Shell(SWT.RESIZE);
			}

			SubmissionRunnable runnable = new SubmissionRunnable();
			WFSubmissionWizard wizard = new WFSubmissionWizard(
					getWorkflowDiagram(), runnable);
			runnable.setWizard(wizard);

			WizardDialog dialog = new WizardDialog(shell, wizard);
			dialog.open();

		} catch (Exception e) {
			WFActivator.log(IStatus.WARNING, "Could not submit workflow", e);
			submissionOver();
		}

	}

	protected WorkflowDiagram getWorkflowDiagram() {
		return workflowDiagram;
	}

	protected void submissionOver() {

		getWorkflowDiagram().checkReadyForSubmission();
	}
}