/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ImageFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LayeredPane;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.parts.ActivityPart;
import de.fzj.unicore.rcp.wfeditor.ui.WFRootEditPart;

/**
 * @author demuth
 * 
 */
public class ActivityFigure extends Figure implements IActivityFigure {

	/**
	 * Since the figure of a graphical editPart cannot be changed dynamically,
	 * we use a panel as a container for the actual figure
	 */
	private IFigure actualFigure;

	private ImageDescriptor imageDescr;
	private ImageData imageData;

	private ImageDescriptor stateImageDescr;
	private Image breakpointImage;

	private Label labelFigure;


	private String label;

	private XYLayout layout;
	/**
	 * Small figure that is added over the actualFigure for visualizing the
	 * state of the activity.
	 */
	private ImageFigure stateFigure;

	/**
	 * Small figure that is added over the actualFigure for visualizing that a
	 * breakpoint is set on the activity.
	 */
	private ImageFigure breakpointFigure;

	private ImageFigure imageFigure;

	private ActivityPart activityPart;
	private IActivity activity;

	private boolean dirty = true;

	private ActivityFigure(ActivityPart activityPart, IActivity activity,
			ImageDescriptor imageDescr, Rectangle initialBounds) {
		this.activityPart = activityPart;
		this.activity = activity;

		actualFigure = new Panel();

		layout = new XYLayout();
		actualFigure.setLayoutManager(layout);
	
		setBounds(initialBounds);
		add(actualFigure);
		setImage(imageDescr);
	}

	public ActivityFigure(ActivityPart activityPart, ImageDescriptor imageDescr) {
		this(activityPart, null, imageDescr, new Rectangle(activityPart.getModel().getLocation(),activityPart.getModel().getSize()));

	}

	public ActivityFigure(IActivity activity, ImageDescriptor imageDescr) {
		this(null, activity, imageDescr,new Rectangle(activity.getLocation(),activity.getSize()));
	}

	public IFigure getToolTip()
	{
		return super.getToolTip();
	}

	@Override
	public void addNotify() {
		super.addNotify();

		buildActualFigure();
		if (labelFigure != null && getLabelLayer() != null) {
			getLabelLayer().add(labelFigure);
		}
	}

	public void buildActualFigure() {

		if (!isDirty()) {
			return;
		}
		
		try {
			if (getChildren().contains(actualFigure)) {
				remove(actualFigure);
			}

			Font systemFont = Display.getDefault().getSystemFont();


			if (label != null) {
				if (labelFigure == null) {
					labelFigure = new SimpleActivityLabel(this, label);

				}

				labelFigure.setFont(systemFont);
				labelFigure.setText(label);

				labelFigure.setBackgroundColor(ColorConstants.white);
				labelFigure.setOpaque(true);
				labelFigure.setVisible(true);
				IFigure labelLayer = getLabelLayer();
				if (labelLayer != null) {
					labelLayer.add(labelFigure);
					repaintLabel();
				}

			}

			if (imageFigure != null) {
				actualFigure.remove(imageFigure);
				layout.setConstraint(imageFigure, null);
				imageFigure.getImage().dispose();
			}

			Rectangle imageBounds = getImageBounds();

			if (imageDescr != null) {
				Image img = new Image(null, imageDescr.getImageData().scaledTo(
						imageBounds.width, imageBounds.height));
				imageFigure = new ImageFigure(img);
				layout.setConstraint(imageFigure, imageBounds);
				actualFigure.add(imageFigure);

			}

			if (stateFigure != null
					&& actualFigure.getChildren().contains(stateFigure)) {
				actualFigure.remove(stateFigure);
				stateFigure.getImage().dispose();
				layout.setConstraint(stateFigure, null);
			}

			if (stateImageDescr != null) {

				Rectangle stateBounds = new Rectangle(0, 0,
						imageBounds.width / 3, imageBounds.height / 3);
				Dimension diff = imageBounds.getBottomRight().getDifference(
						stateBounds.getBottomRight());
				stateBounds.translate(diff.width - 3, diff.height - 6);
				Image img = new Image(null, stateImageDescr.getImageData()
						.scaledTo(stateBounds.width, stateBounds.height));
				stateFigure = new ImageFigure(img);
				layout.setConstraint(stateFigure, stateBounds);
				actualFigure.add(stateFigure);
			}

			if (breakpointFigure != null) {
				actualFigure.remove(breakpointFigure);
				layout.setConstraint(breakpointFigure, null);
				breakpointFigure = null;
			}

			if (breakpointImage != null) {
				Rectangle breakpointBounds = new Rectangle(0, 0,
						imageBounds.width / 3, imageBounds.height / 3);
				Dimension diff = imageBounds.getTopLeft().getDifference(
						breakpointBounds.getTopLeft());
				breakpointBounds.translate(diff.width + 3, diff.height + 6);
				breakpointFigure = new ImageFigure(breakpointImage);
				layout.setConstraint(breakpointFigure, breakpointBounds);
				actualFigure.add(breakpointFigure);
			}

			if (layout != null) {
				layout.setConstraint(actualFigure, getClientArea().getCopy());
			}
			add(actualFigure);
			setDirty(false);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void dispose() {
		if (imageFigure != null && imageFigure.getImage() != null
				&& !imageFigure.getImage().isDisposed()) {
			imageFigure.getImage().dispose();
		}
		if (stateFigure != null && stateFigure.getImage() != null
				&& !stateFigure.getImage().isDisposed()) {
			stateFigure.getImage().dispose();
		}
	}

	protected IActivity getActivity() {

		return getPart() == null ? activity : getPart().getModel();
	}

	protected Rectangle getImageBounds() {
		Rectangle result = new Rectangle(new Point(0, 0), getClientArea().getSize());
		int labelHeight = 0;// getLabelFigureBounds().height;
		int yOffset = labelHeight;
		result.y += yOffset;
		result.height -= yOffset;

		if (imageDescr != null) {
			// scale the whole image to fit the remaining space in the
			// activity's bounds

			int maxHeight = result.height;
			int maxWidth = result.width;

			int originalWidth = imageData.width;
			int originalHeight = imageData.height;

			double scale = Math.min(((double) maxHeight) / originalHeight,
					((double) maxWidth) / originalWidth);
			int newWidth = (int) (originalWidth * scale);
			int newHeight = (int) (originalHeight * scale);

			result.height = newHeight;
			result.width = newWidth;

			// // center image horizontally
			// int widthDiff = result.width - newWidth;
			// result.x = widthDiff/2;

		}

		return result;
	}

	public Label getLabelFigure() {
		return labelFigure;
	}

	public Rectangle getLabelFigureBounds() {

		int height = WFConstants.DEFAULT_ACTIVITY_LABEL_HEIGHT;

		Rectangle imageBounds = getImageBounds();
		if (labelFigure == null) {
			return new Rectangle(0, 0, imageBounds.width, height);
		}
		try {
			Rectangle preferred = labelFigure.getTextBounds();
			Point location = getClientArea().getLocation().getCopy();
			preferred.setLocation(location);
			preferred.y += imageBounds.height / 3 - height / 2;
			preferred.x += imageBounds.width;
			return preferred;
		} catch (Exception e) {
			e.printStackTrace();
			return new Rectangle(0, 0, 0, 0);
		}

	}

	protected IFigure getLabelLayer() {
		IFigure result = getParent();
		while (result != null
				&& !(result instanceof LayeredPane)
				|| ((result instanceof LayeredPane) && ((LayeredPane) result)
						.getLayer(WFRootEditPart.LABEL_LAYER) == null)) {
			result = result.getParent();
		}
		if (result == null) {
			return null;
		}
		return ((LayeredPane) result).getLayer(WFRootEditPart.LABEL_LAYER);

	}

	protected ActivityPart getPart() {
		return activityPart;
	}

	public ImageFigure getStateFigure() {
		return stateFigure;
	}


	protected boolean hasDataSinks() {
		return getActivity().getDataSinkList().size() > 0;
	}

	protected boolean hasDataSources() {
		return getActivity().getDataSourceList().size() > 0;
	}

	protected boolean isDirty() {
		return dirty;
	}

	@Override
	public void paint(Graphics graphics) {

		super.paint(graphics);
		// if(getActivity().isCut()) {
		// graphics.setAlpha(120);
		// graphics.setBackgroundColor(ColorConstants.white);
		// graphics.fillRectangle(getBounds());
		// }
	}

	@Override
	public void removeNotify() {
		super.removeNotify();
		if (labelFigure != null && getLabelLayer() != null) {
			getLabelLayer().remove(labelFigure);
		}
	}

	@Override
	public void repaint() {

		buildActualFigure();

		repaintLabel();
		super.repaint();

	}

	protected void repaintLabel() {

		IFigure labelLayer = getLabelLayer();
		if (labelLayer != null && labelFigure != null) {
			Rectangle labelBounds = getLabelFigureBounds();
			labelLayer.getLayoutManager().setConstraint(labelFigure,
					labelBounds);
			labelLayer.getLayoutManager().layout(labelLayer);
		}
	}

	@Override
	public void setBounds(Rectangle rect) {
		super.setBounds(rect);
		if (actualFigure != null) {
			actualFigure.setBounds(rect);
		}
		// setDirty(true);
		repaintLabel();
	}

	public void setPreferredSize(Dimension dim)
	{
		super.setPreferredSize(dim);
		setDirty(true);
	}
	
	
	/**
	 * Set the breakpoint image for the figure.
	 * 
	 * @param imageDescr
	 */
	public void setBreakpointImage(Image image) {
		boolean dirty = breakpointImage == null ? image != null
				: breakpointImage.equals(image);
		if (dirty) {
			setDirty(true);
		}
		breakpointImage = image;
	}

	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	/**
	 * Set the image for the figure. Replaces the old actual figure
	 * 
	 * @param imageDescr
	 */
	public void setImage(ImageDescriptor imageDescr) {
		this.imageDescr = imageDescr;
		this.imageData = imageDescr.getImageData();
		setDirty(true);
	}

	public void setLabel(String s) {
		boolean dirty = label == null ? s != null : !label.equals(s);
		if (dirty) {
			setDirty(true);
		}
		label = s;

	}

	@Override
	public void setParent(IFigure parent) {
		super.setParent(parent);

	}

	public void setStateFigure(ImageFigure stateFigure) {
		this.stateFigure = stateFigure;
		setDirty(true);
	}

	/**
	 * Set the state image for the figure.
	 * 
	 * @param imageDescr
	 */
	public void setStateImage(ImageDescriptor imageDescr) {
		boolean dirty = stateImageDescr == null ? imageDescr != null
				: !stateImageDescr.equals(imageDescr);
		if (dirty) {
			setDirty(true);
		}
		stateImageDescr = imageDescr;

	}

	public String getLabel() {
		return label;
	}

}
