/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ConnectionEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gef.requests.GroupRequest;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.commands.DeleteTransitionCommand;
import de.fzj.unicore.rcp.wfeditor.model.commands.SplitTransitionCommand;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.parts.TransitionPart;

/**
 * EditPolicy for Transitions. Supports deletion and "splitting", i.e. adding an
 * Activity that splits the transition into an incoming and outgoing connection
 * to the new Activity.
 * 
 * @author Daniel Lee
 */
public class TransitionEditPolicy extends ConnectionEditPolicy {

	/**
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#eraseTargetFeedback(org.eclipse.gef.Request)
	 */
	@Override
	public void eraseTargetFeedback(Request request) {
		Command c = getCommand(request);
		if (c != null && c.canExecute()) {
			getConnectionFigure().setLineWidth(1);
		}
	}

	/**
	 * @see org.eclipse.gef.editpolicies.ConnectionEditPolicy#getCommand(org.eclipse.gef.Request)
	 */
	@Override
	public Command getCommand(Request request) {
		if (REQ_CREATE.equals(request.getType())
				|| REQ_ADD.equals(request.getType())) {
			return getSplitTransitionCommand(request);
		}
		return super.getCommand(request);
	}

	private PolylineConnection getConnectionFigure() {
		return ((TransitionPart) getHost()).getFigure();
	}

	/**
	 * @see ConnectionEditPolicy#getDeleteCommand(org.eclipse.gef.requests.GroupRequest)
	 */
	@Override
	protected Command getDeleteCommand(GroupRequest request) {
		DeleteTransitionCommand cmd = new DeleteTransitionCommand();
		Transition t = (Transition) getHost().getModel();
		if (!t.isDeletable()) {
			return null;
		}
		cmd.setTransition(t);
		return cmd;
	}

	protected Command getSplitTransitionCommand(Request request) {

		EditPart sourceParent = ((TransitionPart) getHost()).getSource()
				.getParent();
		if (!((StructuredActivity) sourceParent.getModel()).canAddChildren()) {
			return null;
		}
		Command command = sourceParent.getCommand(request);
		if (command == null) {
			return null;
		}
		SplitTransitionCommand cmd = new SplitTransitionCommand(command);
		cmd.setTransition(((Transition) getHost().getModel()));
		IActivity act = null;
		if (request instanceof CreateRequest) {
			CreateRequest createRequest = (CreateRequest) request;
			if (createRequest.getNewObject() instanceof IActivity) {
				act = (IActivity) createRequest.getNewObject();
			}
		} else if (request instanceof ChangeBoundsRequest) {
			ChangeBoundsRequest cbr = (ChangeBoundsRequest) request;
			if (cbr.getEditParts() != null && cbr.getEditParts().size() == 1) {
				Object o = cbr.getEditParts().get(0);
				if (o instanceof EditPart) {
					EditPart part = (EditPart) o;
					if (part.getModel() instanceof IActivity) {
						act = (IActivity) part.getModel();
					}
				}
			}

		}
		if (act == null) {
			return null;
		}
		cmd.setActivity(act);
		return cmd;
	}

	/**
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#getTargetEditPart(org.eclipse.gef.Request)
	 */
	@Override
	public EditPart getTargetEditPart(Request request) {
		if (REQ_CREATE.equals(request.getType())
				|| REQ_MOVE.equals(request.getType())) {
			return getHost();
		}
		return null;
	}

	/**
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#showTargetFeedback(org.eclipse.gef.Request)
	 */
	@Override
	public void showTargetFeedback(Request request) {
		Command c = getCommand(request);
		if (c != null && c.canExecute()) {
			getConnectionFigure().setLineWidth(2);
		}
	}

}
