/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.ToolBar;

import de.fzj.unicore.rcp.common.utils.ToolBarUtils;
import de.fzj.unicore.rcp.servicebrowser.filters.ContentFilter;
import de.fzj.unicore.rcp.servicebrowser.nodes.GridNode;
import de.fzj.unicore.rcp.servicebrowser.nodes.Node;
import de.fzj.unicore.rcp.servicebrowser.nodes.NodeFactory;
import de.fzj.unicore.rcp.servicebrowser.ui.ServiceSelectionPanel;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.submission.SubmitterRegistry;

/**
 * FlowWizardPage1
 * 
 * @author Daniel Lee
 */
public class WFSubmissionWizardPage1 extends WizardPage implements
		ISelectionChangedListener {

	private Node selectedNode = null;
	private WorkflowDiagram workflowDiagram;
	private static final String TITLE = "Please select a workflow service for submission.";
	ServiceSelectionPanel serviceSelectionPanel;
	// toolbar manager used to add additional items
	// to the toolbar below the tree viewer
	private ToolBarManager lowerToolBarManager = new ToolBarManager(SWT.RIGHT);

	public WFSubmissionWizardPage1() {
		super("");
		this.setImageDescriptor(ImageDescriptor.createFromFile(
				WFActivator.class, "images/flowbanner.gif"));

	}

	/**
	 * (non-Javadoc) Method declared on IDialogPage.
	 */
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);

		initializeDialogUnits(parent);

		// PlatformUI.getWorkbench().getHelpSystem().setHelp(composite,
		// IIDEHelpContextIds.NEW_PROJECT_WIZARD_PAGE);

		composite.setLayout(new GridLayout());
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));

		SubmitterRegistry submitterRegistry = WFActivator.getDefault()
				.getSubmitterRegistry();
		int depth = submitterRegistry.getMaxServiceDepth(getWorkflowDiagram());
		int style = SWT.BORDER | SWT.SINGLE;
		serviceSelectionPanel = new ServiceSelectionPanel(composite, null,
				null, this, depth, style);
		serviceSelectionPanel.getTreeViewer().setDoubleClickEnabled(false);
		serviceSelectionPanel.getTreeViewer().addDoubleClickListener(
				new IDoubleClickListener() {

					public void doubleClick(DoubleClickEvent event) {
						if (getContainer() instanceof WizardDialog) {
							if (getWizard().canFinish()) {
								getWizard().performFinish();

								((WizardDialog) getContainer()).close();
							}
						}
					}
				});
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 400;
		serviceSelectionPanel.getControl().setLayoutData(gd);
		ContentFilter[] filters = submitterRegistry.getServiceFilters(
				getWorkflowDiagram()).toArray(new ContentFilter[0]);
		for (ContentFilter filter : filters) {
			serviceSelectionPanel.addFilter(filter);
		}

		serviceSelectionPanel.getTreeViewer().setExpandedLevels(2);
		final GridNode gridNode = serviceSelectionPanel.getTreeViewer()
				.getGridNode();
		Object[] candidates = serviceSelectionPanel.getTreeViewer()
				.getAllFilteredChildren(gridNode);
		setTitle(TITLE);
		setErrorMessage(null);
		setPageComplete(false);
		if (candidates == null || candidates.length == 0) {

			setErrorMessage("No suitable workflow services found on the Grid.");
		} else {
			if (candidates[0] instanceof IAdaptable) {
				List<IAdaptable> selected = new ArrayList<IAdaptable>();
				selected.add((IAdaptable) candidates[0]);
				serviceSelectionPanel.getTreeViewer()
						.setSelectedNodes(selected);
			}
		}

		Composite lowerToolBarContainer = new Composite(composite, SWT.NO_FOCUS);
		lowerToolBarContainer.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM,
				true, false));
		Layout layout = new GridLayout(1, true);
		lowerToolBarContainer.setLayout(layout);

		ToolBar lowerToolBar = lowerToolBarManager
				.createControl(lowerToolBarContainer);
		lowerToolBar.setLayout(new GridLayout(lowerToolBar.getItemCount(),
				false));
		GridData data = new GridData(SWT.LEFT, SWT.BOTTOM, true, false);
		lowerToolBar.setLayoutData(data);
		ToolBarUtils.setToolItemText(lowerToolBar);

		setControl(composite);
		Dialog.applyDialogFont(composite);

	}

	@Override
	public void dispose() {
		super.dispose();
		if (serviceSelectionPanel != null) {
			serviceSelectionPanel.dispose();
		}
	}

	public boolean finish() {
		return selectedNode != null;
	}

	public ToolBarManager getLowerToolBarManager() {
		return lowerToolBarManager;
	}

	public Node getSelectedNode() {
		return selectedNode;
	}

	@Override
	public WFSubmissionWizard getWizard() {
		return (WFSubmissionWizard) super.getWizard();
	}

	public WorkflowDiagram getWorkflowDiagram() {
		return workflowDiagram;
	}

	public void selectionChanged(SelectionChangedEvent event) {
		selectedNode = null;
		
		IStructuredSelection selection = (IStructuredSelection) event
				.getSelection();
		if (selection.size() > 0) {
			Node selected = (Node) selection.getFirstElement();
			if (!serviceSelectionPanel.getTreeViewer().getGridNode()
					.equals(selected)) {
				Node original = NodeFactory
						.getNodeFor(selected.getPathToRoot());
				selectedNode = original;
			} 
		}
		getWizard().setSelectedNode(selectedNode);
		boolean canSubmit = getWizard().getSuitableSubmitter() != null;
		if(canSubmit) 
		{
			setErrorMessage(null);
		}
		else
		{
			Node n = serviceSelectionPanel.getTreeViewer().getGridNode();
			Object[] candidates = serviceSelectionPanel.getTreeViewer()
					.getAllFilteredChildren(n);
			if (candidates == null || candidates.length == 0) {
				setErrorMessage("No suitable workflow services found on the Grid.");
			}
			else
			{
				setErrorMessage("No suitable workflow service chosen for submission");
			}
		}
		
		setPageComplete(canSubmit);

	}

	public void setSelectedNode(Node selectedNode) {
		this.selectedNode = selectedNode;
	}

	public void setWorkflowDiagram(WorkflowDiagram workflowDiagram) {
		this.workflowDiagram = workflowDiagram;
	}

}
