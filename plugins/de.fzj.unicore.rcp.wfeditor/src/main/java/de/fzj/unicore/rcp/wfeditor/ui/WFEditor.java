/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EventObject;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.KeyHandler;
import org.eclipse.gef.KeyStroke;
import org.eclipse.gef.MouseWheelHandler;
import org.eclipse.gef.MouseWheelZoomHandler;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.dnd.TemplateTransferDragSourceListener;
import org.eclipse.gef.editparts.ZoomManager;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.DirectEditAction;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.gef.ui.parts.GraphicalEditorWithPalette;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import com.thoughtworks.xstream.XStream;

import de.fzj.unicore.rcp.common.guicomponents.EditorInputChangedListener;
import de.fzj.unicore.rcp.common.guicomponents.IEditorPartWithActions;
import de.fzj.unicore.rcp.common.guicomponents.IEditorPartWithSettableInput;
import de.fzj.unicore.rcp.common.utils.ProgressUtils;
import de.fzj.unicore.rcp.logmonitor.utils.BackgroundJob;
import de.fzj.unicore.rcp.wfeditor.PaletteFactory;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.actions.WFContextMenuProvider;
import de.fzj.unicore.rcp.wfeditor.actions.WFEditorAction;
import de.fzj.unicore.rcp.wfeditor.exceptions.NewerWorkflowModelException;
import de.fzj.unicore.rcp.wfeditor.exceptions.OutdatedWorkflowModelException;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.IEditorActionsExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.model.ExecutionData;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateConstants;
import de.fzj.unicore.rcp.wfeditor.parts.PartFactory;
import de.fzj.unicore.rcp.wfeditor.parts.WorkflowDiagramPart;
import de.fzj.unicore.rcp.wfeditor.utils.WFTransferDropTargetListener;
import de.fzj.unicore.rcp.wfeditor.utils.XStreamConfigurator;

/**
 * 
 * @author hudsonr, demuth
 */
@SuppressWarnings("unchecked")
public abstract class WFEditor extends GraphicalEditorWithPalette implements
		IEditorPartWithActions, PropertyChangeListener,
		ITabbedPropertySheetPageContributor, IEditorPartWithSettableInput {

	public static final String PROPERTY_CONTRIBUTOR_ID = "de.fzj.unicore.rcp.wfeditor";
	private WorkflowDiagram diagram;
	private List<WFEditorAction> wfEditorActions = new ArrayList<WFEditorAction>();
	private boolean deserialize = false, disposed = false;;
	protected PaletteRoot paletteRoot;
	private PaletteFactory paletteFactory;
	private int currentPaletteSize = getInitialPaletteSize();

	private KeyHandler sharedKeyHandler;

	private WFRootEditPart zoomPanel;

	/**
	 * The list of selection listeners.
	 */
	protected List<ISelectionChangedListener> selectionListeners = new ArrayList<ISelectionChangedListener>(
			1);

	private IAction zoomInAction, zoomOutAction, layoutDiagramAction;

	/**
	 * name of the domain in which this instance of the workflow editor is being
	 * used, e.g "chemistry", "QSAR", "supply chain management".
	 */
	private QName applicationDomain = new QName("");


	public WFEditor() {
		registerEditorActions();
	}

	/**
	 * @see org.eclipse.gef.commands.CommandStackListener#commandStackChanged(java.util.EventObject)
	 */
	@Override
	public void commandStackChanged(EventObject event) {
		firePropertyChange(IEditorPart.PROP_DIRTY);
		super.commandStackChanged(event);
	}

	/**
	 * @see org.eclipse.gef.ui.parts.GraphicalEditor#configureGraphicalViewer()
	 */
	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();

		zoomPanel = new WFRootEditPart();
		getGraphicalViewer().setRootEditPart(zoomPanel);

		double start = 0.1, end = 2.0, stepSize = 0.1;
		int numSteps = (int) ((end - start) / stepSize);
		double[] zoomLevels = new double[numSteps];
		for (int i = 0; i < numSteps; i++) {
			zoomLevels[i] = start + i * stepSize;
		}
		zoomPanel.getZoomManager().setZoomLevels(zoomLevels);
		List<String> zoomContributions = Arrays.asList(new String[] {
				ZoomManager.FIT_ALL, ZoomManager.FIT_HEIGHT,
				ZoomManager.FIT_WIDTH });
		zoomPanel.getZoomManager().setZoomLevelContributions(zoomContributions);
		zoomPanel.getZoomManager().setZoomAnimationStyle(
				ZoomManager.ANIMATE_ZOOM_IN_OUT);
		getGraphicalViewer().setProperty(
				MouseWheelHandler.KeyGenerator.getKey(SWT.MOD1),
				MouseWheelZoomHandler.SINGLETON);

		getGraphicalViewer().setEditPartFactory(new PartFactory(this));
		getGraphicalViewer().setKeyHandler(
				new GraphicalViewerKeyHandler(getGraphicalViewer())
						.setParent(getCommonKeyHandler()));

		ContextMenuProvider provider = new WFContextMenuProvider(
				getApplicationDomain(), getGraphicalViewer(),
				getActionRegistry());
		getGraphicalViewer().setContextMenu(provider);
		getSite().registerContextMenu(
				"de.fzj.unicore.rcp.wfeditor.editor.contextmenu", //$NON-NLS-1$
				provider, getGraphicalViewer());
		
		MenuManager paletteContextManager = new MenuManager();
		
		paletteContextManager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		getPaletteViewer().setContextMenu(paletteContextManager);
		getSite().registerContextMenu(
				"de.fzj.unicore.rcp.wfeditor.palette.contextmenu", //$NON-NLS-1$
			paletteContextManager, getPaletteViewer());

	}

	@Override
	protected void configurePaletteViewer() {
		super.configurePaletteViewer();
		getPaletteViewer().setEditPartFactory(new WFPaletteEditPartFactory(this));
	}

	/**
	 * @see org.eclipse.gef.ui.parts.GraphicalEditor#createActions()
	 */
	@Override
	protected void createActions() {
		super.createActions();
		ActionRegistry registry = getActionRegistry();
		IAction action;

		action = new DirectEditAction((IWorkbenchPart) this);
		registry.registerAction(action);
		getSelectionActions().add(action.getId());

		zoomInAction = new Action("Zoom In") {
			@Override
			public void run() {
				zoomIn();
			}
		};
		zoomInAction.setImageDescriptor(WFActivator
				.getImageDescriptor("zoom_in.png"));
		zoomInAction.setId(WFConstants.ACTION_ZOOM_IN);
		registry.registerAction(zoomInAction);
		getSite().getKeyBindingService().registerAction(zoomInAction);

		zoomOutAction = new Action("Zoom Out") {
			@Override
			public void run() {
				zoomOut();
			}
		};
		zoomOutAction.setImageDescriptor(WFActivator
				.getImageDescriptor("zoom_out.png"));
		zoomOutAction.setId(WFConstants.ACTION_ZOOM_OUT);
		registry.registerAction(zoomOutAction);
		getSite().getKeyBindingService().registerAction(zoomOutAction);

		layoutDiagramAction = new Action("Layout Diagram") {
			@Override
			public void run() {
				((WorkflowDiagramPart) getGraphicalViewer().getContents())
						.performAutomaticLayout();
			}
		};
		layoutDiagramAction.setImageDescriptor(WFActivator
				.getImageDescriptor("layout.png"));
		layoutDiagramAction.setId(WFConstants.ACTION_LAYOUT_DIAGRAM);
		registry.registerAction(layoutDiagramAction);

	}

	@Override
	protected void createGraphicalViewer(Composite parent) {
		GraphicalViewer viewer = new WFGraphicalViewer(this);
		viewer.createControl(parent);
		setGraphicalViewer(viewer);
		configureGraphicalViewer();
		hookGraphicalViewer();
		initializeGraphicalViewer();
	}

	protected abstract WorkflowDiagram createNewDiagram();

	/**
	 * Creates an appropriate output stream and writes the activity diagram out
	 * to this stream.
	 * 
	 * @param os
	 *            the base output stream
	 * @throws IOException
	 */
	protected void createOutputStream(OutputStream os) throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		WorkflowDiagramPart diagramPart = ((WorkflowDiagramPart) getGraphicalViewer()
				.getContents());
		diagramPart.getModel().beforeSerialization();
		diagramPart.beforeSerialization();
		XStream xstream = XStreamConfigurator.getInstance()
				.getXStreamForSerialization();
		String s = xstream.toXML(diagram);
		diagram.afterSerialization();
		diagramPart.afterSerialization();
		writer.write(s);
		writer.close();
	}

	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		getEditDomain().setPaletteViewer(getPaletteViewer());
		getSite().setSelectionProvider(new WFSelectionProvider(getGraphicalViewer(),getPaletteViewer()));
	}

	protected void deserializeDiagram() {
		if (getGraphicalViewer() == null) {
			return;
		}
		WorkflowDiagramPart diagramPart = ((WorkflowDiagramPart) getGraphicalViewer()
				.getContents());
		diagramPart.getModel().afterDeserialization();
		diagramPart.afterDeserialization();
		deserialize = false;
	}

	public DefaultEditDomain getEditDomain()
	{
		return super.getEditDomain();
	}
	
	@Override
	public void dispose() {
		disposed = true;
		if (paletteFactory != null) {
			paletteFactory.dispose();
		}
		boolean dirty = false;
		if (getDiagram() != null) {
			getDiagram().removePropertyChangeListener(this);
			dirty = getDiagram().isDirty() || super.isDirty();
			WFRootEditPart root = getRootPanel();
			if(root != null) root.saveViewInfoForDiagram();
		}

		final boolean isDirty = dirty;

		Job disposer = new BackgroundJob("saving submitted workflow") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				if (getDiagram() != null) {
					stopMonitoring();

					IFile file = (IFile) getEditorInput().getAdapter(
							IFile.class);

					if (file != null
							&& file.exists()
							&& getDiagram().getCurrentExecutionStateCipher() >= ExecutionStateConstants.STATE_SUBMITTING
							&& isDirty) {
						doSave(monitor);
					}
				}
				return Status.OK_STATUS;
			}
		};
		disposer.schedule();
		super.dispose();
	}

	/**
	 * @see org.eclipse.ui.ISaveablePart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(final IProgressMonitor monitor) {

		IFile file = ((IFileEditorInput) getEditorInput()).getFile();
		diagram.setDirty(false); // avoid this being triggered multiple times
		doSave(monitor, file);

		Display d = PlatformUI.getWorkbench().getDisplay();
		if (d.isDisposed()) {
			return;
		}
		d.syncExec(new Runnable() {
			public void run() {
				getCommandStack().markSaveLocation();
			}
		});
	}

	public void doSave(IProgressMonitor progress, final IFile file) {
		final IProgressMonitor monitor = ProgressUtils
				.getNonNullMonitorFor(progress);

		try {

			monitor.beginTask("Saving workflow", 100);
			SubProgressMonitor subMonitor = null;
			subMonitor = new SubProgressMonitor(monitor, 10);
			getDiagram().cleanUpDisposedElements(subMonitor);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			createOutputStream(out);
			if (file.exists()) {
				monitor.subTask("Writing data");
				subMonitor = new SubProgressMonitor(monitor, 80);
				file.setContents(new ByteArrayInputStream(out.toByteArray()),
						true, false, subMonitor);
			} else {

				file.create(new ByteArrayInputStream(out.toByteArray()), true,
						monitor);
			}
			diagram.setDirty(false);
			out.close();
			monitor.subTask("Refreshing project");
			subMonitor = new SubProgressMonitor(monitor, 10);
			getDiagram().getProject().refreshLocal(IResource.DEPTH_INFINITE,
					subMonitor);
		} catch (Exception e) {
			WFActivator
					.log(IStatus.ERROR, "Unable to save workflow diagram", e);
		} finally {
			monitor.done();
		}

	}

	/**
	 * @see org.eclipse.ui.ISaveablePart#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
		SaveAsDialog dialog = new SaveAsDialog(getSite().getWorkbenchWindow()
				.getShell());
		dialog.setOriginalFile(((IFileEditorInput) getEditorInput()).getFile());
		dialog.open();
		IPath path = dialog.getResult();
		if (path == null) {
			return;
		}

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final IFile file = workspace.getRoot().getFile(path);

		WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
			@Override
			public void execute(final IProgressMonitor monitor)
					throws CoreException {
				try {
					getDiagram().setFilename(file.getName());
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					diagram.setDirty(false);
					monitor.beginTask("saving workflow", 100);
					SubProgressMonitor subMonitor = null;
					if (monitor != null) {
						new SubProgressMonitor(monitor, 10);
					}
					getDiagram().cleanUpDisposedElements(subMonitor);
					createOutputStream(out);
					if (monitor != null) {
						subMonitor = new SubProgressMonitor(monitor, 90);
					}
					file.create(new ByteArrayInputStream(out.toByteArray()),
							true, subMonitor);
					out.close();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (monitor != null) {
						monitor.done();
					}
				}
			}
		};

		try {
			new ProgressMonitorDialog(getSite().getWorkbenchWindow().getShell())
					.run(false, true, op);
			setInput(new FileEditorInput(file));
			getCommandStack().markSaveLocation();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public IAction getAction(String actionID) {
		return getActionRegistry().getAction(actionID);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class adapter) {
		if (adapter == IPropertySheetPage.class) {
			return new TabbedPropertySheetPage(this);
		} else if (adapter == ZoomManager.class) {
			return getRootPanel().getZoomManager();
		} else if (adapter == IContentOutlinePage.class) {
			return new OverviewContentOutlinePage(getGraphicalViewer());
		}
		return super.getAdapter(adapter);
	}

	public QName getApplicationDomain() {
		return applicationDomain;
	}

	protected KeyHandler getCommonKeyHandler() {
		if (sharedKeyHandler == null) {
			sharedKeyHandler = new KeyHandler();
			sharedKeyHandler
					.put(KeyStroke.getPressed(SWT.DEL, 127, 0),
							getActionRegistry().getAction(
									ActionFactory.DELETE.getId()));
			sharedKeyHandler.put(
					KeyStroke.getPressed(SWT.F2, 0),
					getActionRegistry().getAction(
							GEFActionConstants.DIRECT_EDIT));
			sharedKeyHandler.put(
					KeyStroke.getReleased('', 97, SWT.CTRL),
					getActionRegistry().getAction(
							ActionFactory.SELECT_ALL.getId()));
		}
		return sharedKeyHandler;
	}

	public String getContributorId() {
		return PROPERTY_CONTRIBUTOR_ID;
	}

	public WorkflowDiagram getDiagram() {
		return diagram;
	}

	@Override
	protected int getInitialPaletteSize() {
		return WFActivator.getDefault().getPreferenceStore()
				.getInt(WFConstants.PREF_PALETTE_SIZE);
	}

	/**
	 * @see org.eclipse.gef.ui.parts.GraphicalEditorWithPalette#getPaletteRoot()
	 */
	@Override
	protected PaletteRoot getPaletteRoot() {
		if (paletteRoot == null) {
			paletteRoot = createPaletteRoot();
		}
		return paletteRoot;
	}
	
	public PaletteRoot createPaletteRoot()
	{
		if(paletteFactory == null) paletteFactory = new PaletteFactory(this);
		return paletteFactory.createPalette();
	}

	public WFRootEditPart getRootPanel() {
		return zoomPanel;
	}

	@Override
	public String getTitleToolTip() {
		String toolTip = super.getTitleToolTip();
		if (getDiagram() != null) {
			int executionState = getDiagram().getCurrentExecutionStateCipher();
			if (executionState != ExecutionStateConstants.STATE_UNSUBMITTED) {
				toolTip += " ("
						+ ExecutionStateConstants
								.getStringForState(executionState) + ")";
			}
		}
		return toolTip;
	}

	/**
	 * This list is needed for adding actions to the editor toolbar on a
	 * per-editor-instance basis.
	 */
	public List<WFEditorAction> getWfEditorActions() {
		return wfEditorActions;
	}

	public void gotoMarker(IMarker marker) {
	}

	@Override
	protected void handlePaletteResized(int newSize) {
		currentPaletteSize = newSize;
		WFActivator.getDefault().getPreferenceStore()
				.setValue(WFConstants.PREF_PALETTE_SIZE, currentPaletteSize);
	}

	@Override
	protected void hookPaletteViewer() {
		
		getPaletteViewer().setEditDomain(getEditDomain()); 
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		DefaultEditDomain defaultEditDomain = new DefaultEditDomain(this);
		setEditDomain(defaultEditDomain);
		super.init(site, input);
		// used for closing the editor if the underlying resource is closed or
		// deleted
		new EditorInputChangedListener(this);

	}

	/**
	 * @see org.eclipse.gef.ui.parts.GraphicalEditor#initializeGraphicalViewer()
	 */
	@Override
	protected void initializeGraphicalViewer() {
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				// delay these actions a little cause they may be slightly
				// long-running
				// furthermore, setting the content may cause a recursive
				// attempt to
				// init the workflow editor, as child job editors try to open
				// their parent workflow editor
				if (WorkflowDiagram.CURRENT_MODEL_VERSION
						.compareTo(getDiagram().modelVersion) < 0) {
					WFActivator
					.log(IStatus.ERROR,
							"This workflow has been created by a client with " +
							"version "+getDiagram().modelVersion+" but this " +
									"client only understands workflow models " +
									"up to "
									+WorkflowDiagram.CURRENT_MODEL_VERSION+"."+
									" The workflow will probably cause " +
									"problems.",
							new NewerWorkflowModelException());
					
				}
				else if (WorkflowDiagram.CURRENT_MODEL_VERSION
						.compareTo(getDiagram().modelVersion) > 0) {
					WFActivator
							.log(IStatus.WARNING,
									"This workflow will be updated to the current client version and is therefore modified. Please note, that you will not be able to use it in an old client version after saving it.",
									new OutdatedWorkflowModelException());
					try {
						getDiagram().updateToCurrentModelVersion(
								getDiagram().modelVersion,
								WorkflowDiagram.CURRENT_MODEL_VERSION);
						getDiagram().modelVersion = WorkflowDiagram.CURRENT_MODEL_VERSION;
						getDiagram().setDirty(true);
						getCommandStack().execute(new Command() {
						});
					} catch (Exception e) {
						WFActivator
								.log(IStatus.ERROR,
										"Updating workflow model to current version has failed",
										e);
					}
					WFActivator.log("Update complete");

				}

				getGraphicalViewer().setContents(diagram);
				if (deserialize) {
					deserializeDiagram();
				}

				getGraphicalViewer().addDropTargetListener(
						new WFTransferDropTargetListener(getGraphicalViewer()));
				updatePartName();
			}
		});

	}

	/**
	 * @see org.eclipse.gef.ui.parts.GraphicalEditorWithPalette#initializePaletteViewer()
	 */
	@Override
	protected void initializePaletteViewer() {
		super.initializePaletteViewer();
		getPaletteViewer().addDragSourceListener(
				new TemplateTransferDragSourceListener(getPaletteViewer()));

	}

	@Override
	public boolean isDirty() {
		if (getDiagram() == null
				|| getDiagram().getCurrentExecutionStateCipher() != ExecutionStateConstants.STATE_UNSUBMITTED) {
			return false;
		}
		return getDiagram().isDirty() || super.isDirty();
	}

	public boolean isDisposed() {
		return disposed;
	}

	/**
	 * @see org.eclipse.ui.ISaveablePart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		// TODO make save as work.
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(WorkflowDiagram.PROP_DIRTY)) {
			PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {

				public void run() {
					firePropertyChange(IEditorPart.PROP_DIRTY);
				}

			});
		} else if (evt.getPropertyName().equals(
				IFlowElement.PROP_EXECUTION_STATE)) {
			updatePartName();
		}

	}

	
	protected void registerEditorActions() {
		// iterate over all extensions and gather actions added by them
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		IExtensionPoint extensionPoint = registry
				.getExtensionPoint(WFConstants.EXTENSION_POINT_EDITOR_ACTIONS);
		IConfigurationElement[] members = extensionPoint
				.getConfigurationElements();
		for (int m = 0; m < members.length; m++) {
			IConfigurationElement member = members[m];
			// IExtension extension = member.getDeclaringExtension();
			// String actionName = member.getAttribute(FUNCTION_NAME_ATTRIBUTE);
			try {
				IEditorActionsExtensionPoint ext = (IEditorActionsExtensionPoint) member
						.createExecutableExtension("name");
				List<WFEditorAction> actions = ext
						.getAdditionalEditorActions(this);
				if (actions != null) {
					for (Iterator<WFEditorAction> iter = actions.iterator(); iter
							.hasNext();) {
						WFEditorAction action = iter.next();
						if (getActionRegistry().getAction(action.getId()) == null) {
							getActionRegistry().registerAction(action);
							getSelectionActions().add(action.getId());
							wfEditorActions.add(action);
						}
					}
				}
			} catch (CoreException ex) {
				WFActivator.log(IStatus.WARNING,
						"Could not determine all available actions.", ex);
			}
		}
	}

	public void setAction(String actionID, IAction action) {
		getActionRegistry().removeAction(action);
		action.setId(actionID); // make sure this is set correctly
		getActionRegistry().registerAction(action);
	}

	public void setApplicationDomain(QName contentDomain) {
		this.applicationDomain = contentDomain;
	}

	/**
	 * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void setInput(IEditorInput input) {
		if (diagram != null && diagram.hasRunningActivities()) {
			stopMonitoring();
		}
		super.setInput(input);
		IFile file = (IFile) input.getAdapter(IFile.class);

		try {
			InputStream is = file.getContents(false);
			if (is.available() > 0) {
				try {
					XStream xstream = XStreamConfigurator.getInstance()
							.getXStreamForDeserialization();
					diagram = (WorkflowDiagram) xstream
							.fromXML(new InputStreamReader(
									new BufferedInputStream(is)));
				} finally {
					is.close();
				}

				// diagram has been deserialized
				// try to inform it now. If this fails
				// since the WorkflowDiagramPart has not
				// been created yet, this will be called
				// again after the part has been created.
				deserialize = true;
				deserializeDiagram();
			} else {
				if (diagram == null) {
					WFActivator.log(IStatus.INFO, "Creating new workflow");
					diagram = createNewDiagram();
				} else {
					doSave(null, file);
				}
			}

		} catch (Exception e) {
			WFActivator.log(IStatus.WARNING, "Could not load workflow", e);
			diagram = createNewDiagram();
		}
		diagram.setFilename(file.getName());
		IProject project = file.getProject();
		String parentFolder = file.getParent().getProjectRelativePath()
				.toPortableString();
		diagram.setParentFolder(parentFolder);
		diagram.setProject(project);
		diagram.setCommandStack(getCommandStack());
		diagram.loadExecutionData();
		updatePartName();
		diagram.addPropertyChangeListener(this);
		GraphicalViewer viewer = getGraphicalViewer();
		if (viewer != null) {
			viewer.setContents(diagram);
		}

		if (diagram.hasRunningActivities()) {
			startMonitoring();
		}

	}

	protected void startMonitoring() {

		Job j = new BackgroundJob("starting workflow monitoring") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				WorkflowDiagram diagram = getDiagram();
				if (diagram != null) {
					ExecutionData executionData = diagram.getExecutionData();
					if (executionData != null) {
						try {
							if (executionData.getMonitoringService() != null) {
								WFActivator
										.getDefault()
										.getMonitorerRegistry()
										.monitor(
												diagram,
												executionData
														.getMonitoringService());
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return Status.CANCEL_STATUS;
						}

					}
				}
				return Status.OK_STATUS;
			}
		};
		j.schedule(3000);

	}

	protected void stopMonitoring() {

		WorkflowDiagram diagram = getDiagram();
		if (diagram != null) {
			ExecutionData executionData = diagram.getExecutionData();
			if (executionData != null) {
				try {
					if (executionData.getMonitoringService() != null) {
						WFActivator
								.getDefault()
								.getMonitorerRegistry()
								.stopMonitoring(diagram,
										executionData.getMonitoringService());
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

	protected void updatePartName() {
		String partName = "";
		try {
			partName += getDiagram().getFilename();
		} catch (Exception e1) {

		}
		int executionState = getDiagram().getCurrentExecutionStateCipher();
		if (executionState != ExecutionStateConstants.STATE_UNSUBMITTED) {
			partName += " ("
					+ ExecutionStateConstants.getStringForState(executionState)
					+ ")";
		}

		final String name = partName;
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
			public void run() {
				setPartName(name);
			}
		});
	}

	public void zoomFitToPage() {
		zoomPanel.getZoomManager().setZoomAsText(ZoomManager.FIT_ALL);
	}

	public void zoomIn() {
		zoomPanel.getZoomManager().zoomIn();
	}

	public void zoomOut() {
		zoomPanel.getZoomManager().zoomOut();
	}
}
