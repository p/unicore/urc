package de.fzj.unicore.rcp.wfeditor.model.variables;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import de.fzj.unicore.rcp.wfeditor.extensions.VariableToModifierDataFlowAssembler;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSource;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;

/**
 * Class that can be used to connect a {@link WorkflowVariable} with a
 * {@link WorkflowVariableModifier} via drag & drop. Used in conjunction with a
 * {@link VariableToModifierDataFlowAssembler}
 * 
 * @author bdemuth
 * 
 */
@XStreamAlias("VariableModifierDataSource")
public class VariableModifierDataSource extends DataSource implements
		PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6633340628592964745L;
	private String id;
	public static final QName TYPE = new QName("http://www.unicore.eu/",
			"VariableModifierDataSource");

	public static final String PROP_VARIABLE_MODIFIER = "variable modifier";

	public VariableModifierDataSource(WorkflowVariableModifier modifier) {
		super();
		setModifier(modifier);
		modifier.addPersistentPropertyChangeListener(this);

		WorkflowVariable var = getModifiedVariable();

		if (var != null) {
			Set<String> dataTypes = new HashSet<String>();
			dataTypes.add(var.getType());
			setPropertyValue(PROP_PROVIDED_DATA_TYPES, dataTypes);
		}
		id = createRandomId();
	}

	@Override
	public IFlowElement getFlowElement() {
		return getModifier().getFlowElement();
	}

	@Override
	public String getId() {
		return id;
	}

	public WorkflowVariable getModifiedVariable() {
		return getModifier().getVariable();

	}

	public WorkflowVariableModifier getModifier() {
		return (WorkflowVariableModifier) getPropertyValue(PROP_VARIABLE_MODIFIER);
	}

	@Override
	protected Set<Object> getPropertiesToCopy() {
		Set<Object> propertiesToCopy = super.getPropertiesToCopy();
		propertiesToCopy.add(PROP_VARIABLE_MODIFIER);
		return propertiesToCopy;
	}

	@Override
	public QName getSourceType() {
		return TYPE;
	}

	public boolean isVisible() {
		IFlowElement element = getFlowElement();
		if (element == null) {
			return false;
		}
		if (!getModifier().isDataSourceAndSinkVisible()) {
			return false;
		}

		ShowDataFlowsProperty prop = (ShowDataFlowsProperty) element
				.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS);
		if (prop == null || !prop.isShowingSources()) {
			return false;
		}

		WorkflowVariable var = getModifiedVariable();
		if (var == null) {
			return false;
		} else {
			return prop.isShowingFlowsWithDataType(var.getType());
		}
	}

	public void propertyChange(PropertyChangeEvent evt) {
		if (WorkflowVariableModifier.PROP_TYPE.equals(evt.getPropertyName())) {
			String newValue = (String) evt.getNewValue();
			Set<String> newDataTypes = new HashSet<String>();
			newDataTypes.add(newValue);
			setPropertyValue(PROP_PROVIDED_DATA_TYPES, newDataTypes);
		} else if (WorkflowVariableModifier.PROP_VARIABLE_NAME.equals(evt
				.getPropertyName())) {
			WorkflowVariable var = getModifiedVariable();
			if (var != null) {
				Set<String> newDataTypes = new HashSet<String>();
				newDataTypes.add(var.getType());
				setPropertyValue(PROP_PROVIDED_DATA_TYPES, newDataTypes);
			}
		}
		firePropertyChange(PROP_VARIABLE_MODIFIER, getModifier(), getModifier());
	}

	@Override
	public void setFlowElement(IFlowElement element) {
		getModifier().setFlowElement(element);

	}

	protected void setModifier(WorkflowVariableModifier modifier) {
		setPropertyValue(PROP_VARIABLE_MODIFIER, modifier);
	}

}
