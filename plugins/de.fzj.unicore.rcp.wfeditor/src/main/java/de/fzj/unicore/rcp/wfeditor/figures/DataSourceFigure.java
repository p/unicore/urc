package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;

public class DataSourceFigure extends Figure {

	private Color dataTypeColor;
	private String dataTypeSymbol;

	/**
	 * Lines of text to be displayed inside the tooltip figure.
	 */
	private String[] toolTipText;
	private Font toolTipFont;

	public DataSourceFigure() {
		setLayoutManager(new XYLayout());
	}

	public Color getDataTypeColor() {
		return dataTypeColor;
	}

	public String getDataTypeSymbol() {
		return dataTypeSymbol;
	}

	@Override
	public IFigure getToolTip() {
		IFigure result = new DataSourceToolTip();
		result.setFont(toolTipFont);
		return result;
	}

	public Font getToolTipFont() {
		return toolTipFont;
	}

	public String[] getToolTipText() {
		return toolTipText;
	}

	@Override
	protected void paintFigure(Graphics graphics) {

		Rectangle bounds = getBounds();
		if (dataTypeSymbol != null) {
			Font oldFont = graphics.getFont();
			graphics.setFont(getFont());

			int charWidth = graphics.getFontMetrics().getAverageCharWidth();
			int charHeight = graphics.getFontMetrics().getHeight();
			int labelWidth = charWidth * dataTypeSymbol.length();
			Point center = bounds.getCenter();

			if (getDataTypeColor() != null) {
				graphics.setBackgroundColor(getDataTypeColor());
				graphics.fillRectangle(bounds.getCropped(new Insets(center.y
						- bounds.y + charHeight / 2, 4, 0, 2)));
			}

			graphics.drawText(dataTypeSymbol,
					Math.max(bounds.x + 1, center.x - labelWidth / 2 + 1),
					center.y - charHeight / 2);
			graphics.setFont(oldFont);

		}
		graphics.drawRectangle(bounds.getCropped(new Insets(3, 1, 1, 1)));
	}

	public void setDataTypeColor(Color dataTypeColor) {
		this.dataTypeColor = dataTypeColor;
	}

	public void setDataTypeSymbol(String label) {
		this.dataTypeSymbol = label;
	}

	public void setToolTipFont(Font tooltipFont) {
		this.toolTipFont = tooltipFont;
	}

	public void setToolTipText(String[] toolTipText) {
		this.toolTipText = toolTipText;
	}

}
