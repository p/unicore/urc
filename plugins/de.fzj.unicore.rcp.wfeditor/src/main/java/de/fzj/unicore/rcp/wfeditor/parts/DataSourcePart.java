package de.fzj.unicore.rcp.wfeditor.parts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.figures.DataSourceFigure;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.policies.DragDataEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.ShowDataSinksAndSourcesEditPolicy;
import de.fzj.unicore.rcp.wfeditor.policies.ShowPropertyViewPolicy;
import de.fzj.unicore.rcp.wfeditor.requests.WFEditorRequest;
import de.fzj.unicore.rcp.wfeditor.tools.DataFlowCreationDragTracker;

public class DataSourcePart extends AbstractWFEditPart implements NodeEditPart,
		PropertyChangeListener {

	int index;
	int numTotal;

	private static Font figureFont = null;
	private static Font tooltipFont = null;

	private static AtomicInteger activeInstanceCount = new AtomicInteger();

	public DataSourcePart() {
		super();
	}

	@Override
	public void activate() {
		super.activate();
		getFigure().setVisible(getModel().isVisible());
		getModel().addPropertyChangeListener(this);
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE,
				new DragDataEditPolicy());
		installEditPolicy(WFConstants.POLICY_SHOW_DATA_SINKS_AND_SOURCES,
				new ShowDataSinksAndSourcesEditPolicy(getActivityPart()));
		installEditPolicy(WFConstants.POLICY_OPEN, new ShowPropertyViewPolicy());

	}

	@Override
	protected IFigure createFigure() {
		DataSourceFigure result = new DataSourceFigure();

		activeInstanceCount.incrementAndGet();
		result.setPreferredSize(WFConstants.DEFAULT_DATA_CONNECTOR_WIDTH,
				WFConstants.DEFAULT_DATA_CONNECTOR_HEIGHT);
		if (figureFont == null) {
			Font parentFont = getViewer().getControl().getFont();
			FontData fd = parentFont.getFontData()[0];
			FontData figureFontData = new FontData(fd.getName(),
					WFConstants.DEFAULT_DATA_CONNECTOR_FONT_SIZE, fd.getStyle());
			figureFont = new Font(parentFont.getDevice(), figureFontData);

			FontData tooltipFontData = new FontData(fd.getName(),
					WFConstants.DEFAULT_ENLARGED_DATA_CONNECTOR_FONT_SIZE,
					fd.getStyle());
			tooltipFont = new Font(parentFont.getDevice(), tooltipFontData);
		}
		result.setFont(figureFont);
		result.setToolTipFont(tooltipFont);
		return result;
	}

	@Override
	public void deactivate() {
		super.deactivate();
		getModel().removePropertyChangeListener(this);
		activeInstanceCount.decrementAndGet();
		if (activeInstanceCount.get() == 0) {
			figureFont.dispose();
			figureFont = null;
			tooltipFont.dispose();
			tooltipFont = null;
		}
	}

	protected IActivity getActivity() {

		return getActivityPart().getModel();
	}

	protected ActivityPart getActivityPart() {
		return (ActivityPart) getParent().getParent();
	}

	@Override
	public Command getCommand(Request request) {
		if (!isActive()) {
			return null;
		}
		Command cmd = null;
		// Disable editing completely if workflow diagram disallows editing
		WorkflowDiagram diagram = getActivity().getDiagram();

		if (diagram == null
				|| diagram.isEditable()
				|| REQ_OPEN.equals(request.getType())
				|| REQ_DIRECT_EDIT.equals(request.getType())
				|| ((request instanceof WFEditorRequest) && ((WFEditorRequest) request)
						.isExecutable(diagram))) {
			cmd = super.getCommand(request);
		} else {
			cmd = null;
		}
		return cmd;

	}

	@Override
	public DragTracker getDragTracker(Request request) {
		DataFlowCreationDragTracker result = new DataFlowCreationDragTracker(
				this);
		result.setUnloadWhenFinished(false);
		return result;
	}

	@Override
	public IDataSource getModel() {
		return (IDataSource) super.getModel();
	}

	@Override
	protected List<IDataFlow> getModelSourceConnections() {
		return Arrays.asList(getModel().getOutgoingFlows());
	}

	/**
	 * @see NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	public ConnectionAnchor getSourceConnectionAnchor(
			ConnectionEditPart connection) {
		return new BottomAnchor(getFigure(), -1);
	}

	/**
	 * @see org.eclipse.gef.NodeEditPart#getSourceConnectionAnchor(org.eclipse.gef.Request)
	 */
	public ConnectionAnchor getSourceConnectionAnchor(Request request) {
		return new BottomAnchor(getFigure(), -1);
	}

	/**
	 * @see NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.ConnectionEditPart)
	 */
	public ConnectionAnchor getTargetConnectionAnchor(
			ConnectionEditPart connection) {
		return new ChopboxAnchor(getFigure());
	}

	/**
	 * @see org.eclipse.gef.NodeEditPart#getTargetConnectionAnchor(org.eclipse.gef.Request)
	 */
	public ConnectionAnchor getTargetConnectionAnchor(Request request) {
		return new ChopboxAnchor(getFigure());
	}

	@Override
	public void performRequest(Request request) {
		if (REQ_OPEN.equals(request.getType())) {
			Command cmd = getCommand(request);
			if (cmd != null && cmd.canExecute()) {
				cmd.execute();
			}
		}
	}

	public void propertyChange(final PropertyChangeEvent evt) {
		Display d = PlatformUI.getWorkbench().getDisplay();
		if (!d.isDisposed()) {
			d.asyncExec(new Runnable() {
				public void run() {
					if (!isActive()) {
						return;
					}
					if (evt.getSource() == getModel()) {
						if (IDataSource.PROP_DATA_FLOWS.equals(evt
								.getPropertyName())) {
							refreshSourceConnections();
						} else if (IDataSource.PROP_NAME.equals(evt
								.getPropertyName())) {
							refreshToolTip((String) evt.getNewValue());
						}
					}
					refreshVisuals();
				}
			});
		}

	}

	protected void refreshToolTip(String name) {

	}

	protected void refreshVisibility(ShowDataFlowsProperty prop) {
		getFigure().setVisible(getModel().isVisible());
		for (IDataFlow flow : getModel().getOutgoingFlows()) {
			Object o = getViewer().getEditPartRegistry().get(flow);
			if (o instanceof GraphicalEditPart) {
				GraphicalEditPart part = (GraphicalEditPart) o;
				part.getFigure().setVisible(flow.isVisible());
			}
		}
	}

	@Override
	protected void refreshVisuals() {
		refreshToolTip(getModel().getName());
		index = getModel().getFlowElement().getDataSourceList()
				.indexOf(getModel());
		numTotal = getModel().getFlowElement().getDataSourceList().size();
		getFigure().repaint();
		super.refreshVisuals();
		refreshVisibility((ShowDataFlowsProperty) getModel().getFlowElement()
				.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS));
	}

}
