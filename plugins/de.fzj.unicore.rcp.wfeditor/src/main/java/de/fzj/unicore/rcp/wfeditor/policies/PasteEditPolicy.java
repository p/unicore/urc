/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.policies;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;

import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.model.commands.PasteCommand;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;

/**
 * @author Daniel Lee
 */
public class PasteEditPolicy extends AbstractEditPolicy {

	@Override
	public Command getCommand(Request request) {
		if (WFConstants.REQ_PASTE.equals(request.getType())) {

			if (PasteCommand.executing) {
				return null;
			}
			PasteCommand cmd;
			EditPart host = getHost();
			if (host == null || !host.isActive()) {
				return null;
			}
			EditPart parent = host.getParent();
			StructuredActivity parentModel = (parent.getModel() instanceof StructuredActivity) ? (StructuredActivity) parent
					.getModel() : null;
			if (host.getModel() instanceof StructuredActivity) {
				StructuredActivity hostModel = (StructuredActivity) host
						.getModel();
				if (hostModel.canAddChildren()) {
					cmd = new PasteCommand(hostModel);
				} else {
					cmd = new PasteCommand(parentModel);
				}
			} else {
				cmd = new PasteCommand(parentModel);
			}
			cmd.setViewer(getHost().getViewer());
			return cmd;

		}
		return null;

	}

}
