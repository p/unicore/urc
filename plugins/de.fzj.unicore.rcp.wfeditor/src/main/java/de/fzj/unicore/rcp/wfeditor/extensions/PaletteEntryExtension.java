/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.extensions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.gef.palette.ConnectionCreationToolEntry;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteSeparator;
import org.eclipse.gef.palette.SelectionToolEntry;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.thoughtworks.xstream.converters.Converter;

import de.fzj.unicore.rcp.wfeditor.DefaultCreationTool;
import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.IPaletteEntryExtensionPoint;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.PaletteCategory;
import de.fzj.unicore.rcp.wfeditor.extensionpoints.PaletteElement;
import de.fzj.unicore.rcp.wfeditor.model.FlowElement;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.model.activities.ActivityFactory;
import de.fzj.unicore.rcp.wfeditor.model.activities.IfElseSplitActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.RepeatUntilStartActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.StartActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.VariableDeclarationActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.VariableModifierActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.WhileStartActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSinkList;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSource;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSourceList;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.model.files.WorkflowFile;
import de.fzj.unicore.rcp.wfeditor.model.structures.ContainerActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.IfElseActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.RepeatUntilActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.SequentialActivity;
import de.fzj.unicore.rcp.wfeditor.model.structures.WhileActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.model.variables.VariableModifierDataSink;
import de.fzj.unicore.rcp.wfeditor.model.variables.VariableModifierDataSource;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;
import de.fzj.unicore.rcp.wfeditor.parts.ContainerActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.DataFlowPart;
import de.fzj.unicore.rcp.wfeditor.parts.DataSinkListPart;
import de.fzj.unicore.rcp.wfeditor.parts.DataSinkPart;
import de.fzj.unicore.rcp.wfeditor.parts.DataSourceListPart;
import de.fzj.unicore.rcp.wfeditor.parts.DataSourcePart;
import de.fzj.unicore.rcp.wfeditor.parts.IfElseActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.IfElseSplitActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.RepeatUntilActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.RepeatUntilStartActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.SequentialActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.StartActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.TransitionPart;
import de.fzj.unicore.rcp.wfeditor.parts.VariableDeclarationActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.VariableModifierActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.VariableModifierDataSinkPart;
import de.fzj.unicore.rcp.wfeditor.parts.VariableModifierDataSourcePart;
import de.fzj.unicore.rcp.wfeditor.parts.WhileActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.WhileStartActivityPart;
import de.fzj.unicore.rcp.wfeditor.parts.WorkflowDiagramPart;
import de.fzj.unicore.rcp.wfeditor.parts.WorkflowFilePart;
import de.fzj.unicore.rcp.wfeditor.parts.WorkflowVariablePart;
import de.fzj.unicore.rcp.wfeditor.tools.ControlFlowCreationTool;
import de.fzj.unicore.rcp.wfeditor.tools.DataFlowCreationToolEntry;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;
import de.fzj.unicore.rcp.wfeditor.utils.PackageUtil;

/**
 * @author demuth
 * 
 */
public class PaletteEntryExtension implements IPaletteEntryExtensionPoint {

	private List<Image> images = new ArrayList<Image>();
	private static Set<Class<?>> annotatedClasses;

	public EditPart createEditPart(WFEditor editor, EditPart context,
			Object model) {
		EditPart part = null;
		if (model instanceof IDataSource) {
			IDataSource source = (IDataSource) model;
			if (DataSource.TYPE.equals(source.getSourceType())) {
				part = new DataSourcePart();
			} else if (WorkflowFile.TYPE.equals(source.getSourceType())) {
				part = new WorkflowFilePart();
			} else if (WorkflowVariable.TYPE.equals(source.getSourceType())) {
				part = new WorkflowVariablePart();
			} else if (VariableModifierDataSource.TYPE.equals(source
					.getSourceType())) {
				part = new VariableModifierDataSourcePart();
			}

		} else if (model instanceof IDataSink) {
			IDataSink sink = (IDataSink) model;
			if (DataSink.TYPE.equals(sink.getSinkType())) {
				part = new DataSinkPart();
			} else if (VariableModifierDataSink.TYPE.equals(sink.getSinkType())) {
				part = new VariableModifierDataSinkPart();
			}
		} else if (model instanceof IDataFlow) {
			IDataFlow flow = (IDataFlow) model;
			if (IDataFlow.TYPE.equals(flow.getType())) {
				part = new DataFlowPart();
			}
		} else if (model instanceof DataSinkList) {
			part = new DataSinkListPart();

		} else if (model instanceof DataSourceList) {
			part = new DataSourceListPart();
		} else {

			if (!(model instanceof IFlowElement)) {
				return null;
			}
			IFlowElement element = (IFlowElement) model;
			if (WorkflowDiagram.TYPE.equals(element.getType())) {
				part = new WorkflowDiagramPart();
			} else if (IfElseActivity.TYPE.equals(element.getType())) {
				part = new IfElseActivityPart();
			} else if (WhileActivity.TYPE.equals(element.getType())) {
				part = new WhileActivityPart();
			} else if (RepeatUntilActivity.TYPE.equals(element.getType())) {
				part = new RepeatUntilActivityPart();
			} else if (ContainerActivity.TYPE.equals(element.getType())) {
				part = new ContainerActivityPart();
			} else if (SequentialActivity.TYPE.equals(element.getType())) {
				part = new SequentialActivityPart();
			} else if (StartActivity.TYPE.equals(element.getType())) {
				part = new StartActivityPart();
			} else if (VariableDeclarationActivity.TYPE.equals(element
					.getType())) {
				part = new VariableDeclarationActivityPart();
			} else if (VariableModifierActivity.TYPE.equals(element.getType())) {
				part = new VariableModifierActivityPart();
			} else if (IfElseSplitActivity.TYPE.equals(element.getType())) {
				part = new IfElseSplitActivityPart();
			} else if (WhileStartActivity.TYPE.equals(element.getType())) {
				part = new WhileStartActivityPart(); 
			} else if (RepeatUntilStartActivity.TYPE.equals(element.getType())) {
				part = new RepeatUntilStartActivityPart();
			} else if (Transition.TYPE.equals(element.getType())) {
				part = new TransitionPart();
			}
		}

		return part;
	}

	private List<PaletteElement> createGenericTools(WFEditor editor) {
		List<PaletteElement> elements = new ArrayList<PaletteElement>();
		PaletteCategory category = IPaletteEntryExtensionPoint.PALETTE_CATEGORY_TOOLS;
		ToolEntry tool = new SelectionToolEntry();
		elements.add(new PaletteElement(category, tool));

		PaletteSeparator sep = new PaletteSeparator(
				"de.fzj.unicore.rcp.wfeditor.flowplugin.sep2");
		sep.setUserModificationPermission(PaletteEntry.PERMISSION_NO_MODIFICATION);
		elements.add(new PaletteElement(category, sep));
		return elements;
	}

	private List<PaletteElement> createGenericTransitions(WFEditor editor) {
		List<PaletteElement> elements = new ArrayList<PaletteElement>();
		PaletteCategory category = IPaletteEntryExtensionPoint.PALETTE_CATEGORY_TOOLS;
		ConnectionCreationToolEntry tool = new ConnectionCreationToolEntry(
				"Control Flow",
				"Forward execution control between activities.", null,
				WFActivator.getImageDescriptor("connection16.gif"),
				WFActivator.getImageDescriptor("connection16.gif"));
		tool.setToolClass(ControlFlowCreationTool.class);
		elements.add(new PaletteElement(category, tool));
		tool = new DataFlowCreationToolEntry("Data Flow",
				"Create data flows between activities.", null,
				WFActivator.getImageDescriptor("connection16.gif"),
				WFActivator.getImageDescriptor("connection16.gif"));
		elements.add(new PaletteElement(category, tool));
		return elements;
	}

	private List<PaletteElement> createGenericWorkflowStructures(WFEditor editor) {
		List<PaletteElement> elements = new ArrayList<PaletteElement>();
		PaletteCategory category = IPaletteEntryExtensionPoint.PALETTE_CATEGORY_WORKFLOW_STRUCTURES;
		ImageDescriptor iconLarge;
		ActivityFactory factory;
		CombinedTemplateCreationEntry combined;

		// factory = new ActivityFactory(SequentialActivity.class,editor);
		// combined = new CombinedTemplateCreationEntry(
		// "Sequential Activity",
		// "Create a Sequential Activity",
		// factory,
		// factory,
		// WFActivator.getImageDescriptor("sequence16.gif"),
		// WFActivator.getImageDescriptor("sequence16.gif")
		// );
		// elements.add(new PaletteElement(category,combined);
		// //
		// factory = new ActivityFactory(ContainerActivity.class,editor);
		// combined = new CombinedTemplateCreationEntry(
		// "Parallel Activity",
		// "Create a  Parallel Activity",
		// factory,
		// factory,
		// WFActivator.getImageDescriptor("parallel16.gif"),
		// WFActivator.getImageDescriptor("parallel16.gif")
		// );
		// elements.add(new PaletteElement(category,combined);

		factory = new ActivityFactory(IfElseActivity.class, editor);
		ImageDescriptor imageDescr = WFActivator.getImageDescriptor("if.png");
		iconLarge = ImageDescriptor.createFromImageData(imageDescr
				.getImageData().scaledTo(DEFAULT_ICON_SIZE.width,
						DEFAULT_ICON_SIZE.height));
		combined = new CombinedTemplateCreationEntry("If-Statement",
				"Create a new If-Statement", factory, factory, iconLarge,
				iconLarge);
		elements.add(new PaletteElement(category, combined));
		combined.setToolClass(DefaultCreationTool.class);

		factory = new ActivityFactory(WhileActivity.class, editor);
		imageDescr = WFActivator.getImageDescriptor("while.png");
		iconLarge = ImageDescriptor.createFromImageData(imageDescr
				.getImageData().scaledTo(DEFAULT_ICON_SIZE.width,
						DEFAULT_ICON_SIZE.height));
		combined = new CombinedTemplateCreationEntry("While-Loop",
				"Create a new While-Loop", factory, factory, iconLarge,
				iconLarge);
		elements.add(new PaletteElement(category, combined));
		combined.setToolClass(DefaultCreationTool.class);

		factory = new ActivityFactory(RepeatUntilActivity.class, editor);
		imageDescr = WFActivator.getImageDescriptor("repeat_until.png");
		iconLarge = ImageDescriptor.createFromImageData(imageDescr
				.getImageData().scaledTo(DEFAULT_ICON_SIZE.width,
						DEFAULT_ICON_SIZE.height));
		combined = new CombinedTemplateCreationEntry("Repeat-Loop",
				"Create a new Repeat-Loop", factory, factory, iconLarge,
				iconLarge);
		elements.add(new PaletteElement(category, combined));
		combined.setToolClass(DefaultCreationTool.class);

		factory = new ActivityFactory(ContainerActivity.class, editor);
		imageDescr = WFActivator.getImageDescriptor("container.png");
		iconLarge = ImageDescriptor.createFromImageData(imageDescr
				.getImageData().scaledTo(DEFAULT_ICON_SIZE.width,
						DEFAULT_ICON_SIZE.height));
		combined = new CombinedTemplateCreationEntry(
				"Group",
				"Create a new activity group that might contain sub-activities and structures",
				factory, factory, iconLarge, iconLarge);
		elements.add(new PaletteElement(category, combined));
		combined.setToolClass(DefaultCreationTool.class);

		return elements;
	}

	private List<PaletteElement> createVariables(WFEditor editor) {
		List<PaletteElement> elements = new ArrayList<PaletteElement>();
		PaletteCategory category = IPaletteEntryExtensionPoint.PALETTE_CATEGORY_VARIABLES;
		ImageDescriptor iconLarge;
		ActivityFactory factory;
		CombinedTemplateCreationEntry combined;

		factory = new ActivityFactory(VariableDeclarationActivity.class, editor);
		ImageDescriptor imageDescr = WFActivator
				.getImageDescriptor("variable_declaration.png");
		iconLarge = ImageDescriptor.createFromImageData(imageDescr
				.getImageData().scaledTo(DEFAULT_ICON_SIZE.width,
						DEFAULT_ICON_SIZE.height));
		combined = new CombinedTemplateCreationEntry("Declaration",
				"Declare a workflow variable.", factory, factory, iconLarge,
				iconLarge);
		elements.add(new PaletteElement(category, combined));
		combined.setToolClass(DefaultCreationTool.class);

		factory = new ActivityFactory(VariableModifierActivity.class, editor);
		imageDescr = WFActivator.getImageDescriptor("variable_modifier.png");
		iconLarge = ImageDescriptor.createFromImageData(imageDescr
				.getImageData().scaledTo(DEFAULT_ICON_SIZE.width,
						DEFAULT_ICON_SIZE.height));
		combined = new CombinedTemplateCreationEntry("Modifier",
				"Modify a workflow variable.", factory, factory, iconLarge,
				iconLarge);
		elements.add(new PaletteElement(category, combined));
		combined.setToolClass(DefaultCreationTool.class);
		return elements;
	}

	/**
	 * Perform some cleanup! All created images should be disposed of.
	 */
	public void dispose() {
		for (Image img : images) {
			img.dispose();
		}
	}

	public Set<Converter> getAdditionalConverters() {
		return null;
	}

	/**
	 * contribute activity types to the UNICORE workflow editor
	 */
	public List<PaletteElement> getAdditionalPaletteEntries(WFEditor editor) {
		List<PaletteElement> elements = new ArrayList<PaletteElement>();
		elements.addAll(createGenericTools(editor));
		elements.addAll(createGenericWorkflowStructures(editor));
		elements.addAll(createGenericTransitions(editor));
		elements.addAll(createVariables(editor));
		return elements;
	}

	public Set<Class<?>> getAllModelClasses() {
		if (annotatedClasses == null) {
			Class<?> clazz = FlowElement.class;
			Package modelPackage = clazz.getPackage();
			try {
				// this is expensive, so don't do it often!
				annotatedClasses = PackageUtil.getEclipseClasses(
						WFActivator.getDefault(), modelPackage.getName(), true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return annotatedClasses;
	}

	public Map<String, Class<?>> getOldTypeAliases() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.IExecutableExtension#setInitializationData(org
	 * .eclipse.core.runtime.IConfigurationElement, java.lang.String,
	 * java.lang.Object)
	 */
	public void setInitializationData(IConfigurationElement config,
			String propertyName, Object data) throws CoreException {

	}

}
