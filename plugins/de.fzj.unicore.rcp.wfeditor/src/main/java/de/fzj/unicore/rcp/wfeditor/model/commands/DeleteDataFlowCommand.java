/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.data.IDataFlow;

/**
 * Handles the deletion of data flows between Activities.
 */
public class DeleteDataFlowCommand extends Command {

	private IDataFlow dataFlow;

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		dataFlow.disconnect();
	}

	public void setDataFlow(IDataFlow dataFlow) {
		this.dataFlow = dataFlow;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		dataFlow.reconnect();
	}

}
