package de.fzj.unicore.rcp.wfeditor.figures;

import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.LayerConstants;

import de.fzj.unicore.rcp.wfeditor.parts.StructuredActivityPart;

public class TransitionFigure extends PolylineConnection {

	protected GraphicalEditPart targetPart;

	public TransitionFigure() {
		super();
		setTargetDecoration(new TransitionTargetDecoration(null));
		setTolerance(6);
	}

	@Override
	public void paintFigure(Graphics graphics) {

		Rectangle bounds = new Rectangle(getBounds());
		if (targetPart != null && targetPart.getFigure() != null
				&& targetPart.getFigure().getParent() != null) {
			bounds = bounds.intersect(targetPart.getFigure().getParent()
					.getClientArea());
			Rectangle originalClip = new Rectangle();
			graphics.getClip(originalClip);
			graphics.clipRect(bounds);
			super.paintFigure(graphics);
			graphics.clipRect(originalClip);
		} else {
			super.paintFigure(graphics);
		}

	}

	@Override
	public void setAlpha(int alpha) {
		super.setAlpha(alpha);
		if (getSourceDecoration() instanceof Shape) {
			((Shape) getSourceDecoration()).setAlpha(alpha);
		}
		if (getTargetDecoration() instanceof Shape) {
			((Shape) getTargetDecoration()).setAlpha(alpha);
		}
	}

	public void setTargetPart(GraphicalEditPart targetPart) {
		this.targetPart = targetPart;
		((TransitionTargetDecoration) getTargetDecoration())
				.setTargetPart(targetPart);
		if (targetPart != null
				&& targetPart.getParent() instanceof StructuredActivityPart) {
			// try to inherit the connection router from our parent part
			ConnectionRouter router = ((StructuredActivityPart) targetPart
					.getParent())
					.getConnectionRouter(LayerConstants.CONNECTION_LAYER);
			setConnectionRouter(router);
		}
	}

}
