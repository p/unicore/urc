/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model;

import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gef.commands.CommandStack;

import de.fzj.unicore.rcp.common.interfaces.ISerializable;
import de.fzj.unicore.rcp.wfeditor.model.commands.CutCommand;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSinkList;
import de.fzj.unicore.rcp.wfeditor.model.data.DataSourceList;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionState;
import de.fzj.unicore.rcp.wfeditor.model.states.ExecutionStateDescriptor;
import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableList;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariableModifierList;

/**
 * @author demuth
 * 
 */
public interface IFlowElement extends IPropertySourceWithListeners, IAdaptable,
		Cloneable, ICopyable, ISerializable {

	public static final String CHILDREN = "Children", //$NON-NLS-1$
			PARENT = "Parent", INPUTS = "inputs", //$NON-NLS-1$
			OUTPUTS = "outputs"; //$NON-NLS-1$

	/**
	 * The key for the name property
	 */
	public static final String PROP_NAME = "name"; //$NON-NLS-1$

	/**
	 * The key for the execution state property
	 */
	public static final String PROP_EXECUTION_STATE = "execution state";

	/**
	 * The key for the iteration id property that determines which of the stored
	 * states for the element is the current state and should be visualized in
	 * the diagram
	 */
	public static final String PROP_CURRENT_ITERATION_ID = "iteration";

	/**
	 * Property that defines the name of the workflow variable that is used to
	 * identify iterations within a loop body. This property is only set, if
	 * this FlowElement is a loop body.
	 */
	public static final String PROP_ITERATOR_NAME = "loop iterator name";

	public static final String PROP_DECLARED_VARIABLES = "Declared variables";

	public static final String PROP_VARIABLE_MODIFIERS = "Variable Modifiers";

	public static final String PROP_BREAKPOINT = "Breakpoint";

	/**
	 * Boolean property that is set to true when the flow element was cut from
	 * the diagram by a {@link CutCommand}
	 */
	public static final String PROP_CUT = "has been cut";

	/**
	 * The key for the data sources property. The value is of type
	 * {@link DataSourceList}
	 */
	public static final String PROP_DATA_SOURCE_LIST = "data source list";

	/**
	 * The key for the data sinks property. The value is of type
	 * {@link DataSinkList}
	 */
	public static final String PROP_DATA_SINK_LIST = "data sink list";

	/**
	 * The key for the show data flows property stating whether the diagram
	 * should display the data flows that start or end at this element. The
	 * value is of type {@link ShowDataFlowsProperty}
	 */
	public static final String PROP_SHOW_DATA_FLOWS = "show data flows";

	/**
	 * Called when the element has been created and its part has been activated.
	 * In this state, the element is fully initialized. It should have a figure
	 * and a valid reference to its parent workflow diagram.
	 */
	public void activate();

	/**
	 * This is called after the submission was completed and should be
	 * propagated to child elements.
	 * 
	 * @return
	 */
	public void afterSubmission();

	/**
	 * This is called before saving the diagram to be submitted and before the
	 * conversion to the target workflow language starts. It can be used in
	 * order to perform some action that needs to be completed before submission
	 * and should be propagated to child elements.
	 * 
	 * @return
	 */
	public void beforeSubmission();

	/**
	 * Convenience method for changing the cipher and description of the
	 * currently selected state to the given values.
	 * 
	 * @param cipher
	 * @param description
	 */
	public void changeCurrentExecutionState(int cipher, String description);

	public CommandStack getCommandStack();

	public IFlowElement getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags);

	public ExecutionState getCurrentExecutionState();

	/**
	 * Get the execution state cipher of the current iteration of the Activity.
	 * 
	 * @return
	 */
	public int getCurrentExecutionStateCipher();

	/**
	 * Get the detailed execution state description of the Activity. E.g. fail
	 * reasons are stored in this String.
	 * 
	 * @return
	 */
	public String getCurrentExecutionStateDescription();

	/**
	 * A human readable representation of the integer state cipher. E.g.
	 * "unsubmitted", "running", "failed";
	 * 
	 * @return
	 */
	public String getCurrentExecutionStateName();

	/**
	 * method for getting the value for the property
	 * {@link PROP_CURRENT_ITERATION_ID}
	 */
	public String getCurrentIterationId();

	public DataSinkList getDataSinkList();

	public DataSourceList getDataSourceList();

	public WorkflowDiagram getDiagram();

	public ExecutionStateDescriptor getExecutionStateDescriptor();

	/**
	 * Unique ID for identifying the element internally (e.g. during workflow
	 * execution)
	 * 
	 * @return
	 */
	public String getID();

	public String getName();

	/**
	 * Returns the StructuredActivity that contains this element
	 * 
	 * @return
	 */
	public StructuredActivity getParent();

	/**
	 * A unique name for this type of workflow element. Used for determining a
	 * converter which can transform the element into some XML dialect for
	 * serialization.
	 * 
	 * @return
	 */
	public QName getType();

	/**
	 * Returns the unresolved iteration id of this flow element which consists
	 * of a sequence of variable names separated by a special character sequence
	 * (e.g. ":::${ITERATOR1}:::${ITERATOR2}".
	 * 
	 * @return
	 */
	public String getUnresolvedIterationId();

	/**
	 * Get a list containing all workflow variables that have been declared by
	 * this element. These variables can be used in other parts of the workflow
	 * in order to pass parameters between elements.
	 * 
	 * @return
	 */
	public WorkflowVariableList getVariableList();

	/**
	 * Get a list containing all variable modifiers that have been declared by
	 * this element. Each modifier represents an expression that is evaluated at
	 * workflow runtime in order to modify a workflow variable;
	 * 
	 * @return
	 */
	public WorkflowVariableModifierList getVariableModifierList();

	/**
	 * This method is called shortly after the flow element is created and after
	 * the diagram is set. It can be used for initializing the element's
	 * structure. It is NOT called when calling {@link #getCopy()}. Instead the
	 * element's structure is restored by creating copies.
	 */
	public void init();

	/**
	 * Returns true iff the element can not be moved from its parent to a
	 * different container.
	 * 
	 * @return
	 */
	public boolean isBoundToParent();

	/**
	 * method for getting the value for the property {@link PROP_CUT}
	 */
	public boolean isCut();

	/**
	 * Returns true iff the flow element can be deleted from the diagram.
	 * 
	 * @return
	 */
	public boolean isDeletable();

	/**
	 * Returns true if the iteration id of the element was changed by the user
	 * 
	 * @return
	 */
	public boolean isIterationManuallyChanged();

	/**
	 * claim whether this element contains all needed data for submission to a
	 * workflow execution service.
	 */
	public boolean readyForSubmission();

	public void setBoundToParent(boolean boundToParent);

	public void setCommandStack(CommandStack commandStack);

	/**
	 * method for setting the property {@link PROP_CURRENT_ITERATION_ID}
	 * 
	 * @param id
	 */
	public void setCurrentIterationId(String id);

	/**
	 * method for setting the property {@link PROP_CURRENT_ITERATION_ID}
	 * 
	 * @param id
	 * @param manual
	 *            Set to true if the user sets the value for the current
	 *            iteration id. If this is true, the current iteration id can
	 *            only be changed by the user (not automatically e.g. by a
	 *            monitorer).
	 * 
	 */
	public void setCurrentIterationId(String id, boolean manual);

	/**
	 * method for setting the value for the property {@link PROP_CUT}
	 */
	public void setCut(boolean cut);

	public void setDataSinks(DataSinkList list);

	public void setDataSources(DataSourceList list);

	public void setDeletable(boolean deletable);

	/**
	 * Set a reference to the diagram.
	 * 
	 * @param diagram
	 */
	public void setDiagram(WorkflowDiagram diagram);

	/**
	 * Set the overall execution state and detailed state description of the
	 * activity.
	 * 
	 * @return
	 */
	public void setExecutionStateDescriptor(ExecutionStateDescriptor state);

	/**
	 * Set this to true if the iteration id was manually changed by the user
	 * 
	 * @param iterationManuallyChanged
	 */
	public void setIterationManuallyChanged(boolean iterationManuallyChanged);

	public boolean setName(String s);

	public void setParent(StructuredActivity parent);

	public void setVariableList(WorkflowVariableList list);

	public void setVariableModifierList(WorkflowVariableModifierList list);

}
