/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import java.beans.PropertyChangeEvent;
import java.lang.reflect.Method;
import java.util.Map;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LayoutManager;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.CompoundDirectedGraph;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.draw2d.graph.Subgraph;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.TextCellEditor;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.WFConstants;
import de.fzj.unicore.rcp.wfeditor.figures.ActivityFigure;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.activities.SimpleActivity;
import de.fzj.unicore.rcp.wfeditor.policies.ActivityDirectEditPolicy;

/**
 * @author hudsonr Created on Jul 17, 2003
 */
public class SimpleActivityPart extends ActivityPart {

	
	protected LayoutManager layout = new StackLayout();

	protected ActivityFigure activityFigure;

	public SimpleActivityPart() {
	}

	@Override
	protected boolean allowsDirectEdit(Point requestLoc) {
		IFigure label = getFigure().getLabelFigure();
		label.translateToRelative(requestLoc);
		return label.containsPoint(requestLoc)
		&& super.allowsDirectEdit(requestLoc);

	}

	@Override
	public void applyLayout(CompoundDirectedGraph graph, Map<?, ?> map,
			CompoundCommand cmd) {
		Node n = (Node) map.get(this);
		IFigure label = getFigure().getLabelFigure();
		Rectangle labelBounds = label.getBounds().getCopy();
		n.width -= labelBounds.width;
		super.applyLayout(graph, map, cmd);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void contributeNodesToGraph(CompoundDirectedGraph graph, Subgraph s,
			Map map) {
		Node n = new Node(this, s);
		n.outgoingOffset = getAnchorOffset();
		n.incomingOffset = getAnchorOffset();
		ActivityFigure figure = getFigure();

		Rectangle bounds = figure.getBounds().getCopy();

		// account for the difference between where the figure is and where it
		// should be
		Rectangle r = new Rectangle(getModel().getLocation(),getModel().getSize());
		Dimension diff = r.getLocation().getDifference(bounds.getLocation());

		IFigure label = figure.getLabelFigure();
		if(label != null)
		{
			Rectangle labelBounds = label.getBounds().getCopy();
			label.translateToAbsolute(labelBounds);
			figure.translateToRelative(labelBounds);
			labelBounds.x+=diff.width;
			labelBounds.y+= diff.height;
			r.union(labelBounds);
		}
		

		n.x = r.x;
		n.y = r.y;
		n.width = r.width;
		n.height = r.height;

		map.put(this, n);
		graph.nodes.add(n);
	}

	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE,
				new ActivityDirectEditPolicy());
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected ActivityFigure createFigure() {
		activityFigure = new ActivityFigure(this, getImageDescriptor());
		activityFigure.setLabel(getModel().getName());
		return activityFigure;
	}

	@Override
	protected int getAnchorOffset() {
		return 9;
	}

	@Override
	public ActivityFigure getFigure() {
		return (ActivityFigure) super.getFigure();
	}

	
	/**
	 * Return an image representing an unknown activity by default.
	 * Subclasses must override!
	 * @return
	 */
	protected ImageDescriptor getImageDescriptor() {
		IActivity activity = getModel();
		if(activity != null)
		{
			Class<?> clazz = activity.getClass();
			try {
				Method m = clazz.getMethod("getImageDescriptor");
				if(ImageDescriptor.class.equals(m.getReturnType()))
				{
					return (ImageDescriptor) m.invoke(activity,null);
				}
			} catch (Exception e) {
				// do nothing, this is the normal case
			}
			
		
		}
		return WFActivator.getImageDescriptor("empty_activity.gif");
	}

	public SimpleActivity getSimpleActivity() {
		return (SimpleActivity) getModel();
	}

	@Override
	protected void performDirectEdit() {
		if (manager == null) {
			Label l = getFigure().getLabelFigure();
			manager = new ActivityDirectEditManager(this, TextCellEditor.class,
					new ActivityCellEditorLocator(l), l);
		}
		manager.show();
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		if (IActivity.PROP_SIZE.equals(evt.getPropertyName())) {
			getFigure().setDirty(true);
		}
		super.propertyChange(evt);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void registerVisuals() {
		super.registerVisuals();
		// also register the label figure!
		// otherwise selecting the edit part will not work correctly
		getViewer().getVisualPartMap().put(getFigure().getLabelFigure(), this);
	}

	@Override
	protected void unregisterVisuals() {
		super.unregisterVisuals();
		// also unregister the label figure!
		getViewer().getVisualPartMap().remove(getFigure().getLabelFigure());
	}

	@Override
	protected void visualizeBreakpoint() {
		if (getModel().getPropertyValue(IFlowElement.PROP_BREAKPOINT) != null) {
			activityFigure.setBreakpointImage(WFActivator.getDefault()
					.getImageRegistry()
					.get(WFConstants.IMAGE_ENABLED_BREAKPOINT));
		} else {
			activityFigure.setBreakpointImage(null);
		}
	}



}
