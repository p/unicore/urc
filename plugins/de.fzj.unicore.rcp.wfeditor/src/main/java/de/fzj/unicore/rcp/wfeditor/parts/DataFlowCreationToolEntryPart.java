package de.fzj.unicore.rcp.wfeditor.parts;

import org.eclipse.draw2d.ActionEvent;
import org.eclipse.draw2d.ActionListener;
import org.eclipse.draw2d.ButtonModel;
import org.eclipse.draw2d.ChangeEvent;
import org.eclipse.draw2d.ChangeListener;
import org.eclipse.draw2d.CheckBox;
import org.eclipse.draw2d.Clickable;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.Panel;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.internal.ui.palette.editparts.ToolEntryEditPart;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.ui.IEditorPart;

import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.data.DataFlowFactory;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.tools.DataFlowCreationTool;
import de.fzj.unicore.rcp.wfeditor.tools.DataFlowCreationToolEntry;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;

public class DataFlowCreationToolEntryPart extends ToolEntryEditPart {

	private Font dataTypeFont = null;
	private CheckBox[] dataTypeCheckboxes;
	private ButtonModel internal;
	private DataFlowFactory dataFlowFactory;
	private String[] dataTypeNames;
	private ShowDataFlowsProperty showDataFlowsProperty;

	public DataFlowCreationToolEntryPart() {
		super(null);
	}

	@Override
	public void activate() {
		super.activate();

		IFigure toggleLabel = (IFigure) getFigure().getChildren().get(0);
		FontData fd = toggleLabel.getFont().getFontData()[0];
		FontData smaller = new FontData();
		smaller.setHeight(fd.getHeight() - 1);

		IEditorPart part = ((DefaultEditDomain) getViewer().getEditDomain())
				.getEditorPart();
		if (part instanceof WFEditor) {
			WFEditor editor = (WFEditor) part;

			ShowDataFlowsProperty serialized = (ShowDataFlowsProperty) editor
					.getDiagram().getPropertyValue(
							IFlowElement.PROP_SHOW_DATA_FLOWS);
			if (serialized != null) {
				showDataFlowsProperty = serialized.clone();
				showDataFlowsProperty.setShowingFlows(true);
				getModel().setToolProperty(
						DataFlowCreationTool.PROP_SHOW_DATA_FLOWS,
						showDataFlowsProperty.clone());
			}
		}
		dataTypeFont = new Font(getViewer().getControl().getDisplay(), smaller);
		for (int i = 0; i < dataTypeCheckboxes.length; i++) {
			CheckBox c = dataTypeCheckboxes[i];
			c.setFont(dataTypeFont);
			String dataTypeName = dataTypeNames[i];
			String id = dataFlowFactory.getDataTypeId(dataTypeName);
			c.setSelected(showDataFlowsProperty.isShowingFlowsWithDataType(id));
		}

	}

	protected void activateMyTool() {
		if (!internal.isSelected()) {
			internal.setArmed(true);
			internal.setPressed(true);
			internal.setPressed(false);
			internal.setSelected(true);
			internal.setArmed(false);
		}
	}

	@Override
	public IFigure createFigure() {
		if (dataFlowFactory == null) {
			dataFlowFactory = DataFlowFactory.getInstance();
			showDataFlowsProperty = dataFlowFactory
					.getDefaultShowDataFlowsProperty();
			showDataFlowsProperty.setShowingFlows(true);
			dataTypeNames = dataFlowFactory.getDataTypeNames();
		}
		final Clickable result = new Clickable();

		final Clickable toggle = (Clickable) super.createFigure();

		result.getModel().addChangeListener(new ChangeListener() {

			public void handleStateChanged(ChangeEvent event) {
				ButtonModel my = result.getModel();
				internal = toggle.getModel();

				if (ButtonModel.ENABLED_PROPERTY.equals(event.getPropertyName())) {
					internal.setEnabled(my.isEnabled());
				} else if (ButtonModel.SELECTED_PROPERTY.equals(event
						.getPropertyName())) {
					internal.setSelected(my.isSelected());
					if (my.isSelected()) {
						getModel().setToolProperty(
								DataFlowCreationTool.PROP_SHOW_DATA_FLOWS,
								showDataFlowsProperty.clone());
					}
				} else if (ButtonModel.ARMED_PROPERTY.equals(event
						.getPropertyName())) {
					internal.setArmed(my.isArmed());
				} else if (ButtonModel.MOUSEOVER_PROPERTY.equals(event
						.getPropertyName())) {
					internal.setMouseOver(my.isMouseOver());
				} else if (ButtonModel.PRESSED_PROPERTY.equals(event
						.getPropertyName())) {
					internal.setPressed(my.isPressed());
				}
			}
		});
		result.add(toggle);
		GridLayout layout = new GridLayout(3, false);
		result.setLayoutManager(layout);
		IFigure toggleLabel = (IFigure) result.getChildren().get(0);
		result.setConstraint(toggleLabel, new GridData(GridData.BEGINNING,
				GridData.CENTER, true, false, layout.numColumns, 1));

		Label indent = new Label("      ");
		result.add(indent);
		result.setConstraint(indent, new GridData(GridData.BEGINNING,
				GridData.CENTER, false, false));
		dataTypeCheckboxes = new CheckBox[dataTypeNames.length];

		for (int i = 0; i < dataTypeNames.length; i++) {
			if (i > 0 && i % (layout.numColumns - 1) == 0) {
				Panel filler = new Panel();
				result.add(filler);
			}
			String dataTypeName = dataTypeNames[i];
			final String dataTypeId = dataFlowFactory
					.getDataTypeId(dataTypeName);
			final boolean isRegexp = dataFlowFactory.isRegexp(dataTypeId);
			final CheckBox c = new CheckBox(
					dataFlowFactory.getPluralName(dataTypeName));
			if (showDataFlowsProperty.isShowingFlowsWithDataType(dataTypeId)) {
				c.setSelected(true);
			}
			c.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent event) {
					activateMyTool();

					if (c.isSelected()) {
						showDataFlowsProperty.showFlowsWithDataType(dataTypeId,
								isRegexp);
					} else {
						showDataFlowsProperty.hideFlowsWithDataType(dataTypeId,
								isRegexp);
					}

					getModel().setToolProperty(
							DataFlowCreationTool.PROP_SHOW_DATA_FLOWS,
							showDataFlowsProperty.clone());

				}
			});
			dataTypeCheckboxes[i] = c;
			result.add(c);
			result.setConstraint(c, new GridData(GridData.BEGINNING,
					GridData.CENTER, false, false));
		}
		// result.add(toggle);

		return result;
	}

	@Override
	public void deactivate() {
		if (dataTypeFont != null) {
			dataTypeFont.dispose();
		}
	}

	@Override
	public DataFlowCreationToolEntry getModel() {
		return (DataFlowCreationToolEntry) super.getModel();
	}

	@Override
	protected boolean nameNeededInToolTip() {
		return true;
	}

}
