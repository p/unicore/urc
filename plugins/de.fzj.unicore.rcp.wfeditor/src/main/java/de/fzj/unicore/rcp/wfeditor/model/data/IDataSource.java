package de.fzj.unicore.rcp.wfeditor.model.data;

import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import de.fzj.unicore.rcp.common.interfaces.ISerializable;
import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.IPropertySourceWithListeners;

/**
 * A source of data that can be passed to a data sink inside the workflow e.g.
 * file or workflow variable
 * 
 * @author bdemuth
 * 
 */
public interface IDataSource extends IPropertySourceWithListeners, ICopyable,
		ISerializable, Comparable<IDataSource> {

	public static final String PROP_NAME = "name";

	public static final String PROP_PROVIDED_DATA_TYPES = "provided data types property";

	public static final String PROP_DATA_FLOWS = "data flows";

	public void addOutgoingFlow(IDataFlow flow);

	/**
	 * Add a data flow to the list of flows at the given index. Negative indexes
	 * can be used to append at the end of the list: -1 adds as last element, -2
	 * adds as last but one element, etc.
	 * 
	 * @param flow
	 * @param index
	 */
	public void addOutgoingFlow(IDataFlow flow, int index);

	public IDataSource getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags);

	/**
	 * The parent activity that provides the data source.
	 * 
	 * @return
	 */
	public IFlowElement getFlowElement();

	public String getId();

	public String getName();

	/**
	 * A set of identifiers for types the data may have. This may be used to
	 * refer to MIME types or basic data types (integer, double, string...).
	 * Such identifiers can be used to make a match between data sources and
	 * data sinks (if the intersection of the type sets is not empty, the data
	 * may be exchanged).
	 * 
	 * @return
	 */
	public Set<String> getOutgoingDataTypes();

	/**
	 * Returns the flow with the given id if there is an outgoing flow with this
	 * id, else returns null
	 * 
	 * @param id
	 * @return
	 */
	public IDataFlow getOutgoingFlowById(String id);

	public IDataFlow[] getOutgoingFlows();

	public QName getSourceType();

	/**
	 * Returns true iff the given flow is referenced by this data source, i.e.
	 * the flow originates from this source.
	 * 
	 * @param flow
	 * @return
	 */
	public boolean hasOutgoingFlow(IDataFlow flow);

	public boolean isVisible();

	/**
	 * Remove the given flow from the list of flows.
	 * 
	 * @param flow
	 * @return the index of the removed element or -1 if the element was not
	 *         found
	 */
	public int removeOutgoingFlow(IDataFlow flow);

	public void setFlowElement(IFlowElement element);

	/**
	 * The {@link DataSourceList} that holds and manages this data source.
	 * 
	 * @param parentList
	 */
	public void setParentSourceList(DataSourceList parentList);

}
