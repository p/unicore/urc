/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.ui.actions.Clipboard;

import de.fzj.unicore.rcp.wfeditor.model.ICopyable;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.utils.CloneUtils;

/**
 * @author demuth
 */
public class CutCommand extends Command {

	private IFlowElement[] elements;
	private IFlowElement[] elementCopies;
	private Object oldFromClipboard;
	private CompoundCommand childCommands;
	private boolean canceled = false;

	public CutCommand(IFlowElement[] elements) {
		super();
		this.elements = elements;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {

		Set<IFlowElement> toBeCopied = new HashSet<IFlowElement>(
				Arrays.asList(elements));
		List<IFlowElement> l = new ArrayList<IFlowElement>(elements.length);
		for (IFlowElement e : elements) {
			if (e.isDeletable()
					&& !CloneUtils.isBeingCopied(e.getParent(), toBeCopied)) {
				l.add(e);
			}
		}
		elements = l.toArray(new IFlowElement[l.size()]);

		childCommands = new CompoundCommand();
		Set<String> elementIds = new HashSet<String>();
		List<Command> childCommandsInReverseOrder = new ArrayList<Command>();

		// Instead of pushing the original elements to the clipboard, we create
		// exact copies. This is necessary for doing a proper paste later on,
		// cause the original elements are being disposed.
		elementCopies = new IFlowElement[elements.length];
		Map<Object, Object> mapOfCopies = new HashMap<Object, Object>();
		toBeCopied = new HashSet<IFlowElement>(Arrays.asList(elements));
		if (elements != null && elements.length > 0) {
			for (int i = 0; i < elements.length; i++) {
				IFlowElement e = elements[i];
				if (e.isCut() || e.isDisposed()) {
					// do not cut multiple times!
					// this check is necessary, cause each edit policy
					// will create a cut command that deals with ALL selected
					// elements!
					canceled = true;
					return;
				}

				int copyFlags = ICopyable.FLAG_CUT;
				elementCopies[i] = CloneUtils.getFromMapOrCopy(e, toBeCopied,
						mapOfCopies, copyFlags);
				elementCopies[i].setCut(true);
			}

			for (IFlowElement e : elements) {

				if (e instanceof IActivity) {

					IActivity activity = (IActivity) e;
					DeleteCommand deleteOld = null;

					// when cutting, delete old activities first
					deleteOld = new DeleteCommand();
					deleteOld.setParent(e.getParent());
					deleteOld.setChild(activity);
					deleteOld.setDeleteTransitions(true); // we will deal with
															// transitions
															// ourselves
					if (deleteOld.canExecute()) {
						childCommandsInReverseOrder.add(deleteOld); // used for
																	// undoing
																	// the
																	// deletion
					}
				}
			}

			// set cut states in the original diagram appropriately
			IFlowElement first = elements[0];
			for (IActivity act : first.getDiagram().getActivities().values()) {
				boolean cut = elementIds.contains(act.getID());
				SetPropertyCommand setCutCommand = new SetPropertyCommand(act,
						IFlowElement.PROP_CUT, cut);
				childCommandsInReverseOrder.add(setCutCommand);
				for (Transition t : act.getOutgoingTransitions()) {
					cut = elementIds.contains(t.getID());
					setCutCommand = new SetPropertyCommand(t,
							IFlowElement.PROP_CUT, cut);
					childCommandsInReverseOrder.add(setCutCommand);
				}
			}

		}
		for (int i = childCommandsInReverseOrder.size() - 1; i >= 0; i--) {
			childCommands.add(childCommandsInReverseOrder.get(i));
		}
		childCommands.execute();

		// try to save the old clipboard content in order to restore it later
		try {
			oldFromClipboard = Clipboard.getDefault().getContents();
		} catch (Exception e) {
			// ignore
		}
		Clipboard.getDefault().setContents(elementCopies);
	}

	@Override
	public void redo() {
		if (canceled) {
			return;
		}
		try {
			oldFromClipboard = Clipboard.getDefault().getContents();
		} catch (Exception e) {
			// ignore
		}
		childCommands.redo();

		Clipboard.getDefault().setContents(elementCopies);
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		if (canceled) {
			return;
		}
		if (oldFromClipboard != null) {
			Clipboard.getDefault().setContents(oldFromClipboard);
		} else {
			org.eclipse.swt.dnd.Clipboard cb = new org.eclipse.swt.dnd.Clipboard(
					null);
			cb.clearContents();
			cb.dispose();
		}
		childCommands.undo();

	}

}
