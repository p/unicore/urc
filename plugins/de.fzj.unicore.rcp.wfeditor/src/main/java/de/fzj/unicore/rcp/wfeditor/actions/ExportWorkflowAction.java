/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.actions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.xmlbeans.XmlObject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.WorkflowDiagram;
import de.fzj.unicore.rcp.wfeditor.ui.WFEditor;
import de.fzj.unicore.rcp.wfeditor.ui.WFExportRunnable;
import de.fzj.unicore.rcp.wfeditor.ui.WFExportWizard;

/**
 * @author demuth
 * 
 */
public class ExportWorkflowAction extends WFEditorAction {

	public static String ID = "ExportWorkflowAction";
	private WFEditor editor;
	private WFExportWizard wizard;
	public static String UPLOAD_INPUT_FILES = WFExportWizard.UPLOAD_INPUT_FILES;
	private boolean exportRunning;

	public ExportWorkflowAction(IWorkbenchPart part) {
		super(part);
		this.editor = (WFEditor) part;
		setImageDescriptor(WFActivator
				.getImageDescriptor("export_workflow.gif"));
		setId(ID);
		setText("Export");
		setToolTipText("Export the given workflow to an XML file which can be directly submitted to the chosen workflow service (e.g. by a command line client)");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
	 */
	@Override
	protected boolean calculateEnabled() {
		// return true;
		return !isExportRunning() && getWorkflowDiagram().readyForSubmission();
	}

	protected WorkflowDiagram getWorkflowDiagram() {
		return editor.getDiagram();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.actions.WFEditorAction#isAddedToContextMenu()
	 */
	@Override
	public boolean isAddedToContextMenu() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.actions.WFEditorAction#isAddedToLocalToolbar
	 * ()
	 */
	@Override
	public boolean isAddedToLocalToolbar() {
		return true;
	}

	public boolean isExportRunning() {
		return exportRunning;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzj.unicore.rcp.wfeditor.actions.WFEditorAction#run()
	 */
	@Override
	public void run() {
		setExportRunning(true);
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getShell();
		if (shell == null) {
			shell = new Shell(SWT.RESIZE);
		}
		wizard = new WFExportWizard(getWorkflowDiagram());
		wizard.setSubmissionRunnable(new WFExportRunnable() {

			@Override
			public void run(IProgressMonitor progress)
					throws InvocationTargetException, InterruptedException {
				try {
					super.run(progress);
				} finally {
					if (progress.isCanceled()) {
						setExportRunning(false);
					}
				}
			}

			@Override
			protected void saveWorkflow(XmlObject workflow, String fileName)
					throws FileNotFoundException, IOException {
				try {
					//hack to re-encode the "{" and "}"
					String wf=workflow.toString();
					wf = wf.replace("%7B", "{");
					wf = wf.replace("%7D", "}");
					try{
						workflow = XmlObject.Factory.parse(wf);
					}catch(Exception ignored){}
					super.saveWorkflow(workflow, fileName);
				} finally {
					setExportRunning(false);
				}
			}
		});
		WizardDialog dialog = new WizardDialog(shell, wizard);
		dialog.open();

	}

	public void setExportRunning(boolean submissionRunning) {
		this.exportRunning = submissionRunning;
		update();
	}

}
