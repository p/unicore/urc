package de.fzj.unicore.rcp.wfeditor.model;

import java.util.Map;
import java.util.Set;

import de.fzj.unicore.rcp.wfeditor.model.structures.StructuredActivity;

/**
 * This interface is used when copying and pasting {@link IFlowElement}s within
 * a workflow. The flow element's fields should implement it in order to support
 * a "deep" copy that is usually required. If the fields are complex objects
 * having non-native fields, these should implement this interface as well.
 * There are a couple of combinable flags that can influence the copying
 * behavior, see the flag constants below.
 * 
 * @author bdemuth
 * 
 */
public interface ICopyable extends Cloneable {

	/**
	 * The default flag corresponding to the default copying behavior.
	 */
	public final static int FLAG_NONE = 0;

	/**
	 * Flag constant telling a model object whether it will be cut from the
	 * diagram and be replaced by the copy. This is important to know, cause the
	 * element might keep its name, id, connections etc.
	 * 
	 */
	public final static int FLAG_CUT = 1;

	/**
	 * 
	 * This method is supposed to return a copy of the object that can be pasted
	 * in the diagram. It should create a deep clone but remove references to
	 * other elements, GUI parts or listeners.
	 * 
	 * @param mapOfCopies
	 *            There can be multiple reference paths to a given
	 *            {@link ICopyable} object in the object graph. Looking at this
	 *            map can prevent to create multiple copies of such objects.
	 * @param copyFlags
	 *            TODO
	 * @return
	 */
	public ICopyable getCopy(Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags);

	/**
	 * When inserting copied objects to the workflow diagram, the order might be
	 * important. This method allows to specify classes of objects which must be
	 * inserted after objects of this type. Specifying a class implies that
	 * subclasses should be processed afterwards, too.
	 * 
	 * @return a set of classes or an empty set if we don't care
	 */
	public Set<Class<?>> insertAfter();

	/**
	 * When inserting copied objects to the workflow diagram, the order might be
	 * important. This method allows to specify classes of objects which must be
	 * inserted before objects of this type. Specifying a class implies that
	 * subclasses should be processed earlier, too.
	 * 
	 * @return a set of classes or an empty set if we don't care
	 */
	public Set<Class<?>> insertBefore();

	/**
	 * This method is called to fully embed the copied object into the diagram
	 * e.g. after the copied object is pasted into the diagram. It can be used
	 * to tell other parts of the diagram that the pasted object exists.
	 * 
	 * @param toBeCopied
	 * @param mapOfCopies
	 * @param copyFlags
	 */
	public void insertCopyIntoDiagram(ICopyable copy,
			StructuredActivity parent, Set<IFlowElement> toBeCopied,
			Map<Object, Object> mapOfCopies, int copyFlags);

}
