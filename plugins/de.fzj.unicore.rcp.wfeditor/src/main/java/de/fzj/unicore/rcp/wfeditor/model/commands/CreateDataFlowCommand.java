package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.data.DataFlowFactory;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSink;
import de.fzj.unicore.rcp.wfeditor.model.data.IDataSource;
import de.fzj.unicore.rcp.wfeditor.utils.TransitionUtils;

public class CreateDataFlowCommand extends Command {

	private IDataSource source;
	private IDataSink sink;
	private DataFlowFactory factory;
	private DataFlowAssemblyCommand assemblyCommand;
	private Command createdTransitionCommand;

	public CreateDataFlowCommand() {
		super();

	}

	@Override
	public boolean canExecute() {
		return factory != null && factory.canCreateDataFlow(source, sink);
	}

	@Override
	public boolean canUndo() {
		return (assemblyCommand == null || assemblyCommand.canUndo())
				&& (createdTransitionCommand == null || createdTransitionCommand
						.canUndo());
	}

	@Override
	public void execute() {
		try {

			assemblyCommand = factory.getDataFlowAssemblyCommand(source, sink);
			if (assemblyCommand != null && assemblyCommand.canExecute()) {
				assemblyCommand.execute();
				if (assemblyCommand.getDataFlow() != null
						&& source.getFlowElement() instanceof IActivity
						&& sink.getFlowElement() instanceof IActivity) {
					IActivity sourceParent = (IActivity) source
							.getFlowElement();
					IActivity sinkParent = (IActivity) sink.getFlowElement();

					createdTransitionCommand = TransitionUtils
							.ensureControlFlowBetween(sourceParent, sinkParent);

					if (createdTransitionCommand != null
							&& createdTransitionCommand.canExecute()) {
						createdTransitionCommand.execute();
					}

				}
			}

		} catch (Exception e) {
			WFActivator.log(IStatus.ERROR,
					"Unable to create data flow: " + e.getMessage(), e);
		}

	}

	public IDataSink getSink() {
		return sink;
	}

	public IDataSource getSource() {
		return source;
	}

	@Override
	public void redo() {
		assemblyCommand.redo();
		if (createdTransitionCommand != null) {
			createdTransitionCommand.redo();
		}
	}

	public void setSink(IDataSink sink) {
		this.sink = sink;
		if (source != null && sink != null) {
			factory = DataFlowFactory.getInstance();
		}
	}

	public void setSource(IDataSource source) {
		this.source = source;
		if (source != null && sink != null) {
			factory = DataFlowFactory.getInstance();
		}
	}

	@Override
	public void undo() {
		if (assemblyCommand != null) {
			assemblyCommand.undo();
		}
		if (createdTransitionCommand != null) {
			createdTransitionCommand.undo();
		}
	}

}
