/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;

public class BendpointCommand extends Command {

	protected int index;
	protected Point location;
	protected Transition transition;

	protected int getIndex() {
		return index;
	}

	protected Point getLocation() {
		return location;
	}

	protected Transition getTransition() {
		return transition;
	}

	@Override
	public void redo() {
		execute();
	}

	public void setIndex(int i) {
		index = i;
	}

	public void setLocation(Point p) {
		location = p;
	}

	public void setTransition(Transition t) {
		transition = t;
	}

}
