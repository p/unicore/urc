package de.fzj.unicore.rcp.wfeditor.parts;

import java.beans.PropertyChangeEvent;

import de.fzj.unicore.rcp.wfeditor.figures.DataSourceFigure;
import de.fzj.unicore.rcp.wfeditor.model.IFlowElement;
import de.fzj.unicore.rcp.wfeditor.model.data.ShowDataFlowsProperty;
import de.fzj.unicore.rcp.wfeditor.model.variables.VariableModifierDataSource;
import de.fzj.unicore.rcp.wfeditor.model.variables.WorkflowVariable;

public class VariableModifierDataSourcePart extends DataSourcePart {

	@Override
	public VariableModifierDataSource getModel() {
		return (VariableModifierDataSource) super.getModel();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		super.propertyChange(evt);

	}

	@Override
	protected void refreshVisuals() {

		if (!(getFigure() instanceof DataSourceFigure)) {
			return;
		}
		DataSourceFigure figure = (DataSourceFigure) getFigure();

		WorkflowVariable var = getModel().getModifiedVariable();

		if (var == null) {
			figure.setToolTipText(new String[] { "No workflow variable selected." });
			figure.setDataTypeSymbol("?");
		} else {
			String varName = var.getName();
			String typeName = var.getType();
			String symbol = WorkflowVariable.getSymbolFromTypeName(typeName);
			figure.setToolTipText(new String[] {
					"Selected variable: " + varName, "Type: " + typeName });
			figure.setDataTypeSymbol(symbol);
		}
		super.refreshVisuals();
		refreshVisibility((ShowDataFlowsProperty) getActivity()
				.getPropertyValue(IFlowElement.PROP_SHOW_DATA_FLOWS));
	}

}
