/*******************************************************************************
 * Copyright (c) 2003, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package de.fzj.unicore.rcp.wfeditor.model.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.commands.Command;

import de.fzj.unicore.rcp.wfeditor.model.activities.IActivity;
import de.fzj.unicore.rcp.wfeditor.model.transitions.Transition;
import de.fzj.unicore.rcp.wfeditor.utils.TransitionUtils;

/**
 * RemoveTransitionsCommand
 * 
 * @author demuth
 */
public class RemoveIllegalTransitionsCommand extends Command {

	private IActivity child;
	private List<Transition> transitions;

	public RemoveIllegalTransitionsCommand(IActivity child) {
		this.child = child;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		transitions = new ArrayList<Transition>();
		for (Transition t : child.getIncomingTransitions()) {
			if (!TransitionUtils.isOkToKeep(t)) {
				transitions.add(t);
			}
		}
		for (Transition t : child.getOutgoingTransitions()) {
			if (!TransitionUtils.isOkToKeep(t)) {
				transitions.add(t);
			}
		}
		redo();
	}

	/**
	 * Sets the parent to the passed StructuredActivity
	 * 
	 * @param parent
	 *            the parent
	 * 
	 *            /**
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		for (Transition t : transitions) {
			t.dispose();
			t.source.removeOutput(t);
			t.target.removeInput(t);
		}

	}

	/**
	 * Sets the child to the passed Activity
	 * 
	 * @param child
	 *            the child
	 */
	public void setChild(IActivity child) {
		this.child = child;
	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		for (Transition t : transitions) {
			t.source.addOutput(t);
			t.target.addInput(t);
			if (t.isDisposed()) {
				t.undoDispose();
			}
		}
	}

}
