/*********************************************************************************
 * Copyright (c) 2006 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package de.fzj.unicore.rcp.wfeditor.parts;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPolicy;

import de.fzj.unicore.rcp.wfeditor.WFActivator;
import de.fzj.unicore.rcp.wfeditor.figures.ActivityFigure;
import de.fzj.unicore.rcp.wfeditor.figures.ICollapsibleFigure;
import de.fzj.unicore.rcp.wfeditor.figures.IfElseActivityFigure;
import de.fzj.unicore.rcp.wfeditor.model.structures.IfElseActivity;

/**
 * @author demuth
 * 
 */
public class IfElseActivityPart extends SimpleCollapsibleActivityPart {

	protected ActivityFigure collapsedFigure = null;

	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		removeEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.parts.CollapsibleActivityPart#createFigure()
	 */
	@Override
	protected ICollapsibleFigure createFigure() {
		Point loc = getModel().getLocation();
		Dimension size = getModel().getSize();
		Rectangle r = new Rectangle(loc, size);
		ICollapsibleFigure fig = new IfElseActivityFigure(r);
		return fig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzj.unicore.rcp.wfeditor.parts.CollapsibleActivityPart#getCollapsedFigure
	 * ()
	 */
	@Override
	protected ActivityFigure getCollapsedFigure() {
		if (collapsedFigure == null) {
			collapsedFigure = new ActivityFigure(this,
					WFActivator.getImageDescriptor("if.png"));
		}
		return collapsedFigure;
	}

	public IfElseActivity getModel() {
		return (IfElseActivity) super.getModel();
	}

	
}
