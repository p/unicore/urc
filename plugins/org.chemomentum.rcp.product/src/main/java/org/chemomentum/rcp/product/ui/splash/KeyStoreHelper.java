/*********************************************************************************
 * Copyright (c) 2015 Forschungszentrum Juelich GmbH 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * (1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer at the end. Redistributions in
 * binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * (2) Neither the name of Forschungszentrum Juelich GmbH nor the names of its 
 * contributors may be used to endorse or promote products derived from this 
 * software without specific prior written permission.
 * 
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
package org.chemomentum.rcp.product.ui.splash;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

/**
 * @author bjoernh
 *
 * 20.02.2015 12:28:19
 *
 */
public class KeyStoreHelper {

	/**
	 * Create a {@link KeyStore} at {@link File} using _password.
	 * 
	 * <p>
	 * <b>Important:</b> The {@link File} will be overwritten if it exists.
	 * </p>
	 * 
	 * @param _file
	 * @param _password
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 */
	public static void create(File _file, String _password)
			throws KeyStoreException, NoSuchAlgorithmException,
			CertificateException, IOException {
		OutputStream os = new FileOutputStream(_file);
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		ks.load(null, null);
		ks.store(os, _password.toCharArray());
		os.close();
	}

	/**
	 * Check whether a {@link KeyStore} can be loaded from {@link File} _file
	 * using _password.
	 * 
	 * @param _file
	 * @param _password
	 * @throws Throwable
	 */
	public static void tryKeystore(File _file, String _password)
			throws Throwable

 {
		InputStream fisDefault;
		try {
			fisDefault = new FileInputStream(_file);
		} catch (FileNotFoundException e) {
			throw new IOException("Unable to open file "
					+ _file.getAbsolutePath() + ".", e);
		}
		final char[] pwdChar = _password.toCharArray();

		// check default keystore type
		String ksType = KeyStore.getDefaultType();
		checkKeyStoreType(fisDefault, pwdChar, ksType);
		for (String type : new String[] { "JKS", "PKCS12", "JCEKS" }) {
			// check JKS (if not default)
			if (!KeyStore.getDefaultType().equalsIgnoreCase("jks")) {
				checkKeyStoreType(fisDefault, pwdChar, type);
			}
		}

	}

	/**
	 * @param _inputStream
	 * @param _pwdChar
	 * @param _ksType
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 */
	private static void checkKeyStoreType(InputStream _inputStream,
			final char[] _pwdChar, String _ksType) throws IOException,
			NoSuchAlgorithmException, CertificateException {
		try {
			KeyStore.getInstance(_ksType).load(_inputStream,
					_pwdChar);
		} catch (KeyStoreException e) {
			// silently ignore, only thrown by getInstance
		} finally {
			_inputStream.close();
		}
	}
}
