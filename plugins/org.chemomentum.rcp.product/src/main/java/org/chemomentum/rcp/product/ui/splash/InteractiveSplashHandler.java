package org.chemomentum.rcp.product.ui.splash;


import org.eclipse.core.runtime.IProduct;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.branding.IProductConstants;
import org.eclipse.ui.splash.BasicSplashHandler;

import eu.unicore.util.Log;

/**
 * Splash screen allowing the user to configure 
 * the credential and truststore settings
 */
public class InteractiveSplashHandler extends BasicSplashHandler {

	private AuthWizard authWizard;

	private WizardDialog wizardDialog;

	/**
	 * Keep the splash screen visible and prevent the RCP application from
	 * loading until the close button is clicked.
	 */
	private void doEventLoop() {
		Shell splash = getSplash();
		while (!isAuthenticated()) {
			if (splash.getDisplay() != null) {
				if (splash.getDisplay().readAndDispatch() == false) {
					splash.getDisplay().sleep();
				}
			}
		}
	}

	/**
	 * @see org.eclipse.ui.splash.AbstractSplashHandler#init(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	public void init(Shell splash) {
		super.init(splash);
		Shell shell = new Shell(splash);

		authWizard = new AuthWizard(shell);
		wizardDialog = new WizardDialog(shell, authWizard);
		splash.setVisible(false);
		wizardDialog.open();

		boolean OK = false;
		while(!OK){
			try{
				doEventLoop();
				OK = true;
			}catch(Exception ex){
				String message = Log.createFaultMessage("Could not initialise security", ex);
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_ERROR|SWT.APPLICATION_MODAL);
				messageBox.setMessage(message);
				messageBox.open();
				authWizard.reset();
			}
		}
		splash.setVisible(true);
		
		setupProgressMonitor();
		
		getBundleProgressMonitor();
		
	}

	/**
	 * 
	 */
	private void setupProgressMonitor() {
		String progressRectString = null;
		String messageRectString = null;
		String foregroundColorString = null;
		IProduct product = Platform.getProduct();
		if (product != null) {
			progressRectString = product
					.getProperty(IProductConstants.STARTUP_PROGRESS_RECT);
			messageRectString = product
					.getProperty(IProductConstants.STARTUP_MESSAGE_RECT);
			foregroundColorString = product
					.getProperty(IProductConstants.STARTUP_FOREGROUND_COLOR);
		}
		Rectangle progressRect = StringConverter.asRectangle(
				progressRectString, new Rectangle(10, 10, 300, 15));
		setProgressRect(progressRect);

		Rectangle messageRect = StringConverter.asRectangle(messageRectString,
				new Rectangle(10, 35, 300, 15));
		setMessageRect(messageRect);
		if(foregroundColorString != null) {
			String redS = foregroundColorString.substring(0, 2);
			int red = Integer.parseInt(redS, 16);
			String greenS = foregroundColorString.substring(2,4);
			int green = Integer.parseInt(greenS, 16);
			String blueS = foregroundColorString.substring(4, 6);
			int blue = Integer.parseInt(blueS, 16);
			RGB foregroundRGB = new RGB(red, green, blue);
			setForeground(foregroundRGB);
		}
	}

	private boolean isAuthenticated(){
		return authWizard !=null && authWizard.isAuthenticated();
	}

}
