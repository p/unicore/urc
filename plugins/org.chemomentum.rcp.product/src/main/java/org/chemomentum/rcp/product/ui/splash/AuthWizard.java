package org.chemomentum.rcp.product.ui.splash;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import de.fzj.unicore.rcp.identity.IdentityActivator;
import eu.unicore.util.Log;

/**
 * main wizard for credential and truststore settings  
 * @author rajveer
 */
public class AuthWizard extends Wizard {

	private CredentialsPage credentialsPage;
	private TruststorePage truststorePage;
	private Shell shell;
	private IPreferenceStore preferences;
	protected final CredentialUI credentialUI;
	protected final TruststoreUI truststoreUI;
	private volatile boolean fAuthenticated = false;

	public AuthWizard(Shell shell) {
		super();
		this.shell = shell;
		this.preferences = IdentityActivator.getDefault().getPreferenceStore();
		this.credentialUI = new CredentialUI(shell);
		this.truststoreUI = new TruststoreUI(shell);
		credentialsPage = new CredentialsPage();
		truststorePage = new TruststorePage();
		setNeedsProgressMonitor(true);
	}
	
	@Override
	public String getWindowTitle() {
		return "Security Settings";
	}

	@Override
	public void addPages() {
		addPage(credentialsPage);
		addPage(truststorePage);
	}
	
	@Override
	public boolean performFinish() {		
		boolean canFinish = validateUserInput();
		if (canFinish) {
			try {
				// called before exiting the splash - store values and set up
				// the identity plugin
				IdentityActivator id = IdentityActivator.getDefault();
				id.setCredentialChoice(credentialUI.getCredentialChoice());
				if (id.isUsingUnity()) {
					id.setCredentialPassword(credentialUI.getUnityPassword());
				} else {
					id.setCredentialPassword(credentialUI.getKeystorePassword());
				}
				credentialUI.finish(preferences);
				id.setTruststoreChoice(truststoreUI.getTruststoreChoice());
				id.setTruststorePassword(truststoreUI.getKeystorePassword());
				truststoreUI.finish(preferences);
			} catch (Exception ex) {
				String message = Log.createFaultMessage(
						"Could not initialise security", ex);
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_ERROR);
				messageBox.setMessage(message);
				messageBox.open();
			}
		}
		return canFinish;
	}

	@Override
	public boolean performCancel() {
		abort(shell);
		return true;
	}


	private boolean validateUserInput() {
		boolean credentialOK = credentialUI.validateUserInput();
		boolean truststoreOK = truststoreUI.validateUserInput();
		// clear any previous messages
		credentialsPage.setErrorMessage(null);
		truststorePage.setErrorMessage(null);
		
		// set overall status - if true, splash screen will be exited
		fAuthenticated = credentialOK && truststoreOK;
		if (!fAuthenticated) {
			if (credentialOK) {
				if (this.getContainer().getCurrentPage().equals(credentialsPage)) {
					this.getContainer().showPage(truststorePage);
					// this makes sure that focus is set on right widget
					truststoreUI.validateUserInput();
				}
			
				truststorePage
						.setErrorMessage("Please enter valid truststore settings!");
			} else {
				if (this.getContainer().getCurrentPage()
						.equals(truststorePage)) {
					this.getContainer().showPage(credentialsPage);
					credentialUI.validateUserInput();
				}
				
				credentialsPage
						.setErrorMessage("Please enter valid credentials!");
			}
		}
		return fAuthenticated;
	}

	public boolean isAuthenticated() {
		return fAuthenticated;
	}

	public void reset() {
		fAuthenticated = false;
	}

	// Abort the loading of the RCP application
	private void abort(Shell splash) {
		splash.getDisplay().close();
		System.exit(0);
	}

	/**
	 * @author rajveer
	 * 
	 */
	protected class CredentialsPage extends WizardPage {

		private Composite compositeCredential;
		private final static int F_COLUMN_COUNT = 3;

		protected CredentialsPage() {
			super("First Page");
			setTitle("Credentials");
			setDescription("Please enter your credentials.");
		}

		@Override
		public void createControl(Composite parent) {
			compositeCredential = new Composite(parent, SWT.NONE);
			GridLayout layout = new GridLayout(F_COLUMN_COUNT, false);
			compositeCredential.setLayout(layout);
			credentialUI.setup(preferences);
			credentialUI.addContent(compositeCredential);
			setControl(compositeCredential);
			compositeCredential.layout();
			parent.getShell().layout();
			setPageComplete(true);
		}
	}

	protected class TruststorePage extends WizardPage {

		private Composite compositeTruststore;
		private final static int F_COLUMN_COUNT = 3;

		protected TruststorePage() {
			super("Second Page");
			setTitle("Truststore");
			setDescription("Please setup your truststore.");
		}

		@Override
		public void createControl(Composite parent) {
			compositeTruststore = new Composite(parent, SWT.NONE);
			GridLayout layout = new GridLayout(F_COLUMN_COUNT, false);
			compositeTruststore.setLayout(layout);
			truststoreUI.setup(preferences);
			truststoreUI.addContent(compositeTruststore);
			setControl(compositeTruststore);
			compositeTruststore.layout();
			parent.getShell().layout();
			setPageComplete(true);
		}
	}
}
