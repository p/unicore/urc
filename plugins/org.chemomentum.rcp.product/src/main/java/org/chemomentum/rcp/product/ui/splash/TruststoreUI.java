package org.chemomentum.rcp.product.ui.splash;


import java.io.File;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.IdentityActivator.TruststoreChoice;
import de.fzj.unicore.rcp.identity.IdentityConstants;
import de.fzj.unicore.rcp.identity.utils.InputUtils;


/**
 * UI for truststore settings 
 *  
 * @author rsaini
 * @author schuller
 */
public class TruststoreUI {

	private static final int F_TEXT_WIDTH_HINT = 400;
	private static final int PUSH_BUTTON_WIDTH_HINT = 92;
	private static final int COLUMN_COUNT = 3;
	private static final String TOOLTIP_OPENSSL = "A directory in OpenSSL format.";
	private static final String TOOLTIP_DIRECTORY = "A directory containing files named like *.{pem,cert,crt}.";
	private Button buttonKeystore;
	private Button buttonDirectory;
	private Composite compositeTruststoreChoice;
	private Button buttonOpenSSL;
	private Text textKeystore;
	private Button buttonFileChooser;
	private Text textPassword;
	private Button buttonStorePassword;
	private Text textDirectory;

	private String keystorePath;
	private String keystorePassword;
	private boolean storeKeystorePassword;
	private String directory;
	private boolean useDirectory = false;
	private boolean useOpenSSL = false;

	private final Shell shell;

	public TruststoreUI(Shell shell){
		this.shell = shell;
		IPath path = new Path("certs/demouser.jks");
		path = PathUtils.fixInstallationRelativePath(path);
		keystorePath = path.toOSString();
		keystorePassword = "321";
	}

	/**
	 * read initial UI settings from stored preferences
	 * @param preferences
	 */
	public void setup(IPreferenceStore preferences){
		String path = preferences.getString(IdentityConstants.P_TRUSTSTORE_PATH);
		boolean storePassword = Boolean.parseBoolean(preferences.getString(IdentityConstants.P_TRUSTSTORE_STORE_PASSWORD));
		if("".equals(path)){
			path=keystorePath;
		}
		else if(!storePassword){
			//user has stored the path, but not the password, so
			//the password field should be initially empty
			keystorePassword="";
		}
		String type = preferences.getString(IdentityConstants.P_TRUSTSTORE_TYPE);
		useDirectory = !"".equals(type) && !"KEYSTORE".equals(type);

		if(useDirectory){
			setDirectory(path);
			boolean openSSL = Boolean.parseBoolean(preferences.getString(IdentityConstants.P_TRUSTSTORE_OPENSSL));
			setUseOpenSSL(openSSL);
		}
		else {
			setKeystorePath(path);
			if(storePassword){
				keystorePassword = preferences.getString(IdentityConstants.P_TRUSTSTORE_PASSWORD);
				setStoreKeystorePassword(true);
			}
			else if (!"".equals(path) && !keystorePath.equals(path)) {
				//user has stored the path, but not the password, so
				//the password field should be empty
				keystorePassword="";
			}
			setKeystorePassword(keystorePassword);
		}
	}

	/**
	 * called before exiting the splash screen - store user input in preferences
	 */
	public void finish(IPreferenceStore preferences){
		preferences.putValue(IdentityConstants.P_TRUSTSTORE_TYPE, getTruststoreChoice().toString());
		if(isUseKeystore()){
			preferences.putValue(IdentityConstants.P_TRUSTSTORE_PATH, getKeystorePath());
		}
		else{
			preferences.putValue(IdentityConstants.P_TRUSTSTORE_PATH, getDirectory());
			preferences.putValue(IdentityConstants.P_TRUSTSTORE_OPENSSL, String.valueOf(isOpenSSL()));
		}
		preferences.putValue(IdentityConstants.P_TRUSTSTORE_STORE_PASSWORD, String.valueOf(isStoreKeystorePassword()));
		if(isStoreKeystorePassword()){
			preferences.putValue(IdentityConstants.P_TRUSTSTORE_PASSWORD, getKeystorePassword());
		}
	}

	public void addContent(Composite compositeTruststore) {

		GridLayout layout = new GridLayout(COLUMN_COUNT, false);
		compositeTruststore.setLayout(layout); 

		new Label(compositeTruststore, SWT.NONE);
		new Label(compositeTruststore, SWT.NONE);
		new Label(compositeTruststore, SWT.NONE);

		// deals with the selection events triggered by the radio buttons 
		SelectionAdapter selectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent evt) {
				activate((Button)evt.getSource());
			}
		};

		buttonKeystore = new Button(compositeTruststore, SWT.RADIO);
		buttonKeystore.setText("Keystore");
		buttonKeystore.addSelectionListener(selectionAdapter);	
		buttonKeystore.setSelection(!useDirectory);
		buttonKeystore
				.setToolTipText("Use a local keystore containing trusted certificates.");

		buttonDirectory = new Button(compositeTruststore, SWT.RADIO);
		buttonDirectory.setText("Directory");	
		buttonDirectory.addSelectionListener(selectionAdapter);
		buttonDirectory.setSelection(useDirectory && !useOpenSSL);
		buttonDirectory.setToolTipText(TOOLTIP_DIRECTORY);

		buttonOpenSSL = new Button(compositeTruststore, SWT.RADIO);
		buttonOpenSSL.setText("OpenSSL Directory");
		buttonOpenSSL.addSelectionListener(selectionAdapter);
		buttonOpenSSL.setSelection(useDirectory && useOpenSSL);
		buttonOpenSSL.setToolTipText(TOOLTIP_OPENSSL);

		// filler row
		new Label(compositeTruststore, SWT.NONE);
		new Label(compositeTruststore, SWT.NONE);
		new Label(compositeTruststore, SWT.NONE);

		// details for the truststore choices
		compositeTruststoreChoice = new Composite(compositeTruststore, SWT.NONE);
		compositeTruststoreChoice.setLayout(new GridLayout(COLUMN_COUNT, false));
		GridData gdata = new GridData(SWT.LEFT, SWT.TOP, true, false, 3, 2);		
		compositeTruststoreChoice.setLayoutData(gdata);

		if(useDirectory){
			setCompositeDirectory(compositeTruststoreChoice);
		}
		else{
			setCompositeKeystore(compositeTruststoreChoice);
		}

		compositeTruststore.layout();			
	}

	private void storeKeystoreSettings(){
		if(buttonStorePassword!=null && !buttonStorePassword.isDisposed()){
			storeKeystorePassword = buttonStorePassword.getSelection();
		}
		if(textKeystore!=null && !textKeystore.isDisposed() && textKeystore.getText()!=null){
			keystorePath = textKeystore.getText();
		}
	}

	private void activate(Button sourceButton) {
		if( textDirectory!=null && !textDirectory.isDisposed() && textDirectory.getText() != null){
			directory = textDirectory.getText();
		}
		if (buttonKeystore.getSelection() && buttonKeystore == sourceButton) {
			for (Widget widget : compositeTruststoreChoice.getChildren()) {
				widget.dispose();
			}
			setCompositeKeystore(compositeTruststoreChoice);
			compositeTruststoreChoice.layout();
		} else if (buttonDirectory.getSelection()
				|| buttonOpenSSL.getSelection()) {
			storeKeystoreSettings();
			for (Widget widget : compositeTruststoreChoice.getChildren()) {
				widget.dispose();
			}
			setCompositeDirectory(compositeTruststoreChoice);				
			compositeTruststoreChoice.layout();
		}
	}

	private void setCompositeDirectory(Composite compositeTruststoreChoice) {
		Label label = new Label(compositeTruststoreChoice, SWT.NONE);
		GridData data = new GridData(SWT.LEFT, SWT.CENTER,false, false);
		label.setLayoutData(data);
		label.setText("Directory:"); 
		textDirectory = new Text(compositeTruststoreChoice, SWT.BORDER);		
		data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		data.widthHint = F_TEXT_WIDTH_HINT;
		textDirectory.setLayoutData(data);
		if(directory!=null){
			textDirectory.setText(directory);
		}
		Button directoryChooser = new Button(compositeTruststoreChoice, SWT.PUSH);
		directoryChooser.setText("Browse...");
		data = new GridData(SWT.LEFT, SWT.CENTER,false, false);
		data.widthHint = PUSH_BUTTON_WIDTH_HINT;
		directoryChooser.setLayoutData(data);
		directoryChooser.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent evt) {
				File f = getDirectory(textDirectory.getText());
				if (f != null) {
					textDirectory.setText(f.getAbsolutePath());
				}
			}
		});
		// filler rows
		GridData explLayout = new GridData();
		explLayout.horizontalSpan = 2;
		Label expl = new Label(compositeTruststoreChoice, SWT.NONE);
		expl.setLayoutData(explLayout);
		if (buttonOpenSSL.getSelection()) {
			expl.setText(TOOLTIP_OPENSSL);
		} else {
			expl.setText(TOOLTIP_DIRECTORY);
		}
		for (int i = 0; i < 5; i++) {
			new Label(compositeTruststoreChoice, SWT.NONE);
		}		
	}

	private void setCompositeKeystore(Composite compositeTruststoreChoice) {
		Label label = new Label(compositeTruststoreChoice, SWT.NONE);
		GridData data = new GridData(SWT.LEFT, SWT.CENTER,true, false);
		label.setLayoutData(data);
		label.setText("&File:"); //$NON-NLS-1$		

		textKeystore = new Text(compositeTruststoreChoice, SWT.BORDER);		
		data = new GridData(SWT.LEFT, SWT.CENTER, true, false);
		data.widthHint = F_TEXT_WIDTH_HINT;
		textKeystore.setLayoutData(data);
		textKeystore.setText(keystorePath);

		buttonFileChooser = new Button(compositeTruststoreChoice, SWT.PUSH);
		buttonFileChooser.setText("Browse...");
		data = new GridData();
		data.widthHint = PUSH_BUTTON_WIDTH_HINT;
		buttonFileChooser.setLayoutData(data);
		buttonFileChooser.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent evt) {
				File f = getFile(textKeystore.getText());
				if (f != null) {
					textKeystore.setText(f.getAbsolutePath());
					if (!f.getAbsolutePath().equals(keystorePath)) {
						textPassword.setText("");
						textPassword.setFocus();
					}
				}
			}
		});

		label = new Label(compositeTruststoreChoice, SWT.NONE);
		label.setText("&Password:"); //$NON-NLS-1$	
		data = new GridData();
		label.setLayoutData(data);

		textPassword = new Text(compositeTruststoreChoice, SWT.PASSWORD | SWT.BORDER);
		data = new GridData(SWT.NONE, SWT.NONE, false, false);
		data.widthHint = F_TEXT_WIDTH_HINT;
		textPassword.setLayoutData(data);
		textPassword.setText(keystorePassword);
		buttonStorePassword = new Button(compositeTruststoreChoice, SWT.CHECK);

		buttonStorePassword.setText("Save");
		data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		data.horizontalIndent = 1;
		buttonStorePassword.setSelection(storeKeystorePassword);
		buttonStorePassword.setLayoutData(data);	
		buttonStorePassword.setToolTipText("Saves password in your preferences");
	}

	public boolean validateUserInput() {
		TruststoreChoice choice = getTruststoreChoice();
		boolean OK = false;
		switch (choice) {

		case KEYSTORE:
			OK = validateUserInputKeystore();
			break;

		case DIRECTORY:
		case OPENSSL:
			OK = validateUserInputDirectory();
			break;

		default:
			break;
		}

		return OK;
	}

	/**
	 * check user input for the "keystore" choice
	 */
	private boolean validateUserInputKeystore() {
		String keystore = textKeystore.getText();
		if (keystore.isEmpty()) {
			textKeystore.setFocus();
			return false;
		}
		keystorePassword = textPassword.getText();
		File f = new File(keystore);
		if (!f.exists()) {
			MessageDialog message = new MessageDialog(shell, "Note",
					null, IdentityActivator.QUESTION_CREATE_KEYSTORE + keystore
					+ "?", MessageDialog.QUESTION, new String[] { "Ok",
			"Cancel" }, 0);
			int result = message.open();
			boolean canceled = Window.CANCEL == result;
			if (canceled) {
				textKeystore.setFocus();
				return false;
			} else {
				try {
					keystorePassword = new InputUtils().askPassword(f);
					KeyStoreHelper.create(f, keystorePassword);
				} catch (Exception e) {
					MessageDialog.openError(shell,
							"Error opening truststore",
							"Error opening truststore: "+e.getMessage());
					return false;
				}
			}
		} else if (keystorePassword.isEmpty()) {
			textPassword.setFocus();
			return false;
		}

		// file exists and we have a password
		try {
			KeyStoreHelper.tryKeystore(f, keystorePassword);
			// OK - store preferences now
			storeKeystorePassword = buttonStorePassword.getSelection();
			keystorePath = keystore;
		} catch (Throwable e) {
			MessageDialog.openError(shell,"Error opening truststore",
					"Error opening truststore: "+e.getMessage());
			textKeystore.setFocus();
			return false;
		}
		return true;
	}

	private boolean validateUserInputDirectory(){
		directory = textDirectory.getText();
		useOpenSSL = TruststoreChoice.OPENSSL == getTruststoreChoice();
		boolean OK = true;
		File f = new File(directory);
		if(!f.exists()){
			askCreateDirectory(directory);
			if(!f.exists()){
				OK = false;
			}
		}
		else{
			if(!f.isDirectory()){
				warnNoDirectory();
				textDirectory.setFocus();
				OK = false;
			}
		}
		return OK;
	}

	private boolean askCreateDirectory(final String path){
		MessageDialog message = new MessageDialog(shell, "Note",
				null, "Truststore directory " + path + " does not exist. Create it"
						+ "?", MessageDialog.QUESTION, new String[] { "Ok",
		"Cancel" }, 0);
		int result = message.open();
		boolean OK = Window.OK == result;
		if (OK) {
			OK = new File(path).mkdirs();
			if(!OK){
				MessageDialog.openError(shell, "Could not create directory",
						"Could not create directory '"+path+
						"'. Please select a different path.");
			}
		}
		return OK;
	}

	private void warnNoDirectory(){
		MessageDialog.openError(shell,
				"Not a directory",
				"Truststore path is not a directory! Please select a different path.");
	}

	/**
	 * Helper to open the file chooser dialog.
	 * 
	 * @param startingDirectory
	 *            the directory to open the dialog on.
	 * @return File The File the user selected or <code>null</code> if they do
	 *         not.
	 */
	private File getFile(String path) {
		FileDialog dialog = new FileDialog(shell, SWT.OPEN | SWT.SHEET);
		dialog.setFilterExtensions(Constants.REUSABLE_KEYSTORE_FORMATS);
		File f = new File(path);
		File parent = f.getParentFile();
		if (parent!=null && parent.exists() && parent.canRead()) {
			path = parent.getAbsolutePath() + File.separator;
		} else {
			path = null;
		}
		dialog.setFilterPath(path);
		String file = dialog.open();
		if (file != null) {
			file = file.trim();
			if (file.length() > 0) {
				return new File(file);
			}
		}
		return null;
	}

	private File getDirectory(String path) {
		DirectoryDialog dialog = new DirectoryDialog(shell, SWT.SHEET);
		File f = new File(path);
		File parent = f.getParentFile();
		if (parent!=null && parent.exists() && parent.canRead()) {
			path = parent.getAbsolutePath() + File.separator;
		} else {
			path = null;
		}
		dialog.setFilterPath(path);
		String file = dialog.open();
		if (file != null) {
			file = file.trim();
			if (file.length() > 0) {
				return new File(file);
			}
		}
		return null;
	}

	public boolean isUseKeystore(){
		return TruststoreChoice.KEYSTORE == getTruststoreChoice();
	}

	public TruststoreChoice getTruststoreChoice(){
		if (buttonKeystore.getSelection()) {				
			return TruststoreChoice.KEYSTORE;
		}
		else if (buttonDirectory.getSelection()) {	
			return TruststoreChoice.DIRECTORY;
		}
		else if (buttonOpenSSL.getSelection()) {	
			return TruststoreChoice.OPENSSL;
		}
		throw new IllegalStateException();
	}

	public String getKeystorePath() {
		return keystorePath;
	}

	public void setKeystorePath(String keystorePath) {
		this.keystorePath = keystorePath;
	}

	public String getKeystorePassword() {
		return keystorePassword;
	}

	public void setKeystorePassword(String keystorePassword) {
		this.keystorePassword = keystorePassword;
	}

	public boolean isStoreKeystorePassword() {
		return storeKeystorePassword;
	}

	public void setStoreKeystorePassword(boolean storeKeystorePassword) {
		this.storeKeystorePassword = storeKeystorePassword;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public boolean isOpenSSL(){
		return useOpenSSL;
	}

	public void setUseOpenSSL(boolean useOpenSSL){
		this.useOpenSSL=useOpenSSL;
	}

	public void setUseDirectory(boolean useDirectory){
		this.useDirectory=useDirectory;
	}
}
