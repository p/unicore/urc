/**
 * 
 */
package org.chemomentum.rcp.product.problems;

import de.fzj.unicore.rcp.logmonitor.problems.GraphicalSolution;
import eu.unicore.problemutil.Problem;

/**
 * Use {@link UpdateManagerUI} in order to help the user update the client. This
 * must be changed when switching to p2 for client updates..
 * 
 * @author bdemuth
 *
 * @deprecated Does not do anything at the moment. Will need to be updated once
 *             online updates work again. (bjoernh - 2015-04-29)
 */
public class UpdateClientSolution extends GraphicalSolution
{

	/* (non-Javadoc)
	 * @see de.fzj.unicore.rcp.logmonitor.problems.GraphicalSolution#solve(java.lang.String, eu.unicore.problemutil.Problem[], java.lang.String)
	 */
	public UpdateClientSolution()
	{
		super("UPDATE_CLIENT",
				"CLIENT_OUTDATED",
				"Please update the client.");
	}
	
	public boolean isAvailableFor(String message, Problem[] problems,
			String details) {
		try
		{
			// UpdateManagerUI manager = new UpdateManagerUI();
			return true;
		} catch (Throwable t)
		{
			return false;
		}
	}

	@Override
	public boolean solve(String message, Problem[] problems, String details)
			throws Exception
	{
		// UpdateManagerUI.openConfigurationManager(getParentShell());
		return true;
	}

}
