package org.chemomentum.rcp.product.ui.splash;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

import de.fzj.unicore.rcp.common.Constants;
import de.fzj.unicore.rcp.common.utils.PathUtils;
import de.fzj.unicore.rcp.identity.IdentityActivator;
import de.fzj.unicore.rcp.identity.IdentityActivator.CredentialChoice;
import de.fzj.unicore.rcp.identity.IdentityConstants;
import de.fzj.unicore.rcp.identity.utils.InputUtils;

/**
 * UI for credential settings 
 *  
 * @author rsaini
 * @author schuller
 */
public class CredentialUI{

	private final static int F_TEXT_WIDTH_HINT = 400;
	private final static int PUSH_BUTTON_WIDTH_HINT = 92;
	final static int F_COLUMN_COUNT = 3;

	private final Shell shell;

	private Composite compositeCredentialChoice;

	private Button btnKeystore;
	private Button btnUnity;

	private SelectionAdapter credentialSelectionAdapter;

	private Text fTextKeystore;
	private Text fTextPassword;

	private Button fButtonFileChooser;

	private Button fButtonStorePassword;

	private Text fTextMyProxyURL;
	private Label lblUsername;
	private Label lblMyProxy;
	private Label lblMyProxyPassword;
	private Text fTextMyProxyUser;
	private Text fTextMyProxyPassword;
	private Button btnMyProxyEnable;

	private Text fTextUnityURL;
	private Text fTextUnityUsername;
	private Text fTextUnityPassword;
	private Button btnSaveLoginUnity;


	private String keystorePath;
	private String keystorePassword;
	private boolean storePassword;

	private boolean myProxyEnabled;
	private String myProxyHost;
	private String myProxyUser;
	private String myProxyPassword;

	private String unityHost;
	private String unityPassword;
	private String unityUsername;
	private boolean unityStorePassword;

	private boolean initiallyUseKeystore = true;

	/**
	 * initialise GUI using defaults
	 * @param shell
	 */
	public CredentialUI(Shell shell){
		this.shell = shell;
		IPath path = new Path("certs/demouser.jks");
		path = PathUtils.fixInstallationRelativePath(path);
		keystorePath = path.toOSString();
		keystorePassword = "321";
		storePassword = false;
	}

	/**
	 * read initial UI settings from stored preferences
	 * @param preferences
	 */
	public void setup(IPreferenceStore preferences){
		if("UNITY".equals(preferences.getString(IdentityConstants.P_CREDENTIAL_TYPE))){
			setInitiallyUseKeystore(false);
		}
		boolean storePassword = Boolean.parseBoolean(preferences.getString(IdentityConstants.P_STORE_PASSWORD));
		String path = preferences.getString(IdentityConstants.P_CREDENTIAL_PATH);
		if("".equals(path)){
			path=keystorePath;
		}
		else if(!storePassword){
			//user has stored the path, but not the password, so
			//the password field should be initially empty
			keystorePassword="";
		}
		setKeystorePath(path);
		if(storePassword){
			keystorePassword = preferences.getString(IdentityConstants.P_CREDENTIAL_PASSWORD);
			setStorePassword(true);
		}
		setKeystorePassword(keystorePassword);

		myProxyEnabled = Boolean.parseBoolean(preferences.getString(IdentityConstants.P_MYPROXY_ENABLED));
		setMyProxyHost(preferences.getString(IdentityConstants.P_MYPROXY_HOST));
		setMyProxyUser(preferences.getString(IdentityConstants.P_MYPROXY_USER));
		setMyProxyPassword(preferences.getString(IdentityConstants.P_MYPROXY_PASSWORD));

		setUnityHost(preferences.getString(IdentityConstants.P_UNITY_HOST));
		setUnityUsername(preferences.getString(IdentityConstants.P_UNITY_USER));
		setUnityPassword(preferences.getString(IdentityConstants.P_UNITY_PASSWORD));
		setUnityStorePassword(preferences.getBoolean(IdentityConstants.P_UNITY_STORE_LOGIN));
	}

	/**
	 * called before exiting the splash screen - store UI settings in preferences
	 */
	public void finish(IPreferenceStore preferences){
		preferences.putValue(IdentityConstants.P_CREDENTIAL_TYPE, getCredentialChoice().toString());
		preferences.putValue(IdentityConstants.P_CREDENTIAL_PATH, getKeystorePath());
		preferences.putValue(IdentityConstants.P_STORE_PASSWORD, String.valueOf(isStorePassword()));
		if(isStorePassword()){
			preferences.putValue(IdentityConstants.P_CREDENTIAL_PASSWORD, getKeystorePassword());
		}
		else{
			// delete any existing old value
			preferences.putValue(IdentityConstants.P_CREDENTIAL_PASSWORD, "");
		}
		preferences.putValue(IdentityConstants.P_MYPROXY_ENABLED, String.valueOf(isMyProxyEnabled()));
		if(isMyProxyEnabled()){
			preferences.putValue(IdentityConstants.P_MYPROXY_HOST, myProxyHost);
			preferences.putValue(IdentityConstants.P_MYPROXY_USER, myProxyUser);
			preferences.putValue(IdentityConstants.P_MYPROXY_PASSWORD, myProxyPassword);
		}

		preferences.putValue(IdentityConstants.P_UNITY_HOST, unityHost);
		preferences.putValue(IdentityConstants.P_UNITY_USER, unityUsername);
		preferences.putValue(IdentityConstants.P_UNITY_STORE_LOGIN, Boolean.toString(unityStorePassword));
		if(unityStorePassword){
			preferences.putValue(IdentityConstants.P_UNITY_PASSWORD, unityPassword);
		}
		else{
			// delete any existing old value
			preferences.putValue(IdentityConstants.P_UNITY_PASSWORD, "");
		}

	}


	/**
	 * creates GUI
	 */
	public void addContent(final Composite parent) {
		// space filler before first label
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);

		credentialSelectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent evt) {
				Button btn = (Button)evt.getSource();
				if(btn.getSelection()){
					CredentialChoice choice = getCredentialChoice();
					switch (choice) {
					case KEYSTORE:
						storeUnitySettings();
						for (Widget widget : compositeCredentialChoice.getChildren()) {
							widget.dispose();
						}
						setCompositeKeystore(compositeCredentialChoice);
						break;
					case UNITY:
						storeKeystoreSettings();
						for (Widget widget : compositeCredentialChoice.getChildren()) {
							widget.dispose();
						}
						setCompositeUnity(compositeCredentialChoice);
						break;
					default:
						break;
					}
				}
				parent.layout();
			}
		};

		// create first radio button "Keystore"	
		btnKeystore = new Button(parent, SWT.RADIO);
		btnKeystore.setText("Keystore");
		btnKeystore.addSelectionListener(credentialSelectionAdapter);	
		btnKeystore.setSelection(initiallyUseKeystore);
		btnKeystore.setToolTipText("Use a local keystore containing your keys");		

		// create second radio button "Unity"	
		btnUnity = new Button(parent, SWT.RADIO);
		btnUnity.setText("Unity");
		GridData gd = new GridData(SWT.LEFT, SWT.TOP, false, false);
		gd.horizontalIndent = 25;
		btnUnity.setLayoutData(gd);
		btnUnity.setSelection(!initiallyUseKeystore);
		btnUnity.setToolTipText("Login to a Unity server");

		// placeholder for login choices, by default is Keystore login activated
		compositeCredentialChoice = new Composite(parent, SWT.NONE);
		compositeCredentialChoice.setLayout(new GridLayout(F_COLUMN_COUNT, false));
		GridData gdata = new GridData(SWT.LEFT, SWT.TOP, true, false, 2, 2);		
		compositeCredentialChoice.setLayoutData(gdata);	
		if(initiallyUseKeystore){
			setCompositeKeystore(compositeCredentialChoice);
		}
		else{
			setCompositeUnity(compositeCredentialChoice);
		}
		btnUnity.addSelectionListener(credentialSelectionAdapter);
		new Label(parent, SWT.NONE);

		parent.layout();		
	}

	private void storeUnitySettings(){
		if(fTextUnityURL !=null && fTextUnityURL.getText()!=null){
			unityHost = fTextUnityURL.getText();
		}
		if(fTextUnityUsername !=null && fTextUnityUsername.getText()!=null){
			unityUsername = fTextUnityUsername.getText();
		}
		if(fTextUnityPassword !=null && fTextUnityPassword.getText()!=null){
			unityPassword = fTextUnityPassword.getText();
		}
	}

	private void storeKeystoreSettings(){
		if(fButtonStorePassword!=null){
			storePassword = fButtonStorePassword.getSelection();
		}
		if(fTextKeystore!=null && fTextKeystore.getText()!=null){
			keystorePath = fTextKeystore.getText();
		}
		if(fTextMyProxyURL!=null && fTextMyProxyURL.getText()!=null){
			myProxyHost = fTextMyProxyURL.getText();
		}
		if(fTextMyProxyUser!=null && fTextMyProxyUser.getText()!=null){
			myProxyUser = fTextMyProxyUser.getText();
		}
		if(fTextMyProxyPassword!=null && fTextMyProxyPassword.getText()!=null){
			myProxyPassword = fTextMyProxyPassword.getText();
		}
	}

	// Creates UI for Keystore login 
	private void setCompositeKeystore(Composite compositeKeystore) {
		// Keystore path label
		Label label = new Label(compositeKeystore, SWT.NONE);
		label.setText("File:"); //$NON-NLS-1$		
		GridData data = new GridData(SWT.LEFT, SWT.CENTER,true, false);
		label.setLayoutData(data);

		fTextKeystore = new Text(compositeKeystore, SWT.BORDER);
		data = new GridData(SWT.NONE, SWT.NONE, true, false);
		data.widthHint = F_TEXT_WIDTH_HINT;
		fTextKeystore.setLayoutData(data);
		if(keystorePath!=null)fTextKeystore.setText(keystorePath);

		fButtonFileChooser = new Button(compositeKeystore, SWT.PUSH);
		fButtonFileChooser.setText("Browse...");
		fButtonFileChooser.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent evt) {
				File f = getFile(fTextKeystore.getText());
				if (f != null) {
					fTextKeystore.setText(f.getAbsolutePath());
					if (!(f.getAbsolutePath().equals(keystorePath))) {
						fTextPassword.setText("");
						fTextPassword.setFocus();
					}
				}
			}
		});
		data = new GridData();
		data.widthHint = PUSH_BUTTON_WIDTH_HINT;
		fButtonFileChooser.setLayoutData(data);

		// Password label
		label = new Label(compositeKeystore, SWT.NONE);
		label.setText("&Password:"); //$NON-NLS-1$		

		fTextPassword = new Text(compositeKeystore, SWT.PASSWORD | SWT.BORDER);
		fTextPassword.setText(keystorePassword);
		data = new GridData(SWT.NONE, SWT.NONE, false, false);
		data.widthHint = F_TEXT_WIDTH_HINT;
		fTextPassword.setLayoutData(data);

		fButtonStorePassword = new Button(compositeKeystore, SWT.CHECK);
		fButtonStorePassword.setText("Save");
		fButtonStorePassword.setToolTipText("Saves password in your preferences");

		fButtonStorePassword.setSelection(storePassword);
		data = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		data.horizontalIndent = 1;
		fButtonStorePassword.setLayoutData(data);	

		// add myproxy options
		createMyProxyUI(compositeKeystore);

		Text spacefiller = new Text(compositeKeystore, SWT.BORDER);
		spacefiller.setText("");
		spacefiller.setVisible(false);

	}

	// Creates UI for MyProxy options 
	private void createMyProxyUI(Composite panel) {

		// create third radio button "My Proxy"
		btnMyProxyEnable = new Button(panel, SWT.CHECK);
		btnMyProxyEnable.setText("Download credential from MyProxy");
		GridData gridData = new GridData(SWT.LEFT, SWT.TOP, false, true);
		gridData.horizontalSpan = F_COLUMN_COUNT;
		btnMyProxyEnable.setLayoutData(gridData);			

		lblMyProxy = new Label(panel, SWT.NONE);
		lblMyProxy.setText("My Proxy Address:");
		fTextMyProxyURL = new Text(panel, SWT.BORDER);
		GridData gd = new GridData();
		gd.widthHint = F_TEXT_WIDTH_HINT;
		gd.horizontalSpan = 2;		
		gd.grabExcessHorizontalSpace = true;
		fTextMyProxyURL.setLayoutData(gd);		
		if(myProxyHost!=null)fTextMyProxyURL.setText(myProxyHost);


		lblUsername = new Label(panel, SWT.NONE); 
		lblUsername.setText("Username:");

		fTextMyProxyUser = new Text(panel, SWT.BORDER);
		gd = new GridData();
		gd.widthHint = F_TEXT_WIDTH_HINT;
		fTextMyProxyUser.setLayoutData(gd);
		if(myProxyUser!=null)fTextMyProxyUser.setText(myProxyUser);

		new Label(panel, SWT.NONE);

		lblMyProxyPassword = new Label(panel, SWT.NONE); 
		lblMyProxyPassword.setText("Password:");

		fTextMyProxyPassword = new Text(panel, SWT.PASSWORD | SWT.BORDER);
		gd = new GridData();
		gd.widthHint = F_TEXT_WIDTH_HINT;
		gd.grabExcessHorizontalSpace = true;
		fTextMyProxyPassword.setLayoutData(gd);
		if(myProxyPassword!=null)fTextMyProxyPassword.setText(myProxyPassword);
		// myproxy checkbox enables / disables components
		btnMyProxyEnable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setMyProxyEnabled(btnMyProxyEnable.getSelection());
			}

		});
		boolean initiallyEnabled = myProxyEnabled;
		btnMyProxyEnable.setSelection(initiallyEnabled);
		setMyProxyEnabled(initiallyEnabled);		
	}


	private void setMyProxyEnabled(boolean enabled){
		fTextMyProxyPassword.setEnabled(enabled);
		lblMyProxyPassword.setEnabled(enabled);
		fTextMyProxyURL.setEnabled(enabled);
		lblMyProxy.setEnabled(enabled);
		fTextMyProxyUser.setEnabled(enabled);
		lblUsername.setEnabled(enabled); 
		myProxyEnabled = enabled;
	}

	// Creates UI for Unity login 
	private void setCompositeUnity(Composite compositeUnity) {

		Label unitylabel = new Label(compositeUnity, SWT.NONE);
		unitylabel.setText("Unity Address:");

		fTextUnityURL = new Text(compositeUnity, SWT.BORDER);
		GridData gd = new GridData();
		gd.widthHint = F_TEXT_WIDTH_HINT;
		gd.horizontalSpan = 2;
		gd.horizontalIndent = 8;
		gd.grabExcessHorizontalSpace = true;
		fTextUnityURL.setLayoutData(gd);
		if(unityHost!=null)fTextUnityURL.setText(unityHost);

		Label usernameLbl = new Label(compositeUnity, SWT.NONE); 
		usernameLbl.setText("Username:");

		fTextUnityUsername = new Text(compositeUnity, SWT.BORDER);
		gd = new GridData();
		gd.horizontalIndent = 8;
		gd.widthHint = F_TEXT_WIDTH_HINT;
		fTextUnityUsername.setLayoutData(gd);
		if(unityUsername!=null)fTextUnityUsername.setText(unityUsername);

		new Label(compositeUnity, SWT.NONE);

		Label passwdLbl = new Label(compositeUnity, SWT.NONE); 
		passwdLbl.setText("Password:");

		fTextUnityPassword = new Text(compositeUnity, SWT.PASSWORD | SWT.BORDER);
		gd = new GridData();
		gd.widthHint = F_TEXT_WIDTH_HINT;
		gd.horizontalIndent = 8;
		gd.grabExcessHorizontalSpace = true;
		fTextUnityPassword.setLayoutData(gd);
		if(unityPassword!=null)fTextUnityPassword.setText(unityPassword);


		btnSaveLoginUnity = new Button(compositeUnity, SWT.CHECK);
		gd = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
		gd.grabExcessHorizontalSpace = true;
		btnSaveLoginUnity.setLayoutData(gd);
		btnSaveLoginUnity.setText("Save login");
		btnSaveLoginUnity.setSelection(unityStorePassword);

		// filler rows - TODO handle layout better!
		for(int i=0;i<12;i++){
			new Label(compositeUnity, SWT.NONE);
		}
	}


	public CredentialChoice getCredentialChoice() {
		if (btnKeystore.getSelection()) {				
			return CredentialChoice.KEYSTORE;
		}
		else if (btnUnity.getSelection()) {	
			return CredentialChoice.UNITY;
		}
		throw new IllegalStateException();
	}

	/**
	 * Helper to open the file chooser dialog.
	 * 
	 * @param startingDirectory
	 *            the directory to open the dialog on.
	 * @return File The File the user selected or <code>null</code> if they do
	 *         not.
	 */
	private File getFile(String path) {
		FileDialog dialog = new FileDialog(shell, SWT.OPEN | SWT.SHEET);
		dialog.setFilterExtensions(Constants.REUSABLE_KEYSTORE_FORMATS);
		File f = new File(path);
		File parent = f.getParentFile();
		if (parent!=null && parent.exists() && parent.canRead()) {
			path = parent.getAbsolutePath() + File.separator;
		} else {
			path = null;
		}
		dialog.setFilterPath(path);
		String file = dialog.open();
		if (file != null) {
			file = file.trim();
			if (file.length() > 0) {
				return new File(file);
			}
		}
		return null;
	}

	public boolean validateUserInput() {
		CredentialChoice choice = getCredentialChoice();
		boolean OK = false;
		switch (choice) {
		case KEYSTORE:
			OK = validateUserInputKeystore();
			OK = OK && validateUserInputMyProxy();
			break;
		case UNITY:
			OK = validateUserInputUnity();
			break;

		default:
			break;
		}

		// return overall authentication status (if true, splash screen will be exited)
		return OK;
	}

	private boolean validateUserInputUnity(){
		unityHost = fTextUnityURL.getText();
		// at least we should have a valid URL
		try{
			new URI(unityHost);
		}catch(URISyntaxException use){
			fTextUnityURL.setFocus();
		}
		unityUsername= fTextUnityUsername.getText();
		unityPassword = fTextUnityPassword.getText();
		unityStorePassword = btnSaveLoginUnity.getSelection();
		return unityHost!=null && unityUsername!=null && unityPassword!=null;
	}

	private boolean validateUserInputMyProxy(){
		if(isMyProxyEnabled()){
			myProxyHost = fTextMyProxyURL.getText();
			myProxyUser = fTextMyProxyUser.getText();
			myProxyPassword = fTextMyProxyPassword.getText();
			return myProxyHost!=null && myProxyUser!=null && myProxyPassword!=null;
		}
		else {
			return true;
		}
	}

	/**
	 * check user input for the "keystore" choice
	 */
	private boolean validateUserInputKeystore() {
		String keystore = fTextKeystore.getText();
		if (keystore.isEmpty()) {
			fTextKeystore.setFocus();
			return false;
		}
		keystorePassword = fTextPassword.getText();
		File f = new File(keystore);
		if (!f.exists()) {
			MessageDialog message = new MessageDialog(shell, "Note",
					null, IdentityActivator.QUESTION_CREATE_KEYSTORE + keystore
					+ "?", MessageDialog.QUESTION, new String[] { "Ok",
			"Cancel" }, 0);
			int result = message.open();
			boolean canceled = Window.CANCEL == result;
			if (canceled) {
				fTextKeystore.setFocus();
				return false;
			} else {
				try {
					keystorePassword = new InputUtils().askPassword(f);
					KeyStoreHelper.create(f, keystorePassword);
				} catch (Exception e) {
					MessageDialog.openError(shell,
							"Error opening keystore",
							"Error opening keystore: "+e.getMessage());
					return false;
				}
			}
		} else if (keystorePassword.isEmpty()) {
			fTextPassword.setFocus();
			return false;
		}

		// file exists and we have a password
		try {
			KeyStoreHelper.tryKeystore(f, keystorePassword);
			// OK - store preferences now
			storePassword = fButtonStorePassword.getSelection();
			keystorePath = keystore;
		} catch (Throwable e) {
			MessageDialog.openError(shell,"Error opening keystore",
					"Error opening keystore: "+e.getMessage());
			fTextKeystore.setFocus();
			return false;
		}
		return true;
	}

	public String getKeystorePath() {
		return keystorePath;
	}

	public void setKeystorePath(String keystorePath) {
		this.keystorePath = keystorePath;
	}

	public String getKeystorePassword() {
		return keystorePassword;
	}

	public void setKeystorePassword(String keystorePassword) {
		this.keystorePassword = keystorePassword;
	}

	public boolean isStorePassword() {
		return storePassword;
	}

	public void setStorePassword(boolean storePassword) {
		this.storePassword = storePassword;
	}

	public String getMyProxyHost() {
		return myProxyHost;
	}

	public void setMyProxyHost(String myProxyHost) {
		this.myProxyHost = myProxyHost;
	}

	public String getMyProxyUser() {
		return myProxyUser;
	}

	public void setMyProxyUser(String myProxyUser) {
		this.myProxyUser = myProxyUser;
	}

	public String getMyProxyPassword() {
		return myProxyPassword;
	}

	public void setMyProxyPassword(String myProxyPassword) {
		this.myProxyPassword = myProxyPassword;
	}

	public boolean isMyProxyEnabled() {
		return myProxyEnabled;
	}


	public String getUnityHost() {
		return unityHost;
	}

	public void setUnityHost(String unityHost) {
		this.unityHost = unityHost;
	}

	public String getUnityPassword() {
		return unityPassword;
	}

	public void setUnityPassword(String unityPassword) {
		this.unityPassword = unityPassword;
	}

	public String getUnityUsername() {
		return unityUsername;
	}

	public void setUnityUsername(String unityUsername) {
		this.unityUsername = unityUsername;
	}

	public void setInitiallyUseKeystore(boolean initiallyUseKeystore) {
		this.initiallyUseKeystore = initiallyUseKeystore;
	}

	public void setUnityStorePassword(boolean storePassword) {
		this.unityStorePassword = storePassword;
	}
}
