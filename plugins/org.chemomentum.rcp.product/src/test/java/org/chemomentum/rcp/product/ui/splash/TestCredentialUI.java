package org.chemomentum.rcp.product.ui.splash;


import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class TestCredentialUI {

	public static void main (String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		System.setProperty("osgi.install.area","file:///tmp");
		Composite panel = new Composite(shell, SWT.NONE);
		GridLayout layout = new GridLayout(CredentialUI.F_COLUMN_COUNT, false);
		panel.setLayout(layout);
		CredentialUI cui= new CredentialUI(shell); 
		cui.addContent(panel);
		panel.pack();
		shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) display.sleep ();
		}
		display.dispose();
	}
	
}