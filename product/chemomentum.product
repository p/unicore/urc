<?xml version="1.0" encoding="UTF-8"?>
<?pde version="3.5"?>

<product name="UNICORE Rich Client" uid="org.chemomentum.rcp.product.default" id="org.chemomentum.rcp.product.default" application="de.fzj.unicore.rcp.standalone.application" version="7.3.5" useFeatures="true" includeLaunchers="true">

   <aboutInfo>
      <image path="/org.chemomentum.rcp.product/Chemomentum_ColorLogo150.gif"/>
      <text>
         UNICORE Rich Client

      Version:
      7.1.1

      This software was originally 
      provided by the Chemomentum Project.
      
      It is based on the UNICORE Grid
      Middleware
      (see http://www.unicore.eu)
      and was developed by
      Forschungszentrum Juelich GmbH,
      ICM Warsaw and others.
      </text>
   </aboutInfo>

   <configIni use="default">
   </configIni>

   <launcherArgs>
      <vmArgs>-Xms128m
      </vmArgs>
   </launcherArgs>

   <windowImages i16="/org.chemomentum.rcp.product/icons/unicore_o_16x16.gif" i32="/org.chemomentum.rcp.product/icons/unicore_o_32x32.gif" i48="/org.chemomentum.rcp.product/icons/unicore_o_48x48.gif" i64="/org.chemomentum.rcp.product/icons/unicore_o_64x64.gif" i128="/org.chemomentum.rcp.product/icons/unicore_o_128x128.gif"/>

   <splash
      location="org.chemomentum.rcp.product" />
   <launcher name="UNICORE_Rich_Client">
      <linux icon="../plugins/org.chemomentum.rcp.product/icons/unicore_linux_icon.xpm"/>
      <macosx icon="../plugins/org.chemomentum.rcp.product/icons/unicore_mac_icon.icns"/>
      <solaris/>
      <win useIco="true">
         <ico path="../plugins/org.chemomentum.rcp.product/icons/unicore_windows_icon.ico"/>
         <bmp/>
      </win>
   </launcher>

   <intro introId="org.chemomentum.rcp.help.basic.intro"/>

   <vm>
   </vm>

   <license>
        <url>http://www.unicore.eu/</url>
        <text>
   Copyright (c) 2006-2015, Forschungszentrum Jülich GmbH
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
&quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
         </text>
   </license>

   <plugins>
      <plugin id="com.ibm.icu"/>
      <plugin id="de.fzj.unicore.rcp.common"/>
      <plugin id="de.fzj.unicore.rcp.gpe4eclipse"/>
      <plugin id="de.fzj.unicore.rcp.identity"/>
      <plugin id="de.fzj.unicore.rcp.logmonitor"/>
      <plugin id="de.fzj.unicore.rcp.servicebrowser"/>
      <plugin id="de.fzj.unicore.rcp.standalone"/>
      <plugin id="de.fzj.unicore.rcp.wfeditor"/>
      <plugin id="javax.servlet"/>
      <plugin id="org.apache.ws.security.wss4j"/>
      <plugin id="org.chemomentum.rcp.common"/>
      <plugin id="org.chemomentum.rcp.dataManagement"/>
      <plugin id="org.chemomentum.rcp.servicebrowser"/>
      <plugin id="org.chemomentum.rcp.tracegui"/>
      <plugin id="org.eclipse.ant.core"/>
      <plugin id="org.eclipse.core.boot"/>
      <plugin id="org.eclipse.core.commands"/>
      <plugin id="org.eclipse.core.contenttype"/>
      <plugin id="org.eclipse.core.databinding"/>
      <plugin id="org.eclipse.core.expressions"/>
      <plugin id="org.eclipse.core.filebuffers"/>
      <plugin id="org.eclipse.core.filesystem"/>
      <plugin id="org.eclipse.core.filesystem.linux.ppc" fragment="true"/>
      <plugin id="org.eclipse.core.filesystem.linux.x86" fragment="true"/>
      <plugin id="org.eclipse.core.filesystem.linux.x86_64" fragment="true"/>
      <plugin id="org.eclipse.core.filesystem.macosx" fragment="true"/>
      <plugin id="org.eclipse.core.filesystem.solaris.sparc" fragment="true"/>
      <plugin id="org.eclipse.core.filesystem.win32.x86" fragment="true"/>
      <plugin id="org.eclipse.core.jobs"/>
      <plugin id="org.eclipse.core.net"/>
      <plugin id="org.eclipse.core.resources"/>
      <plugin id="org.eclipse.core.resources.win32" fragment="true"/>
      <plugin id="org.eclipse.core.runtime"/>
      <plugin id="org.eclipse.core.variables"/>
      <plugin id="org.eclipse.draw2d"/>
      <plugin id="org.eclipse.equinox.app"/>
      <plugin id="org.eclipse.equinox.common"/>
      <plugin id="org.eclipse.equinox.ds"/>
      <plugin id="org.eclipse.equinox.preferences"/>
      <plugin id="org.eclipse.equinox.registry"/>
      <plugin id="org.eclipse.equinox.util"/>
      <plugin id="org.eclipse.gef"/>
      <plugin id="org.eclipse.help"/>
      <plugin id="org.eclipse.jface"/>
      <plugin id="org.eclipse.jface.databinding"/>
      <plugin id="org.eclipse.jface.preference"/>
      <plugin id="org.eclipse.jface.text"/>
      <plugin id="org.eclipse.osgi"/>
      <plugin id="org.eclipse.osgi.services"/>
      <plugin id="org.eclipse.rse.core"/>
      <plugin id="org.eclipse.rse.services"/>
      <plugin id="org.eclipse.rse.ui"/>
      <plugin id="org.eclipse.search"/>
      <plugin id="org.eclipse.swt"/>
      <plugin id="org.eclipse.swt.carbon.macosx" fragment="true"/>
      <plugin id="org.eclipse.swt.gtk.linux.ppc" fragment="true"/>
      <plugin id="org.eclipse.swt.gtk.linux.x86" fragment="true"/>
      <plugin id="org.eclipse.swt.gtk.linux.x86_64" fragment="true"/>
      <plugin id="org.eclipse.swt.gtk.solaris.sparc" fragment="true"/>
      <plugin id="org.eclipse.swt.motif.aix.ppc" fragment="true"/>
      <plugin id="org.eclipse.swt.win32.win32.x86" fragment="true"/>
      <plugin id="org.eclipse.swt.wpf.win32.x86" fragment="true"/>
      <plugin id="org.eclipse.text"/>
      <plugin id="org.eclipse.ui"/>
      <plugin id="org.eclipse.ui.carbon" fragment="true"/>
      <plugin id="org.eclipse.ui.forms"/>
      <plugin id="org.eclipse.ui.ide"/>
      <plugin id="org.eclipse.ui.intro"/>
      <plugin id="org.eclipse.ui.views"/>
      <plugin id="org.eclipse.ui.views.properties.tabbed"/>
      <plugin id="org.eclipse.ui.win32" fragment="true"/>
      <plugin id="org.eclipse.ui.workbench"/>
      <plugin id="org.eclipse.ui.workbench.texteditor"/>
      <plugin id="org.eclipse.update.configurator"/>
      <plugin id="org.eclipse.update.core"/>
      <plugin id="org.eclipse.update.core.linux" fragment="true"/>
      <plugin id="org.eclipse.update.core.win32" fragment="true"/>
      <plugin id="org.eclipse.update.ui"/>
      <plugin id="woodstox-core-asl"/>
   </plugins>

   <features>
      <feature id="de.fzj.unicore.rcp.features.basic"/>
      <feature id="org.chemomentum.rcp.features.basic" version="7.3.5"/>
      <feature id="org.eclipse.help"/>
      <feature id="org.eclipse.equinox.p2.user.ui"/>
      <feature id="org.eclipse.e4.rcp"/>
   </features>

   <configurations>
      <plugin id="org.apache.cxf.cxf-rt-core" autoStart="true" startLevel="4" />
      <plugin id="org.eclipse.core.runtime" autoStart="true" startLevel="4" />
      <plugin id="org.eclipse.equinox.ds" autoStart="true" startLevel="1" />
      <plugin id="org.eclipse.equinox.util" autoStart="true" startLevel="1" />
      <plugin id="org.eclipse.osgi.services" autoStart="true" startLevel="1" />
   </configurations>

   <repositories>
      <repository location="http://unicore-dev.zam.kfa-juelich.de/eclipse/7.3/" enabled="true" />
   </repositories>

   <preferencesInfo>
      <targetfile overwrite="false"/>
   </preferencesInfo>

   <cssInfo>
   </cssInfo>

</product>
