UNICORE Rich Client README
==========================
UNICORE Team <unicore-support@lists.sourceforge.net> 
v7.3.5, November 2017

This is the README file for the UNICORE Rich Client, which comes as a
standalone rich client product based on Eclipse.

The UNICORE server is available as a separate download on the
UNICORE@Sourceforge page, http://sourceforge.net/projects/unicore

PREREQUISITES
-------------
- Oracle/OpenJDK Java 8 JRE or SDK for

INSTALLATION & Startup
----------------------
Unzip the client package that matches your platform.

Run 'UNICORE_Rich_Client' or 'UNICORE_Rich_Client.exe'. The client
should start with a splash screen. Inside the splash screen, your
credentials and truststore must be set up.  
For example, a Java keystore file (.jks) containing trusted X509
certificate authorities and your private key(s) must be selected. By
default, a keystore that contains everything you need in order to test
the client with a UNICORE quickstart server installation is
selected. This file is located inside the 'certs' folder in the
client's installation directory. The password of this keystore is
'321'.  A password matching the selected keystore must be provided in
the splash screen. Tick the corresponding checkbox if you would like
to store your password in an encrypted keyring file so you don't have
to enter it at each client start (not recomended).


.Command line arguments for customizing the startup process include:
[options="header", cols="literal,"]
|===============================================================================
|Argument			|	Effect

|-data workspacePath |	The path of the workspace on which to run the Eclipse
platform. The workspace location is also the default location for projects. 
Relative paths are interpreted relative to the directory that Eclipse was
started from. Remember to use double quotes or escape special characters that
the shell could try to interpret otherwise. 
													
|-vm vmPath			| The location of Java Runtime Environment (JRE) to use to
run the Eclipse platform. If not specified, the JRE is at jre, sibling of the
Eclipse executable. Relative paths are interpreted relative to the directory
that Eclipse was started from.
													
|-vmargs args		|	When passed to the Eclipse, this option is used to 
customize the operation of the Java VM used to run Eclipse. If specified, this
option must come at the end	of the command line. The given arguments are
dependant on VM that is being run.
|===============================================================================										

.Examples:
[frame="topbot",grid="none",cols="literal,"]
|=============================================================================== 
|UNICORE_Rich_Client.exe -data 'c:\projects'| Use 'c:\projects' as workspace dir
|UNICORE_Rich_Client.exe -vm 'c:\jre\bin'	| Java can be found at 'c:\jre\bin'
|UNICORE_Rich_Client.exe -vmargs -Xmx256M	| Allow heap to use up to 256 MB
|=============================================================================== 

Arguments for the virtual machine can also be specified in the file 
'UNICORE_Rich_Client.ini'.


Updating from previous versions
-------------------------------

Due to changes in the software, you might run into some issues when 
updating from a previous client version.

If you are using an existing keystore, it might be required to clean it
up, especially removing duplicate certificates, as the new client is a bit more 
restrictive here. If you experience any SSL related issues, please check your 
keystore.

If you use a workspace created using a previous client version, you might 
encounter messages such as "Unable to deserialize cached Grid service 
information". In such a case, simply refresh the Grid browser.


GETTING STARTED
---------------
On first client startup, you will see the welcome screen which provides links
to important functions and additional information. This screen can always be
reached by selecting 'Help -> Welcome' via the window's main toolbar.

In order to connect to a UNICORE server, you need to provide the URL of a
UNICORE registry which comes with every UNICORE server installation and
contains addresses of UNICORE server components. Please do so by leaving the
welcome screen and using the context menu of the 'Grid Browser' view. Select the
'add Registry' menu item and enter a URL and name for the registry. 
Now double-click the Registry in order to list the registered services on the
Grid. If the Registry node turns red or does not list any children, the Registry
is either empty or the client could not connect to it. If this happens, please
check your security setup and the URL that you entered.
Please make sure that a default certificate (that is presented when connecting
to UNICORE services) is selected in the 'Keystore' view.
This view can also be used for importing additional private keys from other
keystores and exporting certificates.
In order to obtain more information about a listed service, right click it and
select 'details' from the context menu that should appear. The Registry should
contain at least one or more execution services, depicted by small computer
icons.

In order to prepare a single job for execution on a UNICORE target system
service, please right-click such a service in the 'Grid Browser' view and select
'create job' from the context menu. After providing parameters for the job,
click the green submit button in the global toolbar. Job outcomes can be
fetched with the button right next to the submit button, or from the context
menu of the job node in the 'Grid Browser' view.


Please note, that a standard UNICORE installation does not provide the full 
UNICORE workflow functionality. Workflow engine server components are being
released separately. The client's workflow capabilities do rely on the 
availability of these server components in the Registry.For creating a workflow
which can be edited and submitted to a workflow engine, use the 
'File -> New -> Workflow Project' menu entry and enter a name for the workflow
project to be created.

A more complete client manual with instructions can be accessed by selecting the
'Help -> Help Contents' menu entry.


REPORTING BUGS
--------------
In case you find any bugs, please use the bug trackers of the UNICORE project at 

http://sourceforge.net/p/unicore/urc-issues/

You may also send an email to the support list: 

unicore-support@lists.sf.net


CONTACTS
--------
- Web site: http://www.unicore.eu/
- Sourceforge project site: http://sourceforge.net/projects/unicore/
- Questions, suggestions, problems: unicore-support@lists.sf.net
- Developers chat: unicore-devel@lists.sf.net (subscription required)
